<?php
    set_time_limit(0);
    require_once('../config.php');
    require_once($CFG->libdir.'/adminlib.php');
    require_once($CFG->libdir.'/authlib.php');
    require_once($CFG->dirroot.'/user/filters/lib.php');
    require_once($CFG->dirroot.'/user/lib.php');
    require_once($CFG->dirroot.'/user/downloaduser.php');
    require_once($CFG->dirroot . '/local/user/selector/lib.php');

	//Check User is logged in or not. If not, redirect to home page.
	require_login();

	if($USER->archetype == $CFG->userTypeStudent){  //redirect if learner
	   redirect($CFG->wwwroot);
	}	
	
	//Request parameters
    $delete       = optional_param('delete', 0, PARAM_INT);
    $confirm      = optional_param('confirm', '', PARAM_ALPHANUM);   //md5 confirmation hash
    $confirmuser  = optional_param('confirmuser', 0, PARAM_INT);
    $sort         = optional_param('sort', 'name', PARAM_ALPHANUM);
    $dir          = optional_param('dir', 'ASC', PARAM_ALPHA);
    $page         = optional_param('page', 1, PARAM_INT);
    $perpage      = optional_param('perpage', $CFG->perpage, PARAM_INT);        // how many per page
    $ru           = optional_param('ru', '2', PARAM_INT);            // show remote users
    $lu           = optional_param('lu', '2', PARAM_INT);            // show local users
    $acl          = optional_param('acl', '0', PARAM_INT);           // id of user to tweak mnet ACL (requires $access)
    $suspend      = optional_param('suspend', 0, PARAM_INT);
    $unsuspend    = optional_param('unsuspend', 0, PARAM_INT);
    $unlock       = optional_param('unlock', 0, PARAM_INT);
    $download       = optional_param('download', 0, PARAM_INT);
    $filename_download       = optional_param('filename','' , PARAM_RAW);
    $sIsManagerYes =optional_param('is_manager_yes', '-1', PARAM_RAW);
     
    $sIsManagerYesArr=explode("@",$sIsManagerYes);
    $pageURL = '/admin/user.php';
    checkPageIsNumeric($pageURL,$_REQUEST['page']);
    
    setBackUrlForUserSection();
      
  
	// Download sample script start
	if($download != 0){
		global $CFG;
		if($download == 1){
			$file = $CFG->dirroot."/user/download/sample_user.csv";
		}else if($download == 2){
			$file = $CFG->dirroot."/user/download/sample_user.csv";
			//$file = $CFG->dirroot."/user/download/sample_user_department.csv";
		}else if($download == 3){
			$file = $CFG->dirroot."/user/download/sample_user.csv";
			//$file = $CFG->dirroot."/user/download/sample_user_department_team.csv";
		}elseif($download == 4){
			if($filename_download != ''){
				$file = $CFG->dirroot."/user/download/user_log//".$filename_download;
			}
		}
		download($file);
	}
	$userUploadResult = array();
	if($_POST){
		$file_name = time().'_'.basename($_FILES["user_list"]["name"]);
		$allowed =  array('csv');
		$filename = $file_name;
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		
		$target_dir = $CFG->dirroot.'/user/upload//'.$file_name;
                
          
		if (in_array($ext,$allowed) && $_FILES["user_list"]["error"] == 0 && move_uploaded_file($_FILES["user_list"]["tmp_name"], $target_dir)) {
			$userUploadResult = upload_user($target_dir);
			//pr($userUploadResult);die;
			$_SESSION['upload_result'] = $userUploadResult;
			if($userUploadResult['status'] == 1){
				$_SESSION['update_msg'] = get_string('upload_success');
			}elseif($userUploadResult['status'] == 2){
				$_SESSION['update_msg'] = $userUploadResult['message'];
				$_SESSION['error_class'] = 'error_bg';
				$_SESSION['upload_result'] = '';
				$userUploadResult = array();
			}else{
				/*$_SESSION['update_msg']  = "Upload Status: </br>";
				$total = $userUploadResult['upload_log']['success'] + $userUploadResult['upload_log']['fail'];
				$_SESSION['update_msg']  .= "Total: ".$total.'</br>';
				$_SESSION['update_msg']  .= "Success: ".$userUploadResult['upload_log']['success'].'</br>';
				$_SESSION['update_msg']  .= "Failure: ".$userUploadResult['upload_log']['fail'].'</br>';
				$_SESSION['update_msg'] .= get_string('error_complete_user').'<a href="'.$CFG->wwwroot.'/admin/user.php?download=4&filename='.$userUploadResult['message'].'" class="download-sample">Here</a>';*/
			}
			unlink($target_dir);
		}else{
			$_SESSION['update_msg'] = get_string('error_complete_user_no_file');
		}
		redirect($CFG->wwwroot.'/admin/user.php');
	}
	// Download sample script ends
	// Get site data for page configuration
    $sitecontext = context_system::instance();
    $site = get_site();
	// Get site data for page configuration ends here


	//Site header
	$header = "$SITE->shortname";

	// Start setting up the page
	$params = array();
	$context = get_context_instance(CONTEXT_SYSTEM);
	$PAGE->set_context($context);
	$PAGE->set_url('/admin/user.php', $params);
	//$PAGE->set_pagelayout('listing');
        $PAGE->set_pagelayout('reportlayout');
	$PAGE->set_pagetype('my-index');
	$PAGE->blocks->add_region('content');
	$PAGE->set_title($header);
	$PAGE->set_heading($header);
	// end setting up the page

	//Check user have permission to access this page : set through admin(site)
    if (!has_capability('moodle/user:update', $sitecontext) and !has_capability('moodle/user:delete', $sitecontext)) {
        print_error('nopermissions', 'error', '', 'edit/delete users');
    }

	// moodle strings used in code
    $stredit   = get_string('edit');
    $strdelete = get_string('delete');
    $strdeletecheck = get_string('deletecheck');
    $strshowallusers = get_string('showallusers');
    $strsuspend = get_string('suspenduser', 'admin');
    $strunsuspend = get_string('unsuspenduser', 'admin');
    $strunlock = get_string('unlockaccount', 'admin');
    $strconfirm = get_string('confirm');


	//get Site url
    if (empty($CFG->loginhttps)) {
        $securewwwroot = $CFG->wwwroot;
    } else {
        $securewwwroot = str_replace('http:','https:',$CFG->wwwroot);
    }

    //$returnurl = new moodle_url('/admin/user.php', array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'page'=>$page));  - default

	//current page url
	
	
	// start- parameters for common search and other links
	$paramArray = array(
						'ch' => optional_param('ch', '', PARAM_ALPHA),
						'key' => optional_param('key', '', PARAM_RAW),
						'username' => optional_param('username', '', PARAM_RAW),
						'name' => optional_param('name', '', PARAM_RAW),
						'email' => optional_param('email', '', PARAM_RAW),
						'role' => optional_param('role', '', PARAM_RAW),
						'phone' => optional_param('phone', '', PARAM_RAW),
						'department' => optional_param('department', '', PARAM_RAW),
						'team' => optional_param('team', '', PARAM_RAW),
						'managers' => optional_param('managers', '', PARAM_RAW),
						'roles' => optional_param('roles', '', PARAM_RAW),
						'sel_mode' => optional_param('sel_mode', 1, PARAM_RAW),
						'user_group' => optional_param('user_group', '', PARAM_RAW),
						'is_active' => optional_param('is_active', 1, PARAM_RAW),
						'company' => optional_param('company', '', PARAM_RAW),
						'job_title' => optional_param('job_title', '', PARAM_RAW),
                                                'city' => optional_param('city', '-1', PARAM_RAW),
                                                'report_to' => optional_param('report_to', '-1', PARAM_RAW),
                                                'is_manager_yes' => $sIsManagerYes,
					  );
	$filterArray = array(							
						'name'=>get_string('name'),
						'username'=>get_string('username'),
						'email'=>get_string('email'),
						'role'=>get_string('role'),
						'phone'=>get_string('phone'),
						'company'=>get_string('company'),
						'job_title'=>get_string('job_title'),
				   );
	// end- parameters for common search and other links

	// Get return page url
	$removeKeyArrayInactiveActive = array('id','flag');
	$returnurl = genParameterizedURL($paramArray, $removeKeyArrayInactiveActive, $pageURL);


	// Moodle default functionality to suspend,unsuspend and delete start here
    if ($confirmuser and confirm_sesskey()) {
        require_capability('moodle/user:update', $sitecontext);
        if (!$user = $DB->get_record('user', array('id'=>$confirmuser, 'mnethostid'=>$CFG->mnet_localhost_id))) {
            print_error('nousers');
        }

        $auth = get_auth_plugin($user->auth);

        $result = $auth->user_confirm($user->username, $user->secret);

        if ($result == AUTH_CONFIRM_OK or $result == AUTH_CONFIRM_ALREADY) {
            redirect($returnurl);
        } else {
            echo $OUTPUT->header();
            redirect($returnurl, get_string('usernotconfirmed', '', fullname($user, true)));
        }

    } else if ($delete and confirm_sesskey()) { 
    	// Delete a selected user, after confirmation
    	
        require_capability('moodle/user:delete', $sitecontext);
		checkUserAccess('user' , $delete);
        $user = $DB->get_record('user', array('id'=>$delete, 'mnethostid'=>$CFG->mnet_localhost_id), '*', MUST_EXIST);
		$PAGE->navbar->add(get_string('manage_user'),new moodle_url('/admin/user.php'));
		$PAGE->navbar->add(get_string('deleteuser', 'admin'));
        if (is_siteadmin($user->id)) {
            print_error('useradminodelete', 'error');
        }

        if ($confirm != md5($delete)) {
            echo $OUTPUT->header();
            $fullname = fullname($user, true);
            echo $OUTPUT->heading(get_string('deleteuser', 'admin'));
            $optionsyes = array('delete'=>$delete, 'confirm'=>md5($delete), 'sesskey'=>sesskey());
            echo $OUTPUT->confirm(get_string('deletecheckfull', '', "'$fullname'"), new moodle_url($returnurl, $optionsyes), $returnurl);
            echo $OUTPUT->footer();
            die;
        } else if (!$user->deleted) {
            if (delete_user($user)) {
				$_SESSION['update_msg']=get_string('record_deleted');
                \core\session\manager::gc(); // Remove stale sessions.
                redirect($returnurl);
            } else {
                \core\session\manager::gc(); // Remove stale sessions.
                echo $OUTPUT->header();
                echo $OUTPUT->notification($returnurl, get_string('deletednot', '', fullname($user, true)));
            }
        }
    } else if ($acl and confirm_sesskey()) {
        if (!has_capability('moodle/user:update', $sitecontext)) {
            print_error('nopermissions', 'error', '', 'modify the NMET access control list');
        }
        if (!$user = $DB->get_record('user', array('id'=>$acl))) {
            print_error('nousers', 'error');
        }
        if (!is_mnet_remote_user($user)) {
            print_error('usermustbemnet', 'error');
        }
        $accessctrl = strtolower(required_param('accessctrl', PARAM_ALPHA));
        if ($accessctrl != 'allow' and $accessctrl != 'deny') {
            print_error('invalidaccessparameter', 'error');
        }
        $aclrecord = $DB->get_record('mnet_sso_access_control', array('username'=>$user->username, 'mnet_host_id'=>$user->mnethostid));
        if (empty($aclrecord)) {
            $aclrecord = new stdClass();
            $aclrecord->mnet_host_id = $user->mnethostid;
            $aclrecord->username = $user->username;
            $aclrecord->accessctrl = $accessctrl;
            $DB->insert_record('mnet_sso_access_control', $aclrecord);
        } else {
            $aclrecord->accessctrl = $accessctrl;
            $DB->update_record('mnet_sso_access_control', $aclrecord);
        }
        $mnethosts = $DB->get_records('mnet_host', null, 'id', 'id,wwwroot,name');
        redirect($returnurl);

    } else if ($suspend and confirm_sesskey()) {
        require_capability('moodle/user:update', $sitecontext);

        if ($user = $DB->get_record('user', array('id'=>$suspend, 'mnethostid'=>$CFG->mnet_localhost_id, 'deleted'=>0))) {
            if (!is_siteadmin($user) and $USER->id != $user->id and $user->suspended != 1) {
                $user->suspended = 1;
                // Force logout.
                \core\session\manager::kill_user_sessions($user->id);
				$userData = new stdClass();
				$userData->suspended = 1;
				$userData->id = $suspend;
				$DB->update_record('user',$userData);
               // user_update_user($user, false);
            }
        }
		$_SESSION['update_msg']=get_string('record_updated');
        redirect($returnurl);

    } else if ($unsuspend and confirm_sesskey()) {
        require_capability('moodle/user:update', $sitecontext);

        if ($user = $DB->get_record('user', array('id'=>$unsuspend, 'mnethostid'=>$CFG->mnet_localhost_id, 'deleted'=>0))) {
            if ($user->suspended != 0) {
                $user->suspended = 0;
				$userData = new stdClass();
				$userData->suspended = 0;
				$userData->id = $unsuspend;
				$DB->update_record('user',$userData);
				resetUserPassword($unsuspend,1);
               // user_update_user($user, false);
            }
        }
	$_SESSION['update_msg']=get_string('record_updated');
        redirect($returnurl);

    } else if ($unlock and confirm_sesskey()) {
        require_capability('moodle/user:update', $sitecontext);

        if ($user = $DB->get_record('user', array('id'=>$unlock, 'mnethostid'=>$CFG->mnet_localhost_id, 'deleted'=>0))) {
            login_unlock_account($user);
        }
        redirect($returnurl);
    }
	// Moodle default functionality to suspend,unsuspend and delete ends here

	//set navigation bar
	$PAGE->navbar->add(get_string('manage_user'));
    echo $OUTPUT->header(); // output header

	//print common search form.
	echo genCommonSearchForm($paramArray, $filterArray, $pageURL);

	//Start - get parameterized  page url
	$removeKeyArray = array('id','flag','perpage');
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);

	// Start - get parameterized  page url for activate and deactivate
	$removeKeyArrayInactiveActive = array('id','flag');
	$genURLInactiveActive = genParameterizedURL($paramArray, $removeKeyArrayInactiveActive, $pageURL);
	// End - get parameterized  page url for activate and deactivate

	//End - get parameterized  page url
	
	// add user button
	global $DB,$USER;
	
	
	if($USER->archetype == $CFG->userTypeManager){
		$downloadLink = 2;
		$departmentStyle = "";
		$departmentselectOptions = '';
		$options = '<option value = "2">Upload Department</option>
					<option value = "3">Upload team</option>';
		$userDepartment = $DB->get_record_sql("SELECT d.id,d.title FROM mdl_department_members as dm LEFT JOIN mdl_department as d on d.id = dm.departmentid WHERE userid = ".$USER->id);
		if(!empty($userDepartment)){
			$departmentselectOptions = "<option value = ".$userDepartment->id.">".$userDepartment->title."</option>";
		}
	}else{
		$downloadLink = 1;
		$departmentStyle = "style = 'display:none;'";
		$departments = $DB->get_records_sql("SELECT id,title FROM {$CFG->prefix}department WHERE deleted = 0 AND status = 1 order by display_order ASC");
		$departmentselectOptions = '';
		if($departments){
			foreach($departments as $department){
				$departmentselectOptions .= "<option value = ".$department->id.">".$department->title."</option>";
			}
		}
		$options = '<option value = "1">Upload All</option>
						<option value = "2">Upload Department</option>
						<option value = "3">Upload team</option>';
	}
	if($CFG->isLdap!=1){
	echo '<div class="add-course-button"><a href="'.$securewwwroot.'/user/editadvanced.php?id=-1" class="button-link add-user"><i></i>'.get_string('adduser').'</a></div>';
	echo '<div class="upload-course-button"><a href="javascript:void(0)" class="button-link upload-user"><i></i>'.get_string('upload_user').'</a></div>';
	}
	echo "<div class = 'upload-user-div'>";
		echo '<a href="'.$securewwwroot.'/admin/user.php?download='.$downloadLink.'" class="download-sample">'.get_string('download_sample').'</a>';
		echo '<form name = "upload_user" id = "upload_user" method = "POST" enctype="multipart/form-data">
					<!--<select name = "upload_type" id = "upload_type">
						'.$options.'
					</select>
					<select name = "departments" id = "departments" '.$departmentStyle.'><option value = "0">Select Department</option>
						'.$departmentselectOptions.'
					</select>
					<select name = "department-teams[]" id = "department-teams" multiple>
					</select> -->
					<div id="user_listOuter"><input type = "file" name = "user_list" id = "user_list"></div>
					<input type = "hidden" name = "upload_type" value = "1">
					<input type = "button" name = "upload" value = "upload" id = "upload-submit">
				</form>';
	echo '</div>';
	if(!empty($_SESSION['upload_result'])){
		$userUploadResult = $_SESSION['upload_result'];
		$_SESSION['upload_result'] = '';
		$fileDownload = $userUploadResult['message'];
		$userUploadResult = $userUploadResult['viewLog'];
		$errorCount = $userUploadResult['error']['users']['manager']+ $userUploadResult['error']['users']['learner'];
		?>
        <div class="clear"></div>
		<div class = "status_report_main">
			<?php
				if(count($userUploadResult['error']['log'])>0){
			?>
			<div class = "download_report">
				<span> <?php echo get_string('error_complete_user').'<U><a href="'.$CFG->wwwroot.'/admin/user.php?download=4&filename='.$fileDownload.'" class="download-sample">Here</a>.</U>'; ?></span>
			</div>
			<?php
				}
			?>
			<?php
			if(count($userUploadResult['error']['log'])>0){
			?>
				<div class = "error_block_main">
					<div class = "error_report_header">Error Report</div>
					<div class = "error_user_report">
						<ul class = "error_user_list">
							<li><?php echo $errorCount ; ?> user(s) not created/updated.
						<?php
							if(count($userUploadResult['error']['log'])>0){
								echo '<ul>';
								ksort($userUploadResult['error']['log']);
								foreach($userUploadResult['error']['log'] as $log){
									?>
									<li class = "error_user"><?php echo $log; ?></li>
									<?php
								}
								echo '</ul>';
							}
							?>
                            </li>
                            </ul>
                            
					</div>
				</div>
			<?php
			}
			?>
			<div class = "success_block_main">
				<div class = "success_report_header">Success Report</div>
				<div class = "success_report">
					<div class ="department_report">
						<ul class="department_list">
							<li><?php echo count($userUploadResult['success']['department']); ?> department(s) created.
						<?php
						if(count($userUploadResult['success']['department'])>0){
							echo '<ul>';
							foreach($userUploadResult['success']['department'] as $department){
								?>
								<li class = "department_name"><?php echo $department; ?></li>
								<?php
							}
							echo '</ul>';
						}
						?>
                        	</li>
                           </ul>
					</div>
					<div class = "team_report">
						<ul class = "team_list">
							<li><?php echo count($userUploadResult['success']['teams']); ?> team(s) created.
						<?php
						if(count($userUploadResult['success']['teams'])>0){
							echo '<ul>';
							foreach($userUploadResult['success']['teams'] as $teams){
								?>
								<li class = "team_name"><?php echo $teams; ?></li>
								<?php
							}
							echo '</ul>';
						}
						?>
                        	</li>
                        </ul>
					</div>
					<div class = "user_report">
						<span class = "user_header">
						<?php 
						$managerCount = $userUploadResult['success']['users']['manager'];
						$learnerCount = $userUploadResult['success']['users']['learner'];
						echo $managerCount + $learnerCount; ?> user(s) created.</span>
						<ul class = "user_list">
							<li class = "user_name"><?php echo $managerCount; ?> manager(s) created.</li>
							<li class = "user_name"><?php echo $learnerCount; ?> learner(s) created.</li>
						</ul>
					</div>
                                    
                                        <div class = "user_report">
						<span class = "user_header">
						<?php 
						$managerCount_updated = $userUploadResult['updated']['users']['manager'];
						$learnerCount_updated = $userUploadResult['updated']['users']['learner'];
						echo $managerCount_updated + $learnerCount_updated; ?> user(s) updated.</span>
						<ul class = "user_list">
							<li class = "user_name"><?php echo $managerCount_updated; ?> manager(s) updated.</li>
							<li class = "user_name"><?php echo $learnerCount_updated; ?> learner(s) updated.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
	ob_start();
	require_once($CFG->dirroot . '/local/includes/user_filter.php');
	$SEARCHHTML = ob_get_contents();
	ob_end_clean();

	echo $SEARCHHTML;
	?>
	
	<script>
	$(document).ready(function(){
		$('.upload-user').click(function(){
			$('.upload-user-div').toggle();
			$('.status_report_main').remove();
			$('.bg-success').remove();
		});
		$('.success_report_header').click(function(){
			$('.success_report').toggle();
		});
		$('.error_report_header').click(function(){
			$('.error_user_report').toggle();
		});
		/*$('#upload_type').on('change',function(){
			$('#departments').val(0);
			var href = $(".download-sample").attr('href');
			if($(this).val() == 1){
				$('#departments').hide();
				$('#department-teams').hide();
				href = href.replace('?download=2', '?download=1');
				href = href.replace('?download=3', '?download=1');
				$(".download-sample").attr('href',href);
			}else{
				if($(this).val() == 2){
					$('#departments').show();
					$('#department-teams').hide();
					href = href.replace('?download=1', '?download=2');
					href = href.replace('?download=3', '?download=2');
					$(".download-sample").attr('href',href);
				}else{
					$('#departments').show();
					$('#department-teams').hide();
					href = href.replace('?download=1', '?download=3');
					href = href.replace('?download=2', '?download=3');
					$(".download-sample").attr('href',href);
				}
			}
		});
		$('#departments').on('change',function(){
			var departmentId = $(this).val();
			var uploadType = $('#upload_type').val();
			if(departmentId != 0){
				if(uploadType == 3){
					$.ajax({
						url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
						type:'POST',
						data:'action=getDepartmentTeamList&departmentId='+departmentId,
						success:function(data){
							$("#department-teams").html(data);
							$('#department-teams').show();
						}
					});
				}else{
					$('#department-teams').hide();
				}
			}else{
				data = "<option value = '0'>Select team</select>";
				$("#department-teams").html(data);
			}
		});*/
		$('#upload-submit').click(function(){
			var filename = $("#user_list").val();
			if($.trim(filename) == ''){
				alert('Please select file');
				return false;
			}
			$("#upload_user").submit();
		});
		$(".delete_user").click(function(event){
			event.preventDefault();
			var userId = $(this).attr("rel");
			var linkHref = $(this).attr("href");
			$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
				type:'POST',
				data:'action=getManagerUsers&id='+userId,
				success:function(data){
					if(data == 1){
						var txt = "<?php echo get_string('userdeleteconfirm')?>";	
						var response = confirm(txt);
						
						if (response == true) {
							window.location.href = linkHref;
						}
						return false;
					}else{
						var txt = "<?php echo get_string('usercannotdelete')?>";	
						alert(txt);
						return false;
					}
				}
			});
		});


		$(".enabled-impersonate").click(function(){

			var txt = "<?php echo get_string('impursanute_confirmation')?>";
			
			
			var response = confirm(txt);
			
			if (response == true) {
			return true;
			}
			return false;
		});

	});
	</script>
 
	<?php
	// update msg text
	if($_SESSION['update_msg']!=''){
		echo '<div class="clear"></div>';
		echo '<div class="bg-success text-success text-center '.$_SESSION['error_class'].'" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
		echo '<div class="clear"></div>';
		$_SESSION['update_msg']='';
		$_SESSION['error_class']='';
	}
	echo '<div class = "borderBlock">';
    // Carry on with the user listing
    $context = context_system::instance();
	
    $extracolumns = get_extra_user_fields($context);
    $columns = array_merge(array('firstname', 'lastname'), $extracolumns,array('phone', 'country', 'lastaccess'));

    /*foreach ($columns as $column) {
        $string[$column] = get_user_field_name($column);
        if ($sort != $column) {
            $columnicon = "";
            if ($column == "lastaccess") {
                $columndir = "DESC";
            } else {
                $columndir = "ASC";
            }
        } else {
            $columndir = $dir == "ASC" ? "DESC":"ASC";
            if ($column == "lastaccess") {
                $columnicon = ($dir == "ASC") ? "sort_desc" : "sort_asc";
            } else {
                $columnicon = ($dir == "ASC") ? "sort_asc" : "sort_desc";
            }
            $columnicon = "<img class='iconsort' src=\"" . $OUTPUT->pix_url('t/' . $columnicon) . "\" alt=\"\" />";

        }
        $$column = "<a href=\"user.php?sort=$column&amp;dir=$columndir\">".$string[$column]."</a>$columnicon";
    }*/

    $override = new stdClass();
    $override->firstname = 'firstname';
    $override->lastname = 'lastname';
    $fullnamelanguage = get_string('fullnamedisplay', '', $override);
    if (($CFG->fullnamedisplay == 'firstname lastname') or
        ($CFG->fullnamedisplay == 'firstname') or
        ($CFG->fullnamedisplay == 'language' and $fullnamelanguage == 'firstname lastname' )) {
        $fullnamedisplay = "$firstname / $lastname";
        if ($sort == "name") { // If sort has already been set to something else then ignore.
            $sort = "firstname";
        }
    } else { // ($CFG->fullnamedisplay == 'language' and $fullnamelanguage == 'lastname firstname').
        $fullnamedisplay = "$lastname / $firstname";
        if ($sort == "name") { // This should give the desired sorting based on fullnamedisplay.
            $sort = "lastname";
        }
    }
	// Override name field column name.
	$fullnamedisplay = get_string('name', 'user');

    //list($extrasql, $params) = $ufiltering->get_sql_filter();
	
	/* added by rajesh to not to show the site admin (2) */	
	/*$userrole =  getUserRole($USER->id);
	if(in_array($userrole, $CFG->custommanagerroleid)){  // added by rajesh 
		//$extrasql .= "u.id !=".$USER->id;
		///$extrasqlcnt.= "id !=".$USER->id; 
	}*/

	// Start get User listing
    $users = get_users_listing($sort, $dir, $page, $perpage, '', '', '',$extrasql, $paramArray, $context);

    $userscount = get_users_listing('usercount', $dir, $page, $perpage, '', '', '',$extrasql, $paramArray, $context);
	$usercount =  $userscount->usercount;
	// end get User listing

    $strall = get_string('all');

    $baseurl = new moodle_url('/admin/user.php', array());
    //echo $OUTPUT->paging_bar($usercount, $page, $perpage, $baseurl);

    flush();

	// Print table
	$tableHtml = '<table class="table1"><thead><tr>';
	$tableHtml .= "<td width='25%'>$fullnamedisplay</td>";
	$tableHtml .= "<td width='17%'>".get_string('contact_details')."</td>";
        $tableHtml .= "<td width='8%'>".get_string('user_identifier')."</td>";
         $tableHtml .= "<td width='10%'>".get_string('city')."</td>";
	$tableHtml .= "<td width='5%'>".get_string('role')."</td>";
       
        
	$tableHtml .= "<td width='10%'>".get_string('noofonlineandclassroomcourses')."</td>";
	$tableHtml .= "<td width='25%'>".get_string('manage')."</td>";
	$tableHtml .= '</tr></thead>';

    if (!$users) { // if no user found.
        $match = array();
		$tableHtml .= '<tr><td colspan = "6">'.get_string('no_results').'</td></tr>';
        $table = NULL;

    } else {

        $countries = get_string_manager()->get_list_of_countries(false);
        if (empty($mnethosts)) {
            $mnethosts = $DB->get_records('mnet_host', null, 'id', 'id,wwwroot,name');
        }

        foreach ($users as $key => $user) {
            if (isset($countries[$user->country])) {
                $users[$key]->country = $countries[$user->country];
            }
        }
        if ($sort == "country") {  // Need to resort by full country name, not code
            foreach ($users as $user) {
                $susers[$user->id] = $user->country;
            }
            asort($susers);
            foreach ($susers as $key => $value) {
                $nusers[] = $users[$key];
            }
            $users = $nusers;
        }
        $allUserOnlineCourses = getOnlineCoursesCount();
        $allUserClassroomCourses = getClassroomCoursesCount();
        //pr($allUserCourses); die;
       
        foreach ($users as $user) {
		    $cntOnlineCourses = 0;
		    $cntClassroomCourses=0;
                    $userIdentifier = (trim($user->user_identifier))?trim($user->user_identifier):getMDash();
                    $city = (trim($user->city))?trim($user->city):getMDash();
		 /*    $uCoursesObj = new user_courses_selector('', array('userid' => $user->id));
	       $uCoursesArr = $uCoursesObj->getUserCourses();
		   // $uCoursesArr = getOnlineAndClassroomCoursesCount($user->id);
			$cntTotalCourses = count($uCoursesArr);
			$cntTotalCourses = 0; */
		    
			if($allUserOnlineCourses[$user->id]){
			$cntOnlineCourses = $allUserOnlineCourses[$user->id]->onlinecourse;			
			}
			if($allUserClassroomCourses[$user->id]){			
				$cntClassroomCourses = $allUserClassroomCourses[$user->id]->classroomcourse;					
			}
			$cntTotalCourses = $cntOnlineCourses+$cntClassroomCourses;
			/* $classroomCourses = $DB->get_records('scheduler_enrollment',array('userid'=>$user->id));
			$cntTotalClassroom = count($classroomCourses);
			$cntTotalCourses = $cntTotalCourses+$cntTotalClassroom; */
            $buttons = array();
            $lastcolumn = '';

            // suspend button
			$disabledclass = '';
			$assigncourses = get_string('assigncourses');
			$editDisable = 'edit';
			$suspendedButton = '';
			$linkEdit = new moodle_url($securewwwroot.'/user/viewuser.php', array('id'=>$user->id, 'course'=>$site->id));
			$linkAssignCourses = new moodle_url($securewwwroot.'/user/assigncourses.php', array('id'=>$user->id));
			$linkAssignCoursesDisableClass = '';
			$edit = 0;
			if($USER->archetype == $CFG->userTypeAdmin && $user->id != $USER->id){
				$edit = 1;
			}else{
				if($user->createdby == $USER->id && $user->id != $USER->id){
					$edit = 1;
				} else if($USER->is_primary == 1 && $user->id != $USER->id){
					$edit = 1;
				}
				if($user->department_id != $USER->department){
					$edit = 0;
				}
				/*if($user->user_role == $CFG->userTypeStudent && $user->createdby == $USER->id && $user->id != $USER->id){
					$edit = 1;
				} else if($user->user_role == $CFG->userTypeManager && $USER->is_primary == 1 && $user->id != $USER->id){
					$edit = 1;
				}*/
			}
            if (has_capability('moodle/user:update', $sitecontext)) {
                if (is_mnet_remote_user($user)) {
                    // mnet users have special access control, they can not be deleted the standard way or suspended
                    $accessctrl = 'allow';
                    if ($acl = $DB->get_record('mnet_sso_access_control', array('username'=>$user->username, 'mnet_host_id'=>$user->mnethostid))) {
                        $accessctrl = $acl->accessctrl;
                    }
                    $changeaccessto = ($accessctrl == 'deny' ? 'allow' : 'deny');
                    $suspendedButton = " (<a href=\"?acl={$user->id}&amp;accessctrl=$changeaccessto&amp;sesskey=".sesskey()."\">".get_string($changeaccessto, 'mnet') . " access</a>)";

                } else {
					if($edit == 1){
						if ($user->suspended) {
							$disabledclass = 'class="disable"';
							$editDisable = 'edit-disable';
							$linkEdit = 'javascript:void(0);';
							$linkAssignCourses = 'javascript:void(0);';
							$linkAssignCoursesDisableClass = ' disable';
							$suspendedButton= html_writer::link(new moodle_url($genURLInactiveActive, array('unsuspend'=>$user->id, 'sesskey'=>sesskey())), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/show'), 'alt'=>get_string("activate"), 'class'=>'')), array('title'=>get_string("activate"),'class'=>'enable'));
						} else {
							if ($user->id == $USER->id or is_siteadmin($user)) {
								// no suspending of admins or self!
							} else {
								$suspendedButton = html_writer::link(new moodle_url($genURLInactiveActive, array('suspend'=>$user->id, 'sesskey'=>sesskey())), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/hide'), 'alt'=>get_string("inactivate"), 'class'=>'')), array('title'=>get_string("inactivate"), 'class'=>'disable'));
							}
						}

						if (login_is_lockedout($user)) {
							$suspendedButton = html_writer::link(new moodle_url($genURLInactiveActive, array('unlock'=>$user->id, 'sesskey'=>sesskey())), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/unlock'), 'alt'=>$strunlock, 'class'=>'iconsmall')), array('title'=>$strunlock));
						}
					}
                }
            }

            // edit button
			//pr($USER);die;
			if($edit == 1){
				if (has_capability('moodle/user:update', $sitecontext)) {
					// prevent editing of admins by non-admins
					//if (is_siteadmin($USER) or !is_siteadmin($user)) {
					//if($USER->archetype == $CFG->userTypeAdmin || ($user->createdby == $USER->id || $USER->is_primary == 1)){
					if($CFG->isLdap!=1)
					{
						$buttons[] = html_writer::link($linkEdit, html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>$stredit, 'class'=>'')), array('title'=>$stredit, 'class'=>$editDisable));
					}
					else{
					//	$editDisable = 'view';
					//	$stredit = 'View';
						$buttons[] = html_writer::link($linkEdit, html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>$stredit, 'class'=>'')), array('title'=>$stredit, 'class'=>$editDisable));
					}
					//}
				}
			}
			
			if($USER->archetype == 'admin' and $CFG->impersonate=='1' and $user->suspended=='0'){
				$linkImpesunate = $CFG->wwwroot.'/local/autologin.php?user_id='.$user->id;
				$buttons[] = '<a title="'.get_string("impursanute").'" class="impersonate enabled-impersonate" href="'.$linkImpesunate.'">'.get_string("impursanute").'</a> ';
			}

			$buttons[] = html_writer::link($linkAssignCourses, html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/group'), 'alt'=>$assigncourses, 'class'=>'iconsmall')), array('title'=>$assigncourses, 'class'=>'assign-courses'.$linkAssignCoursesDisableClass));
			if($suspendedButton != ''){
			    if($edit == 1){
			    	if($CFG->isLdap!=1)
			    	{
				  		$buttons[] = $suspendedButton;
			    	}
				}  
			}
			
			// delete button
			if($edit == 1){
				if (has_capability('moodle/user:delete', $sitecontext)) {
					/*if (is_mnet_remote_user($user) or $user->id == $USER->id or is_siteadmin($user)) {
						// no deleting of self, mnet accounts or admins allowed
					} else {
						$buttons[] = html_writer::link(new moodle_url($returnurl, array('delete'=>$user->id, 'sesskey'=>sesskey())), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/delete'), 'alt'=>$strdelete, 'class'=>'')), array('title'=>$strdelete, 'class'=>'delete'));
					}*/
					
					//if(($USER->archetype == $CFG->userTypeAdmin || ($user->createdby == $USER->id || $USER->is_primary == 1) ) && $user->id !=  $USER->id){
					if($CFG->isLdap!=1)
					{
						if($cntTotalCourses==0){
						$buttons[] = html_writer::link(new moodle_url($returnurl, array('delete'=>$user->id, 'sesskey'=>sesskey(),'confirm'=>md5($user->id))), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/delete'), 'alt'=>$strdelete, 'class'=>'')), array('title'=>$strdelete, 'class'=>'delete delete_user', 'rel'=>$user->id));
						//$buttons[] = html_writer::link("#", html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/delete'), 'alt'=>$strdelete, 'class'=>'')), array('title'=>$strdelete, 'class'=>'delete delete_user', 'rel'=>$user->id));
						}
						else{
							$buttons[] = html_writer::link(new moodle_url('javascript:void(0)', array('delete'=>$user->id, 'sesskey'=>sesskey(),'confirm'=>md5($user->id))), '', array('title'=>$strdelete, 'class'=>'delete-disabled'));
						}
					}
					//}
				}
			}
			
            // the last column - confirm or mnet info
            if (is_mnet_remote_user($user)) {
                // all mnet users are confirmed, let's print just the name of the host there
                if (isset($mnethosts[$user->mnethostid])) {
                    $lastcolumn = get_string($accessctrl, 'mnet').': '.$mnethosts[$user->mnethostid]->name;
                } else {
                    $lastcolumn = get_string($accessctrl, 'mnet');
                }

            } else if ($user->confirmed == 0) {
                if (has_capability('moodle/user:update', $sitecontext)) {
                    $lastcolumn = html_writer::link(new moodle_url($returnurl, array('confirmuser'=>$user->id, 'sesskey'=>sesskey())), $strconfirm);
                } else {
                    $lastcolumn = "<span class=\"dimmed_text\">".get_string('confirm')."</span>";
                }
            }

            if ($user->lastaccess) {
                //$strlastaccess = format_time(time() - $user->lastaccess);
            } else {
                //$strlastaccess = get_string('never');
            }
            $fullname = fullname($user, true);

			$imagevalue = $OUTPUT->user_picture($user, array('courseid' => SITEID, 'size'=>64));
			$tableHtml .= '<tr '.$disabledclass.'>';
			$tableHtml .=  "<td class='email-word-wrap'>".$imagevalue."<span class = 'user-table-name'><a href=\"../user/viewuser.php?id=$user->id&amp;course=$site->id\"><span class = 'user-table-image'>".$fullname."</a><br><a href=\"../user/viewuser.php?id=$user->id&amp;course=$site->id\"><span class = 'user-table-image'>(".$user->username.")</a></span></td>";
	
            /*foreach ($extracolumns as $field) {
				//$tableHtml .=  "<td>".$user->{$field}."</td>";
            }*/                       
                      
			// $userLocation;
			
			
                        $contactDetails = "";
                        if ($CFG->showJobTitle == 1 && $user->job_title) {
                            $contactDetails = "<strong>" . get_string("jobtitle") . ":</strong> " . $user->job_title . "<br/>";
                        }
                        if ($user->report_to) {
                            $contactDetails .= "<strong>" . get_string("report_to") . ":</strong> " . $user->report_to . "<br/>";
                        }
                        
                        $contactDetails .= "<strong>" . get_string("ismanageryescsv") . ":</strong> " . $CFG->isManagerYesOptions[$user->is_manager_yes] . "<br/>";
                        if ($user->email) {
                            $contactDetails .= "<strong>" . get_string("email") . ":</strong> " . $user->email;
                        }

                        $tableHtml .= "<td class='email-word-wrap'>".$contactDetails."</td>";
        
			$ast = '';
			if($user->is_primary == 1){
				$ast = ' (P)';
			}
			$tableHtml .= "<td>".$userIdentifier."</td>";
                        $tableHtml .= "<td>".$city."</td>";
			$tableHtml .= "<td>".ucwords($user->user_role).$ast."</td>";
            if ($user->suspended) {
                foreach ($row as $k=>$v) {
                    //$row[$k] = html_writer::tag('span', $v, array('class'=>'usersuspended'));
                }
            }
			
			$tableHtml .= "<td>".$cntOnlineCourses."/".$cntClassroomCourses."</td>";
			$tableHtml .= "<td><div class='adminiconsBar order'>".implode(' ', $buttons)."</div></td>";
			$tableHtml .= '</tr>';
        }
    }
	$tableHtml .= '</table>';
    // add filters
	
   // $ufiltering->display_add();
    //$ufiltering->display_active();

   /* added by rajesh to redirect the page otherwise filter were adding again and again while we refresh the page */	
    if(isset($_REQUEST['addfilter']) && !empty($_REQUEST['addfilter'])){
	    redirect($returnurl);
	}
	// end 
	
	// print table
    if (!empty($tableHtml)) {
		echo "<h2 class='icon_title'><span class='users'></span>".get_string('users')." (".$usercount.")</h2>";
        echo html_writer::start_tag('div', array('class'=>'borderBlockSpace'));
        //echo html_writer::table($table);
		echo $tableHtml;
        echo html_writer::end_tag('div');
		echo '</div>';
		//echo '<div class = "user-note no-overflow"><span>'.get_string('user_note').'</span></div>';
        echo paging_bar($usercount, $page, $perpage, $genURL); // paging bar
    }
echo $OUTPUT->footer();
