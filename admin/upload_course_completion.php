<?php

    require_once('../config.php');
    require_once($CFG->libdir.'/adminlib.php');
    require_once($CFG->libdir.'/authlib.php');
    require_once($CFG->dirroot.'/user/filters/lib.php');
    function upload_user($target_dir) {
    	GLOBAL $DB,$CFG,$USER,$statusLog,$errorLog,$viewLog;
    	$i = 0;
    	$errorFile = 0;
    	$statusLog['success']['manager'] = 0;
    	$statusLog['success']['learner'] = 0;
    	$statusLog['fail'] = 0;
    	/* **** User Upload Start **** */
    	// Open uploaded user file
    	$csvDataArray = array();
    	$rowCount = 0;
    	if (($handle = fopen($target_dir, "r")) !== FALSE) {
    		$completed_time = array();
    		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
    		{
    			
    			$inx = count($data);
    			$rowCount++;
    			if($rowCount == 1){
    				
    			}else{
    			
    				$emailId = trim($data[1]);
    				$courseId = trim($data[2]);
    				$completed_time = trim($data[0]);
    				$duplicateRec = array_keys($userArray);
    				if(in_array($emailId,$duplicateRec)){
    					continue;
    				}
    				$userArray[$emailId]['email'] = $emailId;
    				
    				$userArray[$emailId]['datesubmitted'] = $completed_time;
    				$userArray[$emailId]['courseid'] = $courseId;
    				
    				$user_rec = $DB->get_record('user',array('email'=>$emailId,'suspended'=>0));
				
    				if($user_rec){
    					 $userArray[$emailId]['status'] = 'user exist';
    					 $courseId = trim($data[2]);
    					 $completed_time = strtotime($completed_time);
    					$course_rec = $DB->get_record('course',array('idnumber'=>$courseId));
    					enrolUserIntoCourseWithoutEmail($course_rec->id,$user_rec->id);
    					if($course_rec){
    						
    						$resourse_sql = "select * from mdl_resource where course='".$course_rec->id."' and is_active='1'";
    						$course_resources = $DB->get_records_sql($resourse_sql);
    						
    						if(count($course_resources)>0){
    							
    							foreach($course_resources as $course_resource){
    							 $sql = "select id from mdl_user_noncourse_lastaccess where userid='".$user_rec->id."' and resource_id='".$course_resource->id."'  and courseid='".$course_rec->id."' LIMIT 1";
    						
    								
    							$already_attempted = $DB->get_field_sql($sql);
	    							if($already_attempted){
	    								$userArray[$emailId]['coursestatus'] = 'Already Completed';
	    							}
	    							else{
	    							
	    								$insert_rec = new stdClass();
	    								$insert_rec->userid = $user_rec->id;
	    								$insert_rec->courseid = $course_rec->id;
	    								$insert_rec->resource_id = $course_resource->id;	    							
	    								$insert_rec->timeaccess =$completed_time;
	    								$insert_rec->firstaccess = $completed_time;
	    								$DB->insert_record('user_noncourse_lastaccess',$insert_rec);
	    								
	    							}
	    							
    							}
    						}
    						
    						$sql = "select s.id,max(ss.id) as scoid from mdl_scorm s LEFT JOIN mdl_scorm_scoes ss ON(s.id=ss.scorm) Where s.course='".$course_rec->id."' AND is_active='1'";
    						
    						$course_scorms = $DB->get_records_sql($sql);
							$scormexist = $DB->record_exists('scorm',array('course'=>$course_rec->id,'is_active'=>'1'));
							
    						if($scormexist){
    							
    							$insert_rec = new stdClass();
    							foreach($course_scorms as $course_scorm){
									$sql = "select id from mdl_scorm_scoes_track where userid='".$user_rec->id."' and scormid='".$course_scorm->id."' and value='completed' LIMIT 1";
									
									$already_attempted = $DB->get_field_sql($sql);
    								
    								if($already_attempted){
    									$userArray[$emailId]['coursestatus'] = 'Already Completed';
    								}else{
									$attempt = 1;
									 $sql = "select max(attempt) as attempt from mdl_scorm_scoes_track where userid='".$user_rec->id."' and scormid='".$course_scorm->id."'";
									$max_attempt =  $DB->get_field_sql($sql);
									if($max_attempt){
									$attempt = $max_attempt+1;
									}
    								$insert_rec->userid = $user_rec->id;
    								$insert_rec->scormid = $course_scorm->id;
    								$insert_rec->scoid = $course_scorm->scoid;
    								$insert_rec->attempt = $attempt;
    								$insert_rec->element = 'x.start.time';
    								$insert_rec->value =$completed_time;
    								$insert_rec->timemodified = $completed_time;
    								$DB->insert_record('scorm_scoes_track',$insert_rec);
    								//////////////
    								$insert_rec->userid = $user_rec->id;
    								$insert_rec->scormid = $course_scorm->id;
    								$insert_rec->scoid = $course_scorm->scoid;
    								$insert_rec->attempt = $attempt;
    								$insert_rec->element = 'cmi.core.lesson_status';
    								$insert_rec->value = 'completed';
    								$insert_rec->timemodified = $completed_time;
    								$DB->insert_record('scorm_scoes_track',$insert_rec);
    								$userArray[$emailId]['coursestatus'] = 'Completed';
    								}
    							}
    						}
    						else{
    							$userArray[$emailId]['coursestatus'] = 'Scorm not Available';
    						}
    					} 
    				}
    				else{
    					$userArray[$emailId]['status'] = 'user not found';
    					$userArray[$emailId]['coursestatus'] = 'NA';
    				}
    				
    			}
				
    		}
    		
    	}
    exportCSVUser($userArray);
    	// Close uploaded user file
    	fclose($handle);
    	/* **** User Upload ends here **** */
    	
    	
    
    }

    function exportCSVUser($userArrays){
    	global $CFG;
    	$reportContentCSV = "Date Submitted,Email Id,CourseId,Status,Course Status" . "\n";
    	foreach ( $userArrays as $userArray) {
    		$reportContentCSV .= $userArray ['datesubmitted'] . "," . $userArray ['email'] . "," . $userArray ['courseid'] . "," . $userArray ['status']."," . $userArray ['coursestatus']. "\n";
    	}
    	$filepath = $CFG->dirroot . "/local/reportexport/temp";
    	chmod ( $filepath, 0777 );
    	$filename = str_replace ( ' ', '_', 'CompletionOfCourse' ) . "_" . date ( "m-d-Y" ) . ".csv";
    	$filepathname = $filepath . '/' . $filename;
    	unlink ( $filepathname );
    	$handler = fopen ( $filepathname, "w" );
    	fwrite ( $handler, $reportContentCSV );
    	exportCSV ( $filepathname );
    }
	//Check User is logged in or not. If not, redirect to home page.
	require_login();

	if($USER->archetype == $CFG->userTypeStudent){  //redirect if learner
	   redirect($CFG->wwwroot);
	}
	//Request parameters

    $pageURL = '/admin/user.php';
    checkPageIsNumeric($pageURL,$_REQUEST['page']);
  
	// Download sample script start

	$userUploadResult = array();
	if($_POST){
		//error_reporting(E_ALL);
		$file_name = time().'_'.basename($_FILES["user_list"]["name"]);
		$allowed =  array('csv');
		$filename = $file_name;
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		
		$target_dir = $CFG->dirroot.'\user\upload\\'.$file_name;
		if (in_array($ext,$allowed) && $_FILES["user_list"]["error"] == 0 && move_uploaded_file($_FILES["user_list"]["tmp_name"], $target_dir)) {
			$userUploadResult = upload_user($target_dir);
			//pr($userUploadResult);die;
			$_SESSION['upload_result'] = $userUploadResult;
			
			unlink($target_dir);
		}
		
	}
	// Download sample script ends
	// Get site data for page configuration
    $sitecontext = context_system::instance();
    $site = get_site();
	// Get site data for page configuration ends here


	//Site header
	$header = "$SITE->shortname";

	// Start setting up the page
	$params = array();
	$context = get_context_instance(CONTEXT_SYSTEM);
	$PAGE->set_context($context);
	$PAGE->set_url('/admin/user.php', $params);
	$PAGE->set_pagelayout('listing');
	$PAGE->set_pagetype('my-index');
	$PAGE->blocks->add_region('content');
	$PAGE->set_title($header);
	$PAGE->set_heading($header);
	// end setting up the page

	//set navigation bar
	$PAGE->navbar->add(get_string('manage_user'));
    echo $OUTPUT->header(); // output header


	global $DB,$USER;
	
	

	echo "<div class = 'upload-user-div'>";
		//echo '<a href="'.$securewwwroot.'/admin/user.php?download='.$downloadLink.'" class="download-sample">'.get_string('download_sample').'</a>';
		echo '<form name = "upload_user" id = "upload_user" method = "POST" enctype="multipart/form-data">
					<!--<select name = "upload_type" id = "upload_type">
						'.$options.'
					</select>
					<select name = "departments" id = "departments" '.$departmentStyle.'><option value = "0">Select Department</option>
						'.$departmentselectOptions.'
					</select>
					<select name = "department-teams[]" id = "department-teams" multiple>
					</select> -->
					<div id="user_listOuter"><input type = "file" name = "user_list" id = "user_list"></div>
					<input type = "hidden" name = "upload_type" value = "1">
					<input type = "button" name = "upload" value = "upload" id = "upload-submit">
				</form>';
	echo '</div>';
	

	?>
	
	<script>
	$(document).ready(function(){
		$('.upload-user-div').show();
		$('.upload-user').click(function(){
			$('.upload-user-div').toggle();
			$('.status_report_main').remove();
			$('.bg-success').remove();
		});
		$('.success_report_header').click(function(){
			$('.success_report').toggle();
		});
		$('.error_report_header').click(function(){
			$('.error_user_report').toggle();
		});
		
		$('#upload-submit').click(function(){
			var filename = $("#user_list").val();
			if($.trim(filename) == ''){
				alert('Please select file');
				return false;
			}
			$("#upload_user").submit();
		});
	
	});
	</script>
 
	<?php
	// update msg text
	if($_SESSION['update_msg']!=''){
		echo '<div class="clear"></div>';
		echo '<div class="bg-success text-success text-center '.$_SESSION['error_class'].'" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
		echo '<div class="clear"></div>';
		$_SESSION['update_msg']='';
		$_SESSION['error_class']='';
	}
	
   

    //echo $OUTPUT->paging_bar($usercount, $page, $perpage, $baseurl);

    flush();

echo $OUTPUT->footer();
