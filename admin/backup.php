<?php

require_once('../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->dirroot.'/'.$CFG->admin.'/lib.php');
require_once($CFG->dirroot.'/'.$CFG->admin.'/easybackup_forms.php');

require_login();

 
$PAGE->set_title("$SITE->shortname: " . get_string('administration','menubar').": ".get_string('easybackup','menubar'));
$PAGE->set_heading($SITE->fullname);

$action_form = new easybackup_form();
if ($data = $action_form->get_data()) {  

    
	$filepath =  $_SERVER['DOCUMENT_ROOT'].'/moodle/databasebk/'.$CFG->dbname.'.sql';
	$zipfilepath =  $filepath.'.gz';
	@unlink($zipfilepath);
	//$command = 'ls';
	$command = 'mysqldump -h '.$CFG->dbhost.' -u '.$CFG->dbuser.' --password='.$CFG->dbpass.' '.$CFG->dbname.' > '.$filepath;
	exec($command,$output);
	$command = 'gzip '.$filepath;
	exec($command,$output);
	forceDownload($zipfilepath);
	redirect($CFG->wwwroot.'/'.$CFG->admin.'/backup.php');
	
}


// do output
echo $OUTPUT->header();

  
echo $OUTPUT->heading(get_string('databasebackup','easybackup'));


$action_form->display();

echo $OUTPUT->footer();
