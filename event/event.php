<?php

/////////////////////////////////////////////////////////////////////////////
//                                                                         //
// NOTICE OF COPYRIGHT                                                     //
//                                                                         //
// Moodle - Calendar extension                                             //
//                                                                         //
// Copyright (C) 2003-2004  Greek School Network            www.sch.gr     //
//                                                                         //
// Designed by:                                                            //
//     Avgoustos Tsinakos (tsinakos@teikav.edu.gr)                         //
//     Jon Papaioannou (pj@moodle.org)                                     //
//                                                                         //
// Programming and development:                                            //
//     Jon Papaioannou (pj@moodle.org)                                     //
//                                                                         //
// For bugs, suggestions, etc contact:                                     //
//     Jon Papaioannou (pj@moodle.org)                                     //
//                                                                         //
// The current module was developed at the University of Macedonia         //
// (www.uom.gr) under the funding of the Greek School Network (www.sch.gr) //
// The aim of this project is to provide additional and improved           //
// functionality to the Asynchronous Distance Education service that the   //
// Greek School Network deploys.                                           //
//                                                                         //
// This program is free software; you can redistribute it and/or modify    //
// it under the terms of the GNU General Public License as published by    //
// the Free Software Foundation; either version 2 of the License, or       //
// (at your option) any later version.                                     //
//                                                                         //
// This program is distributed in the hope that it will be useful,         //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           //
// GNU General Public License for more details:                            //
//                                                                         //
//          http://www.gnu.org/copyleft/gpl.html                           //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////

/**
 * This file is part of the Calendar section Moodle
 *
 * @copyright 2003-2004 Jon Papaioannou (pj@moodle.org)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v2 or later
 * @package calendar
 */

require_once('../config.php');
require_once($CFG->dirroot.'/calendar/event_form.php');
require_once($CFG->dirroot.'/calendar/lib.php');
require_once($CFG->dirroot.'/course/lib.php');

require_login();

$action = optional_param('action', 'new', PARAM_ALPHA);
$eventid = optional_param('id', 0, PARAM_INT);
$courseid = optional_param('courseid', SITEID, PARAM_INT);
$courseid = optional_param('course', $courseid, PARAM_INT);
$day = optional_param('cal_d', 0, PARAM_INT);
$month = optional_param('cal_m', 0, PARAM_INT);
$year = optional_param('cal_y', 0, PARAM_INT);
$time = optional_param('time', 0, PARAM_INT);
if($CFG->is12HourFormat==1){
	$time = setTimeAsPerTimeFormat($time);
}
// added by rajesh . these variables will be used in query string for redirection
$timeVal = optional_param('timeVal', 0, PARAM_INT);
$viewType = optional_param('viewType', 0, PARAM_ALPHA);

if(empty($_SESSION['sess_add_event_from'])){
  $_SESSION['sess_add_event_from'] = $_SERVER['HTTP_REFERER'];
}
// If a day, month and year were passed then convert it to a timestamp. If these were passed
// then we can assume the day, month and year are passed as Gregorian, as no where in core
// should we be passing these values rather than the time. This is done for BC.
if (!empty($day) && !empty($month) && !empty($year)) {
    if (checkdate($month, $day, $year)) {
        $time = make_timestamp($year, $month, $day);
    } else {
        $time = time();
    }
} else if (empty($time)) {
    $time = time();
}

$url = new moodle_url('/calendar/event.php', array('action' => $action));

if ($eventid != 0) {
    $url->param('id', $eventid);
}

if ($courseid != SITEID) {
    $url->param('course', $courseid);
}

$PAGE->set_url($url);
$PAGE->set_pagelayout('standard');

if ($courseid != SITEID && !empty($courseid)) {
    $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
    $courses = array($course->id => $course);
    $issite = false;
} else {
    $course = get_site();
    $courses = calendar_get_default_courses();
    $issite = true;
}
require_login($course, false);

if ($action === 'delete' && $eventid > 0) {
    $deleteurl = new moodle_url('/calendar/delete.php', array('id'=>$eventid));
    if ($courseid > 0) {
        $deleteurl->param('course', $courseid);
    }
    redirect($deleteurl);
}

$calendar = new calendar_information(0, 0, 0, $time);
$calendar->prepare_for_view($course, $courses);

$formoptions = new stdClass;
if ($eventid !== 0) {
	
    $title = get_string('editevent', 'calendar');
    $event = calendar_event::load($eventid);
    if (!calendar_edit_event_allowed($event)) {
        print_error('nopermissions');
    }
    $event->action = $action;
    $event->course = $courseid;
    $event->timedurationuntil = $event->timestart + $event->timeduration;
    $event->count_repeats();

    if (!calendar_add_event_allowed($event)) {
        print_error('nopermissions');
    }
    if($CFG->is12HourFormat){
    	
    $event->timestartformat = date('A',$event->timestart);
    $event->timestart = setTimeAsPerTimeFormat($event->timestart);
  
  /*   echo $event->timedurationuntil;
  echo date('d M Y h:i A',$event->timedurationuntil);
  die; */
    $event->timestartformatuntil = date('A',$event->timedurationuntil);
    $event->timedurationuntil = setTimeAsPerTimeFormat($event->timedurationuntil);
    
    }
} else {
    $title = get_string('addevent', 'event');
    calendar_get_allowed_types($formoptions->eventtypes, $course);
	
    $event = new stdClass();
    $event->action = $action;
    $event->course = $courseid;
    $event->courseid = $courseid;
    $event->timeduration = 0;
    if ($formoptions->eventtypes->courses) {
        if (!$issite) {
            $event->eventtype = 'course';
        } else {
            unset($formoptions->eventtypes->courses);
            unset($formoptions->eventtypes->groups);
        }
    }
    $event->timestart = $time;
    $event = new calendar_event($event);
    if (!calendar_add_event_allowed($event)) {
        print_error('nopermissions');
    }
    
}

$properties = $event->properties(true);
////// Added by Madhab to set the eventtype selected value as "course" to show the course list on loading of page first time ////
$editEventChk = optional_param('id', 0, PARAM_INT);
if($editEventChk != 0) {
	if(!isset($properties->eventtype))
		$properties->eventtype = $CFG->defaultEventType;
} else {
	$properties->eventtype = $CFG->defaultEventType;
}

$formoptions->event = $event;
$formoptions->hasduration = ($event->timeduration > 0);
$mform = new event_form(null, $formoptions);
$mform->set_data($properties);

$back_url = '/calendar/view.php';
if( isset($_SESSION['sess_add_event_from']) && $_SESSION['sess_add_event_from']!= '') {
	$sess_add_event_from_arr = explode('?', $_SESSION['sess_add_event_from']);
	$back_url = str_replace($CFG->wwwroot, "", $sess_add_event_from_arr[0]);
	if(!strstr($back_url, '/calendar/view.php') && !strstr($back_url, '/my/dashboard.php') ){
	  $back_url = '/calendar/view.php';
	}
}
		
//$manageurl = new moodle_url('/calendar/view.php?view=day');
if($viewType && $timeVal){
   $manageurl = new moodle_url($back_url.'?view='.$viewType.'&time='.$timeVal);
}else{
  
	/*if(strstr($back_url, "/my/dashboard.php")){
	  $manageurl = new moodle_url($back_url.'?view=day');
	}else{
	  $manageurl = new moodle_url($back_url);
	}*/
	
	$manageurl = new moodle_url($back_url.'?view=day');
}

if ($mform->is_cancelled()) {
    redirect($manageurl);
} else if ($data = $mform->get_data()) {
//echo date("y-m-d",$data->timestart);die;

//echo $back_url;die;

	if ($data) {		
		if($CFG->is12HourFormat==1){			
			$data->timestart = getTimeAsPerTimeFormat($data->timestart,$_REQUEST['timeformat']);
			$data->timedurationuntil = getTimeAsPerTimeFormat($data->timedurationuntil,$_REQUEST['timeformatuntil']);			 
		}
		//echo date('d M Y h:i A',$data->timedurationuntil);
		//die;
		if ($data->duration == 1) {
			$data->timeduration = $data->timedurationuntil- $data->timestart;
		} else if ($data->duration == 2) {
			$data->timeduration = $data->timedurationminutes * MINSECS;
		} else {
			$data->timeduration = 0;
		}
		
		if($data->id){
		  $data->modifiedby = $USER->id;
		}else{
		  $data->createdby = $USER->id; 
		  $data->modifiedby = $USER->id;
		}
		//pr($data);die;
		$event->update($data);
		//$eventurl = new moodle_url('/event/view.php');
		/*
		if($data->id == 0){
		
			//echo date("Y-m-d", $data->timestart)."===". date("Y-m-d", time());die;
			//echo $data->timestart."===". time();die;
			
			if(strstr($back_url, "/my/dashboard.php")){
				 $eventurl = new moodle_url($back_url.'?view=day&time='.$data->timestart);
			}else{
				if($data->timestart < time()){
				   $eventurl = new moodle_url($back_url.'?view=day&time='.$data->timestart);
				}else{
				   $eventurl = new moodle_url($back_url);
				}
			}
			

		}else{
		
		    if(strstr($back_url, "/my/dashboard.php")){
				$eventurl = new moodle_url($back_url.'?view=day&time='.$data->timestart);
			}else{
				if($viewType && $timeVal){
				   $eventurl = new moodle_url($back_url.'?view='.$viewType.'&time='.$timeVal);
				}else{
				   $eventurl = new moodle_url($back_url);
				}
			}
			
		}*/
		
		$eventurl = new moodle_url($back_url.'?view=day&time='.$data->timestart);
 
		
		if (!empty($event->courseid) && $event->courseid != SITEID) {
			$eventurl->param('course', $event->courseid);
		}
		
		$eventurl->set_anchor('event_'.$event->id);
		redirect($eventurl);
	}
}

$viewcalendarurl = new moodle_url(CALENDAR_URL.'view.php', $PAGE->url->params());
$viewcalendarurl->remove_params(array('id', 'action'));
$viewcalendarurl->param('view', 'upcoming');
$strcalendar = get_string('calendar', 'calendar');

$PAGE->navbar->add($strcalendar, $viewcalendarurl);
$PAGE->navbar->add($title);
$PAGE->set_title($SITE->fullname.': '.$title);
$PAGE->set_heading($SITE->fullname);

$renderer = $PAGE->get_renderer('core_calendar');
$calendar->add_sidecalendar_blocks($renderer);

echo $OUTPUT->header();
//echo $OUTPUT->heading($title);
$mform->display();		

if(isset($properties->eventtype))
	$eventtypeChk = $properties->eventtype;
else	
	$eventtypeChk = optional_param('eventtype', '', PARAM_ALPHA);
if( !empty($eventtypeChk) && ($eventtypeChk != '') ) {
	echo '<script>$(document).ready(function(){ var eventType = "'.$eventtypeChk.'"; if (eventType != "course") { $("#fitem_id_courseid").hide();} else {$("#fitem_id_courseid").show();} });</script>';
}

?>

<script>

$(document).ready(function(){
	 $('#id_repeats').after('&nbsp;<?php echo get_string('occurrences','calendar')?>');
	var eventType = $("#id_eventtype").val();
	if (eventType != "course") { 
	    $("#fitem_id_courseid").hide();
	} else {
	  $("#fitem_id_courseid").show();
	}
	 
	var id_duration_1 = $('#id_duration_1').is(":checked"); 
	if(id_duration_1 == false){
		var timedurationuntil_day =  $('#id_timedurationuntil_day').val();
		timedurationuntil_day = parseInt(timedurationuntil_day) + 1;
		$('#id_timedurationuntil_day').val(timedurationuntil_day);

		
	}
	$(document).on('blur', '#id_name', function(){

		   var cVal = $(this).val();
		   cVal = $.trim(cVal);
		   $(this).val(cVal);


		});
		
	$(document).on("click", "#id_submitbutton", function(){

			var id_name = $('#id_name').val();
			id_name =  $.trim(id_name);
	
			if( id_name != '' ){  
				$(this.form).submit(function(){
				
					 $("#id_submitbutton").prop('disabled', 'disabled');
				
				});
			}
	});	

});
</script>
<?php
if($CFG->is12HourFormat==1){ 
?>
<script>
$( document ).ready(function() {
	var timestartFirmat = $("input[name='timestartformat']").val();
	var PmSelected = '';
	if(timestartFirmat=="PM"){
		PmSelected = 'selected=selected';
	}
	//alert(timestartFirmat);
	var selectbox = ' <select name="timeformat"><option value="0">AM</option><option value="1" '+PmSelected+'>PM</option></select>';
   	$("#id_timestart_minute").after(selectbox);
   	var timestartformatuntil = $("input[name='timestartformatuntil']").val();
	var PmSelected = '';
	if(timestartformatuntil=="PM"){
		PmSelected = 'selected=selected';
	}
   	var selectboxuntil = ' <select name="timeformatuntil" id="timeformatuntil"><option value="0">AM</option><option value="1" '+PmSelected+'>PM</option></select>';
 	$("#id_timedurationuntil_minute").after(selectboxuntil);
	check = $("#id_duration_1").prop("checked"); 
	if(!check){
	$("#timeformatuntil").prop('disabled','disabled');	
	}	
	$("#id_duration_0,#id_duration_2").on('click',function(){
		check = $(this).prop("checked"); 
		if(check){
			$("#timeformatuntil").prop('disabled','disabled');	
		}
	})
	$("#id_duration_1").on('click',function(){
		check = $(this).prop("checked"); 
		if(check){
			$("#timeformatuntil").removeAttr('disabled');	
		}
	})
});
</script>
<?php 
}
echo $OUTPUT->footer();
