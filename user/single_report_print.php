<?php

	/**
		* Custom module - Signle Report Print Page
		* Date Creation - 25/06/2014
		* Date Modification : 25/06/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
	
	require_once('../config.php');
	require_once($CFG->libdir.'/gdlib.php');
	require_once($CFG->libdir.'/adminlib.php');
	require_once($CFG->dirroot.'/user/editlib.php');
	require_once($CFG->dirroot.'/user/profile/lib.php');
	require_once($CFG->dirroot.'/user/lib.php');
	
	//HTTPS is required in this page when $CFG->loginhttps enabled
	require_login();
	
	//$userrole =  getUserRole($USER->id);
	if( $USER->archetype == $CFG->userTypeStudent ) {
		$id      = optional_param('uid', $USER->id, PARAM_INT);
		if($id != $USER->id){
			$grpUsers = fetchGroupsUserIds($USER->id,1);
			if(empty($grpUsers) || !in_array($id,$grpUsers)){
				$id = $USER->id;
			}
		}
	}else{
	  $id      = optional_param('uid', $USER->id, PARAM_INT);    // user id; 
	}

	$header = $SITE->fullname.": ".get_string('overallcourseprogressreport','singlereport');
	
	$PAGE->set_title($header);
	$PAGE->set_heading($header);
	$PAGE->set_pagelayout('print');
	
	$user = $DB->get_record('user', array('id'=>$id), '*', MUST_EXIST);
	$userId = $user->id;


    $sort    = optional_param('sort', 'mc.fullname', PARAM_ALPHANUM);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 0, PARAM_INT);        // how many per page
	$export = optional_param('action', '', PARAM_ALPHANUM);
	
	
	$sDate = '';
	$eDate = '';
		
	/*$sDate =  $CFG->reportSearchStartDate;
	$eDate = $CFG->reportSearchEndDate*/
	
	$sDepartment    = optional_param('department', '-1', PARAM_RAW);
	$sTeam          = optional_param('team', '-1', PARAM_RAW);       
	$sCourse          = optional_param('course', '-1', PARAM_RAW);       
	$sType          = optional_param('type', '-1', PARAM_RAW); 
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);
	
	
	$paramArray = array(
	                'uid' => $userId, 
					'department' => $sDepartment,
					'team' => $sTeam,
					'course' => $sCourse,
					'type' => $sType,
					'startDate' => $sStartDate,
					'endDate' => $sEndDate
				  );
	
	$removeKeyArray = array();

	$courseHTML = '';
	$reportContentCSV = '';
	$reportContentPDF = '';
    if($export && in_array($export, array('exportcsv', 'exportpdf', 'print'))){

		$learnerReport = getLearnerReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
		$HTML = $learnerReport->HTML;
		$reportContentCSV = $learnerReport->reportContentCSV;
		$reportContentPDF = $learnerReport->reportContentPDF;
	
    }else{
	    redirect(new moodle_url('/'));
	}

	/* bof export to csv */
	
    if(isset($export) && $export == 'exportcsv') {
	
		/*$filepath = $CFG->dirroot."/local/reportexport/temp";		
		chmod($filepath, 0777);		
		$filename = str_replace(' ', '_', get_string('overallcourseprogressreport','singlereport'))."_".date("m-d-Y").".csv";  
		$filepathname = $filepath.'/'.$filename;
		unlink($filepathname);
		$handler = fopen($filepathname, "w");
		fwrite($handler, $reportContentCSV);
		fclose($handler);*/
		
		$filepathname = getpdfCsvFileName('csv', get_string('overallcourseprogressreport','singlereport'), $reportContentCSV);
   	    exportCSV($filepathname);
	}
	/* eof export to csv */	
	
	
	/* bof export to pdf */
    if(isset($export) && $export == 'exportpdf') {
			
		$filename = str_replace(' ', '_', get_string('overallcourseprogressreport','singlereport'))."_".date("m-d-Y").".pdf";
		exportPDF($filename, $reportContentPDF, '', get_string('overallcourseprogressreport','singlereport'));
		
	}
	/* eof export to pdf */	
	   
	echo $OUTPUT->header(); 
	echo $HTML;	
?>
<style>
#page { margin: 20px auto 0;}
</style>