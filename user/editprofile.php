<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Allows you to edit a users profile
 *
 * @copyright 1999 Martin Dougiamas  http://dougiamas.com
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package user
 */

require_once('../config.php');
require_once($CFG->libdir.'/gdlib.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->dirroot.'/user/editprofile_form.php');
require_once($CFG->dirroot.'/user/editlib.php');
require_once($CFG->dirroot.'/user/profile/lib.php');
require_once($CFG->dirroot.'/user/lib.php');

//HTTPS is required in this page when $CFG->loginhttps enabled
$PAGE->https_required();

$id   =  $USER->id ;   // user id; -1 if creating new user
$course = optional_param('course', SITEID, PARAM_INT);   // course id (defaults to Site)

$PAGE->set_url('/user/editprofile.php', array('course'=>$course, 'id'=>$id));
if($id){
	$header = $SITE->fullname.": ".get_string('edituser');
}else{
	$header = $SITE->fullname.": ".get_string('adduser');
}
$PAGE->set_title($header);
$PAGE->set_heading($header);
$course = $DB->get_record('course', array('id'=>$course), '*', MUST_EXIST);

if (!empty($USER->newadminuser)) {
    $PAGE->set_course($SITE);
    $PAGE->set_pagelayout('maintenance');
} else {
    if ($course->id == SITEID) {
        require_login();
        $PAGE->set_context(context_system::instance());
    } else {
        require_login($course);
    }
    $PAGE->set_pagelayout('admin');
}

if ($course->id == SITEID) {
    $coursecontext = context_system::instance();   // SYSTEM context
} else {
    $coursecontext = context_course::instance($course->id);   // Course context
}

$systemcontext = context_system::instance();
	//$PAGE->navbar->add(get_string('editmyprofile'), new moodle_url('/admin/user.php'));

if ($id == -1) {
    // creating new user
    $user = new stdClass();
    $user->id = -1;
    $user->auth = 'manual';
    $user->confirmed = 1;
    $user->deleted = 0;
    require_capability('moodle/user:create', $systemcontext);
    //admin_externalpage_setup('addnewuser', '', array('id' => -1));
} else {

    // editing existing user   
    //require_capability('moodle/user:editprofile', $systemcontext);
  
    $user = $DB->get_record('user', array('id'=>$id), '*', MUST_EXIST);
    $PAGE->set_context(context_user::instance($user->id));
    if ($user->id != $USER->id) {
        $PAGE->navigation->extend_for_user($user);
    } else {
        if ($node = $PAGE->navigation->find('myprofile', navigation_node::TYPE_ROOTNODE)) {
            $node->force_open();
        }
    }
}


$PAGE->navbar->add(get_string('editprofile','user'));

// remote users cannot be edited
if ($user->id != -1 and is_mnet_remote_user($user)) {
    redirect($CFG->wwwroot . "/user/view.php?id=$id&course={$course->id}");
}

if ($user->id != $USER->id and is_siteadmin($user) and !is_siteadmin($USER)) {  // Only admins may edit other admins
    print_error('useradmineditadmin');
}

if (isguestuser($user->id)) { // the real guest user can not be edited
    print_error('guestnoeditprofileother');
}

if ($user->deleted) {
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('userdeleted'));
    echo $OUTPUT->footer();
    die;
}

//load user preferences
useredit_load_preferences($user);

//Load custom profile fields data
profile_load_data($user);

//User interests
if (!empty($CFG->usetags)) {
    require_once($CFG->dirroot.'/tag/lib.php');
    $user->interests = tag_get_tags_array('user', $id);
}

if ($user->id !== -1) {
    $usercontext = context_user::instance($user->id);
    $editoroptions = array(
        'maxfiles'   => EDITOR_UNLIMITED_FILES,
        'maxbytes'   => $CFG->maxbytes,
        'trusttext'  => false,
        'forcehttps' => false,
        'context'    => $usercontext
    );

    $user = file_prepare_standard_editor($user, 'description', $editoroptions, $usercontext, 'user', 'profile', 0);
} else {
    $usercontext = null;
    // This is a new user, we don't want to add files here
    $editoroptions = array(
        'maxfiles'=>0,
        'maxbytes'=>0,
        'trusttext'=>false,
        'forcehttps'=>false,
        'context' => $coursecontext
    );
}

// Prepare filemanager draft area.
$draftitemid = 0;
$filemanagercontext = $editoroptions['context'];
$filemanageroptions = array('maxbytes'       => $CFG->maxbytes,
                             'subdirs'        => 0,
                             'maxfiles'       => 1,
                             'accepted_types' => 'web_image');
file_prepare_draft_area($draftitemid, $filemanagercontext->id, 'user', 'newicon', 0, $filemanageroptions);
$user->imagefile = $draftitemid;
//create form
$dob = '';
if($user->dateofbirth){
	$dob = date('m/d/Y', $user->dateofbirth);
}
$userform = new user_editprofile_form(null, array(
    'editoroptions' => $editoroptions,
    'filemanageroptions' => $filemanageroptions,
    'dateofbirth' => $dob,
    'is_instructor' => $user->is_instructor,
    'department_manager' => $user->parent_id,
    'department' => $user->department,
    'userid' => $user->id));
$user->dateofbirth = $dob;
$userform->set_data($user);

if ($usernew = $userform->get_data()) {

    if (empty($usernew->auth)) {
        //user editing self
        $authplugin = get_auth_plugin($user->auth);
        unset($usernew->auth); //can not change/remove
    } else {
        $authplugin = get_auth_plugin($usernew->auth);
    }

    $usernew->timemodified = time();
    $createpassword = false;

    if ($usernew->id == -1) {
        //TODO check out if it makes sense to create account with this auth plugin and what to do with the password
        unset($usernew->id);
        $createpassword = !empty($usernew->createpassword);
        unset($usernew->createpassword);
        $usernew = file_postupdate_standard_editor($usernew, 'description', $editoroptions, null, 'user', 'profile', null);
        $usernew->mnethostid = $CFG->mnet_localhost_id; // always local user
        $usernew->confirmed  = 1;
        $usernew->timecreated = time();
        $createpassword = 0;
        if ($createpassword) {
            $usernew->password = '';
        } else {
            $usernew->password = hash_internal_user_password($usernew->newpassword);
        }
		$createpassword = 1;
		$usernew->createdby = $USER->id;
        $usernew->id = user_create_user($usernew, false);
    } else {
        $usernew = file_postupdate_standard_editor($usernew, 'description', $editoroptions, $usercontext, 'user', 'profile', 0);
		$usernew->updatedby = $USER->id;
		$userRole = $DB->get_record("role",array('name'=>$USER->archetype));
		$usernew->role = $userRole->id;
		$userGroups = $DB->get_records_sql("SELECT g.id FROM {$CFG->prefix}groups_members as gm LEFT JOIN {$CFG->prefix}groups as g ON g.id = gm.groupid WHERE gm.is_active = 1 AND gm.userid = ".$USER->id);
		$userTeam = array();
		if(!empty($userGroups)){
			foreach($userGroups as $group){
				$userTeam[] = $group->id;
			}
		}
		$_POST['team'] = $userTeam;
		$usernew->team = $userTeam;
		$usernew->user_team = array();
		//$usernew->role = 
        // Pass a true old $user here.
        /*if (!$authplugin->user_update($user, $usernew)) {
            // Auth update failed.
            print_error('cannotupdateuseronexauth', '', '', $user->auth);
        }*/
        user_update_user($usernew, false);

        //set new password if specified
        if (!empty($usernew->newpassword)) {
            if ($authplugin->can_change_password()) {
                if (!$authplugin->user_update_password($usernew, $usernew->newpassword)){
                    print_error('cannotupdatepasswordonextauth', '', '', $usernew->auth);
                }
                unset_user_preference('create_password', $usernew); // prevent cron from generating the password
            }
        }

        // force logout if user just suspended
        if (isset($usernew->suspended) and $usernew->suspended and !$user->suspended) {
            \core\session\manager::kill_user_sessions($user->id);
        }
    }

    $usercontext = context_user::instance($usernew->id);

    //update preferences
    useredit_update_user_preference($usernew);

    // update tags
    if (!empty($CFG->usetags) and empty($USER->newadminuser)) {
        useredit_update_interests($usernew, $usernew->interests);
    }

    //update user picture
    if (empty($USER->newadminuser)) {
        useredit_update_picture($usernew, $userform, $filemanageroptions);
    }

    // update mail bounces
    useredit_update_bounces($user, $usernew);

    // update forum track preference
    useredit_update_trackforums($user, $usernew);

    // save custom profile fields data
    profile_save_data($usernew);

    // reload from db
    $usernew = $DB->get_record('user', array('id'=>$usernew->id));

	//Assign role to user
	//assignRoleToUser($usernew); // default user type is 5 i.e. learner

    if ($createpassword) {
        //setnew_password_and_mail($usernew);
        unset_user_preference('create_password', $usernew);
        //set_user_preference('auth_forcepasswordchange', 1, $usernew);
    }
    if ($user->id == $USER->id) {
        // Override old $USER session variable
        foreach ((array)$usernew as $variable => $value) {
            if ($variable === 'description' or $variable === 'password') {
                // These are not set for security nad perf reasons.
                continue;
            }
            $USER->$variable = $value;
        }
        // preload custom fields
        profile_load_custom_fields($USER);

        if (!empty($USER->newadminuser)) {
            unset($USER->newadminuser);
            // apply defaults again - some of them might depend on admin user info, backup, roles, etc.
            admin_apply_default_settings(NULL , false);
            // redirect to admin/ to continue with installation
            redirect("$CFG->wwwroot/$CFG->admin/");
        } else {
            redirect("$CFG->wwwroot/user/view.php?id=$USER->id&course=$course->id");
        }
    } else {
        \core\session\manager::gc(); // Remove stale sessions.
        redirect("$CFG->wwwroot/$CFG->admin/user.php");
    }
    //never reached
}

// make sure we really are on the https page when https login required
$PAGE->verify_https_required();


/// Display page header
if ($user->id == -1 or ($user->id != $USER->id)) {
    if ($user->id == -1) {
        echo $OUTPUT->header();
    } else {
        $PAGE->set_heading($SITE->fullname);
        echo $OUTPUT->header();
        $userfullname = fullname($user, true);
        echo $OUTPUT->heading($userfullname);
    }
} else if (!empty($USER->newadminuser)) {
    $strinstallation = get_string('installation', 'install');
    $strprimaryadminsetup = get_string('primaryadminsetup');

    $PAGE->navbar->add($strprimaryadminsetup);
    $PAGE->set_title($strinstallation);
    $PAGE->set_heading($strinstallation);
    $PAGE->set_cacheable(false);

    echo $OUTPUT->header();
    echo $OUTPUT->box(get_string('configintroadmin', 'admin'), 'generalbox boxwidthnormal boxaligncenter');
    echo '<br />';
} else {
    $streditmyprofile = get_string('editmyprofile');
    $strparticipants  = get_string('participants');
    $strnewuser       = get_string('newuser');
    $userfullname     = fullname($user, true);

    $PAGE->set_title("$course->shortname: $streditmyprofile");
    $PAGE->set_heading($course->fullname);

    echo $OUTPUT->header();
    echo $OUTPUT->heading($userfullname);
}
 ?>
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/style/jquery-ui.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-ui.js"></script>
<?php 
  echo "<style>#fitem_id_role,#fitem_id_department,#fitem_id_team,#fitem_id_username,#fitem_id_newpassword,.femptylabel{display:none;}</style>";

/// Finally display THE form
echo $outerDivStart = "<div class='tabsOuter'>";
	$outerDivEnd = "</div>";
	$userTabs .= "<div class='tabLinks'>";

	$userTabs .= "<div><a href='".$CFG->wwwroot."/user/profile.php' title='".get_string('viewprofile','user')."' class=''>".get_string('viewprofile','user')."<span></span></a></div>";
	$userTabs .= "<div><a href='".$CFG->wwwroot."/user/editprofile.php' title='".get_string('editprofile','user')."' class='current' >".get_string('editprofile','user')."<span></span></a></div>";
    $userTabs .= "<div><a href='".$CFG->wwwroot."/login/change_password.php' title='".get_string('changepassword','user')."' class=''>".get_string('changepassword','user')."<span></span></a></div>";
	echo $userTabs .= "</div>";
	echo '<div class="userprofile borderBlockSpace">';
$userform->display();

echo '</div>';
echo $outerDivEnd;
/// and proper footer
?>
<script>    
$(document).ready(function() {
	$('#start_date').datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: '1900:'+(new Date).getFullYear() ,
		maxDate: +0
	});
	 $("#fitem_id_deletepicture").hide();
		$(".delete_user_pic").unwrap();

		 $(document).on('click', '.image_delete_icon', function(){
			 
			 var r = confirm("<?php echo get_string('delete_user_pic');?>");
			 if (r == true) {
				 $("#id_deletepicture").prop('checked', true);
				 $("#id_submitbutton").trigger('click');
			 } 		
			}); 
});
</script>
<?php
echo $OUTPUT->footer();

