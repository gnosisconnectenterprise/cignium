<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Allows you to edit a users profile
 *
 * @copyright 1999 Martin Dougiamas  http://dougiamas.com
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package user
 */

require_once('../config.php');
require_once($CFG->libdir.'/gdlib.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->dirroot.'/user/editlib.php');
require_once($CFG->dirroot.'/user/profile/lib.php');
require_once($CFG->dirroot.'/user/lib.php');

//HTTPS is required in this page when $CFG->loginhttps enabled

$id     = optional_param('id', $USER->id, PARAM_INT);    // user id; -1 if creating new user
$course = optional_param('course', SITEID, PARAM_INT);   // course id (defaults to Site)
require_login();
$PAGE->set_url('/user/viewuser.php', array('course'=>$course, 'id'=>$id));

if(!$id){
	redirect($CFG->wwwroot.'/admin/user.php');
}
checkUserAccess('user' , $id);

$header = $SITE->fullname.": ".get_string('viewuser');

$PAGE->set_title($header);
$PAGE->set_heading($header);
$course = $DB->get_record('course', array('id'=>$course), '*', MUST_EXIST);

if (!empty($USER->newadminuser)) {
    $PAGE->set_course($SITE);
    $PAGE->set_pagelayout('maintenance');
} else {
    if ($course->id == SITEID) {
        require_login();
        $PAGE->set_context(context_system::instance());
    } else {
        require_login($course);
    }
    $PAGE->set_pagelayout('admin');
}

if ($course->id == SITEID) {
    $coursecontext = context_system::instance();   // SYSTEM context
} else {
    $coursecontext = context_course::instance($course->id);   // Course context
}
$systemcontext = context_system::instance();
$PAGE->navbar->add(get_string('manage_user'),'/admin/user.php');
$PAGE->navbar->add(get_string('viewuser'));

if ($id == -1) {
    // creating new user
    $user = new stdClass();
    $user->id = -1;
    $user->auth = 'manual';
    $user->confirmed = 1;
    $user->deleted = 0;
    require_capability('moodle/user:create', $systemcontext);
    admin_externalpage_setup('addnewuser', '', array('id' => -1));
} else {
    // editing existing user
    require_capability('moodle/user:update', $systemcontext);
    $user = $DB->get_record('user', array('id'=>$id), '*', MUST_EXIST);
    $PAGE->set_context(context_user::instance($user->id));
    if ($user->id != $USER->id) {
        //$PAGE->navigation->extend_for_user($user);
    } else {
        if ($node = $PAGE->navigation->find('myprofile', navigation_node::TYPE_ROOTNODE)) {
            $node->force_open();
        }
    }
}

// remote users cannot be edited
if ($user->id != -1 and is_mnet_remote_user($user)) {
    redirect($CFG->wwwroot . "/user/view.php?id=$id&course={$course->id}");
}

if ($user->id != $USER->id and is_siteadmin($user) and !is_siteadmin($USER)) {  // Only admins may edit other admins
    print_error('useradmineditadmin');
}

if (isguestuser($user->id)) { // the real guest user can not be edited
    print_error('guestnoeditprofileother');
}

$user->imagefile = $draftitemid;

echo $OUTPUT->header();
$query = "SELECT r.name FROM {$CFG->prefix}role_assignments as ra LEFT JOIN {$CFG->prefix}role as r ON r.id = ra.roleid WHERE ra.userid=".$user->id;
$userRole = $DB->get_field_sql($query);
$department = '';
$userDepartment = $DB->get_record('department_members',array('userid'=>$user->id));
if(!empty($userDepartment)){
	$departments = $DB->get_record('department',array('id'=>$userDepartment->departmentid));
	if(!empty($departments)){
	    $identifierED = isset($departments->is_external) && $departments->is_external == 1?getEDAstric('view'):'';
		$department = $identifierED.$departments->title;
	}
}else{
	$department = '';
}
$userGroups = $DB->get_records_sql("SELECT g.name,g.id,gd.id AS gd_id FROM mdl_groups_members AS gm LEFT JOIN mdl_groups AS g ON g.id = gm.groupid LEFT JOIN mdl_group_department as gd ON g.id = gd.team_id AND gd.is_active = 1 WHERE gm.is_active = 1 AND gm.userid = ".$user->id);
/// Finally display THE form
$totalGroups = count($userGroups);
$i=1;
foreach($userGroups as $group){
	if($group->gd_id == '' || $group->gd_id == NULL){
		$identifierGlobal = '<small class = "global-identifier" >'.get_string("global_team_identifier").'</small>';
	}else{
		$identifierGlobal = '';
	}
	if($totalGroups == $i){
		$groupString .= $identifierGlobal.$group->name;
	}else{
		$groupString .= $identifierGlobal.$group->name.',';
	}
	$i++;
}
$userLocation = "";
if(isset($user->city) && $user->city != '' ){
	$userLocation .= $user->city.' ,';
}
if(isset($user->country) && $user->country != ''){
	if($CFG->isdisplayextrauserfields){
	$userLocation .= ' '.get_string($user->country, 'countries').' ,';
	}
}

if(isset($user->state) && $user->state){
	$stateData = $DB->get_record('states',array('id'=>$user->state));
	$userLocation .= ' '.$stateData->state.' ,';
}

$userLocation = substr($userLocation, 0, -1);
$userManager = '';
//if($userRole == $CFG->userTypeStudent){
	if($CFG->showInlineManagerNCompany == 1){
		$managerString = get_string('inlinemanager');
		$userManagerDetails = $DB->get_record_sql("SELECT u.firstname,u.lastname FROM mdl_user as u WHERE u.id = ".$user->inline_manager_id);
	}
	else{
		$managerString = get_string('department_manager');
		$userManagerDetails = $DB->get_record_sql("SELECT u.firstname,u.lastname FROM mdl_user as u WHERE u.id = ".$user->parent_id);
	}
	if($userManagerDetails){
		$userManager = $userManagerDetails->firstname.' '.$userManagerDetails->lastname;
	}
//}
//pr($user);die;
echo $outerDivStart = "<div class='tabsOuter'>";
	$outerDivEnd = "</div>";
	include_once('user_tabs.php');
	echo '<div class="userprofile borderBlockSpace">';
	?>
		<div class = "user-picture-div">
			<div class = "user-picture">
				<?php echo $OUTPUT->user_picture($user, array('courseid' => SITEID, 'size'=>150));
				?>
			</div>
		</div>
		<div class = "user-profile-description">
			<div class = "user-profile">
				<table class = "view-mode">
					<tr>
						<th><?php echo get_string('username');?></th>
						<td><?php echo $user->username;?></td>
					</tr>
					<tr>
						<th><?php echo get_string('email');?></th>
						<td><?php echo $user->email;?></td>
					</tr>
					<tr>
						<th><?php echo get_string('phone');?></th>
						<td><?php echo $user->phone1;?></td>
					</tr>
                    
                     <?php if($CFG->showJobTitle == 1){?>
                     <tr>
						<th><?php echo get_string('job_title', 'user');?></th>
						<td><?php echo $user->job_title?getJobTitles($user->job_title):'';?></td>
					</tr>
                    <?php } ?>
                                        
                     <?php if($CFG->showReportTo == 1){?>
                     <tr>
						<th><?php echo get_string('report_to');?></th>
						<td><?php echo $user->report_to?$user->report_to:'';?></td>
					</tr>
                    <?php } ?>
                                        
                                         <tr>
						<th><?php echo get_string('ismanageryescsv');?></th>
						<td><?php echo $CFG->isManagerYesOptions[$user->is_manager_yes];?></td>
					</tr>
                    
                    <?php if($CFG->showCompany == 1){?>
                     <tr>
						<th><?php echo get_string('company', 'user');?></th>
						<td><?php echo $user->company?getCompany($user->company):'';?></td>
					</tr>
                     <?php } ?>
					<tr>
						<th><?php echo get_string('role');?></th>
						<td><?php echo ucwords($userRole);?></td>
					</tr>
					<tr>
						<th><?php echo get_string('location');?></th>
						<td><?php echo $userLocation;?></td>
					</tr>
					<tr>
						<th><?php echo get_string('department');?></th>
						<td><?php echo $department;?></td>
					</tr>
                    
                    <tr>
                    
						<th><?php echo $managerString;?></th>
						<td><?php echo $userManager;?></td>
					</tr>

					<tr>
						<th><?php echo get_string('team');?></th>
						<td><?php echo $groupString;?></td>
					</tr>
                                        <tr>
						<th><?php echo get_string('user_identifier');?></th>
						<td><?php echo $user->user_identifier;?></td>
					</tr>
					<tr>
						<th><?php echo get_string('description');?></th>
						<td><?php echo $user->description;?></td>
					</tr>
				</table>
				<div class = "edit-button">
					<?php
					
					    $cancelLink = getBackUrlForUserSection();
						$courseHTML .= '<input type = "button" value = "'.get_string('cancel').'" onclick="location.href=\''.$cancelLink.'\';">';
						
						$courseHTML .= '<input type = "button" value = "'.get_string('edit').'" onclick="location.href=\''.$CFG->wwwroot.'/user/editadvanced.php?course=1&id='.$user->id.'\';">';
						
						echo $courseHTML;
					?>
				</div>
			</div>
		</div>
	<?php
	echo '</div>';
echo $outerDivEnd;
/// and proper footer
?>
<script>
$(document).ready(function(){
	$(".edit-user-button").click(function(){
		window.location.href = $(this).attr('ref');
	});
});
</script>
<?php
echo $OUTPUT->footer();

