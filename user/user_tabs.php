<?php 

    $current1 = $current2 = $current3 = $current4 =''; 
    if(strstr($_SERVER['REQUEST_URI'], 'viewuser.php') || strstr($_SERVER['REQUEST_URI'], 'editadvanced.php')){
	  $current1 = 'current';   
	}elseif(strstr($_SERVER['REQUEST_URI'], 'assigncourses.php')){
	  $current2 = 'current';   
	}elseif(strstr($_SERVER['REQUEST_URI'], 'user_performance_report.php') || strstr($_SERVER['REQUEST_URI'], 'classroom_course_report.php')){
	  $current3 = 'current';   
	}elseif(strstr($_SERVER['REQUEST_URI'], 'courseallocation.php') ){
	  $current4 = 'current';   
	}
	$UrlView = 'javascript:void(0);';
	$assignView = 'javascript:void(0);';
	$reportView = 'javascript:void(0);';
	$allocationView = 'javascript:void(0);';
	$class = 'disabled-tab';

	if (!empty($user->id) && $user->id != '' && $user->id != 0 && $user->id != -1) {
		$UrlView		= $CFG->wwwroot."/user/viewuser.php?id=".$user->id."&course=".$SITE->id;
		$assignView		= $CFG->wwwroot."/user/assigncourses.php?id=".$user->id."&course=".$SITE->id;
		$reportView		= $CFG->wwwroot."/reports/user_performance_report.php?uid=".$user->id."&back=5&course=".$SITE->id;
		$allocationView = $CFG->wwwroot."/user/mybadges.php?id=".$user->id."&course=".$SITE->id;
		//$allocationView = 'javascript:void(0);';
		$class = '';
	}
	if($USER->archetype != $CFG->userTypeStudent){  // this tab bar should be hidden for student
		$userTabs .= "<div class='tabLinks'>";
		$userTabs .= "<div><a href='".$UrlView."' title='".get_string('general')."' class='$current1 $class'>".get_string('general')."<span></span></a></div>";
		$userTabs .= "<div><a href='".$assignView."' title='".get_string('courses')."' class='$current2 $class'>".get_string('courses')."<span></span></a></div>";
		$userTabs .= "<div><a href='".$reportView."' title='".get_string('reports')."' class='$current3 $class'>".get_string('reports')."<span></span></a></div>";
		//$userTabs .= "<div><a href='".$allocationView."' title='".get_string('certificates_and_badges')."' class='$current4 $class'>".get_string('certificates_and_badges')."<span></span></a></div>";
		echo $userTabs .= "</div>";
	}
?>