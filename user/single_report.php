<?php

	/**
		* Custom module - Signle Report Page
		* Date Creation - 25/06/2014
		* Date Modification : 25/06/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
	
	require_once('../config.php');
	require_once($CFG->libdir.'/gdlib.php');
	require_once($CFG->libdir.'/adminlib.php');
	require_once($CFG->dirroot.'/user/editlib.php');
	require_once($CFG->dirroot.'/user/profile/lib.php');
	require_once($CFG->dirroot.'/user/lib.php');
	
	//HTTPS is required in this page when $CFG->loginhttps enabled
	require_login();
	
	$sort    = optional_param('sort', 'mc.fullname', PARAM_ALPHANUM);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 10, PARAM_INT);        // how many per page
	$export = optional_param('action', '', PARAM_ALPHANUM);
	

    $sDate = '';
	$eDate = '';
		
	/*$sDate =  $CFG->reportSearchStartDate;
	$eDate = $CFG->reportSearchEndDate*/
	
	$sDepartment    = optional_param('department', '-1', PARAM_RAW);
	$sTeam          = optional_param('team', '-1', PARAM_RAW);       
	$sCourse          = optional_param('course', '-1', PARAM_RAW);       
	$sType          = optional_param('type', '-1', PARAM_RAW); 
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);
	
	
	//$userRole =  getUserRole($USER->id);
	if($USER->archetype == $CFG->userTypeStudent ) {
	//if(in_array($userRole, $CFG->customstudentroleid)){   // added by rajesh 
	  $id = $USER->id;  
	}else{
		$id      = optional_param('uid', $USER->id, PARAM_INT);    // user id; 
		if(!$id){
			redirect($CFG->wwwroot.'/admin/user.php');
		}
		checkUserAccess('user' , $id);
	}
	
	$user = $DB->get_record('user', array('id'=>$id), '*', MUST_EXIST);
	$userId = $user->id;
	
	$paramArray = array(
	                'uid' => $userId, 
					'department' => $sDepartment,
					'team' => $sTeam,
					'course' => $sCourse,
					'type' => $sType,
					'startDate' => $sStartDate,
					'endDate' => $sEndDate
				  );
	
	$removeKeyArray = array();
	
	

	$header = $SITE->fullname.": ".get_string('viewuserreports','singlereport');
	$PAGE->set_title($header);
	$PAGE->set_heading($header);
	$PAGE->set_pagelayout('globaladmin');
	
	if( $USER->archetype == $CFG->userTypeStudent ) {
	//if(in_array($userRole, $CFG->customstudentroleid)){ 
	  $PAGE->navbar->add(get_string('reports','singlereport'));
	  $PAGE->navbar->add(get_string('coursereport'));
	}else{
	  $PAGE->navbar->add(get_string('manage_user'),'/admin/user.php');
	  //$PAGE->navbar->add(get_string('viewuserreports','singlereport'));
	  $PAGE->navbar->add(getUsers($id));
	}

    

    $learnerReport = getLearnerReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
	$HTML = $learnerReport->HTML;
	   
	echo $OUTPUT->header(); 
	echo $HTML;
	echo includeGraphFiles(get_string('overallcourseprogressreport','singlereport'));
	echo $OUTPUT->footer();	
