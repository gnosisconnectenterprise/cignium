<?php

	/**
		* Custom module - User Performance Report Page
		* Date Creation - 03/07/2014
		* Date Modification : 03/07/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
		
	
	require_once("../config.php");

	$sort    = optional_param('sort', 'COALESCE(NULLIF(mu.firstname, ""), mu.lastname), mu.lastname', PARAM_ALPHANUM);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 10, PARAM_INT);        // how many per page
	
	$sDepartment    = optional_param('department', '-1', PARAM_RAW);
	$sTeam          = optional_param('team', '-1', PARAM_RAW);       
	$sUser          = optional_param('user', '-1', PARAM_RAW);       
	$export         = optional_param('action', '', PARAM_ALPHANUM);
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);
	$sType      = optional_param('type', '-1', PARAM_RAW);

	$user_group      = optional_param('user_group','-1', PARAM_RAW);
	$sel_mode      = optional_param('sel_mode', '1', PARAM_RAW);
	$pageURL = '/user/multiuser_report.php';
	checkPageIsNumeric($pageURL,$_REQUEST['page']);
	$paramArray = array(
					'ch' => optional_param('ch', '', PARAM_ALPHA),
					'key' => optional_param('key', '', PARAM_RAW),
					'department' => $sDepartment,
					'team' => $sTeam,
					'user' => $sUser,
					'startDate' => $sStartDate,
					'endDate' => $sEndDate,
					'type' => $sType,
					'user_group' => $user_group,
					'sel_mode' => $sel_mode,
					'job_title' => optional_param('job_title', '-1', PARAM_RAW),
					'company' => optional_param('company', '-1', PARAM_RAW)
				  );
	$filterArray = array(							
						'nm'=>get_string('name','course'),
				   );
	
	////// Define those key, which we need to remove first time //////
	$removeKeyArray = array('perpage');
	
	global $DB, $CFG, $USER;
	
	if( $USER->archetype == $CFG->userTypeStudent ) {
		$groupCourses = checkOwnerAccess($USER->id);
		if(!$groupCourses){
			redirect($CFG->wwwroot);
		}
	}

	$PAGE->set_pagelayout('globaladmin');
	//require_login(); 
	checkLogin();
	
	
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('usereperformancereport','multiuserreport'));
	//$PAGE->navbar->add(get_string('reports','multiuserreport'), new moodle_url($CFG->wwwroot.'/my/adminreportdashboard.php'));
	$PAGE->navbar->add(get_string('reports','multiuserreport'));
	$PAGE->navbar->add(get_string('usereperformancereport','multiuserreport'));
	
	echo $OUTPUT->header(); 
	
	$userPerformanceReport = UserPerformanceReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
	$userHTML = $userPerformanceReport->userHTML;
	echo $userHTML;
	echo includeGraphFiles(get_string('usereperformancereport','multiuserreport'));
	echo $OUTPUT->footer();


?>