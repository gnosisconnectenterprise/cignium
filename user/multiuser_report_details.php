<?php

/**
	* Custom module - Multi User Report Detail Page
	* Date Creation - 03/07/2014
	* Date Modification : 03/07/2014
	* Created By : Rajesh Kumar
	* Last Modified By : Rajesh Kumar
*/

require_once('../config.php');


//HTTPS is required in this page when $CFG->loginhttps enabled
//require_login(); 
checkLogin();
$id      = required_param('uid', PARAM_INT);    // user id; 

if( $USER->archetype == $CFG->userTypeStudent ) {
	$groupCourses = checkOwnerAccess($USER->id);
	if(!$groupCourses){
		redirect($CFG->wwwroot);
	}
}
	
$user = $DB->get_record('user', array('id'=>$id), '*', MUST_EXIST);
$userId = $user->id;

$sort    = optional_param('sort', 'mc.id', PARAM_ALPHANUM);
$dir     = optional_param('dir', 'DESC', PARAM_ALPHA);
$page    = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);        // how many per page
$export = optional_param('action', '', PARAM_ALPHANUM);
$back = optional_param('back', '', PARAM_ALPHANUM);


$sDate = '';
$eDate = '';
	
/*$sDate =  $CFG->reportSearchStartDate;
$eDate = $CFG->reportSearchEndDate*/

$sDepartment    = optional_param('department', '-1', PARAM_RAW);
$sTeam          = optional_param('team', '-1', PARAM_RAW);       
$sCourse          = optional_param('course', '-1', PARAM_RAW);       
$sType          = optional_param('type', '-1', PARAM_RAW); 
$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);

$sCid      = optional_param('cid', 0, PARAM_INT);


$paramArray = array(
				'uid' => $userId, 
				'department' => $sDepartment,
				'team' => $sTeam,
				'course' => $sCourse,
				'type' => $sType,
				'startDate' => $sStartDate,
				'endDate' => $sEndDate,
				'back' => $back,
				'cid' => $sCid
			  );

$removeKeyArray = array();


$PAGE->set_pagelayout('globaladmin');
$PAGE->set_heading($SITE->fullname);
$PAGE->set_title($SITE->fullname.": ".get_string('viewreportdetails','multiuserreport'));
//$PAGE->navbar->add(get_string('reports','multiuserreport'), new moodle_url($CFG->wwwroot.'/my/adminreportdashboard.php'));
$PAGE->navbar->add(get_string('reports','multiuserreport'));


if($sCid){

    if($back == 1){
	 $PAGE->navbar->add(get_string('coursereport','multicoursereport'), new moodle_url($CFG->wwwroot.'/course/course_report.php')); 
	 $PAGE->navbar->add(getCourses($sCid), new moodle_url($CFG->wwwroot.'/course/coursereportdetails.php?cid='.$sCid."&back=".$back));
	}elseif($back == 2){
	 $PAGE->navbar->add(get_string('courseusagesreport','multicoursereport'), new moodle_url($CFG->wwwroot.'/course/multicoursereport.php')); 
	 $PAGE->navbar->add(getCourses($sCid), new moodle_url($CFG->wwwroot.'/course/coursereportdetails.php?cid='.$sCid));
	}else{
	 $PAGE->navbar->add(get_string('coursereport','multicoursereport'), new moodle_url($CFG->wwwroot.'/course/course_report.php'));
	}
	
   
}else{
	if($back == 1){
	 $PAGE->navbar->add(get_string('userreport','multiuserreport'), new moodle_url($CFG->wwwroot.'/user/user_report.php'));
	}elseif($back == 3){
	 $PAGE->navbar->add(get_string('userreport','multiuserreport'), new moodle_url($CFG->wwwroot.'/user/user_report.php'));
	}else{
	 $PAGE->navbar->add(get_string('usereperformancereport','multiuserreport'), new moodle_url($CFG->wwwroot.'/user/multiuser_report.php'));
	}
}
//$PAGE->navbar->add(get_string('viewreportdetails','multiuserreport'));
$PAGE->navbar->add(getUsers($id));

echo $OUTPUT->header(); 


$learnerReport = getLearnerReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
$HTML = $learnerReport->HTML;
echo $HTML;
echo includeGraphFiles(get_string('overallcourseprogressreport','singlereport'));
?>

<script>


 $(document).ready(function(){
 
   scrollToElement($("#course-<?php echo $sCid?>"));
  /* if($('#course-<?php echo $sCid?>').length > 0 ){
	 $('#course-<?php echo $sCid?>').animate({scrollTop: -1000},1000);
   }*/
 
 });
</script>
<?php 
echo $OUTPUT->footer();	