<?php

	/**
		* Custom module - User Credit Hours Page
		* Date Creation - 24/07/2014
		* Date Modification : 24/07/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
		
	
	require_once("../config.php");
	

    //$userrole =  getUserRole($USER->id);
	if( $USER->archetype == $CFG->userTypeStudent ) {
		$groupCourses = checkOwnerAccess($USER->id);
		if(!$groupCourses){
			redirect($CFG->wwwroot);
		}
	}

	$PAGE->set_pagelayout('print');
	//require_login(); 
	checkLogin();

	$sort    = optional_param('sort', 'COALESCE(NULLIF(firstname, ""), lastname), lastname', PARAM_ALPHANUM);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 10, PARAM_INT);        // how many per page
	$pageURL = '/user/user_report.php';
	checkPageIsNumeric($pageURL,$_REQUEST['page']);
	
	$sDate = '';
	$eDate = '';
	$sDate =  $CFG->learningYear['from'];
	$eDate = $CFG->learningYear['to'];
	
	$sDepartment    = optional_param('department', '-1', PARAM_RAW);
	$sTeam          = optional_param('team', '-1', PARAM_RAW);       
	$sUser          = optional_param('user', '-1', PARAM_RAW);       
	$export         = optional_param('action', '', PARAM_ALPHANUM);
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);
	$sUserType     = optional_param('userType', '-1', PARAM_RAW);
	$user_group      = optional_param('user_group','-1', PARAM_RAW);

	$paramArray = array(
					'ch' => optional_param('ch', '', PARAM_ALPHA),
					'key' => optional_param('key', '', PARAM_RAW),
					'department' => $sDepartment,
					'team' => $sTeam,
					'user' => $sUser,
					'startDate' => $sStartDate,
					'endDate' => $sEndDate,
					'userType' => $sUserType,
					'user_group' => $user_group,
					'job_title' => optional_param('job_title', '-1', PARAM_RAW),
					'company' => optional_param('company', '-1', PARAM_RAW),
					'sel_mode' => optional_param('sel_mode', '-1', PARAM_RAW)
					
				  );
	$filterArray = array(							
						'nm'=>get_string('name','course'),
				   );
	
	////// Define those key, which we need to remove first time //////
	$removeKeyArray = array('perpage');
	
	global $DB, $CFG, $USER;
	

	$PAGE->set_pagelayout('globaladmin');
	require_login();
	
	
	$sUserTypeArr = explode("@",$sUserType);
	
	if($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2 ){
		$headerLabelGraph = get_string('usercredithoursreport');
	}elseif(in_array($CFG->userTypeInstructor, $sUserTypeArr)){
		$headerLabelGraph = get_string('instructorcredithoursreport');
		// $graphTitle = get_string('departmentvscredithoursofinstructor');
	}elseif(in_array($CFG->userTypeStudent, $sUserTypeArr)){
		$headerLabelGraph = get_string('learnercredithoursreport');
		//$graphTitle = get_string('departmentvscredithoursoflearner');
	}
	
	

	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('userreport','multiuserreport'));
	//$PAGE->navbar->add(get_string('reports','multiuserreport'), new moodle_url($CFG->wwwroot.'/my/adminreportdashboard.php'));
	$PAGE->navbar->add(get_string('reports','multiuserreport'));
	$PAGE->navbar->add(get_string('usercredithoursreport'));
	
	//$aa = getCourseIdByStatus(77);
	//pr($aa );die;

	//echo date("Y-m-d H:i:s", 1418216437)."===".date("Y-m-d H:i:s", 1357237800)."===".date("Y-m-d H:i:s", 1459362600);
	
	$userReport = getUserCreditHoursReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
	$userHTML = $userReport->userHTML;

		
	echo $OUTPUT->header(); 
	echo $userHTML;
	echo includeGraphFiles($headerLabelGraph);
	echo $OUTPUT->footer();


?>