<?php

/**
	* Custom module - Multi User Credit Hours Report Detail Page
	* Date Creation - 03/07/2014
	* Date Modification : 03/07/2014
	* Created By : Rajesh Kumar
	* Last Modified By : Rajesh Kumar
*/

require_once('../config.php');


//HTTPS is required in this page when $CFG->loginhttps enabled
//require_login(); 
checkLogin();


if( $USER->archetype == $CFG->userTypeStudent ) {
		$id      = optional_param('uid', $USER->id, PARAM_INT);
		if($id != $USER->id){
			$grpUsers = fetchGroupsUserIds($USER->id,1);
			if(empty($grpUsers) || !in_array($id,$grpUsers)){
				$id = $USER->id;
			}
		}
}else{
 $id = required_param('uid', PARAM_INT);    // user id; 
}
	
$user = $DB->get_record('user', array('id'=>$id), '*', MUST_EXIST);
$userId = $user->id;

$sort    = optional_param('sort', 'coursetype_id ASC, coursename', PARAM_ALPHANUM);
$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
$page    = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);        // how many per page
$export = optional_param('action', '', PARAM_ALPHANUM);
$back = optional_param('back', '', PARAM_ALPHANUM);


$sDate = '';
$eDate = '';
	
$sDate =  $CFG->learningYear['from'];
$eDate = $CFG->learningYear['to'];

$sDepartment    = optional_param('department', '-1', PARAM_RAW);
$sTeam          = optional_param('team', '-1', PARAM_RAW); 
$sUser          = optional_param('user', '-1', PARAM_RAW);        
$sCourse          = optional_param('course', '-1', PARAM_RAW);       
$sType          = optional_param('type', '-1', PARAM_RAW); 
$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);

/*if( $USER->archetype == $CFG->userTypeStudent ) {
  $sUserType     = $CFG->userTypeStudent;
}else{
  $sUserType     = optional_param('userType', $CFG->userTypeStudent, PARAM_ALPHA);
}*/
$sUserType     = optional_param('userType', '-1', PARAM_RAW);

$paramArray = array(
				'uid' => $userId, 
				'department' => $sDepartment,
				'team' => $sTeam,
				'user' => $sUser,
				'course' => $sCourse,
				'type' => $sType,
				'startDate' => $sStartDate,
				'endDate' => $sEndDate,
				'back' => $back,
				'userType' => $sUserType
			  );

$removeKeyArray = array();


$sUserTypeArr = explode("@",$sUserType);

if($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2 ){
	$headerLabelGraph = get_string('usercredithoursreport');
}elseif(in_array($CFG->userTypeInstructor, $sUserTypeArr)){
	$headerLabelGraph = get_string('overallinstructorcoursecredithoursreport');
	// $graphTitle = get_string('departmentvscredithoursofinstructor');
}elseif(in_array($CFG->userTypeStudent, $sUserTypeArr)){
	$headerLabelGraph = get_string('overalllearnercoursecredithoursreport');
	//$graphTitle = get_string('departmentvscredithoursoflearner');
}

	
$PAGE->set_heading($SITE->fullname);
$PAGE->set_title($SITE->fullname.": ".get_string('usercredithoursreportdetails'));
$PAGE->navbar->add(get_string('reports','multiuserreport'));
if( $USER->archetype == $CFG->userTypeStudent ) {
  $PAGE->navbar->add(get_string('usercredithoursreport'));
}else{
 $PAGE->navbar->add(get_string('usercredithoursreport'), new moodle_url($CFG->wwwroot.'/user/user_credithours_report.php'));
 $PAGE->navbar->add(getUsers($id));
}
$learnerReport = getUserCourseCreditHoursReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
$HTML = $learnerReport->HTML;
   
echo $OUTPUT->header(); 
echo $HTML;
echo includeGraphFiles($headerLabelGraph);
echo $OUTPUT->footer();	
