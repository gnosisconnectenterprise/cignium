<?php

require_once(dirname(__FILE__) . '/../config.php');
require_once(dirname(__FILE__) . '/lib.php');
require_once($CFG->dirroot . '/local/user/selector/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->libdir . '/filelib.php');
require_login();
$site = get_site();
$userid = optional_param('id',0, PARAM_INT);
$cancel  = optional_param('cancel', false, PARAM_BOOL);
$id      = optional_param('uid', $USER->id, PARAM_INT);    // user id; 
if(!$userid){
	//redirect($CFG->wwwroot.'/admin/user.php');
}
checkUserAccess('user' , $userid);
GLOBAL $DB;

//$courseStatusArr = getCourseIdByStatus($userid);
/*$popularBadges = $DB->get_records_sql("SELECT DISTINCT(b.id),b.name,b.picture,bi.userid,count(b.id)
										FROM mdl_badge AS b
										LEFT JOIN mdl_badge_issued AS bi ON bi.badgeid = b.id
										GROUP BY b.id
										ORDER BY count(b.id) DESC"
									);*/
$PAGE->set_url('/user/mybadges.php', array('id'=>$userid));
$PAGE->set_pagelayout('admin');


$context = context_system::instance();
$returnurl = $CFG->wwwroot.'/admin/user.php';

if ($cancel) {
    redirect($returnurl);
}

$PAGE->navbar->add(get_string('manage_user'), new moodle_url($CFG->wwwroot.'/admin/user.php'));
$PAGE->navbar->add(get_string('mybadges'));

/// Print header
$PAGE->set_title("$site->fullname: $stradduserstogroup");
$PAGE->set_heading($site->fullname);
echo $OUTPUT->header();

/// Print the editing form
$user->id = $userid;
echo $outerDivStart = "<div class='tabsOuter'>";
	$outerDivEnd = "</div>";
	include_once('user_tabs.php');
	/*echo '<div class="userprofile">';
	echo '<div class="left-content-badges">';
	echo '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
	echo '<tr class = "tr-header">';
		echo '<th width="25%">';
		echo 'Badges Earned';
		echo '</th>';
		echo '<th width="75%">';
		echo 'Description';
		echo '</th>';
	echo '</tr>';
	if(!empty($courseStatusArr['type'][2])){
		$i=1;
		foreach($courseStatusArr['type'][2] as $course){
			$badges = $DB->get_records('badge',array('courseid'=>$course));
			if(!empty($badges)){
				foreach($badges as $badge){
					if($i %2 == 0){
						$class = 'tr-even';
					}else{
						$class = 'tr-odd';
					}
					$path = $CFG->wwwroot.'/theme/gourmet/pix/badge/'.$badge->picture; 
					echo '<tr class = "'.$class.'">';
					echo '<td align="center">';
					echo '<div class = "badge-image">';
					echo '<img src = "'.$path.'">';
					echo '</div>';
					echo '<div class = "badge-name">';
					echo $badge->name;
					echo '</div>';
					echo '</td>';

					echo '<td valign="top">';
					echo '<div class = "badge-name" >';
						echo $badge->description;
					echo '</div>';
					echo '<div class="share"> 
								<!--div><a target="_blank" href="http://www.twitter.com/compunnel"><i class="fa fa-twitter"></i></a></div>
								<div><a target="_blank" href="http://www.facebook.com/compunnel"><i class="fa fa-facebook"></i></a></div>
								<div><a target="_blank" href="http://www.linkedin.com/in/compunnel"><i class="fa fa-linkedin"></i></a></div-->		  
						</div>';
					echo '</td>';
					echo '</tr>';
					$i++;
				}
			}
?>
<?php
		}
	}else{
		echo '<tr>';
			echo '<td colspan = "2">';
			echo 'No badges earned yet';
			echo '</td>';
		echo '</tr>';
	}
	echo '</table>';
echo '</div>';

echo '<div class="right-content-badges" >';
	echo '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
	echo '<tr>';
		echo '<th >';
		echo 'Popular badges';
		echo '</th>';
	echo '</tr>';
	if(!empty($popularBadges)){
		$j=1;
		foreach($popularBadges as $badge){
					if($j %2 == 0){
						$class = 'tr-even';
					}else{
						$class = 'tr-odd';
					}
					$path = $CFG->wwwroot.'/theme/gourmet/pix/badge/'.$badge->picture; 
					echo '<tr class = "'.$class.'">';
						echo '<td>';
							echo '<div class = "badge-image" >';
								echo '<img src = "'.$path.'">';
							echo '</div>';
							echo '<div class = "badge-name" >';
								echo $badge->name;
							echo '</div>';
						echo '</td>';
					echo '</tr>';
					$j++;
?>
<?php
		}
	}else{
		echo '<tr>';
			echo '<td>';
			echo 'No popular badges';
			echo '</td>';
		echo '</tr>';
	}
	echo '</table>';
echo '</div>';
echo '</div>';*/
echo $outerDivEnd;
echo '<table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-top: 20px;">';
	echo '<tr align="center" >';
		echo '<td style="border: medium none;">';
		echo "Coming Soon";
		echo '</td>';
	echo '</tr>';
echo '</table>';

echo $OUTPUT->footer();
?>