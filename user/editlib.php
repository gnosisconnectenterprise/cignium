<?php

function cancel_email_update($userid) {
    unset_user_preference('newemail', $userid);
    unset_user_preference('newemailkey', $userid);
    unset_user_preference('newemailattemptsleft', $userid);
}

function useredit_load_preferences(&$user, $reload=true) {
    global $USER;

    if (!empty($user->id)) {
        if ($reload and $USER->id == $user->id) {
            // reload preferences in case it was changed in other session
            unset($USER->preference);
        }

        if ($preferences = get_user_preferences(null, null, $user->id)) {
            foreach($preferences as $name=>$value) {
                $user->{'preference_'.$name} = $value;
            }
        }
    }
}

function useredit_update_user_preference($usernew) {
    $ua = (array)$usernew;
    foreach($ua as $key=>$value) {
        if (strpos($key, 'preference_') === 0) {
            $name = substr($key, strlen('preference_'));
            set_user_preference($name, $value, $usernew->id);
        }
    }
}

/**
 * Updates the provided users profile picture based upon the expected fields
 * returned from the edit or edit_advanced forms.
 *
 * @global moodle_database $DB
 * @param stdClass $usernew An object that contains some information about the user being updated
 * @param moodleform $userform The form that was submitted to edit the form
 * @return bool True if the user was updated, false if it stayed the same.
 */
function useredit_update_picture(stdClass $usernew, moodleform $userform, $filemanageroptions = array()) {
    global $CFG, $DB;
    require_once("$CFG->libdir/gdlib.php");

    $context = context_user::instance($usernew->id, MUST_EXIST);
    $user = $DB->get_record('user', array('id'=>$usernew->id), 'id, picture', MUST_EXIST);

    $newpicture = $user->picture;
    // Get file_storage to process files.
    $fs = get_file_storage();
    if (!empty($usernew->deletepicture)) {
        // The user has chosen to delete the selected users picture
        $fs->delete_area_files($context->id, 'user', 'icon'); // drop all images in area
        $newpicture = 0;

    } else {
        // Save newly uploaded file, this will avoid context mismatch for newly created users.
        file_save_draft_area_files($usernew->imagefile, $context->id, 'user', 'newicon', 0, $filemanageroptions);
        if (($iconfiles = $fs->get_area_files($context->id, 'user', 'newicon')) && count($iconfiles) == 2) {
            // Get file which was uploaded in draft area
            foreach ($iconfiles as $file) {
                if (!$file->is_directory()) {
                    break;
                }
            }
            // Copy file to temporary location and the send it for processing icon
            if ($iconfile = $file->copy_content_to_temp()) {
                // There is a new image that has been uploaded
                // Process the new image and set the user to make use of it.
                // NOTE: Uploaded images always take over Gravatar
                $newpicture = (int)process_new_icon($context, 'user', 'icon', 0, $iconfile);
                // Delete temporary file
                @unlink($iconfile);
                // Remove uploaded file.
                $fs->delete_area_files($context->id, 'user', 'newicon');
            } else {
                // Something went wrong while creating temp file.
                // Remove uploaded file.
                $fs->delete_area_files($context->id, 'user', 'newicon');
                return false;
            }
        }
    }

    if ($newpicture != $user->picture) {
        $DB->set_field('user', 'picture', $newpicture, array('id' => $user->id));
        return true;
    } else {
        return false;
    }
}

function useredit_update_bounces($user, $usernew) {
    if (!isset($usernew->email)) {
        //locked field
        return;
    }
    if (!isset($user->email) || $user->email !== $usernew->email) {
        set_bounce_count($usernew,true);
        set_send_count($usernew,true);
    }
}

function useredit_update_trackforums($user, $usernew) {
    global $CFG;
    if (!isset($usernew->trackforums)) {
        //locked field
        return;
    }
    if ((!isset($user->trackforums) || ($usernew->trackforums != $user->trackforums)) and !$usernew->trackforums) {
        require_once($CFG->dirroot.'/mod/forum/lib.php');
        forum_tp_delete_read_records($usernew->id);
    }
}

function useredit_update_interests($user, $interests) {
    tag_set('user', $user->id, $interests);
}

function useredit_shared_definition(&$mform, $editoroptions = null, $filemanageroptions = null,$userData) {
    global $CFG, $USER, $DB;
	
	//$userrole =  getUserRole($USER->id);
    $disable_fields = "";
    if($CFG->isLdap==1){
    	$disable_fields = 'class="disabled_user_form_fields" disabled="disabled"';
    }
   
    $user = $DB->get_record('user', array('id' => $USER->id));
    useredit_load_preferences($user, false);

    $strrequired = get_string('required');

    // Add the necessary names.
    foreach (useredit_get_required_name_fields() as $fullname) {
        $mform->addElement('text', $fullname,  get_string($fullname),  'maxlength="100" size="30"'.$disable_fields);
        $mform->addRule($fullname, $strrequired, 'required', null, 'client');
        $mform->setType($fullname, PARAM_NOTAGS);
    }

    $enabledusernamefields = useredit_get_enabled_name_fields();
    // Add the enabled additional name fields.
    foreach ($enabledusernamefields as $addname) {
        $mform->addElement('text', $addname,  get_string($addname), 'maxlength="100" size="30"');
        $mform->setType($addname, PARAM_NOTAGS);
    }

    // Do not show email field if change confirmation is pending
    if (!empty($CFG->emailchangeconfirmation) and !empty($user->preference_newemail)) {
        $notice = get_string('emailchangepending', 'auth', $user);
        $notice .= '<br /><a href="edit.php?cancelemailchange=1&amp;id='.$user->id.'">'
                . get_string('emailchangecancel', 'auth') . '</a>';
        $mform->addElement('static', 'emailpending', get_string('email'), $notice);
    } else {
        $mform->addElement('text', 'email', get_string('email'), 'maxlength="100" size="30"' .$disable_fields);
        $mform->addRule('email', $strrequired, 'required', null, 'client');
        $mform->addRule('email', get_string('invalidemail'), 'email', null, 'client');
        $mform->setType('email', PARAM_EMAIL);
    }
     
	
    if( $USER->archetype == $CFG->userTypeStudent || $USER->archetype == $CFG->userTypeAdmin ) { 
	    $mform->addElement('text', 'phone1', get_string('phone'), 'maxlength="20" size="25"' .$disable_fields);
		//$mform->addRule('phone1', get_string('phonenumbererror','plugin'), 'numeric', null, 'client');
		$mform->setType('phone1', PARAM_NOTAGS);
	}
	
	createSelectBoxFor($mform, 'job_title', '', $disable_fields);
	createSelectBoxFor($mform, 'company', '', $disable_fields);
	
	$archtype == '';
	if($USER->archetype == $CFG->userTypeAdmin){
		$archtype = "'".implode("','",$CFG->userTypeInSystem)."'";
	}elseif($USER->archetype == $CFG->userTypeManager){
		$archtype = "'".implode("','",$USER->userAccess)."'";
	}elseif($USER->archetype == $CFG->userTypeStudent){
		$archtype = "'".$CFG->userTypeStudent."'";
	}else{
		$archtype = "'".implode("','",$CFG->userTypeInSystem)."'";
	}
	if($_SERVER['PHP_SELF'] != '/user/editprofile.php'){
		$roles = $DB->get_records_sql("SELECT id,name FROM {$CFG->prefix}role WHERE name IN ($archtype)"); // v1.1 change - add new role manager.
		$roleselect = array('0'=>get_string('select_role','plugin'));
		foreach($roles as $role){
			$roleselect[$role->id] = ucwords($role->name);
		}
		$displayPrimary = 'style = "display:none;"';

		$mform->addElement('select', 'role', get_string('role'), $roleselect);
		$mform->addRule('role', get_string('required'), 'required', null, 'client');
		$mform->addRule('role', get_string('required'), 'nonzero', 'nonzero', 'client');
		$userRole = getUserRoleDetails($userData->id);
		
		if($USER->archetype == $CFG->userTypeManager){
			//$mform->hardFreeze('role');
		}

		$mform->addElement('advcheckbox', 'is_instructor', get_string('is_instructor'));
		$mform->setDefault('is_instructor', 0);
		
		// Set instructor
		if(!empty($userData) || !empty($_POST['is_instructor'])){
			if(isset($userData)){
				//$userId = $userData->id;
			}else{
				//$userId = $_POST['id'];
			}
		}
        
		/*$depQuery = "SELECT id,title, is_external FROM {$CFG->prefix}department ";
		$depQuery .= " WHERE deleted = 0 AND status = 1 ";
		if($CFG->showExternalDepartment != 1){
		 $depQuery .= " AND is_external = '0' ";
		}
		$depQuery .= " order by title ASC";
		$departments = $DB->get_records_sql($depQuery);
		$departmentselect = array('0'=>get_string('select_department'));
		foreach($departments as $department){
		   $identifierED = $department->is_external == 1?getEDAstric('input'):'';
		   $departmentselect[$department->id] = $identifierED.$department->title;
		}*/
		$displayDepartment = 'style = "display:none;"';
		$showDepartment = 0;
		$showDepartmentManager = 0;
		$displayDepartmentManager = 'style = "display:none;"';
		if(isset($_POST['role'])){
			 $role = $DB->get_record("role",array('id'=>$_POST['role']));
			 if($role->name != $CFG->userTypeAdmin){
				$showDepartment = 1;
			 }
			 if($role->name != $CFG->userTypeStudent){
				$showDepartmentManager = 1;
			 }
			 if($role->name == $CFG->userTypeManager && $USER->archetype == $CFG->userTypeAdmin){
				$displayPrimary = 'style = "display:block;"';
			 }
		}else{
			 if($userRole->name == $CFG->userTypeManager && $USER->archetype == $CFG->userTypeAdmin){
				$displayPrimary = 'style = "display:block;"';
			 }
		}
		if((!empty($userRole) && $userRole->name != $CFG->userTypeAdmin) || $showDepartment){
			$displayDepartment = 'style = "display:block;"';
		}
		if((!empty($userRole) && $userRole->name == $CFG->userTypeStudent) || $showDepartmentManager){
			$displayDepartmentManager = 'style = "display:block;"';
		}
		
		createDepartmentSelectBox($mform, 'edit', 'user', $displayDepartment, '');
		
		/*$mform->addElement('html', '<div id = "department_select" '.$displayDepartment.'>');
			$mform->addElement('select', 'department', get_string('department'), $departmentselect,$disable_fields);
			if($userRole->name == $CFG->userTypeManager){
				//$mform->getElement('department')->setMultiple(true);
			}
			
			if($CFG->showExternalDepartment == 1){
		     $identifierED = getEDAstric('label');
		     $mform->addElement('static', 'external_department_instruction', '',$identifierED);
	        }
		  
		$mform->addElement('html', '</div>');*/


		$mform->addElement('html', '<div id = "is_primary" '.$displayPrimary.'>');
			$mform->addElement('checkbox', 'is_primary', get_string('is_primary'));
			$mform->setDefault('is_primary', 0);
		$mform->addElement('html', '</div>');

		if($userData->id && $userData->id != -1){
			$userDepartment = $DB->get_records_sql("SELECT departmentid FROM {$CFG->prefix}department_members WHERE userid = ".$userData->id);
			$userDepartments = array();
			if($userDepartment){
				foreach($userDepartment as $department){
					$userDepartments[] = $department->departmentid;
				}
				//$mform->getElement('department')->setSelected($userDepartments);
			}
			if($USER->archetype == $CFG->userTypeManager && !empty($userDepartments)){
				$mform->hardFreeze('department');
			}
		}else{
			$userDepartment = $DB->get_records_sql("SELECT departmentid FROM {$CFG->prefix}department_members WHERE userid = ".$USER->id);
			$userDepartments = array();
			if($userDepartment){
				foreach($userDepartment as $department){
					$userDepartments[] = $department->departmentid;
				}
				//$mform->getElement('department')->setSelected($userDepartments);
			}
			if($USER->archetype == $CFG->userTypeManager && !empty($userDepartments)){
				$mform->hardFreeze('department');
				$mform->SetDefault('department',$userDepartments[0]);
			}
		}
		//$mform->addRule('department', get_string('required'), 'required', null, 'client');
		
		/* v1.1 Change add department managers listing start*/
		//$displayDepartmentManager = 'style = "display:none;"';
		if($userData->id && $userData->id != -1){
			$userDepartment = $DB->get_record_sql("SELECT departmentid FROM {$CFG->prefix}department_members WHERE userid = ".$userData->id);
		}else{
			$userDepartment = $DB->get_record_sql("SELECT departmentid FROM {$CFG->prefix}department_members WHERE userid = ".$USER->id);
		}
		if($USER->archetype == $CFG->userTypeAdmin){
			
			if((!empty($userDepartment->departmentid)) || !empty($_POST['department'])){
				if(!empty($_POST['department'])){
					$departmentId = $_POST['department'];
				}elseif(!empty($userDepartment->departmentid)){
					$departmentId = $userDepartment->departmentid;
				}else{
					$departmentId = 0;
				}
				
				$departmentManagers = getUsersList(array($CFG->userTypeManager),$departmentId);
				$departmentManagerSelect = array('0'=>get_string('select_department_manager'));
				if(!empty($departmentManagers)){
					foreach($departmentManagers as $departmentManager){
						if($departmentManager->id != $userData->id){
							$departmentManagerSelect[$departmentManager->id] = $departmentManager->firstname.' '.$departmentManager->lastname;
						}
					}
				}
				if(!empty($departmentManagerSelect)){
					//$displayDepartmentManager = 'style = "display:block"';
				}
			}
			else{
				$departmentManagerSelect = array('0'=>get_string('select_department_manager'));
			}
		}else{
			$userDepartment = $DB->get_record_sql("SELECT departmentid FROM {$CFG->prefix}department_members WHERE userid = ".$USER->id); 
			if(!empty($_POST['department'])){
				$departmentId = $_POST['department'];
			}elseif(!empty($userDepartment->departmentid)){
				$departmentId = $userDepartment->departmentid;
			}else{
				$departmentId = 0;
			}
			$departmentManagers = getUsersList(array($CFG->userTypeManager),$departmentId);
			$departmentManagerSelect = array('0'=>get_string('select_department_manager'));
			if(!empty($departmentManagers)){
				foreach($departmentManagers as $departmentManager){
					if($departmentManager->id != $userData->id){
						$departmentManagerSelect[$departmentManager->id] = $departmentManager->firstname.' '.$departmentManager->lastname;
					}
				}
			}
			//$displayDepartmentManager = 'style = "display:block"';
		}
		$mform->addElement('html', '<div id = "department_manager" '.$displayDepartmentManager.'>');
			$mform->addElement('select', 'department_manager', get_string('department_manager'), $departmentManagerSelect);
		$mform->addElement('html', '</div>');
		if($userData->id && $userData->id != -1 && $userData->parent_id  != 0){
			$userRole = $DB->get_record_sql("SELECT ra.roleid,u.id FROM {$CFG->prefix}role_assignments as ra LEFT JOIN {$CFG->prefix}user as u ON u.id = ra.userid WHERE ra.userid = ".$userData->id);

			if(isset($_POST['role']) || $userRole){
				if(isset($_POST['role'])){
					$roleId = $_POST['role'];
				}else{
					$roleId = $userRole->roleid;
				}
				$role = $DB->get_record("role",array('id'=>$roleId));
				if($role->name == $CFG->userTypeStudent){
					$mform->setDefault('department_manager',$userData->parent_id);
					//$mform->addRule('department_manager', get_string('required'), 'required', null, 'client');
				}
			}
		}else{
			if($USER->archetype == $CFG->userTypeManager ) { 
				$mform->setDefault('department_manager',$USER->id);
				//$mform->addRule('department_manager', get_string('required'), 'required', 'nonzero', 'client');
			}
		}
		/* v1.1 Change add department managers listing end*/
		/*$searchString = '';
		if($USER->archetype != $CFG->userTypeAdmin ) { 
			$searchString .= " WHERE createdby = ".$USER->id;
		}
		$teams = $DB->get_records_sql("SELECT id,name FROM {$CFG->prefix}groups".$searchString);*/
		if($userData->id && $userData->id != -1){
			$userDepartment = $DB->get_record_sql("SELECT departmentid FROM {$CFG->prefix}department_members WHERE userid = ".$userData->id);
			if(!empty($_POST['department'])){
				$departmentId = $_POST['department'];
			}elseif(!empty($userDepartment->departmentid)){
				$departmentId = $userDepartment->departmentid;
			}else{
				$departmentId = 0;
			}
			if($userDepartment){
				$teams = getDepartmentteams($departmentId);
			}else{
				$teams = array(); 
			}
		}else{
			if(!empty($_POST['department'])){
				$departmentId = $_POST['department'];
				$teams = getDepartmentteams($departmentId);
			}else{
				$userDepartment = $DB->get_record_sql("SELECT departmentid FROM {$CFG->prefix}department_members WHERE userid = ".$USER->id);
				$teams = getDepartmentteams($userDepartment->departmentid);
			}
		}
		
		
		$teamselect = array('0'=>get_string('select_team'));
		$teamIds = array();
		
		//pr($teams);die;
		if(!empty($teams)){
			foreach($teams as $team){
				$teamselect[$team->id] = $team->name;
				$teamIds[] = $team->id;
			}
		}
		
		
		$select = $mform->addElement('select', 'team', get_string('team','group'), $teamselect,$disable_fields);
		$mform->getElement('team')->setMultiple(true);
		$displayUserGroup = "";
		if($USER->archetype == $CFG->userTypeManager || $USER->archetype == $CFG->userTypeStudent){
			$displayUserGroup = "style='display:none;'";
		}
		$mform->addElement('html', '<div id = "show-user-group" '.$displayUserGroup.'>');
		$userTeam = $DB->get_records_sql("SELECT g.id,g.name FROM mdl_groups as g LEFT JOIN mdl_group_department as gd ON gd.team_id = g.id WHERE gd.id IS NULL ORDER BY g.name ASC");
		$userTeamSelect[0] = get_string("select_user_group");
		if(!empty($userTeam)){
			foreach($userTeam as $uteam){
				$userTeamSelect[$uteam->id] = $uteam->name;
			}
		}
		$select = $mform->addElement('select', 'user_team', get_string('global_team'), $userTeamSelect,$disable_fields);
		$mform->getElement('user_team')->setMultiple(true);
		$mform->addElement('html', '</div>');

		/*if($userData->id && $userData->id != -1){
			$userDepartment = $DB->get_record_sql("SELECT departmentid FROM {$CFG->prefix}department_members WHERE userid = ".$userData->id);
			$where = '';
			if(!empty($teamIds)){
				$teamNotIn = implode(',',$teamIds);
				$where = " AND gm.groupid NOT IN ($teamNotIn)";
			}
			$userGroups = $DB->get_records_sql("SELECT g.id FROM {$CFG->prefix}groups_members as gm LEFT JOIN {$CFG->prefix}groups as g ON g.id = gm.groupid WHERE g.createdby != '".$USER->id."' AND gm.is_active = '1' AND gm.userid = ".$userData->id.$where);
			//pr($userGroups);die;
		}else{
			$userDepartment = $DB->get_record_sql("SELECT departmentid FROM {$CFG->prefix}department_members WHERE userid = ".$USER->id);
			$userGroups = $DB->get_records_sql("SELECT g.id FROM {$CFG->prefix}groups_members as gm LEFT JOIN {$CFG->prefix}groups as g ON g.id = gm.groupid WHERE g.createdby != '".$USER->id."' AND gm.is_active = '1' AND gm.userid = ".$USER->id);
		}
		//pr($userGroups);die;
		if(!empty($userGroups)){
			foreach($userGroups as $key=>$value){
				$mform->addElement("html", "<input type='hidden' name='team[]' value='".$value->id."' />");

			}
		}*/
	}

	if( $USER->archetype != $CFG->userTypeStudent && $USER->archetype != $CFG->userTypeAdmin   && $USER->archetype != $CFG->userTypeManager) {  // added by rajesh
	
		$choices = array();
		$choices['0'] = get_string('emaildisplayno');
		$choices['1'] = get_string('emaildisplayyes');
		$choices['2'] = get_string('emaildisplaycourse');
		$mform->addElement('select', 'maildisplay', get_string('emaildisplay'), $choices);
		$mform->setDefault('maildisplay', 2);
		
		$choices = array();
		$choices['0'] = get_string('textformat');
		$choices['1'] = get_string('htmlformat');
		$mform->addElement('select', 'mailformat', get_string('emailformat'), $choices);
		$mform->setDefault('mailformat', 1);
		
	}else{
	    $mform->addElement('hidden', 'maildisplay');
		$mform->setDefault('maildisplay', 1);
		
		$mform->addElement('hidden', 'mailformat');
		$mform->setDefault('mailformat', 1);
	}

   

    if (!empty($CFG->allowusermailcharset)) {
        $choices = array();
        $charsets = get_list_of_charsets();
        if (!empty($CFG->sitemailcharset)) {
            $choices['0'] = get_string('site').' ('.$CFG->sitemailcharset.')';
        } else {
            $choices['0'] = get_string('site').' (UTF-8)';
        }
        $choices = array_merge($choices, $charsets);
        $mform->addElement('select', 'preference_mailcharset', get_string('emailcharset'), $choices);
    }


    if( $USER->archetype != $CFG->userTypeStudent && $USER->archetype != $CFG->userTypeAdmin   && $USER->archetype != $CFG->userTypeManager) {  // added by rajesh
	
		$choices = array();
		$choices['0'] = get_string('emaildigestoff');
		$choices['1'] = get_string('emaildigestcomplete');
		$choices['2'] = get_string('emaildigestsubjects');
		$mform->addElement('select', 'maildigest', get_string('emaildigest'), $choices);
		$mform->setDefault('maildigest', 0);
		$mform->addHelpButton('maildigest', 'emaildigest');
	
		$choices = array();
		$choices['1'] = get_string('autosubscribeyes');
		$choices['0'] = get_string('autosubscribeno');
		$mform->addElement('select', 'autosubscribe', get_string('autosubscribe'), $choices);
		$mform->setDefault('autosubscribe', 1);
	
		if (!empty($CFG->forum_trackreadposts)) {
			$choices = array();
			$choices['0'] = get_string('trackforumsno');
			$choices['1'] = get_string('trackforumsyes');
			$mform->addElement('select', 'trackforums', get_string('trackforums'), $choices);
			$mform->setDefault('trackforums', 0);
		}
		
		$editors = editors_get_enabled();
		if (count($editors) > 1) {
			$choices = array('' => get_string('defaulteditor'));
			$firsteditor = '';
			foreach (array_keys($editors) as $editor) {
				if (!$firsteditor) {
					$firsteditor = $editor;
				}
				$choices[$editor] = get_string('pluginname', 'editor_' . $editor);
			}
			$mform->addElement('select', 'preference_htmleditor', get_string('textediting'), $choices);
			$mform->setDefault('preference_htmleditor', '');
		} else {
			// Empty string means use the first chosen text editor.
			$mform->addElement('hidden', 'preference_htmleditor');
			$mform->setDefault('preference_htmleditor', '');
			$mform->setType('preference_htmleditor', PARAM_PLUGIN);
		}
		
    }else{
	
	    $mform->addElement('hidden', 'maildigest');
		$mform->setDefault('maildigest', 0);
		
		$mform->addElement('hidden', 'autosubscribe');
		$mform->setDefault('autosubscribe', 1);
		
		if (!empty($CFG->forum_trackreadposts)) {
			$mform->addElement('hidden', 'trackforums');
			$mform->setDefault('trackforums', 0);
		}
		
		// Empty string means use the first chosen text editor.
		$mform->addElement('hidden', 'preference_htmleditor');
		$mform->setDefault('preference_htmleditor', '');
		$mform->setType('preference_htmleditor', PARAM_PLUGIN);
	}
	
   

    $mform->addElement('text', 'city', get_string('city'), 'maxlength="120" size="21"' .$disable_fields);
    $mform->setType('city', PARAM_TEXT);
    if (!empty($CFG->defaultcity)) {
        $mform->setDefault('city', $CFG->defaultcity);
    }

    $choices = get_string_manager()->get_list_of_countries();
    if($CFG->isdisplayextrauserfields){
	    $choices= array(''=>get_string('selectacountry')) + $choices;
           
	    $mform->addElement('select', 'country', get_string('country'), $choices);
	    if (!empty($CFG->country)) {
	        $mform->setDefault('country', $CFG->country);
	    }
            
            $stateRecs = $DB->get_records('states');
            $stateselect = array('0'=>get_string('select_state'));
            if($stateRecs){
                    foreach($stateRecs as $stateRec){
                            $stateselect[$stateRec->id] = $stateRec->state;
                    }
            }
            $mform->addElement('select', 'state', get_string('state_label'), $stateselect);
            $mform->setDefault('state', $user->state);
            $mform->disabledIf('state', 'country', 'neq', "US");
    }
	if( $USER->archetype != $CFG->userTypeStudent && $USER->archetype != $CFG->userTypeAdmin   && $USER->archetype != $CFG->userTypeManager) {  // added by rajesh

		$choices = get_list_of_timezones();
		$choices['99'] = get_string('serverlocaltime');
		if ($CFG->forcetimezone != 99) {
			$mform->addElement('static', 'forcedtimezone', get_string('timezone'), $choices[$CFG->forcetimezone]);
		} else {
			$mform->addElement('select', 'timezone', get_string('timezone'), $choices);
			$mform->setDefault('timezone', 99);
		}
	
		$mform->addElement('select', 'lang', get_string('preferredlanguage'), get_string_manager()->get_list_of_translations());
		$mform->setDefault('lang', $CFG->lang);
		
		// Multi-Calendar Support - see MDL-18375.
		$calendartypes = \core_calendar\type_factory::get_list_of_calendar_types();
		// We do not want to show this option unless there is more than one calendar type to display.
		if (count($calendartypes) > 1) {
			$mform->addElement('select', 'calendartype', get_string('preferredcalendar', 'calendar'), $calendartypes);
			$mform->setDefault('calendartype', $CFG->calendartype);
		}
	
		if (!empty($CFG->allowuserthemes)) {
			$choices = array();
			$choices[''] = get_string('default');
			$themes = get_list_of_themes();
			foreach ($themes as $key=>$theme) {
				if (empty($theme->hidefromselector)) {
					$choices[$key] = get_string('pluginname', 'theme_'.$theme->name);
				}
			}
			$mform->addElement('select', 'theme', get_string('preferredtheme'), $choices);
		}
	
		$mform->addElement('editor', 'description_editor', get_string('userdescription'), null, $editoroptions);
		$mform->setType('description_editor', PARAM_CLEANHTML);
		$mform->addHelpButton('description_editor', 'userdescription');
    }else{
	
	    $choices = get_list_of_timezones(); 
		if ($CFG->forcetimezone != 99) {
			$mform->addElement('hidden', 'timezone');
			$mform->setDefault('timezone', $choices[$CFG->forcetimezone]);
		} else {
			$mform->addElement('hidden', 'timezone');
			$mform->setDefault('timezone', 99);
		}
	
		$mform->addElement('hidden', 'lang');
		$mform->setDefault('lang', $CFG->lang);
	
		// Multi-Calendar Support - see MDL-18375.
		$calendartypes = \core_calendar\type_factory::get_list_of_calendar_types();
		// We do not want to show this option unless there is more than one calendar type to display.
		if (count($calendartypes) > 1) {
			$mform->addElement('hidden', 'calendartype');
			$mform->setDefault('calendartype', $CFG->calendartype);
		}
	
	
		$mform->addElement('hidden', 'description_editor');
		
	}
    

    if (empty($USER->newadminuser)) {
		 if( $USER->archetype != $CFG->userTypeStudent && $USER->archetype != $CFG->userTypeAdmin   && $USER->archetype != $CFG->userTypeManager) {
			$mform->addElement('header', 'moodle_picture', get_string('pictureofuser'));
		 }else{
			//$mform->addElement('header', 'moodle_picture', get_string('pictureofuser'));
		 }

        if (!empty($CFG->enablegravatar)) {
            //$mform->addElement('html', html_writer::tag('p', get_string('gravatarenabled')));
        }

        //$mform->addElement('static', 'currentpicture', get_string('currentpicture'));
		if( $USER->archetype != $CFG->userTypeStudent && $USER->archetype != $CFG->userTypeAdmin   && $USER->archetype != $CFG->userTypeManager) {
			$mform->addElement('checkbox', 'deletepicture', get_string('delete'));
			$mform->setDefault('deletepicture', 0);
		}
//pr($filemanageroptions);die;
		//$mform->addElement('filemanager', 'imagefile', 'test', null, array('subdirs' => 0, 'maxbytes' => 0, 'maxfiles' => 50, 'accepted_types' => array('*') ));
		//if(strstr($_SERVER['REQUEST_URI'], 'editadvanced.php')){
		//pr($mform);die;
		if($_REQUEST['id']!='-1'){
		$mform->addElement('static', 'currentpicture', get_string('currentpicture'));
		}
		//}
		
		$mform->addElement('checkbox', 'deletepicture', get_string('delete'));
		$mform->setDefault('deletepicture', 0);
		
        $mform->addElement('filemanager', 'imagefile', get_string('newpicture'), null, $filemanageroptions);
		if( $USER->archetype != $CFG->userTypeStudent && $USER->archetype != $CFG->userTypeAdmin   && $USER->archetype != $CFG->userTypeManager) {
			$mform->addHelpButton('imagefile', 'newpicture');
		}
		if( $USER->archetype != $CFG->userTypeStudent && $USER->archetype != $CFG->userTypeAdmin   && $USER->archetype != $CFG->userTypeManager) {
			$mform->addElement('text', 'imagealt', get_string('imagealt'), 'maxlength="100" size="30"');
		}else{
		}
        $mform->setType('imagealt', PARAM_TEXT);

    }

    if( $USER->archetype != $CFG->userTypeStudent && $USER->archetype != $CFG->userTypeAdmin   && $USER->archetype != $CFG->userTypeManager) {  // added by rajesh
 
		// Display user name fields that are not currenlty enabled here if there are any.
		$disabledusernamefields = useredit_get_disabled_name_fields($enabledusernamefields);
		if (count($disabledusernamefields) > 0) {
			$mform->addElement('header', 'moodle_additional_names', get_string('additionalnames'));
			foreach ($disabledusernamefields as $allname) {
				$mform->addElement('text', $allname, get_string($allname), 'maxlength="100" size="30"');
				$mform->setType($allname, PARAM_NOTAGS);
			}
		}
	
		if (!empty($CFG->usetags) and empty($USER->newadminuser)) {
			$mform->addElement('header', 'moodle_interests', get_string('interests'));
			$mform->addElement('tags', 'interests', get_string('interestslist'), array('display' => 'noofficial'));
			$mform->addHelpButton('interests', 'interestslist');
		}
	
		/// Moodle optional fields
		$mform->addElement('header', 'moodle_optional', get_string('optional', 'form'));
	
		$mform->addElement('text', 'url', get_string('webpage'), 'maxlength="255" size="50"');
		$mform->setType('url', PARAM_URL);
	
		$mform->addElement('text', 'icq', get_string('icqnumber'), 'maxlength="15" size="25"');
		$mform->setType('icq', PARAM_NOTAGS);
	
		$mform->addElement('text', 'skype', get_string('skypeid'), 'maxlength="50" size="25"');
		$mform->setType('skype', PARAM_NOTAGS);
	
		$mform->addElement('text', 'aim', get_string('aimid'), 'maxlength="50" size="25"');
		$mform->setType('aim', PARAM_NOTAGS);
	
		$mform->addElement('text', 'yahoo', get_string('yahooid'), 'maxlength="50" size="25"');
		$mform->setType('yahoo', PARAM_NOTAGS);
	
		$mform->addElement('text', 'msn', get_string('msnid'), 'maxlength="50" size="25"');
		$mform->setType('msn', PARAM_NOTAGS);
	
		$mform->addElement('text', 'idnumber', get_string('idnumber'), 'maxlength="255" size="25"');
		$mform->setType('idnumber', PARAM_NOTAGS);
	
		$mform->addElement('text', 'institution', get_string('institution'), 'maxlength="255" size="25"');
		$mform->setType('institution', PARAM_TEXT);
	
		$mform->addElement('text', 'department', get_string('department'), 'maxlength="255" size="25"');
		$mform->setType('department', PARAM_TEXT);
	
		$mform->addElement('text', 'phone1', get_string('phone'), 'maxlength="20" size="25"');
		$mform->setType('phone1', PARAM_NOTAGS);
	
		$mform->addElement('text', 'phone2', get_string('phone2'), 'maxlength="20" size="25"');
		$mform->setType('phone2', PARAM_NOTAGS);
	
		$mform->addElement('text', 'address', get_string('address'), 'maxlength="255" size="25"');
		$mform->setType('address', PARAM_TEXT);
     }

}

/**
 * Return required user name fields for forms.
 *
 * @return array required user name fields in order according to settings.
 */
function useredit_get_required_name_fields() {
    global $CFG;

    // Get the name display format.
    $nameformat = $CFG->fullnamedisplay;

    // Names that are required fields on user forms.
    $necessarynames = array('firstname', 'lastname');
    $languageformat = get_string('fullnamedisplay');

    // Check that the language string and the $nameformat contain the necessary names.
    foreach ($necessarynames as $necessaryname) {
        $pattern = "/$necessaryname\b/";
        if (!preg_match($pattern, $languageformat)) {
            // If the language string has been altered then fall back on the below order.
            $languageformat = 'firstname lastname';
        }
        if (!preg_match($pattern, $nameformat)) {
            // If the nameformat doesn't contain the necessary name fields then use the languageformat.
            $nameformat = $languageformat;
        }
    }

    // Order all of the name fields in the postion they are written in the fullnamedisplay setting.
    $necessarynames = order_in_string($necessarynames, $nameformat);
    return $necessarynames;
}

/**
 * Gets enabled (from fullnameformate setting) user name fields in appropriate order.
 *
 * @return array Enabled user name fields.
 */
function useredit_get_enabled_name_fields() {
    global $CFG;

    // Get all of the other name fields which are not ranked as necessary.
    $additionalusernamefields = array_diff(get_all_user_name_fields(), array('firstname', 'lastname'));
    // Find out which additional name fields are actually being used from the fullnamedisplay setting.
    $enabledadditionalusernames = array();
    foreach ($additionalusernamefields as $enabledname) {
        if (strpos($CFG->fullnamedisplay, $enabledname) !== false) {
            $enabledadditionalusernames[] = $enabledname;
        }
    }

    // Order all of the name fields in the postion they are written in the fullnamedisplay setting.
    $enabledadditionalusernames = order_in_string($enabledadditionalusernames, $CFG->fullnamedisplay);
    return $enabledadditionalusernames;
}

/**
 * Gets user name fields not enabled from the setting fullnamedisplay.
 *
 * @param array $enabledadditionalusernames Current enabled additional user name fields.
 * @return array Disabled user name fields.
 */
function useredit_get_disabled_name_fields($enabledadditionalusernames = null) {
    // If we don't have enabled additional user name information then go and fetch it (try to avoid).
    if (!isset($enabledadditionalusernames)) {
        $enabledadditionalusernames = useredit_get_enabled_name_fields();
    }

    // These are the additional fields that are not currently enabled.
    $nonusednamefields = array_diff(get_all_user_name_fields(),
            array_merge(array('firstname', 'lastname'), $enabledadditionalusernames));
    return $nonusednamefields;
}


