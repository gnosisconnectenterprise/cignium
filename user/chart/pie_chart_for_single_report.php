<link class="include" rel="stylesheet" type="text/css" href="../jqplot/jquery.jqplot.min.css" />
<link rel="stylesheet" type="text/css" href="../jqplot/examples/examples.min.css" />
<link type="text/css" rel="stylesheet" href="../jqplot/examples/syntaxhighlighter/styles/shCoreDefault.min.css" />
<link type="text/css" rel="stylesheet" href="../jqplot/examples/syntaxhighlighter/styles/shThemejqPlot.min.css" />
<!--[if lt IE 9]><script language="javascript" type="text/javascript" src="../excanvas.js"></script><![endif]-->
<!-- Example scripts go here -->
<style type="text/css">
.jqplot-data-label {
/*color: #444;*/
/*      font-size: 1.1em;*/
    }
</style>
<div id="chart2" style="height:300px; width:500px;"></div>

<script class="code" type="text/javascript">
$(document).ready(function(){
  var data = [
    ['Heavy Industry', 12],['Retail', 9], ['Light Industry', 14]];
  var plot2 = jQuery.jqplot ('chart2', [data], 
    {
      seriesDefaults: {
        renderer: jQuery.jqplot.PieRenderer, 
        rendererOptions: {
          // Turn off filling of slices.
          fill: false,
          showDataLabels: true, 
          // Add a margin to seperate the slices.
          sliceMargin: 4, 
          // stroke the slices with a little thicker line.
          lineWidth: 5
        }
      }, 
      legend: { show:true, location: 'e' }
    }
  );
});
</script>
<!-- End example scripts -->
<!-- Don't touch this! -->
<script class="include" type="text/javascript" src="../jqplot/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="../jqplot/examples/syntaxhighlighter/scripts/shCore.min.js"></script>
<script type="text/javascript" src="../jqplot/examples/syntaxhighlighter/scripts/shBrushJScript.min.js"></script>
<script type="text/javascript" src="../jqplot/examples/syntaxhighlighter/scripts/shBrushXml.min.js"></script>
<!-- End Don't touch this! -->
<!-- Additional plugins go here -->
<script class="include" language="javascript" type="text/javascript" src="../jqplot/plugins/jqplot.pieRenderer.min.js"></script>
<script class="include" language="javascript" type="text/javascript" src="../jqplot/plugins/jqplot.donutRenderer.min.js"></script>
<!-- End additional plugins -->
