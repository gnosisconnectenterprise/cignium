<?php

	/**
		* Custom module - User Performance Report Print Page
		* Date Creation - 10/07/2014
		* Date Modification : 10/07/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/

	require_once("../config.php");
	
	//$userrole =  getUserRole($USER->id);
	if( $USER->archetype == $CFG->userTypeStudent ) {
		$groupCourses = checkOwnerAccess($USER->id);
		if(!$groupCourses){
			redirect($CFG->wwwroot);
		}
	}

	$PAGE->set_pagelayout('print');
	//require_login(); 
	checkLogin();
	
	
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('usereperformancereport','multiuserreport'));
	$PAGE->navbar->add(get_string('reports','multiuserreport'), new moodle_url($CFG->wwwroot.'/my/adminreportdashboard.php'));
	
	$sort    = optional_param('sort', 'COALESCE(NULLIF(mu.firstname, ""), mu.lastname), mu.lastname', PARAM_ALPHANUM);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 0, PARAM_INT);        // how many per page
	
	$sDepartment    = optional_param('department', '-1', PARAM_RAW);
	$sTeam          = optional_param('team', '-1', PARAM_RAW);       
	$sUser          = optional_param('user', '-1', PARAM_RAW);       
	$export         = optional_param('action', '', PARAM_ALPHANUM);
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);
	$sType      = optional_param('type', '-1', PARAM_RAW);
	
	$selMode		= optional_param('sel_mode', '1' , PARAM_RAW);
	$userGroup      = optional_param('user_group', '-1', PARAM_RAW);

	$paramArray = array(
					'ch' => optional_param('ch', '', PARAM_ALPHA),
					'key' => optional_param('key', '', PARAM_RAW),
					'department' => $sDepartment,
					'team' => $sTeam,
					'user' => $sUser,
					'startDate' => $sStartDate,
					'endDate' => $sEndDate,
					'type' => $sType,
					'sel_mode' => $selMode,
					'user_group' => $userGroup,
					'job_title' => optional_param('job_title', '-1', PARAM_RAW),
					'company' => optional_param('company', '-1', PARAM_RAW)
				  );
	$filterArray = array(							
						'nm'=>get_string('name','course'),
				   );
	
	////// Define those key, which we need to remove first time //////
	$removeKeyArray = array('perpage');
	
	
	$userHTML = '';
	$reportContentCSV = '';
	$reportContentPDF = '';
    if($export && in_array($export, array('exportcsv', 'exportpdf', 'print'))){
	
		$userPerformanceReport = UserPerformanceReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
		$userHTML = $userPerformanceReport->userHTML;
		$reportContentCSV = $userPerformanceReport->reportContentCSV;
		$reportContentPDF = $userPerformanceReport->reportContentPDF;		
		
	}else{
	    redirect(new moodle_url('/'));
	}
			

	/* bof export to csv */
	
	if(isset($export) && $export == 'exportcsv') {
	
		echo $filepath = $CFG->dirroot."/local/reportexport/temp";		
		chmod($filepath, 0777);		
		$filename = str_replace(' ', '_', get_string('usereperformancereport','multiuserreport'))."_".date("m-d-Y").".csv";  
		$filepathname = $filepath.'/'.$filename;
		unlink($filepathname);
		$handler = fopen($filepathname, "w");
		fwrite($handler, $reportContentCSV);
		exportCSV($filepathname);
	}
	/* eof export to csv */	
	
	
	/* bof export to pdf */
	if(isset($export) && $export == 'exportpdf') {
	
		//echo $reportContentPDF;die;
		$filename = str_replace(' ', '_', get_string('usereperformancereport','multiuserreport'))."_".date("m-d-Y").".pdf";
		exportPDF($filename, $reportContentPDF, '', get_string('usereperformancereport','multiuserreport'));
		
	}
	/* eof export to pdf */	
	
	echo $OUTPUT->header(); 
	echo $userHTML;
	//echo $OUTPUT->footer();

?>
<style>
#page { margin: 20px auto 0;}
</style>
