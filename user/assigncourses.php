<?php

	require_once(dirname(__FILE__) . '/../config.php');
	require_once(dirname(__FILE__) . '/lib.php');
	require_once($CFG->dirroot . '/local/user/selector/lib.php');
	require_once($CFG->dirroot . '/course/lib.php');
	require_once($CFG->libdir . '/filelib.php');
	require_login();
	$site = get_site();
	$userid = optional_param('id',0, PARAM_INT);
	if(!$userid){
		redirect($CFG->wwwroot.'/admin/user.php');
	}
	checkUserAccess('userAssignCourse' , $userid);
	if (optional_param('add', false, PARAM_BOOL) && confirm_sesskey()) {
		assignCourseToUser($userid,$_REQUEST['addcourse']);
		redirect($CFG->wwwroot."/user/assigncourses.php?id=".$userid);
	}
	if (optional_param('remove', false, PARAM_BOOL) && confirm_sesskey()) {
		removeCourseFromUser($userid,$_REQUEST['removecourse']);
		redirect($CFG->wwwroot."/user/assigncourses.php?id=".$userid);
	}

	$context = context_system::instance();
	$returnurl = $CFG->wwwroot.'/admin/user.php';
	
	//Start page setting data
	$PAGE->set_url('/user/assignusers.php', array('id'=>$userid));
	$PAGE->set_pagelayout('globaladmin');
	$PAGE->navbar->add(get_string('manage_user'), new moodle_url($CFG->wwwroot.'/admin/user.php'));
	$PAGE->navbar->add(get_string('assigncourses'));
	$PAGE->set_title("$site->fullname: $stradduserstogroup");
	$PAGE->set_heading($site->fullname);
	echo $OUTPUT->header();/// Print header
	//Start page setting data
	$userDepartment = $DB->get_record_sql("SELECT d.departmentid FROM mdl_department_members as d WHERE d.is_active = 1 AND d.userid = ".$userid);
	$departmentId = 0;
	if(!empty($userDepartment)){
		$departmentId = $userDepartment->departmentid;
	}
	// Set assigned and unassigned course array
	$courses = new user_courses_selector('', array('userid' => $userid,'departmentId' => $departmentId));
	$courses->get_non_user_courses();
	$courses->get_user_courses();

	/// Print the editing form
	$user->id = $userid;
	echo $outerDivStart = "<div class='tabsOuter'>";
		$outerDivEnd = "</div>";
		include_once('user_tabs.php');
		echo '<div class="">';
	?>

	<div class="borderBlockSpace" id="addmembersform">
		<form id="assignform" method="post" action="<?php echo $CFG->wwwroot; ?>/user/assigncourses.php?id=<?php echo $userid; ?>">
		<div>
		<input type="hidden" name="sesskey" value="<?php p(sesskey()); ?>" />

		<table class="assignTable">
        <tr>
          <td id='potentialcell' class="potentialcell">
			  <p>
				<label for="addselect"><?php echo get_string('nonusercourses_user');?></label>
			  </p>
			  <div id="addselect_wrapper" class="userselector">
				<select multiple="multiple" name = "addcourse[]" size = '20'>
					<?php $courses->course_option_list(1); ?>
				</select>
                <div class="search-noncourse-box searchBoxDiv" >
                    <div class="search-input" ><input class="searchBoxInput" type="text" value="" placeholder="Search" id="addselect_searchtext" name="addselect_searchtext" ></div>
                    <div class="search_clear_button"><input type="button" title="Search" id="addselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="addselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
          
                </div>
            
			  </div>
		  </td>
		  <td id='buttonscell'>
			<div class="arrow_button">
				<input class="moveLeftButton" type="button" name="remove" id="remove" value="<?php echo get_string('remove').'&nbsp;'.$OUTPUT->rarrow(); ?>" title="<?php print_string('remove'); ?>" disabled /><input class="moveRightButton" type="button" name="add" id="add" value="<?php echo $OUTPUT->larrow().'&nbsp;'.get_string('add'); ?>" title="<?php print_string('add'); ?>" disabled />
			</div>
		  </td>
		   <td id='existingcell' class="potentialcell">
			  <p>
				<label for="removeselect"><?php echo get_string('usercourses_user');?></label><a href="#" name="updatecourse" id="updateCourse" class="updateDate"  title = "<?php echo get_string('update_date'); ?>">update</a>
			  </p>
			  <div id="removeselect_wrapper" class="userselector">
				<select multiple="multiple" name = "removecourse[]" size = '20'>
					<?php $courses->course_option_list(2); ?>
				</select>
                <div class="search-course-box searchBoxDiv" >
                    <div class="search-input" ><input class="searchBoxInput" type="text" value="" placeholder="Search" id="removeselect_searchtext" name="removeselect_searchtext" ></div>
                    <div class="search_clear_button"><input type="button" title="Search" id="removeselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="removeselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
          
                </div>
                
			  </div>
			</td>
		  <!--<td>
			<p><?php echo($strusergroupmembership) ?></p>
			<div id="group-usersummary"></div>
		  </td>-->
		</tr>
		</table>
		</form>
		
		<?php $user_back_url = getBackUrlForUserSection();?>
		<form action="<?php echo $user_back_url;?>" method="post" class="userform">
		<table class="assignTable">
		<tr>
			<td id="backcell" colspan="3">
			<?php 
			 $urlCancel = $user_back_url;
				  echo '<input type = "button" value = "'.get_string('back').'" '.$styleSheet.' style="margin-top:10px" onclick="location.href=\''.$urlCancel.'\';">';
				  ?>
			
			</td>
		</tr>
		</table>
		</form>
		</div>
	</div>
<a href="#end-date-div" id="inline-iframe" rel="inline-iframe" title="<?php print_string('allocation'); ?>">click</a>
<div class="end-date-div" id="end-date-div">
	<iframe src="" id="i-frame" width="100%" height="100%"></iframe>
</div>
	<?php
	echo $outerDivEnd;
	?>
	<script>
	$(document).ready(function(){
		$("#inline-iframe").colorbox({inline:true, width:"675px", height:"468px"});
		var userId = "<?php echo $userid;?>";
		$("#addselect_searchtext_btn").click(function(){
			var searchText = $('#addselect_searchtext').val();

			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/user/searchcourses.php',
					type:'POST',
					data:'action=searchNonCourse&userid=<?php echo $userid; ?>&search_text='+searchText,
					success:function(data){
						$('#addselect_wrapper').find('select').html(data);
					}
			});
		});
		$("#addselect_clearbutton").click(function(){
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/user/searchcourses.php',
					type:'POST',
					data:'action=searchNonCourse&userid=<?php echo $userid; ?>',
					success:function(data){
						$('#addselect_wrapper').find('select').html(data);
						$("#addselect_searchtext").val('');
					}
			});
		});
		$("#removeselect_clearbutton").click(function(){
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/user/searchcourses.php',
					type:'POST',
					data:'action=searchUserCourse&userid=<?php echo $userid; ?>',
					success:function(data){
						$('#removeselect_wrapper').find('select').html(data);
						$("#removeselect_searchtext").val('');
					}
			});
		});
		$("#removeselect_searchtext_btn").click(function(){
			var searchText = $('#removeselect_searchtext').val();
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/user/searchcourses.php',
					type:'POST',
					data:'action=searchUserCourse&userid=<?php echo $userid; ?>&search_text='+searchText,
					success:function(data){
						$('#removeselect_wrapper').find('select').html(data);
					}
			});
		});
		$(document).on("change", "[name = 'addcourse[]']",function(){ 
			$("#add").removeAttr("disabled");
		});
		$(document).on("change", "[name = 'removecourse[]']",function(){ 
			$("#remove").removeAttr("disabled");
		});
		$("#backtolist").click(function(){
			window.location.href = '<?php $urlCancel = new moodle_url($CFG->wwwroot.'/user/viewuser.php', array('id'=>$userid,'course'=>1)); echo $urlCancel;?>';
		});
		var fromCourse = $('#addselect_wrapper select');
		var toCourse = $('#removeselect_wrapper select');
		$(document).on("click","#add",function(){
			var courseList = '';
			fromCourse.find('option:selected').each(function(){
				courseList += $(this).val()+',';
			});	
			if(courseList == '' || courseList == ','){
				alert("<?php echo get_string('select_course_to_enrol');?>");
				return false;
			}

			courseList = courseList.substr(0, courseList.length - 1);

			var source = '<?php echo $CFG->wwwroot;?>/course/enrolcourse.php?element=course&assigntype=user&assignId='+userId+'&elementList='+courseList;
			$("#i-frame").attr('src',source);
			$(".cboxContent #end-date-div").attr("style",'display:block');
			$("#inline-iframe").trigger("click");
		});
		$(document).on("click","#updateCourse",function(){
			var courseList = '';
			toCourse.find('option:selected').each(function(){
				courseList += $(this).val()+',';
			});	
			if(courseList == '' || courseList == ','){
				alert("<?php echo get_string('select_course_to_update');?>");
				return false;
			}

			courseList = courseList.substr(0, courseList.length - 1);
			var source = '<?php echo $CFG->wwwroot;?>/course/enrolcourse.php?element=course&action=update&assigntype=user&assignId='+userId+'&elementList='+courseList;
			$("#i-frame").attr('src',source);
			$(".cboxContent #end-date-div").attr("style",'display:block');
			$("#inline-iframe").trigger("click");
		});
		$(document).on("click","#remove",function(){
			var courseList = '';
			toCourse.find('option:selected').each(function(){
				courseList += $(this).val()+',';
			});
			if(courseList == '' || courseList == ','){
				alert("<?php echo get_string('select_course_to_unenrol');?>");
				return false;
			}
			courseList = courseList.substr(0, courseList.length - 1);
			var source = '<?php echo $CFG->wwwroot;?>/course/unenrolcourse.php?element=course&assigntype=user&assignId='+userId+'&elementList='+courseList;
			$("#i-frame").attr('src',source);
			$(".cboxContent #end-date-div").attr("style",'display:block');
			$("#inline-iframe").trigger("click");

		});
});
	</script>
<style>
#end-date-div{ display:none;}
#colorbox #end-date-div{display:block}
#inline-iframe{ position:absolute; left:-100000px;}
</style>
	<?php
	echo $OUTPUT->footer();
