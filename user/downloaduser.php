<?php
/**
* User Module - User Bulk upload functionality
* @author Gnosis Team
* Date Creation - 22/10/2014
* Date Modification : 27/10/2014
* Last Modified By : Bhavana Kadwal
*/

global $CFG;
$GLOBALS['statusLog'] = array();
$GLOBALS['errorLog'] = array();
$GLOBALS['viewLog'] = array();

$GLOBALS['viewLog']['success'] = array();
$GLOBALS['viewLog']['success']['department'] = array();
$GLOBALS['viewLog']['success']['teams'] = array();
$GLOBALS['viewLog']['success']['users'] = array();
$GLOBALS['viewLog']['success']['users']['manager'] = 0;
$GLOBALS['viewLog']['success']['users']['learner'] = 0;
$GLOBALS['viewLog']['error'] = array();
$GLOBALS['viewLog']['error']['users']['manager'] = 0;
$GLOBALS['viewLog']['error']['users']['learner'] = 0;
// include user profile/preferences files
require_once($CFG->dirroot.'/user/editlib.php');
require_once($CFG->dirroot.'/user/profile/lib.php');

/**
 * This function is created to download a csv file.
 * @param string $file => Download file name
 * @return nothing.
*/
function download($file) {
	$filename = basename($file);
	header('Content-Type: application/csv');
	header('Content-Disposition: attachment; filename="' . $filename . '"');
	header('Content-Length: ' . filesize($file));
	readfile($file);
	exit();
}

/**
 * This function is created to upload users in system.
 * @param string $target_dir => uploaded csv file path(complete)
 * @return array $resultArray => status array for this upload.
*/

function upload_user($target_dir) {
	GLOBAL $DB,$CFG,$USER,$statusLog,$errorLog,$viewLog;
	$i = 0;
	$errorFile = 0;
	$statusLog['success']['manager'] = 0;
	$statusLog['success']['learner'] = 0;      
        
	$statusLog['fail'] = 0;
        
        
        $statusLog['updated']['manager'] = 0;
	$statusLog['updated']['learner'] = 0;
        
        
	/* **** User Upload Start **** */
	// Open uploaded user file
	$csvDataArray = array();
	$rowCount = 0;
	if (($handle = fopen($target_dir, "r")) !== FALSE) {
		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
		{
			$inx = count($data);
			$rowCount++;
			if($rowCount == 1){
				$dataArray = array('Username*','First Name*','Last Name*','Email*','Phone','City','Country','Date of Birth','Description','Role*','Can be Instructor','Department*','Department Manager*','Team','User Identifier','Is Manager');
				if($data === $dataArray){ // Correct file
					$errorLog[$rowCount] = $data;
					$errorLog[$rowCount][$inx] = 'Status'; // Add another column to show error.
				}else{ // File mismatch
					$errorLog[$rowCount] = $data;
					$j = $rowCount+1;
					$errorLog[$j][] = get_string('invalid_csv_file');
					$errorFile = 1;
					break;
				}
			}else{
                           
				$flatRow = trim(implode('', $data));
				if($flatRow == ''){
					$errorLog[$rowCount] = array();
				}else{
					$data['curr_index'] = $rowCount;
					if(strtolower($data[9]) == strtolower($CFG->userTypeManager)){
						$csvDataArray['manager'][] = $data;
					}else if(strtolower($data[9]) == strtolower($CFG->userTypeAdmin)){
						$csvDataArray['admin'][] = $data;
					}else{
						$csvDataArray['learner'][] = $data;
					}
					$errorLog[$rowCount] = $data;
				}
			}
		}
	}
	if ($errorFile == 0) {
            
           
		if(!empty($csvDataArray['admin'])){
			foreach($csvDataArray['admin'] as $csvDataAdminRow){
				uploadUser($csvDataAdminRow);
			}
		}
		if(!empty($csvDataArray['manager'])){
			foreach($csvDataArray['manager'] as $csvDataManagerRow){
				uploadUser($csvDataManagerRow);
			}
		}
               
		if(!empty($csvDataArray['learner'])){                   
			foreach($csvDataArray['learner'] as $csvDataLearnerRow){ 
                          
				uploadUser($csvDataLearnerRow);
			}
		}
	}
	// Close uploaded user file
	fclose($handle);
	/* **** User Upload ends here **** */

	$resultArray = array();
	/*if(count($viewLog['error']['log']) < 1){ // Data inserted successfully
		$resultArray['status'] = 1;
		$resultArray['message'] = get_string('upload_success');
		return $resultArray;
	}else{*/
		if($errorFile == 1){ // Invalid file format
			$resultArray['status'] = 2;
			$resultArray['message'] = get_string('invalid_csv_file');
			return $resultArray;
		}else{ // error in file data
			if(count($viewLog['error']['log']) < 1){ // Data inserted successfully
				$resultArray['status'] = 1;
			}else{
				$resultArray['status'] = 3;
			}
			$filename = time().'_error_log.csv';
			$file = $CFG->dirroot."/user/download/user_log//".$filename;
			$fp = fopen($file, 'w');
			foreach ($errorLog as $fields) {
				fputcsv($fp, $fields);
			}
			fclose($fp);
			$resultArray['message'] = $filename;
			$resultArray['upload_log'] = $statusLog;
			$resultArray['viewLog'] = $viewLog;
                        
                        //pr($resultArray);die;
			return $resultArray;
		}
	//}
}
function create_user($usernew){
	Global $USER,$CFG,$DB;
	$usernew->id = user_create_user($usernew, false); // Call to user/lib.php user creation function
	//send_confirmation_email_to_user($usernew); // Send email to user.
	if($usernew->id){
		$usernew = $DB->get_record('user', array('id'=>$usernew->id));
		$usercontext = context_user::instance($usernew->id);
		//update preferences
		useredit_update_user_preference($usernew);
		// save custom profile fields data
		profile_save_data($usernew);
		// reload from db
		unset_user_preference('create_password', $usernew);
                
                //Auto enrollment into courses
               // auto_enrollment_into_courses($usernew);
                $output = auto_enrollment_into_courses($usernew,true);
		return $usernew->id;
	}
	return 0;
}

function update_user($user){
	Global $USER,$CFG,$DB;
        
        $updateUser = new stdClass();
        $updateUser->id = $user->id;
        $updateUser->suspended = 0;
	
        $updateUser->firstname = $user->firstname;
        $updateUser->lastname = $user->lastname;
        $updateUser->email = $user->email;
        $updateUser->phone1 = $user->phone1;
        $updateUser->role = $user->role;
        $updateUser->is_instructor = $user->is_instructor;
        $updateUser->department = $user->department;
        $updateUser->department_manager = $user->department_manager;
        $updateUser->city = $user->city;
        $updateUser->country = $user->country;
        $updateUser->dateofbirth = $user->dateofbirth;
        $updateUser->description = $user->description;
        $updateUser->timemodified = $user->timemodified;
        $updateUser->parent_id = $user->parent_id;
        $updateUser->team = $user->team;
        $updateUser->user_identifier = $user->user_identifier;
        $updateUser->is_manager_yes = $user->is_manager_yes;
        $updateUser->usertype = $user->usertype;
      // pr($updateUser);
        user_update_user($updateUser, false);
	$output = auto_enrollment_into_courses($updateUser,false);
	return 1;
}

function uploadUser($data){
	GLOBAL $USER,$DB,$CFG,$statusLog,$errorLog,$viewLog;
       
        try{
            
            $err		= 0;
	$dmanagerId	= 0;
	$errStr = array();
	$i = 0;
	$inx = count($data);
	/* Get row data from CSV starts here*/
	$username		= $data[0];
	$password		= $CFG->systemAutoPassword;
	$fname			= $data[1];
	$lname			= $data[2];
	$email			= $data[3];
	$phone			= $data[4];
	$city			= $data[5];
	$country		= $data[6];
	$dob			= $data[7];
	$description	= $data[8];
	$userRole		= trim($data[9]);
	$isInstructor	= $data[10];
	$dManager		= $data[12];
	$team			= $data[13];
        $userIdentifier			= $data[14];
        $is_manager_AD_Flag	= $data[15];
       // pr($data);
        $userExists =0;
	/* Get row data from CSV ends here*/

	/************Required field check starts here***********************/
	if(trim($fname) == ''){
		$err = 1;
		$errStr[] = get_string('first_name_check');;
	}
	if(trim($lname) == ''){
		$err = 1;
		$errStr[] = get_string('last_name_check');
	}
	if(trim($username) == ''){
		$err = 1;
		$errStr[] = get_string('user_name_check');
	}else{
		if (strtolower($username) !== clean_param($username, PARAM_USERNAME)) { // validate Username format
			$err = 1;
			$errStr[] = get_string('invalidusername');
		}else{
			$userExists = isUserAlreadyExist($username); // check if user already exists
			if($userExists){
                          
				//$err = 1;
				//$errStr[] = get_string('user_name_exists_check');
                           
			}
		}
	}
	if(trim($userRole) == ''){
		$err = 1;
		$errStr[] = get_string('role_name_check');
	}else{
		$RoleArray = $DB->get_record_sql("SELECT r.id FROM mdl_role r WHERE LOWER(r.name) = '".strtolower($userRole)."'");
		if(!empty($RoleArray)){
			$roleId = $RoleArray->id;
		}else{
			$err = 1;
			$errStr[] = get_string('role_name_check');
		}
	}
	if(trim($email) == ''){
		$err = 1;
		$errStr[] = get_string('email_name_check');
	}else{
                if($userExists){
                    $emailcheck = "select email from mdl_user where email='".$email."' AND mnethostid={$CFG->mnet_localhost_id} AND id<>".$userExists;
                }else{
                    $emailcheck = "select email from mdl_user where email='".$email."' AND mnethostid={$CFG->mnet_localhost_id}";
                }
               
		
                if (!validate_email($email)) { // validate email format
			$err = 1;
			$errStr[] = get_string('invalidemail');
		} else if ($DB->get_record_sql($emailcheck)) { // check if email already registered
			$err = 1;
			$errStr[] = get_string('emailexists');
		}
	}
        
	if(trim($dob) != ''){
		$dob = strtotime($dob);
		if($dob > time()){ // Date of birth cannot be greater than today's date.
			$err = 1;
			$errStr[] = get_string('dateofbirth_error');
		}
	}
	$countryKey = '';
	// Check country in country list
	if(trim($country) != ''){
		$choices = get_string_manager()->get_list_of_countries();
		if(!in_array($country,$choices)){
			$err = 1;
			$errStr[] = get_string('country_name_check');
		}else{
			$countryKey = array_search(strtolower($country), array_map('strtolower', $choices));
		}
	}
	/****** Get user department, manager, team on the basis of upload type starts ********/
	$department		= $data[11];
	$departmentExists =isDepartmentAlreadyExist($department); // Check if department exists or not
	if($USER->archetype == $CFG->userTypeManager){
		$userDepartment = $DB->get_record('department_members',array('userid'=>$USER->id));
		if($userDepartment->departmentid == $departmentExists && $USER->is_primary == 1){
			$departmentId = $departmentExists;
		}else{
			$departmentId = 0;
		}
	}else{
		if($departmentExists){
				$departmentId = $departmentExists;
		}else{
			if(strtolower($userRole) == strtolower($CFG->userTypeManager)){
				include_once($CFG->dirroot.'/department/departmentlib.php');
				$departmentData = new stdClass();
				$departmentData->title = $department;
				$departmentData->description['text'] = '';
				$departmentId = insert_department($departmentData);
				if($departmentId){
					$CsvRow = $data['curr_index'];
					$viewLog['success']['department'][] = "#".$CsvRow." '".$department."' Department has been created.";
				}else{
					$departmentId = 0;
				}
			}else{
				$departmentId = 0;
			}
		}
	}
	/****** Get user department, manager, team on the basis of upload type ends ********/
	if($departmentId == 0){
		$err = 1;
		$errStr[] = get_string('department_name_exists_check');
	}
	if(strtolower($userRole) == strtolower($CFG->userTypeStudent)){
		if(trim($dManager) == ''){
			$err = 1;
			$errStr[] = get_string('department_manager_name_check');
		}else{
			// Check department manager exists or not
			$dmanagerExists = $DB->get_record_sql("SELECT u.id FROM mdl_user u 
												LEFT JOIN mdl_department_members as dm on dm.userid = u.id
												LEFT JOIN mdl_role_assignments as ra on ra.userid = u.id AND ra.contextid = 1
												LEFT JOIN mdl_role as r ON r.id = ra.roleid AND r.name = 'manager'
												WHERE u.username = '".addslashes($dManager)."' AND dm.departmentid = ".$departmentId);
			if(empty($dmanagerExists)){
				$err = 1;
				$errStr[] = get_string('department_manager_exists_check');
			}else{
				$dmanagerId = $dmanagerExists->id;
			}
		}
	}
        
        if(trim($userIdentifier)){
            $uniqueIdRecordExist = $DB->get_record('user',array('user_identifier'=>$userIdentifier,'deleted'=>0));
            if($uniqueIdRecordExist && $uniqueIdRecordExist->id != $userExists){
                 $err = 1;
                 $errStr[] = get_string('uniqueid_already_exist');
            }
        }
        
	/*******************Required field check ends here***********************/
	if($err == 0){ // If no error in file
		/* complete user data array starts here*/
		$user = new stdClass;
                
                if($userExists){
                    $user->id = $userExists;
                }
		$user->course = 1;
		$user->username = $username;
		$user->auth = 'manual';
		$user->suspended = 0;
		$user->newpassword = $password;
		$user->firstname = $fname;
		$user->lastname = $lname;
		$user->email = $email;
		$user->phone1 = $phone;
		$user->role = $roleId;
		$user->is_instructor = $isInstructor;
		$user->department = $departmentId;
		$user->department_manager = $dmanagerId;
		$user->maildisplay = 1;
		$user->mailformat = 1;
		$user->maildigest = 0;
		$user->autosubscribe = 1;
		$user->trackforums = 0;
		$user->preference_htmleditor = '';
		$user->city = $city;
		$user->country = $countryKey;
		$user->timezone = 99;
		$user->lang = 'en';
		$user->description_editor = '';
		$user->imagefile = '';
		$user->dateofbirth = $dob;
		$user->description = $description;
		$user->submitbutton = 'Save';
		$user->timemodified = time();
		$user->descriptiontrust = 0;
		$user->descriptionformat = '';
		$user->mnethostid = 1;
		$user->confirmed = 1;
		$user->timecreated = time();
		$user->password = hash_internal_user_password($password);
		$user->createdby = $USER->id;
		$user->is_primary = 0;
		$user->calendartype = 'gregorian';
		$user->parent_id = $dmanagerId;
                $user->user_identifier = $userIdentifier;
                $user->usertype = 'internal';
                
                if(trim($is_manager_AD_Flag)=='Yes'){
                   /* if($country=='United States'){
                        //Us manager team
                    }else{
                        //Non manager us team
                    }*/
                    $user->is_manager_yes = 1;
                }else{
                    $user->is_manager_yes = 0;
                }
              // pr($user);die; 
		$teamIdArray = array();
		if($team == ''){
			$team = 0;
		}else if(!is_array($team)){
			$teams = explode(',',$team);
			if(!empty($teams)){
				foreach($teams as $teamname){
					$teamname = trim($teamname);
					if($teamname != ''){
							$teamId = $DB->get_record_sql("SELECT g.id FROM mdl_groups g
															LEFT JOIN mdl_group_department as d ON d.team_id = g.id
															WHERE g.name = '".addslashes($teamname)."' AND d.department_id = ".$departmentId);
							if(!empty($teamId)){
								$teamIdArray[] = $teamId->id;
							}else{
								$teamData = new stdClass();
								$teamData->name = $teamname;
								$teamData->department = $departmentId;
								$teamData->courseid = 0;
								$teamData->description_editor['text'] = '';
								$groupId = customGroupsCreateGroup($teamData);
								$teamIdArray[] = $groupId;
								$CsvRow = $data['curr_index'];
								$viewLog['success']['teams'][] = "#".$CsvRow." '".$teamname."' Team has been created in '".$department."' department.";
							}
					}
				}
				$team = $teamIdArray;
			}
		}
		$user->team = $team;
                
                               
                $cntInx = $inx-1;
		$CsvRow = $data['curr_index'];
		unset($errorLog[$CsvRow]['curr_index']);
               // pr($user);die;
                if($userExists){                    
                            $updateUser = update_user($user);
                    
                            $errorLog[$CsvRow][$cntInx] = 'Success';
                            $viewLog['success']['users']['log'][$CsvRow] = "#".$CsvRow.' User Updated';
                            if(strtolower($userRole) == strtolower($CFG->userTypeManager)){
                                    $statusLog['updated']['manager'] = $statusLog['updated']['manager'] + 1;
                                    $viewLog['updated']['users']['manager'] = $viewLog['updated']['users']['manager']+ 1;
                            }else{
                                    $statusLog['updated']['learner'] = $statusLog['updated']['learner'] + 1;
                                    $viewLog['updated']['users']['learner'] = $viewLog['updated']['users']['learner'] + 1;
                            }
                          
                }else{
                    $userCreated = create_user($user); // Insert user data in database
                    
                    if($userCreated == 0 || $userCreated = ''){
			// insert error log in array
			$err = 1;
			$errorLog[$CsvRow][$cntInx] = 'Error: '.get_string('error_uploading_user');
			$viewLog['error'][] = "#".$CsvRow.'Error: '.get_string('error_uploading_user');
			$statusLog['fail'] = $statusLog['fail'] + 1;
                    }else{

                            $errorLog[$CsvRow][$cntInx] = 'Success';
                            $viewLog['success']['users']['log'][$CsvRow] = "#".$CsvRow.' User Created';
                            if(strtolower($userRole) == strtolower($CFG->userTypeManager)){
                                    $statusLog['success']['manager'] = $statusLog['success']['manager'] + 1;
                                    $viewLog['success']['users']['manager'] = $viewLog['success']['users']['manager']+ 1;
                            }else{
                                    $statusLog['success']['learner'] = $statusLog['success']['learner'] + 1;
                                    $viewLog['success']['users']['learner'] = $viewLog['success']['users']['learner'] + 1;
                            }
                    }
                }
            
		/* complete user data array ends here*/
		
		
                
                
		
	}else{
		$cntInx = $inx-1;
		$CsvRow = $data['curr_index'];
		unset($errorLog[$CsvRow]['curr_index']);
		// insert error log in array
		$err = 1;
		$errorLog[$CsvRow][$cntInx] = 'Error: '.implode('; ',$errStr).';';
		$viewLog['error']['log'][$CsvRow] = "#".$CsvRow.' Error: '.implode('; ',$errStr).';';
		if(strtolower($userRole) == strtolower($CFG->userTypeManager)){
			$viewLog['error']['users']['manager'] = $viewLog['error']['users']['manager']+ 1;
		}else{
			$viewLog['error']['users']['learner'] = $viewLog['error']['users']['learner'] + 1;
		}
		$statusLog['fail'] = $statusLog['fail'] + 1;
	}
            
        } catch (Exception $ex) {
            pr($ex);

        }
	
}

?>