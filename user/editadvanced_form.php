<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->dirroot.'/lib/formslib.php');

class user_editadvanced_form extends moodleform {

    // Define the form
    function definition() {
        global $USER, $CFG, $COURSE;

        $mform =& $this->_form;
        $editoroptions = null;
        $filemanageroptions = null;
        $userid = $USER->id;
        $disable_fields = "";
        if($CFG->isLdap==1){
   			$disable_fields = 'class="disabled_user_form_fields" disabled="disabled"';
        }
        if (is_array($this->_customdata)) {
            if (array_key_exists('editoroptions', $this->_customdata)) {
                $editoroptions = $this->_customdata['editoroptions'];
            }
            if (array_key_exists('filemanageroptions', $this->_customdata)) {
                $filemanageroptions = $this->_customdata['filemanageroptions'];
            }
            if (array_key_exists('userid', $this->_customdata)) {
                $userid = $this->_customdata['userid'];
            }
        }

        //Accessibility: "Required" is bad legend text.
        $strgeneral  = get_string('general');
        $strrequired = get_string('required');

        /// Add some extra hidden fields
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        $mform->addElement('hidden', 'course', $COURSE->id);
        $mform->setType('course', PARAM_INT);

        /// Print the required moodle fields first
        //$mform->addElement('header', 'moodle', $strgeneral);
		
		
        $mform->addElement('html', '<input type="text" name="prevent_autofill" id="prevent_autofill" value="" style="display:none;" />
<input type="password" name="password_fake" id="password_fake" value="" style="display:none;" />'); // To prevent to autofill username and password field in chrome

		$mform->addElement('html', '<div class= "adduser-div">');
			/*if($userid && $userid != -1){
				$mform->addElement('html', '<div class= "user-image">');
				 $mform->addElement('static', 'currentpicture', get_string('currentpicture'));
				$mform->addElement('html', '</div>');
			}*/
			    $mform->addElement('html', '<div class= "user-details">');
                            
                                $mform->addElement('text', 'user_identifier', get_string('user_identifier'));
				//$mform->addRule('user_identifier', $strrequired, 'required', null, 'client');
				$mform->setType('user_identifier', PARAM_RAW);
			
                                $mform->addElement('text', 'username', get_string('username'), "maxlength='100' size='20' autocomplete='false' ".$disable_fields);
				$mform->addRule('username', $strrequired, 'required', null, 'client');
				$mform->setType('username', PARAM_RAW);
                                
                                                            

				$auths = core_component::get_plugin_list('auth');
				$enabled = get_string('pluginenabled', 'core_plugin');
				$disabled = get_string('plugindisabled', 'core_plugin');
				$auth_options = array($enabled=>array(), $disabled=>array());
				foreach ($auths as $auth => $unused) {
					if (is_enabled_auth($auth)) {
						$auth_options[$enabled][$auth] = get_string('pluginname', "auth_{$auth}");
					} else {
						$auth_options[$disabled][$auth] = get_string('pluginname', "auth_{$auth}");
					}
				}
				if( $USER->archetype != $CFG->userTypeStudent && $USER->archetype != $CFG->userTypeAdmin && $USER->archetype != $CFG->userTypeManager ) {
				//if(in_array($userrole, $CFG->customsiteadminroleid) && $USER->id == 2){
					$mform->addElement('selectgroups', 'auth', get_string('chooseauthmethod','auth'), $auth_options);
					$mform->addHelpButton('auth', 'chooseauthmethod', 'auth');
				}else{
					$mform->addElement('hidden', 'auth', get_string('chooseauthmethod','auth'), $auth_options);
				}
				
				if( $USER->archetype != $CFG->userTypeStudent && $USER->archetype != $CFG->userTypeAdmin  && $USER->archetype != $CFG->userTypeManager) {
					$mform->addElement('advcheckbox', 'suspended', get_string('suspended','auth'));
					$mform->addHelpButton('suspended', 'suspended', 'auth');
				}else{
					$mform->addElement('hidden', 'suspended1', get_string('suspended','auth'));
					$mform->setDefault('suspended1', 0);
				}
				
				if( $USER->archetype != $CFG->userTypeStudent && $USER->archetype != $CFG->userTypeAdmin   && $USER->archetype != $CFG->userTypeManager ) {
					$mform->addElement('checkbox', 'createpassword', get_string('createpassword','auth'));
				}else{
					$mform->addElement('hidden', 'createpassword', get_string('createpassword','auth'));
				}

				
				$mform->addElement('passwordunmask', 'newpassword', get_string('password'), 'size="20 "maxlength="20" minlength="8"'.$disable_fields);
				if($this->_customdata['userid'] == 0 || $this->_customdata['userid'] == '' || $this->_customdata['userid'] == -1){
					$mform->addRule('newpassword', $strrequired, 'required', null, 'client');
				}
				if( $USER->archetype != $CFG->userTypeStudent && $USER->archetype != $CFG->userTypeAdmin  && $USER->archetype != $CFG->userTypeManager) {
					$mform->addHelpButton('newpassword', 'newpassword');
				}
				if (!empty($CFG->passwordpolicy)){
					$mform->addElement('static', 'passwordpolicyinfo', '', print_password_policy().'.');
				}
				$mform->setType('newpassword', PARAM_RAW);
				$mform->disabledIf('newpassword', 'createpassword', 'checked');

				if( $USER->archetype != $CFG->userTypeStudent && $USER->archetype != $CFG->userTypeAdmin ) {
					//$mform->addElement('advcheckbox', 'preference_auth_forcepasswordchange', get_string('forcepasswordchange'));
				}else{
					//$mform->addElement('hidden', 'preference_auth_forcepasswordchange', get_string('forcepasswordchange'));
				}
				$mform->addHelpButton('preference_auth_forcepasswordchange', 'forcepasswordchange');
				$mform->disabledIf('preference_auth_forcepasswordchange', 'createpassword', 'checked');

				/// shared fields
				useredit_shared_definition($mform, $editoroptions, $filemanageroptions,$this->_customdata['userData']);
				//$mform->addElement('html', '<div class="fitem fitem_ftextarea " id="fitem_id_dateofbirth"><div class="fitemtitle"><label for="start_date">'.get_string("dateofbirth").' </label></div><div class="felement ftextarea"><input type="text" id="start_date" value="'.$dateofbirth.'" name="dateofbirth" readonly = "true"></div></div>');
				if($CFG->isdisplayextrauserfields){
					$mform->addElement('text', 'dateofbirth',get_string("dateofbirth"),array('id'=>'start_date','readonly'=>"true"));
					if($dateofbirth){
						//$mform->setDefault('dateofbirth', $dateofbirth);
					}
				}
				$mform->addElement('textarea', 'description', get_string('user_description'));

				if($USER->archetype == $CFG->userTypeAdmin && (isset($this->_customdata['userData']) || !empty($this->_customdata['userData']->id))){
					$userRole = getUserRoleDetails($this->_customdata['userData']->id);
					if(!empty($userRole) && $userRole->name == $CFG->userTypeStudent){
						GLOBAL $CFG,$DB,$USER;
						$userDepartment = $DB->get_record('department_members',array('userid'=>$this->_customdata['userData']->createdby,'is_active'=>1));
						$userRole = array($CFG->userTypeManager,$CFG->userTypeAdmin);
						$userList = getDepartmentManagerAdmin($userDepartment->departmentid);
						if(!empty($userList)){
							foreach($userList as $user){
								$changeownership[$user->id] = $user->firstname.' '.$user->lastname;
							}
						}
						$mform->addElement('select', 'changeownership', get_string('manageby'), $changeownership,$disable_fields);
						$mform->getElement('changeownership')->setSelected($this->_customdata['userData']->createdby);
					}
				}
				/// Next the customisable profile fields
				profile_definition($mform, $userid);

                $showResetButton = null;
				if ($userid == -1) {
					//$btnstring = get_string('createuser');
					$btnstring = get_string('savechanges');
					$showResetButton = true;
				} else {
					//$btnstring = get_string('updatemyprofile');
					$btnstring = get_string('savechanges');
					$showResetButton = false;
				}

    if($this->_customdata['userData']->id>0){
			$created_on = getDateFormat($this->_customdata['userData']->timecreated, $CFG->customDefaultDateFormat);
			$mform->addElement('html','<div class="fitem "><div class="fitemtitle"><div class="fstaticlabel"><label>'.get_string("timecreated","program").'</label></div></div><div class="felement fstatic">'.$created_on.'</div></div>');
		}
		
				
		$this->add_action_buttons(true, $btnstring, $showResetButton);
		$mform->addElement('html', '</div>');
		$mform->addElement('html', '</div>');
    }

    function definition_after_data() {
        global $USER, $CFG, $DB, $OUTPUT;
        $mform =& $this->_form;
        if ($userid = $mform->getElementValue('id')) {
            $user = $DB->get_record('user', array('id'=>$userid));
        } else {
            $user = false;
        }
	
		// select user department,team and role
		if($user){
			//$mform->getElement('department')->setValue($user->department);
			if(isset($_POST['department'])){
				$mform->getElement('department')->setSelected($_POST['department']);
			}else{
				$userDepartment = $DB->get_records_sql("SELECT departmentid FROM {$CFG->prefix}department_members WHERE userid = ".$user->id);
				$userDepartments = array();
				if(!empty($userDepartment)){
					foreach($userDepartment as $department){
						$userDepartments[] = $department->departmentid;
					}
					$mform->getElement('department')->setSelected($userDepartments);
				}else{
					$mform->getElement('department')->setSelected(0);
				}
			}
			if($USER->archetype == $CFG->userTypeManager && !empty($userDepartments)){
				//$mform->getElement('department')->hardFreeze();
			}
			$userGroups = $DB->get_records_sql("SELECT g.name,g.id FROM {$CFG->prefix}groups_members as gm LEFT JOIN {$CFG->prefix}groups as g ON g.id = gm.groupid WHERE gm.is_active = 1 AND gm.userid = ".$user->id);
			$userGroup = array();
			if($userGroups){
				foreach($userGroups as $group){
					$userGroup[] = $group->id;
				}
				$mform->getElement('team')->setSelected($userGroup);
				$mform->getElement('user_team')->setSelected($userGroup);
			}
			if($_POST['role']){
				$mform->getElement('role')->setValue($_POST['role']);
			}else{
				$userRole = $DB->get_record_sql("SELECT ra.roleid,u.id FROM {$CFG->prefix}role_assignments as ra LEFT JOIN {$CFG->prefix}user as u ON u.id = ra.userid WHERE ra.userid = ".$user->id);
				if(!empty($userRole)){
					$mform->getElement('role')->setValue($userRole->roleid);
				}
			}
			if($_POST['email']){
				$mform->getElement('email')->setValue($_POST['email']);
			}else{
				$mform->getElement('email')->setValue($user->email);
			}
			if($user->department || $_POST['department']){
				if(isset($_POST['department_manager'])){
					$mform->getElement('department_manager')->setValue($_POST['department_manager']);
				}else{
					$mform->getElement('department_manager')->setValue($user->parent_id);
				}
			}
			//pr($user);die;
			if($user->is_instructor || (isset($_POST['is_instructor']) && $_POST['is_instructor'] == 1)){
				$is_instructor = $user->is_instructor;
				if(isset($_POST['is_instructor'])){
					$is_instructor = $_POST['is_instructor'];
				}else{
				}
				$mform->getElement('is_instructor')->setValue($is_instructor);
			}
			$isAssignedInstructor = $DB->get_records_sql("SELECT c.id FROM mdl_course as c WHERE c.primary_instructor = ".$user->id);
			if(!empty($isAssignedInstructor)){
				$mform->hardFreeze('is_instructor');
			}
		}else{
			$userDepartment = $DB->get_records_sql("SELECT departmentid FROM {$CFG->prefix}department_members WHERE userid = ".$USER->id);
			
			$userDepartments = array();
			if($userDepartment){
				foreach($userDepartment as $department){
					$userDepartments[] = $department->departmentid;
				}
					
				$mform->getElement('department')->setSelected($userDepartments);
			}
			if(isset($_POST['department'])){
				if(isset($_POST['department_manager'])){
					$mform->getElement('department_manager')->setValue($_POST['department_manager']);
				}
			}
		}
		if($USER->archetype == $CFG->userTypeManager && !empty($userDepartments)){
			//$mform->getElement('department')->hardFreeze();
		}
        // if language does not exist, use site default lang
        if ($langsel = $mform->getElementValue('lang')) {
            $lang = reset($langsel);
            // check lang exists
            if (!get_string_manager()->translation_exists($lang, false)) {
                $lang_el =& $mform->getElement('lang');
                $lang_el->setValue($CFG->lang);
            }
        }

        // user can not change own auth method
        if ($userid == $USER->id) {
            $mform->hardFreeze('auth');
            $mform->hardFreeze('preference_auth_forcepasswordchange');
        }

        // admin must choose some password and supply correct email
        if (!empty($USER->newadminuser)) {
            $mform->addRule('newpassword', get_string('required'), 'required', null, 'client');
            if ($mform->elementExists('suspended')) {
                $mform->removeElement('suspended');
            }
        }

        // require password for new users
        if ($userid > 0) {
            if ($mform->elementExists('createpassword')) {
                $mform->removeElement('createpassword');
            }
        }

        if ($user and is_mnet_remote_user($user)) {
            // only local accounts can be suspended
            if ($mform->elementExists('suspended')) {
                $mform->removeElement('suspended');
            }
        }
        if ($user and ($user->id == $USER->id or is_siteadmin($user))) {
            // prevent self and admin mess ups
            if ($mform->elementExists('suspended')) {
                $mform->hardFreeze('suspended');
            }
        }

        // print picture
        if (empty($USER->newadminuser)) {
        	$imagevalue = "";
            if ($user) {
                $context = context_user::instance($user->id, MUST_EXIST);
                $fs = get_file_storage();
                $hasuploadedpicture = ($fs->file_exists($context->id, 'user', 'icon', 0, '/', 'f2.png') || $fs->file_exists($context->id, 'user', 'icon', 0, '/', 'f2.jpg'));
                if($hasuploadedpicture){
                	$imagevalue .= '<div class="image_delete_wrapper">';
                }
                //if (!empty($user->picture) && $hasuploadedpicture) {
                    $imagevalue .= $OUTPUT->user_picture($user, array('courseid' => SITEID, 'size'=>64,'class'=>'delete_user_pic'));
                    if($hasuploadedpicture){
                    $imagevalue .= "<div class='image_delete_icon'><a href='javascript:void(0)' title='".get_string('remove_pic_title')."'>Remove</a></div></div>";
                    }
               // } else {
                //    $imagevalue = get_string('none');
                //}
				$imageelement = $mform->getElement('currentpicture');
				$imageelement->setValue($imagevalue);
            } else {
               // $imagevalue = get_string('none');
            }

            if ($user && $mform->elementExists('deletepicture') && !$hasuploadedpicture) {
                $mform->removeElement('deletepicture');
            }
        }

        /// Next the customisable profile fields
        profile_definition_after_data($mform, $userid);
    }

    function validation($usernew, $files) {
        global $CFG, $DB,$USER;
		 $err = array();
		 $role = $DB->get_record("role",array('id'=>$usernew['role']));
		 if($role->name != $CFG->userTypeAdmin){
			if($usernew['department'] == 0){
				$err['department'] = get_string('required');
			}else{
				if($USER->archetype == $CFG->userTypeAdmin){
					if($role->name == $CFG->userTypeStudent){
						if($usernew['department_manager'] == 0){
							$err['department_manager'] = get_string('required');
						}
					}
				}else{
					if($role->name == $CFG->userTypeStudent){
						if($usernew['department_manager'] == 0){
							$err['department_manager'] = get_string('required');
						}
					}
				}
			}
		 }
		if(!empty($_POST['dateofbirth'])){
			$endDateTime = strtotime($_POST['dateofbirth']);
			$now = strtotime('now');
			if($now<$endDateTime){
				$err['dateofbirth'] = get_string('dateofbirth_error');
			}
		}
        $usernew = (object)$usernew;
        $usernew->username = trim($usernew->username);
		//pr($usernew);die;
        $user = $DB->get_record('user', array('id'=>$usernew->id));
       
        if($usernew->user_identifier){
            $uniqueIdRecordExist = $DB->get_record('user',array('user_identifier'=>$usernew->user_identifier,'deleted'=>0));
            if($uniqueIdRecordExist && $uniqueIdRecordExist->id != $user->id){
                 $err['user_identifier'] = get_string('uniqueid_already_exist');
            }
        }
        

        if (!$user and !empty($usernew->createpassword)) {
            if ($usernew->suspended) {
                // Show some error because we can not mail suspended users.
                $err['suspended'] = get_string('error');
            }
			if (!empty($usernew->newpassword)) {
                $errmsg = ''; // Prevent eclipse warning.
                if (!check_password_policy($usernew->newpassword, $errmsg)) {
                    $err['newpassword'] = $errmsg;
                }
            } else if (!$user) {
                $err['newpassword'] = get_string('required');
            }
        } else {
            if (!empty($usernew->newpassword)) {
                $errmsg = ''; // Prevent eclipse warning.
                if (!check_password_policy($usernew->newpassword, $errmsg)) {
                    $err['newpassword'] = $errmsg;
                }
            } else if (!$user) {
                $err['newpassword'] = get_string('required');
            }
        }
     
        
        
        if (empty($usernew->username)) {
            //might be only whitespace
            $err['username'] = get_string('required');
        //} else if (!$user or $user->username !== $usernew->username) {
		} else if (!$user or core_text::strtolower($user->username) !== core_text::strtolower($usernew->username)) {
            //check new username does not exist
			
			$uQuery = "SELECT id FROM mdl_user where lower(username)= '".core_text::strtolower($usernew->username)."' AND mnethostid='".$CFG->mnet_localhost_id."'";
			$userNameExist = $DB->get_field_sql($uQuery);
			if($userNameExist) {
                $err['username'] = get_string('usernameexists');
            }
            //check allowed characters
            else {
                if (strtolower($usernew->username) !== clean_param($usernew->username, PARAM_USERNAME)) {
                    $err['username'] = get_string('invalidusername');
                }
            }
			
            /*if ($DB->record_exists('user', array('username'=>$usernew->username, 'mnethostid'=>$CFG->mnet_localhost_id))) {
                $err['username'] = get_string('usernameexists');
            }
            //check allowed characters
            if ($usernew->username !== core_text::strtolower($usernew->username)) {
                $err['username'] = get_string('usernamelowercase');
            } else {
                if ($usernew->username !== clean_param($usernew->username, PARAM_USERNAME)) {
                    $err['username'] = get_string('invalidusername');
                }
            }*/
        }
		
        if (!$user) {
            if (!validate_email($usernew->email)) {
                $err['email'] = get_string('invalidemail');
			}else if ($DB->record_exists('user', array('email'=>$usernew->email, 'mnethostid'=>$CFG->mnet_localhost_id))) { // check if email already registered
				$err['email'] = get_string('emailexists');
			}
        }else{
			if (!validate_email($usernew->email)) {
                $err['email'] = get_string('invalidemail');
			}else if ($DB->get_record_sql("SELECT u.id FROM mdl_user u WHERE u.mnethostid =1 AND email = '".$usernew->email."' AND id != ".$usernew->id)) { // check if email already registered
				$err['email'] = get_string('emailexists');
			}
		}
		if(!empty($user)){
			$userRole = getUserRoleDetails($usernew->id);
			$userDepartment = $DB->get_record_sql("SELECT dm.departmentid FROM mdl_department_members as dm WHERE dm.userid = $usernew->id AND dm.is_active = 1");
			if($userRole->name == $CFG->userTypeManager && (strtolower($role->name) != strtolower($userRole->name))){
				$queryOtherDepartment = "SELECT Count(*) as rowcnt FROM mdl_user AS u LEFT JOIN mdl_department_members as dm ON dm.userid = u.id AND dm.is_active = 1 WHERE u.parent_id = $usernew->id AND dm.departmentid = $userDepartment->departmentid";
				$managersLearner = $DB->get_record_sql($queryOtherDepartment);
				if(!empty($managersLearner) && $managersLearner->rowcnt >0){
					$err['role'] = get_string('usercannotchangerole');
				}
			}
			if($usernew->department != $userDepartment->departmentid && $userRole->name != $CFG->userTypeAdmin){
				$queryOtherDepartment = "SELECT Count(*) as rowcnt FROM mdl_user AS u LEFT JOIN mdl_department_members as dm ON dm.userid = u.id AND dm.is_active = 1 WHERE u.parent_id = $usernew->id AND dm.departmentid = $userDepartment->departmentid";
				$managersLearner = $DB->get_record_sql($queryOtherDepartment);
				if(!empty($managersLearner) && $managersLearner->rowcnt >0){
					$err['department'] = get_string('usercannotchangedepartment');
				}
			}
		}
		/// Next the customisable profile fields
        $err += profile_validation($usernew, $files);
		
        if (count($err) == 0){
            return true;
        } else {
            return $err;
        }
    }
}


