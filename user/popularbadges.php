<?php

require_once(dirname(__FILE__) . '/../config.php');

GLOBAL $DB;

$courseStatusArr = getCourseIdByStatus($userid);

$PAGE->set_url('/user/mybadges.php', array('id'=>$userid));
$PAGE->set_pagelayout('admin');


$context = context_system::instance();
$returnurl = $CFG->wwwroot.'/admin/user.php';

if ($cancel) {
    redirect($returnurl);
}

$PAGE->navbar->add(get_string('manage_user'), new moodle_url($CFG->wwwroot.'/admin/user.php'));
$PAGE->navbar->add(get_string('mybadges'));

/// Print header
$PAGE->set_title("$site->fullname: $stradduserstogroup");
$PAGE->set_heading($site->fullname);
echo $OUTPUT->header();

/// Print the editing form
$user->id = $userid;
echo $outerDivStart = "<div class='tabsOuter'>";
	$outerDivEnd = "</div>";
	include_once('user_tabs.php');
	echo '<div class="userprofile">';
	echo '<div class="left-content-badges">';
	echo '<table>';
	echo '<tr>';
		echo '<td>';
		echo 'Badges Earned';
		echo '</td>';
		echo '<td>';
		echo 'Description';
		echo '</td>';
	echo '</tr>';
	if(!empty($courseStatusArr)){
		foreach($courseStatusArr['type'][2] as $course){
			$badges = $DB->get_records('badge',array('courseid'=>$course));
			if(!empty($badges)){
				foreach($badges as $badge){
					$path = $CFG->wwwroot.'/theme/gourmet/pix/badge/'.$badge->picture; 
					echo '<tr>';
					echo '<td>';
					echo '<div class = "badge-image">';
					echo '<img src = "'.$path.'">';
					echo '</div>';
					echo '<div class = "badge-neme">';
					echo $badge->name;
					echo '</div>';
					echo '</td>';

					echo '<td>';
					echo $badge->description;
					echo '</td>';
					echo '</tr>';
				}
			}
?>
<?php
		}
	}
	echo '</table>';
echo '</div>';

echo '<div class="right-content-badges">';
	echo '<table>';
	echo '<tr>';
		echo '<td>';
		echo 'Badges Earned';
		echo '</td>';
		echo '<td>';
		echo 'Description';
		echo '</td>';
	echo '</tr>';
	if(!empty($courseStatusArr)){
		foreach($courseStatusArr['type'][2] as $course){
			$badges = $DB->get_records('badge',array('courseid'=>$course));
			if(!empty($badges)){
				foreach($badges as $badge){
					$path = $CFG->wwwroot.'/theme/gourmet/pix/badge/'.$badge->picture; 
					echo '<tr>';
					echo '<td>';
					echo '<div class = "badge-image">';
					echo '<img src = "'.$path.'">';
					echo '</div>';
					echo '<div class = "badge-neme">';
					echo $badge->name;
					echo '</div>';
					echo '</td>';

					echo '<td>';
					echo $badge->description;
					echo '</td>';
					echo '</tr>';
				}
			}
?>
<?php
		}
	}
	echo '</table>';
echo '</div>';
echo '</div>';
echo $outerDivEnd;
echo $OUTPUT->footer();
?>