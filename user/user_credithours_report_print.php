<?php

	/**
		* Custom module - User Credit Hours Print Page
		* Date Creation - 24/07/2014
		* Date Modification : 24/07/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/

	require_once("../config.php");
	
	//$userrole =  getUserRole($USER->id);
	if( $USER->archetype == $CFG->userTypeStudent ) {
		$groupCourses = checkOwnerAccess($USER->id);
		if(!$groupCourses){
			redirect($CFG->wwwroot);
		}
	}

	$PAGE->set_pagelayout('print');
	//require_login(); 
	checkLogin();
	
	$sort    = optional_param('sort', 'COALESCE(NULLIF(firstname, ""), lastname), lastname', PARAM_ALPHANUM);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 0, PARAM_INT);        // how many per page
	
	
	$sDate = '';
	$eDate = '';
	$sDate =  $CFG->learningYear['from'];
	$eDate = $CFG->learningYear['to'];
	
	$sDepartment    = optional_param('department', '-1', PARAM_RAW);
	$sTeam          = optional_param('team', '-1', PARAM_RAW);       
	$sUser          = optional_param('user', '-1', PARAM_RAW);       
	$export         = optional_param('action', '', PARAM_ALPHANUM);
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);
	$sUserType     = optional_param('userType', $CFG->userTypeStudent, PARAM_ALPHA);
	$user_group      = optional_param('user_group','-1', PARAM_RAW);

	$paramArray = array(
					'ch' => optional_param('ch', '', PARAM_ALPHA),
					'key' => optional_param('key', '', PARAM_RAW),
					'department' => $sDepartment,
					'team' => $sTeam,
					'user' => $sUser,
					'startDate' => $sStartDate,
					'endDate' => $sEndDate,
					'userType' => $sUserType,
					'user_group' => $user_group,
					'job_title' => optional_param('job_title', '-1', PARAM_RAW),
					'company' => optional_param('company', '-1', PARAM_RAW),
					'sel_mode' => optional_param('sel_mode', '-1', PARAM_RAW)
				  );
	$filterArray = array(							
						'nm'=>get_string('name','course'),
				   );
	
	////// Define those key, which we need to remove first time //////
	$removeKeyArray = array('perpage');
	
	
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('usercredithoursreport'));
	$PAGE->navbar->add(get_string('reports','multiuserreport'), new moodle_url($CFG->wwwroot.'/my/adminreportdashboard.php'));
	
	$userHTML = '';
	$reportContentCSV = '';
	$reportContentPDF = '';
    if($export && in_array($export, array('exportcsv', 'exportpdf', 'print'))){
	
		$userReport = getUserCreditHoursReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
		$userHTML = $userReport->userHTML;
		$reportContentCSV = $userReport->reportContentCSV;
		$reportContentPDF = $userReport->reportContentPDF;		
		
	}else{
	    redirect(new moodle_url('/'));
	}
			

	/* bof export to csv */
	
	if($sUserType == $CFG->userTypeInstructor){
		  $headerLabelGraph = get_string('instructorcredithoursreport');
	}else{
		  $headerLabelGraph = get_string('learnercredithoursreport');
	}
	   
	
	if(isset($export) && $export == 'exportcsv') {
	
		echo $filepath = $CFG->dirroot."/local/reportexport/temp";		
		chmod($filepath, 0777);		
		$filename = str_replace(' ', '_', $headerLabelGraph)."_".date("m-d-Y").".csv";  
		$filepathname = $filepath.'/'.$filename;
		unlink($filepathname);
		$handler = fopen($filepathname, "w");
		fwrite($handler, $reportContentCSV);
		exportCSV($filepathname);
	}
	/* eof export to csv */	
	
	
	/* bof export to pdf */
	if(isset($export) && $export == 'exportpdf') {
	
		//echo $reportContentPDF;die;
		$filename = str_replace(' ', '_', $headerLabelGraph)."_".date("m-d-Y").".pdf";
		exportPDF($filename, $reportContentPDF, '', $headerLabelGraph);
		
	}
	/* eof export to pdf */	
	
	echo $OUTPUT->header(); 
	echo $userHTML;
	//echo $OUTPUT->footer();

?>
<style>
#page { margin: 20px auto 0;}
</style>
