<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Allows you to edit a users profile
 *
 * @copyright 1999 Martin Dougiamas  http://dougiamas.com
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package user
 */

require_once('../config.php');
require_once($CFG->libdir.'/gdlib.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->dirroot.'/user/editadvanced_form.php');
require_once($CFG->dirroot.'/user/editlib.php');
require_once($CFG->dirroot.'/user/profile/lib.php');
require_once($CFG->dirroot.'/user/lib.php');
global $CFG;


//HTTPS is required in this page when $CFG->loginhttps enabled
$PAGE->https_required();
require_login();
$id     = optional_param('id', $USER->id, PARAM_INT);    // user id; -1 if creating new user
$course = optional_param('course', SITEID, PARAM_INT);   // course id (defaults to Site)

$PAGE->set_url('/user/editadvanced.php', array('course'=>$course, 'id'=>$id));
if($id && $id != -1){
	$header = $SITE->fullname.": ".get_string('edit_user');
}else{
	$header = $SITE->fullname.": ".get_string('adduser');
}
checkUserAccess('user' , $id);
$PAGE->set_title($header);
$PAGE->set_heading($header);
$course = $DB->get_record('course', array('id'=>$course), '*', MUST_EXIST);

if (!empty($USER->newadminuser)) {
    $PAGE->set_course($SITE);
    $PAGE->set_pagelayout('maintenance');
} else {
    if ($course->id == SITEID) {
        require_login();
        $PAGE->set_context(context_system::instance());
    } else {
        require_login($course);
    }
    $PAGE->set_pagelayout('admin');
}

if ($course->id == SITEID) {
    $coursecontext = context_system::instance();   // SYSTEM context
} else {
    $coursecontext = context_course::instance($course->id);   // Course context
}
$systemcontext = context_system::instance();
$PAGE->navbar->add(get_string('manage_user'), new moodle_url('/admin/user.php'));
if($id && $id != -1){
	$PAGE->navbar->add(get_string('edit_user'));
}else{
	$PAGE->navbar->add(get_string('adduser'));
}
if ($id == -1) {
    // creating new user
    $user = new stdClass();
    $user->id = -1;
    $user->auth = 'manual';
    $user->confirmed = 1;
    $user->deleted = 0;
    require_capability('moodle/user:create', $systemcontext);
    //admin_externalpage_setup('addnewuser', '', array('id' => -1));
} else {
    // editing existing user
    require_capability('moodle/user:update', $systemcontext);
    $user = $DB->get_record('user', array('id'=>$id), '*', MUST_EXIST);
  
    $PAGE->set_context(context_user::instance($user->id));
    if ($user->id != $USER->id) {
        //$PAGE->navigation->extend_for_user($user);
    } else {
        if ($node = $PAGE->navigation->find('myprofile', navigation_node::TYPE_ROOTNODE)) {
            $node->force_open();
        }
    }
}

// remote users cannot be edited
if ($user->id != -1 and is_mnet_remote_user($user)) {
    redirect($CFG->wwwroot . "/user/view.php?id=$id&course={$course->id}");
}

if ($user->id != $USER->id and is_siteadmin($user) and !is_siteadmin($USER)) {  // Only admins may edit other admins
    print_error('useradmineditadmin');
}

if (isguestuser($user->id)) { // the real guest user can not be edited
    print_error('guestnoeditprofileother');
}

if ($user->deleted) {
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('userdeleted'));
    echo $OUTPUT->footer();
    die;
}

//load user preferences
useredit_load_preferences($user);

//Load custom profile fields data
profile_load_data($user);

//User interests
if (!empty($CFG->usetags)) {
    require_once($CFG->dirroot.'/tag/lib.php');
    $user->interests = tag_get_tags_array('user', $id);
}
		
if ($user->id !== -1) {
    $usercontext = context_user::instance($user->id);
    $editoroptions = array(
        'maxfiles'   => EDITOR_UNLIMITED_FILES,
        'maxbytes'   => $CFG->maxbytes,
        'trusttext'  => false,
        'forcehttps' => false,
        'context'    => $usercontext
    );

    $user = file_prepare_standard_editor($user, 'description', $editoroptions, $usercontext, 'user', 'profile', 0);
	
	/*$overviewfilesoptions['maxfiles'] = 1;
	$overviewfilesoptions['maxbytes'] = 0;
	$overviewfilesoptions['subdirs'] = 0;
	$overviewfilesoptions['accepted_types'][0] = '.jpg';
	$overviewfilesoptions['accepted_types'][1] = '.gif';
	$overviewfilesoptions['accepted_types'][2] = '.png';
	$overviewfilesoptions['context'] = $usercontext;


	file_prepare_standard_filemanager($user, 'icon', $overviewfilesoptions, $usercontext, 'user', 'user', 0);
	file_prepare_standard_filemanager($user, 'icon', $editoroptions, $usercontext, 'user', 'draft', 0);*/



} else {
    $usercontext = null;
    // This is a new user, we don't want to add files here
    $editoroptions = array(
        'maxfiles'=>0,
        'maxbytes'=>0,
        'trusttext'=>false,
        'forcehttps'=>false,
        'context' => $coursecontext
    );
}

// Prepare filemanager draft area.
$draftitemid = 0;
$filemanagercontext = $editoroptions['context'];
$filemanageroptions = array('maxbytes'       => $CFG->maxbytes,
                             'subdirs'        => 0,
                             'maxfiles'       => 1,
                             'accepted_types' => 'web_image');
//pr($filemanagercontext);die;
file_prepare_draft_area($draftitemid, $usercontext->id, 'user', 'draft', 0, $filemanageroptions);
$user->imagefile = $draftitemid;
//create form
//pr($user);die;
$dob = '';
if($user->dateofbirth){
	$dob = date('m/d/Y', $user->dateofbirth);
}
$userform = new user_editadvanced_form(null, array(
    'editoroptions' => $editoroptions,
    'filemanageroptions' => $filemanageroptions,
    'dateofbirth' => $dob,
    'userid' => $user->id,
	'userData'=>$user));
$user->dateofbirth = $dob;
$userform->set_data($user);

if ($userform->is_cancelled()) {
	if (!empty($user->id) && ($user->id != -1)) {
		$url = new moodle_url($CFG->wwwroot.'/user/viewuser.php', array('id'=>$user->id,'course'=>1));
	} else {
		$backLink = getBackUrlForUserSection();
		//$url = new moodle_url($CFG->wwwroot.'/admin/user.php');
		$url = $backLink;
	}
	
    redirect($url);
}elseif ($usernew = $userform->get_data()) {
	
    if (empty($usernew->auth)) {
        //user editing self
        $authplugin = get_auth_plugin($user->auth);
        unset($usernew->auth); //can not change/remove
    } else {
        $authplugin = get_auth_plugin($usernew->auth);
    }
//pr($usernew);die;
    if($usernew->country !='US'){
        $usernew->state = 0;
    }
    
    $usernew->timemodified = time();
    $createpassword = false;

    if ($usernew->id == -1) {
        //TODO check out if it makes sense to create account with this auth plugin and what to do with the password
        unset($usernew->id);
        $createpassword = !empty($usernew->createpassword);
        unset($usernew->createpassword);
        $usernew = file_postupdate_standard_editor($usernew, 'description', $editoroptions, null, 'user', 'profile', null);
        $usernew->mnethostid = $CFG->mnet_localhost_id; // always local user
        $usernew->confirmed  = 1;
        $usernew->timecreated = time();
		$createpassword = 0;
        if ($createpassword) {
            $usernew->password = '';
        } else {
            $usernew->password = hash_internal_user_password($usernew->newpassword);
        }
		$createpassword = 1;
		$usernew->createdby = $USER->id;
        $usernew->id = user_create_user($usernew, false);
        //$usernew->id = 128;
         $_SESSION['update_msg'] = get_string('useraddsuccess');
         $_SESSION['error_class'] = 'success'; 
	 send_confirmation_email_to_user($usernew);
         
         //Auto enrollment into course
        // $output = auto_enrollment_into_courses($usernew,true);
    } else {
       // pr($usernew);die;       
        //Auto enrollment into course
	//$output = auto_enrollment_into_courses($usernew,false);
        
        $usernew = file_postupdate_standard_editor($usernew, 'description', $editoroptions, $usercontext, 'user', 'profile', 0);
		$usernew->updatedby = $USER->id;
        // Pass a true old $user here.
        if (!$authplugin->user_update($user, $usernew)) {
            // Auth update failed.
            print_error('cannotupdateuseronexauth', '', '', $user->auth);
        }
        $_SESSION['update_msg'] = get_string('userupdatesuccess');
        $_SESSION['error_class'] = 'success';
        user_update_user($usernew, false);

        //set new password if specified
        if (!empty($usernew->newpassword)) {
            if ($authplugin->can_change_password()) {
                if (!$authplugin->user_update_password($usernew, $usernew->newpassword)){
                    print_error('cannotupdatepasswordonextauth', '', '', $usernew->auth);
                }
                unset_user_preference('create_password', $usernew); // prevent cron from generating the password
            }
        }

        // force logout if user just suspended
        if (isset($usernew->suspended) and $usernew->suspended and !$user->suspended) {
            \core\session\manager::kill_user_sessions($user->id);
        }
    }
	
    $usercontext = context_user::instance($usernew->id);

    //update preferences
    useredit_update_user_preference($usernew);

    // update tags
    if (!empty($CFG->usetags) and empty($USER->newadminuser)) {
        useredit_update_interests($usernew, $usernew->interests);
    }

    //update user picture
    if (empty($USER->newadminuser)) {
        useredit_update_picture($usernew, $userform, $filemanageroptions);
    }

    // update mail bounces
    useredit_update_bounces($user, $usernew);

    // update forum track preference
    useredit_update_trackforums($user, $usernew);

    // save custom profile fields data
    profile_save_data($usernew);

    // reload from db
    $usernew = $DB->get_record('user', array('id'=>$usernew->id));

	//Assign role to user
	//assignRoleToUser($usernew); // default user type is 5 i.e. learner

    if ($createpassword) {
        //setnew_password_and_mail($usernew);
        unset_user_preference('create_password', $usernew);
        //set_user_preference('auth_forcepasswordchange', 1, $usernew);
    }
    if ($user->id == $USER->id) {
        // Override old $USER session variable
        foreach ((array)$usernew as $variable => $value) {
            if ($variable === 'description' or $variable === 'password') {
                // These are not set for security nad perf reasons.
                continue;
            }
            $USER->$variable = $value;
        }
        // preload custom fields
        profile_load_custom_fields($USER);

        if (!empty($USER->newadminuser)) {
            unset($USER->newadminuser);
            // apply defaults again - some of them might depend on admin user info, backup, roles, etc.
            admin_apply_default_settings(NULL , false);
            // redirect to admin/ to continue with installation
            redirect("$CFG->wwwroot/$CFG->admin/");
        } else {
            redirect("$CFG->wwwroot/user/view.php?id=$USER->id&course=$course->id");
        }
    } else {
        \core\session\manager::gc(); // Remove stale sessions.
        $backLink = getBackUrlForUserSection();
        //redirect("$CFG->wwwroot/$CFG->admin/user.php");
        redirect($backLink);
    }
    //never reached
}

// make sure we really are on the https page when https login required
$PAGE->verify_https_required();


/// Display page header
if ($user->id == -1 or ($user->id != $USER->id)) {
    if ($user->id == -1) {
        echo $OUTPUT->header();
    } else {
        $PAGE->set_heading($SITE->fullname);
        echo $OUTPUT->header();
        $userfullname = fullname($user, true);
        echo $OUTPUT->heading($userfullname);
    }
} else if (!empty($USER->newadminuser)) {
    $strinstallation = get_string('installation', 'install');
    $strprimaryadminsetup = get_string('primaryadminsetup');

    $PAGE->navbar->add($strprimaryadminsetup);
    $PAGE->set_title($strinstallation);
    $PAGE->set_heading($strinstallation);
    $PAGE->set_cacheable(false);

    echo $OUTPUT->header();
    echo $OUTPUT->box(get_string('configintroadmin', 'admin'), 'generalbox boxwidthnormal boxaligncenter');
    echo '<br />';
} else {
    $streditmyprofile = get_string('editmyprofile');
    $strparticipants  = get_string('participants');
    $strnewuser       = get_string('newuser');
    $userfullname     = fullname($user, true);

    $PAGE->set_title("$course->shortname: $streditmyprofile");
    $PAGE->set_heading($course->fullname);

    echo $OUTPUT->header();
    echo $OUTPUT->heading($userfullname);
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/style/jquery-ui.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-ui.js"></script>
<?php 
/// Finally display THE form
	echo $outerDivStart = "<div class='tabsOuter'>";
	$outerDivEnd = "</div>";
	include_once('user_tabs.php');
	echo '<div class="userprofile borderBlockSpace">';
$userform->display();

echo '</div>';
echo $outerDivEnd;
/// and proper footer

$userRoleDetails = array();
if($_POST['role']){
	$userRoleDetails = $DB->get_record("role",array('id'=>$_POST['role']));
}elseif(isset($_REQUEST['id']) && !empty($_REQUEST['id']) && $_REQUEST['id']!='-1'){
  $userRoleDetails = getUserRoleDetails($_REQUEST['id']);
}
?>
<script>  
$(document).ready(function() { 
	$('.disabled_user_form_fields').attr('disabled', true);
var img = '<img src="<?php echo $CFG->wwwroot; ?>/theme/image.php?theme=gourmet&amp;component=core&amp;image=req" alt="Required field" title="Required field" class="req">';
    <?php if(isset($userRoleDetails->name) && $userRoleDetails->name == $CFG->userTypeAdmin ){?> 
      $("#id_team").html("<option value = '0'><?php echo get_string('select_team'); ?></option>");
	  $("#fitem_id_team").hide();
	<?php } ?>
	
	<?php if(count($userRoleDetails) == 0 ){?>
		<?php if(isset($_POST['role']) && $_POST['role'] == $CFG->adminTypeId ){?> 
		  $("#id_team").html("<option value = '0'><?php echo get_string('select_team'); ?></option>");
		  $("#fitem_id_team").hide();
		  $('#department_manager').hide();
		<?php } ?>
		
		<?php if(isset($_POST['role']) && $_POST['role'] == $CFG->managerTypeId  ){?> 
			$('#department_manager').hide();
			$("#show-user-group").show();
		<?php } ?>
		
		<?php if(isset($_POST['role']) && $_POST['role'] == $CFG->studentTypeId && isset($_POST['department']) && $_POST['department'] != '0' ){?> 
				$('#department_manager').show();
				$("#show-user-group").show();
		<?php } ?>
	<?php } ?>	

	 <?php 
		if(isset($_POST['role']->name) && $_POST['role']->name == $CFG->userTypeStudent ){?>
				if($("#fitem_id_department_manager").find('.fitemtitle').find('label').find('img').attr('class') == 'req'){
				}else{
					$("#fitem_id_department_manager").find('.fitemtitle').find('label').append(img);
				}
				$("#department_manager").show();
				$("#show-user-group").show();
		<?php 
		}else if(isset($userRoleDetails->name) && $userRoleDetails->name == $CFG->userTypeStudent ){?> 
				if($("#fitem_id_department_manager").find('.fitemtitle').find('label').find('img').attr('class') == 'req'){
				}else{
					$("#fitem_id_department_manager").find('.fitemtitle').find('label').append(img);
				}
				$("#department_manager").show();
				$("#show-user-group").show();
	<?php }else{
			if(isset($userRoleDetails->name) && $userRoleDetails->name == $CFG->userTypeManager ){
			?>
				$("#show-user-group").show();
		<?php
			}else if(isset($_POST['role']->name) && $_POST['role']->name == $CFG->userTypeManager){
			?>
				$("#show-user-group").show();
			<?php
			}else{
			?>
				$("#show-user-group").hide();
				<?php
			}
	} ?>
	<?php
		if($USER->archetype == $CFG->userTypeManager){
		?>
			$("#show-user-group").hide();
		<?php
		}
	?>
	$('#start_date').datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: '1900:'+(new Date).getFullYear() ,
		maxDate: +0
	});
	if($("#fitem_id_department").find('.fitemtitle').find('label').find('img').attr('class') == 'req'){
	}else{
		$("#fitem_id_department").find('.fitemtitle').find('label').append(img);
	}
	$("#id_role").change(function(){
		//var userRole = $(this).val();
		$('#id_is_primary').removeAttr('checked');
		var userRole = $(this).find('option:selected').text();
		if(userRole.toLowerCase() == "<?php echo $CFG->userTypeAdmin;?>".toLowerCase()){ // admin
			$('#department_select').hide();
			$('#department_manager').hide();
			$("#id_team").html("<option value = '0'><?php echo get_string('select_team'); ?></option>");
			$("#fitem_id_team").hide();
			$('#is_primary').hide();
			$("#show-user-group").hide();
		}else if(userRole.toLowerCase() == "<?php echo $CFG->userTypeManager;?>".toLowerCase()){ // manager
			//$('#id_department').attr('multiple','multiple');
			$('#department_select').show();
			if("<?php echo $CFG->userTypeAdmin;?>".toLowerCase() == "<?php echo $USER->archetype;?>".toLowerCase() ){
				$('#is_primary').show();
				$("#show-user-group").show();
			}
			$('#department_manager').hide();
			$("#fitem_id_team").show();
		}else if(userRole.toLowerCase() == "<?php echo $CFG->userTypeStudent;?>".toLowerCase()){// learner
			//$("#id_department").val(0);
			$('#is_primary').hide();
			//$('#id_department').removeAttr('multiple');
			if($("#fitem_id_department").find('.fitemtitle').find('label').find('img').attr('class') == 'req'){
			}else{
				$("#fitem_id_department").find('.fitemtitle').find('label').append(img);
			}
			$('#department_select').show();
			$("#fitem_id_team").show();
			$("#show-user-group").show();
			$("#department_manager").show();
			if("<?php echo $CFG->userTypeManager;?>".toLowerCase() == "<?php echo $USER->archetype;?>".toLowerCase() ){
				if($("#fitem_id_department_manager").find('.fitemtitle').find('label').find('img').attr('class') == 'req'){
				}else{
					$("#fitem_id_department_manager").find('.fitemtitle').find('label').append(img);
				}
				//$("#department_manager").show();
			}
			if("<?php echo $CFG->userTypeAdmin;?>".toLowerCase() == "<?php echo $USER->archetype;?>".toLowerCase() ){
				$("#show-user-group").show();
			}
		}else{
			//$("#id_department").val(0);
			$('#is_primary').hide();
			$("#show-user-group").hide();
			if($("#fitem_id_department").find('.fitemtitle').find('label').find('img').attr('class') == 'req'){
			}else{
				$("#fitem_id_department").find('.fitemtitle').find('label').append(img);
			}
			$('#department_select').hide();
			
			$("#id_team").html("<option value = '0'><?php echo get_string('select_team'); ?></option>");
	        $("#fitem_id_team").hide();
	        $("#department_manager").hide();
		}
	});
	$("#id_department").change(function(){
		var userRole = $("#id_role").val();
		var userRoleText = $("#id_role").find('option:selected').text();
		var userId = $("input[name=id]").val();
		var departmentId = $(this).val();
		$('#id_is_primary').removeAttr('checked');
		if(departmentId == 0){
			$(this).parent().find('#id_error_department').remove();
			$(this).parent().find('#id_error_break_department').remove();
			$(this).before('<span id="id_error_department" class="error" tabindex="0"> Required</span><br class="error" id="id_error_break_department">');
			$(this).parent().addClass('error');
		}else{
			$(this).parent().find('#id_error_department').remove();
			$(this).parent().find('#id_error_break_department').remove();
			$(this).parent().removeClass('error');
		}
		
		if(departmentId != 0){
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
					type:'POST',
					data:'action=getDepartmentTeamList&departmentId='+departmentId,
					success:function(data){
						$("#id_team").html(data);
					}
			});
		}else{
			$("#id_team").html("<option value = '0'><?php echo get_string('select_team'); ?></option>");
		}
		if(departmentId != 0 && userRoleText.toLowerCase() == "<?php echo $CFG->userTypeStudent;?>".toLowerCase()){
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
					type:'POST',
					data:'action=getDepartmentManagerList&departmentId='+departmentId+'&userRole='+userRole+'&userId='+userId,
					success:function(data){
						if($("#fitem_id_department_manager").find('.fitemtitle').find('label').find('img').attr('class') == 'req'){
						}else{
							$("#fitem_id_department_manager").find('.fitemtitle').find('label').append(img);
						}
						$("#id_department_manager").html(data);
						$("#department_manager").show();
					}
			});
		}else{
			$("#department_manager").hide();
		}
	});
	$("#id_department").on('blur',function(){
		var departmentId = $(this).val();
		$('#id_is_primary').removeAttr('checked');
		if(departmentId == 0){
			$(this).parent().find('span').remove();
			$(this).parent().find('br').remove();
			$(this).before('<span id="id_error_department" class="error" tabindex="0"> Required</span><br class="error" id="id_error_break_department">');
			$(this).parent().addClass('error');
		}else{
			$(this).parent().find('span').remove();
			$(this).parent().find('br').remove();
			$(this).parent().removeClass('error');
		}
	});
	$("#id_department_manager").on('blur',function(){
		var departmentId = $(this).val();
		if(departmentId == 0){
			$(this).parent().find('span').remove();
			$(this).parent().find('br').remove();
			$(this).before('<span id="id_error_department" class="error" tabindex="0"> Required</span><br class="error" id="id_error_break_department">');
			$(this).parent().addClass('error');
		}else{
			$(this).parent().find('span').remove();
			$(this).parent().find('br').remove();
			$(this).parent().removeClass('error');
		}
	});
	$("#id_department_manager").on('change',function(){
		var departmentId = $(this).val();
		if(departmentId == 0){
			$(this).parent().find('span').remove();
			$(this).parent().find('br').remove();
			$(this).before('<span id="id_error_department" class="error" tabindex="0"> Required</span><br class="error" id="id_error_break_department">');
			$(this).parent().addClass('error');
		}else{
			$(this).parent().find('span').remove();
			$(this).parent().find('br').remove();
			$(this).parent().removeClass('error');
		}
	});
	$("#id_submitbutton").on('click',function(event){
		var userRole = $("#id_role").find('option:selected').text();
		if(userRole.toLowerCase() == "<?php echo $CFG->userTypeAdmin;?>".toLowerCase()){ // admin
		}else if(userRole.toLowerCase() == "<?php echo $CFG->userTypeManager;?>".toLowerCase()){ // manager
		}else if(userRole.toLowerCase() == "<?php echo $CFG->userTypeStudent;?>".toLowerCase()){// learner
			$("#id_department").trigger('change');
			$("#id_department_manager").trigger('change');
		}else{
		}	
		$('.disabled_user_form_fields').attr('disabled', false);
		$('#mform1').submit();
	});
	var roleChk = "<?php echo $_POST['role'];?>";
	if(roleChk != '' && roleChk != 0 && roleChk == 5){
		if($("#fitem_id_department_manager").find('.fitemtitle').find('label').find('img').attr('class') == 'req'){
		}else{
			$("#fitem_id_department_manager").find('.fitemtitle').find('label').append(img);
		}
	}

	$("#id_is_primary").on('click',function(event){
		var currentUser = $( "input[name='id']" ).val();
		var departmentId = $("#id_department").val();
		if($(this).is(':checked') && departmentId != 0){
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
					type:'POST',
					data:'action=getDepartmentPrimaryManager&departmentId='+departmentId+'&currentUser='+currentUser,
					success:function(data){
						if(data != ''){
							var message = "A primary manager("+data+") already exists for this department.\n Do you want to change this primary manager?";
							var r = confirm(message);
							if (r == true) {
								return true;
							} else {
								$('#id_is_primary').removeAttr('checked');
							}
						}else{
							return true;
						}
					}
			});
		}else{
			if(departmentId == 0){
				$("#id_department").parent().find('span').remove();
				$("#id_department").parent().find('br').remove();
				$("#id_department").before('<span id="id_error_department" class="error" tabindex="0"> Required</span><br class="error" id="id_error_break_department">');
				$("#id_department").parent().addClass('error');
				$('#id_is_primary').removeAttr('checked');
			}
		}
	});

	 $(document).on('blur', '#id_username,#id_firstname,#id_lastname,#id_email,#id_newpassword', function(){

		   var cVal = $(this).val();
		   cVal = $.trim(cVal);
		   $(this).val(cVal);


		}); 
	 $("#fitem_id_deletepicture").hide();
	$(".delete_user_pic").unwrap();

	 $(document).on('click', '.image_delete_icon', function(){
		 
		 var r = confirm("<?php echo get_string('delete_user_pic');?>");
		 if (r == true) {
			 $("#id_deletepicture").prop('checked', true);
			 $("#id_submitbutton").trigger('click');
		 } 		
		});
                
        $("#id_country").on('change',function(){
                var countryvalue = $(this).val();
                
                if(countryvalue == 'US'){                  
                    //$("#fitem_id_state").show();
                }else{ 
                    $("#id_state option").removeAttr("selected");
                    //$("#id_state").val(0).attr("selected", "selected");
                    $("#id_state option[value=0]").attr("selected", "selected");
                    //$("#fitem_id_state").hide();
                }
        });

});


</script>


<?php

echo $OUTPUT->footer();

