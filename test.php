<?php

require_once('config.php');
global $USER,$CFG,$DB;
if($_REQUEST['action'] == 'scormentry'){
	$scormData = $DB->get_records_sql("SELECT * FROM mdl_scorm_scoes_track WHERE element = 'x.start.time' ORDER BY userid,scormid");
	if(!empty($scormData)){
		$launchData = new stdClass;
		$scormCourse = array();
		foreach($scormData as $scorm){
			if(!array_key_exists($scorm->scormid,$scormCourse)){
				$courseArray = $DB->get_record_sql("SELECT s.course FROM mdl_scorm as s WHERE s.id = ".$scorm->scormid);
				$courseId = $courseArray->course;
				$scormCourse[$scorm->scormid] = $courseId;
			}else{
				$courseId = $scormCourse[$scorm->scormid];
			}
			if(!empty($courseArray)){
				$launchData->userid = $scorm->userid;
				$launchData->courseid = $courseId;
				$launchData->created_on = $scorm->value;
				$launchData->asset_id = $scorm->scormid;
				$launchData->module = 18;
				$launchData->attempt = $scorm->attempt;
				$insertedRecord = $DB->insert_record('launch_log',$launchData);
			}
		}	
	}
	$resourceData = $DB->get_records_sql("SELECT * FROM mdl_user_noncourse_lastaccess un ORDER BY userid,courseid");
	if(!empty($resourceData)){
		$launchData = new stdClass;
		foreach($resourceData as $resource){
			$attempt = 1;
			$attemptCount = $DB->get_record_sql("SELECT COUNT(*) as last_attempt FROM mdl_launch_log un WHERE un.asset_id = ".$resource->resource_id." AND un.module = 17 and un.userid = ".$resource->userid);
			if(!empty($attemptCount) && $attemptCount->last_attempt != 0){
				$attempt = $attemptCount->last_attempt;
			}
			$launchData->userid = $resource->userid;
			$launchData->courseid = $resource->courseid;
			$launchData->created_on = $resource->timeaccess;
			$launchData->asset_id = $resource->resource_id;
			$launchData->module = 17;
			$launchData->attempt = $attempt;
			$insertedRecord = $DB->insert_record('launch_log',$launchData);
		}
	}
}elseif($_REQUEST['action'] == 'programorder'){ // Set program Course Order
	$programCoursesRecords = $DB->get_records('program_course',array('is_active'=>1),'id ASC');
	$programData = array();
	foreach($programCoursesRecords as $programCourse){
		$programData[$programCourse->programid][] = $programCourse;
	}
	foreach($programData as $pcData){
		$order = 1;
		foreach($pcData as $courses){
			$updateData = new stdClass();
			$updateData = $courses;
			$updateData->course_order = $order;
			$updated = $DB->update_record('program_course',$updateData);
			$order++;
		}
		
	}
	die('Done');
}
?>