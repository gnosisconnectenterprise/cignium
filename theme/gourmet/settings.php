<?php
$settings = null;

defined('MOODLE_INTERNAL') || die;
    
	$ADMIN->add('themes', new admin_category('theme_gourmet', 'gourmet'));

	// "genericsettings" settingpage
	$temp = new admin_settingpage('theme_gourmet_generic',  get_string('geneicsettings', 'theme_gourmet'));
	
    // Logo file setting.   
    $name = 'theme_gourmet/logo';
    $title = get_string('logo','theme_gourmet');
    $description = get_string('logodesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);  

    // Layout - sidebar position.
    $name = 'theme_gourmet/layout';
    $title = get_string('layout', 'theme_gourmet');
    $description = get_string('layoutdesc', 'theme_gourmet');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Include Awesome Font from Bootstrapcdn
    $name = 'theme_gourmet/bootstrapcdn';
    $title = get_string('bootstrapcdn', 'theme_gourmet');
    $description = get_string('bootstrapcdndesc', 'theme_gourmet');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 0);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    // Navbar Seperator.
    $name = 'theme_gourmet/navbarsep';
    $title = get_string('navbarsep' , 'theme_gourmet');
    $description = get_string('navbarsepdesc', 'theme_gourmet');
    $nav_thinbracket = get_string('nav_thinbracket', 'theme_gourmet');
    $nav_doublebracket = get_string('nav_doublebracket', 'theme_gourmet');
    $nav_thickbracket = get_string('nav_thickbracket', 'theme_gourmet');
    $nav_slash = get_string('nav_slash', 'theme_gourmet');
    $nav_pipe = get_string('nav_pipe', 'theme_gourmet');
    $dontdisplay = get_string('dontdisplay', 'theme_gourmet');
    $default = '\f105';
    $choices = array('\f105'=>$nav_thinbracket, '/'=>$nav_slash, '\f101'=>$nav_doublebracket, '\f054'=>$nav_thickbracket, '|'=>$nav_pipe);
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Copyright setting.
    $name = 'theme_gourmet/copyright';
    $title = get_string('copyright', 'theme_gourmet');
    $description = get_string('copyrightdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Footerwidget setting.
    $name = 'theme_gourmet/footerwidget';
    $title = get_string('footerwidget', 'theme_gourmet');
    $description = get_string('footerwidgetdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Custom CSS file.
    $name = 'theme_gourmet/customcss';
    $title = get_string('customcss', 'theme_gourmet');
    $description = get_string('customcssdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('theme_gourmet', $temp);
    
    /* Header Settings */
    $temp = new admin_settingpage('theme_gourmet_header', get_string('headerheading', 'theme_gourmet'));
	$temp->add(new admin_setting_heading('theme_gourmet_header', get_string('headerheadingsub', 'theme_gourmet'),
            format_text(get_string('headerdesc' , 'theme_gourmet'), FORMAT_MARKDOWN)));
    
    // Header widget setting
    $name = 'theme_gourmet/headerwidget';
    $title = get_string('headerwidget','theme_gourmet');
    $description = get_string('headerwidgetdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Header phone number settings
    $name = 'theme_gourmet/headerphone';
    $title = get_string('headerphone','theme_gourmet');
    $description = get_string('headerphonedesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Header email settings
    $name = 'theme_gourmet/headeremail';
    $title = get_string('headeremail','theme_gourmet');
    $description = get_string('headeremaildesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('theme_gourmet', $temp);
    
    /* Custom Menu Settings */
    $temp = new admin_settingpage('theme_gourmet_custommenu', get_string('custommenuheading', 'theme_gourmet'));
	            
    // Description for mydashboard
    $name = 'theme_gourmet/mydashboardinfo';
    $heading = get_string('mydashboardinfo', 'theme_gourmet');
    $information = get_string('mydashboardinfodesc', 'theme_gourmet');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Enable dashboard display in custommenu.
    $name = 'theme_gourmet/displaymydashboard';
    $title = get_string('displaymydashboard', 'theme_gourmet');
    $description = get_string('displaymydashboarddesc', 'theme_gourmet');
    $default = true;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Description for mycourse
    $name = 'theme_gourmet/mycoursesinfo';
    $heading = get_string('mycoursesinfo', 'theme_gourmet');
    $information = get_string('mycoursesinfodesc', 'theme_gourmet');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Toggle courses display in custommenu.
    $name = 'theme_gourmet/displaymycourses';
    $title = get_string('displaymycourses', 'theme_gourmet');
    $description = get_string('displaymycoursesdesc', 'theme_gourmet');
    $default = true;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Set wording for dropdown course list
	$name = 'theme_gourmet/mycoursetitle';
	$title = get_string('mycoursetitle','theme_gourmet');
	$description = get_string('mycoursetitledesc', 'theme_gourmet');
	$default = 'course';
	$choices = array(
		'course' => get_string('mycourses', 'theme_gourmet'),
		'lesson' => get_string('mylessons', 'theme_gourmet'),
		'class' => get_string('myclasses', 'theme_gourmet'),
		'module' => get_string('mymodules', 'theme_gourmet'),
		'Unit' => get_string('myunits', 'theme_gourmet'),
	);
	$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
	$setting->set_updatedcallback('theme_reset_all_caches');
	$temp->add($setting);
    
    $ADMIN->add('theme_gourmet', $temp);
    
    
    /* Footer Blocks Settings */
	$temp = new admin_settingpage('theme_gourmet_footerblocks', get_string('footerblocksheading', 'theme_gourmet'));
	$temp->add(new admin_setting_heading('theme_gourmet_footerblocks', get_string('footerblocksheadingsub', 'theme_gourmet'),
            format_text(get_string('footerblocksdesc' , 'theme_gourmet'), FORMAT_MARKDOWN)));
            
	
	//Enable Footer Blocks.
    $name = 'theme_gourmet/enablefooterblocks';
    $title = get_string('enablefooterblocks', 'theme_gourmet');
    $description = get_string('enablefooterblocksdesc', 'theme_gourmet');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
	
	//Footer Block One.
	$name = 'theme_gourmet/footerblock1';
    $title = get_string('footerblock1', 'theme_gourmet');
    $description = get_string('footerblock1desc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Footer Block Two.
	$name = 'theme_gourmet/footerblock2';
    $title = get_string('footerblock2', 'theme_gourmet');
    $description = get_string('footerblock2desc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Footer Block Three.
	$name = 'theme_gourmet/footerblock3';
    $title = get_string('footerblock3', 'theme_gourmet');
    $description = get_string('footerblock3desc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
        
    $ADMIN->add('theme_gourmet', $temp);
    
    
    /* Frontpage Alerts */
    $temp = new admin_settingpage('theme_gourmet_alerts', get_string('alertsheading', 'theme_gourmet'));
	$temp->add(new admin_setting_heading('theme_gourmet_alerts', get_string('alertsheadingsub', 'theme_gourmet'),
            format_text(get_string('alertsdesc' , 'theme_gourmet'), FORMAT_MARKDOWN)));
    
    /* Alert 1 */
    
    // Description for Alert One
    $name = 'theme_gourmet/alert1info';
    $heading = get_string('alert1', 'theme_gourmet');
    $information = get_string('alert1desc', 'theme_gourmet');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Enable Alert One
    $name = 'theme_gourmet/enable1alert';
    $title = get_string('enablealert', 'theme_gourmet');
    $description = get_string('enablealertdesc', 'theme_gourmet');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert One Type.
    $name = 'theme_gourmet/alert1type';
    $title = get_string('alerttype' , 'theme_gourmet');
    $description = get_string('alerttypedesc', 'theme_gourmet');
    $alert_info = get_string('alert_info', 'theme_gourmet');
    $alert_warning = get_string('alert_warning', 'theme_gourmet');
    $alert_general = get_string('alert_general', 'theme_gourmet');
    $default = 'info';
    $choices = array('info'=>$alert_info, 'error'=>$alert_warning, 'success'=>$alert_general);
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert One Title.
    $name = 'theme_gourmet/alert1title';
    $title = get_string('alerttitle', 'theme_gourmet');
    $description = get_string('alerttitledesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert One Text.
    $name = 'theme_gourmet/alert1text';
    $title = get_string('alerttext', 'theme_gourmet');
    $description = get_string('alerttextdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Alert 2 */
    
    // Description for Alert Two
    $name = 'theme_gourmet/alert2info';
    $heading = get_string('alert2', 'theme_gourmet');
    $information = get_string('alert2desc', 'theme_gourmet');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Enable Alert Two
    $name = 'theme_gourmet/enable2alert';
    $title = get_string('enablealert', 'theme_gourmet');
    $description = get_string('enablealertdesc', 'theme_gourmet');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert Two Type.
    $name = 'theme_gourmet/alert2type';
    $title = get_string('alerttype' , 'theme_gourmet');
    $description = get_string('alerttypedesc', 'theme_gourmet');
    $alert_info = get_string('alert_info', 'theme_gourmet');
    $alert_warning = get_string('alert_warning', 'theme_gourmet');
    $alert_general = get_string('alert_general', 'theme_gourmet');
    $default = 'info';
    $choices = array('info'=>$alert_info, 'error'=>$alert_warning, 'success'=>$alert_general);
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert Two Title.
    $name = 'theme_gourmet/alert2title';
    $title = get_string('alerttitle', 'theme_gourmet');
    $description = get_string('alerttitledesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert Two Text.
    $name = 'theme_gourmet/alert2text';
    $title = get_string('alerttext', 'theme_gourmet');
    $description = get_string('alerttextdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Alert 3 */
    
    // Description for Alert Three.
    $name = 'theme_gourmet/alert3info';
    $heading = get_string('alert3', 'theme_gourmet');
    $information = get_string('alert3desc', 'theme_gourmet');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Enable Alert Three.
    $name = 'theme_gourmet/enable3alert';
    $title = get_string('enablealert', 'theme_gourmet');
    $description = get_string('enablealertdesc', 'theme_gourmet');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert Three Type.
    $name = 'theme_gourmet/alert3type';
    $title = get_string('alerttype' , 'theme_gourmet');
    $description = get_string('alerttypedesc', 'theme_gourmet');
    $alert_info = get_string('alert_info', 'theme_gourmet');
    $alert_warning = get_string('alert_warning', 'theme_gourmet');
    $alert_general = get_string('alert_general', 'theme_gourmet');
    $default = 'info';
    $choices = array('info'=>$alert_info, 'error'=>$alert_warning, 'success'=>$alert_general);
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert Three Title.
    $name = 'theme_gourmet/alert3title';
    $title = get_string('alerttitle', 'theme_gourmet');
    $description = get_string('alerttitledesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert Three Text.
    $name = 'theme_gourmet/alert3text';
    $title = get_string('alerttext', 'theme_gourmet');
    $description = get_string('alerttextdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
            
    
    $ADMIN->add('theme_gourmet', $temp);
 
 
    /* Slideshow Widget Settings */
    $temp = new admin_settingpage('theme_gourmet_slideshow', get_string('slideshowheading', 'theme_gourmet'));
    $temp->add(new admin_setting_heading('theme_gourmet_slideshow', get_string('slideshowheadingsub', 'theme_gourmet'),
            format_text(get_string('slideshowdesc' , 'theme_gourmet'), FORMAT_MARKDOWN)));
    
    // Enable Slideshow.    
    $name = 'theme_gourmet/useslideshow';
    $title = get_string('useslideshow', 'theme_gourmet');
    $description = get_string('useslideshowdesc', 'theme_gourmet');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    

    /* Slide 1 */
     
    // Description for Slide One
    $name = 'theme_gourmet/slide1info';
    $heading = get_string('slide1', 'theme_gourmet');
    $information = get_string('slideinfodesc', 'theme_gourmet');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);

    // Title.
    $name = 'theme_gourmet/slide1';
    $title = get_string('slidetitle', 'theme_gourmet');
    $description = get_string('slidetitledesc', 'theme_gourmet');
    $setting = new admin_setting_configtext($name, $title, $description, '');
    $default = '';
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Image.
    $name = 'theme_gourmet/slide1image';
    $title = get_string('slideimage', 'theme_gourmet');
    $description = get_string('slideimagedesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'slide1image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Caption.
    $name = 'theme_gourmet/slide1caption';
    $title = get_string('slidecaption', 'theme_gourmet');
    $description = get_string('slidecaptiondesc', 'theme_gourmet');
    $setting = new admin_setting_configtextarea($name, $title, $description, '');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // URL.
    $name = 'theme_gourmet/slide1url';
    $title = get_string('slideurl', 'theme_gourmet');
    $description = get_string('slideurldesc', 'theme_gourmet');
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    /* Slide 2 */
     
    //Description for Slide Two
    $name = 'theme_gourmet/slide2info';
    $heading = get_string('slide2', 'theme_gourmet');
    $information = get_string('slideinfodesc', 'theme_gourmet');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);

    // Title.
    $name = 'theme_gourmet/slide2';
    $title = get_string('slidetitle', 'theme_gourmet');
    $description = get_string('slidetitledesc', 'theme_gourmet');
    $setting = new admin_setting_configtext($name, $title, $description, '');
    $default = '';
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Image.
    $name = 'theme_gourmet/slide2image';
    $title = get_string('slideimage', 'theme_gourmet');
    $description = get_string('slideimagedesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'slide2image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Caption.
    $name = 'theme_gourmet/slide2caption';
    $title = get_string('slidecaption', 'theme_gourmet');
    $description = get_string('slidecaptiondesc', 'theme_gourmet');
    $setting = new admin_setting_configtextarea($name, $title, $description, '');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // URL.
    $name = 'theme_gourmet/slide2url';
    $title = get_string('slideurl', 'theme_gourmet');
    $description = get_string('slideurldesc', 'theme_gourmet');
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    /* Slide 3 */

    //Description for Slide Three
    $name = 'theme_gourmet/slide3info';
    $heading = get_string('slide3', 'theme_gourmet');
    $information = get_string('slideinfodesc', 'theme_gourmet');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Title.
    $name = 'theme_gourmet/slide3';
    $title = get_string('slidetitle', 'theme_gourmet');
    $description = get_string('slidetitledesc', 'theme_gourmet');
    $setting = new admin_setting_configtext($name, $title, $description, '');
    $default = '';
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Image.
    $name = 'theme_gourmet/slide3image';
    $title = get_string('slideimage', 'theme_gourmet');
    $description = get_string('slideimagedesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'slide3image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Caption.
    $name = 'theme_gourmet/slide3caption';
    $title = get_string('slidecaption', 'theme_gourmet');
    $description = get_string('slidecaptiondesc', 'theme_gourmet');
    $setting = new admin_setting_configtextarea($name, $title, $description, '');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // URL.
    $name = 'theme_gourmet/slide3url';
    $title = get_string('slideurl', 'theme_gourmet');
    $description = get_string('slideurldesc', 'theme_gourmet');
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    /* Slide 4 */
     
    // Description for Slide Four
    $name = 'theme_gourmet/slide4info';
    $heading = get_string('slide4', 'theme_gourmet');
    $information = get_string('slideinfodesc', 'theme_gourmet');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);

    // Title.
    $name = 'theme_gourmet/slide4';
    $title = get_string('slidetitle', 'theme_gourmet');
    $description = get_string('slidetitledesc', 'theme_gourmet');
    $setting = new admin_setting_configtext($name, $title, $description, '');
    $default = '';
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Image.
    $name = 'theme_gourmet/slide4image';
    $title = get_string('slideimage', 'theme_gourmet');
    $description = get_string('slideimagedesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'slide4image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Caption.
    $name = 'theme_gourmet/slide4caption';
    $title = get_string('slidecaption', 'theme_gourmet');
    $description = get_string('slidecaptiondesc', 'theme_gourmet');
    $setting = new admin_setting_configtextarea($name, $title, $description, '');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // URL.
    $name = 'theme_gourmet/slide4url';
    $title = get_string('slideurl', 'theme_gourmet');
    $description = get_string('slideurldesc', 'theme_gourmet');
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $ADMIN->add('theme_gourmet', $temp);
    
    /* Frontpage Promo Box */
    $temp = new admin_settingpage('theme_gourmet_promo', get_string('promoheading', 'theme_gourmet'));
	$temp->add(new admin_setting_heading('theme_gourmet_promo', get_string('promosub', 'theme_gourmet'),
            format_text(get_string('promodesc' , 'theme_gourmet'), FORMAT_MARKDOWN)));
    
    // Enable Promo Box
    $name = 'theme_gourmet/usepromo';
    $title = get_string('usepromo', 'theme_gourmet');
    $description = get_string('usepromodesc', 'theme_gourmet');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Frontpage Promo Box Title
    $name = 'theme_gourmet/promotitle';
    $title = get_string('promotitle', 'theme_gourmet');
    $description = get_string('promotitledesc', 'theme_gourmet');
    $setting = new admin_setting_configtext($name, $title, $description, '');
    $default = '';
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Frontpage Promo Copy
    $name = 'theme_gourmet/promocopy';
    $title = get_string('promocopy', 'theme_gourmet');
    $description = get_string('promocopydesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
        
    //Frontpage Promo Box CTA button text
    $name = 'theme_gourmet/promocta';
    $title = get_string('promocta', 'theme_gourmet');
    $description = get_string('promoctadesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Frontpage Promo Box CTA button URL
    $name = 'theme_gourmet/promoctaurl';
    $title = get_string('promoctaurl', 'theme_gourmet');
    $description = get_string('promoctaurldesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

        
    $ADMIN->add('theme_gourmet', $temp);
    

	/* Home Blocks Settings */	
	$temp = new admin_settingpage('theme_gourmet_homeblocks', get_string('homeblocksheading', 'theme_gourmet'));
	$temp->add(new admin_setting_heading('theme_gourmet_homeblocks', get_string('homeblocksheadingsub', 'theme_gourmet'),
            format_text(get_string('homeblocksdesc' , 'theme_gourmet'), FORMAT_MARKDOWN)));
            
	
	//Enable Home Blocks.
    $name = 'theme_gourmet/usehomeblocks';
    $title = get_string('usehomeblocks', 'theme_gourmet');
    $description = get_string('usehomeblocksdesc', 'theme_gourmet');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
     // Block Item Min Height
	$name = 'theme_gourmet/homeblockminheight';
	$title = get_string('homeblockminheight','theme_gourmet');
	$description = get_string('homeblockminheightdesc', 'theme_gourmet');
	$default = '360';
	$setting = new admin_setting_configtext($name, $title, $description, $default);
	$setting->set_updatedcallback('theme_reset_all_caches');
	$temp->add($setting);
	
	/* Home Block 1 */	
	$name = 'theme_gourmet/homeblock1';
    $title = get_string('homeblocktitle', 'theme_gourmet');
    $description = get_string('homeblocktitledesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_gourmet/homeblock1image';
    $title = get_string('homeblockimage', 'theme_gourmet');
    $description = get_string('homeblockimagedesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'homeblock1image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_gourmet/homeblock1content';
    $title = get_string('homeblockcontent', 'theme_gourmet');
    $description = get_string('homeblockcontentdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_gourmet/homeblock1buttontext';
    $title = get_string('homeblockbuttontext', 'theme_gourmet');
    $description = get_string('homeblockbuttontextdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_gourmet/homeblock1buttonurl';
    $title = get_string('homeblockbuttonurl', 'theme_gourmet');
    $description = get_string('homeblockbuttonurldesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Home Block 2 */    
	$name = 'theme_gourmet/homeblock2';
    $title = get_string('homeblocktitle', 'theme_gourmet');
    $description = get_string('homeblocktitledesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_gourmet/homeblock2image';
    $title = get_string('homeblockimage', 'theme_gourmet');
    $description = get_string('homeblockimagedesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'homeblock2image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_gourmet/homeblock2content';
    $title = get_string('homeblockcontent', 'theme_gourmet');
    $description = get_string('homeblockcontentdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_gourmet/homeblock2buttontext';
    $title = get_string('homeblockbuttontext', 'theme_gourmet');
    $description = get_string('homeblockbuttontextdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_gourmet/homeblock2buttonurl';
    $title = get_string('homeblockbuttonurl', 'theme_gourmet');
    $description = get_string('homeblockbuttonurldesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Home Block 3 */    
	$name = 'theme_gourmet/homeblock3';
    $title = get_string('homeblocktitle', 'theme_gourmet');
    $description = get_string('homeblocktitledesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_gourmet/homeblock3image';
    $title = get_string('homeblockimage', 'theme_gourmet');
    $description = get_string('homeblockimagedesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'homeblock3image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_gourmet/homeblock3content';
    $title = get_string('homeblockcontent', 'theme_gourmet');
    $description = get_string('homeblockcontentdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_gourmet/homeblock3buttontext';
    $title = get_string('homeblockbuttontext', 'theme_gourmet');
    $description = get_string('homeblockbuttontextdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_gourmet/homeblock3buttonurl';
    $title = get_string('homeblockbuttonurl', 'theme_gourmet');
    $description = get_string('homeblockbuttonurldesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    $ADMIN->add('theme_gourmet', $temp);
    

    /* Frontpage Awards */
    $temp = new admin_settingpage('theme_gourmet_awards', get_string('awardsheading', 'theme_gourmet'));
	$temp->add(new admin_setting_heading('theme_gourmet_awards', get_string('awardssub', 'theme_gourmet'),
            format_text(get_string('awardsdesc' , 'theme_gourmet'), FORMAT_MARKDOWN)));
    
    // Enable Awards Section
    $name = 'theme_gourmet/useawards';
    $title = get_string('useawards', 'theme_gourmet');
    $description = get_string('useawardsdesc', 'theme_gourmet');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Award 1 */
    
    // Description for Award One
    $name = 'theme_gourmet/award1info';
    $heading = get_string('award1', 'theme_gourmet');
    $information = get_string('award1desc', 'theme_gourmet');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Award Image 
    $name = 'theme_gourmet/award1image';
    $title = get_string('awardimage', 'theme_gourmet');
    $description = get_string('awardimagedesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'award1image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Award Image Alt Text
    $name = 'theme_gourmet/award1alttext';
    $title = get_string('awardalttext', 'theme_gourmet');
    $description = get_string('awardalttextdesc', 'theme_gourmet');
    $default = 'award';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    

    // Award URL
    $name = 'theme_gourmet/award1url';
    $title = get_string('awardurl', 'theme_gourmet');
    $description = get_string('awardurldesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    
    /* Award 2 */
    
    // Description for Award Two
    $name = 'theme_gourmet/award2info';
    $heading = get_string('award2', 'theme_gourmet');
    $information = get_string('award2desc', 'theme_gourmet');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Award Image 
    $name = 'theme_gourmet/award2image';
    $title = get_string('awardimage', 'theme_gourmet');
    $description = get_string('awardimagedesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'award2image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Award Image Alt Text
    $name = 'theme_gourmet/award2alttext';
    $title = get_string('awardalttext', 'theme_gourmet');
    $description = get_string('awardalttextdesc', 'theme_gourmet');
    $default = 'award';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Award URL
    $name = 'theme_gourmet/award2url';
    $title = get_string('awardurl', 'theme_gourmet');
    $description = get_string('awardurldesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    /* Award 3 */
    
    // Description for Award Three
    $name = 'theme_gourmet/award3info';
    $heading = get_string('award3', 'theme_gourmet');
    $information = get_string('award3desc', 'theme_gourmet');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Award Image 
    $name = 'theme_gourmet/award3image';
    $title = get_string('awardimage', 'theme_gourmet');
    $description = get_string('awardimagedesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'award3image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Award Image Alt Text
    $name = 'theme_gourmet/award3alttext';
    $title = get_string('awardalttext', 'theme_gourmet');
    $description = get_string('awardalttextdesc', 'theme_gourmet');
    $default = 'award';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Award URL
    $name = 'theme_gourmet/award3url';
    $title = get_string('awardurl', 'theme_gourmet');
    $description = get_string('awardurldesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Award 4 */
    
    // Description for Award Four
    $name = 'theme_gourmet/award4info';
    $heading = get_string('award4', 'theme_gourmet');
    $information = get_string('award4desc', 'theme_gourmet');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Award Image 
    $name = 'theme_gourmet/award4image';
    $title = get_string('awardimage', 'theme_gourmet');
    $description = get_string('awardimagedesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'award4image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Award Image Alt Text
    $name = 'theme_gourmet/award4alttext';
    $title = get_string('awardalttext', 'theme_gourmet');
    $description = get_string('awardalttextdesc', 'theme_gourmet');
    $default = 'award';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Award URL
    $name = 'theme_gourmet/award4url';
    $title = get_string('awardurl', 'theme_gourmet');
    $description = get_string('awardurldesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Award 5 */
    
    // Description for Award Five
    $name = 'theme_gourmet/award5info';
    $heading = get_string('award5', 'theme_gourmet');
    $information = get_string('award5desc', 'theme_gourmet');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Award Image 
    $name = 'theme_gourmet/award5image';
    $title = get_string('awardimage', 'theme_gourmet');
    $description = get_string('awardimagedesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'award5image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Award Image Alt Text
    $name = 'theme_gourmet/award5alttext';
    $title = get_string('awardalttext', 'theme_gourmet');
    $description = get_string('awardalttextdesc', 'theme_gourmet');
    $default = 'award';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Award URL
    $name = 'theme_gourmet/award5url';
    $title = get_string('awardurl', 'theme_gourmet');
    $description = get_string('awardurldesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Award 6 */
    
    // Description for Award Six
    $name = 'theme_gourmet/award6info';
    $heading = get_string('award6', 'theme_gourmet');
    $information = get_string('award6desc', 'theme_gourmet');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Award Image 
    $name = 'theme_gourmet/award6image';
    $title = get_string('awardimage', 'theme_gourmet');
    $description = get_string('awardimagedesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'award6image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Award Image Alt Text
    $name = 'theme_gourmet/award6alttext';
    $title = get_string('awardalttext', 'theme_gourmet');
    $description = get_string('awardalttextdesc', 'theme_gourmet');
    $default = 'award';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Award URL
    $name = 'theme_gourmet/award6url';
    $title = get_string('awardurl', 'theme_gourmet');
    $description = get_string('awardurldesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
       
    $ADMIN->add('theme_gourmet', $temp);
    
    /* Frontpage Testimonials */
    $temp = new admin_settingpage('theme_gourmet_testimonials', get_string('testimonialsheading', 'theme_gourmet'));
	$temp->add(new admin_setting_heading('theme_gourmet_testimonials', get_string('testimonialssub', 'theme_gourmet'),
            format_text(get_string('testimonialsdesc' , 'theme_gourmet'), FORMAT_MARKDOWN)));
    
    // Enable Testimonails section
    $name = 'theme_gourmet/usetestimonials';
    $title = get_string('usetestimonials', 'theme_gourmet');
    $description = get_string('usetestimonialsdesc', 'theme_gourmet');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Testimonial 1 */
    
    // Description for Testimonial One
    $name = 'theme_gourmet/testimonial1info';
    $heading = get_string('testimonial1', 'theme_gourmet');
    $information = get_string('testimonial1desc', 'theme_gourmet');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Testimonial Image 
    $name = 'theme_gourmet/testimonial1image';
    $title = get_string('testimonialimage', 'theme_gourmet');
    $description = get_string('testimonialimagedesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'testimonial1image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Name
    $name = 'theme_gourmet/testimonial1name';
    $title = get_string('testimonialname', 'theme_gourmet');
    $description = get_string('testimonialnamedesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Title
    $name = 'theme_gourmet/testimonial1title';
    $title = get_string('testimonialtitle', 'theme_gourmet');
    $description = get_string('testimonialtitledesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Content 
    $name = 'theme_gourmet/testimonial1content';
    $title = get_string('testimonialcontent', 'theme_gourmet');
    $description = get_string('testimonialcontentdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Testimonial 2 */
    
    // Description for Testimonial Two
    $name = 'theme_gourmet/testimonial2info';
    $heading = get_string('testimonial2', 'theme_gourmet');
    $information = get_string('testimonial2desc', 'theme_gourmet');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Testimonial Image 
    $name = 'theme_gourmet/testimonial2image';
    $title = get_string('testimonialimage', 'theme_gourmet');
    $description = get_string('testimonialimagedesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'testimonial2image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Name
    $name = 'theme_gourmet/testimonial2name';
    $title = get_string('testimonialname', 'theme_gourmet');
    $description = get_string('testimonialnamedesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Title
    $name = 'theme_gourmet/testimonial2title';
    $title = get_string('testimonialtitle', 'theme_gourmet');
    $description = get_string('testimonialtitledesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Content 
    $name = 'theme_gourmet/testimonial2content';
    $title = get_string('testimonialcontent', 'theme_gourmet');
    $description = get_string('testimonialcontentdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Testimonial 3 */
    
    // Description for Testimonial Three
    $name = 'theme_gourmet/testimonial3info';
    $heading = get_string('testimonial3', 'theme_gourmet');
    $information = get_string('testimonial3desc', 'theme_gourmet');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Testimonial Image 
    $name = 'theme_gourmet/testimonial3image';
    $title = get_string('testimonialimage', 'theme_gourmet');
    $description = get_string('testimonialimagedesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'testimonial3image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Name
    $name = 'theme_gourmet/testimonial3name';
    $title = get_string('testimonialname', 'theme_gourmet');
    $description = get_string('testimonialnamedesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Title
    $name = 'theme_gourmet/testimonial3title';
    $title = get_string('testimonialtitle', 'theme_gourmet');
    $description = get_string('testimonialtitledesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Content 
    $name = 'theme_gourmet/testimonial3content';
    $title = get_string('testimonialcontent', 'theme_gourmet');
    $description = get_string('testimonialcontentdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Testimonial 4 */
    
    // Description for Testimonial Four
    $name = 'theme_gourmet/testimonial4info';
    $heading = get_string('testimonial4', 'theme_gourmet');
    $information = get_string('testimonial4desc', 'theme_gourmet');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Testimonial Image 
    $name = 'theme_gourmet/testimonial4image';
    $title = get_string('testimonialimage', 'theme_gourmet');
    $description = get_string('testimonialimagedesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'testimonial4image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Name
    $name = 'theme_gourmet/testimonial4name';
    $title = get_string('testimonialname', 'theme_gourmet');
    $description = get_string('testimonialnamedesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Title
    $name = 'theme_gourmet/testimonial4title';
    $title = get_string('testimonialtitle', 'theme_gourmet');
    $description = get_string('testimonialtitledesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Content 
    $name = 'theme_gourmet/testimonial4content';
    $title = get_string('testimonialcontent', 'theme_gourmet');
    $description = get_string('testimonialcontentdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('theme_gourmet', $temp);
    

	/* Social Network Settings */
	$temp = new admin_settingpage('theme_gourmet_social', get_string('socialheading', 'theme_gourmet'));
	$temp->add(new admin_setting_heading('theme_gourmet_social', get_string('socialheadingsub', 'theme_gourmet'),
            format_text(get_string('socialdesc' , 'theme_gourmet'), FORMAT_MARKDOWN)));
            
    // Enable social media in the topbar
    $name = 'theme_gourmet/enabletopbarsocial';
    $title = get_string('enabletopbarsocial', 'theme_gourmet');
    $description = get_string('enabletopbarsocialdesc', 'theme_gourmet');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Enable social media in the bottombar
    $name = 'theme_gourmet/enablebottombarsocial';
    $title = get_string('enablebottombarsocial', 'theme_gourmet');
    $description = get_string('enablebottombarsocialdesc', 'theme_gourmet');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
            
    // Twitter url setting.
    $name = 'theme_gourmet/twitter';
    $title = get_string('twitter', 'theme_gourmet');
    $description = get_string('twitterdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Facebook url setting.
    $name = 'theme_gourmet/facebook';
    $title = get_string('facebook', 'theme_gourmet');
    $description = get_string('facebookdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // YouTube url setting.
    $name = 'theme_gourmet/youtube';
    $title = get_string('youtube', 'theme_gourmet');
    $description = get_string('youtubedesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // LinkedIn url setting.
    $name = 'theme_gourmet/linkedin';
    $title = get_string('linkedin', 'theme_gourmet');
    $description = get_string('linkedindesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Flickr url setting.
    $name = 'theme_gourmet/flickr';
    $title = get_string('flickr', 'theme_gourmet');
    $description = get_string('flickrdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Google+ url setting.
    $name = 'theme_gourmet/googleplus';
    $title = get_string('googleplus', 'theme_gourmet');
    $description = get_string('googleplusdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Pinterest url setting.
    $name = 'theme_gourmet/pinterest';
    $title = get_string('pinterest', 'theme_gourmet');
    $description = get_string('pinterestdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Instagram url setting.
    $name = 'theme_gourmet/instagram';
    $title = get_string('instagram', 'theme_gourmet');
    $description = get_string('instagramdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Skype account setting.
    $name = 'theme_gourmet/skype';
    $title = get_string('skype', 'theme_gourmet');
    $description = get_string('skypedesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // RSS url setting.
    $name = 'theme_gourmet/rss';
    $title = get_string('rss', 'theme_gourmet');
    $description = get_string('rssdesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    //Description for iOS Icons
    $name = 'theme_gourmet/iosiconinfo';
    $heading = get_string('iosicon', 'theme_gourmet');
    $information = get_string('iosicondesc', 'theme_gourmet');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // iPhone Icon.
    $name = 'theme_gourmet/iphoneicon';
    $title = get_string('iphoneicon', 'theme_gourmet');
    $description = get_string('iphoneicondesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'iphoneicon');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // iPhone Retina Icon.
    $name = 'theme_gourmet/iphoneretinaicon';
    $title = get_string('iphoneretinaicon', 'theme_gourmet');
    $description = get_string('iphoneretinaicondesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'iphoneretinaicon');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // iPad Icon.
    $name = 'theme_gourmet/ipadicon';
    $title = get_string('ipadicon', 'theme_gourmet');
    $description = get_string('ipadicondesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'ipadicon');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // iPad Retina Icon.
    $name = 'theme_gourmet/ipadretinaicon';
    $title = get_string('ipadretinaicon', 'theme_gourmet');
    $description = get_string('ipadretinaicondesc', 'theme_gourmet');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'ipadretinaicon');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('theme_gourmet', $temp);
       
    
    /* Analytics Settings */
    $temp = new admin_settingpage('theme_gourmet_analytics', get_string('analyticsheading', 'theme_gourmet'));
	$temp->add(new admin_setting_heading('theme_gourmet_analytics', get_string('analyticsheadingsub', 'theme_gourmet'),
            format_text(get_string('analyticsdesc' , 'theme_gourmet'), FORMAT_MARKDOWN)));
    
    // Enable Analytics
    $name = 'theme_gourmet/useanalytics';
    $title = get_string('useanalytics', 'theme_gourmet');
    $description = get_string('useanalyticsdesc', 'theme_gourmet');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Google Analytics ID
    $name = 'theme_gourmet/analyticsid';
    $title = get_string('analyticsid', 'theme_gourmet');
    $description = get_string('analyticsiddesc', 'theme_gourmet');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
        
    $ADMIN->add('theme_gourmet', $temp);
