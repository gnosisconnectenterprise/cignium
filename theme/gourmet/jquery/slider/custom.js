var slideLength;
var current_page_nomber=1;
var slidewidth;

$(document).ready(function(){
	slideLength = $("#scroller > div.scroller_div").length;
	slidewidth = $("#scroller > div.scroller_div").outerWidth(true);
	alert(slideLength);
	alert(slidewidth);
	$("#scroller").width(parseInt(slideLength)*parseInt(slidewidth));
	
	// Next button click to move to next slide
	$(document).on("click",".previousPage",function(){
		if(current_page_nomber > 1){
			current_page_nomber--;
			moveToPage(current_page_nomber); // function call to move to the slide nubmer passed as argument
		}
	});
	
	// Previous button click to move to previous slide
	$(document).on("click",".NextPage",function(){
		if(current_page_nomber < slideLength){
			current_page_nomber++;
			moveToPage(current_page_nomber); // function call to move to the slide nubmer passed as argument
		}
	});	
	
	
});

// funciton to move to Next or Previous slide
function moveToPage(movetopagecounter){
	
	if( movetopagecounter == 1){
		$(".previousPage").addClass("disable");
		$(".NextPage").removeClass("disable");
	} else if( movetopagecounter == slideLength){
		$(".NextPage").addClass("disable");
		$(".previousPage").removeClass("disable");
	} else{
		$(".previousPage").removeClass("disable");
		$(".NextPage").removeClass("disable");
	}
	var leftPos = -(parseInt(movetopagecounter-1) * parseInt(slidewidth)) + "px";
	$("#scroller").stop(true,true).animate({left:leftPos});
	
}


