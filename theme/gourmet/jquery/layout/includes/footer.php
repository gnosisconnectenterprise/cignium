<?php
$hascopyright = (empty($PAGE->theme->settings->copyright)) ? false : $PAGE->theme->settings->copyright;
$hasfooterwidget = (empty($PAGE->theme->settings->footerwidget)) ? false : $PAGE->theme->settings->footerwidget;

/* Footer blocks settings */
$hasfooterblocks = (empty($PAGE->theme->settings->enablefooterblocks)) ? false : $PAGE->theme->settings->enablefooterblocks;
$hasfooterblock1 = (empty($PAGE->theme->settings->footerblock1)) ? false : $PAGE->theme->settings->footerblock1;
$hasfooterblock2 = (empty($PAGE->theme->settings->footerblock2)) ? false : $PAGE->theme->settings->footerblock2;
$hasfooterblock3 = (empty($PAGE->theme->settings->footerblock3)) ? false : $PAGE->theme->settings->footerblock3;

$footerblock1 = $PAGE->theme->settings->footerblock1;
$footerblock2 = $PAGE->theme->settings->footerblock2;
$footerblock3 = $PAGE->theme->settings->footerblock3;

$footerwidget = $PAGE->theme->settings->footerwidget;

?>
	<div class="footer-content"> 
        <div class="container">
            <?php if ($hasfooterblocks) {?>
            <div class="row footerblocks">                
                <?php if ($hasfooterblock1) {?>
                <div class="footer-col col-md-3 col-sm-12">
                    <div class="footer-col-inner">
                        <?php echo $footerblock1 ?>
                    </div><!--//footer-col-inner-->
                </div><!--//foooter-col-->
                <?php }?>
                <?php if ($hasfooterblock2) {?>
                <div class="footer-col col-md-6 col-sm-12">
                    <div class="footer-col-inner">
                        <?php echo $footerblock2 ?>
                    </div><!--//footer-col-inner-->
                </div><!--//foooter-col--> 
                <?php }?>
                <?php if ($hasfooterblock3) {?>
                <div class="footer-col col-md-3 col-sm-12">
                    <div class="footer-col-inner">
                        <?php echo $footerblock3 ?>
                    </div><!--//footer-col-inner-->            
                </div><!--//foooter-col-->  
            <?php }?> 
            </div>  
            <?php }?>  
            <!--<p class="helplink"><?php //echo page_doc_link(get_string('moodledocslink')); ?></p>-->
            <?php if ($hasfooterwidget) {?>
            <div class="footerwidget"><?php echo $footerwidget ?></div>
            <?php }?>  
            
        </div>    
	      
    </div>
	<div class="bottom-bar">
        <div class="container">
            <div class="row">
                <?php if ($hassocialnetworks && $PAGE->theme->settings->enablebottombarsocial) { ?>
                <ul class="social pull-right col-md-6 col-sm-12 col-xs-12">
                    <?php if ($hastwitter) { ?>
                    <li><a href="<?php echo $twitter ?>"><i class="fa fa-twitter"></i></a></li>
                    <?php } ?>
                    <?php if ($hasfacebook) { ?>
                    <li><a href="<?php echo $facebook ?>"><i class="fa fa-facebook"></i></a></li>
                    <?php } ?>
                    <?php if ($hasgoogleplus) { ?>
                    <li><a href="<?php echo $googleplus ?>"><i class="fa fa-google-plus"></i></a></li> 
                    <?php } ?>
                    <?php if ($haslinkedin) { ?>
                    <li><a href="<?php echo $linkedin ?>"><i class="fa fa-linkedin"></i></a></li>
                    <?php } ?>
                    <?php if ($hasskype) { ?>
                    <li><a href="<?php echo 'skype:'.$skype.'?call' ?>"><i class="fa fa-skype"></i></a></li>
                    <?php } ?>
                    <?php if ($hasyoutube) { ?>
                    <li><a href="<?php echo $youtube ?>"><i class="fa fa-youtube"></i></a></li>
                    <?php } ?>
                    <?php if ($hasflickr) { ?>
                    <li><a href="<?php echo $flickr ?>"><i class="fa fa-flickr"></i></a></li>
                    <?php } ?>
                    <?php if ($hasinstagram) { ?>
                    <li><a href="<?php echo $instagram ?>"><i class="fa fa-instagram"></i></a></li>
                    <?php } ?>
                    <?php if ($haspinterest) { ?>
                    <li><a href="<?php echo $pinterest ?>"><i class="fa fa-youtube"></i></a></li>
                    <?php } ?>
                    
                    <?php if ($hasrss) { ?>   
                    <li class="row-end"><a href="<?php echo $rss ?>"><i class="fa fa-rss"></i></a></li>   
                    <?php } ?> 
                </ul><!--//social-->
                <?php } ?>
                
                <?php 
				$CMSPages = getCMSPages(); 
				if(count($CMSPages) > 0){
				?>
                    <div class="footer-menu">
                        <ul>
                        <?php foreach($CMSPages as $pages){?>
                            <li><a href="<?php echo $CFG->wwwroot;?>/cms/cms_content.php?pageid=<?php echo $pages->id;?>" title="<?php echo $pages->page_name;?>"><?php echo $pages->page_name;?></a></li>
                        <?php } ?>    
                        </ul>
                    </div>
                <?php } ?>                
                
                 <div class="info col-md-6 col-sm-6 col-xs-12" style="padding:0">           
            <?php if($hasheaderwidget) {?>
            <div class="footer-widget f-left">
                <?php echo $headerwidget ?>
            </div>
            <?php }?>
            <?php if ($hasheaderphone || $hasheaderemail) {?>
            <div class="contact pull-left">
                <?php if ($hasheaderphone) {?>
                <p class="phone"><i class="fa fa-phone"></i><?php echo $headerphone ?></p>
                <?php }?>
                <?php if ($hasheaderemail) {?>
                <p class="email"><i class="fa fa-envelope"></i><a href="mailto:<?php echo $headeremail ?>"><?php echo $headeremail ?></a></p>
                <?php } ?>
            </div>
            <?php } ?>
        </div>           
                
				<?php if ($hascopyright) {
                //echo '<small class="copyright col-md-6 col-sm-12 col-xs-12"><p class="copy">&copy; '.date("Y").' '.$hascopyright.'</p></small>';
                } ?>
                
            </div><!--//row-->
        </div><!--//container-->
    </div>