<?php 

/**
		* Custom Module - Custom breadcrumb page
		* Date Creation - 04/05/2014
		* Date Modification : 23/06/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
	
?>
<ul class="nav" >
  <?php if($customMenu == ''){?>
  <!-- <li><a href="<?php echo $CFG->wwwroot;?>" title="<?php echo get_string('home','menubar');?>"><?php echo get_string('home','menubar');?></a></li>-->
  <?php } ?>
  <?php if(/*$userrole == 1*/$USER->archetype == 'manager'){ // globaladmin role ?>
  <li class="dropdown"><a data-toggle="dropdown" class="dropdown-toggle my-dashboard" href="<?php echo $CFG->wwwroot;?>" title="<?php echo get_string('mydashboard','menubar');?>"><?php echo get_string('mydashboard','menubar');?><b class="caret"></b></a>
    <!--<ul class="dropdown-menu">
      <li><a href="<?php echo $CFG->wwwroot;?>" title="<?php echo get_string('dashboard','menubar');?>"><em><i class="fa fa-dashboard"></i><?php echo get_string('dashboard','menubar');?></em></a></li>
      <li><a href="<?php echo $CFG->wwwroot;?>/calendar/view.php?view=month" title="<?php echo get_string('calendar','menubar');?>"><em><i class="fa fa-calendar"></i><?php echo get_string('calendar','menubar');?></em></a></li>
      <li><a href="<?php echo $CFG->wwwroot;?>/blog/index.php" title="<?php echo get_string('blogs','menubar');?>"><em><i class="fa fa-file"></i><?php echo get_string('blogs','menubar');?></em></a></li>
       <li><a href="<?php echo $CFG->wwwroot;?>/event/view.php" title="<?php echo get_string('events','menubar');?>"><em><i class="fa fa-file"></i><?php echo get_string('events','menubar');?></em></a></li>
      <li><a href="<?php //echo $CFG->wwwroot;?>/badges/mybadges.php" title="<?php //echo get_string('badges','menubar');?>"><em><i class="fa fa-certificate"></i><?php //echo get_string('badges','menubar');?></em></a></li>
      <li><a href="<?php echo $CFG->wwwroot;?>/user/files.php" title="<?php echo get_string('filemanagement','menubar');?>"><em><i class="fa fa-file"></i><?php echo get_string('filemanagement','menubar');?></em></a></li>
      <li><a href="<?php echo $CFG->wwwroot;?>/login/logout.php" title="<?php echo get_string('logout','menubar');?>"><em><i class="fa fa-sign-out"></i><?php echo get_string('logout','menubar');?></em></a></li>
    </ul>-->
  </li>
  <li class="dropdown"><a title="<?php echo get_string('manage','menubar');?>" data-toggle="dropdown" class="dropdown-toggle my-learning-page" href="#cm_submenu_10"><?php echo get_string('manage','menubar');?><b class="caret"></b></a>
    <ul class="dropdown-menu">
      <li><a href="<?php echo $CFG->wwwroot;?>/course/index.php" title="<?php echo get_string('courses','menubar');?>"><?php echo get_string('courses','menubar');?></a></li>
	  <li><a href="<?php echo $CFG->wwwroot;?>/admin/user.php" title="<?php echo get_string('users','menubar');?>"><?php echo get_string('users','menubar');?></a></li>
     <li><a href="<?php echo $CFG->wwwroot;?>/department/index.php" title="<?php echo get_string('managedepartments','menubar');?>"><?php echo get_string('managedepartments','menubar');?></a></li>
	 <li><a href="<?php echo $CFG->wwwroot;?>/group/managegroups.php" title="<?php echo get_string('managegroups','menubar');?>"><?php echo get_string('managegroups','menubar');?></a></li>
	 <li><a href="<?php echo $CFG->wwwroot;?>/calendar/view.php" title="<?php echo get_string('events','menubar');?>"><?php echo get_string('events','menubar');?></a></li>
     <li><a href="<?php echo $CFG->wwwroot;?>/messages/index.php" title="<?php echo get_string('managemessages','menubar');?>"><?php echo get_string('managemessages','menubar');?></a></li>
	 <li><a href="<?php echo $CFG->wwwroot;?>/cms/index.php" title="<?php echo get_string('managecms','menubar');?>"><?php echo get_string('managecms','menubar');?></a></li>
	 <!--<li><a href="<?php echo $CFG->wwwroot;?>/badges/managebadges.php" title="<?php echo get_string('badges','menubar');?>"><?php echo get_string('badges','menubar');?></a></li>-->
    </ul>
  </li>
 <!-- <li class="dropdown"><a title="<?php echo get_string('courses','menubar');?>" data-toggle="dropdown" class="dropdown-toggle my-learning-page" href="#cm_submenu_10"><?php echo get_string('courses','menubar');?><b class="caret"></b></a>
    <ul class="dropdown-menu">
      <li><a href="<?php echo $CFG->wwwroot;?>/course/index.php" title="<?php echo get_string('managecourses','menubar');?>"><?php echo get_string('managecourses','menubar');?></a></li>
      <li><a href="<?php echo $CFG->wwwroot;?>/course/edit.php?category=1&returnto=category" title="<?php echo get_string('addnewcourse','menubar');?>"><?php echo get_string('addnewcourse','menubar');?></a></li>
    </ul>
  </li>
  <li class="dropdown"><a title="<?php echo get_string('users','menubar');?>" data-toggle="dropdown" class="dropdown-toggle" href="#cm_submenu_1"><?php echo get_string('users','menubar');?><b class="caret"></b></a>
    <ul class="dropdown-menu">
      <li class="dropdown-submenu"><a title="<?php echo get_string('myprofile','menubar');?>" data-toggle="dropdown" class="dropdown-toggle" href="#cm_submenu_2"><?php echo get_string('myprofile','menubar');?></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo $CFG->wwwroot;?>/user/profile.php" title="<?php echo get_string('viewprofile','menubar');?>"><?php echo get_string('viewprofile','menubar');?></a></li>
          <li><a href="<?php echo $CFG->wwwroot;?>/user/editadvanced.php" title="<?php echo get_string('editprofile','menubar');?>"><?php echo get_string('editprofile','menubar');?></a></li>
          <li><a href="<?php echo $CFG->wwwroot;?>/login/change_password.php" title="<?php echo get_string('changepassword','menubar');?>"><?php echo get_string('changepassword','menubar');?></a></li>
          <li><a href="<?php echo $CFG->wwwroot;?>/message/index.php" title="<?php echo get_string('messages','menubar');?>"><?php echo get_string('messages','menubar');?></a></li>
        </ul>
      </li>
      
      <li class="dropdown-submenu"><a title="<?php echo get_string('accounts','menubar');?>" data-toggle="dropdown" class="dropdown-toggle" href="#cm_submenu_3"><?php echo get_string('accounts','menubar');?></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo $CFG->wwwroot;?>/admin/user.php" title="<?php echo get_string('browselistofusers','menubar');?>"><?php echo get_string('browselistofusers','menubar');?></a></li>
          <li><a href="<?php echo $CFG->wwwroot;?>/admin/user/user_bulk.php" title="<?php echo get_string('bulkuseractions','menubar');?>"><?php echo get_string('bulkuseractions','menubar');?></a></li>
          <li><a href="<?php echo $CFG->wwwroot;?>/user/editadvanced.php?id=-1" title="<?php echo get_string('addanewuser','menubar');?>"><?php echo get_string('addanewuser','menubar');?></a></li>
          <li><a href="<?php echo $CFG->wwwroot;?>/admin/tool/uploaduser/index.php" title="<?php echo get_string('uploadusers','menubar');?>"><?php echo get_string('uploadusers','menubar');?></a></li>
        </ul>
      </li>
      
       <!-- <li class="dropdown"><a class="dropdown-toggle" href="<?php echo $CFG->wwwroot;?>/blog/index.php" title="<?php echo get_string('myblogs','menubar');?>"><?php echo get_string('myblogs','menubar');?><b class="caret"></b></a>
  </li>
  
      <li class="dropdown-submenu"><a title="<?php echo get_string('permissions','menubar');?>" data-toggle="dropdown" class="dropdown-toggle" href="#cm_submenu_4"><?php echo get_string('permissions','menubar');?></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo $CFG->wwwroot;?>/admin/roles/manage.php" title="<?php echo get_string('defineroles','menubar');?>"><?php echo get_string('defineroles','menubar');?></a></li>
          <li><a href="<?php echo $CFG->wwwroot;?>/admin/roles/assign.php?contextid=1" title="<?php echo get_string('assignsystemroles','menubar');?>"><?php echo get_string('assignsystemroles','menubar');?></a></li>
        </ul>
      </li>
      
      <li class="dropdown-submenu"><a title="<?php echo get_string('groups','menubar');?>" data-toggle="dropdown" class="dropdown-toggle" href="#cm_submenu_4"><?php echo get_string('groups','menubar');?></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo $CFG->wwwroot;?>/group/managegroups.php" title="<?php echo get_string('managegroups','menubar');?>"><?php echo get_string('managegroups','menubar');?></a></li>
          <li><a href="<?php echo $CFG->wwwroot;?>/group/addgroup.php" title="<?php echo get_string('addgroup','menubar');?>"><?php echo get_string('addgroup','menubar');?></a></li>
        </ul>
      </li>
    </ul>
  </li> -->
  <li class="dropdown"><a title="<?php echo get_string('reports','menubar');?>" data-toggle="dropdown" class="dropdown-toggle my-reports" href="#cm_submenu_5"><?php echo get_string('reports','menubar');?><!-- b class="caret"></b --></a>
    <ul class="dropdown-menu">
	</ul>
  </li>
  
    <li class="dropdown"><a class="dropdown-toggle my-blog" href="<?php echo $CFG->wwwroot;?>/blog/index.php" title="<?php echo get_string('myblogs','menubar');?>"><?php echo get_string('myblogs','menubar');?><b class="caret"></b></a>
  </li>
  <?php /*?><li class="dropdown"><a title="<?php echo get_string('siteadministration','menubar');?>" data-toggle="dropdown" class="dropdown-toggle" href="#cm_submenu_5"><?php echo get_string('siteadministration','menubar');?><b class="caret"></b></a>
    <ul class="dropdown-menu">
      <li class="dropdown-submenu"><a title="<?php echo get_string('themesettings','menubar');?>" data-toggle="dropdown" class="dropdown-toggle" href="#cm_submenu_6"><?php echo get_string('themesettings','menubar');?></a>
        <ul class="dropdown-menu">
          <!-- li class="dropdown-submenu"><a title="<?php echo get_string('themes','menubar');?>" data-toggle="dropdown" class="dropdown-toggle" href="#cm_submenu_7"><?php echo get_string('themes','menubar');?></a>
            <ul class="dropdown-menu">
             < <li><a href="<?php //echo $CFG->wwwroot;?>/admin/settings.php?section=themesettings" title="<?php //echo get_string('themesettings','menubar');?>"><?php //echo get_string('themesettings','menubar');?></a></li>-->
              <!--li><a href="<?php echo $CFG->wwwroot;?>/theme/index.php" title="<?php echo get_string('themeselector','menubar');?>"><?php echo get_string('themeselector','menubar');?></a></li>
              <?php 
			  if(count($themeListing ) > 0 ){
				 foreach($themeListing  as $themename){
				   if($themename == 'gourmet'){
			  ?>
              <li class="dropdown-submenu"><a title="<?php echo $themename;?>" data-toggle="dropdown" class="dropdown-toggle" href="#cm_submenu_8"><?php echo $themename;?></a>
                <ul class="dropdown-menu" !-->
                  <li><a href="<?php echo $CFG->wwwroot;?>/admin/settings.php?section=theme_gourmet_generic" title="<?php echo get_string('gourmetgeneralsettings','menubar');?>"><?php echo get_string('gourmetgeneralsettings','menubar');?></a></li>
                  <li><a href="<?php echo $CFG->wwwroot;?>/admin/settings.php?section=theme_gourmet_header" title="<?php echo get_string('gourmetsiteheadersettings','menubar');?>"><?php echo get_string('gourmetsiteheadersettings','menubar');?></a></li>
                 <!-- <li><a href="<?php //echo $CFG->wwwroot;?>/admin/settings.php?section=theme_gourmet_custommenu" title="<?php //echo get_string('gourmetcustommenus','menubar');?>"><?php //echo get_string('gourmetcustommenus','menubar');?></a></li>-->
                  <li><a href="<?php echo $CFG->wwwroot;?>/admin/settings.php?section=theme_gourmet_footerblocks" title="<?php echo get_string('gourmetfootercontentblocks','menubar');?>"><?php echo get_string('gourmetfootercontentblocks','menubar');?></a></li>
                  <li><a href="<?php echo $CFG->wwwroot;?>/admin/settings.php?section=theme_gourmet_alerts" title="<?php echo get_string('gourmetfrontpagealerts','menubar');?>"><?php echo get_string('gourmetfrontpagealerts','menubar');?></a></li>
                  <li><a href="<?php echo $CFG->wwwroot;?>//admin/settings.php?section=theme_gourmet_slideshow" title="<?php echo get_string('gourmetfrontpageslideshow','menubar');?>"><?php echo get_string('gourmetfrontpageslideshow','menubar');?></a></li>
                  <li><a href="<?php echo $CFG->wwwroot;?>/admin/settings.php?section=theme_gourmet_promo" title="<?php echo get_string('gourmetfrontpagepromobox','menubar');?>"><?php echo get_string('gourmetfrontpagepromobox','menubar');?></a></li>
                  <li><a href="<?php echo $CFG->wwwroot;?>/admin/settings.php?section=theme_gourmet_homeblocks" title="<?php echo get_string('gourmetfrontpagecontentblocks','menubar');?>"><?php echo get_string('gourmetfrontpagecontentblocks','menubar');?></a></li>
                  <li><a href="<?php echo $CFG->wwwroot;?>/admin/settings.php?section=theme_gourmet_awards" title="<?php echo get_string('gourmetfrontpageawardssection','menubar');?>"><?php echo get_string('gourmetfrontpageawardssection','menubar');?></a></li>
                  <li><a href="<?php echo $CFG->wwwroot;?>/admin/settings.php?section=theme_gourmet_testimonials" title="<?php echo get_string('gourmetfrontpagetestimonials','menubar');?>"><?php echo get_string('gourmetfrontpagetestimonials','menubar');?></a></li>
                  <li><a href="<?php echo $CFG->wwwroot;?>/admin/settings.php?section=theme_gourmet_social" title="<?php echo get_string('gourmetsocialnetworking','menubar');?>"><?php echo get_string('gourmetsocialnetworking','menubar');?></a></li>
                  <li><a href="<?php echo $CFG->wwwroot;?>/admin/settings.php?section=theme_gourmet_analytics" title="<?php echo get_string('gourmetgoogleanalytics','menubar');?>"><?php echo get_string('gourmetgoogleanalytics','menubar');?></a></li>
                <!-- /ul>
              </li>
              <?php }else{ ?>
              <li><a href="<?php echo $CFG->wwwroot;?>/admin/settings.php?section=themesetting<?php echo $themename;?>" title="<?php echo $themename;?>"><?php echo $themename;?></a> </li>
              <?php }}}?>
			  
            </ul>
          </li -->
        </ul>
      </li>
      <!-- li class="dropdown-submenu"><a title="<?php echo get_string('security','menubar');?>" data-toggle="dropdown" class="dropdown-toggle" href="#cm_submenu_8"><?php echo get_string('security','menubar');?></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo $CFG->wwwroot;?>/admin/settings.php?section=httpsecurity" title="<?php echo get_string('httpsecurity','menubar');?>"><?php echo get_string('httpsecurity','menubar');?></a></li>
        </ul>
      </li -->
      <!-- li class="dropdown-submenu"><a title="<?php echo get_string('plugins','menubar');?>" data-toggle="dropdown" class="dropdown-toggle" href="#cm_submenu_9"><?php echo get_string('plugins','menubar');?></a>
        <ul class="dropdown-menu">
          <li class="dropdown-submenu"><a title="<?php echo get_string('authentication','menubar');?>" data-toggle="dropdown" class="dropdown-toggle" href="#cm_submenu_10"><?php echo get_string('authentication','menubar');?></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo $CFG->wwwroot;?>/admin/auth_config.php?auth=ldap" title="<?php echo get_string('ldapserver','menubar');?>"><?php echo get_string('ldapserver','menubar');?></a></li>
              <li><a href="<?php echo $CFG->wwwroot;?>/admin/auth_config.php?auth=cas" title="<?php echo get_string('casserversso','menubar');?>"><?php echo get_string('casserversso','menubar');?></a></li>
            </ul>
          </li>
        </ul>
      </li -->
      <li><a href="<?php echo $CFG->wwwroot;?>/admin/backup.php" title="<?php echo get_string('easybackup','menubar');?>"><?php echo get_string('easybackup','menubar');?></a></li>
    </ul>
  </li>
  <?php */?>
  
	<!--<li class="dropdown">
	<a title="<?php echo get_string('manage','menubar');?>" data-toggle="dropdown" class="dropdown-toggle" href="#cm_submenu_11"><?php echo get_string('manage','menubar');?><b class="caret"></b></a>
    <ul class="dropdown-menu">
      <li><a href="<?php echo $CFG->wwwroot;?>/cms/index.php" title="<?php echo get_string('managecms','menubar');?>"><?php echo get_string('managecms','menubar');?></a></li>
      <li><a href="<?php echo $CFG->wwwroot;?>/messages/index.php" title="<?php echo get_string('managemessages','menubar');?>"><?php echo get_string('managemessages','menubar');?></a></li>
     <li><a href="<?php echo $CFG->wwwroot;?>/department/index.php" title="<?php echo get_string('managedepartments','menubar');?>"><?php echo get_string('managedepartments','menubar');?></a></li>
    </ul>
  </li>-->
  <?php 
  }else if(/*$userrole == 5*/$USER->archetype == 'student'){ // student role  ?>
   <li class="dropdown"><a  data-toggle="dropdown" class="dropdown-toggle my-dashboard" href="<?php echo $CFG->wwwroot;?>" title="<?php echo get_string('mydashboard','menubar');?>"><?php echo get_string('mydashboard','menubar');?><b class="caret"></b></a>
   <!-- <ul class="dropdown-menu">
      <li><a href="<?php echo $CFG->wwwroot;?>" title="<?php echo get_string('dashboard','menubar');?>"><em><i class="fa fa-dashboard"></i><?php echo get_string('dashboard','menubar');?></em></a></li>
      <li><a href="<?php echo $CFG->wwwroot;?>/calendar/view.php?view=month" title="<?php echo get_string('calendar','menubar');?>"><em><i class="fa fa-calendar"></i><?php echo get_string('calendar','menubar');?></em></a></li>
   <li><a href="<?php echo $CFG->wwwroot;?>/blog/index.php" title="<?php echo get_string('blogs','menubar');?>"><em><i class="fa fa-file"></i><?php echo get_string('blogs','menubar');?></em></a></li>
            <li><a href="<?php echo $CFG->wwwroot;?>/event/view.php" title="<?php echo get_string('events','menubar');?>"><em><i class="fa fa-file"></i><?php echo get_string('events','menubar');?></em></a></li>
    <li><a href="<?php //echo $CFG->wwwroot;?>/badges/mybadges.php" title="<?php //echo get_string('badges','menubar');?>"><em><i class="fa fa-certificate"></i><?php //echo get_string('badges','menubar');?></em></a></li>
      <li><a href="<?php echo $CFG->wwwroot;?>/user/files.php" title="<?php echo get_string('filemanagement','menubar');?>"><em><i class="fa fa-file"></i><?php echo get_string('filemanagement','menubar');?></em></a></li>
      <li><a href="<?php echo $CFG->wwwroot;?>/login/logout.php" title="<?php echo get_string('logout','menubar');?>"><em><i class="fa fa-sign-out"></i><?php echo get_string('logout','menubar');?></em></a></li>
    </ul>-->
  </li>
  
  
  <li class="dropdown"><a title="<?php echo get_string('mylearningpage','menubar');?>" data-toggle="dropdown" class="dropdown-toggle my-learning-page" href="#cm_submenu_10"><?php echo get_string('mylearningpage','menubar');?><b class="caret"></b></a>
    <ul class="dropdown-menu">
      <li><a href="<?php echo $CFG->wwwroot;?>/my/learningpage.php" title="<?php echo get_string('courselist','menubar');?>"><?php echo get_string('courselist','menubar');?></a></li>
      <li><a href="<?php echo $CFG->wwwroot;?>/my/nonlearningpage.php" title="<?php echo get_string('noncoursematerial','menubar');?>"><?php echo get_string('noncoursematerial','menubar');?></a></li>
    </ul>
  </li>

   
  
  <li class="dropdown"><a title="<?php echo get_string('reports','menubar');?>" data-toggle="dropdown" class="dropdown-toggle my-reports" href="#cm_submenu_10"><?php echo get_string('reports','menubar');?><b class="caret"></b></a>
    <ul class="dropdown-menu">
      <li><a href="#cm_submenu_11" title="<?php echo get_string('viewreports','menubar');?>"><?php echo get_string('viewreports','menubar');?></a></li>
    </ul>
  </li>
  
   <li class="dropdown"><a class="dropdown-toggle my-blog" href="<?php echo $CFG->wwwroot;?>/blog/index.php" title="<?php echo get_string('myblogs','menubar');?>"><?php echo get_string('myblogs','menubar');?><b class="caret"></b></a>
  </li>
  
  
  <?php /*?><li class="dropdown">
  <a title="<?php echo get_string('myprofile','menubar');?>" data-toggle="dropdown" class="dropdown-toggle my-profile" href="#cm_submenu_1"><?php echo get_string('myprofile','menubar');?><b class="caret"></b></a>
  <ul class="dropdown-menu">
    <li><a href="<?php echo $CFG->wwwroot;?>/user/profile.php" title="<?php echo get_string('viewprofile','menubar');?>"><?php echo get_string('viewprofile','menubar');?></a></li>
    <li><a href="<?php echo $CFG->wwwroot;?>/user/edit.php" title="<?php echo get_string('editprofile','menubar');?>"><?php echo get_string('editprofile','menubar');?></a></li>
    <li><a href="<?php echo $CFG->wwwroot;?>/login/change_password.php" title="<?php echo get_string('changepassword','menubar');?>"><?php echo get_string('changepassword','menubar');?></a></li>
    <li><a href="<?php echo $CFG->wwwroot;?>/message/index.php" title="<?php echo get_string('messages','menubar');?>"><?php echo get_string('messages','menubar');?></a></li>
    </li>
  </ul>
  </li><?php */?>
  
  <?php } ?>
</ul>
<div class="searchtab pull-right">
	<ul>
    	<li class="pull-left"><a href="javascript:void(0)" class="course-catalog pull-right">Course Catalog</a></li>
    </ul>
    <a href="javascript:void(0)" class="search-button"></a>
    <div class="search-view">
    <span class="arrow"></span>
    	<input name="" type="text" value="Search..." />
        <input name="" type="button" value="Go" />
    </div>
</div>
