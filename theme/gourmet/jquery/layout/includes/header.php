<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/* LOGO Image */
$haslogo = (!empty($PAGE->theme->settings->logo));

/* Logo settings */
if ($haslogo) {
$logourl = $PAGE->theme->setting_file_url('logo', 'logo');
} else {
$logourl = $OUTPUT->pix_url('logo', 'theme');
}

$userrole =  getUserRole($USER->id);

/* Social Media Settings */
$hastwitter     = (empty($PAGE->theme->settings->twitter)) ? false : $PAGE->theme->settings->twitter;
$hasfacebook    = (empty($PAGE->theme->settings->facebook)) ? false : $PAGE->theme->settings->facebook;
$hasgoogleplus  = (empty($PAGE->theme->settings->googleplus)) ? false : $PAGE->theme->settings->googleplus;
$haslinkedin    = (empty($PAGE->theme->settings->linkedin)) ? false : $PAGE->theme->settings->linkedin;
$hasyoutube     = (empty($PAGE->theme->settings->youtube)) ? false : $PAGE->theme->settings->youtube;
$hasflickr      = (empty($PAGE->theme->settings->flickr)) ? false : $PAGE->theme->settings->flickr;
$haspinterest   = (empty($PAGE->theme->settings->pinterest)) ? false : $PAGE->theme->settings->pinterest;
$hasinstagram   = (empty($PAGE->theme->settings->instagram)) ? false : $PAGE->theme->settings->instagram;
$hasskype       = (empty($PAGE->theme->settings->skype)) ? false : $PAGE->theme->settings->skype;
$hasrss         = (empty($PAGE->theme->settings->rss)) ? false : $PAGE->theme->settings->rss;

$twitter = $PAGE->theme->settings->twitter;
$facebook = $PAGE->theme->settings->facebook;
$googleplus = $PAGE->theme->settings->googleplus;
$linkedin = $PAGE->theme->settings->linkedin;
$youtube = $PAGE->theme->settings->youtube;
$flickr = $PAGE->theme->settings->flickr;
$pinterest = $PAGE->theme->settings->pinterest;
$instagram = $PAGE->theme->settings->instagram;
$skype = $PAGE->theme->settings->skype;
$rss = $PAGE->theme->settings->rss;

// If any of the above social networks are true, sets this to true.
$hassocialnetworks = ($hasfacebook || $hastwitter || $hasgoogleplus || $hasflickr || $hasinstagram || $haslinkedin || $haspinterest || $hasskype || $hasrss || $hasyoutube ) ? true : false;

/* Header widget settings */
$hasheaderwidget = (!empty($PAGE->theme->settings->headerwidget));
$headerwidget = $PAGE->theme->settings->headerwidget;
/* Header phone settings */
$hasheaderphone = (!empty($PAGE->theme->settings->headerphone));
$headerphone = $PAGE->theme->settings->headerphone;
/* Header email settings */
$hasheaderemail = (!empty($PAGE->theme->settings->headeremail));
$headeremail = $PAGE->theme->settings->headeremail;

?>
<script>
var userrole = '<?php echo $userrole;?>';
</script>
<header id="page-header" class="header clearfix">
    <div class="top-bar">
        <div class="container">   
        <!--Logo-->
        <h1 class="logo col-md-1 col-sm-1 col-xs-12">
            <a href="<?php echo $CFG->wwwroot ?>"><img id="logo" src="<?php echo $logourl ?>" alt="<?php echo $SITE->shortname; ?>" /></a>
        </h1>
        <?php if(isset($USER->id) &&  !in_array($USER->id, array(0,2))){
			$actionCounts = checkCurrentActionsHeader();
			$messageCount = 0;
			$eventCount = 0;
			$courseCount = 0;
			$messageDisplay = "";
			$eventDisplay = "";
			$courseDisplay = "";
			if(isset($actionCounts['messages_data']) && isset($actionCounts['messages_data']['count'])){
				$messageCount = $actionCounts['messages_data']['count'];
			}
			if($messageCount == 0 ){
				$messageDisplay = "Style = 'display:none;'";
			}

			if(isset($actionCounts['events_data']) && isset($actionCounts['events_data']['count'])){
				$eventCount = $actionCounts['events_data']['count'];
			}
			if($eventCount == 0 ){
				$eventDisplay = "Style = 'display:none;'";
			}

			if(isset($actionCounts['course_data']) && isset($actionCounts['course_data']['count'])){
				$courseCount = $actionCounts['course_data']['count'];
			}
			if($courseCount == 0 ){
				$courseDisplay = "Style = 'display:none;'";
			}

		?>
			<?php/* if($userrole == 1 || $userrole == 5){*/?>
			<?php if($USER->archetype == 'student' || $USER->archetype == 'manager'){?>
                <a href="<?php echo $CFG->wwwroot.'/messages/mymessages.php'; ?>" class="email pull-left"><span <?php echo $messageDisplay; ?> ><?php echo $messageCount; ?></span></a>
                <a href="javascript:void(0)" class="bell pull-left"><span <?php echo $eventDisplay; ?>><?php echo $eventCount; ?></span></a>
            <?php } if(/*$userrole == 5*/ $USER->archetype == 'student'){?>
                <a href="javascript:void(0)" class="greenline pull-left"><span <?php echo $courseDisplay; ?>><?php echo $courseCount; ?></span></a>
        <?php }} ?>
        <!--Share-->
        <div class="share pull-right">
        	<a href="javascript:void(0)" class="share-icon pull-right" title="Share"></a>
           <?php if ($hassocialnetworks && $PAGE->theme->settings->enabletopbarsocial) { ?>
            <ul class="social-icons col-md-3 col-sm-3 col-xs-12 hidden-xs">
            <span class="arrow"></span>     
                <?php if ($hastwitter) { ?>
                <li><a href="<?php echo $twitter ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <?php } ?>
                <?php if ($hasfacebook) { ?>
                <li><a href="<?php echo $facebook ?>" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                <?php } ?>
                <?php if ($hasgoogleplus) { ?>
                <li><a href="<?php echo $googleplus ?>" target="_blank" ><i class="fa fa-google-plus"></i></a></li> 
                <?php } ?>
                <?php if ($haslinkedin) { ?>
                <li><a href="<?php echo $linkedin ?>" target="_blank" ><i class="fa fa-linkedin"></i></a></li>
                <?php } ?>
                <?php if ($hasskype) { ?>
                <li><a href="<?php echo 'skype:'.$skype.'?call' ?>" target="_blank" ><i class="fa fa-skype"></i></a></li>
                <?php } ?>
                <?php if ($hasyoutube) { ?>
                <li><a href="<?php echo $youtube ?>" target="_blank" ><i class="fa fa-youtube"></i></a></li>
                <?php } ?>
                <?php if ($hasflickr) { ?>
                <li><a href="<?php echo $flickr ?>" target="_blank" ><i class="fa fa-flickr"></i></a></li>
                <?php } ?>
                <?php if ($hasinstagram) { ?>
                <li><a href="<?php echo $instagram ?>" target="_blank" ><i class="fa fa-instagram"></i></a></li>
                <?php } ?>
                <?php if ($haspinterest) { ?>
                <li><a href="<?php echo $pinterest ?>" target="_blank" ><i class="fa fa-youtube"></i></a></li>
                <?php } ?>
                
                <?php if ($hasrss) { ?>   
                <li class="row-end"><a href="<?php echo $rss ?>" target="_blank" ><i class="fa fa-rss"></i></a></li>   
                <?php } ?>          
            </ul><!--//social-icons-->
            <?php } ?></div>
            <div class="logininfo-container pull-right">
            	<div class="pull-left">
                <?php echo $PAGE->headingmenu ?>
                <?php echo $OUTPUT->login_info() ?>
                </div>
				<?php if($USER->id>0){?>
                <div class="arrow-bottom pull-left">
                	<a href="javascript:void(0)" class="arrow-bottom-icon pull-right"></a>                    
                    <ul class="profile-dropdown">
                        <span class="arrow"></span> 
                        <li><a href="<?php echo $CFG->wwwroot;?>/user/profile.php" class="view-profile"><?php echo get_string('viewprofile','user');?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot;?>/user/editprofile.php" class="edit-profile"><?php echo get_string('editprofile','user');?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot;?>/login/change_password.php" class="change-password"><?php echo get_string('changepassword','user');?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot;?>/login/logout.php?sesskey=<?php echo sesskey();?>" class="logout"><?php echo get_string('logout','user');?></a></li>
						<?php 
							if((isset($USER->original_archetype) && $USER->original_archetype == 'manager') || $USER->archetype == 'manager'){
								if($USER->archetype == 'student'){
									?>
									<li><a href="<?php echo $CFG->wwwroot;?>/local/switchrole.php?role=1&sesskey=<?php echo sesskey();?>" class="switchrole"><?php echo get_string('switchtoadmin','user');?></a></li>
								<?php
								}else{
								?>
									<li><a href="<?php echo $CFG->wwwroot;?>/local/switchrole.php?role=2&sesskey=<?php echo sesskey();?>" class="switchrole"><?php echo get_string('switchtolearner','user');?></a></li>
								<?php
								}
							}?>
                    </ul>                    
                </div>
                <?php }?>				
				
                <?php if (isloggedin()) : ?>
                <?php echo $OUTPUT->user_picture($USER); ?>
                <?php endif; ?>
            </div>        
        </div>      
    </div><!--//top-bar-->    
    <!--<div class="header-main container">
        <h1 class="logo col-md-4 col-sm-4 col-xs-12">
            <a href="<?//php echo $CFG->wwwroot ?>"><img id="logo" src="<?//php echo $logourl ?>" alt="<?//php echo $SITE->shortname; ?>" /></a>
        </h1>
        <div class="info col-md-8 col-sm-8 col-xs-12">           
            <?//php if($hasheaderwidget) {?>
            <div class="header-widget">
                <?//php echo $headerwidget ?>
            </div>
            <?//php }?>
            <?//php if ($hasheaderphone || $hasheaderemail) {?>
            <div class="contact pull-right">
                <?//php if ($hasheaderphone) {?>
                <p class="phone"><i class="fa fa-phone"></i><?//php echo $headerphone ?></p>
                <?//php }?>
                <?//php if ($hasheaderemail) {?>
                <p class="email"><i class="fa fa-envelope"></i><a href="mailto:<?//php echo $headeremail ?>"><?//php echo $headeremail ?></a></p>
                <?//php } ?>
            </div>
            <?//php } ?>
        </div>
    </div><!--//header-main--><!--//info-->
</header>
<script>
$(document).ready(function(){
	var refreshId = setInterval(function() {
		var userId = "<?php echo $USER->id;?>";
		if(userId != 0 && userId != 2 && userId != ''){
			$.ajax({
				type: "POST",
				url: "<?php echo $CFG->wwwroot; ?>/local/ajax.php",
				data: "action=checkCurrentActionsHeader",
				success: function(data){
					var obj = jQuery.parseJSON(data);
					if(obj.messages_data.count != 0){
						$(".email.pull-left").find('span').html(obj.messages_data.count);
						$(".email.pull-left").find('span').show();
					}else{
						$(".email.pull-left").find('span').hide();
					}
					if(obj.events_data.count != 0){
						$(".bell.pull-left").find('span').html(obj.events_data.count);
						$(".bell.pull-left").find('span').show();
					}else{
						$(".bell.pull-left").find('span').hide();
					}

					if(obj.course_data.count != 0){
						$(".greenline.pull-left").find('span').html(obj.course_data.count);
						$(".greenline.pull-left").find('span').show();
					}else{
						$(".greenline.pull-left").find('span').hide();
					}
				}
			});
		}
	}, 180000);
});
</script>