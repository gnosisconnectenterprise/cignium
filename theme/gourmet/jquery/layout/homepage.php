<?php

/**
		* Custom module - Home Page Layout
		* Date Creation - 04/05/2014
		* Date Modification : 23/06/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
    
$hashiddendock = (empty($PAGE->layout_options['noblocks']) && $PAGE->blocks->region_has_content('hidden-dock', $OUTPUT));
$hasleftsidebar = (empty($PAGE->theme->settings->layout)) ? false : $PAGE->theme->settings->layout;
$hasanalytics = (empty($PAGE->theme->settings->useanalytics)) ? false : $PAGE->theme->settings->useanalytics;
echo $OUTPUT->doctype();
$userrole =  getUserRole($USER->id);

$userrole =  getUserRole($USER->id);
global $mobile;

?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
<title><?php echo $OUTPUT->page_title(); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="Moodle Theme by 3rd Wave Media | elearning.3rdwavemedia.com" />
<?php require_once(dirname(__FILE__).'/includes/iosicons.php'); ?>
<link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' rel='stylesheet' type='text/css'>
<?php echo $OUTPUT->standard_head_html() ?>
<!-- Respond.js IE8 support of media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<?php if ($hasanalytics) { ?>
<!-- Start Google Analytics -->
<?php require_once(dirname(__FILE__).'/includes/analytics.php'); ?>
<!-- End Google Analytics -->
<?php } ?>
</head>
<body <?php echo $OUTPUT->body_attributes(); ?>>
<?php echo $OUTPUT->standard_top_of_body_html(); ?>
<div class="wrapper">
  <?php require_once(dirname(__FILE__).'/includes/header.php'); ?>

  <nav role="navigation" class="main-nav">
    <?php /* ?>
    <div class="container">
      <div class="navbar-header">
        <button class="navbar-toggle btn-navbar" type="button" data-toggle="collapse" data-target="#nav-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <!--//nav-toggle-->
      </div>
      <!--//navbar-header-->
      <div id="nav-collapse" class="nav-collapse collapse"> <?php echo $customMenu = $OUTPUT->custom_menu();?>
        <?php require_once(dirname(__FILE__).'/includes/custommenu.php'); ?>
      </div>
    </div>
	  <?php */ ?>
  </nav>

  <div id="page" class="container">
    <?php require_once(dirname(__FILE__).'/includes/alerts.php');?>
    <?php require_once(dirname(__FILE__).'/includes/slideshow.php');?>
    <?php require_once(dirname(__FILE__).'/includes/homepromo.php');?>
    <?php require_once(dirname(__FILE__).'/includes/homeblocks.php');?>
    <?php require_once(dirname(__FILE__).'/includes/testimonials.php'); ?>
    <?php echo $OUTPUT->main_content();?>
    <?php /*?>
    <div id="page-content" class="row">
      <section id="region-main" class="col-md-12">
        <div class="region-main-inner">
          <?php require_once(dirname(__FILE__).'/includes/custombreadcrumb.php'); ?>
          <?php
				echo $OUTPUT->course_content_header();
				echo $OUTPUT->main_content();
				echo $OUTPUT->course_content_footer();
            ?>
        </div>
        <!--//region-main-inner-->
      </section>
    </div>
    <!--//page-content-->
	<?pph */ ?>
    <?php require_once(dirname(__FILE__).'/includes/awards.php'); ?>
  </div>
  <!--//#page-->
</div>
<!--wrapper-->
<footer id="page-footer" class="footer">
  <?php require_once(dirname(__FILE__).'/includes/footer.php'); ?>
</footer>
<?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
<style>
div[role="main"]{
display:none;
}
</style>
</html>
