<?php

/**
		* Custom module - Global Admin Layout
		* Date Creation - 04/05/2014
		* Date Modification : 23/06/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
	
$hasleftsidebar = (empty($PAGE->theme->settings->layout)) ? false : $PAGE->theme->settings->layout;
$hasanalytics = (empty($PAGE->theme->settings->useanalytics)) ? false : $PAGE->theme->settings->useanalytics;
echo $OUTPUT->doctype();

$userrole =  getUserRole($USER->id);


?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
<title><?php echo $OUTPUT->page_title(); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="Moodle Theme by 3rd Wave Media | elearning.3rdwavemedia.com" />
<?php require_once(dirname(__FILE__).'/includes/iosicons.php'); ?>
<link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' rel='stylesheet' type='text/css'>
<?php echo $OUTPUT->standard_head_html() ?>
<!-- Respond.js IE8 support of media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<?php if ($hasanalytics) { ?>
<!-- Start Google Analytics -->
<?php require_once(dirname(__FILE__).'/includes/analytics.php'); ?>
<!-- End Google Analytics -->
<?php } ?>
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jscrollpane/jquery.jscrollpane.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jscrollpane/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jscrollpane/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jscrollpane/mwheelIntent.js"></script>
</head>
<body <?php echo $OUTPUT->body_attributes(); ?>>
<?php echo $OUTPUT->standard_top_of_body_html() ?>
<div class="wrapper">
  <?php require_once(dirname(__FILE__).'/includes/header.php'); ?>
  <nav role="navigation" class="main-nav">
    <div class="container">
      <div class="navbar-header">
        <button class="navbar-toggle btn-navbar" type="button" data-toggle="collapse" data-target="#nav-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <!--//nav-toggle-->
      </div>
      <!--//navbar-header-->
      <div id="nav-collapse" class="nav-collapse collapse"> <?php echo $customMenu = $OUTPUT->custom_menu();?>
        <?php require_once(dirname(__FILE__).'/includes/custommenu.php'); ?>
      </div>
    </div>
  </nav>
  <!--//main-nav-->
  <div id="page" class="container">
    <div id="page-content" class="row">
      <?php if ($hasleftsidebar) { ?>
      <section id="region-main" class="col-md-9 pull-right">
      <?php } else { ?>
		<?php
            require_once(dirname(__FILE__).'/includes/custombreadcrumb.php');
        ?>
      <section id="region-main" class="col-md-9">
        <?php } ?>
        <div class="region-main-inner">
          <?php
				//require_once(dirname(__FILE__).'/includes/custombreadcrumb.php');
                echo $OUTPUT->course_content_header();
                echo $OUTPUT->main_content();
                echo $OUTPUT->course_content_footer();
                ?>
        </div>
        <!--//region-main-inner-->
      </section>
      <?php if ($hasleftsidebar) { ?>
      <?php echo $OUTPUT->blocks('side-pre', 'col-md-3 pull-left'); ?>
      <?php } else { ?>
      <?php echo $OUTPUT->blocks('side-pre', 'col-md-3 pull-right'); ?>
      <?php } ?>
      
       <aside data-droptarget="1" data-blockregion="side-pre" class="col-md-3 pull-right block-region" >
          <div aria-labelledby="instance-5-header" data-instanceid="5" data-block="settings" role="navigation" class="block_settings  blockformydoc" >
            <div class="box block_tree_box" id="settingsnav">
              <div><?php echo $OUTPUT->heading(get_string('mydocuments','learnercourse'));?></div>
              <?php require_once($CFG->dirroot . '/local/user/mydocfiles.php'); ?>              
         </div>
        </div>
        </aside>
        
      <?php /*$myDocs = getMyDocuments();?>
      <?php if(count($myDocs) > 0){?>
            
          <aside data-droptarget="1" data-blockregion="side-pre" class="col-md-3 pull-right block-region" >
          <div aria-labelledby="instance-5-header" data-instanceid="5" data-block="settings" role="navigation" class="block_settings  blockformydoc">
            <div class="box block_tree_box" id="settingsnav">
              <div><?php echo $OUTPUT->heading(get_string('mydocuments','learnercourse'));?></div>
             
              <table cellspacing="0" cellpadding="0">
                <?php 
                          foreach($myDocs as $docs){
                            
                            $id = $docs->id;
                            $itemid = $docs->itemid;
							$filepath = $docs->filepath;
							$filenameStr = $docs->filename;
							$filenameArr = explode(".",$filenameStr,2);
							$filename = $filenameArr[0];
                        ?>
                                <tr id="doc<?php echo $id;?>">
                                  <td><?php echo $filename;?></td>
                                  <td><a href="javascript:;" title="<?php echo get_string('view','learnercourse');?>" alt="<?php echo get_string('view','learnercourse');?>"><?php echo get_string('view','learnercourse');?></a></td>
                                  <td id="docdeletebtn<?php echo $id;?>"><a href="javascript:;" onClick="deleteMyDoc(<?php echo $id;?>)" title="<?php echo get_string('delete','learnercourse');?>" alt="<?php echo get_string('delete','learnercourse');?>"><?php echo get_string('delete','learnercourse');?></a></td>
                                </tr>
              <?php } ?>
              </table>
              <form name="uploadfilefrm" id="uploadfilefrm" enctype="multipart/form-data">
                <div><input type="file" name="uploaddocfile" id="uploaddocfile" ></div>
                <label for="uploadfile" generated="true" class="error" style="display:none"><?php print get_string('Invalidimageextension'); ?></label>
                <div><input type="submit" value="<?php echo get_string('uploaddocument','learnercourse');?>" name="submitdocfile" id="submitdocfile" ></div>
              </form>
            </div>
          </div>
        </div>
        </aside>
     <?php }*/ ?>
	 
	 
	 <?php if($USER->archetype == $CFG->userTypeManager){  ?>
	 
	 
	 <aside data-droptarget="1" data-blockregion="side-pre" class="col-md-3 pull-right block-region right-message" style="padding-top:10px;">
          <div aria-labelledby="instance-5-header" data-instanceid="5" data-block="settings" role="navigation" class="block_settings  blockformydoc">
            <div class="lower-right-bottom">
			<div class="block-header messages-icon"><span></span>Messages</a><a href="javascript:void(0)" class="message-setting pull-right"></div>
			<div class="block-content scroll-pane">
			<?php
									
				$msgSql = "SELECT * from {$CFG->prefix}my_messages where deleted = 0 AND  status = 1 and message_from='".$USER->id."' ORDER BY timecreated DESC limit 0,5";
				
				$messages = $DB->get_records_sql($msgSql);
				
				if(count($messages)>0){
					foreach($messages as $message){
						$user = $DB->get_record('user',array('id'=>$message->message_from));
					?>
						<div class = 'message-block'>
							
							<div class = 'message-sender-image'>
							<?php echo $OUTPUT->user_picture($user, $userpictureparams);?>
							</div>
							<div class = 'message-content'>
								<div class = "message-sender-info">
								<?php echo  ucfirst($user->firstname)." ".ucfirst($user->lastname)."<br>";
										
									echo date('d F Y h:ia',$message->timecreated); // user M in place of F for short month name
								?>
								</div>
								<div class = "message-content-details">
									<?php
										if(strlen($message->message_content) >100){
											echo substr($message->message_content,0,100)." ...";
										}else{
											echo $message->message_content;
										}
									?>
								</div>
							</div>
						</div>
					<?php
					}
				}
			?>
			</div>
		</div>
          </div>
        </div>
        </aside>
		<script>$('.scroll-pane').jScrollPane();</script>	 
	 <?php } ?>
    <!--//page-content-->
  </div>
  <!--//#page-->
</div>
<!--wrapper-->
<footer id="page-footer" class="footer">
  <?php require_once(dirname(__FILE__).'/includes/footer.php'); ?>
</footer>
<!--<style>

	.blockformydoc{

		background: none repeat scroll 0 0 padding-box #F5F5F5;
		border: medium none;
		border-radius: 0;
		box-shadow: none;
		margin-bottom: 30px;
		padding-bottom: 15px;
		padding-top: 0;
		
	}	
	
	.error{
	  color:#FF0000;
	  font-size:11px;
	}
</style>-->

<script src="<?php echo $CFG->wwwroot;?>/local/js/jquery.validate.js" type="text/javascript" charset="utf-8"></script>
<script>


 $(document).ready(function(){
 
     $('.block').hide(); 
     $('.block').each(function(){
	 
	   var hasClass = $(this).hasClass('block_calendar_month');
	   if(hasClass == true){
	     $(this).show();
	   }else{
	     $(this).hide();
	   }
	 
	 });
	 
	 /*$('input[type=file]').change(function(e){
	  var file = $(this).val();
	  var filename = file.split('\\').pop();
	  $('#filename').html(filename);
	});*/

	
        /*$("#uploadfilefrm").validate({
            rules: {
                'uploaddocfile': {required:true,accept: 'JPG|JPEG|PNG|GIF|BMP|PDF|DOC|TXT'}
            },
            messages: {
   
                'uploaddocfile':  {
                     required: "<?php print get_string('pleaseselectfile','learnercourse'); ?>",
                     accept: "<?php print get_string('invalidfileextension','learnercourse'); ?>"
                }
            }
        });*/
	 
	 
	

 });
 
function deleteMyDoc(docid){

	 var docid = docid;
 
    if(confirm("<?php echo get_string('areyousureyouwanttodeletethisfile','learnercourse');?>")){
 
		 $.ajax({
		 
		   url:'<?php echo $CFG->wwwroot;?>/local/ajax.php?action=deleteMyDoc',
		   type: 'POST',
		   data:'id='+docid,
		   beforeSend:function(){
			   $('#docdeletebtn'+docid).html('<img src="<?php echo $CFG->wwwroot;?>/pix/i/loading_small.gif" width="16" height="16" />');   
		   },
		   dataType:'json',
		   success:function(data){
			   var error = data.error;
			   var success = data.success;
			 
			   if(success == true){ 
			     var id = data.response;
				 $('#doc'+id).remove();
			   }else{
			      alert(error);
			   }
		   }
		 
		 });
	 
	 }else{
	    return false;
	 }
	 
}

</script>
<?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>