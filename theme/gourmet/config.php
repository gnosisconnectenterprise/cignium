<?php

global $USER;

$THEME->name = 'gourmet';

$THEME->doctype = 'html5';
$THEME->yuicssmodules = array();
$THEME->parents = array('bootstrapbase');
/*if($USER->department == 61){
$THEME->sheets = array('bootstrap.min','font-awesome.min', 'flexslider', 'theme1',  'style', 'home', 'rtl', 'responsive', 'settings');
}else{*/
$THEME->sheets = array('bootstrap.min','font-awesome.min', 'flexslider', 'theme',  'style','theme_3', 'home', 'rtl', 'responsive', 'settings');
//}
//$THEME->javascripts = array();
$THEME->supportscssoptimisation = false;
$THEME->enable_dock = false;
$THEME->editor_sheets = array();

$THEME->rendererfactory = 'theme_overridden_renderer_factory';

$THEME->plugins_exclude_sheets = array(
    'block' => array(
        'html',
    ),
);

//$userrole =  getUserRole($USER->id);

if($USER->archetype != $CFG->userTypeStudent && $USER->archetype != $CFG->userTypeAdmin && $USER->archetype != $CFG->userTypeManager){  

//if(in_array($USER->id, array(2)) || in_array($userrole, array('-1'))){

	$THEME->layouts = array(
		// Most backwards compatible layout without the blocks - this is the layout used by default.
		'base' => array(
			'file' => 'columns1.php',
			'regions' => array(),
		),
		// Standard layout with blocks, this is recommended for most pages with general information.
		'standard' => array(
			'file' => 'columns2.php',
			'regions' => array('side-pre'),
			'defaultregion' => 'side-pre',
		),
		// Main course page.
		'course' => array(
			'file' => 'columns2.php',
			'regions' => array('side-pre'),
			'defaultregion' => 'side-pre',
			'options' => array('langmenu'=>true),
		),
		'coursecategory' => array(
			'file' => 'columns2.php',
			'regions' => array('side-pre'),
			'defaultregion' => 'side-pre',
		),
		// part of course, typical for modules - default page layout if $cm specified in require_login().
		'incourse' => array(
			'file' => 'columns2.php',
			'regions' => array('side-pre'),
			'defaultregion' => 'side-pre',
		),
		// The site home page.
		'frontpage' => array(
			'file' => 'frontpage.php',
			'regions' => array('side-pre', 'hidden-dock'),
			'defaultregion' => 'side-pre',
			'options' => array('nonavbar'=>true),
		),
		// Server administration scripts.
		'admin' => array(
			'file' => 'columns2.php',
			'regions' => array('side-pre'),
			'defaultregion' => 'side-pre',
		),
		// My dashboard page.
		'mydashboard' => array(
			'file' => 'columns2.php',
			'regions' => array('side-pre'),
			'defaultregion' => 'side-pre',
			'options' => array('langmenu'=>true),
		),
		// My public page.
		'mypublic' => array(
			'file' => 'columns2.php',
			'regions' => array('side-pre'),
			'defaultregion' => 'side-pre',
		),
		'login' => array(
			'file' => 'cmspages.php',
			'regions' => array(),
			'options' => array('langmenu'=>true),
		),
	
		// Pages that appear in pop-up windows - no navigation, no blocks, no header.
		'popup' => array(
			'file' => 'popup.php',
			'regions' => array(),
			'options' => array('nofooter'=>true, 'nonavbar'=>true),
		),
		// No blocks and minimal footer - used for legacy frame layouts only!
		'frametop' => array(
			'file' => 'columns1.php',
			'regions' => array(),
			'options' => array('nofooter'=>true, 'nocoursefooter'=>true),
		),
		// Embeded pages, like iframe/object embeded in moodleform - it needs as much space as possible.
		'embedded' => array(
			'file' => 'embedded.php',
			'regions' => array()
		),
		// Used during upgrade and install, and for the 'This site is undergoing maintenance' message.
		// This must not have any blocks, links, or API calls that would lead to database or cache interaction.
		// Please be extremely careful if you are modifying this layout.
		'maintenance' => array(
			'file' => 'maintenance.php',
			'regions' => array(),
		),
		// Should display the content and basic headers only.
		'print' => array(
			'file' => 'columns1.php',
			'regions' => array(),
			'options' => array('nofooter'=>true, 'nonavbar'=>false),
		),
		// The pagelayout used when a redirection is occuring.
		'redirect' => array(
			'file' => 'embedded.php',
			'regions' => array(),
		),
		// The pagelayout used for reports.
		'report' => array(
			'file' => 'columns2.php',
			'regions' => array('side-pre'),
			'defaultregion' => 'side-pre',
		),
		// The pagelayout used for safebrowser and securewindow.
		'secure' => array(
			'file' => 'secure.php',
			'regions' => array('side-pre'),
			'defaultregion' => 'side-pre'
		),
		'classroom' => array(
					'file' => 'classroom.php',
					'regions' => array(),
					'options' => array('langmenu'=>true),
			),
		'classroompopup' => array(
					'file' => 'classroompopup.php',
					'regions' => array(),
					'options' => array('nofooter'=>true, 'nonavbar'=>false),
			),
		
		'cmspages' => array(
			 'file' => 'cmspages.php',
             'regions' => array(),
			'options' => array('langmenu'=>true),
		)
	);

}else{

	$THEME->layouts = array(
		// Most backwards compatible layout without the blocks - this is the layout used by default.
		'base' => array(
			'file' => 'globaladmin.php',
			'regions' => array('side-pre'),
			'defaultregion' => 'side-pre',
		),
		// Standard layout with blocks, this is recommended for most pages with general information.
		'standard' => array(
			'file' => 'globaladmin.php',
		    'regions' => array('side-pre'),
			'defaultregion' => 'side-pre',
		),
		// Main course page.
		'course' => array(
			'file' => 'globaladmin.php',
              'regions' => array('side-pre'),
			'defaultregion' => 'side-pre',
		),
		'coursecategory' => array(
			'file' => 'globaladmin.php',
            'regions' => array('side-pre'),
			'defaultregion' => 'side-pre',
		),
		// part of course, typical for modules - default page layout if $cm specified in require_login().
		'incourse' => array(
			'file' => 'globaladmin.php',
              'regions' => array('side-pre'),
			'defaultregion' => 'side-pre',
		),
		// The site home page.
		'frontpage' => array(
			'file' => 'homepage.php',
			'regions' => array(),
			'options' => array('nonavbar'=>true),
		),
		// Server administration scripts.
		'admin' => array(
			 'file' => 'globaladmin.php',
              'regions' => array('side-pre'),
			'defaultregion' => 'side-pre',
		),
		// My dashboard page.
		'mydashboard' => array(
			 'file' => 'globaladmin.php',
             'regions' => array('side-pre'),
			'defaultregion' => 'side-pre',
			'options' => array('langmenu'=>true),
		),
		'learnerdashboard' => array(
			 'file' => 'learnerdashboard.php',
             'regions' => array(),
			'options' => array('langmenu'=>true),
		),
		'admindashboard' => array(
			 'file' => 'admindashboard.php',
             'regions' => array(),
			'options' => array('langmenu'=>true),
		),
		'adminreportdashboard' => array(
			 'file' => 'adminreportdashboard.php',
              'regions' => array('side-pre'),
			'defaultregion' => 'side-pre',
		),
		
		'reportlayout' => array(
			 'file' => 'reportlayout.php',
             'regions' => array(),
			'options' => array('langmenu'=>true),
		),
		// My public page.
		'mypublic' => array(
			 'file' => 'globaladmin.php',
              'regions' => array('side-pre'),
			'defaultregion' => 'side-pre',
		),
		'login' => array(
			'file' => 'loginpage.php',
			'regions' => array(),
			'options' => array('langmenu'=>true),
		),
	
		// Pages that appear in pop-up windows - no navigation, no blocks, no header.
		'popup' => array(
			'file' => 'popup.php',
			'regions' => array(),
			'options' => array('nofooter'=>true, 'nonavbar'=>true),
		),
		// No blocks and minimal footer - used for legacy frame layouts only!
		'frametop' => array(
			 'file' => 'globaladmin.php',
             'regions' => array(),
			'options' => array('nofooter'=>true, 'nocoursefooter'=>true),
		),
		// Embeded pages, like iframe/object embeded in moodleform - it needs as much space as possible.
		'embedded' => array(
			'file' => 'embedded.php',
			'regions' => array()
		),
		// Used during upgrade and install, and for the 'This site is undergoing maintenance' message.
		// This must not have any blocks, links, or API calls that would lead to database or cache interaction.
		// Please be extremely careful if you are modifying this layout.
		'maintenance' => array(
			'file' => 'maintenance.php',
			'regions' => array(),
		),
		// Should display the content and basic headers only.
		'print' => array(
			 'file' => 'print.php',
             'regions' => array(),
			'options' => array('nofooter'=>true, 'nonavbar'=>false),
		),
		// The pagelayout used when a redirection is occuring.
		'redirect' => array(
			'file' => 'embedded.php',
			'regions' => array(),
		),
		// The pagelayout used for reports.
		'report' => array(
			 'file' => 'globaladmin.php',
              'regions' => array('side-pre'),
			'defaultregion' => 'side-pre',
		),
		// The pagelayout used for safebrowser and securewindow.
		'secure' => array(
			'file' => 'secure.php',
			'regions' => array('side-pre'),
			'defaultregion' => 'side-pre'
		),
		'mydocfiles' => array(
			'file' => 'mydocfiles.php',
			'regions' => array(),
			'options' => array('nofooter'=>true, 'nonavbar'=>false),
		),
		'classroom' => array(
					'file' => 'classroom.php',
					'regions' => array(),
					'options' => array('langmenu'=>true),
		),
		'classroompopup' => array(
					'file' => 'classroompopup.php',
					'regions' => array(),
					'options' => array('nofooter'=>true, 'nonavbar'=>false),
		),
		'cmspages' => array(
			 'file' => 'cmspages.php',
             'regions' => array(),
			'options' => array('langmenu'=>true),
		)
	);
}

$THEME->javascripts_footer = array(
    'main'
);


$THEME->csspostprocess = 'theme_gourmet_process_css';
