<?php

   /**
		* Date Creation - 04/05/2014
		* Date Modification : 23/06/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
 
  class theme_gourmet_core_renderer extends theme_bootstrapbase_core_renderer {
 	
 	/*
     * This renders a notification message.
     * Uses bootstrap compatible html.
     */
    public function notification($message, $classes = 'notifyproblem') {
        $message = clean_text($message);
        $type = '';

        if ($classes == 'notifyproblem') {
            $type = 'alert alert-error';
        }
        if ($classes == 'notifysuccess') {
            $type = 'alert alert-success';
        }
        if ($classes == 'notifymessage') {
            $type = 'alert alert-info';
        }
        if ($classes == 'redirectmessage') {
            $type = 'alert alert-block alert-info';
        }
        return "<div class=\"$type\">$message</div>";
    } 
    
		
    protected function render_custom_menu(custom_menu $menu) {
    	/*
    	* This code replaces adds the current enrolled
    	* courses to the custommenu.
    	*/
    
    	$hasdisplaymycourses = (empty($this->page->theme->settings->displaymycourses)) ? false : $this->page->theme->settings->displaymycourses;
        if (isloggedin() && !isguestuser() && $hasdisplaymycourses) {
        	$mycoursetitle = $this->page->theme->settings->mycoursetitle;
            if ($mycoursetitle == 'module') {
				$branchtitle = get_string('mymodules', 'theme_gourmet');
			} else if ($mycoursetitle == 'unit') {
				$branchtitle = get_string('myunits', 'theme_gourmet');
			} else if ($mycoursetitle == 'class') {
				$branchtitle = get_string('myclasses', 'theme_gourmet');
			} else {
				$branchtitle = get_string('mycourses', 'theme_gourmet');
			}
			$branchlabel = '<i class="fa fa-briefcase"></i>'.$branchtitle;
            $branchurl   = new moodle_url('/my/index.php');
            $branchsort  = 10000;
 
            $branch = $menu->add($branchlabel, $branchurl, $branchtitle, $branchsort);
 			if ($courses = enrol_get_my_courses(NULL, 'fullname ASC')) {
 				foreach ($courses as $course) {
 					if ($course->visible){
 						$branch->add(format_string($course->fullname), new moodle_url('/course/view.php?id='.$course->id), format_string($course->shortname));
 					}
 				}
 			} else {
                $noenrolments = get_string('noenrolments', 'theme_gourmet');
 				$branch->add('<em>'.$noenrolments.'</em>', new moodle_url('/'), $noenrolments);
 			}
            
        }
        
        /*
    	* This code replaces adds the My Dashboard
    	* functionality to the custommenu.
    	*/
        $hasdisplaymydashboard = (empty($this->page->theme->settings->displaymydashboard)) ? false : $this->page->theme->settings->displaymydashboard;
        if (isloggedin() && !isguestuser() && $hasdisplaymydashboard) {
            $branchlabel = '<i class="fa fa-dashboard"></i>'.get_string('mydashboard', 'theme_gourmet');
            $branchurl   = new moodle_url('/my/index.php');
            $branchtitle = get_string('mydashboard', 'theme_gourmet');
            $branchsort  = 10000;
 
            $branch = $menu->add($branchlabel, $branchurl, $branchtitle, $branchsort);
            $branch->add('<em><i class="fa fa-home"></i>'.get_string('myhome').'</em>',new moodle_url('/my/index.php'),get_string('myhome'));
 			$branch->add('<em><i class="fa fa-user"></i>'.get_string('profile').'</em>',new moodle_url('/user/profile.php'),get_string('profile'));
 			$branch->add('<em><i class="fa fa-calendar"></i>'.get_string('pluginname', 'block_calendar_month').'</em>',new moodle_url('/calendar/view.php'),get_string('pluginname', 'block_calendar_month'));
 			$branch->add('<em><i class="fa fa-envelope"></i>'.get_string('pluginname', 'block_messages').'</em>',new moodle_url('/message/index.php'),get_string('pluginname', 'block_messages'));
 			$branch->add('<em><i class="fa fa-certificate"></i>'.get_string('badges').'</em>',new moodle_url('/badges/mybadges.php'),get_string('badges'));
 			$branch->add('<em><i class="fa fa-file"></i>'.get_string('privatefiles', 'block_private_files').'</em>',new moodle_url('/user/files.php'),get_string('privatefiles', 'block_private_files'));
 			$branch->add('<em><i class="fa fa-sign-out"></i>'.get_string('logout').'</em>',new moodle_url('/login/logout.php'),get_string('logout'));    
        }
         
        return parent::render_custom_menu($menu);
    }
    
    
    /**
    * Get the HTML for blocks in the given region.
    *
    * @since 2.5.1 2.6
    * @param string $region The region to get HTML for.
    * @return string HTML.
    * Written by G J Barnard
    */
    
    public function gourmetblocks($region, $classes = array(), $tag = 'aside') {
        $classes = (array)$classes;
        $classes[] = 'block-region';
        $attributes = array(
            'id' => 'block-region-'.preg_replace('#[^a-zA-Z0-9_\-]+#', '-', $region),
            'class' => join(' ', $classes),
            'data-blockregion' => $region,
            'data-droptarget' => '1'
        );
        return html_writer::tag($tag, $this->blocks_for_region($region), $attributes);
    }
	
	/**
     * Return the standard string that says whether you are logged in (and switched
     * roles/logged in as another user).
     * @param bool $withlinks if false, then don't include any links in the HTML produced.
     * If not set, the default is the nologinlinks option from the theme config.php file,
     * and if that is not set, then links are included.
     * @return string HTML fragment.
     */
	 
	 // this is overrided method of lib/ouputrenderes.php
	 
    public function login_info($withlinks = null) {  
        global $USER, $CFG, $DB, $SESSION;

        if (during_initial_install()) {
            return '';
        }

        if (is_null($withlinks)) {
            $withlinks = empty($this->page->layout_options['nologinlinks']);
        }

        $loginpage = ((string)$this->page->url === get_login_url());
        $course = $this->page->course;
        if (\core\session\manager::is_loggedinas()) {
            $realuser = \core\session\manager::get_realuser();
            $fullname = fullname($realuser, true);
            if ($withlinks) {
                $loginastitle = get_string('loginas','login');
                $realuserinfo = " [<a href=\"$CFG->wwwroot/course/loginas.php?id=$course->id&amp;sesskey=".sesskey()."\"";
                $realuserinfo .= "title =\"".$loginastitle."\">$fullname</a>] ";
            } else {
                $realuserinfo = " [$fullname] ";
            }
        } else {
            $realuserinfo = '';
        }

        $loginurl = get_login_url();

        if (empty($course->id)) {
            // $course->id is not defined during installation
            return '';
        } else if (isloggedin()) {
            $context = context_course::instance($course->id);

           //$userrole =  getUserRole($USER->id);
		   
            if( $USER->archetype == $CFG->userTypeAdmin ) {
			//if(in_array($userrole, $CFG->custommanagerroleid)){  // added by rajesh 
				$fullname = fullname($USER, true);
			}else{
				$fullname = $USER->firstname;
			}
            // Since Moodle 2.0 this link always goes to the public profile page (not the course profile page)
            if ($withlinks) {
                $linktitle = get_string('viewprofile');
                $username = "<a href=\"$CFG->wwwroot/user/profile.php?id=$USER->id\" title=\"$linktitle\">$fullname</a>";
            } else {
                $username = $fullname;
            }
            if (is_mnet_remote_user($USER) and $idprovider = $DB->get_record('mnet_host', array('id'=>$USER->mnethostid))) {
                if ($withlinks) {
                    $username .= " from <a href=\"{$idprovider->wwwroot}\">{$idprovider->name}</a>";
                } else {
                    $username .= " from {$idprovider->name}";
                }
            }
            if (isguestuser()) {
                $loggedinas = $realuserinfo.get_string('loggedinasguest');
                if (!$loginpage && $withlinks) {
                    $loggedinas .= " (<a href=\"$loginurl\">".get_string('login').'</a>)';
                }
            } else if (is_role_switched($course->id)) { // Has switched roles
                $rolename = '';
                if ($role = $DB->get_record('role', array('id'=>$USER->access['rsw'][$context->path]))) {
                    $rolename = ': '.role_get_name($role, $context);
                }
                $loggedinas = get_string('loggedinas', 'login', $username).$rolename;
                if ($withlinks) {
                    $url = new moodle_url('/course/switchrole.php', array('id'=>$course->id,'sesskey'=>sesskey(), 'switchrole'=>0, 'returnurl'=>$this->page->url->out_as_local_url(false)));
                    $loggedinas .= '('.html_writer::tag('a', get_string('switchrolereturn'), array('href'=>$url)).')';
                }
            } else {
                $loggedinas = $realuserinfo.get_string('loggedinas', 'login', $username);
                if ($withlinks) {
					//$userrole =  getUserRole($USER->id);
					if( $USER->archetype == $CFG->userTypeAdmin ) {
					//if(in_array($userrole, $CFG->custommanagerroleid)){  // added by rajesh 
					// $loggedinas .= " (<a href=\"$CFG->wwwroot/login/logout.php?sesskey=".sesskey()."\">".get_string('logout').'</a>)';
					}
                }
            }
        } else {
            //$loggedinas = get_string('loggedinnot', 'login');
            if (!$loginpage && $withlinks) {
               // $loggedinas .= " (<a href=\"$loginurl\">".get_string('login').'</a>)';
               $loggedinas .= " <a href=\"$loginurl\">".get_string('login').'</a>';
            }
        }

        $loggedinas = '<div class="logininfo">'.$loggedinas.'</div>';

        if (isset($SESSION->justloggedin)) {
            unset($SESSION->justloggedin);
            if (!empty($CFG->displayloginfailures)) {
                if (!isguestuser()) {
                    if ($count = count_login_failures($CFG->displayloginfailures, $USER->username, $USER->lastlogin)) {
                        $loggedinas .= '&nbsp;<div class="loginfailures">';
                        if (empty($count->accounts)) {
                            $loggedinas .= get_string('failedloginattempts', '', $count);
                        } else {
                            $loggedinas .= get_string('failedloginattemptsall', '', $count);
                        }
                        if (file_exists("$CFG->dirroot/report/log/index.php") and has_capability('report/log:view', context_system::instance())) {
                            $loggedinas .= ' (<a href="'.$CFG->wwwroot.'/report/log/index.php'.
                                                 '?chooselog=1&amp;id=1&amp;modid=site_errors">'.get_string('logs').'</a>)';
                        }
                        $loggedinas .= '</div>';
                    }
                }
            }
        }

        return $loggedinas;
    }
	
	
    
 }
 
global $USER;
//$userrole =  getUserRole($USER->id);
// this is using for admin and manager
if( $USER->archetype == $CFG->userTypeAdmin || $USER->archetype == $CFG->userTypeManager ) {
//if(in_array($userrole, array(1))){
 
  require_once($CFG->dirroot . "/course/renderer.php");
  class theme_gourmet_core_course_renderer extends core_course_renderer { 
  
     /**
     * Renders HTML to display particular course category - list of it's subcategories and courses
     *
     * Invoked from /course/index.php
     *
     * @param int|stdClass|coursecat $category
     */
	 
    public function course_category($category) { 
        global $CFG;
        require_once($CFG->libdir. '/coursecatlib.php'); 
		$CFG->libdir. '/coursecatlib.php';
        $coursecat = coursecat::get(is_object($category) ? $category->id : $category);
		
        $site = get_site();
        $output = '';

        $this->page->set_button($this->course_search_form('', 'navbar'));
		
        if (!$coursecat->id) {
            if (can_edit_in_category()) {
                // add 'Manage' button instead of course search form
                $managebutton = $this->single_button(new moodle_url('/course/management.php'), get_string('managecourses'), 'get');
                $this->page->set_button($managebutton);
            }
            if (coursecat::count_all() == 1) {
                // There exists only one category in the system, do not display link to it
                $coursecat = coursecat::get_default();
                $strfulllistofcourses = get_string('fulllistofcourses');
                $this->page->set_title("$site->fullname: $strfulllistofcourses");
            } else {
                $strcategories = get_string('categories');
                $this->page->set_title("$site->fullname: $strcategories");
            }
        } else {
            $this->page->set_title("$site->fullname: ". $coursecat->get_formatted_name());

            // Print the category selector
            $output .= html_writer::start_tag('div', array('class' => 'categorypicker'));
            $select = new single_select(new moodle_url('/course/index.php'), 'categoryid',
                    coursecat::make_categories_list(), $coursecat->id, null, 'switchcategory');
            $select->set_label(get_string('categories').':');
            $output .= $this->render($select);
            $output .= html_writer::end_tag('div'); // .categorypicker
        }

        // Print current category description
        $chelper = new coursecat_helper();
        if ($description = $chelper->get_category_formatted_description($coursecat)) {
            $output .= $this->box($description, array('class' => 'generalbox info'));
        }

        // Prepare parameters for courses and categories lists in the tree
        $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_AUTO)
                ->set_attributes(array('class' => 'category-browse category-browse-'.$coursecat->id));

        $coursedisplayoptions = array();
        $catdisplayoptions = array();
        $browse = optional_param('browse', null, PARAM_ALPHA);
        $perpage = optional_param('perpage', $CFG->coursesperpage, PARAM_INT);
        $page = optional_param('page', 0, PARAM_INT);
        $baseurl = new moodle_url('/course/index.php');
        if ($coursecat->id) {
            $baseurl->param('categoryid', $coursecat->id);
        }
        if ($perpage != $CFG->coursesperpage) {
            $baseurl->param('perpage', $perpage);
        }
        $coursedisplayoptions['limit'] = $perpage;
        $catdisplayoptions['limit'] = $perpage;
        if ($browse === 'courses' || !$coursecat->has_children()) {
            $coursedisplayoptions['offset'] = $page * $perpage;
            $coursedisplayoptions['paginationurl'] = new moodle_url($baseurl, array('browse' => 'courses'));
            $catdisplayoptions['nodisplay'] = true;
            $catdisplayoptions['viewmoreurl'] = new moodle_url($baseurl, array('browse' => 'categories'));
            $catdisplayoptions['viewmoretext'] = new lang_string('viewallsubcategories');
        } else if ($browse === 'categories' || !$coursecat->has_courses()) {
            $coursedisplayoptions['nodisplay'] = true;
            $catdisplayoptions['offset'] = $page * $perpage;
            $catdisplayoptions['paginationurl'] = new moodle_url($baseurl, array('browse' => 'categories'));
            $coursedisplayoptions['viewmoreurl'] = new moodle_url($baseurl, array('browse' => 'courses'));
            $coursedisplayoptions['viewmoretext'] = new lang_string('viewallcourses');
        } else {
            // we have a category that has both subcategories and courses, display pagination separately
            $coursedisplayoptions['viewmoreurl'] = new moodle_url($baseurl, array('browse' => 'courses', 'page' => 1));
            $catdisplayoptions['viewmoreurl'] = new moodle_url($baseurl, array('browse' => 'categories', 'page' => 1));
        }
        $chelper->set_courses_display_options($coursedisplayoptions)->set_categories_display_options($catdisplayoptions);

        // Display course category tree
        $output .= $this->coursecat_tree($chelper, $coursecat);

        // Add course search form (if we are inside category it was already added to the navbar)
        if (!$coursecat->id) {
            $output .= $this->course_search_form();
        }

        // Add action buttons
        $output .= $this->container_start('buttons');
        $context = get_category_or_system_context($coursecat->id);
        if (has_capability('moodle/course:create', $context)) {
            // Print link to create a new course, for the 1st available category.
            if ($coursecat->id) {
                $url = new moodle_url('/course/edit.php', array('category' => $coursecat->id, 'returnto' => 'course')); // change only  'returnto' => 'category' to 'returnto' => 'course'
            } else {
                $url = new moodle_url('/course/edit.php', array('category' => $CFG->defaultrequestcategory, 'returnto' => 'topcat'));
            }
            $output .= $this->single_button($url, get_string('addnewcourse'), 'get');
        }
        ob_start();
        if (coursecat::count_all() == 1) {
            print_course_request_buttons(context_system::instance());
        } else {
            print_course_request_buttons($context);
        }
        $output .= ob_get_contents();
        ob_end_clean();
        $output .= $this->container_end();

        return $output;
    }
	

		  /**
		 * Returns HTML to display course content (summary, course contacts and optionally category name)
		 *
		 * This method is called from coursecat_coursebox() and may be re-used in AJAX
		 *
		 * @param coursecat_helper $chelper various display options
		 * @param stdClass|course_in_list $course
		 * @return string
		 */
		protected function coursecat_coursebox_content(coursecat_helper $chelper, $course) {
			global $CFG;
			if ($chelper->get_show_courses() < self::COURSECAT_SHOW_COURSES_EXPANDED) {
				return '';
			}
			if ($course instanceof stdClass) {
				require_once($CFG->libdir. '/coursecatlib.php');
				$course = new course_in_list($course);
			}
			$content = '';

			// display course summary
			if ($course->has_summary()) {
				$content .= html_writer::start_tag('div', array('class' => 'summary'));
				$content .= $chelper->get_course_formatted_summary($course,
						array('overflowdiv' => true, 'noclean' => true, 'para' => false));
	
				$content .= html_writer::end_tag('div'); // .summary
			}
			// Check the course module exists.
		
			$courseFormatType = getCourseFormat($course->id);
			if($courseFormatType == 'singleactivity'){
				$courseFormat = getCourseFormatOptions($course->id, $format = 'singleactivity', $activitytypename = 'activitytype');
			}else{
				$courseFormat = $courseFormatType;
			}
            $moduleId = getModuleId($course->id);

			/* added by rajesh */
			
			$resourceOrScormid = getScormOrResourceId($course->id, $moduleId);
			//echo "resourceOrScormid==$resourceOrScormid<br>";
			$content .= html_writer::start_tag('div', array('class' => 'courseaction'));
			$content .= html_writer::tag('a',get_string('enrolledgroups','course'), array('href'=>$CFG->wwwroot.'/enrol/groups.php?courseid='.$course->id, 'class'=>'courseactionenrol'));
			$content .= " ".html_writer::tag('a',get_string('enrolledusers','course'), array('href'=>$CFG->wwwroot.'/enrol/users.php?id='.$course->id, 'class'=>'courseactionenrol'));
			
			if($courseFormat == 'resource'){
			
				if($resourceOrScormid){
				  $content .= " ".html_writer::tag('a',get_string('updatefile','course'), array('href'=>$CFG->wwwroot.'/course/modedit.php?update='.$resourceOrScormid.'&return=1', 'class'=>'courseactionenrol')); 
				}else{
				  $content .= " ".html_writer::tag('a',get_string('addafile','course'), array('href'=>$CFG->wwwroot.'/course/view.php?id='.$course->id, 'class'=>'courseactionenrol')); 
				}


			}elseif($courseFormat == 'scorm'){
				if($resourceOrScormid){
				  $content .= " ".html_writer::tag('a',get_string('updatescrom','course'), array('href'=>$CFG->wwwroot.'/course/modedit.php?update='.$resourceOrScormid.'&return=1', 'class'=>'courseactionenrol')); 
				}else{
				  $content .= " ".html_writer::tag('a',get_string('updatescrom','course'), array('href'=>$CFG->wwwroot.'/course/view.php?id='.$course->id, 'class'=>'courseactionenrol')); 
				}
			}elseif($courseFormat == 'topics'){
				  $content .= " ".html_writer::tag('a',get_string('managetopics','course'), array('href'=>$CFG->wwwroot.'/course/view.php?id='.$course->id.'&sesskey='.sesskey().'&edit=1', 'class'=>'courseactionenrol')); 
			}
			$content .= " ".html_writer::tag('a',get_string('edit','course'), array('href'=>'edit.php?id='.$course->id, 'class'=>'courseactionedit'));
			$content .= " ".html_writer::tag('a',get_string('delete','course'), array('href'=>$CFG->wwwroot.'/course/delete.php?id='.$course->id, 'class'=>'courseactionenrol'));
			$content .= html_writer::end_tag('div'); // .summary
			// end
				
	
			// display course overview files
			$contentimages = $contentfiles = '';
			foreach ($course->get_course_overviewfiles() as $file) {
				$isimage = $file->is_valid_image();
				$url = file_encode_url("$CFG->wwwroot/pluginfile.php",
						'/'. $file->get_contextid(). '/'. $file->get_component(). '/'.
						$file->get_filearea(). $file->get_filepath(). $file->get_filename(), !$isimage);
				if ($isimage) {
					$contentimages .= html_writer::tag('div',
							html_writer::empty_tag('img', array('src' => $url)),
							array('class' => 'courseimage'));
				} else {
					$image = $this->output->pix_icon(file_file_icon($file, 24), $file->get_filename(), 'moodle');
					$filename = html_writer::tag('span', $image, array('class' => 'fp-icon')).
							html_writer::tag('span', $file->get_filename(), array('class' => 'fp-filename'));
					$contentfiles .= html_writer::tag('span',
							html_writer::link($url, $filename),
							array('class' => 'coursefile fp-filename-icon'));
				}
			}
			$content .= $contentimages. $contentfiles;
	
			// display course contacts. See course_in_list::get_course_contacts()
			if ($course->has_course_contacts()) {
				$content .= html_writer::start_tag('ul', array('class' => 'teachers'));
				foreach ($course->get_course_contacts() as $userid => $coursecontact) {
					$name = $coursecontact['rolename'].': '.
							html_writer::link(new moodle_url('/user/view.php',
									array('id' => $userid, 'course' => SITEID)),
								$coursecontact['username']);
					$content .= html_writer::tag('li', $name);
				}
				$content .= html_writer::end_tag('ul'); // .teachers
			}
	
			// display course category if necessary (for example in search results)
			if ($chelper->get_show_courses() == self::COURSECAT_SHOW_COURSES_EXPANDED_WITH_CAT) {
				require_once($CFG->libdir. '/coursecatlib.php');
				if ($cat = coursecat::get($course->category, IGNORE_MISSING)) {
					$content .= html_writer::start_tag('div', array('class' => 'coursecat'));
					$content .= get_string('category').': '.
							html_writer::link(new moodle_url('/course/index.php', array('categoryid' => $cat->id)),
									$cat->get_formatted_name(), array('class' => $cat->visible ? '' : 'dimmed'));
					$content .= html_writer::end_tag('div'); // .coursecat
				}
			}
	
			return $content;
		}
	
		
	}
	
	require_once($CFG->dirroot . "/enrol/renderer.php");
    class theme_gourmet_core_enrol_renderer extends core_enrol_renderer { 
	
		/**
		 * Generates the HTML for the given enrolments + available actions
		 *
		 * @param int $userid
		 * @param array $enrolments
		 * @param moodle_url $pageurl
		 * @return string
		 */
		public function user_enrolments_and_actions($enrolments, $assignGroupsForUser) {
			$output = '';
			foreach ($enrolments as $ue) {
				$enrolmentoutput = $ue['text'].' '.$ue['period'];
				if ($ue['dimmed']) {
					$enrolmentoutput = html_writer::tag('span', $enrolmentoutput, array('class'=>'dimmed_text'));
				} else {
					$enrolmentoutput = html_writer::tag('span', $enrolmentoutput);
				}
				
				foreach ($ue['actions'] as $action) {
				
				   $arr = $action->get_attributes();
				   
				   if($assignGroupsForUser == '---' ){
					  $enrolmentoutput .= $this->render($action);
				   }else{
				     if($arr['title'] == 'Edit'){
					   $enrolmentoutput .= $this->render($action);
					 }
				   }	
				}
				$output .= html_writer::tag('div', $enrolmentoutput, array('class'=>'enrolment'));
			}
			return $output;
		}
	
 
	
		public function render_course_enrolment_users_table(course_enrolment_users_table $table,
				moodleform $mform) {
	
	        global $CFG;
			$table->initialise_javascript();
	
			$buttons = $table->get_manual_enrol_buttons();
			$buttonhtml = '';
			if (count($buttons) > 0) {
				$buttonhtml .= html_writer::start_tag('div', array('class' => 'enrol_user_buttons'));
				$buttonhtml .= '<input type="button" value="'.get_string('backtomanagecourses','course').'" name="backtomanagecourses" onclick="window.location.href=\''.$CFG->wwwroot.'/course/index.php\'" >&nbsp;&nbsp;&nbsp;';
				foreach ($buttons as $button) {
					$buttonhtml .= $this->render($button);
				}
				$buttonhtml .= html_writer::end_tag('div');
			}
	
			$content = '';
			if (!empty($buttonhtml)) {
				$content .= $buttonhtml;
			}
			$content .= $mform->render();
	
			$content .= $this->output->render($table->get_paging_bar());
	
			// Check if the table has any bulk operations. If it does we want to wrap the table in a
			// form so that we can capture and perform any required bulk operations.
			if ($table->has_bulk_user_enrolment_operations()) {
				$content .= html_writer::start_tag('form', array('action' => new moodle_url('/enrol/bulkchange.php'), 'method' => 'post'));
				foreach ($table->get_combined_url_params() as $key => $value) {
					if ($key == 'action') {
						continue;
					}
					$content .= html_writer::empty_tag('input', array('type' => 'hidden', 'name' => $key, 'value' => $value));
				}
				$content .= html_writer::empty_tag('input', array('type' => 'hidden', 'name' => 'action', 'value' => 'bulkchange'));
				$content .= html_writer::table($table);
				$content .= html_writer::start_tag('div', array('class' => 'singleselect bulkuserop'));
				$content .= html_writer::start_tag('select', array('name' => 'bulkuserop'));
				$content .= html_writer::tag('option', get_string('withselectedusers', 'enrol'), array('value' => ''));
				$options = array('' => get_string('withselectedusers', 'enrol'));
				foreach ($table->get_bulk_user_enrolment_operations() as $operation) {
					$content .= html_writer::tag('option', $operation->get_title(), array('value' => $operation->get_identifier()));
				}
				$content .= html_writer::end_tag('select');
				$content .= html_writer::empty_tag('input', array('type' => 'submit', 'value' => get_string('go')));
				$content .= html_writer::end_tag('div');
	
				$content .= html_writer::end_tag('form');
			} else {
				$content .= html_writer::table($table);
			}
			$content .= $this->output->render($table->get_paging_bar());
			if (!empty($buttonhtml)) {
				$content .= $buttonhtml;
			}
			
			return $content;
		}
		
	
  } //end class

}

if( $USER->archetype == $CFG->userTypeStudent || $USER->archetype == $CFG->userTypeAdmin || $USER->archetype == $CFG->userTypeManager ) {
//if(in_array($userrole, array(1, 3, 5))){
  
  require_once($CFG->dirroot . "/calendar/renderer.php");
  
 
  class theme_gourmet_core_calendar_renderer extends core_calendar_renderer { 
  
         
  
		  public function show_upcoming_events(calendar_information $calendar, $futuredays, $maxevents, moodle_url $returnurl = null) {
	
			if ($returnurl === null) {
				$returnurl = $this->page->url;
			}
		
			$events = calendar_get_upcoming($calendar->courses, $calendar->groups, $calendar->users, $futuredays, $maxevents);
	
			$output  = html_writer::start_tag('div', array('class'=>'header upcoming-events'));
			if (calendar_user_can_add_event($calendar->course)) {
				$output .= $this->add_event_button($calendar->course->id);
			}
			$output .= html_writer::tag('label', get_string('upcomingevents', 'calendar'), array('for'=>'cal_course_flt_jump', 'style'=>'font-size:19px'));
			 //$output .= $this->course_filter_selector($returnurl);
			$output .= html_writer::end_tag('div');
			$output .= '<div class="clear"></div>';
			
			$output .= '<div class="legend-container" style="margin:3% 0.5% 0 !IMPORTANT"><div class="legend-classroom"><span></span>'.get_string('classroom','calendar').'</div><div class="legend-course"><span></span>'.get_string('online','calendar').'</div><div class="legend-global"><span></span>'.get_string('global','calendar').'</div><div class="legend-user"><span></span>'.get_string('user','calendar').'</div></div><div class = "clear"></div>';
	
			$output .= '<div class="clear"></div>';
			if ($events) {
				$output .= html_writer::start_tag('div', array('class'=>'eventlist scroll-pane'));
				foreach ($events as $event) {
					// Convert to calendar_event object so that we transform description
					// accordingly
					$event = new calendar_event($event);
					$event->calendarcourseid = $calendar->courseid;
					$output .= $this->event($event);
				}
				$output .= html_writer::end_tag('div');
			} else {
				$output .= html_writer::tag('p',get_string('noupcomingevents', 'calendar'));
			}
	
			return $output;
		}
		
		/**
		 * Creates a button to add a new event
		 *
		 * @param int $courseid
		 * @param int $day
		 * @param int $month
		 * @param int $year
		 * @param int $time the unixtime, used for multiple calendar support. The values $day,
		 *     $month and $year are kept for backwards compatibility.
		 * @return string
		 */
		protected function add_event_button($courseid, $day = null, $month = null, $year = null, $time = null) {
			// If a day, month and year were passed then convert it to a timestamp. If these were passed
			// then we can assume the day, month and year are passed as Gregorian, as no where in core
			// should we be passing these values rather than the time. This is done for BC.
			if (!empty($day) && !empty($month) && !empty($year)) {
				if (checkdate($month, $day, $year)) {
					$time = make_timestamp($year, $month, $day);
				} else {
					$time = time();
				}
			} else if (empty($time)) {
				$time = time();
			}
			$output = html_writer::start_tag('div', array('class'=>'buttons'));
			$output .= html_writer::start_tag('form', array('action' => LOCAL_CALENDAR_URL . 'event.php', 'method' => 'get'));
			$output .= html_writer::start_tag('div');
			$output .= html_writer::empty_tag('input', array('type'=>'hidden', 'name' => 'action', 'value' => 'new'));
			//$output .= html_writer::empty_tag('input', array('type'=>'hidden', 'name' => 'course', 'value' => $courseid));
			$output .= html_writer::empty_tag('input', array('type'=>'hidden', 'name' => 'time', 'value' => time()));
			$output .= html_writer::empty_tag('input', array('type'=>'submit', 'value' => get_string('addevent', 'event'), 'title'=>get_string('addevent', 'event')));
			$output .= html_writer::end_tag('div');
			$output .= html_writer::end_tag('form');
			$output .= html_writer::end_tag('div');
			return $output;
		}
		
		/**
		 * Displays an event
		 *
		 * @param calendar_event $event
		 * @param bool $showactions
		 * @return string
		 */
		public function event(calendar_event $event, $showactions=true) {
			global $CFG, $USER, $DB;
	
			$event = calendar_add_event_metadata($event);
			if($event->courseid){
				$cTypeId = getCourseTypeIdByCourseId($event->courseid);
				$event->coursetype_id = $cTypeId;
			}
			$context = $event->context;
	
			$anchor  = html_writer::tag('a', '', array('name'=>'event_'.$event->id));
	
			$table = new html_table();
            
			
			if($event->eventtype == 'global'){
				$table->attributes = array('class'=>'event global-event', 'cellspacing'=>'0', 'cellpadding'=>'0', 'border'=>'0');
			}else if($event->eventtype == 'user'){
				if($event->sessionid != 0){
					$table->attributes = array('class'=>'event classroom-event', 'cellspacing'=>'0', 'cellpadding'=>'0', 'border'=>'0');
				}else{
					$table->attributes = array('class'=>'event user-event', 'cellspacing'=>'0', 'cellpadding'=>'0', 'border'=>'0');
				}
			}else if($event->eventtype == 'course'){
				if($event->coursetype_id == $CFG->courseTypeOnline){
				    $table->attributes = array('class'=>'event course-event', 'cellspacing'=>'0', 'cellpadding'=>'0', 'border'=>'0');
				}else{
					$table->attributes = array('class'=>'event classroom-event', 'cellspacing'=>'0', 'cellpadding'=>'0', 'border'=>'0');
				}
				//$table->attributes = array('class'=>'event course-event', 'cellspacing'=>'0', 'cellpadding'=>'0', 'border'=>'0');
			}

			$table->data = array(
				0 => new html_table_row(),
				1 => new html_table_row(),
			);
	
	        
			/*
			if (!empty($event->icon)) {
				$table->data[0]->cells[0] = new html_table_cell($anchor.$event->icon);
			} else {
				$table->data[0]->cells[0] = new html_table_cell($anchor.$this->output->spacer(array('height'=>16, 'width'=>16, 'br'=>true)));
			}
			$table->data[0]->cells[0]->attributes['class'] .= ' picture';
			*/

			$table->data[0]->cells[1] = new html_table_cell();
			
			
			
			if($event->eventtype == 'course'){
				
				$table->data[0]->cells[1]->attributes['class'] .= $event->coursetype_id == $CFG->courseTypeOnline?'topic calendar_event_course':'topic calendar_event_classroom';
				//$table->data[0]->cells[1]->attributes['class'] .= ' topic calendar_event_course';
			}else if($event->eventtype == 'global'){
				$table->data[0]->cells[1]->attributes['class'] .= ' topic calendar_event_global';
			}else if($event->eventtype == 'user'){
				if($event->sessionid != 0){
					$table->data[0]->cells[1]->attributes['class'] .= ' topic calendar_event_classroom';
				}else{
					$table->data[0]->cells[1]->attributes['class'] .= ' topic calendar_event_user';
				}
			}else{
				$table->data[0]->cells[1]->attributes['class'] .= ' topic';
			}
			if (!empty($event->time)) {
				$table->data[0]->cells[1]->text .= html_writer::tag('span', $event->time, array('class'=>'date'));
			} else {
				$table->data[0]->cells[1]->text .= html_writer::tag('span', calendar_time_representation($event->timestart), array('class'=>'date'));
			}
			if (!empty($event->referer)) {
				$table->data[0]->cells[1]->text .= html_writer::tag('div', $event->referer, array('class'=>'referer'));
			} else {
				$table->data[0]->cells[1]->text .= html_writer::tag('div', format_string($event->name, false, array('context' => $context)), array('class'=>'name'));
			}
			
			
			
			if(isset($event->createdby) && $event->createdby){
			  $table->data[0]->cells[1]->text .= html_writer::tag('div', '<strong>'.get_string('createdby').': </strong>'.$event->createdby_name." (".$event->createdby_username.")", array('class'=>'name'));
			}
			
			if(isset($event->class_location) && $event->class_location){
			  $table->data[0]->cells[1]->text .= html_writer::tag('div', '<strong>'.get_string('location', 'scheduler').': </strong>'.$event->class_location, array('class'=>'name'));
			}else{
			
			 	if(isset($event->class_web_access_link) && $event->class_web_access_link){
			       $table->data[0]->cells[1]->text .= html_writer::tag('div', '<strong>'.get_string('location', 'scheduler').': </strong>'.strip_tags($event->class_web_access_link), array('class'=>'name'));
			   }
			}
			
			if(isset($event->instructor) && $event->instructor){
			   $table->data[0]->cells[1]->text .= html_writer::tag('div', '<strong>'.get_string('instructor','scheduler').': </strong>'.$event->instructor, array('class'=>'name'));
			}
			
			
			if (!empty($event->courselink)) {
		
				if($USER->archetype == $CFG->userTypeStudent){
				    $table->data[0]->cells[1]->text .= html_writer::tag('div', $event->courselink, array('class'=>'course'));
				}else{
				
				    if($event->courseid){
						$fullname = getCourses($event->courseid);
						$url = new moodle_url('/course/course_preview.php', array('id' => $event->courseid));
						$courselink = html_writer::link($url, $fullname);
						$event->courselink = $courselink;
						$table->data[0]->cells[1]->text .= html_writer::tag('div', $event->courselink, array('class'=>'course'));
					}
				}
			}else{ // incase of classroom
			
			    if($event->sessionid){
			     $classObj = $DB->get_record_sql("SELECT ms.course, ms.id as classid, mc.fullname as coursename, ms.name FROM mdl_scheduler ms INNER JOIN mdl_scheduler_slots mss ON (ms.id = mss.schedulerid) INNER JOIN mdl_course mc ON (mc.id = ms.course) where mss.id = '".$event->sessionid."'");
				}
				
				

				if(isset($classObj) && count($classObj) > 0 ){
				  $classCourseId = $classObj->course;
				  $classId = $classObj->classid;
				  $courseName = $classObj->coursename;
				  $className = $classObj->name;
				  
				  $isInstructor = isSecondaryInstructorOfClass($USER->id, $classId);
				 
				
				  if($USER->archetype == $CFG->userTypeStudent){
				     
					 if($isInstructor){
				       //$url = new moodle_url('/mod/scheduler/list.php', array('id' => $classCourseId,'key' => $className));
					   $url = new moodle_url('/course/classroom_preview.php', array('id' => $classCourseId,'classId' => $classId));
					 }else{
					   $url = new moodle_url('/course/classroom_preview.php', array('id' => $classCourseId,'classId' => $classId));
					 }
					 
				   }else{
				  
				     if($isInstructor){
				        //$url = new moodle_url('/mod/scheduler/list.php', array('id' => $classCourseId,'key' => $className));
						$url = new moodle_url('/course/classroom_preview.php', array('id' => $classCourseId,'classId' => $classId));
					 }else{
					   $url = new moodle_url('/course/classroom_preview.php', array('id' => $classCourseId,'classId' => $classId));
					 }
				   }
				  $formatedName =  format_string($courseName, false, array('context' => $context));
				  $clscourselink = html_writer::link($url, $formatedName);
				  $table->data[0]->cells[1]->text .= html_writer::tag('div', $clscourselink, array('class'=>'name'));
			    }
				
			
				
			}
			// Show subscription source if needed.
			if (!empty($event->subscription) && $CFG->calendar_showicalsource) {
				if (!empty($event->subscription->url)) {
					$source = html_writer::link($event->subscription->url, get_string('subsource', 'calendar', $event->subscription));
				} else {
					// File based ical.
					$source = get_string('subsource', 'calendar', $event->subscription);
				}
				$table->data[0]->cells[1]->text .= html_writer::tag('div', $source, array('class' => 'subscription'));
			}
	
			/*
			$table->data[1]->cells[0] = new html_table_cell('&nbsp;');
			$table->data[1]->cells[0]->attributes['class'] .= 'side';
			*/

			$table->data[1]->cells[1] = new html_table_cell(format_text($event->description, $event->format, array('context' => $context)));
			$table->data[1]->cells[1]->attributes['class'] .= ' description';
			if (isset($event->cssclass)) {
				if($event->eventtype == 'course'){
					//$event->cssclass = ' calendar_event_course';
					$event->cssclass =  $event->coursetype_id == $CFG->courseTypeOnline?' calendar_event_course':' calendar_event_classroom';
				}else if($event->eventtype == 'global'){
					$event->cssclass = ' calendar_event_global';
				}else if($event->eventtype == 'user'){
					if($event->sessionid != 0){
						$event->cssclass = ' calendar_event_classroom';
					}else{
						$event->cssclass = ' calendar_event_user';
					}
				}else{
					$event->cssclass = ' topic';
				}
				$table->data[1]->cells[1]->attributes['class'] .= ' '.$event->cssclass;
			}
			
			if (calendar_edit_event_allowed($event) && $showactions) {
			
			  if(isset($event->sessionid) && $event->sessionid){
			  }else{
				if (empty($event->cmid)) {
					$editlink = new moodle_url(LOCAL_CALENDAR_URL.'event.php', array('action'=>'edit', 'id'=>$event->id));
					if($USER->id == $event->userid || $USER->archetype == $CFG->userTypeAdmin){
						$deletelink = new moodle_url(LOCAL_CALENDAR_URL.'delete.php', array('id'=>$event->id));
					}else{
						$deletelink = null;
					}
					if (!empty($event->calendarcourseid)) {
						//$editlink->param('course', $event->calendarcourseid);
						//$deletelink->param('course', $event->calendarcourseid);
					}
				} else {
					$editlink = new moodle_url('/course/mod.php', array('update'=>$event->cmid, 'return'=>true, 'sesskey'=>sesskey()));
					$deletelink = null;
				}
			
			 }	
				

				///// Provide full access (Update/ Delete all events) to Admin/ Superadmin and partial access (Update/ Delete events only those created by user) to Learner - By Madhab /////
				if ( ($USER->archetype != $CFG->userTypeStudent) || ($USER->id == $event->userid) ) {	
				
				
					//$commands .= html_writer::empty_tag('img', array('src'=>$this->output->pix_url('t/edit'), 'alt'=>get_string('tt_editevent', 'calendar'), 'title'=>get_string('tt_editevent', 'calendar')));
					 if(isset($event->sessionid) && $event->sessionid){
					 }else{
					 
					 
							$commands  = html_writer::start_tag('div', array('class'=>'commands'));
							$commands .= html_writer::start_tag('a', array('href'=>$editlink, 'class'=>'edit', 'title'=>get_string('tt_editevent', 'calendar')));
							$commands .= html_writer::end_tag('a');
					
							if ($deletelink != null) {
								$commands .= html_writer::start_tag('a', array('href'=>$deletelink, 'class'=>'delete', 'title'=>get_string('tt_deleteevent', 'calendar')));
								//$commands .= html_writer::empty_tag('img', array('src'=>$this->output->pix_url('t/delete'), 'alt'=>get_string('tt_deleteevent', 'calendar'), 'title'=>get_string('tt_deleteevent', 'calendar')));
								$commands .= html_writer::end_tag('a');
							}
							$commands .= html_writer::end_tag('div');
					}
					
					if($USER->id == $event->userid || $USER->archetype == $CFG->userTypeAdmin){
					  $table->data[1]->cells[1]->text .= $commands;
					}
				}
			}
			

			$outputEventBlock = $anchor.html_writer::table($table) . html_writer::start_tag('div', array('class'=>'event-spacer')) . html_writer::end_tag('div');

			return $outputEventBlock;
			//return html_writer::table($table);
		}
		
		  /**
		 * Displays a month in detail
		 *
		 * @param calendar_information $calendar
		 * @param moodle_url $returnurl the url to return to
		 * @return string
		 */
		public function show_month_detailed(calendar_information $calendar, moodle_url $returnurl  = null) {
			global $CFG;
			$eventsByDate = array();
			$eventTypeArr = array($CFG->courseEvent."Event", $CFG->globalEvent."Event", $CFG->userEvent."Event");
	
			if (empty($returnurl)) {
				$returnurl = $this->page->url;
			}
	
			// Get the calendar type we are using.
			$calendartype = \core_calendar\type_factory::get_calendar_instance();
	
			// Store the display settings.
			$display = new stdClass;
			$display->thismonth = false;
	
			// Get the specified date in the calendar type being used.
			$date = $calendartype->timestamp_to_date_array($calendar->time);
			$thisdate = $calendartype->timestamp_to_date_array(time());
			if ($date['mon'] == $thisdate['mon'] && $date['year'] == $thisdate['year']) {
				$display->thismonth = true;
				$date = $thisdate;
				$calendar->time = time();
			}
	
			// Get Gregorian date for the start of the month.
			$gregoriandate = $calendartype->convert_to_gregorian($date['year'], $date['mon'], 1);
			// Store the gregorian date values to be used later.
			list($gy, $gm, $gd, $gh, $gmin) = array($gregoriandate['year'], $gregoriandate['month'], $gregoriandate['day'],
				$gregoriandate['hour'], $gregoriandate['minute']);
	
			// Get the starting week day for this month.
			$startwday = dayofweek(1, $date['mon'], $date['year']);
			// Get the days in a week.
			$daynames = calendar_get_days();
			// Store the number of days in a week.
			$numberofdaysinweek = $calendartype->get_num_weekdays();
	
			$display->minwday = calendar_get_starting_weekday();
			$display->maxwday = $display->minwday + ($numberofdaysinweek - 1);
			$display->maxdays = calendar_days_in_month($date['mon'], $date['year']);
	
			// These are used for DB queries, so we want unixtime, so we need to use Gregorian dates.
			$display->tstart = make_timestamp($gy, $gm, $gd, $gh, $gmin, 0);
			$display->tend = $display->tstart + ($display->maxdays * DAYSECS) - 1;
	
			// Align the starting weekday to fall in our display range
			// This is simple, not foolproof.
			if ($startwday < $display->minwday) {
				$startwday += $numberofdaysinweek;
			}
	
			// Get events from database
			$events = calendar_get_events($display->tstart, $display->tend, $calendar->users, $calendar->groups, $calendar->courses);
			if (!empty($events)) {
				foreach($events as $eventid => $event) {
					$event = new calendar_event($event);
					if (!empty($event->modulename)) {
						$cm = get_coursemodule_from_instance($event->modulename, $event->instance);
						if (!groups_course_module_visible($cm)) {
							unset($events[$eventid]);
						}
					}
				}
			}

			// Extract information: events vs. time
			// $eventsBydate is added by rajesh in below function to get the events by date and event type
			calendar_events_by_day($events, $date['mon'], $date['year'], $eventsbyday, $durationbyday, $typesbyday, $calendar->courses, $eventsByDate);
				
	
				// added by rajesh to get the events date and event type
				
				/*if (isset($eventsbyday) && count($eventsbyday) > 0) {
				   
				   foreach($eventsbyday as $eDate => $eventIdArr) {
				   
						if(isset($events) && count($events) > 0){
							$courseEventArr = array();
							$globalEventArr = array();
							$userEventArr = array();
							foreach($events as $eVal){
							   
							   $eID = $eVal->id;
							   if(in_array($eID, $eventIdArr)){ 
								   $eventType = $eVal->eventtype; 
								   if($eventType == $CFG->courseEvent){
									 $courseEventArr[$eID] = $eVal;
									 $eventsByDate[$eDate]['courseEvent'][] = $eID;
								   }elseif($eventType == $CFG->globalEvent){
									 $globalEventArr[$eID] = $eVal;
									 $eventsByDate[$eDate]['globalEvent'][] = $eID;
								   }elseif($eventType == $CFG->userEvent){
									 $userEventArr[$eID] = $eVal;
									 $eventsByDate[$eDate]['userEvent'][] = $eID;
								   }
							   }
							   
							}
						}
				   }		
				}*/

				// end 
				//pr($eventsbyday);
			//if(basename($_SERVER["PHP_SELF"]) != 'learnerdashboard.php'){
				$output  = html_writer::start_tag('div', array('class'=>'header'));
				if (calendar_user_can_add_event($calendar->course)) {
					$output .= $this->add_event_button($calendar->course->id, 0, 0, 0, $calendar->time); 
				}
				//$output .= get_string('detailedmonthview', 'calendar').': '.$this->course_filter_selector($returnurl);
				$output .= html_writer::end_tag('div', array('class'=>'header'));
			//}
			// Controls
			$output .= html_writer::tag('div', calendar_top_controls('month', array('id' => $calendar->courseid, 'time' => $calendar->time)), array('class' => 'controls'));

			$output .= '<div class="legend-container"><div class="legend-classroom"><span></span>'.get_string('classroom','calendar').'</div><div class="legend-course"><span></span>'.get_string('online','calendar').'</div><div class="legend-global"><span></span>'.get_string('global','calendar').'</div><div class="legend-user"><span></span>'.get_string('user','calendar').'</div></div><div class = "clear"></div>';

			$output .= html_writer::start_tag('div', array('class'=>'eventlist scroll-pane'));
	
			$table = new html_table();
			$table->attributes = array('class'=>'calendarmonth calendartable');
			$table->summary = get_string('calendarheading', 'calendar', userdate($calendar->time, get_string('strftimemonthyear')));
			$table->data = array();
	
			// Get the day names as the header.
			$header = array();
			for($i = $display->minwday; $i <= $display->maxwday; ++$i) {
				$header[] = $daynames[$i % $numberofdaysinweek]['shortname'];
			}
			$table->head = $header;
	
			// For the table display. $week is the row; $dayweek is the column.
			$week = 1;
			$dayweek = $startwday;
	
			$row = new html_table_row(array());
	
			// Paddding (the first week may have blank days in the beginning)
			for($i = $display->minwday; $i < $startwday; ++$i) {
				$cell = new html_table_cell('&nbsp;');
				$cell->attributes = array('class'=>'nottoday dayblank');
				$row->cells[] = $cell;
			}
	
			// Now display all the calendar
			$weekend = CALENDAR_DEFAULT_WEEKEND;
			if (isset($CFG->calendar_weekend)) {
				$weekend = intval($CFG->calendar_weekend);
			}
	
			$daytime = $display->tstart - DAYSECS;
			for ($day = 1; $day <= $display->maxdays; ++$day, ++$dayweek) {
				$daytime = $daytime + DAYSECS;
				if($dayweek > $display->maxwday) {
					// We need to change week (table row)
					$table->data[] = $row;
					$row = new html_table_row(array());
					$dayweek = $display->minwday;
					++$week;
				}
	
				// Reset vars
				$cell = new html_table_cell();

				// Added by Madhab //
				$dashboardPageCheck = basename($_SERVER['PHP_SELF']);
				if($dashboardPageCheck == $CFG->pageDashboard){
					if($dashboardPageCheck == $CFG->pageDashboard)
						$dayhref = calendar_get_link_href(new moodle_url('/my/'.$CFG->pageDashboard, array('view' => 'day', 'course' => $calendar->courseid)), 0, 0, 0, $daytime);
					else if($dashboardPageCheck == $CFG->pageDashboard)
						$dayhref = calendar_get_link_href(new moodle_url('/my/'.$CFG->pageDashboard, array('view' => 'day', 'course' => $calendar->courseid)), 0, 0, 0, $daytime);
				} else {
					$dayhref = calendar_get_link_href(new moodle_url(CALENDAR_URL.'view.php', array('view' => 'day', 'course' => $calendar->courseid)), 0, 0, 0, $daytime);
				}
				// Added by Madhab //
	
				$cellclasses = array();
	
				if ($weekend & (1 << ($dayweek % $numberofdaysinweek))) {
					// Weekend. This is true no matter what the exact range is.
					$cellclasses[] = 'weekend';
				}
	
				// Special visual fx if an event is defined
				if (isset($eventsbyday[$day])) {
					if(count($eventsbyday[$day]) == 1) {
						$title = get_string('oneevent', 'calendar');
					} else {
						$title = get_string('manyevents', 'calendar', count($eventsbyday[$day]));
					}
					$cell->text = html_writer::tag('div', html_writer::link($dayhref, $day, array('title'=>$title)), array('class'=>'day'));
				} else {
					$cell->text = html_writer::tag('div', $day, array('class'=>'day'));
				}
	
				// Special visual fx if an event spans many days
				$durationclass = false;
				if (isset($typesbyday[$day]['durationglobal'])) {
					$durationclass = 'duration_global';
				} else if (isset($typesbyday[$day]['durationcourse'])) {
					$durationclass = 'duration_course';
				} else if (isset($typesbyday[$day]['durationgroup'])) {
					$durationclass = 'duration_group';
				} else if (isset($typesbyday[$day]['durationuser'])) {
					$durationclass = 'duration_user';
				}
				if ($durationclass) {
					$cellclasses[] = 'duration';
					$cellclasses[] = $durationclass;
				}
	
				// Special visual fx for today
				if ($display->thismonth && $day == $date['mday']) {
					$cellclasses[] = 'day today';
				} else {
					$cellclasses[] = 'day nottoday';
				}
				$cell->attributes = array('class'=>join(' ',$cellclasses));
	            //pr($events);die;
	            
				
				
				
				if (isset($eventsbyday[$day])) {
					$cell->text .= html_writer::start_tag('ul', array('class'=>'events-new'));
					$eventsInDate = 0;
					foreach($eventsbyday[$day] as $eventindex) {
					
					    // added by rajesh to exit loop if event is more than 3 in any date.
						$eventsInDate++;
						if($eventsInDate > 3){
						  continue;
						}
						// end
						// If event has a class set then add it to the event <li> tag
						$attributes = array();
						if (!empty($events[$eventindex]->class)) {
							$attributes['class'] = $events[$eventindex]->class;
						}
						$dayhref->set_anchor('event_'.$events[$eventindex]->id);
						$eventsName = $events[$eventindex]->name;
						$eventsName = $eventsName!='' && strlen($eventsName) > 7 ? substr($eventsName, 0, 7)."..." : $eventsName;
						$link = html_writer::link($dayhref, format_string($eventsName, true), array('title'=>$events[$eventindex]->name));
						$cell->text .= html_writer::tag('li', $link, $attributes);
					}
					$cell->text .= html_writer::end_tag('ul');
					
					if(count($eventsbyday[$day]) > 3 ){
					   $dayhref->set_anchor();
					   $cell->text .= html_writer::link($dayhref, ' <img title="'.get_string('moreevents','event').'" alt="'.get_string('moreevents','event').'" src="'.$CFG->wwwroot.'/theme/gourmet/pix_core/t/expanded.png" >', array('title'=>get_string('moreevents','event'), 'class'=>'more-event'));
			       }	
				}
				
				$dayhref->set_anchor('event_'.$events[$eventindex]->id);
				
				
				
				
				
				
				//pr($eventsByDate);
				// added by rajesh 
				
				//pr($eventTypeArr);
				/*foreach($eventTypeArr as $eventTypeVal){
				
					if (isset($eventsByDate[$day][$eventTypeVal])) {
						$cell->text .= html_writer::start_tag('ul', array('class'=>'events-new'));
						$eventCnt = count($eventsByDate[$day][$eventTypeVal]);
						foreach($eventsByDate[$day][$eventTypeVal] as $eventindex) {
							// If event has a class set then add it to the event <li> tag
							$attributes = array();
							if (!empty($events[$eventindex]->class)) {
								$attributes['class'] = $events[$eventindex]->class;
							}
							$dayhref->set_anchor('event_'.$events[$eventindex]->id);
							$eventName = $eventCnt>1?(($events[$eventindex]->name)."..."):$events[$eventindex]->name;
							$eventName = date("g:ma", $events[$eventindex]->timestart)." ".$eventName;
							$link = html_writer::link($dayhref, format_string($eventName, true));
							$cell->text .= html_writer::tag('li', $link, $attributes);
							break;
						}
						$cell->text .= html_writer::end_tag('ul');
					}
				}*/
				
				/*if (isset($durationbyday[$day])) {
					$cell->text .= html_writer::start_tag('ul', array('class'=>'events-underway'));
					foreach($durationbyday[$day] as $eventindex) {
						$cell->text .= html_writer::tag('li', '['.format_string($events[$eventindex]->name,true).']', array('class'=>'events-underway'));
					}
					$cell->text .= html_writer::end_tag('ul');
				}*/
				$row->cells[] = $cell;
			}
	
			// Paddding (the last week may have blank days at the end)
			for($i = $dayweek; $i <= $display->maxwday; ++$i) {
				$cell = new html_table_cell('&nbsp;');
				$cell->attributes = array('class'=>'nottoday dayblank');
				$row->cells[] = $cell;
			}
			$table->data[] = $row;
			$output .= html_writer::table($table);

			$output .= html_writer::end_tag('div');
	
			return $output;
		}
		
		 /**
		 * Displays the calendar for a single day
		 *
		 * @param calendar_information $calendar
		 * @return string
		 */
		public function show_day(calendar_information $calendar, moodle_url $returnurl = null) {
	
			if ($returnurl === null) {
				$returnurl = $this->page->url;
			}
			//echo strtotime("2014-09-29 13:50:00");
	        //pr( date("Y-m-d H:i:s", 1411998600));die;
			
			$events = calendar_get_upcoming($calendar->courses, $calendar->groups, $calendar->users, 1, 100, $calendar->timestamp_today());
	
			$output  = html_writer::start_tag('div', array('class'=>'header'));
			if (calendar_user_can_add_event($calendar->course)) {
				$output .= $this->add_event_button($calendar->course->id, 0, 0, 0, $calendar->time);
			}
			//$output .= html_writer::tag('label', get_string('dayview', 'calendar'), array('for'=>'cal_course_flt_jump'));
			//$output .= $this->course_filter_selector($returnurl, get_string('dayview', 'calendar'));
			$output .= html_writer::end_tag('div');
			// Controls
			$output .= html_writer::tag('div', calendar_top_controls('day', array('id' => $calendar->courseid, 'time' => $calendar->time)), array('class'=>'controls'));

			$output .= '<div class="legend-container"><div class="legend-classroom"><span></span>'.get_string('classroom','calendar').'</div><div class="legend-course"><span></span>'.get_string('online','calendar').'</div><div class="legend-global"><span></span>'.get_string('global','calendar').'</div><div class="legend-user"><span></span>'.get_string('user','calendar').'</div></div><div class = "clear"></div>';
	
			if (empty($events)) {
				// There is nothing to display today.
				$output .= html_writer::start_tag('div', array('class'=>'eventlist scroll-pane'));
				$output .= $this->output->heading(get_string('daywithnoevents', 'calendar'), 3);
				$output .= html_writer::end_tag('div');
				
			} else {
				$output .= html_writer::start_tag('div', array('class'=>'eventlist scroll-pane'));
				$underway = array();

				// First, print details about events that start today
				$todayEvent = false;
				$noEventMessageShow = false;
				$timeExists = optional_param('time', '', PARAM_INT);
				$isViewDay = optional_param('view', '', PARAM_ALPHA);
				$today = date('Y-m-d');
				$queriedday = date('Y-m-d', $timeExists); //from query parameter			

				foreach ($events as $event) {
					$event = new calendar_event($event);
					$event->calendarcourseid = $calendar->courseid;

					/******* Open - As discussed with Management, we are removing the check for time of the events from listing - Madhab *******/
					/*
					if($isViewDay == 'day') {
						if($today == $queriedday) {
							if ($event->timestart >= $calendar->timestamp_today() && $event->timestart <= $calendar->timestamp_tomorrow()-1) {  // Print it now
								$event->time = calendar_format_event_time($event, time(), null, false, $calendar->timestamp_today());
								$output .= $this->event($event);
								$todayEvent = true;
							} else {                                                                 // Save this for later
								$underway[] = $event;
							}
						} else {
							$event->time = calendar_format_event_time($event, time(), null, false, $calendar->timestamp_today());
							$output .= $this->event($event);
							$todayEvent = true;
						}
					} else {
						if ($event->timestart >= $calendar->timestamp_today() && $event->timestart <= $calendar->timestamp_tomorrow()-1) {  // Print it now
							$event->time = calendar_format_event_time($event, time(), null, false, $calendar->timestamp_today());
							$output .= $this->event($event);
							$todayEvent = true;
						} else {                                                                 // Save this for later
							$underway[] = $event;
						}
					}
					*/
					/******* Closed - As discussed with Management, we are removing the check for time of the events from listing - Madhab *******/
					
					$event->time = calendar_format_event_time($event, time(), null, false, $calendar->timestamp_today());
					$output .= $this->event($event);
					
					$todayEvent = true;
				}

				// There is nothing to display today.
				if(!$todayEvent && !$noEventMessageShow) {
					$output .= $this->output->heading(get_string('daywithnoevents', 'calendar'), 3);
					$noEventMessageShow = true;
				}
				
				/******* Commented as we don't want to show those events are underway - by Madhab ******/
				/*
				// Then, show a list of all events that just span this day
				if (!empty($underway)) {
					$output .= $this->output->heading(get_string('spanningevents', 'calendar'), 3);
					foreach ($underway as $event) {
						$event->time = calendar_format_event_time($event, time(), null, false, $calendar->timestamp_today());
						$output .= $this->event($event);
					}
				}
				*/
	
				$output .= html_writer::end_tag('div');
			}
	
			return $output;
		}
	

  } //end class	
  
  
  
  
}
