<?php

/**
		* Custom module - Print Layout
		* Date Creation - 09/07/2014
		* Date Modification : 09/07/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
	
$hasleftsidebar = (empty($PAGE->theme->settings->layout)) ? false : $PAGE->theme->settings->layout;
$hasanalytics = (empty($PAGE->theme->settings->useanalytics)) ? false : $PAGE->theme->settings->useanalytics;
echo $OUTPUT->doctype();

//$userrole =  getUserRole($USER->id);

set_time_limit(0);
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
<title><?php echo $OUTPUT->page_title(); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="Moodle Theme by 3rd Wave Media | elearning.3rdwavemedia.com" />
<?php require_once(dirname(__FILE__).'/includes/iosicons.php'); ?>
<link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' rel='stylesheet'  type='text/css'>
<?php echo $OUTPUT->standard_head_html() ?>
<link href='<?php echo $CFG->wwwroot;?>/theme/gourmet/style/print.css' rel='stylesheet'  type='text/css' media="print">
<script>
  $(window).bind("load", function() {
     //window.print(); 
	setTimeout("window.print()",2000);
  });
</script>
</head>
<body <?php echo $OUTPUT->body_attributes(); ?>>
<?php echo $OUTPUT->standard_top_of_body_html() ?>
<div class="wrapper">
  <div class="header-fixed">
  <!--//main-nav-->
  </div>
  <div id="page" class="container">
    <div id="page-content" class="row">
      <?php if ($hasleftsidebar) { ?>
      <section id="region-main" class="col-md-9 pull-right">
      <?php } else { ?>
		
      <section id="region-main" class="col-md-9">
        <?php } ?>
        <div class="region-main-inner">
          <?php echo $OUTPUT->main_content();?>
        </div>
        <!--//region-main-inner-->
      </section>
     
       
    <!--//page-content-->
  </div>
  <!--//#page-->
</div>
</div>
<!--wrapper-->

<?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>