<?php

/**
		* Custom module - Global Admin Layout
		* Date Creation - 04/05/2014
		* Date Modification : 23/06/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
	
$hasleftsidebar = (empty($PAGE->theme->settings->layout)) ? false : $PAGE->theme->settings->layout;
$hasanalytics = (empty($PAGE->theme->settings->useanalytics)) ? false : $PAGE->theme->settings->useanalytics;
echo $OUTPUT->doctype();
loadThemeCss();

//$userrole =  getUserRole($USER->id);


?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
<title><?php echo $OUTPUT->page_title(); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="Moodle Theme by 3rd Wave Media | elearning.3rdwavemedia.com" />
<?php require_once(dirname(__FILE__).'/includes/iosicons.php'); ?>
<link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' rel='stylesheet' type='text/css'>
<?php echo $OUTPUT->standard_head_html() ?>
<!-- Respond.js IE8 support of media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<?php if ($hasanalytics) { ?>
<!-- Start Google Analytics -->
<?php require_once(dirname(__FILE__).'/includes/analytics.php'); ?>
<!-- End Google Analytics -->
<?php } ?>
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/javascript/classroom.js"></script>
</head>
<body <?php echo $OUTPUT->body_attributes(); ?>>
<?php echo $OUTPUT->standard_top_of_body_html() ?>
<div class="wrapper" id="page-classroom-listing">
  <div class="header-fixed">
    <header id="page-header" class="header clearfix">
      <div class="top-bar">
        <div class="container">
          <?php require_once(dirname(__FILE__).'/includes/header.php'); ?>
          <nav role="navigation" class="main-nav">
            <div class="container">
              <div class="navbar-header">
                <button class="navbar-toggle btn-navbar" type="button" data-toggle="collapse" data-target="#nav-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <!--//nav-toggle-->
              </div>
              <!--//navbar-header-->
              <div id="nav-collapse" class="nav-collapse collapse"> <?php echo $customMenu = $OUTPUT->custom_menu();?>
                <?php require_once(dirname(__FILE__).'/includes/custommenu.php'); ?>
              </div>
            </div>
          </nav>
          <?php //require_once(dirname(__FILE__).'/includes/custombreadcrumb.php'); ?>
        </div>
        <!--Container-->
      </div>
      <!--//top-bar-->
    </header>
    <!--//main-nav-->
  </div>
  <div id="page" class="container">
    <?php require_once(dirname(__FILE__).'/includes/custombreadcrumb.php'); ?>
    <div id="page-content" class="row">
      <?php if ($hasleftsidebar) { ?>
      <section id="region-main" class="col-md-9 pull-right">
      <?php } else { ?>
      <section id="region-main" class="col-md-9">
        <?php } ?>
        <div class="region-main-inner">
          <?php
				//require_once(dirname(__FILE__).'/includes/custombreadcrumb.php');
                echo $OUTPUT->course_content_header();
                echo $OUTPUT->main_content();
                echo $OUTPUT->course_content_footer();
                ?>
        </div>
        <!--//region-main-inner-->
      </section>



    
      
     <aside data-droptarget="1" data-blockregion="side-pre" class="col-md-3 pull-right block-region" id="block-region-side-pre">
 
     <div aria-labelledby="instance-3-header" data-instanceid="3" data-block="calendar_month" role="complementary" class="block_calendar_month  block" id="inst3" style="display: none;">
        <div class="content load-calendar">
            <div style="padding:10px;border: 1px solid #c0c0c0;text-align: center;min-height:188px;"><?php echo get_string('loadingcalender', 'calendar');?></div>
        </div>
    </div>
  </aside>

      <aside data-droptarget="1" data-blockregion="side-pre" class="col-md-3 pull-right block-region" >
        <div aria-labelledby="instance-5-header" data-instanceid="5" data-block="settings" role="navigation" class="block_settings  blockformydoc" >
          <div class="box block_tree_box" id="settingsnav">
            <div>
              <?php 
					echo '<h2><span></span>'.get_string('mydocuments','learnercourse').'</h2>';
					$OUTPUT->heading(get_string('mydocuments','learnercourse'));
			  ?>
            </div>
            <?php require_once($CFG->dirroot . '/local/user/mydocfiles.php'); ?>
          </div>
        </div>
      </aside>
     
      <?php if($USER->archetype == $CFG->userTypeAdmin || $USER->archetype == $CFG->userTypeManager){  ?>
      <aside data-droptarget="1" data-blockregion="side-pre" class="col-md-3 pull-right block-region right-message" style="padding-top:10px;">
      <div aria-labelledby="instance-5-header" data-instanceid="5" data-block="settings" role="navigation" class="block_settings  blockformydoc">
        <div class="lower-right-bottom">
          <div class="block-header messages-icon"><span></span>Messages</a><a href="<?php echo $CFG->wwwroot.'/messages/add_message.php';?>" class="message-setting pull-right" title="<?php echo get_string('new_message','messages');?>"></a><a href="<?php echo $CFG->wwwroot.'/messages/index.php';?>" class="add-message pull-right" title="<?php echo get_string('manage_messages',"messages");?>"></a></div>
          <div class="block-content scroll-pane">
            <?php
									
				/*$msgSql = "SELECT * from {$CFG->prefix}my_messages where deleted = 0 AND  status = 1 and message_from='".$USER->id."' ORDER BY timecreated DESC limit 0,5";
				
				$messages = $DB->get_records_sql($msgSql);*/
				$userId = $USER->id;
				$groupSql = "SELECT groupid from {$CFG->prefix}groups_members where userid='".$userId."'";
				$groupRow = $DB->get_records_sql($groupSql);
				$groupId = array();
				$teamId = "";
				if(count($groupRow)>0){
					foreach($groupRow as $groupValue){		
						$groupId[] = $groupValue->groupid; 		
					}
					$teamId =  implode(",",$groupId);
				}	
								
				$msgSql = "SELECT * from {$CFG->prefix}my_messages where deleted = 0 AND  status = 1 and ( FIND_IN_SET('".$USER->department."',department) || FIND_IN_SET('".$userId."',show_to) ||  FIND_IN_SET('".$teamId."',team)) ORDER BY timecreated DESC limit 0,5";
				
				$messages = $DB->get_records_sql($msgSql);
				
				if(count($messages)>0){
					foreach($messages as $message){
					   // $message_content = $message->message_content?nl2br($message->message_content):$message->message_content;
					   $message_content = $message->message_title?nl2br($message->message_title):$message->message_title;
						$user = $DB->get_record('user',array('id'=>$message->message_from));
					?>
            <div class = 'message-block'>
              <div class = 'message-sender-image'> <?php echo $OUTPUT->user_picture($user, $userpictureparams);?> </div>
              <div class = 'message-content'>
                <div class = "message-sender-info"> <?php echo  ucfirst($user->firstname)." ".ucfirst($user->lastname)."<br>";
										
									echo getDateFormat($message->timecreated, $CFG->customDefaultDateTimeFormat); // user M in place of F for short month name
								?> </div>
              </div>
              <div class = "clear"></div>
              <div class = "message-content-details"> <a href = "<?php echo $CFG->wwwroot.'/messages/read_message.php?id='.$message->id;?>">
                <?php
										if(strlen($message_content) >100){
											echo substr($message_content,0,100)." ...";
										}else{
											echo $message_content;
										}
									?>
                </a> </div>
            </div>
            <?php
					}
				}else{ ?>
            <div class = "no-reults"> <?php echo get_string("no_results"); ?> </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
    </aside>
   
    <?php } ?>
    <!--//page-content-->
  </div>
  <!--//#page-->
</div>
<!--wrapper-->
<footer id="page-footer" class="footer">
  <?php require_once(dirname(__FILE__).'/includes/footer.php'); ?>
</footer>
<!--<style>

	.blockformydoc{

		background: none repeat scroll 0 0 padding-box #F5F5F5;
		border: medium none;
		border-radius: 0;
		box-shadow: none;
		margin-bottom: 30px;
		padding-bottom: 15px;
		padding-top: 0;
		
	}	
	
	.error{
	  color:#FF0000;
	  font-size:11px;
	}
</style>-->
<script src="<?php echo $CFG->wwwroot;?>/local/js/jquery.validate.js" type="text/javascript" charset="utf-8"></script>
<script>



$(document).ready(function(){
 
  
	  
     $('.block').hide(); 
     $('.block').each(function(){
	 
	   var hasClass = $(this).hasClass('block_calendar_month');
	   if(hasClass == true){
	     $(this).show();
	   }else{
	     $(this).hide();
	   }
	 
	 });
	 
	 /*$('input[type=file]').change(function(e){
	  var file = $(this).val();
	  var filename = file.split('\\').pop();
	  $('#filename').html(filename);
	});*/

	
        /*$("#uploadfilefrm").validate({
            rules: {
                'uploaddocfile': {required:true,accept: 'JPG|JPEG|PNG|GIF|BMP|PDF|DOC|TXT'}
            },
            messages: {
   
                'uploaddocfile':  {
                     required: "<?php print get_string('pleaseselectfile','learnercourse'); ?>",
                     accept: "<?php print get_string('invalidfileextension','learnercourse'); ?>"
                }
            }
        });*/
	 
	 
	

 });
 
function deleteMyDoc(docid){

	 var docid = docid;
 
    if(confirm("<?php echo get_string('areyousureyouwanttodeletethisfile','learnercourse');?>")){
 
		 $.ajax({
		 
		   url:'<?php echo $CFG->wwwroot;?>/local/ajax.php?action=deleteMyDoc',
		   type: 'POST',
		   data:'id='+docid,
		   beforeSend:function(){
			   $('#docdeletebtn'+docid).html('<img src="<?php echo $CFG->wwwroot;?>/pix/i/loading_small.gif" width="16" height="16" />');   
		   },
		   dataType:'json',
		   success:function(data){
			   var error = data.error;
			   var success = data.success;
			 
			   if(success == true){ 
			     var id = data.response;
				 $('#doc'+id).remove();
			   }else{
			      alert(error);
			   }
		   }
		 
		 });
	 
	 }else{
	    return false;
	 }
	 
}


</script>
<?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>
