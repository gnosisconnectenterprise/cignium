<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/* LOGO Image */
$haslogo = (!empty($PAGE->theme->settings->logo));

/* Logo settings */
if ($haslogo) {
	$logourl = $PAGE->theme->setting_file_url('logo', 'logo');
} else {
	$logourl = $OUTPUT->pix_url('logo', 'theme');
}
$showFullLogo = 0;
if(empty($USER->id)){
	$logourl = $CFG->wwwroot.'/home/images/logo.png';
	$showFullLogo = 1;
}


$userrole =  getUserRole($USER->id);

/* Social Media Settings */
$hastwitter     = (empty($PAGE->theme->settings->twitter)) ? false : $PAGE->theme->settings->twitter;
$hasfacebook    = (empty($PAGE->theme->settings->facebook)) ? false : $PAGE->theme->settings->facebook;
$hasgoogleplus  = (empty($PAGE->theme->settings->googleplus)) ? false : $PAGE->theme->settings->googleplus;
$haslinkedin    = (empty($PAGE->theme->settings->linkedin)) ? false : $PAGE->theme->settings->linkedin;
$hasyoutube     = (empty($PAGE->theme->settings->youtube)) ? false : $PAGE->theme->settings->youtube;
$hasflickr      = (empty($PAGE->theme->settings->flickr)) ? false : $PAGE->theme->settings->flickr;
$haspinterest   = (empty($PAGE->theme->settings->pinterest)) ? false : $PAGE->theme->settings->pinterest;
$hasinstagram   = (empty($PAGE->theme->settings->instagram)) ? false : $PAGE->theme->settings->instagram;
$hasskype       = (empty($PAGE->theme->settings->skype)) ? false : $PAGE->theme->settings->skype;
$hasrss         = (empty($PAGE->theme->settings->rss)) ? false : $PAGE->theme->settings->rss;

$twitter = $PAGE->theme->settings->twitter;
$facebook = $PAGE->theme->settings->facebook;
$googleplus = $PAGE->theme->settings->googleplus;
$linkedin = $PAGE->theme->settings->linkedin;
$youtube = $PAGE->theme->settings->youtube;
$flickr = $PAGE->theme->settings->flickr;
$pinterest = $PAGE->theme->settings->pinterest;
$instagram = $PAGE->theme->settings->instagram;
$skype = $PAGE->theme->settings->skype;
$rss = $PAGE->theme->settings->rss;

// If any of the above social networks are true, sets this to true.
$hassocialnetworks = ($hasfacebook || $hastwitter || $hasgoogleplus || $hasflickr || $hasinstagram || $haslinkedin || $haspinterest || $hasskype || $hasrss || $hasyoutube ) ? true : false;

/* Header widget settings */
$hasheaderwidget = (!empty($PAGE->theme->settings->headerwidget));
$headerwidget = $PAGE->theme->settings->headerwidget;
/* Header phone settings */
$hasheaderphone = (!empty($PAGE->theme->settings->headerphone));
$headerphone = $PAGE->theme->settings->headerphone;
/* Header email settings */
$hasheaderemail = (!empty($PAGE->theme->settings->headeremail));
$headeremail = $PAGE->theme->settings->headeremail;
?>
<script>
var userrole = '<?php echo $userrole;?>';
</script>

           
        <!--Logo-->
        <h1 class="logo col-md-2 col-sm-1 col-xs-12">
            <a href="<?php echo $CFG->wwwroot ?>"><img id="logo" src="<?php echo $logourl ?>" alt="<?php echo $SITE->shortname; ?>" /></a>
        </h1>
        <?php if(isset($USER->id) &&  !in_array($USER->id, array(0,2))){
			$actionCounts = checkCurrentActionsHeader();
			$messageCount = 0;
			$eventCount = 0;
			$courseCount = 0;
			$requestCount = 0;
			$messageDisplay = "";
			$eventDisplay = "";
			$courseDisplay = "";
			$requestDisplay = "";
			if(isset($actionCounts['messages_data']) && isset($actionCounts['messages_data']['count'])){
				$messageCount = $actionCounts['messages_data']['count'];
			}
			if($messageCount == 0 ){
				$messageDisplay = "Style = 'display:none;'";
			}

			if(isset($actionCounts['request_data']) && isset($actionCounts['request_data']['count'])){
				$requestCount = $actionCounts['request_data']['count'];
			}
			if($requestCount == 0 ){
				$requestDisplay = "Style = 'display:none;'";
			}

			if(isset($actionCounts['events_data']) && isset($actionCounts['events_data']['count'])){
				$eventCount = $actionCounts['events_data']['count'];
			}
			if($eventCount == 0 ){
				$eventDisplay = "Style = 'display:none;'";
			}

			if(isset($actionCounts['course_data']) && isset($actionCounts['course_data']['count'])){
				$courseCount = $actionCounts['course_data']['count'];
			}
			if($courseCount == 0 ){
				$courseDisplay = "Style = 'display:none;'";
			}

		?>
			<?php/* if($userrole == 1 || $userrole == 5){*/?>
			<?php if($USER->archetype == $CFG->userTypeAdmin || $USER->archetype == $CFG->userTypeStudent || $USER->archetype == $CFG->userTypeManager){
				$email_class = '';
				
				if($messageCount>0){
					$email_class = 'emailoriginal';
				}
				$bell_class = '';
				if($eventCount>0){
					$bell_class = 'belloriginal';
				}
				$requests_icon_class = '';
				if($courseCount>0){
					$requests_icon_class = 'requests-iconoriginal';
				}
				?>
                <a href="<?php echo $CFG->wwwroot.'/messages/mymessages.php'; ?>" class="email <?php echo $email_class?> pull-left" title = "<?php echo get_string('my_messages');?>"><span <?php echo $messageDisplay; ?>><?php echo $messageCount; ?></span></a>
                <a href="<?php echo $CFG->wwwroot.'/calendar/view.php?view=month'; ?>" class="bell <?php echo $bell_class ?> pull-left" title = "<?php echo get_string('events');?>"><span <?php echo $eventDisplay; ?>><?php echo $eventCount; ?></span></a>
				<?php
				if($USER->archetype != $CFG->userTypeStudent){
					$requestTitle = get_string('requests','menubar');
					$greenline_class = '';
					if($requestCount>0){
						$greenline_class = 'greenlineoriginal';
					}
					
				?>
					<a href="<?php echo $CFG->wwwroot.'/my/requests.php?type=1'; ?>" class="greenline <?php echo $greenline_class?> pull-left" title = "<?php echo $requestTitle;?>"><span <?php echo $requestDisplay; ?>><?php echo $requestCount; ?></span></a>
				<?php
			}
				?>
            <?php } if(/*$userrole == 5*/ $USER->archetype == $CFG->userTypeStudent){?>
<!--                <a href="<?php echo $CFG->wwwroot.'/my/learningpage.php?type=1'; ?>" class="requests-icon <?php echo $requests_icon_class?> pull-left" title = "<?php echo get_string('new_courses');?>"><span <?php echo $courseDisplay; ?>><?php echo $courseCount; ?></span></a>-->
        <?php }} ?>
        <!--Share-->
        <?php if($CFG->isSocialMedia) { ?>
        <div class="share pull-right">
        	<a href="javascript:void(0)" class="share-icon pull-right" title="Share"></a>
           <?php if ($hassocialnetworks && $PAGE->theme->settings->enabletopbarsocial) { ?>
            <ul class="social-icons col-md-3 col-sm-3 col-xs-12 hidden-xs">
            <span class="arrow"></span>     
                <?php if ($hastwitter) { ?>
                <li><a href="<?php echo $twitter ?>" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                <?php } ?>
                <?php if ($hasfacebook) { ?>
                <li><a href="<?php echo $facebook ?>" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                <?php } ?>
                <?php if ($hasgoogleplus) { ?>
                <li><a href="<?php echo $googleplus ?>" target="_blank" ><i class="fa fa-google-plus"></i></a></li> 
                <?php } ?>
                <?php if ($haslinkedin) { ?>
                <li><a href="<?php echo $linkedin ?>" target="_blank"  title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                <?php } ?>
                <?php if ($hasskype) { ?>
                <li><a href="<?php echo 'skype:'.$skype.'?call' ?>" target="_blank" ><i class="fa fa-skype"></i></a></li>
                <?php } ?>
                <?php if ($hasyoutube) { ?>
                <li><a href="<?php echo $youtube ?>" target="_blank" ><i class="fa fa-youtube"></i></a></li>
                <?php } ?>
                <?php if ($hasflickr) { ?>
                <li><a href="<?php echo $flickr ?>" target="_blank" ><i class="fa fa-flickr"></i></a></li>
                <?php } ?>
                <?php if ($hasinstagram) { ?>
                <li><a href="<?php echo $instagram ?>" target="_blank" ><i class="fa fa-instagram"></i></a></li>
                <?php } ?>
                <?php if ($haspinterest) { ?>
                <li><a href="<?php echo $pinterest ?>" target="_blank" ><i class="fa fa-youtube"></i></a></li>
                <?php } ?>
                
                <?php if ($hasrss) { ?>   
                <li class="row-end"><a href="<?php echo $rss ?>" target="_blank" ><i class="fa fa-rss"></i></a></li>   
                <?php } ?>          
            </ul><!--//social-icons-->
            <?php } ?></div>
            <?php } ?>
            <div class="logininfo-container pull-right">
            	<div class="pull-left">
                <?php echo $PAGE->headingmenu ?>
                <?php 
					if(!empty($USER->id)){
							echo $OUTPUT->login_info();
					}
				?>
                </div>
				<?php if($USER->id>0){?>
                <div class="arrow-bottom pull-left">
                	<a href="javascript:void(0)" class="arrow-bottom-icon pull-right"></a>                    
                    <ul class="profile-dropdown">
                        <span class="arrow"></span> 
                        <li><a href="<?php echo $CFG->wwwroot;?>/user/profile.php" class="view-profile"><?php echo get_string('viewprofile','user');?></a></li>
                        <?php if($USER->auth=='manual'){ ?>
                        <li><a href="<?php echo $CFG->wwwroot;?>/user/editprofile.php" class="edit-profile"><?php echo get_string('editprofile','user');?></a></li>
                        <li><a href="<?php echo $CFG->wwwroot;?>/login/change_password.php" class="change-password"><?php echo get_string('changepassword','user');?></a></li>
                        <?php } ?>
                        <li><a href="<?php echo $CFG->wwwroot;?>/login/logout.php?sesskey=<?php echo sesskey();?>" class="logout"><?php echo get_string('logout','user');?></a></li>
						<?php 
							if((isset($USER->original_archetype) && $USER->original_archetype == $CFG->userTypeAdmin) || $USER->archetype == $CFG->userTypeAdmin){
								if($USER->archetype == $CFG->userTypeStudent){
									?>
									<!--<li><a href="<?php echo $CFG->wwwroot;?>/local/switchrole.php?role=1&sesskey=<?php echo sesskey();?>" class="switchrole"><?php echo get_string('switchtoadmin','user');?></a></li>-->
								<?php
								}else{
								?>
									<!--<li><a href="<?php echo $CFG->wwwroot;?>/local/switchrole.php?role=2&sesskey=<?php echo sesskey();?>" class="switchrole"><?php echo get_string('switchtolearner','user');?></a></li>-->
								<?php
								}
							}elseif((isset($USER->original_archetype) && $USER->original_archetype ==	$CFG->userTypeManager) || $USER->archetype == $CFG->userTypeManager){
								if($USER->archetype == $CFG->userTypeStudent){
									?>
									<li><a href="<?php echo $CFG->wwwroot;?>/local/switchrole.php?role=1&sesskey=<?php echo sesskey();?>" class="switchrole"><?php echo get_string('switchtomanager','user');?></a></li>
								<?php
								}else{
								?>
									<li><a href="<?php echo $CFG->wwwroot;?>/local/switchrole.php?role=2&sesskey=<?php echo sesskey();?>" class="switchrole"><?php echo get_string('switchtolearner','user');?></a></li>
								<?php
								}
							} elseif(((isset($USER->currentRole) && $USER->currentRole == $CFG->userTypeStudent) || $USER->archetype == $CFG->userTypeStudent) && $USER->is_instructor == 1){
								if($USER->currentRole == $CFG->userTypeStudent){
									?>
									<!--<li><a href="<?php echo $CFG->wwwroot;?>/local/switchrole.php?role=1&sesskey=<?php echo sesskey();?>" class="switchrole"><?php echo get_string('switchtoinstructor','user');?></a></li>-->
								<?php
								}else{
								?>
									<!--<li><a href="<?php echo $CFG->wwwroot;?>/local/switchrole.php?role=2&sesskey=<?php echo sesskey();?>" class="switchrole"><?php echo get_string('switchtolearner','user');?></a></li>-->
								<?php
								}
							}
							?>
                    </ul>                    
                </div>
                <?php }?>				
				
                <?php if (isloggedin()) : ?>
                <?php echo $OUTPUT->user_picture($USER); ?>
                <?php endif; ?>
            </div> 
       

<script>
var _JS_WWW_ROOT = '<?php echo $CFG->wwwroot;?>';
var _JS_enable_record = "<?php echo get_string('enable_record')?>";
var _JS_disable_record = "<?php echo get_string('disable_record')?>";
var _JS_publish_course = "<?php echo get_string('publish_course','course')?>";
var _JS_noasset = "<?php echo get_string('noasset');?>";
var _JS_notpublished = "<?php echo get_string('notpublished');?>";
var _JS_publishcoursewithoutanyasset =  "<?php echo get_string('publishcoursewithoutanyasset','course')?>";
var _JS_publishprogram =   "<?php echo get_string('publishprogram','program')?>";
var _JS_nocoursesinprogram =   "<?php echo get_string('nocoursesinprogram');?>";
var _JS_notpublishedprogram =   "<?php echo get_string('notpublishedprogram');?>";

function maintainSelectCount(a,r,t){var s=0,i=0,e="",n="";a.find("option").each(function(){s++}),e=0==s?"None":t+"("+s+")",a.find("optgroup").attr("label",e),r.find("option").each(function(){i++}),n=0==i?"None":t+"("+i+")",r.find("optgroup").attr("label",n)}$(document).ready(function(){$(".adminiconsBar a.enable").click(function(){var a=$(this).attr("class"),r="";if("enable"==a){r=_JS_enable_record;var t=confirm(r);return 1==t?!0:!1}}),$(".adminiconsBar a.disable").click(function(){var a=$(this).attr("class"),r="";if("disable"==a){r=_JS_disable_record;var t=confirm(r);return 1==t?!0:!1}}),$("a.publish,a.publish_course").click(function(a){var r=$(this).attr("class");if("undefined"!=typeof $(this).attr("have-specific")){var t=$(this).attr("have-specific");if(""!=t){if("-1"==t){var s=($(this).attr("class"),$(this).attr("have-asset"));if(s)var i=_JS_publish_course;else var i=_JS_publishcoursewithoutanyasset}else var i=($(this).attr("class"),_JS_publish_course);var e=confirm(i);if(1==e){if("publish_course"==r){var n=$(this).attr("rel");$.ajax({url:_JS_WWW_ROOT+"/local/ajax.php",type:"POST",data:"action=publishCourse&courseId="+n,success:function(a){"success"==a?window.location.reload():alert("noasset"==a?_JS_noasset:_JS_notpublished)}})}return!0}return!1}var o=$(this).attr("msg-specific");return alert(o),!1}a.preventDefault();var n=($(this).attr("class"),$(this).attr("rel"));$.ajax({url:_JS_WWW_ROOT+"/local/ajax.php",type:"POST",data:"action=publishCourse&courseId="+n,success:function(a){if("success"==a){var r=_JS_publish_course,t=confirm(r);if(1!=t)return!1;window.location.reload()}else alert("noasset"==a?_JS_noasset:_JS_notpublished)}})}),$("a.publish-program").click(function(){var a=_JS_publishprogram,r=confirm(a);if(1==r){var t=$(this).attr("rel");return $.ajax({url:_JS_WWW_ROOT+"/local/ajax.php",type:"POST",data:"action=publishProgram&programId="+t,success:function(a){"success"==a?window.location.reload():alert("nocourses"==a?_JS_nocoursesinprogram:_JS_notpublishedprogram)}}),!0}return!1}),$(".calendar_filters, h3.eventskey").hide()});
</script>