<?php 

/**
		* Custom Module - Custom breadcrumb page
		* Date Creation - 04/05/2014
		* Date Modification : 23/06/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
	
?>
<div id="page-navbar" class="clearfix breadcrumb-new">
  <nav class="breadcrumb-button">
    <?php //echo $OUTPUT->page_heading_button(); ?>
  </nav>
  <?php 
  global $course, $DB, $CFG;
  //$userrole =  getUserRole($USER->id);
  
  if(!$course && isset($_GET['courseid'])){
    $course->fullname = getCourses($_GET['courseid']);
	$course->id = $_GET['courseid'];
  }

		if(strstr($_SERVER['REQUEST_URI'],'admin/backup.php') || strstr($_SERVER['REQUEST_URI'],'enrol/users.php') || strstr($_SERVER['REQUEST_URI'],'course/delete.php') ||  strstr($_SERVER['REQUEST_URI'],'mod/scorm/view.php') || strstr($_SERVER['REQUEST_URI'],'enrol/editenrolment.php') || strstr($_SERVER['REQUEST_URI'],'local/group/delete.php') || strstr($_SERVER['REQUEST_URI'],'enrol/unenroluser.php') || strstr($_SERVER['REQUEST_URI'],'group/managegroups.php') || strstr($_SERVER['REQUEST_URI'],'group/addgroup.php') || strstr($_SERVER['REQUEST_URI'],'group/assignusers.php') || strstr($_SERVER['REQUEST_URI'],'group/delete.php') || strstr($_SERVER['REQUEST_URI'],'enrol/groups.php') || strstr($_SERVER['REQUEST_URI'],'blog/index.php') || strstr($_SERVER['REQUEST_URI'],'user/files.php') || strstr($_SERVER['REQUEST_URI'],'calendar/view.php') || strstr($_SERVER['REQUEST_URI'],'calendar/export.php') || strstr($_SERVER['REQUEST_URI'],'event/view.php') || strstr($_SERVER['REQUEST_URI'],'event/event.php') || strstr($_SERVER['REQUEST_URI'],'my/nonlearningpage.php') ||  strstr($_SERVER['REQUEST_URI'],'my/dashboard.php') ||  strstr($_SERVER['REQUEST_URI'],'my/dashboard.php') ||(strstr($_SERVER['REQUEST_URI'],'course/view.php') && $USER->archetype == $CFG->userTypeStudent) ){ 
		
			if(strstr($_SERVER['REQUEST_URI'],'admin/backup.php')){
				$bcarr = array(get_string('siteadministration','menubar') => '', get_string('easybackup','menubar')=>$CFG->wwwroot.'/admin/backup.php');
			}elseif(strstr($_SERVER['REQUEST_URI'],'enrol/users.php')){
				$bcarr = array(get_string('courses','course') => '', get_string('managecourses','course') => $CFG->wwwroot.'/course/index.php', $course->fullname => $CFG->wwwroot.'/course/view.php?id='.$course->id, get_string('enrolusers','course') => '');
			}elseif(strstr($_SERVER['REQUEST_URI'],'enrol/groups.php')){
				$bcarr = array(get_string('courses','course') => '', get_string('managecourses','course') => $CFG->wwwroot.'/course/index.php', $course->fullname => $CFG->wwwroot.'/course/view.php?id='.$course->id, get_string('enrolgroups','course') => '');
			}elseif(strstr($_SERVER['REQUEST_URI'],'course/delete.php')){
				$bcarr = array(get_string('courses','course') => '', get_string('managecourses','course') => $CFG->wwwroot.'/course/index.php', get_string('delete','course') => '');
			}elseif(strstr($_SERVER['REQUEST_URI'],'mod/scorm/view.php')){
				$bcarr = array(get_string('mylearningpage','menubar') => $CFG->wwwroot.'/my/learningpage.php', $course->fullname => $CFG->wwwroot.'/course/view.php?id='.$course->id);
			}elseif(strstr($_SERVER['REQUEST_URI'],'enrol/editenrolment.php')){
				$bcarr = array(get_string('courses','course') => '', get_string('managecourses','course') => $CFG->wwwroot.'/course/index.php', $course->fullname => $CFG->wwwroot.'/course/view.php?id='.$course->id, get_string('users','course') => '', get_string('enrolledusers','course') => $CFG->wwwroot.'/enrol/users.php?id='.$course->id, get_string('editenrolement','course') => '');
			}elseif(strstr($_SERVER['REQUEST_URI'],'enrol/unenroluser.php')){
				$bcarr = array(get_string('courses','course') => '', get_string('managecourses','course') => $CFG->wwwroot.'/course/index.php', $course->fullname => $CFG->wwwroot.'/course/view.php?id='.$course->id, get_string('users','course') => '', get_string('enrolledusers','course') => $CFG->wwwroot.'/enrol/users.php?id='.$course->id, get_string('unenrol','course') => '');
			}elseif(strstr($_SERVER['REQUEST_URI'],'group/managegroups.php')){
				$bcarr = array(get_string('managegroups','group') => '');
			}elseif(strstr($_SERVER['REQUEST_URI'],'group/addgroup.php')){
				if(isset($_GET['id']) && $_GET['id']!=''){
				  $bcarr = array(get_string('managegroups','group') => $CFG->wwwroot.'/group/managegroups.php',get_string('editgroup','group') =>'');
				}else{
				  $bcarr = array(get_string('managegroups','group') => $CFG->wwwroot.'/group/managegroups.php',get_string('addnewgroup','group') =>'');
				}
			}elseif(strstr($_SERVER['REQUEST_URI'],'group/assignusers.php')){
				 $bcarr = array(get_string('managegroups','group') => $CFG->wwwroot.'/group/managegroups.php',get_string('adduserstogroup','group') =>'');
			}elseif(strstr($_SERVER['REQUEST_URI'],'group/delete.php')){
				$bcarr = array( get_string('managegroups','group') => $CFG->wwwroot.'/group/managegroups.php',get_string('gdeletegroup','group') => '');
			}elseif(strstr($_SERVER['REQUEST_URI'],'blog/index.php')){
				$bcarr = array(get_string('blogs','menubar') => $CFG->wwwroot.'/blog/index.php');
			}elseif(strstr($_SERVER['REQUEST_URI'],'user/files.php')){
				$bcarr = array(get_string('filemanagement','menubar') => $CFG->wwwroot.'/user/files.php');
			}elseif(strstr($_SERVER['REQUEST_URI'],'calendar/view.php')){
				$bcarr = array(get_string('manage_events') => '');
				//$bcarr = array(get_string('manage_events') => $CFG->wwwroot.'/calendar/view.php?view=month');
			}elseif(strstr($_SERVER['REQUEST_URI'],'calendar/export.php')){
				$bcarr = array(get_string('manage_events') => $CFG->wwwroot.'/calendar/view.php?view=month', get_string('calendarexport','menubar') => $CFG->wwwroot.'/calendar/export.php');
			}elseif(strstr($_SERVER['REQUEST_URI'],'event/view.php')){
				//$bcarr = array(get_string('events','menubar'));
				$bcarr = array(get_string('events','menubar') => $CFG->wwwroot.'/event/view.php');
			}elseif(strstr($_SERVER['REQUEST_URI'],'event/event.php')){
			
			    if(isset($_GET['id']) && $_GET['id']!=''){
				  $bcarr = array(get_string('manage_events') => $CFG->wwwroot.'/calendar/view.php?view=day', get_string('editevent','menubar') => '');
				}else{
				  $bcarr = array(get_string('manage_events') => $CFG->wwwroot.'/calendar/view.php?view=day', get_string('addevent','menubar') => '');
				}
			}elseif(strstr($_SERVER['REQUEST_URI'],'my/nonlearningpage.php')){
				$bcarr = array(get_string('mylearningpage','menubar') => '', get_string('noncoursematerial','menubar') => $CFG->wwwroot.'/my/nonlearningpage.php');
			}elseif(strstr($_SERVER['REQUEST_URI'],'course/view.php')){
			     $bcarr = array(get_string('mylearningpage','menubar') => $CFG->wwwroot.'/my/learningpage.php', $course->fullname => $CFG->wwwroot.'/course/view.php?id='.$course->id);           
			}
    ?>
  <div class="breadcrumb-nav"> <?php echo getBreadCrumb($bcarr); ?> </div>
  <?php }else{?>
  <div class="breadcrumb-nav"> <?php echo $OUTPUT->navbar(); ?> </div>
  <?php } ?>
</div>
