<?php
$hascopyright = (empty($PAGE->theme->settings->copyright)) ? false : $PAGE->theme->settings->copyright;
$hasfooterwidget = (empty($PAGE->theme->settings->footerwidget)) ? false : $PAGE->theme->settings->footerwidget;

/* Footer blocks settings */
$hasfooterblocks = (empty($PAGE->theme->settings->enablefooterblocks)) ? false : $PAGE->theme->settings->enablefooterblocks;
$hasfooterblock1 = (empty($PAGE->theme->settings->footerblock1)) ? false : $PAGE->theme->settings->footerblock1;
$hasfooterblock2 = (empty($PAGE->theme->settings->footerblock2)) ? false : $PAGE->theme->settings->footerblock2;
$hasfooterblock3 = (empty($PAGE->theme->settings->footerblock3)) ? false : $PAGE->theme->settings->footerblock3;

$footerblock1 = $PAGE->theme->settings->footerblock1;
$footerblock2 = $PAGE->theme->settings->footerblock2;
$footerblock3 = $PAGE->theme->settings->footerblock3;

$footerwidget = $PAGE->theme->settings->footerwidget;


?>
	<div class="footer-content"> 
        <div class="container">
            <?php if ($hasfooterblocks) {?>
            <div class="row footerblocks">                
                <?php if ($hasfooterblock1) {?>
                <div class="footer-col col-md-3 col-sm-12">
                    <div class="footer-col-inner">
                        <?php echo $footerblock1 ?>
                    </div><!--//footer-col-inner-->
                </div><!--//foooter-col-->
                <?php }?>
                <?php if ($hasfooterblock2) {?>
                <div class="footer-col col-md-6 col-sm-12">
                    <div class="footer-col-inner">
                        <?php echo $footerblock2 ?>
                    </div><!--//footer-col-inner-->
                </div><!--//foooter-col--> 
                <?php }?>
                <?php if ($hasfooterblock3) {?>
                <div class="footer-col col-md-3 col-sm-12">
                    <div class="footer-col-inner">
                        <?php echo $footerblock3 ?>
                    </div><!--//footer-col-inner-->            
                </div><!--//foooter-col-->  
            <?php }?> 
            </div>  
            <?php }?>  
            <!--<p class="helplink"><?php //echo page_doc_link(get_string('moodledocslink')); ?></p>-->
            <?php if ($hasfooterwidget) {?>
            <div class="footerwidget"><?php echo $footerwidget ?></div>
            <?php }?>  
            
        </div>    
	      
    </div>
	<div class="bottom-bar">
        <div class="container">
            <div class="rowConatct">
				   
                	<!--<?php if($hasheaderwidget) {?>
                        <div class="footer-widget f-left">
                            <?php //echo $headerwidget ?>
                        </div>
					<?php }?>-->
					<?php if ($CFG->showContactUs1 || $CFG->showEmail || $CFG->showContactUs2) {?>
                    <div class="footerContact">
                    <ul>
                    
						<?php if ($CFG->showEmail) {?>
							<li class="email"><a class="fa fa-envelope" href="mailto:<?php echo $headeremail ?>"><span class="hide_text"><?php echo $headeremail ?></span></a></li>
						<?php } ?>
                        <?php if ($CFG->showContactUs1 || $CFG->showContactUs2) {?>
                        <li><a href="javascript:void(0);" class="contactIcon"></a><ul>
							<?php if($CFG->showContactUs1) {?>
                                <li><i class="contactIcon"></i> <?php echo $headerwidget ?></li>
                            <?php }?>
                            <?php if ($CFG->showContactUs2) {?>
                                <li class="phone"> <i class="fa fa-phone"></i><p><?php echo $headerphone ?></p></li>
                            <?php }?>
                            </ul>
                        </li>
                        <?php } ?>
						
                    </ul>
                    </div>
					<?php }else{
						if ($CFG->showCopyRight){
					?>
						<div class="copyright">Copyright &copy; 2014 InfoPro Learning, Inc. All rights reserved.</div>
					<?php
						}
					} ?>
				           
                <?php if ($hassocialnetworks && $PAGE->theme->settings->enablebottombarsocial) { ?>
                <ul class="social pull-right col-md-6 col-sm-12 col-xs-12">
                    <?php if ($hastwitter) { ?>
                    <li><a href="<?php echo $twitter ?>"><i class="fa fa-twitter"></i></a></li>
                    <?php } ?>
                    <?php if ($hasfacebook) { ?>
                    <li><a href="<?php echo $facebook ?>"><i class="fa fa-facebook"></i></a></li>
                    <?php } ?>
                    <?php if ($hasgoogleplus) { ?>
                    <li><a href="<?php echo $googleplus ?>"><i class="fa fa-google-plus"></i></a></li> 
                    <?php } ?>
                    <?php if ($haslinkedin) { ?>
                    <li><a href="<?php echo $linkedin ?>"><i class="fa fa-linkedin"></i></a></li>
                    <?php } ?>
                    <?php if ($hasskype) { ?>
                    <li><a href="<?php echo 'skype:'.$skype.'?call' ?>"><i class="fa fa-skype"></i></a></li>
                    <?php } ?>
                    <?php if ($hasyoutube) { ?>
                    <li><a href="<?php echo $youtube ?>"><i class="fa fa-youtube"></i></a></li>
                    <?php } ?>
                    <?php if ($hasflickr) { ?>
                    <li><a href="<?php echo $flickr ?>"><i class="fa fa-flickr"></i></a></li>
                    <?php } ?>
                    <?php if ($hasinstagram) { ?>
                    <li><a href="<?php echo $instagram ?>"><i class="fa fa-instagram"></i></a></li>
                    <?php } ?>
                    <?php if ($haspinterest) { ?>
                    <li><a href="<?php echo $pinterest ?>"><i class="fa fa-youtube"></i></a></li>
                    <?php } ?>
                    
                    <?php if ($hasrss) { ?>   
                    <li class="row-end"><a href="<?php echo $rss ?>"><i class="fa fa-rss"></i></a></li>   
                    <?php } ?> 
                </ul><!--//social-->
                <?php } ?>
                
                <?php if($CFG->showPoweredBy == 1){ ?>
					<div id="footer_right_text">Powered By GnosisConnect LMS<span></span></div>
				<?php } ?>
                
                <?php 
				$CMSPages = getFooterCMSPages(); 
				if(count($CMSPages) > 0){
					$addCss = '';
					if($CFG->showPoweredBy == 0){
						$addCss = " pull-right";
					}
				?>
                    <div class="footer-menu <?php echo $addCss;?>">
                        <ul>
                        <?php 
						$OthersLi = '';
						$OthersLiStart = '';
						$OthersLiEnd = '';
						$cmsCount = 1;
						$totalCmsPages = count($CMSPages);
						if($totalCmsPages > 3){
							$OthersLiStart	= '<li class="linkPopup"><a href="javascript:void(0);"><span class="hide_text">Additional Items</span></a><ul>';
							$OthersLiEnd 	= '</ul></li>';
						}
						foreach($CMSPages as $pages){
								if($pages->is_external == 1){
									$target = 'target = "_blank"';
									$targetLink = $pages->external_url;
								}else{
									$target = '';
									$targetLink = $CFG->wwwroot.'/cms/cms_content.php?pageid='.$pages->id;
								}
								if($cmsCount>3){	
									$OthersLi .= '<li><a href="'.$targetLink.'" title="'.$pages->page_name.'" '.$target.'>'.$pages->page_name.'</a></li>';
								}else{
							?>
                            	<li><a href="<?php echo $targetLink;?>" title="<?php echo $pages->page_name;?>" <?php echo $target;?>><?php echo $pages->page_name;?></a></li>
                        	<?php
							}
							$cmsCount++;
						 } 
						 echo $OthersLiStart.$OthersLi.$OthersLiEnd;
						 ?>    
                        </ul>
                    </div>
                    
                <?php } ?>   
				
				
                <?php 
				if ($hascopyright) {
                //echo '<small class="copyright col-md-6 col-sm-12 col-xs-12"><p class="copy">&copy; '.date("Y").' '.$hascopyright.'</p></small>';
                } ?>
                
            </div><!--//row-->
        </div><!--//container-->
    </div>

<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/style/colorbox.css" />
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jscrollpane/jquery.jscrollpane.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jscrollpane/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jscrollpane/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jscrollpane/mwheelIntent.js"></script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/colorbox/jquery.colorbox.js"></script>
<!--<script type="text/javascript" src="<?php// echo $CFG->wwwroot;?>/theme/gourmet/javascript/classroom.js"></script>-->

<script>    
    
<?php 
if(!strstr($_SERVER['REQUEST_URI'],'cms/cms_content.php')){
?>
	if($('.block-content').length > 0){
		$('.block-content.scroll-pane').jScrollPane();
		$('.eventlist.scroll-pane').jScrollPane();
	}
<?php
}
?>

function filePickerScroll (){
	$('.mydoc-block .scroll-pane').jScrollPane();
}
$(document).on('click','.mydoc-block .ygtvspacer, .mydoc-block .fp-vb-tree, .mydoc-block .fp-vb-details, .mydoc-block .fp-vb-icons', function(){
  //  filePickerScroll();
	 setTimeout('filePickerScroll()',1000); // added by rajesh
});

$(document).on('click','.moodle-dialogue-exception .closebutton', function(){ 
	var errorDiv = $('.filemanager:visible').first();
	var scrollPos = errorDiv.offset().top;
	$(window).scrollTop(scrollPos);
});

$(document).on('click','.fp-btn-mkdir', function(){ 
	$('[id ^= "fm-newname-"]').attr('maxlength',150);
});

<?php if(strstr($_SERVER['REQUEST_URI'],'course/edit.php') || strstr($_SERVER['REQUEST_URI'],'user/editadvanced.php') || strstr($_SERVER['REQUEST_URI'],'department/add_department.php') || strstr($_SERVER['REQUEST_URI'],'group/addgroup.php')){?>
 $('.fp-restrictions span').append('<?php echo get_string('allowedfiletype', 'course');?>');
<?php }else if(strstr($_SERVER['REQUEST_URI'],'program/addprogram.php')){?>
$('#id_programimage .fp-restrictions span').append('<?php echo get_string('allowedfiletype', 'course');?>');
<?php } ?>

function scrollToElement($element, time) {
    var position = false;

    try {
        position = $element.offset().top - 105;
    } catch (err) {
        ;
    }
    if (!position) {
        return;
    }

    if (typeof time === 'undefined' || !time) {
        time = 1000; // default time
    }

    $('html, body').animate({
        scrollTop : position
    }, time);
}


function loadRightPanelCalendar(actionUrl){


	$('.load-calendar').html('<div style="padding:10px;border: 1px solid #c0c0c0;text-align: center;min-height:188px;"><?php echo get_string('loadingcalender', 'calendar');?></div>');
	var url = '<?php echo $CFG->wwwroot;?>/local/ajax.php?action=getRightPanelCalendar&time=<?php echo time();?>';
	if(actionUrl!='' && actionUrl!=undefined){
		url = actionUrl;
	}

	$.ajax({
	  
		url:url,
		type:'GET',
		async: false ,
		cache: false,
		//data:'action=getRightPanelCalendar',
		dataType:'html',
		success:function(data){

		  $('.load-calendar').html(data);
			   
		}
	  
	  });
	  

}
var ShowFeedback = 0;
<?php
if(in_array($USER->archetype,$CFG->showFeedbackTo) && $CFG->showFeedback == 1){
	?>
		ShowFeedback = 1;
		<?php
}
?>


$(document).ready(function(){
	//Jquery for feedback.
	// Requires jQuery!
	var userRole = "<?php if(isset($USER->archetype)){echo $USER->archetype;}else { echo "";}?>";
	var CFGRole = "<?php echo $CFG->userTypeStudent?>";
	if(ShowFeedback == 1){
		jQuery.ajax({
			url: "https://csgagileteam.atlassian.net/s/63862878b658cdbbc71c6f0fd713a695-T/en_USoa34hq/64012/15/1.4.23/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs.js?locale=en-US&collectorId=9516dbd5",
			type: "get",
			cache: true,
			dataType: "script"
		});
	}
	$("#id_resetbutton").on('click',function(event){

		$("span.error").remove();
		$("br.error").remove();
		
		$("div").removeClass('error');
	});
	 $(document).on('click','.block .minicalendar .calendar-controls a.previous, .block .minicalendar .calendar-controls a.next', function(event){
		 
		 event.preventDefault();
		 var actionUrl = $(this).attr('href');
		 loadRightPanelCalendar(actionUrl);
	 
	 });
	 $(".linkPopup > a").on('click',function(event){
		$(".contactIcon").next('ul').hide();
		$(this).next('ul').toggle();
		return false;
	});
	$(".contactIcon").on('click',function(event){
		$(".linkPopup > a").next('ul').hide();
		$(this).next('ul').toggle();
		return false;
	});
	$(document).on('click',function(event){
		$(".linkPopup > a,.contactIcon").next('ul').hide();
	})
	 
	 
	 $(document).on('keypress','input', function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
        }
     });

	$("#searchForm #key").keypress(function (event) {
			var chars = event.which || event.keyCode;
			if(chars == 63){
				return false;
			}
	});
	$(document).on('change', '#searchForm #key', function(){
		var textContent = $(this).val();
		textContent = textContent.replace('?','');
		$(this).val(textContent);
	});

	 
});

   
	 
	 function clickSearchForm(){
	 
	    var keyVal = $('.search-input #key').val();
		keyValN = keyVal.replace(":",": ");
		keyValN = $.trim(keyValN);
		$('.search-input #key').val(keyValN);
                
                if($("#report_allsubssription").val() != undefined){
                    $("#report_allsubssription").click();
                }else if($("#user_search").val() !=undefined){
                        $("#user_search").click();
                }else{
                    document.searchForm.submit();
                }
                
	   
	 }
	 	

 <?php if(!strstr($_SERVER['REQUEST_URI'], 'calendar/view.php') && !strstr($_SERVER['REQUEST_URI'], 'my/dashboard.php') ){?>
		 loadRightPanelCalendar();
<?php }else{ ?>
 $('#block-region-side-pre').hide();
<?php } ?>
function submitSearchForm(e){
	 var key=e.keyCode || e.which;
	  if (key==13){
	     clickSearchForm();
	  }
	  return false;
	}	

function clearSearchBox(){
	$("#search-form #key").val('');
        
         if($("#report_allsubssription").val() !=undefined){
            $("#report_allsubssription").click();
        }else if($("#user_search").val() !=undefined){
            $("#user_search").click();
        }else{
            document.searchForm.submit();
        }
	//document.searchForm.submit();	
}

$( "#search" ).click(function() {
    if($("#report_allsubssription").val() !=undefined){
            $("#report_allsubssription").click();
    }else if($("#user_search").val() !=undefined){
            $("#user_search").click();
    }else{
        document.searchForm.submit();
    }

});

$(".report_data_sync_popup").find("a").colorbox({iframe:true, width:"250px", height:"150px"});
$(".annual_enrollments_sync_popup").find("a").colorbox({iframe:true, width:"250px", height:"150px"});	
</script>