jQuery(document).ready(function($) {

    
	$('.collapsed').click(function(){
		$('html, body').animate({scrollTop:$('#'+$(this).find('a.fheader').attr('id')).position().top}, 'slow');
	});
    /* ======= Flexslider ======= */
	if($('.flexslider').length > 0){
		$('.flexslider').flexslider({
			animation: "fade"
		});
	}
    
    /* ======= Twitter Bootstrap hover dropdown ======= */
    
    // apply dropdownHover to all elements with the data-hover="dropdown" attribute
    //$('li.dropdown>[data-toggle="dropdown"]').dropdownHover();
    
    /* ======= Carousels ======= */
	if($('#testimonials-carousel').length > 0){
		$('#testimonials-carousel').carousel({interval: false, pause: "hover"});
	}
	
	/*---Share Dropdown----*/
	$('.share-icon').click(function(){
		if($("ul.social-icons").css("display") == "block"){
			$('ul.social-icons').hide('slow');
		} else {
			$('ul.social-icons').show('slow');
		}
		$('ul.profile-dropdown').hide('slow');
	});
	$('html').click(function() {
		$('ul.social-icons').hide('slow');
		$('ul.profile-dropdown').hide('slow');
	})

	 $('.share-icon').click(function(e){
		 e.stopPropagation();
	 });
	 $('.arrow-bottom-icon').click(function(e){
		 e.stopPropagation();
	 });
	/*---Profile Dropdown----*/
	$('.arrow-bottom-icon').click(function(){
		if($("ul.profile-dropdown").css("display") == "block"){
			$('ul.profile-dropdown').hide('slow');
		} else {
			$('ul.profile-dropdown').show('slow');
		}
		$('ul.social-icons').hide('slow');
	});
	
	/*---Profile Dropdown----*/
	$('.search-button').click(function(){
		if($(".search-view").css("display") == "block"){
			$('.search-view').hide();
		} else {
			$('.search-view').show();
		}
	});
	
	$('.wrapper').click(function(e){
		if($(".search-view").css("display") == "block"){
			if(e.target.className != 'search-button' && $(e.target).parent('div').attr('class') != 'search-view' && e.target.className != 'search-view'){
				$('.search-view').hide();
			}
		}
	});
	$('#perpagecount').change(function(){
		var perpage = $(this).val();
		var url = $(this).attr('rel');
		if(perpage == 'All'){
			perpage = 0;
		}
		window.location.href = url+perpage;
	});
	
});


