<?php
//die("switched off email");exit;
require_once(dirname(dirname(__FILE__)).'/config.php');
$courseid = $_POST['courseid'];

//error_reporting(E_ALL);
require('phpmailer/class.phpmailer.php'); // path to the PHPMailer class
require('phpmailer/class.smtp.php');

$servername = 'localhost';
$username = 'wbgnosis';
$password = 'S!Sdbuser';
$dbname    = 'cignium';


$siteUrl = 'http://cignium.gnosisconnect.com'; 
$lmsName = "TRANZACT Learning Management System (LMS)";
$lmsEmail = "https://support.cignium.com";
// Create connection

$link = mysqli_connect($servername, $username, $password, $dbname);

if (!$link) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}
$todayTimestamp = strtotime(date('Y-m-d'));

$courseQuery	= "SELECT c.id,c.fullname,c.idnumber,u.id as user_id,u.firstname,u.lastname,u.email,uc.end_date,um.email as manager_email,DATEDIFF(FROM_UNIXTIME(uc.end_date),FROM_UNIXTIME(".$todayTimestamp.")) as date_diff,c.criteria FROM mdl_user_course_mapping uc LEFT JOIN mdl_course as c ON uc.courseid = c.id LEFT JOIN mdl_user as u ON uc.userid = u.id LEFT JOIN mdl_user as um ON u.parent_id = um.id LEFT JOIN mdl_report_learner_performance_details as mrlpd ON(uc.userid=mrlpd.user_id  and mrlpd.course_id=c.id) WHERE uc.`status` = 1 AND c.is_active = 1 AND u.suspended=0 and u.deleted=0 and mrlpd.course_status!='completed' AND c.id=".$courseid;

$result	= mysqli_query($link,$courseQuery);
$fromUser = array();
$count=0;
if(mysqli_num_rows($result)>0){
	while ($email = mysqli_fetch_object($result)){
                if($email->end_date > 0 && $email->end_date <= time()){
                    continue;
                }
                $toUser = new stdClass();
		$toUser->email = $email->email;
		$managerEmail = $email->manager_email;
		$toUser->firstname = $email->firstname;
		$toUser->lastname = $email->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 1;
		$toUser->id = $email->user_id;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		$messageHtml = '';
		$subject = 'Reminder: '.$lmsName;
		$launchurl = $siteUrl;
		if($email->criteria == 1){
			$launchurl = $siteUrl.'/course/courselaunch.php?id='.$email->id.'&access=direct';	
		}
		$emailText = $toUser->firstname." ".$toUser->lastname.","."\n\r"."\n\r";
               /* if($email->end_date){
                    $emailText .= "The following online course is assigned to you and due to expire on ".date('M d, Y', $email->end_date)."\n\r"."\n\r";
                }else{
                    $emailText .= "The following online course is assigned to you\n\r"."\n\r";
                }*/
		 $emailText .= "You are receiving this email because you have not completed a course that has been assigned to you on our learning platform, GNOSIS. This means that you have not started the course, finished viewing the course and/or not passed the course. \n\r";

		/*if($email->idnumber != ''){
			$emailText .= "\"".$email->idnumber."\" titled ";
		}*/
		$emailText .= "\"".$email->fullname."\""."\n\r"."\n\r";

               /* if($email->end_date){
                    $emailText .= "Please take this course before the due date. Click here to complete the course: <a href = '".$launchurl."'>".$launchurl."</a>."."\n\r"."In case you are unable to take it before the due date, please submit a request for extension. To request an extension, please log on to $lmsName using the following link (<a href='".$siteUrl."'>$siteUrl</a>) and follow the steps below:"."\n\r"."\n\r";
                    $emailText .= "1) Navigate to your Learning Page and find the required Online Course."."\n\r"."2) To see more details and available actions, expand the course by clicking the \"+\" sign on the far right hand side of the Course Description."."\n\r"."3) Click the \"Request Extension\" button. Your manager will receive a notification of your request."."\n\r"."4) You will be notified when your manager approves or declines your extension request."."\n\r"."\n\r";

                }else{
                    $emailText .= "Click here to complete the course: <a href = '".$launchurl."'>".$launchurl."</a>."."\n\r"."\n\r";
                }*/
                
		$emailText .= "Click here to log in and complete the course as soon as possible: <a href = '".$launchurl."'>".$launchurl."</a>."."\n\r"."\n\r";		
		$emailText .= "Please do not reply to this e-mail. If you require support, please log a ticket with the Help Desk at: "."\n\r".$lmsEmail; 

		$messageHtml = text_to_html($emailText);
                
                //echo $messageHtml;die;
		email_to_user1($toUser, $fromUser, $subject, $emailText, $messageHtml, '', '',$managerEmail);
		$insertQuery = "INSERT INTO `mdl_cron_email_log` (`id` ,`mail_to_id` ,`mail_to_firstname` ,`mail_to_lastname` ,`mail_to_email` ,`mail_subject` ,`mail_body` ,`mail_format` ,`file_location` ,`flag` ,`created_on` ,`updated_on`) VALUES (NULL , '".$email->user_id."', '".$email->firstname."', '".$email->lastname."', '".$email->email."', '".addslashes($subject)."', '".addslashes($emailText)."', '1', '', '1', '".strtotime('now')."', '".strtotime('now')."')";
		$insertResult	= mysqli_query($link,$insertQuery);
                $count++;
              /* if($count > 5){
                   break;
               }*/
	}
}

$output = array();
$output['count']=$count;
$output['success']=1;
$output['success_message']="Reminder email has been sent to ".$count." users.";

echo json_encode($output);
die;
function user_name($user){
	return trim($user->firstname.' '.$user->lastname);
}
function email_to_user1($toUser, $fromUser, $subject, $emailText, $messageHtml,$emailFile, $emailFileName,$managerEmail){
        global $CFG;    
	
        $mail = new PHPMailer();
	$mail->IsSMTP();  // telling the class to use SMTP	
	
	$mail->SMTPSecure = 'tls';
	$mail->Mailer = "smtp";
   
	$mail->Host = 'email-smtp.us-east-1.amazonaws.com';
	$mail->Port = 587;
	//$mail->IDHost = 'gmail.com';
	$mail->SMTPAuth = true; // turn on SMTP authentication
	$mail->Username = "AKIAJMAB3VND3JQF7JQQ"; // SMTP username
	$mail->Password = "ApyAW81ULkxOW2WC2BQK4qz5s5p3ITsYFrOAT1TajQgg"; // SMTP password

	$visitor_email = $toUser->email;       
        
        //$visitor_email = 'cynthia.rogers@cignium.com';
        //$visitor_email = 'nabab.dhakar@compunnel.in';
        
	$fromAddress = "TRANZACT.Education@tranzact.net";
        //$fromAddress = "support@gnosisconnect.com";
	//$name = "Learning Portal Administrator";       
	 $name = "TRANZACT Education";
        
	$mail->AddAddress($visitor_email,$toUser->firstname." ".$toUser->lastname);
	//$mail->SetFrom($visitor_email, $name);
	//$mail->AddReplyTo($visitor_email,$name);
	$mail->SetFrom($fromAddress, $name);
	$mail->AddReplyTo($fromAddress,$name);
        //$managerEmail = "prashatn.sah@compunnel.in";	
        //$mail->AddCC($managerEmail);
        if(isset($CFG->addcc_in_email) && trim($CFG->addcc_in_email)){
            $mail->AddCC(trim($CFG->addcc_in_email));
			$mail->AddCC("Brianna.Capik@Tranzact.net");
        }
                    
	if($emailFile != ''){
		$mail->addAttachment($emailFile,$emailFileName);
	}

	$mail->Subject  = $subject;
	$mail->Body     = $emailText;
	if ($messageHtml!= '') {
        $mail->isHTML(true);
        $mail->Encoding = 'quoted-printable';
        $mail->Body    =  ($messageHtml);
        $mail->AltBody =  "\n$emailText\n";
    } else {
        $mail->IsHTML(false);
        $mail->Body =  "\n$emailText\n";
    }
	$mail->Send();
}
/*
function text_to_html($text, $smileyignored = null, $para = true, $newlines = true) {
    // Remove any whitespace that may be between HTML tags.
    $text = preg_replace("~>([[:space:]]+)<~i", "><", $text);

    // Remove any returns that precede or follow HTML tags.
    $text = preg_replace("~([\n\r])<~i", " <", $text);
    $text = preg_replace("~>([\n\r])~i", "> ", $text);

    // Make returns into HTML newlines.
    if ($newlines) {
        $text = nl2br($text);
    }

    // Wrap the whole thing in a div if required.
    if ($para) {
        // In 1.9 this was changed from a p => div.
        return '<div class="text_to_html">'.$text.'</div>';
    } else {
        return $text;
    }
}
return true;*/

?>