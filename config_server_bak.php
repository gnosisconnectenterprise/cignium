<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'mysqli';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'localhost';

/*if($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '10.1.7.180'){

	$CFG->dbname    = 'moodle26';
	$CFG->dbuser    = '26moodleuser';
	$CFG->dbpass    = '!26moodleDB1680';
	
	$CFG->wwwroot   = 'http://10.1.7.180/moodle';
	$CFG->dataroot  = '/var/www/moodledata';
	$CFG->wwwpath  = '/var/www/moodle';
	
	ini_set ('display_errors', 'on');
	ini_set ('log_errors', 'on');
	ini_set ('display_startup_errors', 'on');
	ini_set ('error_reporting', E_ALL);
	
}elseif($_SERVER['HTTP_HOST'] == 'instituteofexcellence.wordandbrown.com'){*/

	$CFG->dbname    = 'cignium';
	$CFG->dbuser    = 'wbgnosis';
	$CFG->dbpass    = 'S!Sdbuser';
	
	if ( isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) )
		$CFG->wwwroot   = 'https://cignium.gnosisconnect.com';
	else
		$CFG->wwwroot   = 'http://cignium.gnosisconnect.com';
	$CFG->dataroot  = '/var/www/moodledata';
	$CFG->wwwpath  = '/var/www/cignium/';
//}


$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => 3306,
  'dbsocket' => '',
);

$CFG->admin     = 'admin';

$CFG->directorypermissions = 0777;

// custom variables
date_default_timezone_set('America/New_York');

$CFG->perPageCount = 5;
$CFG->excludedUsers = array(1,2,6,3); //default array(0)
//$CFG->customDefaultDateFormat = 'dS M Y h:i A';
$CFG->customDefaultDateTimeFormat = 'M d, Y (h:i A)';
$CFG->customDefaultDateTimeFormatForCSV = 'M d Y (h:i A)';
$CFG->customDefaultDateTimeFormatForPrint = 'M d, Y';
$CFG->DateFormatForRequests = 'M d, Y';
$CFG->customDefaultTimeFormat1 = 'h:i A';
$CFG->customDefaultTimeFormat2 = 'g:i A';

$CFG->customDefaultDateFormat = 'M d, Y'; //Please update the customDefaultDateFormatForDB variable in case of any update in this variable.(As per mysql format) (REFER http://dev.mysql.com/doc/refman/5.5/en/date-and-time-functions.html#function_date-format)
$CFG->customDefaultDateFormatForDB = '%b %d, %Y';

$CFG->customDefaultDateFormatForCSV = 'M d Y';
$CFG->customDateFormat = 'm/d/Y';
$CFG->reportSearchStartDate = date("m-d-Y", mktime(0, 0, 0, 01, 01, (date('Y'))));
$CFG->reportSearchEndDate = date('m-d-Y', strtotime("+1 years"));
$CFG->programOverviewFilesLimit = 1;
$CFG->programOverviewFilesExt = '.jpg,.gif,.png';
$CFG->courseType = 'course';
$CFG->programType = 'program';
$CFG->programImageFileArea = 'programimage';
$CFG->programMaterialFileArea = 'programmaterial';

//// Open Setting User Type to be used in different places/ validation - Madhab ////
$CFG->userTypeStudent = 'learner';
$CFG->userTypeAdmin = 'admin';
$CFG->userTypeManager = 'manager';
$CFG->userTypeInstructor = 'instructor';
$CFG->userTypeInSystem = array($CFG->userTypeAdmin,$CFG->userTypeStudent,$CFG->userTypeManager);
$CFG->userTypeInSystemForManagerAccess = array($CFG->userTypeStudent,$CFG->userTypeManager);

$CFG->userTypeAccessByManagerPrimary = array($CFG->userTypeStudent,$CFG->userTypeManager);
$CFG->userTypeAccessByManager = array($CFG->userTypeStudent);

//// Closed Setting User Type to be used in different places/ validation - Madhab ////

//// Open Setting Page Name to be used in different places/ validation - Madhab ////
$CFG->pageDashboard = 'dashboard.php';
$CFG->pageAdminDashboard = 'admin_dashboard.php';
$CFG->pageManagerDashboard = 'manager_dashboard.php';
$CFG->pageLearnerDashboard = 'learnerdashboard.php';
$CFG->pageDashboardCalendar = 'dashboard_calender.php';
$CFG->pageAjaxDashboardCalendar = 'ajax_dashboard_calender.php';
$CFG->pageAjaxCalendar = 'ajax_calender.php';
$CFG->pageDepartmentlib = 'departmentlib.php';
$CFG->pageAddDepartment = 'add_department.php';
$CFG->pageAddDepartmentForm = 'adddepartment_form.php';
$CFG->pageDeleteDepartment = 'delete_department.php';
$CFG->pageDepartmentAssigncourses = 'assigncourses.php';

$CFG->pageTeamAssigncourses = 'assigncourses.php';

$CFG->pageCmsLib = 'cmslib.php';
$CFG->pageDeleteCms = 'delete_cms.php';
$CFG->pageAddCms = 'add_cms.php';
$CFG->pageAddCmsForm = 'addcms_form.php';

$CFG->pageManageBadges = 'managebadges.php';
$CFG->pageEditBadgesForm = 'editbadges_form.php';
$CFG->pageEditBadges = 'editbadges.php';


// Program pages
$CFG->pageAddPrograms= 'addprogram.php';
$CFG->pageProgramAssignCourses = 'assigncourses.php';
$CFG->pageProgramAssignUsers = 'assignusers.php';
//// Closed Setting Page Name to be used in different places/ validation - Madhab ////

$CFG->defaultEventType = 'course';
$CFG->courseEvent = 'course';
$CFG->globalEvent = 'global';
$CFG->userEvent = 'user';

// Report Pages
$CFG->pageMultiUserReport = 'multiuser_report.php';
$CFG->pageUserReport = 'user_report.php';
$CFG->pageMultiCourseReport = 'multicoursereport.php';
$CFG->pageCourseReport = 'course_report.php';
$CFG->pageCourseReportPrint = 'course_report_print.php';
$CFG->pageCourseReportDetails = 'coursereportdetails.php';
$CFG->pageGlobalRequestReport = 'request_report.php';
$CFG->pageGlobalRequestReportPrint = 'request_report_print.php';



// Default Images
$CFG->courseDefaultImage = $CFG->wwwroot.'/theme/gourmet/pix/course-default.jpg'; 
$CFG->programDefaultImage = $CFG->wwwroot.'/theme/gourmet/pix/program-img.jpg';
$CFG->teamDefaultImage = $CFG->wwwroot.'/theme/gourmet/pix/group-img.jpg';
$CFG->departmentDefaultImage = $CFG->wwwroot.'/theme/gourmet/pix/department-img.jpg';

$CFG->teamDefaultImagePath = $CFG->wwwpath.'/theme/gourmet/pix/group/'; 
$CFG->teamDefaultImageURL = $CFG->wwwroot.'/theme/gourmet/pix/group/';

$CFG->departmentDefaultImagePath = $CFG->wwwpath.'/theme/gourmet/pix/department/'; 
$CFG->departmentDefaultImageURL = $CFG->wwwroot.'/theme/gourmet/pix/department/';  

$CFG->studentTypeId = 5;
$CFG->managerTypeId = 3;
$CFG->adminTypeId = 1;

$CFG->programModule = 'program';
$CFG->courseModule = 'course';
$CFG->classroomModule = 'classroom';
$CFG->classModule = 'class';
$CFG->userModule = 'user';
$CFG->departmentModule = 'department';
$CFG->teamModule = 'team';
$CFG->messageModule = 'message';
$CFG->eventModule = 'event';
$CFG->cmsModule = 'cms';

//classroom Configuration 
$CFG->classroomSettingsForm = 'classroom_settings_form.php';

$CFG->courseTypeOnline = 1;
$CFG->courseTypeClassroom = 2;
$CFG->courseOnline = 'online';
$CFG->courseClassroom = 'classroom';

$CFG->classroomModuleName = 'scheduler';
$CFG->classroomModuleId = 23;
$CFG->classroomOpen = 0;
$CFG->classroomInvite = 1;
$CFG->documentUploadFolderName = "upload";
$CFG->completed = 'Completed';
$CFG->inprogress = 'In Progress';
$CFG->notstarted = 'Not Started';
$CFG->courseStatusArr = array(3 => $CFG->notstarted, 1 => $CFG->inprogress, 2 => $CFG->completed);

$CFG->resourceModule = 'resource';
$CFG->scormModule = 'scrom';
$CFG->schedulerModule = 'scheduler';
$CFG->resourceModuleId = 17;
$CFG->scormModuleId = 18;
$CFG->schedulerModuleId = 23;

$CFG->pageCourseLaunch = 'courselaunch.php';
$CFG->pageClassroomPreview = 'classroom_preview.php';

$CFG->classroomOpenText = 'Open';
$CFG->classroomInviteText = 'Invite';

$CFG->classroomNoAccess = 0;
$CFG->classroomFullAccess = 1;
$CFG->classroomMediumAccess = 2;
$CFG->classroomLowAccess = 3;


$CFG->requestForClass = 3;
$CFG->waitingForManagerApprovalForClass= 0;
$CFG->classApproved = 1;
$CFG->classDeclined = 2;
$CFG->classInCompleted = 0;
$CFG->classCompleted = 1;

$CFG->classroomDefaultImage = $CFG->wwwroot.'/theme/gourmet/pix/course-default2.jpg';
$CFG->pageSingleClassroomReport = 'single_classroom_report.php'; 
$CFG->pageSingleClassroomPrintReport = 'single_classroom_report_print.php';
$CFG->pageMultiClassroomReport = 'multi_classroom_report.php'; 

$CFG->pageMultiClassroomPrintReport = 'multi_classroom_report_print.php'; 
$CFG->pageMultiClassroomReportSearch = 'multi_classroom_report_search.php'; 
$CFG->pageMultiClassroomReportDetails = 'classroom_report_details.php'; 
$CFG->declinedLearnerClassroomReportSearch = 'class_request_report_search.php';
$CFG->pageDeclinedLearnerReport = 'class_request_report.php';
$CFG->pageDeclinedLearnerReportPrint = 'class_request_report_print.php';
$CFG->showuseridentity = 'username';

$CFG->courseCriteriaCompliance = 1;
$CFG->courseCriteriaElective = 2;
$CFG->courseCriteriaElectiveGlobal = 3;
$CFG->courseCompliance = 'Compliance';
$CFG->courseElective = 'Elective';
$CFG->courseGlobalElective = 'Global Elective';
$CFG->courseElectiveGlobal = 1;
$CFG->courseElectiveNotGlobal = 0;
$CFG->courseCriteriaArr = array($CFG->courseCriteriaCompliance => $CFG->courseCompliance, $CFG->courseCriteriaElective => $CFG->courseElective);
$CFG->complianceIcon = $CFG->wwwroot.'/theme/gourmet/pix/compalince.png';
$CFG->globalElectiveIcon = $CFG->wwwroot.'/theme/gourmet/pix/global-elective.png';
//$CFG->learningYear = array('from'=>'04-01-'.date('Y', strtotime("0 year")), 'to'=>'03-31-'.date('Y', strtotime("+1 year")));
$CFG->learningYear = array('from'=>'01-01-'.date('Y', strtotime("+0 year")), 'to'=>'12-31-'.date('Y', strtotime("+0 year")));
$CFG->pageUserCreditHoursReport = 'user_credithours_report.php';
$CFG->pageUserCourseCreditHoursReport = 'user_course_credithours_report.php';

/** PDF Variables Start Here
*  document unit of measure [pt=point, mm=millimeter, cm=centimeter, in=inch]
*/

$CFG->printWindowParameter = 'width=830, height=400, top=100, left=100, scrollbars=1';
$CFG->pdfTableStyle = 'style="font-size: 8pt;" border="1" cellpadding="2" cellspacing="0"';
$CFG->pdfTableHeaderStyle = 'style="background-color:#EDEDEF"';
$CFG->pdfTableEvenRowStyle = 'style="background-color:#F6F6F6" nobr="true" ';
$CFG->pdfTableOddRowStyle = 'style="background-color:#FFFFFF" nobr="true"';
$CFG->pdfMainHeadingFontStyle = 'style="font-size:16pt;"';
$CFG->pdfInnerTableEvenRowStyle = 'style="background-color:#FFFFFF" nobr="true" ';
$CFG->pdfInnerTableOddRowStyle = 'style="background-color:#D5EFF8" nobr="true" ';
$CFG->pdfTableRowAttribute = 'nobr="true"';
$CFG->pdfSpanAttribute = 'style="font-size:16pt;margin-top:20px;margin-bottom:20px"';
$CFG->durationStart =0; //hours 
$CFG->durationEnd =8; //hours
$CFG->slotOfDuration =30; //minute
$CFG->pdfLogoName = 'logo.png';
$CFG->pdfLogoPath = $CFG->wwwpath.'/local/lib/tcpdf/images/';

/** PDF Variables End Here */
/* starts blog related variables */ 

$CFG->siteTitleForBlog = 'GnosisConnect\'s Learning Management System (LMS)';
$CFG->certificateFileName = 'certificate.pdf';
require_once(dirname(__FILE__) .'/configuration.php');

/* blog related variables end */ 

require_once(dirname(__FILE__) .'/configuration.php');
require_once(dirname(__FILE__) . '/lib/setup.php');


// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
