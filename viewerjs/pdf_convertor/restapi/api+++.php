<?php
    
	/* 
		This is an example class script proceeding secured API
		To use this class you should keep same as query string and function name
		Ex: If the query string value rquest=delete_user Access modifiers doesn't matter but function should be
		     function delete_user(){
				 You code goes here
			 }
		Class will execute the function dynamically;
		
		usage :
		
		    $object->response(output_data, status_code);
			$object->_request	- to get santinized input 	
			
			output_data : JSON (I am using)
			status_code : Send status message for headers
			
		Add This extension for localhost checking :
			Chrome Extension : Advanced REST client Application
			URL : https://chrome.google.com/webstore/detail/hgmloofddffdnphfgcellkdfbfbjeloo
		
		
 	*/
	
	ini_set('error_reporting', 1);
    error_reporting(E_ALL);
	require_once("Rest.inc.php");

	class API extends REST {
	
		public $data = "";
		Const WWW_PATH = '/var/www/html/openoffice/';
		private $pythonFilePath = "";
		private $allowedDomains = array();
		
		// Some interesting fields, the user will need the key and secret

		private $consumer_key = '';
		private $consumer_secret = '';

	
    	public function __construct(){
			parent::__construct();				// Init parent contructor
			$this->getAllowedDomains();
			$this->getPythonFilePath();
		}
		
		private function getPythonFilePath(){
		  //$this->pythonFilePath = self::WWW_PATH."html/pdf_convertor/DocumentConverter.py ";
		  $this->pythonFilePath = "DocumentConverter.py ";
		}
		
				
		/*
		 * Public method for access api.
		 * This method dynamically call the method based on the query string
		 *
		 */
		public function processApi(){

			$func = strtolower(trim(str_replace("/","",$this->_request['request'])));
			if((int)method_exists($this,$func) > 0)
				$this->$func();
			else
				$this->response('',404);				// If the method not exist with in this class, response would be "Page not found".
		}
		
		/*
		 *  get allowed domain 
		*/
		
		private function getAllowedDomains(){
			$this->allowedDomains = array('lms.gnosisconnect.com', 'dev.gnosislms.com');
		}
		
		
				 
		private function fvPDFCreator($paramsArr = array()){

			//print_r($this->pythonFilePath);print_r($this->allowedDomains);print_r($this->_request);die;
			
		
			// Cross validation if the request method is POST else it will return "Not Acceptable" status
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			$id = (int)$this->_request['id'];
			$domain = $this->_request['domain'];
			$fileName = $this->_request['fileName'];
			
			if(in_array($domain, $this->allowedDomains)){				

				$inputFolder = self::WWW_PATH."uploads/".$domain."/".$id."/".$fileName;
				//$inputFolder = "/uploads/".$domain."/".$id."/".$fileName;
				$sourceFileNameArr = explode(".",$fileName);
				$arrLen = count($sourceFileNameArr) > 0 ? (count($sourceFileNameArr) - 1):'';
				$fileExt = $sourceFileNameArr[$arrLen];
				$fileNameInPdf = count($sourceFileNameArr) > 0 ? ($sourceFileNameArr[0].".pdf"):'';
				
				$outputFolder = $fileNameInPdf?(self::WWW_PATH."uploads/".$domain."/".$id."/".$fileNameInPdf):'';
				//$outputFolder = $fileNameInPdf?("/uploads/".$domain."/".$id."/".$fileNameInPdf):'';
					
				if($inputFolder && $outputFolder){
							
					//$officePath = $CFG->FVOfficePath;
					//$headLessCommand = $CFG->FVHeadLessCommand;
					//$officeCommand = '"'.$officePath.'" '.$headLessCommand;
						
					$pythonPath = "python ";
					$pythonFilePath = $this->pythonFilePath;
					$pythonCommand = $pythonPath.' '.$pythonFilePath.' '.$inputFolder.' '.$outputFolder;
					
					try {
							
						if($inputFolder){
							exec($pythonCommand, $output, $return);
						}
							
						//print_r($output);
						//print_r($return);die;
							
					} catch (Exception $e) {
						echo $e->getMessage();
					}
				}
				
				if($return){
				 $success = array('status' => "Error", "msg" => $output[0]);
				 //$this->response($this->json($success),500);
				 echo "error";
				}else{
				 $success = array('status' => "Success", "msg" => "Successfully created.");
				 //$this->response($this->json($success),200);
				 echo "success";
				} 

			}else
				$this->response('',204);	// If no records "No Content" status
		}
				
		
		
		/*
		 *	Encode array into JSON
		*/
		private function json($data){
			if(is_array($data)){
				return json_encode($data);
			}
		}
	}
	
	// Initiate Library
	
	$api = new API;
	$api->processApi();
?>