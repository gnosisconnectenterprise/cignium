<?php 
ini_set('error_reporting', 1);
error_reporting(E_ALL);

include('../config.php');

global $USER;

$CFG->FVOutputFile = 'output.pdf';	
$wwwpath = str_replace('\\','\\\\', $CFG->wwwpath);
$wwwroot = $CFG->wwwroot;	
$CFG->VJOsName = php_uname();
//if( strstr($_SERVER['SERVER_SOFTWARE'],'PHP')){ // Window Seeting

if (substr($CFG->VJOsName, 0, 7) == "Windows"){
	
	$CFG->FVOfficePath = "C:/Program Files (x86)/OpenOffice.org 3/program/soffice.exe";
	$CFG->FVHeadLessCommand = "-headless -nologo -norestore -accept=socket,host=localhost,port=2002;urp;StarOffice.ServiceManager";
	
	$CFG->FVPythonPath = "C:\\Program Files (x86)\\OpenOffice.org 3\\program\\python";
	$CFG->FVPythonFilePath = $wwwpath."\\viewerjs\\DocumentConverter.py";

	
}else{  // Linux Setting

	//$CFG->FVOfficePath = "C:\\Program Files (x86)\\OpenOffice.org 3\\program\\soffice.exe";
	//$CFG->FVHeadLessCommand = "-headless -nologo -norestore -accept=socket,host=localhost,port=2002;urp;StarOffice.ServiceManager";
	
	$CFG->FVPythonPath = "python ";
	$CFG->FVPythonFilePath = $wwwpath."/viewerjs/DocumentConverter.py ";
	
}



function fvPDFCreator($inputFolder, $outputFolder){

	global $CFG;
	$return = 1;

	if($inputFolder && $outputFolder){
		//if( strstr($_SERVER['SERVER_SOFTWARE'],'PHP')){
		if (substr($CFG->VJOsName, 0, 7) == "Windows"){
				
			$officePath = $CFG->FVOfficePath;
			$headLessCommand = $CFG->FVHeadLessCommand;
			$officeCommand = '"'.$officePath.'" '.$headLessCommand;
				
			$pythonPath = $CFG->FVPythonPath;
			$pythonFilePath = $CFG->FVPythonFilePath;
			//$inputFolder = $inputFolder?$inputFolder:$CFG->FVInputFolder;
				
			$pythonCommand = '"'.$pythonPath.'" '.$pythonFilePath.' '.$inputFolder.' '.$outputFolder;
				
			try {
					
				$isOfficeStartedStr = shell_exec("netstat -anp tcp");
				if(!strstr($isOfficeStartedStr, '127.0.0.1:2002')){
					$startOffCommand = "fileviewer.bat";
					exec($startOffCommand, $out, $rt);
					//echo "<pre>";print_r($out);print_r($ret);die;

						
				}

				//system("cmd /c ".$startOffCommand);
				//exec('c:\WINDOWS\system32\cmd.exe /c START '.$startOffCommand);
				//echo $str = exec('start /B /C '.$startOffCommand, $result);
				//echo shell_exec($officeCommand);
				//print_r($result);die;
				if($inputFolder){
					//exec($officeCommand, $out, $rt);
					//if($rt == 0){
					exec($pythonCommand, $output, $return);
					//}
				}

					
			} catch (Exception $e) {
				echo $e->getMessage();
			}
				
		}else{
				
			//$officePath = $CFG->FVOfficePath;
			//$headLessCommand = $CFG->FVHeadLessCommand;
			//$officeCommand = '"'.$officePath.'" '.$headLessCommand;
				
			$pythonPath = $CFG->FVPythonPath;
			$pythonFilePath = $CFG->FVPythonFilePath;
			//$inputFolder = $inputFolder?$inputFolder:$CFG->FVInputFolder;
			$pythonCommand = $pythonPath.' '.$pythonFilePath.' '.$inputFolder.' '.$outputFolder;
				
			try {
					
				/* su wwww-data
				 echo $HOME
				exit;
				chown www-data:www-data /var/www -R */

				//exec($officeCommand);
				if($inputFolder){
					exec($pythonCommand, $output, $return);
				}
					
					
			} catch (Exception $e) {
				echo $e->getMessage();
			}
		}

	}
	//echo "<pre>";print_r($output);print_r($return);die;

	//unlink($inputFolder);
	//unlink($outputFolder);

	return $return;

}

