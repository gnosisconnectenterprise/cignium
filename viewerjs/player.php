<?php 
include('../config.php');


if(isset($_GET['contextId']) && $_GET['contextId'] && isset($_GET['download']) && $_GET['download'] == 1){
	$resourceDownloadPath = getResourceDownloadPath($_GET['contextId']);
	forceDownload($resourceDownloadPath);
}else{

	$contextId = isset($_GET['contextId']) && $_GET['contextId']?urldecode($_GET['contextId']):0;
	$isResourceDownloadable = 0;
	if($contextId){
		$isResourceDownloadable = isResourceDownloadable($contextId);
		//$resourceDownloadPath = getResourceDownloadPath($contextId);
	}
	
}

$outputFile = isset($_GET['outputFile']) && $_GET['outputFile']?urldecode($_GET['outputFile']):'';
$inputFolder = $CFG->FVResourceUploadURL.$contextId."/".$outputFile;

$fileExt = isset($_GET['fileExt']) && $_GET['fileExt']?$_GET['fileExt']:'';

$FVAllowedAudioTypes = getFVAllowedAudioTypes();
$FVAllowedVideoTypes = getFVAllowedVideoTypes();
//pr($FVAllowedVideoTypes);die;
//echo "<pre>";print_r($_REQUEST);die;




?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<!-- Website Design By: www.happyworm.com -->
<title>Demo : Multi instanced jPlayer video players</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="<?php echo $CFG->wwwroot;?>/jPlayer-2.9.2/dist/skin/blue.monday/css/jplayer.blue.monday.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/jPlayer-2.9.2/lib/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/jPlayer-2.9.2/dist/jplayer/jquery.jplayer.min.js"></script>
<script type="text/javascript">
//<![CDATA[

$(document).ready(function(){

	<?php if(in_array($fileExt, $FVAllowedVideoTypes)){?>
		
		$("#jquery_jplayer_1").jPlayer({
			ready: function () {
				$(this).jPlayer("setMedia", {
					title: "<?php echo $outputFile;?>",
					flv: "<?php echo $inputFolder;?>",
					m4v: "<?php echo $inputFolder;?>",
					ogv: "<?php echo $inputFolder;?>",
					webmv: "<?php echo $inputFolder;?>"/*,
					poster: "http://www.jplayer.org/video/poster/Big_Buck_Bunny_Trailer_480x270.png"*/
				}).jPlayer("play");
			},
			play: function() { // To avoid multiple jPlayers playing together.
				$(this).jPlayer("pauseOthers");
			},
			swfPath: "<?php echo $CFG->wwwroot;?>/jPlayer-2.9.2/dist/jplayer",
			supplied: "<?php echo $fileExt == 'flv'?'flv':'m4v, ogv, webmv'; ?>",
			globalVolume: true,
			useStateClassSkin: true,
			autoBlur: false,
			smoothPlayBar: true,
			keyEnabled: true
		});

	 <?php }elseif(in_array($fileExt, $FVAllowedAudioTypes)){ ?>
		
		$("#jquery_jplayer_1").jPlayer({
			ready: function () {
				$(this).jPlayer("setMedia", {
					title: "<?php echo $outputFile;?>",
					mp3: "<?php echo $inputFolder;?>"
				}).jPlayer("play");
			},
			swfPath: "<?php echo $CFG->wwwroot;?>/jPlayer-2.9.2/dist/jplayer",
			supplied: "mp3",
			wmode: "window",
			useStateClassSkin: true,
			autoBlur: false,
			smoothPlayBar: true,
			keyEnabled: true,
			remainingDuration: true,
			toggleDuration: true
		});

	 <?php } ?>	


	$(document).on('click', '#download', function() { 
        window.open("/viewerjs/player.php?download=1&contextId=<?php echo $contextId;?>", "_parent")
	});
	
});
//]]>
</script>

<style>

body {
    background-color: #888;
    background-image: url("images/texture.png");
    bottom: 0px;
    left: 0;
    overflow: auto;
    padding-bottom: 6px;
    padding-top: 6px;
    position: absolute;
    right: 0;
    text-align: center;
    top: 5px;
}

.toolbarButton.download::before {
    content: url("images/toolbarButton-download.png");
    display: inline-block;
}
#titlebarRight > * {
    float: left;
}

.toolbarButton, .dropdownToolbarButton {
    margin: 3px 2px 4px 0;
}

.toolbarButton, .dropdownToolbarButton {
    -moz-user-select: none;
    border: 1px solid transparent;
    border-radius: 2px;
    color: hsl(0, 0%, 95%);
    cursor: default;
    font-size: 12px;
    line-height: 14px;
    min-width: 16px;
    padding: 2px 6px;
    transition-duration: 150ms;
    transition-property: background-color, border-color, box-shadow;
    transition-timing-function: ease;
}
.toolbarButton {
    background-color: rgba(0, 0, 0, 0);
    background-image: none;
    border: 0 none;
    border-radius: 2px;
    height: 25px;
    min-width: 32px;
}


#documentName {
    font-size: 14px;
}
#documentName {
    color: #f2f2f2;
    font-family: sans-serif;
    line-height: 14px;
    margin-left: 10px;
    margin-right: 10px;
    margin-top: 8px;
}

#titlebar {
    background-image: url("images/texture.png"), linear-gradient(rgba(69, 69, 69, 0.95), rgba(82, 82, 82, 0.99));
    box-shadow: 0 1px 3px rgba(50, 50, 50, 0.75);
    height: 32px;
    left: 0;
    overflow: hidden;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: 2;
}


#titlebar {
    margin: 0;
    padding: 0;
}

#titlebar::-moz-selection {
    background: rgba(0, 0, 255, 0.3) none repeat scroll 0 0;
}

#titlebarRight {
    position: absolute;
    right: 0;
    top: 0;
}

</style>
</head>
<body>

<div id = "titlebar">
    <div id = "documentName"><?php echo $outputFile; ?></div>
     <div id = "titlebarRight">
         <?php if($isResourceDownloadable){ ?>
         <button id = "download"  class = "toolbarButton download" title = "Download"></button>
         <?php } ?>                                                     
     </div>                       
</div>

<?php if(in_array($fileExt, $FVAllowedVideoTypes)){?>

<div id="jp_container_1" class="jp-video jp-video-360p" style="margin:40px auto;background-color: #000; " role="application" aria-label="media player">
	<div class="jp-type-single">
		<div id="jquery_jplayer_1" class="jp-jplayer" style="margin:auto;"></div>
		<div class="jp-gui">
			<div class="jp-video-play">
				<button class="jp-video-play-icon" role="button" tabindex="0">play</button>
			</div>
			<div class="jp-interface">
				<div class="jp-progress">
					<div class="jp-seek-bar">
						<div class="jp-play-bar"></div>
					</div>
				</div>
				<div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
				<div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
				<div class="jp-controls-holder">
					<div class="jp-controls">
						<button class="jp-play" role="button" tabindex="0">play</button>
						<button class="jp-stop" role="button" tabindex="0">stop</button>
					</div>
					<div class="jp-volume-controls">
						<button class="jp-mute" role="button" tabindex="0">mute</button>
						<button class="jp-volume-max" role="button" tabindex="0">max volume</button>
						<div class="jp-volume-bar">
							<div class="jp-volume-bar-value"></div>
						</div>
					</div>
					<div class="jp-toggles">
						<button class="jp-repeat" role="button" tabindex="0">repeat</button>
						<button class="jp-full-screen" role="button" tabindex="0">full screen</button>
					</div>
				</div>
				<div class="jp-details" style="display:none">
					<div class="jp-title" aria-label="title">&nbsp;</div>
				</div>
			</div>
		</div>
		<div class="jp-no-solution">
			<span>Update Required</span>
			To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
		</div>
	</div>
</div>
 <?php }elseif(in_array($fileExt, $FVAllowedAudioTypes)){ ?>

<div id="jquery_jplayer_1" class="jp-jplayer"  style="margin:auto;"></div>
<div id="jp_container_1" class="jp-audio" style="margin:40px auto;background-color: #000; " role="application" aria-label="media player">
	<div class="jp-type-single">
		<div class="jp-gui jp-interface">
			<div class="jp-controls">
				<button class="jp-play" role="button" tabindex="0">play</button>
				<button class="jp-stop" role="button" tabindex="0">stop</button>
			</div>
			<div class="jp-progress">
				<div class="jp-seek-bar">
					<div class="jp-play-bar"></div>
				</div>
			</div>
			<div class="jp-volume-controls">
				<button class="jp-mute" role="button" tabindex="0">mute</button>
				<button class="jp-volume-max" role="button" tabindex="0">max volume</button>
				<div class="jp-volume-bar">
					<div class="jp-volume-bar-value"></div>
				</div>
			</div>
			<div class="jp-time-holder">
				<div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
				<div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
				<div class="jp-toggles">
					<button class="jp-repeat" role="button" tabindex="0">repeat</button>
				</div>
			</div>
		</div>
		<div class="jp-details" style="display:none">
			<div class="jp-title" aria-label="title">&nbsp;</div>
		</div>
		
		
		<div class="jp-no-solution">
			<span>Update Required</span>
			To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
		</div>
	</div>
</div>

<?php } ?>
</body>

</html>




