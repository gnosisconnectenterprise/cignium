<?php
	require_once('config.php');
	global $USER,$CFG,$DB;

	// Start setting up the page
	$header = get_string('error');
	$PAGE->set_context($context);
	$PAGE->set_url('/index.php', $params);
	$PAGE->set_pagelayout('learnerdashboard');
	$PAGE->set_pagetype('my-index');
	$PAGE->blocks->add_region('content');
	$PAGE->set_title($header);
	$PAGE->set_heading($header);

	echo $OUTPUT->header();
	echo '<b>'.get_string('error_page_message').'</b>';
	echo $OUTPUT->footer();
?>