<?php

require_once(dirname(__FILE__) . '/../config.php');
require_once(dirname(__FILE__) . '/lib.php');
require_once($CFG->dirroot . '/local/user/selector/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->libdir . '/filelib.php');
require_login();
$site = get_site();
$courseid = required_param('id', PARAM_INT);
$course->id = $courseid;
$cancel  = optional_param('cancel', false, PARAM_BOOL);
global $DB,$CFG,$USER;
$PAGE->set_url('/user/assignusers.php', array('id'=>$courseid));
$PAGE->set_pagelayout('admin');
if(isset($_REQUEST['formsubmit']) && $_REQUEST['formsubmit'] == 1){
	SaveCourseMappingData($_REQUEST);
	redirect($CFG->wwwroot.'/course/index.php');
}

$context = context_system::instance();
$returnurl = $CFG->wwwroot.'/course/index.php';

if ($cancel) {
	$urlCancel = new moodle_url($CFG->wwwroot.'/course/courseview.php', array('id'=>$course->id));
    redirect($urlCancel);
}
$courseCreatedBy = $DB->get_record_sql("SELECT id,createdby,publish FROM  mdl_course WHERE id =".$courseid);
$PAGE->navbar->add(get_string('managecourses'), new moodle_url($CFG->wwwroot.'/course/index.php'));
if($courseCreatedBy->createdby == $USER->id || $USER->archetype == $CFG->userTypeAdmin){
	$PAGE->navbar->add(get_string('editcourse','course'), new moodle_url($CFG->wwwroot.'/course/edit.php',array('id'=>$courseid)));
}
if($courseCreatedBy->publish == 0){
	redirect($returnurl);
}
//$PAGE->navbar->add(get_string('course_allocation','course'));
$PAGE->navbar->add(get_string('allocation'));


$courses = new courses_assignment_selector('', array('courseid' => $courseid));

$courses->courseDepartmentList();
$courses->courseNonDepartmentList();

$courses->courseTeamList();
$courses->courseNonTeamList();

$courses->courseUserList();
$courses->courseNonUserList();


/// Print header
$PAGE->set_title("$site->fullname: $stradduserstogroup");
$PAGE->set_heading($site->fullname);
echo $OUTPUT->header();


/// Print the editing form
$courseHTML = '';
$courseHTML .= "<div class='tabsOuter'>";
	if($courseCreatedBy->createdby == $USER->id || $USER->archetype == $CFG->userTypeAdmin){
		$courseHTML .= "<div class='tabLinks'>";
		include_once('course_tabs.php');
		$courseHTML .= "</div>";
	}
	echo $courseHTML;
/*
$block IS 'team_course'
$block IS 'non_team_course'
$block IS 'department_course'
$block IS 'non_department_course'
$block IS 'user_course'
$block IS 'non_user_course'
*/

$activeIds = getActiveCourses($courseid);

if($activeIds == ''){
	echo '<div style="text-align: center; margin-top: 20px; height: 60px;">';
	echo get_string('cannotenroluser');
	echo '</div>';
	echo $OUTPUT->footer();
	die;
}
$display = '';
if($USER->archetype != $CFG->userTypeAdmin){
	$display = 'style = "display:none"';
}
?>
<form id="assignform" method="post" action="<?php echo $CFG->wwwroot; ?>/course/courseallocation.php?id=<?php echo $courseid; ?>">
<input type = 'hidden' name = "formsubmit" value = "1">
<?php
echo '<fieldset class="" id="yui_3_13_0_2_1404228340264_14">';
echo '<div class="msg-tab-html">
		<div class="msg-tab-label">'.get_string('enroll_to').'</div>
		<div class="msg-tab-block">
			<div class="tab-msg-all">
				<div class="msg-tab-block" style="float:left;padding:1%;">
					<input type="checkbox" name="department_all" id="department_all"  > All
				</div>
			</div>
		<div class="tab-box">';
$activeLink = 'activeLink';
if($USER->archetype == $CFG->userTypeAdmin){
	$activeLink = '';
	echo '<a href="javascript:;" class="tabLink activeLink" id="cont-1" '.$display.'>'.get_string('departments').'</a>';
}
echo '<a href="javascript:;" class="tabLink '.$activeLink.'" id="cont-2">'.get_string('teams').'</a> 
			<a href="javascript:;" class="tabLink " id="cont-3">'.get_string('users').'</a> 
		</div>';
$hide = '';
if($USER->archetype == $CFG->userTypeAdmin){
	echo '<div class="tabcontent paddingAll" id="cont-1-1"><div class="departments">';
	$hide = 'hide';
?>
<div id="addmembersform">
    <div>
    <input type="hidden" name="sesskey" value="<?php p(sesskey()); ?>" />

    <table class="generaltable generalbox groupmanagementtable boxaligncenter" summary="">
    <tr>
	 <td id='potentialcell'>
          <p>
            <label for="addselect"><?php echo get_string('coursenondepartment');?></label>
          </p>
		  <div id="department_select_wrapper" class="userselector">
			<select multiple="multiple" name = "department_course[]" size = '20'>
				<?php $courses->course_option_list('non_department_course'); ?>
			</select>
			<!-- <div>
				<label for="addselect_searchtext"><?php echo get_string('search');?></label>
				<input type="text" value="" size="15" id="department_addselect_searchtext" name="addselect_searchtext">
				<input type="button" value="Clear" id="department_addselect_clearbutton" >
			</div> -->
		  </div>
      </td>
      <td id='buttonscell'>
        <p class="arrow_button">
            <input name="add" id="department_add" type="button" value="<?php echo $OUTPUT->larrow().'&nbsp;'.get_string('add'); ?>" title="<?php print_string('add'); ?>" disabled = ""/>
            <input name="remove" id="department_remove" type="button" value="<?php echo get_string('remove').'&nbsp;'.$OUTPUT->rarrow(); ?>" title="<?php print_string('remove'); ?>" disabled = ""/>
        </p>
      </td>
	   <td id='existingcell'>
          <p>
            <label for="removeselect"><?php echo get_string('coursedepartment');?></label>
          </p>
          <div id="department_remove_wrapper" class="userselector">
			<select multiple="multiple" name = "add_department_course[]" size = '20'>
				<?php $courses->course_option_list('department_course'); ?>
			</select>
			<!-- <div>
				<label for="removeselect_searchtext"><?php echo get_string('search');?></label>
				<input type="text" value="" size="15" id="department_removeselect_searchtext" name="removeselect_searchtext">
				<input type="button" value="Clear" id="department_removeselect_clearbutton" >
			</div> -->
		  </div>
        </td>
    </tr>
    </table>
    </div>
</div>
<?php
echo '</div>																					
  </div>';
}
echo ' <div class="tabcontent paddingAll '.$hide.'" id="cont-2-1">';
  ?>
<div id="addmembersform">
    <div>
    <input type="hidden" name="sesskey" value="<?php p(sesskey()); ?>" />

    <table class="generaltable generalbox groupmanagementtable boxaligncenter" summary="">
    <tr>
	 <td id='potentialcell'>
          <p>
            <label for="addselect"><?php echo get_string('coursenonteams');?></label>
          </p>
		  <div id="team_addselect_wrapper" class="userselector">
			<select multiple="multiple" name = "addteam[]" size = '20'>
				<?php $courses->course_option_list('non_team_course'); ?>
			</select>
			<!--<div>
				<label for="addselect_searchtext"><?php echo get_string('search');?></label>
				<input type="text" value="" size="15" id="team_addselect_searchtext" name="addselect_searchtext">
				<input type="button" value="Clear" id="team_addselect_clearbutton" >
			</div>-->
		  </div>
      </td>
      <td id='buttonscell'>
        <p class="arrow_button">
            <input name="add" id="team_add" type="button" value="<?php echo $OUTPUT->larrow().'&nbsp;'.get_string('add'); ?>" title="<?php print_string('add'); ?>" disabled = ""/>
            <input name="remove" id="team_remove" type="button" value="<?php echo get_string('remove').'&nbsp;'.$OUTPUT->rarrow(); ?>" title="<?php print_string('remove'); ?>" disabled = ""/>
        </p>
      </td>
	   <td id='existingcell'>
          <p>
            <label for="removeselect"><?php echo get_string('courseteams');?></label>
          </p>
          <div id="team_removeselect_wrapper" class="userselector">
			<select multiple="multiple" name = "addteamcourses[]" size = '20'>
				<?php $courses->course_option_list('team_course'); ?>
			</select>
			<!--<div>
				<label for="removeselect_searchtext"><?php echo get_string('search');?></label>
				<input type="text" value="" size="15" id="team_removeselect_searchtext" name="removeselect_searchtext">
				<input type="button" value="Clear" id="team_removeselect_clearbutton" >
			</div> -->
		  </div>
        </td>
    </tr>
    </table>
    </div>
</div>
<?php
	echo '</div><div class="tabcontent paddingAll hide" id="cont-3-1">';
 ?>
<div id="addmembersform">
    <div>
    <input type="hidden" name="sesskey" value="<?php p(sesskey()); ?>" />

    <table class="generaltable generalbox groupmanagementtable boxaligncenter" summary="">
    <tr>
	 <td id='potentialcell'>
          <p>
            <label for="addselect"><?php echo get_string('coursenonuser');?></label>
          </p>
		  <div id="user_addselect_wrapper" class="userselector">
			<select multiple="multiple" name = "adduser[]" size = '20'>
				<?php $courses->course_option_list('non_user_course'); ?>
			</select>
			<!--<div>
				<label for="addselect_searchtext"><?php echo get_string('search');?></label>
				<input type="text" value="" size="15" id="user_addselect_searchtext" name="addselect_searchtext">
				<input type="button" value="Clear" id="user_addselect_clearbutton" >
			</div> -->
		  </div>
      </td>
      <td id='buttonscell'>
        <p class="arrow_button">
            <input name="add" id="user_add" type="button" value="<?php echo $OUTPUT->larrow().'&nbsp;'.get_string('add'); ?>" title="<?php print_string('add'); ?>" disabled = ""/>
            <input name="remove" id="user_remove" type="button" value="<?php echo get_string('remove').'&nbsp;'.$OUTPUT->rarrow(); ?>" title="<?php print_string('remove'); ?>" disabled = ""/>
        </p>
      </td>
	   <td id='existingcell'>
          <p>
            <label for="removeselect"><?php echo get_string('courseuser');?></label>
          </p>
          <div id="user_removeselect_wrapper" class="userselector">
			<select multiple="multiple" name = "addusercourse[]" size = '20'>
				<?php $courses->course_option_list('user_course'); ?>
			</select>
			<!--<div>
				<label for="removeselect_searchtext"><?php echo get_string('search');?></label>
				<input type="text" value="" size="15" id="user_removeselect_searchtext" name="removeselect_searchtext">
				<input type="button" value="Clear" id="user_removeselect_clearbutton" >
			</div>-->
		  </div>
        </td>
    </tr>
    </table>
    </div>
    
</div>
<?php
echo '</div></div></div>';
?>
</fieldset>
<fieldset style="margin: 10px 0px 0px; text-align: center; clear: both;">
	<div >
		<div class="fitem fitem_actionbuttons fitem_fgroup" id="fgroup_id_buttonar">
			<div class="felement fgroup" >
            
                <input type="button" id="id_cancel" class=" btn-cancel" onclick="skipClientValidation = true; return true;" value="<?php echo get_string('cancel')?>" name="cancel">
				<input type="button" id="id_submitbutton" value="<?php echo get_string('savechanges')?>" name="submitbutton">
				
			</div>
		</div>
	</div>
</fieldset>
</form>
<script>
$(document).ready(function(){

	var userRole = "<?php echo $USER->archetype; ?>";
	var userManager = "<?php echo $CFG->userTypeManager; ?>";
	
	$(".tabLink").each(function(){
      $(this).click(function(){
        tabeId = $(this).attr("id");
        $(".tabLink").removeClass("activeLink");
        $(this).addClass("activeLink");
        $(".tabcontent").addClass("hide");
        $("#"+tabeId+"-1").removeClass("hide");					
        return false;	  
      });
    }); 
	 
	$("#addselect_searchtext").keyup(function(){
		var searchText = $(this).val();
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/user/searchcourses.php',
				type:'POST',
				data:'action=searchNonCourse&courseid=<?php echo $courseid; ?>&search_text='+searchText,
				success:function(data){
					$('#addselect_wrapper').find('select').html(data);
				}
		});
	});
	
	$("#addselect_clearbutton").click(function(){
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/user/searchcourses.php',
				type:'POST',
				data:'action=searchNonCourse&courseid=<?php echo $courseid; ?>',
				success:function(data){
					$('#addselect_wrapper').find('select').html(data);
					$("#addselect_searchtext").val('');
				}
		});
	});
	
	$("#removeselect_clearbutton").click(function(){
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/user/searchcourses.php',
				type:'POST',
				data:'action=searchUserCourse&courseid=<?php echo $courseid; ?>',
				success:function(data){
					$('#removeselect_wrapper').find('select').html(data);
					$("#removeselect_searchtext").val('');
				}
		});
	});
	
	$("#removeselect_searchtext").keyup(function(){
		var searchText = $(this).val();
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/user/searchcourses.php',
				type:'POST',
				data:'action=searchUserCourse&courseid=<?php echo $courseid; ?>&search_text='+searchText,
				success:function(data){
					$('#removeselect_wrapper').find('select').html(data);
				}
		});
	});
	
	if(userRole != userManager){
		var fromDepartment = $('#department_select_wrapper select');
		var toDepartment = $('#department_remove_wrapper select');
	}

	var fromTeam = $('#team_addselect_wrapper select');
	var toTeam = $('#team_removeselect_wrapper select');

	var fromUser = $('#user_addselect_wrapper select');
	var toUser = $('#user_removeselect_wrapper select');

	$(document).on("change", "[name = 'department_course[]']",function(){ 
		$("#department_add").removeAttr("disabled");
	});
	$(document).on("change", "[name = 'add_department_course[]']",function(){
		$("#department_remove").removeAttr("disabled");
	});

	$(document).on("change", "[name = 'addteam[]']",function(){
		$("#team_add").removeAttr("disabled");
	});
	$(document).on("change", "[name = 'addteamcourses[]']",function(){
		$("#team_remove").removeAttr("disabled");
	});
	$(document).on("change", "[name = 'adduser[]']",function(){
		$("#user_add").removeAttr("disabled");
	});
	$(document).on("change", "[name = 'addusercourse[]']",function(){
		if($(this).attr('class') == 'option-disable'){
			$("#user_remove").addAttr("disabled");
		}else{
			$("#user_remove").removeAttr("disabled");
		}
	});

	$(document).on("click","#department_add",function(){
			fromDepartment.find('option:selected').appendTo(toDepartment.find('optgroup'));
			maintainSelectCount(fromDepartment,toDepartment,"<?php echo get_string('departments'); ?>");
	});
	$(document).on("click","#team_add",function(){
			fromTeam.find('option:selected').appendTo(toTeam.find('optgroup'));
			maintainSelectCount(fromTeam,toTeam,"<?php echo get_string('teams'); ?>");
	});
	$(document).on("click","#user_add",function(){
			fromUser.find('option:selected').appendTo(toUser.find('optgroup'));
			maintainSelectCount(fromUser,toUser,"<?php echo get_string('users'); ?>");
	});

	$(document).on("click","#department_remove",function(){
			toDepartment.find('option:selected').appendTo(fromDepartment.find('optgroup'));
			maintainSelectCount(fromDepartment,toDepartment,"<?php echo get_string('departments'); ?>");
	});
	$(document).on("click","#team_remove",function(){
			toTeam.find('option:selected').appendTo(fromTeam.find('optgroup'));
			maintainSelectCount(fromTeam,toTeam,"<?php echo get_string('teams'); ?>");
	});
	$(document).on("click","#user_remove",function(){
		var err = 0;
		toUser.find( "option:selected" ).each(function() {
			if($(this).attr('class') == 'option-disable'){
				err = 1;
			}else{
				$(this).appendTo(fromUser.find('optgroup'));
			}
		});
		if(err == 1){
			alert("<?php echo get_string('assignment_error');?>");
			toUser.find( "option" ).each(function() {
				//$(this).removeAttr('selected');
			});
		}
		maintainSelectCount(fromUser,toUser,"<?php echo get_string('users'); ?>");
	});
	$("#id_submitbutton").click(function(){
		if(userRole != userManager){
			fromDepartment.find('optgroup').find("option").each(function(){
				$(this).removeAttr("selected");
			});
		}
		fromTeam.find('optgroup').find("option").each(function(){
			$(this).removeAttr("selected");
		});
		fromUser.find('optgroup').find("option").each(function(){
			$(this).removeAttr("selected");
		});
		if($("#department_all").is(':checked')){
			if(userRole != userManager){
				fromDepartment.find('optgroup').find("option").each(function(){
					$(this).appendTo(toDepartment.find('optgroup'));
				});
			}
			fromTeam.find('optgroup').find("option").each(function(){
				$(this).appendTo(toTeam.find('optgroup'));
			});
			fromUser.find('optgroup').find("option").each(function(){
				$(this).appendTo(toUser.find('optgroup'));
			});
		}
		if(userRole != userManager){
			toDepartment.find('option').prop('selected', true);
		}
		toTeam.find('option').prop('selected', true);
		toUser.find('option').prop('selected', true);
		$("#assignform").submit();
	});
	$("#department_all").click(function(){
		if($("#department_all").is(':checked')){
			$(".tab-box").hide();
			$(".tabcontent").hide();
		}else{
			$(".tab-box").show();
			$(".tabcontent").show();
		}
	});
	$('#id_cancel').click(function(){
		window.location.href = '<?php $urlCancel = new moodle_url($CFG->wwwroot.'/course/courseview.php', array('id'=>$courseid)); echo $urlCancel; ?>';
	});
	
	
	/*  Below code is being used for focus on departments/teams/users automatically while we are using chrome*/
	/*$('[name = "department_course[]"]').focus();
	$('[name = "department_course[]"]').focus(function () {
      $(this).select();
    });
	*/
	// End 

});
</script>
<?php
echo '</div>';
echo $OUTPUT->footer();