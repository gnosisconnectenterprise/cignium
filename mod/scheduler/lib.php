<?PHP

/**
 * Library (public API) of the scheduler module
 *
 * @package mod
 * @subpackage scheduler
 * @copyright 2011 Henning Bostelmann and others (see README.txt)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined ( 'MOODLE_INTERNAL' ) || die ();

// / Library of functions and constants for module scheduler
include_once $CFG->dirroot . '/mod/scheduler/locallib.php';
include_once $CFG->dirroot . '/mod/scheduler/mailtemplatelib.php';

define ( 'SCHEDULER_TIMEUNKNOWN', 0 ); // This is used for appointments for which no time is entered
define ( 'SCHEDULER_SELF', 0 ); // Used for setting conflict search scope
define ( 'SCHEDULER_OTHERS', 1 ); // Used for setting conflict search scope
define ( 'SCHEDULER_ALL', 2 ); // Used for setting conflict search scope

define ( 'MEAN_GRADE', 0 ); // Used for grading strategy
define ( 'MAX_GRADE', 1 ); // Used for grading strategy

/**
 * Given an object containing all the necessary data,
 * will create a new instance and return the id number
 * of the new instance.
 * 
 * @param object $scheduler
 *        	the current instance
 * @return int the new instance id
 * @uses $DB
 */
function scheduler_add_instance($scheduler) {
	global $DB,$USER;
	$pre_requisite_text = $scheduler->pre_requisite['text'];
	$scheduler->pre_requisite = $pre_requisite_text;
	$scheduler->timecreated = time ();
	
	$scheduler->timemodified = time ();
	$scheduler->createdby = $USER->id;
/* 	if($scheduler->startdate==$scheduler->enddate){
		$scheduler->enddate = $scheduler->enddate+86399;
	} */
	$scheduler->enddate = $scheduler->enddate+86399;
	if($scheduler->no_of_seats==0)
	{
		$scheduler->no_of_seats = '';
	}
	$id = $DB->insert_record ( 'scheduler', $scheduler );
	$scheduler->id = $id;
	if($scheduler->enddate > time ()){
		emailForClassAddUpdateToTeacher($scheduler->teacher,$scheduler->id,'add');
	}
	//scheduler_grade_item_update ( $scheduler );
	
	return $id;
}

/**
 * Given an object containing all the necessary data,
 * (defined by the form in mod.html) this function
 * will update an existing instance with new data.
 * 
 * @param object $scheduler
 *        	the current instance
 * @return object the updated instance
 * @uses $DB
 */
function scheduler_update_instance($scheduler) {
	global $DB;
//pr($scheduler->pre_requisite);
	$scheduler->timemodified = time ();
	$scheduler->id = $scheduler->instance;
	$pre_requisite_text = $scheduler->pre_requisite['text'];
	$scheduler->pre_requisite = $pre_requisite_text;
	
	/* if($scheduler->startdate==$scheduler->enddate){
	$scheduler->enddate = $scheduler->enddate+86399;
	} */
	$scheduler->enddate = $scheduler->enddate+86399;
	if($scheduler->no_of_seats==0)
	{
		$scheduler->no_of_seats = '';
	}
	
	$teacherRecord = $DB->get_record_sql('SELECT s.* FROM mdl_scheduler as s WHERE s.id = '.$scheduler->id);
	
	$scheduler->introformat=1;
	$DB->update_record ( 'scheduler', $scheduler );
	if($scheduler->enddate > time ()){
		if($teacherRecord->teacher != $scheduler->teacher){
			emailForClassAddUpdateToTeacher($teacherRecord->teacher,$scheduler->id,'delete');
			emailForClassAddUpdateToTeacher($scheduler->teacher,$scheduler->id,'add');
		}else{
			if($teacherRecord->name != $scheduler->name || $teacherRecord->startdate != $scheduler->startdate || $teacherRecord->enddate != $scheduler->enddate || $teacherRecord->no_of_seats != $scheduler->no_of_seats  || $teacherRecord->pre_requisite != $scheduler->pre_requisite || $teacherRecord->enrolmenttype != $scheduler->enrolmenttype){
				emailForClassAddUpdateToTeacher($scheduler->teacher,$scheduler->id,'update');
			}
		}
	}
	// update grade item and grades
	//scheduler_update_grades ( $scheduler );
	
	return true;
}

/**
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 * 
 * @param int $id
 *        	the instance to be deleted
 * @return boolean true if success, false otherwise
 * @uses $DB
 */
function scheduler_delete_instance($id) {
	global $CFG, $DB;
	
	if (! $scheduler = $DB->get_record ( 'scheduler', array (
			'id' => $id 
	) )) {
		return false;
	}
	
	$result = $DB->delete_records ( 'scheduler', array (
			'id' => $scheduler->id 
	) );
	
	$oldslots = $DB->get_records ( 'scheduler_slots', array (
			'schedulerid' => $scheduler->id 
	), '', 'id, id' );
	if ($oldslots) {
		foreach ( array_keys ( $oldslots ) as $slotid ) {
			// will delete appointments and remaining related events
			scheduler_delete_slot ( $slotid );
		}
	}
	
	scheduler_grade_item_delete ( $scheduler );
	
	return $result;
}

/**
 * Return a small object with summary information about what a
 * user has done with a given particular instance of this module
 * Used for user activity reports.
 * $return->time = the time they did it
 * $return->info = a short text description
 * 
 * @param object $course
 *        	the course instance
 * @param object $user
 *        	the concerned user instance
 * @param object $mod
 *        	the current course module instance
 * @param object $scheduler
 *        	the activity module behind the course module instance
 * @return object an information object as defined above
 */
function scheduler_user_outline($course, $user, $mod, $scheduler) {
	$return = NULL;
	return $return;
}

/**
 * Prints a detailed representation of what a user has done with
 * a given particular instance of this module, for user activity reports.
 * 
 * @param object $course
 *        	the course instance
 * @param object $user
 *        	the concerned user instance
 * @param object $mod
 *        	the current course module instance
 * @param object $scheduler
 *        	the activity module behind the course module instance
 * @param
 *        	boolean true if the user completed activity, false otherwise
 */
function scheduler_user_complete($course, $user, $mod, $scheduler) {
	return true;
}

/**
 * Given a course and a time, this module should find recent activity
 * that has occurred in scheduler activities and print it out.
 * Return true if there was output, or false is there was none.
 * 
 * @param object $course
 *        	the course instance
 * @param boolean $isteacher
 *        	true tells a teacher uses the function
 * @param int $timestart
 *        	a time start timestamp
 * @return boolean true if anything was printed, otherwise false
 */
function scheduler_print_recent_activity($course, $isteacher, $timestart) {
	return false;
}

/**
 * Function to be run periodically according to the moodle
 * This function searches for things that need to be done, such
 * as sending out mail, toggling flags etc .
 * ..
 * 
 * @return boolean always true
 * @uses $CFG
 * @uses $DB
 */
function scheduler_cron() {
	global $CFG, $DB;
	
	$date = make_timestamp ( date ( 'Y' ), date ( 'm' ), date ( 'd' ), date ( 'H' ), date ( 'i' ) );
	
	// for every appointment in all schedulers
	$select = 'emaildate > 0 AND emaildate <= ? AND starttime > ?';
	$slots = $DB->get_records_select ( 'scheduler_slots', $select, array (
			$date,
			$date 
	), 'starttime' );
	
	foreach ( $slots as $slot ) {
		// get teacher
		$teacher = $DB->get_record ( 'user', array (
				'id' => $slot->teacherid 
		) );
		
		// get course
		$scheduler = $DB->get_record ( 'scheduler', array (
				'id' => $slot->schedulerid 
		) );
		$course = $DB->get_record ( 'course', array (
				'id' => $scheduler->course 
		) );
		
		// get appointed student list
		$appointments = $DB->get_records ( 'scheduler_appointment', array (
				'slotid' => $slot->id 
		), '', 'id, studentid' );
		
		// if no email previously sent and one is required
		foreach ( $appointments as $appointment ) {
			$student = $DB->get_record ( 'user', array (
					'id' => $appointment->studentid 
			) );
			cron_setup_user ( $student, $course );
			$vars = scheduler_get_mail_variables ( $scheduler, $slot, $teacher, $student );
			scheduler_send_email_from_template ( $student, $teacher, $course, 'remindtitle', 'reminder', $vars, 'scheduler' );
		}
		// mark as sent
		$slot->emaildate = - 1;
		$DB->update_record ( 'scheduler_slots', $slot );
	}
	
	cron_setup_user ();
	
	return true;
}

/**
 * Returns the users with data in one scheduler
 * (users with records in journal_entries, students and teachers)
 * 
 * @param int $schedulerid
 *        	the id of the activity module
 * @uses $CFG
 * @uses $DB
 */
function scheduler_get_participants($schedulerid) {
	global $CFG, $DB;
	
	// Get students using slots they have
	$sql = '
        SELECT DISTINCT
        u.*
        FROM
        {user} u,
        {scheduler_slots} s,
        {scheduler_appointment} a
        WHERE
        s.schedulerid = ? AND
        s.id = a.slotid AND
        u.id = a.studentid
        ';
	$students = $DB->get_records_sql ( $sql, array (
			$schedulerid 
	) );
	
	// Get teachers using slots they have
	$sql = '
        SELECT DISTINCT
        u.*
        FROM
        {user} u,
        {scheduler_slots} s
        WHERE
        s.schedulerid = ? AND
        u.id = s.teacherid
        ';
	$teachers = $DB->get_records_sql ( $sql, array (
			$schedulerid 
	) );
	
	if ($students and $teachers) {
		$participants = array_merge ( array_values ( $students ), array_values ( $teachers ) );
	} elseif ($students) {
		$participants = array_values ( $students );
	} elseif ($teachers) {
		$participants = array_values ( $teachers );
	} else {
		$participants = array ();
	}
	
	// Return students array (it contains an array of unique users)
	return ($participants);
}

/**
 * This function returns if a scale is being used by one newmodule
 * it it has support for grading and scales.
 * Commented code should be
 * modified if necessary. See forum, glossary or journal modules
 * as reference.
 *
 * @param int $newmoduleid
 *        	ID of an instance of this module
 * @return mixed
 * @uses $DB
 *      
 */
function scheduler_scale_used($cmid, $scaleid) {
	global $DB;
	
	$return = false;
	
	// note : scales are assigned using negative index in the grade field of the appointment (see mod/assignement/lib.php)
	$rec = $DB->get_record ( 'scheduler', array (
			'id' => $cmid,
			'scale' => - $scaleid 
	) );
	
	if (! empty ( $rec ) && ! empty ( $scaleid )) {
		$return = true;
	}
	
	return $return;
}

/**
 * Checks if scale is being used by any instance of scheduler
 *
 * This is used to find out if scale used anywhere
 * 
 * @param $scaleid int        	
 * @return boolean True if the scale is used by any scheduler
 */
function scheduler_scale_used_anywhere($scaleid) {
	global $DB;
	
	if ($scaleid and $DB->record_exists ( 'scheduler', array (
			'scale' => - $scaleid 
	) )) {
		return true;
	} else {
		return false;
	}
}

/*
 * Course resetting API
 *
 */

/**
 * Called by course/reset.php
 * 
 * @param $mform form
 *        	passed by reference
 */
function scheduler_reset_course_form_definition(&$mform) {
	global $COURSE, $DB;
	
	$mform->addElement ( 'header', 'schedulerheader', get_string ( 'modulenameplural', 'scheduler' ) );
	
	if ($DB->record_exists ( 'scheduler', array (
			'course' => $COURSE->id 
	) )) {
		
		$mform->addElement ( 'checkbox', 'reset_scheduler_slots', get_string ( 'resetslots', 'scheduler' ) );
		$mform->addElement ( 'checkbox', 'reset_scheduler_appointments', get_string ( 'resetappointments', 'scheduler' ) );
		$mform->disabledIf ( 'reset_scheduler_appointments', 'reset_scheduler_slots', 'checked' );
	}
}

/**
 * Default values for the reset form
 */
function scheduler_reset_course_form_defaults($course) {
	return array (
			'reset_scheduler_slots' => 1,
			'reset_scheduler_appointments' => 1 
	);
}

/**
 * This function is used by the remove_course_userdata function in moodlelib.
 * If this function exists, remove_course_userdata will execute it.
 * This function will remove all posts from the specified forum.
 * 
 * @param
 *        	data the reset options
 * @return void
 */
function scheduler_reset_userdata($data) {
	global $CFG, $DB;
	
	$status = array ();
	$componentstr = get_string ( 'modulenameplural', 'scheduler' );
	
	$sqlfromslots = 'FROM {scheduler_slots} WHERE schedulerid IN ' . '(SELECT sc.id FROM {scheduler} sc ' . ' WHERE sc.course = :course)';
	
	$params = array (
			'course' => $data->courseid 
	);
	
	$strreset = get_string ( 'reset' );
	
	if (! empty ( $data->reset_scheduler_appointments ) || ! empty ( $data->reset_scheduler_slots )) {
		
		$slots = $DB->get_recordset_sql ( 'SELECT * ' . $sqlfromslots, $params );
		$success = true;
		foreach ( $slots as $slot ) {
			// delete calendar events
			$success = $success && scheduler_delete_calendar_events ( $slot );
			
			// delete appointments
			$success = $success && $DB->delete_records ( 'scheduler_appointment', array (
					'slotid' => $slot->id 
			) );
		}
		$slots->close ();
		
		// reset gradebook
		$schedulers = $DB->get_records ( 'scheduler', $params );
		foreach ( $schedulers as $scheduler ) {
			scheduler_grade_item_update ( $scheduler, 'reset' );
		}
		
		$status [] = array (
				'component' => $componentstr,
				'item' => get_string ( 'resetappointments', 'scheduler' ),
				'error' => ! $success 
		);
	}
	if (! empty ( $data->reset_scheduler_slots )) {
		if ($DB->execute ( 'DELETE ' . $sqlfromslots, $params )) {
			$status [] = array (
					'component' => $componentstr,
					'item' => get_string ( 'resetslots', 'scheduler' ),
					'error' => false 
			);
		}
	}
	return $status;
}

/**
 *
 * @param string $feature
 *        	FEATURE_xx constant for requested feature
 * @return mixed True if module supports feature, null if doesn't know
 */
function scheduler_supports($feature) {
	switch ($feature) {
		case FEATURE_GROUPS :
			return true;
		case FEATURE_GROUPINGS :
			return true;
		case FEATURE_GROUPMEMBERSONLY :
			return true;
		case FEATURE_MOD_INTRO :
			return true;
		case FEATURE_COMPLETION_TRACKS_VIEWS :
			return false;
		case FEATURE_GRADE_HAS_GRADE :
			return true;
		case FEATURE_GRADE_OUTCOMES :
			return false;
		case FEATURE_BACKUP_MOODLE2 :
			return true;
		
		default :
			return null;
	}
}

/* Gradebook API */
/*
 * add xxx_update_grades() function into mod/xxx/lib.php
 * add xxx_grade_item_update() function into mod/xxx/lib.php
 * patch xxx_update_instance(), xxx_add_instance() and xxx_delete_instance() to call xxx_grade_item_update()
 * patch all places of code that change grade values to call xxx_update_grades()
 * patch code that displays grades to students to use final grades from the gradebook
 */

/**
 * Update activity grades
 *
 * @param object $scheduler        	
 * @param int $userid
 *        	specific user only, 0 means all
 */
function scheduler_update_grades($scheduler, $userid = 0, $nullifnone = true) {
	global $CFG, $DB;
	require_once ($CFG->libdir . '/gradelib.php');
	
	if ($scheduler->scale == 0) {
		scheduler_grade_item_update ( $scheduler );
	} else if ($grades = scheduler_get_user_grades ( $scheduler, $userid )) {
		foreach ( $grades as $k => $v ) {
			if ($v->rawgrade == - 1) {
				$grades [$k]->rawgrade = null;
			}
		}
		scheduler_grade_item_update ( $scheduler, $grades );
	} else {
		scheduler_grade_item_update ( $scheduler );
	}
}

/**
 * Create grade item for given scheduler
 *
 * @param object $scheduler
 *        	object
 * @param
 *        	mixed optional array/object of grade(s); 'reset' means reset grades in gradebook
 * @return int 0 if ok, error code otherwise
 */
function scheduler_grade_item_update($scheduler, $grades = NULL) {
	global $CFG, $DB;
	require_once ($CFG->libdir . '/gradelib.php');
	
	if (! isset ( $scheduler->courseid )) {
		$scheduler->courseid = $scheduler->course;
	}
	$moduleid = $DB->get_field ( 'modules', 'id', array (
			'name' => 'scheduler' 
	) );
	$cmid = $DB->get_field ( 'course_modules', 'id', array (
			'module' => $moduleid,
			'instance' => $scheduler->id 
	) );
	
	if ($scheduler->scale == 0) {
		// delete any grade item
		scheduler_grade_item_delete ( $scheduler );
		return 0;
	} else {
		$params = array (
				'itemname' => $scheduler->name,
				'idnumber' => $cmid 
		);
		
		if ($scheduler->scale > 0) {
			$params ['gradetype'] = GRADE_TYPE_VALUE;
			$params ['grademax'] = $scheduler->scale;
			$params ['grademin'] = 0;
		} else if ($scheduler->scale < 0) {
			$params ['gradetype'] = GRADE_TYPE_SCALE;
			$params ['scaleid'] = - $scheduler->scale;
		} else {
			$params ['gradetype'] = GRADE_TYPE_TEXT; // allow text comments only
		}
		
		if ($grades === 'reset') {
			$params ['reset'] = true;
			$grades = NULL;
		}
		
		return grade_update ( 'mod/scheduler', $scheduler->courseid, 'mod', 'scheduler', $scheduler->id, 0, $grades, $params );
	}
}

/**
 * Return grade for given user or all users.
 *
 * @param int $schedulerid
 *        	id of scheduler
 * @param int $userid
 *        	optional user id, 0 means all users
 * @return array array of grades, false if none
 */
function scheduler_get_user_grades($scheduler, $userid = 0) {
	global $CFG, $DB;
	
	if ($scheduler->scale == 0) {
		return false;
	}
	
	$usersql = '';
	$params = array ();
	if ($userid) {
		$usersql = ' AND a.studentid = :userid';
		$params ['userid'] = $userid;
	}
	$params ['sid'] = $scheduler->id;
	
	$sql = 'SELECT a.id, a.studentid, a.grade ' . 'FROM {scheduler_slots} s JOIN {scheduler_appointment} a ON s.id = a.slotid ' . 'WHERE s.schedulerid = :sid AND a.grade IS NOT NULL' . $usersql;
	
	$grades = $DB->get_records_sql ( $sql, $params );
	$finalgrades = array ();
	$gradesums = array ();
	
	foreach ( $grades as $grade ) {
		$gradesums [$grade->studentid] = new stdClass ();
		$finalgrades [$grade->studentid] = new stdClass ();
		$finalgrades [$grade->studentid]->userid = $grade->studentid;
	}
	if ($scheduler->scale > 0) { // Grading numerically
		foreach ( $grades as $aGrade ) {
			$gradesums [$aGrade->studentid]->sum = @$gradesums [$aGrade->studentid]->sum + $aGrade->grade;
			$gradesums [$aGrade->studentid]->count = @$gradesums [$aGrade->studentid]->count + 1;
			$gradesums [$aGrade->studentid]->max = (@$gradesums [$aGrade->studentid]->max < $aGrade->grade) ? $aGrade->grade : @$gradesums [$aGrade->studentid]->max;
		}
		
		// / compute the adequate strategy
		foreach ( $gradesums as $student => $aGradeSet ) {
			switch ($scheduler->gradingstrategy) {
				case MAX_GRADE :
					$finalgrades [$student]->rawgrade = $aGradeSet->max;
					break;
				case MEAN_GRADE :
					$finalgrades [$student]->rawgrade = $aGradeSet->sum / $aGradeSet->count;
					break;
			}
		}
	} else { // Scales
		$scaleid = - ($scheduler->scale);
		$maxgrade = '';
		if ($scale = $DB->get_record ( 'scale', array (
				'id' => $scaleid 
		) )) {
			$scalegrades = make_menu_from_list ( $scale->scale );
			foreach ( $grades as $aGrade ) {
				$gradesums [$aGrade->studentid]->sum = @$gradesums [$aGrade->studentid]->sum + $aGrade->grade;
				$gradesums [$aGrade->studentid]->count = @$gradesums [$aGrade->studentid]->count + 1;
				$gradesums [$aGrade->studentid]->max = (@$gradesums [$aGrade->studentid]->max < $aGrade) ? $aGrade->grade : @$gradesums [$aGrade->studentid]->max;
			}
			$maxgrade = $scale->name;
		}
		
		// / compute the adequate strategy
		foreach ( $gradesums as $student => $aGradeSet ) {
			switch ($scheduler->gradingstrategy) {
				case MAX_GRADE :
					$finalgrades [$student]->rawgrade = $aGradeSet->max;
					break;
				case MEAN_GRADE :
					$finalgrades [$student]->rawgrade = $aGradeSet->sum / $aGradeSet->count;
					break;
			}
		}
	}
	// include any empty grades
	if ($userid > 0) {
		if (! array_key_exists ( $userid, $finalgrades )) {
			$finalgrades [$userid] = new stdClass ();
			$finalgrades [$userid]->userid = $userid;
			$finalgrades [$userid]->rawgrade = null;
		}
	} else {
		$gradeitem = grade_item::fetch ( array (
				'itemtype' => 'mod',
				'itemmodule' => 'scheduler',
				'iteminstance' => $scheduler->id,
				'courseid' => $scheduler->course 
		) );
		$existinggrades = grade_grade::fetch_all ( array (
				'itemid' => $gradeitem->id 
		) );
		if ($existinggrades) {
			foreach ( $existinggrades as $grade ) {
				$u = $grade->userid;
				if (! array_key_exists ( $u, $finalgrades )) {
					$finalgrades [$u] = new stdClass ();
					$finalgrades [$u]->userid = $u;
					$finalgrades [$u]->rawgrade = null;
				}
			}
		}
	}
	return $finalgrades;
}

/**
 * Update all grades in gradebook.
 */
function scheduler_upgrade_grades() {
	global $DB;
	
	$sql = "SELECT COUNT('x')
        FROM {scheduler} s, {course_modules} cm, {modules} m
        WHERE m.name='scheduler' AND m.id=cm.module AND cm.instance=s.id";
	$count = $DB->count_records_sql ( $sql );
	
	$sql = "SELECT s.*, cm.idnumber AS cmidnumber, s.course AS courseid
        FROM {scheduler} s, {course_modules} cm, {modules} m
        WHERE m.name='scheduler' AND m.id=cm.module AND cm.instance=s.id";
	$rs = $DB->get_recordset_sql ( $sql );
	if ($rs->valid ()) {
		$pbar = new progress_bar ( 'schedulerupgradegrades', 500, true );
		$i = 0;
		foreach ( $rs as $scheduler ) {
			$i ++;
			upgrade_set_timeout ( 60 * 5 ); // set up timeout, may also abort execution
			scheduler_update_grades ( $scheduler );
			$pbar->update ( $i, $count, "Updating scheduler grades ($i/$count)." );
		}
		upgrade_set_timeout (); // reset to default timeout
	}
	$rs->close ();
}

/**
 * Delete grade item for given scheduler
 *
 * @param object $scheduler
 *        	object
 * @return object scheduler
 */
function scheduler_grade_item_delete($scheduler) {
	global $CFG;
	require_once ($CFG->libdir . '/gradelib.php');
	
	if (! isset ( $scheduler->courseid )) {
		$scheduler->courseid = $scheduler->course;
	}
	
	return grade_update ( 'mod/scheduler', $scheduler->courseid, 'mod', 'scheduler', $scheduler->id, 0, NULL, array (
			'deleted' => 1 
	) );
}

// function to get class list of classroom
function classroomSchedulerList($courseid) {
	global $DB, $CFG, $USER;
	
	$allTypeOfUsersOfClassroom = getAllTypeOfUsersOfClassroom($courseid, 2);
	$where = "";
	$params = array (
			'course' => $courseid 
	) ;
	 
	if($USER->archetype == $CFG->userTypeManager ){
			    
			if(isset($_REQUEST['classId'])) {
			}
			elseif( isset($allTypeOfUsersOfClassroom['createdby']) && in_array($USER->id, $allTypeOfUsersOfClassroom['createdby'])){

			}else{
			
				if(isset($allTypeOfUsersOfClassroom['primary_instructor']) && in_array($USER->id, $allTypeOfUsersOfClassroom['primary_instructor'])){
				}else{
					   
					   if(isset($allTypeOfUsersOfClassroom['secondary_instructor']) && in_array($USER->id, $allTypeOfUsersOfClassroom['secondary_instructor'])){
					   	$where .= " AND teacher=".$USER->id ;
							$params = array (
							        'course' => $courseid,
									'teacher' => $USER->id 
							) ;
						   
				        }
				}
			}
				
	}elseif($USER->archetype == $CFG->userTypeStudent ){

			if(isset($allTypeOfUsersOfClassroom['primary_instructor']) && in_array($USER->id, $allTypeOfUsersOfClassroom['primary_instructor'])){
			}else{
				   
			   if(isset($allTypeOfUsersOfClassroom['secondary_instructor']) && in_array($USER->id, $allTypeOfUsersOfClassroom['secondary_instructor'])){
			   	$where .= " AND teacher=".$USER->id ;
					$params = array (
							'course' => $courseid,
							'teacher' => $USER->id 
					) ;
				   
				}
			}

	}	
	$sql = "select * from {$CFG->prefix}scheduler where course=".$courseid.$where. " ORDER BY name ASC";
//	$scheduler_list = $DB->get_records ( 'scheduler', $params);
	$scheduler_list = $DB->get_records_sql ( $sql);
	return $scheduler_list;
}
// function to get class list of classroom
function classroomSchedulerListWithPaging($courseid,$page,$perpage,$count=false,$searchText='') {
	global $DB, $CFG, $USER;

	$allTypeOfUsersOfClassroom = getAllTypeOfUsersOfClassroom($courseid, 2);
	$where = "";
	$params = array (
			'course' => $courseid
	) ;
	$page = $page - 1;
	$page = $page*$perpage;
	$limit = '';
	if($perpage != 0 && $count==false){
		$limit = " LIMIT $page,$perpage";
	}

	if($USER->archetype == $CFG->userTypeManager ){
		 
		if( isset($allTypeOfUsersOfClassroom['createdby']) && in_array($USER->id, $allTypeOfUsersOfClassroom['createdby'])){

		}else{
				
			if(isset($allTypeOfUsersOfClassroom['primary_instructor']) && in_array($USER->id, $allTypeOfUsersOfClassroom['primary_instructor'])){
			}else{

				if(isset($allTypeOfUsersOfClassroom['secondary_instructor']) && in_array($USER->id, $allTypeOfUsersOfClassroom['secondary_instructor'])){
					$where .= " AND teacher=".$USER->id ;
					$params = array (
							'course' => $courseid,
							'teacher' => $USER->id
					) ;
						
				}
			}
		}

	}elseif($USER->archetype == $CFG->userTypeStudent ){

		if(isset($allTypeOfUsersOfClassroom['primary_instructor']) && in_array($USER->id, $allTypeOfUsersOfClassroom['primary_instructor'])){
		}else{
				
			if(isset($allTypeOfUsersOfClassroom['secondary_instructor']) && in_array($USER->id, $allTypeOfUsersOfClassroom['secondary_instructor'])){
				$where .= " AND teacher=".$USER->id ;
				$params = array (
						'course' => $courseid,
						'teacher' => $USER->id
				) ;
					
			}
		}

	}
	if($searchText != ''){
		$where .= " AND (LOWER(name) LIKE '%".$searchText."%')";
	}
	$sql = "select * from {$CFG->prefix}scheduler where course=".$courseid.$where. " ORDER BY startdate".$limit;
	//	$scheduler_list = $DB->get_records ( 'scheduler', $params);
	$scheduler_list = $DB->get_records_sql ( $sql);
	return $scheduler_list;
}
// function to get class Session.
function classSessionList($classid,$type='moduleid') {
	global $DB;
	if($type=='moduleid'){
		$class_module = $DB->get_record ( 'course_modules', array (
				'id' => $classid
		) );
		$sch_id =  $class_module->instance;
	}
	else{
		$sch_id = $classid;
	}
	
	$session_list = $DB->get_records ( 'scheduler_slots', array (
			'schedulerid' => $sch_id 
	) );
	
	return $session_list;
}

//Function to get all session of course
function classSessionListOfCourse($courseId) {
	global $DB;
	$sql = "select mss.* from mdl_scheduler_slots  as mss LEFT JOIN mdl_scheduler as ms ON(mss.schedulerid=ms.id) Where ms.course='".$courseId."'";
	$session_list = $DB->get_records_sql($sql);
	$allSessionArr = array();
	$allSessionList = $session_list;
	if(count($allSessionList) > 0 ){
		foreach($allSessionList as $rows){
			$allSessionArr[$rows->schedulerid][] = $rows;
		}
	}
	return $allSessionArr;
}
//function get class name,classroom name
function getClassDetailsFromModuleId($classId){
	global $DB,$CFG,$USER;
	
	$fullnameField = extractMDashInSql('c.fullname');	
	$classNameField = extractMDashInSql('s.name');	
	 $sql = "SELECT s.id,s.startdate,s.enddate,s.teacher,s.no_of_seats, $fullnameField classroomname, $classNameField as classname, s.submitted_by, if(s.submitted_by>0, concat( u.firstname, ' ', u.lastname), '') as submitted_by_name, if(s.submitted_by>0, u.username,'') as submitted_by_username, if(s.submitted_by>0, s.submitted_on, '') as submitted_on
FROM {$CFG->prefix}scheduler AS s 
LEFT JOIN {$CFG->prefix}course AS c ON s.course = c.id
LEFT JOIN {$CFG->prefix}user AS u ON u.id = s.submitted_by
WHERE s.id=".$classId;
     $sql .= " ORDER BY classname ASC";

	$classDetails = $DB->get_record_sql($sql);

return 	$classDetails;
}

// function to get class Session.
function isSessionAvailable($classid) {
	global $DB;

	
	$isexist = $DB->record_exists ( 'scheduler_slots', array (
			'schedulerid' => $classid
	) );
	if($isexist){
		return true;
	}
	return false;
}

function classLearnerList($courseModuleId, $sTypeArr = array(), $is_class_submitted = 0) {
	global $DB;
	$class_module = $DB->get_record ( 'course_modules', array (
			'id' => $courseModuleId
	) );
	
	$learner_list = array();
	if($class_module->instance){
	
     $query = "select se.*,u.firstname,u.lastname from mdl_scheduler_enrollment as se LEFT JOIN mdl_user as u on se.userid=u.id where se.scheduler_id=".$class_module->instance." AND is_approved IN(1) ";
	 
	 $query1 = '';$query2 = '';$query3 = '';
	 /*if(isset($sTypeArr) && count($sTypeArr) > 0 && in_array(5, $sTypeArr) ){
	   // $sType = implode(",", $sTypeArr);
		//$sType = str_replace(array(1,2,3,4,5), array(5,5,5,5,1), $sType);
		//echo "SELECT mse.userid , msa.studentid , msa.attended,  mse.scheduler_id, mss.schedulerid, msa.slotid   from mdl_scheduler_appointment msa left join mdl_scheduler_slots mss ON (mss.id = msa.slotid) LEFT JOIN mdl_scheduler_enrollment mse ON (mse.scheduler_id = mss.schedulerid AND mse.userid = msa.studentid) LEFT JOIN mdl_scheduler ms ON (ms.id = mse.scheduler_id) where mse.scheduler_id=".$class_module->instance." AND mse.is_approved IN(1) AND mse.is_completed = '0' AND ms.isclasscompleted = '1' ";die;
	     $student_ids = $DB->get_field_sql("SELECT group_concat(msa.studentid) as student_id from mdl_scheduler_appointment msa left join mdl_scheduler_slots mss ON (mss.id = msa.slotid) LEFT JOIN mdl_scheduler_enrollment mse ON (mse.scheduler_id = mss.schedulerid AND mse.userid = msa.studentid) LEFT JOIN mdl_scheduler ms ON (ms.id = mse.scheduler_id) where mse.scheduler_id=".$class_module->instance." AND mse.is_approved IN(1) AND mse.is_completed = '0' AND ms.isclasscompleted = '1' ");
		 $student_ids = $student_ids?$student_ids:0;
		 $query1 = " AND (se.userid in ($student_ids) ";
		 
		 if(isset($sTypeArr) && count($sTypeArr) > 0 && (in_array(1, $sTypeArr) || in_array(2, $sTypeArr)) ){
	        $sType = implode(",", $sTypeArr);
			$sType = str_replace(array(1,2,3,4,5), array(0,1,3,4,5), $sType); 
			$query2 = " OR se.is_completed in ($sType) ";
		 }
		 
		 if(isset($sTypeArr) && count($sTypeArr) > 0 && (in_array(3, $sTypeArr) || in_array(4, $sTypeArr)) ){
			$sType = implode(",", $sTypeArr);
			$sType = str_replace(array(1,2,3,4,5), array(5,5,0,1,5), $sType); 
	//echo "SELECT mse.userid , msa.studentid , msa.attended,  mse.scheduler_id, mss.schedulerid, msa.slotid  from mdl_scheduler_appointment msa left join mdl_scheduler_slots mss ON (mss.id = msa.slotid) LEFT JOIN mdl_scheduler_enrollment mse ON (mse.scheduler_id = mss.schedulerid AND mse.userid = msa.studentid) where msa.attended IN (1,5,5,5)  AND mse.scheduler_id=".$class_module->instance." AND mse.is_approved IN(1)";die;
			$student_ids = $DB->get_field_sql("SELECT group_concat(msa.studentid) as student_id from mdl_scheduler_appointment msa left join mdl_scheduler_slots mss ON (mss.id = msa.slotid) LEFT JOIN mdl_scheduler_enrollment mse ON (mse.scheduler_id = mss.schedulerid AND mse.userid = msa.studentid) where msa.attended IN ($sType) AND mse.scheduler_id=".$class_module->instance." AND mse.is_approved IN(1) ");
			$student_ids = $student_ids?$student_ids:0;
			$query3= " OR se.userid in ($student_ids) ";
		 }
		 
		
		 
		 $query .= $query1.$query2.$query3.")" ;
		
	 
	 
	 }else{*/
	 
	 if($is_class_submitted == 1){
	 
		 if(isset($sTypeArr) && count($sTypeArr) > 0 && (in_array(1, $sTypeArr) || in_array(2, $sTypeArr)) ){
			$sType = implode(",", $sTypeArr);
			$sType = str_replace(array(1,2,3,4,5), array(0,1,3,4,5), $sType); 
			$query .= " AND se.is_completed in ($sType) ";
		 }
		 
		 if(isset($sTypeArr) && count($sTypeArr) > 0 && (in_array(3, $sTypeArr) || in_array(4, $sTypeArr)) ){
			$sType = implode(",", $sTypeArr);
			$sType = str_replace(array(1,2,3,4,5), array(5,5,0,1,5), $sType); 
	//echo "SELECT mse.userid , msa.studentid , msa.attended,  mse.scheduler_id, mss.schedulerid, msa.slotid  from mdl_scheduler_appointment msa left join mdl_scheduler_slots mss ON (mss.id = msa.slotid) LEFT JOIN mdl_scheduler_enrollment mse ON (mse.scheduler_id = mss.schedulerid AND mse.userid = msa.studentid) where msa.attended IN (1,5,5,5)  AND mse.scheduler_id=".$class_module->instance." AND mse.is_approved IN(1)";die;
			$student_ids = $DB->get_field_sql("SELECT group_concat(msa.studentid) as student_id from mdl_scheduler_appointment msa left join mdl_scheduler_slots mss ON (mss.id = msa.slotid) LEFT JOIN mdl_scheduler_enrollment mse ON (mse.scheduler_id = mss.schedulerid AND mse.userid = msa.studentid) where msa.attended IN ($sType) AND mse.scheduler_id=".$class_module->instance." AND mse.is_approved IN(1) ");
			$student_ids = $student_ids?$student_ids:0;
			$query .= " AND se.userid in ($student_ids) ";
		 }
    }
	/* }*/
	 
	 $query .= "  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
	 
    $learner_list = $DB->get_records_sql($query);
}
/* 	$learner_list = $DB->get_records('scheduler_enrollment', array (
			'scheduler_id' => $class_module->instance,
			'is_approved'=>1
	) ); */

	return $learner_list;
}


// function to save performance data
function savePerformanceData($performanceData,$noshow=0){
	global $DB,$USER,$CFG;
	
	$grade_rec = new stdClass ();
	foreach ( $performanceData ['userid'] as $key => $userid ) {
	$grade_rec->studentid = trim ( $userid );
	
	$sesisonNotAttended = 0;
	$sesisonAttended = 0;
	// $grade_rec->appointmentnote = trim($_REQUEST['appointmentnote'][$key]);
	
	foreach ( $performanceData ['score'] as $sessionid => $score ) {
		
			$grade_rec->attended = trim ( $performanceData ['attended'] [$sessionid] [$userid] );
			$blank_grade = 1;
			if(!$grade_rec->attended)
			{
				$blank_grade = 0;
			}
			
			if($blank_grade==0 && $sesisonNotAttended==0){
				$sesisonNotAttended = 1;
			}elseif($grade_rec->attended){
				$sesisonAttended = 1;
			}
			
			$grade_rec->score = $performanceData ['score'] [$sessionid] [$userid];
			$grade_rec->grade = $performanceData ['grade'] [$sessionid] [$userid];
			$grade_rec->slotid = trim ( $sessionid );
			$grade_exist = $DB->record_exists ( 'scheduler_appointment', array (
					'slotid' => $sessionid,
					'studentid' => $userid
			) );
			if ($grade_exist) {
				$record = $DB->get_record ( 'scheduler_appointment', array (
						'slotid' => $sessionid,
						'studentid' => $userid
				) );
				$grade_rec->id = $record->id;
				$DB->update_record ( 'scheduler_appointment', $grade_rec );
			} else {
				$DB->insert_record ( 'scheduler_appointment', $grade_rec );
			}
		}
	
	$remarks_update = new stdClass ();
	$remarks_update->id = $key;
	$remarks_update->remarks = $performanceData ['remarks'] [$userid];
	$remarks_update->is_completed = isset($performanceData['is_completed'][$userid])?$performanceData['is_completed'][$userid]:0;
	$remarks_update->timemodified = time();

	$DB->update_record ( 'scheduler_enrollment', $remarks_update );
	//if($noshow==1 && $remarks_update->is_completed!=1 && $sesisonNotAttended==1){	
		// Changed as per discussion with Kamlesh Sir.
	if($noshow==1 && $remarks_update->is_completed!=1 && $sesisonAttended==0){	
						
			classNoShowMail($userid,$performanceData['classid']);			
		
	}
	
	}
	
}

//function get end time of session as per duration

function endTimeByDuration($starttime,$duration)
{
	$end_time =$starttime+ ($duration*60);
	return $end_time;
}

// function to get course module id from course module table
function getSchedulerModuleId($scheduler_id, $course_id) {
	global $DB,$CFG;
	
	$course_module = $DB->get_record ( 'course_modules', array (
			'module' => $CFG->classroomModuleId,
			'course' => $course_id,
			'instance' => $scheduler_id 
	) );
	
	return $course_module;
}

//function to get grade drop down

function getGradeData(){
	global $DB;
	$grade_data = $DB->get_records( 'scheduler_grade',array('isactive'=>1));
	
	return 	$grade_data;
}

// functio to get userdata
function userData($userid){
	global $DB;
	$user_rec = $DB->get_record('user',array('id'=>$userid));
	return $user_rec;
}
function getClassInstructor($scheduler){
	global $DB,$USER;
	$instructor = "";
	if(!$scheduler->teacher){
		$course = $DB->get_record('course',array('id'=>$scheduler->course));
		if($course->primary_instructor){
			$instructor = $course->primary_instructor;
		}
		else{
			$instructor = $course->createdby;
		}
		
	}
	else{
		$instructor = $scheduler->teacher;
	}
	$instructor_record = $DB->get_record('user',array('id'=>$instructor));
	return $instructor_record;
	
}


// save Attendence and report documents
function saveAttendenceDocuments($id,$studentid,$title="",$file_name,$original_file_name=""){
	global $DB;
	$document_rec = new stdClass();
	$document_rec->scheduler_id = $id;
	$document_rec->studentid = $studentid;
	$document_rec->title = $title;
	$document_rec->file_name = $file_name;
	$document_rec->original_file_name = $original_file_name;
	
	$document_rec->createddate = time();
	$DB->insert_record('scheduler_appointment_files',$document_rec);
	return;
}



// delete Attendence and report documents
function deleteAttendenceDocument($id){
	
	global $DB,$CFG;
	$document_rec = $DB->get_record('scheduler_appointment_files', array('id'=>$id));
	$target =$CFG->dirroot."//".$CFG->documentUploadFolderName."/";
	$file = $target.$document_rec->file_name;
	$DB->delete_records('scheduler_appointment_files', array('id'=>$id));
	if (!unlink($file))
	{
	
	
	/* $_SESSION['update_msg'] = get_string('documentdeleted','scheduler');
	$_SESSION['error_class'] = 'success'; */
	
	}
	/* $_SESSION['update_msg'] = get_string('documentdeleted','scheduler');
	$_SESSION['error_class'] = 'success'; */
	return;
}

// function to get all open class users

function getAllOpenClassUsers($classId){
	global $DB,$CFG;
	$query = "SELECT mdm.userid as userid	FROM mdl_scheduler_open_for AS sos
													LEFT JOIN mdl_department_members AS mdm ON sos.enrolid = mdm.departmentid
													WHERE sos.enrolfor = 'department' AND sos.classid=".$classId;

	$department_users = $DB->get_records_sql($query);
	pr($department_users);
	$query = "SELECT gm.userid as userid
													FROM mdl_scheduler_open_for AS sos
													LEFT JOIN mdl_groups_members AS gm ON sos.enrolid = gm.groupid
													WHERE sos.enrolfor = 'team' AND sos.classid=".$classId;
	$team_users = $DB->get_records_sql($query);
	pr($team_users);
	$query = "SELECT sos.enrolid as userid
													FROM mdl_scheduler_open_for AS sos													
													WHERE sos.enrolfor = 'user' AND sos.classid=".$classId;
	$users = $DB->get_records_sql($query);
	pr($users);
	die;
	
	
}
//function to get all uploaded files 
 function getAllFiles($id,$studentid){
 	global $DB;
 	$files = $DB->get_records('scheduler_appointment_files',array('scheduler_id'=>$id,'studentid'=>$studentid));
 	return $files;
 }
/**
 * This function add Class to user
 * 
 * @global object
 * @param int $userid
 *        	user Id
 * @param array $classes
 *        	class Id array
 * @return nothing assign class to user
 *         manage enrolments entries
 */
function assignUserToClass($classId, $enrolUsers,$ismanagerappraoval=0) {
	global $DB, $USER, $CFG;
	
	$all_users = getUserManager();
	if (count($enrolUsers)>0) {
		$time = time ();
		
		foreach ( $enrolUsers as $enrolUser ) {
			
			add_to_log(1, 'scheduler', 'Assign user to class', "Assign user to class", $enrolUser, $classId);
			 $record_exist = $DB->get_record('scheduler_enrollment',array('scheduler_id'=>$classId,'userid'=>$enrolUser));
			
			 $assignClass = new stdClass();
			 $assignClass->scheduler_id = $classId;
		
			if(empty($record_exist)){
			
				
				$assignClass->userid = $enrolUser;
				$assignClass->timecreated = $time;
				$assignClass->timemodified = $time;
				
				if(($all_users[$enrolUser]->usermanagerid==$USER->id && $USER->archetype != $CFG->userTypeAdmin) || $ismanagerappraoval==1){
					$assignClass->is_approved = 1;
				}
				else{
					$assignClass->is_approved = 0;
				}
				$assignClass->request_date = $time;
				$assignClass->response_date = $time;
				$assignClass->action_taken_by = $USER->id;
				$assignClass->assigned_by = $USER->id;
				
				$DB->insert_record ( 'scheduler_enrollment', $assignClass );
				if($assignClass->is_approved==1)
				{
					addEventForClass($classId,$enrolUser);
				}
				
			}
			else{
			
				$assignClass->id = $record_exist->id;
				if(($all_users[$enrolUser]->usermanagerid==$USER->id && $USER->archetype != $CFG->userTypeAdmin) || $ismanagerappraoval==1) {
					$assignClass->is_approved = 1;
				}
				else{
					$assignClass->is_approved = 0;
				}
				$assignClass->action_taken_by = $USER->id;
				$assignClass->request_date = $time;
				$assignClass->response_date = $time;
				$DB->update_record( 'scheduler_enrollment', $assignClass );
				if($assignClass->is_approved==1)
				{
					addEventForClass($classId,$enrolUser);
				}
			}
			
		}
		
	}
}

/**
 * This function remove userFrom class
 *
 * @global object
 * @param int $userid
 *        	user Id
 * @param array $classes
 *        	class Id array
 * @return nothing assign class to user
 *         manage enrolments entries
 */

// function add event in class
function addEventForClass($classId,$userId){
	global $DB;
	$schedulerData = $DB->get_record('scheduler', array('id' => $classId));
	$course = $DB->get_record('course',array('id' => $schedulerData->course));
	$schedulerSlots = $DB->get_records_sql('SELECT ss.id,ss.sessionname,ss.starttime,ss.duration FROM mdl_scheduler_slots as ss WHERE ss.schedulerid = '.$schedulerData->id);
	if(!empty($schedulerSlots)){
		foreach($schedulerSlots as $Slots){
			$title = $course->fullname.':'.$Slots->sessionname;
			$description = $course->summary;
			$eventAdded = addClassEvent($title,$description,$userId,$Slots->id,$Slots->starttime,$Slots->duration);
		}
	}
	
}

function removeUserFromClass($classId, $enrolUsers) {
	
	global $DB, $USER, $CFG;
	$all_users = getUserManager();
	
	if (count($enrolUsers)>0) {
		$time = time ();
		foreach ( $enrolUsers as $enrolUser ) {
			add_to_log(1, 'scheduler', 'Unassign user from class', "Unassign user from class", $enrolUser, $classId);
			$classUser = $DB->get_record('scheduler_enrollment',array('scheduler_id'=>$classId,'userid'=>$enrolUser));
				
					$assignClass = new stdClass();
					$assignClass->id = $classUser->id;
					$assignClass->is_approved = 2;
					$assignClass->timemodified = $time;
					$assignClass->email_status = 0;
					$assignClass->email_sent_date = '';
					$assignClass->action_taken_by = $USER->id;
					sendUnassignInvitation($classId,$enrolUser);
					$DB->update_record ( 'scheduler_enrollment', $assignClass );
					deleteEventForClass($classId,$enrolUser);
					
					
		}
		

	}
	
}


// function to approve a user by maanager
// $param id is scheduler_enrollment id
 
function approveUser($id){
	global $DB,$CFG,$USER;
	$update_rec = new stdClass();
	$update_rec->id = $id;
	$update_rec->is_approved = 1;
	$update_rec->action_taken_by = $USER->id;
	$update_rec->timemodified = time();
	
	$DB->update_record('scheduler_enrollment',$update_rec);
	return;
		
}

//Check user id able to approved or not

function checkApprovedAccess($id){
	global $DB,$CFG,$USER;
	
	$invitedUserList = getInvitedUsers();
	$invitedUserList_keys = (count($invitedUserList)>0)?array_keys($invitedUserList):array();
	$appoint_rec = $DB->get_record('scheduler_enrollment',array('id'=>$id));
	
	if(!in_array($appoint_rec->userid,$invitedUserList_keys)){
		redirect($CFG->wwwroot.'/mod/scheduler/approve_user_list.php');
	}
	
}


//fucntion to get departmentusers
function getDepartmentUsers(){
	global $CFG, $DB, $USER;
	$departmentId = $USER->department;
	$excludedUsers = implode(',',$CFG->excludedUsers);
	if($departmentId != 0){
		//$departmentCheck = ' AND u.department = '.$departmentId;
		$departmentCheck = ' AND mdm.departmentid in ('.$departmentId.')';
	}
	$query = "SELECT u.id,u.firstname,u.lastname
													FROM mdl_user AS u
													LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id
													LEFT JOIN mdl_role AS r ON r.id = ra.roleid
													LEFT JOIN mdl_department_members AS mdm ON u.id = mdm.userid
													WHERE u.confirmed = 1 AND u.deleted = 0 AND u.suspended = 0 AND u.id NOT IN (".$USER->id.",".$excludedUsers.")".$departmentCheck;
		
	
	
	//$query = "SELECT * FROM	mdl_user as u  WHERE u.confirmed = 1 AND  u.deleted = 0 AND  u.suspended = 0 AND u.id NOT IN (".$USER->id.",".$excludedUsers.") $where ORDER BY u.firstname ASC";
	$userList = $DB->get_records_sql($query);

	return $userList;
}

function getInvitedUsers(){	
	global $CFG, $DB, $USER;
	$departmentId = $USER->department;
	$excludedUsers = implode(',',$CFG->excludedUsers);
	if($departmentId != 0){
		//$departmentCheck = ' AND u.department = '.$departmentId;
		$departmentCheck = ' AND mdm.departmentid in ('.$departmentId.')';
	}
	$query = "SELECT u.id,u.firstname,u.lastname,mce.id as appoint_id,mce.is_approved,mce.scheduler_id,msch.name
													FROM mdl_user AS u
													LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id
													LEFT JOIN mdl_role AS r ON r.id = ra.roleid
													LEFT JOIN mdl_department_members AS mdm ON u.id = mdm.userid
													LEFT JOIN mdl_scheduler_enrollment AS mce ON  u.id = mce.userid
													LEFT JOIN mdl_scheduler AS msch ON mce.scheduler_id = msch.id
													WHERE u.confirmed = 1 AND u.deleted = 0 AND u.suspended = 0 AND u.id NOT IN (".$USER->id.",".$excludedUsers.")".$departmentCheck;
		
	
	
	//$query = "SELECT * FROM	mdl_user as u  WHERE u.confirmed = 1 AND  u.deleted = 0 AND  u.suspended = 0 AND u.id NOT IN (".$USER->id.",".$excludedUsers.") $where ORDER BY u.firstname ASC";
	$userList = $DB->get_records_sql($query);
	
	return $userList;
}

//function to send invitation
function sendInvitation($classId, $enrolUsers,$skipmanagerapproval=0){
	global $DB, $USER, $CFG;
	$all_users = getUserManager();
	if (count($enrolUsers)>0) {
		$time = time ();
		$schedule_rec = $DB->get_record('scheduler',array('id'=>$classId));
		foreach ( $enrolUsers as $enrolUser ) {
			
			
			 $record_exist = $DB->get_record('scheduler_enrollment',array('scheduler_id'=>$classId,'userid'=>$enrolUser));
			
			 $assignClass = new stdClass();
			 $assignClass->id = $record_exist->id;
			 $assignClass->scheduler_id = $classId;
			 $assignClass->email_status = 1;
			 $assignClass->email_sent_date = $time;
			 
			$assignClass->action_taken_by = $USER->id;
			$assignClass->assigned_by = $USER->id;
			emailForSendInvitation($enrolUser,$schedule_rec,$all_users,$record_exist->id,$skipmanagerapproval);
			$DB->update_record( 'scheduler_enrollment', $assignClass );
			
			
		}
		
	}
	
	
	
}

//function to get totaluser and approved user

function getCountOfClassUser($classId){
	global $DB,$CFG,$USER;
	$countUser = new stdClass();
	
	 $sql = 'select count(*) as no_of_approved_user from mdl_scheduler_enrollment where is_approved=1 AND scheduler_id='.$classId;
	
	$approvedUser = $DB->get_record_sql($sql);

	$sql = 'select count(*) as total_user from mdl_scheduler_enrollment where is_approved IN(0,1) AND scheduler_id='.$classId;
	$totalAssinedUser = $DB->get_record_sql($sql);
	
	$countUser->no_of_approved_user = $approvedUser->no_of_approved_user;
	$countUser->total_user = $totalAssinedUser->total_user;
	return $countUser;
	
}

// Delete classroom 
function deleteClassroom($classId){
	global $DB,$CFG,$USER;
	$countUser = new stdClass();
	$DB->delete_records('scheduler',array('id'=>$classId));

}

//activate deactivate classroom
function activateDeactivateClassroom($classId,$status){
	global $DB,$CFG,$USER;
	$classroom = new stdClass();
	$classroom->id =$classId; 
	$classroom->isactive =$status;
	$DB->update_record('scheduler',$classroom);

}
//activate deactivate session
function activateDeactivateSession($sessionId,$status){
	global $DB,$CFG,$USER;
	$sessionRec = new stdClass();
	$sessionRec->id =$sessionId;
	$sessionRec->isactive =$status;
	$DB->update_record('scheduler_slots',$sessionRec);

}
function getUserClasses($userid,$paramArray,$page,$perpage,$count=1){
	global $DB,$CFG,$USER;
	$offset = $page - 1;
	$offset = $offset*$perpage;
	$limit = '';
	if($perpage != 0){
		$limit = " LIMIT $offset, $perpage";
	}
	$removeKeyArray = array();
	$pageURL = $_SERVER['PHP_SELF'];
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
	
	 $sql = "SELECT s.id,s.name as classname,se.is_completed,c.fullname as coursename from {$CFG->prefix}course AS c LEFT JOIN {$CFG->prefix}scheduler AS s ON c.id=s.course 
	LEFT JOIN {$CFG->prefix}scheduler_enrollment AS se ON s.id=se.scheduler_id WHERE se.userid=".$userid." AND se.is_approved='1' order by s.id,s.name";
	 
	//$sql = "SELECT s.id,s.name as classname,se.is_completed from {$CFG->prefix}scheduler AS s LEFT JOIN {$CFG->prefix}scheduler_enrollment AS se ON s.id=se.scheduler_id WHERE se.userid=".$USER->id;
	
	$class_data = $DB->get_records_sql($sql);
	return $class_data;
	
}

function assignUsersHtml1($id,$style,$disabled_open_invitation, $display=''){
	
	global $DB,$CFG,$USER;
	if(!$id){
		$id = "''";
	}
	$courses = new class_assignment_selector('', array('courseid' => $id));
	
	
	
	
	$courses->courseDepartmentList();
	$courses->courseNonDepartmentList();
	
	$courses->courseTeamList();
	$courses->courseNonTeamList();
	
	$courses->opencourseUserList();
	$courses->opencourseNonUserList();
	
	$html = '';
	$html .= '<fieldset class="" id="departmet_team_user" style="'.$style.'"><div class="fitem "><div class="msg-tab-html" id="yui_3_13_0_3_1415604087895_328">
		<div class="fitemtitle">';
		//$html .= '<label>'.get_string('availableto','scheduler').'</label>';
		$html .= '</div><div class="felement fstatic" id="yui_3_13_0_3_1415604087895_327">
			
		<div class="tab-box"><a href="javascript:;" class="tabLink activeLink" id="cont-1">Departments</a><a href="javascript:;" class="tabLink" id="cont-2">Teams</a><a href="javascript:;" class="tabLink" id="cont-3">Users</a> 
		</div><div class="tabcontent paddingAll" id="cont-1-1"><div class="departments" id="yui_3_13_0_3_1415604087895_326"><div id="addmembersform">
    <div id="yui_3_13_0_3_1415604087895_325">


    <table class="assignTable">
        <tr>
          <td id="potentialcell" class="potentialcell">
          <p>
            <label for="addselect">Departments</label>
          </p>
		  <div id="department_select_wrapper" class="userselector">
			<select multiple="multiple" '.$disabled_open_invitation.'  name="department_course[]" size="20" id="department_course_dd">';
	 $html .= $courses->course_option_list('non_department_course',true);
			$html .= '</select>';
			$html .= getEDAstric('lower-assign-filter', $display);
			
	 $html .= '<div class="searchBoxDiv">
						<div id="search_non_dept_course"><div class="search-input" ><input class="searchBoxInput" type="text" value="" placeholder="Search" id="department_addselect_searchtext" name="addselect_searchtext" ></div></div>
						<div class="search_clear_button search_non_dept_course"><input type="button" title="Search" id="department_addselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="department_addselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
				  
					</div>';
					
		
	  $html .= '</div>
      </td>
      <td id="buttonscell">
        <div class="arrow_button">
			<input type="hidden" id="invite_to_department" name="invite_to_department" />
			<input class="moveLeftButton" name="remove" id="department_remove" type="button" value="Remove&nbsp;►" title="Remove" disabled="">
             <input class="moveRightButton" name="add" id="department_add" type="button" value="◄&nbsp;Add" title="Add" disabled="">
        </div>
      </td>
	   <td id="existingcell" class="potentialcell">
          <p>
            <label for="removeselect">Invited Departments</label>
          </p>
          <div id="department_remove_wrapper" class="userselector">';
		
			
			$html .='<select multiple="multiple" '.$disabled_open_invitation.' id="add_department_course" name="add_department_course[]" size="20">';
			$html .= $courses->course_option_list('department_course',true);
			$html .= '</select>';
			$html .= getEDAstric('lower-assign-filter', $display);
			$html .= '  <div class="searchBoxDiv">
                <div id="search_dept_program"><div class="search-input" ><input class="searchBoxInput" type="text" value="" placeholder="Search" id="department_removeselect_searchtext" name="removeselect_searchtext" ></div></div>
                
                <div class="search_clear_button search_dept_program"><input type="button" title="Search" id="department_removeselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="department_removeselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
          
            </div>';
		
		  $html .= '</div>
        </td>
    </tr>
    </tbody></table>
    </div>
</div>
</div>																					
  </div> <div class="tabcontent paddingAll hide" id="cont-2-1"><div id="addmembersform">
    <div>
  

    <table class="assignTable">
        <tr>
          <td id="potentialcell" class="potentialcell">
          <p>
            <label for="addselect">Teams (Department)</label>
          </p>
		  <div id="team_addselect_wrapper" class="userselector">
			<select multiple="multiple" name="addteam[]" '.$disabled_open_invitation.' id="team_course_dd" size="20">';
			 $html .= $courses->course_option_list('non_team_course',true);
				$html .= '</select>';
			$html .= '  <div class="searchBoxDiv">
                <div id="search_non_team_program"><div class="search-input" ><input class="searchBoxInput" type="text" value="" placeholder="Search" id="team_addselect_searchtext" name="addselect_searchtext" ></div></div>
                
                <div class="search_clear_button search_non_team_program"><input type="button" title="Search" id="team_addselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="team_addselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
          
            </div>';
		  $html .= '</div>
      </td>
      <td id="buttonscell">
        <div class="arrow_button">
						<input type="hidden" id="invite_to_team" name="invite_to_team" />
			<input class="moveLeftButton" name="remove" id="team_remove" type="button" value="Remove&nbsp;►" title="Remove" disabled="">
            <input class="moveRightButton" name="add" id="team_add" type="button" value="◄&nbsp;Add" title="Add" disabled="">
        </div>
      </td>
	   <td id="existingcell" class="potentialcell">
          <p>
            <label for="removeselect">Invited Teams</label>
          </p>
          <div id="team_removeselect_wrapper" class="userselector">
						';
			
			$html .= '<select multiple="multiple" id="addteamcourses" '.$disabled_open_invitation.' name="addteamcourses[]" size="20">';
			 $html .= $courses->course_option_list('team_course',true);
				$html .= '</select>';

			
			$html .= ' <div class="searchBoxDiv">
							<div id="search_team_program"><div class="search-input" ><input class="searchBoxInput" type="text" value="" placeholder="Search" id="team_removeselect_searchtext" name="removeselect_searchtext" ></div></div>
							
							<div class="search_clear_button search_team_program"><input type="button" title="Search" id="team_removeselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="team_removeselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
				  
					   </div>';
					   
		  $html .= '</div>
        </td>
    </tr>
    </tbody></table>
    </div>
</div>
</div><div class="tabcontent paddingAll hide" id="cont-3-1"><div id="addmembersform">
    <div>
   

    <table class="assignTable">
        <tr>
          <td id="potentialcell" class="potentialcell">
          <p>
            <label for="addselect">Users (Username)</label>
          </p>';
		  $html .= '<div id="user_addselect_wrapper" class="userselector">';
				  $html .= '<select multiple="multiple" id="user_addselect_wrapper2" name="adduser[]" size="20" '.$disabled_open_invitation.'>';
				  $html .= $courses->course_option_list('non_user_course',true); 
				  $html .= '</select> ';

		   $html .= '</div>';
          
           $html .= '<fieldset>
						<legend>'.get_string('filter_legend').'</legend>
						<div class="non-enrolled-class-user-filter-box">';


                     $html .= '<div class="searchBoxDiv">
						<!--label for="addselect_searchtext">'.get_string('search').'</label-->
					
						
						<div id="search-form"><div class="search-input" ><input type="text" value="" placeholder="Search" id="user_addselect_searchtext" name="addselect_searchtext" ></div></div>
						
						<div class="search_clear_button"><input type="button" title="Search" id="user_addselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="user_addselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
						
					</div>';
					
				ob_start();
				$filterBox = "non-enrolled-";
				$filterBoxNonEnrolled = $filterBox;
				$filter = 1;
				require_once($CFG->dirroot . '/local/includes/non_enrolled_class_user_filter.php');
				$SEARCHHTML = ob_get_contents();
				ob_end_clean();
				$html .= $SEARCHHTML;
			
			$html .= '</div> </fieldset>';
		 
		  
      $html .= '</td>
      <td id="buttonscell">
        <div class="arrow_button">
		  		<input type="hidden" id="invite_to_user" name="invite_to_user" />
             <input class="moveLeftButton" name="remove" id="user_remove" type="button" value="Remove&nbsp;►" title="Remove" disabled="">
             <input class="moveRightButton" name="add" id="user_add" type="button" value="◄&nbsp;Add" title="Add" disabled="">
        </div>
      </td>
	   <td id="existingcell" class="potentialcell">
          <p>
            <label for="removeselect">Invited Users</label>
          </p>';
		  
         $html .= '<div id="user_removeselect_wrapper" class="userselector">';
			 $html .= '<select multiple="multiple" id="addusercourse" '.$disabled_open_invitation.' name="addusercourse[]" size="20">';
			 $html .= $courses->course_option_list('user_course',true);
			 $html .= '</select>';
		  $html .= '</div>';
          $html .= '<fieldset>
						<legend>'.get_string('filter_legend').'</legend> <div class="enrolled-class-user-filter-box">';


		       $html .= '<div class="searchBoxDiv">
				<!--label for="removeselect_searchtext">'.get_string('search').'</label-->
				<div id="search-form"><div class="search-input" ><input type="text" value="" placeholder="Search" id="user_removeselect_searchtext" name="removeselect_searchtext" ></div></div>
				
				<div class="search_clear_button"><input type="button" title="Search" id="user_removeselect_searchtext_btn" value="Search" name="search"><a title="Clear" id="user_removeselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
				
			    </div>';
			   
				ob_start();
				$filterBox = "enrolled-";
				$filterBoxEnrolled = $filterBox;
				$filter = 2; 
				require($CFG->dirroot . '/local/includes/enrolled_class_user_filter.php');
				$SEARCHHTML2 = ob_get_contents();
				ob_end_clean();
				$html .= $SEARCHHTML2;
				
			$html .= '</div></fieldset>';
			
        $html .= '</td>
    </tr>
    </tbody></table>
    </div>
    
</div>';

$html .= '<script>
var courseId = '.$id.';
var SiteUrl = "'.$CFG->wwwroot.'";
$("#department_addselect_searchtext_btn").click(function(){
		var searchText = $("#department_addselect_searchtext").val();
		$.ajax({
				url:SiteUrl+"/local/searchpage.php",
				type:"POST",
				data:"action=searchOpenNonDepartment&courseid="+courseId+"&search_text="+searchText,
				success:function(data){
					$("#department_course_dd").html(data);
				}
		});
	});
	$("#department_addselect_clearbutton").click(function(){
		var searchText = "";
		$.ajax({
				url:SiteUrl+"/local/searchpage.php",
				type:"POST",
				data:"action=searchOpenNonDepartment&courseid="+courseId+"&search_text="+searchText,
				success:function(data){
					$("#department_course_dd").html(data);
					$("#department_addselect_searchtext").val("");
				}
		});
	});

	$("#department_removeselect_searchtext_btn").click(function(){
		var searchText = $("#department_removeselect_searchtext").val();
		$.ajax({
				url:SiteUrl+"/local/searchpage.php",
				type:"POST",
				data:"action=searchOpenDepartment&courseid="+courseId+"&search_text="+searchText,
				success:function(data){
					$("#add_department_course").html(data);
				}
		});
	});
	$("#department_removeselect_clearbutton").click(function(){
		var searchText = "";
		$.ajax({
				url:SiteUrl+"/local/searchpage.php",
				type:"POST",
				data:"action=searchOpenDepartment&courseid="+courseId+"&search_text="+searchText,
				success:function(data){
					$("#add_department_course").html(data);
					$("#department_removeselect_searchtext").val("");
				}
		});
	});

	//************** TEAM ***************
	$("#team_addselect_searchtext_btn").click(function(){
		var searchText = $("#team_addselect_searchtext").val();
		$.ajax({
				url:SiteUrl+"/local/searchpage.php",
				type:"POST",
				data:"action=searchOpenNonTeam&courseid="+courseId+"&search_text="+searchText,
				success:function(data){
					$("#team_course_dd").html(data);
				}
		});
	});
	$("#team_addselect_clearbutton").click(function(){
		var searchText = "";
		$.ajax({
				url:SiteUrl+"/local/searchpage.php",
				type:"POST",
				data:"action=searchOpenNonTeam&courseid="+courseId+"&search_text="+searchText,
				success:function(data){
					$("#team_course_dd").html(data);
					$("#team_addselect_searchtext").val("");
				}
		});
	});

	$("#team_removeselect_searchtext_btn").click(function(){
		var searchText = $("#team_removeselect_searchtext").val();
		$.ajax({
				url:SiteUrl+"/local/searchpage.php",
				type:"POST",
				data:"action=searchOpenTeam&courseid="+courseId+"&search_text="+searchText,
				success:function(data){
					$("#addteamcourses").html(data);
				}
		});
	});
	$("#team_removeselect_clearbutton").click(function(){
		var searchText = "";
		$.ajax({
				url:SiteUrl+"/local/searchpage.php",
				type:"POST",
				data:"action=searchOpenTeam&courseid="+courseId+"&search_text="+searchText,
				success:function(data){
					$("#addteamcourses").html(data);
					$("#team_removeselect_searchtext").val("");
				}
		});
	});

  </script>';

 $html .= '</div>';
 $html .= get_string("openclassinstruction","scheduler");
 $html .= '</div></div></fieldset>';
	return $html;
	
}
/*
function assignUsersHtml($id,$style){
	
	global $DB,$CFG,$USER;
	if(!$id){
		$id = "''";
	}
	$courses = new class_assignment_selector('', array('courseid' => $id));
	
	
	
	
	$courses->courseDepartmentList();
	$courses->courseNonDepartmentList();
	
	$courses->courseTeamList();
	$courses->courseNonTeamList();
	
	$courses->opencourseUserList();
	$courses->opencourseNonUserList();
	
	$html = '';
	$html .= '<fieldset class="" id="departmet_team_user" style="'.$style.'"><div class="msg-tab-html" id="yui_3_13_0_3_1415604087895_328">
		
		<div class="msg-tab-block" id="yui_3_13_0_3_1415604087895_327">
			
		<div class="tab-box"><a href="javascript:;" class="tabLink activeLink" id="cont-1">Departments</a><a href="javascript:;" class="tabLink" id="cont-2">Teams</a> 
			<a href="javascript:;" class="tabLink" id="cont-3">Users</a> 
		</div><div class="tabcontent paddingAll" id="cont-1-1"><div class="departments" id="yui_3_13_0_3_1415604087895_326"><div id="addmembersform">
    <div id="yui_3_13_0_3_1415604087895_325">


    <table class="generaltable generalbox groupmanagementtable boxaligncenter" summary="" id="yui_3_13_0_3_1415604087895_324">
    <tbody id="yui_3_13_0_3_1415604087895_323"><tr id="yui_3_13_0_3_1415604087895_322">
	 <td id="potentialcell">
          <p>
            <label for="addselect">Departments</label>
          </p>
		  <div id="department_select_wrapper" class="userselector">
			<select multiple="multiple"  name="department_course[]" size="20" id="yui_3_13_0_3_1415604087895_321">';
	$html .= $courses->course_option_list('non_department_course',true);
			$html .= '</select>
		
		  </div>
      </td>
      <td id="buttonscell">
        <p class="arrow_button">
            <input name="add" id="department_add" type="button" value="◄&nbsp;Add" title="Add" disabled="">
            <input name="remove" id="department_remove" type="button" value="Remove&nbsp;►" title="Remove" disabled="">
        </p>
      </td>
	   <td id="existingcell">
          <p>
            <label for="removeselect">Enrolled Departments</label>
          </p>
          <div id="department_remove_wrapper" class="userselector checkboxwrapper">
			<ul class="open_class_checkboxes">';
			$html .= $courses->departmentCheckboxes();		
			$html .= '</ul>';
			$department_size =  count($courses->courseDepartmentList);
			if($department_size<=20){
				$department_size = 20;
			}
			else{
				$department_size = $department_size+1;
			}
			
			$html .='<select multiple="multiple" id="add_department_course" name="add_department_course[]" size="'.$department_size.'">';
			$html .= $courses->course_option_list('department_course',true);
			$html .= '</select>
		
		  </div>
        </td>
    </tr>
    </tbody></table>
    </div>
</div>
</div>																					
  </div> <div class="tabcontent paddingAll hide" id="cont-2-1"><div id="addmembersform">
    <div>
  

    <table class="generaltable generalbox groupmanagementtable boxaligncenter" summary="">
    <tbody><tr>
	 <td id="potentialcell">
          <p>
            <label for="addselect">Teams</label>
          </p>
		  <div id="team_addselect_wrapper" class="userselector">
			<select multiple="multiple" name="addteam[]" size="20">';
			 $html .= $courses->course_option_list('non_team_course',true);
				$html .= '</select>
			
		  </div>
      </td>
      <td id="buttonscell">
        <p class="arrow_button">
            <input name="add" id="team_add" type="button" value="◄&nbsp;Add" title="Add" disabled="">
            <input name="remove" id="team_remove" type="button" value="Remove&nbsp;►" title="Remove" disabled="">
        </p>
      </td>
	   <td id="existingcell">
          <p>
            <label for="removeselect">Enrolled Teams</label>
          </p>
          <div id="team_removeselect_wrapper" class="userselector checkboxwrapper">
						<ul class="open_class_checkboxes">';
			$html .= $courses->teamCheckboxes();		
			$html .= '</ul>	';
			$team_size =  count($courses->courseTeamList);
			if($team_size<=20){
				$team_size = 20;
			}
			else{
				$team_size = $team_size+1;
			}
			$html .= '<select multiple="multiple" id="addteamcourses" name="addteamcourses[]" size="'.$team_size.'">';
			 $html .= $courses->course_option_list('team_course',true);
				$html .= '</select>
			
		  </div>
        </td>
    </tr>
    </tbody></table>
    </div>
</div>
</div><div class="tabcontent paddingAll hide" id="cont-3-1"><div id="addmembersform">
    <div>
   

    <table class="generaltable generalbox groupmanagementtable boxaligncenter" summary="">
    <tbody><tr>
	 <td id="potentialcell">
          <p>
            <label for="addselect">Users</label>
          </p>
		  <div id="user_addselect_wrapper" class="userselector">
			<select multiple="multiple" name="adduser[]" size="20">';
	$html .= $courses->course_option_list('non_user_course',true); 
			
		  $html .= '</select></div>
      </td>
      <td id="buttonscell">
        <p class="arrow_button">
            <input name="add" id="user_add" type="button" value="◄&nbsp;Add" title="Add" disabled="">
            <input name="remove" id="user_remove" type="button" value="Remove&nbsp;►" title="Remove" disabled="">
        </p>
      </td>
	   <td id="existingcell">
          <p>
            <label for="removeselect">Enrolled Users</label>
          </p>
          <div id="user_removeselect_wrapper" class="userselector checkboxwrapper">
		  			<ul class="open_class_checkboxes">';
			$html .= $courses->userCheckboxes();		
			$html .= '</ul>	';
			$user_size =  count($courses->courseUserList);
			if($user_size<=20){
				$user_size = 20;
			}
			else{
				$user_size = $user_size+1;
			}
			$html .= '<select multiple="multiple" id="addusercourse" name="addusercourse[]" size="'.$user_size.'">';
		  $html .= $courses->course_option_list('user_course',true);
		
		  $html .= '</select></div>
        </td>
    </tr>
    </tbody></table>
    </div>
    
</div>
</div></div></div></fieldset>';
	return $html;
	
	}*/

function getOpenDraftUsers($schedulerId){
	global $DB;
	$sql = "SELECT dm.userid as userid
FROM mdl_department_members AS dm
LEFT JOIN mdl_scheduler_open_for AS sof
ON dm.departmentid = sof.enrolid
WHERE sof.enrolfor = 'department' AND sof.classid = '".$schedulerId."'
 UNION
 SELECT gm.userid as userid
FROM mdl_groups_members AS gm
LEFT JOIN mdl_scheduler_open_for AS sof
ON gm.groupid = sof.enrolid
WHERE sof.enrolfor = 'team' AND sof.classid = '".$schedulerId."'
UNION
SELECT sof.enrolid as userid
FROM  mdl_scheduler_open_for AS sof
WHERE sof.enrolfor = 'user' AND sof.classid = '".$schedulerId."'";
	
	$openTypeAllUsers = $DB->get_records_sql($sql);
	return $openTypeAllUsers;
} 

//function to update class completion
function updateClassCompletion($classId){
	global $DB,$USER;
	$update_sch = new stdClass();
	$update_sch->id = $classId;
	$update_sch->isclasscompleted = 1;
	$update_sch->submitted_by = $USER->id;
	$update_sch->submitted_on = time();	
	$DB->update_record('scheduler',$update_sch);
}
class class_assignment_selector {

	protected $courseId;
	protected $name;
	
	public $selMode;
	public $userGroup;
	public $department;
	public $team;
	public $managers;
	public $roles;
    public $job_title;
	public $company;
        
        //Add code to add country filter, on 16th feb 2016
        public $country;
	
	
	
	public $courseUserList;
	public $courseNonUserList;
	public $coursePreferedUserList;
	protected $courseOthersUserList;
	public function __construct($name, $options) {
		$this->courseId = $options ['courseid'];
		//$this->name = addslashes ( $name );
		
		$this->selMode = isset($options['selMode'])?$options['selMode']:'';
		$this->userGroup = isset($options['userGroup'])?$options['userGroup']:'';
		$this->department = isset($options['department'])?$options['department']:'';
		$this->team = isset($options['team'])?$options['team']:'';
		$this->managers = isset($options['managers'])?$options['managers']:'';
		$this->roles = isset($options['roles'])?$options['roles']:'';
		$this->job_title = isset($options['job_title'])?$options['job_title']:'';
		$this->company = isset($options['company'])?$options['company']:'';
                
                 //Add code to add country filter, on 16th feb 2016
                $this->country = isset($options['country'])?$options['country']:'';
                
		        $this->name = isset($name)?strtolower(addslashes($name)):'';
	}
	
	public function opencourseUserList() {
		global $CFG, $DB, $USER;
		
		$where = '';
		if($this->name != ''){
			$where = " AND (LOWER(u.firstname) LIKE '%".$this->name."%' || LOWER(u.lastname) LIKE '%".$this->name."%' || LOWER(u.username) LIKE '%".$this->name."%' || LOWER(CONCAT(u.firstname,' ',u.lastname)) LIKE '%".$this->name."%')";
		}
                
                
		//Code Added to add country filter on 16th feb 2016
                if($this->country !='' && $this->country !='-1'){
                    $countryExplode = explode('@',$this->country);
                    $countryCSV = '';
                    foreach($countryExplode as $countrycode){
                        $countryCSV .="'".$countrycode."',";
                    }
                    $countryCSV = trim($countryCSV,',');
                    $where .=" AND u.country IN(".$countryCSV.")";
                }
                //end code of country filter
		
		$where .= ($this->selMode!= '')?getSelectFilterCondition($this):'';
		
	
		//	$cm = get_coursemodule_from_id('scheduler', $this->courseId);
		$query = "SELECT u.id,CONCAT(u.firstname, ' ', u.lastname) AS name,sof.email_status AS email_status,u.username as unique_name 
		          FROM mdl_user as u 
		          LEFT JOIN mdl_scheduler_open_for sof  ON u.id = sof.enrolid 
				  LEFT JOIN mdl_department_members as dm ON dm.userid = u.id AND dm.is_active = 1
				  LEFT JOIN mdl_department as d ON dm.departmentid = d.id
				  WHERE sof.classid = ".$this->courseId." AND sof.enrolfor = 'user'  AND d.`status` = 1 AND d.deleted = 0 $where  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
		// $query = "SELECT u.id,CONCAT(u.firstname, ' ', u.lastname) AS name,sc.is_approved FROM {$CFG->prefix}scheduler_enrollment AS sc LEFT JOIN {$CFG->prefix}user AS u ON (u.id = sc.userid) WHERE u.confirmed=1 AND u.deleted=0 AND u.suspended=0 AND sc.scheduler_id = ". $this->courseId." AND sc.isunassigned=0  ORDER BY u.firstname ASC";
		$courseUserList = $DB->get_records_sql ( $query );
	
		$this->courseUserList = $courseUserList;
	}
	
	public function opencourseNonUserList(){
		global $CFG,$DB,$USER;
		$where = "";
		$teacher_rec = $DB->get_record('scheduler',array('id'=>$this->courseId));
		$enrolledUserList = count($this->courseUserList)>0?array_keys($this->courseUserList):array();
		
		if(count($enrolledUserList) > 0 ){
			$enrolledUserListIds = implode(",", $enrolledUserList);
			$where = " AND u.id NOT IN ($enrolledUserListIds)";
		}
		
		if($this->name != ''){
			$where .= " AND (LOWER(u.firstname) LIKE '%".$this->name."%' || LOWER(u.lastname) LIKE '%".$this->name."%' || LOWER(u.username) LIKE '%".$this->name."%' || LOWER(CONCAT(u.firstname,' ',u.lastname)) LIKE '%".$this->name."%')";
		}

		//Code Added to add country filter on 16th feb 2016
                if($this->country !='' && $this->country !='-1'){
                    $countryExplode = explode('@',$this->country);
                    $countryCSV = '';
                    foreach($countryExplode as $countrycode){
                        $countryCSV .="'".$countrycode."',";
                    }
                    $countryCSV = trim($countryCSV,',');
                    $where .=" AND u.country IN(".$countryCSV.")";
                }
                //end code of country filter
                
		$where .= ($this->selMode!= '')?getSelectFilterCondition($this):'';

	
		if($teacher_rec && $teacher_rec->teacher>0){
			$CFG->excludedUsers[] = $teacher_rec->teacher;
		}
		
		$excludedUsers = implode(',',$CFG->excludedUsers);
	
		 $query = "SELECT u.id,CONCAT(u.firstname, ' ', u.lastname) AS name,u.username as unique_name 
		 			FROM mdl_user as u 
					LEFT JOIN mdl_department_members as dm ON dm.userid = u.id AND dm.is_active = 1
					LEFT JOIN mdl_department as d ON dm.departmentid = d.id
					WHERE u.confirmed = 1 AND u.deleted = 0 AND u.suspended = 0  
					AND u.id NOT IN (".$USER->id.",".$excludedUsers.")  
					AND d.`status` = 1 AND d.deleted = 0 $where  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
	
		$courseNonUserList = $DB->get_records_sql($query);
		
		$this->courseNonUserList = $courseNonUserList;
	}
	public function courseUserList() {
		global $CFG, $DB, $USER;
		
	//	$cm = get_coursemodule_from_id('scheduler', $this->courseId);		
		 $query = "SELECT u.id,CONCAT(u.firstname, ' ', u.lastname) AS name,sc.is_approved,u.username as unique_name FROM {$CFG->prefix}scheduler_enrollment AS sc LEFT JOIN {$CFG->prefix}user AS u ON (u.id = sc.userid) WHERE u.confirmed=1 AND u.deleted=0 AND u.suspended=0 AND sc.scheduler_id = ". $this->courseId." AND sc.is_approved IN(0,1)   ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";		
		$courseUserList = $DB->get_records_sql ( $query );	
		
		$this->courseUserList = $courseUserList;
	}
	
	
	public function courseNonUserList($courseid=0,$departmentId =0,$teamId=0, $job_title, $company) {
		global $CFG, $DB, $USER;
		
		$where = '';
		$scheduler = $DB->get_record('scheduler',array('id'=> $this->courseId));
		if($scheduler->enrolmenttype==0){
			if($scheduler->open_for==0){
				$openTypeAllUsers = getOpenDraftUsers($scheduler->id);
				$userList = count($openTypeAllUsers)>0?array_keys($openTypeAllUsers):array();
				if(count($userList) > 0 ){
					$userListIds = implode(",", $userList);
					$where .= " AND u.id IN ($userListIds)";
				
				}
				else{
					$where .= " AND u.id IN (0)";
				}
			}
			
			/* $sql = "select userid from {$CFG->prefix}scheduler_enrollment where scheduler_id=".$scheduler->id." AND is_approved IN(3,2)";
			$sch_enrolment = $DB->get_records_sql($sql);
			//$sch_enrolment = $DB->get_records('scheduler_enrollment',array('scheduler_id'=> $scheduler->id,'is_approved'=>3),null,'userid');
		
			 $no_of_openuser =  count($sch_enrolment);
			$openUserList = $no_of_openuser>0?array_keys($sch_enrolment):array(0);
			
			
			if(count($openUserList) > 0 ){
				$openUserIds = implode(",", $openUserList);
				$where .= " AND u.id IN ($openUserIds)";
			} */
			
		}
		
		$no_of_nonuser =  count($this->courseUserList);
		$enrolledUserList = $no_of_nonuser>0?array_keys($this->courseUserList):array();
		
		$teamCheck = '';
		$excludedUsers = implode(',',$CFG->excludedUsers);
		if(count($enrolledUserList) > 0 ){
			$enrolledUserIds = implode(",", $enrolledUserList);
			$where .= " AND u.id NOT IN ($enrolledUserIds)";
		}
		
		if($departmentId != 0){
			//$departmentCheck = ' AND u.department = '.$departmentId;
			$departmentCheck = ' AND mdm.departmentid in ('.$departmentId.')';
		}
		if($teamId != 0){
			//$departmentCheck = ' AND u.department = '.$departmentId;
			$teamCheck = ' AND gm.groupid in ('.$teamId.') AND gm.is_active = 1 AND mg.is_active = 1 ';
		}
		
		$jobTitleCheck = '';
		if(!in_array($job_title, array(0, '-1'))){
			//$departmentCheck = ' AND u.department = '.$departmentId;
			$jobTitleCheck = ' AND u.job_title in ('.$job_title.')';
		}
		
		$companyCheck = '';
		if(!in_array($company, array(0, '-1'))){
			//$departmentCheck = ' AND u.department = '.$departmentId;
			$companyCheck = ' AND u.company in ('.$company.')';
		}
		
		
		  $query = "SELECT u.id,CONCAT(u.firstname, ' ', u.lastname) AS name,u.username as unique_name
													FROM mdl_user AS u
													LEFT JOIN mdl_program_user_mapping AS pum ON pum.user_id = u.id
													LEFT JOIN mdl_programs AS mp ON mp.id = pum.program_id 
													LEFT JOIN mdl_program_course AS pc ON pc.programid = pum.program_id
													LEFT JOIN mdl_department_members AS mdm ON u.id = mdm.userid 
													LEFT JOIN mdl_department AS md ON md.id = mdm.departmentid 
		 											LEFT JOIN mdl_groups_members AS gm ON u.id = gm.userid 
													LEFT JOIN mdl_groups AS mg ON mg.id = gm.groupid 	  									
													WHERE  mp.status = 1 AND md.status=1 AND pum.status = 1 AND mdm.is_active = 1  AND md.deleted=0 AND mp.deleted = 0 AND mp.publish = 1 AND  u.confirmed = 1 AND u.deleted = 0 AND u.suspended = 0 AND pc.courseid=".$courseid." AND u.id NOT IN (".$scheduler->teacher.",".$excludedUsers.")".$where.$departmentCheck.$teamCheck.$jobTitleCheck.$companyCheck."  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
													
		 
		 $coursePreferedUserList = $DB->get_records_sql($query);
		
		 $this->coursePreferedUserList = $coursePreferedUserList;
		 
		 
		 $preferedUserList = count($this->coursePreferedUserList)>0?array_keys($this->coursePreferedUserList):array();
		 
		 if(count($preferedUserList) > 0 ){
		 	$preferedUserListIds = implode(",", $preferedUserList);
		 	$where .= " AND u.id NOT IN ($preferedUserListIds)";
		 }
		 
		   $query = "SELECT u.id,CONCAT(u.firstname, ' ', u.lastname) AS name,u.username as unique_name
													FROM mdl_user AS u
													LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id
													LEFT JOIN mdl_role AS r ON r.id = ra.roleid
													LEFT JOIN mdl_department_members AS mdm ON u.id = mdm.userid
													LEFT JOIN mdl_department AS md ON md.id = mdm.departmentid
		  											LEFT JOIN mdl_groups_members AS gm ON u.id = gm.userid		  										
													WHERE md.status=1 AND md.deleted=0 AND u.confirmed = 1 AND u.deleted = 0 AND u.suspended = 0 AND u.id NOT IN (".$scheduler->teacher.",".$excludedUsers.")".$where.$departmentCheck.$teamCheck.$jobTitleCheck.$companyCheck."  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
		 
 
		
		//$query = "SELECT * FROM	mdl_user as u  WHERE u.confirmed = 1 AND  u.deleted = 0 AND  u.suspended = 0 AND u.id NOT IN (".$USER->id.",".$excludedUsers.") $where ORDER BY u.firstname ASC";
		$courseNonUserList = $DB->get_records_sql($query);
		//pr($courseNonUserList);
		$this->courseNonUserList = $courseNonUserList;
	}
	
	public function courseDepartmentList(){
		global $CFG,$DB;

         $where = "";
		 if($this->name != ''){
			$where .= " AND (LOWER(d.title) LIKE '%".$this->name."%' )";
		 }
		 
		 $where .= externalCheckForDepartment();
		  $extDeptIdentifier = get_string('external_department_identifier', 'department');
		  $query = "SELECT d.id,(if(d.is_external = 1, concat('".$extDeptIdentifier."', d.title), d.title)) AS name,d.is_external, sof.email_status AS email_status FROM mdl_department as d LEFT JOIN mdl_scheduler_open_for sof  ON d.id = sof.enrolid WHERE sof.classid = ".$this->courseId." AND sof.enrolfor = '".$CFG->departmentModule."' AND d.status = 1  AND d.deleted = '0'  $where  ORDER BY d.title ASC";
		

		$courseDepartmentList = $DB->get_records_sql($query);
		
		$this->courseDepartmentList = $courseDepartmentList;
	}
	public function courseNonDepartmentList(){
		global $CFG,$DB;
		$where = "";
		$enrolledDepartmentList = count($this->courseDepartmentList)>0?array_keys($this->courseDepartmentList):array();
		if(count($enrolledDepartmentList) > 0 ){
			$enrolledDepartmentIds = implode(",", $enrolledDepartmentList);
			$where .= " AND d.id NOT IN ($enrolledDepartmentIds)";
		}
		
	    if($this->name != ''){
			$where .= " AND (LOWER(d.title) LIKE '%".$this->name."%' )";
		}
		
		$where .= externalCheckForDepartment();
		 $extDeptIdentifier = get_string('external_department_identifier', 'department');
		$query = "SELECT d.id,(if(d.is_external = 1, concat('".$extDeptIdentifier."', d.title), d.title)) AS name,d.is_external FROM mdl_department as d WHERE d.status = 1 AND d.deleted = '0' ".$where."  ORDER BY d.title ASC";
		$courseNonDepartmentList = $DB->get_records_sql($query);
		
		$this->courseNonDepartmentList = $courseNonDepartmentList;
	}
	public function courseTeamList(){
		global $CFG,$DB;
		$where = "";
		if($this->name != ''){
			$where .= " AND (LOWER(gc.name) LIKE '%".$this->name."%' )";
		}
	
		$query = "SELECT gc.id,CONCAT(IF((md.title IS NULL),'".get_string("global_team_identifier")."',''),gc.name,IF((md.title IS NULL),'',CONCAT('(',md.title,')'))) as name,sof.email_status AS email_status FROM mdl_groups as gc LEFT JOIN mdl_scheduler_open_for sof  ON gc.id = sof.enrolid LEFT JOIN mdl_group_department AS gd ON gd.team_id = gc.id LEFT JOIN mdl_department AS md ON md.id= gd.department_id WHERE sof.classid = ".$this->courseId." AND sof.enrolfor = '".$CFG->teamModule."'    $where ORDER BY LOWER(CONCAT(IF((md.title IS NULL),'".get_string("global_team_identifier")."',''),gc.name, IF((md.title IS NULL),'', CONCAT('(',md.title,')')))) ASC";
	
		$courseTeamList = $DB->get_records_sql($query);
		
		$this->courseTeamList = $courseTeamList;
	}
	public function courseNonTeamList(){
		global $CFG,$DB;
		$where = "";
		
		if($this->name != ''){
			$where .= " AND (LOWER(gc.name) LIKE '%".$this->name."%' )";
		}
		$enrolledTeamList = count($this->courseTeamList)>0?array_keys($this->courseTeamList):array();
		if(count($enrolledTeamList) > 0 ){
			$enrolledTeamIds = implode(",", $enrolledTeamList);
			$where .= " AND gc.id NOT IN ($enrolledTeamIds)";
		}
		
		$where .= " AND gc.is_active = 1 ";
		
		// $query = "SELECT g.id,CONCAT(IF((d.title IS NULL),'".get_string("global_team_identifier")."',''),g.name,IF((d.title IS NULL),'',CONCAT('(',d.title,')'))) as name FROM mdl_groups AS g  LEFT JOIN mdl_group_department as gd ON gd.team_id = g.id LEFT JOIN mdl_department d ON d.deleted = 0 AND d.id = gd.department_id WHERE 1=1 $where ORDER BY LOWER(CONCAT(IF((d.title IS NULL),'".get_string("global_team_identifier")."',''),g.name, IF((d.title IS NULL),'', CONCAT('(',d.title,')')))) ASC";
		//$query = "SELECT gc.id,gc.name AS name,sof.email_status AS email_status,gd.department_id as unique_id FROM mdl_groups as gc LEFT JOIN mdl_scheduler_open_for sof  ON gc.id = sof.enrolid LEFT JOIN mdl_group_department AS gd ON gd.team_id = gc.id WHERE sof.classid = ".$this->courseId." AND sof.enrolfor = '".$CFG->teamModule."'  ORDER BY gc.name ASC";
		//working
		
		//echo $query = "SELECT gc.id,CONCAT(IF((md.title IS NULL),'".get_string("global_team_identifier")."',''),g.name,IF((md.title IS NULL),'',CONCAT('(',md.title,')'))) as name,md.title as unique_name FROM mdl_groups as gc LEFT JOIN mdl_group_department AS gd ON md.team_id = gc.id LEFT JOIN mdl_department AS md ON md.id= md.department_id  WHERE 1=1 AND md.status = 1 AND md.deleted = '0' ".$where."  ORDER BY LOWER(CONCAT(IF((md.title IS NULL),'".get_string("global_team_identifier")."',''),g.name, IF((md.title IS NULL),'', CONCAT('(',md.title,')')))) ASC";
		//die;
		$query = "SELECT gc.id,CONCAT(IF((md.title IS NULL),'".get_string("global_team_identifier")."',''),gc.name,IF((md.title IS NULL),'',CONCAT('(',md.title,')'))) as name FROM mdl_groups as gc LEFT JOIN mdl_group_department AS gd ON gd.team_id = gc.id LEFT JOIN mdl_department AS md ON md.id= gd.department_id  WHERE 1=1  ".$where."  ORDER BY LOWER(CONCAT(IF((md.title IS NULL),'".get_string("global_team_identifier")."',''),gc.name, IF((md.title IS NULL),'', CONCAT('(',md.title,')')))) ASC";
	//die;
		$courseNonTeamList = $DB->get_records_sql($query);
		
		$this->courseNonTeamList = $courseNonTeamList;
	}
	
	
	public function course_option_list($block = 'user_course', $return = false) {
	global $USER,$CFG,$DB;
		$courseOthers = array();
		if($block == 'team_course'){
			$courses = $this->courseTeamList;
			$courseOthers = $this->courseOthersTeamList;
			$header = get_string('teams');
		}elseif($block == 'non_team_course'){
			$courses = $this->courseNonTeamList;
			$header = get_string('teams');
		}elseif($block == 'department_course'){
			$courses = $this->courseDepartmentList;
			$courseOthers = $courses;
			$header = get_string('departments');
		}elseif($block == 'non_department_course'){
			$courses = $this->courseNonDepartmentList;
			$header = get_string('departments');
		}elseif($block == 'user_course'){
			$courses = $this->courseUserList;
			$courseOthers = $this->courseOthersUserList;
			$header = get_string('users');
		}elseif($block == 'non_user_course'){
			$courses = $this->courseNonUserList;
			$header = get_string('users');
		}
		$option = '';
		if(!empty($courses)){
			$option .= '<optgroup label="'.$header.' ('.count($courses).')">';
			
			foreach($courses as $course){
				$disable = '';
				$approved_class = "";
				$email_status = '';
				if($course->email_status==1){
					$email_status = 'email_sent';
				}
				if($course->is_approved==1){
					$approved_class = 'approved';
				}
				else{
					$approved_class = 'wating_for_approval';
				}
				$unique_name = "";
				if($course->unique_name)
				{
					$unique_name = "(".$course->unique_name.")";
				}
				
				$option .= "<option value = '".$course->id."' class='".$approved_class.' '.$email_status." ' >".$course->name.$unique_name."</option>";
			}
		}else{
			$option .= '<optgroup label="None">';
		}
		
		$option .= '</optgroup>';
		if($return){
		  return $option;
		}else{
		  echo $option;
		}
	}
	
	
	//function to display nonuser select box and prefered user (which user assigned in program) select box
	public function classNonUserList($return = false) {
		global $USER, $CFG, $DB;
		$courseOthers = array ();
		
			$users = $this->courseNonUserList;
			
			$preferedUsers = $this->coursePreferedUserList;;
			$header = get_string ( 'users' );
			$preferedHeader = get_string ( 'preferedusertext','scheduler' );
	
	
		$option = '';
		
		if (! empty ( $preferedUsers )) {
			$option .= '<optgroup id="prefered_user" label="' . $preferedHeader . ' (' . count ( $preferedUsers ) . ')">';
			foreach ( $preferedUsers as $user ) {
				$sch_enrolment_rec = $DB->get_record('scheduler_enrollment',array('userid'=>$user->id,'scheduler_id'=>$this->courseId));
				$class="";
				if($sch_enrolment_rec->is_approved==2){
					$class= "class='user_declined'";
				}
				$option .= "<option value ='".$user->id ."' ".$class.">" . $user->name. "(".$user->unique_name.")</option>";
			}
		} 
		
		
		$option .= '</optgroup>';
		if (! empty ( $users )) {
			$option .= '<optgroup id="other_user" label="' . $header . ' (' . count ( $users ) . ')">';
			foreach ( $users as $user ) {
				$sch_enrolment_rec = $DB->get_record('scheduler_enrollment',array('userid'=>$user->id,'scheduler_id'=>$this->courseId));
				$class="";
				if($sch_enrolment_rec->is_approved==2){
					$class= "class='user_declined'";
				}
				$option .= "<option value ='".$user->id ."' ".$class.">" . $user->name. "(".$user->unique_name.")</option>";
			}
		} else {
			$option .= '<optgroup id="other_user" label="None">';
		}
		
	
		$option .= '</optgroup>';
		
		
		if ($return) {
			return $option;
		} else {
			echo $option;
		}
	}
	
	
	public function departmentCheckboxes() {
	global $USER,$CFG,$DB;
	$departments = $this->courseDepartmentList;	
	$style = "";
	if(count($departments)==0){
		$style = "style='display:none'";
	}
	$option .= "<li ><input type='checkbox' class='select_all' name='' value='' rel='department' ".$style." /></li>";
		foreach($departments as $department){		
			$option .= "<li ><input type='checkbox' class='department_checkbox_id' name='open_for_department[]' value='".$department->id."' /></li>";
		}		
		 return $option;
		
	}
	public function teamCheckboxes() {
	global $USER,$CFG,$DB;
	$departments = $this->courseTeamList;
	$style = "";
	if(count($departments)==0){
		$style = "style='display:none'";
	}

		$option .= "<li ><input type='checkbox' class='select_all' name='' value='' rel='team' ".$style." /></li>";
	
	
		foreach($departments as $department){		
			$option .= "<li ><input type='checkbox' class='team_checkbox_id' name='open_for_team[]' value='".$department->id."' /></li>";
		}		
		 return $option;
	}
	
	public function userCheckboxes() {
		global $USER,$CFG,$DB;
		$departments = $this->courseUserList;
		$style = "";
		if(count($departments)==0){
			$style = "style='display:none'";
		}
	
			$option .= "<li ><input type='checkbox' class='select_all' name='' value='' rel='user' ".$style." /></li>";
	
		foreach($departments as $department){
			$option .= "<li ><input type='checkbox' class='user_checkbox_id' name='open_for_user[]' value='".$department->id."' /></li>";
		}
		return $option;
	}
}


// function to update open_for all status in scheduler
function update_open_for($scheduler_id,$open_for){
	global $DB;
	if(trim($open_for)){
		$open_for = 1;
	}else{
		$open_for = 0;
	}
	$update_open_for = new stdClass();
	$update_open_for->id = $scheduler_id;
	$update_open_for->open_for = $open_for;
	$DB->update_record('scheduler',$update_open_for);
}
?>
