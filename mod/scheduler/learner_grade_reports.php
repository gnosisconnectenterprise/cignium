<?php

$id = $courseModuleId; // Course Module ID, or

$session_list = classSessionList ( $id );
$learner_list = classLearnerList ( $id, $sTypeArr, $is_class_submitted );


$moduleArr->teacher = $classArr['teacher'];
$moduleArr->course = $classArr['course'];
$durationText = getClassDuration($classArr['class_start_date'], $classArr['class_end_date']);
$durationTextCSV = getClassDuration($classArr['class_start_date'], $classArr['class_end_date'], 'csv');
$classInstructorName = fullname(getClassInstructor($moduleArr));

$reportContentPDF = '';
$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" width="100%">';
	$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td><strong>'.get_string("course","classroomreport").':</strong> '.$courseName.'</td>';
		$reportContentPDF .= '<td><strong>'.get_string("classname","classroomreport").':</strong> '.$classArr['classname'].'</td>';
	$reportContentPDF .= '</tr>';

	$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td><strong>'.get_string('class_date_label').':</strong> '.$durationText.'</td>';
		$reportContentPDF .= '<td><strong>'.get_string('instructor','scheduler').':</strong> '.$classInstructorName.'</td>';
	$reportContentPDF .= '</tr>';


	if($classArr['no_of_seats'] && $classArr['class_submittedby_name']){
		$reportContentPDF .= '<tr>';
			$reportContentPDF .= '<td><strong>'.get_string('no_of_seats','scheduler').':</strong> '.$classArr['no_of_seats'].'</td>';
			$reportContentPDF .= '<td><strong>'.get_string('classsubmittedby','scheduler').':</strong> '.$classArr['class_submittedby_name'].'</td>';
		$reportContentPDF .= '</tr>';
	}elseif($classArr['no_of_seats'] && $classArr['class_submittedby_name']==''){
		$reportContentPDF .= '<tr>';
			$reportContentPDF .= '<td colspan="2"><strong>'.get_string('no_of_seats','scheduler').':</strong> '.$classArr['no_of_seats'].'</td>';
		$reportContentPDF .= '</tr>';
	}elseif($classArr['no_of_seats'==''] && $classArr['class_submittedby_name']){
		$reportContentPDF .= '<tr>';
			$reportContentPDF .= '<td  colspan="2" ><strong>'.get_string('classsubmittedby','scheduler').':</strong> '.$classArr['class_submittedby_name'].'</td>';
		$reportContentPDF .= '</tr>';
	}
	
	
	if($is_class_submitted == 1 && $userCount > 0){
	
		$reportContentPDF .= '<tr>';
				$reportContentPDF .= '<td><strong>'.get_string('numberofusers','classroomreport').':</strong> '.$userCount.'</td>';
				$reportContentPDF .= '<td><strong>'.get_string('totalinviteduser','classroomreport').':</strong> '.$totalInviteUser.'</td>';
		$reportContentPDF .= '</tr>';
			
		
		$reportContentPDF .= '<tr>';
				$reportContentPDF .= '<td><strong>'.get_string('managerforapproval','classroomreport').':</strong> '.$waitingUser.'</td>';
				$reportContentPDF .= '<td><strong>'.get_string('approveduser','classroomreport').':</strong> '.$approvedUser.'</td>';
		$reportContentPDF .= '</tr>';
			
		$reportContentPDF .= '<tr>';
				$reportContentPDF .= '<td><strong>'.get_string('declineduser','classroomreport').':</strong> '.$declineUser.'</td>';
				$reportContentPDF .= '<td><strong>'.get_string('inprogress','classroomreport').':</strong> '.$in_completed_count.'</td>';
		$reportContentPDF .= '</tr>';

		$reportContentPDF .= '<tr>';
				$reportContentPDF .= '<td><strong>'.get_string('completed','classroomreport').':</strong> '.$completed_count.'</td>';
				$reportContentPDF .= '<td><strong>'.get_string('status','classroomreport').':</strong> '.$sTypeStr.'</td>';
		$reportContentPDF .= '</tr>';		
		
	}else{
	  	$reportContentPDF .= '<tr>';
				$reportContentPDF .= '<td colspan="2"><strong>'.get_string('status','classroomreport').':</strong> '.$sTypeStr.'</td>';
		$reportContentPDF .= '</tr>';		
	}	
	
	
	  $reportContentPDF .= '<tr><br /><td width="100%"  ><span '.$CFG->pdfSpanAttribute.'><strong>'.get_string('overalluserprogressreport','classroomreport').'</strong></span><br /></td></tr>';
	  
 $reportContentPDF .= '</table>';
 $reportContentPDF .= '<table '.$CFG->pdfTableStyle.'>';


$reportContentCSV = '';
$reportContentCSV .= get_string('course','classroomreport').",".$courseName."\n";
$reportContentCSV .= get_string('classname','classroomreport').",".$classArr['classname']."\n";
$reportContentCSV .= get_string('class_date_label').",".$durationTextCSV."\n";
$reportContentCSV .= get_string('instructor','scheduler').",".$classInstructorName."\n";
if($classArr['no_of_seats']){
	$reportContentCSV .= get_string('no_of_seats','scheduler').",".$classArr['no_of_seats']."\n";
}
if($classArr['class_submittedby_name']){	
	$reportContentCSV .= get_string('classsubmittedby','scheduler').",".$classArr['class_submittedby_name']."\n";
}
if($is_class_submitted == 1 && $userCount > 0){
	
	$reportContentCSV .= get_string('numberofusers','classroomreport').",".$userCount."\n";
	$reportContentCSV .= get_string('totalinviteduser','classroomreport').",".$totalInviteUser."\n";
	$reportContentCSV .= get_string('managerforapproval','classroomreport').",".$waitingUser."\n";
	$reportContentCSV .= get_string('approveduser','classroomreport').",".$approvedUser."\n";
	$reportContentCSV .= get_string('declineduser','classroomreport').",".$declineUser."\n";
	$reportContentCSV .= get_string('inprogress','classroomreport').",".$in_completed_count."\n";
	$reportContentCSV .= get_string('completed','classroomreport').",".$completed_count."\n";
}
$reportContentCSV .= get_string('status','classroomreport').",".$sTypeStr."\n";
$reportContentCSV .= get_string('overalluserprogressreport','classroomreport')."\n\n";


if ($learner_list && $session_list) {
   
    $courseHTML .= '<table class = "table1" id="table1">';
   
    $cntSession = count($session_list);
	$CSVCommas = ' ';
    if($cntSession > 0 ){
	  for($i=0; $i < $cntSession; $i++){
	    $CSVCommas .= ",";
	  }
	}
	
    //$reportContentCSV .= get_string('username','classroomreport').", ,".get_string("sessionstext","scheduler").$CSVCommas.get_string ( 'remarks', 'scheduler' ).",".get_string ( 'classcompleted', 'scheduler' ).",".get_string ( 'document', 'classroomreport' )."\n";
	$reportContentCSV .= get_string('name').", ,".get_string("sessionstext","scheduler").$CSVCommas.get_string ( 'userclassstatus', 'scheduler' )."\n";
			 
			 
	$reportContentPDF .= '<tr '.$CFG->pdfTableHeaderStyle.'>
	                <td rowspan="2" style="vertical-align:middle" ><strong>'.get_string('name').'</strong></td>
					<td rowspan="2"><strong>&nbsp;</strong></td>
					<td colspan="'.count($session_list).'" ><strong>'.get_string('sessionstext','scheduler').'</strong></td>
					<td rowspan="2" style="vertical-align:middle" ><strong>'.get_string('userclassstatus','scheduler').'</strong></td>
					</tr>';
					
	$reportContentPDF .= '<tr '.$CFG->pdfTableHeaderStyle.'>';
					
					$CSVSessionName = '';
					foreach ( $session_list as $session ) {
						$reportContentPDF .= '<td ><strong>' . $session->sessionname . '</strong></td>';
						$CSVSessionName .= ", ".$session->sessionname;
					}
							
	$reportContentPDF .= '</tr>';
	
	$reportContentCSV .= " , ".$CSVSessionName.", , , , \n";
									
	/*$reportContentPDF .= '<td>
	                   <table border="0" cellpadding="0" cellspacing="0" width="100%">';
							if(count($session_list)>=1){
								$reportContentPDF .= '<tr><td style="text-align:center; border-bottom:1px solid #000" colspan="'.count($session_list).'"><strong>'.get_string("sessionstext","scheduler").'</strong></td></tr>';		
							}
							$reportContentPDF .= '<tr>';
							$CSVSessionName = '';
							foreach ( $session_list as $session ) {
								$reportContentPDF .= "<td width='33%'>" . $session->sessionname . "</td>";
								$CSVSessionName .= ", ".$session->sessionname;
							}
							$reportContentCSV .= " , ".$CSVSessionName.", , , , \n";
							$reportContentPDF .= '</tr>';
							$reportContentPDF .= '
					   </table>
					</td>';
	//$reportContentPDF .= "<td width = '10%' align='align_left' ><strong>" . get_string ( 'userclassstatus', 'scheduler' ) . "</strong></td>";
	//$reportContentPDF .= '</tr>';
 */

	$courseHTML .= '<tr class="headingRow">
	                <td width="14%" style="vertical-align:middle" >'.get_string('name').'</td><td width="10%" class="bor_left">&nbsp;</td>';
	$courseHTML .= '<td width="35%" style="padding:0;">
	                   <table border="0" cellpadding="0" cellspacing="0" width="100%" class="margin_none">';
							if(count($session_list)>=1){
								$courseHTML .= '<tr><td colspan="'.count($session_list).'" style="text-align:center; border-bottom:1px solid #b9b9b9" class="bor_left">'.get_string("sessionstext","scheduler").'</td></tr>';		
							}
							$courseHTML .= '<tr>';
							$extraPDFCol = '';
							foreach ( $session_list as $session ) {
								$courseHTML .= "<td width='500' class='bor_left'>" . $session->sessionname . "</td>";
							}
							$courseHTML .= '</tr>';
							$courseHTML .= '
					   </table>
					</td>';
	//$courseHTML .= "<td width = '15%' align='align_left' >" . get_string ( 'remarks', 'scheduler' ) . "</td>";
	$courseHTML .= "<td width = '10%' align='align_left' class='bor_left' style='vertical-align:middle' >" . get_string ( 'userclassstatus', 'scheduler' ) . "</td>";
	//$courseHTML .= "<td width = '15%' align='align_left' >".get_string('document','classroomreport')."</td>";
	$courseHTML .= '</tr>';
    
	if(count($learner_list) > 0 ){
			foreach ( $learner_list as $learner ) {		
					
					$courseHTML .= "<tr class='rep_row'><td class='align_left'  width = '14%'><span class='f-left'><input type='hidden' name='userid[" . $learner->id . "]' value='" . $learner->userid . "' />" . $learner->firstname." ".$learner->lastname . "</span></td>";
				//	$reportContentPDF .= "<tr ".$CFG->pdfTableRowAttribute."><td width = '14%' style='vertical-align:middle'><input type='hidden' name='userid[" . $learner->id . "]' value='" . $learner->userid . "' />" . $learner->firstname." ".$learner->lastname . "</td>";
				
			
					
					if($session_list){
						$courseHTML .= "<td width = '10%' class='gradeCell'>
										  <div class='att'>" . get_string ( 'attended', 'scheduler' ) . "</div>
										  <div class='score'>" . get_string ( 'score', 'scheduler' ) . "</div>
										  <div class='grade'>" . get_string ( 'grade', 'scheduler' ) . "</div>
										</td>";
										
						
					}
					
					   $courseHTML .= '<td class="sessionCell">
											 <table border="0" cellpadding="0" cellspacing="0" width="100%" class="session_table"><tr>';
											  $CSVAttended = '';
											  $CSVScore = '';
											  $CSVGrade = '';
											  foreach ( $session_list as $session ) {
													$session_rec = $DB->get_record ( 'scheduler_appointment', array (
																														'slotid' => $session->id,
													
																														'studentid' => $learner->userid) );
																														
												    if($is_class_submitted == 1){																	
														$courseHTML .= "<td  width='500'>
																		  <div class='att'>".($session_rec->attended == 1?'Yes':'No')."</div>
																		  <div class='score'>".($session_rec->score!=''?$session_rec->score:getMDash())."</div>
																		  <div class='grade'>".($session_rec->grade!=''?$session_rec->grade:getMDash())."</div>
																	   </td>";
																	   
														$CSVAttended .= ", ".($session_rec->attended == 1?'Yes':'No');
														$CSVScore .= ", ".($session_rec->score!=''?$session_rec->score:getMDashForCSV());
														$CSVGrade .= ", ".($session_rec->grade!=''?$session_rec->grade:getMDashForCSV());
														
															
														
													}else{
													
													    $courseHTML .= "<td  width='500'>
																		  <div class='att'>".getMDash()."</div>
																		  <div class='score'>".getMDash()."</div>
																		  <div class='grade'>".getMDash()."</div>
																	   </td>";
																	   
														$CSVAttended .= ", ".(getMDashForCSV());
														$CSVScore .= ", ".(getMDashForCSV());
														$CSVGrade .= ", ".(getMDashForCSV());	
														
													}			   
																
											  }	
											  
											
									  
																					
					   
					   $courseHTML .= '</tr></table></td>';
					  // $courseHTML .= '<td class="align_left">'.nl2br($learner->remarks).'</td>';
					  
					   if($is_class_submitted == 1){
					   
					     $courseHTML .= '<td class="align_left">'.($learner->is_completed == 1?get_string( 'classcompleted', 'scheduler' ):get_string('classincompleted', 'scheduler')).'</td>';
					     
					   }else{
					      $courseHTML .= '<td class="align_left">'.(getMDash()).'</td>';
					   }
					   
					  				
						/*$reportContentPDF .= "<td width = '10%'>
										  <div style='background-color: #d2f3ff;border-bottom: 1px solid #ececee;'><strong>" . get_string ( 'attended', 'scheduler' ) . "</strong></div>
										  <div style='background-color: #e5e5e5;border-bottom: 1px solid #ececee;'><strong>" . get_string ( 'score', 'scheduler' ) . "</strong></div>
										  <div style='background-color: #ffebd0;border-bottom: 1px solid #ececee;' ><strong>" . get_string ( 'grade', 'scheduler' ) . "</strong></div>
										</td>";	*/
					   
					  
						
						
						 $attendedColPDF = '';
						 $scoreColPDF = '';
						 $gradeColPDF = '';
						 foreach ( $session_list as $session ) {
													  
								$session_rec = $DB->get_record ( 'scheduler_appointment', array (
																						'slotid' => $session->id,
																						'studentid' => $learner->userid) );
								if($is_class_submitted == 1){
	
									$attendedColPDF .= '<td style="background-color: #f1faff;border-bottom: 1px solid #000;border-right: 1px solid #000;">'.($session_rec->attended == 1?'Yes':'No').'</td>';
									$scoreColPDF .= '<td style="background-color: #f3f3f3;border-bottom: 1px solid #000;border-right: 1px solid #000;">'.($session_rec->score!=''?$session_rec->score:getMDash()).'</td>';
									$gradeColPDF .= '<td style="background-color: #fff9ef;border-bottom: 1px solid #000;border-right: 1px solid #000;">'.($session_rec->grade!=''?$session_rec->grade:getMDash()).'</td>';

								}else{
								
								    $attendedColPDF .= '<td style="background-color: #f1faff;border-bottom: 1px solid #000;border-right: 1px solid #000;">'.(getMDash()).'</td>';
									$scoreColPDF .= '<td style="background-color: #f3f3f3;border-bottom: 1px solid #000;border-right: 1px solid #000;">'.(getMDash()).'</td>';
									$gradeColPDF .= '<td style="background-color: #fff9ef;border-bottom: 1px solid #000;border-right: 1px solid #000;">'.(getMDash()).'</td>';
													 
								}					 
						}
						
					   $statColPDF = '';	
					   if($is_class_submitted == 1){
					   
					    $statColPDF = '<td >'.($learner->is_completed == 1?get_string( 'classcompleted', 'scheduler' ):get_string ( 'classincompleted', 'scheduler' )).'</td>';
					     
					   }else{
					      $statColPDF = '<td>'.(getMDash()).'</td>';
					   }
					   
						//$reportContentPDF .= '<tr '.$CFG->pdfTableRowAttribute.'><td>' . $learner->firstname.' '.$learner->lastname . '</td></tr>';	
						$reportContentPDF .= '<tr  '.$CFG->pdfTableRowAttribute.'><td style="border-bottom: 1px solid #fff;border-right: 1px solid #000;" >&nbsp;</td><td style="background-color: #d2f3ff;border-bottom: 1px solid #000;border-right: 1px solid #000;">' . get_string ( 'attended', 'scheduler' ) .'</td>'.$attendedColPDF.$statColPDF.'</tr>';
						$reportContentPDF .= '<tr  '.$CFG->pdfTableRowAttribute.' ><td style="border-bottom: 1px solid #fff;border-right: 1px solid #000;">' . $learner->firstname.' '.$learner->lastname . '</td><td style="background-color: #e5e5e5;border-bottom: 1px solid #000;border-right: 1px solid #000;">' . get_string ( 'score', 'scheduler' ) ."</td>".$scoreColPDF.$statColPDF.'</tr>';
						$reportContentPDF .= '<tr  '.$CFG->pdfTableRowAttribute.' ><td>&nbsp;</td><td style="background-color: #ffebd0;border-bottom: 1px solid #000;border-right: 1px solid #000;">' . get_string ( 'grade', 'scheduler' ) .'</td>'.$gradeColPDF.$statColPDF.'</tr>';									
												
												
					    
						
										
					   /*$reportContentPDF .= '<td class="sessionCell">
													 <table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>';
													  foreach ( $session_list as $session ) {
													  
															   $session_rec = $DB->get_record ( 'scheduler_appointment', array (
																														'slotid' => $session->id,
																														'studentid' => $learner->userid) );
															    if($is_class_submitted == 1){
																															
															       	$reportContentPDF .= "<td  width='33%'>
																						  <div style='background-color: #f1faff;border-bottom: 1px solid #ececee;'>".($session_rec->attended == 1?'Yes':'No')."</div>
																						  <div style='background-color: #f3f3f3;border-bottom: 1px solid #ececee;'>".($session_rec->score!=''?$session_rec->score:getMDash())."</div>
																						  <div style='background-color: #fff9ef;border-bottom: 1px solid #fff;'>".($session_rec->grade!=''?$session_rec->grade:getMDash())."</div>
																					 </td>";	
																}else{
																
																  	 $reportContentPDF .= "<td  width='33%'>
																						  <div class='att'>".(getMDash())."</div>
																						  <div class='score'>".(getMDash())."</div>
																						  <div class='grade'>".(getMDash())."</div>
																					 </td>";	
																					 
																}					 
													  }					
												
					
						$reportContentPDF .= '</tr></table></td>';
						
						
					   if($is_class_submitted == 1){
					   
					    $reportContentPDF .= '<td class="align_left">'.($learner->is_completed == 1?get_string( 'classcompleted', 'scheduler' ):get_string ( 'classincompleted', 'scheduler' )).'</td>';
					     
					   }else{
					      $reportContentPDF .= '<td class="align_left">'.(getMDash()).'</td>';
					   }
					   
	                 	*/
						//$allFiles = getAllFiles ( $sClassId, $learner->userid );

						
				           if($is_class_submitted == 1){
						     $reportContentCSV .= $learner->firstname." ".$learner->lastname.",".get_string("attended","scheduler").$CSVAttended.",".($learner->is_completed == 1?get_string( 'classcompleted', 'scheduler' ):get_string ( 'classincompleted', 'scheduler' ))."\n";
						   }else{
						      $reportContentCSV .= $learner->firstname." ".$learner->lastname.",".get_string("attended","scheduler").$CSVAttended.",".(getMDashForCSV())."\n";
						   }
						  $reportContentCSV .= " ,".get_string("score","scheduler").$CSVScore.", , \n"; 
						  $reportContentCSV .= " ,".get_string("grade","scheduler").$CSVGrade.", , \n"; 
													

						$courseHTML .= "</tr>";
					

					//$reportContentPDF .= "</tr>";
					
				}
		}
		
		$courseHTML .= "</table>";
		
}else{
  $courseHTML .= '<table class = "table1" id="table1">';
  $courseHTML .= '<tr class="headingRow">
	                <td width="14%" style="vertical-align:middle" >'.get_string('name').'</td>';
					
	if(count($session_list)>=1){
	$courseHTML .= '<td width="35%" style="padding:0;">
	                   <table border="0" cellpadding="0" cellspacing="0" width="100%" class="margin_none">';
							if(count($session_list)>=1){
								$courseHTML .= '<tr><td colspan="'.count($session_list).'" style="text-align:center; border-bottom:1px solid #b9b9b9" class="bor_left">'.get_string("sessionstext","scheduler").'</td></tr>';		
							}
							$courseHTML .= '<tr>';
							foreach ( $session_list as $session ) {
								$courseHTML .= "<td width='33%' class='bor_left'>" . $session->sessionname . "</td>";
							}
							$courseHTML .= '</tr>';
							$courseHTML .= '
					   </table>
					</td>';
					
	}else{
	   $courseHTML .= '<td width="10%" class="bor_left">'.get_string("sessionstext","scheduler").'</td>';
	}				
	//$courseHTML .= "<td width = '15%' align='align_left' >" . get_string ( 'remarks', 'scheduler' ) . "</td>";
	$courseHTML .= "<td width = '10%' align='align_left' class='bor_left' style='vertical-align:middle' >" . get_string ( 'userclassstatus', 'scheduler' ) . "</td>";
	//$courseHTML .= "<td width = '15%' align='align_left' >".get_string('document','classroomreport')."</td>";
	$courseHTML .= '</tr>';
  $courseHTML .=  '<tr><td colspan="5" align="center" ><div>'.get_string('no_results').'</div></td></tr>';
  $reportContentPDF .=  '<tr><td colspan="5" align="center"  >'.get_string('no_results').'</td></tr>';
  $courseHTML .= "</table>";
}


$courseHTML .= "<style>
			     	#page .course-listing table tr:first-child td div{font-weight: normal !important;} 
				</style>";
$reportContentPDF .= "</table>";
if (! empty ( $courseHTML )) {
	$courseHTML .=  paging_bar ( $programCount, $page, $perpage, $genURL );
}

