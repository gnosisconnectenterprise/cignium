jQuery(document).ready(function(){
	
	if ($("#check_open_for").prop('checked')==true){ 
		enableSaveInvite();
	       $("#departmet_team_user").hide();
	    }
		else{			
			
			$("#departmet_team_user").show();
		}
	if($("#id_enrolmenttype").val() == 1)
		{
		$("#open_for").hide();
		$("#departmet_team_user").hide();
		$("#id_sendinvite").hide();
		}
	
	$("#id_enrolmenttype").change(function(){
		enrolmenttype  = $(this).val();
		if(enrolmenttype==1){
			$("#open_for").hide();
			$("#departmet_team_user").hide();
			$("#id_sendinvite").hide();
		}
		else {
			$("#open_for").show();
			if ($("#check_open_for").prop('checked')==true){ 
			       $("#departmet_team_user").hide();
			    }
				else{
					$("#departmet_team_user").show();
				}
			
			if($("#id_sendinvite").attr("rel")=='disabled'){
			$("#id_sendinvite").hide();
			}
			else{
			$("#id_sendinvite").show();
			}
		}
	})
	
	$("#check_open_for").click(function(){
		if ($("#check_open_for").prop('checked')==true){ 
			enableSaveInvite();
			
		       $("#departmet_team_user").hide();
		    }
			else{
				disableSaveInvite();
				$("#departmet_team_user").show();
			}
	})
	
	
	$(".tabLink").each(function(){
      $(this).click(function(){
        tabeId = $(this).attr("id");
        $(".tabLink").removeClass("activeLink");
        $(this).addClass("activeLink");
        $(".tabcontent").addClass("hide");
        $("#"+tabeId+"-1").removeClass("hide");					
        return false;	  
      });
    }); 
	 

	var fromDepartment = $('#department_select_wrapper select');
	var toDepartment = $('#department_remove_wrapper select');

	var fromTeam = $('#team_addselect_wrapper select');
	var toTeam = $('#team_removeselect_wrapper select');

	var fromUser = $('.tab_availableto_div #user_addselect_wrapper select');
	var toUser = $('.tab_availableto_div #user_removeselect_wrapper select');

	$(document).on("change", "[name = 'department_course[]']",function(){ 
		$("#department_add").removeAttr("disabled");
	});
	$(document).on("change", "[name = 'add_department_course[]']",function(){
		$("#department_remove").removeAttr("disabled");
		enableSaveInvite()
	});

	$(document).on("change", "[name = 'addteam[]']",function(){
		$("#team_add").removeAttr("disabled");
	
	});
	$(document).on("change", "[name = 'addteamcourses[]']",function(){
		
		$("#team_remove").removeAttr("disabled");
		enableSaveInvite()
	});
	$(document).on("change", ".tab_availableto_div [name = 'adduser[]']",function(){
		
		$(".tab_availableto_div #user_add").removeAttr("disabled");
	});
	$(document).on("change", ".tab_availableto_div [name = 'addusercourse[]']",function(){
		if($(this).attr('class') == 'option-disable'){
			$(".tab_availableto_div #user_remove").addAttr("disabled");
		}else{
			$(".tab_availableto_div #user_remove").removeAttr("disabled");
		}
		enableSaveInvite()
	});

	$(document).on("click","#department_add",function(){		
		$("#department_remove").removeAttr("disabled");
		//enableSaveInvite();
		

		fromDepartment.find('option:selected').appendTo(toDepartment.find('optgroup'));
		
		maintainSelectCount(fromDepartment,toDepartment,"Deparments");
		var optionList = '';
		toDepartment.find('option:selected').each(function(){
			optionList += $(this).val()+',';
		});
		
		if(optionList == '' || optionList == ','){
			
			return false;
		}
		
		
		optionList = optionList.substr(0, optionList.length - 1);
		var classid = $("#courseid").val();
		saveOptionData('department',classid,optionList,'add');
		toDepartment.find('option:selected').removeAttr("selected");
	
	});
	
	$(document).on("click","#team_add",function(){
		
		$("#team_remove").removeAttr("disabled");
		//enableSaveInvite();
		fromTeam.find('option:selected').appendTo(toTeam.find('optgroup'));
		maintainSelectCount(fromTeam,toTeam,"Teams");
		var optionList = '';
		toTeam.find('option:selected').each(function(){
			optionList += $(this).val()+',';
		});
		
		if(optionList == '' || optionList == ','){
			
			return false;
		}
				
		optionList = optionList.substr(0, optionList.length - 1);
		var classid = $("#courseid").val();
		saveOptionData('team',classid,optionList,'add');
		toTeam.find('option:selected').removeAttr("selected");
	});
	$(document).on("click",".tab_availableto_div #user_add",function(){
				
		$(".tab_availableto_div  #user_remove").removeAttr("disabled");
		//enableSaveInvite();
		fromUser.find('option:selected').appendTo(toUser.find('optgroup'));
		maintainSelectCount(fromUser,toUser,"Users");
		var optionList = '';
		toUser.find('option:selected').each(function(){
			optionList += $(this).val()+',';
		});
		
		if(optionList == '' || optionList == ','){
			
			return false;
		}
		
		
		optionList = optionList.substr(0, optionList.length - 1);
		var classid = $("#courseid").val();
		saveOptionData('user',classid,optionList,'add');
		toUser.find('option:selected').removeAttr("selected");
			
	});

	$(document).on("click","#department_remove",function(){
		
		var optionList = '';
		toDepartment.find('option:selected').each(function(){
			optionList += $(this).val()+',';
		});
		
		if(optionList == '' || optionList == ','){
			
			return false;
		}		
		
		optionList = optionList.substr(0, optionList.length - 1);		
		var classid = $("#courseid").val();
		saveOptionData('department',classid,optionList,'remove');
		toDepartment.find('option:selected').appendTo(fromDepartment.find('optgroup'));
		fromDepartment.find('option:selected').removeClass();
		maintainSelectCount(fromDepartment,toDepartment,"Deparments");
			
	});
	$(document).on("click","#team_remove",function(){
		var optionList = '';
		toTeam.find('option:selected').each(function(){
			optionList += $(this).val()+',';
		});
		
		if(optionList == '' || optionList == ','){			
			return false;
		}
		
		
		optionList = optionList.substr(0, optionList.length - 1);
		var classid = $("#courseid").val();
		saveOptionData('team',classid,optionList,'remove');
		toTeam.find('option:selected').appendTo(fromTeam.find('optgroup'));
		fromTeam.find('option:selected').removeClass();
		maintainSelectCount(fromTeam,toTeam,"Teams");
		
	});
	$(document).on("click",".tab_availableto_div #user_remove",function(){
		var optionList = '';
		toUser.find('option:selected').each(function(){
			optionList += $(this).val()+',';
		});
		
		if(optionList == '' || optionList == ','){
			
			return false;
		}
		
		
		optionList = optionList.substr(0, optionList.length - 1);
		var classid = $("#courseid").val();
		saveOptionData('user',classid,optionList,'remove');
		
		toUser.find('option:selected').appendTo(fromUser.find('optgroup'));
		fromUser.find('option:selected').removeClass();
		maintainSelectCount(fromUser,toUser,"Users");
		
	});
	$("#id_submitbutton").click(function(){
		var toDepartment = $('#department_remove_wrapper select');
		var fromTeam = $('#team_addselect_wrapper select');
		var toTeam = $('#team_removeselect_wrapper select');

		toDepartment.find('option').prop('selected', true);
		toTeam.find('option').prop('selected', true);
		toUser.find('option').prop('selected', true);
		
	});

	$("#id_sendinvite").click(function(){		
		
		var toDepartment = $('#department_remove_wrapper select');
		var fromTeam = $('#team_addselect_wrapper select');
		var toTeam = $('#team_removeselect_wrapper select');
		var selected_val=0;
		var val = [];
	    $("#department_remove_wrapper select option:selected").each(function () {
	    	  selected_val=1;
	          val.push(this.value);
	    });
	     var invite_to_department = val.join(',');
	     $("#invite_to_department").val(invite_to_department);
	     
	     var val = [];
	     
	     $("#team_removeselect_wrapper select option:selected").each(function () {
	    	   selected_val=1;
	           val.push(this.value);
	     });
	      var invite_to_team = val.join(',');      
	      $("#invite_to_team").val(invite_to_team);

	      
	      var val = [];
	      $("#user_removeselect_wrapper select option:selected").each(function () {
	    	  selected_val=1;
	            val.push(this.value);
	      });
	       var invite_to_user = val.join(',');
	       $("#invite_to_user").val(invite_to_user);
	    if($("#check_open_for").prop('checked')==false && selected_val==0){
	    	alert('Please select atleast one Department/Team/User.');
	    	return false;
	    }
		toDepartment.find('option').prop('selected', true);
		toTeam.find('option').prop('selected', true);
		toUser.find('option').prop('selected', true);
		
	});

	if($("#check_open_for").prop('checked')==false){
		disableSaveInvite();
	}
	
});

/* Save open class enrolment data using ajax
 * type => user/team/department
 * classId=>classid
 * list=>selected List itmes
 * actionType=>It can be add or remove 
 */
function saveOptionData(type,classId,list,actionType){
	if(type != '' && classId != '' && list != '' ){
		$.ajax({
				url:'ajax.php',
				type:'POST',
				data:'action=saveClassData&type='+type+'&classId='+classId+'&list='+list+'&actionType='+actionType,
				success:function(data){
				}
		});
	}
}

function showDepartment(value,type){
	
	if(type=='enroltype'){
		if(value==1){
			$("#department_list").hide();
			$("#open_for").hide();
		}
		else{
			$("#open_for").show();
			if ($("#check_open_for").prop('checked')==true){ 
			       $("#department_list").hide();
			    }
				else{
					$("#department_list").show();
				}
		}
	}
	else{
		if ($("#check_open_for").prop('checked')==true){ 
	       $("#department_list").hide();
	    }
		else{
			$("#department_list").show();
		}
	}
}

//Enable Invite Button when class is open.
function enableSaveInvite(){
	if (!$("#id_sendinvite").hasClass("selected")) {
	    //do stuff
	}
	var isDisabled = $("#id_sendinvite").attr("rel");	
	if(isDisabled!='disabled'){
	$("#id_sendinvite").removeAttr("disabled");
	}
}

//Disable Invite Button when class is open.
function disableSaveInvite(){
	
	var isDisabled = $("#id_sendinvite").attr("rel");
	
	if(isDisabled!='disabled'){
		$('#id_sendinvite').prop('disabled', true);

	}
}
