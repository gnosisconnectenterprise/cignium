<?php
	require_once('../../config.php');
	
	require_once($CFG->dirroot.'/mod/scheduler/lib.php');
	
	global $USER,$CFG;

	require_login();



	
	
	$header = "$SITE->shortname";	
	
	//Start Editor options
	$editorOptions = array('maxfiles' => EDITOR_UNLIMITED_FILES, 'maxbytes'=>$CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true);	
	//End Editor options

	$id = required_param('id', PARAM_INT);

	
	
	// Start setting up the page
	$params = array();
	$PAGE->set_context($context);
	$PAGE->set_url('/mod/scheduler/list.php?id='+$id, $params);
	$PAGE->set_pagelayout('general');
	if ($id) {
		if (! $cm = get_coursemodule_from_id('scheduler', $id)) {
			print_error('invalidcoursemodule');
		}
	
		if (! $course = $DB->get_record('course', array('id' => $cm->course))) {
			print_error('coursemisconf');
		}
	
		if (! $scheduler = $DB->get_record('scheduler', array('id' => $cm->instance))) {
			print_error('invalidcoursemodule');
		}
	
	}
	if(isset($_REQUEST['cancel'])){
		redirect($CFG->wwwroot."/mod/scheduler/list.php?id=".$cm->course);
	}
	if(isset($_REQUEST['submit'])){
	
		$scheduler_id = trim($_REQUEST['scheduler_id']);
	
		$sech_rec = new stdClass();
		$sech_rec->id = $scheduler_id;
		$sech_rec->enrolmenttype = trim($_REQUEST['enrolmenttype']);
		$sech_rec->no_of_seats = trim($_REQUEST['no_of_seats']);
		$sech_rec->location = trim($_REQUEST['location']);
		$sech_rec->web_access_link = trim($_REQUEST['web_access_link']);
		$sech_rec->pre_requisite = trim($_REQUEST['pre_requisite']);
		$DB->delete_records('scheduler_open_for',array('classid'=>$sech_rec->id));
		
		if(trim($_REQUEST['open_for']) || $sech_rec->enrolmenttype==1){
			$open_for = 1;
		}
		else{
			$open_for = 0;
		}
		$sech_rec->open_for = $open_for;
		if($sech_rec->enrolmenttype==0 && $open_for==0){
			
			$open_rec =  new stdClass();
			$open_rec->classid = $scheduler_id;
			foreach($_REQUEST['departments'] as $department)
			{
				$open_rec->departmentid =$department;
			
				$DB->insert_record('scheduler_open_for',$open_rec);
			}
			
		}
		
			
		
		$DB->update_record('scheduler',$sech_rec);
		$_SESSION['update_msg'] = get_string('settingssuccess','scheduler');
		$_SESSION['error_class'] = 'success';
			
	redirect($CFG->wwwroot."/mod/scheduler/list.php?id=".$cm->course);
	
	}

	$PAGE->navbar->add(get_string('managecourses'), new moodle_url($CFG->wwwroot.'/course/index.php'));
	$PAGE->navbar->add(get_string('editcourse','course'), new moodle_url($CFG->wwwroot.'/course/edit.php',array('id'=>$cm->course)));
	//$PAGE->navbar->add(get_string('manage_assets'));
	$PAGE->navbar->add(get_string('schedulingandtracking'),new moodle_url($CFG->wwwroot.'/mod/scheduler/list.php',array('id'=>$cm->course)));
	
	
	//$PAGE->navbar->add(get_string('course_allocation','course'));
	$PAGE->navbar->add(get_string('setting','scheduler'));
	

	// End setting up the page

	//Print header
	echo $OUTPUT->header();

	//$addDepartmentForm = new adddepartment_form($CFG->pageAddDepartment,$adddepartmentdata);

	$returnUrl = $CFG->wwwroot."/mod/scheduler/list.php?id="+$id;
	$departments = $DB->get_records_sql("SELECT id,title FROM {$CFG->prefix}department WHERE deleted = 0 AND status = 1 order by display_order ASC");

	// Start Add department form

	
	
	//Start Print form
	echo '<div class="left_content">
			<div class="category_section">';
	$courseHTML .=   "</div>";
	$courseHTML .=   "<div class='tabLinks'>";
	include_once($CFG->dirroot.'/course/course_tabs.php');
	
	$courseHTML .=   "</div>";
	//print_object($scheduler);
$courseHTML .= '<div class="clear"></div><div class="userprofile view_assests">';

/// route to screen
 $courseHTML .=  '<form action="" method="post" onsubmit="return checkValidation()"><input type="hidden" name="scheduler_id" value="'.$scheduler->id.'" /><table class = "table1">';
 
$courseHTML .=  "<tr><td width = '23%' align='align_left' >".get_string('enroloptions','scheduler')."</td>";
//$courseHTML .=  "<td width = '22%' align='align_left' >Description</td>";
$courseHTML .=  "<td width = '10%' align='align_left' ><select name='enrolmenttype' id='id_enrolmenttype' onchange='showDepartment(this.value,\"enroltype\")'>
	<option value='0' ".is_checked($scheduler->enrolmenttype,0,'select').">Open</option>
	<option value='1' ".is_checked($scheduler->enrolmenttype,1,'select').">Invite</option>
</select>";
$style = "";
$departStyle = "";
if($scheduler->enrolmenttype==1)
{
	$style = "display:none";
}
else{
	if($scheduler->open_for==1){
		$departStyle = "display:none"; 
	}
}
$courseHTML .=  "<div id='open_for' style='".$style."'><input type='checkbox' id='check_open_for' name='open_for'  ".is_checked($scheduler->open_for,1,'checkbox')." onclick='showDepartment(this.value,\"open_for\")' /> All</div><div id='department_list' style='".$style.$departStyle."'>";
$sql = "select departmentid from mdl_scheduler_open_for where classid=".$scheduler->id;
//$selected_departments = $DB->get_records('scheduler_open_for',array('classid'=>$scheduler->id));
$selected_departments = $DB->get_records_sql($sql);

$selected_departments_arr = (count($selected_departments)>0)?array_keys($selected_departments):array();

foreach($departments as $department){
	if(in_array($department->id,$selected_departments_arr)){
		
		$checked = 'checked=checked';
	}
	else{
		$checked = '';
	}
	$courseHTML .= " <input type='checkbox' name='departments[]' class='department_checkbox' value='".$department->id."' $checked>".$department->title;

}
$courseHTML .= "</div>
			</td></tr>";


$courseHTML .=  "<tr><td width = '23%' align='align_left' >".get_string('no_of_seats','scheduler')."</td>";
//$courseHTML .=  "<td width = '22%' align='align_left' >Description</td>";
$courseHTML .=  "<td width = '10%' align='align_left' ><input size='2' name='no_of_seats' type='text' value='".$scheduler->no_of_seats."'  id='id_no_of_seats'></td></tr>";

$courseHTML .=  "<tr><td width = '23%' align='align_left' >".get_string('location','scheduler')."</td>";
//$courseHTML .=  "<td width = '22%' align='align_left' >Description</td>";
$courseHTML .=  "<td width = '10%' align='align_left' ><input size='2' name='location' type='text' value='".$scheduler->location."' id='id_location'></td></tr>";

$courseHTML .=  "<tr><td width = '23%' align='align_left' >".get_string('webaccesslink','scheduler')."</td>";
//$courseHTML .=  "<td width = '22%' align='align_left' >Description</td>";
$courseHTML .=  "<td width = '10%' align='align_left' ><textarea wrap='virtual' rows='10' cols='50' name='web_access_link' id='id_pre_requisite'>".$scheduler->web_access_link."</textarea></td></tr>";
$courseHTML .=  "<tr><td width = '23%' align='align_left' >".get_string('pre_requisite','scheduler')."</td>";
//$courseHTML .=  "<td width = '22%' align='align_left' >Description</td>";
$courseHTML .=  "<td width = '10%' align='align_left' ><textarea wrap='virtual' rows='10' cols='50' name='pre_requisite' id='id_pre_requisite'>".$scheduler->pre_requisite."</textarea></td></tr>";
//$tableHtml .= "<td width = '15%' align='align_left' >$createdby</td>";

$courseHTML .=  "<tr><td colspan='4'><input type='submit' name='cancel' value='Cancel' /><input type='submit' name='submit' value='Save & Continue' /></td></tr>";
	
	
	$courseHTML .=  "</table></form></div>";
	if (!empty($courseHTML)) {
		echo '<div class = "course-listing">';
	
		echo html_writer::start_tag('div', array('class'=>'no-overflow'));
		echo $courseHTML;
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
	
		//Pring paging bar
		echo paging_bar($programCount, $page, $perpage, $genURL);
	}
	echo '</div>';
	//End print form

	// End Add department form
	/*Start Javascript to delete department image*/
?>
<script>
	$(document).ready(function(){
		$(".removeimage").click(function(){
			message = "<?php echo get_string('delete_image_alert_message')?>";
			var r = confirm(message);
			if (r == true) {
				var departmentId = $(this).attr('rel');
				$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
					type:'POST',
					data:'action=deleteDepartmentImage&departmentId='+departmentId,
					success:function(data){
						alert("<?php echo get_string('image_deleted_successfully');?>");
						window.location.reload(true);
					}
				});
			}else{
				return false;
			}
		});
		
	});

	function checkValidation(){
		var enroltype = $("#id_enrolmenttype").val();
		
		if ($("#check_open_for").prop('checked')==false && enroltype==0){
			var checked = $('input[name="departments[]"]:checked');
			
			if(checked.length==0){
				alert('Please select atleast one department');
				return false;
			}
			
		}
		return true;
		
	}
	function showDepartment(value,type){
		
		if(type=='enroltype'){
			if(value==1){
				$("#department_list").hide();
				$("#open_for").hide();
			}
			else{
				$("#open_for").show();
				if ($("#check_open_for").prop('checked')==true){ 
				       $("#department_list").hide();
				    }
					else{
						$("#department_list").show();
					}
			}
		}
		else{
			if ($("#check_open_for").prop('checked')==true){ 
		       $("#department_list").hide();
		    }
			else{
				$("#department_list").show();
			}
		}
		//alert($("#check_open_for").val());
	}
</script>
<?php
/*End Javascript to delete department image*/

//Print footer
	echo $OUTPUT->footer();
?>