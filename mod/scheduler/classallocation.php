<?php
require_once (dirname ( __FILE__ ) . '/../../config.php');
require_once (dirname ( __FILE__ ) . '/lib.php');
require_once ($CFG->dirroot . '/local/user/selector/lib.php');
require_once ($CFG->dirroot . '/course/lib.php');
require_once ($CFG->libdir . '/filelib.php');
require_once ($CFG->dirroot . '/mod/scheduler/lib.php');
require_once ($CFG->dirroot . '/mod/scheduler/locallib.php');

require_login ();
$site = get_site ();
$id = required_param ( 'id', PARAM_INT );
if ($id) {
	if (! $cm = get_coursemodule_from_id ( 'scheduler', $id )) {
		print_error ( 'invalidcoursemodule' );
	}

	if (! $course = $DB->get_record ( 'course', array ('id' => $cm->course))) {
		print_error ( 'coursemisconf' );
	}

	if (! $scheduler = $DB->get_record ( 'scheduler', array ('id' => $cm->instance))) {
		print_error ( 'invalidcoursemodule' );
	}
}

// pr($scheduler);
$nonUserSelectDisable = "";
$userSelectDisable = "";
$courses = new class_assignment_selector ( '', array (
		'courseid' => $scheduler->id
) );

$PAGE->set_url ( '/user/assignusers.php', array ('id' => $courseid));
$PAGE->set_pagelayout ( 'classroompopup' );


if (isset ( $_REQUEST ['sendinvite'] ) && $_REQUEST ['formsubmit'] == 1) {
	$enrol_users = $_REQUEST ['addusercourse'];

	if (count ( $enrol_users ) > 0) {
		sendInvitation ( $scheduler->id, $enrol_users );
		$_SESSION ['update_msg'] = get_string ( 'sendinvitesuccess', 'scheduler' );
		$_SESSION ['error_class'] = 'success';
	} else {
		$_SESSION ['update_msg'] = get_string ( 'sendinviteerror', 'scheduler' );
		$_SESSION ['error_class'] = 'error_bg';
	}
	
	redirect($CFG->wwwroot.'/mod/scheduler/classallocation.php?id='.$_REQUEST["id"]);
}

if (isset ( $_REQUEST ['saveopeninvite'] ) && $_REQUEST ['formsubmit'] == 1) {
	update_open_for($scheduler->id,$_REQUEST['open_for']);
	enrolIntoOpenCourse($scheduler->id,$_REQUEST['addteamcourses'],'team');
	enrolIntoOpenCourse($scheduler->id,$_REQUEST['add_department_course'],'department');
	enrolIntoOpenCourse($scheduler->id,$_REQUEST['addusercourse'],'user');
	$_SESSION ['update_msg'] = get_string ( 'sendinvitesuccess', 'scheduler' );
	$_SESSION ['error_class'] = 'success';
	redirect($CFG->wwwroot.'/mod/scheduler/classallocation.php?id='.$_REQUEST["id"]);
}

if (isset ( $_REQUEST ['sendopeninvite'] ) && $_REQUEST ['formsubmit'] == 1) {
	update_open_for($scheduler->id,$_REQUEST['open_for']);
	//enrolIntoOpenCourse($scheduler->id,$_REQUEST['addteamcourses'],'team');
	//enrolIntoOpenCourse($scheduler->id,$_REQUEST['add_department_course'],'department');
	//enrolIntoOpenCourse($scheduler->id,$_REQUEST['addusercourse'],'user');
	$open_for_department = explode(',',$_REQUEST['invite_to_department']) ;	
	$open_for_team = explode(',',$_REQUEST['invite_to_team']) ;
	$open_for_user=  explode(',',$_REQUEST['invite_to_user']) ;
	sendOpenInvitetoUsers($scheduler->id,$open_for_department,$open_for_team,$open_for_user,$_REQUEST['open_for']);
	$_SESSION ['update_msg'] = get_string ( 'sendinvitesuccess', 'scheduler' );
	$_SESSION ['error_class'] = 'success';
	redirect($CFG->wwwroot.'/mod/scheduler/classallocation.php?id='.$_REQUEST["id"]);
}



$context = context_system::instance ();
$returnurl = $CFG->wwwroot . '/course/index.php';

if ($cancel) {
	$urlCancel = new moodle_url ( $CFG->wwwroot . '/course/courseview.php', array ('id' => $course->id));
	redirect ( $urlCancel );
}

$PAGE->navbar->add ( get_string ( 'managecourses' ), new moodle_url ( $CFG->wwwroot . '/course/index.php' ) );
$PAGE->navbar->add ( get_string ( 'editcourse', 'course' ), new moodle_url ( $CFG->wwwroot . '/course/edit.php', array (
		'id' => $cm->course
) ) );

$PAGE->navbar->add ( get_string ( 'schedulingandtracking' ), new moodle_url ( $CFG->wwwroot . '/mod/scheduler/list.php', array (
		'id' => $cm->course
) ) );

$PAGE->navbar->add ( get_string ( 'allocation' ) );

$courses->courseUserList ();
$courses->courseNonUserList ( $cm->course );
$where = '';
$departmentwhere = '';
$disabled_open_invitation = '';
if($course->publish == 0){
   $nonUserSelectDisable = "disabled";
   $userSelectDisable = "disabled";
   $disabled_open_invitation = 'disabled="disabled"';
   //$noteForDisabled = "Course is not published yet.";
}else{

	  if($scheduler->isclasscompleted == 1 ){
	    $nonUserSelectDisable = "disabled";
        $userSelectDisable = "disabled";
        $disabled_open_invitation = 'disabled="disabled"';
       // $noteForDisabled = "Class has been completed.";
	  }elseif($CFG->isClassAllowForPastDate == 0 && $scheduler->enddate<time()){
	    $nonUserSelectDisable = "disabled";
        $userSelectDisable = "disabled";
        $disabled_open_invitation = 'disabled="disabled"';
        //$noteForDisabled = "Class has been ended.";
		
	  }else{
	    $session_list = classSessionList($id);
		if(count($session_list)==0){
		 $nonUserSelectDisable = "disabled";
		 $userSelectDisable = "disabled";
		 $disabled_open_invitation = 'disabled="disabled"';
		 //$noteForDisabled = "There is no session associated with class.";
		}
	  }	
}

if ($scheduler->enrolmenttype == $CFG->classroomOpen) {

	if ($scheduler->open_for == 0) {
		$sql = "SELECT sof.enrolid FROM mdl_scheduler_open_for AS sof
WHERE sof.enrolfor = 'department' AND sof.classid = '" . $scheduler->id . "'";
		$department_rec = $DB->get_records_sql ( $sql );

		$departmentList = count ( $department_rec ) > 0 ? array_keys ( $department_rec ) : array ();
		if (count ( $departmentList ) > 0) {
			$departmentListIds = implode ( ",", $departmentList );
			$departmentwhere .= " AND d.id IN ($departmentListIds)";
		} else {
			$departmentwhere .= " AND d.id IN (0)";
		}

		$openTypeAllUsers = getOpenDraftUsers ( $scheduler->id );
		$userList = count ( $openTypeAllUsers ) > 0 ? array_keys ( $openTypeAllUsers ) : array ();
		if (count ( $userList ) > 0) {
			$userListIds = implode ( ",", $userList );
			$where .= " AND dm.userid IN ($userListIds)";
		} else {
				
			$where .= " AND dm.userid IN (0)";
		}
	}
	$nonUserSelectDisable = "disabled";
	
	$information_text = get_string ( 'openclassinformationtext', 'scheduler' );
} else {
	$information_text = get_string ( 'inviteclassinformationtext', 'scheduler' );
}
$disable_send_invite = "";
$disable_send_invite = " disabled=disabled";

 $PAGE->set_title ( "$site->fullname: $stradduserstogroup" );
 $PAGE->set_heading ( $site->fullname );
 
 echo $OUTPUT->header (); 

// / Print the editing form
$courseHTML = '';
$courseHTML .= "";
if ($_SESSION ['update_msg'] != '') {
$courseHTML .= '<div class="clear"></div>';
$courseHTML .= '<div class="bg-success text-success text-center ' . $_SESSION ['error_class'] . '" style = "display:block !important;">' . $_SESSION ['update_msg'] . '</div>';
$courseHTML .= '<div class="clear"></div>';
$_SESSION ['update_msg'] = '';
}
echo $courseHTML;

?>
<style>
#end-date-div {
	display: none;
}
#colorbox #end-date-div {
	display: block;
	height: 420px;
}
#inline-iframe {
	position: absolute;
	left: -100000px;
}
</style>
<a href="#end-date-div" id="inline-iframe" rel="inline-iframe"
title="Enrollment">click</a>
<div class="end-date-div" id="end-date-div">
  <iframe src="" id="i-frame" width="100%" height="100%"></iframe>
</div>

<?php 
$enrol_visiblity = '';
if($scheduler->enrolmenttype==1){
        	$style = "display:none";
			$display = "style = 'display:none;'";
        }
        if($scheduler->enrolmenttype==0){
        	
        	$open_for_checked = $scheduler->open_for;
        	$enrol_visiblity = 'style="display:none"';
        	echo ' <script src="'.$CFG->wwwroot.'/mod/scheduler/scripts/assignuser.js"></script>';
        ?>

 <div class="tabLinks">
<a id="tab_availableto" class="current" href="javascript:void(0)"><?php echo get_string('availableto','scheduler'); ?><span></span></a>
<a id="tab_enrolto" href="javascript:void(0)">Enroll to<span></span></a>
</div>


<div class="tab_availableto_div borderBlockSpace">
<form id="availabletoform" method="post"
action="<?php echo $CFG->wwwroot; ?>/mod/scheduler/classallocation.php?id=<?php echo $id; ?>">
  <input type='hidden' name="courseid" id="courseid" value="<?php echo $scheduler->id?>">
  <input type='hidden' name="classid" id="classid" value="<?php echo $cm->course?>">
  <input type='hidden' name="formsubmit" value="1">
<?php 

$style = '';

$display = '';
$open_for_html =  "<div id='open_for' class='margin_none paddBottom'> <input type='checkbox' id='check_open_for' name='open_for'  ".is_checked($open_for_checked,1,'checkbox')." onclick='showDepartment(this.value,\"open_for\")'  ".$disabled_open_invitation."/> ".get_string('openforall','scheduler')."</div>";

echo '<div class="">'.$open_for_html.'</div>';
echo assignUsersHtml1($scheduler->id,$style,$disabled_open_invitation, $display); 
?>
<input name="saveopeninvite" value="Save" style="display:none" type="submit" id="id_submitbutton">
<input rel="enabled" name="sendopeninvite" value="Invite" type="submit" id="id_sendinvite">
</form>
</div>
<script type="text/javascript">
jQuery(document).ready(function(){
	$("#tab_availableto").click(function(){
		$(".tab_availableto_div").show();
		$(".tab_enrolto_div").hide();
		$(".tabLinks a").removeClass('current');
		$(this).addClass('current');
		});
	$("#tab_enrolto").click(function(){
		$(".tab_availableto_div").hide();
		$(".tab_enrolto_div").show();
		$(".tabLinks a").removeClass('current');
		$(this).addClass('current');
		});
	$(".enrolled-class-user-filter-box").click(function(){
		disableSaveInvite();
		});
	$("#department_removeselect_searchtext_btn,#team_removeselect_searchtext_btn,#user_removeselect_searchtext,#department_removeselect_clearbutton,#team_removeselect_clearbutton,#user_removeselect_clearbutton").click(function(){
		disableSaveInvite();		
		});

        })
</script>
<?php } ?>
<div <?php echo $enrol_visiblity?> class="tab_enrolto_div">
<form id="assignform" method="post"
action="<?php echo $CFG->wwwroot; ?>/mod/scheduler/classallocation.php?id=<?php echo $id; ?>">
  <input type='hidden' name="courseid" id="courseid"
value="<?php echo $scheduler->id?>">
  <input type='hidden'
name="classid" id="classid" value="<?php echo $cm->course?>">
  <input
type='hidden' name="formsubmit" value="1">
  <?php
$depQuery = '';  
if($CFG->showExternalDepartment != 1){
 $depQuery .= " AND d.is_external = 0 ";
} 

$extDeptIdentifier = get_string('external_department_identifier', 'department'); 
$sql = "SELECT DISTINCT d.id,(if(d.is_external = 1, concat('".$extDeptIdentifier."', d.title), d.title)) AS title,d.is_external
FROM {$CFG->prefix}department AS d
LEFT JOIN {$CFG->prefix}department_members AS dm ON d.id = dm.departmentid
WHERE d.deleted = 0 AND d.status = 1  " . $where .$depQuery. "
UNION 
SELECT DISTINCT d.id,(if(d.is_external = 1, concat('".$extDeptIdentifier."', d.title), d.title)) AS title,d.is_external
FROM {$CFG->prefix}department AS d WHERE d.status=1 " . $departmentwhere . $depQuery. "
ORDER BY title ASC";
// echo $sql = "SELECT d.id,d.title FROM {$CFG->prefix}department as d LEFT JOIN {$CFG->prefix}department_members as dm ON d.id = dm.departmentid WHERE d.deleted = 0 AND d.status = 1 ".$where." order by d.title ASC";
// die;
//echo $sql;die;
$departments = $DB->get_records_sql ( $sql );

echo '<fieldset class="" id="yui_3_13_0_2_1404228340264_14">';
?>

  <?php
echo '<div class="borderBlockSpace">

<div class="">

<div class="tab-box ">';

echo '<div class="msg-tab-label">Enroll To</div><div class="selectBoxOuter">';

echo createDepartmentSelectBoxForFilter('department', $departments, array('-1'), '', false);
 
echo '<select id="id_team"><option value="0">' . get_string ( 'select_team' ) . '</option></select>';
$activeLink = 'activeLink';
//if(count($departments) > 0 ){

$identifierED = getEDAstric('lower-assign-filter');

echo createSelectBoxFilterFor('job_title', 'job_title', $sJobTitleArr, false);
echo createSelectBoxFilterFor('company', 'company', $sCompanyArr, false);

echo '</div><div class="classAllocationStatus">'; 
echo $identifierED;
echo '<div class="enroll_status">
    <ul>
      <li><span id="decline"></span> Declined</li>
      <li><span id="waiting"></span>Waiting For Approval</li>
      <li><span id="approved"></span>Approved</li>
    </ul>
  </div></div>';

echo '</div>';

$hide = '';

echo '<div class="borderBlock borderBlockSpace" id="cont-3-1">';
?>
  <div id="addmembersform">
    <div class="information_text"><?php echo $information_text."<br>".$noteForDisabled; ?></div>
    <div>
      <input type="hidden" name="sesskey" value="<?php p(sesskey()); ?>" />
      <table class="assignTable">
        <tr>
          <td id='potentialcell' class="potentialcell"><p>
              <label for="addselect"><?php echo get_string('coursenonuser');?></label>
            </p>
            <div id="user_addselect_wrapper" class="userselector">
              <!--div class="<?php echo $nonUserSelectDisable?>"></div-->
              <select multiple="multiple" id="non_user_list" name="adduser[]"
size='20' <?php echo $nonUserSelectDisable?>>
                <?php $courses->classNonUserList(); ?>
              </select>
            </div></td>
          <td id='buttonscell'><div class="arrow_button">
              <input class="moveLeftButton" name="remove" id="user_remove" type="submit"
value="<?php echo get_string('remove').'&nbsp;'.$OUTPUT->rarrow(); ?>"
title="<?php print_string('remove'); ?>" disabled="" />
              <input class="moveRightButton" name="add" id="user_add" type="submit"
value="<?php echo $OUTPUT->larrow().'&nbsp;'.get_string('add'); ?>"
title="<?php print_string('add'); ?>" disabled="" />
            </div></td>
          <td id='existingcell' class="potentialcell"><p>
              <label for="removeselect"><?php echo get_string('courseuser');?></label>
            </p>
            <div id="user_removeselect_wrapper" class="userselector">
              <select multiple="multiple" id="addusercourse"
name="addusercourse[]" size='20'
<?php echo $userSelectDisable;?>>
                <?php $courses->course_option_list('user_course'); ?>
              </select>
            </div></td>
        </tr>
      </table>
    </div>
    <fieldset
style="margin: 10px 0px 0px; text-align: center; clear: both;">
    <div>
      <div class="fitem fitem_actionbuttons fitem_fgroup"
id="fgroup_id_buttonar">
        <div class="felement fgroup">
         
          <?php if($scheduler->isclasscompleted==0){?>
          <input type="submit" <?php echo $disable_send_invite;?> id="id_submitbutton1"  value="<?php echo get_string('sendinvite','scheduler')?>" name="sendinvite">
		  <span class="element" style="display:block">NOTE: Clicking 'Send Invite' button will send a subsequent email invitation to the user.</span>
          <?php }?>
        </div>
      </div>
    </div>
    </fieldset>
  </div>
  <?php

echo '</div></div></div>';
?>
  </fieldset>
</form>
</div>

<script>
$(document).ready(function(){
	
	$("#inline-iframe").colorbox({inline:true, width:"675px", height:"468px"});
	
	$("body").on('click','.select_all', function() {
	var type = $(this).attr("rel");
	var selectbox_id = '';
	var checkbox_id = '';
	
	if(type=='team'){
	selectbox_id = 'addteamcourses';
	checkbox_id = 'team_checkbox_id';
	remove_button_id =  'team_remove';
	}
	if(type=='department'){
	selectbox_id = 'add_department_course';
	checkbox_id = 'department_checkbox_id';
	remove_button_id =  'department_remove';
	}
	if(type=='user'){
	selectbox_id = 'addusercourse';
	checkbox_id = 'user_checkbox_id';
	remove_button_id =  'user_remove';
	}
	
	var team_id = $(this).val();
	
	if ($(this).prop('checked')==true){ 
	
	$('#'+selectbox_id+' option').attr("selected", "selected");
	$('.'+checkbox_id).prop("checked", "checked");
	$("#"+remove_button_id).removeAttr("disabled");
	}
	else{
	$('#'+selectbox_id+' option').attr("selected",false);
	$('.'+checkbox_id).prop("checked", false);
	$("#"+remove_button_id).prop("disabled","disabled");
	}
	
	
	})
	
	$("body").on('click','.user_checkbox_id', function() {
	
		var user_id = $(this).val();
		$(".tab_enrolto_div #user_remove").removeAttr("disabled");
		if ($(this).prop('checked')==true){ 
		$('#addusercourse option[value="'+user_id+'"]').attr("selected", "selected");
		}
		else{
		$('#addusercourse option[value="'+user_id+'"]').attr("selected",false);
		}
		
		
	})
	var userRole = "<?php echo $USER->archetype; ?>";
	var userManager = "<?php echo $CFG->userTypeManager; ?>";
	
	$(".tabLink").each(function(){
		$(this).click(function(){
		tabeId = $(this).attr("id");
		$(".tabLink").removeClass("activeLink");
		$(this).addClass("activeLink");
		$(".tabcontent").addClass("hide");
		$("#"+tabeId+"-1").removeClass("hide");					
		return false;	  
		});
	}); 	
	
	
	var fromUser = $('#user_addselect_wrapper select');
	var toUser = $('#user_removeselect_wrapper select');	
	
	$(document).on("click", ".ui-multiselect-checkboxes li",function(){
	$(".tab_enrolto_div #user_remove").removeAttr("disabled");
	});
	
	$(document).on("change", ".tab_enrolto_div [name = 'adduser[]']",function(){
		$(".tab_enrolto_div #user_add").removeAttr("disabled");
	});
	$(document).on("change", ".tab_enrolto_div [name = 'addusercourse[]']",function(){
		if($(this).attr('class') == 'option-disable'){
			$(".tab_enrolto_div #user_remove").addAttr("disabled");
		}else{
			$(".tab_enrolto_div #user_remove").removeAttr("disabled");
		}
		$("#id_submitbutton1").removeAttr("disabled");
	
	});
	
	$(document).on("click",".tab_enrolto_div #user_add",function(){
	
		parent.top.$('#cboxLoadedContent').css('margin-top','0').css('height','100%');
		parent.top.$('#cboxTitle,#cboxClose').css('z-index','-1');
		
		var val = [];
		
		$("#user_addselect_wrapper select option:selected").each(function () {
		val.push(this.value);
		});
		var invite_to_user = val.join(',');
		
		
		var enrolid = $("#courseid").val();
		var userId =invite_to_user;
		
		var source = '<?php echo $CFG->wwwroot;?>/mod/scheduler/enrolintoclass.php?element=user&assigntype=classroom&assignId='+enrolid+'&elementList='+userId;
		$("#i-frame").attr('src',source);
		
		$(".cboxContent #end-date-div").attr("style",'display:block');
		$("#inline-iframe").trigger("click");			
		return false;
	});
	
	$(document).on("click",".tab_enrolto_div #user_remove",function(){
		parent.top.$('#cboxLoadedContent').css('margin-top','0').css('height','100%');
		parent.top.$('#cboxTitle,#cboxClose').css('z-index','-1');
		
		var val = [];
		$(".tab_enrolto_div #user_removeselect_wrapper select option:selected").each(function () {
		val.push(this.value);
		});
		var invite_to_user = val.join(',');
		
		
		var enrolid = $("#courseid").val();
		var userId =invite_to_user;
		
		var source = '<?php echo $CFG->wwwroot;?>/course/unenrolcourse.php?element=user&assigntype=classroom&assignId='+enrolid+'&elementList='+userId;
		$("#i-frame").attr('src',source);
		
		
		$(".cboxContent #end-date-div").attr("style",'display:block');
		$("#inline-iframe").trigger("click");			
		return false;
	
	});
	
	$(document).on("click","#cboxOverlay",function(){
		parent.top.$('#cboxLoadedContent').css('margin-top','43px').css('height','100%');
		parent.top.$('#cboxTitle,#cboxClose').css('z-index','0');
	});
	
	
	$("#department, #job_title, #company").change(function(){
		
		var departmentId = $('#department').val();
		departmentId = departmentId=='-1'?0:departmentId;
		var id =$("#courseid").val(); 
		var classid =$("#classid").val(); 	
		var job_title =$("#job_title").val();
		var company =$("#company").val();
		var url ='<?php echo $CFG->wwwroot;?>/mod/scheduler/ajax.php?action=getNonUserList&departmentId='+departmentId+'&id='+id+'&classid='+classid;
		console.log(url);
		$.ajax({
			url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
			type:'POST',
			data:'action=getDepartmentTeamList&departmentId='+departmentId,
			success:function(data){
			$("#id_team").html(data);
			}
		});
		
		$.ajax({
			url:'<?php echo $CFG->wwwroot;?>/mod/scheduler/ajax.php',
			type:'POST',
			data:'action=getNonUserList&departmentId='+departmentId+'&id='+id+'&classid='+classid+'&job_title='+job_title+'&company='+company,
			success:function(data){
			$("#non_user_list").html(data);
			}
		});
		
		
	});
	
	$("#id_team").change(function(){
	
		var departmentId = $("#department").val();
		departmentId = departmentId=='-1'?0:departmentId;
		var teamId = $(this).val();
		var id =$("#courseid").val(); 
		var classid =$("#classid").val(); 	
		
		
		$.ajax({
			url:'<?php echo $CFG->wwwroot;?>/mod/scheduler/ajax.php',
			type:'POST',
			data:'action=getTeamUserData&departmentId='+departmentId+'&teamId='+teamId+'&id='+id+'&classid='+classid,
			success:function(data){
			$("#non_user_list").html(data);
			}
		});
	});
});


function maintainClassSelectCount(fromSelect,toSelect,SelectText){
	var fromCnt = 0;
	var toCnt = 0;
	var fromLabel = '';
	var toLabel = '';
	
	fromSelect.find('#prefered_user').find('option').each(function(){
	fromCnt++;
	});
	fromLabel = 'Prefered Users '+'('+fromCnt+')';
	fromSelect.find('#prefered_user').attr('label',fromLabel);
	fromCnt = 0;
	fromSelect.find('#other_user').find('option').each(function(){
	fromCnt++;
	});
	
	fromLabel = 'Users '+'('+fromCnt+')';
	fromSelect.find('#other_user').attr('label',fromLabel);
	
	//fromSelect.find('optgroup').attr('label',fromLabel);
	
	toSelect.find('option').each(function(){
	toCnt++;
	});
	if(toCnt == 0){
	toLabel = 'None';
	}else{
	toLabel = SelectText+'('+toCnt+')';
	}
	toSelect.find('optgroup').attr('label',toLabel);
}
</script>
<style>
	#cboxClose{ display:none}
	#page-user-assignusers #page #region-main .msg-tab-html .msg-tab-label {
	margin-top: 13px;
	}
	
	#id_submitbutton1 {
	margin-right: 0px !important;
	}
	#i-frame{border:0;}
</style>
<?php
echo '</div>';
echo $OUTPUT->footer();