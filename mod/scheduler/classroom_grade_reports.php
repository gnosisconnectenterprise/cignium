<?php


$learnerList = isset($reportsArr['classroom'][$cid]['class'][$classId]['approved']['users'])?$reportsArr['classroom'][$cid]['class'][$classId]['approved']['users']:array();
$sessionList = isset($reportsArr['classSessions'])?$reportsArr['classSessions']:array();

$classInstructorName = isset($reportsArr['classroom'][$cid]['class'][$classId]['classInstructorFullName'])?$reportsArr['classroom'][$cid]['class'][$classId]['classInstructorFullName']:getMDash();

$classInstructorNameCSV = isset($reportsArr['classroom'][$cid]['class'][$classId]['classInstructorFullName'])?$reportsArr['classroom'][$cid]['class'][$classId]['classInstructorFullName']:getMDashForCSV();

$classStartDate = isset($reportsArr['classroom'][$cid]['class'][$classId]['classStartDate'])?$reportsArr['classroom'][$cid]['class'][$classId]['classStartDate']:0;
$classEndDate = isset($reportsArr['classroom'][$cid]['class'][$classId]['classEndDate'])?$reportsArr['classroom'][$cid]['class'][$classId]['classEndDate']:0;
$className = isset($reportsArr['classroom'][$cid]['class'][$classId]['className'])?$reportsArr['classroom'][$cid]['class'][$classId]['className']:'';
$classSubmittedByName = isset($reportsArr['classroom'][$cid]['class'][$classId]['classSubmittedByName'])?$reportsArr['classroom'][$cid]['class'][$classId]['classSubmittedByName']:'';
$noOfSeats = isset($reportsArr['classroom'][$cid]['class'][$classId]['noOfSeats'])?$reportsArr['classroom'][$cid]['class'][$classId]['noOfSeats']:0;


$durationText = getClassDuration($classStartDate, $classEndDate);
$durationTextCSV = getClassDuration($classStartDate, $classEndDate, 'csv');


$reportContentPDF = '';
$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" width="100%">';
	$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td><strong>'.get_string("course","classroomreport").':</strong> '.$courseName.'</td>';
		$reportContentPDF .= '<td><strong>'.get_string("classname","classroomreport").':</strong> '.$className.'</td>';
	$reportContentPDF .= '</tr>';

	$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td><strong>'.get_string('class_date_label').':</strong> '.$durationText.'</td>';
		$reportContentPDF .= '<td><strong>'.get_string('instructor','scheduler').':</strong> '.$classInstructorName.'</td>';
	$reportContentPDF .= '</tr>';


	if($noOfSeats && $classSubmittedByName){
		$reportContentPDF .= '<tr>';
			$reportContentPDF .= '<td><strong>'.get_string('no_of_seats','scheduler').':</strong> '.$noOfSeats.'</td>';
			$reportContentPDF .= '<td><strong>'.get_string('classsubmittedby','scheduler').':</strong> '.$classSubmittedByName.'</td>';
		$reportContentPDF .= '</tr>';
	}elseif($noOfSeats && $classSubmittedByName==''){
		$reportContentPDF .= '<tr>';
			$reportContentPDF .= '<td colspan="2"><strong>'.get_string('no_of_seats','scheduler').':</strong> '.$noOfSeats.'</td>';
		$reportContentPDF .= '</tr>';
	}elseif($classArr['no_of_seats'==''] && $classSubmittedByName){
		$reportContentPDF .= '<tr>';
			$reportContentPDF .= '<td  colspan="2" ><strong>'.get_string('classsubmittedby','scheduler').':</strong> '.$classSubmittedByName.'</td>';
		$reportContentPDF .= '</tr>';
	}
	
	
	if($userCount > 0){
	
		$reportContentPDF .= '<tr>';
				$reportContentPDF .= '<td><strong>'.get_string('numberofusers','classroomreport').':</strong> '.$userCount.'</td>';
				$reportContentPDF .= '<td><strong>'.get_string('totalinviteduser','classroomreport').':</strong> '.$totalInviteUser.'</td>';
		$reportContentPDF .= '</tr>';
			
		
		$reportContentPDF .= '<tr>';
				$reportContentPDF .= '<td><strong>'.get_string('managerforapproval','classroomreport').':</strong> '.$waitingUser.'</td>';
				$reportContentPDF .= '<td><strong>'.get_string('approveduser','classroomreport').':</strong> '.$approvedUser.'</td>';
		$reportContentPDF .= '</tr>';
			
		$reportContentPDF .= '<tr>';
				$reportContentPDF .= '<td><strong>'.get_string('declineduser','classroomreport').':</strong> '.$declineUser.'</td>';
				$reportContentPDF .= '<td><strong>'.get_string('inprogress','classroomreport').':</strong> '.$in_completed_count.'</td>';
		$reportContentPDF .= '</tr>';

		$reportContentPDF .= '<tr>';
				$reportContentPDF .= '<td><strong>'.get_string('completed','classroomreport').':</strong> '.$completed_count.'</td>';
				$reportContentPDF .= '<td><strong>'.get_string('status','classroomreport').':</strong> '.$sTypeName.'</td>';
		$reportContentPDF .= '</tr>';		
		
	}	
	
	
	 // $reportContentPDF .= '<tr><br /><td width="100%"  ><span '.$CFG->pdfSpanAttribute.'><strong>'.get_string('overalluserprogressreport','classroomreport').'</strong></span><br /></td></tr>';
	  
 $reportContentPDF .= '</table>';
 
 $reportContentPDF .= getGraphImageHTML(get_string('overalluserprogressreport','classroomreport'));
 
 
 $reportContentPDF .= '<table '.$CFG->pdfTableStyle.'>';


$reportContentCSV = '';
$reportContentCSV .= get_string('course','classroomreport').",".$courseName."\n";
$reportContentCSV .= get_string('classname','classroomreport').",".$className."\n";
$reportContentCSV .= get_string('class_date_label').",".$durationTextCSV."\n";
$reportContentCSV .= get_string('instructor','scheduler').",".$classInstructorName."\n";
if($noOfSeats){
	$reportContentCSV .= get_string('no_of_seats','scheduler').",".$noOfSeats."\n";
}
if($classSubmittedByName){	
	$reportContentCSV .= get_string('classsubmittedby','scheduler').",".$classSubmittedByName."\n";
}
if($userCount > 0){
	
	$reportContentCSV .= get_string('numberofusers','classroomreport').",".$userCount."\n";
	$reportContentCSV .= get_string('totalinviteduser','classroomreport').",".$totalInviteUser."\n";
	$reportContentCSV .= get_string('managerforapproval','classroomreport').",".$waitingUser."\n";
	$reportContentCSV .= get_string('approveduser','classroomreport').",".$approvedUser."\n";
	$reportContentCSV .= get_string('declineduser','classroomreport').",".$declineUser."\n";
	$reportContentCSV .= get_string('inprogress','classroomreport').",".$in_completed_count."\n";
	$reportContentCSV .= get_string('completed','classroomreport').",".$completed_count."\n";
}
$reportContentCSV .= get_string('status','classroomreport').",".$sTypeName."\n";
$reportContentCSV .= get_string('overalluserprogressreport','classroomreport')."\n\n";


$cntSession = count($sessionList);
$cntLearner = count($learnerList);
if ($cntSession > 0  && $cntLearner > 0 ) {
   
    $courseHTML .= '<table class = "table1" id="table1">';
	$CSVCommas = ' ';
    if($cntSession > 0 ){
	  for($i=0; $i < $cntSession; $i++){
	    $CSVCommas .= ",";
	  }
	}
	
	$reportContentCSV .= get_string('name').", ,".get_string("sessionstext","scheduler").$CSVCommas.get_string ( 'userclassstatus', 'scheduler' )."\n";
			 
			 
	$reportContentPDF .= '<tr '.$CFG->pdfTableHeaderStyle.'>
	                <td rowspan="2" style="vertical-align:middle" ><strong>'.get_string('name').'</strong></td>
					<td rowspan="2"><strong>&nbsp;</strong></td>
					<td colspan="'.$cntSession.'" ><strong>'.get_string('sessionstext','scheduler').'</strong></td>
					<td rowspan="2" style="vertical-align:middle" ><strong>'.get_string('userclassstatus','scheduler').'</strong></td>
					</tr>';
					
	$reportContentPDF .= '<tr '.$CFG->pdfTableHeaderStyle.'>';
					
					$CSVSessionName = '';
					
					foreach ( $sessionList as $session ) {
					
						$sessionDuration = getClassroomSessionDateDuration($session->starttime, $session->duration); 
						$sessionDurationCSV = getClassroomSessionDateDuration($session->starttime, $session->duration, 'minutes', 'csv'); 
						$sessionName = $session->sessionname."<br>(". $sessionDuration.")";
						$sessionNameCSV = $session->sessionname." (". $sessionDurationCSV.")";
						$reportContentPDF .= '<td ><strong>' . $sessionName . '</strong></td>';
						$CSVSessionName .= ", ".$sessionNameCSV;
					}
							
	$reportContentPDF .= '</tr>';
	
	$reportContentCSV .= " , ".$CSVSessionName.", , , , \n";

	$courseHTML .= '<tr class="headingRow">
	                <td width="14%" style="vertical-align:middle" >'.get_string('name').'</td><td width="10%" class="bor_left">&nbsp;</td>';
	$courseHTML .= '<td width="35%" style="padding:0;">
	                   <table border="0" cellpadding="0" cellspacing="0" width="100%" class="margin_none">';
							if(count($sessionList)>=1){
								$courseHTML .= '<tr><td colspan="'.count($sessionList).'" style="text-align:center; border-bottom:1px solid #b9b9b9" class="bor_left">'.get_string("sessionstext","scheduler").'</td></tr>';		
							}
							$courseHTML .= '<tr>';
							$extraPDFCol = '';
							
							foreach ( $sessionList as $session ) {
					
								$sessionDuration = getClassroomSessionDateDuration($session->starttime, $session->duration); 
								$sessionDurationCSV = getClassroomSessionDateDuration($session->starttime, $session->duration, 'minutes', 'csv'); 
								$sessionName = $session->sessionname."<br>(". $sessionDuration.")";
								$sessionNameCSV = $session->sessionname." (". $sessionDurationCSV.")";

								$courseHTML .= "<td width='500' class='bor_left'>" . $sessionName . "</td>";
							}
					
						
							$courseHTML .= '</tr>';
							$courseHTML .= '
					   </table>
					</td>';
	$courseHTML .= "<td width = '10%' align='align_left' class='bor_left' style='vertical-align:middle' >" . get_string ( 'userclassstatus', 'scheduler' ) . "</td>";
	
	$courseHTML .= '</tr>';
    
	if($cntLearner > 0 ){
			foreach ( $learnerList as $learnerId=>$learner ) {		
					
					$classStatus = $learner['class_status'];
					
					$courseHTML .= "<tr class='rep_row'><td class='align_left'  width = '14%'><span class='f-left'>" . $learner['userFullname'] . "</span></td>";

					if($cntSession > 0 ){
						$courseHTML .= "<td width = '10%' class='gradeCell'>
										  <div class='att'>" . get_string ( 'attended', 'scheduler' ) . "</div>
										  <div class='score'>" . get_string ( 'score', 'scheduler' ) . "</div>
										  <div class='grade'>" . get_string ( 'grade', 'scheduler' ) . "</div>
										</td>";
										
						
					}
					
					   $courseHTML .= '<td class="sessionCell">
											 <table border="0" cellpadding="0" cellspacing="0" width="100%" class="session_table"><tr>';
											  $CSVAttended = '';
											  $CSVScore = '';
											  $CSVGrade = '';
											  
											  $attendedColPDF = '';
											  $scoreColPDF = '';
										      $gradeColPDF = '';
											  
											  foreach ( $sessionList as $sessionId=>$session ) {
													$sessionAttended = isset($learner['sessions'][$sessionId]['sessionAttended'])?$learner['sessions'][$sessionId]['sessionAttended']:0;
													
													$sessionScore = isset($learner['sessions'][$sessionId]['sessionScore'])?$learner['sessions'][$sessionId]['sessionScore']:0;
													$sessionGrade = isset($learner['sessions'][$sessionId]['sessionGrade'])?$learner['sessions'][$sessionId]['sessionGrade']:0;
													
																						
												    if($classStatus != get_string('classnoshow','classroomreport')){																	
														$courseHTML .= "<td  width='500'>
																		  <div class='att'>".($sessionAttended == 1?'Yes':'No')."</div>
																		  <div class='score'>".($sessionScore!=''?$sessionScore:getMDash())."</div>
																		  <div class='grade'>".($sessionGrade!=''?$sessionGrade:getMDash())."</div>
																	   </td>";
																	   
														$attendedColPDF .= '<td style="background-color: #f1faff;border-bottom: 1px solid #000;border-right: 1px solid #000;">'.($session_rec->attended == 1?'Yes':'No').'</td>';
									                    $scoreColPDF .= '<td style="background-color: #f3f3f3;border-bottom: 1px solid #000;border-right: 1px solid #000;">'.($session_rec->score!=''?$session_rec->score:getMDash()).'</td>';
									                    $gradeColPDF .= '<td style="background-color: #fff9ef;border-bottom: 1px solid #000;border-right: 1px solid #000;">'.($session_rec->grade!=''?$session_rec->grade:getMDash()).'</td>';
														
														$CSVAttended .= ", ".($sessionAttended == 1?'Yes':'No');
														$CSVScore .= ", ".($sessionScore?$sessionScore:getMDashForCSV());
														$CSVGrade .= ", ".($sessionGrade?$sessionGrade:getMDashForCSV());

													}else{
													
													    $courseHTML .= "<td  width='500'>
																		  <div class='att'>".getMDash()."</div>
																		  <div class='score'>".getMDash()."</div>
																		  <div class='grade'>".getMDash()."</div>
																	   </td>";

														$attendedColPDF .= '<td style="background-color: #f1faff;border-bottom: 1px solid #000;border-right: 1px solid #000;">'.(getMDash()).'</td>';
														$scoreColPDF .= '<td style="background-color: #f3f3f3;border-bottom: 1px solid #000;border-right: 1px solid #000;">'.(getMDash()).'</td>';
														$gradeColPDF .= '<td style="background-color: #fff9ef;border-bottom: 1px solid #000;border-right: 1px solid #000;">'.(getMDash()).'</td>';	
														
														
														$CSVAttended .= ", ".(getMDashForCSV());
														$CSVScore .= ", ".(getMDashForCSV());
														$CSVGrade .= ", ".(getMDashForCSV());
														
													}			   
																
											  }	
											  
											
									  
																					
					   
					   $courseHTML .= '</tr></table></td>';
					


                       $statColPDF = '';	
					   if($is_class_submitted == 1){
					   
					     $courseHTML .= '<td class="align_left">'.$classStatus.'</td>';
						 
						 $statColPDF = '<td style="border-bottom: 1px solid #fff;border-top: 1px solid #fff;" >'.$classStatus.'</td>';
					     
					     
					   }else{
					      $courseHTML .= '<td class="align_left">'.$classStatus.'</td>';
						  //$statColPDF = '<td >'.(getMDash()).'</td>';
						  $statColPDF = '<td >'.$classStatus.'</td>';
					   }
					   
					   
					   
					   

					   

						$reportContentPDF .= '<tr  '.$CFG->pdfTableRowAttribute.'><td style="border-bottom: 1px solid #fff;border-right: 1px solid #000;" >&nbsp;</td><td style="background-color: #d2f3ff;border-bottom: 1px solid #000;border-right: 1px solid #000;">' . get_string ( 'attended', 'scheduler' ) .'</td>'.$attendedColPDF.'<td >'.(getSpace()).'</td></tr>';
						$reportContentPDF .= '<tr  '.$CFG->pdfTableRowAttribute.' >
						                       <td style="border-bottom: 1px solid #fff;border-right: 1px solid #000;">' . $learner['userFullname'] . '</td>
											   <td style="background-color: #e5e5e5;border-bottom: 1px solid #000;border-right: 1px solid #000;">' . get_string ( 'score', 'scheduler' ) ."</td>".$scoreColPDF.$statColPDF.'</tr>';
						$reportContentPDF .= '<tr  '.$CFG->pdfTableRowAttribute.' ><td>&nbsp;</td><td style="background-color: #ffebd0;border-bottom: 1px solid #000;border-right: 1px solid #000;">' . get_string ( 'grade', 'scheduler' ) .'</td>'.$gradeColPDF.'<td  >'.(getSpace()).'</td></tr>';									
												

						  $reportContentCSV .= $learner['userFullname'].",".get_string("attended","scheduler").$CSVAttended."\n";
						  $reportContentCSV .= " ,".get_string("score","scheduler").$CSVScore.",".($classStatus)."\n";
						  $reportContentCSV .= " ,".get_string("grade","scheduler").$CSVGrade.", , \n"; 
													

						$courseHTML .= "</tr>";

					
				}
		}
		
		$courseHTML .= "</table>";
		
}else{
  $courseHTML .= '<table class = "table1" id="table1">';
  $courseHTML .= '<tr class="headingRow">
	                <td width="14%" style="vertical-align:middle" >'.get_string('name').'</td>';
					
	if(count($sessionList)>=1){
	$courseHTML .= '<td width="35%" style="padding:0;">
	                   <table border="0" cellpadding="0" cellspacing="0" width="100%" class="margin_none">';
							if(count($sessionList)>=1){
								$courseHTML .= '<tr><td colspan="'.count($sessionList).'" style="text-align:center; border-bottom:1px solid #b9b9b9" class="bor_left">'.get_string("sessionstext","scheduler").'</td></tr>';		
							}
							$courseHTML .= '<tr>';
							foreach ( $sessionList as $session ) {
								$courseHTML .= "<td width='33%' class='bor_left'>" . $session->sessionname . "</td>";
							}
							$courseHTML .= '</tr>';
							$courseHTML .= '
					   </table>
					</td>';
					
	}else{
	   $courseHTML .= '<td width="10%" class="bor_left">'.get_string("sessionstext","scheduler").'</td>';
	}				
	//$courseHTML .= "<td width = '15%' align='align_left' >" . get_string ( 'remarks', 'scheduler' ) . "</td>";
	$courseHTML .= "<td width = '10%' align='align_left' class='bor_left' style='vertical-align:middle' >" . get_string ( 'userclassstatus', 'scheduler' ) . "</td>";
	//$courseHTML .= "<td width = '15%' align='align_left' >".get_string('document','classroomreport')."</td>";
	$courseHTML .= '</tr>';
  $courseHTML .=  '<tr><td colspan="5" align="center" ><div>'.get_string('no_results').'</div></td></tr>';
  $reportContentPDF .=  '<tr><td colspan="5" align="center"  >'.get_string('no_results').'</td></tr>';
  $courseHTML .= "</table>";
}


$courseHTML .= "<style>
			     	#page .course-listing table tr:first-child td div{font-weight: normal !important;} 
				</style>";
$reportContentPDF .= "</table>";

if (! empty ( $courseHTML )) {
	$courseHTML .=  paging_bar ( $userCount, $page, $perpage, $genURL );
}

