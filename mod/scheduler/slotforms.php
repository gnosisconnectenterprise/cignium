<?PHP
/**
 * Slot-related forms of the scheduler module
 * (using Moodle formslib)
 *
 * @package    mod
 * @subpackage scheduler
 * @copyright  2013 Henning Bostelmann and others (see README.txt)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');
require_once($CFG->dirroot . '/course/moodleform_mod.php');
/**
 * Base class for slot-related forms
 */
abstract class scheduler_slotform_base extends moodleform {

    protected $scheduler;
    protected $cm;
    protected $context;
    protected $usergroups;
    protected $has_duration = false;

    public function __construct($action, $scheduler, $cm, $usergroups, $customdata=null) {
        $this->scheduler = $scheduler;
        $this->cm = $cm;
        $this->context = context_module::instance($cm->id);
        $this->usergroups = $usergroups;
        parent::__construct($action, $customdata);
    }

    protected function add_base_fields() {

        global $CFG, $USER;

        $mform = $this->_form;

        // exclusivity
        $maxexclusive = $CFG->scheduler_maxstudentsperslot;
        $exclusivemenu['0'] = get_string('unlimited', 'scheduler');
        for ($i = 1; $i <= $maxexclusive; $i++) {
            $exclusivemenu[(string)$i] = $i;
        }
        $mform->addElement('select', 'exclusivity', get_string('multiplestudents', 'scheduler'), $exclusivemenu);
        $mform->setDefault('exclusivity', 0);
        $mform->addHelpButton('exclusivity', 'exclusivity', 'scheduler');
		
        // reuse the slot?
        $mform->addElement('selectyesno', 'reuse', get_string('reuse', 'scheduler'));
        $mform->setDefault('reuse', 1);
        $mform->addHelpButton('reuse', 'reuse', 'scheduler');

        // location of the appointment
        $mform->addElement('text', 'appointmentlocation', get_string('location', 'scheduler'), array('size'=>'30'));
        $mform->setType('appointmentlocation', PARAM_NOTAGS);
        $mform->addRule('appointmentlocation', get_string('error'), 'maxlength', 50);
        $mform->setDefault('appointmentlocation', scheduler_get_last_location($this->scheduler));
        $mform->addHelpButton('appointmentlocation', 'location', 'scheduler');

       
    }

    protected function add_minutes_field($name, $label, $defaultval, $minuteslabel = 'minutes') {
    	
        $mform = $this->_form;
       
       	$duration_arr = durationArray(true);
        $mform->addElement('select', $name,get_string($label, 'scheduler'), $duration_arr, array('style'=>'min-width:200px'));
     
    }

    protected function add_duration_field($minuteslabel = 'minutes') {
        $this->add_minutes_field('duration', 'durationwithtime', $this->scheduler->defaultslotduration, $minuteslabel);
        $this->has_duration = true;
    }


    public function validation($data, $files) {
        $errors = parent::validation($data, $files);

        // Check duration for valid range
        if ($this->has_duration) {
            $limits = array('min' => 1, 'max' => 24*60);
            if ($data['duration'] < $limits['min'] || $data['duration'] > $limits['max']) {
                $errors['durationgroup'] = get_string('durationrange', 'scheduler', $limits);
            }
        }

        return $errors;
    }
}

class scheduler_editslot_form extends scheduler_slotform_base {

    protected $slotid;

    protected function definition() {

        global $DB,$CFG;
       
        $mform = $this->_form;
            
        $this->slotid = 0;
        if (isset($this->_customdata['slotid'])) {
            $this->slotid = $this->_customdata['slotid'];
        }
      //  $mform->addElement('header','general', get_string('general', 'form'));
        
        $mform->addElement('text', 'sessionname', get_string('title'), array('size'=>'64'));
        $mform->setType('sessionname', PARAM_CLEANHTML);
        $mform->addRule('sessionname', get_string('required'), 'required', null, 'client');
        // Start date/time of the slot
        if($CFG->is12HourFormat==1){
        	 
        	$this->scheduler->startdate = setTimeAsPerTimeFormat($this->scheduler->startdate);
        }
        $mform->addElement('date_time_selector', 'starttime', get_string('date', 'scheduler'));
     
       
       
        
       
        $mform->setDefault('starttime',$this->scheduler->startdate);
       // $mform->addHelpButton('starttime', 'choosingslotstart', 'scheduler');
        $mform->addElement('hidden', 'timestartformat');
        $this->add_duration_field();
        $mform->addElement('text', 'location', get_string('location', 'scheduler'), array('size'=>'2'));
        $mform->setType('location', PARAM_CLEANHTML);
       // $this->add_intro_editor(false, get_string('webaccesslink', 'scheduler'));
        // Duration of the slot
        $mform->addElement('editor', 'web_access_link', get_string('webaccesslink', 'scheduler'));
        $mform->setType('web_access_link', PARAM_RAW);
       

        // Ignore conflict checkbox
        $mform->addElement('checkbox', 'ignoreconflicts', get_string('ignoreconflicts', 'scheduler'));
        $mform->setDefault('ignoreconflicts', true);
        $mform->addHelpButton('ignoreconflicts', 'ignoreconflicts', 'scheduler');

        // Common fields
      
        $this->add_base_fields();

        // Display slot from date
        $mform->addElement('date_selector', 'hideuntil', get_string('displayfrom', 'scheduler'));
        $mform->setDefault('hideuntil', time());

        // Send e-mail reminder
        $mform->addElement('date_selector', 'emaildate', get_string('emailreminderondate', 'scheduler'), array('optional'  => true));
        $mform->setDefault('remindersel', -1);

        // Slot comments
        $mform->addElement('editor', 'notes', get_string('comments', 'scheduler'), array('rows' => 3, 'columns' => 60), array('collapsed' => true));
        $mform->setType('notes', PARAM_RAW);

        // Appointments

        $repeatarray = array();
        $grouparray = array();
        $repeatarray[] = $mform->createElement('header', 'appointhead', get_string('appointmentno', 'scheduler', '{no}'));

        // Choose student
        $students = scheduler_get_possible_attendees($this->cm, $this->usergroups);
        $studentsmenu = array('0' => get_string('choosedots'));
        if ($students) {
            foreach ($students as $astudent) {
                if ($this->scheduler->schedulermode == 'oneonly' && scheduler_has_slot($astudent->id, $this->scheduler, true, false, $this->slotid)) {
                    continue;
                }
                if ($this->scheduler->schedulermode == 'onetime' && scheduler_has_slot($astudent->id, $this->scheduler, true, true, $this->slotid)) {
                    continue;
                }
                $studentsmenu[$astudent->id] = fullname($astudent);
            }
        }
        $grouparray[] = $mform->createElement('select', 'studentid', '', $studentsmenu);

        // Seen tickbox
        $grouparray[] = $mform->createElement('static', 'attendedlabel', '', get_string('seen', 'scheduler'));
        $grouparray[] = $mform->createElement('checkbox', 'attended');

        // Grade
        if ($this->scheduler->scale != 0) {
            $gradechoices = scheduler_get_grading_choices($this->scheduler);
            $grouparray[] = $mform->createElement('static', 'attendedlabel', '', get_string('grade', 'scheduler'));
            $grouparray[] = $mform->createElement('select', 'grade', '', $gradechoices);
        }

        $repeatarray[] = $mform->createElement('group', 'studgroup', get_string('student', 'scheduler'), $grouparray, null, false);

        // Appointment notes
        $repeatarray[] = $mform->createElement('editor', 'appointmentnote', get_string('appointmentnotes', 'scheduler'), 
                          array('rows' => 3, 'columns' => 60), array('collapsed' => true));

        if (isset($this->_customdata['repeats'])) {
            $repeatno = $this->_customdata['repeats'];
        } else if ($this->slotid) {
            $repeatno = $DB->count_records('scheduler_appointment', array('slotid' => $this->slotid));
            $repeatno += 1;
        } else {
            $repeatno = 1;
        }

        $repeateloptions = array();
        $nostudcheck = array('studentid', 'eq', 0);
        $repeateloptions['attended']['disabledif'] = $nostudcheck;
        $repeateloptions['appointmentnote']['disabledif'] = $nostudcheck;
        $repeateloptions['grade']['disabledif'] = $nostudcheck;
        $repeateloptions['appointhead']['expanded'] = true;

       /*  $this->repeat_elements($repeatarray, $repeatno, $repeateloptions,
                        'appointment_repeats', 'appointment_add', 1, get_string('addappointment', 'scheduler')); */

        $this->add_action_buttons(true,get_string('savechanges'),true);

    }

    public function validation($data, $files) {
    	global $DB,$CFG;
        $errors = parent::validation($data, $files);
		// Avoid slots starting in the past (too far)

     
        $sec_rec = $DB->get_record('scheduler',array('id'=> $this->scheduler->id));
        if($CFG->is12HourFormat==1){
        	$data['starttime'] = getTimeAsPerTimeFormat($data['starttime'],$_REQUEST['timeformat']);
        }
        if($data['duration']==0){
        	$errors['duration'] = get_string('required');
        }
        //$mform->addRule('sessionname', get_string('required'), 'required', null, 'client');
        if($data['starttime'] <$sec_rec->startdate || $data['starttime'] > $sec_rec->enddate){
        	//$errors['starttime'] = 'Start Time should be between '.date('d M Y',$sec_rec->startdate).' and '.date('d M Y',$sec_rec->enddate);
        	$errors['starttime'] = 'Start Date should be between '.date('d M Y',$sec_rec->startdate).' and '.date('d M Y',$sec_rec->enddate);
        } 
        $end_time = endTimeByDuration($data['starttime'],$data['duration']);
        
        $schduler_slots = $sec_rec = $DB->get_records('scheduler_slots',array('schedulerid'=> $this->scheduler->id));
         foreach($schduler_slots as $slot){
         	$slot_endtime = endTimeByDuration($slot->starttime,$slot->duration);
         	if(($data['starttime'] >= $slot->starttime && $data['starttime'] < $slot_endtime) || ($end_time > $slot->starttime && $end_time <= $slot_endtime)){
         		if($this->slotid!=$slot->id)
         		{         		
         		$errors['starttime'] = 'Session already available on this time period';
         		break;
         		}
         	}
         }
        return $errors;
    }
}


class scheduler_addsession_form extends scheduler_slotform_base {

    protected function definition() {

        global $DB;

        $mform = $this->_form;

        // Start and end of range
        $mform->addElement('date_selector', 'rangestart', get_string('date', 'scheduler'));
        $mform->setDefault('rangestart', time());

        $mform->addElement('date_selector', 'rangeend', get_string('enddate', 'scheduler'));
        $mform->setDefault('rangeend', time());

        // Weekdays selection
        $weekdays = array('monday', 'tuesday', 'wednesday', 'thursday', 'friday');
        foreach ($weekdays as $day) {
            $label = ($day == 'monday') ? get_string('addondays', 'scheduler') : '';
            $mform->addElement('advcheckbox', $day, $label, get_string($day, 'scheduler'));
            $mform->setDefault($day, true);
        }
        $mform->addElement('advcheckbox', 'saturday', '', get_string('saturday', 'scheduler'));
        $mform->addElement('advcheckbox', 'sunday', '', get_string('sunday', 'scheduler'));

        // Start and end time
        $hours = array();
        $minutes = array();
        for ($i=0; $i<=23; $i++) {
            $hours[$i] = sprintf("%02d", $i);
        }
        for ($i=0; $i<60; $i+=5) {
            $minutes[$i] = sprintf("%02d", $i);
        }
        $starttimegroup = array();
        $starttimegroup[] = $mform->createElement('select', 'starthour', get_string('hour', 'form'), $hours);
        $starttimegroup[] = $mform->createElement('select', 'startminute', get_string('minute', 'form'), $minutes);
        $mform->addGroup ($starttimegroup, 'starttime', get_string('starttime', 'scheduler'), null, false);
        $endtimegroup = array();
        $endtimegroup[] = $mform->createElement('select', 'endhour', get_string('hour', 'form'), $hours);
        $endtimegroup[] = $mform->createElement('select', 'endminute', get_string('minute', 'form'), $minutes);
        $mform->addGroup ($endtimegroup, 'endtime', get_string('endtime', 'scheduler'), null, false);

        // Divide into slots?
        $mform->addElement('selectyesno', 'divide', get_string('divide', 'scheduler'));
        $mform->setDefault('divide', 1);

        // Duration of the slot
        $this->add_duration_field('minutesperslot');

        // Break between slots
        $this->add_minutes_field('break', 'break', 0, 'minutes');

        // Force when overlap?
        $mform->addElement('selectyesno', 'forcewhenoverlap', get_string('forcewhenoverlap', 'scheduler'));

        // Common fields
        $this->add_base_fields();

        // Display slot from date - relative
        $hideuntilsel = array();
        $hideuntilsel[0] =  get_string('now', 'scheduler');
        $hideuntilsel[DAYSECS] = get_string('onedaybefore', 'scheduler');
        for ($i = 2; $i < 7; $i++) {
            $hideuntilsel[DAYSECS*$i] = get_string('xdaysbefore', 'scheduler', $i);
        }
        $hideuntilsel[WEEKSECS] = get_string('oneweekbefore', 'scheduler');
        for ($i = 2; $i < 7; $i++) {
            $hideuntilsel[WEEKSECS*$i] = get_string('xweeksbefore', 'scheduler', $i);
        }
        $mform->addElement('select', 'hideuntilrel', get_string('displayfrom', 'scheduler'), $hideuntilsel);
        $mform->setDefault('hideuntilsel', 0);

        // E-mail reminder from
        $remindersel = array();
        $remindersel[-1] = get_string('never', 'scheduler');
        $remindersel[0] = get_string('onthemorningofappointment', 'scheduler');
        $remindersel[DAYSECS] = get_string('onedaybefore', 'scheduler');
        for ($i = 2; $i < 7; $i++) {
            $remindersel[DAYSECS * $i] = get_string('xdaysbefore', 'scheduler', $i);
        }
        $remindersel[WEEKSECS] = get_string('oneweekbefore', 'scheduler');
        for ($i = 2; $i < 7; $i++) {
            $remindersel[WEEKSECS*$i] = get_string('xweeksbefore', 'scheduler', $i);
        }

        $mform->addElement('select', 'emaildaterel', get_string('emailreminder', 'scheduler'), $remindersel);
        $mform->setDefault('remindersel', -1);

        $this->add_action_buttons();

    }

    public function validation($data, $files) {
        $errors = parent::validation($data, $files);

        // Range is negative
        $fordays = ($data['rangeend'] - $data['rangestart']) / DAYSECS;
        if ($fordays < 0) {
            $errors['rangeend'] = get_string('negativerange', 'scheduler');
        }

        // Time range is negative
        $starttime = $data['starthour']*60+$data['startminute'];
        $endtime = $data['endhour']*60+$data['endminute'];
        if ($starttime > $endtime)  {
            $errors['endtime'] = get_string('negativerange', 'scheduler');
        }

        // First slot is in the past
        if ($data['rangestart'] < time() - DAYSECS) {
            $errors['rangestart'] = get_string('startpast', 'scheduler');
        }

        // Break must be nonnegative
        if ($data['break'] < 0) {
            $errors['breakgroup'] = get_string('breaknotnegative', 'scheduler');
        }


        return $errors;
    }
}
