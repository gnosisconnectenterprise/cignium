<?PHP  

/**
 * This page prints a particular instance of scheduler and handles
 * top level interactions
 * 
 * @package    mod
 * @subpackage scheduler
 * @copyright  2011 Henning Bostelmann and others (see README.txt)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->dirroot.'/mod/scheduler/lib.php');
require_once($CFG->dirroot.'/mod/scheduler/locallib.php');


$id = optional_param('id', '', PARAM_INT);    // Course Module ID, or
$studentid = optional_param('studentid', '', PARAM_INT);    // Course Module ID, or
$file_container_id = $id.'_'.$studentid;




$PAGE->set_pagelayout('mydocfiles');

//$title = $course->shortname . ': ' . format_string($scheduler->name);
$PAGE->set_title($title);
$PAGE->set_heading($course->fullname);

echo $OUTPUT->header();
if(isset($_REQUEST['submit'])){

	$target =$CFG->dirroot."//".$CFG->documentUploadFolderName."/";
	$image_prefix = time();
	$file_name= $image_prefix.'_'.basename($_FILES ['document'] ['name']);
	 $target = $target . $file_name;

	$title = trim($_REQUEST['title']);
	if($_FILES ['document'] ['name']==""){
		$message =  "Please upload a file";
		$_SESSION ['update_msg'] = $message;
		$_SESSION ['error_class'] = 'error_bg';
	}
	else{
	if (move_uploaded_file ( $_FILES ['document'] ['tmp_name'], $target )) {
		saveAttendenceDocuments($_REQUEST['id'],$_REQUEST['studentid'],$_REQUEST['title'],$file_name,$_FILES ['document'] ['name']);
		
		$message =  "The file " . basename ( $_FILES ['document'] ['name'] ) . " has been uploaded.";
		$_SESSION ['update_msg'] = $message;
		$_SESSION ['error_class'] = 'success';
	} else {
		$message =  "Sorry, there was a problem uploading your file.";
		$_SESSION ['update_msg'] = $message;
		$_SESSION ['error_class'] = 'error_bg';
	}
	}
}
// teacher side
?>

<form method="post" action="#" enctype="multipart/form-data">
<input type="hidden" name="id" value="<?php echo $id?>" />
<input type="hidden" name="action" value="uploadDocuments" />

<input type="hidden" name="studentid" value="<?php echo $studentid?>" />
<table>
<?php

if ($_SESSION ['update_msg'] != '') {
	$courseHTML .= '<tr><td colspan="2"><div class="clear"></div>';
	$courseHTML .= '<div class="bg-success text-success text-center ' . $_SESSION ['error_class'] . '" style = "display:block !important;">' . $_SESSION ['update_msg'] . '</div>';
	$courseHTML .= '<div class="clear"></div></td></tr>';
	echo $courseHTML;
	$_SESSION ['update_msg'] = '';
}
?>
<tr>
	<td><?php echo get_string('title')?></td><td> <input type="text" name="title" /> </td></tr>
	<tr><td><?php echo get_string('uploadfile','scheduler')?> *</td><td><input type="file" name="document" /></td></tr>
	 <tr><td></td><td><input type="submit" name="submit"	value="<?php echo get_string('uploadfile','scheduler')?>" /></td></tr>
	 </table>
</form>
<?php 
    

  
echo $OUTPUT->footer($course);

?>

<script>
window.onunload = function() {
    if (window.opener && !window.opener.closed) {
        window.opener.popUpClosed('<?php echo $file_container_id;?>');
    }
};
</script>
