<?php
require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->dirroot.'/mod/scheduler/lib.php');
$elementList  = optional_param('elementList', false, PARAM_RAW);
$assignId  = optional_param('assignId', false, PARAM_RAW);
$scheduler = $DB->get_record('scheduler',array('id'=>$assignId));
$courses = new class_assignment_selector('', array('courseid' => $scheduler->id));
$PAGE->set_pagelayout('classroompopup');

echo $OUTPUT->header();
$class_details = getClassDetailsFromModuleId($assignId);
$headerHtml = getModuleHeaderHtml($class_details, $CFG->classModule);
$display = "";
if($elementList == $USER->id && $USER->archetype == $CFG->userTypeStudent){
	$display = "style='display:none;'";
}
$where = " AND u.id IN (".$elementList.")";

	$query = "SELECT u.id,CONCAT(u.firstname,' ', u.lastname,' (',u.username,')') AS name,'' as enddate,'' as namewithouttime FROM mdl_user as u  WHERE u.deleted = 0 ";
	$orderBy = "  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
	$query = $query.$where.$orderBy;
	
$optionListArray = $DB->get_records_sql($query);
$label = get_string('noofusers');

if(isset($_REQUEST['save'])){
	
	if((isset($_REQUEST['ismanagerappraoval']) && $_REQUEST['ismanagerappraoval']!=0) || $_REQUEST['ismanagerappraoval']=='on'){
		$ismanagerappraoval = 1;
	}
	else{
		$ismanagerappraoval = 0;
	}
	
	//echo $ismanagerappraoval  = optional_param('ismanagerappraoval', 0, PARAM_INT);
	//die;
	$enrol_users_str = $_REQUEST['elementList'];
	 $enrol_users = explode(',',$enrol_users_str);
	
	$courses->courseUserList();
	 $total_users =  count($courses->courseUserList)+count($enrol_users);
 	if($total_users>$scheduler->no_of_seats && $scheduler->no_of_seats>0){
		$_SESSION['update_msg'] = get_string('errornoofinvite','scheduler',$scheduler->no_of_seats);
		$_SESSION['error_class'] = 'error_bg';
	
	}
	else{
		
		//checkUsersWithNoOfSeats($scheduler->id,$enrol_users);
		assignUserToClass($scheduler->id,$enrol_users,$ismanagerappraoval);
		if($scheduler->enddate>time()){
			sendInvitation($scheduler->id,$enrol_users,$ismanagerappraoval);
		}
		$_SESSION['update_msg'] = get_string('userenrolledsuccess','scheduler',$scheduler->no_of_seats);
		$_SESSION['error_class'] = 'success';
		
		echo '<script> 
				parent.parent.top.$("#cboxLoadedContent").css("margin-top","43px").css("height","100%");
		parent.parent.top.$("#cboxTitle,#cboxClose").css("z-index","0");
				parent.window.location.reload();</script>';
		echo '<script> parent.$.colorbox.close();</script>';
	} 
	
	//redirect($CFG->wwwroot.'/course/index.php');
}
$courseHTML = "";
if ($_SESSION ['update_msg'] != '') {
	$courseHTML .= "<div class='tabsOuter'>";
	$courseHTML .= '<div class="clear"></div>';
	$courseHTML .= '<div class="bg-success text-success text-center ' . $_SESSION ['error_class'] . '" style = "display:block !important;">' . $_SESSION ['update_msg'] . '</div>';
	$courseHTML .= '<div class="clear"></div>';
	$courseHTML .= "</div>";
	//$_SESSION ['update_msg'] = '';
}
echo $courseHTML;
?>

<form id="assignform" method="post" action="">
<div class = "enrol-main">
	<div class = "row heading">
		<?php echo $headerHtml; ?>
	</div>
	<div class="row_outer">
	<div class = "row row1" <?php echo $display;?>>
		<label for = "element_list"><?php echo  get_string('noofusers');?></label>
		<span class = "element">
			<select class = "elements" id = "element_list" name = "adduser" multiple disabled>
			<?php 
			if(!empty($optionListArray)){
				foreach($optionListArray as $optionList){?>
					<option value = "<?php echo $optionList->id; ?>" enddate="<?php echo $optionList->enddate; ?>"><?php echo $optionList->name; ?></option>
				<?php
				}
			}
			?>
			<select>
		</span>
	</div>
	<?php if( $scheduler->enddate>time()){ 
	echo '<div class = "row row2">
		<label>&nbsp;</label>
		<span class = "element">
			<input type="checkbox" name="ismanagerappraoval" id="ismanagerappraoval" />
			<label for = "ismanagerappraoval">'.get_string("skipmanagerapproval","scheduler").'</label>
		</span>
	</div>';
	 }
	 else if($CFG->isClassAllowForPastDate==1){
	 	echo '<input type="hidden" name="ismanagerappraoval" value="1" />';
	 	echo '<div class = "row row2">
		<span class = "element" style="padding-left:100px; display:block">Note : '.get_string("enddatepasedtext","scheduler").'</span>
	</div>';
	 	
	 	}
	 	else{
	 		echo '<input type="hidden" name="ismanagerappraoval" value="0" />';
	 	}
	 
	  ?>	
	<div class = "row row3">
	
		<input type="button" name="cancel" id="cancel_data" value="cancel">
			<input type="submit" name="save" id="save_data" value="save">
	</div>
	</div>
</div>
</form>

<link href='<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/css/jquery-ui.css' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-ui.min.js"></script>
<script>
$(document).ready(function(){


	$("#cancel_data").click(function(){
		parent.parent.top.$('#cboxLoadedContent').css('margin-top','43px').css('height','100%');
		parent.parent.top.$('#cboxTitle,#cboxClose').css('z-index','0');
		parent.$.colorbox.close();
	});
	$("#save_data").click(function(){
		
		
		var ismanagerappraoval = $("#ismanagerappraoval").val();
		var elementList = "";
		$("#element_list").find('option').each(function(){
			elementList += $(this).val()+',';
		});
		elementList = elementList.substr(0, elementList.length - 1);
	
		
				if(elementList != ""){
					$("#element_list").attr('disable',false);
					return true;
				}else{
					
					alert('User not fond for enrolment');
				}
				return false;
		
	});
});




</script>

<?php
echo $OUTPUT->footer();
?>