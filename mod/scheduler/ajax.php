<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file processes AJAX enrolment actions and returns JSON
 *
 * The general idea behind this file is that any errors should throw exceptions
 * which will be returned and acted upon by the calling AJAX script.
 *
 * @package    core_enrol
 * @copyright  2010 Sam Hemelryk
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
	* Common module - All Ajax Hits at this page
	* Date Creation - 01/06/2014
	* Date Modification : 27/10/2014
	* Last Modified By : Bhavana Kadwal
*/

//define('AJAX_SCRIPT', true);

require('../../config.php');

global $USER,$DB,$CFG;
require_once($CFG->dirroot.'/mod/scheduler/lib.php');
// Must have the sesskey

$action  = required_param('action', PARAM_ALPHANUMEXT);

checkLogin();
$outcome = new stdClass();
$outcome->success = true;
$outcome->response = new stdClass();
$outcome->error = '';


switch ($action) {
  

	case 'getNonUserList':
		GLOBAL $USER;
		$id = required_param('id', PARAM_INT);
		$classId =required_param('classid', PARAM_INT);
		$courses = new class_assignment_selector('', array('courseid' => $id));
		$departmentId = required_param('departmentId', PARAM_RAW);
		
		$job_title = optional_param('job_title', '-1', PARAM_RAW);
		$company = optional_param('company', '-1', PARAM_RAW);
		
		$courses->courseUserList();
		$courses->courseNonUserList($classId, $departmentId, 0, $job_title, $company);
		$courses->classNonUserList();
		//$courses->course_option_list('non_user_course'); 
		die;
		break;	
	
	case 'enrolUserToCourse':
		GLOBAL $USER;
		$courseId = required_param('courseId', PARAM_RAW);
		$enrolId  = CheckCourseEnrolment($courseId);
		$enrolledId = enrolUserToCourse($enrolId,$USER->id,$courseId);
		$userMapId = assignProgramCourseToUsers(0,$courseId,$USER->id,'user',$USER->id);
		break;	
	case 'getDepartmentTeamList':
		GLOBAL $USER;
		$departmentId = required_param('departmentId', PARAM_RAW);
		$departmentTeams = getDepartmentTeams($departmentId);
		$option = '<option value = "0">'.get_string('select_team').'</option>';
		if(!empty($departmentTeams)){
			foreach($departmentTeams as $departmentTeam){
				$option .= '<option value = "'.$departmentTeam->id.'" '.$selected.'>'.$departmentTeam->name.'</option>';
			}
		}
		echo $option;die;
		break;
	case 'getTeamUserData':
		GLOBAL $USER;
		$id = required_param('id', PARAM_INT);
		$classId =required_param('classid', PARAM_INT);
		$courses = new class_assignment_selector('', array('courseid' => $id));
		$departmentId = required_param('departmentId', PARAM_RAW);
		$teamId = required_param('teamId', PARAM_RAW);
		$courses->courseUserList();
		$courses->courseNonUserList($classId,$departmentId,$teamId);
		$courses->classNonUserList();
		//$courses->course_option_list('non_user_course'); 
		die;
		break;	
		
		case 'deleteClass' :
			global $CFG,$DB;
			require_once($CFG->dirroot.'/course/lib.php');
			$classId_module_id =required_param('classid', PARAM_INT);
			$classDetails = $DB->get_record_sql("SELECT s.teacher,s.id,s.enddate FROM mdl_course_modules AS cm LEFT JOIN mdl_scheduler as s ON cm.instance = s.id WHERE cm.id = ".$classId_module_id);
			if(!empty($classDetails) && $classDetails->enddate > time()){
				emailForClassAddUpdateToTeacher($classDetails->teacher,$classDetails->id,'delete');
			}
			add_to_log(1, 'scheduler', 'Delete Class', "Delete Class", $classId_module_id, $classId_module_id);
			course_delete_module($classId_module_id);
			$_SESSION['update_msg'] =  get_string('classdeletesuccess','scheduler');;
			$_SESSION['error_class'] = 'success';
			echo "success";die;
			break;
		
		case 'changeClassStatus' :
			global $CFG;
			$classId =required_param('classid', PARAM_INT);
			$status =required_param('status', PARAM_INT);
			$action_text = "Class ".$classId." Status Changed ".$status;
			add_to_log(1, 'scheduler',$action_text, $action_text, $classId, $classId);
			activateDeactivateClassroom($classId,$status);
			if($status==1){
				$okTxt = get_string('classpublishedsuccess','scheduler');
			}
			else{
				$okTxt = get_string('classunpublishedsuccess','scheduler');
			}
			$_SESSION['update_msg'] = $okTxt;
			$_SESSION['error_class'] = 'success';
			echo "success";die;
			break;
		case 'changeSessionStatus' :
			global $CFG;
			$sessionId =required_param('session', PARAM_INT);
			$status =required_param('status', PARAM_INT);
			$action_text = "Session ".$sessionId." Status Changed ".$status;
			//add_to_log(1, 'scheduler','', "Session ".$sessionId." Status Changed ".$status, $sessionId, $classId);
			add_to_log(1, 'scheduler',$action_text, $action_text, $sessionId, $sessionId);
			activateDeactivateSession($sessionId,$status);
			if($status==1){
				$okTxt = get_string('sessionpublishedsuccess','scheduler');
			}
			else{
				$okTxt = get_string('sessionunpublishedsuccess','scheduler');
			}
			$_SESSION['update_msg'] = $okTxt;
			$_SESSION['error_class'] = 'success';
			echo "success";die;
			break;
					
		case 'deleteSession' :
				global $CFG;
				$slotid =required_param('slotid', PARAM_INT);
				add_to_log(1, 'scheduler','', "Session ".$slotid." Deleted ".$status, $slotid, $slotid);
				$DB->delete_records('scheduler_slots',array('id'=>$slotid));
				$DB->delete_records('event',array('sessionid'=>$slotid));
				$_SESSION['update_msg'] = get_string('sessiondeletesuccess','scheduler');
				$_SESSION['error_class'] = 'success';
				echo "success";die;
				break;
				
		case 'sendInvite' :
				global $CFG;
				require_once($CFG->dirroot.'/course/lib.php');
				$classId =required_param('classid', PARAM_INT);
				add_to_log(1, 'scheduler', 'send invite ', "Delete Class", $classId_module_id, $classId_module_id);
				getAllOpenClassUsers($classId);
						
				echo "success";die;
				break;
		case 'deleteDocument' :
			global $CFG;
			require_once ($CFG->dirroot . '/mod/scheduler/lib.php');
			$documentid = optional_param ( 'documentid', 0, PARAM_INT );
			deleteAttendenceDocument ( $documentid );		
			echo "success";die;
			break;
		case 'getDocumentWithoutRefresh' :
				global $CFG;
				require_once ($CFG->dirroot . '/mod/scheduler/lib.php');
				 $filecontainer_id = optional_param ( 'filecontainer_id', '', PARAM_TEXT );
				$class_user_id = explode('_',$filecontainer_id);
				$allFiles = getAllFiles ( $class_user_id[0],$class_user_id[1] );
				foreach ( $allFiles as $file ) {
					$title = $file->title;
					if($title==""){
						$title = $file->original_file_name;
					}
					//$courseHTML .= "<div class='file_div_".$file->id."'><a class='upfile_name' href='" . $CFG->wwwroot . "/upload/" . $file->file_name . "' target='_blank'>" . $title. "</a> <a class='upfile_delete' href='" . $CFG->wwwroot . "/mod/scheduler/learner_grade.php?id=" . $id . "&documentid=" . $file->id . "&delete=1' rel='".$file->id."' >Delete</a></div>";
					$courseHTML .= "<div class='file_div_".$file->id."'><a class='upfile_name' href='" . $CFG->wwwroot . "/upload/" . $file->file_name . "' target='_blank'>" . $title. "</a> <a class='upfile_delete'  rel='".$file->id."' >Delete</a></div>";
				}
				echo $courseHTML;die;
				break;
		case 'getSessionWithoutRefresh' :
			global $CFG;
			
			// $schedulerId = optional_param ( 'classModuleId', '', PARAM_TEXT );
			$classModuleId = optional_param ( 'classModuleId', '', PARAM_TEXT );
			$classModuleId_arr = explode('_',$classModuleId);
			$schedulerId = $classModuleId_arr[0];
		 $moduleId = $classModuleId_arr[1];
		 $class_module_id = $schedulerId.'_'.$moduleId;
		 $countUser = getCountOfClassUser($schedulerId);
			 $session_list = classSessionList($moduleId);
			 $courseHTML =  "<table class = 'table1 ' class='expandclass".$schedulerId."'><tr>";
			 $courseHTML .=  "<td width = '20%' align='align_left' >".get_string('sessionname','scheduler')."</td>";
			 //$courseHTML .=  "<td width = '22%' align='align_left' >Description</td>";
			 $courseHTML .=  "<td width = '60%' align='align_left' >".get_string('duration_date')."</td>";
			 //$tableHtml .= "<td width = '15%' align='align_left' >$createdby</td>";
			 $courseHTML .=  "<td width = '20%' align='align_left' >".get_string('manage')."</td>";
			 $courseHTML .=  '</tr>';
			 if (!$session_list) {
			 	$courseHTML .=  "<tr><td colspan = '3'>".get_string('no_results')."</td></tr>";
			 	$table = NULL;
			 } else {
			 	$returnurl = $CFG->wwwroot.'/mod/scheduler/view.php';
			 	foreach ($session_list as $session) {
			 			
			 			
			 		$buttons = array();
			 		$edit_session_url  = html_writer::link(new moodle_url($returnurl, array('what'=>'updateslot', 'id'=>$moduleId,'slotid'=>$session->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>get_string('edit'), 'class'=>'iconsmall ')), array('title'=>get_string('edit'), 'class'=>'edit session-popup','classid'=>$class_module_id));
			 		$delete_session_url = '<a href="" class="delete delete_session" title = "'.get_string('delete').'" rel="'.$session->id.'">'.get_string('delete').'</a>';
			 		$disable_delete_session_url = '<a href="javascript:void(0)" class="delete-disabled" title = "'.get_string('delete').'" rel="'.$session->id.'">'.get_string('delete').'</a>';
			 		
			 				
			 			$buttons[] = $edit_session_url;
			 
			 			if($countUser->total_user==0){
			 				$buttons[] = $delete_session_url;
			 			}
			 			else{
			 				if($delete_session_disabled){
			 					$buttons[] = $disable_delete_session_url;
			 				}
			 				else{
			 					$buttons[] = $delete_session_url;
			 				}
			 			}
			 
			 	
			 		$courseHTML .= '<td class="align_left"><span class="f-left">'.$session->sessionname.'</span></td>';
			 			
			 
			 			
			 		$end_time =endTimeByDuration($session->starttime,$session->duration);
			 		$duration_text = date('M d, Y h:i A',$session->starttime).' to '.date('h:i A',$end_time);
			 		$courseHTML .=  "<td>".$duration_text."</td>";
			 		$courseHTML .=  "<td>".implode(' ', $buttons)."</td>";
			 		$courseHTML .=  "</tr>";
			 	}
			 }
			 $courseHTML .=  '</table>';
			 echo $courseHTML; 
			die;
			case 'saveClassData':
				GLOBAL $USER,$CFG,$DB;
				$classId =required_param('classId', PARAM_INT);
				 $list =  optional_param('list',0, PARAM_RAW);
				$type = required_param('type', PARAM_ALPHA);
				$list_arr = explode(',',$list);
				$actionType = required_param('actionType', PARAM_ALPHA);
				enrolIntoOpenCourse($classId,$list_arr,$type,$actionType);
				break;
    default:
        throw new enrol_ajax_exception('unknowajaxaction');
}

echo json_encode($outcome);
die();