<?php
	require_once('../config.php');	
	global $CFG;
	require_once($CFG->libdir.'/formslib.php');
	require_once($CFG->pageDepartmentlib);
	
	class classroomsettings_form extends moodleform{
		//This method contains definition of the form i.e. all the elements to be displayed
		function definition() {
			//Defining global objects User and Configuration		
			global $USER, $CFG;
			//Form object
			$mform =& $this->_form;
			//data received to populate this form   
			$adddepartmentdata = $this->_customdata;
			
			if (!isset($adddepartmentdata->id)){ 
				$editoroptions = $this->_customdata['editoroptions'];
			}
			else{
				$editoroptions = $adddepartmentdata->editoroptions;
			}
			// Start Print department image if exists
			/*if(!empty($adddepartmentdata->id)){
				if($adddepartmentdata->picture != ''){
					$view =  $CFG->departmentDefaultImagePath.$adddepartmentdata->picture;
					$view1 =  $CFG->departmentDefaultImageURL.$adddepartmentdata->picture;
					if(file_exists($view) && $adddepartmentdata->picture !=''){
						$mform->addElement('html','<div class="fitem"><div class="fitemtitle"><label for="id_image">'.get_string("current_image").'</label></div>');
						$mform->addElement('html','<div class="felement fstatic"><a href="#"><img width="64" height="64" class="userpicture defaultuserpic" src="'.$view1.'"></a>');
						$mform->addElement('html','<a class = "removeimage" rel = "'.$adddepartmentdata->id.'" title = "'.get_string('delete').'"><span id="'.$adddepartmentdata->id.'" rel="login">'.get_string("delete").'</span></a>');
						$mform->addElement('html','</div></div>');
					}
				}
			}*/
			// End Print department image if exists

			$mform->addElement('text','title', get_string('department_name','plugin'),array('maxlength'=>100));   
			$mform->setDefault('title', $adddepartmentdata->title);       
			$mform->addRule('title', get_string('valid_title','department'), 'required', null, 'server');
			
			$test = array("text"=>$adddepartmentdata->description, 'format'=>1);
			
			$mform->addElement('editor','description', get_string('department_description','plugin'), null, $editoroptions);
			$mform->setDefault('description', $test);  
			$mform->setType('description', PARAM_RAW);

			//$mform->addElement('file','department_image', get_string('department_image', 'plugin'));
			//$mform->addElement('html','<div class="fitem"><div class="fl italicmsg">'.get_string("department_image_size", 'plugin').'</div></div>');
			if ($filesOptions = programOverviewFilesOptions($programData)) {
				$mform->addElement('filemanager', 'testimage_filemanager', get_string('department_image', 'plugin'), null, $filesOptions);
			}		
			$showResetButton = true;
			if (isset($adddepartmentdata->id)) {
				$mform->addElement('hidden','id');
				$mform->setDefault('id', $adddepartmentdata->id);
				
				$mform->addElement('hidden','createdby');
				$mform->setDefault('createdby', $adddepartmentdata->createdby);
				$showResetButton = false;
			}
			
			$this->add_action_buttons(true, get_string('savechanges'), $showResetButton);
		}
		function validation($data){ 
			global $USER, $CFG,$DB;
			$errors = array();
			
			
			return $errors;
		}
	}
?>