<?PHP  

/**
 * This page prints a particular instance of scheduler and handles
 * top level interactions
 * 
 * @package    mod
 * @subpackage scheduler
 * @copyright  2011 Henning Bostelmann and others (see README.txt)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->dirroot.'/mod/scheduler/lib.php');
require_once($CFG->dirroot.'/mod/scheduler/locallib.php');
$id = required_param('id', PARAM_INT);

$courseHTML = '';
//$title = $course->shortname . ': ' . format_string($scheduler->name);
$PAGE->set_title($title);
$PAGE->set_heading($course->fullname);
require_login();
$scheduler_enrollment = $DB->get_record('scheduler_enrollment',array('id'=>$id));
if($scheduler_enrollment){
	$all_users = getUserManager();
	$courseHTML .=  $all_users[$scheduler_enrollment->userid]->usermanagerid;
	if($all_users[$scheduler_enrollment->userid]->usermanagerid==$USER->id){
		if($scheduler_enrollment->is_approved==1){
			$courseHTML .=  'Already Approved';
		}
		else{
			$update_rec = new stdClass();
			$update_rec->id = $id;
			$update_rec->is_approved =1;
			$update_rec->action_taken_by = $USER->id;
			$update_rec->timemodified = time();
			$DB->update_record('scheduler_enrollment',$update_rec);
			$courseHTML .=  get_string('successapproved','scheduler');
		}
	}
	else{
		$courseHTML .=  'Access Denied';
	}
}
else{
	$courseHTML .=  'Invalid Access';
}


echo $OUTPUT->header();



$courseHTML .=  "<div class='tabsOuter'>";

$courseHTML .= '<div class="clear"></div><div class="userprofile view_assests">';

//$getDepartmentUsers = getDepartmentUsers($USER->department);

    
  if (!empty($courseHTML)) {
		echo '<div class = "course-listing">';
		
        echo html_writer::start_tag('div', array('class'=>'no-overflow'));
        echo $courseHTML;
        echo html_writer::end_tag('div');
        echo html_writer::end_tag('div');

		//Pring paging bar
        echo paging_bar($programCount, $page, $perpage, $genURL);
    }
    echo "</div>";
echo $OUTPUT->footer($course);

?>
