<?php

/**
 * Defines the scheduler module settings form.
 * 
 * @package    mod
 * @subpackage scheduler
 * @copyright  2011 Henning Bostelmann and others (see README.txt)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/course/moodleform_mod.php');
require_once($CFG->dirroot.'/mod/scheduler/lib.php');

//require_once($CFG->dirroot . '/mod/scheduler/locallib.php');

/**
* overrides moodleform for test setup
*/
class mod_scheduler_mod_form extends moodleform_mod {

	function definition() {

	    global $CFG, $COURSE, $OUTPUT;
	    $mform    =& $this->_form;
	    $this->current->pre_requisite = array('text'=>$this->current->pre_requisite);
	   
	    $scheduler_info = & $this->current;
	   
	  	$mform->addElement('html','<div class="filedset_outer ">');
	    $mform->addElement('header','general', get_string('general', 'form'));
	    $mform->addElement('text', 'name', get_string('name'), array('size'=>'64'));
	    $mform->setType('name', PARAM_CLEANHTML);
	    $mform->addRule('name', get_string('required'), 'required', null, 'client');

        // Introduction.
	    $instructorArr[''][''] = get_string('selectinstructor');
	    $instructorList = getInstructor();
	    if(count($instructorList) >0 ){
	    	foreach($instructorList as $arr){
	    		$roleIdArr = getUserRoleDetails($arr->id);
	    		$roleName = $roleIdArr->name;
	    		if($roleName){
	    			$roleName = ucfirst($roleName);
	    			$instructorArr[$roleName][$arr->id] = $arr->fullname;
	    		}
	    	}
	    }
	
	    $mform->addElement('selectgroups', 'teacher', get_string('instructor','scheduler'), $instructorArr, array('style'=>'min-width:200px'));
	   // $mform->addHelpButton('teacher', 'secondaryinstructor');
	    $mform->addRule('teacher', get_string('required'), 'required', null, 'client');
	    $mform->setDefault('teacher',$COURSE->primary_instructor);
	    
	     
        $mform->addElement('date_selector', 'startdate', get_string('startscheduledate', 'scheduler'));
        $mform->addElement('date_selector', 'enddate', get_string('endscheduledate', 'scheduler'));
        
        $mform->addElement('header', 'settings', get_string('settings'));
        $mform->addElement('text', 'no_of_seats', get_string('no_of_seats', 'scheduler'), array('size'=>'2'));
        
        $mform->setType('no_of_seats', PARAM_INT);
         
        //  $mform->setDefault('no_of_seats', 10);
        
        
        
     //   $mform->addElement('text', 'location', get_string('location', 'scheduler'), array('size'=>'2'));
   //    $mform->setType('location', PARAM_CLEANHTML);
         
        
     //   $this->add_intro_editor(false, get_string('webaccesslink', 'scheduler'));
        
        $mform->addElement('editor', 'pre_requisite', get_string('pre_requisite', 'scheduler'));
        $mform->setType('pre_requisite', PARAM_RAW);
        $enroloptions[1] =  get_string('inviteenroloption', 'scheduler');
        $enroloptions[0] =  get_string('openenroloption', 'scheduler');
        $mform->addElement('select', 'enrolmenttype', get_string('enroloptions', 'scheduler'), $enroloptions);
       if($scheduler_info->instance){
       	$total_users = getCountOfClassUser($scheduler_info->instance);
       	if($total_users->total_user>0){
       	//	$mform->hardFreeze('startdate');
       	//	$mform->hardFreeze('enddate');
       		$mform->hardFreeze('enrolmenttype');
       		$mform->setConstants('enrolmenttype', $scheduler_info->enrolmenttype);
       	//	$mform->setConstants('startdate', $scheduler_info->startdate);
       	//	$mform->setConstants('enddate', $scheduler_info->enddate);
       	}
       	
       
       }
        
        $style = "";
		$display = '';
        $departStyle = "";
     
       if(!$scheduler_info->id || $scheduler_info->enrolmenttype==1){
      	$style = "display:none";
		$display = "style = 'display:none;'";
      	$open_for_checked = 1;
      }
      else{
      	$open_for_checked = $scheduler_info->open_for;
      }
        if($scheduler_info->enrolmenttype==1)
        {
        	
        	$style = "display:none";
			$display = "style = 'display:none;'";
        }
        else{
        	if($scheduler->open_for==1){
        		$departStyle = "display:none";
        	}
        }
        $disabled_open_invitation = "";       
       if($scheduler_info->isclasscompleted==1){
       	$disabled_open_invitation = 'disabled="disabled"';
       }
        $open_for_html =  "<div id='open_for' style='".$style."'> <input type='checkbox' id='check_open_for' name='open_for'  ".is_checked($open_for_checked,1,'checkbox')." onclick='showDepartment(this.value,\"open_for\")'  ".$disabled_open_invitation."/> ".get_string('openforall','scheduler')."</div>";
      //  $mform->addElement('html', $open_for_html);
        $assignUsersHtml = assignUsersHtml1($scheduler_info->id,$style,$disabled_open_invitation, $display);
		
       // $mform->addElement('html', $assignUsersHtml); 
        
      
        
        
      
   
    //   $mform->addElement('textarea', 'pre_requisite', get_string("pre_requisite", "scheduler"), 'wrap="virtual" rows="10" cols="50"');
        
        $mform->addHelpButton('schedulermode', 'appointmentmode', 'scheduler');
	    $mform->addElement('text', 'staffrolename', get_string('staffrolename', 'scheduler'), array('size'=>'48'));
	    $mform->setType('staffrolename', PARAM_CLEANHTML);
	    $mform->addHelpButton('staffrolename', 'staffrolename', 'scheduler');
	
	    $modeoptions['onetime'] = get_string('oneatatime', 'scheduler');
	    $modeoptions['oneonly'] = get_string('oneappointmentonly', 'scheduler');
	    $mform->addElement('select', 'schedulermode', get_string('mode', 'scheduler'), $modeoptions);
	    $mform->addHelpButton('schedulermode', 'appointmentmode', 'scheduler');

	    $reuseguardoptions[24] = 24 . ' ' . get_string('hours');
	    $reuseguardoptions[48] = 48 . ' ' . get_string('hours');
	    $reuseguardoptions[72] = 72 . ' ' . get_string('hours');
	    $reuseguardoptions[96] = 96 . ' ' . get_string('hours');
	    $reuseguardoptions[168] = 168 . ' ' . get_string('hours');
	    $mform->addElement('select', 'reuseguardtime', get_string('reuseguardtime', 'scheduler'), $reuseguardoptions);
	    $mform->addHelpButton('reuseguardtime', 'reuseguardtime', 'scheduler');

	    $mform->addElement('text', 'defaultslotduration', get_string('defaultslotduration', 'scheduler'), array('size'=>'2'));
	    $mform->setType('defaultslotduration', PARAM_INT);
	    $mform->addHelpButton('defaultslotduration', 'defaultslotduration', 'scheduler');
        $mform->setDefault('defaultslotduration', 60); 

        $mform->addElement('modgrade', 'scale', get_string('grade'));
        $mform->setDefault('scale', 100);
        
        $gradingstrategy[MAX_GRADE] = get_string('maxgrade', 'scheduler');
        $gradingstrategy[MEAN_GRADE] = get_string('meangrade', 'scheduler');
  
	    $mform->addElement('select', 'gradingstrategy', get_string('gradingstrategy', 'scheduler'), $gradingstrategy);
	    $mform->addHelpButton('gradingstrategy', 'gradingstrategy', 'scheduler');
        $mform->disabledIf('gradingstrategy', 'scale', 'eq', 0);

        $yesno[0] = get_string('no');
        $yesno[1] = get_string('yes');
	    $mform->addElement('select', 'allownotifications', get_string('notifications', 'scheduler'), $yesno);
	    $mform->addHelpButton('allownotifications', 'notifications', 'scheduler');

		// Legacy. This field is still in the DB but is meaningless, meanwhile.
	 
	    $mform->addElement('hidden', 'class',1);
	    
	
	     
        $this->standard_coursemodule_elements();
        $buttonarray=array();
        $buttonarray[] = &$mform->createElement('reset', 'resetbutton', get_string('reset'));
        $buttonarray[] = &$mform->createElement('cancel');
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('savechanges'));
        $sendinvite_oprions = array();
        
       
        if($scheduler_info->enrolmenttype==1){
        	$sendinvite_oprions['style'] = 'display:none';
        }
        else{
        	if($scheduler_info->open_for!=1){
        		$sendinvite_oprions =  array('disabled'=>'disabled');
        	}
        	
        }
        if(!isSessionAvailable($scheduler_info->id))
        {
        	$sendinvite_oprions['rel'] ='disabled';
        	$sendinvite_oprions['title'] =get_string('classcannotassign','scheduler');
        }
        else{
        	$sendinvite_oprions['rel'] ='enabled';
        }
       
     //   $buttonarray[] = &$mform->createElement('submit', 'sendinvite', get_string('sendinviteopen','scheduler'),$sendinvite_oprions);
        
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
		$mform->addElement('html','</div>');
//         /$this->add_action_buttons(true,get_string('savechanges'),false);
        //$this->add_action_buttons();
    }
    
    function validation($data, $files) {
    	global $DB,$CFG;
   
    	$errors = parent::validation($data, $files);
  
    	$curtime = time();
    	
    	
    	$startdate = $data['startdate']+(60*60*24);
  		$enddate= $data['enddate']+(60*60*24);
  		
    	if(!$data['instance']){
    		if($CFG->isClassAllowForPastDate==0){
	    		if(isset($data['startdate']) && $data['startdate']!='' && $startdate < $curtime){
	    			$errors['startdate'] = get_string('startdate_can_not_be_less_than_current_date', 'scheduler');
	    		}
    		}
    	if (isClassAlreadyExist(trim($data['name']),trim($data['course']))) {
						$errors['name'] = get_string('class_title_already_exist', 'scheduler', $data['name']);
					}
					if($data['enrolmenttype']==0 && $CFG->isClassAllowForPastDate==1){
					
						if(isset($data['startdate']) && $data['startdate']!='' && $startdate < $curtime){
							$errors['startdate'] = get_string('startdate_can_not_be_less_than_current_date_in_openclass', 'scheduler');
						}
					}
		
    	}
    	else{
    		if (isClassAlreadyExist(trim($data['name']),trim($data['course']),$data['instance'])) {
    			$errors['name'] = get_string('class_title_already_exist', 'scheduler', $data['name']);
    		}
    	}
    		if(isset($data['enddate']) && $data['enddate']!='' && $data['enddate'] < $data['startdate']){
    			$errors['enddate'] = get_string('enddate_can_not_be_less_than_start_date', 'scheduler');
    		}
    		
    	if($data['instance']){
    			$scheduler_slots = $DB->get_records('scheduler_slots',array('schedulerid'=>$data['instance']));
    		
    			if(count($scheduler_slots)>0){
    				foreach($scheduler_slots as $scheduler_slot){
    					$session_start =$scheduler_slot->starttime;
    					$session_end = endTimeByDuration($session_start,$scheduler_slot->duration);
    				
    					if(($session_start<$data['startdate'] || $session_start>$enddate) || ($session_end<$data['startdate'] || $session_start>$enddate)){
    						$errors['startdate'] = get_string('session_conflict_while_class_update', 'scheduler');
    						break;
    					}
    				}
    			}
	    		$scheduler_rec = $DB->get_record('scheduler',array('id'=>$data['instance'])); 
	    		
	    		if($CFG->isClassAllowForPastDate==0)
	    		{
	    			if($scheduler_rec->startdate !=$data['startdate'] && isset($data['startdate']) && $data['startdate']!='' && $startdate < $curtime){
		    			$errors['startdate'] = get_string('startdate_can_not_be_less_than_current_date', 'scheduler');
		    		}
		    		if($scheduler_rec->enddate !=$data['enddate'] && isset($data['enddate']) && $data['enddate']!='' && $enddate < $curtime){
		    			$errors['enddate'] = get_string('enddate_can_not_be_less_than_current_date', 'scheduler');
		    		}
	    		}
	    		if($data['enrolmenttype']==0){
	    				
	    			if($scheduler_rec->startdate !=$data['startdate'] && isset($data['startdate']) && $data['startdate']!='' && $startdate < $curtime){
	    				$errors['startdate'] = get_string('startdate_can_not_be_less_than_current_date_in_openclass', 'scheduler');
	    			}
	    			
	    			if($scheduler_rec->enddate !=($data['enddate']+86399) && isset($data['enddate']) && $data['enddate']!='' && $enddate < $curtime){
	    				$errors['enddate'] = get_string('enddate_can_not_be_less_than_current_date', 'scheduler');
	    			}
	    		}
	    		
    			$query = "SELECT userid FROM {$CFG->prefix}scheduler_enrollment WHERE scheduler_id = ". $data['instance']." AND is_approved IN(0,1)";
    			$courseUserList = $DB->get_records_sql ( $query );
 
    			 $total_users =  count($courseUserList);
    			$enrolled_users = array_keys($courseUserList);
    			
    			if(in_array($data['teacher'],$enrolled_users)){
    				$errors['teacher'] = get_string('instructor_already_part_of_class_as_learner', 'scheduler',$total_users);
    			}
    			
    			if($data['no_of_seats']>0 && $data['no_of_seats']<$total_users){
    				$errors['no_of_seats'] = get_string('no_of_seats_no_less_then_invited_users', 'scheduler',$total_users);
    			}
    		}
    	
    	// Add field validation check for duplicate shortname.
    
    
   
    
    
    	//pr($errors);die;
    	return $errors;
    }

}

?>

