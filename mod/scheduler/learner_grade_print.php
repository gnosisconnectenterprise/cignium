<?PHP

/**
 * This page prints a particular instance of scheduler and handles
 * top level interactions
 *
 * @package mod
 * @subpackage scheduler
 * @copyright 2011 Henning Bostelmann and others (see README.txt)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once (dirname ( __FILE__ ) . '/../../config.php');
require_once ($CFG->dirroot . '/mod/scheduler/lib.php');
require_once ($CFG->dirroot . '/mod/scheduler/locallib.php');

$id = required_param ( 'id', PARAM_INT ); // Course Module ID, or

if ($id) {
	if (! $cm = get_coursemodule_from_id ( 'scheduler', $id )) {
		print_error ( 'invalidcoursemodule' );
	}
	
	if (! $course = $DB->get_record ( 'course', array (
			'id' => $cm->course 
	) )) {
		print_error ( 'coursemisconf' );
	}
	
	if (! $scheduler = $DB->get_record ( 'scheduler', array (
			'id' => $cm->instance 
	) )) {
		print_error ( 'invalidcoursemodule' );
	}
}
$session_list = classSessionList ( $id );
$learner_list = classLearnerList ( $id );
$class_details = getClassDetailsFromModuleId($scheduler->id);
// $courseTypeIdInTab = getCourseTypeIdByCourseId($course->id);
checkUserAccess ( 'class', $scheduler->id );

$PAGE->set_url ( '/mod/scheduler/learner_grade.php', array (
		'id' => $id 
) );
$PAGE->set_pagelayout ( 'classroompopup' );





// $title = $course->shortname . ': ' . format_string($scheduler->name);
$PAGE->set_title ( $title );
$PAGE->set_heading ( $course->fullname );

echo $OUTPUT->header ();
$courseHTML = '';

$courseHTML .= "<div class='borderBlockSpace attendance-performance'>";

$headerHtml = getModuleHeaderHtml($class_details, $CFG->classModule);
$courseHTML .= $headerHtml;
$grade_rec = getGradeData ();
$courseHTML .= '<div class="clear"></div>';
if (! $learner_list || !$session_list) {
$courseHTML .= get_string('norecordfound','scheduler');
}
else{
// / route to screen

$courseHTML .= '<div id="">';

$courseHTML .= '<table class = "table1" id="table1">';

$courseHTML .= '<tr class="headingRow"><td width="14%">Name</td><td width="10%" class="bor_left">&nbsp;</td>';
if($session_list){
$courseHTML .= '';
}
$courseHTML .= '<td width="35%" style="padding:0"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="margin_none">';
if(count($session_list)>=1){
	$courseHTML .= '<tr><td class="bor_left" style="text-align:center; border-bottom:1px solid #b9b9b9" colspan="'.count($session_list).'">'.get_string("sessionstext","scheduler").'</td></tr>';		
}
$courseHTML .= '<tr>';
foreach ( $session_list as $session ) {

    $sessionDuration = getClassroomSessionDateDuration($session->starttime, $session->duration); 
	$sessionName = $session->sessionname."<br>(". $sessionDuration.")";
	$courseHTML .= "<td width='500' class='bor_left'>" . $sessionName. "</td>";
}
$courseHTML .= '</tr>';
$courseHTML .= '</table></td>';
$courseHTML .= "<td width = '15%' align='align_left' class='bor_left'>" . get_string ( 'remarks', 'scheduler' ) . "</td>";
$courseHTML .= "<td width = '10%' align='align_left' class='bor_left'>" . get_string ( 'classcompleted', 'scheduler' ) . "</td>";

$courseHTML .= '</tr>';

if (! $learner_list) {
	$courseHTML .= "<tr><td colspan = '3'>" . get_string ( 'no_results' ) . "</td></tr>";
	$table = NULL;
} else {
	foreach ( $learner_list as $learner ) {		
		
		$document_url = $CFG->wwwroot . "/mod/scheduler/upload_document.php?id=" . $learner->scheduler_id . "&studentid=" . $learner->userid;
		// print_object($learner);
		
		$courseHTML .= "<tr class='rep_row'><td class='align_left'  width = '14%'><span class='f-left'><input type='hidden' name='userid[" . $learner->id . "]' value='" . $learner->userid . "' />" . $learner->firstname." ".$learner->lastname . "</span></td>";
		if($session_list){
		$courseHTML .= "<td width = '10%' class='gradeCell'><div class='att'>" . get_string ( 'attended', 'scheduler' ) . "</div><div class='score'>" . get_string ( 'score', 'scheduler' ) . "</div><div>" . get_string ( 'grade', 'scheduler' ) . "</div></td>";
		}
		$courseHTML .= '<td class="sessionCell"><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>';
		foreach ( $session_list as $session ) {
			$session_rec = $DB->get_record ( 'scheduler_appointment', array (
					'slotid' => $session->id,
					'studentid' => $learner->userid 
			) );
			$courseHTML .= "<td  width='500'><div class='att'>".($session_rec->attended == 1?'Yes':'No')."</div><div class='score'>".($session_rec->score!=''?$session_rec->score:'---')."</div><div>".($session_rec->grade!=''?$session_rec->grade:'---')."</div></td>";
		}
		$courseHTML .= '</tr></table></td>';
		$courseHTML .= '<td class="align_left">'.($learner->remarks == ""?"":$learner->remarks).'</textarea></td>';
		$courseHTML .= '<td class="align_left">'.($learner->is_completed == 1?get_string( 'classcompleted', 'scheduler' ):"").'</td>';
	
		
		
		$courseHTML .= "</tr>";
	}
//	$courseHTML .= "<tr><td colspan='4'><input type='submit' name='submit' value='Save & Continue' /></td></tr>";
}

$courseHTML .= "</table></div>";
if ($learner_list) {
	
}
		
}
//$courseHTML .="</div>";
// teacher side

if (! empty ( $courseHTML )) {
	//echo '<div class = "course-listing">';
	
	echo html_writer::start_tag ( 'div', array (
			'class' => 'no-overflow' 
	) );
	echo $courseHTML;
	echo html_writer::end_tag ( 'div' );
	//echo html_writer::end_tag ( 'div' );
	
	// Pring paging bar
	
}
echo "</div>";
echo $OUTPUT->footer ( $course );

?>
<script>
$(window).bind("load", function() {
	  window.print();
	  });


</script>