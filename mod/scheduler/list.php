<?PHP  

/**
 * This page prints a particular instance of scheduler and handles
 * top level interactions
 * 
 * @package    mod
 * @subpackage scheduler
 * @copyright  2011 Henning Bostelmann and others (see README.txt)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->dirroot.'/mod/scheduler/lib.php');
require_once($CFG->dirroot.'/mod/scheduler/locallib.php');

require_login();
$id = optional_param('id', '', PARAM_INT);    // Course Module ID, orsubmitSearchForm


$allTypeOfUsersOfClassroom = getAllTypeOfUsersOfClassroom($id, 2);

$PAGE->set_url('/mod/scheduler/list.php', array('id' => $id));
//echo $id;

if($id >0){
	$course = $DB->get_record('course',array('id'=> $id));
	if(getCourseTypeIdByCourseId($id)!= $CFG->courseTypeClassroom){
		redirect($CFG->wwwroot."/course/courseview.php?id=".$id);
	}
}else{
	redirect($CFG->wwwroot);
}
$dir          = optional_param('dir', 'DESC', PARAM_ALPHA);
$page         = optional_param('page', 1, PARAM_INT);
$perpage      = optional_param('perpage', 10, PARAM_INT);
$pageURL = '/mod/scheduler/list.php?id='.$id;
$paramArray = array(
		'id' => optional_param('id', '', PARAM_ALPHA),
		'key' => optional_param('key', '', PARAM_RAW),
		'name' => optional_param('name', '', PARAM_RAW),
		'des' => optional_param('des', '', PARAM_RAW)
);
$filterArray = array(
		'name'=>get_string('name'),
		'des'=>get_string('description')
);

checkPageIsNumeric($pageURL,$_REQUEST['page']);

$courseTypeIdInTab = getCourseTypeIdByCourseId($id);

if($courseTypeIdInTab == $CFG->courseTypeClassroom){
 checkUserAccess('classroom',$id);
}else{
 checkUserAccess('class',$cid);
}

$allSessionArr = classSessionListOfCourse($id);
/// This is a pre-header selector for downloded documents generation


/// Print the page header

$strschedulers = get_string('modulenameplural', 'scheduler');
$strscheduler  = get_string('modulename', 'scheduler');
$strtime = get_string('time');
$strdate = get_string('date', 'scheduler');
$strstart = get_string('start', 'scheduler');
$strend = get_string('end', 'scheduler');
$strname = get_string('name');
$strseen = get_string('seen', 'scheduler');
$strnote = get_string('comments', 'scheduler');
$strgrade = get_string('grade', 'scheduler');
$straction = get_string('action', 'scheduler');
$strduration = get_string('class_date_label');
$stremail = get_string('email');
$PAGE->set_pagelayout('classroom');
$PAGE->navbar->add(get_string('managecourses'), new moodle_url($CFG->wwwroot.'/course/index.php'));
$PAGE->navbar->add(get_string('editcourse','course'), new moodle_url($CFG->wwwroot.'/course/edit.php',array('id'=>$id)));
//$PAGE->navbar->add(get_string('manage_assets'));
$PAGE->navbar->add(get_string('schedulingandtracking'));

//$title = $course->shortname . ': ' . format_string($scheduler->name);
$PAGE->set_title($title);
$PAGE->set_heading($course->fullname);

echo $OUTPUT->header();

$courseHTML = '';
if($_SESSION['update_msg']!=''){
	$courseHTML .= '<div class="clear"></div>';
	$courseHTML .= '<div class="bg-success text-success text-center '.$_SESSION['error_class'].'" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
	$courseHTML .= '<div class="clear"></div>';
	$_SESSION['update_msg']='';
}
$searchTextField = '';
if(isset($_REQUEST['key']) && trim($_REQUEST['key']) !=''){
	$searchTextField = $_REQUEST['key'];
	$searchText = addslashes(strtolower(trim($_REQUEST['key'])));
}
$scheduler_list = classroomSchedulerListWithPaging($id,$page,$perpage,false,$searchText);
$courseHTML .=  "<div class='tabsOuter'>";

$courseHTML .=   "<div class='tabLinks'>";

include_once($CFG->dirroot.'/course/course_tabs.php');
$courseHTML .=   "</div>";
$courseHTML .= '<div class="clear"></div><div class="borderBlockSpace">';

$classroomAccess = haveClassroomCourseAccess($id);
$headerFilterDiv = '<div id="common-search">';
$headerFilterDiv .= '<form method="get" action="'.$pageURL.'" id="searchForm" name="searchForm">';
$headerFilterDiv .=	'<div id="search-form">';
$headerFilterDiv .=	'<input type = "hidden" name = "id" value = "'.$id.'">';

$headerFilterDiv .=	'<div class="search-input"><input type="text" value="'.( ($searchTextField != '') ? $searchTextField : '').'" onkeypress="submitSearchForm(event)" placeholder="Search" id="key" name="key"></div>';
$headerFilterDiv .= '</div></form>
			<div class="search_clear_button"><input type="button" title="Search" onclick="document.searchForm.submit();" value="Search" name="search"><a href="javascript:void(0);" id="clear" title='.get_string("clear").' onClick="clearSearchBox();">'.get_string("clear").'</a>';
$headerFilterDiv .='</div></div>';
$courseHTML .= $headerFilterDiv;

if( $classroomAccess == $CFG->classroomFullAccess){
	$courseHTML .=  '<div class = "add-class-button"><a class="button-link addbutton " id="add-assests" href="'.$securewwwroot.'/course/modedit.php?add=scheduler&type=&course='.$id.'&section=1&return=0&sr=0" ><i></i><span>'.get_string('addscheduler', 'scheduler').'</span></a></div><div class="clear"></div>';
}

/// route to screen
$courseHTML .= '<div class="coursenametitle"><h3>'.$course->fullname.'</h3></div>';
$courseHTML .=  '<div class="borderBlock"><div class="classlist-outertable"><table class = "" id="page-classlist-outertable"><tr class="head"><th></th>';
$courseHTML .=  "<th width = '15%' align='align_left' >".get_string('name','scheduler')."</th>";
$courseHTML .=  "<th width = '12%' align='align_left' >".get_string('noofusers','scheduler')."<br><span class='approved_req'>(".get_string('approved_requests','scheduler').")</span></th>";
$courseHTML .=  "<th width = '15%' align='align_left' >".get_string('instructor','scheduler')."</th>";
//$courseHTML .=  "<td width = '22%' align='align_left' >Description</td>";
$courseHTML .=  "<th width = '22%' align='align_left' >".$strduration."</th>";
//$tableHtml .= "<td width = '15%' align='align_left' >$createdby</td>";
$courseHTML .=  "<th width = '33%' align='align_left' >".get_string('manage')."</th>";
$courseHTML .=  '</tr>';
	if (!$scheduler_list) {
		$courseHTML .=  "<tr><td colspan = '6'>".get_string('no_results')."</td></tr>";
		$table = NULL;
	} else {
		$countClasses = count(classroomSchedulerListWithPaging($id,$page,$perpage,true));
		
		foreach ($scheduler_list as $schedule) {
			
			$course_module = getSchedulerModuleId($schedule->id,$id);
			//$session_list = classSessionList($course_module->id);
			$session_list = $allSessionArr[$schedule->id];
			$disabled_button = 0;
			$delete_session_disabled = 0;
			if(count($session_list)==0){
				$disabled_button = 1;
			}
			if(count($session_list)==1 || $schedule->isclasscompleted==1){
				$delete_session_disabled = 1;
			}
			
			$buttons = array();
			
			$class_preview = '<a href="'.$CFG->wwwroot.'/course/classroom_preview.php?id='.$id.'&classId='.$schedule->id.'" class="view" title="'.get_string('previewlink','course').'">'.get_string('previewlink','course').'</a>';	
			$action_view = '<a href="'.$CFG->wwwroot.'/mod/scheduler/view.php?id='.$course_module->id.'" class="view" title="'.get_string('viewdetail','scheduler').'">'.get_string('viewdetail','scheduler').'</a>';
			if($schedule->isclasscompleted==0){
			$action_edit = '<a href="'.$CFG->wwwroot.'/course/modedit.php?update='.$course_module->id.'&return=1" class="edit" title="'.get_string('edit').'">'.get_string('previewlink','course').'</a>';
			}
			else{
			$action_edit = '<a href="javascript:void(0)" class="edit-disable" title="'.get_string('edit').'">'.get_string('previewlink','course').'</a>';
			}
			if($disabled_button || $course->publish==0){
				if($course->publish==0){
					//$title_text = get_string('classcannotassignuntilclasspublished','scheduler');
					$title_text =get_string('assignusers');
				}
				else{
					//$title_text =get_string('classcannotassign','scheduler');Deactivate
					$title_text =get_string('assignusers');
				}
				$assign_user = '<a href="javascript:void(0)" class="assign-group-disabled" title = "'.$title_text.'">'.$title_text.'</a>';
			}
			else{			
							
				$assign_user = '<a href="'.$CFG->wwwroot.'/mod/scheduler/classallocation.php?id='.$course_module->id.'" class="assign-group classroom-popup" title = "'.get_string('assignusers').'">'.get_string('assignusers').'</a>';
				
			  }
			
			
			
			$referenceMat = '<a href="'.$CFG->wwwroot.'/mod/scheduler/materialsview.php?id='.$id.'&classId='.$schedule->id.'" class="refrence-material classroom-popup" title = "'.get_string('materialsview').'">'.get_string('materialsview').'</a>';
			
			$deleteClass = '<a href="" class="delete delete_class" title = "'.get_string('delete').'" rel="'.$course_module->id.'">Delete Class</a>';
			$deleteClass_disabled = '<a href="" class="delete-disabled" title = "'.get_string('delete').'" rel="'.$course_module->id.'">Delete Class</a>';
			if($disabled_button){
				$grade_to_user = '<a href="javascript:void(0)" class="attendance-schedule-disabled" title = "'.get_string('attendance','scheduler').'">'.get_string('classcannotattendenceperformance','scheduler').'</a>';
			}
			else{
				if($schedule->startdate>time()){
					$grade_to_user = '<a href="javascript:void(0)" class="attendance-schedule-disabled" title = "'.get_string('attendance','scheduler').'">'.get_string('classcannotattendenceperformance','scheduler').'</a>';
				}else{
				$grade_to_user = '<a href="'.$CFG->wwwroot.'/mod/scheduler/learner_grade.php?id='.$course_module->id.'" class="attendance-schedule classroom-popup" title = "'.get_string('attendance','scheduler').'">'.get_string('attendance','scheduler').'</a>';
				}
			}
			if($schedule->isactive==1){
				$changeClassstatus = '<a href="" class="classdisable change_classstatus" title = "'.get_string('deactivatelink','course').'" rel="'.$schedule->id.'" status="0">'.get_string('deactivatelink','course').'</a>';
				$changeClassstatus_disabled = '<a href="javascript:void(0)" class="classdisable-disabled" title = "'.get_string('deactivatelink','course').'" rel="'.$schedule->id.'" status="0">'.get_string('deactivatelink','course').'</a>';
			}
			else{
				
				$changeClassstatus = '<a href="" class="classenable change_classstatus" title = "'.get_string('activatelink','course').'" rel="'.$schedule->id.'" status="1">'.get_string('activatelink','course').'</a>';
				$changeClassstatus_disabled = '<a href="javascript:void(0)" class="classenable-disabled" title = "'.get_string('activatelink','course').'" rel="'.$schedule->id.'" status="1">'.get_string('activatelink','course').'</a>';
				$action_edit = '<a href="javascript:void(0)" class="edit-disable" title="'.get_string('edit').'">'.get_string('previewlink','course').'</a>';
				$grade_to_user = '<a href="javascript:void(0)" class="attendance-schedule-disabled" title = "'.get_string('attendance','scheduler').'">'.get_string('classcannotattendenceperformance','scheduler').'</a>';
				$assign_user = '<a href="javascript:void(0)" class="assign-group-disabled" title = "'.get_string('assignusers').'">'.$title_text.'</a>';
				$referenceMat = '<a href="javascript:void(0)" class="refrence-material-disabled" title = "'.get_string('materialsview').'">'.get_string('materialsview').'</a>';
			}
			
			
			
			$sendinvite_users = '<a href="" class="assign-group send_invite" title = "'.get_string('attendance','scheduler').'">'.get_string('attendance','scheduler').'</a>';
			 
			
			$countUser = getCountOfClassUser($schedule->id);
			if($classroomAccess == $CFG->classroomFullAccess){
				 
				 $buttons[] = $class_preview;
				 $buttons[] = $action_edit;
			   	 $buttons[] = $assign_user;
			     $buttons[] = $grade_to_user;
				 $buttons[] = $referenceMat;
				 
				 if($countUser->total_user==0){
				 	$buttons[] = $deleteClass;
				 }
				 else{
				 	if($schedule->isclasscompleted==1){
				 	$buttons[] = $changeClassstatus_disabled;
				 	}
				 	else{
				 		$buttons[] = $changeClassstatus;
				 	}
				 }
			
				 
			}elseif($classroomAccess == $CFG->classroomMediumAccess){
			
			    $buttons[] = $class_preview;
				$buttons[] = $grade_to_user;			 
			    $buttons[] = $referenceMat;
				 
			}elseif($classroomAccess == $CFG->classroomLowAccess){
				
				$buttons[] = $class_preview;
				$buttons[] = $grade_to_user;
				$buttons[] = $referenceMat;
				 
			}
			
			
		
			$courseHTML .= '<tr  ><td><a href="#" id="plus-minus-icon-'.$schedule->id.'" class="no-course plusminusIcon" rel="'.$schedule->id.'"></a></td><td class="align_left right-icon"><span class="f-left">'.$schedule->name.'</span></td>';
			if(strlen($schedule->intro) >200){
				$description = substr($schedule->intro,0,200)." ...";
			}else{
				$description = $schedule->intro;
			}
		
		
			$courseHTML .=  "<td>".$countUser->no_of_approved_user."/".$countUser->total_user."</td>";
			$duration_text = getClassDuration($schedule->startdate,$schedule->enddate);
	
			$courseHTML .=  "<td>".fullname(getClassInstructor($schedule))."</td>";
			$courseHTML .=  "<td>".$duration_text."</td>";
			
			$courseHTML .=  "<td>".implode(' ', $buttons);
			$courseHTML .= ' </td>';
			$courseHTML .=  "</tr>";
			
			$courseHTML .= "<tr id='dbox".$schedule->id."' style='display:none'><td colspan='6' >";
						
			
			$courseHTML .=  "<div class='borderBlockSpace'><div class='session_table_".$schedule->id."_".$course_module->id."'><table class = 'table1 ' class='expandclass".$schedule->id."'><thead><tr>";
			$courseHTML .=  "<td width = '20%' align='align_left' >".get_string('sessionname','scheduler')."</td>";
			//$courseHTML .=  "<td width = '22%' align='align_left' >Description</td>";
			$courseHTML .=  "<td width = '60%' align='align_left' >".get_string('duration_date')."</td>";
			//$tableHtml .= "<td width = '15%' align='align_left' >$createdby</td>";
			$courseHTML .=  "<td width = '20%' align='align_left' >".get_string('manage')."</td>";
			$courseHTML .=  '</thead></tr>';
			if (!$session_list) {
				$courseHTML .=  "<tr><td colspan = '3'>".get_string('no_results')."</td></tr>";
				$table = NULL;
			} else {
				$returnurl = $CFG->wwwroot.'/mod/scheduler/view.php';
				$active_session = 0;
				foreach($session_list as $session){
					if($session->isactive){
						$active_session =$active_session+1;
					}
				}
				
				foreach ($session_list as $session) {
					
					$class_module_id = $schedule->id.'_'.$course_module->id;
					$buttons = array();
					if($schedule->isclasscompleted==0 && $schedule->isactive==1){
						$edit_session_url  = html_writer::link(new moodle_url($returnurl, array('what'=>'updateslot', 'id'=>$course_module->id,'slotid'=>$session->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>$stredit, 'class'=>'iconsmall ')), array('title'=>get_string('edit'), 'class'=>'edit session-popup','classid'=>$class_module_id));
					}
					else{
						$edit_session_url  = html_writer::link(new moodle_url('javascript:void(0)'), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>$stredit, 'class'=>'iconsmall ')), array('title'=>get_string('edit'), 'class'=>'edit-disable','classid'=>$class_module_id));
					}
					
					if($session->isactive==0){
						$edit_session_url  = html_writer::link(new moodle_url('javascript:void(0)'), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>$stredit, 'class'=>'iconsmall ')), array('title'=>get_string('edit'), 'class'=>'edit-disable','classid'=>$class_module_id));
					}
			
						$delete_session_url = '<a href="" class="delete delete_session" title = "'.get_string('delete').'" rel="'.$session->id.'">'.get_string('delete').'</a>';
					
					
						
						$disable_delete_session_url = '<a href="javascript:void(0)" class="delete-disabled" title = "'.get_string('delete').'" rel="'.$session->id.'">'.get_string('delete').'</a>';
					if($classroomAccess == $CFG->classroomFullAccess){
							
						 $buttons[] = $edit_session_url;
						
						 if($countUser->total_user==0){
						 	$buttons[] = $delete_session_url;
						 }
						 else{
						 	if($delete_session_disabled){
						 		$buttons[] = $disable_delete_session_url;
						 	}
						 	else{
						 		
						 		if($session->isactive==1){
						 			if($active_session==1){
						 				$changeSessionstatus = '<a href="" class="classdisable-disabled" title = "'.get_string('deactivatelink','course').'" rel="'.$session->id.'" status="0">'.get_string('deactivatelink','course').'</a>';
						 			}
						 			else{
						 				$changeSessionstatus = '<a href="" class="classdisable change_sessionstatus" title = "'.get_string('deactivatelink','course').'" rel="'.$session->id.'" status="0">'.get_string('deactivatelink','course').'</a>';
						 			}
						 		}
						 		else{
						 			$changeSessionstatus = '<a href="" class="classenable change_sessionstatus" title = "'.get_string('activatelink','course').'" rel="'.$session->id.'" status="1">'.get_string('activatelink','course').'</a>';
						 		}
						 		$buttons[] = $changeSessionstatus;
						 	}
						 }
						
					}elseif($classroomAccess == $CFG->classroomMediumAccess){
							
						
							
					}elseif($classroomAccess == $CFG->classroomLowAccess){
							
						
							
					}
					
					$courseHTML .= '<td class="align_left"><span class="f-left">'.$session->sessionname.'</span></td>';
			
					$end_time =endTimeByDuration($session->starttime,$session->duration);
					$duration_text = date($CFG->customDefaultDateTimeFormat,$session->starttime).' to '.date($CFG->customDefaultTimeFormat1,$end_time);
					$courseHTML .=  "<td>".$duration_text."</td>";
					$courseHTML .=  "<td>".implode(' ', $buttons)."</td>";
					$courseHTML .=  "</tr>";
				}
			}
			$courseHTML .=  '</table></div></div>';
			if( $classroomAccess == $CFG->classroomFullAccess){
				if($CFG->isClassAllowForPastDate==1 && $schedule->isclasscompleted==0 && $schedule->isactive==1){
					
					$courseHTML .=  '<div class = "add-course-button add-session "><a class="button-link add-session-btn add-session-button session-popup " classid="'.$schedule->id.'_'.$course_module->id.'" id="add-assests" href="'.$CFG->wwwroot.'/mod/scheduler/view.php?id='.$course_module->id.'&what=addslot" ><i></i><span>'.get_string('addsingleslot', 'scheduler').'</span></a></div>';
				}
				else if($schedule->enddate>time())
				{
				$courseHTML .=  '<div class = "add-course-button add-session "><a class="button-link add-session-btn add-session-button session-popup "  classid="'.$schedule->id.'_'.$course_module->id.'" id="add-assests" href="'.$CFG->wwwroot.'/mod/scheduler/view.php?id='.$course_module->id.'&what=addslot" ><i></i><span>'.get_string('addsingleslot', 'scheduler').'</span></a></div>';
				}
			}
			$courseHTML .= "</td></tr>";
			
		}
	}
	$courseHTML .=  "</table></div></div></div>";
	
// teacher side
    
  if (!empty($courseHTML)) {
		echo '<div class = "">';
		
        echo html_writer::start_tag('div', array('class'=>''));
        echo $courseHTML;
        echo html_writer::end_tag('div');
        echo html_writer::end_tag('div');

		//Pring paging bar
        echo paging_bar($countClasses, $page, $perpage, $pageURL);
    }
    echo "</div>";
echo $OUTPUT->footer($course);

?>

<script>


$(document).ready(function(){
	
	
	
	$(".add-session-button").click(function(){
		$(this).attr("title","<?php echo get_string('addsingleslot', 'scheduler') ?>");
		return true;
		});
	$(".delete_session").click(function(){
		
		var slotid = $(this).attr("rel");
		var txt = "Do you want to delete this Session?";
		
		var response = confirm(txt);
		
		if (response == true) {
			$.ajax({
				url:'<?php $CFG->wwwroot;?>/mod/scheduler/ajax.php',
				type:'POST',
				data:"action=deleteSession&slotid="+slotid,
				success:function(data){
						if(data == 'success'){
							var okTxt = "session deleted successfully";
							//alert(okTxt);
							window.location.reload();
							
						}else{
							alert("unable to process request. Please try again later");
						}
				}
			});
		}
		return false;
	});
	
	$(".delete_class").click(function(){
		var cmid = $(this).attr("rel");
		var txt = "Do you want to delete this class?";
		
		var response = confirm(txt);
		
		if (response == true) {
			$.ajax({
				url:'<?php $CFG->wwwroot;?>/mod/scheduler/ajax.php',
				type:'POST',
				data:"action=deleteClass&classid="+cmid,
				success:function(data){
						if(data == 'success'){
							var okTxt = "class deleted successfully";
							//alert(okTxt);
							window.location.reload();
							
						}else{
							alert("unable to process request. Please try again later");
						}
				}
			});
		}
		return false;
	});



	// change session status
	
	$(".change_sessionstatus").click(function(){
		var sessionid = $(this).attr("rel");
		var status = $(this).attr("status");
		if(status==1){
			var txt = "Do you want to activate this session?";
		}
		else{
			var txt = "Do you want to deactivate this session?";
		}
		
		
		var response = confirm(txt);
		
		if (response == true) {
			$.ajax({
				url:'<?php $CFG->wwwroot;?>/mod/scheduler/ajax.php',
				type:'POST',
				data:"action=changeSessionStatus&session="+sessionid+"&status="+status,
				success:function(data){
						if(data == 'success'){
							if(status==1){
								var okTxt = "class activate successfully";
							}
							else{
								var okTxt = "class unpublish successfully";
							}
							//alert(okTxt);
							
							window.location.reload();
							
						}else{
							alert("unable to process request. Please try again later");
						}
				}
			});
		}
		return false;
	});
	
	$(".change_classstatus").click(function(){
		var classid = $(this).attr("rel");
		var status = $(this).attr("status");
		if(status==1){
			var txt = "Do you want to activate this class?";
		}
		else{
			var txt = "Do you want to deactivate this class?";
		}
		
		
		var response = confirm(txt);
		
		if (response == true) {
			$.ajax({
				url:'<?php $CFG->wwwroot;?>/mod/scheduler/ajax.php',
				type:'POST',
				data:"action=changeClassStatus&classid="+classid+"&status="+status,
				success:function(data){
						if(data == 'success'){
							if(status==1){
								var okTxt = "class activate successfully";
							}
							else{
								var okTxt = "class deactivate successfully";
							}
							//alert(okTxt);
							
							window.location.reload();
							
						}else{
							alert("unable to process request. Please try again later");
						}
				}
			});
		}
		return false;
	});

	$(".send_invite").click(function(){
		var cmid = $(this).attr("rel");
		var txt = "Do you want to send invitation to users?";
		
		var response = confirm(txt);
		
		if (response == true) {
			$.ajax({
				url:'<?php $CFG->wwwroot;?>/mod/scheduler/ajax.php',
				type:'POST',
				data:"action=sendInvite&classid="+cmid,
				success:function(data){
						if(data == 'success'){
							var okTxt = "class deleted successfully";
							//alert(okTxt);
							window.location.reload();
							
						}else{
							alert("unable to process request. Please try again later");
						}
				}
			});
		}
		return false;
	});
var lastRow = $('#page-classlist-outertable > tbody > tr:last');
lastRow.prev('tr').addClass('bor_none borderShow');
	
});

</script>
