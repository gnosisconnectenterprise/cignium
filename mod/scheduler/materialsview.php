<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Lists the course categories
 *
 * @copyright 1999 Martin Dougiamas  http://dougiamas.com
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package course
 */

require_once("../../config.php");
require_once($CFG->dirroot. '/course/lib.php');
require_once($CFG->libdir. '/coursecatlib.php');
require_once($CFG->libdir. '/filelib.php');


$cid = required_param('id', PARAM_INT);
$classId = required_param('classId', PARAM_INT);
$updateid = optional_param('updateid', 0, PARAM_INT);
$moduleid = optional_param('moduleid', 0, PARAM_INT);
$flag = optional_param('flag', 0, PARAM_INT);
$showAddDiv = optional_param('show', 0, PARAM_INT);
$deleteId = optional_param('deleteid', 0, PARAM_INT);

global $DB,$CFG,$USER;
if($cid && $classId){
	$course = $DB->get_record('course',array('id'=> $cid));
	$scheduler = $DB->get_record('scheduler',array('course'=> $cid, 'id'=> $classId));
	if(empty($scheduler)){
	   redirect($CFG->wwwroot.'/course/index.php');
	}
}else{
	 redirect($CFG->wwwroot.'/course/index.php');
}


checkUserAccess('class',$classId);

if($cid && $classId && $updateid){ 
   if(!haveReferenceMaterialAccess($classId, $updateid)){
     redirect($CFG->wwwroot.'/mod/scheduler/materialsview.php?id='.$cid.'&classId='.$classId.'&show=1');
   }
}

if($cid && $deleteId && $moduleid){


   $haveEnrollment = isClassHaveAnyEnrollment($classId);
  
   
   if($haveEnrollment){
   
	     $_SESSION['update_msg'] = get_string('refmatcannotdeletehaveenrollmentinclassroom','course');
	     $_SESSION['error_class'] = 'error_bg';
	     redirect(new moodle_url($CFG->wwwroot.'/mod/scheduler/materialsview.php?id='.$cid."&classId=".$classId));
		 
   }
   
   
	$isResourceDeleted = deleteRefrenceMaterial($cid, $moduleid, $deleteId,$classId);
	
	if($isResourceDeleted){
	  $_SESSION['update_msg'] = get_string('record_deleted');
	  $_SESSION['error_class'] = '';
	  redirect(new moodle_url($CFG->wwwroot.'/mod/scheduler/materialsview.php?id='.$cid."&classId=".$classId));
	}else{
	  redirect(new moodle_url($CFG->wwwroot.'/course/index.php'));
	}
}


$PAGE->set_pagelayout('classroompopup');
$PAGE->set_heading($SITE->fullname);
$PAGE->set_title($SITE->fullname);

$PAGE->navbar->add(get_string('managecourses'), new moodle_url($CFG->wwwroot.'/course/index.php'));
$PAGE->navbar->add(get_string('editcourse','course'), new moodle_url($CFG->wwwroot.'/course/edit.php',array('id'=>$cid)));
$PAGE->navbar->add(get_string('schedulingandtracking'), new moodle_url($CFG->wwwroot.'/mod/scheduler/list.php',array('id'=>$cid)));
$PAGE->navbar->add(get_string('managematerials'));

$resData = getReferenceMaterialsOfClass($classId);

$courseHTML = '<div class="borderBlockSpace">';
//$courseHTML .= "<div class='tabLinks'>";
//include_once($CFG->dirroot. '/course/course_tabs.php');
//$courseHTML .= "</div>";
$courseHTML .= '<div class="clear"></div><div class="">';

	if($updateid == 0){
		if($scheduler->isclasscompleted==0)
		{
		$courseHTML .= '<div ><a class="button-link add-assests" href="javascript:void(0);"  id="add-assests"><i></i>'.get_string('addreferencematerial').'</a></div>';
		}
		if($_SESSION['update_msg']!=''){
			$courseHTML .= '<div class="clear"></div>';
			$courseHTML .= '<div class="bg-success text-success text-center '.$_SESSION['error_class'].'" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
			$courseHTML .= '<div class="clear"></div>';
			$_SESSION['update_msg']='';
		}
		$courseHTML .= '<div class="assets_ele clear" id ="assets_element" style="'.(($showAddDiv == 1) ? 'display:block;' : 'display:none;').'"><table cellspacing="0" cellpadding="0" border="0" class="generaltable"><tr><th width="30%">'.get_string('referencematerialnamelabel').'</th><th width="35%">'.get_string('referencematerialdescription').'</th><th width="20%">'.get_string('referencematerialtype').'</th><th width="15%">'.get_string('referencematerialupload').'</th></tr>';
		$courseHTML .= '<tr><form method="post" id="asset_form" action="'.$CFG->wwwroot.'/course/medium.php">';
		$courseHTML .= '<td style="text-align:center;"><input type ="text" id="asset_name" name ="asset_name"></td><td style="text-align:center;"><textarea id="asset_description" name ="asset_description" style="width:300px;height:50px;" ></textarea><!--input type ="textarea" id="asset_description" name ="asset_description" --></td>';
		
            $resourceType = getResourceType();

		    $courseHTML .= '<td style="text-align:center;"><input type ="hidden" id="asset_type" name ="asset_type" value="resource">
			<select id="resource_type" name= "resource_type">
			<option value="">'.get_string('selecttype','course').'</option>';
			if(count($resourceType) > 0 ){
			  foreach($resourceType as $resourceTypeArr){
			    $courseHTML .= '<option value="'.$resourceTypeArr->id.'">'.$resourceTypeArr->name.'</option>';
			  }
			}
			$courseHTML .= '</select>
			'.get_string('allowstudentaccess','course').' <input type="checkbox"  name="allow_student_access" id="allow_student_access" value="1">
			</td>';

		
		$courseHTML .= '<td style="text-align:center;">
			<a href="javascript:void(0);" title="Upload" id="asset_upload_file" class="uploadFileBtn">Upload</a>
			<input type="hidden" name="course_id" id="course_id" value="'.$course->id.'">
			<input type="hidden" name="action_type" id="action_type" value="add">
			<input type="hidden" name="classId" id="class" value="'.$classId.'">
		</td>';
		$courseHTML .= '</form></tr>';
		$courseHTML .= '</table></div>';
	}elseif($updateid > 0){ 
	
		if($moduleid >0){
		
		$asset_data = $DB->get_record_sql("select * from {$CFG->prefix}resource where id = $updateid");
		//$courseHTML .= '<div class="add-course-button add-assests"><a href="javascript:void(0);"  id="add-assests"><i></i>'.get_string('add_assets').'</a></div>';
		$courseHTML .= '<div class="add-course-button"><a class="button-link add-assests" href="'.$CFG->wwwroot.'/mod/scheduler/materialsview.php?id='.$course->id.'&classId='.$classId.'&show=1"  id="add-assests"><i></i>'.get_string('addreferencematerial').'</a></div>';
		$courseHTML .= '<div class="assets_ele clear" id ="assets_element" style="display:block;"><table cellspacing="0" cellpadding="0" border="0"  class="generaltable"><tr><th width="30%">'.get_string('referencematerialnamelabel').'</th><th width="35%">'.get_string('referencematerialdescription').'</th><th width="20%">Type</th><th width="15%">'.get_string('referencematerialupload').'</th></tr>';


			$courseHTML .= '<tr><form method="post" id="asset_form" action="'.$CFG->wwwroot.'/course/medium.php">';
			$courseHTML .= '<td style="text-align:center;"><input type ="text" id="asset_name" name ="asset_name" value="'.$asset_data->name.'"></td><td style="text-align:center;"><textarea id="asset_description" name ="asset_description" style="width:300px;height:50px;" >'.strip_tags($asset_data->intro).'</textarea><!--input type ="textarea" id="asset_description" name ="asset_description" value="'.strip_tags($asset_data->intro).'"--></td>';

                 $resourceType = getResourceType();
				 $courseHTML .= '<td style="text-align:center;"><input type ="hidden" id="asset_type" name ="asset_type" value="resource">
								 <select id="resource_type" name= "resource_type" >
								 <option value="">'.get_string('selecttype','course').'</option>';
								 
									if(count($resourceType) > 0 ){
									  foreach($resourceType as $resourceTypeArr){
									    if($asset_data->resource_type == $resourceTypeArr->id){
										   $courseHTML .= '<option value="'.$resourceTypeArr->id.'" selected="selected" >'.$resourceTypeArr->name.'</option>';
										}else{
										   $courseHTML .= '<option value="'.$resourceTypeArr->id.'">'.$resourceTypeArr->name.'</option>';
										}
									  }
									}
			     $courseHTML .= '</select>
				 '.get_string('allowstudentaccess','course').' : <input type="checkbox"  name="allow_student_access" id="allow_student_access" value="1" '.($asset_data->allow_student_access == 1?"checked='checked'":"").'>
				 </td>';

			
			$courseHTML .= '<td style="text-align:center;"><a href="javascript:void(0);" title="Upload" id="asset_upload_file" class="uploadFileBtn">Upload</a>
			<input type="hidden" name="course_id" id="course_id" value="'.$course->id.'">
			<input type="hidden" name="moduleid" id="moduleid" value="'.$moduleid.'">
			<input type="hidden" name="action_type" id="action_type" value="update">
			<input type="hidden" name="classId" id="classId" value="'.$classId.'">
			</td></form></tr>';

		$courseHTML .= '</table></div>';
		$courseHTML .= '<div class="asset_save"><input type="button" id="asset_save" value="'.get_string('quickupdate', 'course').'" name="asset_save"></div>';
		
		}else{ 
		
		    /*$updateResult = getActiveCourseCount($cid, 1);
			if(!$updateResult && $flag == 0){
				$_SESSION['update_msg'] = get_string('atleastonereferencematerialmustbeactivated');
				$_SESSION['error_class'] = 'error_bg';
			}else{*/

				$DB->set_field('resource', 'is_active', $flag, array('id'=>$updateid));

				$_SESSION['update_msg'] = get_string('record_updated');
				$_SESSION['error_class'] = '';
			//}
			redirect(new moodle_url($CFG->wwwroot.'/mod/scheduler/materialsview.php?id='.$course->id."&classId=".$classId));
		}
	}


$courseHTML .= '<div class="borderBlock"><h2 class="icon_title">'.get_string('materialsview').'</h2><div class="borderBlockSpace"><table class="table1" cellspacing="0" cellpadding="0" border="0" width="100%">'. 
					'<thead><tr><td width="35%">'.get_string('referencematerialname').'</td><td width="20%">'.get_string('type').'</td><td width="20%">'.get_string('refematcreatedby').'</td><td width="25%">'.get_string('referencematerialmanage').'</td></tr></thead>';
					
		if($resData){

			foreach($resData as $rdata){

                $haveRefMatAccess = haveReferenceMaterialAccess($rdata->scheduler_id, $rdata->resource_id);
				$rdataIntro = $rdata->intro?nl2br($rdata->intro):$rdata->intro;
				$refmatCreatedByName = $rdata->refmat_createdbyname;
				$refmatCreatedBy = $rdata->refmat_createdby;
				if(strlen($rdataIntro) >100){
					$rdataIntro = substr($rdataIntro,0,100)." ...";
				}

				$course_moduleid = $DB->get_field('course_modules','id',array('course'=>$course->id,'instance'=>$rdata->resource_id,'module'=>17));

				if($rdata->is_active == 1){
					$courseHTML .=  '<tr><td>'.$rdata->name.'</td><td>'.$rdata->resoursetype_name.'</td><td>'.$refmatCreatedByName.'</td>';
					$courseHTML .= '<td>';
					
					$isRExist = isResourceFileExist($course_moduleid);
					if($isRExist){
					  $launchurl = $CFG->wwwroot.'/mod/resource/view.php?id='.$course_moduleid.'&redirect=1';
					  $courseHTML .= "<a href=\"javascript:void(0);\" onclick=\"window.open('".$launchurl."', '', '".$CFG->FVPreviewWindowParameter."'); \" class = \"view\" title=\"".get_string('previewlink','course')."\">".get_string('previewlink','course')."</a>";
					}else{
					   $courseHTML .= "<a href='javascript:;' onclick='alert(\"".get_string('clamdeletedfile')."\")' title='".get_string('download','learnercourse')."' class = \"view\" title=\"".get_string('previewlink','course')."\">".get_string('previewlink','course')."</a>";
					}
					if($scheduler->isclasscompleted==0){
						if($haveRefMatAccess){
							//$courseHTML .= '<a href="'.$CFG->wwwroot.'/mod/resource/view.php?id='.$course_moduleid.'" class="view"			 title="'.get_string('previewlink','course').'">'.get_string('previewlink','course').'</a> ';
		
							$courseHTML .= '<a href="'.$CFG->wwwroot.'/mod/scheduler/materialsview.php?updateid='.$rdata->resource_id.'&type=resource&classId='.$classId.'&id='.$course->id.'&moduleid='.$course_moduleid.'" class="edit" title="'.get_string('edit','course').'">'.get_string('edit','course').'</a> ' . 
									  '<a href="'.$CFG->wwwroot.'/mod/scheduler/materialsview.php?updateid='.$rdata->resource_id.'&type=resource&classId='.$classId.'&id='.$course->id.'&flag=0" class="disable" title="'.get_string('deactivatelink','course').'">'.get_string('deactivatelink','course').'</a>';
									  
									  
							//$haveEnrollment = isClassHaveAnyEnrollment($classId);
							//if(!$haveEnrollment){
							  $courseHTML .= '<a href="'.$CFG->wwwroot.'/mod/scheduler/materialsview.php?deleteid='.$rdata->resource_id.'&classId='.$classId.'&type=resource&id='.$course->id.'&moduleid='.$course_moduleid.'" class="delete" title="'.get_string('delete','course').'">'.get_string('delete','course').'</a> ';
							//}		  
									 
								  
						}
					}else{
					$courseHTML .= '<a href="javascript:void(0);" class="edit-disable"  title="'.get_string('edit','course').'">'.get_string('edit','course').'</a> 
					<a href="javascript:void(0);" class="classdisable-disabled" title="'.get_string('deactivatelink','course').'">'.get_string('deactivatelink','course').'</a><a href="javascript:void(0);" class="delete-disabled" title="'.get_string('delete','course').'">'.get_string('delete','course').'</a>';
					
					
					}		  
					$courseHTML .=  '</td></tr>';
				}else{
					$courseHTML .=  '<tr class="disable"><td>'.$rdata->name.'</td><td>Resource</td><td>'.$rdataIntro.'</td>';
					
					$courseHTML .= "<td>";
					
					$isRExist = isResourceFileExist($course_moduleid);
					if($isRExist){
						$launchurl = $CFG->wwwroot.'/mod/resource/view.php?id='.$course_moduleid.'&redirect=1';
						$courseHTML .= "<a href=\"javascript:void(0);\" onclick=\"window.open('".$launchurl."', '', '".$CFG->FVPreviewWindowParameter."'); \" class = \"view\" title=\"".get_string('previewlink','course')."\">".get_string('previewlink','course')."</a>";
					}else{
						$courseHTML .= "<a href='javascript:;' onclick='alert(\"".get_string('clamdeletedfile')."\")' title='".get_string('download','learnercourse')."' class = \"view\" title=\"".get_string('previewlink','course')."\">".get_string('previewlink','course')."</a>";
					}
					
					$courseHTML .= '<a href="javascript:void(0);" class="edit-disable"  title="'.get_string('edit','course').'">'.get_string('edit','course').'</a> ' . 
								  '<a href="'.$CFG->wwwroot.'/mod/scheduler/materialsview.php?updateid='.$rdata->resource_id.'&type=resource&classId='.$classId.'&id='.$course->id.'&flag=1" class="enable" title="'.get_string('activatelink','course').'">'.get_string('deactivatelink','course').'</a>';
					$courseHTML .=  '</td></tr>';
				}
			}
				
		}else{ 
			$courseHTML .=  '<tr><td colspan="4">'.get_string('noreferencematerialfound').'</td></tr>';
		}
	$courseHTML .= '</table></div></div>';
//$courseHTML .= '<input type = "button" value = "Edit" onclick="location.href=\''.$CFG->wwwroot.'/course/edit.php?id='.$course->id.'\';">';
$urlCancel = new moodle_url($CFG->wwwroot.'/mod/scheduler/list.php', array('id'=>$cid));
//$courseHTML .= '<div class="clear" style="padding-top:18px;"></div><input type = "button" value = "Cancel" onclick="location.href=\''.$urlCancel.'\';">';
$courseHTML .= '</div></div>';
echo $OUTPUT->header(); 
echo $OUTPUT->skip_link_target();
///// Open - Added by Madhab to show the Common Search Area /////
$paramArray = array(
				'ch' => optional_param('ch', '', PARAM_ALPHA),
				'key' => optional_param('key', '', PARAM_RAW),
				'id' => optional_param('id', '', PARAM_ALPHANUM),
				'nm' => optional_param('nm', '', PARAM_ALPHANUM),
				'desc' => optional_param('desc', '', PARAM_ALPHANUM),
				'tag' => optional_param('tag', '', PARAM_ALPHANUM),
				'cnm' => optional_param('cnm', '', PARAM_ALPHANUM),
			  );
$filterArray = array(							
					'id'=>get_string('id','course'),
					'nm'=>get_string('name','course'),
					'desc'=>get_string('description','course'),
					'tag'=>get_string('tag','course'),
					'cnm'=>get_string('category','course')
			   );
$pageURL = '/course/index.php';
echo $courseHTML;

echo $OUTPUT->footer();


?>
<style>
#page-course-index-category .asset_save {  margin-top: -10px; }
</style>

<script language="javascript" type="text/javascript">
$(document).ready(function(){
	$('#add-assests').click(function(event){
		$('#assets_element').show();
	});
	$('#asset_upload_file').click(function(event){
		var asset_name = $.trim($('#asset_name').val());
		var asset_description = $.trim($('#asset_description').val());
		

		var asset_type = $('#resource_type').val();

		if(asset_name == ''){
			alert("<?php echo get_string('enterreferencematerialname');?>");
			$('#asset_name').val(asset_name);
			return false;
		}
		if(asset_description == ''){
			alert("<?php echo get_string('enterreferencematerialdescription');?>");
			$('#asset_description').val(asset_description);
			return false;
		}
		if(asset_type == ''){
			alert("<?php echo get_string('enterreferencematerialtype');?>");
			return false;
		}
		$('#asset_form').submit();

	});
	
	$('#asset_save').click(function(event){
		var asset_name = $.trim($('#asset_name').val());
		var asset_description = $.trim($('#asset_description').val());

		var asset_type = $('#resource_type').val();
		
		if(asset_name == ''){
			alert("<?php echo get_string('enterreferencematerialname');?>");
			$('#asset_name').val(asset_name);
			return false;
		}
		if(asset_description == ''){
			alert("<?php echo get_string('enterreferencematerialdescription');?>");
			$('#asset_description').val(asset_description);
			return false;
		}
		if(asset_type == ''){
			alert("<?php echo get_string('enterreferencematerialtype');?>");
			return false;
		}
		
		$("#asset_type").removeAttr("disabled");
		$('#asset_form').append('<input type = "hidden" name="quickedit" value="1">');
		$('#asset_form').submit();
		
	});
	
	$('a.delete').click(function(event){
	
		var status = $(this).attr('class');
		var message = '';
		if(status == "delete"){
			message = "<?php echo get_string('delete_record')?>";
		}
		var r = confirm(message);
		if (r == true) {
			return true;
		} else {
			return false;
		}
	});
	
	
	
});
</script>