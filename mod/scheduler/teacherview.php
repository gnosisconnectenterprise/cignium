<script>
	$(document).ready(function(){
	
		$(document).on('blur', '#id_sessionname', function(){

			   var cVal = $(this).val();
			   cVal = $.trim(cVal);
			   $(this).val(cVal);


			});
	});
	</script>
<?php

/**
 * Contains various sub-screens that a teacher can see.
 *
 * @package    mod
 * @subpackage scheduler
 * @copyright  2011 Henning Bostelmann and others (see README.txt)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

function scheduler_prepare_formdata($scheduler, $slot) {
    global $DB;

    $data = clone (object) $slot;
    $data->notes = array();
    $data->notes['text'] = $slot->notes;
    $data->notes['format'] = $slot->notesformat;
    if ($slot->emaildate < 0){
        $data->emaildate = 0;
    }
    
    $appointments = $DB->get_records('scheduler_appointment', array('slotid' => $data->id));
    $i = 0;
    foreach ($appointments as $appointment) {
        $data->studentid[$i] = $appointment->studentid;
        $data->attended[$i] = $appointment->attended;
        $data->appointmentnote[$i]['text'] = $appointment->appointmentnote;
        $data->appointmentnote[$i]['format'] = $appointment->appointmentnoteformat;
        $data->grade[$i] = $appointment->grade;
        $i++;
    }
    return $data;
}

function scheduler_save_slotform($scheduler, $course, $slotid, $data) {

    global $DB, $OUTPUT,$CFG;
    // make new slot record
    $slot = new stdClass();
    $slot->schedulerid = $scheduler->id;
    $slot->starttime = $data->starttime;
    $slot->duration = $data->duration;
    $slot->exclusivity = $data->exclusivity;
    $slot->sessionname = $data->sessionname;
    $slot->teacherid = $data->teacherid;
    $slot->notes = $data->notes['text'];
    $slot->notesformat = $data->notes['format'];
    $slot->appointmentlocation = $data->appointmentlocation;
    $slot->hideuntil = $data->hideuntil;
    $slot->reuse = $data->reuse;
    $slot->emaildate = $data->emaildate;
    $slot->timemodified = time();
    $web_access_link = $data->web_access_link['text'];
    $slot->web_access_link = $web_access_link;
    $slot->location = $data->location;;
    if($CFG->is12HourFormat==1){
    $slot->starttime = getTimeAsPerTimeFormat($data->starttime,$_REQUEST['timeformat']);
   	 $data->starttime = $slot->starttime; 
    }

    if (!$slotid) {
    	
        // add it
       
        $slot->id = $DB->insert_record('scheduler_slots', $slot);
        
        $title = $course->fullname.':'.$data->sessionname;
        
        $description = $course->summary;
       $already_enrolled_users =  $DB->get_records('scheduler_enrollment',array('scheduler_id'=>$scheduler->id,'is_approved'=>1));
       foreach($already_enrolled_users as $enrolled_user){
       	addClassEvent($title,$description,$enrolled_user->userid,$slot->id,$data->starttime,$data->duration);
		emailForSessionAddUpdate($enrolled_user->userid,$slot->id,'add');
       }
	   addClassEvent($title,$description,$scheduler->teacher,$slot->id,$data->starttime,$data->duration);
	   emailForSessionAddUpdateToTeacher($scheduler->teacher,$slot->id,'add');
    } else {
        // update it
        $slot->id = $slotid;
      
		$slotRecord = $DB->get_record('scheduler_slots',array('id'=>$slotid));
		$teacherRecord = $DB->get_record_sql('SELECT s.teacher FROM mdl_scheduler as s WHERE s.id = '.$slot->schedulerid);
		$title = $course->fullname.' : '.$data->sessionname;
        $DB->update_record('scheduler_slots', $slot);
        
        updateClassEvent($slotid,$data->starttime,$data->duration,$title);
        
		if(!empty($slotRecord) && ($slotRecord->starttime != $slot->starttime || $slotRecord->duration != $slot->duration || $slotRecord->sessionname != $slot->sessionname)){
			$already_enrolled_users =  $DB->get_records('scheduler_enrollment',array('scheduler_id'=>$scheduler->id,'is_approved'=>1));
			foreach($already_enrolled_users as $user){
				emailForSessionAddUpdate($user->userid,$slotid,'update');
			}
			emailForSessionAddUpdateToTeacher($teacherRecord->teacher,$slotid,'update');
		}
        
    }
   // $DB->delete_records('scheduler_appointment', array('slotid'=>$slot->id)); // cleanup old appointments
    for ($i = 0; $i < $data->appointment_repeats; $i++) {
        if ($data->studentid[$i] > 0) {
            $appointment = new stdClass();
            $appointment->slotid = $slot->id;
            $appointment->studentid = $data->studentid[$i];
            $appointment->attended = isset($data->attended[$i]);
            $appointment->appointmentnote = $data->appointmentnote[$i]['text'];
            $appointment->appointmentnoteformat = $data->appointmentnote[$i]['format'];
            if (isset($data->grade)) {
                $selgrade = $data->grade[$i];
                $appointment->grade = ($selgrade >= 0) ? $selgrade : null;
            }
            $DB->insert_record('scheduler_appointment', $appointment);
            scheduler_update_grades($scheduler, $appointment->studentid);
        }
    }

    scheduler_events_update($slot, $course);
}


function scheduler_print_schedulebox($scheduler, $cm, $studentid, $groupid = 0) {
    global $CFG, $OUTPUT;

    $availableslots = scheduler_get_available_slots($studentid, $scheduler->id);

    $startdatemem = '';
    $starttimemem = '';
    $availableslotsmenu = array();
    foreach ($availableslots as $slot) {
        $startdatecnv = scheduler_userdate($slot->starttime, 1);
        $starttimecnv = scheduler_usertime($slot->starttime, 1);

        $startdatestr = ($startdatemem != '' and $startdatemem == $startdatecnv) ? "-----------------" : $startdatecnv;
        $starttimestr = ($starttimemem != '' and $starttimemem == $starttimecnv) ? '' : $starttimecnv;

        $startdatemem = $startdatecnv;
        $starttimemem = $starttimecnv;

        $url = $CFG->wwwroot.'/mod/scheduler/view.php?id='.$cm->id.'&slotid='.$slot->id;
        if ($groupid) {
            $url .= '&what=schedulegroup&subaction=dochooseslot&groupid='.$groupid;
        } else {
            $url .= '&what=schedule&subaction=dochooseslot&studentid='.$studentid;
        }
        $availableslotsmenu[$url] = "$startdatestr $starttimestr";
    }

    $chooser = new url_select($availableslotsmenu);
    //    $formoptions = array('action' => $CFG->wwwroot.'/mod/scheduler/view.php?what=schedule&subaction=dochooseslot&studentid='.$studentid);
    //  $form = html_writer::tag('form', $chooser, $formoptions);

    if ($availableslots) {
        echo $OUTPUT->box_start();
        echo $OUTPUT->heading(get_string('chooseexisting', 'scheduler'), 2);
        echo $OUTPUT->render($chooser);
        echo $OUTPUT->box_end();
    }
}

// load group restrictions
$modinfo = get_fast_modinfo($course);

$usergroups = '';


if ($action) {
    include_once($CFG->dirroot.'/mod/scheduler/slotforms.php');
    include($CFG->dirroot.'/mod/scheduler/teacherview.controller.php');
}
if($CFG->is12HourFormat==1)
{
?>
 <script>
$( document ).ready(function() {
	var timestartFirmat = $("input[name='timestartformat']").val();
	var PmSelected = '';
	if(timestartFirmat=="PM"){
		PmSelected = 'selected=selected';
	}
	//alert(timestartFirmat);
	var selectbox = ' <select name="timeformat"><option value="0">AM</option><option value="1" '+PmSelected+'>PM</option></select>';
   $("#id_starttime_minute").after(selectbox);
});
</script> 
<?php 
}
/************************************ View : New single slot form ****************************************/
if ($action == 'addslot') {
    $actionurl = new moodle_url('/mod/scheduler/view.php', array('what' => 'addslot', 'id' => $cm->id));
    $returnurl = new moodle_url('/mod/scheduler/view.php', array('what' => 'view', 'id' => $cm->id));

    $mform = new scheduler_editslot_form($actionurl, $scheduler, $cm, $usergroups);

    if ($mform->is_cancelled()) {
        redirect($returnurl);
    } else if ($formdata = $mform->get_data()) {
        $slotid = scheduler_save_slotform ($scheduler, $course, 0, $formdata);
        $_SESSION['update_msg'] = get_string('sessionadded','scheduler');
        $_SESSION['error_class'] = 'success';
    		closeColorboxpopup();
		die;
       // redirect($returnurl);
       // echo $OUTPUT->heading(get_string('oneslotadded', 'scheduler'));
    } else {
       // echo $OUTPUT->heading(get_string('addsingleslot', 'scheduler'));
       echo '<div class="add_session_popup">';
        $mform->display();
        echo '</div>';
        echo '<script>
        
$("#id_resetbutton").on("click",function(event){
       
	$("span.error").remove();
	$("br.error").remove();
        
	$("div").removeClass("error");
    $("fieldset").removeClass("error");
     $("fieldset br").remove(); 		
});
</script>';
        echo $OUTPUT->footer($course);
        die;
    }
    // return code for include
  //  return -1;
}
/************************************ View : Update single slot form ****************************************/
if ($action == 'updateslot') {

    $slotid = required_param('slotid', PARAM_INT);
    $slot = $DB->get_record('scheduler_slots', array('id' => $slotid));
    
   
    $data = scheduler_prepare_formdata($scheduler, $slot);

    if($CFG->is12HourFormat==1){
    	$data->timestartformat = date('A',$slot->starttime);
    	$data->starttime = setTimeAsPerTimeFormat($slot->starttime);
    }
   
    $actionurl = new moodle_url('/mod/scheduler/view.php',
                    array('what' => 'updateslot', 'id' => $cm->id, 'slotid' => $slotid, 'page' => $page, 'offset' => $offset));
    $returnurl = new moodle_url('/mod/scheduler/view.php',
                    array('what' => 'view', 'id' => $cm->id, 'page' => $page, 'offset' => $offset));

    $mform = new scheduler_editslot_form($actionurl, $scheduler, $cm, $usergroups, array('slotid' => $slotid));
   
    $data->web_access_link = array('text'=>$data->web_access_link);
   $mform->set_data($data);

    if ($mform->is_cancelled()) {
        redirect($returnurl);
    } else if ($formdata = $mform->get_data()) {
    	
        scheduler_save_slotform ($scheduler, $course, $slotid, $formdata);
        $_SESSION['update_msg'] = get_string('sessionupdated','scheduler');
        $_SESSION['error_class'] = 'success';
        
   		closeColorboxpopup();
        die;
      
       // echo $OUTPUT->heading(get_string('slotupdated', 'scheduler'));
    } else {
    	
        //echo $OUTPUT->heading(get_string('updatesingleslot', 'scheduler'));
        echo '<div class="add_session_popup">';
        $mform->display();
        echo '</div>';
        echo '<script>

$("#id_resetbutton").on("click",function(event){

	$("span.error").remove();
	$("br.error").remove();
        
	$("div").removeClass("error");
    $("fieldset").removeClass("error");
    $("fieldset br").remove(); 	
});
       		
</script>';
      
        echo $OUTPUT->footer($course);
        die;
    }
 

}
/************************************ Add session multiple slots form ****************************************/
if ($action == 'addsession') {

    $actionurl = new moodle_url('/mod/scheduler/view.php',
                    array('what' => 'addsession', 'id' => $cm->id, 'page' => $page));
    $returnurl = new moodle_url('/mod/scheduler/view.php',
                    array('what' => 'view', 'id' => $cm->id, 'page' => $page));

    $mform = new scheduler_addsession_form($actionurl, $scheduler, $cm, $usergroups);

    if ($mform->is_cancelled()) {
        redirect($returnurl);
    } else if ($formdata = $mform->get_data()) {
    	
        scheduler_action_doaddsession($scheduler, $formdata);
    } else {
        echo $OUTPUT->heading(get_string('addsession', 'scheduler'));
        $mform->display();
        
        echo $OUTPUT->footer($course);
        die;
    }
}
/************************************ Schedule a student form ***********************************************/

/************************************ Schedule a whole group in form ***********************************************/

//****************** Standard view ***********************************************//


/// print top tabs

$tabrows = array();
$row  = array();




/// print heading
//echo $OUTPUT->heading(format_string($scheduler->name), 2);

$session_list = classSessionList($id);


$stredit = get_string('edit');
$strdelete = get_string('delete');
$strattendance = get_string('attendance','scheduler');


$courseHTML = '';
if($_SESSION['update_msg']!=''){
	$courseHTML .= '<div class="clear"></div>';
	$courseHTML .= '<div class="bg-success text-success text-center '.$_SESSION['error_class'].'" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
	$courseHTML .= '<div class="clear"></div>';
	$_SESSION['update_msg']='';
}
$courseHTML .=  "<div class='tabsOuter'>";

$courseHTML .=   "<div class='tabLinks'>";
include_once($CFG->dirroot.'/course/course_tabs.php');

$courseHTML .=   "</div>";
$courseHTML .= '<div class="clear"></div><div class="userprofile view_assests">';

$classroomCourseId = $DB->get_field_sql("SELECT course FROM {$CFG->prefix}course_modules WHERE id = '".$id."'");
$addClassAccess = haveClassroomCourseAccess($id);
if($addClassAccess == $CFG->classroomFullAccess){
    $courseHTML .=  '<div class = "add-course-button"><a class="addbutton " id="add-assests" href="'.$CFG->wwwroot.'/mod/scheduler/view.php?id='.$id.'&what=addslot" title="'.get_string('addsingleslot', 'scheduler').'"><i></i><span>'.get_string('addsingleslot', 'scheduler').'</span></a></div>';
}



/// route to screen
$courseHTML .=  '<table class = "table1"><tr>';
$courseHTML .=  "<td width = '23%' align='align_left' >".get_string('sessionname','scheduler')."</td>";
//$courseHTML .=  "<td width = '22%' align='align_left' >Description</td>";
$courseHTML .=  "<td width = '20%' align='align_left' >".get_string('duration_date')."</td>";
//$tableHtml .= "<td width = '15%' align='align_left' >$createdby</td>";
$courseHTML .=  "<td width = '30%' align='align_left' >".get_string('manage')."</td>";
$courseHTML .=  '</tr>';
if (!$session_list) {
	$courseHTML .=  "<tr><td colspan = '3'>".get_string('no_results')."</td></tr>";
	$table = NULL;
} else {
	$returnurl = $CFG->wwwroot.'/mod/scheduler/view.php';
	foreach ($session_list as $session) {
			
	
		$buttons = array();
		$buttons[] = html_writer::link(new moodle_url($returnurl, array('what'=>'updateslot', 'id'=>$id,'slotid'=>$session->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>$stredit, 'class'=>'iconsmall')), array('title'=>get_string('edit'), 'class'=>'edit'));
		$buttons[] = html_writer::link(new moodle_url($returnurl, array('what'=>'deleteslot', 'id'=>$id,'slotid'=>$session->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/delete'), 'alt'=>$strdelete, 'class'=>'iconsmall')), array('title'=>get_string('delete'), 'class'=>'delete'));
		$buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/mod/scheduler/learner_grade.php', array('id'=>$id,'slotid'=>$session->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/view'), 'alt'=>$strattendance, 'class'=>'iconsmall')), array('title'=>$strattendance, 'class'=>'view'));
		$courseHTML .= '<td class="align_left"><span class="f-left">'.$session->sessionname.'</span></td>';
	
			
		
		$end_time =endTimeByDuration($session->starttime,$session->duration);
		$duration_text = date('M d, Y h:i A',$session->starttime).' to '.date('h:i A',$end_time);
		$courseHTML .=  "<td>".$duration_text."</td>";
		$courseHTML .=  "<td>".implode(' ', $buttons)."</td>";
		$courseHTML .=  "</tr>";
	}
}
$courseHTML .=  "</table></div>";
// teacher side

if (!empty($courseHTML)) {
	echo '<div class = "course-listing">';

	echo html_writer::start_tag('div', array('class'=>'no-overflow'));
	echo $courseHTML;
	echo html_writer::end_tag('div');
	echo html_writer::end_tag('div');

	//Pring paging bar
	echo paging_bar($programCount, $page, $perpage, $genURL);
}
echo "</div>";

/// print page












/// some slots already exist


/// print table of students needing an appointment
?>

