<?PHP

/**
 * This page prints a particular instance of scheduler and handles
 * top level interactions
 *
 * @package mod
 * @subpackage scheduler
 * @copyright 2011 Henning Bostelmann and others (see README.txt)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once (dirname ( __FILE__ ) . '/../../config.php');
require_once ($CFG->dirroot . '/mod/scheduler/lib.php');
require_once ($CFG->dirroot . '/mod/scheduler/locallib.php');

$id = required_param ( 'id', PARAM_INT ); // Course Module ID, or

$delete = optional_param ( 'delete', 0, PARAM_INT );
$documentid = optional_param ( 'documentid', 0, PARAM_INT );

$return_url = $CFG->wwwroot . "/mod/scheduler/learner_grade.php?id=" . $id;
if ($delete) {
	
	deleteAttendenceDocument ( $documentid );
	
	// redirect($return_url);
}
if (isset ( $_REQUEST ['submit'] )) {
	savePerformanceData($_REQUEST);	
	$_SESSION ['update_msg'] = get_string ( 'attendancesuccess', 'scheduler' );
	$_SESSION ['error_class'] = 'success';
}
if (isset ( $_REQUEST ['saveandsubmit'] )) {
	
	savePerformanceData($_REQUEST,1);
	updateClassCompletion($_REQUEST['classid']);
	$_SESSION ['update_msg'] = get_string ( 'classcompletionsuccess', 'scheduler' );
	$_SESSION ['error_class'] = 'success';
}
if ($id) {
	if (! $cm = get_coursemodule_from_id ( 'scheduler', $id )) {
		print_error ( 'invalidcoursemodule' );
	}
	
	if (! $course = $DB->get_record ( 'course', array (
			'id' => $cm->course 
	) )) {
		print_error ( 'coursemisconf' );
	}
	
	if (! $scheduler = $DB->get_record ( 'scheduler', array (
			'id' => $cm->instance 
	) )) {
		print_error ( 'invalidcoursemodule' );
	}
}
$disable_field = "";
if($scheduler->isclasscompleted==1)
{
	$disable_field = 'disabled="disabled"';
}
$session_list = classSessionList ( $id );
$learner_list = classLearnerList ( $id );
$class_details = getClassDetailsFromModuleId($scheduler->id);

// $courseTypeIdInTab = getCourseTypeIdByCourseId($course->id);
checkUserAccess ( 'class', $scheduler->id );

$PAGE->set_url ( '/mod/scheduler/learner_grade.php', array (
		'id' => $id 
) );
$PAGE->set_pagelayout ( 'classroompopup' );

$PAGE->navbar->add ( get_string ( 'managecourses' ), new moodle_url ( $CFG->wwwroot . '/course/index.php' ) );
$PAGE->navbar->add ( get_string ( 'editcourse', 'course' ), new moodle_url ( $CFG->wwwroot . '/course/edit.php', array (
		'id' => $course->id 
) ) );
// $PAGE->navbar->add(get_string('manage_assets'));
$PAGE->navbar->add ( get_string ( 'schedulingandtracking' ), new moodle_url ( $CFG->wwwroot . '/mod/scheduler/list.php', array (
		'id' => $course->id 
) ) );
$PAGE->navbar->add ( get_string ( 'sessionlink', 'scheduler' ), new moodle_url ( $CFG->wwwroot . '/mod/scheduler/view.php', array (
		'id' => $id 
) ) );
$PAGE->navbar->add ( get_string ( 'attendance', 'scheduler' ) );

// $title = $course->shortname . ': ' . format_string($scheduler->name);
$PAGE->set_title ( $title );
$PAGE->set_heading ( $course->fullname );

echo $OUTPUT->header ();
$courseHTML = '';
if ($_SESSION ['update_msg'] != '') {
	$courseHTML .= '<div class="clear"></div>';
	$courseHTML .= '<div class="bg-success text-success text-center ' . $_SESSION ['error_class'] . '" style = "display:block !important;">' . $_SESSION ['update_msg'] . '</div>';
	$courseHTML .= '<div class="clear"></div>';
	$_SESSION ['update_msg'] = '';
}

$headerHtml = getModuleHeaderHtml($class_details, $CFG->classModule);
$courseHTML .= "<div class='borderBlockSpace'><div class='attendance-performance'>";

$courseHTML .= $headerHtml;
$grade_rec = getGradeData ();
$courseHTML .= '<div class="clear"></div>';
if (! $learner_list || !$session_list) {
   $courseHTML .=  get_string('no_results');
}
else{
// / route to screen
$courseHTML .= '<a class="print_icon pull-right" title="'.get_string('print','classroomreport').'" id="printbun" href="javascript: void(0);"  rel="'.$CFG->wwwroot.'/mod/scheduler/learner_grade_print.php?id='.$id.'">Print</a><form action="" method="post" id="gradeform"><div id="attendence_data"><table class = "table1" id="table1">';
$courseHTML .= '<input type="hidden" name="classid" value="'.$scheduler->id.'" />';
$courseHTML .= '<tr class="headingRow"><td width="14%">'.get_string("fullname","classroomreport").'</td><td width="10%" class="bor_left">&nbsp;</td>';
if($session_list){
$courseHTML .= '';
}
$courseHTML .= '<td width="35%" style="padding:0"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="margin_none">';
if(count($session_list)>=1){
	$courseHTML .= '<tr><td  class="bor_left" style="text-align:center; border-bottom:1px solid #b9b9b9" colspan="'.count($session_list).'">'.get_string("sessionstext","scheduler").'</td></tr>';		
}
$courseHTML .= '<tr>';
foreach ( $session_list as $session ) {
    $sessionDuration = getClassroomSessionDateDuration($session->starttime, $session->duration); 
	$sessionName = $session->sessionname."<br>(". $sessionDuration.")";
	$courseHTML .= "<td width='33%' class='bor_left'>" . $sessionName . "</td>";
}
$courseHTML .= '</tr>';
$courseHTML .= '</table></td>';
$courseHTML .= "<td width = '15%' align='align_left'  class='bor_left'>" . get_string ( 'remarks', 'scheduler' ) . "</td>";
$courseHTML .= "<td width = '10%' align='align_left'  class='bor_left'>" . get_string ( 'classcompleted', 'scheduler' ) . "</td>";
$courseHTML .= "<td width = '15%' align='align_left'  class='bor_left'>".get_string('action')."</td>";
$courseHTML .= '</tr>';

if (! $learner_list) {
	$courseHTML .= "<tr><td colspan = '3'>" . get_string ( 'no_results' ) . "</td></tr>";
	$table = NULL;
} else {
	foreach ( $learner_list as $learner ) {		
		
		$document_url = $CFG->wwwroot . "/mod/scheduler/upload_document.php?id=" . $learner->scheduler_id . "&studentid=" . $learner->userid;
		// print_object($learner);
		$file_container_id = $learner->scheduler_id.'_'.$learner->userid;
		$courseHTML .= "<tr class='rep_row'><td class='align_left'  width = '14%'><span class='f-left'><input type='hidden' name='userid[" . $learner->id . "]' value='" . $learner->userid . "' />" . $learner->firstname." ".$learner->lastname . "</span></td>";
		if($session_list){
		$courseHTML .= "<td width = '10%' class='gradeCell'><div class='att'>" . get_string ( 'attended', 'scheduler' ) . "</div><div class='score'>" . get_string ( 'score', 'scheduler' ) . "</div><div>" . get_string ( 'grade', 'scheduler' ) . "</div></td>";
		}
		$courseHTML .= '<td class="sessionCell"><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>';
		foreach ( $session_list as $session ) {
			$session_rec = $DB->get_record ( 'scheduler_appointment', array (
					'slotid' => $session->id,
					'studentid' => $learner->userid 
			) );
			$courseHTML .= "<td  width='33%'><div class='att'><input type='checkbox' ".$disable_field." name='attended[" . $session->id . "][" . $learner->userid . "]' value='1' " . is_checked ( $session_rec->attended, '1', 'checkbox' ) . " /></div><div class='score'><input type='text' name='score[" . $session->id . "][" . $learner->userid . "]' ".$disable_field."   onkeypress='return numbersonly(event,this, 3)' value='" . $session_rec->score . "' class='attendance_textbox' /></div><div><input type='text' name='grade[" . $session->id . "][" . $learner->userid . "]'   onkeypress='return allowOnlyGrade(event,this,2)' value='" . $session_rec->grade . "' ".$disable_field." class='attendance_textbox' /></div></td>";
		}
		$courseHTML .= '</tr></table></td>';
		$courseHTML .= '<td class="align_left"><textarea name="remarks[' . $learner->userid . ']" '.$disable_field.'>' . $learner->remarks . '</textarea></td>';
		$courseHTML .= '<td class="align_left"><input type="checkbox" '.$disable_field.' name="is_completed[' . $learner->userid . ']"' . is_checked ( $learner->is_completed, "1", "checkbox" ) . ' value="1" /></td>';
		$courseHTML .= "<td>";
		if($scheduler->isclasscompleted==0)
		{
		$courseHTML .= "<a class='uploadFileBtn' onclick='uploadDocuments(\"$document_url\")'  href='#' title='".get_string('uploadfile','scheduler')."'>".get_string('uploadfile','scheduler')."</a>";
		}
		
		$allFiles = getAllFiles ( $scheduler->id, $learner->userid );
		
		$courseHTML .= "<div class='files_container' id='".$file_container_id."'>";
		foreach ( $allFiles as $file ) {
			$title = $file->title;
			if($title==""){
				$title = $file->original_file_name;
			}
			//$courseHTML .= "<div class='file_div_".$file->id."'><a class='upfile_name' href='" . $CFG->wwwroot . "/upload/" . $file->file_name . "' target='_blank'>" . $title. "</a> <a class='upfile_delete' href='" . $CFG->wwwroot . "/mod/scheduler/learner_grade.php?id=" . $id . "&documentid=" . $file->id . "&delete=1' rel='".$file->id."' >Delete</a></div>";
			$courseHTML .= "<div class='file_div_".$file->id."'><a class='upfile_name' href='" . $CFG->wwwroot . "/upload/" . $file->file_name . "' target='_blank'>" . $title. "</a>";
			if($scheduler->isclasscompleted==0){
			 $courseHTML .= "<a class='upfile_delete'  rel='".$file->id."' title=".get_string('delete').">Delete</a>";
			}
			 $courseHTML .= "</div>";
		}
		$courseHTML .= "</div>";
		$courseHTML .= "</td>";
		$courseHTML .= "</tr>";
	}
//	$courseHTML .= "<tr><td colspan='4'><input type='submit' name='submit' value='Save & Continue' /></td></tr>";
}

$courseHTML .= "</table></div>";
if ($learner_list && $scheduler->isclasscompleted==0) {
	
		$courseHTML .="<input type='submit' name='submit' value='Save' />";
		if($scheduler->enddate<time())
		{	
		$courseHTML .="<input type='submit' name='saveandsubmit' id='saveandsubmit' value='Save & Submit' />";
		}
		 else{
		$courseHTML .="<input type='submit' name='saveandsubmit' id='saveandsubmit' value='Save & Submit' disabled='disabled' />";
		} 
	
}
 else{
	$courseHTML .="<input type='submit' name='submit' value='Save' disabled='disabled' />";	
	$courseHTML .="<input type='submit' name='saveandsubmit' id='saveandsubmit' value='Save & Submit' disabled='disabled' />";
} 
		$courseHTML .="</form>";
}
//$courseHTML .="</div>";
// teacher side

if (! empty ( $courseHTML )) {
	//echo '<div class = "course-listing">';
	
	echo html_writer::start_tag ( 'div', array (
			'class' => '' 
	) );
	echo $courseHTML;
	echo html_writer::end_tag ( 'div' );
	//echo html_writer::end_tag ( 'div' );
	
	// Pring paging bar
	echo paging_bar ( $programCount, $page, $perpage, $genURL );
}
echo "</div></div>";
echo $OUTPUT->footer ( $course );

?>
<script>



function popUpClosed(file_id) {
	
	$.ajax({
		url:'<?php $CFG->wwwroot;?>/mod/scheduler/ajax.php',
		type:'POST',
		data:"action=getDocumentWithoutRefresh&filecontainer_id="+file_id,
		success:function(data){
				
					
					$("#"+file_id).html(data);
					
				
		}
	});
   // window.location.reload();
}
function uploadDocuments(url) {	
    var myWindow = window.open(url, "", "width=500, height=400");
  /*   myWindow.onbeforeunload = function () {
       location.reload();
    } */
}
function numbersonly(e,obj,length){
	var unicode=e.charCode? e.charCode : e.keyCode
			
	if (unicode!=8){ //if the key isn't the backspace key (which we should allow)
		if (unicode<48||unicode>57) //if not a number
		return false //disable key press
		if (obj.value.length>=length)
			return false;
		}

	
		
	}

function allowOnlyGrade(e,obj,length)
{
	var unicode=e.charCode? e.charCode : e.keyCode;
	if (unicode!=8){
	 var ch = String.fromCharCode(unicode);
     var filter = /[a-zA-Z]/   ;
     if(!filter.test(ch) && unicode!='45' && unicode!='43'){
         
    	 return false;
     }
     
     if (obj.value.length>=length)
 		return false;
	}	
 	
	
}

	

function limitlength(obj, length){
	var maxlength=length
	if (obj.value.length>maxlength)
	obj.value=obj.value.substring(0, maxlength)
	}

function deleteDocumentConfirm(){
	var r = confirm("<?php echo get_string('classcompletionconfirmation','scheduler')?>");
    if (r == true) {
        return true;
    } else {
        return false;
    }	
}

$(document).ready(function(){
	$("#printbun").bind("click", function(e) {
		var url = $(this).attr("rel");
		window.open(url, "", "width=400, height=400, top=100, left=100, scrollbars=1");
	});
	$(document).on('click', '.upfile_delete', function () {
		var r = confirm("<?php echo get_string('documentdeleteconfirm','scheduler')?>");
	    if (r == true) {
	    	var fileid = $(this).attr("rel");
	    	
	    
	    	$.ajax({
				url:'<?php $CFG->wwwroot;?>/mod/scheduler/ajax.php',
				type:'POST',
				data:"action=deleteDocument&documentid="+fileid,
				success:function(data){
						if(data == 'success'){
							var okTxt = '<?php echo get_string('documentdeleted','scheduler');?>';
							alert(okTxt);
							$(".file_div_"+fileid).remove();
							
						}else{
							alert("unable to process request. Please try again later");
						}
				}
			});
	    	
	    }
	    return false;
		 });
	$(document).on('click', '#saveandsubmit', function () {
		var r = confirm("<?php echo get_string('classcompletionconfirmation','scheduler')?>");
	    if (r == true) {
		    
	    	return true;
	    	
	    }
	    return false;
		 });
	

	});

</script>