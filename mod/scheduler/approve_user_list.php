<?PHP  

/**
 * This page prints a particular instance of scheduler and handles
 * top level interactions
 * 
 * @package    mod
 * @subpackage scheduler
 * @copyright  2011 Henning Bostelmann and others (see README.txt)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->dirroot.'/mod/scheduler/lib.php');
require_once($CFG->dirroot.'/mod/scheduler/locallib.php');

$PAGE->set_url('/mod/scheduler/list.php', array('id' => $id));
require_login();
//$title = $course->shortname . ': ' . format_string($scheduler->name);
$PAGE->set_title($title);
$PAGE->set_heading($course->fullname);

echo $OUTPUT->header();
$courseHTML = '';
if($_SESSION['update_msg']!=''){
	$courseHTML .= '<div class="clear"></div>';
	$courseHTML .= '<div class="bg-success text-success text-center '.$_SESSION['error_class'].'" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
	$courseHTML .= '<div class="clear"></div>';
	$_SESSION['update_msg']='';
}
$courseHTML .=  "<div class='tabsOuter'>";

$courseHTML .= '<div class="clear"></div><div class="userprofile view_assests">';

//$getDepartmentUsers = getDepartmentUsers($USER->department);
$invitedUserList = getInvitedUsers();

/// route to screen
$courseHTML .=  '<table class = "table1"><tr>';
$courseHTML .=  "<td width = '23%' align='align_left' >User Name</td>";
$courseHTML .=  "<td width = '23%' align='align_left' >Class Name</td>";
$courseHTML .=  "<td width = '20%' align='align_left' >Status</td>";

$courseHTML .=  "<td width = '30%' align='align_left' >".get_string('manage')."</td>";
$courseHTML .=  '</tr>';
	if (!$invitedUserList) {
		$courseHTML .=  "<tr><td colspan = '3'>".get_string('no_results')."</td></tr>";
		$table = NULL;
	} else {
		foreach ($invitedUserList as $invitedUser) {
			$buttons = array();
			
			$approve_view = '<a href="'.$CFG->wwwroot.'/mod/scheduler/approve_user.php?id='.$invitedUser->appoint_id.'" class="view" title="'.get_string('approveuser','scheduler').'">'.get_string('approveuser','scheduler').'</a>';
			
			
			$buttons[] = $approve_view;
			
			
			$courseHTML .= '<tr>';
			$courseHTML .= '<td class="align_left"><span class="f-left">'.$invitedUser->firstname.' '.$invitedUser->lastname.'</span></td>';
		
			//$description = $program->description;
			//$courseHTML .=  "<td>".nl2br($description)."</td>";
			$courseHTML .=  "<td>".$invitedUser->name."</td>";
			$courseHTML .=  "<td>".$invitedUser->is_approved."</td>";
			$courseHTML .=  "<td>".$invitedUser->scheduler_id.'---'.implode(' ', $buttons)."</td>";
			
			
			$courseHTML .=  "</tr>";
		}
	}
	$courseHTML .=  "</table></div>";
// teacher side
    
  if (!empty($courseHTML)) {
		echo '<div class = "course-listing">';
		
        echo html_writer::start_tag('div', array('class'=>'no-overflow'));
        echo $courseHTML;
        echo html_writer::end_tag('div');
        echo html_writer::end_tag('div');

		//Pring paging bar
        echo paging_bar($programCount, $page, $perpage, $genURL);
    }
    echo "</div>";
echo $OUTPUT->footer($course);

?>
