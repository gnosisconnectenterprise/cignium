<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Form classes for editing badges
 *
 * @package    core
 * @subpackage badges
 * @copyright  2012 onwards Totara Learning Solutions Ltd {@link http://www.totaralms.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author     Yuliya Bozhko <yuliya.bozhko@totaralms.com>
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');
require_once($CFG->libdir . '/badgeslib.php');
require_once($CFG->libdir . '/filelib.php');

/**
 * Form to edit badge details.
 *
 */
class edit_details_form extends moodleform {

    /**
     * Defines the form
     */
    public function definition() {
        global $CFG,$DB,$USER;

        $mform = $this->_form;
        $badge = (isset($this->_customdata['badge'])) ? $this->_customdata['badge'] : false;
        $action = $this->_customdata['action'];

		if(!empty($badge->id)){
				if($badge->picture != ''){
					$view =  $CFG->dirroot.'/theme/gourmet/pix/badge/'.$badge->picture;
					$view1 =  $CFG->wwwroot.'/theme/gourmet/pix/badge/'.$badge->picture;
					//echo $view;die;
					if(file_exists($view) && $badge->picture !=''){
						$mform->addElement('html','<div class="fitem"><div class="fitemtitle"><label for="id_image">'.get_string("current_image").'</label></div>');
						$mform->addElement('html','<div class="felement fstatic"><a href="#"><img width="64" height="64" class="userpicture defaultuserpic" src="'.$view1.'"></a>');
						//$mform->addElement('html','<a class = "removeimage" rel = "'.$badge->id.'"><span id="'.$badge->id.'" rel="login">'.get_string("delete").'</span></a>');
						$mform->addElement('html','</div></div>');
					}
				}
			}
        $mform->addElement('text', 'name', get_string('name','badges'), array('size' => '70','maxlength'=>50));

        // Using PARAM_FILE to avoid problems later when downloading badge files.
        $mform->setType('name', PARAM_FILE);
        $mform->addRule('name', get_string('required'), 'required');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        $mform->addElement('textarea', 'description', get_string('description', 'badges'), 'wrap="virtual" rows="8" cols="70"');
        $mform->setType('description', PARAM_CLEANHTML);
        $mform->addRule('description', get_string('required'), 'required');

        
		$mform->addElement('file', 'badge_image',get_string('badge_image','badges'));
		if(!empty($badge->id)){
		}else{
			$mform->addRule('badge_image', get_string('required'), 'required');
		}
		if($USER->archetype!="admin"){
				$badgesWhere = "and createdby='".$USER->id."'";
			}else{
				$badgesWhere = "";
			}	
		
		$courses = $DB->get_records_sql('SELECT id, fullname FROM mdl_course WHERE is_active = 1 '.$badgesWhere.' ORDER BY fullname ASC');	
        $courseselect = array('0'=>'Select course');
		foreach($courses as $course){
			$courseselect[$course->id] = $course->fullname;
		}
		$mform->addElement('select', 'courseid', get_string('course'), $courseselect);

        $mform->addElement('text', 'issuername', get_string('issuername','badges'), array('size' => '70','maxlength'=>100));
        $mform->setType('issuername', PARAM_NOTAGS);
        $mform->addRule('issuername', get_string('required'), 'required');
        if (isset($CFG->badges_defaultissuername)) {
            $mform->setDefault('issuername', $CFG->badges_defaultissuername);
        }
        //$mform->addHelpButton('issuername', 'issuername', 'badges');

        $mform->addElement('text', 'issuercontact', get_string('email'), array('size' => '70','maxlength'=>100));
        if (isset($CFG->badges_defaultissuercontact)) {
            $mform->setDefault('issuercontact', $CFG->badges_defaultissuercontact);
        }
        $mform->setType('issuercontact', PARAM_RAW);
		$mform->addElement('html', '<input type="hidden" id="id_expiry_0" checked="checked" value="0" name="expiry">');

        // Set issuer URL.
        // Have to parse URL because badge issuer origin cannot be a subfolder in wwwroot.
        $url = parse_url($CFG->wwwroot);
        $mform->addElement('hidden', 'issuerurl', $url['scheme'] . '://' . $url['host']);
        $mform->setType('issuerurl', PARAM_URL);

        $mform->addElement('hidden', 'action', $action);
        $mform->setType('action', PARAM_TEXT);

        if ($action == 'new') {
            $showResetButton = true;
        } else {
            // Add hidden fields.
            $mform->addElement('hidden', 'id', $badge->id);
            $mform->setType('id', PARAM_INT);

        }
		
		$showResetButton = true;
		if(!empty($badge->id)){
		  $showResetButton = false;
		}
        $this->add_action_buttons(true, null, $showResetButton);
    }

    /**
     * Validates form data
     */
    public function validation($data, $files) {
        global $DB;
        $errors = parent::validation($data, $files);

        if (!empty($data['issuercontact']) && !validate_email($data['issuercontact'])) {
            $errors['issuercontact'] = get_string('invalidemail');
        }

        if ($data['expiry'] == 2 && $data['expireperiod'] <= 0) {
            $errors['expirydategr'] = get_string('error:invalidexpireperiod', 'badges');
        }

        if ($data['expiry'] == 1 && $data['expiredate'] <= time()) {
            $errors['expirydategr'] = get_string('error:invalidexpiredate', 'badges');
        }

        // Check for duplicate badge names.
        if ($data['action'] == 'new') {
            $duplicate = $DB->record_exists_select('badge', 'name = :name AND status != :deleted',
                array('name' => $data['name'], 'deleted' => BADGE_STATUS_ARCHIVED));
        } else {
            $duplicate = $DB->record_exists_select('badge', 'name = :name AND id != :badgeid AND status != :deleted',
                array('name' => $data['name'], 'badgeid' => $data['id'], 'deleted' => BADGE_STATUS_ARCHIVED));
        }

        if ($duplicate) {
            $errors['name'] = get_string('error:duplicatename', 'badges');
        }

        return $errors;
    }
}