<?php
require_once(dirname(dirname(__FILE__)) . '/config.php');
require_once($CFG->libdir . '/badgeslib.php');

$perpage   = optional_param('perpage', 10, PARAM_INT);
$page      = optional_param('page', 1, PARAM_INT);
$id      = optional_param('id', 0, PARAM_INT);
$delete      = optional_param('delete', 0, PARAM_INT);
$confirm      = optional_param('confirm', 0, PARAM_INT);

require_login();


$hdr = get_string('managebadges', 'badges');
$returnurl = new moodle_url('/badges/index.php', $urlparams);
$PAGE->set_context($context);
$PAGE->set_url('/badges/index.php', $params);
$PAGE->set_pagelayout('learnerdashboard');
$PAGE->set_pagetype('my-index');
$PAGE->blocks->add_region('content');
$PAGE->set_title($hdr);
$PAGE->set_heading($hdr);
$PAGE->navbar->add(get_string('managebadges',"badges"));
echo $OUTPUT->header();
echo '<div style="text-align:center; font-size:30px">';
echo "Coming Soon...";
echo '</div>';
/*
$PAGE->set_url($returnurl);
$PAGE->set_pagelayout('course');

$PAGE->set_heading($title . ': ' . $hdr);
$PAGE->set_title($hdr);
$action = optional_param('action', 2, PARAM_INT);
$paramArray = array(
					'ch' => optional_param('ch', '', PARAM_ALPHA),
					'key' => optional_param('key', '', PARAM_RAW),
					'name' => optional_param('name', '', PARAM_RAW),
					'des' => optional_param('des', '', PARAM_RAW),
					'issuename' => optional_param('issuename', '', PARAM_RAW)
					);
$filterArray = array(
						'name'=>get_string("only_name","badges"),
						'des'=>get_string("only_description","badges"),
						'issuename'=>get_string("issuername","badges")
				   );
$pageURL = '/badges/'.$CFG->pageManageBadges;
$addBadgeURL = '/badges/'.$CFG->pageEditBadges;
$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
$addBadgeGenURL = genParameterizedURL($paramArray, $removeKeyArray, $addBadgeURL);
if($action=='1')
{
	if($id != 0){
		$newdiscussion = new stdClass();
		$newdiscussion->status=1;
		$newdiscussion->id=$id;
		$updid = $DB->update_record("badge", $newdiscussion);
		$_SESSION['update_msg']=get_string('record_updated');
	}
	redirect($genURL);
}elseif($action=='0')
{
	if($id != 0){
		$newdiscussion = new stdClass();
		$newdiscussion->status=0;
		$newdiscussion->id=$id;
		$updid = $DB->update_record("badge", $newdiscussion);
		$_SESSION['update_msg']=get_string('record_updated');
	}
	redirect($genURL);
}
if ($delete && $delete != 0) {
    $badge = new badge($delete);
    if (!$confirm) {
		$PAGE->navbar->add(get_string('managebadges',"badges"),$genURL);
		$PAGE->navbar->add(get_string('delbadge',"badges"));
		echo $OUTPUT->header();
		$message = $badge->name.'</br>';
		$message .= get_string("delete_msg");
		echo '<div class="box generalbox" id="notice">
				<p>'.$message.'</p>
				<div class="buttons">
					<div class="singlebutton">
						<form action="'.$genURL.'" method="post">
							<div>
								<input type="submit" value="Yes">
								<input type="hidden" value="'.$delete.'" name="delete">
								<input type="hidden" value="1" name="confirm">
							</div>
						</form>
					</div>
					<div class="singlebutton">
						<form action="'.$genURL.'" method="post">
							<div>
								<input type="submit" value="No">
							</div>
						</form>
					</div>
				</div>
			</div>';
		echo $OUTPUT->footer();
		die;
    } else {
		$_SESSION['update_msg']=get_string('record_deleted');
		$last_id = $DB->delete_records_select('badge','id='.$badge->id);
        redirect($genURL);
    }
}


$PAGE->navbar->add(get_string('managebadges',"badges"),$genURL);
echo $OUTPUT->header();

$badges	= getBadgesList('name','ASC',$page, $perpage, $paramArray);
$badgecount	= getBadgesList('badgecount','ASC', $page, $perpage, $paramArray);


echo genCommonSearchForm($paramArray, $filterArray, $pageURL);
echo '<a class="addbutton adddepartment" href="'.$CFG->wwwroot.'/badges/'.$CFG->pageEditBadges.'" title="'.get_string("add_badge","badges").'"><i></i><span>'.get_string("add_badge","badges").'</span></a>';
if($_SESSION['update_msg']!=''){
		echo '<div class="clear"></div>';
		echo '<div class="bg-success text-success text-center" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
		echo '<div class="clear"></div>';
		$_SESSION['update_msg']='';
}
echo '<div class = "course-listing">';
echo "<h2>".get_string('badges',"badges")." (".$badgecount.")</h2>";
echo '<table class="table1" cellpadding="0" cellspacing="0" style="width: 100%;">
		<tr class="table1_head">
			<td width="15%" class="align_left">'.get_string("only_name","badges").'</td>
			<td width="30%" class="align_left">'.get_string("only_description","badges").'</td>
			<td width="15%" class="align_left">'.get_string("issuername","badges").'</td>
			<td width="20%" class="align_left">'.get_string("timecreated","badges").'</td>
			<td width="20%" align="align_left">'.get_string("manage").'</td>
		</tr>';
if(!empty($badges)){
	foreach($badges as $badge){
			$editDisable = 'edit';
			$chkParamFirst = strpos($genURL, '?');
			if($chkParamFirst === false) {
				$linkUrl = $genURL.'?id='.$badge->id;
				$deleteLink = $genURL.'?delete='.$badge->id;
			} else {
				$linkUrl = $genURL.'&id='.$badge->id;
				$deleteLink = $genURL.'&delete='.$badge->id;
			}

			$chkParamFirst2 = strpos($addBadgeGenURL, '?');
			if($chkParamFirst2 === false) {
				$editLink = $addBadgeGenURL.'?id='.$badge->id;
			} else {
				$editLink = $addBadgeGenURL.'&id='.$badge->id;
			}
			if($badge->status=='0')
			{
				$disabledclass = 'class="disable"';
				$editDisable = 'edit-disable';
				$editLink = 'javascript:void(0);';
				$tooltip=get_string("activate");
				$status='<a href="'.$linkUrl.'&action=1" title="'.$tooltip.'" class = "enable" ></a>';
			}else{
				$disabledclass = '';
				$tooltip=get_string("inactivate");
				$status='<a href="'.$linkUrl.'&action=0" title="'.$tooltip.'" class = "disable" ></a>';
			}
			if(strlen($badge->description) >200){
				$description = substr($badge->description,0,200)." ...";
			}else{
				$description = $badge->description;
			}
			echo '<tr '.$disabledclass.'>
			<td class="align_left">'.$badge->name.'</td>
			<td class="align_left" >'.$description .'</td>
			<td class="align_left">'.$badge->issuername .'</td>
			<td class="align_left">'.date('dS Y M h:i:s A',$badge->timecreated) .'</td>
			<td align="center">
				<div class="adminiconsBar order">
									<a title="'.get_string("edit").'" class="'.$editDisable.'" href="'.$editLink.'">'.get_string("edit").'</a> '.$status.' <a title="'.get_string("delete").'" class="delete" href="'.$deleteLink.'">'.get_string("delete").'</a>';
				echo'</div>
			</td>
			</tr>';
		
	}
}else{
	echo "<tr><td colspan = '5'>".get_string('no_results')."</td></tr>";
}
echo '</table>';
echo '</div>';
$baseurl = new moodle_url('/badges/'.$CFG->pageManageBadges, array());
echo paging_bar($badgecount, $page, $perpage,$baseurl);
*/
echo $OUTPUT->footer();
