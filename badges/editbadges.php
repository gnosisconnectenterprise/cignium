<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Editing badge details, criteria, messages
 *
 * @package    core
 * @subpackage badges
 * @copyright  2012 onwards Totara Learning Solutions Ltd {@link http://www.totaralms.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author     Yuliya Bozhko <yuliya.bozhko@totaralms.com>
 */

require_once(dirname(dirname(__FILE__)) . '/config.php');
require_once($CFG->dirroot . '/badges/'.$CFG->pageEditBadgesForm);

$badgeid = optional_param('id',0, PARAM_INT);
global $DB,$USER;

require_login();

if (empty($CFG->enablebadges)) {
    print_error('badgesdisabled', 'badges');
}

//check role assign univmanager
if($USER->archetype != $CFG->userTypeAdmin)
{
	checkUserAccess("badges" , $badgeid);
}
	
$context = get_context_instance(CONTEXT_SYSTEM);

$PAGE->set_context($context);
$PAGE->set_url($currenturl);
$PAGE->set_heading(get_string('manage_badges','badges'));
$PAGE->set_title(get_string('manage_badges','badges'));
$PAGE->navbar->add(get_string('manage_badges','badges'),$CFG->wwwroot.'/badges/'.$CFG->pageManageBadges);
if($badgeid)
	$PAGE->navbar->add(get_string('edit_badge','badges'));
else
	$PAGE->navbar->add(get_string('add_badge','badges'));

$statusmsg = '';
$errormsg  = '';

if($badgeid){
	$badge = $DB->get_record('badge',array('id'=>$badgeid));
}

$form = new edit_details_form($currenturl, array('badge' => $badge));
$form->set_data($badge);

if ($form->is_cancelled()) {
    redirect(new moodle_url('/badges/'.$CFG->pageManageBadges));
} else if ($form->is_submitted() && $form->is_validated() && ($data = $form->get_data())) {
	$now = time();
	if(isset($data->courseid) && $data->courseid != 0){
		$courseid = $data->courseid;
		$fordb->type = 2;
		$fordb->courseid = $courseid;
	}else{
		$fordb->courseid = null;
		$fordb->type = 1;
	
	}
    $fordb->name = $data->name;
    $fordb->description = $data->description;
    $fordb->timemodified = $now;
    $fordb->usermodified = $USER->id;
    $fordb->issuername = $data->issuername;
    $fordb->issuerurl = $data->issuerurl;
    $fordb->issuercontact = $data->issuercontact;
    $fordb->expiredate = null;
    $fordb->expireperiod = null;
    $fordb->messagesubject = get_string('messagesubject', 'badges');
    $fordb->message = get_string('messagebody', 'badges',
            html_writer::link($CFG->wwwroot . '/badges/mybadges.php', get_string('mybadges', 'badges')));
    $fordb->attachment = 1;
	$fordb->status = 1;
    $fordb->notification = 0;
	if(!empty($_FILES) && $_FILES["badge_image"]["error"] == 0){
			$path = $CFG->dirroot.'/theme/gourmet/pix/badge/'; 
			if(!is_dir($path)){
				mkdir($path,0777);
				chmod($path,0777);
			}
			$imgname = time().'_'.$_FILES["badge_image"]["name"];
			$imgDataExists = array();
			if(isset($data->id) && $data->id!='' && $data->id!=0){
				$imgDataExists = $DB->get_record_sql("select * from mdl_badge where id = ".$data->id);
			}
			if(!empty($imgDataExists)){
				$img2 = $imgDataExists->picture;
				$path_old = $path.$img2;
				if($img2 != ''){
					if(file_exists($path_old)){
						@unlink($path_old);
					}
				}
				if(move_uploaded_file($_FILES["badge_image"]["tmp_name"], $path.$imgname)){
					$fordb->picture = $imgname;
				}
			}else{
				if(move_uploaded_file($_FILES["badge_image"]["tmp_name"], $path.$imgname)){
					$fordb->picture = $imgname;
				}
			}
		}
	if(isset($data->id) && $data->id!='' && $data->id!=0){
		$fordb->id = $data->id;
		$newid = $DB->update_record('badge', $fordb);
		$_SESSION['update_msg']=get_string('record_updated');
		redirect($CFG->wwwroot.'/badges/'.$CFG->pageManageBadges);	
	}else{
		$fordb->timecreated = $now;
		$fordb->usercreated = $USER->id;
		$newid = $DB->insert_record('badge', $fordb);
		$_SESSION['update_msg']=get_string('record_added');
		redirect($CFG->wwwroot.'/badges/'.$CFG->pageManageBadges);	
	}

        // Need to unset message_editor options to avoid errors on form edit.
        unset($badge->messageformat);
        unset($badge->message_editor);
        if ($badge->save()) {
			 badges_process_badge_image($badge, $form->save_temp_file('image'));
            $form->set_data($badge);
            $statusmsg = get_string('changessaved');
        } else {
            $errormsg = get_string('error:save', 'badges');
        }
}

echo $OUTPUT->header();

$form->display();

echo $OUTPUT->footer();