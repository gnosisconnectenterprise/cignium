<?php
require_once("../config.php");
require_once("lib.php");
global $DB,$CFG,$USER;
checkLogin();
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', $CFG->perpage, PARAM_INT);
$pageURL = '/reports/online_course_usage_report.php';
checkPageIsNumeric($pageURL,$_REQUEST['page']);
$sDepartment    = optional_param('department', '-1', PARAM_RAW);
$sTeam          = optional_param('team', '-1', PARAM_RAW);       
$sProgram       = optional_param('program', '-1', PARAM_RAW);     
$sCourse        = optional_param('course', '-1', PARAM_RAW);
$sType      = optional_param('type', '-1', PARAM_RAW);
$user_group      = optional_param('user_group','-1', PARAM_RAW);
$sel_mode      = optional_param('sel_mode', '1', PARAM_RAW);

$userStatus=optional_param('userstatus', '0', PARAM_RAW);
$sCountry    = optional_param('country', '-1', PARAM_RAW);
$sCity    = optional_param('city', '-1', PARAM_RAW);
$action      = optional_param('action', '', PARAM_RAW);
$sJobTitle = optional_param('job_title', '-1', PARAM_RAW);
$sLeadersName = optional_param('report_to', '-1', PARAM_RAW);

$sIsManagerYes =optional_param('is_manager_yes', '-1', PARAM_RAW);

if( $USER->archetype == $CFG->userTypeStudent ) {
	$groupCourses = checkOwnerAccess($USER->id);
	if(!$groupCourses)
	{
		redirect(new moodle_url('/my/dashboard.php'));
	}
}
checkLogin();
if($action == 'print'){
	$PAGE->set_pagelayout('print');
}else{
	$PAGE->set_pagelayout('reportlayout');
}

$PAGE->set_heading($SITE->fullname);
$PAGE->set_title($SITE->fullname.": ".get_string('courseusagesreport','multicoursereport'));
$PAGE->navbar->add(get_string('reports','multicoursereport'));
$PAGE->navbar->add(get_string('courseusagesreport','multicoursereport'));
$paramArray = array(
					'department' => $sDepartment,
					'team' => $sTeam,
					'program' => $sProgram,
					'course' => $sCourse,
					'type' => $sType,
					'sel_mode' => $sel_mode,
					'user_group' => $user_group,
					'userstatus'=>$userStatus,
                                        'country'=>$sCountry,
                                        'city'=>$sCity,
                                        'is_manager_yes'=>$sIsManagerYes,
                                        'report_to' => optional_param('report_to', '', PARAM_RAW),
                                        'job_title' => optional_param('job_title', '', PARAM_RAW),
                                        'ch' => optional_param('ch', '', PARAM_ALPHA),
                                        'key' => optional_param('key', '', PARAM_RAW),
				  );
if($action == 'exportcsv' || $action == 'print' || $action == 'exportpdf'){
	$returnHTML = onlineCourseUsageExport($paramArray,$page,$perpage,$action);
	$perpage = 0;
}
$removeKeyArray = array();
$sDepartmentArr = explode("@",$sDepartment);
$sCountryArr = explode("@",$sCountry);
$sTeamArr = explode("@",$sTeam);
$sCourseArr = explode("@",$sCourse);
$sProgramArr = explode("@",$sProgram);
$sTypeArr = explode("@",$sType);
$userStatusArr=explode("@",$userStatus);
$sCityArr = explode("@",$sCity);
$sJobTitleArr	= explode('@',$sJobTitle);
$sLeadersNameArr = explode('@',$sLeadersName);


$sisManagerArr = explode('@',$sIsManagerYes);

echo $OUTPUT->header();
$enrolledData = getOnlineCourseUsageData($paramArray,$page,$perpage,'*');


$enrolledDataorder = setdisplayorderofdata($enrolledData);
//pr($enrolledData);die;

$enrollentCount = getOnlineCourseUsageData($paramArray,$page,0,'count_graph');
//pr($enrollentCount);die;
$enrollentCountPagination = getOnlineCourseUsageData($paramArray,$page,0,'count');
$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);

$arrEnrollmentData['graph_heading'] = get_string('courseusagesreport','multicoursereport');
$arrEnrollmentData['total']['string'] = get_string('numberofcourses','multiuserreport');
$arrEnrollmentData['total']['value'] = $enrollentCount->total_enrollment;
$arrEnrollmentData['data'][0]['string'] = get_string('inprogress','multiuserreport');
$arrEnrollmentData['data'][0]['class'] = 'inprogress';
$arrEnrollmentData['data'][0]['value'] = $enrollentCount->in_progress;
$arrEnrollmentData['data'][1]['string'] = get_string('notstarted','multiuserreport');
$arrEnrollmentData['data'][1]['class'] = 'notstarted';
$arrEnrollmentData['data'][1]['value'] = $enrollentCount->not_started;
$arrEnrollmentData['data'][2]['string'] = get_string('completed','multiuserreport');
$arrEnrollmentData['data'][2]['class'] = 'completed';
$arrEnrollmentData['data'][2]['value'] = $enrollentCount->completed;
$arrEnrollmentData['url'] = $genURL;
$arrEnrollmentData['action'] = $action;
$total_records = 0;
if($enrollentCountPagination){
	$total_records = $enrollentCountPagination->total_records;
}
if($action !='print'){
    echo genCommonSearchForm($paramArray, $filterArray, $pageURL);
}

echo '<div class="tabsOuter borderBlockSpace">';
if($action == 'print'){
	echo $returnHTML;
}else{
	ob_start();
	require_once($CFG->dirroot . '/local/includes/multicoursereportsearch.php');
	$SEARCHHTML = ob_get_contents();
	ob_end_clean();
	echo $SEARCHHTML;
}


$manual_reminder_script = $CFG->wwwroot."/email/reminder_manual.php";
$reminderLinkTooltip = get_string('course_completion_reminder_email');
$reminderLinkName = get_string('course_completion_reminder_email_link_name');
//echo "<div class='reminder-link'><a href='#' title='".$reminderLinkTooltip."'>".$reminderLinkName."</a></div>";
//echo "<div class='reminder-success' style='display:none;'>Reminder has been sent.</div>";
echo '<div class="f-right">'.get_string('reports_heading','',getReportSyncTime()).'</div>';
echo '<div class="clear"></div>';
$PerformanceData = getGraphData($arrEnrollmentData);
echo  '<div class=""><div class=""><table cellspacing="0" cellpadding="0" border="0" width="100%" class="generaltable"><tbody>';
echo '<tr><th width="40%">'.get_string('coursetitle','multicoursereport').'</th><th width="15%">'.get_string('enrolled','multiuserreport').'</th><th width="15%">'.get_string('notstarted','multiuserreport').'</th><th width="15%">'.get_string('inprogress','multiuserreport').'</th><th width="15%">'.get_string('completed','multiuserreport').'</th></tr>';
if(!empty($enrolledData)){
	foreach($enrolledData as $enrolled){
                if($enrolled->depth==1){
                        $enrolled->fullname = "<span class='childcourse'>".$enrolled->fullname."</span>";
                }
		$complianceDiv = getCourseComplianceIcon($enrolled->course_id);
		$enrolled->enrolled = $enrolled->not_started + $enrolled->in_progress + $enrolled->completed;
		$enrolled->fullname .= $complianceDiv;
		if($action != 'print'){
			$link = $CFG->wwwroot.'/reports/course_progress_report.php?cid='.$enrolled->course_id.'&department='.$sDepartment.'&country='.$sCountry.'&city='.$sCity.'&team='.$sTeam.'&back=2&userstatus='.$userStatus.'&job_title='.$sJobTitle.'&report_to='.$sLeadersName.'&is_manager_yes='.$sIsManagerYes;
			echo '<tr>';
			echo '<td><a href = "'.$link.'">'.$enrolled->fullname.'</a></td>';
			echo '<td><a href = "'.$link.'">'.$enrolled->enrolled.'</a></td>';
			echo '<td><a href = "'.$link.'&type=3">'.$enrolled->not_started.'</a></td>';
			echo '<td><a href = "'.$link.'&type=1">'.$enrolled->in_progress.'</a></td>';
			echo '<td><a href = "'.$link.'&type=2">'.$enrolled->completed.'</a></td>';
			echo '</tr>';
		}else{                    
			echo '<tr>';
			echo '<td>'.$enrolled->fullname.'</td>';
			echo '<td>'.$enrolled->enrolled.'</td>';
			echo '<td>'.$enrolled->not_started.'</td>';
			echo '<td>'.$enrolled->in_progress.'</td>';
			echo '<td>'.$enrolled->completed.'</td>';
			echo '</tr>';
		}
	}
}else{
	echo '<tr><td colspan = "5">'.get_string('no_results').'</td></tr>';
}
echo '</table><tbody></div></div>';
echo '</div>';
if($action != 'print'){
	echo paging_bar($total_records, $page, $perpage, $genURL);
}
$userHTML .=  '<script language="javascript" type="text/javascript">';
$userHTML .=  '	$(document).ready(function(){$("#printbun").bind("click", function(e) {var url = $(this).attr("rel");
window.open(url, "'.get_string('courseusagesreport','multiuserreport').'", "'.$CFG->printWindowParameter.'");
});
}); ';
$userHTML .= '</script>';
echo $userHTML;
echo $OUTPUT->footer();
?>
<script language="javascript" type="text/javascript">
$(document).ready(function(){
$('.reminder-link').click(function(){    
        $('#fade').show();
        $.ajax({	  
	    url:'<?php echo $CFG->wwwroot;?>/email/reminder_manual.php',
		type:'POST',
		data:'action=sendremindermanual',
                dataType:'json',
		success:function(data){
                    $('#fade').hide();
                    var success = data.success;
                    if(success == 1){
                        alert(data.success_message);
                        //$('.reminder-success').show(data.success_message);
                    }else{
                        alert("Error in sending email.");
                    }
                    
                    
		}
	  
	  });
    
});

});
</script>
<div id="fade" ></div>
