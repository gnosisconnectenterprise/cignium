<?php
	require_once("../config.php");
	require_once("lib.php");
	
	if( $USER->archetype == $CFG->userTypeStudent ) {
			$id      = optional_param('uid', $USER->id, PARAM_INT);
			if($id != $USER->id){
				$grpUsers = fetchGroupsUserIds($USER->id,1);
				if(empty($grpUsers) || !in_array($id,$grpUsers)){
					$id = $USER->id;
				}
			}
	}else{
	 $id = required_param('uid', PARAM_INT);    // user id; 
	}
		
	$user = $DB->get_record('user', array('id'=>$id), '*', MUST_EXIST);
	$userId = $user->id; 
	checkLogin();
	
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', $CFG->perpage, PARAM_INT);        // how many per page
	$pageURL = '/reports/user_credithours_report.php';
	checkPageIsNumeric($pageURL,$_REQUEST['page']);
	
	$sDate = '';
	$eDate = '';
	$sDate =  $CFG->learningYear['from'];
	$eDate = $CFG->learningYear['to'];
	
	$sDepartment    = optional_param('department', '-1', PARAM_RAW);
	$sTeam          = optional_param('team', '-1', PARAM_RAW);       
	$sUser          = optional_param('user', '-1', PARAM_RAW);       
	$export         = optional_param('action', '', PARAM_ALPHANUM);
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);
	$sUserType     = optional_param('userType', '-1', PARAM_RAW);
	$sCourse          = optional_param('course', '-1', PARAM_RAW);
	$user_group      = optional_param('user_group','-1', PARAM_RAW);
	$action      = optional_param('action', '', PARAM_RAW);
	$paramArray = array(
                                        'ch' => optional_param('ch', '', PARAM_ALPHA),
					'key' => optional_param('key', '', PARAM_RAW),
					'uid' => $userId,					
					'department' => $sDepartment,
					'team' => $sTeam,
					'user' => $sUser,
					'course' => $sCourse,
					'userType' => $sUserType,
					'startDate' => $sStartDate,
					'endDate' => $sEndDate,					
					'back' => $back					
				  );
	$filterArray = array(							
						'nm'=>get_string('name','course'),
                                                'uid' => $userId,
				   );
	$sDateSelected = '';
	if($sStartDate){
		$sDateArr = getFormattedTimeStampOfDate($sStartDate);
		$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';
	
	}
		
	$eDateSelected = '';
	if($sEndDate){
		$eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
		$eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
	}
	if($sDateSelected){
		$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;		
	}
		
	if($eDateSelected){
		$sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;		
	}
	$removeKeyArray = array('perpage');
	$sDepartmentArr = explode("@",$sDepartment);
	if($action == 'print'){
		$PAGE->set_pagelayout('print');
	}else{
		$PAGE->set_pagelayout('reportlayout');
	}	
	$sUserTypeArr = explode("@",$sUserType);
		
	if($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2 ){
		$headerLabelGraph = get_string('usercredithoursreport');
	}elseif(in_array($CFG->userTypeInstructor, $sUserTypeArr)){
		$headerLabelGraph = get_string('overallinstructorcoursecredithoursreport');	
	}elseif(in_array($CFG->userTypeStudent, $sUserTypeArr)){
		$headerLabelGraph = get_string('overalllearnercoursecredithoursreport');	
	}	
	

	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('userreport','multiuserreport'));	
	$PAGE->navbar->add(get_string('reports','multiuserreport'));
	if( $USER->archetype == $CFG->userTypeStudent ) {
	  $PAGE->navbar->add(get_string('usercredithoursreport'));
	}else{
	 $PAGE->navbar->add(get_string('usercredithoursreport'), new moodle_url($CFG->wwwroot.'/reports/credithours_report.php'));
	 $PAGE->navbar->add(getUsers($id));
	}	

	$linkToDetailsURL = genParameterizedURL(array_merge($paramArray, array('uid'=>':userId','back'=>1)), $removeKeyArray, '/reports/user_credithours_report.php');
	if($action == 'exportcsv' || $action == 'print' || $action == 'exportpdf'){		
		$returnHTML = usercreditHourReportExport($paramArray,$page,$perpage,$action,$headerLabelGraph);
		$perpage = 0;
	}
		
	echo $OUTPUT->header(); 

	$creditHourData = getOverallUserCreditHours($paramArray,  $page, $perpage, '*');
	//pr($creditHourData);die;	
	$creditHourDataTotalCount = getOverallUserCreditHours($paramArray,  $page, $perpage, 'count');
	
	$creditHourDataGraphData = getOverallUserCreditHours($paramArray,  $page, $perpage, 'graph_data');
	
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
	$onlineCourseCreditHour = $creditHourDataGraphData[1]->credit_hours;
	$classroomCourseCreditHour = $creditHourDataGraphData[2]->credit_hours;
	
	$totalCreditHours = $onlineCourseCreditHour + $classroomCourseCreditHour;
	$totalCreditHoursDisplay = setCreditHoursFormat($totalCreditHours);
	
	$onlineCourseCreditHourDisplay = setCreditHoursFormat($onlineCourseCreditHour);
	$classroomCourseCreditHourDisplay = setCreditHoursFormat($classroomCourseCreditHour);
	$userHTML = $userReport->userHTML;

	$arrEnrollmentData['graph_heading'] = $headerLabelGraph;
	$arrEnrollmentData['total']['string'] = get_string('coursecredithourswithtime');
	$arrEnrollmentData['total']['value'] = $totalCreditHours;
	$arrEnrollmentData['total']['displayvalue'] = $totalCreditHoursDisplay;
	$classRoomClass = 'inprogress';
	if(in_array($CFG->userTypeStudent, $sUserTypeArr) || ($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2)){
		$arrEnrollmentData['data'][0]['string'] = get_string('onlinecourse');
		$arrEnrollmentData['data'][0]['class'] = 'inprogress';
		$arrEnrollmentData['data'][0]['value'] = $onlineCourseCreditHour;
		$arrEnrollmentData['data'][0]['displayvalue'] = $onlineCourseCreditHourDisplay;
		$classRoomClass = 'notstarted';
	}
	$arrEnrollmentData['data'][1]['string'] = get_string('classroomcourse');
	$arrEnrollmentData['data'][1]['class'] = $classRoomClass;
	$arrEnrollmentData['data'][1]['value'] = $classroomCourseCreditHour;	
	$arrEnrollmentData['data'][1]['displayvalue'] = $classroomCourseCreditHourDisplay;
	$arrEnrollmentData['url'] = $genURL;
	$arrEnrollmentData['action'] = $action;
	$total_records = 0;
	if($creditHourDataTotalCount){
		$total_records = $creditHourDataTotalCount->total_records;
	}
	//echo $genURL;die;
       // echo $pageURL;die;
        if($action != 'print'){
              echo genCommonSearchForm($paramArray, $filterArray, $pageURL);
        }
      
	echo '<div class="tabsOuter borderBlockSpace">';
	if($action == 'print'){
		echo $returnHTML;
	}else{
		ob_start();
		require_once($CFG->dirroot . '/local/includes/user_credithours_report_filter.php');
		$SEARCHHTML = ob_get_contents();
		ob_end_clean();
		echo $SEARCHHTML;
	}
	echo '<div class="f-right">'.get_string('reports_heading','',getReportSyncTime()).'</div>';
	echo '<div class="clear"></div>';
	$PerformanceData = getGraphData($arrEnrollmentData);
	
	$userHTML = "";
	$userHTML .=  '<div class=""><div class=""><table cellspacing="0" cellpadding="0" border="0" width="100%" class="generaltable"><tbody>';
		$userHTML .= '<tr class="table1_head">
				<th width="30%">'.get_string("coursetitle","singlereport").'</th>
				<th width="15%">'.get_string("coursecredithourswithtime").'</th>
				<th width="15%">'.get_string('coursetype','course').'</th>
				<th width="20%">'.get_string("completiondate").'</th>
				</tr>';
	
	if(count($creditHourData)>0){
		foreach($creditHourData as $crData){
                        $courseTypeText = ($crData->course_type=='online course')?get_string('onlinecourse'):$crData->course_type;
			$userHTML .=  '<tr>';					
					$userHTML .=  '<td>'.$crData->coursefullname.'</td>';
					$userHTML .=  '<td>'.setCreditHoursFormat($crData->credithours).'</td>';
					$userHTML .=  '<td>'.$courseTypeText.'</td>';
					$userHTML .=  '<td>'.getDateFormat($crData->complition_time, $CFG->customDefaultDateFormat).'</td>';				
			$userHTML .=  '</tr>';
				
		}
	}
	else{
	
			$userHTML .= '<tr><td colspan = "4">'.get_string('no_results').'</td></tr>';
		
	}
	$userHTML .= '</table><tbody></div></div>';
	$userHTML .= '</div>';
	
	
	if($action != 'print'){
		$userHTML .= paging_bar($total_records, $page, $perpage, $genURL);
	}

	$userHTML .=  '<script language="javascript" type="text/javascript">';
	$userHTML .=  '	$(document).ready(function(){$("#printbun").bind("click", function(e) {var url = $(this).attr("rel");
	window.open(url, "'.get_string('courseusagesreport','multiuserreport').'", "'.$CFG->printWindowParameter.'");
	});
	}); ';
	$userHTML .= '</script>';
	echo $userHTML;
	echo includeGraphFiles($headerLabelGraph);
	echo $OUTPUT->footer();


?>