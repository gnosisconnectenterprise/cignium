<?php
require_once("../config.php");
require_once("lib.php");
global $DB,$CFG,$USER;
checkLogin();
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', $CFG->perpage, PARAM_INT);
$pageURL = '/reports/classroom_course_usage_report.php';
checkPageIsNumeric($pageURL,$_REQUEST['page']);
$sDate = '';
$eDate = '';
$sclasses    = optional_param('classes', '-1', PARAM_RAW);   
$sCourse        = optional_param('course', '-1', PARAM_RAW);       
$sType          = optional_param('type', '-1', PARAM_RAW); 
$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
$sEndDate       = optional_param('endDate', $eDate, PARAM_RAW);
$action			= optional_param('action', '', PARAM_RAW);

 $key = optional_param('key', '', PARAM_RAW);
 
$sCourseArr = explode("@",$sCourse);
$sclassesArr = explode("@",$sclasses);
$sTypeArr = explode("@",$sType);
$sDateSelected = '';
if($sStartDate){
	$sDateArr = getFormattedTimeStampOfDate($sStartDate);
	$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

}		   

$eDateSelected = '';
if($sEndDate){
   $eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
   $eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
}
checkLogin();
if($action == 'print'){
	$PAGE->set_pagelayout('print');
}else{
	$PAGE->set_pagelayout('reportlayout');
}
$PAGE->set_heading($SITE->fullname);

$PAGE->set_title($SITE->fullname.": ".get_string('courseusagesreport','classroomreport'));

$PAGE->navbar->add(get_string('reports','classroomreport'));
$PAGE->navbar->add(get_string('courseusagesreport','classroomreport'));

$paramArray = array('classes' => $sclasses,'course' => $sCourse,'type' => $sType,'startDate' => $sStartDate,'endDate' => $sEndDate,'key' =>$key);
$removeKeyArray = array();
if($action == 'exportcsv' || $action == 'print' || $action == 'exportpdf'){
	$returnHTML = getClassroomUsageReportToCSV($paramArray,$page,$perpage,$action);
	$perpage = 0;
}

echo $OUTPUT->header();
$enrollentCountPagination = array();
$enrolledData = getClassroomUsageReport($paramArray,$page,$perpage,'');
$enrollentCount = getClassroomUsageReport($paramArray,$page,0,'count_graph');
$enrollentCountPagination = getClassroomUsageReport($paramArray,$page,0,'count');
$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);

$arrEnrollmentData['graph_heading'] = get_string('classroomusagesreport','classroomreport');
$arrEnrollmentData['total']['string'] = get_string('totalassignedusers','classroomreport');
$arrEnrollmentData['total']['value'] = $enrollentCount['enrolled'];
$arrEnrollmentData['data'][0]['string'] =get_string('inprogress','classroomreport');
$arrEnrollmentData['data'][0]['class'] = 'inprogress';
$arrEnrollmentData['data'][0]['value'] = $enrollentCount['incomplete'];
$arrEnrollmentData['data'][1]['string'] = get_string('classnoshow','classroomreport');
$arrEnrollmentData['data'][1]['class'] = 'notstarted';
$arrEnrollmentData['data'][1]['value'] = $enrollentCount['no_show'];
$arrEnrollmentData['data'][2]['string'] = get_string('completed','classroomreport');
$arrEnrollmentData['data'][2]['class'] = 'completed';
$arrEnrollmentData['data'][2]['value'] = $enrollentCount['completed'];
$arrEnrollmentData['data'][3]['string'] = get_string('classnodata','classroomreport');
$arrEnrollmentData['data'][3]['class'] = 'nodata';
$arrEnrollmentData['data'][3]['value'] = $enrollentCount['no_data'];
$arrEnrollmentData['url'] = $genURL;
$arrEnrollmentData['action'] = $action;
$total_records = 0;
if($enrollentCountPagination){
	$total_records = $enrollentCountPagination['total_records'];
}
$arrAssets = array();
$arrCourses = array();
if(!empty($enrolledData)){
	foreach($enrolledData as $enrolled){
		$arrCourses[] = $enrolled->classroom_id;
	}
	$arrAssets = getClassroomClasses($arrCourses,$userId,$paramArray);
}
if($action !='print'){
    echo genCommonSearchForm($paramArray, $filterArray, $pageURL);
}

//echo $CFG->dirroot . '/local/includes/'.$CFG->pageMultiClassroomReportSearch;die;
echo '<div class="tabsOuter borderBlockSpace">';
if($action == 'print'){
	$filterData = $returnHTML;
}else{
	ob_start(); 
	require_once($CFG->dirroot . '/local/includes/'.$CFG->pageMultiClassroomReportSearch);
	$filterData = ob_get_contents();
	ob_end_clean();
}
echo $filterData;
echo '<div class="f-right">'.get_string('reports_heading','',getReportSyncTime()).'</div>';
echo '<div class="clear"></div>';
$PerformanceData = getGraphData($arrEnrollmentData);
echo  '<div class=""><div class=""><table cellspacing="0" cellpadding="0" border="0" width="100%" class="table1 generaltable"><tbody>';
echo '<tr><th width="40%">'.get_string('coursetitle','classroomreport').'</th><th width="15%">'.get_string('enrolled','classroomreport').'</th><th width="15%">'.get_string('classnoshow','classroomreport').'</th><th width="15%">'.get_string('inprogress','classroomreport').'</th><th width="15%">'.get_string('completed','classroomreport').'</th></tr>';
if(!empty($enrolledData)){
	foreach($enrolledData as $enrolled){
		$classmMain = 'evenR';
		echo '<tr class="'.$classmMain.'" id="course-'.$enrolled->classroom_id.'">';
			echo '<td>'.$enrolled->classroom_name.'</td>';
			echo '<td>'.getSpace().'</td>';
			echo '<td>'.getSpace().'</td>';
			echo '<td>'.getSpace().'</td>';
			echo '<td>'.getSpace().'</td>';
		echo '</tr>';
		if(!empty($arrAssets[$enrolled->classroom_id])){
			$assetCount = 1;
			$assestHTML = '';
			$assestHTML .= '<div class="a-box" id="abox'.$enrolled->classroom_id.'" >';
			foreach($arrAssets[$enrolled->classroom_id] as $assetData){
				if($action != 'print'){
					$assetData->class_name = "<a href = '".$CFG->wwwroot."/reports/class_usage_report.php?cid=".$enrolled->classroom_id."&class_id=".$assetData->class_id."'>".$assetData->class_name."</a>";
				}
				$classEvenOdd = $assetCount%2==0 ?'even':'odd';
				$assestHTML .=  '<div class="'.$classEvenOdd.'" >';
					$assestHTML .=  '<div style="width:40%" class = "break-all" >'.$assetData->class_name.'</div>';
					$assestHTML .=  '<div style="width:15%">'.$assetData->enrolled.'</div>';
					$assestHTML .=  '<div style="width:15%">'.$assetData->no_show.'</div>';
					$assestHTML .=  '<div style="width:15%">'.$assetData->incomplete.'</div>';
					$assestHTML .=  '<div style="width:15%">'.$assetData->completed.'</div>';
				$assestHTML .=  '</div>';
				$assetCount++;
			}
			$assestHTML .=  '</div>';
		}
		echo '<tr><td colspan = "6" class="borderLeftRight padding_none">';
			echo $assestHTML;
		echo '</td></tr>';
	}
}else{
	echo '<tr><td colspan = "5">'.get_string('no_results').'</td></tr>';
}
echo '</tbody></table></div></div>';
echo '</div>';
if($action != 'print'){
	echo paging_bar($total_records, $page, $perpage, $genURL);
}
$userHTML .=  '<script language="javascript" type="text/javascript">';
$userHTML .=  '	$(document).ready(function(){$("#printbun").bind("click", function(e) {
var url = $(this).attr("rel");
window.open(url, "'.get_string('classroomusagesreport','classroomreport').'", "'.$CFG->printWindowParameter.'");
});
}); ';
$userHTML .= '</script>';
echo $userHTML;
echo $OUTPUT->footer();
?>