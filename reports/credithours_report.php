<?php
	require_once("../config.php");
	require_once("lib.php");
	if( $USER->archetype == $CFG->userTypeStudent ) {
		$groupCourses = checkOwnerAccess($USER->id);
		if(!$groupCourses){
			redirect($CFG->wwwroot);
		}
	}	 
	checkLogin();
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', $CFG->perpage, PARAM_INT);        // how many per page
	$pageURL = '/reports/credithours_report.php';
	checkPageIsNumeric($pageURL,$_REQUEST['page']);
	
	$sDate = '';
	$eDate = '';
	$sDate =  $CFG->learningYear['from'];
	$eDate = $CFG->learningYear['to'];
	
	$sDepartment    = optional_param('department', '-1', PARAM_RAW);
	$sTeam          = optional_param('team', '-1', PARAM_RAW);       
	$sUser          = optional_param('user', '-1', PARAM_RAW);       
	$export         = optional_param('action', '', PARAM_ALPHANUM);
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);
	$sUserType     = optional_param('userType', '-1', PARAM_RAW);
	$user_group      = optional_param('user_group','-1', PARAM_RAW);
	$action      = optional_param('action', '', PARAM_RAW);
	$sJobTitle = optional_param('job_title', '-1', PARAM_RAW);
        $sReportTo = optional_param('report_to', '-1', PARAM_RAW);
        $sIsManagerYes =optional_param('is_manager_yes', '-1', PARAM_RAW);
        
        $sIsManagerYesArr = explode("@",$sIsManagerYes);
	$sCompany = optional_param('company', '-1', PARAM_RAW);
	$paramArray = array(
					'ch' => optional_param('ch', '', PARAM_ALPHA),
					'key' => optional_param('key', '', PARAM_RAW),
					'department' => $sDepartment,
					'team' => $sTeam,
					'user' => $sUser,
					'startDate' => $sStartDate,
					'endDate' => $sEndDate,
					'userType' => $sUserType,
					'user_group' => $user_group,
					'job_title' =>$sJobTitle,
                                        'report_to' =>$sReportTo,
                                        'is_manager_yes' =>$sIsManagerYes,
					'company' => $sCompany,
					'sel_mode' => optional_param('sel_mode', '-1', PARAM_RAW)
					
				  );
	$filterArray = array(							
						'nm'=>get_string('name','course'),
				   );
	$sDateSelected = '';
	if($sStartDate){
		$sDateArr = getFormattedTimeStampOfDate($sStartDate);
		$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';
	
	}
	$sJobTitleArr = explode("@",$sJobTitle);
	$sCompanyArr = explode("@",$sCompany);
        
	$eDateSelected = '';
	if($sEndDate){
		$eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
		$eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
	}
	if($sDateSelected){
		$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;		
	}
		
	if($eDateSelected){
		$sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;		
	}
	$removeKeyArray = array('perpage','key','ch');
	$sDepartmentArr = explode("@",$sDepartment);
	if($action == 'print'){
		$PAGE->set_pagelayout('print');
	}else{
		$PAGE->set_pagelayout('reportlayout');
	}
	$sUserTypeArr = explode("@",$sUserType);
	
	if($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2 ){
		$headerLabelGraph = get_string('usercredithoursreport');
	}elseif(in_array($CFG->userTypeInstructor, $sUserTypeArr)){
		$headerLabelGraph = get_string('instructorcredithoursreport');		
	}elseif(in_array($CFG->userTypeStudent, $sUserTypeArr)){
		$headerLabelGraph = get_string('learnercredithoursreport');
	}
	
	

	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('userreport','multiuserreport'));
	//$PAGE->navbar->add(get_string('reports','multiuserreport'), new moodle_url($CFG->wwwroot.'/my/adminreportdashboard.php'));
	$PAGE->navbar->add(get_string('reports','multiuserreport'));
	$PAGE->navbar->add(get_string('usercredithoursreport'));
	
	//$aa = getCourseIdByStatus(77);
	//pr($aa );die;

	//echo date("Y-m-d H:i:s", 1418216437)."===".date("Y-m-d H:i:s", 1357237800)."===".date("Y-m-d H:i:s", 1459362600);
	$linkToDetailsURL = genParameterizedURL(array_merge($paramArray, array('uid'=>':userId','back'=>1)), $removeKeyArray, '/reports/user_credithours_report.php');
	if($action == 'exportcsv' || $action == 'print' || $action == 'exportpdf'){
           
		$returnHTML = creditHourReportExport($paramArray,$page,$perpage,$action,$headerLabelGraph);
		$perpage = 0;
	}
		
	echo $OUTPUT->header(); 

	$creditHourData = getOverallCreditHours($paramArray,  $page, $perpage, '*');
	//pr($creditHourData);die;
	
	$creditHourDataTotalCount = getOverallCreditHours($paramArray,  $page, $perpage, 'count');
	
      
	$creditHourDataGraphData = getOverallCreditHours($paramArray,  $page, $perpage, 'graph_data');
	
	$genURL = genParameterizedURL($paramArray, $removeKeyArray1, $pageURL);
     
	$onlineCourseCreditHour = $creditHourDataGraphData[1]->credit_hours;
	$classroomCourseCreditHour = $creditHourDataGraphData[2]->credit_hours;
	
	$totalCreditHours = $onlineCourseCreditHour + $classroomCourseCreditHour;
	$totalCreditHoursDisplay = setCreditHoursFormat($totalCreditHours);
	
	$onlineCourseCreditHourDisplay = setCreditHoursFormat($onlineCourseCreditHour);
	$classroomCourseCreditHourDisplay = setCreditHoursFormat($classroomCourseCreditHour);
	$userHTML = $userReport->userHTML;

	$arrEnrollmentData['graph_heading'] = $headerLabelGraph;
	$arrEnrollmentData['total']['string'] = get_string('coursecredithourswithtime');
	$arrEnrollmentData['total']['displayvalue'] = $totalCreditHoursDisplay;
	
	$arrEnrollmentData['total']['value'] = $totalCreditHours;
	$classRoomClass = 'inprogress';
	if(in_array($CFG->userTypeStudent, $sUserTypeArr) || ($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2)){
		$arrEnrollmentData['data'][0]['string'] = get_string('onlinecourse');
		$arrEnrollmentData['data'][0]['class'] = 'inprogress';
		$arrEnrollmentData['data'][0]['value'] = $onlineCourseCreditHour;
		$arrEnrollmentData['data'][0]['displayvalue'] = $onlineCourseCreditHourDisplay;
		$classRoomClass = 'notstarted';
	}
	$arrEnrollmentData['data'][1]['string'] = get_string('classroomcourse');
	$arrEnrollmentData['data'][1]['class'] = $classRoomClass;
	$arrEnrollmentData['data'][1]['value'] = $classroomCourseCreditHour;
	$arrEnrollmentData['data'][1]['displayvalue'] = $classroomCourseCreditHourDisplay;
	$arrEnrollmentData['url'] = $genURL;
	$arrEnrollmentData['action'] = $action;
	$total_records = 0;
	if($creditHourDataTotalCount){
		$total_records = $creditHourDataTotalCount->total_records;
	}
        if($action != 'print'){
            echo genCommonSearchForm($paramArray, $filterArray, $pageURL);
        }
	
	echo '<div class="tabsOuter borderBlockSpace">';
	if($action == 'print'){
		echo $returnHTML;
	}else{
             
		ob_start();
		require_once($CFG->dirroot . '/local/includes/credithours_report_filter.php');
		$SEARCHHTML = ob_get_contents();
		ob_end_clean();
		echo $SEARCHHTML;
	}
        
        //$lastSyncTime = getReportSyncTime();
	echo '<div class="f-right">'.get_string('reports_heading','',getReportSyncTime()).'</div>';
	echo '<div class="clear"></div>';
	$PerformanceData = getGraphData($arrEnrollmentData);
	if($CFG->isCignium  == 'Y'){
		if($USER->department == _THEME_COLOR_TRANZACT_DEPARTMENT){
			$CFG->creditReportColor = _THEME_COLOR_TRANZACT;
		}elseif($USER->department == _THEME_COLOR_TRANZACTIS_DEPARTMENT){
			$CFG->creditReportColor = _THEME_COLOR_TRANZACTIS;
		}elseif($USER->department == _THEME_COLOR_CIGNIUM_DEPARTMENT){
			$CFG->creditReportColor = _THEME_COLOR_CIGNIUM;
		}
	}
	$userHTML = "";
	$userHTML .=  '<div class=""><div class=""><table cellspacing="0" cellpadding="0" border="0" width="100%" class="generaltable"><tbody>';
	if(in_array($CFG->userTypeStudent, $sUserTypeArr) || ($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2)){
		
		$userHTML .= '<tr>';
		$userHTML .= '<th width="15%" rowspan="2" >'.get_string('fullname','multiuserreport').'</th>';
		$userHTML .= '<th width="15%" rowspan="2" >'.get_string('username').'</th>';
		$userHTML .= '<th width="25%" colspan="3" style="text-align:center;height:35px;">'.get_string('noofcourses').'</th>';
		$userHTML .= '<th width="25%" colspan="3" class = "blueHighlight" style="text-align:center;height:35px;background-color:'.$CFG->creditReportColor.' !important">'.get_string('coursecredithourswithtime').'</th>';
		$userHTML .= '<th width="10%" rowspan="2">'.get_string('manager','multiuserreport').'</th>';
		$userHTML .= '<th width="10%" rowspan="2">'.get_string('department','multiuserreport').'</th>';
		$userHTML .= '</tr>';
		$userHTML .= '<tr>';
		$userHTML .= '<th>'.get_string('onlinecourse').'</th>';
		$userHTML .= '<th>'.get_string('classroomcourse').'</th>';
		$userHTML .= '<th>'.get_string('total').'</th>';
		$userHTML .= '<th>'.get_string('onlinecourse').'</th>';
		$userHTML .= '<th>'.get_string('classroomcourse').'</th>';
		$userHTML .= '<th class = "orangeHighlight" >'.get_string('total').'</th>';
		$userHTML .= '</tr>';
	}else{
		$userHTML .= '<tr>';
		$userHTML .= '<th width="19%">'.get_string('fullname','multiuserreport').'</th>';
		$userHTML .= '<th width="19%">'.get_string('username').'</th>';
		$userHTML .= '<th width="11%">&nbsp;'.get_string('noofcourses').'</th>';
		$userHTML .= '<th width="13%">&nbsp;'.get_string('coursecredithourswithtime').'</th>';
		$userHTML .= '<th width="19%">'.get_string('manager','multiuserreport').'</th>';
		$userHTML .= '<th width="19%">'.get_string('department','multiuserreport').'</th>';
		$userHTML .= '</tr>';
	}
	
	if(count($creditHourData)>0){
		foreach($creditHourData as $crData){
			//pr($crData);die;
			$manager_name = '';
			if($CFG->showInlineManagerNCompany==1){
	  			$manager_name = $crData->inline_manager_name;
	  			if(trim($manager_name)==''){
	  				$manager_name = $crData->usermanager;
	  			}
	  		}else{
	  			$manager_name = $crData->usermanager;
	  		}
			$managerName = ($manager_name)?$manager_name:getMDash();
				if(in_array($CFG->userTypeStudent, $sUserTypeArr) || ($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2)){
					$linkUrlDetails = str_replace(array(":userId"), array($crData->user_id), $linkToDetailsURL);
					$userHTML .=  '<tr>';
					if($action=='print'){
						$userHTML .=  '<td>'.$crData->userfullname.'</td>';
						$userHTML .=  '<td>'.$crData->username.'</td>';
					}else{
					$userHTML .=  '<td><a href="'.$linkUrlDetails.'" >'.$crData->userfullname.'</a></td>';
					$userHTML .=  '<td><a href="'.$linkUrlDetails.'" >'.$crData->username.'</a></td>';
					}
					$userHTML .= '<td>'.$crData->online_course.'</td>';
					$userHTML .= '<td>'.$crData->classroom_course.'</td>';
					$userHTML .= '<td>'.$crData->total_course.'</td>';
					$userHTML .= '<td>'.setCreditHoursFormat($crData->online_credit_hours).'</td>';
					$userHTML .= '<td>'.setCreditHoursFormat($crData->classroom_credit_hours).'</td>';
					$userHTML .= '<td>'.setCreditHoursFormat($crData->total_credit_hours).'</td>';
					$userHTML .=  '<td>'.$managerName.'</td>';
					$userHTML .=  '<td>'.$crData->departmenttitle.'</td>';
					$userHTML .=  '</tr>';
				}
				else{
					$linkUrlDetails = str_replace(array(":userId"), array($crData->user_id), $linkToDetailsURL);
					$userHTML .=  '<tr>';
					if($action=='print'){
					$userHTML .=  '<td>'.$crData->userfullname.'</td>';
					$userHTML .=  '<td>'.$crData->username.'</td>';
					}else{
					$userHTML .=  '<td><a href="'.$linkUrlDetails.'" >'.$crData->userfullname.'</a></td>';
					$userHTML .=  '<td><a href="'.$linkUrlDetails.'" >'.$crData->username.'</a></td>';
					}
					$userHTML .=  '<td>'.$crData->classroom_course.'</td>';
					$userHTML .=  '<td>'.setCreditHoursFormat($crData->classroom_credit_hours).'</td>';
					$userHTML .=  '<td>'.$managerName.'</td>';
					$userHTML .=  '<td>'.$crData->departmenttitle.'</td>';
					$userHTML .=  '</tr>';
				}	
		}
	}
	else{
		if(in_array($CFG->userTypeStudent, $sUserTypeArr) || ($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2)){
			$userHTML .= '<tr><td colspan = "10">'.get_string('no_results').'</td></tr>';
		}else{
			$userHTML .= '<tr><td colspan = "6">'.get_string('no_results').'</td></tr>';
		}
	}
	$userHTML .= '</table><tbody></div></div>';
	$userHTML .= '</div>';
	
	
	if($action != 'print'){
		$userHTML .= paging_bar($total_records, $page, $perpage, $genURL);
	}

	$userHTML .=  '<script language="javascript" type="text/javascript">';
	$userHTML .=  '	$(document).ready(function(){$("#printbun").bind("click", function(e) {var url = $(this).attr("rel");
window.open(url, "'.get_string('courseusagesreport','multiuserreport').'", "'.$CFG->printWindowParameter.'");
});
}); ';
	$userHTML .= '</script>';
	echo $userHTML;
	echo includeGraphFiles($headerLabelGraph);
	echo $OUTPUT->footer();


?>