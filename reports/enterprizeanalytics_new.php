<?php

require_once('../config.php');

$sDate = '';
$eDate = '';

$sStartDate = optional_param('startDate', $sDate, PARAM_RAW);
$sEndDate = optional_param('endDate', $eDate, PARAM_RAW);

$sDateSelected = '';
if ($sStartDate) {
    $sDateArr = getFormattedTimeStampOfDate($sStartDate);
    $sDateSelected = isset($sDateArr['dateMDY']) ? $sDateArr['dateMDY'] : '';
}
$eDateSelected = '';
if ($sEndDate) {
    $eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
    $eDateSelected = isset($eDateArr['dateMDY']) ? $eDateArr['dateMDY'] : '';
}
$from = strtotime($sDateSelected);
$to = strtotime($eDateSelected);

global $DB;
//default assignmnet
$usersWhere = '';
$loginWhere = '';
$courseWhere = '';
$onlineCourseWhere = '';
$classroomCourseWhere = '';
$programWhere = '';
$onlineCourseCompletionWhere = '';
$classroomCourseCompletionWhere = '';

//Users Records
if (!empty($from) && !empty($to)) {
    $usersWhere = " AND timecreated between $from AND $to";
} elseif (!empty($from) && empty($to)) {
    $usersWhere = " AND timecreated >$from";
} elseif (empty($from) && !empty($to)) {
    $usersWhere = " AND timecreated < $to";
}
$usersSQL = "SELECT COUNT(*) AS totaluserswithdeleted, (
                        SELECT COUNT(*)
                        FROM mdl_user
                        WHERE deleted = 0 AND id NOT IN (1,2) $usersWhere ) AS totalusers, (
                        SELECT COUNT(*)
                        FROM mdl_user
                        WHERE deleted = 0 AND suspended = 0 AND id NOT IN (1,2) $usersWhere ) AS activeuser,
                        (  
                        SELECT COUNT(*)
                        FROM mdl_user
                        WHERE deleted = 0 AND suspended = 1 AND id NOT IN (1,2) $usersWhere ) AS deactiveuser
                        FROM mdl_user";

$usersrecords = $DB->get_record_sql($usersSQL);

//Course record
if (!empty($from) && !empty($to)) {
    $courseWhere = " AND timecreated between $from AND $to";
} elseif (!empty($from) && empty($to)) {
    $courseWhere = " AND timecreated >$from";
} elseif (empty($from) && !empty($to)) {
    $courseWhere = " AND timecreated < $to";
}

$courseSQL = "SELECT COUNT(*) AS totalcoursewithdeleted, (
                    SELECT COUNT(*)
                    FROM mdl_course
                    WHERE deleted = 0 AND id NOT IN (1) $courseWhere) AS totalcourse, (
                    SELECT COUNT(*)
                    FROM mdl_course
                    WHERE deleted = 0 AND is_active = 1 AND id NOT IN (1) $courseWhere) AS activecourse,
                    (
                    SELECT COUNT(*)
                    FROM mdl_course
                    WHERE deleted = 0 AND is_active = 0 AND id NOT IN (1) $courseWhere) AS deactivecourse, (
                    SELECT COUNT(*)
                    FROM mdl_course
                    WHERE deleted = 0 AND publish= 0  AND id NOT IN (1) $courseWhere) AS unpublishedcourse from mdl_course";

$courseRecords = $DB->get_record_sql($courseSQL);

//Assignments Records
if (!empty($from) && !empty($to)) {
    $onlineCourseWhere = " AND ucm.created_on between $from AND $to";
    $classroomCourseWhere = " AND se.timecreated between $from AND $to";
    $programWhere = " AND pum.created_on between $from AND $to";
} elseif (!empty($from) && empty($to)) {
    $onlineCourseWhere = " AND ucm.created_on > $from";
    $classroomCourseWhere = " AND se.timecreated > $from";
    $programWhere = " AND pum.created_on > $from";
} elseif (empty($from) && !empty($to)) {
    $onlineCourseWhere = " AND ucm.created_on < $to";
    $classroomCourseWhere = " AND se.timecreated < $to";
    $programWhere = " AND pum.created_on < $to";
}

$assignmentSQL = "SELECT COUNT(*) AS onlinecourseassignment, (
                        SELECT COUNT(*)
                        FROM mdl_scheduler_enrollment se
                        JOIN mdl_user u ON u.id = se.userid
                        JOIN mdl_scheduler mc ON mc.id = se.scheduler_id
                        WHERE se.is_approved=1 AND u.deleted = 0 AND mc.isactive = 1 $classroomCourseWhere) AS classassignment,
                        (
                        SELECT COUNT(*)
                        FROM mdl_program_user_mapping pum
                        JOIN mdl_user u ON u.id = pum.user_id AND u.deleted = 0
                        JOIN mdl_programs p ON p.id = pum.program_id AND p.status = 1
                        WHERE pum.status = 1 $programWhere) AS programassignment
                        FROM mdl_user_course_mapping ucm
                        JOIN mdl_user u ON u.id = ucm.userid
                        JOIN mdl_course c ON c.id = ucm.courseid
                        WHERE u.deleted = 0 AND c.deleted = 0 $onlineCourseWhere";

$assignmentRecords = $DB->get_record_sql($assignmentSQL);
$totalassignment = $assignmentRecords->onlinecourseassignment + $assignmentRecords->classassignment + $assignmentRecords->programassignment;

//Completion record
if (!empty($from) && !empty($to)) {
    $onlineCourseCompletionWhere = " AND cud.completion_date between $from AND $to";
    $classroomCourseCompletionWhere = " AND se.timemodified between $from AND $to";
} elseif (!empty($from) && empty($to)) {
    $onlineCourseCompletionWhere = " AND cud.completion_date > $from";
    $classroomCourseCompletionWhere = " AND se.timemodified > $from";
} elseif (empty($from) && !empty($to)) {
    $onlineCourseCompletionWhere = " AND cud.completion_date < $to";
    $classroomCourseCompletionWhere = " AND se.timemodified < $to";
}
$assignmentCompletedSQL = "SELECT COUNT(temp.id) AS onlineassignment, (
                            SELECT COUNT(*)
                            FROM mdl_scheduler_enrollment se
                            JOIN mdl_user u ON u.id = se.userid
                            JOIN mdl_scheduler mc ON mc.id = se.scheduler_id
                            WHERE se.is_approved=1 AND u.deleted = 0 AND mc.isactive = 1 AND se.is_completed = 1 $classroomCourseCompletionWhere) AS classcompleted
                            FROM (
                            SELECT cud.*
                            FROM mdl_reports_data cud
                            JOIN mdl_user_course_mapping ucm ON ucm.userid = cud.user_id AND ucm.courseid = cud.course_id AND ucm.status = 1 AND cud.course_status IN ('completed', 'passed') $onlineCourseCompletionWhere
                            JOIN mdl_user u ON u.id = ucm.userid AND u.deleted = 0
                            GROUP BY cud.user_id, cud.course_id) AS temp";

$assignmentCompletedRecords = $DB->get_record_sql($assignmentCompletedSQL);
$totalAssignmentCompletedRecords = ($assignmentCompletedRecords->onlineassignment + $assignmentCompletedRecords->classcompleted);

$newuserSQL = "select YEAR(FROM_UNIXTIME( timecreated )) as Year, count(id) as totalcount from mdl_user WHERE deleted = 0 AND id NOT IN (1,2) $usersWhere and timecreated>0 GROUP BY YEAR( FROM_UNIXTIME( timecreated ) )";
$newUserRecords = $DB->get_records_sql($newuserSQL);

$newCourseSQL = "SELECT YEAR(FROM_UNIXTIME( timecreated )) as Year, COUNT(id) as totalcoursecount FROM mdl_course WHERE deleted = 0 AND id NOT IN (1) $courseWhere and timecreated>0 GROUP BY YEAR( FROM_UNIXTIME( timecreated ) )";
$newCourseRecords = $DB->get_records_sql($newCourseSQL);

$onlineAssignmentSQL = "SELECT YEAR(FROM_UNIXTIME( ucm.created_on )) as Year, COUNT(*) AS onlinecourseassignment
                        FROM mdl_user_course_mapping ucm
                        JOIN mdl_user u ON u.id = ucm.userid
                        JOIN mdl_course c ON c.id = ucm.courseid
                        WHERE u.deleted = 0 AND c.deleted = 0 $onlineCourseWhere GROUP BY YEAR( FROM_UNIXTIME( ucm.created_on ) )";


$newassignmentRecord = $DB->get_records_sql($onlineAssignmentSQL);

$classAssignmentSQL = "SELECT YEAR(FROM_UNIXTIME( se.timecreated )) as Year, COUNT(*) as classcount
                            FROM mdl_scheduler_enrollment se
                            JOIN mdl_user u ON u.id = se.userid
                            JOIN mdl_scheduler mc ON mc.id = se.scheduler_id
                            WHERE se.is_approved=1 AND u.deleted = 0 AND mc.isactive = 1 AND se.is_completed = 1 $classroomCourseWhere GROUP BY YEAR( FROM_UNIXTIME( se.timecreated ) )";


$newclassassignmentRecord = $DB->get_records_sql($classAssignmentSQL);

$programAssignmentSQL = "SELECT YEAR(FROM_UNIXTIME( pum.created_on )) as Year, COUNT(*) as programcount
                            FROM mdl_program_user_mapping pum
                            JOIN mdl_user u ON u.id = pum.user_id AND u.deleted = 0
                            JOIN mdl_programs p ON p.id = pum.program_id AND p.status = 1
                            WHERE pum.status = 1 $programWhere GROUP BY YEAR( FROM_UNIXTIME( pum.created_on ))";


$newprogramignmentRecord = $DB->get_records_sql($programAssignmentSQL);

$onlineCompletionSQL = " SELECT YEAR(FROM_UNIXTIME( temp.last_accessed )) as Year, COUNT(temp.id) AS onlineassignment from (SELECT cud.*
                            FROM mdl_reports_data cud
                            JOIN mdl_user_course_mapping ucm ON ucm.userid = cud.user_id AND ucm.courseid = cud.course_id AND ucm.status = 1 AND cud.course_status IN ('completed', 'passed') $onlineCourseCompletionWhere
                            JOIN mdl_user u ON u.id = ucm.userid AND u.deleted = 0
                            GROUP BY cud.user_id, cud.course_id)as temp group by YEAR(FROM_UNIXTIME(temp.last_accessed))";
$newonlineCompletionRecord = $DB->get_records_sql($onlineCompletionSQL);

$classCompletionSQL = " SELECT YEAR(FROM_UNIXTIME( se.timemodified )) as Year, COUNT(*) as classcompletion
                            FROM mdl_scheduler_enrollment se
                            JOIN mdl_user u ON u.id = se.userid
                            JOIN mdl_scheduler mc ON mc.id = se.scheduler_id
                            WHERE se.is_approved=1 AND u.deleted = 0 AND mc.isactive = 1 AND se.is_completed = 1 $classroomCourseCompletionWhere group by YEAR(FROM_UNIXTIME(se.timemodified))";
$classCompletionRecord = $DB->get_records_sql($classCompletionSQL);

//Login User

if (!empty($from) && !empty($to)) {
    $loginWhere = " AND time between $from AND $to";
} elseif (!empty($from) && empty($to)) {
    $loginWhere = " AND time >$from";
} elseif (empty($from) && !empty($to)) {
    $loginWhere = " AND time < $to";
}
$loginSQL = "select count(distinct(userid)) as totalloggedinuser from mdl_log where action  = 'login' $loginWhere";
$loginRecord = $DB->get_record_sql($loginSQL);

$newLoginSQL = "select YEAR(FROM_UNIXTIME( time )) as Year, count(distinct(userid)) as totalloggedinuser from mdl_log where action  = 'login' $loginWhere and time>0 GROUP BY YEAR( FROM_UNIXTIME( time ) )";
$newLoginRecords = $DB->get_records_sql($newLoginSQL);

$records = new stdClass();
$records->users = $usersrecords;
$records->logeedin = $loginRecord;
$records->courses = $courseRecords;
$records->assignment = $assignmentRecords;
$records->assignmentcompleted = $assignmentCompletedRecords;
$records->yearwiseusers = $newUserRecords;
$records->yearwisecourses = $newCourseRecords;
$records->yearwiseuniquelogin = $newLoginRecords;
$records->yearwiseonlineassignment = $newassignmentRecord;
$records->yearwiseclassassignment = $newclassassignmentRecord;
$records->yearwiseprogramassignment = $newprogramignmentRecord;
$records->yearwiseonlinecompletion = $newonlineCompletionRecord;
$records->yearwiseclasscompletion = $classCompletionRecord;

$myJSON = json_encode($records);
echo $myJSON;


