<?php
require_once("../config.php");
require_once("lib.php");
global $DB,$CFG,$USER;

checkLogin();

$userid    = optional_param('userid', 1, PARAM_INT);
if(!$userid){
	$userid = $USER->id;
}

$page    = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);        // how many per page
$action  = optional_param('action', '', PARAM_ALPHANUM);
$type  = optional_param('type', '-1', PARAM_RAW);
$status  = optional_param('status', '-1', PARAM_RAW);
$back = optional_param('back', 1, PARAM_INT);
$cType = optional_param('ctype', 2, PARAM_INT);
$course = optional_param('course', '-1', PARAM_RAW);
$classes = optional_param('classes', '-1', PARAM_RAW);
$sDate = '';
$sUserId = $userid;
$eDate = '';
$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);
$sDateSelected = '';
if($sStartDate){
	$sDateArr = getFormattedTimeStampOfDate($sStartDate);
	$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';
}		   
$eDateSelected = '';
if($sEndDate){
	$eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
	$eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
}
checkLogin();

if($action == 'print'){
	$PAGE->set_pagelayout('print');
}else{
	$PAGE->set_pagelayout('reportlayout');
}
$PAGE->set_heading($SITE->fullname);
$PAGE->set_title($SITE->fullname.": ".get_string('overallclassroomreport','scheduler'));
	
if($USER->archetype == 'learner'){
	 $PAGE->navbar->add(get_string('reports','multiuserreport'));
	 $PAGE->navbar->add(get_string('classroomreport'));
}else{
	if($back == 2){
	 $PAGE->navbar->add(get_string('reports','multiuserreport'));
	 $PAGE->navbar->add(get_string('usereperformancereport','multiuserreport'), new moodle_url($CFG->wwwroot.'/reports/learner_performance_report.php'));
	}elseif($back == 3){
	  $PAGE->navbar->add(get_string('reports','multiuserreport'));
	  $PAGE->navbar->add(get_string('userreport','multiuserreport'), new moodle_url($CFG->wwwroot.'/reports/user_report.php'));
	}elseif($back == 5){
	  $PAGE->navbar->add(get_string('manage_user'),'/admin/user.php');
	}else{
	 $PAGE->navbar->add(get_string('reports','multiuserreport'));
	 $PAGE->navbar->add(get_string('usereperformancereport','multiuserreport'), new moodle_url($CFG->wwwroot.'/reports/learner_performance_report.php'));
	}
	$PAGE->navbar->add(getUsers($userid));	
}
$paramArray = array(
	'back' => $back,
	'type' => $type,
	'status' => $status,
	'userid' => $userid,
	'ctype' => $cType,
	'course' => $course,
	'classes' => $classes,
	'startDate' => $sStartDate,
	'endDate' => $sEndDate
    );
if($action == 'exportcsv' || $action == 'print' || $action == 'exportpdf'){
	$returnHTML = exportLearnerClassReport($paramArray,$action);
	$perpage = 0;
}
$pageURL = '/reports/classroom_course_report.php';
checkPageIsNumeric($pageURL,$_REQUEST['page']);
$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
$sType = $paramArray['type'];
$sCourse = $paramArray['course'];
$sClasses = $paramArray['classes'];
$sCourseArr = explode("@",$sCourse);
$sClassesArr = explode("@",$sClasses);
$sTypeArr = explode("@",$sType);
echo $OUTPUT->header();
$enrolledData = getLearnerClassroomReport($paramArray,$page,$perpage,'');
$enrollentCount = getLearnerClassroomReport($paramArray,$page,0,'count_graph');
$enrollentCountPagination = getLearnerClassroomReport($paramArray,$page,0,'count');
$arrSessions = array();
$arrClass = array();
if(!empty($enrolledData)){
	foreach($enrolledData as $enrolled){
		$arrClass[] = $enrolled->class_id;
	}
	$arrSessions = getLearnerClassroomSessions($paramArray, $arrClass);
}
$total_records = 0;
if($enrollentCountPagination){
	$total_records = $enrollentCountPagination->total_records;
}
$arrEnrollmentData['graph_heading'] = get_string('overallclassroomreport','scheduler');
$arrEnrollmentData['total']['string'] = get_string('totalassignedusers','classroomreport');
$arrEnrollmentData['total']['value'] = $enrollentCount->total_records;
$arrEnrollmentData['data'][0]['string'] =get_string('inprogress','classroomreport');
$arrEnrollmentData['data'][0]['class'] = 'inprogress';
$arrEnrollmentData['data'][0]['value'] = $enrollentCount->incomplete;
$arrEnrollmentData['data'][1]['string'] = get_string('classnoshow','classroomreport');
$arrEnrollmentData['data'][1]['class'] = 'notstarted';
$arrEnrollmentData['data'][1]['value'] = $enrollentCount->no_show;
$arrEnrollmentData['data'][2]['string'] = get_string('completed','classroomreport');
$arrEnrollmentData['data'][2]['class'] = 'completed';
$arrEnrollmentData['data'][2]['value'] = $enrollentCount->completed;
$arrEnrollmentData['data'][3]['string'] = get_string('classnodata','classroomreport');
$arrEnrollmentData['data'][3]['class'] = 'nodata';
$arrEnrollmentData['data'][3]['value'] = $enrollentCount->no_data;
$arrEnrollmentData['url'] = $genURL;
$arrEnrollmentData['action'] = $action;
echo '<div class="clear"></div>';
if($USER->archetype != 'learner' && empty($export)) {
	$cSelected1 = $cType == 1?"selected='selected'":"";
	$cSelected2 = $cType == 2?"selected='selected'":"";
	echo '<select name="" backto="'.$sBack.'" id="reporttype" rel="'.$sUserId.'" class="reportTypeSelect" ><option value="'.$CFG->courseTypeOnline.'" '.$cSelected1.'>'.get_string('onlinecourse').'</option><option value="'.$CFG->courseTypeClassroom.'" '.$cSelected2.' >'.get_string('classroomcourse').'</option></select>';
}
echo '<div class="tabsOuter attendance-performance">';
if( $USER->archetype != $CFG->userTypeStudent && empty($export)) {
	if($back == 5){
		$user->id = $userid;
		ob_start();
		include_once($CFG->dirroot.'/user/user_tabs.php');
		$HTMLTabs .= ob_get_contents();
		ob_end_clean();
		echo $HTMLTabs;
		echo '<div class="borderBlockSpace">';
	}else{
		echo '<div class="borderBlockSpace">';
		if(empty($action)){
			ob_start();
			require_once($CFG->dirroot . '/local/includes/classroom_course_report_search.php');
			$SEARCHHTML = ob_get_contents();
			ob_end_clean();
			echo $SEARCHHTML;
		}else{
			echo $returnHTML;
		}
	}
}else{
	echo '<div class="borderBlockSpace">';
	if(empty($action)){
		ob_start();
		require_once($CFG->dirroot . '/local/includes/classroom_course_report_search.php');
		$SEARCHHTML = ob_get_contents();
		ob_end_clean();
		echo $SEARCHHTML;
	}else{
		echo $returnHTML;
	}
}
echo '<div class="f-right">'.get_string('reports_heading','',getReportSyncTime()).'</div>';
echo '<div class="clear"></div>';
$PerformanceData = getGraphData($arrEnrollmentData);
//pr($enrolledData);die;
echo '<table class = "generaltable" id="table1">';
	echo '<tr class=""><th  width="50%">Title</th>';
		echo "<th width = '12%' align='align_left' >" . get_string ( 'attended', 'scheduler' ) . "</th>";
		echo "<th width = '12%' align='align_left' >" . get_string ( 'grade', 'scheduler' ) . "</th>";
		echo "<th width = '12%' align='align_left' >" . get_string ( 'score', 'scheduler' ) . "</th>";
		echo "<th width = '14%' align='align_left' >" . get_string ( 'classcompletedstatus', 'scheduler' ) . "</th>";	
	echo '</tr>';
if(!empty($enrolledData)){
	foreach($enrolledData as $enrolData){
		$classDuration	= getClassDuration($enrolData->class_start, $enrolData->class_end);
		$classTitle		= $enrolData->classroom_name.' - '.$enrolData->class_name." (".$classDuration.")";
		if($enrolData->class_status == 0){
			$class_status = get_string('classnodata','classroomreport');
		}elseif($enrolData->class_status == 1){
				$class_status = get_string('classcompleted','classroomreport');
		}elseif($enrolData->class_status == 2){
			$class_status = get_string('classincomplete','classroomreport');
		}else{
			$class_status = get_string('classnoshow','classroomreport');
		}
		echo "<tr class='evenR'><td class='align_left'  width = '14%'><span class='f-left'>" . $classTitle . "</span></td>";
		echo "<td></td>";
		echo "<td></td>";
		echo "<td></td>";
		echo '<td>'.$class_status.'</td>';
		echo "</tr>";
		if(isset($arrSessions[$enrolData->class_id]) && !empty($arrSessions[$enrolData->class_id])){
			$courseHTML = '';
			$courseHTML .= "<tr><td colspan='5' class='borderLeftRight'>";
			$courseHTML .= "<div class='a-box'>";
			$i=0;
			foreach($arrSessions[$enrolData->class_id]['session'] as $session){
				$class = $i%2!=0?'even':'odd';
				$attended = 'No';
				$score = getMDash();
				$grade = getMDash();
				if($enrolData->class_status != 0){
					if($session['attendence'] == 1){
						if($attendedStatus == 0){
							$attendedStatus = 1;
						}
						$attended = 'Yes';
					}
					if($session['score']){
						$score = $session['score'];
					}
					if($session['grade']){
						$grade = $session['grade'];
					}
				}
				$courseHTML .= "<div class='".$class."' ><div style='width:50%'>" . $session['sessionname'] . "</div>";
				$courseHTML .= "<div style='width:12%'>".$attended."</div>";
				$courseHTML .= "<div style='width:12%'>".$grade."</div>";
				$courseHTML .= "<div style='width:12%'>".$score."</div>";
				$courseHTML .= "<div style='width:12%'></div>";
				$courseHTML .= "</div>";
			}
			$courseHTML .= "</div></td></tr>";
			echo $courseHTML;
		}
	}
}else{
	echo '<tr><td colspan="5">'.get_string('norecordfound','scheduler').'</td></tr>';
}
echo '</table>';
echo '</div>';
echo '</div>';
if($action != 'print'){
	echo paging_bar($total_records, $page, $perpage, $genURL);
}
echo  '<script>	$(document).ready(function(){										
$("#reporttype").change(function(){ 
	var userid = $(this).attr("rel");
	var backto = '.$back.';
	url = "'.$CFG->wwwroot.'/reports/user_performance_report.php?uid="+userid+"&back="+backto;		
window.location.href = url;
});
$("#printbun").bind("click", function(e) {
var url = $(this).attr("rel");
window.open(url, "'.get_string('courseusagesreport','classroomreport').'", "'.$CFG->printWindowParameter.'");
});	
}); 
</script>';
echo $OUTPUT->footer();
?>