<?php
require_once("../config.php");
require_once("lib.php");
global $DB,$CFG,$USER;
checkLogin();
$userId = optional_param('uid', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_RAW);
$back = optional_param('back', '', PARAM_RAW);
$sBack = $back;
$sCid = optional_param('cid', '', PARAM_RAW);
if( $USER->archetype == $CFG->userTypeStudent ) {
	$groupCourses = checkOwnerAccess($USER->id);
	if(!$groupCourses)
	{
		$userId = $USER->id;
	}elseif($userId == 0){
		$userId = $USER->id;
	}
}
$user->id = $userId;
checkLogin();
if($action == 'print'){
	$PAGE->set_pagelayout('print');
}else{
	$PAGE->set_pagelayout('reportlayout');
}
$PAGE->set_heading($SITE->fullname);

$PAGE->set_title($SITE->fullname.": ".get_string('usereperformancereport','multiuserreport'));
if($sCid){

    if($back == 1){
		$PAGE->navbar->add(get_string('reports','multiuserreport'));
		if($userId != $USER->id && $USER->archetype != $CFG->userTypeStudent){
			$PAGE->navbar->add(get_string('coursereport','multicoursereport'), new moodle_url($CFG->wwwroot.'/reports/course_report.php')); 
		}
		$PAGE->navbar->add(getCourses($sCid), new moodle_url($CFG->wwwroot.'/reports/course_progress_report.php?cid='.$sCid."&back=".$back));
	}elseif($back == 2){
		$PAGE->navbar->add(get_string('reports','multiuserreport'));
		if($userId != $USER->id && $USER->archetype != $CFG->userTypeStudent){
			$PAGE->navbar->add(get_string('courseusagesreport','multicoursereport'), new moodle_url($CFG->wwwroot.'/reports/online_course_usage_report.php')); 
		}
		$PAGE->navbar->add(getCourses($sCid), new moodle_url($CFG->wwwroot.'/reports/course_progress_report.php?cid='.$sCid));
	}elseif($back == 5 || $back == 6){
		$PAGE->navbar->add(get_string('managecourses'), new moodle_url($CFG->wwwroot.'/course/index.php'));
	}else{
		$PAGE->navbar->add(get_string('reports','multiuserreport'));
		 $PAGE->navbar->add(get_string('coursereport','multicoursereport'), new moodle_url($CFG->wwwroot.'/reports/course_report.php'));
	}
	
   
}else{
	if($back == 1){
		$PAGE->navbar->add(get_string('reports','multiuserreport'));
		$PAGE->navbar->add(get_string('userreport','multiuserreport'), new moodle_url($CFG->wwwroot.'/reports/user_report.php'));
	}elseif($back == 3){
		$PAGE->navbar->add(get_string('reports','multiuserreport'));
		 $PAGE->navbar->add(get_string('userreport','multiuserreport'), new moodle_url($CFG->wwwroot.'/reports/user_report.php'));
	}elseif($back == 5 || $back == 6){
		$PAGE->navbar->add(get_string('manage_user'),'/admin/user.php');
	}else{
		$PAGE->navbar->add(get_string('reports','multiuserreport'));
		if($userId == $USER->id && $USER->archetype == $CFG->userTypeStudent){
			$PAGE->navbar->add(get_string('coursereport'));
		}else{
			$PAGE->navbar->add(get_string('usereperformancereport','multiuserreport'), new moodle_url($CFG->wwwroot.'/reports/learner_performance_report.php'));
		}
	}
}

$userSql = "SELECT user_id,first_name,last_name,enrolled,not_started,in_progress,completed FROM mdl_report_learner_performance e WHERE e.user_id = ".$userId;
$userData = $DB->get_record_sql($userSql);
if($userId == $USER->id && $USER->archetype == $CFG->userTypeStudent){
}else{
	$PAGE->navbar->add($userData->first_name.' '.$userData->last_name);
}

$page    = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', $CFG->perpage, PARAM_INT);
$pageURL = '/reports/user_performance_report.php';
checkPageIsNumeric($pageURL,$_REQUEST['page']);
$sStartDate     = optional_param('startDate', '' , PARAM_RAW);
$sEndDate      = optional_param('endDate', '', PARAM_RAW);
$sStartDateCSV     = optional_param('sStartDate', '' , PARAM_RAW);
$sEndDateCSV = optional_param('sEndDate', '', PARAM_RAW);
$sDateSelected = '';
$startDate = '';
$endDate = '';
if($action != 'exportcsv' && $action != 'print' && $action != 'exportpdf'){
	if($sStartDate){
		$sDateArr = getFormattedTimeStampOfDate($sStartDate);
		$startDate = $sDateArr['timestamp'];
		$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

	}
	$eDateSelected = '';
	if(!empty($sEndDate)){
		$eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
		$endDate = $eDateArr['timestamp'];
		$eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
	}
}else{
	if($sStartDateCSV){
		$sDateArr = getFormattedTimeStampOfDate($sStartDateCSV);
		$startDate = $sDateArr['timestamp'];
		$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

	}
	$eDateSelected = '';
	if(!empty($sEndDateCSV)){
		$eDateArr = getFormattedTimeStampOfDate($sEndDateCSV, 1);
		$endDate = $eDateArr['timestamp'];
		$eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
	}
}
$paramArray = array('startDate' => $startDate,'endDate' => $endDate,'uid'=>$userId,'sStartDate'=>$sStartDate,'sEndDate'=>$sEndDate);
$removeKeyArray = array();
if($action == 'exportcsv' || $action == 'print' || $action == 'exportpdf'){
	$paramArrayCSV = array('startDate' => $startDate,'endDate' => $endDate,'uid'=>$userId,'eDateSelected'=>$eDateSelected,'sDateSelected'=>$sDateSelected);
	$returnHTML = learnerPerformanceDetailsExport($paramArrayCSV,$page,$perpage,$action,$userId);
	$perpage = 0;
}
echo $OUTPUT->header();

$enrolledData = getUserPerformanceReport($paramArray,$page,$perpage,$userId);
$enrollentCount = getUserPerformanceReport($paramArray,$page,0,$userId,'count');
$enrolledDataorder = setdisplayorderofdata($enrolledData);
//pr($enrolledData);

$arrAssets = array();
$arrCourses = array();
if(!empty($enrolledData)){
	foreach($enrolledData as $enrolled){
		$arrCourses[] = $enrolled->course_id;
	}
	$arrAssets = getCourseAssets($arrCourses,$userId);
}
$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
$arrEnrollmentData['graph_heading'] = get_string('overallcourseprogressreport','singlereport');
$arrEnrollmentData['total']['string'] = get_string('numberofcourses','multiuserreport');
$arrEnrollmentData['total']['value'] = $enrollentCount['total_records'];
$arrEnrollmentData['data'][0]['string'] = get_string('inprogress','multiuserreport');
$arrEnrollmentData['data'][0]['class'] = 'inprogress';
$arrEnrollmentData['data'][0]['value'] = $enrollentCount['in_progress'];
$arrEnrollmentData['data'][1]['string'] = get_string('notstarted','multiuserreport');
$arrEnrollmentData['data'][1]['class'] = 'notstarted';
$arrEnrollmentData['data'][1]['value'] = $enrollentCount['not_started'];
$arrEnrollmentData['data'][2]['string'] = get_string('completed','multiuserreport');
$arrEnrollmentData['data'][2]['class'] = 'completed';
$arrEnrollmentData['data'][2]['value'] = $enrollentCount['completed'];
$arrEnrollmentData['url'] = $genURL;
$arrEnrollmentData['action'] = $action;
$total_records = 0;
if($enrollentCount){
	$total_records = $enrollentCount['total_records'];
}
if($action != 'print' && !$sCid && $USER->archetype != 'learner'){
	echo '<select class="reportTypeSelect" name="" pagename="user_performance_report.php" id="reporttype" rel="'.$userId.'"><option value="'.$CFG->courseTypeOnline.'">'.get_string('onlinecourse').'</option><option value="'.$CFG->courseTypeClassroom.'">'.get_string('classroomcourse').'</option></select>';
}


echo '<div class="tabsOuter borderBlockSpace">';
if($action != 'print'){
	if($back == 5){
		ob_start(); 
		require_once($CFG->dirroot . '/user/user_tabs.php');
		$filterData = ob_get_contents();
		ob_end_clean();
		echo $filterData;
		echo '<div class="clear"></div></br>';
	}elseif($back == 6){
		$courseHTML .= "<div class='tabLinks'>";
		ob_start();
		$course->id = $sCid;
		include_once($CFG->dirroot . '/course/course_tabs.php');
		$HTMLTabs = ob_get_contents();
		ob_end_clean();
		$courseHTML .= $HTMLTabs;
		$courseHTML .= "</div>";
		echo $courseHTML;
		echo '<div class="clear"></div></br>';
	}else{
		ob_start();
		require_once($CFG->dirroot . '/local/includes/multiuser_report_details_search.php');
		$SEARCHHTML = ob_get_contents();
		ob_end_clean();
		echo $SEARCHHTML;
	}
}else{
	echo $returnHTML;
}
echo '<div class="f-right">'.get_string('reports_heading','',getReportSyncTime()).'</div>';
echo '<div class="clear"></div>';
$PerformanceData = getGraphData($arrEnrollmentData);

echo  '<div class=""><div class=""><table cellspacing="0" cellpadding="0" border="0" width="100%" class="table1 generaltable"><tbody>';
echo '<tr  class="table1_head"><th width="20%">'.get_string("coursetitle","singlereport").'</th><th width="10%">'.get_string("enrolmentdate","singlecoursereport").'</th><th width="13%">'.get_string("status","singlereport").'</th><th width="12%" >'.get_string("score","singlereport").'</th><th width="15%" >'.get_string("assettimespent","singlereport").'</th><th width="15%" >'.get_string("comletiondate").'</th><th width="15%" >'.get_string("lastaccessed","singlereport").'</th></tr>';
$rowCount=0;
if(!empty($enrolledData)){   
        $enrolledDatesSql = "SELECT courseid,created_on,createdby FROM mdl_user_course_mapping WHERE userid=".$userId;
        $EnrolledDates = $DB->get_records_sql($enrolledDatesSql);
        
	foreach($enrolledData as $enrolled){
                
                if($enrolled->depth==1){
                    $childClass = "childcourse";
                    $childAssetClass = 'child_assets_padding';
                }else{
                    $childClass = '';
                    $childAssetClass = 'parent_assets_padding';
                }
                if(isset($EnrolledDates[$enrolled->course_id])){
                    $EnrollmentDate =getDateFormat($EnrolledDates[$enrolled->course_id]->created_on, $CFG->customDefaultDateTimeFormat);
                }else{
                    $EnrollmentDate = getMDash();
                }
                
		$complianceDiv = getCourseComplianceIcon($enrolled->course_id);
		$classmMain = 'evenR';
		$assestHTML = '';
		if(!empty($arrAssets[$enrolled->course_id])){
			$assetCount = 1;
			$assestHTML .= '<div class="a-box" id="abox'.$enrolled->course_id.'" >';
			foreach($arrAssets[$enrolled->course_id] as $assetData){
				if($assetData->last_accessed){
					$lastAccessed = getDateFormat($assetData->last_accessed, $CFG->customDefaultDateTimeFormat);
				}else{
					$lastAccessed = get_string('notaccessed','learnercourse');
				}
				if($assetData->completion_date){
					$completionDateTime = getDateFormat($assetData->completion_date, $CFG->customDefaultDateTimeFormat);

				}else{
					 $completionDateTime = getMDash();
				}
                                
                                $assetScore = ($assetData->score)?$assetData->score:getMDash();
				$timeSpent = $assetData->time_spent?(convertCourseSpentTime($assetData->time_spent)):getMDash();
				$classEvenOdd = $assetCount%2==0 ?'even':'odd';
				$assestHTML .=  '<div class="'.$classEvenOdd.'" >';
				$assestHTML .=  '<div style="width:20%" class = "break-all" ><span class="'.$childAssetClass.'">'.$assetData->asset_name.'</span></div>';
                                $assestHTML .=  '<div style="width:10%">'.getMdash().'</div>';
				$assestHTML .=  '<div style="width:13%">'.ucwords($assetData->asset_status).'</div>';
				$assestHTML .=  '<div style="width:12%">'.$assetScore.'</div>';
				$assestHTML .=  '<div style="width:15%">'.$timeSpent.'</div>';
				$assestHTML .=  '<div style="width:20%">'.$completionDateTime.'</div>';
				$assestHTML .=  '<div style="width:20%">'.$lastAccessed.'</div>';
				$assestHTML .=  '</div>';
				$assetCount++;
			}
			$assestHTML .=  '</div>';
		}
		echo '<tr class="'.$classmMain.'" id="course-'.$enrolled->course_id.'">';
                        
                        echo '<td><span class="'.$childClass.'">'.$enrolled->course_name.$complianceDiv.'</span></td>';
                        echo '<td>'.$EnrollmentDate.'</td>';
			echo '<td>'.ucwords($enrolled->course_status).'</td>';
			echo '<td>'.getSpace().'</td>';
			echo '<td>'.getSpace().'</td>';
			echo '<td>'.getSpace().'</td>';
			echo '<td>'.getSpace().'</td>';
		echo '</tr>';
		echo '<tr><td colspan = "7" class="borderLeftRight padding_none">';                      
			echo $assestHTML;                        
		echo '</td></tr>';
		$rowCount++;
	}
}else{
	echo '<tr><td colspan = "7">'.get_string('no_results').'</td></tr>';
}
echo '</table><tbody></div></div>';
echo '</div>';
if($action != 'print'){
	echo paging_bar($total_records, $page, $perpage, $genURL);
}
$userHTML .=  '<script language="javascript" type="text/javascript">';
$userHTML .=  '	$(document).ready(function(){$("#printbun").bind("click", function(e) {var url = $(this).attr("rel");
window.open(url, "'.get_string('overallcourseprogressreport','singlereport').'", "'.$CFG->printWindowParameter.'");
});
}); ';
$userHTML .= '</script>';
echo $userHTML;
?>
<script language="javascript" type="text/javascript">
$(document).ready(function(){
	$("#printbun").bind("click", 
		function(e) {
		var url = $(this).attr("rel");
		window.open(url, "<?php echo get_string('overallcourseprogressreport','singlereport'); ?>", "<?php echo $CFG->printWindowParameter; ?>");
	});
	$("#reporttype").change(function(){
			var userid = $(this).attr("rel");
			var pagename = $(this).attr("pagename");
			var ctype = $(this).val();
			if(pagename=="single_report.php"){
			  url = "<?php echo $CFG->wwwroot; ?>/course/classroom_course_report.php?back=<?php echo ($sBack?$sBack:1); ?>&userid="+userid+"&ctype=2";	
			}else if(pagename=="user_performance_report.php"){
			  url = "<?php echo $CFG->wwwroot; ?>/reports/classroom_course_report.php?back=<?php echo ($sBack?$sBack:2); ?>&userid="+userid+"&ctype=2";	
			}
			window.location.href = url;
	});
}); 
</script>
<?php
echo $OUTPUT->footer();
?>