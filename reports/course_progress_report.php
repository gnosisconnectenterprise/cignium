<?php
	require_once("../config.php");
	require_once($CFG->dirroot . '/reports/lib.php');
	$sort    = optional_param('sort', 'COALESCE(NULLIF(mu.firstname, ""), mu.lastname), mu.lastname', PARAM_ALPHANUM);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', $CFG->perpage, PARAM_INT);        // how many per page
	$cid     = required_param('cid', PARAM_INT);
	$export  = optional_param('action', '', PARAM_ALPHANUM);
	$type  = optional_param('type', '', PARAM_RAW);
	$back  = optional_param('back', 2, PARAM_INT);
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);
			  
        $userstatus=optional_param('userstatus',-1,PARAM_RAW);
        $sDepartment    = optional_param('department', '-1', PARAM_RAW);
        $sCountry    = optional_param('country', '-1', PARAM_RAW);
        $sCity    = optional_param('city', '-1', PARAM_RAW);        
        $sJob_title    = optional_param('job_title', '-1', PARAM_RAW);
        $sReport_to    = optional_param('report_to', '-1', PARAM_RAW);
        $sIsManagerYes =optional_param('is_manager_yes', '-1', PARAM_RAW);
        $sTeam = optional_param('team', '-1', PARAM_RAW); 
	
	$paramArray = array(
	'cid' => $cid,
	'type' => $type,
	'back' => $back,
	'startDate' => $sStartDate,
	'endDate' => $sEndDate,
	'userstatus'=>$userstatus,
        'department' => $sDepartment,
	'team' => $sTeam,
        'country' => $sCountry,
        'city' => $sCity,
         'report_to' => $sReport_to,
         'job_title' => $sJob_title,
         'is_manager_yes' => $sIsManagerYes,
        'ch' => optional_param('ch', '', PARAM_ALPHA),
	'key' => optional_param('key', '', PARAM_RAW)
    );
        
        $filterArray = array(
	'cid' => $cid,
	'type' => $type,
	'back' => $back,
	'startDate' => $sStartDate,
	'endDate' => $sEndDate,
	'userstatus'=>$userstatus,
        'department' => $sDepartment,
	'team' => $sTeam,
        'country' => $sCountry,
        'city' => $sCity
        
    );
  
	
	$removeKeyArray = array('perpage');

	if($cid >0){
		$course = $DB->get_record('course',array('id'=> $cid));
		//$course = $DB->get_record('course',array('id'=> $cid), '*', MUST_EXIST);
	}else{
		redirect($CFG->wwwroot);
	}
	
	//$userrole =  getUserRole($USER->id);
	if( $USER->archetype == $CFG->userTypeStudent ) {
		$groupCourses = checkOwnerAccess($USER->id);
		if(!$groupCourses)
		{
			redirect($CFG->wwwroot);
		}
	}
	
		
	//require_login(); 
	checkLogin();
	
	
	$PAGE->set_pagelayout('reportlayout');
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('viewreportdetails','multicoursereport'));
	//$PAGE->navbar->add(get_string('reports','multicoursereport'), new moodle_url($CFG->wwwroot.'/my/adminreportdashboard.php'));
	
	
    if($back == 1){
		$PAGE->navbar->add(get_string('reports','multicoursereport'));
		$PAGE->navbar->add(get_string('coursereport','multicoursereport'), new moodle_url($CFG->wwwroot.'/reports/course_report.php'));
    }elseif($back == 6){
		$PAGE->navbar->add(get_string('managecourses'), new moodle_url($CFG->wwwroot.'/course/index.php'));
	}else{
		$PAGE->navbar->add(get_string('reports','multicoursereport'));
		$PAGE->navbar->add(get_string('courseusagesreport','multicoursereport'), new moodle_url($CFG->wwwroot.'/reports/online_course_usage_report.php')); 
    } 

	//$PAGE->navbar->add(get_string('viewreportdetails','multicoursereport'));
	$PAGE->navbar->add(getCourses($cid));
        
	if($CFG->isCignium == 'Y'){
		$courseVsMultipleUserReport = getCourseProgressReportScore($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
	}else{
		$courseVsMultipleUserReport = getCourseProgressReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
	}
	//$courseVsMultipleUserReport = getCourseProgressReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
	$courseHTML = $courseVsMultipleUserReport->courseHTML;
		   
	echo $OUTPUT->header();
        $pageURL = '/reports/course_progress_report.php';
        echo genCommonSearchForm($paramArray, $filterArray, $pageURL);	
	echo $courseHTML;
	

	echo includeGraphFiles(get_string('overalluserprogressreport','singlecoursereport'));
	
	echo $OUTPUT->footer();	
	//$courseHTML .= '<input type = "button" value = "'.get_string('back','multicoursereport').'" onclick="location.href=\''.$CFG->wwwroot.'/course/multicoursereport.php\';">';
