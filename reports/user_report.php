<?php
	require_once("../config.php");
	require_once($CFG->dirroot . '/reports/lib.php');	
	if( $USER->archetype == $CFG->userTypeStudent ) {
		$groupCourses = checkOwnerAccess($USER->id);
		if(!$groupCourses){
			redirect($CFG->wwwroot);
		}
	}	
	$PAGE->set_pagelayout('reportlayout');	
	checkLogin();	
	$sort    = optional_param('sort', 'COALESCE(NULLIF(firstname, ""), lastname), lastname', PARAM_ALPHANUM);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', $CFG->perpage, PARAM_INT);        // how many per page
	$pageURL = '/reports/user_report.php';
	checkPageIsNumeric($pageURL,$_REQUEST['page']);
	
	$sDate = '';
	$eDate = '';
	
	$sDepartment    = optional_param('department', '-1', PARAM_RAW);
	$sTeam          = optional_param('team', '-1', PARAM_RAW);       
	$sUser          = optional_param('user', '-1', PARAM_RAW);       
	$export         = optional_param('action', '', PARAM_ALPHANUM);
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);
	$sStatus      = optional_param('status', '0', PARAM_RAW);
        $sCity      = optional_param('city', '-1', PARAM_RAW);
        $sJobTitle      = optional_param('job_title', '-1', PARAM_RAW);
        $sReportTo      = optional_param('report_to', '-1', PARAM_RAW);
	
        $sIsManagerYes =optional_param('is_manager_yes', '-1', PARAM_RAW);
		
	$paramArray = array(
					'ch' => optional_param('ch', '', PARAM_ALPHA),
					'key' => optional_param('key', '', PARAM_RAW),
					'user_group' => optional_param('user_group', '-1', PARAM_RAW),
					'sel_mode' => optional_param('sel_mode', 1, PARAM_RAW),
					'department' => $sDepartment,
					'team' => $sTeam,
					'user' => $sUser,
					'startDate' => $sStartDate,
					'endDate' => $sEndDate,
					'status' => $sStatus,
					'job_title' => $sJobTitle,
                                        'report_to' => $sReportTo,
                                        'is_manager_yes' => $sIsManagerYes,
					'company' => optional_param('company', '-1', PARAM_RAW),
                                        'city' => $sCity,
				  );
	
	$filterArray = array(							
						'nm'=>get_string('name','course'),
				   );
	
	
	$removeKeyArray = array('perpage');
	
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('userreport','multiuserreport'));
	$PAGE->navbar->add(get_string('reports','multiuserreport'));
	$PAGE->navbar->add(get_string('userreport','multiuserreport'));
	echo $OUTPUT->header();
	$userReport = getUserReport_scheduler($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
	$userHTML = $userReport->userHTML;
        echo genCommonSearchForm($paramArray, $filterArray, $pageURL);
	echo $userHTML;	
	echo includeGraphFiles(get_string('userreport','multiuserreport'));	
	echo $OUTPUT->footer();
	

