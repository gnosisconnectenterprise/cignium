<?php
require_once("../config.php");
require_once("lib.php");
global $DB,$CFG,$USER;
checkLogin();
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', $CFG->perpage, PARAM_INT);
$pageURL = '/reports/learner_performance_report.php';
checkPageIsNumeric($pageURL,$_REQUEST['page']);
$sDepartment    = optional_param('department', '-1', PARAM_RAW);
$sTeam          = optional_param('team', '-1', PARAM_RAW);       
$sUser          = optional_param('user', '-1', PARAM_RAW);
$sType      = optional_param('type', '-1', PARAM_RAW);
$user_group      = optional_param('user_group','-1', PARAM_RAW);
$sel_mode      = optional_param('sel_mode', '1', PARAM_RAW);
$action      = optional_param('action', '', PARAM_RAW);
$sJobTitle = optional_param('job_title', '-1', PARAM_RAW);
$sReportTo = optional_param('report_to', '-1', PARAM_RAW);
$sIsManagerYes =optional_param('is_manager_yes', '-1', PARAM_RAW);
$sCompany = optional_param('company', '-1', PARAM_RAW);
if( $USER->archetype == $CFG->userTypeStudent ) {
	$groupCourses = checkOwnerAccess($USER->id);
	if(!$groupCourses)
	{
		redirect(new moodle_url('/my/dashboard.php'));
	}
}
checkLogin();
if($action == 'print'){
	$PAGE->set_pagelayout('print');
}else{
	$PAGE->set_pagelayout('reportlayout');
}
$PAGE->set_heading($SITE->fullname);

$PAGE->set_title($SITE->fullname.": ".get_string('usereperformancereport','multiuserreport'));
$PAGE->navbar->add(get_string('reports','multiuserreport'));
$PAGE->navbar->add(get_string('usereperformancereport','multiuserreport'));

//echo $action;die;
$paramArray = array('department' => $sDepartment,'team' => $sTeam,'user' => $sUser,'type' => $sType,'user_group' => $user_group,'sel_mode' => $sel_mode,'job_title' =>$sJobTitle,'report_to' =>$sReportTo,'is_manager_yes' =>$sIsManagerYes,'company' =>$sCompany,'ch' => optional_param('ch', '', PARAM_ALPHA),
					'key' => optional_param('key', '', PARAM_RAW));
$removeKeyArray = array();
if($action == 'exportcsv' || $action == 'print' || $action == 'exportpdf'){
	$returnHTML = learnerPerformanceToCSV($paramArray,$page,$perpage,$action);
	$perpage = 0;
}
$sDepartmentArr = explode("@",$sDepartment);
$sTeamArr = explode("@",$sTeam);
$sUserArr = explode("@",$sUser);
$sTypeArr = explode("@",$sType);
$sJobTitleArr = explode("@",$sJobTitle);
$sCompanyArr = explode("@",$sCompany);

$sIsManagerArr = explode("@",$sIsManagerYes);
echo $OUTPUT->header();

$enrolledData = getLearnerPerformanceReport($paramArray,$page,$perpage,'*');
$enrollentCount = getLearnerPerformanceReport($paramArray,$page,0,'count_graph');
$enrollentCountPagination = getLearnerPerformanceReport($paramArray,$page,0,'count');
$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);

$arrEnrollmentData['graph_heading'] = get_string('usereperformancereport','multiuserreport');
$arrEnrollmentData['total']['string'] = get_string('numberofcourses','multiuserreport');
$arrEnrollmentData['total']['value'] = $enrollentCount->total_enrollment;
$arrEnrollmentData['data'][0]['string'] = get_string('inprogress','multiuserreport');
$arrEnrollmentData['data'][0]['class'] = 'inprogress';
$arrEnrollmentData['data'][0]['value'] = $enrollentCount->total_inprogress;
$arrEnrollmentData['data'][1]['string'] = get_string('notstarted','multiuserreport');
$arrEnrollmentData['data'][1]['class'] = 'notstarted';
$arrEnrollmentData['data'][1]['value'] = $enrollentCount->total_notstarted;
$arrEnrollmentData['data'][2]['string'] = get_string('completed','multiuserreport');
$arrEnrollmentData['data'][2]['class'] = 'completed';
$arrEnrollmentData['data'][2]['value'] = $enrollentCount->total_completed;
$arrEnrollmentData['url'] = $genURL;
$arrEnrollmentData['action'] = $action;
$total_records = 0;
if($enrollentCountPagination){
	$total_records = $enrollentCountPagination->total_records;
}

if($action != 'print'){
    echo genCommonSearchForm($paramArray, $filterArray, $pageURL);
}

echo '<div class="tabsOuter borderBlockSpace">';
if($action == 'print'){
	$filterData = $returnHTML;
}else{
	ob_start(); 
	require_once($CFG->dirroot . '/local/includes/multiuserreportsearch.php');
	$filterData = ob_get_contents();
	ob_end_clean();
}
echo $filterData;
echo '<div class="f-right">'.get_string('reports_heading','',getReportSyncTime()).'</div>';
echo '<div class="clear"></div>';
$PerformanceData = getGraphData($arrEnrollmentData);
echo  '<div class=""><div class=""><table cellspacing="0" cellpadding="0" border="0" width="100%" class="generaltable"><tbody>';
echo '<tr><th width="12%">'.get_string('fullname','multiuserreport').'</th><th width="12%">'.get_string('username').'</th><th width="12%">'.get_string('jobtitle').'</th><th width="14%">'.get_string('report_to').'</th><th width="21%">'.get_string('ismanageryescsv').'</th><th width="5%">'.get_string('enrolled','multiuserreport').'</th><th width="8%">'.get_string('notstarted','multiuserreport').'</th><th width="8%">'.get_string('inprogress','multiuserreport').'</th><th width="8%">'.get_string('completed','multiuserreport').'</th></tr>';
if(!empty($enrolledData)){
        $mDash = getMDash();
	foreach($enrolledData as $enrolled){
                $enrolled->title = ($enrolled->title)?$enrolled->title:$mDash;
                $enrolled->report_to = ($enrolled->report_to)?$enrolled->report_to:$mDash;
                
                $enrolled->is_manager_yes = $CFG->isManagerYesOptions[$enrolled->is_manager_yes];
		if($action != 'print'){
			$link = $CFG->wwwroot.'/reports/user_performance_report.php?back=2&uid='.$enrolled->user_id;
			echo '<tr>';
			echo '<td><a href = "'.$link.'">'.$enrolled->fullname.'</a></td>';
			echo '<td><a href = "'.$link.'">'.$enrolled->user_name.'</a></td>';
                        echo '<td>'.$enrolled->title.'</td>';
                        echo '<td>'.$enrolled->report_to.'</td>';
                        echo '<td>'.$enrolled->is_manager_yes.'</td>';
			echo '<td><a href = "'.$link.'">'.$enrolled->enrolled.'</a></td>';
			echo '<td><a href = "'.$link.'&type=not_started">'.$enrolled->not_started.'</a></td>';
			echo '<td><a href = "'.$link.'&type=in_progress">'.$enrolled->in_progress.'</a></td>';
			echo '<td><a href = "'.$link.'&type=completed">'.$enrolled->completed.'</a></td>';
			echo '</tr>';
		}else{
			echo '<tr>';
			echo '<td>'.$enrolled->fullname.'</td>';
			echo '<td>'.$enrolled->user_name.'</td>';
                        echo '<td>'.$enrolled->title.'</td>';
                        echo '<td>'.$enrolled->report_to.'</td>';
                         echo '<td>'.$enrolled->is_manager_yes.'</td>';
			echo '<td>'.$enrolled->enrolled.'</td>';
			echo '<td>'.$enrolled->not_started.'</td>';
			echo '<td>'.$enrolled->in_progress.'</td>';
			echo '<td>'.$enrolled->completed.'</td>';
			echo '</tr>';
		}
	}
}else{
	echo '<tr><td colspan = "6">'.get_string('no_results').'</td></tr>';
}
echo '</table><tbody></div></div>';
echo '</div>';
if($action != 'print'){
	echo paging_bar($total_records, $page, $perpage, $genURL);
}
$userHTML .=  '<script language="javascript" type="text/javascript">';
$userHTML .=  '	$(document).ready(function(){$("#printbun").bind("click", function(e) {var url = $(this).attr("rel");
window.open(url, "'.get_string('usereperformancereport','multiuserreport').'", "'.$CFG->printWindowParameter.'");
});
}); ';
$userHTML .= '</script>';
echo $userHTML;
echo $OUTPUT->footer();
?>
