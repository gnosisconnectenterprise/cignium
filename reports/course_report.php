<?php
	require_once("../config.php");
	if( $USER->archetype == $CFG->userTypeStudent ) {
		$groupCourses = checkOwnerAccess($USER->id);
		if(!$groupCourses)
		{
			redirect(new moodle_url('/my/learnerdashboard.php'));
		}
	}	 
	checkLogin();		
	$PAGE->set_pagelayout('reportlayout');
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('coursereport','multicoursereport'));
	$PAGE->navbar->add(get_string('reports','multicoursereport'));
	$PAGE->navbar->add(get_string('coursereport','multicoursereport'));

	$sort    = optional_param('sort', 'mc.fullname', PARAM_RAW);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', $CFG->perpage, PARAM_INT);        // how many per page
	$export = optional_param('action', '', PARAM_ALPHANUM);
	$pageURL = '/reports/course_report.php';
	checkPageIsNumeric($pageURL,$_REQUEST['page']);
	require_once($CFG->dirroot . '/reports/lib.php');		
	$sDate = '';
	$eDate = '';		
	$sCategory          = optional_param('category', '-1', PARAM_RAW); 
	$sActive          = optional_param('active', '-1', PARAM_RAW); 
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);
	$paramArray = array(
					'category' => $sCategory,
					'active' => $sActive,
					'startDate' => $sStartDate,
					'endDate' => $sEndDate,
                                        'ch' => optional_param('ch', '', PARAM_ALPHA),
                                        'key' => optional_param('key', '', PARAM_RAW)
				  );	
	$removeKeyArray = array();
    $courseReport = getCourseReport_scheduler($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
	$courseHTML = $courseReport->courseHTML;	   
	echo $OUTPUT->header(); 
	echo $OUTPUT->skip_link_target();
        
	echo $courseHTML;	
    echo includeGraphFiles(get_string('coursereport','multicoursereport'));
	echo $OUTPUT->footer();
