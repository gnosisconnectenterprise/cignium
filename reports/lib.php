<?php
//function Get  Course Report using scheduler

function getCourseReport_scheduler($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export=''){
	
	global $DB, $CFG, $USER;
	$courseReport = new stdClass();
	$isReport = true;
	$offset = $page - 1;
	$offset = $offset*$perpage;
	$limit = '';
	if($perpage != 0){
		$limit = "LIMIT $offset, $perpage";
	}

	$pageURL = '/reports/'.$CFG->pageCourseReport;
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);

	$printUrl = '/reports/'.$CFG->pageCourseReportPrint;
	$genPrintURL = genParameterizedURL($paramArray, $removeKeyArray, $printUrl);

	$sCategory      = $paramArray['category'];
	$sActive       = $paramArray['active'];
	$sStartDate     = $paramArray['startDate'];
	$sEndDate       = $paramArray['endDate'];

	$sDateSelected = '';
	if($sStartDate){
		$sDateArr = getFormattedTimeStampOfDate($sStartDate);
		$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

	}

	$eDateSelected = '';
	if($sEndDate){
		$eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
		$eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
	}

	$sCategoryArr = explode("@",$sCategory);
	$sActiveArr = explode("@",$sActive);

	$sCategoryName = $sCategory=='-1'?(get_string('all','multicoursereport')):getCourseCategory($sCategoryArr);



	if($sActive == '-1'){
		$sActiveName = get_string('all','multicoursereport');
	}else{		 
		$repActiveArr = array(0 => get_string('statusdeactive','multicoursereport'), 1 => get_string('statusactive','multicoursereport'), 2 => get_string('statusunpublished','multicoursereport'));
		$sActiveArr2 = str_replace(array(0,1,2), $repActiveArr, $sActiveArr);
		$sActiveName = count($sActiveArr2) > 0 ? implode(", ", $sActiveArr2) : 0;
	}

	// search condition start here
	$searchString = "";
	$subSearchString = "";

	if( $USER->archetype == $CFG->userTypeManager) {
		 
                $sql = "select DISTINCT course_id FROM mdl_online_course_usage_details WHERE user_department={$USER->department}";
		$managerCourseIdArr = array_keys($DB->get_records_sql($sql));//getManagerCourse($isReport);
                
                
                
		$groupCourses = fetchGroupsCourseIds(0,0,1);
		$managerCourseIdArr = array_merge($managerCourseIdArr, $groupCourses);
		$managerCourseIds = count($managerCourseIdArr) > 0 ? implode(",", $managerCourseIdArr) : 0 ;
		$searchString .= ' AND mc.id IN ('.$managerCourseIds.') ';
		$subSearchString .= ' AND msc.id IN ('.$managerCourseIds.') ';
	}else{
		if( $USER->archetype == $CFG->userTypeStudent) {
			$groupCourses = fetchGroupsCourseIds(0,0,1);
			$managerCourseIds = count($groupCourses) > 0 ? implode(",", $groupCourses) : 0 ;
			$searchString .= ' AND mc.id IN ('.$managerCourseIds.') ';
			$subSearchString .= ' AND msc.id IN ('.$managerCourseIds.') ';
		}
	}
	if(count($sCategoryArr) > 0 && !in_array($sCategoryArr[0] , array(0, '-1'))){
		$sCategoryStr = implode(",", $sCategoryArr);
		$searchString .= " AND mc.category IN ($sCategoryStr) ";
		$subSearchString .= " AND msc.category IN ($sCategoryStr) ";
	}
	$sActiveArrr = $sActiveArr;
	$searchStatusString = '';
	
	if(count($sActiveArrr) > 0 &&  !in_array($sActiveArrr[0] , array('-1'))){	 
		 
		if(count($sActiveArrr) == 3){

		}else{
			if(in_array(2, $sActiveArrr)){
				foreach($sActiveArrr as $kk=>$sActiveVal){
					if($sActiveVal == 2){
						unset($sActiveArrr[$kk]);
					}
				}
				$searchStatusString .= " AND (mc.publish IN (0) ";					
				if(count($sActiveArrr) > 0 ){
					$sActiveStr = implode(",", $sActiveArrr);
					$searchStatusString .= "  OR mc.is_active IN ($sActiveStr)";
				}
				$searchStatusString .= ")";
			}else{
				$sActiveStr = implode(",", $sActiveArrr);
				$searchStatusString .= " AND mc.publish IN (1) AND mc.is_active IN ($sActiveStr)";
			}
		}		 
	}

	if($sDateSelected){
		$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
		$searchString .= " AND mc.timecreated >= ($sStartDateTime) ";
		$subSearchString .= " AND msc.timecreated >= ($sStartDateTime) ";
	}

	if($eDateSelected){
		$sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
		$searchString .= " AND mc.timecreated <= ($sEndDateTime) ";
		$subSearchString .= " AND msc.timecreated <= ($sEndDateTime) ";
	}

        $paramArray['key'] = addslashes($paramArray['key']);
    if(($paramArray['key'] != '')) {
        $searchString .= " AND (mc.fullname like '%".$paramArray['key']."%' OR mcc.name like '%".$paramArray['key']."%' )";
    }
	// search condition end here
	$fullnameField = extractMDashInSql('mc.fullname');
	$query = "select  mc.id,mc.id as course_id, $fullnameField as fullname, mc.summary, mc.timecreated,mc.enddate ";
	$query .= ",mcc.name as category";
	$query .= ",concat(mu_created.firstname, ' ', mu_created.lastname) as createdbyname, mu_created.username as createdby_username";
	$query .= ",mc.publish, mc.is_active";
	$query .= ", (SELECT count(msc.id) FROM {$CFG->prefix}course msc WHERE msc.is_active = '1' AND msc.publish = '1'  AND msc.deleted = '0' AND msc.coursetype_id = '".$CFG->courseTypeOnline."' AND msc.id!='1'  $subSearchString) AS publish_and_active";
	$query .= ", (SELECT count(msc.id) FROM {$CFG->prefix}course msc WHERE msc.is_active = '0' AND msc.publish = '1'  AND msc.deleted = '0' AND msc.coursetype_id = '".$CFG->courseTypeOnline."' AND msc.id!='1'  $subSearchString) AS publish_and_deactive";
	$query .= ", (SELECT count(msc.id) FROM {$CFG->prefix}course msc WHERE msc.publish = '0'  AND msc.deleted = '0' AND msc.id!='1' AND msc.coursetype_id = '".$CFG->courseTypeOnline."' $subSearchString) AS unpublished";
	$query .= " FROM {$CFG->prefix}course mc  ";
	$query .= " LEFT JOIN {$CFG->prefix}course_categories mcc ON (mc.category = mcc.id)  ";
	$query .= " LEFT JOIN {$CFG->prefix}user mu_created ON (mu_created.id = mc.createdby) ";
	$query .= " WHERE 1 = 1  ";
	$query .= " AND mc.deleted = '0'  ";
	$query .= "   AND mc.coursetype_id = '".$CFG->courseTypeOnline."'";

	$query .= "   AND mc.id != '1' ";
	$query .=    $searchString;
	$queryCount = $query."  GROUP BY mc.id ";
	$query .=    $searchStatusString;
	$query .= "  GROUP BY mc.id ";
	$query .= " ORDER BY $sort ";


	$queryLimit = $query.$limit;
//echo $query;die;
	$courseAllArr = $DB->get_records_sql($query);
	$courseCount = count($courseAllArr);
	$courseArr = $DB->get_records_sql($queryLimit);


	$courseCountRes = $DB->get_records_sql($queryCount);
	$courseCountAll = count($courseCountRes);

	$courseHTML = '';
      //  echo $export;die;
        if($export != 'print'){
            $courseHTML .= genCommonSearchForm($paramArray, $filterArray, $pageURL);
        }
        
	$style = !empty($export)?"style=''":'';
	$courseHTML .= "<div class='tabsOuter' ".$style."><div class='borderBlockSpace'>";
	$courseHTML .= '<div class="clear"></div>';
	$exportHTML = '';
	if(empty($export)){
		 
		ob_start();
		require_once($CFG->dirroot . '/local/includes/coursereportsearch_filter.php');
		$SEARCHHTML = ob_get_contents();
		ob_end_clean();

		$courseHTML .= $SEARCHHTML;

		$exportHTML .= '<div class="exports_opt_box" > ';
		$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','multicoursereport').'" href="'.$genPrintURL.'&action=exportcsv">'.get_string('downloadcsv','multicoursereport').'</a> ';
		$exportHTML .= '<span class="seperater">&nbsp;</span>';
		$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','multicoursereport').'" id="exportpdf1" href="'.$genPrintURL.'&action=exportpdf">'.get_string('downloadpdf','multicoursereport').'</a> ';
		$exportHTML .= '<span class="seperater">&nbsp;</span>';
		$exportHTML .= '<a rel="'.$genPrintURL.'&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','multicoursereport').'" class="print_icon">'.get_string('print','multicoursereport').'</a>';
		$exportHTML .= '</div>';
		 
	}

	$courseHTML .= '<div class="userprofile view_assests">';

	$publishAndActive = 0;
	$publishAndDeactive = 0;
	$unpublishAndDeactive = 0;
	$unpublishAndActive = 0;
	$publishAndActiveExploded = false;
	$publishAndDeactiveExploded = false;
	$unpublishedExploded = false;

	$reportContentPDF = '';
	if($courseCount > 0 ){

		foreach($courseAllArr as $courses){

			$publishAndActive = $courses->publish_and_active;
			$publishAndDeactive = $courses->publish_and_deactive;
			$unpublished = $courses->unpublished;
			break;
		}


		if(in_array(1, $sActiveArr)){
			$publishAndActiveExploded = true;
		}

		if(in_array(0, $sActiveArr)){
			$publishAndDeactiveExploded = true;
		}

		if(in_array(2, $sActiveArr)){
			$unpublishedExploded = true;
		}
		 
		$publishAndActivePercentage = numberFormat(($publishAndActive*100)/$courseCountAll);
		$publishAndDeactivePercentage = numberFormat(($publishAndDeactive*100)/$courseCountAll);
		$unpublishedPercentage = numberFormat(($unpublished*100)/$courseCountAll);

		 
		$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
		$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
		$sDateSelectedForPrintCSV = getDateFormatForPrint($sDateSelected, 'csv');
		$eDateSelectedForPrintCSV = getDateFormatForPrint($eDateSelected, 'csv');

		if(!empty($export) && $export == 'print'){

			$reportName = get_string('coursereport','multicoursereport');
			$courseHTML .= getReportPrintHeader($reportName);
			$courseHTML .= '<table border="0" cellpadding="0" cellspacing="0"  class="generaltable margin_bottom">';
			$courseHTML .= '<tr>';
			$courseHTML .= '<td width="500"><strong>'.get_string('category','multicoursereport').':</strong> '.$sCategoryName.'</td>';
			$courseHTML .= '<td width="500"><strong>'.get_string('status','multicoursereport').':</strong>  '.$sActiveName.'</td>';
			$courseHTML .= '</tr>';
			$courseHTML .= '<tr>';
			$courseHTML .= '<td><strong>'.get_string('daterangefrom','multicoursereport').':</strong> '.$sDateSelectedForPrint.'</td>';
			$courseHTML .= '<td><strong>'.get_string('daterangeto','multicoursereport').':</strong>  '.$eDateSelectedForPrint.'</td>';
			$courseHTML .= '</tr>';
			$courseHTML .= '<tr>';
			$courseHTML .= '<td width="50%"  ><strong>'.get_string('numberofcourses','multicoursereport').':</strong> '.$courseCountAll.'</td>';
			$courseHTML .= '<td width="50%" ><strong>'.get_string('statusactive','multicoursereport').':</strong>  '.$publishAndActive.' ('.floatval($publishAndActivePercentage).'%)</td>';
			$courseHTML .= '</tr>';
			$courseHTML .= '<tr>';
			$courseHTML .= '<td width="50%"  ><strong>'.get_string('statusdeactive','multicoursereport').':</strong> '.$publishAndDeactive.' ('.floatval($publishAndDeactivePercentage).'%)</td>';
			$courseHTML .= '<td width="50%" ><strong>'.get_string('statusunpublished','multicoursereport').':</strong>  '.$unpublished.' ('.floatval($unpublishedPercentage).'%)</td>';
			$courseHTML .= '</tr>';
			$courseHTML .= '</table>';

		}
		 
		$courseHTML .= '<div class = "single-report-start" id="watch"   >
								<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('coursereport','multicoursereport').$exportHTML.'</span>
								  <div class="single-report-graph-left" id="chartContainer">'.get_string('loading','multicoursereport').'</div>
								  <div class="single-report-graph-right">
									<div class="course-count-heading">'.get_string('numberofcourses','multicoursereport').'</div>
									<div class="course-count">'.$courseCountAll.'</div>
									<div class="seperator">&nbsp;</div>
									<div class="course-status">
									 
									  <div class="notstarted"><h6>'.get_string('statusdeactive','multicoursereport').'</h6><span>'.$publishAndDeactive.'</span></div>
									  <div class="clear"></div>
									  <div class="inprogress"><h6>'.get_string('statusunpublished','multicoursereport').'</h6><span>'.$unpublished.'</span></div>
									  <div class="clear"></div>
									   <div class="completed"><h6>'.get_string('statusactive','multicoursereport').'</h6><span>'.$publishAndActive.'</span></div>
									 
									</div>
								  </div>
								</div>
							  </div>
							  <div class="clear"></div>';

	}

	$courseHTML .= '<div class=""><div class=""><table cellspacing="0" cellpadding="0" border="0" class="generaltable"><tbody>';
	$courseHTML .= '<tr>';
	$courseHTML .=  '<th width="30%">'.get_string('coursetitle','multicoursereport').'</th>';
	$courseHTML .=  '<th width="15%">'.get_string('category','multicoursereport').'</th>';
	$courseHTML .=  '<th width="15%">'.get_string('dateofcreation','multicoursereport').'</th>';
        $courseHTML .=  '<th width="10%">'.get_string('expiry_date_course_form').'</th>';
	$courseHTML .=  '<th width="15%">'.get_string('status','multicoursereport').'</th>';
	$courseHTML .=  '<th width="15%">'.get_string('createdby','multicoursereport').'</th>';
	$courseHTML .=  '</tr>';
	$reportContentCSV .= get_string('category','multicoursereport').",".$sCategoryName."\n".get_string('status','multicoursereport').",".$sActiveName."\n";
	$reportContentCSV .= get_string('daterangefrom','multicoursereport').",".$sDateSelectedForPrintCSV."\n".get_string('daterangeto','multicoursereport').",".$eDateSelectedForPrintCSV."\n";
	$reportContentCSV .= get_string('numberofcourses','multicoursereport').",".$courseCountAll."\n".get_string('statusactive','multicoursereport').",".$publishAndActive." (".floatval($publishAndActivePercentage)."%)\n";
	$reportContentCSV .= get_string('statusdeactive','multicoursereport').",".$publishAndDeactive." (".floatval($publishAndDeactivePercentage)."%)\n".get_string('statusunpublished','multicoursereport').",".$unpublished." (".floatval($unpublishedPercentage)."%)\n";
	$reportContentCSV .= get_string('coursereport','multicoursereport')."\n";
	$reportContentCSV .= get_string('coursetitle','multicoursereport').",".get_string('category','multicoursereport').",".get_string('dateofcreation','multicoursereport').",".get_string('expiry_date_course_form').",".get_string('status','multicoursereport').",".get_string('createdby','multicoursereport')."\n";
	$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" class="generaltable">';
	$reportContentPDF .= '<tr>';
	$reportContentPDF .= '<td width="50%" ><strong>'.get_string('category','multicoursereport').':</strong> '.$sCategoryName.'</td>';
	$reportContentPDF .= '<td width="50%" ><strong>'.get_string('status','multicoursereport').':</strong>  '.$sActiveName.'</td>';
	$reportContentPDF .= '</tr>';
	$reportContentPDF .= '<tr>';
	$reportContentPDF .= '<td width="50%"  ><strong>'.get_string('daterangefrom','multicoursereport').':</strong> '.$sDateSelectedForPrint.'</td>';
	$reportContentPDF .= '<td width="50%" ><strong>'.get_string('daterangeto','multicoursereport').':</strong>  '.$eDateSelectedForPrint.'</td>';
	$reportContentPDF .= '</tr>';
	$reportContentPDF .= '<tr>';
	$reportContentPDF .= '<td width="50%"  ><strong>'.get_string('numberofcourses','multicoursereport').':</strong> '.$courseCountAll.'</td>';
	$reportContentPDF .= '<td width="50%" ><strong>'.get_string('statusactive','multicoursereport').':</strong>  '.$publishAndActive.' ('.floatval($publishAndActivePercentage).'%)</td>';
	$reportContentPDF .= '</tr>';
	$reportContentPDF .= '<tr>';
	$reportContentPDF .= '<td width="50%"  ><strong>'.get_string('statusdeactive','multicoursereport').':</strong> '.$publishAndDeactive.' ('.floatval($publishAndDeactivePercentage).'%)</td>';
	$reportContentPDF .= '<td width="50%" ><strong>'.get_string('statusunpublished','multicoursereport').':</strong>  '.$unpublished.' ('.floatval($unpublishedPercentage).'%)</td>';
	$reportContentPDF .= '</tr>';
	$reportContentPDF .= '</table>';
	$reportContentPDF .= getGraphImageHTML(get_string('coursereport','multicoursereport'));
	
        //pr($courseArr);die;
	$currentTime = time();
	if(count($courseArr) > 0 ){
                $enrolledDataorder = setdisplayorderofdata($courseArr);
                
		$i = 0;
		$format = $CFG->customDefaultDateFormat;
		$formatCsv = $CFG->customDefaultDateFormatForCSV;
		foreach($courseArr as $courses){
			$courseId = $courses->id;
			$courseName = $courses->fullname;
                        $courseNameCSV = str_replace(',',' ',$courses->fullname);
                        if($courses->depth==1){
                            $courseName = "<span class='childcourse'>".$courseName."</span>";
                        }
			$summary = $courses->summary;
			$category = $courses->category;
			$timeCreated = $courses->timecreated;
                        $endDate = $courses->enddate;
                        
                        
                        
			$timeCreatedFormat = $timeCreated?getDateFormat($timeCreated, $format):getMDash();
			$timeCreatedFormatCSV = $timeCreated?getDateFormat($timeCreated, $formatCsv):getMDashForCSV();
                        
                        $timeEndDateFormat = $endDate?getDateFormat($endDate, $format):getMDash();
			$timeEndDateFormatCSV = $endDate?getDateFormat($endDate, $formatCsv):getMDashForCSV();
                        
			$courseImg = getCourseImage($course);
			$is_active = $courses->is_active;
			$publish = $courses->publish;
			$complianceDiv = getCourseComplianceIcon($courseId);

			if($publish == 1){
					
				if($is_active == 1){
					$cStatus = get_string('statusactive', 'multicoursereport');
                                        
                                        if($endDate < $currentTime){
                                            $cStatus = get_string('expired');
                                        }
				}elseif($is_active == 0){
					$cStatus = get_string('statusdeactive', 'multicoursereport');
				}
                                
                                

			}elseif($publish == 0){
				$cStatus = get_string('statusunpublished', 'multicoursereport');
			}

			$createdBy = $courses->createdbyname;
			$courseHTML .=  '<tr>';
			if(empty($export)){
				$courseHTML .=  '<td><a href="'.$CFG->wwwroot.'/reports/course_progress_report.php?cid='.$courseId.'&back=1" >'.$courseName.$complianceDiv.'</a></td>';
			}else{
				$courseHTML .=  '<td>'.$courseName.$complianceDiv.'</td>';
			}
			$courseHTML .=  '<td>'.$category.'</td>';
			$courseHTML .=  '<td>'.$timeCreatedFormat.'</td>';
                        $courseHTML .=  '<td>'.$timeEndDateFormat.'</td>';
			$courseHTML .=  '<td>'.$cStatus.'</td>';
			$courseHTML .=  '<td>'.$createdBy.'</td>';
			$courseHTML .=  '</tr>';
			$reportContentCSV .= $courseNameCSV.",".$category.",".$timeCreatedFormatCSV.",".$timeEndDateFormatCSV.",".$cStatus.",".$createdBy."\n";
		
			$i++;
		}

		$courseHTML .=  '<script language="javascript" type="text/javascript">';
		$courseHTML .=  '	$(document).ready(function(){

												$("#printbun").bind("click", function(e) {
													var url = $(this).attr("rel");
													window.open(url, "'.get_string('coursereport','multicoursereport').'", "'.$CFG->printWindowParameter.'");
												});

						             }); ';

		$courseHTML .=  ' window.onload = function () {
		
										var chart = new CanvasJS.Chart("chartContainer",
										{
											title:{
												text: ""
											},
											theme: "theme2",
											data: [
											{
												type: "doughnut",
												indexLabelFontFamily: "Arial",
												indexLabelFontSize: 12,
												startAngle:0,
												indexLabelFontColor: "dimgrey",
												indexLabelLineColor: "darkgrey",
												toolTipContent: "{y}%",


												dataPoints: [
												{  y: '.$unpublishedPercentage.', label: "'.get_string('statusunpublished','multicoursereport').' ('.$unpublished.')", exploded : "'.$unpublishedExploded.'"  },											
												{  y: '.$publishAndDeactivePercentage.', label: "'.get_string('statusdeactive','multicoursereport').' ('.$publishAndDeactive.')", exploded : "'.$publishAndDeactiveExploded.'"  },
												{  y: '.$publishAndActivePercentage.', label: "'.get_string('statusactive','multicoursereport').' ('.$publishAndActive.')", exploded : "'.$publishAndActiveExploded.'"  },											
												]

											}
											]
										}); ';
		 
		if(count($courseArr) > 0){
			$courseHTML .=  '	chart.render(); ';
		}
		 
		$courseHTML .=  '  }


									</script>
			
									<script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js"></script>';



	}else{
		$courseHTML .=  '<tr><td colspan="6" style="border-bottom:1px solid #b9b9b9!important;" class="no_record" >'.get_string('norecordfound','singlecoursereport').'</td></tr>';
		$reportContentCSV .= get_string('norecordfound','multicoursereport')."\n";
		$reportContentPDF .= get_string('norecordfound','multicoursereport')."\n";
	}

	$reportContentPDF .= '</table>';

	$courseHTML .= '</tbody></table></div></div></div></div></div>';


	if(empty($export)){
		$courseHTML .= paging_bar($courseCount, $page, $perpage, $genURL);
	}
	$courseHTML .= '';

	$courseReport->courseHTML = $courseHTML;
	$courseReport->reportContentCSV = $reportContentCSV;
	$courseReport->reportContentPDF = $reportContentPDF;

	return $courseReport;

} 



// request report scheduler lokesh

function getGlobalRequestReport_scheduler($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export=''){
	global $DB, $CFG, $USER;
	$globalRequestReport = new stdClass();
	$isReport = true;

	$offset = $page - 1;
	$offset = $offset*$perpage;
	$limit = '';
	if($perpage != 0){
		$limit = "LIMIT $offset, $perpage";
	}

	////// Getting common URL for the Search //////
	$pageURL = $_SERVER['PHP_SELF'];
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);

	$printUrl = '/reports/'.$CFG->pageGlobalRequestReportPrint;
	$genPrintURL = genParameterizedURL($paramArray, $removeKeyArray, $printUrl);

	$sDepartment    = $paramArray['department'];
	$sTeam          = $paramArray['team'];
	$sCourse        = $paramArray['course'];
	$sProgram       = $paramArray['program'];
	$sType          = $paramArray['type'];
	$sStartDate     = $paramArray['startDate'];
	$sEndDate       = $paramArray['endDate'];
	$sUserGroup      = $paramArray['user_group'];
	$sUser      = $paramArray['user'];
	$sClasses      = $paramArray['classes'];

	$sJobTitle          = $paramArray['job_title'];
	$sCompany          = $paramArray['company'];
	$sJobTitleArr = explode("@",$sJobTitle);
	$sCompanyArr = explode("@",$sCompany);
	$sJobTitleName = $sJobTitle=='-1'?(get_string('all','multiuserreport')):getJobTitles($sJobTitleArr);
	$sCompanyName = $sCompany=='-1'?(get_string('all','multiuserreport')):getCompany($sCompanyArr);



	$sDateSelected = '';
	if($sStartDate){
		$sDateArr = getFormattedTimeStampOfDate($sStartDate);
		$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';
	}

	$eDateSelected = '';
	if($sEndDate){
		$eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
		$eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
	}

	$sDepartmentArr = explode("@",$sDepartment);
	$sTeamArr = explode("@",$sTeam);
	$sCourseArr = explode("@",$sCourse);
	$sProgramArr = explode("@",$sProgram);
	$sTypeArr = explode("@",$sType);
	$sUserGroupArr = explode("@",$sUserGroup);
	$sUserArr = explode("@",$sUser);
	$sClassesArr = explode("@",$sClasses);

	if($paramArray['sel_mode'] == 2){
		$sTeamArr = $sUserGroupArr;
	}
	
	$sDepartmentName = $sDepartment=='-1'?(get_string('all','multicoursereport')):getDepartments($sDepartmentArr, $isReport);
	if($USER->archetype != $CFG->userTypeAdmin ){
		$sDepartmentName = getDepartments($USER->department, $isReport);
	}
	$sTeamName = $sTeam=='-1'?(get_string('all','multicoursereport')):getTeams($sTeamArr);
	$sCourseName = $sCourse=='-1'?(get_string('all','multicoursereport')):getCourses($sCourseArr);
	$sProgramName = $sProgram=='-1'?(get_string('all','multicoursereport')):getPrograms($sProgramArr);
	$sUserGroupName = $sUserGroup=='-1'?(get_string('all','multiuserreport')):getUserGroup($sUserGroupArr);
	$sClassName = $sClasses=='-1'?(get_string('all','multicoursereport')):getClasses($sClassesArr);
	$sUserName = $sUser=='-1'?(get_string('all','multicoursereport')):getUsers($sUserArr);


	// search condition start here

	if($sType == '-1'){
		$sTypeName = get_string('all','multicoursereport');
	}else{
		$sTypeArrNames = str_replace(array(1, 2, 3), array(get_string('openrequestreport'), get_string('accept_request_closed'), get_string('decline_request_closed')), $sTypeArr);
		 
		$sTypeArrIds = array();
		if(count($sTypeArr) > 0 ){
			foreach($sTypeArr as $sT){
				if($sT == 1){
					$sTypeArrIds[] = 0;
					$sTypeArrIds[] = 3;
				}elseif($sT == 2){
					$sTypeArrIds[] = 1;
				}elseif($sT == 3){
					$sTypeArrIds[] = 2;
				}
			}
		}
		//pr($sTypeArrIds);die;
		$sTypeIds = '0,1,2,3'; // 0 & 3 =>open , 1=>accepted , 2=>declined
		$sTypeName = implode(", ", $sTypeArrNames);
		$sTypeIds = count($sTypeArrIds) > 0 ? implode(",", $sTypeArrIds):$sTypeIds;
		//$searchString .= " AND request_status IN ($sTypeIds) ";
	}


	$senderDepartmentId = $request->sender_department_id?$request->sender_department_id:0;
	$receiverTeamTitle = $request->receiver_teamtitle?$request->receiver_teamtitle:getMDash();
	$receiverTeamTitleCSV = $request->receiver_teamtitle?$request->receiver_teamtitle:getMDashForCSV();
	$receiverTeamId = $request->receiver_team_id?$request->receiver_team_id:0;
	$receiverDepartmentTitle = $request->receiver_departmenttitle?$request->receiver_departmenttitle:getMDash();
	$receiverDepartmentTitleCSV = $request->receiver_departmenttitle?$request->receiver_departmenttitle:getMDashForCSV();
	$receiverDepartmentId = $request->receiver_department_id?$request->receiver_department_id:0;



	if(count($sDepartmentArr) > 0 &&  !in_array($sDepartmentArr[0] , array(0, '-1'))){
		$sDepartmentStr = implode(",", $sDepartmentArr);
		$searchString .= " AND sender_department_id IN ($sDepartmentStr) ";
	}

	if(count($sTeamArr) > 0 &&  !in_array($sTeamArr[0] , array(0, '-1'))){
		$TeamSearchStringArr = array();
		foreach($sTeamArr as $sTeamVal){
			$TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,sender_team_id) ";
		}
		if(count($TeamSearchStringArr) > 0 ){
			$TeamSearchString = implode(" OR ", $TeamSearchStringArr);
			$searchString .= " AND ($TeamSearchString) ";
		}
	}
	else if($_REQUEST['sel_mode']==2){
		
		$sql = "select GROUP_CONCAT( distinct IF(gd.id IS NULL,mgm.groupid,'') SEPARATOR ',') as global_groupid  from mdl_groups_members as mgm LEFT JOIN mdl_group_department as gd ON(mgm.groupid=gd.team_id) ";
		$globalTeamRec = $DB->get_record_sql($sql);
		$sTeamArr =  array_filter(explode(',',$globalTeamRec->global_groupid));
		if(count($sTeamArr) > 0){
			$TeamSearchStringArr = array();
			foreach($sTeamArr as $sTeamVal){
				$TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,sender_team_id) ";
			}
			if(count($TeamSearchStringArr) > 0 ){
				$TeamSearchString = implode(" OR ", $TeamSearchStringArr);
				$searchString .= " AND ($TeamSearchString) ";
			}
		}
	}


	if(count($sUserGroupArr) > 0 &&  !in_array($sUserGroupArr[0] , array(0, '-1'))){
		$TeamSearchStringArr = array();
		foreach($sUserGroupArr as $sTeamVal){
			$TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,sender_team_id) ";
		}
		if(count($TeamSearchStringArr) > 0 ){
			$TeamSearchString = implode(" OR ", $TeamSearchStringArr);
			$searchString .= " AND ($TeamSearchString) ";
		}
	}


	if(count($sProgramArr) > 0 &&  !in_array($sProgramArr[0] , array(0, '-1'))){
		$sProgramStr = implode(",", $sProgramArr);
		$searchString .= " AND program_id IN ($sProgramStr) ";
	}


	if(count($sUserArr) > 0 &&  !in_array($sUserArr[0] , array(0, '-1'))){
		$sUserStr = implode(",", $sUserArr);
		$searchString .= " AND sender_id IN ($sUserStr) ";
	}

	if(count($sCourseArr) > 0 &&  !in_array($sCourseArr[0] , array(0, '-1'))){
		$sCourseStr = implode(",", $sCourseArr);
		$searchString .= " AND course_id IN ($sCourseStr) ";
	}else{
		/*if($_REQUEST["sel_mode"] == 2){
		 $courseIdArr = getOpenTeamsCourse($sTeamArr);
		}else{
		$courseIdArr = getCourseListByDnT($sDepartmentArr, $sTeamArr, $sProgramArr, $sCourseArr, 2, $isReport);
		}
		$sCourseArr = count($courseIdArr) > 0 ? implode(",", $courseIdArr) : 0;
		$searchString .= " AND mc.id IN ($sCourseArr) ";
		$searchStringView .= " AND mc_id IN ($sCourseArr) ";*/
	}

	if(count($sClassesArr) > 0 &&  !in_array($sClassesArr[0] , array(0, '-1'))){
		$sClassesStr = implode(",", $sClassesArr);
		$searchString .= " AND classid IN ($sClassesStr) ";
	}

	if($CFG->showJobTitle == 1 && count($sJobTitleArr) > 0 && !in_array($sJobTitleArr[0] , array(0, '-1'))){
		$sJobTitleStr = count($sJobTitleArr) > 0 ? implode(",", $sJobTitleArr) : 0;
		$searchString .= " AND jobid IN ($sJobTitleStr) ";
	}

	if($CFG->showCompany == 1 &&  count($sCompanyArr) > 0 && !in_array($sCompanyArr[0] , array(0, '-1'))){
		$sCompanyStr = count($sCompanyArr) > 0 ? implode(",", $sCompanyArr) : 0;
		$searchString .= " AND comid IN ($sCompanyStr) ";
	}


	if($sDateSelected){
		/*list($month, $day, $year) = explode('/', $sDateSelected);
		 $sStartDateTime =  mktime(0, 0, 0, $month, $day, $year);*/
		$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
		$searchString .= " AND requested_on >= ($sStartDateTime) ";
	}

	if($eDateSelected){
		/*list($month, $day, $year) = explode('/', $eDateSelected);
		 $sEndDateTime = mktime(0, 0, 0, $month, $day, $year);*/
		$sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
		$searchString .= " AND requested_on <= ($sEndDateTime) ";
	}

	// search condition end here lok

	$requestArr = getGlobalRequestData_cron(1, $searchString,$page,$perpage,0,$sTypeArrIds);

	$requestCount = getGlobalRequestData_cron(0, $searchString,$page,$perpage,1,$sTypeArrIds);


	$globalRequestRec = getGlobalRequestData_cron(0, $searchString,$page,$perpage,0);
	//pr($globalRequestRec );die;



	$openReq = 0;
	$acceptReq = 0;
	$declineReq = 0;
	$totalReq = 0;

	$requestArrN = array();
	if(count($globalRequestRec) > 0){

		foreach($globalRequestRec as $k=>$reqArr){
			$reqStatus = $reqArr->request_status;
			if($reqStatus == 0 || $reqStatus == 3){
				$openReq = $openReq+$reqArr->cnt;
				 
			}elseif($reqStatus == 1){
				$acceptReq = $reqArr->cnt;

			}elseif($reqStatus == 2){
				$declineReq = $reqArr->cnt;

			}


			/*   if(count($sTypeArrIds) > 0 && in_array($reqStatus, $sTypeArrIds)){
			 $requestArrN[$k] = $reqArr;
			}  */
		}
		$totalReq = $openReq+$acceptReq+$declineReq;
		 
	}

	//pr($requestArrN);die;

	$openReqPercentage = 0;
	$acceptReqPercentage = 0;
	$declineReqPercentage = 0;
	$openReqExploded = false;
	$acceptReqExploded = false;
	$declineReqExploded = false;

	$openReqPercentage = numberFormat(($openReq*100)/$totalReq);
	$acceptReqPercentage = numberFormat(($acceptReq*100)/$totalReq);
	$declineReqPercentage = numberFormat(($declineReq*100)/$totalReq);

	if(in_array(1, $sTypeArr)){
		$openReqExploded = true;
	}

	if(in_array(2, $sTypeArr)){
		$acceptReqExploded = true;
	}

	if(in_array(3, $sTypeArr)){
		$declineReqExploded = true;
	}




	//pr($requestArr );die;
	$requestHTML = '';
	$style = !empty($export)?"style=''":'';
	$requestHTML .= "<div class='tabsOuter borderBlockSpace' ".$style.">";
	$requestHTML .= '<div class="clear"></div>';
	$exportHTML = '';
	if(empty($export)){
		 
		ob_start();
		require_once($CFG->dirroot . '/local/includes/global_request_report_search_filter.php');
		$SEARCHHTML = ob_get_contents();
		ob_end_clean();

		$requestHTML .= $SEARCHHTML;

		$exportHTML .= '<div class="exports_opt_box"> ';
		$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','multicoursereport').'" href="'.$genPrintURL.'&action=exportcsv">'.get_string('downloadcsv','multicoursereport').'</a> ';
		$exportHTML .= '<span class="seperater">&nbsp;</span>';
		$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','multicoursereport').'" id="exportpdf1" href="'.$genPrintURL.'&action=exportpdf">'.get_string('downloadpdf','multicoursereport').'</a> ';
		$exportHTML .= '<span class="seperater">&nbsp;</span>';
		$exportHTML .= '<a rel="'.$genPrintURL.'&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','multicoursereport').'" class="print_icon">'.get_string('print','multicoursereport').'</a>';
		$exportHTML .= '</div>';
		 
	}

	$requestHTML .= '<div class="userprofile view_assests">';


	if(count($requestArrN) > 0 ){
		$requestCount  = count($requestArrN);
		$requestArr = $requestArrN;
	}

	if($requestCount > 0 ){

		$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
		$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
		$sDateSelectedForPrintCSV = getDateFormatForPrint($sDateSelected, 'csv');
		$eDateSelectedForPrintCSV = getDateFormatForPrint($eDateSelected, 'csv');


		if(!empty($export) && $export == 'print'){

			$reportName = get_string('requestsreport');
			$requestHTML .= getReportPrintHeader($reportName);

			$requestHTML .= '<table border="0" cellpadding="0" cellspacing="0" class="generaltable margin_bottom">';
			$requestHTML .= '<tr>';
			if($paramArray['sel_mode'] == 2){
				$requestHTML .= '<td colspan = "2" ><strong>'.get_string('global_team').':</strong> '.$sUserGroupName.'</td>';
			}else{
				$requestHTML .= '<td><strong>'.get_string('department','multicoursereport').':</strong> '.$sDepartmentName.'</td>';
				$requestHTML .= '<td><strong>'.get_string('team','multicoursereport').':</strong>  '.$sTeamName.'</td>';
			}
			$requestHTML .= '</tr>';
			$requestHTML .= '<tr>';
			$requestHTML .= '<td width="500"><strong>'.get_string('program','multicoursereport').':</strong> '.$sProgramName.'</td>';
			$requestHTML .= '<td width="500"><strong>'.get_string('course','multicoursereport').':</strong> '.$sCourseName.'</td>';
			$requestHTML .= '</tr>';
			$requestHTML .= '<tr>';
			$requestHTML .= '<td><strong>'.get_string('class','multicoursereport').':</strong> '.$sClassName.'</td>';
			$requestHTML .= '<td><strong>'.get_string('user','multicoursereport').':</strong> '.$sUserName.'</td>';
			$requestHTML .= '</tr>';

			if($CFG->showJobTitle == 1 && $CFG->showCompany == 1){
				$requestHTML .= '<tr>';
				$requestHTML .= '<td ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
				$requestHTML .= '<td><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
				$requestHTML .= '</tr>';
			}elseif($CFG->showJobTitle == 1 && $CFG->showCompany == 0){
				$requestHTML .= '<tr>';
				$requestHTML .= '<td colspan="2" ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
				$requestHTML .= '</tr>';
			}elseif($CFG->showJobTitle == 0 && $CFG->showCompany == 1){
				$requestHTML .= '<tr>';
				$requestHTML .= '<td colspan="2" ><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
				$requestHTML .= '</tr>';
			}

			$requestHTML .= '<tr>';
			$requestHTML .= '<td colspan="2"><strong>'.get_string('statusreport').':</strong> '.$sTypeName.'</td>';
			$requestHTML .= '</tr>';
			$requestHTML .= '<tr>';
			$requestHTML .= '<td><strong>'.get_string('daterangefrom','multicoursereport').':</strong> '.$sDateSelectedForPrint.'</td>';
			$requestHTML .= '<td><strong>'.get_string('daterangeto','multicoursereport').':</strong>  '.$eDateSelectedForPrint.'</td>';
			$requestHTML .= '</tr>';
			

			$requestHTML .= '<tr>';
			$requestHTML .= '<td><strong>'.get_string('numberofrequest').':</strong> '.$totalReq.'</td>';
			$requestHTML .= '<td><strong>'.get_string('openrequestreport').':</strong>  '.$openReq.' ('.floatval($openReqPercentage).'%)</td>';
			$requestHTML .= '</tr>';
			
			$requestHTML .= '<tr>';
			$requestHTML .= '<td><strong>'.get_string('accept_request_closed').':</strong>  '.$acceptReq.' ('.floatval($acceptReqPercentage).'%)</td>';
			$requestHTML .= '<td><strong>'.get_string('decline_request_closed').':</strong> '.$declineReq.' ('.floatval($declineReqPercentage).'%)</td>';
			$requestHTML .= '</tr>';
			
			$requestHTML .= '</table>';
			//$requestHTML .= '<span '.$CFG->pdfSpanAttribute.'>&nbsp;<br /><strong>'.get_string('courseusagesreport','multicoursereport').'</strong></span><br /><br />';
		}

		$openReqPercentage = numberFormat(($openReq*100)/$totalReq);
		$acceptReqPercentage = numberFormat(($acceptReq*100)/$totalReq);
		$declineReqPercentage = numberFormat(($declineReq*100)/$totalReq);
		 
		$requestHTML .= '<div class = "single-report-start" id="watch">
								<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('requestsreport').$exportHTML.'</span>
								  <div class="single-report-graph-left" id="chartContainer">'.get_string('loading','multicoursereport').'</div>
								  <div class="single-report-graph-right">
									<div class="course-count-heading">'.get_string('numberofrequest').'</div>
									<div class="course-count">'.$totalReq.'</div>
									<div class="seperator">&nbsp;</div>
									<div class="course-status">
									  <div class="inprogress"><h6>'.get_string('openrequestreport').'</h6><span>'.$openReq.'</span></div>
									  		 <div class="clear"></div>
									  <div class="notstarted"><h6>'.get_string('decline_request_closed').'</h6><span>'.$declineReq.'</span></div>
									  <div class="clear"></div>
									  <div class="completed"><h6>'.get_string('accept_request_closed').'</h6><span>'.$acceptReq.'</span></div>
									 
									</div>
								  </div>
								</div>
							  </div>
							  <div class="clear"></div>';

	}



	$requestHTML .= '<div class=""><div class=""><table cellspacing="0" cellpadding="0" border="0" class="generaltable"><tbody>';

	$requestHTML .= '<tr>';
	$requestHTML .=  '<th width="15%">'.get_string('requesttypereport').'</th>';
	//$requestHTML .=  '<th width=15%">'.get_string('requesttextreport').'</th>';
	$requestHTML .=  '<th width="20%">'.get_string('requestforreport').'</th>';
	$requestHTML .=  '<th width="11%">'.get_string('requestbyreport').'</th>';
	$requestHTML .=  '<th width="11%">'.get_string('requestprocesseddbyreport').'</th>';
	$requestHTML .=  '<th width="12%">'.get_string('requesteddatereport').'</th>';
	$requestHTML .=  '<th width="12%">'.get_string('responsedatereport').'</th>';
	$requestHTML .=  '<th width="10%">'.get_string('requeststatusreport').'</th>';
	$requestHTML .=  '<th width="9%">'.get_string('decline_remarks').'</th>';


	$requestHTML .=  '</tr>';

	$reportContentCSV = '';
	$reportContentCSV .= get_string('daterangefrom','multicoursereport').",".$sDateSelectedForPrintCSV."\n".get_string('daterangeto','multicoursereport').",".$eDateSelectedForPrintCSV."\n";
	if($paramArray['sel_mode'] == 2){
		$reportContentCSV .= get_string('global_team').",".$sUserGroupName."\n";
	}else{
		$reportContentCSV .= get_string('department','multicoursereport').",".$sDepartmentName."\n".get_string('team','multicoursereport').",".$sTeamName ."\n";
	}
	$reportContentCSV .= get_string('program','multicoursereport').",".$sProgramName."\n".get_string('course','multicoursereport').",".$sCourseName ."\n";
	$reportContentCSV .= get_string('class','multicoursereport').",".$sClassName."\n".get_string('user','multicoursereport').",".$sUserName ."\n";
	if($CFG->showJobTitle == 1){
		$reportContentCSV .= get_string('job_title','user').",".$sJobTitleName."\n";
	}
	if($CFG->showCompany == 1){
		$reportContentCSV .= get_string('company','user').",".$sCompanyName."\n";
	}
	$reportContentCSV .= get_string('statusreport').",".$sTypeName ."\n";
	$reportContentCSV .= get_string('numberofrequest').",".$totalReq."\n".get_string('openrequestreport').",".$openReq." (".floatval($openReqPercentage)."%)" ."\n";
	$reportContentCSV .= get_string('accept_request_closed').",".$acceptReq." (".floatval($acceptReqPercentage)."%)"."\n".get_string('decline_request_closed').",".$declineReq." (".floatval($declineReqPercentage)."%)" ."\n";
	$reportContentCSV .= get_string('requestsreport')."\n";
	//$reportContentCSV .= get_string('requesttypereport').",".get_string('requesttextreport').",".get_string('requestforreport').",".get_string('requestbyreport').",".get_string('requestprocesseddbyreport').",".get_string('requesteddatereport').",".get_string('responsedatereport').",".get_string('requeststatusreport').",".get_string('decline_remarks')."\n";

	$reportContentCSV .= get_string('requesttypereport').",".get_string('requestforreport').",".get_string('requestbyreport').",".get_string('requestprocesseddbyreport').",".get_string('requesteddatereport').",".get_string('responsedatereport').",".get_string('requeststatusreport').",".get_string('decline_remarks')."\n";


	$reportContentPDF = '';
	$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" class="generaltable">';
	$reportContentPDF .= '<tr>';
	if($paramArray['sel_mode'] == 2){
		$reportContentPDF .= '<td colspan = "2" ><strong>'.get_string('global_team').':</strong> '.$sUserGroupName.'</td>';
	}else{
		$reportContentPDF .= '<td><strong>'.get_string('department','multicoursereport').':</strong> '.$sDepartmentName.'</td>';
		$reportContentPDF .= '<td><strong>'.get_string('team','multicoursereport').':</strong>  '.$sTeamName.'</td>';
	}
	$reportContentPDF .= '</tr>';
	$reportContentPDF .= '<tr>';
	$reportContentPDF .= '<td><strong>'.get_string('program','multicoursereport').':</strong> '.$sProgramName.'</td>';
	$reportContentPDF .= '<td><strong>'.get_string('course','multicoursereport').':</strong> '.$sCourseName.'</td>';
	$reportContentPDF .= '</tr>';
	$reportContentPDF .= '<tr>';
	$reportContentPDF .= '<td><strong>'.get_string('class','multicoursereport').':</strong> '.$sClassName.'</td>';
	$reportContentPDF .= '<td><strong>'.get_string('user','multicoursereport').':</strong> '.$sUserName.'</td>';
	$reportContentPDF .= '</tr>';

	if($CFG->showJobTitle == 1 && $CFG->showCompany == 1){
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
		$reportContentPDF .= '<td><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
		$reportContentPDF .= '</tr>';
	}elseif($CFG->showJobTitle == 1 && $CFG->showCompany == 0){
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td colspan="2" ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
		$reportContentPDF .= '</tr>';
	}elseif($CFG->showJobTitle == 0 && $CFG->showCompany == 1){
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td colspan="2" ><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
		$reportContentPDF .= '</tr>';
	}

	$reportContentPDF .= '<tr>';
	$reportContentPDF .= '<td colspan="2"><strong>'.get_string('statusreport').':</strong> '.$sTypeName.'</td>';
	$reportContentPDF .= '</tr>';
	$reportContentPDF .= '<tr>';
	$reportContentPDF .= '<td><strong>'.get_string('daterangefrom','multicoursereport').':</strong> '.$sDateSelectedForPrint.'</td>';
	$reportContentPDF .= '<td><strong>'.get_string('daterangeto','multicoursereport').':</strong>  '.$eDateSelectedForPrint.'</td>';
	$reportContentPDF .= '</tr>';

	$reportContentPDF .= '<tr>';
	$reportContentPDF .= '<td><strong>'.get_string('numberofrequest').':</strong> '.$totalReq.'</td>';
	$reportContentPDF .= '<td><strong>'.get_string('openrequestreport').':</strong>  '.$openReq.' ('.floatval($openReqPercentage).'%)</td>';
	$reportContentPDF .= '</tr>';

	$reportContentPDF .= '<tr>';
	$reportContentPDF .= '<td><strong>'.get_string('accept_request_closed').':</strong>  '.$acceptReq.' ('.floatval($acceptReqPercentage).'%)</td>';
	$reportContentPDF .= '<td><strong>'.get_string('decline_request_closed').':</strong> '.$declineReq.' ('.floatval($declineReqPercentage).'%)</td>';
	$reportContentPDF .= '</tr>';

	//$reportContentPDF .= '<tr><br /><td width="100%"  ><span '.$CFG->pdfSpanAttribute.'><strong>'.get_string('requestsreport').'</strong></span><br /></td></tr>';

	$reportContentPDF .= '</table>';

	$reportContentPDF .= getGraphImageHTML(get_string('requestsreport'));

	if($requestCount > 0 ){
		//pr($requestArr );die;

		$requestGlobalArr = $requestArr;
		 
		 
		foreach($requestGlobalArr as $request){

			$format = $CFG->DateFormatForRequests;
			$formatCsv = $CFG->customDefaultDateFormatForCSV;

			$formatDateTime = $CFG->customDefaultDateTimeFormat;
			$formatDateTimeCsv = $CFG->customDefaultDateTimeFormatForCSV;

			$requestType = $request->request_type;
			$requestStatus = $request->request_status;
			$programId = $request->program_id;
			$courseId = $request->course_id;
			$numberOfSeats = $request->no_of_seats;
			$enrolmentType = $request->enrolmenttype;
			$requestText = $request->request_text;
			$createdBy = $request->createdby;
			$actionTakenBy = $request->action_taken_by;
			$senderName = $request->sender_name;
			$senderUsername = $request->sender_username;
			$receiverName = $request->receiver_name;
			$receiverUsername = $request->receiver_username;
			$courseName = $request->course_name;
			$classId = $request->classid;
			$className = $request->class_name;
			$prograName = $request->program_name;

			$sessionList = array();
			$sessionListArr = array();
			if($classId && !isset($sessionListArr[$classId])){
				$sessionList = classSessionList ( $classId , 'classid');
				$sessionListArr[$classId] = $sessionList;
			}
			$sessionName = '';
			$sessionNameCSV = '';
			if(isset($sessionListArr[$classId]) && count($sessionListArr[$classId]) > 0 ){
				 
				$sessionNameCSV .= ",".get_string('classsessionscsv').",,,,,,\n";
				if(empty($export) || (!empty($export) && $export !== 'exportcsv')){
					$sessionName .= "<br>".get_string('classsessions');
				}
				foreach($sessionListArr[$classId] as $session){

					if(!empty($export) && $export === 'exportcsv'){
						$sessionDuration = getClassroomSessionDateDuration($session->starttime, $session->duration, 'minutes', 'csv');
						$sessionNameCSV .= ",".$session->sessionname.": ". $sessionDuration.",,,,,,\n";
					}else{
						$sessionDuration = getClassroomSessionDateDuration($session->starttime, $session->duration);
						$sessionName .= "<br><strong>".$session->sessionname."</strong>: ". $sessionDuration;
					}

				}

			}

			$requestFor = $courseName?($className?$courseName.": ".$className.($sessionName):$courseName):($prograName?$prograName:getMDash());
			$requestedOn = $request->requested_on;
			$reponseOn = $request->reponse_on;
			$startDate = $request->startdate;
			$endDate = $request->enddate;

			$senderTeamTitle = $request->sender_teamtitle?$request->sender_teamtitle:getMDash();
			$senderTeamTitleCSV = $request->sender_teamtitle?$request->sender_teamtitle:getMDashForCSV();
			$senderTteamId = $request->sender_team_id?$request->sender_team_id:0;
			$senderDepartmentTitle = $request->sender_departmenttitle?$request->sender_departmenttitle:getMDash();
			$senderDepartmentTitleCSV = $request->sender_departmenttitle?$request->sender_departmenttitle:getMDashForCSV();
			$senderDepartmentId = $request->sender_department_id?$request->sender_department_id:0;
			$receiverTeamTitle = $request->receiver_teamtitle?$request->receiver_teamtitle:getMDash();
			$receiverTeamTitleCSV = $request->receiver_teamtitle?$request->receiver_teamtitle:getMDashForCSV();
			$receiverTeamId = $request->receiver_team_id?$request->receiver_team_id:0;
			$receiverDepartmentTitle = $request->receiver_departmenttitle?$request->receiver_departmenttitle:getMDash();
			$receiverDepartmentTitleCSV = $request->receiver_departmenttitle?$request->receiver_departmenttitle:getMDashForCSV();
			$receiverDepartmentId = $request->receiver_department_id?$request->receiver_department_id:0;

			$requestDate = $requestedOn?getDateFormat($requestedOn, $format):getMDash();
			$responseDate = ($requestStatus == 0 || $requestStatus == 3)?getMDash():($reponseOn?getDateFormat($reponseOn, $format):getMDash());

			$requestDateCSV = $requestedOn?getDateFormat($requestedOn, $formatCsv):getMDashForCSV();
			$responseDateCSV = ($requestStatus == 0 || $requestStatus == 3)?getMDashForCSV():($reponseOn?getDateFormat($reponseOn, $formatCsv):getMDashForCSV());

			$requestDateTime = $requestedOn?getDateFormat($requestedOn, $formatDateTime):getMDash();
			$responseDateTime = ($requestStatus == 0 || $requestStatus == 3)?getMDash():($reponseOn?getDateFormat($reponseOn, $formatDateTime):getMDash());

			$requestDateTimeCSV = $requestedOn?getDateFormat($requestedOn, $formatDateTimeCsv):getMDashForCSV();
			$responseDateTimeCSV = ($requestStatus == 0 || $requestStatus == 3)?getMDashForCSV():($reponseOn?getDateFormat($reponseOn, $formatDateTimeCsv):getMDashForCSV());

			$startDateTime = $startDate?getDateFormat($startDate, $formatDateTime):getMDash();
			$endDateTime = $endDate?getDateFormat($endDate, $formatDateTime):getMDash();

			$startDateTimeCSV = $startDate?getDateFormat($startDate, $formatDateTimeCsv):getMDashForCSV();
			$endDateTimeCSV = $endDate?getDateFormat($endDate, $formatDateTimeCsv):getMDashForCSV();


			if($requestStatus == 0 || $requestStatus == 3){
				$requestText = '';
				$requestTextCSV = '';
				$tdDate = $requestedOn;
			}else{
				$requestText = $requestDate.' - ';
				$requestTextCSV = $requestDateCSV.' - ';
				$tdDate = $reponseOn;
			}
			if($request->request_type == 'classroom'){
				$timeString = $startDateTime." - ".$endDateTime;
				$timeStringCSV = $startDateTime." - ".$endDateTime;
				$timeString = '';
				$timeStringCSV = '';
				$requestText .= str_replace('##__TIME__##',$timeString,$request->request_text);
				$requestTextCSV .= str_replace('##__TIME__##',$timeStringCSV,$request->request_text);
				if(($request->user_id == $actionTakenBy) && ($request->user_id != $USER->id) && $requestStatus == 2){
					$requestText .= date($CFG->DateFormatForRequests,$tdDate)." - ".get_string('request_declined_by_learner');
					$requestTextCSV .= date($formatCsv,$tdDate)." - ".get_string('request_declined_by_learner');
				}
				$reqId = $request->classid;
			}else{
				$requestText .= $request->request_text;
				$requestTextCSV .= $request->request_text;
				$reqId = $request->id;
			}
			if($requestStatus == 0 || $requestStatus == 3){
				//$actionItem = get_string('openrequestreport');
				$actionItem = $requestStatus == 0?(get_string('openrequestreport')." (".get_string('managerforapproval','classroomreport').")"):(get_string('openrequestreport')." (".get_string('totalinviteduser','classroomreport').")");
			}else{
				if($requestStatus == 1){
					$actionItem = get_string('accept_request_closed');
				}else{
					$actionItem = get_string('decline_request_closed');
				}
			}
			if($request->request_type == get_string('program_extension')){
				$reqTypeText = 'program extension';
			}elseif($request->request_type == get_string('course_extension')){
				$reqTypeText = 'online course extension';
			}else{
				$reqTypeText = $request->request_type=='course'?get_string('onlinecourse'):($request->request_type == 'classroom'?get_string('classroomcourse'):$request->request_type);
			}


			$declinedRemarks = ($requestStatus!=1 && $request->declined_remarks)?$request->declined_remarks:getMDash();
			$declinedRemarksCSV = $request->declined_remarks?$request->declined_remarks:getMDashForCSV();



			$requestHTML .=  '<tr>';
			$requestHTML .= '<tr id = "'.$request->program_id.'_'.$request->course_id.'">';
			$requestHTML .= "<td>".ucwords($reqTypeText)."</td>";
			//$requestHTML .= "<td class='email-word-wrap' >".$requestText."</td>";
			$requestHTML .= "<td>".$requestFor."</td>";
			$requestHTML .= "<td>".($senderUsername?($senderName."<br>(".$senderUsername.")"):(getMDash()))."</td>";
			$requestHTML .= "<td>".($receiverUsername?($receiverName."<br>(".$receiverUsername.")"):(getMDash()))."</td>";
			$requestHTML .= "<td>".$requestDate."</td>";
			$requestHTML .= "<td>".$responseDate."</td>";
			$requestHTML .= "<td>".$actionItem."</td>";
			$requestHTML .= "<td >".$declinedRemarks."</td>";
			$requestHTML .= '</tr>';

			//$reportContentCSV .= ucwords($reqTypeText).",".$requestTextCSV.",".$requestFor.",".($senderUsername?($senderName." (".$senderUsername.")"):(getMDashForCSV())).",".($receiverUsername?($receiverName." (".$receiverUsername.")"):(getMDashForCSV())).",".$requestDateCSV.",".$responseDateCSV.",".$actionItem.",".$declinedRemarksCSV."\n";
			$reportContentCSV .= ucwords($reqTypeText).",".$requestFor.",".($senderUsername?($senderName." (".$senderUsername.")"):(getMDashForCSV())).",".($receiverUsername?($receiverName." (".$receiverUsername.")"):(getMDashForCSV())).",".$requestDateCSV.",".$responseDateCSV.",".$actionItem.",".$declinedRemarksCSV."\n";

			if($sessionNameCSV){
				$reportContentCSV .= $sessionNameCSV;
			}


			$i++;


		}

		$requestHTML .=  '<script language="javascript" type="text/javascript">';
		$requestHTML .=  '	$(document).ready(function(){

												$("#printbun").bind("click", function(e) {
													var url = $(this).attr("rel");
													window.open(url, "'.get_string('requestsreport').'", "'.$CFG->printWindowParameter.'");
												});

						             }); ';

		$requestHTML .=  ' window.onload = function () {
		
										var chart = new CanvasJS.Chart("chartContainer",
										{
											title:{
												text: ""
											},
											theme: "theme2",
											data: [
											{
												type: "doughnut",
												indexLabelFontFamily: "Arial",
												indexLabelFontSize: 12,
												startAngle:0,
												indexLabelFontColor: "dimgrey",
												indexLabelLineColor: "darkgrey",
												toolTipContent: "{y}%",


												dataPoints: [
												{  y: '.$openReqPercentage.', label: "'.get_string('openrequestreport').' ('.$openReq.')", exploded: "'.$openReqExploded.'" },												
												{  y: '.$declineReqPercentage.', label: "'.get_string('decline_request_closed').' ('.$declineReq.')", exploded: "'.$declineReqExploded.'"  },
												{  y: '.$acceptReqPercentage.', label: "'.get_string('accept_request_closed').' ('.$acceptReq.')", exploded: "'.$acceptReqExploded.'"  },
			
												]

											}
											]
										});';
		 
		if($openReqPercentage || $acceptReqPercentage || $declineReqPercentage){
			$requestHTML .=  '	chart.render(); ';
		}
		 
		$requestHTML .=  '  }
									</script>
									<script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js"></script>';

	}else{
		$requestHTML .=  '<tr><td colspan="9" >'.get_string('norecordfound','singlecoursereport').'</td></tr>';
		$reportContentCSV .= get_string('norecordfound','multicoursereport')."\n";
	}



	$requestHTML .= '</tbody></table></div></div></div></div>';

	if(empty($export)){
		$requestHTML .= paging_bar($requestCount, $page, $perpage, $genURL);
		//$requestHTML .= '<input type = "button" value = "'.get_string('back','multicoursereport').'" onclick="location.href=\''.$CFG->wwwroot.'/my/adminreportdashboard.php\';">';
	}
	$requestHTML .= '';

	$globalRequestReport->requestHTML = $requestHTML;
	$globalRequestReport->reportContentCSV = $reportContentCSV;
	$globalRequestReport->reportContentPDF = $reportContentPDF;

	return $globalRequestReport;

}

//scheduler lokesh
 

function getUserReport_scheduler($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export=''){
	global $DB, $CFG,$USER;
	$isReport = true;	 
	$userReport = new stdClass(); 
	
	$pageURL = $_SERVER['PHP_SELF'];
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);	 
	$actionUrl = '/reports/user_report_print.php';
	$genActionURL = genParameterizedURL($paramArray, $removeKeyArray, $actionUrl);	 
	 
	$sDepartment    = $paramArray['department'];
	$sTeam          = $paramArray['team'];
	$sUser          = $paramArray['user'];
	$sStatus       =  $paramArray['status'];
	$sStartDate     = $paramArray['startDate'];
	$sEndDate       = $paramArray['endDate'];	 
	$sUserGroup		= $paramArray['user_group'];	 
	$sJobTitle          = $paramArray['job_title'];
	$sCompany          = $paramArray['company'];
        $sCity          = $paramArray['city'];
        
	$sJobTitleArr = explode("@",$sJobTitle);
	$sCompanyArr = explode("@",$sCompany);
	$sJobTitleName = $sJobTitle=='-1'?(get_string('all','multiuserreport')):getJobTitles($sJobTitleArr);
	$sCompanyName = $sCompany=='-1'?(get_string('all','multiuserreport')):getCompany($sCompanyArr);	 
	$sDepartmentArr = explode("@",$sDepartment);
	$sTeamArr = explode("@",$sTeam);
	$sUserArr = explode("@",$sUser);	 
        $sCityArr = explode("@",$sCity);	 
	
        $sIsManagerYes=$paramArray['is_manager_yes'];        
        $sIsManagerYesArr=explode("@",$sIsManagerYes);
		
       
	$sUserGroupArr = explode("@",$sUserGroup);
	 
	if($_REQUEST["sel_mode"] == 2){
		$sTeamArr = $sUserGroupArr;
	}
	 
	$sStatusArr = explode("@",$sStatus);
	 
	$sDepartmentName = $sDepartment=='-1'?(get_string('all','multiuserreport')):getDepartments($sDepartmentArr, $isReport);
	if($USER->archetype != $CFG->userTypeAdmin ){
		$sDepartmentName = getDepartments($USER->department, $isReport);
	}
	$sTeamName = $sTeam=='-1'?(get_string('all','multiuserreport')):getTeams($sTeamArr);
	$sUserFullName = $sUser=='-1'?(get_string('all','multiuserreport')):getUsers($sUserArr);
	 
	$sUserGroupName = $sUserGroup=='-1'?(get_string('all','multiuserreport')):getUserGroup($sUserGroupArr);	 
	 
	if($sStatus == '-1'){
		$sStatusName = get_string('all','multiuserreport');
	}else{

		$repStatusArr = array(0 => get_string('statusactive','multiuserreport'), 1 => get_string('statusdeactive','multiuserreport'));
		$sStatusArr2 = str_replace(array(0,1), $repStatusArr, $sStatusArr);
		$sStatusName = count($sStatusArr2) > 0 ? implode(", ", $sStatusArr2) : 0;
	}	 
	$searchString = "";
	$searchDString = "";
	$searchTString = "";	 
	 
	$sDateSelected = '';
	if($sStartDate){
		$sDateArr = getFormattedTimeStampOfDate($sStartDate);
		$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';		 
	}	 
	$eDateSelected = '';
	if($sEndDate){
		$eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
		$eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
	}
	 
	if(count($sUserArr) > 0 && !in_array($sUserArr[0] , array(0, '-1'))){
		$sUserStr = count($sUserArr) > 0 ? implode(",", $sUserArr) : 0;
		$searchString .= " AND mu.id IN ($sUserStr) ";
	}else{
		if($_REQUEST["sel_mode"] == 2){
			$usersArr = getOpenTeamsUser($sTeamArr);
		}else{
			$usersArr = getUserListByDnT($sDepartmentArr, $sTeamArr, $sUserArr, 2, $isReport, 0, $jobTitleArr, $companyArr);
		}
		$managerUsersIdSql = '';
		if(count($usersArr) > 0 ){
			$usersId = implode(",", $usersArr);
			$searchString .= " AND mu.id IN ($usersId)";
		}else{
			$searchString .= " AND mu.id IN (0)";
		}		 
	}
	$sAdManagersName = getAdManagersNonManagersList($sIsManagerYesArr);
        if(count($sIsManagerYesArr) && !in_array('-1',$sIsManagerYesArr))
        {
           $searchString .= " AND mu.is_manager_yes in(".@implode(',',$sIsManagerYesArr).") ";
        }
       
        if(count($sCityArr) && !in_array('-1',$sCityArr))
        {  
          
            $CityArr = ConvertCityIdToNameArr($sCityArr);
            $city = @implode(",",$CityArr);
            $searchString .= " AND mu.city IN(".$city.") ";
        }
        
	if($CFG->showJobTitle == 1 && count($sJobTitleArr) > 0 && !in_array($sJobTitleArr[0] , array(0, '-1'))){
		$sJobTitleStr = count($sJobTitleArr) > 0 ? implode(",", $sJobTitleArr) : 0;
		$searchString .= " AND jt.id IN ($sJobTitleStr) ";
	}
        $sleaders_name = '';
        if($paramArray['report_to'] != '' && $paramArray['report_to'] != '-1' && count($paramArray['report_to']) > 0){
			 $id_explode = explode("@",$paramArray['report_to']);
			 $List = getLeadersNameList();

			 $leaders_name = '';
			 foreach($id_explode as $id){
                                $leaders_name .= '"'.$List[$id].'",';
                                $sleaders_name .= $List[$id].' | ';
			 }
                         $sleaders_name = trim($sleaders_name,' | ');
			 $leaders_name = trim($leaders_name,',');
			 $searchString .= " AND mu.report_to IN ($leaders_name)";
        }  
	if($CFG->showCompany == 1 &&  count($sCompanyArr) > 0 && !in_array($sCompanyArr[0] , array(0, '-1'))){
		$sCompanyStr = count($sCompanyArr) > 0 ? implode(",", $sCompanyArr) : 0;
		$searchString .= " AND com.id IN ($sCompanyStr) ";
	}
	 
	 
	 
	if($sDateSelected){
		$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
		$searchString .= " AND mu.timecreated >= ($sStartDateTime) ";
	}
	 
	if($eDateSelected){
		$sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
		$searchString .= " AND mu.timecreated <= ($sEndDateTime) ";
	}
	 
	$searchStatusString = '';
	if(count($sStatusArr) > 0 &&  !in_array($sStatusArr[0] , array('-1'))){
		$sStatusStr = implode(",", $sStatusArr);
		$searchStatusString .= " AND mu.suspended IN ($sStatusStr)";
	}
        
        $paramArray['key'] = addslashes($paramArray['key']);
        if(($paramArray['key'] != '')) {
		$searchString .= " AND (mu.firstname like '%".$paramArray['key']."%' OR mu.lastname like '%".$paramArray['key']."%' OR CONCAT(mu.firstname,' ', mu.lastname) like '%".$paramArray['key']."%' OR mu.username like '%".$paramArray['key']."%' OR mu.user_identifier like '%".$paramArray['key']."%' OR mu.email like '%".$paramArray['key']."%' OR mu.city like '%".$paramArray['key']."%') ";
	}
	//echo $searchString;die;
	$allSystemUserArr = getUserListingForUserReport($page,$perpage,$sort,$dir,$searchString,$searchStatusString);
	//pr($allSystemUserArr);die;
	$allSystemUserCount = 0;
	if(count($allSystemUserArr) > 0 ){
		$allSystemUserAllArr = getUserListingForUserReport($page,$perpage,$sort,$dir,$searchString,$searchStatusString,1);
		$allSystemUserCount = count($allSystemUserAllArr);
	}
	 
	$deactivatedUser = getUserListingForUserReportCount($searchString,1);	 
	$activateUser = getUserListingForUserReportCount($searchString,0);
	$deactivatedUser = $deactivatedUser?$deactivatedUser:0;
	$activateUser = $activateUser?$activateUser:0;	 
	$totalUsers = $deactivatedUser + $activateUser;
	 
	$userHTML = '';
	$style = !empty($export)?"style=''":'';
	$userHTML .= "<div class='tabsOuter borderBlockSpace' ".$style."  >";
	$userHTML .= '<div class="clear"></div>'; 
	
	if(empty($export)){
		ob_start();
		require_once($CFG->dirroot . '/local/includes/userreportsearch_filter.php');
		$userHTML .= ob_get_contents();
		ob_end_clean();

		$exportHTML = '<div class="exports_opt_box"> ';
		$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','multiuserreport').'" href="'.$genActionURL.'&perpage=0&&action=exportcsv">'.get_string('downloadcsv','multiuserreport').'</a> ';
		$exportHTML .= '<span class="seperater">&nbsp;</span>';
		$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','multiuserreport').'" id="exportpdf1" href="'.$genActionURL.'&perpage=0&&action=exportpdf">'.get_string('downloadpdf','multiuserreport').'</a> ';
		$exportHTML .= '<span class="seperater">&nbsp;</span>';
		$exportHTML .= '<a rel="'.$genActionURL.'&perpage=0&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','multiuserreport').'" class="print_icon">'.get_string('print','multiuserreport').'</a>';
		$exportHTML .= '</div>';
	}
	 
	$userHTML .= '<div class="userprofile view_assests">';
	 
	$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
	$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
	$sDateSelectedForPrintCSV = getDateFormatForPrint($sDateSelected, 'csv');
	$eDateSelectedForPrintCSV = getDateFormatForPrint($eDateSelected, 'csv');
	 
	if($allSystemUserCount > 0){		 
		$deactivatedUserExploded = false;
		$activateUserExploded = false;
		 
		if(in_array(1, $sStatusArr)){
			$deactivatedUserExploded = true;
		}
		 
		if(in_array(0, $sStatusArr)){
			$activateUserExploded = true;
		}		 
		$deactivatedUserPercentage = numberFormat(($deactivatedUser*100)/$totalUsers);
		$activateUserPercentage = numberFormat(($activateUser*100)/$totalUsers); 
		 
		if(!empty($export) && $export == 'print'){
			 
			$reportName = get_string('userreport','multiuserreport');
			$userHTML .= getReportPrintHeader($reportName);
			 
			$userHTML .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="generaltable margin_bottom">';
			if($_REQUEST["sel_mode"] == 2){
				$userHTML .= '<tr>';
				$userHTML .= '<td  ><strong>'.get_string('global_team').':</strong> '.$sUserGroupName.'</td>';
				$userHTML .= '<td  ><strong>'.get_string('user','multiuserreport').':</strong> '.$sUserFullName.'</td>';
				$userHTML .= '</tr>';
				$userHTML .= '<tr>';
				$userHTML .= '<td colspan="2" ><strong>'.get_string('status','multiuserreport').':</strong> '.$sStatusName.'</td>';
				$userHTML .= '</tr>';
			}else{
				$userHTML .= '<tr>';
				$userHTML .= '<td width="500"><strong>'.get_string('department','multiuserreport').':</strong> '.$sDepartmentName.'</td>';
				$userHTML .= '<td width="500"><strong>'.get_string('team','multiuserreport').':</strong>  '.$sTeamName.'</td>';
				$userHTML .= '</tr>';
				 
				$userHTML .= '<tr>';
				$userHTML .= '<td><strong>'.get_string('user','multiuserreport').':</strong> '.$sUserFullName.'</td>';
				$userHTML .= '<td ><strong>'.get_string('status','multiuserreport').':</strong> '.$sStatusName.'</td>';
				$userHTML .= '</tr>';
			}
			 
			if($CFG->showJobTitle == 1 && $CFG->showCompany == 1){
				$userHTML .= '<tr>';
				$userHTML .= '<td ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
				$userHTML .= '<td><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
				$userHTML .= '</tr>';
			}elseif($CFG->showJobTitle == 1 && $CFG->showCompany == 0){
				$userHTML .= '<tr>';
				$userHTML .= '<td colspan="2" ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
				$userHTML .= '</tr>';
			}elseif($CFG->showJobTitle == 0 && $CFG->showCompany == 1){
				$userHTML .= '<tr>';
				$userHTML .= '<td colspan="2" ><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
				$userHTML .= '</tr>';
			}
                        
                        if($CFG->showReportTo == 1){
                                if(!$sleaders_name){
                                        $reportToPrint = get_string('all');
                                }else{
                                        $reportToPrint = $sleaders_name;
                                }
                                $userHTML .= '<tr>';
				$userHTML .= '<td colspan="2" ><strong>'.get_string('report_to').':</strong> '.$reportToPrint.'</td>';
				$userHTML .= '</tr>';
                                
                                
                            
                        }
                        $userHTML .= '<tr>';
                        $userHTML .= '<td colspan="2" ><strong>'.get_string('ismanageryescsv').':</strong> '.$sAdManagersName .'</td>';
                        $userHTML .= '</tr>';
			$userHTML .= '<tr>';
			$userHTML .= '<td><strong>'.get_string('startdate','multiuserreport').':</strong> '.$sDateSelectedForPrint.'</td>';
			$userHTML .= '<td><strong>'.get_string('enddate','multiuserreport').':</strong>  '.$eDateSelectedForPrint.'</td>';
			$userHTML .= '</tr>';

			$userHTML .= '<tr>';
			$userHTML .= '<td  width="100%" colspan="2"><strong>'.get_string('graphusers','multiuserreport').':</strong> '.$totalUsers.'</td>';
			$userHTML .= '</tr>';
			$userHTML .= '<tr>';
			$userHTML .= '<td  width="50%" ><strong>'.get_string('statusactive','multicoursereport').':</strong> '.$activateUser.' ('.floatval($activateUserPercentage).'%)</td>';
			$userHTML .= '<td  width="50%" ><strong>'.get_string('statusdeactive','multicoursereport').':</strong> '.$deactivatedUser.' ('.floatval($deactivatedUserPercentage).'%)</td>';
			$userHTML .= '</tr>';
			$userHTML .= '</table>';
		}		 
		$userHTML .= '<div class = "single-report-start" id="watch">
						<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('userreport','multiuserreport').$exportHTML.'</span>
						  <div class="single-report-graph-left" id="chartContainer">'.get_string('loading','multiuserreport').'</div>
						  <div class="single-report-graph-right">
							<div class="course-count-heading">'.get_string('graphusers','multiuserreport').'</div>
							<div class="course-count">'.$totalUsers.'</div>
							<div class="seperator">&nbsp;</div>
							<div class="course-status">
							  <div class="inprogress"><h6>'.get_string('statusactive','multicoursereport').'</h6><span>'.$activateUser.'</span></div>
							  <div class="clear"></div>
							  <div class="notstarted"><h6>'.get_string('statusdeactive','multicoursereport').'</h6><span>'.$deactivatedUser.'</span></div>
							</div>
						  </div>
						</div>
					  </div>
					  <div class="clear"></div>';
	}
	 
	$userHTML .= '<div class="clear"></div>';
	$userHTML .= '<div class=""><table class="generaltable" cellspacing="0" cellpadding="0" border="0" width="100%"><tbody>';
	 
	$userHTML .= '<tr>';
	$userHTML .= '<th width="10%">'.get_string('fullname','multiuserreport').'</th>';
	$userHTML .= '<th width="10%">'.get_string('username','multiuserreport').'</th>';
        $userHTML .= '<th width="10%">'.get_string('user_identifier').'</th>';
        $userHTML .= '<th width="10%">'.get_string('city').'</th>';
	$userHTML .= '<th width="22%">'.get_string('contactdetails','multiuserreport').'</th>';
	$userHTML .= '<th width="15%">'.get_string('manager','multiuserreport').'</th>';
	$userHTML .= '<th width="15%">'.get_string('department','multiuserreport').'</th>';
	$userHTML .= '<th width="8%">'.get_string('team','multiuserreport').'</th>';
	$userHTML .= '</tr>';
	 
	 
	$reportContentCSV = '';
	if($_REQUEST["sel_mode"] == 2){
		$reportContentCSV .= get_string('global_team').",".$sUserGroupName."\n";
	}else{
		$reportContentCSV .= get_string('department','multiuserreport').",".$sDepartmentName."\n".get_string('team','multiuserreport').",".$sTeamName ."\n";
		 
	}
	$reportContentCSV .= get_string('user','multiuserreport').",".$sUserFullName."\n";
	if($CFG->showJobTitle == 1){
		$reportContentCSV .= get_string('jobtitle').",".$sJobTitleName."\n";
	}
        
        if($CFG->showReportTo == 1){
		
                if(!$sleaders_name){
                        $reportContentCSV .= get_string('report_to').','.get_string('all')." \n";
                }else{
                        $reportContentCSV .= get_string('report_to').','.$sleaders_name." \n";
                }
	}
        
        $reportContentCSV .= get_string('ismanageryescsv').','.$sAdManagersName." \n";
        
	if($CFG->showCompany == 1){
		$reportContentCSV .= get_string('company','user').",".$sCompanyName."\n";
	}
	$reportContentCSV .= get_string('status','multiuserreport').",".$sStatusName."\n";
	$reportContentCSV .= get_string('daterangefrom','multicoursereport').",".$sDateSelectedForPrintCSV."\n".get_string('daterangeto','multicoursereport').",".$eDateSelectedForPrintCSV."\n";
	$reportContentCSV .= get_string('graphusers','multiuserreport').",".$totalUsers."\n";
	$reportContentCSV .= get_string('statusactive','multiuserreport').",".$activateUser." (".floatval($activateUserPercentage)."%)\n";
	$reportContentCSV .= get_string('statusdeactive','multiuserreport').",".$deactivatedUser." (".floatval($deactivatedUserPercentage)."%)\n";
	$reportContentCSV .= get_string('userreport','multiuserreport')."\n";
	$reportContentCSV .= get_string('fullname','multiuserreport').",".get_string('username','multiuserreport').",".get_string('user_identifier').",".get_string('jobtitle').",".get_string('report_to').",".get_string('ismanageryescsv').",".get_string('city').",".get_string('email').",".get_string('manager','multiuserreport').",".get_string('department','multiuserreport').",".get_string('team','multiuserreport')."\n";
	 
	 
	$reportContentPDF = '';
	 
	$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="generaltable">';
	 
	if($_REQUEST["sel_mode"] == 2){
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td  width="50%" ><strong>'.get_string('global_team').':</strong> '.$sUserGroupName.'</td>';
		$reportContentPDF .= '<td  width="50%" ><strong>'.get_string('user','multiuserreport').':</strong> '.$sUserFullName.'</td>';
		$reportContentPDF .= '</tr>';

		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td  width="100%" colspan="2"><strong>'.get_string('status','multiuserreport').':</strong> '.$sStatusName.'</td>';
		$reportContentPDF .= '</tr>';

	}else{
		 
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td width="50%" ><strong>'.get_string('department','multiuserreport').':</strong> '.$sDepartmentName.'</td>';
		$reportContentPDF .= '<td width="50%" ><strong>'.get_string('team','multiuserreport').':</strong>  '.$sTeamName.'</td>';
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td  width="50%" ><strong>'.get_string('user','multiuserreport').':</strong> '.$sUserFullName.'</td>';
		$reportContentPDF .= '<td  width="50%" ><strong>'.get_string('status','multiuserreport').':</strong> '.$sStatusName.'</td>';
		$reportContentPDF .= '</tr>';

	}
	 
	if($CFG->showJobTitle == 1 && $CFG->showCompany == 1){
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
		$reportContentPDF .= '<td><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
		$reportContentPDF .= '</tr>';
	}elseif($CFG->showJobTitle == 1 && $CFG->showCompany == 0){
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td colspan="2" ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
		$reportContentPDF .= '</tr>';
	}elseif($CFG->showJobTitle == 0 && $CFG->showCompany == 1){
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td colspan="2" ><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
		$reportContentPDF .= '</tr>';
	}
	 
	 
	$reportContentPDF .= '<tr>';
	$reportContentPDF .= '<td width="50%" ><strong>'.get_string('daterangefrom','multicoursereport').':</strong> '.$sDateSelectedForPrint.'</td>';
	$reportContentPDF .= '<td width="50%"  ><strong>'.get_string('daterangeto','multicoursereport').':</strong>  '.$eDateSelectedForPrint.'</td>';
	$reportContentPDF .= '</tr>';
	 
	$reportContentPDF .= '<tr>';
	$reportContentPDF .= '<td  width="100%" colspan="2"><strong>'.get_string('graphusers','multiuserreport').':</strong> '.$totalUsers.'</td>';
	$reportContentPDF .= '</tr>';
	$reportContentPDF .= '<tr>';
	$reportContentPDF .= '<td  width="50%" ><strong>'.get_string('statusactive','multicoursereport').':</strong> '.$activateUser.' ('.floatval($activateUserPercentage).'%)</td>';
	$reportContentPDF .= '<td  width="50%" ><strong>'.get_string('statusdeactive','multicoursereport').':</strong> '.$deactivatedUser.' ('.floatval($deactivatedUserPercentage).'%)</td>';
	$reportContentPDF .= '</tr>';
	$reportContentPDF .= '</table>';
	 
	$reportContentPDF .= getGraphImageHTML(get_string('userreport','multiuserreport'));	 
	
	if(count($allSystemUserArr) > 0 ){
		$curtime = time();
		$mDash = getMDash();
		$mDashCSV = getMDashForCSV();
                //pr($allSystemUserArr);die;
		foreach($allSystemUserArr as $userId=>$data){
               
                        $uniqueID = (trim($data->user_identifier))?trim($data->user_identifier):getMdash();
                        
                        $uniqueIDCSV = (trim($data->user_identifier))?trim($data->user_identifier):getMDashForCSV();
			$totalLearnerInDept = 0;
			$assestHTML = '';
			$fullName = $data->fullname;
			$userName = $data->username?$data->username:getMDash();
			$userNameCSV = $data->username?$data->username:getMDashForCSV();
			 
			$jobTitleHtml = '';
			$companyHtml = '';
                        $contactDetails = "";
                        $contactDetailsCSV = "";
			if($CFG->showJobTitle == 1 && $data->job_title){
				$contactDetails .= "<strong>".get_string("jobtitle").":</strong> ".$data->job_title."<br/>";
			                               
                        }
                        if($data->report_to){
				$contactDetails .= "<strong>".get_string("report_to").":</strong> ".$data->report_to."<br/>";
			}
                        if($data->email){
				$contactDetails .= "<strong>".get_string("email").":</strong> ".$data->email."<br/>";
			}
                        
                        $contactDetails .= "<strong>".get_string("ismanageryescsv").":</strong> ".$CFG->isManagerYesOptions[$data->is_manager_yes];
                        
			if($CFG->showCompany == 1 && $data->company){
				$companyHtml = $data->company;
			}
			$manager_name = '';
	  		if($CFG->showInlineManagerNCompany==1){
	  			$manager_name = $data->inlinemanager_name;
	  			if(trim($manager_name)==''){
	  				$manager_name = $data->usermanager;
	  			}
	  		}else{
	  			$manager_name = $data->usermanager;
	  		}
	  		//$manager_name = $manager_name?$manager_name:$mDash;
			//$contactDetails = ($jobTitleHtml?$jobTitleHtml."<br>":'').$data->email.($data->phone1?"<br>".$data->phone1:'').($data->phone2?"<br>".$data->phone2:'').($companyHtml?"<br>".$companyHtml:'');
			//$contactDetailsCSV = $jobTitleHtml.'|'.$data->email.($data->phone1?"|".$data->phone1:'').($data->phone2?"|".$data->phone2:'').'|'.$companyHtml;
			$userManager =$manager_name?$manager_name:$mDash;
			$userManagerCSV = $manager_name?$manager_name:$mDashCSV;
			$departmentTitle = $data->departmenttitle?$data->departmenttitle:$mDash;
			$departmentTitleCSV = $data->departmenttitle?$data->departmenttitle:$mDashCSV;
			$departmentIdStr = $data->department_id;
			$teamTitle = $data->teamtitle?(str_replace(",",",<!-- <br> -->",$data->teamtitle)):$mDash;
			$teamTitleCSV =  $data->teamtitle?(str_replace(",","|", $data->teamtitle)):$mDashCSV;
			$createdById = $data->createdby;
			$user->id = $userId;
			$userHTML .=  '<tr>';
			if(!empty($export)){
				$userHTML .=  '<td>'.$fullName.'</td>';
				$userHTML .=  '<td>'.$userName.'</td>';
			}else{
				 

				$pBack =  strstr($_SERVER['REQUEST_URI'], 'reports/user_report.php')?3:1;
				$userHTML .=  '<td><a href="'.$CFG->wwwroot.'/reports/user_performance_report.php?uid='.$userId.'&back='.$pBack.'" >'.$fullName.'</a></td>';
				$userHTML .=  '<td><a href="'.$CFG->wwwroot.'/reports/user_performance_report.php?uid='.$userId.'&back='.$pBack.'" >'.$userName.'</a></td>';
			}
                        
                        if(trim($data->city)){
                            $city = $data->city;
                            $cityCSV = $data->city;
                        }else{
                            $city = $mDash;
                            $cityCSV = $mDashCSV;
                        }
			$userHTML .=  '<td>'.$uniqueID.'</td>';
                        $userHTML .=  '<td>'.$city.'</td>';
			$userHTML .=  '<td class="email-word-wrap">'.$contactDetails.'</td>';
			$userHTML .=  '<td>'.$userManager.'</td>';
			$userHTML .=  '<td>'.$departmentTitle.'</td>';
			$userHTML .=  '<td nowrap="true">'.$teamTitle.'</td>';
			$userHTML .=  '</tr>';
			 $data->job_title = ($data->job_title)?str_replace(',',' | ',$data->job_title):getMDashForCSV();
                         $data->report_to = ($data->report_to)?$data->report_to :getMDashForCSV();
			$reportContentCSV .= $fullName.",".$userNameCSV.",".$uniqueIDCSV.",".$data->job_title.",".$data->report_to.",".$CFG->isManagerYesOptions[$data->is_manager_yes].",".$cityCSV.",".$data->email.",".$userManagerCSV.",".$departmentTitleCSV.",".$teamTitleCSV."\n";
			
		}
		 
	

		$userHTML .=  '<script language="javascript" type="text/javascript">';
		$userHTML .=  '	$(document).ready(function(){
			
											$("#printbun").bind("click", function(e) {
												var url = $(this).attr("rel");
												window.open(url, "'.get_string('userereport','multiuserreport').'", "'.$CFG->printWindowParameter.'");
											});
			
									 }); ';
		if($allSystemUserCount > 0 ){
			$graphtLabelHtml = '';
			$userHTML .=  ' window.onload = function () {
	 
													var chart = new CanvasJS.Chart("chartContainer", {
																  theme: "theme2",//theme1
																  title:{
																	  /*text: "'.get_string('departmentvsuserchart', 'multiuserreport').'"*/
																 },
																  data: [
																  {
																	  // Change type to "bar", "splineArea", "area", "spline", "pie","column",  "doughnut" etc.
																	  type: "doughnut",
																	  indexLabelFontFamily: "Arial",
																	  indexLabelFontSize: 12,
																	  startAngle:0,
																	  indexLabelFontColor: "dimgrey",
																	  indexLabelLineColor: "darkgrey",
																	  toolTipContent: "{y}%",
																	  //indexLabelPlacement: "inside",
																	  ';
	  	
	  	
			$userHTML .=  '  dataPoints: [ ';
			 
			$graphtLabelHtml .= '{  y: '.$activateUserPercentage.', label: "'.get_string('statusactive','multiuserreport').' ('.$activateUser.')", exploded: "'.$activateUserExploded.'" },';
			$graphtLabelHtml .= '{  y: '.$deactivatedUserPercentage.', label: "'.get_string('statusdeactive','multiuserreport').' ('.$deactivatedUser.')", exploded: "'.$deactivatedUserExploded.'" },';
	  	
			 
			$userHTML .= 	$graphtLabelHtml;
			$userHTML .=  '  ]
																  }
																  ]
															  });
				
															  chart.render();' ;
			 
			 
	  	
			 
			$userHTML .=  '  } ';
			 
			$userHTML .=  ' </script>
										<script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js">';
			 
			$userHTML .=  '</script>';
		}
		 
		 
		 
	}else{
		$userHTML .=  '<tr><td colspan="8" style="border-bottom:1px solid #b9b9b9!important;">'.get_string('norecordfound','multiuserreport').'</td></tr>';
		$reportContentCSV .= get_string('norecordfound','multiuserreport')."\n";
		$reportContentPDF .= get_string('norecordfound','multiuserreport')."\n";
	}
	$userHTML .= '</tbody></table></div></div></div>';
	 
	if(empty($export)){
		$userHTML .= paging_bar($allSystemUserCount, $page, $perpage, $genURL);
	}
	$userHTML .= '';	 
	$userReport->userHTML = $userHTML;
	$userReport->reportContentCSV = $reportContentCSV;
	$userReport->reportContentPDF = $reportContentPDF;	 
	return $userReport;
}
/*
@param $arrEnrollmentData array
*/
function getGraphData($arrEnrollmentData,$return = false){
	GLOBAL $CFG,$DB,$USER;
	$exportHTML = '';
	if($arrEnrollmentData['action'] != 'print' && $arrEnrollmentData['action'] != 'exportpdf'){
		$exportHTML .= '<div class="exports_opt_box"> ';
		$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','singlereport').'" href="'.$arrEnrollmentData['url'].'&action=exportcsv">'.get_string('downloadcsv','singlereport').'</a> ';
		$exportHTML .= '<span class="seperater">&nbsp;</span>';
		$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','singlereport').'" id="exportpdf1" href="'.$arrEnrollmentData['url'].'&action=exportpdf">'.get_string('downloadpdf','singlereport').'</a> ';
		$exportHTML .= '<span class="seperater">&nbsp;</span>';
		$exportHTML .= '<a rel="'.$arrEnrollmentData['url'].'&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','singlereport').'" class="print_icon">'.get_string('print','singlereport').'</a>';
		$exportHTML .= '</div>';
	}

	$divHtml .= '<div class = "single-report-start" id="watch"><div class = "single-report-graph-box"> <span class="main-heading">'.$arrEnrollmentData['graph_heading'].$exportHTML.'</span>';
	$javascriptHtml = '';

	$dataCount = count($arrEnrollmentData['data']);
	
	if(isset($arrEnrollmentData['total']['value']) && $arrEnrollmentData['total']['value'] != 0){
		$displayTotal = $arrEnrollmentData['total']['value'];
		if(isset($arrEnrollmentData['total']['displayvalue'])){
			$displayTotal =$arrEnrollmentData['total']['displayvalue'];  
		}
		$divHtml .= '<div class="single-report-graph-left" id="chartContainer" >'.get_string('loading','multiuserreport').'</div><div class="single-report-graph-right"><div class="course-count-heading" >'.$arrEnrollmentData['total']['string'].'</div><div class="course-count" >'.$displayTotal.'</div><div class="seperator">&nbsp;</div><div class="course-status">';

		$javascriptHtml .=  '<script language="javascript" type="text/javascript">';
		$javascriptHtml .=  'window.onload = function () {var chart = new CanvasJS.Chart("chartContainer",{title:{text: ""},theme: "theme2",data: [{type: "doughnut",indexLabelFontFamily: "Arial",indexLabelFontSize: 12,startAngle:0,indexLabelFontColor: "dimgrey",indexLabelLineColor: "darkgrey",toolTipContent: "{y}%",dataPoints: [';
		$i = 0;
		foreach($arrEnrollmentData['data'] as $key=>$data){
			$displayText = $data["value"];
			if(isset($data["displayvalue"])){
				$displayText = $data["displayvalue"];
			}
			$divHtml .= '<div class="'.$data["class"].'"> <h6>'.$data["string"].'</h6><span>'.$displayText.'</span></div>';
			
			$percentage = 0;
			if($data["value"] != 0){
				$percentage = numberFormat(($data["value"]*100)/$arrEnrollmentData['total']['value']);
			}
			$javascriptHtml .= '{y: '.$percentage.', label: "'.$data["string"].' ('.$displayText.')", exploded:"" }';

			if($dataCount != $i){
				$divHtml .= '<div class="clear"></div>';
				$javascriptHtml .= ',';
			}
			$i++;
		}
		$javascriptHtml .=  ']}]});chart.render();';
		$javascriptHtml .=  '}</script><script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js" ></script>';
		$divHtml .= '</div></div>';
	}
	$divHtml .= '</div></div><div class="clear"></div>';
	if($return == true){
		return $divHtml.$javascriptHtml;
	}
	echo $divHtml.$javascriptHtml;
	echo includeGraphFiles($arrEnrollmentData['graph_heading']);
}
function getLearnerPerformanceReport($paramArray,$page,$perpage,$field = ''){
	GLOBAL $CFG,$DB,$USER;
	$sDepartmentArr = explode("@",$paramArray['department']);
	$sTeamArr = explode("@",$paramArray['team']);
	$sUserArr = explode("@",$paramArray['user']);
	$sTypeArr = explode("@",$paramArray['type']);
	$sUserGroup	= explode("@",$paramArray['user_group']);
	$sJobTitleArr = explode("@",$paramArray['job_title']);
	$sCompanyArr = explode("@",$paramArray['company']);
	
        $sIsManagerYes=$paramArray['is_manager_yes'];
        $sIsManagerYesArr=explode("@",$sIsManagerYes);
        
	$repTypeArr = $CFG->courseStatusArr;
	$sTypeArr2 = str_replace(array(3, 1, 2), $repTypeArr, $sTypeArr); 
	$sTypeName = implode(", ", $sTypeArr2);
	$where = "";
	$blankArray = array('-1','',0);
	if(!empty($sDepartmentArr) && !in_array($sDepartmentArr[0],$blankArray)){
		$where .= "AND rlp.department IN (".implode(',',$sDepartmentArr).") ";
	}
	if(isset($paramArray['sel_mode']) && $paramArray['sel_mode'] == 2){
		$where .= "AND (NOT FIND_IN_SET(0,rlp.global_groupid)) AND rlp.global_groupid IS NOT NULL AND rlp.global_groupid != '' ";
	}
	if($USER->archetype == $CFG->userTypeManager){
		$groupOwnerTeamIds = fetchGroupsList();
		if(!empty($groupOwnerTeamIds)){
			$where = "AND (rlp.department = ".$USER->department." ";
			$TeamSearchStringArr = array();
			foreach($groupOwnerTeamIds as $sTeamVal){
			  $TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,rlp.global_groupid) ";
			  $TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,rlp.group_id) ";
			}
			if(count($TeamSearchStringArr) > 0 ){
			   $TeamSearchString = implode(" OR ", $TeamSearchStringArr);
			   $where .= "OR ($TeamSearchString) ";
			}
			$where .= " )";
		}else{
			$where = "AND rlp.department = ".$USER->department." ";
		}
	}elseif($USER->archetype == $CFG->userTypeStudent){
		$groupOwnerTeamIds = fetchGroupsList();
		$TeamSearchStringArr = array();
		foreach($groupOwnerTeamIds as $sTeamVal){
			$TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,rlp.global_groupid) ";
			$TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,rlp.group_id) ";
		}
		if(count($TeamSearchStringArr) > 0 ){
		   $TeamSearchString = implode(" OR ", $TeamSearchStringArr);
		   $where .= "AND ($TeamSearchString) ";
		}
	}
	if(!empty($sUserArr) && !in_array($sUserArr[0],$blankArray)){
		$where .= "AND rlp.user_id IN (".implode(',',$sUserArr).") ";
	}
	if(!empty($sJobTitleArr) && !in_array($sJobTitleArr[0],$blankArray)){
		$where .= "AND rlp.job_title IN (".implode(',',$sJobTitleArr).") ";
	}
        
        if($paramArray['report_to'] != '' && $paramArray['report_to'] != '-1' && count($paramArray['report_to']) > 0){
			 $id_explode = explode("@",$paramArray['report_to']);
			 $List = getLeadersNameList();

			 $leaders_name = '';
			 foreach($id_explode as $id){
                                    $leaders_name .= '"'.$List[$id].'",';
			 }
			 $leaders_name = trim($leaders_name,',');
			 $where .= "AND mu.report_to IN ($leaders_name)";
        }
	if(!empty($sCompanyArr) && !in_array($sCompanyArr[0],$blankArray)){
		$where .= "AND company IN (".implode(',',$sCompanyArr).") ";
	}
        
        if(count($sIsManagerYesArr) && !in_array('-1',$sIsManagerYesArr))
        {
           $where .= "AND mu.is_manager_yes in(".@implode(',',$sIsManagerYesArr).") ";
        }
       
	if($field != 'count_graph' && !empty($sTypeArr) && !in_array($sTypeArr[0],$blankArray)){
		$typeSearchArray = array();
		foreach($sTypeArr as $sType){
			if($sType == 3){
				$typeSearchArray[] = "not_started > 0";
			}elseif($sType == 2){
				$typeSearchArray[] = "completed > 0";
			}elseif($sType == 1){
				$typeSearchArray[] = "in_progress > 0";
			}
		}
		if(count($typeSearchArray) > 0 ){
		   $typeSearchString = implode(" OR ", $typeSearchArray);
		   $where .= "AND ($typeSearchString) ";
		}
	}
	if(count($sTeamArr) > 0 &&  !in_array($sTeamArr[0] ,$blankArray)){
		$TeamSearchStringArr = array();
		foreach($sTeamArr as $sTeamVal){
		  $TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,rlp.group_id) ";
		}
		if(count($TeamSearchStringArr) > 0 ){
		   $TeamSearchString = implode(" OR ", $TeamSearchStringArr);
		   $where .= "AND ($TeamSearchString) ";
		}
	}
	if(isset($paramArray['sel_mode']) && $paramArray['sel_mode'] == 2){
		if(count($sUserGroup) > 0 &&  !in_array($sUserGroup[0] ,$blankArray)){
			$TeamSearchStringArr = array();
			foreach($sUserGroup as $sTeamVal){
			  $TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,rlp.global_groupid) ";
			}
			if(count($TeamSearchStringArr) > 0 ){
			   $TeamSearchString = implode(" OR ", $TeamSearchStringArr);
			   $where .= "AND ($TeamSearchString) ";
			}
		}
	}
        
        $paramArray['key'] = addslashes($paramArray['key']);
        if(($paramArray['key'] != '')) {
		$where .= "AND (rlp.first_name like '%".$paramArray['key']."%' OR rlp.last_name like '%".$paramArray['key']."%' OR rlp.user_name like '%".$paramArray['key']."%' OR CONCAT(rlp.first_name,' ',rlp.last_name) like '%".$paramArray['key']."%' ) ";
	}
        
	if($where != ''){
		$where = ' WHERE '.LTRIM($where,'AND');
               // echo $where;die;
	}
	$sql = 'SELECT ##SELECT_FIELDS## FROM mdl_report_learner_performance rlp LEFT JOIN mdl_user mu ON(rlp.user_id=mu.id) LEFT JOIN mdl_job_title jt ON(mu.job_title=jt.id)'.$where.' ORDER BY COALESCE(NULLIF(first_name, ""), last_name), last_name ASC';
	        
        $limit = '';
	if($field == '*'){
		$sql = str_replace('##SELECT_FIELDS##',"rlp.id,rlp.user_id,mu.report_to,mu.is_manager_yes,jt.title,concat(rlp.first_name,' ', rlp.last_name) as fullname,rlp.user_name,rlp.enrolled,rlp.not_started,rlp.in_progress,rlp.completed",$sql);
		if($perpage != 0){
			$offset = $page - 1;
			$offset = $offset*$perpage;
			$sql .= " LIMIT $offset, $perpage";
		}
               //echo $sql;
                
		$data = $DB->get_records_sql($sql);
	}else{
		$sql = str_replace('##SELECT_FIELDS##','count(*) as total_records,SUM(enrolled) as total_enrollment,sum(not_started) as total_notstarted,sum(in_progress) as total_inprogress, sum(completed) as total_completed',$sql);
		$data = $DB->get_record_sql($sql);
	}
	return $data;
}
function getUserPerformanceReport($paramArray,$page,$perpage,$userId,$fields = ''){
	GLOBAL $CFG,$DB,$USER;

	$sStartDate = $paramArray['startDate'];
	$sEndDate = $paramArray['endDate'];
	$where = " WHERE user_id = ".$userId;
	$blankArray = array('-1','',0);
	if(!empty($sStartDate) && !in_array($sStartDate,$blankArray)){
		$where .= " AND last_accessed >= ".$sStartDate;
	}
	if(!empty($sEndDate) && !in_array($sEndDate,$blankArray)){
		$where .= " AND last_accessed <= ".$sEndDate;
	}
	$limit = '';
	if($fields == ''){
		$sql = 'SELECT ##SELECT_FIELDS## FROM mdl_report_learner_performance_details '.$where.' GROUP BY course_id ORDER BY course_name ASC';
		$sql = str_replace('##SELECT_FIELDS##',"*",$sql);
		if($perpage != 0){
			$offset = $page - 1;
			$offset = $offset*$perpage;
			$sql .= " LIMIT $offset, $perpage";
		}
		$data = $DB->get_records_sql($sql);
	}else{
		$sql = 'SELECT id,count(*) as status_count,course_status FROM (SELECT id,course_id,course_status FROM mdl_report_learner_performance_details u '.$where.' GROUP BY u.course_id) as z GROUP BY course_status';
		$data1 = $DB->get_records_sql($sql);
		$total = 0;
		$data['not_started'] = 0;
		$data['in_progress'] = 0;
		$data['completed'] = 0;
		foreach($data1 as $dataRec){
			$total += $dataRec->status_count;
			$index = str_replace(' ','_',$dataRec->course_status);
			$data[$index] = $dataRec->status_count;
		}
		$data['total_records'] = $total;
	}
	return $data;
}
function getCourseAssets($arrCourses,$userId){
	GLOBAL $CFG,$DB,$USER;
	$data = array();
	if(!empty($arrCourses)){
		$where = " WHERE user_id = ".$userId." AND course_id IN (".implode(',',$arrCourses).") ";
		$sql = 'SELECT ##SELECT_FIELDS## FROM mdl_report_learner_performance_details '.$where.'ORDER BY course_name ASC,asset_name ASC';
		$sql = str_replace('##SELECT_FIELDS##',"*",$sql);
		$assetsData = $DB->get_records_sql($sql);
		foreach($assetsData as $asset){
			$data[$asset->course_id][] = $asset;
		}
	}
	return $data;
}
function learnerPerformanceToCSV($paramArray,$page,$perpage,$export){
	GLOBAL $CFG,$DB,$USER;
	$sDepartmentArr = explode("@",$paramArray['department']);
	$sTeamArr = explode("@",$paramArray['team']);
	$sUserArr = explode("@",$paramArray['user']);
	$sTypeArr = explode("@",$paramArray['type']);
	$sUserGroup	= explode("@",$paramArray['user_group']);
	$sJobTitleArr = explode("@",$paramArray['job_title']);
	$sCompanyArr = explode("@",$paramArray['company']);
        
        $sIsManagerYes=$paramArray['is_manager_yes'];
        $sIsManagerYesArr=explode("@",$sIsManagerYes);
        
	$blankArray = array('-1','',0);
	if(empty($sTypeArr) || in_array($sTypeArr[0],$blankArray)){
	  $sTypeName = get_string('all','multiuserreport');
	}else{
	  $repTypeArr = $CFG->courseStatusArr;
	  $sTypeArr2 = str_replace(array(3, 1, 2), $repTypeArr, $sTypeArr); 
	  $sTypeName = implode(", ", $sTypeArr2);
	}
	$sJobTitleName = (empty($sJobTitleArr) || in_array($sJobTitleArr[0],$blankArray))?(get_string('all','multiuserreport')):getJobTitles($sJobTitleArr);
	$sCompanyName = (empty($sCompanyArr) || in_array($sCompanyArr[0],$blankArray))?(get_string('all','multiuserreport')):getCompany($sCompanyArr);
	$sDepartmentName = (empty($sDepartmentArr) || in_array($sDepartmentArr[0],$blankArray))?(get_string('all','multiuserreport')):getDepartments($sDepartmentArr, true);
	$sTeamName = (empty($sTeamArr) || in_array($sTeamArr[0],$blankArray))?(get_string('all','multiuserreport')):getTeams($sTeamArr);
	$sUserFullName = (empty($sUserArr) || in_array($sUserArr[0],$blankArray))?(get_string('all','multiuserreport')):getUsers($sUserArr);
	$sUserGroupName = (empty($sUserGroup) || in_array($sUserGroup[0],$blankArray))?(get_string('all','multiuserreport')):getUserGroup($sUserGroup);
	
        $sAdManagersName = getAdManagersNonManagersList($sIsManagerYesArr);
        
        $sleaders_name = '';
        if($paramArray['report_to'] != '' && $paramArray['report_to'] != '-1' && count($paramArray['report_to']) > 0){
                $id_explode = explode("@",$paramArray['report_to']);
                $List = getLeadersNameList();               
                foreach($id_explode as $id){
                       $sleaders_name .= $List[$id].' | ';
                }             
                $sleaders_name = trim($sleaders_name, ' | ');
        }
        if(!$sleaders_name){
            $sleaders_name = get_string('all','multiuserreport');
        }
        
        
	if($USER->archetype != $CFG->userTypeAdmin ){
		$sDepartmentName = getDepartments($USER->department, $isReport);
	}
	if($export == 'exportcsv'){
		$enrolledData = getLearnerPerformanceReport($paramArray,1,0,'*');
		$enrollentCount = getLearnerPerformanceReport($paramArray,1,0,'count');
		$reportContentCSV = '';
		if($_REQUEST["sel_mode"] == 2){
			$reportContentCSV .= get_string('global_team').",".$sUserGroupName."\n";
		}else{
			$reportContentCSV .= get_string('department','multiuserreport').",".$sDepartmentName."\n".get_string('team','multiuserreport').",".$sTeamName ."\n";
		}
		if($CFG->showJobTitle == 1){
		  $reportContentCSV .= get_string('job_title','user').",".$sJobTitleName."\n";
		}
                
                if($CFG->showReportTo == 1){
                    $reportContentCSV .= get_string('report_to').",".$sleaders_name."\n";
		}
                
                $reportContentCSV .= get_string('ismanageryescsv').",".$sAdManagersName."\n";
                
		if($CFG->showCompany == 1){
		  $reportContentCSV .= get_string('company','user').",".$sCompanyName."\n";
		}
				
		$reportContentCSV .= get_string('user','multiuserreport').",".$sUserFullName."\n";
		$reportContentCSV .= get_string('statusreport').",".$sTypeName."\n";
		$notStartedPercentage = 0;
		$inProgressPercentage = 0;
		$completedPercentage = 0;
		if($enrollentCount->total_enrollment != 0){
			$notStartedPercentage = floatval(($enrollentCount->total_notstarted*100)/$enrollentCount->total_enrollment);
			$inProgressPercentage = floatval(($enrollentCount->total_inprogress*100)/$enrollentCount->total_enrollment);
			$completedPercentage = floatval(($enrollentCount->total_completed*100)/$enrollentCount->total_enrollment);
		}
		$reportContentCSV .= get_string('numberofcourses','multiuserreport').",".$enrollentCount->total_enrollment."\n";
		$reportContentCSV .= get_string('notstarted','multiuserreport').",".$enrollentCount->total_notstarted." (".round($notStartedPercentage,2)."%)\n";
		$reportContentCSV .= get_string('inprogress','multiuserreport').",".$enrollentCount->total_inprogress." (".round($inProgressPercentage,2)."%)\n";
		$reportContentCSV .= get_string('completed','multiuserreport').",".$enrollentCount->total_completed." (".round($completedPercentage,2)."%)\n";

		$reportContentCSV .= get_string('usereperformancereport','multiuserreport')."\n";

		$reportContentCSV .= get_string('fullname','multiuserreport').",".get_string('username').",".get_string('jobtitle').",".get_string('report_to').",".get_string('ismanageryescsv').",".get_string('enrolled','multiuserreport').",".get_string('notstarted','multiuserreport').",".get_string('inprogress','multiuserreport').",".get_string('completed','multiuserreport')."\n";
		
		if(!empty($enrolledData)){
			foreach($enrolledData as $enrolled){
                                $enrolled->title = ($enrolled->title)?str_replace(',',' | ',$enrolled->title):getMDashForCSV();
                                $enrolled->report_to = ($enrolled->report_to)?$enrolled->report_to:getMDashForCSV();
                                $enrolled->is_manager_yes = $CFG->isManagerYesOptions[$enrolled->is_manager_yes];
                                
				$reportContentCSV .= $enrolled->fullname.",".$enrolled->user_name.",".$enrolled->title.",".$enrolled->report_to.",".$enrolled->is_manager_yes.",".$enrolled->enrolled.",".$enrolled->not_started.",".$enrolled->in_progress.",".$enrolled->completed."\n";
			}
		}else{
			$reportContentCSV .= get_string('norecordfound','multiuserreport')."\n";
		}
		echo $filepath = $CFG->dirroot."/local/reportexport/temp";		
		chmod($filepath, 0777);		
		$filename = str_replace(' ', '_', get_string('usereperformancereport','multiuserreport'))."_".date("m-d-Y").".csv";  
		$filepathname = $filepath.'/'.$filename;
		unlink($filepathname);
		$handler = fopen($filepathname, "w");
		fwrite($handler, $reportContentCSV);
		exportCSV($filepathname);
	}elseif($export == 'print'){
		$reportName = get_string('usereperformancereport','multiuserreport');
		$userHTML .= getReportPrintHeader($reportName);
		$userHTML .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="generaltable margin_bottom">';
		$userHTML .= '<tr>';
		if($_REQUEST["sel_mode"] == 2){
			$userHTML .= '<td colspan = "2" ><strong>'.get_string('global_team').':</strong> '.$sUserGroupName.'</td>';
		}else{
			$userHTML .= '<td><strong>'.get_string('department','multiuserreport').':</strong> '.$sDepartmentName.'</td>';
			$userHTML .= '<td><strong>'.get_string('team','multiuserreport').':</strong>  '.$sTeamName.'</td>';
		}
		$userHTML .= '</tr>';

		if($CFG->showJobTitle == 1 && $CFG->showCompany == 1){
			$userHTML .= '<tr>';
			$userHTML .= '<td ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
			$userHTML .= '<td><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
			$userHTML .= '</tr>';
		}elseif($CFG->showJobTitle == 1 && $CFG->showCompany == 0){
			$userHTML .= '<tr>';
			$userHTML .= '<td colspan="2" ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
			$userHTML .= '</tr>';
		}elseif($CFG->showJobTitle == 0 && $CFG->showCompany == 1){
			$userHTML .= '<tr>';
			$userHTML .= '<td colspan="2" ><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
			$userHTML .= '</tr>';
		}
                
                if($CFG->showReportTo == 1){
			$userHTML .= '<tr>';
			$userHTML .= '<td colspan="2" ><strong>'.get_string('report_to').':</strong> '.$sleaders_name.'</td>';
			$userHTML .= '</tr>';
		}

		$userHTML .= '<tr>';
		$userHTML .= '<td><strong>'.get_string('user','multiuserreport').':</strong> '.$sUserFullName.'</td>';
		$userHTML .= '<td><strong>'.get_string('statusreport').':</strong> '.$sTypeName.'</td>';
		$userHTML .= '</tr>';
		$enrollentCount = getLearnerPerformanceReport($paramArray,1,0,'count');
		$notStartedPercentage = 0;
		$inProgressPercentage = 0;
		$completedPercentage = 0;
		if($enrollentCount->total_enrollment != 0){
			$notStartedPercentage = floatval(($enrollentCount->total_notstarted*100)/$enrollentCount->total_enrollment);
			$inProgressPercentage = floatval(($enrollentCount->total_inprogress*100)/$enrollentCount->total_enrollment);
			$completedPercentage = floatval(($enrollentCount->total_completed*100)/$enrollentCount->total_enrollment);
		}
		$userHTML .= '<tr>';
		$userHTML .= '<td><strong>'.get_string('numberofcourses','multiuserreport').':</strong> '.$enrollentCount->total_enrollment.'</td>';
		$userHTML .= '<td><strong>'.get_string('notstarted','multiuserreport').':</strong> '.$enrollentCount->total_notstarted.' ('.round($notStartedPercentage,2).'%)</td>';
		$userHTML .= '</tr>';
		$userHTML .= '<tr>';
		$userHTML .= '<td><strong>'.get_string('inprogress','multiuserreport').':</strong> '.$enrollentCount->total_inprogress.' ('.round($inProgressPercentage,2).'%)</td>';
		$userHTML .= '<td><strong>'.get_string('completed','multiuserreport').':</strong> '.$enrollentCount->total_completed.' ('.round($completedPercentage,2).'%)</td>';
		$userHTML .= '</tr>';

		$userHTML .= '</table>';
		return $userHTML;
	}elseif($export == 'exportpdf'){
		$enrollentCount = getLearnerPerformanceReport($paramArray,1,0,'count');
		$notStartedPercentage = 0;
		$inProgressPercentage = 0;
		$completedPercentage = 0;
		if($enrollentCount->total_enrollment != 0){
			$notStartedPercentage = floatval(($enrollentCount->total_notstarted*100)/$enrollentCount->total_enrollment);
			$inProgressPercentage = floatval(($enrollentCount->total_inprogress*100)/$enrollentCount->total_enrollment);
			$completedPercentage = floatval(($enrollentCount->total_completed*100)/$enrollentCount->total_enrollment);
		}
		$reportContentPDF = '';
		$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="generaltable">';
		$reportContentPDF .= '<tr>';
		if($_REQUEST["sel_mode"] == 2){
		$reportContentPDF .= '<td colspan = "2" ><strong>'.get_string('global_team').':</strong> '.$sUserGroupName.'</td>';
		}else{


		$reportContentPDF .= '<td><strong>'.get_string('department','multiuserreport').':</strong> '.$sDepartmentName.'</td>';
		$reportContentPDF .= '<td><strong>'.get_string('team','multiuserreport').':</strong>  '.$sTeamName.'</td>';
		}
		$reportContentPDF .= '</tr>';

		if($CFG->showJobTitle == 1 && $CFG->showCompany == 1){
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
		$reportContentPDF .= '<td><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
		$reportContentPDF .= '</tr>';
		}elseif($CFG->showJobTitle == 1 && $CFG->showCompany == 0){
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td colspan="2" ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
		$reportContentPDF .= '</tr>';
		}elseif($CFG->showJobTitle == 0 && $CFG->showCompany == 1){
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td colspan="2" ><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
		$reportContentPDF .= '</tr>';
		}

		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td><strong>'.get_string('user','multiuserreport').':</strong> '.$sUserFullName.'</td>';
		$reportContentPDF .= '<td><strong>'.get_string('statusreport').':</strong> '.$sTypeName.'</td>';
		$reportContentPDF .= '</tr>';

		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td><strong>'.get_string('numberofcourses','multiuserreport').':</strong> '.$enrollentCount->total_enrollment.'</td>';
		$reportContentPDF .= '<td><strong>'.get_string('notstarted','multiuserreport').':</strong>  '.$enrollentCount->total_notstarted.' ('.round($notStartedPercentage,2).'%)</td>';				
		$reportContentPDF .= '</tr>';

		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td><strong>'.get_string('inprogress','multiuserreport').':</strong>  '.$enrollentCount->total_inprogress.' ('.round($inProgressPercentage,2).'%)</td>';				
		$reportContentPDF .= '<td><strong>'.get_string('completed','multiuserreport').':</strong>  '.$enrollentCount->total_completed.' ('.round($completedPercentage,2).'%)</td>';							
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '</table>';

		$reportContentPDF .= getGraphImageHTML(get_string('usereperformancereport','multiuserreport'));
		$filename = str_replace(' ', '_', get_string('usereperformancereport','multiuserreport'))."_".date("m-d-Y").".pdf";
		exportPDF($filename, $reportContentPDF, '', get_string('usereperformancereport','multiuserreport'));
	}
}
function learnerPerformanceDetailsExport($paramArray,$page,$perpage,$export,$userId){
	GLOBAL $CFG,$DB,$USER;
	$sDateSelected = $paramArray['sDateSelected'];
	$eDateSelected = $paramArray['eDateSelected'];
	$enrollentCount = getUserPerformanceReport($paramArray,1,0,$userId,'count');
	$notStartedPercentage = 0;
	$inProgressPercentage = 0;
	$completedPercentage = 0;
	$userName = getUsers($userId, 1);
	if($enrollentCount['total_records'] != 0){
		$notStartedPercentage = floatval(($enrollentCount['not_started']*100)/$enrollentCount['total_records']);
		$inProgressPercentage = floatval(($enrollentCount['in_progress']*100)/$enrollentCount['total_records']);
		$completedPercentage = floatval(($enrollentCount['completed']*100)/$enrollentCount['total_records']);
	}
	if($export == 'exportcsv'){
		$enrolledData = getUserPerformanceReport($paramArray,1,0,$userId);
                $setDataorder = setdisplayorderofdata($enrolledData,'course_id');
               // pr($enrolledData);die;
		$arrAssets = array();
		$arrCourses = array();
		if(!empty($enrolledData)){
			foreach($enrolledData as $enrolled){
				$arrCourses[] = $enrolled->course_id;
			}
			$arrAssets = getCourseAssets($arrCourses,$userId);
		}
		$sDateSelectedForPrintCSV = getDateFormatForPrint($sDateSelected, 'csv');
		$eDateSelectedForPrintCSV = getDateFormatForPrint($eDateSelected, 'csv');
		$reportContentCSV .= get_string('user','singlereport').",".$userName."\n";
		$reportContentCSV .= get_string('numberofcourses','singlereport').",".$enrollentCount['total_records']."\n";
		$reportContentCSV .= get_string('notstarted','singlereport').",".$enrollentCount['not_started']." (".round($notStartedPercentage,2)."%)\n";
		$reportContentCSV .= get_string('inprogress','singlereport').",".$enrollentCount['in_progress'] ." (".round($inProgressPercentage,2)."%)\n";
		$reportContentCSV .= get_string('completed','singlereport').",".$enrollentCount['completed'] ." (".round($completedPercentage,2)."%)\n";
		$reportContentCSV .= get_string('lastaccesseddatefrom').",".$sDateSelectedForPrintCSV."\n".get_string('lastaccesseddateto').",".$eDateSelectedForPrintCSV."\n\n";
		$reportContentCSV .= get_string('overallcourseprogressreport','singlereport')."\n";
		
		$reportContentCSV .= get_string('coursetitle','singlereport').",".get_string('enrolmentdate','singlecoursereport').",".get_string('status','singlereport').",".get_string('score','singlereport').",".get_string('assettimespent','singlereport').",".get_string('comletiondate').",".get_string('lastaccessed','singlereport')."\n";
		if(!empty($enrolledData)){
                    
                    $enrolledDatesSql = "SELECT courseid,created_on,createdby FROM mdl_user_course_mapping WHERE userid=".$userId;
                    $EnrolledDates = $DB->get_records_sql($enrolledDatesSql);
			foreach($enrolledData as $enrolled){
                            
                                if(isset($EnrolledDates[$enrolled->course_id])){
                                    $EnrollmentDate =getDateFormat($EnrolledDates[$enrolled->course_id]->created_on, $CFG->customDefaultDateTimeFormatForCSV);
                                }else{
                                    $EnrollmentDate = getMDashForCSV();
                                }
                                
				$reportContentCSV .= $enrolled->course_name.",".$EnrollmentDate.",".ucwords($enrolled->course_status).",".(getSpace('csv')).",".(getSpace('csv')).",".(getSpace('csv')).",".(getSpace('csv'))."\n";
				if(!empty($arrAssets[$enrolled->course_id])){
					foreach($arrAssets[$enrolled->course_id] as $assetData){
						if($assetData->last_accessed){
							$lastAccessedCSV = getDateFormat($assetData->last_accessed, $CFG->customDefaultDateTimeFormatForCSV);
						}else{
							$lastAccessedCSV = get_string('notaccessed','learnercourse');
						}
						if($assetData->completion_date){
							$completionDateTimeCSV = getDateFormat($assetData->completion_date, $CFG->customDefaultDateTimeFormatForCSV);
						}else{
							$completionDateTimeCSV = getMDashForCSV();
						}
						if(!$assetData->score){
							$assetData->score = getMDashForCSV();//0;
						}
						$timeSpent = $assetData->time_spent?(convertCourseSpentTime($assetData->time_spent)):getMDashForCSV();
						$reportContentCSV .= "  ".$assetData->asset_name.",".getMDashForCSV().",".ucwords($assetData->asset_status).",".$assetData->score.",".$timeSpent.",".$completionDateTimeCSV.",".$lastAccessedCSV."\n";
					}
				}
			}
		}else{
			$reportContentCSV .= get_string('norecordfound','singlereport')."\n";
		}
		$filepathname = getpdfCsvFileName('csv', get_string('overallcourseprogressreport','singlereport'), $reportContentCSV);
   	    exportCSV($filepathname);
	}elseif($export == 'exportpdf'){
		$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
		$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
		$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0"  class="generaltable"><tr><td colspan="2"></td></tr>';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td><strong>'.get_string('user','singlereport').':</strong> '.$userName.'</td>';
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td><strong>'.get_string('lastaccesseddatefrom').':</strong> '.$sDateSelectedForPrint.'</td>';
		$reportContentPDF .= '<td><strong>'.get_string('lastaccesseddateto').':</strong>  '.$eDateSelectedForPrint.'</td>';				
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td><strong>'.get_string('numberofcourses','singlereport').':</strong> '.$enrollentCount['total_records'].'</td>';
		$reportContentPDF .= '<td><strong>'.get_string('notstarted','singlereport').':</strong>  '.$enrollentCount['not_started'].' ('.round($notStartedPercentage,2).'%)</td>';				
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td><strong>'.get_string('inprogress','singlereport').':</strong>  '.$enrollentCount['in_progress'].' ('.round($inProgressPercentage,2).'%)</td>';
		$reportContentPDF .= '<td><strong>'.get_string('completed','singlereport').':</strong>  '.$enrollentCount['completed'].' ('.round($completedPercentage,2).'%)</td>';
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '</table>';
		$reportContentPDF .= getGraphImageHTML(get_string('overallcourseprogressreport','singlereport'));
		$filename = str_replace(' ', '_', get_string('overallcourseprogressreport','singlereport'))."_".date("m-d-Y").".pdf";
		exportPDF($filename, $reportContentPDF, '', get_string('overallcourseprogressreport','singlereport'));
	}elseif($export == 'print'){
		$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
		$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
		$reportName = get_string('overallcourseprogressreport','singlereport');
		$HTML .= getReportPrintHeader($reportName);
		$HTML .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="generaltable margin_bottom">';
		$HTML .= '<tr>';
		$HTML .= '<td colspan="2"><strong>'.get_string('user','singlereport').':</strong> '.$userName.'</td>';
		$HTML .= '</tr>';
		$HTML .= '<tr>';
		$HTML .= '<td width="50%"><strong>'.get_string('lastaccesseddatefrom').':</strong> '.$sDateSelectedForPrint.'</td>';
		$HTML .= '<td width="50%"><strong>'.get_string('lastaccesseddateto').':</strong>  '.$eDateSelectedForPrint.'</td>';				
		$HTML .= '</tr>';

		$HTML .= '<tr>';
		$HTML .= '<td><strong>'.get_string('numberofcourses','multiuserreport').':</strong> '.$enrollentCount['total_records'].'</td>';
		$HTML .= '<td><strong>'.get_string('notstarted','multiuserreport').':</strong> '.$enrollentCount['not_started'].' ('.round($notStartedPercentage,2).'%)</td>';
		$HTML .= '</tr>';
		$HTML .= '<tr>';
		$HTML .= '<td><strong>'.get_string('inprogress','multiuserreport').':</strong> '.$enrollentCount['in_progress'].' ('.round($inProgressPercentage,2).'%)</td>';
		$HTML .= '<td><strong>'.get_string('completed','multiuserreport').':</strong> '.$enrollentCount['completed'].' ('.round($completedPercentage,2).'%)</td>';
		$HTML .= '</tr>';
		$HTML .= '</table>';
		return $HTML;
	}
}


function getCourseProgressReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export=''){

	global $DB, $CFG, $USER, $SITE;
	$loginUserId = $USER->id;
	$isReport = true;
		
	$sType = $paramArray['type'];
	$sBack = $paramArray['back'];
		
	$sTypeArr = explode("@",$sType);

	$sStartDate     = $paramArray['startDate'];
	$sEndDate       = $paramArray['endDate'];

	$sDateSelected = '';
	if($sStartDate){
		$sDateArr = getFormattedTimeStampOfDate($sStartDate);
		$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

	}
		
	$eDateSelected = '';
	if($sEndDate){
		$eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
		$eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
	}
		
		
	$offset = $page - 1;
	$offset = $offset*$perpage;
	$limit = '';
	if($perpage != 0){
		$limit = "LIMIT $offset, $perpage";
	}

	////// Getting common URL for the Search //////
	$pageURL = $_SERVER['PHP_SELF'];
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
		
	$printUrl = '/reports/course_progress_report_print.php';
	$genPrintURL = genParameterizedURL($paramArray, $removeKeyArray, $printUrl);

	$cid    = $paramArray['cid'];
	$courseName = getCourses($cid);
		
	$where = '';		
		
	if($USER->archetype == $CFG->userTypeAdmin){
		$where .= '';
	}elseif($USER->archetype == $CFG->userTypeManager){
		$teamUser = fetchGroupsUserIds($USER->id,1);
		if(!empty($teamUser)){
			$groupsList =  implode(',',$teamUser);
			$where = " AND (mrlp.department = ".$USER->department." OR mrlpd.user_id IN ($groupsList)) ";
		}else{
			$where .= ' AND (mrlp.department = "'.$USER->department.'")';
		}
	}elseif($USER->archetype == $CFG->userTypeStudent){
		$teamUser = fetchGroupsUserIds($USER->id,1);
		if(!empty($teamUser)){
			$groupsList =  implode(',',$teamUser);
			$where = " AND (mrlpd.user_id IN ($groupsList)) ";
		}else{
			$where .= ' AND (mrlp.department = "'.$USER->department.'")';
		}
	}		
		
	if($sDateSelected && $eDateSelected){
		
		$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
		$sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;

		$where .= " AND ((mrlpd.last_accessed >= $sStartDateTime && mrlpd.last_accessed <= $sEndDateTime ) )";

	}elseif($sDateSelected && $eDateSelected == ''){
		
		$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;

		$where .= " AND (mrlpd.last_accessed >= ($sStartDateTime) )";

	}elseif($sDateSelected =='' && $eDateSelected){
		
		$sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
		$where .= " AND (mrlpd.last_accessed<= ($sEndDateTime))";
	}
		
		
	$query = "SELECT mrlpd.user_id,concat(mrlp.first_name,' ',mrlp.last_name) as fullname,mrlp.user_name,mrlpd.course_id,MAX(last_accessed) as last_access, if(course_status='completed',completion_date,0) as completion_date,if(course_status='not started','Not Started',if(course_status='completed','Completed','In Progress')) as course_status,mrlp.company_name,mrlp.manager_name,mrlp.inline_manager_name FROM  mdl_report_learner_performance mrlp LEFT JOIN mdl_report_learner_performance_details mrlpd on (mrlp.user_id=mrlpd.user_id)";
	$query .= " WHERE 1 = 1";
	$query .= " AND mrlpd.course_id = '".$cid."' ";
	$query .= $where;
	
	$query .= " GROUP BY mrlpd.user_id ";
	$query .= " ORDER BY COALESCE(NULLIF(mrlp.first_name, ''), mrlp.last_name), mrlp.last_name ";
	$queryLimit = $query.$limit;

	$limitReportsArr = $DB->get_records_sql($queryLimit);
		
	$completedPercentage = 0;
	$inProgressPercentage = 0;
	$notStartedPercentage = 0;
	$enrolmentCount = getCourseEnrolmentCount($cid,$where);
	$in_progress_count = isset($enrolmentCount['in progress']->course_status_count)?$enrolmentCount['in progress']->course_status_count:0;
	$completed_count = isset($enrolmentCount['completed']->course_status_count)?$enrolmentCount['completed']->course_status_count:0;
	$not_started_count = isset($enrolmentCount['not started']->course_status_count)?$enrolmentCount['not started']->course_status_count:0;
	$userCount = $in_progress_count+$completed_count+$not_started_count;
		
	$completedPercentage = numberFormat(($completed_count*100)/$userCount);
	$inProgressPercentage = numberFormat(($in_progress_count*100)/$userCount);
	$notStartedPercentage = numberFormat(($not_started_count*100)/$userCount);
		
	$in_progress_count_exploded = in_array(1, $sTypeArr)?true:false;
	$completed_count_exploded = in_array(2, $sTypeArr)?true:false;
	$not_started_count_exploded = in_array(3, $sTypeArr)?true:false;
		
	$courseHTML = '';
	$exportHTML = '';
	if(empty($export)){

		$exportHTML .= '<div class="exports_opt_box"> ';
		$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','singlereport').'" href="'.$genPrintURL.'&action=exportcsv">'.get_string('downloadcsv','singlereport').'</a> ';
		$exportHTML .= '<span class="seperater">&nbsp;</span>';
		$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','singlereport').'" id="exportpdf1" href="'.$genPrintURL.'&action=exportpdf">'.get_string('downloadpdf','singlereport').'</a> ';
		$exportHTML .= '<span class="seperater">&nbsp;</span>';
		$exportHTML .= '<a rel="'.$genPrintURL.'&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','singlereport').'" class="print_icon">'.get_string('print','singlereport').'</a>';
		$exportHTML .= '</div>';
			
	}
		
	$style = (!empty($export) || strstr($_SERVER['REQUEST_URI'], 'reports/course_progress_report.php'))?"style=''":'';
		
	$courseHTML .= "<div class='tabsOuter' ".$style." >";
	if(empty($export) && $sBack == 6){

		$courseHTML .= "<div class='tabLinks'>";
		ob_start();
		$course->id = $cid;
		include_once($CFG->dirroot . '/course/course_tabs.php');
		$HTMLTabs = ob_get_contents();
		ob_end_clean();
		$courseHTML .= $HTMLTabs;
		$courseHTML .= "</div>";
	}
		
	$courseHTML .= "<div class='borderBlockSpace'>";
		
	if(empty($export) && !strstr($_SERVER['REQUEST_URI'], 'course/singlecoursereport.php')){
		ob_start();
		require_once($CFG->dirroot . '/local/includes/course_report_details_search.php');
		$SEARCHHTML = ob_get_contents();
		ob_end_clean();
		$courseHTML .= $SEARCHHTML;
	}

	$courseHTML .= '<div class="clear"></div>';
	$courseHTML .= '<div class="userprofile view_assests">';
		
	$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
	$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
	$sDateSelectedForPrintCSV = getDateFormatForPrint($sDateSelected, 'csv');
	$eDateSelectedForPrintCSV = getDateFormatForPrint($eDateSelected, 'csv');

	if(!empty($export)){
		$reportName = get_string('overalluserprogressreport','singlecoursereport');
		$courseHTML .= getReportPrintHeader($reportName);
		$courseHTML .= '<table border="0" cellpadding="0" cellspacing="0"  class="generaltable margin_bottom">';
		$courseHTML .= '<tr>';
		$courseHTML .= '<td colspan="2"><strong>'.get_string('course','singlecoursereport').':</strong> '.$courseName.'</td>';
		$courseHTML .= '</tr>';
			
		$courseHTML .= '<td width="50%"><strong>'.get_string('lastaccesseddatefrom').':</strong> '.$sDateSelectedForPrint.'</td>';
		$courseHTML .= '<td width="50%" ><strong>'.get_string('lastaccesseddateto').':</strong>  '.$eDateSelectedForPrint.'</td>';
		$courseHTML .= '</tr>';
		$courseHTML .= '<tr>';
		$courseHTML .= '<td><strong>'.get_string('notstarted','singlecoursereport').':</strong> '.$not_started_count.' ('.floatval($notStartedPercentage).'%)</td>';
		$courseHTML .= '<td><strong>'.get_string('inprogress','singlecoursereport').':</strong> '.$in_progress_count.' ('.floatval($inProgressPercentage).'%)</td>';
		$courseHTML .= '</tr>';
		$courseHTML .= '<tr>';
		$courseHTML .= '<td ><strong>'.get_string('completed','singlecoursereport').':</strong> '.$completed_count.' ('.floatval($completedPercentage).'%)</td>';
		$courseHTML .= '<td><strong>'.get_string('numberofusers','singlecoursereport').':</strong> '.$userCount.'</td>';
		$courseHTML .= '</tr>';
		$courseHTML .= '</table>';
	}
	$courseHTML .= '<div class="f-right">'.get_string('reports_heading','',getReportSyncTime()).'</div>';
	$courseHTML .= '<div class="clear"></div>';
	if($userCount > 0){
			
		$courseHTML .= ' <div class = "single-report-start" id="watch">
						<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('overalluserprogressreport','singlecoursereport').$exportHTML.'</span>
						  <div class="single-report-graph-left" id="chartContainer">'.get_string('loading','singlecoursereport').'</div>
						  <div class="single-report-graph-right">
							<div class="course-count-heading">'.get_string('numberofusers','singlecoursereport').'</div>
							<div class="course-count">'.$userCount.'</div>
							<div class="seperator">&nbsp;</div>
							<div class="course-status">
							  <div class="notstarted"><h6>'.get_string('notstarted','singlecoursereport').'</h6><span>'.$not_started_count.'</span></div>
							  <div class="clear"></div>
							  <div class="inprogress"><h6>'.get_string('inprogress','singlecoursereport').'</h6><span>'.$in_progress_count.'</span></div>
							  <div class="clear"></div>
							  <div class="completed"><h6>'.get_string('completed','singlecoursereport').'</h6><span>'.$completed_count.'</span></div>
							</div>
						  </div>
						</div>
					  </div>
					  <div class="clear"></div>';
	}
		
		
		
		
	$courseHTML .= '<div class="">';
	$courseHTML .= '<table  class="generaltable" cellpadding="0" border="0" cellspacing="0"></tbody>';
	$courseHTML .= '<tr>';
		
	if($CFG->showInlineManagerNCompany == 1 && !empty($export) && $export == 'print'){
		$courseHTML .=  '<th width="16%">'.get_string('fullname','singlecoursereport').'</th>';
		$courseHTML .=  '<th width="15%">'.get_string('username').'</th>';
		$courseHTML .=  '<th width="13%">'.get_string('company').'</th>';
		$courseHTML .=  '<th width="13%">'.get_string('inlinemanager').'</th>';
		$courseHTML .=  '<th width="13%">'.get_string('status','singlecoursereport').'</th>';
		//$courseHTML .=  '<th width="25%">'.get_string('score','singlecoursereport').'</th>';
		$courseHTML .=  '<th width="15%">'.get_string('comletiondate').'</th>';
		$courseHTML .=  '<th width="15%">'.get_string('lastaccessed','singlecoursereport').'</th>';
	}else{
		$courseHTML .=  '<th width="23%">'.get_string('fullname','singlecoursereport').'</th>';
		$courseHTML .=  '<th width="22%">'.get_string('username').'</th>';
		$courseHTML .=  '<th width="15%">'.get_string('status','singlecoursereport').'</th>';
		//$courseHTML .=  '<th width="25%">'.get_string('score','singlecoursereport').'</th>';
		$courseHTML .=  '<th width="20%">'.get_string('comletiondate').'</th>';
		$courseHTML .=  '<th width="20%">'.get_string('lastaccessed','singlecoursereport').'</th>';
	}
		
	$courseHTML .= '</tr>';
		
		

	$reportContentCSV = '';
	$reportContentCSV .= get_string('course','singlecoursereport').",".$courseName."\n";
	$reportContentCSV .= get_string('numberofusers','singlecoursereport').",".$userCount."\n";
	$reportContentCSV .= get_string('notstarted','singlecoursereport').",".$not_started_count." (".floatval($notStartedPercentage)."%)\n";
	$reportContentCSV .= get_string('inprogress','singlecoursereport').",".$in_progress_count." (".floatval($inProgressPercentage)."%)\n";
	$reportContentCSV .= get_string('completed','singlecoursereport').",".$completed_count." (".floatval($completedPercentage)."%)\n";
		
	$reportContentCSV .= get_string('lastaccesseddatefrom').",".$sDateSelectedForPrintCSV."\n".get_string('lastaccesseddateto').",".$eDateSelectedForPrintCSV."\n";
	$reportContentCSV .= get_string('overalluserprogressreport','singlecoursereport')."\n";
	// $reportContentCSV .= get_string('fullname','singlecoursereport')." (".get_string('username')."),".get_string('status','singlecoursereport').",".get_string('score','singlecoursereport').",".get_string('lastaccessed','singlecoursereport')."\n";

	if($CFG->showInlineManagerNCompany == 1){
		$reportContentCSV .= get_string('fullname','singlecoursereport').",".get_string('username').",".get_string('company').",".get_string('inlinemanager').",".get_string('status','singlecoursereport').",".get_string('comletiondate').",".get_string('lastaccessed','singlecoursereport')."\n";
	}else{
		$reportContentCSV .= get_string('fullname','singlecoursereport').",".get_string('username').",".get_string('status','singlecoursereport').",".get_string('comletiondate').",".get_string('lastaccessed','singlecoursereport')."\n";
	}
		
	$reportContentPDF = '';
	$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0"  class="generaltable">';
	$reportContentPDF .= '<tr>';
	$reportContentPDF .= '<td colspan="2"><strong>'.get_string('course','singlecoursereport').':</strong> '.$courseName.'</td>';
	
	$reportContentPDF .= '</tr>';


	$reportContentPDF .= '<tr>';
	$reportContentPDF .= '<td><strong>'.get_string('lastaccesseddatefrom').':</strong> '.$sDateSelectedForPrint.'</td>';
	$reportContentPDF .= '<td><strong>'.get_string('lastaccesseddateto').':</strong>  '.$eDateSelectedForPrint.'</td>';
	$reportContentPDF .= '</tr>';
	$reportContentPDF .= '<tr>';
	$reportContentPDF .= '<td><strong>'.get_string('notstarted','singlecoursereport').':</strong> '.$not_started_count.' ('.floatval($notStartedPercentage).'%)</td>';
	$reportContentPDF .= '<td><strong>'.get_string('inprogress','singlecoursereport').':</strong> '.$in_progress_count.' ('.floatval($inProgressPercentage).'%)</td>';
	$reportContentPDF .= '</tr>';
	$reportContentPDF .= '<tr>';
	
	$reportContentPDF .= '<td ><strong>'.get_string('completed','singlecoursereport').':</strong> '.$completed_count.' ('.floatval($completedPercentage).'%)</td>';
	$reportContentPDF .= '<td><strong>'.get_string('numberofusers','singlecoursereport').':</strong> '.$userCount.'</td>';
	$reportContentPDF .= '</tr>';

	//$reportContentPDF .= '<tr><br /><td width="100%"  ><span '.$CFG->pdfSpanAttribute.'><strong>'.get_string('overalluserprogressreport','singlecoursereport').'</strong></span><br /></td></tr>';
	$reportContentPDF .= '</table>';

	if(!empty($export) && $export == 'exportpdf' && strstr($_SERVER['REQUEST_URI'], 'reports/course_progress_report_print.php')){
		$reportContentPDF .= getGraphImageHTML(get_string('overalluserprogressreport','singlecoursereport'));
	}else{
		$reportContentPDF .= getGraphImageHTML(get_string('viewreportdetails','multicoursereport'));
	}





	$htmlSpace = getSpace();
	$htmlSpaceCSV = getSpace('csv');
	//pr($limitReportsArr);die;
		
	$extraParams = "startDate=".$sStartDate."&endDate=".$sEndDate;
	if($userCount > 0){
			
		$curtime = time();
		$i=0;
		foreach($limitReportsArr as $data){
			$i++;
			//$classEvenOddR = $i%2==0 ?'evenR':'oddR';
			$classEvenOddR = 'oddR';
			$courseStatus = $data->course_status;
			$manager_name = '';
			if($CFG->showInlineManagerNCompany==1){
				$manager_name = $data->inline_manager_name;
				if(trim($manager_name)==''){
					$manager_name = $data->manager_name;
				}
			$inlineCompany = $data->company_name?$data->company_name:(!empty($export) && $export == 'exportcsv'?getMDashForCSV():getMDash());
			}else{
				$manager_name = $data->manager_name;
			}
			//$manager_name = '';
			$manager_nameCSV = $manager_name?$manager_name:getMDashForCSV();
			$manager_name = $manager_name?$manager_name:getMDash();
			//$courseScore = '';
			//$lastAccessed = getMDash();
				
			$completionDate = $data->completion_date;
			if($completionDate){
				$completionDateTime = getDateFormat($completionDate, $CFG->customDefaultDateTimeFormat);
				$completionDateTimeCSV = getDateFormat($completionDate, $CFG->customDefaultDateTimeFormatForCSV);

			}else{
				$completionDateTime = getMDash();
				$completionDateTimeCSV = getMDashForCSV();
			}
				
			$lastAccessedTime = $data->last_access;
			if($lastAccessedTime){
				$lastAccessed = getDateFormat($lastAccessedTime, $CFG->customDefaultDateTimeFormat);
				$lastAccessedCSV = getDateFormat($lastAccessedTime, $CFG->customDefaultDateTimeFormatForCSV);

			}else{
				$lastAccessed = get_string('notaccessed','learnercourse');
				$lastAccessedCSV = get_string('notaccessed','learnercourse');
			}

			$userId = $data->user_id;
			$fullName = $data->fullname;
			$userName = $data->user_name;
			//$imagevalue = $OUTPUT->user_picture($user, array('size'=>64));

			$courseHTML .=  '<tr class="'.$classEvenOddR.'" >';
			if(!empty($export)){
				$courseHTML .=  '<td>'.$fullName.'</td>';
				$courseHTML .=  '<td>'.$userName.'</td>';
			}else{
				if(!$sBack){
					$sBack = 3;
				}
				$userDetailsLink = $CFG->wwwroot."/reports/user_performance_report.php?uid=".$userId."&cid=".$cid."&back=".$sBack.'&'.$extraParams;
				$courseHTML .=  '<td><a href="'.$userDetailsLink.'">'.$fullName.'</a></td>';
				$courseHTML .=  '<td><a href="'.$userDetailsLink.'" >'.$userName.'</a></td>';
			}
				
			if($CFG->showInlineManagerNCompany == 1 && !empty($export) && $export == 'print' ){
				$courseHTML .=  '<td>'.$inlineCompany.'</td>';
				$courseHTML .=  '<td>'.$manager_name.'</td>';
			}
			$courseHTML .=  '<td>'.$courseStatus.'</td>';
			//$courseHTML .=  '<td>'.($courseScore?$courseScore:getMDash()).'</td>';
			$courseHTML .=  '<td>'.$completionDateTime.'</td>';
			$courseHTML .=  '<td>'.$lastAccessed.'</td>';
			$courseHTML .=  '</tr>';

			//$courseHTML .=  '<tr><td colspan="5">';
				
			//$reportContentCSV .= $fullName." (".$userName."),".$courseStatus.",".($courseScore?$courseScore:getMDashForCSV()).",".(getMDashForCSV())."\n";
				
			if($CFG->showInlineManagerNCompany == 1){
				$reportContentCSV .= $fullName.",".$userName.",".$inlineCompany.",".$manager_nameCSV.",".$courseStatus.",".$completionDateTimeCSV.",".$lastAccessedCSV."\n";
			}else{
				$reportContentCSV .= $fullName.",".$userName.",".$courseStatus.",".$completionDateTimeCSV.",".$lastAccessedCSV."\n";
			}
			

			$assestHTML = '';
			$courseHTML .=  $assestHTML;
			// $courseHTML .= '</td></tr>';

		}


		//if($notStartedPercentage || $inProgressPercentage || $completedPercentage){
		$courseHTML .=  '<script language="javascript" type="text/javascript">';
		$courseHTML .=  '	$(document).ready(function(){

								$("#printbun").bind("click", function(e) {
									var url = $(this).attr("rel");
									window.open(url, "'.get_string('coursevsmultipleuserreport','singlecoursereport').'", "'.$CFG->printWindowParameter.'");
								});

						     }); ';


		$courseHTML .=  ' window.onload = function () {
			
										var chart = new CanvasJS.Chart("chartContainer",
										{
											title:{
												text: ""
											},
											theme: "theme2",
						       
											data: [
											{
												type: "doughnut",
												indexLabelFontFamily: "Arial",
												indexLabelFontSize: 12,
												startAngle:0,
												indexLabelFontColor: "dimgrey",
												indexLabelLineColor: "darkgrey",
												toolTipContent: "{y}%",
					
											
												dataPoints: [
												{  y: '.$inProgressPercentage.', label: "'.get_string('inprogress','singlereport').' ('.$in_progress_count.')", exploded:"'.$in_progress_count_exploded.'" },
												{  y: '.$notStartedPercentage.', label: "'.get_string('notstarted','singlereport').' ('.$not_started_count.')", exploded:"'.$not_started_count_exploded.'" },
												{  y: '.$completedPercentage.', label: "'.get_string('completed','singlereport').' ('.$completed_count.')", exploded:"'.$completed_count_exploded.'" },
					
												]

												/*dataPoints: [
												{  y: '.$notStartedPercentage.', label: "'.get_string('notstarted','singlereport').' {y}%" },
												{  y: '.$inProgressPercentage.', label: "'.get_string('inprogress','singlereport').' {y}%" },
												{  y: '.$completedPercentage.', label: "'.get_string('completed','singlereport').' {y}%" },
					
												]*/
											}
											]
						
										});';
			
		if($notStartedPercentage || $inProgressPercentage || $completedPercentage){
			$courseHTML .=  '	chart.render(); ';
		}
			
		$courseHTML .=  '  }
									</script>
									<script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js"></script>';
			
		//}

	}else{
		$courseHTML .=  '<tr><td colspan="5" align="center" >'.get_string('no_results').'</td></tr>';
		$reportContentCSV .= get_string('norecordfound','singlecoursereport')."\n";
		$reportContentPDF .= get_string('norecordfound','singlecoursereport')."\n";
	}
		
	$reportContentPDF .= '</table>';
		
	$courseHTML .= '</tbody></table></div></div></div></div>';

	if(empty($export)){
			
		$courseHTML .= paging_bar($userCount, $page, $perpage, $genURL);
		$styleSheet = $userCount?'':'style=""';
		if(strstr($_SERVER['REQUEST_URI'], 'course/coursereportdetails.php')){
				
			if($sBack == 1){
				$backUrl = $CFG->wwwroot.'/course/course_report.php';
			}else{
				$backUrl = $CFG->wwwroot.'/course/multicoursereport.php?'.$extraParams;
			}
				
			//$courseHTML .= '<div id="backcell"><input type = "button" value = "'.get_string('back','multicoursereport').'" '.$styleSheet.' onclick="location.href=\''.$backUrl.'\';"></div>';
		}else{
			$urlCancel = new moodle_url($CFG->wwwroot.'/course/courseview.php', array('id'=>$cid));
			//$courseHTML .= '<div id="backcell"><input type = "button" value = "'.get_string('cancel','singlecoursereport').'" '.$styleSheet.' onclick="location.href=\''.$urlCancel.'\';"></div>';
		}
	}
		
	$courseHTML .= '';
	$courseHTML .=  '';

	$courseVsMultipleUserReport->courseHTML = $courseHTML;
	$courseVsMultipleUserReport->reportContentCSV = $reportContentCSV;
	$courseVsMultipleUserReport->reportContentPDF = $reportContentPDF;
		
	return $courseVsMultipleUserReport;
}

function getCourseEnrolmentCount($courseId,$where){
	global $DB;
	//$query = "SELECT course_status,count(distinct(mrlpd.user_id)) as course_status_count FROM mdl_report_learner_performance mrlp LEFT JOIN mdl_report_learner_performance_details mrlpd on (mrlp.user_id=mrlpd.user_id)";
	$query = "SELECT course_status,count(distinct(mrlpd.user_id)) as course_status_count FROM mdl_report_learner_performance mrlp LEFT JOIN mdl_report_learner_performance_details mrlpd on (mrlp.user_id=mrlpd.user_id) left join mdl_user as mu on mrlpd.user_id=mu.id";
        
	$query .= " WHERE 1 = 1";
	$query .= " AND mrlpd.course_id = '".$courseId."' ";
	$query .= $where;
	
	$query .= " GROUP BY mrlpd.course_status ";
	$query .= " ORDER BY COALESCE(NULLIF(mrlp.first_name, ''), mrlp.last_name), mrlp.last_name ";
	$enrolmentCount = $DB->get_records_sql($query);
	return $enrolmentCount;
}

function getOnlineCourseUsageData($paramArray,$page,$perpage,$field = ''){
	GLOBAL $CFG,$DB,$USER;
	$sCourseArr = explode("@",$paramArray['course']);
	$sProgramArr = explode("@",$paramArray['program']);
	$sDepartmentArr = explode("@",$paramArray['department']);
	$sTeamArr = explode("@",$paramArray['team']);
	$sTypeArr = explode("@",$paramArray['type']);
	$sUserGroup	= explode("@",$paramArray['user_group']);
	
	$userstatus=explode("@",$paramArray['userstatus']);
	$sCountryArr = explode("@",$paramArray['country']);
        
        $CityIdArr = explode("@",$paramArray['city']);
      // pr($CityIdArr);die;
         $isManagerYes=explode("@",$paramArray['is_manager_yes']);
       
        
	$repTypeArr = $CFG->courseStatusArr;
	$sTypeArr2 = str_replace(array(3, 1, 2), $repTypeArr, $sTypeArr); 
	$sTypeName = implode(", ", $sTypeArr2);
	$where = "";
	$blankArray = array('-1','',0);
	if(isset($paramArray['sel_mode']) && $paramArray['sel_mode'] == 2){
		$where .= "AND (NOT FIND_IN_SET(0,global_groupid)) AND global_groupid IS NOT NULL AND global_groupid != '' ";
	}
	
	if(count($isManagerYes) && !in_array('-1',$isManagerYes))
        {
           $where .= "AND mdl_user.is_manager_yes in(".@implode(',',$isManagerYes).") ";
        }
        
        if(count($userstatus) && !in_array('-1',$userstatus))
        {
           $where .= "AND mdl_user.suspended in(".@implode(',',$userstatus).") ";
        }
        
        if(count($sCountryArr) && !in_array('-1',$sCountryArr))
        {
            
            array_walk($sCountryArr,function (&$val){ $val="'".$val."'";});
            $countries = @implode(",",$sCountryArr);
           
           $where .= "AND mdl_user.country IN(".$countries.") ";
        }
        
        $ConvertCityIdToNameArr = ConvertCityIdToNameArr($CityIdArr);
        if($ConvertCityIdToNameArr){
           
            $sCity = implode(',',$ConvertCityIdToNameArr);
            $where .= "AND mdl_user.city IN(".$sCity.") ";
        }
        
        if($paramArray['report_to'] != '' && $paramArray['report_to'] != '-1' && count($paramArray['report_to']) > 0){
			 $id_explode = explode("@",$paramArray['report_to']);
			 $List = getLeadersNameList();

			 $leaders_name = '';
			 foreach($id_explode as $id){
							 $leaders_name .= '"'.$List[$id].'",';
			 }
			 $leaders_name = trim($leaders_name,',');
			 $where .= " AND mdl_user.report_to IN ($leaders_name)";
        }
        
        if($paramArray['job_title'] != '' && $paramArray['job_title'] != '-1' && $paramArray['job_title'] != 0 && !in_array('-1',$paramArray['job_title']) && !in_array('0', $paramArray['job_title'])) {
		$jobIds = count($paramArray['job_title']) > 0 ?implode(",", explode("@",$paramArray['job_title'])):0;
		$where .= " AND mdl_user.job_title IN ($jobIds)";
	}
        
	//echo $where;die;
	if(!empty($sDepartmentArr) && !in_array($sDepartmentArr[0],$blankArray)){
		$sDepartmentStringArr = array();
		foreach($sDepartmentArr as $sDepartmentVal){
		  $sDepartmentStringArr[] = " FIND_IN_SET($sDepartmentVal,r.user_department) ";
		}
		if(count($sDepartmentStringArr) > 0 ){
		   $sDepartmentString = implode(" OR ", $sDepartmentStringArr);
		   $where .= "AND ($sDepartmentString) ";
		}
	}
	if($USER->archetype == $CFG->userTypeManager){
		$groupOwnerTeamIds = fetchGroupsList();
		if(!empty($groupOwnerTeamIds)){
			$where .= "AND (FIND_IN_SET(".$USER->department.",r.user_department) ";
			$TeamSearchStringArr = array();
			foreach($groupOwnerTeamIds as $sTeamVal){
			  $TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,global_groupid) ";
			  $TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,groupid) ";
			}
			if(count($TeamSearchStringArr) > 0 ){
			   $TeamSearchString = implode(" OR ", $TeamSearchStringArr);
			   $where .= "OR ($TeamSearchString) ";
			}
			$where .= " )";
		}else{
			$where .= "AND (FIND_IN_SET(".$USER->department.",r.user_department)) ";
		}
		$where .= "AND user_department = ".$USER->department." ";
	}elseif($USER->archetype == $CFG->userTypeStudent){
		$groupOwnerTeamIds = fetchGroupsList();
		$TeamSearchStringArr = array();
		foreach($groupOwnerTeamIds as $sTeamVal){
			$TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,global_groupid) ";
			$TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,groupid) ";
		}
		if(count($TeamSearchStringArr) > 0 ){
		   $TeamSearchString = implode(" OR ", $TeamSearchStringArr);
		   $where .= "AND ($TeamSearchString) ";
		}
	}
	if(count($sTeamArr) > 0 &&  !in_array($sTeamArr[0] ,$blankArray)){
		$TeamSearchStringArr = array();
		foreach($sTeamArr as $sTeamVal){
		  $TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,groupid) ";
		}
		if(count($TeamSearchStringArr) > 0 ){
		   $TeamSearchString = implode(" OR ", $TeamSearchStringArr);
		   $where .= "AND ($TeamSearchString) ";
		}
	}
	if(isset($paramArray['sel_mode']) && $paramArray['sel_mode'] == 2){
		if(count($sUserGroup) > 0 &&  !in_array($sUserGroup[0] ,$blankArray)){
			$TeamSearchStringArr = array();
			foreach($sUserGroup as $sTeamVal){
			  $TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,global_groupid) ";
			}
			if(count($TeamSearchStringArr) > 0 ){
			   $TeamSearchString = implode(" OR ", $TeamSearchStringArr);
			   $where .= "AND ($TeamSearchString) ";
			}
		}
	}
	if(!empty($sProgramArr) && !in_array($sProgramArr[0],$blankArray)){
		$sProgramArrStringArr = array();
		foreach($sProgramArr as $sDepartmentVal){
		  $sProgramArrStringArr[] = " FIND_IN_SET($sDepartmentVal,programid) ";
		}
		if(count($sProgramArrStringArr) > 0 ){
		   $sProgramArrString = implode(" OR ", $sProgramArrStringArr);
		   $where .= "AND ($sProgramArrString) ";
		}
	}
	if(!empty($sCourseArr) && !in_array($sCourseArr[0],$blankArray)){
	   $sCourseArrString = implode(",", $sCourseArr);
	   $where .= "AND course_id IN ($sCourseArrString) ";
	}
	$typeSearch = '';
	if($field != 'count_graph' && !empty($sTypeArr) && !in_array($sTypeArr[0],$blankArray)){
		$typeSearchArray = array();
		foreach($sTypeArr as $sType){
			if($sType == 3){
				$typeSearchArray[] = "not_started > 0";
			}elseif($sType == 2){
				$typeSearchArray[] = "completed > 0";
			}elseif($sType == 1){
				$typeSearchArray[] = "in_progress > 0";
			}
		}
		if(count($typeSearchArray) > 0 ){
		   $typeSearchString = implode(" OR ", $typeSearchArray);
		   $typeSearch .= "WHERE ($typeSearchString) ";
		}
	}
        
        $paramArray['key'] = addslashes($paramArray['key']);
        if(($paramArray['key'] != '')) {
		$where .= " AND (fullname like '%".$paramArray['key']."%' ) ";
	}
	if($where != ''){
		$where = ' WHERE '.LTRIM($where,'AND');
	}
	
	$limit = '';
	if($field == '*'){
		$fullnameField = extractMDashInSql('fullname');
		
		//$sql = 'select * from (SELECT ##SELECT_FIELDS## FROM mdl_online_course_usage_details '.$where.' GROUP BY course_id ORDER BY fullname ASC) tableA '.$typeSearch.'';
		//$sql = str_replace('##SELECT_FIELDS##',"id,course_id,".$fullnameField." as fullname,SUM(not_started) as not_started,SUM(in_progress) as in_progress,SUM(completed) as completed,department,groupid,programid",$sql);
		
		$sql = 'select * from (SELECT ##SELECT_FIELDS## FROM mdl_online_course_usage_details as r join mdl_user on r.user_id=mdl_user.id ' . $where . ' GROUP BY course_id ORDER BY fullname ASC) tableA ' . $typeSearch . '';
        $sql = str_replace('##SELECT_FIELDS##', "r.id,course_id," . $fullnameField . " as fullname,mdl_user.city,mdl_user.country,mdl_user.state,SUM(not_started) as not_started,SUM(in_progress) as in_progress,SUM(completed) as completed,r.department,r.groupid,programid", $sql);
        //echo $sql;die;
		if($perpage != 0){
			$offset = $page - 1;
			$offset = $offset*$perpage;
			$sql .= " LIMIT $offset, $perpage";
		}	
		//echo $sql;die;
		$data = $DB->get_records_sql($sql);
	}else{
		
		//$sql = 'SELECT count(*) as total_records,SUM(not_started) as not_started,SUM(in_progress) as in_progress,SUM(completed) as completed FROM ( SELECT id,r.course_id,r.fullname,SUM(r.not_started) as not_started,SUM(r.in_progress) as in_progress,SUM(r.completed) as completed ,SUM(r.blank_status) as blank_status FROM mdl_online_course_usage_details r '.$where.' GROUP BY r.course_id ) AS z';
		$sql = 'SELECT count(*) as total_records,SUM(not_started) as not_started,SUM(in_progress) as in_progress,SUM(completed) as completed FROM ( SELECT r.id,r.course_id,r.fullname,SUM(r.not_started) as not_started,SUM(r.in_progress) as in_progress,SUM(r.completed) as completed ,SUM(r.blank_status) as blank_status FROM mdl_online_course_usage_details r join mdl_user on r.user_id=mdl_user.id '.$where.' GROUP BY r.course_id ) AS z';
        //echo $sql;die;
		$data = $DB->get_record_sql($sql);
		$data->total_enrollment = $data->not_started + $data->in_progress + $data->completed;
	}
       
	return $data;
}
function onlineCourseUsageExport($paramArray,$page,$perpage,$action){
	GLOBAL $CFG,$DB,$USER;
	$sDepartmentArr = explode("@",$paramArray['department']);
	$sTeamArr = explode("@",$paramArray['team']);
	$sUserArr = explode("@",$paramArray['user']);
	$sTypeArr = explode("@",$paramArray['type']);
	$sUserGroup	= explode("@",$paramArray['user_group']);
	$sProgramArr = explode("@",$paramArray['program']);
	$sCourseArr = explode("@",$paramArray['course']);
	$blankArray = array('-1','',0);
	if(empty($sTypeArr) || in_array($sTypeArr[0],$blankArray)){
	  $sTypeName = get_string('all','multiuserreport');
	}else{
	  $repTypeArr = $CFG->courseStatusArr;
	  $sTypeArr2 = str_replace(array(3, 1, 2), $repTypeArr, $sTypeArr); 
	  $sTypeName = implode(", ", $sTypeArr2);
	}
	$sDepartmentName = (empty($sDepartmentArr) || in_array($sDepartmentArr[0],$blankArray))?(get_string('all','multiuserreport')):getDepartments($sDepartmentArr, true);
	$sTeamName = (empty($sTeamArr) || in_array($sTeamArr[0],$blankArray))?(get_string('all','multiuserreport')):getTeams($sTeamArr);
	$sUserFullName = (empty($sUserArr) || in_array($sUserArr[0],$blankArray))?(get_string('all','multiuserreport')):getUsers($sUserArr);
	$sUserGroupName = (empty($sUserGroup) || in_array($sUserGroup[0],$blankArray))?(get_string('all','multiuserreport')):getUserGroup($sUserGroup);
	$sCourseName = (empty($sCourseArr) || in_array($sCourseArr[0],$blankArray))?(get_string('all','multicoursereport')):getCourses($sCourseArr);
	$sProgramName = (empty($sProgramArr) || in_array($sProgramArr[0],$blankArray))?(get_string('all','multicoursereport')):getPrograms($sProgramArr);
	if($USER->archetype != $CFG->userTypeAdmin ){
		$sDepartmentName = getDepartments($USER->department, $isReport);
	}
	if($action == 'print'){
		
		$enrollentCount = getOnlineCourseUsageData($paramArray,$page,0,'count_graph');
		//pr($enrollentCount);die;
		$notStartedPercentage = 0;
		$inProgressPercentage = 0;
		$completedPercentage = 0;
		
		if($enrollentCount->total_enrollment != 0){
		//	echo $enrollentCount->completed*100;die;
			$notStartedPercentage = floatval(($enrollentCount->not_started*100)/$enrollentCount->total_enrollment);
			$inProgressPercentage = floatval(($enrollentCount->in_progress*100)/$enrollentCount->total_enrollment);
			$completedPercentage = floatval(($enrollentCount->completed*100)/$enrollentCount->total_enrollment);
		}
		//echo $notStartedPercentage;die;
		$reportName = get_string('courseusagesreport','multicoursereport');
		$courseHTML .= getReportPrintHeader($reportName);

		$courseHTML .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="generaltable margin_bottom">';
		$courseHTML .= '<tr>';
		if($paramArray['sel_mode'] == 2){
			$courseHTML .= '<td colspan = "2" ><strong>'.get_string('global_team').':</strong> '.$sUserGroupName.'</td>';
		}else{
			$courseHTML .= '<td><strong>'.get_string('department','multicoursereport').':</strong> '.$sDepartmentName.'</td>';
			$courseHTML .= '<td><strong>'.get_string('team','multicoursereport').':</strong>  '.$sTeamName.'</td>';
		}
		$courseHTML .= '</tr>';
		$courseHTML .= '<tr>';
		$courseHTML .= '<td width="500"><strong>'.get_string('program','multicoursereport').':</strong> '.$sProgramName.'</td>';
		$courseHTML .= '<td width="500"><strong>'.get_string('course','multicoursereport').':</strong> '.$sCourseName.'</td>';
		$courseHTML .= '</tr>';
		$courseHTML .= '<tr>';
		$courseHTML .= '<td colspan="2"><strong>'.get_string('statusreport').':</strong> '.$sTypeName.'</td>';
		$courseHTML .= '</tr>';
		$courseHTML .= '<tr>';
		$courseHTML .= '<td><strong>'.get_string('numberofusers','multicoursereport').':</strong> '.$enrollentCount->total_enrollment.'</td>';
		$courseHTML .= '<td><strong>'.get_string('notstarted','multicoursereport').':</strong>  '.$enrollentCount->not_started.' ('.round($notStartedPercentage,2).'%)</td>';
		$courseHTML .= '</tr>';
		
		$courseHTML .= '<tr>';
		$courseHTML .= '<td><strong>'.get_string('inprogress','multicoursereport').':</strong>  '.$enrollentCount->in_progress.' ('.round($inProgressPercentage,2).'%)</td>';				
		$courseHTML .= '<td><strong>'.get_string('completed','multicoursereport').':</strong>  '.$enrollentCount->completed.' ('.round($completedPercentage,2).'%)</td>';
		$courseHTML .= '</tr>';
		
		$courseHTML .= '</table>';
		return $courseHTML;
	}elseif($action == 'exportpdf'){
		$enrollentCount = getOnlineCourseUsageData($paramArray,$page,0,'count_graph');
		$notStartedPercentage = 0;
		$inProgressPercentage = 0;
		$completedPercentage = 0;
		if($enrollentCount->total_enrollment != 0){
			$notStartedPercentage = floatval(($enrollentCount->not_started*100)/$enrollentCount->total_enrollment);
			$inProgressPercentage = floatval(($enrollentCount->in_progress*100)/$enrollentCount->total_enrollment);
			$completedPercentage = floatval(($enrollentCount->completed*100)/$enrollentCount->total_enrollment);
		}
		$reportContentPDF = '';
		$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="generaltable">';
		$reportContentPDF .= '<tr>';
		if($paramArray['sel_mode'] == 2){
			$reportContentPDF .= '<td colspan = "2" ><strong>'.get_string('global_team').':</strong> '.$sUserGroupName.'</td>';
		}else{
			$reportContentPDF .= '<td><strong>'.get_string('department','multicoursereport').':</strong> '.$sDepartmentName.'</td>';
			$reportContentPDF .= '<td><strong>'.get_string('team','multicoursereport').':</strong>  '.$sTeamName.'</td>';
		}
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td><strong>'.get_string('program','multicoursereport').':</strong> '.$sProgramName.'</td>';
		$reportContentPDF .= '<td><strong>'.get_string('course','multicoursereport').':</strong> '.$sCourseName.'</td>';
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td colspan="2"><strong>'.get_string('statusreport').':</strong> '.$sTypeName.'</td>';
		$reportContentPDF .= '</tr>';				
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td><strong>'.get_string('numberofusers','multicoursereport').':</strong> '.$enrollentCount->total_enrollment.'</td>';
		$reportContentPDF .= '<td><strong>'.get_string('notstarted','multicoursereport').':</strong>  '.$enrollentCount->not_started.' ('.round($notStartedPercentage,2).'%)</td>';				
		$reportContentPDF .= '</tr>';

		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td><strong>'.get_string('inprogress','multicoursereport').':</strong>  '.$enrollentCount->in_progress.' ('.round($inProgressPercentage,2).'%)</td>';				
		$reportContentPDF .= '<td><strong>'.get_string('completed','multicoursereport').':</strong>  '.$enrollentCount->completed.' ('.round($completedPercentage,2).'%)</td>';							
		$reportContentPDF .= '</tr>';

		$reportContentPDF .= '</table>';

		$reportContentPDF .= getGraphImageHTML(get_string('courseusagesreport','multicoursereport'));

		$filename = str_replace(' ', '_', get_string('courseusagesreport','multicoursereport'))."_".date("m-d-Y").".pdf";
		exportPDF($filename, $reportContentPDF, '', get_string('courseusagesreport','multicoursereport'));
	}elseif($action == 'exportcsv'){
		$enrollentCount = getOnlineCourseUsageData($paramArray,$page,0,'count_graph');
		$notStartedPercentage = 0;
		$inProgressPercentage = 0;
		$completedPercentage = 0;
		if($enrollentCount->total_enrollment != 0){
			$notStartedPercentage = floatval(($enrollentCount->not_started*100)/$enrollentCount->total_enrollment);
			$inProgressPercentage = floatval(($enrollentCount->in_progress*100)/$enrollentCount->total_enrollment);
			$completedPercentage = floatval(($enrollentCount->completed*100)/$enrollentCount->total_enrollment);
		}
		$reportContentCSV = '';
		if($paramArray['sel_mode'] == 2){
			$reportContentCSV .= get_string('global_team').",".$sUserGroupName."\n";
		}else{
			$reportContentCSV .= get_string('department','multicoursereport').",".$sDepartmentName."\n".get_string('team','multicoursereport').",".$sTeamName ."\n";
		}							  
		$reportContentCSV .= get_string('program','multicoursereport').",".$sProgramName."\n".get_string('course','multicoursereport').",".$sCourseName ."\n";
		$reportContentCSV .= get_string('statusreport').",".$sTypeName ."\n";
		$reportContentCSV .= get_string('numberofusers','multicoursereport').",".$enrollentCount->total_enrollment."\n";
		$reportContentCSV .= get_string('notstarted','multicoursereport').",".$enrollentCount->not_started." (".round($notStartedPercentage,2)."%)\n";
		$reportContentCSV .= get_string('inprogress','multicoursereport').",".$enrollentCount->in_progress." (".round($inProgressPercentage,2)."%)\n";
		$reportContentCSV .= get_string('completed','multicoursereport').",".$enrollentCount->completed." (".round($completedPercentage,2)."%)\n";
		
		$reportContentCSV .= get_string('courseusagesreport','multicoursereport')."\n";
		$reportContentCSV .= get_string('coursetitle','multicoursereport').",".get_string('enrolled','multicoursereport').",".get_string('notstarted','multicoursereport').",".get_string('inprogress','multicoursereport').",".get_string('completed','multicoursereport')."\n";
		$enrolledData = getOnlineCourseUsageData($paramArray,1,0,'*');
                
                $setDataorder = setdisplayorderofdata($enrolledData,'course_id');
		if(!empty($enrolledData)){
			foreach($enrolledData as $enrolled){
				$enrolled->enrolled = $enrolled->not_started + $enrolled->in_progress + $enrolled->completed;
				$reportContentCSV .= str_replace(',',' ',$enrolled->fullname).",".$enrolled->enrolled.",".$enrolled->not_started.",".$enrolled->in_progress.",".$enrolled->completed."\n";
			}
		}else{
			$reportContentCSV .= get_string('norecordfound','multicoursereport')."\n";
		}
		$filepath = $CFG->dirroot."/local/reportexport/temp";		
		chmod($filepath, 0777);		
		$filename = str_replace(' ', '_', get_string('courseusagesreport','multicoursereport'))."_".date("m-d-Y").".csv";  
		$filepathname = $filepath.'/'.$filename;
		unlink($filepathname);
		$handler = fopen($filepathname, "w");
		fwrite($handler, $reportContentCSV);
   	    exportCSV($filepathname);
	}
}


///Credit Hours Report

function getOverallCreditHours($paramArray,  $page, $perpage, $field = ''){
	global $DB, $CFG,$USER;
	$isReport = true;
	$blankArray = array('-1','',0);
	$userCHReport = new stdClass();
		
       
	$pageURL = $_SERVER['PHP_SELF'];
        
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
		

	$linkToDetailsURL = genParameterizedURL(array_merge($paramArray, array('uid'=>':userId','back'=>1)), $removeKeyArray, '/user/user_course_credithours_report.php');
		
	$actionUrl = '/reports/credithours_report_print.php';
	$genActionURL = genParameterizedURL($paramArray, $removeKeyArray, $actionUrl);
	
	$sDepartmentArr = explode("@",$paramArray['department']);	
	$sTeamArr = explode("@",$paramArray['team']);
	
	$sUserArr = explode("@",$paramArray['user']);
	$sUserTypeArr = explode("@",$paramArray['userType']);
	$sJobTitleArr = explode("@",$paramArray['job_title']);
	$sCompanyArr = explode("@",$paramArray['company']);
	$sUserGroupArr	= explode("@",$paramArray['user_group']);	
	$sTypeArr = explode("@",$paramArray['type']);	
	$sStartDate     = $paramArray['startDate'];
	$sEndDate       = $paramArray['endDate'];
        
        $sIsManagerYes=$paramArray['is_manager_yes'];
        $sIsManagerYesArr=explode("@",$sIsManagerYes);
		
	$where = "";
	if(!empty($sDepartmentArr) && !in_array($sDepartmentArr[0],$blankArray)){
		$where .= " AND departmentid IN (".implode(',',$sDepartmentArr).") ";
	}

	if($USER->archetype == $CFG->userTypeManager){
		$groupOwnerTeamIds = fetchGroupsList();
		if(!empty($groupOwnerTeamIds)){
			$where .= " AND (departmentid = ".$USER->department." ";
			$TeamSearchStringArr = array();
			foreach($groupOwnerTeamIds as $sTeamVal){
				$TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,groupid) ";
			}
			if(count($TeamSearchStringArr) > 0 ){
				$TeamSearchString = implode(" OR ", $TeamSearchStringArr);
				$where .= " OR ($TeamSearchString) ";
			}
			$where .= " )";
		}else{
			$where .= " AND departmentid = ".$USER->department;
		}
	}
	elseif($USER->archetype == $CFG->userTypeStudent){
		$groupOwnerTeamIds = fetchGroupsList();
		$TeamSearchStringArr = array();
		foreach($groupOwnerTeamIds as $sTeamVal){
			$TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,groupid) ";
		}
		if(count($TeamSearchStringArr) > 0 ){
			$TeamSearchString = implode(" OR ", $TeamSearchStringArr);
			$where .= "AND ($TeamSearchString) ";
		}
	}
	
	if($sUserTypeArr[0] != '-1'){
		$sUserTypeStr = array();
		foreach($sUserTypeArr as $sUserT){
			$sUserTypeStr[] = "user_type IN ('".$sUserT."') ";
			
		}
		if(count($sUserTypeStr)>0){
			$sUserTypeSearchStr =  implode(" OR ", $sUserTypeStr);
			$where .= " AND ($sUserTypeSearchStr) ";
		}		
	}
	if(!empty($sUserArr) && !in_array($sUserArr[0],$blankArray)){
		$where .= " AND user_id IN (".implode(',',$sUserArr).") ";
	}
	if(!empty($sJobTitleArr) && !in_array($sJobTitleArr[0],$blankArray)){
		$where .= " AND jobid IN (".implode(',',$sJobTitleArr).") ";
	}
        
        if($paramArray['report_to'] != '' && $paramArray['report_to'] != '-1' && count($paramArray['report_to']) > 0){
			 $id_explode = explode("@",$paramArray['report_to']);
			 $List = getLeadersNameList();

			 $leaders_name = '';
			 foreach($id_explode as $id){
                                $leaders_name .= '"'.$List[$id].'",';
			 }
			 $leaders_name = trim($leaders_name,',');
			 $where .= " AND mu.report_to IN ($leaders_name)";
        }
	if(!empty($sCompanyArr) && !in_array($sCompanyArr[0],$blankArray)){
		$where .= " AND comid IN (".implode(',',$sCompanyArr).") ";
	}
	
	if(count($sIsManagerYesArr) && !in_array('-1',$sIsManagerYesArr))
        {
           $where .= " AND mu.is_manager_yes in(".@implode(',',$sIsManagerYesArr).") ";
        }
        
	if(count($sTeamArr) > 0 &&  !in_array($sTeamArr[0] ,$blankArray)){
		$TeamSearchStringArr = array();
		foreach($sTeamArr as $sTeamVal){
			$TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,groupid) ";
		}
		if(count($TeamSearchStringArr) > 0 ){
			$TeamSearchString = implode(" OR ", $TeamSearchStringArr);
			$where .= " AND ($TeamSearchString) ";
		}
	}
	if($_REQUEST['sel_mode']==2){
		$where .= " AND (NOT FIND_IN_SET(0,global_groupid)) AND global_groupid IS NOT NULL AND global_groupid != '' ";
	}
	if(count($sUserGroupArr) > 0 &&  !in_array($sUserGroupArr[0] ,$blankArray)){
		$TeamSearchStringArr = array();
		foreach($sUserGroupArr as $sUserGroupVal){
			$TeamSearchStringArr[] = " FIND_IN_SET($sUserGroupVal,global_groupid) ";
		}
		if(count($TeamSearchStringArr) > 0 ){
			$TeamSearchString = implode(" OR ", $TeamSearchStringArr);
			$where .= " AND ($TeamSearchString) ";
		}
	}
	
	$sDateSelected = '';
	if($sStartDate){
		$sDateArr = getFormattedTimeStampOfDate($sStartDate);
		$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';
	
	}
	
	$eDateSelected = '';
	if($sEndDate){
		$eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
		$eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
	}
	
	if($sDateSelected){
		$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
		$where .= " AND (complition_time >= ($sStartDateTime) || (complition_time is null))";
	}
	
	if($eDateSelected){
		$sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
		$where .= " AND (complition_time <= ($sEndDateTime) || (complition_time is null))";
	}
        
        $paramArray['key'] = addslashes($paramArray['key']);
         if(($paramArray['key'] != '')) {
             $where .= " AND (mrcch.firstname like '%".$paramArray['key']."%' OR mrcch.lastname like '%".$paramArray['key']."%' OR mrcch.userfullname like '%".$paramArray['key']."%' OR mrcch.username like '%".$paramArray['key']."%' OR mrcch.departmenttitle like '%".$paramArray['key']."%' OR mrcch.usermanager like '%".$paramArray['key']."%' ) ";
         }
         
	if($where != ''){
		//echo $where;
		$where = ' WHERE '.LTRIM(trim($where),'AND');
		//echo $where;die;
	}
	////dd
	$sql = "SELECT 	##SELECT_FIELDS## FROM mdl_report_course_credit_hours mrcch LEFT JOIN mdl_user mu ON(mrcch.user_id=mu.id) ";
	$sql .= $where;
      
	if($field=='graph_data'){
         //echo $where;
		$graphRec = getOverallCreditHoursForGraph($where);
		return $graphRec;
	}
	
        
	if($field == '*'){
		$sql = str_replace('##SELECT_FIELDS##',"mrcch.user_id,
			mrcch.username,
                        mrcch.job_title,
			mrcch.userfullname,
                        mu.report_to,
                        mu.is_manager_yes,
			mrcch.departmenttitle,
			mrcch.usermanager,
			mrcch.inline_manager_name,
			COUNT(distinct CASE WHEN mrcch.coursetype_id = 1 THEN mrcch.courseid END) as online_course,
			COUNT(distinct CASE WHEN mrcch.coursetype_id = 2 THEN mrcch.courseid END) as classroom_course,
			COUNT(distinct mrcch.courseid) as total_course,
			sum(CASE WHEN mrcch.coursetype_id = 1 THEN mrcch.credithours END) as online_credit_hours,
			sum(CASE WHEN mrcch.coursetype_id = 2 THEN mrcch.credithours END) as classroom_credit_hours,
			sum(mrcch.credithours) as total_credit_hours",$sql);
		$sql .= "GROUP BY mrcch.user_id
			ORDER BY mrcch.userfullname";
		if($perpage != 0){
			$offset = $page - 1;
			$offset = $offset*$perpage;
			$sql .= " LIMIT $offset, $perpage";
		}
		
		$data = $DB->get_records_sql($sql);
		
	}else{
		$sql = str_replace('##SELECT_FIELDS##','count(distinct user_id) as total_records',$sql);
		
		$data = $DB->get_record_sql($sql);
	}
	return $data;
	

}



function getOverallUserCreditHours($paramArray,  $page, $perpage, $field = ''){
	global $DB, $CFG,$USER;
	$isReport = true;
	$blankArray = array('-1','',0);
	$userCHReport = new stdClass();

	$pageURL = $_SERVER['PHP_SELF'];
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);


	$linkToDetailsURL = genParameterizedURL(array_merge($paramArray, array('uid'=>':userId','back'=>1)), $removeKeyArray, '/user/user_course_credithours_report.php');

	$actionUrl = '/reports/credithours_report_print.php';
	$genActionURL = genParameterizedURL($paramArray, $removeKeyArray, $actionUrl);

	$sDepartmentArr = explode("@",$paramArray['department']);
	$sTeamArr = explode("@",$paramArray['team']);

	$sUserArr = explode("@",$paramArray['user']);
	$sUserTypeArr = explode("@",$paramArray['userType']);
	$sUserGroupArr	= explode("@",$paramArray['user_group']);
	$sTypeArr = explode("@",$paramArray['type']);
	$sStartDate     = $paramArray['startDate'];
	$sEndDate       = $paramArray['endDate'];
	$userId    = $paramArray['uid'];
	$isUserDeleted = isUserDeleted($userId);
	if($isUserDeleted){
		redirect("/");
	}


	$sJobTitleName = $sJobTitle=='-1'?(get_string('all','multiuserreport')):getJobTitles($sJobTitleArr);
	$sCompanyName = $sCompany=='-1'?(get_string('all','multiuserreport')):getCompany($sCompanyArr);

	$where = "";
	if(!empty($sDepartmentArr) && !in_array($sDepartmentArr[0],$blankArray)){
		$where .= " AND departmentid IN (".implode(',',$sDepartmentArr).") ";
	}

	if($USER->archetype == $CFG->userTypeManager){
		$groupOwnerTeamIds = fetchGroupsList();
		if(!empty($groupOwnerTeamIds)){
			$where .= " AND (departmentid = ".$USER->department." ";
			$TeamSearchStringArr = array();
			foreach($groupOwnerTeamIds as $sTeamVal){
				$TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,groupid) ";
			}
			if(count($TeamSearchStringArr) > 0 ){
				$TeamSearchString = implode(" OR ", $TeamSearchStringArr);
				$where .= " OR ($TeamSearchString) ";
			}
			$where .= " )";
		}else{
			$where .= " AND departmentid = ".$USER->department;
		}
	}

	if($sUserTypeArr[0] != '-1'){
		$sUserTypeStr = array();
		foreach($sUserTypeArr as $sUserT){
			$sUserTypeStr[] = "user_type IN ('".$sUserT."') ";
				
		}
		if(count($sUserTypeStr)>0){
			$sUserTypeSearchStr =  implode(" OR ", $sUserTypeStr);
			$where .= " AND ($sUserTypeSearchStr) ";
		}
	}
	if(!empty($sUserArr) && !in_array($sUserArr[0],$blankArray)){
		$where .= " AND user_id IN (".implode(',',$sUserArr).") ";
	}

	if(count($sTeamArr) > 0 &&  !in_array($sTeamArr[0] ,$blankArray)){
		$TeamSearchStringArr = array();
		foreach($sTeamArr as $sTeamVal){
			$TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,groupid) ";
		}
		if(count($TeamSearchStringArr) > 0 ){
			$TeamSearchString = implode(" OR ", $TeamSearchStringArr);
			$where .= " AND ($TeamSearchString) ";
		}
	}
	if(count($sUserGroupArr) > 0 &&  !in_array($sUserGroupArr[0] ,$blankArray)){
		$TeamSearchStringArr = array();
		foreach($sUserGroupArr as $sUserGroupVal){
			$TeamSearchStringArr[] = " FIND_IN_SET($sUserGroupVal,groupid) ";
		}
		if(count($TeamSearchStringArr) > 0 ){
			$TeamSearchString = implode(" OR ", $TeamSearchStringArr);
			$where .= " AND ($TeamSearchString) ";
		}
	}

	$sDateSelected = '';
	if($sStartDate){
		$sDateArr = getFormattedTimeStampOfDate($sStartDate);
		$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

	}

	$eDateSelected = '';
	if($sEndDate){
		$eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
		$eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
	}

	if($sDateSelected){
		$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
		$where .= " AND (complition_time >= ($sStartDateTime) || (complition_time is null))";
	}

	if($eDateSelected){
		$sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
		$where .= " AND (complition_time <= ($sEndDateTime) || (complition_time is null))";
	}
	if($userId){
		$where .= " AND user_id='$userId'";
	}
        
        $paramArray['key'] = addslashes($paramArray['key']);
        if(($paramArray['key'] != '')) {
		$where .= " AND (mrcch.coursename like '%".$paramArray['key']."%' ) ";
	}
	if($where != ''){
		//echo $where;
		$where = ' WHERE '.LTRIM(trim($where),'AND');
		//echo $where;die;
	}
	////dd
	$sql = "SELECT 	##SELECT_FIELDS## FROM mdl_report_course_credit_hours mrcch  ";
	$sql .= $where;
	if($field=='graph_data'){
		$graphRec = getOverallCreditHoursForGraph($where);
		return $graphRec;
	}
        
	if($field == '*'){
		$sql = str_replace('##SELECT_FIELDS##',"if(mrcch.class_id=0,mrcch.coursename,concat(mrcch.coursename,' - ',mrcch.class_name)) as coursefullname,mrcch.courseid,mrcch.coursename,
			if(mrcch.coursetype_id=1,'online course','Classroom Course') as course_type,
			sum(mrcch.credithours) as credithours,mrcch.complition_time",$sql);
		$sql .= "GROUP BY mrcch.courseid,mrcch.class_id
			ORDER BY mrcch.coursename";
		if($perpage != 0){
			$offset = $page - 1;
			$offset = $offset*$perpage;
			$sql .= " LIMIT $offset, $perpage";
		}
		//echo $sql;die;
		$data = $DB->get_records_sql($sql);

	}else{
		$sql = str_replace('##SELECT_FIELDS##','count(distinct courseid) as total_records',$sql);

		$data = $DB->get_record_sql($sql);
	}
	return $data;


}
function creditHourReportExport($paramArray,$page,$perpage,$action,$headerLabelGraph){
	GLOBAL $CFG,$DB,$USER;
	

	$sDepartmentArr = explode("@",$paramArray['department']);
	$sTeamArr = explode("@",$paramArray['team']);
	
	$sUserArr = explode("@",$paramArray['user']);
	$sUserTypeArr = explode("@",$paramArray['userType']);
	$sJobTitleArr = explode("@",$paramArray['job_title']);
	$sCompanyArr = explode("@",$paramArray['company']);
	$sUserGroupArr	= explode("@",$paramArray['user_group']);
	$sTypeArr = explode("@",$paramArray['type']);
	$sStartDate     = $paramArray['startDate'];
	$sEndDate       = $paramArray['endDate'];
        
        $sIsManagerYes=$paramArray['is_manager_yes'];
        $sIsManagerYesArr=explode("@",$sIsManagerYes);
        
        $sAdManagersName = getAdManagersNonManagersList($sIsManagerYesArr);
        
	$blankArray = array('-1','',0);
	if($sUserTypeArr[0] == '-1' || count($sUserTypeArr) > 2 ){
		$sUserTypeName = get_string('all','multiuserreport');
	}else{
		$sUserTypeName = implode(", ",array_map('ucfirst', $sUserTypeArr));
	}
	$sDateSelected = '';
	if($sStartDate){
		$sDateArr = getFormattedTimeStampOfDate($sStartDate);
		$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';
	
	}
	if($sEndDate){
		$eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
		$eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
	}
	if($sDateSelected){
		$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
	}
	if($eDateSelected){
		$sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
	}
        $sleaders_name = '';
        if($paramArray['report_to'] != '' && $paramArray['report_to'] != '-1' && count($paramArray['report_to']) > 0){
			 $id_explode = explode("@",$paramArray['report_to']);
			 $List = getLeadersNameList();			
			 foreach($id_explode as $id){
                                $sleaders_name .= $List[$id].' | ';
			 }
			 $sleaders_name = trim($sleaders_name,' | ');
			
        }
	if(!$sleaders_name){
            $sleaders_name = get_string('all','multiuserreport');
        }
        
	$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
	$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
	$sDateSelectedForPrintCSV = getDateFormatForPrint($sDateSelected, 'csv');
	$eDateSelectedForPrintCSV = getDateFormatForPrint($eDateSelected, 'csv');
	
	$sJobTitleName = (empty($sJobTitleArr) || in_array($sJobTitleArr[0],$blankArray))?(get_string('all','multiuserreport')):getJobTitles($sJobTitleArr);
	$sCompanyName = (empty($sCompanyArr) || in_array($sCompanyArr[0],$blankArray))?(get_string('all','multiuserreport')):getCompany($sCompanyArr);
	$sUserGroupName = (empty($sUserGroupArr) || in_array($sUserGroupArr[0],$blankArray))?(get_string('all','multiuserreport')):getUserGroup($sUserGroupArr);
	
	$sDepartmentName = (empty($sDepartmentArr) || in_array($sDepartmentArr[0],$blankArray))?(get_string('all','multiuserreport')):getDepartments($sDepartmentArr);
	$sTeamName = (empty($sTeamArr) || in_array($sTeamArr[0],$blankArray))?(get_string('all','multiuserreport')):getTeams($sTeamArr);
	$sUserFullName = (empty($sUserArr) || in_array($sUserArr[0],$blankArray))?(get_string('all','multiuserreport')):getUsers($sUserArr);
	//$sUserGroupName = (empty($sUserGroup) || in_array($sUserGroup[0],$blankArray))?(get_string('all','multiuserreport')):getUserGroup($sUserGroup);
	$sCourseName = (empty($sCourseArr) || in_array($sCourseArr[0],$blankArray))?(get_string('all','multicoursereport')):getCourses($sCourseArr);
	$sProgramName = (empty($sProgramArr) || in_array($sProgramArr[0],$blankArray))?(get_string('all','multicoursereport')):getPrograms($sProgramArr);
	if($USER->archetype != $CFG->userTypeAdmin ){
		$sDepartmentName = getDepartments($USER->department, $isReport);
	}
	$creditHourDataGraphData = getOverallCreditHours($paramArray,  $page, 0, 'graph_data');
	$onlineCourseCreditHour = $creditHourDataGraphData[1]->credit_hours;
	$classroomCourseCreditHour = $creditHourDataGraphData[2]->credit_hours;
	$totalCreditHours = $onlineCourseCreditHour + $classroomCourseCreditHour;
	$OCCreditHourPercentage =  numberFormat(($onlineCourseCreditHour*100)/$totalCreditHours);
	$CCCreditHourPercentage = numberFormat(($classroomCourseCreditHour*100)/$totalCreditHours);
	if($action == 'print'){
		
		$reportName = $headerLabelGraph;
		$userHTML .= getReportPrintHeader($reportName);
		
		$userHTML .= '<table border="0" cellpadding="0" cellspacing="0" class="generaltable margin_bottom" >';
		
		if($_REQUEST["sel_mode"] == 2){
			$userHTML .= '<tr>';
			$userHTML .= '<td><strong>'.get_string('usertype').':</strong> '.$sUserTypeName.'</td>';
			$userHTML .= '<td><strong>'.get_string('user','multiuserreport').':</strong> '.$sUserFullName.'</td>';
			$userHTML .= '</tr>';
		
			$userHTML .= '<tr>';
			$userHTML .= '<td colspan="2"><strong>'.get_string('global_team').':</strong> '.$sUserGroupName.'</td>';
			$userHTML .= '</tr>';
		
		}else{
				
			$userHTML .= '<tr>';
			$userHTML .= '<td><strong>'.get_string('usertype').':</strong> '.$sUserTypeName.'</td>';
			$userHTML .= '<td colspan="2"><strong>'.get_string('user','multiuserreport').':</strong> '.$sUserFullName.'</td>';
			$userHTML .= '</tr>';
		
			$userHTML .= '<tr>';
			$userHTML .= '<td><strong>'.get_string('department','multiuserreport').':</strong> '.$sDepartmentName.'</td>';
			$userHTML .= '<td><strong>'.get_string('team','multiuserreport').':</strong>  '.$sTeamName.'</td>';
			$userHTML .= '</tr>';
				
		}
		
		if($CFG->showJobTitle == 1 && $CFG->showCompany == 1){
			$userHTML .= '<tr>';
			$userHTML .= '<td ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
			$userHTML .= '<td><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
			$userHTML .= '</tr>';
		}elseif($CFG->showJobTitle == 1 && $CFG->showCompany == 0){
			$userHTML .= '<tr>';
			$userHTML .= '<td colspan="2" ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
			$userHTML .= '</tr>';
		}elseif($CFG->showJobTitle == 0 && $CFG->showCompany == 1){
			$userHTML .= '<tr>';
			$userHTML .= '<td colspan="2" ><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
			$userHTML .= '</tr>';
		}
                
                if($CFG->showReportTo == 1){
			$userHTML .= '<tr>';
			$userHTML .= '<td colspan="2" ><strong>'.get_string('report_to').':</strong> '.$sleaders_name.'</td>';
			$userHTML .= '</tr>';
		}
		$userHTML .= '<tr>';
                $userHTML .= '<td colspan="2" ><strong>'.get_string('ismanageryescsv').':</strong> '.$sAdManagersName.'</td>';
                $userHTML .= '</tr>';	
			
		$userHTML .= '<tr>';
		$userHTML .= '<td><strong>'.get_string('credithourscomletiondate').':</strong> '.$sDateSelectedForPrint.'</td>';
		$userHTML .= '<td><strong>'.get_string('credithourscomletiondateto').':</strong>  '.$eDateSelectedForPrint.'</td>';
		$userHTML .= '</tr>';
		
		if(in_array($CFG->userTypeStudent, $sUserTypeArr) || ($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2)){
			
			$userHTML .= '<tr>';
			$userHTML .= '<td><strong>'.get_string('coursecredithourswithtime').':</strong> '.(setCreditHoursFormat($totalCreditHours)).'</td>';
			$userHTML .= '<td><strong>'.get_string('onlinecourse').':</strong> '.setCreditHoursFormat($onlineCourseCreditHour).' ('.floatval($OCCreditHourPercentage).'%)</td>';
			$userHTML .= '</tr>';
			$userHTML .= '<tr>';
			$userHTML .= '<td colspan="2"><strong>'.get_string('classroomcourse').':</strong> '.setCreditHoursFormat($classroomCourseCreditHour).' ('.floatval($CCCreditHourPercentage).'%)</td>'; 					 
			$userHTML .= '</tr>';
				
		}else{
			$userHTML .= get_string('classroomcourse').",".setCreditHoursFormat($classroomCourseCreditHour)." (".floatval($CCCreditHourPercentage)."%)\n";
			$userHTML .= '<tr>';
			$userHTML .= '<td><strong>'.get_string('coursecredithourswithtime').':</strong> '.(setCreditHoursFormat($totalCreditHours)).'</td>';
			$userHTML .= '<td><strong>'.get_string('classroomcourse').':</strong> '.setCreditHoursFormat($classroomCourseCreditHour).' ('.floatval($CCCreditHourPercentage).'%)</td>';
			$userHTML .= '</tr>';				
		}
		$userHTML .= '</table>';
		//$userHTML .= '<span '.$CFG->pdfSpanAttribute.'>&nbsp;<br /><strong>'.get_string('userreport','multiuserreport').'</strong></span><br /><br />';
		
		return $userHTML;
	}else{		
		$reportContentPDF = '';
		$reportContentCSV = '';
			    $reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0"  class="generaltable">';
			    $reportContentCSV .= $headerLabelGraph."\n";
			    if($_REQUEST["sel_mode"] == 2){
			    	$reportContentCSV .= get_string('usertype').",".$sUserTypeName."\n";
			    	$reportContentCSV .= get_string('global_team').",".$sUserGroupName."\n";
			    	$reportContentCSV .= get_string('user','multiuserreport').",".$sUserFullName."\n";
					$reportContentPDF .= '<tr>';
					    $reportContentPDF .= '<td><strong>'.get_string('usertype').':</strong> '.$sUserTypeName.'</td>';
						$reportContentPDF .= '<td ><strong>'.get_string('user','multiuserreport').':</strong> '.$sUserFullName.'</td>';
					$reportContentPDF .= '</tr>';
					
				   $reportContentPDF .= '<tr>';
				    $reportContentPDF .= '<td colspan="2" ><strong>'.get_string('global_team').':</strong> '.$sUserGroupName.'</td>';
				   $reportContentPDF .= '</tr>';
				 
			    }else{
			    	$reportContentCSV .= get_string('usertype').",".$sUserTypeName."\n";
			    	$reportContentCSV .= get_string('department','multiuserreport').",".$sDepartmentName."\n".get_string('team','multiuserreport').",".$sTeamName ."\n";
			    	$reportContentCSV .= get_string('user','multiuserreport').",".$sUserFullName."\n";
					 $reportContentPDF .= '<tr>';
					    $reportContentPDF .= '<td><strong>'.get_string('usertype').':</strong> '.$sUserTypeName.'</td>';
						$reportContentPDF .= '<td ><strong>'.get_string('user','multiuserreport').':</strong> '.$sUserFullName.'</td>';
					 $reportContentPDF .= '</tr>';
					
					 $reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td><strong>'.get_string('department','multiuserreport').':</strong> '.$sDepartmentName.'</td>';
						$reportContentPDF .= '<td><strong>'.get_string('team','multiuserreport').':</strong>  '.$sTeamName.'</td>';
					 $reportContentPDF .= '</tr>';
			    }
						   
				
				if($CFG->showJobTitle == 1 && $CFG->showCompany == 1){
					$reportContentCSV .= get_string('job_title','user').",".$sJobTitleName."\n";
					$reportContentCSV .= get_string('company','user').",".$sCompanyName."\n";
					$reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
						$reportContentPDF .= '<td><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
					$reportContentPDF .= '</tr>';
				}elseif($CFG->showJobTitle == 1 && $CFG->showCompany == 0){
					$reportContentCSV .= get_string('job_title','user').",".$sJobTitleName."\n";
					$reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td colspan="2" ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
					$reportContentPDF .= '</tr>';
				}elseif($CFG->showJobTitle == 0 && $CFG->showCompany == 1){
					$reportContentCSV .= get_string('company','user').",".$sCompanyName."\n";
					$reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td colspan="2" ><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
					$reportContentPDF .= '</tr>';
				}
				if($CFG->showReportTo == 1){
					$reportContentCSV .= get_string('report_to').",".$sleaders_name ."\n";
					$reportContentPDF .= '<tr>';
                                        $reportContentPDF .= '<td colspan="2" ><strong>'.get_string('report_to').':</strong> '.$sleaders_name .'</td>';
					$reportContentPDF .= '</tr>';
				}
									 
                                $reportContentCSV .= get_string('ismanageryescsv').",".$sAdManagersName ."\n";
                                 
				$reportContentCSV .= get_string('credithourscomletiondate').",".$sDateSelectedForPrintCSV."\n".get_string('credithourscomletiondateto').",".$eDateSelectedForPrintCSV."\n";
				
				$reportContentCSV .= get_string('coursecredithourswithtime').",".(setCreditHoursFormat($totalCreditHours))."\n";
						
				$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td  width="50%"  ><strong>'.get_string('credithourscomletiondate').':</strong> '.$sDateSelectedForPrint.'</td>';
					$reportContentPDF .= '<td  width="50%"  ><strong>'.get_string('credithourscomletiondateto').':</strong>  '.$eDateSelectedForPrint.'</td>';	
				$reportContentPDF .= '</tr>';
				
							
				 if(in_array($CFG->userTypeStudent, $sUserTypeArr) || ($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2)){
				 	$reportContentCSV .= get_string('onlinecourse').",".setCreditHoursFormat($onlineCourseCreditHour)." (".floatval($OCCreditHourPercentage)."%)\n";
				 	$reportContentCSV .= get_string('classroomcourse').",".setCreditHoursFormat($classroomCourseCreditHour)." (".floatval($CCCreditHourPercentage)."%)\n";
					$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td><strong>'.get_string('coursecredithourswithtime').':</strong> '.(setCreditHoursFormat($totalCreditHours)).'</td>'; 	
					$reportContentPDF .= '<td><strong>'.get_string('onlinecourse').':</strong> '.setCreditHoursFormat($onlineCourseCreditHour).' ('.floatval($OCCreditHourPercentage).'%)</td>';
					$reportContentPDF .= '</tr>';
					$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td colsapn="2" ><strong>'.get_string('classroomcourse').':</strong> '.setCreditHoursFormat($classroomCourseCreditHour).' ('.floatval($CCCreditHourPercentage).'%)</td>'; 					 $reportContentPDF .= '</tr>';
					
				}else{	
					$reportContentCSV .= get_string('classroomcourse').",".setCreditHoursFormat($classroomCourseCreditHour)." (".floatval($CCCreditHourPercentage)."%)\n";
					$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td><strong>'.get_string('coursecredithourswithtime').':</strong> '.(setCreditHoursFormat($totalCreditHours)).'</td>'; 	
					$reportContentPDF .= '<td><strong>'.get_string('classroomcourse').':</strong> '.setCreditHoursFormat($classroomCourseCreditHour).' ('.floatval($CCCreditHourPercentage).'%)</td>';
					$reportContentPDF .= '</tr>';
			
				}	
				
				if( $totalCreditHours == 0){
				  $reportContentPDF .= '<tr><br /><td width="100%"  ><span '.$CFG->pdfSpanAttribute.'><strong>'.$headerLabelGraph.'</strong></span><br /></td></tr>';
				}
				
				$reportContentPDF .= '</table>';
				$creditHourData = getOverallCreditHours($paramArray,  $page, 0, '*');
				//pr($creditHourData);die;
				if(in_array($CFG->userTypeStudent, $sUserTypeArr) || ($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2)){
				
					$reportContentCSV .= get_string('fullname','multiuserreport').','.get_string('username').','.get_string('jobtitle').','.get_string('report_to').','.get_string('ismanageryescsv').','.get_string('onlinecourse').','.get_string('classroomcourse').','.get_string('total').",".get_string('onlinecourse').','.get_string('classroomcourse').','.get_string('total').','.get_string('manager','multiuserreport').','.get_string('department','multiuserreport')."\n";
					
				}else{
					
					$reportContentCSV .= get_string('fullname','multiuserreport').','.get_string('username').','.get_string('jobtitle').','.get_string('report_to').','.get_string('ismanageryescsv').','.get_string('noofcourses').','.get_string('coursecredithourswithtime').','.get_string('manager','multiuserreport').','.get_string('department','multiuserreport')."\n";
					
				}
				//pr($creditHourData);die;
				if(count($creditHourData)>0){
					foreach($creditHourData as $crData){
						//pr($crData);die;
						$manager_name = '';
						if($CFG->showInlineManagerNCompany==1){
							$manager_name = $crData->inline_manager_name;
							if(trim($manager_name)==''){
								$manager_name = $crData->usermanager;
							}
						}else{
							$manager_name = $crData->usermanager;
						}
						$managerName = ($manager_name)?$manager_name:getMDashForCSV();
                                                $crData->is_manager_yes = $CFG->isManagerYesOptions[$crData->is_manager_yes];
                                                
                                                $crData->job_title = ($crData->job_title)?$crData->job_title:getMDashForCSV();
                                                 $crData->report_to = ($crData->report_to)?$crData->report_to:getMDashForCSV();
						if(in_array($CFG->userTypeStudent, $sUserTypeArr) || ($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2)){
							$reportContentCSV .=  $crData->userfullname.',';
							$reportContentCSV .=  $crData->username.',';
                                                        $reportContentCSV .=  str_replace(',',' | ',$crData->job_title).',';
                                                        $reportContentCSV .=  $crData->report_to.',';
                                                        $reportContentCSV .=  $crData->is_manager_yes.',';
							$reportContentCSV .= $crData->online_course.',';
							$reportContentCSV .= $crData->classroom_course.',';
							$reportContentCSV .= $crData->total_course.',';
							$reportContentCSV .= setCreditHoursFormat($crData->online_credit_hours).',';
							$reportContentCSV .= setCreditHoursFormat($crData->classroom_credit_hours).',';
							$reportContentCSV .= setCreditHoursFormat($crData->total_credit_hours).',';
							$reportContentCSV .=  $managerName.',';
							$reportContentCSV .=  $crData->departmenttitle;
							$reportContentCSV .=  "\n";
						}
						else{
							$reportContentCSV .=  $crData->userfullname.',';
							$reportContentCSV .=  $crData->username.',';
                                                        $reportContentCSV .=  str_replace(',',' | ',$crData->job_title).',';
                                                        $reportContentCSV .=  $crData->report_to.',';
                                                        $reportContentCSV .=  $crData->is_manager_yes.',';
							$reportContentCSV .= $crData->classroom_course.',';													
							$reportContentCSV .= setCreditHoursFormat($crData->classroom_credit_hours).',';							
							$reportContentCSV .= $managerName.',';
							$reportContentCSV .=  $crData->departmenttitle;
							$reportContentCSV .=  "\n";
							
						}
					}
				}
				//echo $reportContentCSV;die;
		$reportContentPDF .= getGraphImageHTML($headerLabelGraph);
		if($action=='exportcsv'){
			$filepath = $CFG->dirroot."/local/reportexport/temp";
			chmod($filepath, 0777);
			$filename = str_replace(' ', '_', $headerLabelGraph)."_".date("m-d-Y").".csv";
			$filepathname = $filepath.'/'.$filename;
			unlink($filepathname);
			$handler = fopen($filepathname, "w");
			fwrite($handler, $reportContentCSV);
			exportCSV($filepathname);
		}else{
			$filename = str_replace(' ', '_',$headerLabelGraph)."_".date("m-d-Y").".pdf";
			exportPDF($filename, $reportContentPDF, '', $headerLabelGraph);
		}
		
	}
	
}

function usercreditHourReportExport($paramArray,$page,$perpage,$action,$headerLabelGraph){
	GLOBAL $CFG,$DB,$USER;



	$sUserArr = explode("@",$paramArray['user']);
	$sUserTypeArr = explode("@",$paramArray['userType']);
	$sJobTitleArr = explode("@",$paramArray['job_title']);
	$sCompanyArr = explode("@",$paramArray['company']);
	$sUserGroupArr	= explode("@",$paramArray['user_group']);
	$sTypeArr = explode("@",$paramArray['type']);
	$sStartDate     = $paramArray['startDate'];
	$sEndDate       = $paramArray['endDate'];
	$userId    = $paramArray['uid'];
	$userName = getUsers($userId, 1);
	$blankArray = array('-1','',0);
	
	if($sUserTypeArr[0] == '-1' || count($sUserTypeArr) > 2 ){
		$sUserTypeName = get_string('all','multiuserreport');
	}else{
		$sUserTypeName = implode(", ",array_map('ucfirst', $sUserTypeArr));
	}
	$sDateSelected = '';
	if($sStartDate){
		$sDateArr = getFormattedTimeStampOfDate($sStartDate);
		$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

	}
	if($sEndDate){
		$eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
		$eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
	}
	if($sDateSelected){
		$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
	}
	if($eDateSelected){
		$sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
	}

	$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
	$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
	//echo $sDateSelected;die;
	$sDateSelectedForPrintCSV = getDateFormatForPrint($sDateSelected, 'csv');
	$eDateSelectedForPrintCSV = getDateFormatForPrint($eDateSelected, 'csv');

	$sJobTitleName = (empty($sJobTitleArr) || in_array($sJobTitleArr[0],$blankArray))?(get_string('all','multiuserreport')):getJobTitles($sJobTitleArr);
	$sCompanyName = (empty($sCompanyArr) || in_array($sCompanyArr[0],$blankArray))?(get_string('all','multiuserreport')):getCompany($sCompanyArr);
	$sUserGroupName = (empty($sUserGroupArr) || in_array($sUserGroupArr[0],$blankArray))?(get_string('all','multiuserreport')):getUserGroup($sUserGroupArr);

	$sDepartmentName = (empty($sDepartmentArr) || in_array($sDepartmentArr[0],$blankArray))?(get_string('all','multiuserreport')):getDepartments($sDepartmentArr);
	$sTeamName = (empty($sTeamArr) || in_array($sTeamArr[0],$blankArray))?(get_string('all','multiuserreport')):getTeams($sTeamArr);
	$sUserFullName = (empty($sUserArr) || in_array($sUserArr[0],$blankArray))?(get_string('all','multiuserreport')):getUsers($sUserArr);
	//$sUserGroupName = (empty($sUserGroup) || in_array($sUserGroup[0],$blankArray))?(get_string('all','multiuserreport')):getUserGroup($sUserGroup);
	$sCourseName = (empty($sCourseArr) || in_array($sCourseArr[0],$blankArray))?(get_string('all','multicoursereport')):getCourses($sCourseArr);
	$sProgramName = (empty($sProgramArr) || in_array($sProgramArr[0],$blankArray))?(get_string('all','multicoursereport')):getPrograms($sProgramArr);
	if($USER->archetype != $CFG->userTypeAdmin ){
		$sDepartmentName = getDepartments($USER->department, $isReport);
	}
	$creditHourDataGraphData = getOverallUserCreditHours($paramArray,  $page, 0, 'graph_data');
	//pr($creditHourDataGraphData);die;
	$onlineCourseCreditHour = $creditHourDataGraphData[1]->credit_hours;
	$classroomCourseCreditHour = $creditHourDataGraphData[2]->credit_hours;
	$totalCreditHours = $onlineCourseCreditHour + $classroomCourseCreditHour;
	$OCCreditHourPercentage =  numberFormat(($onlineCourseCreditHour*100)/$totalCreditHours);
	$CCCreditHourPercentage = numberFormat(($classroomCourseCreditHour*100)/$totalCreditHours);
	if($action == 'print'){

		$reportName = $headerLabelGraph;
		$HTML .= getReportPrintHeader($reportName);
		$HTML .= '<table border="0" cellpadding="0" cellspacing="0"  class="generaltable margin_bottom">';
		$HTML .= '<tr>';
		$HTML .= '<td><strong>'.get_string('user','singlereport').':</strong> '.$userName.'</td>';
		$HTML .= '<td><strong>'.get_string('usertype').':</strong> '.$sUserTypeName.'</td>';
		$HTML .= '</tr>';
		
		$HTML .= '<tr>';
		$HTML .= '<td  width="500" ><strong>'.get_string('credithourscomletiondate').':</strong> '.$sDateSelectedForPrint.'</td>';
		$HTML .= '<td  width="500" ><strong>'.get_string('credithourscomletiondateto').':</strong>  '.$eDateSelectedForPrint.'</td>';
		$HTML .= '</tr>';
		if(in_array($CFG->userTypeStudent, $sUserTypeArr) || ($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2)){
			$HTML .= '<tr>';
			$HTML .= '<td><strong>'.get_string('coursecredithourswithtime').':</strong> '.(setCreditHoursFormat($totalCreditHours)).'</td>';
			$HTML .= '<td><strong>'.get_string('onlinecourse').':</strong> '.setCreditHoursFormat($onlineCourseCreditHour).' ('.floatval($OCCreditHourPercentage).'%)</td>';
			$HTML .= '</tr>';
			$HTML .= '<tr>';
			$HTML .= '<td colspan="2" ><strong>'.get_string('classroomcourse').':</strong> '.setCreditHoursFormat($classroomCourseCreditHour).' ('.floatval($CCCreditHourPercentage).'%)</td>';
			$HTML .= '</tr>';
			
		}else{
			$HTML .= '<tr>';
			$HTML .= '<td><strong>'.get_string('coursecredithourswithtime').':</strong> '.(setCreditHoursFormat($totalCreditHours)).'</td>';
			$HTML .= '<td><strong>'.get_string('classroomcourse').':</strong> '.setCreditHoursFormat($classroomCourseCreditHour).' ('.floatval($CCCreditHourPercentage).'%)</td>';
			$HTML .= '</tr>';
		
		}
		$HTML .= '</table>';
		//$userHTML .= '<span '.$CFG->pdfSpanAttribute.'>&nbsp;<br /><strong>'.get_string('userreport','multiuserreport').'</strong></span><br /><br />';

		return $HTML;
	}else{
		
		
		$reportContentPDF = '';
		$reportContentCSV = '';
		
		$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0"  class="generaltable">';
		$reportContentCSV .= $headerLabelGraph."\n";
		
		$reportContentCSV .= get_string('user','singlereport').",".$userName."\n";
		$reportContentCSV .= get_string('usertype').",".$sUserTypeName."\n";
		$reportContentCSV .= get_string('credithourscomletiondate').",".$sDateSelectedForPrintCSV."\n".get_string('credithourscomletiondateto').",".$eDateSelectedForPrintCSV."\n";

		$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" class="generaltable"><tr><td colspan="4"></td></tr>';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td width="50%" ><strong>'.get_string('user','singlereport').':</strong> '.$userName.'</td>';
		$reportContentPDF .= '<td width="50%" ><strong>'.get_string('usertype').':</strong> '.$sUserTypeName.'</td>';
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td  width="50%" ><strong>'.get_string('credithourscomletiondate').':</strong> '.$sDateSelectedForPrint.'</td>';
		$reportContentPDF .= '<td  width="50%" ><strong>'.get_string('credithourscomletiondateto').':</strong>  '.$eDateSelectedForPrint.'</td>';
		$reportContentPDF .= '</tr>';
		$reportContentCSV .= get_string('coursecredithourswithtime').",".(setCreditHoursFormat($totalCreditHours))."\n";
		
		if(in_array($CFG->userTypeStudent, $sUserTypeArr) || ($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2)){
			$reportContentPDF .= '<tr>';
			$reportContentPDF .= '<td><strong>'.get_string('coursecredithourswithtime').':</strong> '.(setCreditHoursFormat($totalCreditHours)).'</td>';
			$reportContentPDF .= '<td><strong>'.get_string('onlinecourse').':</strong> '.setCreditHoursFormat($onlineCourseCreditHour).' ('.floatval($OCCreditHourPercentage).'%)</td>';
			$reportContentPDF .= '</tr>';
			$reportContentPDF .= '<tr>';
			$reportContentPDF .= '<td colspan="2" ><strong>'.get_string('classroomcourse').':</strong> '.setCreditHoursFormat($classroomCourseCreditHour).' ('.floatval($CCCreditHourPercentage).'%)</td>';
			$reportContentPDF .= '</tr>';
			$reportContentCSV .= get_string('onlinecourse').",".setCreditHoursFormat($onlineCourseCreditHour)." (".floatval($OCCreditHourPercentage)."%)\n";
			$reportContentCSV .= get_string('classroomcourse').",".setCreditHoursFormat($classroomCourseCreditHour)." (".floatval($CCCreditHourPercentage)."%)\n";
		}else{
			$reportContentPDF .= '<tr>';
			$reportContentPDF .= '<td><strong>'.get_string('coursecredithourswithtime').':</strong> '.(setCreditHoursFormat($totalCreditHours)).'</td>';
			$reportContentPDF .= '<td><strong>'.get_string('classroomcourse').':</strong> '.setCreditHoursFormat($classroomCourseCreditHour).' ('.floatval($CCCreditHourPercentage).'%)</td>';
			$reportContentPDF .= '</tr>';
			$reportContentCSV .= get_string('classroomcourse').",".setCreditHoursFormat($classroomCourseCreditHour)." (".floatval($CCCreditHourPercentage)."%)\n";
		}
		$reportContentPDF .= '</table>';
		$creditHourData = getOverallUserCreditHours($paramArray,  $page, 0, '*');
		$reportContentCSV .= get_string("coursetitle","singlereport").','.get_string("coursecredithourswithtime").','.get_string('coursetype','course').','.get_string("completiondate")."\n";
		if(count($creditHourData)>0){
			foreach($creditHourData as $crData){
				$reportContentCSV .=  $crData->coursefullname.','.setCreditHoursFormat($crData->credithours).','.$crData->course_type.','.getDateFormatForPrint(getDateFormat($crData->complition_time, $CFG->customDefaultDateFormat),'csv')."\n";				
		
			}
		}
		else{
		
			$userHTML .= '<tr><td colspan = "4">'.get_string('no_results').'</td></tr>';
		
		}
		//echo $reportContentCSV;die;
		$reportContentPDF .= getGraphImageHTML($headerLabelGraph);
		if($action=='exportcsv'){
			$filepath = $CFG->dirroot."/local/reportexport/temp";
			chmod($filepath, 0777);
			$filename = str_replace(' ', '_', $headerLabelGraph)."_".date("m-d-Y").".csv";
			$filepathname = $filepath.'/'.$filename;
			unlink($filepathname);
			$handler = fopen($filepathname, "w");
			fwrite($handler, $reportContentCSV);
			exportCSV($filepathname);
		}else{
			$filename = str_replace(' ', '_',$headerLabelGraph)."_".date("m-d-Y").".pdf";
			exportPDF($filename, $reportContentPDF, '', $headerLabelGraph);
		}
	}

}

function getOverallCreditHoursForUser($userId=81,$paramArray,$page,$perpage){
	global $DB,$CFG,$USER;
        
        
	$sql = "SELECT mrcch.courseid,mrcch.coursename,
			if(mrcch.coursetype_id=1,'online course','Classroom Course') as course_type,
			mrcch.credithours,mrcch.complition_time
			FROM mdl_report_course_credit_hours mrcch
			where mrcch.user_id='".$userId."'
			order by mrcch.coursename";
	$creditHoursData = $DB->get_records_sql($sql);
        
        
	return $creditHoursData;
}

function getOverallCreditHoursForGraph($where){
	global $DB,$CFG,$USER;
	$sql = "select mrcch.coursetype_id,sum(mrcch.credithours) as credit_hours from mdl_report_course_credit_hours mrcch LEFT JOIN mdl_user mu ON(mrcch.user_id=mu.id)";
	$sql .= $where;
	$sql .= " group by mrcch.coursetype_id";
		
	$creditHoursData = $DB->get_records_sql($sql);
	return $creditHoursData;
}
function getClassroomUsageReport($paramArray,$page,$perpage,$fields = ''){
	GLOBAL $CFG,$DB,$USER;
	$where = "";
	$sStartDate = $paramArray['startDate'];
	$sEndDate = $paramArray['endDate'];
	$sCourseArr = explode("@",$paramArray['course']);
	$sclassesArr = explode("@",$paramArray['classes']);
	$sTypeArr = explode("@",$paramArray['type']);
	$blankArray = array('-1','',0);
	$blankArrayType = array('-1','');
	$sDateSelected = '';
	if($sStartDate){
		$sDateArr = getFormattedTimeStampOfDate($sStartDate);
		$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

	}		   
	
	$eDateSelected = '';
	if($sEndDate){
	   $eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
	   $eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
	}
	if($sDateSelected){
		$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
		$where .= "AND class_start >= ".$sStartDateTime." ";
	}
	if($eDateSelected){
        $sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
		$where .= "AND class_end <= ".$sEndDateTime." ";
	}
	if(!empty($sCourseArr) && !in_array($sCourseArr[0],$blankArray)){
		$where .= "AND classroom_id IN (".implode(',',$sCourseArr).") ";
	}
	if(!empty($sclassesArr) && !in_array($sclassesArr[0],$blankArray)){
		$where .= "AND class_id IN (".implode(',',$sclassesArr).") ";
	}
	if($fields != 'count_graph' && !empty($sTypeArr) && !in_array($sTypeArr[0],$blankArrayType)){
		$typeSearchArray = array();
		foreach($sTypeArr as $sType){
			if($sType == 3){
				$typeSearchArray[] = "enrolled > 0 AND class_submitted = 0";
			}elseif($sType == 2){
				$typeSearchArray[] = "no_show > 0 AND class_submitted = 1";
			}elseif($sType == 1){
				$typeSearchArray[] = "completed > 0";
			}elseif($sType == 0){
				$typeSearchArray[] = "incomplete > 0";
			}
		}
		if(count($typeSearchArray) > 0 ){
		   $typeSearchString = implode(" OR ", $typeSearchArray);
		   $where .= "AND ($typeSearchString) ";
		}
	}
	if($USER->archetype != $CFG->userTypeAdmin){
		$where .= "AND (r.primary_instructor = ".$USER->id." || r.secondary_instructor = ".$USER->id." || r.classroom_creator = ".$USER->id." || r.class_creator = ".$USER->id.")";
	}
        
        $paramArray['key'] = addslashes($paramArray['key']);
        if(($paramArray['key'] != '')) {
		$where .= "AND (classroom_name like '%".$paramArray['key']."%' OR class_name like '%".$paramArray['key']."%') ";
	}
	if($where != ''){
		$where = ' WHERE '.LTRIM($where,'AND');
	}
	if($fields == ''){
		$sql = 'SELECT ##SELECT_FIELDS## FROM mdl_reports_classroom_report r '.$where.' GROUP BY classroom_id ORDER BY classroom_name,class_name';
		$sql = str_replace('##SELECT_FIELDS##',"*",$sql);
		if($perpage != 0){
			$offset = $page - 1;
			$offset = $offset*$perpage;
			$sql .= " LIMIT $offset, $perpage";
		}
		//echo $sql;
		$data = $DB->get_records_sql($sql);
	}else{
		$sql = 'SELECT r.id,SUM(r.enrolled) as enrolled,SUM(r.no_show) as no_show,SUM(r.incomplete) as incomplete,SUM(r.completed) as completed,count(*) as total_records FROM mdl_reports_classroom_report r '.$where.' GROUP BY r.classroom_id';
		$data1 = $DB->get_records_sql($sql);
		$total = 0;
		$data['no_show'] = 0;
		$data['incomplete'] = 0;
		$data['completed'] = 0;
		$data['enrolled'] = 0;
		foreach($data1 as $dataRec){
			$total += $dataRec->total_records;
			$data['enrolled'] += $dataRec->enrolled;
			$data['no_show'] += $dataRec->no_show;
			$data['incomplete'] += $dataRec->incomplete;
			$data['completed'] += $dataRec->completed;
		}
		$data['no_data'] = $data['enrolled'] - ($data['no_show']+$data['incomplete']+$data['completed']);
		$data['total_records'] = count($data1);
	}
	return $data;
}
function getClassroomClasses($arrCourses,$userId,$paramArray){
	GLOBAL $CFG,$DB,$USER;
	$data = array();
	$where = "";
	$sStartDate = $paramArray['startDate'];
	$sEndDate = $paramArray['endDate'];
	$sclassesArr = explode("@",$paramArray['classes']);
	$sTypeArr = explode("@",$paramArray['type']);
	$sDateSelected = '';
	if($sStartDate){
		$sDateArr = getFormattedTimeStampOfDate($sStartDate);
		$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

	}		   
	
	$eDateSelected = '';
	if($sEndDate){
	   $eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
	   $eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
	}
	$blankArray = array('-1','',0);
	$blankArrayType = array('-1','');
	if(!empty($arrCourses)){
		$where = " WHERE classroom_id IN (".implode(',',$arrCourses).")";
		if($sDateSelected){
			$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
			$where .= " AND class_start >= ".$sStartDateTime." ";
		}
		if($eDateSelected){
			$sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
			$where .= " AND class_end <= ".$sEndDateTime." ";
		}
		if(!empty($sclassesArr) && !in_array($sclassesArr[0],$blankArray)){
			$where .= " AND class_id IN (".implode(',',$sclassesArr).") ";
		}
		if($field != 'count_graph' && !empty($sTypeArr) && !in_array($sTypeArr[0],$blankArrayType)){
			$typeSearchArray = array();
			foreach($sTypeArr as $sType){
				if($sType == 3){
					$typeSearchArray[] = "enrolled > 0 AND class_submitted = 0";
				}elseif($sType == 2){
					$typeSearchArray[] = "no_show > 0 AND class_submitted = 1";
				}elseif($sType == 1){
					$typeSearchArray[] = "completed > 0";
				}elseif($sType == 0){
					$typeSearchArray[] = "incomplete > 0";
				}
			}
			if(count($typeSearchArray) > 0 ){
			   $typeSearchString = implode(" OR ", $typeSearchArray);
			   $where .= "AND ($typeSearchString) ";
			}
		}
		if($USER->archetype != $CFG->userTypeAdmin){
			$where .= "AND (r.primary_instructor = ".$USER->id." || r.secondary_instructor = ".$USER->id." || r.classroom_creator = ".$USER->id." || r.class_creator = ".$USER->id.")";
		}
		$sql = 'SELECT ##SELECT_FIELDS## FROM mdl_reports_classroom_report r '.$where.' ORDER BY classroom_name ASC,class_name ASC';
		$sql = str_replace('##SELECT_FIELDS##',"*",$sql);
		//echo $sql;die;
		$assetsData = $DB->get_records_sql($sql);
		foreach($assetsData as $asset){
			$data[$asset->classroom_id][] = $asset;
		}
	}
	return $data;
}
function getClassroomUsageReportToCSV($paramArray,$page,$perpage,$action){
	GLOBAL $DB,$CFG,$USER;
	$sStartDate = $paramArray['startDate'];
	$sEndDate = $paramArray['endDate'];
	$sCourseArr = explode("@",$paramArray['course']);
	$sclassesArr = explode("@",$paramArray['classes']);
	$sTypeArr = explode("@",$paramArray['type']);
	$blankArray = array('-1','',0);
	$blankArrayType = array('-1','');
	$sClassroomName = (empty($sCourseArr) || in_array($sCourseArr[0],$blankArray))?(get_string('all','classroomreport')):getCourses($sCourseArr);
	$sClassName = (empty($sclassesArr) || in_array($sclassesArr[0],$blankArray))?(get_string('all','classroomreport')):getClasses($sclassesArr);

	if(empty($sTypeArr) || in_array($sTypeArr[0],$blankArrayType)){
	  $sTypeName = get_string('all','classroomreport');
	}else{		  
	  $repTypeArr = array(0=>get_string('inprogress','classroomreport'),1=>get_string('completed','classroomreport'), get_string('classnoshow','classroomreport'),get_string('classnodata','classroomreport'));
	  $sTypeArr2 = str_replace(array(0, 1, 2,3), $repTypeArr, $sTypeArr); 
	  $sTypeName = implode(", ", $sTypeArr2);
	}
	$sDateSelected = '';
	if($sStartDate){
		$sDateArr = getFormattedTimeStampOfDate($sStartDate);
		$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

	}		   
	
	$eDateSelected = '';
	if($sEndDate){
	   $eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
	   $eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
	}
	$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
	$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
	$sDateSelectedForPrintCSV = getDateFormatForPrint($sDateSelected, 'csv');
	$eDateSelectedForPrintCSV = getDateFormatForPrint($eDateSelected, 'csv');
	if($action == 'print'){
		$reportName = get_string('courseusagesreport','classroomreport');
		$courseHTML .= getReportPrintHeader($reportName);

		$courseHTML .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="generaltable margin_bottom">';
		$courseHTML .= '<tr>';
		$courseHTML .= '<td width="500"><strong>'.get_string('course','classroomreport').':</strong> '.$sClassroomName.'</td>';
		$courseHTML .= '<td width="500"><strong>'.get_string('classname','classroomreport').':</strong>  '.$sClassName.'</td>';
		$courseHTML .= '</tr>';
		$courseHTML .= '<tr>';
		$courseHTML .= '<td colspan="2"><strong>'.get_string('status','classroomreport').':</strong> '.$sTypeName.'</td>';	
		$courseHTML .= '</tr>';
		$courseHTML .= '<tr>';
		$courseHTML .= '<td><strong>'.get_string('classstartfrom').':</strong> '.$sDateSelectedForPrint.'</td>';
		$courseHTML .= '<td><strong>'.get_string('classstartto').':</strong>  '.$eDateSelectedForPrint.'</td>';		
		$courseHTML .= '</tr>';
		$enrollentCount = getClassroomUsageReport($paramArray,1,0,'count_graph');
		$notStartedPercentage = 0;
		$inProgressPercentage = 0;
		$completedPercentage = 0;
		$noDataPercentage = 0;
		if($enrollentCount["enrolled"] != 0){
			$notStartedPercentage = floatval(($enrollentCount["no_show"]*100)/$enrollentCount["enrolled"]);
			$inProgressPercentage = floatval(($enrollentCount["incomplete"]*100)/$enrollentCount["enrolled"]);
			$completedPercentage = floatval(($enrollentCount["completed"]*100)/$enrollentCount["enrolled"]);
			$noDataPercentage = floatval(($enrollentCount["no_data"]*100)/$enrollentCount["enrolled"]);
		}
		$courseHTML .= '<td width="50%"><strong>'.get_string('totalassignedusers','multicoursereport').':</strong> '.$enrollentCount["enrolled"].'</td>';
		$courseHTML .= '<td width="50%"><strong>'.get_string('classnoshow','classroomreport').':</strong> '.$enrollentCount["no_show"].' ('.numberFormat($notStartedPercentage).'%)</td>';
		$courseHTML .= '</tr>';
		$courseHTML .= '<tr>';
		$courseHTML .= '<td width="50%"><strong>'.get_string('inprogress','classroomreport').':</strong>  '.$enrollentCount["incomplete"].' ('.numberFormat($inProgressPercentage).'%)</td>';
		$courseHTML .= '<td width="50%"><strong>'.get_string('completed','multicoursereport').':</strong> '.$enrollentCount["completed"].' ('.numberFormat($completedPercentage).'%)</td>';
		$courseHTML .= '</tr>';
		$courseHTML .= '<tr>';
		$courseHTML .= '<td width="100%" colspan="2"><strong>'.get_string('classnodata','classroomreport').':</strong> '.$enrollentCount["no_data"].' ('.numberFormat($noDataPercentage).'%)</td>';
		$courseHTML .= '</tr>';
		$courseHTML .= '</table>';
		return $courseHTML;
	}elseif($action == 'exportpdf'){
		$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" width="100%">';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td width="50%" ><strong>'.get_string('course','classroomreport').':</strong> '.$sClassroomName.'</td>';
		$reportContentPDF .= '<td width="50%"><strong>'.get_string('classname','classroomreport').':</strong>  '.$sClassName.'</td>';
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td colspan="2"><strong>'.get_string('status','classroomreport').':</strong> '.$sTypeName.'</td>';	
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td width="50%"><strong>'.get_string('classstartfrom').':</strong> '.$sDateSelectedForPrint.'</td>';
		$reportContentPDF .= '<td width="50%"><strong>'.get_string('classstartto').':</strong>  '.$eDateSelectedForPrint.'</td>';		
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '<tr>';
		$enrollentCount = getClassroomUsageReport($paramArray,1,0,'count_graph');
		$notStartedPercentage = 0;
		$inProgressPercentage = 0;
		$completedPercentage = 0;
		$noDataPercentage = 0;
		if($enrollentCount["enrolled"] != 0){
			$notStartedPercentage = floatval(($enrollentCount["no_show"]*100)/$enrollentCount["enrolled"]);
			$inProgressPercentage = floatval(($enrollentCount["incomplete"]*100)/$enrollentCount["enrolled"]);
			$completedPercentage = floatval(($enrollentCount["completed"]*100)/$enrollentCount["enrolled"]);
			$noDataPercentage = floatval(($enrollentCount["no_data"]*100)/$enrollentCount["enrolled"]);
		}
		$reportContentPDF .= '<td width="50%"><strong>'.get_string('totalassignedusers','multicoursereport').':</strong> '.$enrollentCount["enrolled"].'</td>';
		$reportContentPDF .= '<td width="50%"><strong>'.get_string('classnoshow','classroomreport').':</strong> '.$enrollentCount["no_show"].' ('.numberFormat($notStartedPercentage).'%)</td>';		 
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td width="50%"><strong>'.get_string('inprogress','classroomreport').':</strong>  '.$enrollentCount["incomplete"].' ('.numberFormat($inProgressPercentage).'%)</td>';	
		$reportContentPDF .= '<td width="50%"><strong>'.get_string('completed','multicoursereport').':</strong> '.$enrollentCount["completed"].' ('.numberFormat($completedPercentage).'%)</td>';	
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td width="100%" colspan="2"><strong>'.get_string('classnodata','classroomreport').':</strong> '.$enrollentCount["no_data"].' ('.numberFormat($noDataPercentage).'%)</td>';	
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '</table>';
		$reportContentPDF .= getGraphImageHTML(get_string('classroomusagesreport','classroomreport'));
		$filename = str_replace(' ', '_', get_string('courseusagesreport','classroomreport'))."_".date("m-d-Y").".pdf";
		exportPDF($filename, $reportContentPDF, '', get_string('courseusagesreport','classroomreport'));
	}elseif($action == 'exportcsv'){
		$enrolledData = getClassroomUsageReport($paramArray,1,0,'');
		$enrollentCount = getClassroomUsageReport($paramArray,1,0,'count_graph');
		$notStartedPercentage = 0;
		$inProgressPercentage = 0;
		$completedPercentage = 0;
		$noDataPercentage = 0;
		if($enrollentCount["enrolled"] != 0){
			$notStartedPercentage = floatval(($enrollentCount["no_show"]*100)/$enrollentCount["enrolled"]);
			$inProgressPercentage = floatval(($enrollentCount["incomplete"]*100)/$enrollentCount["enrolled"]);
			$completedPercentage = floatval(($enrollentCount["completed"]*100)/$enrollentCount["enrolled"]);
			$noDataPercentage = floatval(($enrollentCount["no_data"]*100)/$enrollentCount["enrolled"]);
		}
		$reportContentCSV .= get_string('course','classroomreport').",".$sClassroomName."\n".get_string('classname','classroomreport').",".$sClassName."\n";
		$reportContentCSV .= get_string('status','classroomreport').",".$sTypeName."\n";
		$reportContentCSV .= get_string('classstartfrom').",".$sDateSelectedForPrintCSV."\n".get_string('classstartto').",".$eDateSelectedForPrintCSV."\n";
		$reportContentCSV .= get_string('enrolled','multicoursereport').",".$enrollentCount["enrolled"]."\n";
		$reportContentCSV .= get_string('classnoshow','classroomreport').",".$enrollentCount["no_show"]." (".numberFormat($notStartedPercentage)."%)\n";
		$reportContentCSV .= get_string('inprogress','classroomreport').",".$enrollentCount["incomplete"]." (".numberFormat($inProgressPercentage)."%)\n";
		$reportContentCSV .= get_string('completed','multicoursereport').",".$enrollentCount["completed"]." (".numberFormat($completedPercentage)."%)\n";
		$reportContentCSV .= get_string('classnodata','classroomreport').",".$enrollentCount["no_data"]." (".numberFormat($noDataPercentage)."%)\n";
		$reportContentCSV .= get_string('classroomusagesreport','classroomreport')."\n";
		
		$reportContentCSV .= get_string('coursetitle','classroomreport').",".get_string('totalassignedusers','classroomreport').",".get_string('classnoshow','classroomreport').",".get_string('inprogress','classroomreport').",".get_string('completed','classroomreport')."\n";
		$arrAssets = array();
		$arrCourses = array();
		if(!empty($enrolledData)){
			foreach($enrolledData as $enrolled){
				$arrCourses[] = $enrolled->classroom_id;
			}
			$arrAssets = getClassroomClasses($arrCourses,$userId,$paramArray);
		}
		if(!empty($enrolledData)){
			foreach($enrolledData as $enrolled){
				$reportContentCSV .= $enrolled->classroom_name."\n";
				if(!empty($arrAssets[$enrolled->classroom_id])){
					foreach($arrAssets[$enrolled->classroom_id] as $assetData){
						$reportContentCSV .= getSpace('csv').$assetData->class_name.",".$assetData->enrolled.",".$assetData->no_show.",".$assetData->incomplete.",".$assetData->completed."\n";
					}
				}
			}
		}else{
			$reportContentCSV .= get_string('no_results')."\n";
		}
		$filepath = $CFG->dirroot."/local/reportexport/temp";		
		chmod($filepath, 0777);		
		$filename = str_replace(' ', '_', get_string('courseusagesreport','classroomreport'))."_".date("m-d-Y").".csv";  
		$filepathname = $filepath.'/'.$filename;
		unlink($filepathname);
		$handler = fopen($filepathname, "w");
		fwrite($handler, $reportContentCSV);
   	    exportCSV($filepathname);
	}
}
function getClassroomReportData($paramArray,$page,$perpage,$userId,$fields = ''){
	GLOBAL $CFG,$USER,$DB;
	$classId = $paramArray['class_id'];
	$where = '';
	$sTypeArr = explode("@",$paramArray['type']);
	$blankArrayType = array('-1');
	if($paramArray['class_submitted'] == 1 && !empty($sTypeArr) && !in_array($sTypeArr[0],$blankArrayType)){
		$typeSearchArray = array();
		foreach($sTypeArr as $sType){
			if($sType == 3){
				$typeSearchArray[] = "enrolled > 0 AND class_submitted = 0";
			}elseif($sType == 2){
				$typeSearchArray[] = "attendedStr = 0 AND is_completed != 1";
			}elseif($sType == 1){
				$typeSearchArray[] = "is_completed = 1";
			}elseif($sType == 0){
				$typeSearchArray[] = "attendedStr = 1 AND is_completed != 1";
			}
		}
		if(count($typeSearchArray) > 0 ){
		   $typeSearchString = implode(" OR ", $typeSearchArray);
		   $where = " WHERE ($typeSearchString)";
		}
	}
	if($fields == ''){
		$sql = 'SELECT * FROM ( SELECT r.userid,r.user_fullname,r.username,r.is_completed, IF(GROUP_CONCAT(r.attended) REGEXP 0 AND GROUP_CONCAT(r.attended) REGEXP 1,1, IF(GROUP_CONCAT(r.attended) REGEXP 1,1,0)) AS attendedStr FROM mdl_reports_classroom_details r WHERE r.scheduler_id = '.$classId.' GROUP BY r.userid ) as a'.$where;
		$sql = str_replace('##SELECT_FIELDS##',"*",$sql);
		if($perpage != 0){
			$offset = $page - 1;
			$offset = $offset*$perpage;
			$sql .= " LIMIT $offset, $perpage";
		}
		$data = $DB->get_records_sql($sql);
	}else{
		$sql = 'SELECT count(*) as total_records FROM ( SELECT r.userid,r.user_fullname,r.username,r.is_completed, IF(GROUP_CONCAT(r.attended) REGEXP 0 AND GROUP_CONCAT(r.attended) REGEXP 1,1, IF(GROUP_CONCAT(r.attended) REGEXP 1,1,0)) AS attendedStr FROM mdl_reports_classroom_details r WHERE r.scheduler_id = '.$classId.' GROUP BY r.userid ) as a'.$where;
		$data = $DB->get_record_sql($sql);
	}
	return $data;
}
function getUserSessionData($userAray,$classId){
	GLOBAL $CFG,$USER,$DB;
	if(!empty($userAray)){
		$sql = 'SELECT * FROM mdl_reports_classroom_details r WHERE r.scheduler_id = '.$classId.' AND r.userid IN ('.implode(",",$userAray).')';
		$data = $DB->get_records_sql($sql);
		$userData = array();
		foreach($data as $sessionUser){
			if(!array_key_exists($sessionUser->userid,$userData)){
				$userData[$sessionUser->userid] = array();
			}
			$userData[$sessionUser->userid]['is_completed'] = $sessionUser->is_completed;
			$userData[$sessionUser->userid]['session'][$sessionUser->session_id]['attendence'] = $sessionUser->attended;
			$userData[$sessionUser->userid]['session'][$sessionUser->session_id]['grade'] = $sessionUser->grade;
			$userData[$sessionUser->userid]['session'][$sessionUser->session_id]['score'] = $sessionUser->score;
		}
		
		return($userData);
	}
}
function exportClassReportPdf($chartData,$paramArray,$action){
	GLOBAL $CFG,$USER,$DB;
	if($action == 'exportpdf'){
		$reportContentPDF = '';
		$durationText = getClassDuration($chartData->class_start, $chartData->class_end);
		$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" width="100%">';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td><strong>'.get_string("course","classroomreport").':</strong> '.$chartData->classroom_name.'</td>';
		$reportContentPDF .= '<td><strong>'.get_string("classname","classroomreport").':</strong> '.$chartData->class_name.'</td>';
		$reportContentPDF .= '</tr>';

		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td><strong>'.get_string('class_date_label').':</strong> '.$durationText.'</td>';
		$reportContentPDF .= '<td><strong>'.get_string('instructor','scheduler').':</strong> '.$chartData->class_instructor_fullname.'</td>';
		$reportContentPDF .= '</tr>';
		$noOfSeats = $chartData->no_of_seats;
		$classSubmittedByName = $chartData->class_submittedby_name;
		if($noOfSeats && $classSubmittedByName){
			$reportContentPDF .= '<tr>';
			$reportContentPDF .= '<td><strong>'.get_string('no_of_seats','scheduler').':</strong> '.$noOfSeats.'</td>';
			$reportContentPDF .= '<td><strong>'.get_string('classsubmittedby','scheduler').':</strong> '.$classSubmittedByName.'</td>';
			$reportContentPDF .= '</tr>';
		}elseif($noOfSeats != '' && $classSubmittedByName==''){
			$reportContentPDF .= '<tr>';
			$reportContentPDF .= '<td colspan="2"><strong>'.get_string('no_of_seats','scheduler').':</strong> '.$noOfSeats.'</td>';
			$reportContentPDF .= '</tr>';
		}elseif($noOfSeats == '' && $classSubmittedByName){
			$reportContentPDF .= '<tr>';
			$reportContentPDF .= '<td  colspan="2" ><strong>'.get_string('classsubmittedby','scheduler').':</strong> '.$classSubmittedByName.'</td>';
			$reportContentPDF .= '</tr>';
		}
		$reportContentPDF .= '</table>';
		$reportContentPDF .= getGraphImageHTML(get_string('overalluserprogressreport','classroomreport'));
		$filename = str_replace(' ', '_', get_string('overalluserprogressreport','classroomreport'))."_".date("m-d-Y").".pdf";
		exportPDF($filename, $reportContentPDF, '', get_string('overalluserprogressreport','classroomreport'));
	}else{
		$reportContentCSV = '';
		$durationTextCSV = getClassDuration($chartData->class_start, $chartData->class_end, 'csv');
		$reportContentCSV .= get_string('course','classroomreport').",".$chartData->classroom_name."\n";
		$reportContentCSV .= get_string('classname','classroomreport').",".$chartData->class_name."\n";
		$reportContentCSV .= get_string('class_date_label').",".$durationTextCSV."\n";
		$reportContentCSV .= get_string('instructor','scheduler').",".$chartData->class_instructor_fullname."\n";
		$noOfSeats = $chartData->no_of_seats;
		$classSubmittedByName = $chartData->class_submittedby_name;

		if($noOfSeats){
			$reportContentCSV .= get_string('no_of_seats','scheduler').",".$noOfSeats."\n";
		}
		if($classSubmittedByName){	
			$reportContentCSV .= get_string('classsubmittedby','scheduler').",".$classSubmittedByName."\n";
		}
		if($chartData->enrolled > 0){
			if($chartData->class_submitted != 1){
				$chartData->nodata = $chartData->enrolled;
				$chartData->no_show = 0;
				$chartData->incomplete = 0;
				$chartData->completed = 0;
			}else{
				$chartData->nodata = 0;
			}
			$reportContentCSV .= get_string('numberofcourses','multiuserreport').",".$chartData->enrolled."\n";
			$reportContentCSV .= get_string('notstarted','multiuserreport').",".$chartData->no_show."\n";
			$reportContentCSV .= get_string('inprogress','multiuserreport').",".$chartData->incomplete."\n";
			$reportContentCSV .= get_string('completed','multiuserreport').",".$chartData->completed."\n";
			$reportContentCSV .= get_string('classnodata','classroomreport').",".$chartData->nodata."\n";
		}
		$sTypeArr = explode("@",$paramArray['type']);
		$blankArrayType = array('-1');
		if(in_array($sTypeArr[0],$blankArrayType)){
		  $sTypeName = get_string('all','classroomreport');
		}else{
		  $repTypeArr = array(0=>get_string('inprogress','classroomreport'),1=>get_string('completed','classroomreport'), get_string('classnoshow','classroomreport'));
		  $sTypeArr2 = str_replace(array(0, 1, 2), $repTypeArr, $sTypeArr); 
		  $sTypeName = implode(", ", $sTypeArr2);
		}
		$reportContentCSV .= get_string('status','classroomreport').",".$sTypeName."\n";
		$reportContentCSV .= get_string('overalluserprogressreport','classroomreport')."\n\n";
		$CSVCommas = ' ';
		$classId = $chartData->class_id;
		$sessionHeaderSql = "SELECT r.session_id as id,r.sessionname,r.session_starttime,r.session_endtime,r.session_duration FROM mdl_reports_classroom_details r WHERE r.scheduler_id = ".$classId." GROUP BY r.session_id";
		$sessionHeaderData = $DB->get_records_sql($sessionHeaderSql);
		$cntSession = count($sessionHeaderData);
		if($cntSession > 0 ){
		  for($i=0; $i < $cntSession; $i++){
			$CSVCommas .= ",";
		  }
		}
		$reportContentCSV .= get_string('name').", ,".get_string("sessionstext","scheduler").$CSVCommas.get_string ( 'userclassstatus', 'scheduler' )."\n";
		$CSVSessionName = '';	
		foreach ( $sessionHeaderData as $session ) {
			$sessionDurationCSV = getClassroomSessionDateDuration($session->session_starttime, $session->session_duration, 'minutes', 'csv'); 
			$sessionNameCSV = $session->sessionname." (". $sessionDurationCSV.")";
			$CSVSessionName .= ", ".$sessionNameCSV;
		}
		$reportContentCSV .= " , ".$CSVSessionName.", , , , \n";
		$enrolledData = getClassroomReportData($paramArray,1,0,$userId);
		$arrSessions = array();
		$arrUser = array();
		if(!empty($enrolledData)){
			foreach($enrolledData as $enrolled){
				$arrUser[] = $enrolled->userid;
			}
			$arrSessions = getUserSessionData($arrUser, $classId);
		}
		if(!empty($enrolledData)){
			foreach($enrolledData as $enrolled){	
				$CSVAttended = '';
				$CSVScore = '';
				$CSVGrade = '';
				$attendedStatus = 0;
				foreach($arrSessions[$enrolled->userid]['session'] as $key=>$session){
					$attended = 'No';
					$score = getMDashForCSV();
					$grade = getMDashForCSV();
					if($chartData->class_submitted == 1){
						if($session['attendence'] == 1){
							if($attendedStatus == 0){
								$attendedStatus = 1;
							}
							$attended = 'Yes';
						}
						if($session['score']){
							$score = $session['score'];
						}
						if($session['grade']){
							$grade = $session['grade'];
						}
					}
					$CSVAttended .= ", ".$attended;
					$CSVScore .= ", ".$score;
					$CSVGrade .= ", ".$grade;		   
				}
				if($chartData->class_submitted != 1){
					$userStatus = get_string('classnodata','classroomreport');
				}else{
					if($arrSessions[$enrolled->userid]['is_completed'] == 1){
						$userStatus = get_string('classcompleted','classroomreport');
					}elseif($attendedStatus == 1){
						$userStatus = get_string('classincomplete','classroomreport');
					}else{
						$userStatus = get_string('classnoshow','classroomreport');
					}
				}
				$reportContentCSV .= $enrolled->user_fullname.",".get_string("attended","scheduler").$CSVAttended."\n";
				$reportContentCSV .= " ,".get_string("score","scheduler").$CSVScore.",".($userStatus)."\n";
				$reportContentCSV .= " ,".get_string("grade","scheduler").$CSVGrade.", , \n"; 
			}
		}

		$filepath = $CFG->dirroot."/local/reportexport/temp";		
		chmod($filepath, 0777);		
		$filename = str_replace(' ', '_', get_string('overalluserprogressreport','classroomreport'))."_".date("m-d-Y").".csv";  
		$filepathname = $filepath.'/'.$filename;
		unlink($filepathname);
		$handler = fopen($filepathname, "w");
		fwrite($handler, $reportContentCSV);
   	    exportCSV($filepathname);
	}
}
function getLearnerClassroomReport($paramArray,$page,$perpage,$fields){
	Global $CFG,$DB,$USER;
	$userId		= $paramArray['userid'];
	$sType		= $paramArray['type'];
	$sCourse	= $paramArray['course'];
	$sClasses	= $paramArray['classes'];
	$sCourseArr	= explode("@",$sCourse);
	$sClassesArr= explode("@",$sClasses);
	$sTypeArr	= explode("@",$sType);
	$sStartDate	= $paramArray['startDate'];
	$sEndDate	= $paramArray['endDate'];
	$blankArray = array(0,'-1','');
	$sDateSelected = '';
	$where = '';
	if($sStartDate){
		$sDateArr = getFormattedTimeStampOfDate($sStartDate);
		$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';
		$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
		$where .= ' AND class_start >= '.$sStartDateTime;
	}
	$eDateSelected = '';
	if($sEndDate){
		$eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
		$eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
		$sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
		$where .= ' AND class_end <= '.$sEndDateTime;
	}
	if(!empty($sCourseArr) && !in_array($sCourseArr[0],$blankArray)){
		$where .= ' AND classroom_id IN ('.implode(',',$sCourseArr).')';
	}
	if(!empty($sClassesArr) && !in_array($sClassesArr[0],$blankArray)){
		$where .= ' AND class_id IN ('.implode(',',$sClassesArr).')';
	}
	$blankArrayType = array('-1');
	$whereType = '';
	if($fields != 'count_graph' && !empty($sTypeArr) && !in_array($sTypeArr[0],$blankArrayType)){
		$typeSearchArray = array();
		foreach($sTypeArr as $sType){
			if($sType == 3){
				$typeSearchArray[] = "class_status = 0";
			}elseif($sType == 2){
				$typeSearchArray[] = "class_status = 3";
			}elseif($sType == 1){
				$typeSearchArray[] = "class_status = 1";
			}elseif($sType == 0){
				$typeSearchArray[] = "class_status = 2";
			}
		}
		if(count($typeSearchArray) > 0 ){
		   $typeSearchString = implode(" OR ", $typeSearchArray);
		   $whereType = " WHERE ($typeSearchString)";
		}
	}
	if($fields == ''){
		$sql = "SELECT * FROM ( SELECT cr.id,cr.classroom_id,cr.class_id,cr.classroom_name,cr.class_name,cr.class_start,cr.class_end,IF(cr.class_submitted = 1,IF(cd.is_completed = 1,1,IF(GROUP_CONCAT(cd.attended) REGEXP 0 AND GROUP_CONCAT(cd.attended) REGEXP 1,2, IF(GROUP_CONCAT(cd.attended) REGEXP 1,2,3))),0) as class_status FROM mdl_reports_classroom_report AS cr LEFT JOIN mdl_reports_classroom_details AS cd ON cr.class_id = cd.scheduler_id WHERE cd.userid = ".$userId." $where GROUP BY classroom_id,cr.class_id ORDER BY classroom_name,class_name ) as a $whereType";
		if($perpage != 0){
			$offset = $page - 1;
			$offset = $offset*$perpage;
			$sql .= " LIMIT $offset, $perpage";
		}
		$data = $DB->get_records_sql($sql);
	}else{
		$sql = "SELECT class_status,count(*) as row_count FROM ( SELECT cr.id,cr.classroom_id,cr.class_id,cr.classroom_name,cr.class_name,cr.class_start,cr.class_end,IF(cr.class_submitted = 1,IF(cd.is_completed = 1,1,IF(GROUP_CONCAT(cd.attended) REGEXP 0 AND GROUP_CONCAT(cd.attended) REGEXP 1,2, IF(GROUP_CONCAT(cd.attended) REGEXP 1,2,3))),0) as class_status FROM mdl_reports_classroom_report AS cr LEFT JOIN mdl_reports_classroom_details AS cd ON cr.class_id = cd.scheduler_id WHERE cd.userid = ".$userId." $where GROUP BY classroom_id,cr.class_id ORDER BY classroom_name,class_name ) as a $whereType GROUP BY class_status";
		$data1 = $DB->get_records_sql($sql);
		$data->total_records = 0;
		$data->completed = 0;
		$data->incomplete = 0;
		$data->no_show = 0;
		$data->no_data = 0;
		if(isset($data1[0])){
			$data->no_data = $data1[0]->row_count;
			$data->total_records += $data->no_data;
		}
		if(isset($data1[1])){
			$data->completed = $data1[1]->row_count;
			$data->total_records += $data->completed;
		}
		if(isset($data1[2])){
			$data->incomplete = $data1[2]->row_count;
			$data->total_records += $data->incomplete;
		}
		if(isset($data1[3])){
			$data->no_show = $data1[3]->row_count;
			$data->total_records += $data->no_show;
		}
	}
	return $data;
}
function getLearnerClassroomSessions($paramArray,$classArray){
	if(!empty($classArray)){
		GLOBAL $USER,$CFG,$DB;
		$userId = $paramArray['userid'];
		$classSql = "SELECT cd.id,cr.class_id,cd.session_id,cd.sessionname,cd.attended,cd.score,cd.grade,cd.is_completed,cd.session_starttime,cd.session_duration FROM mdl_reports_classroom_report AS cr LEFT JOIN mdl_reports_classroom_details AS cd ON cr.class_id = cd.scheduler_id WHERE cd.userid = ".$userId." AND cr.class_id IN  (".implode(',',$classArray).")";
		$classData = $DB->get_records_sql($classSql);
		$SessionData = array();
		foreach($classData as $sessionUser){
			if(!array_key_exists($sessionUser->class_id,$SessionData)){
				$SessionData[$sessionUser->class_id] = array();
			}
			$SessionData[$sessionUser->class_id]['session'][$sessionUser->session_id]['sessionname'] = $sessionUser->sessionname;
			$SessionData[$sessionUser->class_id]['session'][$sessionUser->session_id]['session_starttime'] = $sessionUser->session_starttime;
			$SessionData[$sessionUser->class_id]['session'][$sessionUser->session_id]['session_duration'] = $sessionUser->session_duration;

			$SessionData[$sessionUser->class_id]['session'][$sessionUser->session_id]['attendence'] = $sessionUser->attended;
			$SessionData[$sessionUser->class_id]['session'][$sessionUser->session_id]['grade'] = $sessionUser->grade;
			$SessionData[$sessionUser->class_id]['session'][$sessionUser->session_id]['score'] = $sessionUser->score;
		}
		return $SessionData;
	}
}
function exportLearnerClassReport($paramArray,$action){
	global $CFG,$DB,$USER;
	$userId = $paramArray['userid'];
	$userName = getUsers($userId, 1);
	$sStartDate     = $paramArray['startDate'];
	$sEndDate       = $paramArray['endDate'];
	$sType = $paramArray['type'];
	$sCourse = $paramArray['course'];
	$sClasses = $paramArray['classes'];
	$sCourseArr = explode("@",$sCourse);
	$sClassesArr = explode("@",$sClasses);
	$sTypeArr = explode("@",$sType);
	if($sTypeArr[0] == '-1'){
	  $sTypeName = get_string('all','classroomreport');
	}else{
	  $repTypeArr = array(0=>get_string('inprogress','classroomreport'),1=>get_string('completed','classroomreport'), get_string('classnoshow','classroomreport'),3=>get_string('classnodata','classroomreport'));
	  $sTypeArr2 = str_replace(array(0, 1, 2), $repTypeArr, $sTypeArr); 
	  $sTypeName = implode(", ", $sTypeArr2);
	}
	$sDateSelected = '';
	if($sStartDate){
		$sDateArr = getFormattedTimeStampOfDate($sStartDate);
		$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';
	}
	$eDateSelected = '';
	if($sEndDate){
	   $eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
	   $eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
	}
	$sClassroomName = $sCourse=='-1'?(get_string('all','classroomreport')):getCourses($sCourseArr);
	$sClassName = $sClasses=='-1'?(get_string('all','classroomreport')):getClasses($sClassesArr);

	if($action == 'print'){
		$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
		$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);

		$reportName = get_string('overallclassroomreport','scheduler');
		$printHtml .= getReportPrintHeader($reportName);
		$printHtml .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="generaltable margin_bottom">';
		$printHtml .= '<tr>';
		$printHtml .= '<td width="50%"><strong>'.get_string('user','singlereport').':</strong> '.$userName.'</td>';
		$printHtml .= '<td width="50%"><strong>'.get_string('course','classroomreport').':</strong> '.$sClassroomName.'</td>';
		$printHtml .= '</tr>';
		$printHtml .= '<tr>';
		$printHtml .= '<td><strong>'.get_string('classname','classroomreport').':</strong>  '.$sClassName.'</td>';
		$printHtml .= '<td><strong>'.get_string('status','classroomreport').':</strong> '.$sTypeName.'</td>';
		$printHtml .= '</tr>';
		$printHtml .= '<tr>';
		$printHtml .= '<td><strong>'.get_string('classstartfrom').':</strong> '.$sDateSelectedForPrint.'</td>';
		$printHtml .= '<td><strong>'.get_string('classstartto').':</strong>  '.$eDateSelectedForPrint.'</td>';		
		$printHtml .= '</tr>';
		$printHtml .= '</table>';
		return $printHtml;
	}elseif($action == 'exportpdf'){
		$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
		$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
		$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" width="100%">';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td width="50%"><strong>'.get_string('user','singlereport').':</strong> '.$userName.'</td>';
		$reportContentPDF .= '<td width="50%"><strong>'.get_string('course','classroomreport').':</strong> '.$sClassroomName.'</td>';
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td><strong>'.get_string('classname','classroomreport').':</strong>  '.$sClassName.'</td>';
		$reportContentPDF .= '<td><strong>'.get_string('status','classroomreport').':</strong> '.$sTypeName.'</td>';
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td><strong>'.get_string('classstartfrom').':</strong> '.$sDateSelectedForPrint.'</td>';
		$reportContentPDF .= '<td><strong>'.get_string('classstartto').':</strong>  '.$eDateSelectedForPrint.'</td>';	
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '<tr>';
		$enrollentCount = getLearnerClassroomReport($paramArray,1,0,'count_graph');
		$noShowPercentage = 0;
		$inCompletedPercentage = 0;
		$completedPercentage = 0;
		$noDataPercentage = 0;
		if($enrollentCount->total_records != 0){
			$noShowPercentage = floatval(($enrollentCount->no_show*100)/$enrollentCount->total_records);
			$inCompletedPercentage = floatval(($enrollentCount->incomplete*100)/$enrollentCount->total_records);
			$completedPercentage = floatval(($enrollentCount->completed*100)/$enrollentCount->total_records);
			$noDataPercentage = floatval(($enrollentCount->no_data*100)/$enrollentCount->total_records);
		}
		$reportContentPDF .= '<td><strong>'.get_string('numberofcourses','classroomreport').':</strong> '.$enrollentCount->total_records.'</td>';
		$reportContentPDF .= '<td><strong>'.get_string('classnoshow','classroomreport').':</strong> '.$enrollentCount->no_show.' ('.round(floatval($noShowPercentage),2).'%)</td>';
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td><strong>'.get_string('inprogress','classroomreport').':</strong> '.$enrollentCount->incomplete.' ('.round(floatval($inCompletedPercentage),2).'%)</td>';
		$reportContentPDF .= '<td><strong>'.get_string('completed','classroomreport').':</strong> '.$enrollentCount->completed.' ('.round(floatval($completedPercentage),2).'%)</td>';;
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '<tr>';
		$reportContentPDF .= '<td colspan="2" ><strong>'.get_string('classnodata','classroomreport').':</strong> '.$enrollentCount->no_data.' ('.round(floatval($noDataPercentage),2).'%)</td>';
		$reportContentPDF .= '</tr>';
		$reportContentPDF .= '</table>';
		$reportContentPDF .= getGraphImageHTML(get_string('overallclassroomreport','scheduler'));
		$filename = str_replace(' ', '_', get_string('overallclassroomreport','scheduler'))."_".date("m-d-Y").".pdf";
		exportPDF($filename, $reportContentPDF, '', get_string('overallclassroomreport','scheduler'));
	}elseif($action == 'exportcsv'){
		$enrollentCount = getLearnerClassroomReport($paramArray,1,0,'count_graph');
		$noShowPercentage = 0;
		$inCompletedPercentage = 0;
		$completedPercentage = 0;
		$noDataPercentage = 0;
		if($enrollentCount->total_records != 0){
			$noShowPercentage = round(floatval(($enrollentCount->no_show*100)/$enrollentCount->total_records),2);
			$inCompletedPercentage = round(floatval(($enrollentCount->incomplete*100)/$enrollentCount->total_records),2);
			$completedPercentage = round(floatval(($enrollentCount->completed*100)/$enrollentCount->total_records),2);
			$noDataPercentage = round(floatval(($enrollentCount->no_data*100)/$enrollentCount->total_records),2);
		}
		$reportContentCSV .= get_string('user','singlereport').",".$userName."\n";
		$reportContentCSV .= get_string('course','classroomreport').",".$sClassroomName."\n";
		$reportContentCSV .= get_string('classname','classroomreport').",".$sClassName."\n";
		$reportContentCSV .= get_string('status','classroomreport').",".$sTypeName."\n";
		$reportContentCSV .= get_string('classstartfrom').",".$sDateSelectedForPrintCSV."\n";
		$reportContentCSV .= get_string('classstartto').",".$eDateSelectedForPrintCSV."\n";
		$reportContentCSV .= get_string('numberofcourses','classroomreport').",".$userCount."\n";
		$reportContentCSV .= get_string('classnoshow','classroomreport').",".$enrollentCount->no_show." (".floatval($noShowPercentage)."%)\n";
		$reportContentCSV .= get_string('inprogress','classroomreport').",".$enrollentCount->incomplete." (".floatval($inCompletedPercentage)."%)\n";
		$reportContentCSV .= get_string('completed','classroomreport').",".$enrollentCount->completed." (".floatval($completedPercentage)."%)\n";
		$reportContentCSV .= get_string('classnodata','classroomreport').",".$enrollentCount->no_data." (".floatval($noDataPercentage)."%)\n";
		$reportContentCSV .= get_string('overallclassroomreport','scheduler')."\n";
		
		$reportContentCSV .= get_string('title').','.get_string ( 'attended', 'scheduler' ).','.get_string ( 'grade', 'scheduler' ).','. get_string ( 'score', 'scheduler' ).','.get_string ( 'classcompletedstatus', 'scheduler' )."\n";
		$enrolledData = getLearnerClassroomReport($paramArray,1,0,'');
		$arrSessions = array();
		$arrClass = array();
		if(!empty($enrolledData)){
			foreach($enrolledData as $enrolled){
				$arrClass[] = $enrolled->class_id;
			}
			$arrSessions = getLearnerClassroomSessions($paramArray, $arrClass);
		}
		if(!empty($enrolledData)){
			foreach($enrolledData as $enrolData){
				$classDuration	= getClassDuration($enrolData->class_start, $enrolData->class_end, 'csv');
				$classTitle		= $enrolData->classroom_name.' - '.$enrolData->class_name." (".$classDuration.")";
				if($enrolData->class_status == 0){
					$class_status = get_string('classnodata','classroomreport');
				}elseif($enrolData->class_status == 1){
						$class_status = get_string('classcompleted','classroomreport');
				}elseif($enrolData->class_status == 2){
					$class_status = get_string('classincomplete','classroomreport');
				}else{
					$class_status = get_string('classnoshow','classroomreport');
				}
				$reportContentCSV .=  $classTitle .', , , ,'.$class_status."\n";
				if(isset($arrSessions[$enrolData->class_id]) && !empty($arrSessions[$enrolData->class_id])){
					foreach($arrSessions[$enrolData->class_id]['session'] as $session){
						$attended = 'No';
						$score = getMDashForCSV();
						$grade = getMDashForCSV();
						if($enrolData->class_status == 1){
							if($session['attendence'] == 1){
								if($attendedStatus == 0){
									$attendedStatus = 1;
								}
								$attended = 'Yes';
							}
							if($session['score']){
								$score = $session['score'];
							}
							if($session['grade']){
								$grade = $session['grade'];
							}
						}
						$reportContentCSV .= getSpace('csv').$session['sessionname'].','.$attended.','.$grade.','.$score."\n";
					}
				}
			}
		}else{
			$reportContentCSV .= get_string('norecordfound','scheduler')."\n";
		}
		$filepath = $CFG->dirroot."/local/reportexport/temp";		
		chmod($filepath, 0777);		
		$filename = str_replace(' ', '_', get_string('overallclassroomreport','scheduler'))."_".date("m-d-Y").".csv";  
		$filepathname = $filepath.'/'.$filename;
		unlink($filepathname);
		$handler = fopen($filepathname, "w");
		fwrite($handler, $reportContentCSV);
   	    exportCSV($filepathname);
	}
}

function getCourseProgressReportScore($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export=''){

	global $DB, $CFG, $USER, $SITE;
	$loginUserId = $USER->id;
	$isReport = true;
		
	$sType = $paramArray['type'];
	$sBack = $paramArray['back'];
	
	$userstatus=$paramArray['userstatus'];
        $userstatusArr=explode("@",$userstatus);
        
        $sdepartment=$paramArray['department'];
        $departmentArr=explode("@",$sdepartment);
        
        $scountry=$paramArray['country'];
        $countryArr=explode("@",$scountry);
        
        $scity=$paramArray['city'];
        $cityArr=explode("@",$scity);        
        
        $steam=$paramArray['team'];
        $teamArr=explode("@",$steam);
		
	$sTypeArr = explode("@",$sType);

        $sIsManagerYes=$paramArray['is_manager_yes'];
        $sIsManagerYesArr=explode("@",$sIsManagerYes);
        
	$sStartDate     = $paramArray['startDate'];
	$sEndDate       = $paramArray['endDate'];

	$sDateSelected = '';
	if($sStartDate){
		$sDateArr = getFormattedTimeStampOfDate($sStartDate);
		$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

	}
		
	$eDateSelected = '';
	if($sEndDate){
		$eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
		$eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
	}
		
		
	$offset = $page - 1;
	$offset = $offset*$perpage;
	$limit = '';
	if($perpage != 0){
		$limit = "LIMIT $offset, $perpage";
	}

	////// Getting common URL for the Search //////
	$pageURL = $_SERVER['PHP_SELF'];
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
		
	$printUrl = '/reports/course_progress_report_print.php';
	$genPrintURL = genParameterizedURL($paramArray, $removeKeyArray, $printUrl);

	$cid    = $paramArray['cid'];
	$courseName = getCourses($cid);
		
	$where = '';	
  if(count($userstatus) && !in_array('-1',$userstatusArr))
    {
       $where .= "AND mu.suspended in(".@implode(',',$userstatusArr).") ";
    }
    if(count($sIsManagerYes) && !in_array('-1',$sIsManagerYesArr))
    {
       $where .= "AND mu.is_manager_yes in(".@implode(',',$sIsManagerYesArr).") ";
    }
    
    
            
    if(count($countryArr) && !in_array('-1',$countryArr))
    {
        array_walk($countryArr,function (&$val){ $val="'".$val."'";});
        $countries = @implode(",",$countryArr);
       $where .= "AND mu.country IN(".$countries.") ";
    }
    
    if(count($cityArr) && !in_array('-1',$cityArr))
    {        
        $cityArr = ConvertCityIdToNameArr($cityArr);
        $city = @implode(",",$cityArr);
        $where .= "AND mu.city IN(".$city.") ";
    }
    
    if($paramArray['job_title'] != '' && $paramArray['job_title'] != '-1' && $paramArray['job_title'] != 0 && !in_array('-1', 	$paramArray['job_title']) && !in_array('0', $paramArray['job_title'])) {
		$jobIds = count($paramArray['job_title']) > 0 ?implode(",", explode("@",$paramArray['job_title'])):0;
		$where .= " AND mu.job_title IN ($jobIds)";
    }
    
    if($paramArray['report_to'] != '' && $paramArray['report_to'] != '-1' && count($paramArray['report_to']) > 0){
			 $id_explode = explode("@",$paramArray['report_to']);
			 $List = getLeadersNameList();

			 $leaders_name = '';
			 foreach($id_explode as $id){
                            $leaders_name .= '"'.$List[$id].'",';
			 }
			 $leaders_name = trim($leaders_name,',');
			 $where .= " AND mu.report_to IN ($leaders_name)";
    }
    
    if(count($departmentArr) && !in_array('-1',$departmentArr))
    {
       $where .= "AND mrlp.department IN(".@implode(',',$departmentArr).") ";
    }
     if(count($teamArr) && !in_array('-1',$teamArr))
    {
                $TeamSearchStringArr = array();
		foreach($teamArr as $sTeamVal){
			$TeamSearchStringArr[] = " FIND_IN_SET($sTeamVal,group_id) ";
		}
		if(count($TeamSearchStringArr) > 0 ){
			$TeamSearchString = implode(" OR ", $TeamSearchStringArr);
			$where .= " AND ($TeamSearchString) ";
		}
    }
    
    
    $typeSearch = '';
        foreach($sTypeArr as $sType){
                if($sType == 3){
                        $typeSearch = "course_status = 'not started'";
                }elseif($sType == 2){
                        $typeSearch = "course_status = 'completed'";
                }elseif($sType == 1){
                        $typeSearch = "course_status = 'in progress'";
                }
        }
        if($typeSearch){
           
           $where .= "AND ($typeSearch) ";
        }
		
	if($USER->archetype == $CFG->userTypeAdmin){
		$where .= '';
	}elseif($USER->archetype == $CFG->userTypeManager){
		$teamUser = fetchGroupsUserIds($USER->id,1);
		if(!empty($teamUser)){
			$groupsList =  implode(',',$teamUser);
			$where = " AND (mrlp.department = ".$USER->department." OR mrlpd.user_id IN ($groupsList)) ";
		}else{
			$where .= ' AND (mrlp.department = "'.$USER->department.'")';
		}
	}elseif($USER->archetype == $CFG->userTypeStudent){
		$teamUser = fetchGroupsUserIds($USER->id,1);
		if(!empty($teamUser)){
			$groupsList =  implode(',',$teamUser);
			$where = " AND (mrlpd.user_id IN ($groupsList)) ";
		}else{
			$where .= ' AND (mrlp.department = "'.$USER->department.'")';
		}
	}		
		
	if($sDateSelected && $eDateSelected){
		
		$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
		$sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;

		$where .= " AND ((mrlpd.last_accessed >= $sStartDateTime && mrlpd.last_accessed <= $sEndDateTime ) )";

	}elseif($sDateSelected && $eDateSelected == ''){
		
		$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;

		$where .= " AND (mrlpd.last_accessed >= ($sStartDateTime) )";

	}elseif($sDateSelected =='' && $eDateSelected){
		
		$sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
		$where .= " AND (mrlpd.last_accessed<= ($sEndDateTime))";
	}
        
        $paramArray['key'] = addslashes($paramArray['key']);
        if(($paramArray['key'] != '')) {
		$where .= " AND (mu.firstname like '%".$paramArray['key']."%' OR mu.lastname like '%".$paramArray['key']."%' OR CONCAT(mu.firstname,' ',mu.lastname) like '%".$paramArray['key']."%' OR mu.username like '%".$paramArray['key']."%' OR mu.city like '%".$paramArray['key']."%' OR mu.user_identifier like '%".$paramArray['key']."%'  ) ";
	}
		
	$query = "SELECT mrlpd.user_id,mu.report_to,jt.title,mu.dateofbirth,mu.is_manager_yes,cm.created_on,cm.end_date as enrollmentexpiry,cm.createdby,mu.user_identifier,mu.city,mu.country,mu.state,concat(mrlp.first_name,' ',mrlp.last_name) as fullname,mrlp.user_name,mrlpd.course_id,MAX(last_accessed) as last_access, if(course_status='completed',completion_date,0) as completion_date,if(course_status='not started','Not Started',if(course_status='completed','Completed','In Progress')) as course_status,mrlp.company_name,mrlp.manager_name,mrlp.inline_manager_name,mrlpd.score FROM  mdl_report_learner_performance mrlp LEFT JOIN mdl_report_learner_performance_details mrlpd on (mrlp.user_id=mrlpd.user_id) left join mdl_user as mu on mrlpd.user_id=mu.id LEFT JOIN mdl_user_course_mapping cm ON(mrlpd.course_id=cm.courseid and mrlpd.user_id=cm.userid)";
    //$query = "SELECT mrlpd.user_id,concat(mrlp.first_name,' ',mrlp.last_name) as fullname,mrlp.user_name,mrlpd.course_id,MAX(last_accessed) as last_access, if(course_status='completed',completion_date,0) as completion_date,if(course_status='not started','Not Started',if(course_status='completed','Completed','In Progress')) as course_status,mrlp.company_name,mrlp.manager_name,mrlp.inline_manager_name,mrlpd.score FROM  mdl_report_learner_performance mrlp LEFT JOIN mdl_report_learner_performance_details mrlpd on (mrlp.user_id=mrlpd.user_id)";
	
        $query .=" LEFT JOIN mdl_job_title jt ON(mu.job_title=jt.id)";
        
        $query .= " WHERE 1 = 1";
	$query .= " AND mrlpd.course_id = '".$cid."' ";
	$query .= $where;
	
	$query .= " GROUP BY mrlpd.user_id ";
	$query .= " ORDER BY COALESCE(NULLIF(mrlp.first_name, ''), mrlp.last_name), mrlp.last_name ";
	$queryLimit = $query.$limit;
//echo $queryLimit;die;
	$limitReportsArr = $DB->get_records_sql($queryLimit);
		
        //pr($limitReportsArr);die;
	$completedPercentage = 0;
	$inProgressPercentage = 0;
	$notStartedPercentage = 0;
	$enrolmentCount = getCourseEnrolmentCount($cid,$where);
	$in_progress_count = isset($enrolmentCount['in progress']->course_status_count)?$enrolmentCount['in progress']->course_status_count:0;
	$completed_count = isset($enrolmentCount['completed']->course_status_count)?$enrolmentCount['completed']->course_status_count:0;
	$not_started_count = isset($enrolmentCount['not started']->course_status_count)?$enrolmentCount['not started']->course_status_count:0;
	$userCount = $in_progress_count+$completed_count+$not_started_count;
		
	$completedPercentage = numberFormat(($completed_count*100)/$userCount);
	$inProgressPercentage = numberFormat(($in_progress_count*100)/$userCount);
	$notStartedPercentage = numberFormat(($not_started_count*100)/$userCount);
		
	$in_progress_count_exploded = in_array(1, $sTypeArr)?true:false;
	$completed_count_exploded = in_array(2, $sTypeArr)?true:false;
	$not_started_count_exploded = in_array(3, $sTypeArr)?true:false;
		
	$courseHTML = '';
	$exportHTML = '';
	if(empty($export)){

		$exportHTML .= '<div class="exports_opt_box"> ';
		$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','singlereport').'" href="'.$genPrintURL.'&action=exportcsv">'.get_string('downloadcsv','singlereport').'</a> ';
		$exportHTML .= '<span class="seperater">&nbsp;</span>';
		$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','singlereport').'" id="exportpdf1" href="'.$genPrintURL.'&action=exportpdf">'.get_string('downloadpdf','singlereport').'</a> ';
		$exportHTML .= '<span class="seperater">&nbsp;</span>';
		$exportHTML .= '<a rel="'.$genPrintURL.'&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','singlereport').'" class="print_icon">'.get_string('print','singlereport').'</a>';
		$exportHTML .= '</div>';
			
	}
		
	$style = (!empty($export) || strstr($_SERVER['REQUEST_URI'], 'reports/course_progress_report.php'))?"style=''":'';
	
	$courseHTML .= "<div class='tabsOuter' ".$style." >";
	if(empty($export) && $sBack == 6){

		$courseHTML .= "<div class='tabLinks'>";
		ob_start();
		$course->id = $cid;
		include_once($CFG->dirroot . '/course/course_tabs.php');
		$HTMLTabs = ob_get_contents();
		ob_end_clean();
		$courseHTML .= $HTMLTabs;
		$courseHTML .= "</div>";
	}
		
	$courseHTML .= "<div class='borderBlockSpace'>";
		
	if(empty($export) && !strstr($_SERVER['REQUEST_URI'], 'course/singlecoursereport.php')){
		ob_start();
		require_once($CFG->dirroot . '/local/includes/course_report_details_search.php');
		$SEARCHHTML = ob_get_contents();
		ob_end_clean();
		$courseHTML .= $SEARCHHTML;
	}

	$courseHTML .= '<div class="clear"></div>';
	$courseHTML .= '<div class="userprofile view_assests">';
		
	$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
	$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
	$sDateSelectedForPrintCSV = getDateFormatForPrint($sDateSelected, 'csv');
	$eDateSelectedForPrintCSV = getDateFormatForPrint($eDateSelected, 'csv');

	if(!empty($export)){
		$reportName = get_string('overalluserprogressreport','singlecoursereport');
		$courseHTML .= getReportPrintHeader($reportName);
		$courseHTML .= '<table border="0" cellpadding="0" cellspacing="0"  class="generaltable margin_bottom">';
		$courseHTML .= '<tr>';
                    $courseHTML .= '<td colspan="2"><strong>'.get_string('course','singlecoursereport').':</strong> '.$courseName.'</td>';
		$courseHTML .= '</tr>';
			
		$courseHTML .= '<td width="50%"><strong>'.get_string('lastaccesseddatefrom').':</strong> '.$sDateSelectedForPrint.'</td>';
		$courseHTML .= '<td width="50%" ><strong>'.get_string('lastaccesseddateto').':</strong>  '.$eDateSelectedForPrint.'</td>';
		$courseHTML .= '</tr>';
		$courseHTML .= '<tr>';
		$courseHTML .= '<td><strong>'.get_string('notstarted','singlecoursereport').':</strong> '.$not_started_count.' ('.floatval($notStartedPercentage).'%)</td>';
		$courseHTML .= '<td><strong>'.get_string('inprogress','singlecoursereport').':</strong> '.$in_progress_count.' ('.floatval($inProgressPercentage).'%)</td>';
		$courseHTML .= '</tr>';
		$courseHTML .= '<tr>';
		$courseHTML .= '<td ><strong>'.get_string('completed','singlecoursereport').':</strong> '.$completed_count.' ('.floatval($completedPercentage).'%)</td>';
		$courseHTML .= '<td><strong>'.get_string('numberofusers','singlecoursereport').':</strong> '.$userCount.'</td>';
		$courseHTML .= '</tr>';
		$courseHTML .= '</table>';
	}
	$courseHTML .= '<div class="f-right">'.get_string('reports_heading','',getReportSyncTime()).'</div>';
	$courseHTML .= '<div class="clear"></div>';
	if($userCount > 0){
			
		$courseHTML .= ' <div class = "single-report-start" id="watch">
						<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('overalluserprogressreport','singlecoursereport').$exportHTML.'</span>
						  <div class="single-report-graph-left" id="chartContainer">'.get_string('loading','singlecoursereport').'</div>
						  <div class="single-report-graph-right">
							<div class="course-count-heading">'.get_string('numberofusers','singlecoursereport').'</div>
							<div class="course-count">'.$userCount.'</div>
							<div class="seperator">&nbsp;</div>
							<div class="course-status">
							  <div class="notstarted"><h6>'.get_string('notstarted','singlecoursereport').'</h6><span>'.$not_started_count.'</span></div>
							  <div class="clear"></div>
							  <div class="inprogress"><h6>'.get_string('inprogress','singlecoursereport').'</h6><span>'.$in_progress_count.'</span></div>
							  <div class="clear"></div>
							  <div class="completed"><h6>'.get_string('completed','singlecoursereport').'</h6><span>'.$completed_count.'</span></div>
							</div>
						  </div>
						</div>
					  </div>
					  <div class="clear"></div>';
	}
		
		
		
		
	$courseHTML .= '<div class="">';
	$courseHTML .= '<table  class="generaltable" cellpadding="0" border="0" cellspacing="0"></tbody>';
	$courseHTML .= '<tr>';
		
	if($CFG->showInlineManagerNCompany == 1 && !empty($export) && $export == 'print'){
		//$courseHTML .=  '<th width="10%">'.get_string('fullname','singlecoursereport').'</th>';
                $courseHTML .=  '<th width="8%">'.get_string('fullname','singlecoursereport')."(".get_string('username').")".'</th>';
		$courseHTML .=  '<th width="5%">'.get_string('jobtitle').'</th>';
                $courseHTML .=  '<th width="10%">'.get_string('report_to').'</th>';
                $courseHTML .=  '<th width="8%">'.get_string('user_identifier').'</th>';
                $courseHTML .=  '<th width="10%">'.get_string('location').'</th>';
                $courseHTML .=  '<th width="8%">'.get_string('ismanageryescsv').'</th>';
                $courseHTML .=  '<th width="7%">'.get_string('enrolmentdate','singlecoursereport').'</th>';
                $courseHTML .=  '<th width="10%">'.get_string('enrolmentexpirydate').'</th>';
		$courseHTML .=  '<th width="10%">'.get_string('company').'</th>';
		$courseHTML .=  '<th width="11%">'.get_string('inlinemanager').'</th>';
		$courseHTML .=  '<th width="11%">'.get_string('status','singlecoursereport').'</th>';
		$courseHTML .=  '<th width="10%">'.get_string('score','singlecoursereport').'</th>';
		$courseHTML .=  '<th width="10%">'.get_string('comletiondate').'</th>';
		$courseHTML .=  '<th width="10%">'.get_string('lastaccessed','singlecoursereport').'</th>';
                
	}else{
            
		$courseHTML .=  '<th width="7%">'.get_string('fullname','singlecoursereport')." (".get_string('username').")".'</th>';
		$courseHTML .=  '<th width="5%">'.get_string('jobtitle').'</th>';
                $courseHTML .=  '<th width="10%">'.get_string('report_to').'</th>';
                $courseHTML .=  '<th width="8%">'.get_string('user_identifier').'</th>';
                $courseHTML .=  '<th width="10%">'.get_string('location').'</th>';
                $courseHTML .=  '<th width="9%">'.get_string('ismanageryescsv').'</th>';
                $courseHTML .=  '<th width="8%">'.get_string('dateofbirth').'</th>';
                $courseHTML .=  '<th width="8%">'.get_string('enrolmentdate','singlecoursereport').'</th>';
                 $courseHTML .=  '<th width="9%">'.get_string('enrolmentexpirydate').'</th>';
		$courseHTML .=  '<th width="7%">'.get_string('status','singlecoursereport').'</th>';
		$courseHTML .=  '<th width="3%">'.get_string('score','singlecoursereport').'</th>';
		$courseHTML .=  '<th width="8%">'.get_string('comletiondate').'</th>';
		$courseHTML .=  '<th width="8%">'.get_string('lastaccessed','singlecoursereport').'</th>';
                
	}
		
	$courseHTML .= '</tr>';
		
		

	$reportContentCSV = '';
	$reportContentCSV .= get_string('course','singlecoursereport').",".$courseName."\n";
	$reportContentCSV .= get_string('numberofusers','singlecoursereport').",".$userCount."\n";
	$reportContentCSV .= get_string('notstarted','singlecoursereport').",".$not_started_count." (".floatval($notStartedPercentage)."%)\n";
	$reportContentCSV .= get_string('inprogress','singlecoursereport').",".$in_progress_count." (".floatval($inProgressPercentage)."%)\n";
	$reportContentCSV .= get_string('completed','singlecoursereport').",".$completed_count." (".floatval($completedPercentage)."%)\n";
		
	$reportContentCSV .= get_string('lastaccesseddatefrom').",".$sDateSelectedForPrintCSV."\n".get_string('lastaccesseddateto').",".$eDateSelectedForPrintCSV."\n";
	$reportContentCSV .= get_string('overalluserprogressreport','singlecoursereport')."\n";
	// $reportContentCSV .= get_string('fullname','singlecoursereport')." (".get_string('username')."),".get_string('status','singlecoursereport').",".get_string('score','singlecoursereport').",".get_string('lastaccessed','singlecoursereport')."\n";

	if($CFG->showInlineManagerNCompany == 1){
		$reportContentCSV .= get_string('fullname','singlecoursereport').",".get_string('username').",".get_string('user_identifier').",".get_string('ismanageryescsv').",".get_string('jobtitle').",".get_string('report_to').",".get_string('country').",".get_string('city').",".get_string('dateofbirth').",".get_string('enrolmentdate','singlecoursereport').",".get_string('enrolmentexpirydate').",".get_string('company').",".get_string('inlinemanager').",".get_string('status','singlecoursereport').",".get_string('score','singlecoursereport').",".get_string('comletiondate').",".get_string('lastaccessed','singlecoursereport')."\n";
	}else{
		$reportContentCSV .= get_string('fullname','singlecoursereport').",".get_string('username').",".get_string('user_identifier').",".get_string('ismanageryescsv').",".get_string('jobtitle').",".get_string('report_to').",".get_string('country').",".get_string('city').",".get_string('dateofbirth').",".get_string('enrolmentdate','singlecoursereport').",".get_string('enrolmentexpirydate').",".get_string('status','singlecoursereport').",".get_string('score','singlecoursereport').",".get_string('comletiondate').",".get_string('lastaccessed','singlecoursereport')."\n";
	}
		
	$reportContentPDF = '';
	$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0"  class="generaltable">';
	$reportContentPDF .= '<tr>';
	$reportContentPDF .= '<td colspan="2"><strong>'.get_string('course','singlecoursereport').':</strong> '.$courseName.'</td>';
	
	$reportContentPDF .= '</tr>';


	$reportContentPDF .= '<tr>';
	$reportContentPDF .= '<td><strong>'.get_string('lastaccesseddatefrom').':</strong> '.$sDateSelectedForPrint.'</td>';
	$reportContentPDF .= '<td><strong>'.get_string('lastaccesseddateto').':</strong>  '.$eDateSelectedForPrint.'</td>';
	$reportContentPDF .= '</tr>';
	$reportContentPDF .= '<tr>';
	$reportContentPDF .= '<td><strong>'.get_string('notstarted','singlecoursereport').':</strong> '.$not_started_count.' ('.floatval($notStartedPercentage).'%)</td>';
	$reportContentPDF .= '<td><strong>'.get_string('inprogress','singlecoursereport').':</strong> '.$in_progress_count.' ('.floatval($inProgressPercentage).'%)</td>';
	$reportContentPDF .= '</tr>';
	$reportContentPDF .= '<tr>';
	
	$reportContentPDF .= '<td ><strong>'.get_string('completed','singlecoursereport').':</strong> '.$completed_count.' ('.floatval($completedPercentage).'%)</td>';
	$reportContentPDF .= '<td><strong>'.get_string('numberofusers','singlecoursereport').':</strong> '.$userCount.'</td>';
	$reportContentPDF .= '</tr>';

	//$reportContentPDF .= '<tr><br /><td width="100%"  ><span '.$CFG->pdfSpanAttribute.'><strong>'.get_string('overalluserprogressreport','singlecoursereport').'</strong></span><br /></td></tr>';
	$reportContentPDF .= '</table>';

	if(!empty($export) && $export == 'exportpdf' && strstr($_SERVER['REQUEST_URI'], 'reports/course_progress_report_print.php')){
		$reportContentPDF .= getGraphImageHTML(get_string('overalluserprogressreport','singlecoursereport'));
	}else{
		$reportContentPDF .= getGraphImageHTML(get_string('viewreportdetails','multicoursereport'));
	}





	$htmlSpace = getSpace();
	$htmlSpaceCSV = getSpace('csv');
	//pr($limitReportsArr);
		
	$extraParams = "startDate=".$sStartDate."&endDate=".$sEndDate;
	if($userCount > 0){
			
		$curtime = time();
		$i=0;
               
		foreach($limitReportsArr as $data){
			$i++;
			//$classEvenOddR = $i%2==0 ?'evenR':'oddR';
                        $uniqueID = (trim($data->user_identifier))?trim($data->user_identifier):getMDash();
                        $uniqueIDCSV = (trim($data->user_identifier))?trim($data->user_identifier):getMDashForCSV();
			$classEvenOddR = 'oddR';
			$courseStatus = $data->course_status;
                        
                        if($courseStatus == 'Completed'){
                           // pr($data);die;
                            
                            $completed_on = $data->completion_date;
                            $isCertificateEnabled = isCertificateEnabled($data->course_id,'course');
                            if($isCertificateEnabled==1 && empty($export)){
                                $certificateLink = certificateLink($data->course_id,'course',$completed_on,$data->user_id);
                                $courseStatus .= $certificateLink;
                            }
                        }
                        
                        
                       // echo $certificateLink;
			$manager_name = '';
			if($CFG->showInlineManagerNCompany==1){
				$manager_name = $data->inline_manager_name;
				if(trim($manager_name)==''){
					$manager_name = $data->manager_name;
				}
			$inlineCompany = $data->company_name?$data->company_name:(!empty($export) && $export == 'exportcsv'?getMDashForCSV():getMDash());
			}else{
				$manager_name = $data->manager_name;
			}
			//$manager_name = '';
			$manager_nameCSV = $manager_name?$manager_name:getMDashForCSV();
			$manager_name = $manager_name?$manager_name:getMDash();
			$courseScore =  $data->score;
			//$lastAccessed = getMDash();
				
			$completionDate = $data->completion_date;
			if($completionDate){
				$completionDateTime = getDateFormat($completionDate, $CFG->customDefaultDateTimeFormat);
				$completionDateTimeCSV = getDateFormat($completionDate, $CFG->customDefaultDateTimeFormatForCSV);

			}else{
				$completionDateTime = getMDash();
				$completionDateTimeCSV = getMDashForCSV();
			}
                       // pr($data);die;
                        $hiredate = $data->dateofbirth;
			if($hiredate){
				$hireDateTime = getDateFormat($hiredate, $CFG->customDefaultDateTimeFormat);
				$hireDateTimeCSV = getDateFormat($hiredate, $CFG->customDefaultDateTimeFormatForCSV);

			}else{
				$hireDateTime = getMDash();
				$hireDateTimeCSV = getMDashForCSV();
			}
			
                        
			$lastAccessedTime = $data->last_access;
			if($lastAccessedTime){
				$lastAccessed = getDateFormat($lastAccessedTime, $CFG->customDefaultDateTimeFormat);
				$lastAccessedCSV = getDateFormat($lastAccessedTime, $CFG->customDefaultDateTimeFormatForCSV);

			}else{
				$lastAccessed = get_string('notaccessed','learnercourse');
				$lastAccessedCSV = get_string('notaccessed','learnercourse');
			}

			$userId = $data->user_id;
			$fullName = $data->fullname;
			$userName = $data->user_name;
			//$imagevalue = $OUTPUT->user_picture($user, array('size'=>64));
                        $fullnameUsername = $fullName."<br/>(".$userName.")";
			$courseHTML .=  '<tr class="'.$classEvenOddR.'" >';
			if(!empty($export)){
				$courseHTML .=  '<td >'.$fullnameUsername.'</td>';
				//$courseHTML .=  '<td>'.$userName.'</td>';
			}else{
				if(!$sBack){
					$sBack = 3;
				}
				$userDetailsLink = $CFG->wwwroot."/reports/user_performance_report.php?uid=".$userId."&cid=".$cid."&back=".$sBack.'&'.$extraParams;
				$courseHTML .=  '<td ><a href="'.$userDetailsLink.'">'.$fullnameUsername.'</a></td>';
				//$courseHTML .=  '<td><a href="'.$userDetailsLink.'" >'.$userName.'</a></td>';
			}
                       //pr($data);die;
                        
                        if(isset($data->report_to) && $data->report_to != ''){                               
                                $report_to = $data->report_to;                                
                        }else{
                            if(!empty($export)){
                                $report_to = getMDashForCSV();
                            }else{
                                $report_to = getMDash();
                            }
                        }
                        if(isset($data->title) && $data->title != ''){                               
                                $job_title = $data->title;                                
                        }else{
                            if(!empty($export)){
                                $job_title = getMDashForCSV();
                            }else{
                                $job_title = getMDash();
                            }
                        }
                        
                        $courseHTML .=  '<td class="email-word-wrap">'.$job_title.'</td>';
                        $courseHTML .=  '<td class="email-word-wrap">'.$report_to.'</td>';
			$userLocation = '';
                        if(isset($data->country) && $data->country != ''){                               
                                $userLocation = "<strong>".get_string("country").":</strong>".get_string($data->country, 'countries')."<br/>";                                
                        }else{
                            if(!empty($export)){
                                $country = getMDashForCSV();
                            }else{
                                //$userLocation = getMDash();
                            }
                        }

                        if(isset($data->city) && $data->city != ''){                               
                                $userLocation .=  "<strong>".get_string("city").":</strong>".$data->city;
                                $city = $data->city; 
                        }else{
                            if(!empty($export)){
                                //$userLocation = getMDashForCSV();
                                $city = getMDashForCSV();
                            }else{
                                //$userLocation = getMDash();
                            }
                        }
                        
                        if(!$userLocation){
                            $userLocation = getMDash();;
                        }
                        
                        $courseHTML .=  '<td class="email-word-wrap">'.$uniqueID.'</td>';
                        $courseHTML .=  '<td class="email-word-wrap">'.$userLocation.'</td>';
                        $courseHTML .=  '<td class="email-word-wrap">'.$CFG->isManagerYesOptions[$data->is_manager_yes].'</td>';
                       
                        $courseHTML .=  '<td class="email-word-wrap">'.$hireDateTime.'</td>';
                        
                        $EnrolledDates = getDateFormat($data->created_on, $CFG->customDefaultDateTimeFormat);
                        $EnrolledDatesCSV = getDateFormat($data->created_on, $CFG->customDefaultDateTimeFormatForCSV);

                         $courseHTML .=  '<td class="email-word-wrap">'.$EnrolledDates.'</td>';
                         
                        if($data->enrollmentexpiry){
                            $EnrollmentExpiry = getDateFormat($data->enrollmentexpiry, $CFG->customDefaultDateTimeFormat);
                            $EnrollmentExpiryCSV = getDateFormat($data->enrollmentexpiry, $CFG->customDefaultDateTimeFormatForCSV);
                        }else{
                            $EnrollmentExpiry = getMDash();
                            $EnrollmentExpiryCSV = getMDashForCSV();
                        }
                        $courseHTML .=  '<td class="email-word-wrap">'.$EnrollmentExpiry.'</td>';
                         
			if($CFG->showInlineManagerNCompany == 1 && !empty($export) && $export == 'print' ){
				$courseHTML .=  '<td class="email-word-wrap">'.$inlineCompany.'</td>';
				$courseHTML .=  '<td class="email-word-wrap">'.$manager_name.'</td>';
			}
                        
                        $score = ($courseScore?$courseScore:getMDash());
                      
                       
                                                
                        
			$courseHTML .=  '<td class="email-word-wrap">'.$courseStatus.'</td>';
			$courseHTML .=  '<td class="email-word-wrap">'.($courseScore?$courseScore:getMDash()).'</td>';
                        
			$courseHTML .=  '<td class="email-word-wrap">'.$completionDateTime.'</td>';
			$courseHTML .=  '<td class="email-word-wrap">'.$lastAccessed.'</td>';
                       
			$courseHTML .=  '</tr>';

			//$courseHTML .=  '<tr><td colspan="5">';
			
			//$reportContentCSV .= $fullName." (".$userName."),".$courseStatus.",".($courseScore?$courseScore:getMDashForCSV()).",".(getMDashForCSV())."\n";
			$scoreCSV = $courseScore?$courseScore:getMDashForCSV();
                        
                        $isManagerYes = $CFG->isManagerYesOptions[$data->is_manager_yes];
                        
			if($CFG->showInlineManagerNCompany == 1){
				$reportContentCSV .= $fullName.",".$userName.",".$uniqueIDCSV.",".$isManagerYes.",".str_replace(',',' | ',$job_title).",".$report_to.",".$country.",".str_replace(",",' ',$city).",".$hireDateTimeCSV.",".$EnrolledDatesCSV.",".$EnrollmentExpiryCSV.",".$inlineCompany.",".$manager_nameCSV.",".$courseStatus.",".$scoreCSV.",".$completionDateTimeCSV.",".$lastAccessedCSV."\n";
			}else{
				$reportContentCSV .= $fullName.",".$userName.",".$uniqueIDCSV.",".$isManagerYes.",".str_replace(',',' | ',$job_title).",".$report_to.",".$country.",".str_replace(",",' ',$city).",".$hireDateTimeCSV.",".$EnrolledDatesCSV.",".$EnrollmentExpiryCSV.",".$courseStatus.",".$scoreCSV.",".$completionDateTimeCSV.",".$lastAccessedCSV."\n";
			}
			

			$assestHTML = '';
			$courseHTML .=  $assestHTML;
                        
                       // echo $courseHTML;die;
			// $courseHTML .= '</td></tr>';

		}


		//if($notStartedPercentage || $inProgressPercentage || $completedPercentage){
		$courseHTML .=  '<script language="javascript" type="text/javascript">';
		$courseHTML .=  '	$(document).ready(function(){

								$("#printbun").bind("click", function(e) {
									var url = $(this).attr("rel");
									window.open(url, "'.get_string('coursevsmultipleuserreport','singlecoursereport').'", "'.$CFG->printWindowParameter.'");
								});

						     }); ';


		$courseHTML .=  ' window.onload = function () {
			
										var chart = new CanvasJS.Chart("chartContainer",
										{
											title:{
												text: ""
											},
											theme: "theme2",
						       
											data: [
											{
												type: "doughnut",
												indexLabelFontFamily: "Arial",
												indexLabelFontSize: 12,
												startAngle:0,
												indexLabelFontColor: "dimgrey",
												indexLabelLineColor: "darkgrey",
												toolTipContent: "{y}%",
					
											
												dataPoints: [
												{  y: '.$inProgressPercentage.', label: "'.get_string('inprogress','singlereport').' ('.$inProgressPercentage.'%)", exploded:false },
												{  y: '.$notStartedPercentage.', label: "'.get_string('notstarted','singlereport').' ('.$notStartedPercentage.'%)", exploded:false},
												{  y: '.$completedPercentage.', label: "'.get_string('completed','singlereport').' ('.$completedPercentage.'%)", exploded:false },
					
												]

												/*dataPoints: [
												{  y: '.$notStartedPercentage.', label: "'.get_string('notstarted','singlereport').' {y}%" },
												{  y: '.$inProgressPercentage.', label: "'.get_string('inprogress','singlereport').' {y}%" },
												{  y: '.$completedPercentage.', label: "'.get_string('completed','singlereport').' {y}%" },
					
												]*/
											}
											]
						
										});';
			
		if($notStartedPercentage || $inProgressPercentage || $completedPercentage){
			$courseHTML .=  '	chart.render(); ';
		}
			
		$courseHTML .=  '  }
									</script>
									<script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js"></script>';
			
		//}

	}else{
		$courseHTML .=  '<tr><td colspan="12" align="center" >'.get_string('no_results').'</td></tr>';
		$reportContentCSV .= get_string('norecordfound','singlecoursereport')."\n";
		$reportContentPDF .= get_string('norecordfound','singlecoursereport')."\n";
	}
		
	$reportContentPDF .= '</table>';
		
	$courseHTML .= '</tbody></table></div></div></div></div>';

	if(empty($export)){
			
		$courseHTML .= paging_bar($userCount, $page, $perpage, $genURL);
		$styleSheet = $userCount?'':'style=""';
		if(strstr($_SERVER['REQUEST_URI'], 'course/coursereportdetails.php')){
				
			if($sBack == 1){
				$backUrl = $CFG->wwwroot.'/course/course_report.php';
			}else{
				$backUrl = $CFG->wwwroot.'/course/multicoursereport.php?'.$extraParams;
			}
				
			//$courseHTML .= '<div id="backcell"><input type = "button" value = "'.get_string('back','multicoursereport').'" '.$styleSheet.' onclick="location.href=\''.$backUrl.'\';"></div>';
		}else{
			$urlCancel = new moodle_url($CFG->wwwroot.'/course/courseview.php', array('id'=>$cid));
			//$courseHTML .= '<div id="backcell"><input type = "button" value = "'.get_string('cancel','singlecoursereport').'" '.$styleSheet.' onclick="location.href=\''.$urlCancel.'\';"></div>';
		}
	}
		
	$courseHTML .= '';
	$courseHTML .=  '';

	$courseVsMultipleUserReport->courseHTML = $courseHTML;
	$courseVsMultipleUserReport->reportContentCSV = $reportContentCSV;
	$courseVsMultipleUserReport->reportContentPDF = $reportContentPDF;
		
	return $courseVsMultipleUserReport;
}
?>
