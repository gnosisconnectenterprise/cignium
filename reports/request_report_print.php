<?php
	set_time_limit(0);
	require_once("../config.php");
	require_once ($CFG->dirroot . '/mod/scheduler/lib.php');
	require_once($CFG->dirroot . '/reports/lib.php');		
	if( $USER->archetype == $CFG->userTypeStudent ) {
	 redirect(new moodle_url('/my/learnerdashboard.php'));
	}	
	checkLogin();		
	
	$PAGE->set_pagelayout('print');
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('requestsreport'));

	$sort    = optional_param('sort', 'fullname', PARAM_RAW);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 0, PARAM_INT);        // how many per page
	$export = optional_param('action', '', PARAM_ALPHANUM);
	
	$pageURL = '/my/request_report.phpp';
	checkPageIsNumeric($pageURL,$_REQUEST['page']);
				
	$sDate = '';
	$eDate = '';
		
	$sDepartment    = optional_param('department', '-1', PARAM_RAW);
	$sTeam          = optional_param('team', '-1', PARAM_RAW);  
	$sProgram       = optional_param('program', '-1', PARAM_RAW);     
	$sCourse        = optional_param('course', '-1', PARAM_RAW);       
	$sType          = optional_param('type', '-1', PARAM_RAW); 
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate       = optional_param('endDate', $eDate, PARAM_RAW);
	$sType          = optional_param('type', '-1', PARAM_RAW); 
	$selMode		= optional_param('sel_mode', '1' , PARAM_RAW);
	$userGroup      = optional_param('user_group', '-1', PARAM_RAW);
	$sUser      = optional_param('user', '-1', PARAM_RAW);
	$sClasses      = optional_param('classes', '-1', PARAM_RAW);	
	
	$paramArray = array(
					'department' => $sDepartment,
					'team' => $sTeam,
					'program' => $sProgram,
					'course' => $sCourse,
					'type' => $sType,
					'startDate' => $sStartDate,
					'endDate' => $sEndDate,
					'sel_mode' => $selMode,
					'user_group' => $userGroup,
					'user' => $sUser,
					'classes' => $sClasses,
					'job_title' => optional_param('job_title', '-1', PARAM_RAW),
					'company' => optional_param('company', '-1', PARAM_RAW)
				  );
	
	$removeKeyArray = array();	
	
	$requestHTML = '';
	$reportContentCSV = '';
	$reportContentPDF = '';
    if($export && in_array($export, array('exportcsv', 'exportpdf', 'print'))){
		$globalRequestReport = getGlobalRequestReport_scheduler($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
		$requestHTML = $globalRequestReport->requestHTML;
		$reportContentCSV = $globalRequestReport->reportContentCSV;
		$reportContentPDF = $globalRequestReport->reportContentPDF;
    }else{
	    redirect(new moodle_url('/'));
	}

	/* bof export to csv */
	
    if(isset($export) && $export == 'exportcsv') {	
		$filepathname = getpdfCsvFileName('csv', get_string('requestsreport'), $reportContentCSV);
   	    exportCSV($filepathname);
	}
	/* eof export to csv */	
	
	
	/* bof export to pdf */
    if(isset($export) && $export == 'exportpdf') {			
		$filename = str_replace(' ', '_', get_string('requestsreport'))."_".date("m-d-Y").".pdf";
		exportPDF($filename, $reportContentPDF, '', get_string('requestsreport'));		
	}
	/* eof export to pdf */	
		
	   
	echo $OUTPUT->header(); 
	echo $requestHTML;

?>
<style>
#page { margin: 20px auto 0;}
</style>