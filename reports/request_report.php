<?php
	require_once("../config.php");
	require_once ($CFG->dirroot . '/mod/scheduler/lib.php');
	require_once($CFG->dirroot . '/reports/lib.php');
	if( $USER->archetype == $CFG->userTypeStudent ) {
	 redirect(new moodle_url('/my/learnerdashboard.php'));
	}	
	checkLogin();	 
	$PAGE->set_pagelayout('reportlayout');
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('requestsreport'));
	$PAGE->navbar->add(get_string('reports','multicoursereport'));
	$PAGE->navbar->add(get_string('requestsreport'));
	
	$sort    = optional_param('sort', 'fullname', PARAM_RAW);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', $CFG->perpage, PARAM_INT);        // how many per page
	$export = optional_param('action', '', PARAM_ALPHANUM);
	
	$pageURL = '/reports/request_report.phpp';
	checkPageIsNumeric($pageURL,$_REQUEST['page']);
				
	$sDate = '';
	$eDate = '';
	
	$sDepartment    = optional_param('department', '-1', PARAM_RAW);
	$sTeam          = optional_param('team', '-1', PARAM_RAW);  
	$sProgram       = optional_param('program', '-1', PARAM_RAW);     
	$sCourse        = optional_param('course', '-1', PARAM_RAW);       
	$sType          = optional_param('type', '-1', PARAM_RAW); 
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate       = optional_param('endDate', $eDate, PARAM_RAW);
	$sType          = optional_param('type', '-1', PARAM_RAW); 
	$selMode		= optional_param('sel_mode', '1' , PARAM_RAW);
	$userGroup      = optional_param('user_group', '-1', PARAM_RAW);
	$sUser          = optional_param('user', '-1', PARAM_RAW);
	$sClasses       = optional_param('classes', '-1', PARAM_RAW);	
	
	$paramArray = array(
					'department' => $sDepartment,
					'team' => $sTeam,
					'program' => $sProgram,
					'course' => $sCourse,
					'type' => $sType,
					'startDate' => $sStartDate,
					'endDate' => $sEndDate,
					'sel_mode' => $selMode,
					'user_group' => $userGroup,
					'user' => $sUser,
					'classes' => $sClasses,
					'job_title' => optional_param('job_title', '-1', PARAM_RAW),
					'company' => optional_param('company', '-1', PARAM_RAW)
				  );
	
	$removeKeyArray = array();
  
	echo $OUTPUT->header(); 	

	$globalRequestReport = getGlobalRequestReport_scheduler($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
	$requestHTML = $globalRequestReport->requestHTML;
	echo $requestHTML;
	echo includeGraphFiles(get_string('requestsreport'));
	echo $OUTPUT->footer();
