<?php
require_once("../config.php");
require_once("lib.php");
global $DB,$CFG,$USER;
checkLogin();
$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);
$pageURL = '/reports/class_usage_report.php';
checkPageIsNumeric($pageURL,$_REQUEST['page']);

$page    = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 10, PARAM_INT);        // how many per page
$cid     = required_param('cid', PARAM_INT);
$classId     = optional_param('class_id', 0, PARAM_INT);
if($classId == 0 || $classId == ''){
	$classId = $DB->get_record_sql('SELECT id FROM mdl_scheduler WHERE course = '.$cid.' ORDER BY name asC LIMIT 0,1')->id;
}
$action  = optional_param('action', '', PARAM_ALPHANUM);
$type  = optional_param('type', '-1', PARAM_RAW);
$status  = optional_param('status', '-1', PARAM_RAW);
$back  = optional_param('back', 2, PARAM_INT);

$sTypeArr = explode("@",$type);
$removeKeyArray = array('perpage');
checkLogin();
if($action == 'print'){
	$PAGE->set_pagelayout('print');
}else{
	$PAGE->set_pagelayout('reportlayout');
}
$PAGE->set_heading($SITE->fullname);

$PAGE->set_title($SITE->fullname.": ".get_string('viewreportdetails','multicoursereport'));


if($back == 1){
	$PAGE->navbar->add(get_string('reports','multicoursereport'));
	$PAGE->navbar->add(get_string('coursereport','classroomreport'), new moodle_url($CFG->wwwroot.'/course/course_report.php'));
}if($back == 3){
	$PAGE->navbar->add(get_string('managecourses'), new moodle_url($CFG->wwwroot.'/course/index.php'));
}else{
	$PAGE->navbar->add(get_string('reports','multicoursereport'));
	$PAGE->navbar->add(get_string('courseusagesreport','classroomreport'), new moodle_url($CFG->wwwroot.'/reports/classroom_course_usage_report.php')); 
} 
$PAGE->navbar->add(getCourses($cid));
$haveAccess = haveClassroomCourseAccess($cid);
if($haveAccess == 0){
	redirect($CFG->wwwroot);
}
$removeKeyArray = array();
if($classId){
	 include_once($CFG->wwwpath."/mod/scheduler/lib.php");
	 $classDetails = getClassDetailsFromModuleId($classId);
	 $headerHtml = getModuleHeaderHtml($classDetails, $CFG->classModule);
}
$chartDataSql = "SELECT * FROM mdl_reports_classroom_report r WHERE r.class_id = ".$classId;
$chartData = $DB->get_record_sql($chartDataSql);
$is_class_submitted = $chartData->class_submitted;
$userCount = $enrollentCount->total_records;
if($chartData->class_submitted != 1){
	$chartData->nodata = $chartData->enrolled;
	$chartData->no_show = 0;
	$chartData->incomplete = 0;
	$chartData->completed = 0;
}else{
	$chartData->nodata = 0;
}
$paramArray = array(
	'cid' => $cid,
	'type' => $type,
	'status' => $status,
	'back' => $back,
	'class_id' => $classId,
	'class_submitted' => $chartData->class_submitted
);
$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
$arrEnrollmentData['graph_heading'] = get_string('overalluserprogressreport','classroomreport');
$arrEnrollmentData['total']['string'] = get_string('numberofcourses','multiuserreport');
$arrEnrollmentData['total']['value'] = $chartData->enrolled;
$arrEnrollmentData['data'][0]['string'] = get_string('inprogress','classroomreport');
$arrEnrollmentData['data'][0]['class'] = 'inprogress';
$arrEnrollmentData['data'][0]['value'] = $chartData->incomplete;
$arrEnrollmentData['data'][1]['string'] = get_string('classnoshow','classroomreport');
$arrEnrollmentData['data'][1]['class'] = 'notstarted';
$arrEnrollmentData['data'][1]['value'] = $chartData->no_show;
$arrEnrollmentData['data'][2]['string'] = get_string('completed','classroomreport');
$arrEnrollmentData['data'][2]['class'] = 'completed';
$arrEnrollmentData['data'][2]['value'] = $chartData->completed;
$arrEnrollmentData['data'][3]['string'] = get_string('classnodata','classroomreport');
$arrEnrollmentData['data'][3]['class'] = 'nodata';
$arrEnrollmentData['data'][3]['value'] = $chartData->nodata;
$arrEnrollmentData['url'] = $genURL;
$arrEnrollmentData['action'] = $action;
if($action == 'print'){
	$sTypeArr = explode("@",$type);
	$blankArrayType = array('-1');
	if(in_array($sTypeArr[0],$blankArrayType)){
	  $sTypeName = get_string('all','classroomreport');
	}else{
	  $repTypeArr = array(0=>get_string('inprogress','classroomreport'),1=>get_string('completed','classroomreport'), get_string('classnoshow','classroomreport'));
	  $sTypeArr2 = str_replace(array(0, 1, 2), $repTypeArr, $sTypeArr); 
	  $sTypeName = implode(", ", $sTypeArr2);
	}
	$reportName = get_string('overalluserprogressreport','classroomreport');
	$printHeader1 = getReportPrintHeader($reportName);
	$printHeader = '<table border="0" cellpadding="0" cellspacing="0" class="generaltable margin_bottom" >';
	$printHeader .= '<tr>';
	$printHeader .= '<td width="50%"><strong>'.get_string('name','scheduler').':</strong> '.$chartData->class_name.'</td>';
	$printHeader .= '<td width="50%"><strong>'.get_string('status','classroomreport').':</strong> '.$sTypeName.'</td>';
	$printHeader .= '</tr>';
	$printHeader .= '</table>';
	$perpage = 0;
}elseif($action == 'exportpdf' || $action == 'exportcsv'){
	exportClassReportPdf($chartData,$paramArray,$action);
}
echo $OUTPUT->header();
$enrolledData = getClassroomReportData($paramArray,$page,$perpage,$userId);
$enrollentCount = getClassroomReportData($paramArray,$page,0,$userId,'count');
$arrSessions = array();
$arrUser = array();
if(!empty($enrolledData)){
	foreach($enrolledData as $enrolled){
		$arrUser[] = $enrolled->userid;
	}
	$arrSessions = getUserSessionData($arrUser, $classId);
}
$sessionHeaderSql = "SELECT r.session_id as id,r.sessionname,r.session_starttime,r.session_endtime,r.session_duration FROM mdl_reports_classroom_details r WHERE r.scheduler_id = ".$classId." GROUP BY r.session_id";
$sessionHeaderData = $DB->get_records_sql($sessionHeaderSql);
$cntSession = count($sessionHeaderData);

echo '<div class="tabsOuter attendance-performance">';
if($action != 'print'){
	if(empty($export) && $back == 3){
		$courseHTML .= "<div class='tabLinks'>";
		ob_start();
		$course->id = $cid;
		include_once($CFG->dirroot . '/course/course_tabs.php');
		$HTMLTabs = ob_get_contents();
		ob_end_clean();
		$courseHTML .= $HTMLTabs;
		$courseHTML .= "</div>";
		echo $courseHTML;
	}
	echo '<div class="clear"></div>';
}
echo '<div class="borderBlockSpace">';
if($action == 'print'){
	echo $printHeader1;
}
echo $headerHtml;
if($action == 'print'){
	echo $printHeader;
}
if($action != 'print'){
	ob_start();
	require_once($CFG->dirroot . '/local/includes/classroom_report_details_search.php');
	$SEARCHHTML = ob_get_contents();
	ob_end_clean();
	echo $SEARCHHTML;
}

echo '<div class="f-right">'.get_string('reports_heading','',getReportSyncTime()).'</div>';
echo '<div class="clear"></div>';
$PerformanceData = getGraphData($arrEnrollmentData);
echo  '<div class=""><div class=""><table cellspacing="0" cellpadding="0" border="0" width="100%" id="table1" class="table1 generaltable">';
	echo '<tr class="headingRow">';
		echo '<td width="14%" style="vertical-align:middle" >'.get_string('name').'</td>';
		echo '<td width="10%" class="bor_left">&nbsp;</td>';
		echo '<td width="35%" style="padding:0;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="margin_none">';
		if($cntSession>=1){
			echo '<tr><td colspan="'.$cntSession.'" style="text-align:center; border-bottom:1px solid #b9b9b9" class="bor_left">'.get_string("sessionstext","scheduler").'</td></tr>';		
		}else{
			echo '<tr><td style="text-align:center; border-bottom:1px solid #b9b9b9" class="bor_left">'.get_string("sessionstext","scheduler").'</td></tr>';
		}
		echo '<tr>';
		$extraPDFCol = '';
		if(!empty($sessionHeaderData)){
			foreach ( $sessionHeaderData as $session ) {

				$sessionDuration = getClassroomSessionDateDuration($session->session_starttime, $session->session_duration);
				$sessionName = $session->sessionname."<br>(". $sessionDuration.")";
				echo "<td width='500' class='bor_left'>" . $sessionName . "</td>";
			}
		}
		echo '</tr></table></td>';
		echo "<td width = '10%' align='align_left' class='bor_left' style='vertical-align:middle' >" . get_string ( 'userclassstatus', 'scheduler' ) . "</td>";
	
	echo '</tr>';
	if(!empty($enrolledData)){
		foreach($enrolledData as $enrolled){
			echo "<tr class='rep_row'><td class='align_left'  width = '14%'><span class='f-left'>" . $enrolled->user_fullname. "</span></td>";
			if($cntSession > 0 ){
				echo "<td width = '10%' class='gradeCell'><div class='att'>" . get_string ( 'attended', 'scheduler' ) . "</div><div class='score'>" . get_string ( 'score', 'scheduler' ) . "</div><div class='grade'>" . get_string ( 'grade', 'scheduler' ) . "</div></td>";
			}
			echo '<td class="sessionCell"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="session_table"><tr>';
			$attendedStatus = 0;
			foreach($arrSessions[$enrolled->userid]['session'] as $key=>$session){
				$attended = 'No';
				$score = getMDash();
				$grade = getMDash();
				if($chartData->class_submitted == 1){
					if($session['attendence'] == 1){
						if($attendedStatus == 0){
							$attendedStatus = 1;
						}
						$attended = 'Yes';
					}
					if($session['score']){
						$score = $session['score'];
					}
					if($session['grade']){
						$grade = $session['grade'];
					}
				}
				echo "<td width='500'><div class='att'>".$attended."</div><div class='score'>".$score."</div><div class='grade'>".$grade."</div></td>";
			}
			if($chartData->class_submitted != 1){
				$userStatus = get_string('classnodata','classroomreport');
			}else{
				if($arrSessions[$enrolled->userid]['is_completed'] == 1){
					$userStatus = get_string('classcompleted','classroomreport');
				}elseif($attendedStatus == 1){
					$userStatus = get_string('classincomplete','classroomreport');
				}else{
					$userStatus = get_string('classnoshow','classroomreport');
				}
			}
			echo '</tr></table></td>';
			echo '<td class="align_left">'.$userStatus.'</td>';
			echo "</tr>";
		}
	}else{
			echo '<tr><td colspan = "3">'.get_string('no_results').'</td></tr>';
	}
echo '</table></div></div>';
echo '</div>';
echo '</div>';
if($action != 'print'){
	echo paging_bar($userCount, $page, $perpage, $genURL);
}
$userHTML .=  '<script language="javascript" type="text/javascript">';
$userHTML .=  '	$(document).ready(function(){$("#printbun").bind("click", function(e) {var url = $(this).attr("rel");
window.open(url, "'.get_string('courseusagesreport','classroomreport').'", "'.$CFG->printWindowParameter.'");
});
}); ';
$userHTML .= '</script>';
echo $userHTML;
echo $OUTPUT->footer();
?>