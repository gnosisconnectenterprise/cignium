<?php
	/**
	* Message module - Message Listing.
	* Date Creation - 12/06/2014
	* Date Modification : 13/06/2014
	* Last Modified By : Bhavana Kadwal
	*/
	require_once('../config.php');
	global $USER,$CFG,$DB;
		
    $page = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', $CFG->perpage, PARAM_INT);
	$ord      = optional_param('ord', 'asc', PARAM_TEXT);
	$searchText = (isset($_REQUEST['key']) ? $_REQUEST['key'] : '');
	$id = optional_param('id', 0, PARAM_INT);
	$action = optional_param('action', 2, PARAM_INT);
	
	$paramArray = array();
	
	if(get_string('key')==$searchText)
	{
		$searchText='';
	}
	
    //If user is not manager, redirect to home page.
    if($USER->archetype == $CFG->userTypeStudent){
    	redirect($CFG->wwwroot .'/course');
    }

	// If Not logged in ask user to login
    if(empty($USER->id)){
    	require_login();
    }
	$context = get_context_instance(CONTEXT_SYSTEM);

	// Page Header and breafcrum starts Here
	$header = $SITE->fullname.": ".get_string('managemessages','menubar');
    
	$params = array();
	$PAGE->set_context($context);
	$PAGE->set_url('/messages/index.php', $params);
	$PAGE->set_pagelayout('mydashboard');
	$PAGE->set_pagetype('my-index');
	$PAGE->blocks->add_region('content');
	$PAGE->set_title($header);
	$PAGE->set_heading($header);
	
	$PAGE->navbar->add(get_string('manage_messages',"messages"));
	// Page Header and breafcrum ends Here

	$paramArray = array(
				'ch' => optional_param('ch', '', PARAM_ALPHA),
				'key' => optional_param('key', '', PARAM_RAW),				
				'searchText' => optional_param('searchText', '', PARAM_ALPHANUM)				
			  );
	$filterArray = array(							
					'searchText'=>get_string("messages","messages")					
			   );
	$removeKeyArray = array('id','action');		
	$pageURL = '/messages/index.php';

		////// Getting common URL for the Search List //////
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);

	// This condition will update message Status
	if(isset($id)){
		if($action=='1')
		{
			$newdiscussion = new stdClass();
			$newdiscussion->status=1;
			$newdiscussion->id=$id;
			$updid = $DB->update_record('my_messages', $newdiscussion);
			$_SESSION['update_msg'] = get_string('record_updated');
			redirect($genURL);
		}elseif($action=='0')
		{
			$newdiscussion = new stdClass();
			$newdiscussion->status=0;
			$newdiscussion->id=$id;
			$updid = $DB->update_record('my_messages', $newdiscussion);
			$_SESSION['update_msg'] = get_string('record_updated');
			redirect($genURL);
		}
	}
	
	$where='';
	$searchText = strtolower($searchText);
	$searchText = addslashes($searchText);
	$params=array();

	if($searchText!='')
	{
		$where .= " AND (LOWER(strip_tags(message_content)) LIKE '%".$searchText."%' || LOWER(message_title) LIKE '%".$searchText."%')";
	}

	if($paramArray['ch'] != '') {
			if($paramArray['ch'] == 'OTH')
				$where .= " AND (LOWER(strip_tags(message_content)) REGEXP '^[^a-zA-Z]')";
			else
				$where .= "AND (LOWER(strip_tags(message_content)) like '".$paramArray['ch']."%' OR LOWER(strip_tags(message_content)) like '".ucwords($paramArray['ch'])."%')";
		}
	
	if($USER->archetype != $CFG->userTypeAdmin ) { 
		$where .= " AND createdby = ".$USER->id;
	}
	
	$messageSql = "SELECT count(id) as totalrecord FROM {$CFG->prefix}my_messages where deleted='0' ".$where." order by id DESC";
	
	$messageRow = $DB->get_record_sql($messageSql);
	
	$portal_sql = "SELECT * FROM {$CFG->prefix}my_messages where deleted='0' ".$where." order by message_title ASC";
			
	$start=($page-1)*$perpage;
	if($perpage){
	$limi=" limit $start, $perpage ";
	}
	else{
		$limi = '';
	}
	
	$records = $DB->get_records_sql($portal_sql."".$limi,$params);
	
	echo $OUTPUT->header();	
	
	echo '<div class="category_section">';
	
	echo '<a class="button-link add-message" href="'.$CFG->wwwroot.'/messages/add_message.php" ><i></i><span>'.get_string("new_message",'messages').'</span></a>';
	
	$parameter='';
	foreach($_GET as $key=>$para){
		if($key!='ord' && $key!='sort'){
			$parameter.="$key=$para&";
		}
	}
	
	
	echo genCommonSearchForm($paramArray, $filterArray, $pageURL);
	echo '<div class="clear"></div>';
	
	//// Record update Message
	if($_SESSION['update_msg']!=''){
		echo '<div class="clear"></div>';
		echo '<div class="bg-success text-success text-center" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
		echo '<div class="clear"></div>';
		$_SESSION['update_msg']='';
	}
	/// Record update message ends
	echo '<div class="borderBlock"><h2 class="icon_title">'.get_string("messages","messages").' ('.$messageRow->totalrecord.')</h2><div class="borderBlockSpace"><table class="table1" cellpadding="0" cellspacing="0" style="width: 100%;">
	<tr class="table1_head">
	<td width="20%" class="align_left">'.get_string("message_title","messages").'</td>
	<td width="40%" class="align_left">'.get_string("message","messages").'</td>
	<td width="15%" class="align_left">'.get_string("timesent","messages").'</td>	
	<td width="25%" class="align_left">'.get_string("manage").'</td>
	</tr>';
	if($records){
		$rowCount=0;
		$totalRec=count($records);
		foreach($records as $key => $message){
		
		        $message_content = $message->message_content?nl2br($message->message_content):$message->message_content;
				$linkEdit = $CFG->wwwroot.'/messages/add_message.php?id='.$message->id.'&'.$parameter;
				$editDisable = 'edit';
				if($message->status=='0')
				{
					$classDisable = 'disable';
					$tooltip=get_string("activate");
					$disabledclass = 'class="disable"';
					$editDisable = 'edit-disable';
					$linkEdit = 'javascript:void(0);';
					$status='<a href="'.$CFG->wwwroot.'/messages/index.php?action=1&id='.$message->id.'&'.$parameter.'" title="'.$tooltip.'"   class="enable"></a>';					
				}else{
					$classDisable = 'enable';
					$tooltip=get_string("deactivatelink","messages");
					$status='<a href="'.$CFG->wwwroot.'/messages/index.php?action=0&id='.$message->id.'&'.$parameter.'" class="disable" title="'.$tooltip.'" ></a>';
				}
				
			if(++$rowCount % 2 == 0){
				$rowClass = ' class="table_odd '.$classDisable.'"';
			}else{
				$rowClass = ' class="'.$classDisable.'"';
			}
			echo '<tr'.$rowClass.'>
			<td class="align_left" >';
			echo $message->message_title;
			echo '</td>';
			echo '<td class="align_left" >';
			if(strlen($message_content) >100){
				echo substr($message_content,0,100)." ...";
			}else{
				echo $message_content;
			}

			echo '</td>';
			echo '<td class="align_left"><span>'.date($CFG->customDefaultDateFormat,$message->timecreated).'</span></td>';
			
				echo '<td><div class="adminiconsBar order">';
				echo '<a title="'.get_string("edit").'" class="'.$editDisable.'" href="'.$linkEdit.'">'.get_string("edit").'</a>';
				if($USER->archetype == $CFG->userTypeAdmin){
					echo $status;
				}else{
					echo '';
				}	
				echo '<a title="'.get_string("delete").'" class="delete delete_message" href="'.$CFG->wwwroot.'/messages/delete_message.php?id='.$message->id.'&action=1&'.$parameter.'">'.get_string("delete").'</a>';
			echo '</div></td>
			</tr>';
		}
	}else{
		echo '<tr>
		<td colspan="4" align="center">'.get_string("no_results").'</td>
		</tr>';
	}	
	echo '</table></div></div></div>';
	
	////// Define those key, which we need to remove first time //////
	$removeKeyArray = array('id','flag','perpage');	
	////// Getting common URL for the Search //////
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);	
	echo paging_bar($messageRow->totalrecord, $page, $perpage, $genURL);
	
	echo $OUTPUT->footer();
?>
<script language="javascript" type="application/javascript">
setInterval(function(){$('.update_msg').slideUp();},10000);
</script>
    
<script>


$(document).ready(function(){


	$(".delete_message").click(function(){
		
		var slotid = $(this).attr("rel");
		var txt = "<?php echo get_string('messagedeleteconfirm')?>";
		
		var response = confirm(txt);
		
		if (response == true) {
		return true;
		}
		return false;
	});
	
	

	
});

</script>