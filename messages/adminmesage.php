<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Manage files in folder in private area.
 *
 * @package   core_files
 * @copyright 2010 Petr Skoda (http://skodak.org)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

//require('../../config.php');
require_once("$CFG->dirroot/local/user/files_form.php");
require_once("$CFG->dirroot/repository/lib.php");

require_login();


$returnurl = optional_param('returnurl', '', PARAM_LOCALURL);

if (empty($returnurl)) {

    $uurl = str_replace(array('/moodle'),"",$_SERVER['PHP_SELF']);
    $returnurl = new moodle_url($uurl);
}

$context = context_user::instance($USER->id);
require_capability('moodle/user:manageownfiles', $context);

/*$title = get_string('myfiles');
$struser = get_string('user');

$PAGE->set_url('/user/files.php');
$PAGE->set_context($context);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_pagelayout('mydocfiles');
$PAGE->set_pagetype('user-files');
*/
$maxbytes = $CFG->userquota;
$maxareabytes = $CFG->userquota;
if (has_capability('moodle/user:ignoreuserquota', $context)) {
    $maxbytes = USER_CAN_IGNORE_FILE_SIZE_LIMITS;
    $maxareabytes = FILE_AREA_MAX_BYTES_UNLIMITED;
}

$data = new stdClass();
$data->returnurl = $returnurl;
$options = array('subdirs' => 1, 'maxbytes' => $maxbytes,'mypanel'=> 'right_panel', 'accepted_types' => '*',
        'areamaxbytes' => $maxareabytes);
file_prepare_standard_filemanager($data, 'files', $options, $context, 'user', 'private', 0);

$mform = new my_files_form(null, array('data'=>$data, 'options'=>$options));

if ($mform->is_cancelled()) {
    redirect($returnurl);
} else if ($formdata = $mform->get_data()) {
    $formdata = file_postupdate_standard_filemanager($formdata, 'files', $options, $context, 'user', 'private', 0);
    redirect($returnurl);
}

//echo $OUTPUT->header();
echo $OUTPUT->box_start('generalbox');
$mform->display();
echo $OUTPUT->box_end();
?>
<script>

$(document).ready(function(){

     // $('.fp-vb-icons').removeClass('checked');
	//$('.fp-vb-details').addClass('checked');
	//$('.fp-vb-tree').addClass('fp-vb-tree checked');
	$('#settingsnav .fp-viewbar').hide();
	$('#settingsnav #id_cancel').hide();
	
});


</script>
<?php
//echo $OUTPUT->footer();
?>
