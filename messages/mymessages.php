<?php
/**
	* Message module - List og messages sent to a user
	* Date Creation - 13/06/2014
	* Date Modification : 13/06/2014
	* Last Modified By : Bhavana Kadwal
*/
	require_once('../config.php');
	global $USER,$CFG,$DB;
	
	$page = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', $CFG->perpage, PARAM_INT);
	$ord      = optional_param('ord', 'asc', PARAM_TEXT);
	$searchText = (isset($_REQUEST['key']) ? $_REQUEST['key'] : '');
	$id = optional_param('id', 0, PARAM_INT);
	$action = optional_param('action', 2, PARAM_INT);
	if(get_string('searchtext')==$searchText)
	{
		$searchText='';
	}
	
	
    //if userid not available, ask user to login
    if($USER->archetype != $CFG->userTypeAdmin && $USER->archetype != $CFG->userTypeStudent  && $USER->archetype!=$CFG->userTypeManager){
    	redirect($CFG->wwwroot .'/course');
    }
    if(empty($USER->id)){
    	require_login();
    }
	$context = get_context_instance(CONTEXT_SYSTEM);

	require_login();
	$header = $SITE->fullname.": ".get_string('mymessages','messages');
   
	$params = array();
	$PAGE->set_context($context);
	$PAGE->set_url('/messages/index.php', $params);
	$PAGE->set_pagelayout('mydashboard');
	$PAGE->set_pagetype('my-index');
	$PAGE->blocks->add_region('content');
	$PAGE->set_title($header);
	$PAGE->set_heading($header);
	
	$PAGE->navbar->add(get_string('my_message',"messages"));
	
	$where='';
	$searchText = strtolower($searchText);
	$searchText = addslashes($searchText);
	$params=array();
	if($searchText!='')
	{
		$where .= " AND (LOWER(strip_tags(message_content)) LIKE '%".$searchText."%' || LOWER(message_title) LIKE '%".$searchText."%')";
	}	
	
	echo $OUTPUT->header();
	//echo '<style type="text/css">#common-search{width:100%;}</style>';

	$paramArray = array(
				'ch' => optional_param('ch', '', PARAM_ALPHA),
				'key' => optional_param('key', '', PARAM_RAW),				
				'searchText' => optional_param('searchText', '', PARAM_ALPHANUM)				
			  );
	$filterArray = array(							
					'searchText'=>get_string("messages","messages")					
			   );
	$pageURL = '/messages/mymessages.php';
	echo genCommonSearchForm($paramArray, $filterArray, $pageURL);
	
	
	$groupSql = "SELECT groupid from {$CFG->prefix}groups_members where userid='".$USER->id."'";
	$groupRow = $DB->get_records_sql($groupSql);
	$groupId = array();
	$teamId = "";
	if(count($groupRow)>0){
		foreach($groupRow as $groupValue){		
			$groupId[] = $groupValue->groupid; 		
		}
		$teamId =  implode(",",$groupId);
	}	
	if($paramArray['ch'] != '') {
		if($paramArray['ch'] == 'OTH')
			$where .= " AND (LOWER(strip_tags(message_content)) REGEXP '^[^a-zA-Z]')";
		else
			$where .= "AND (LOWER(strip_tags(message_content)) like '".$paramArray['ch']."%' OR LOWER(strip_tags(message_content)) like '".ucwords($paramArray['ch'])."%')";
	}
	$messageSql = "SELECT count(id) as totalrecord from {$CFG->prefix}my_messages where deleted = 0 AND  status = 1 ".$where." and ( FIND_IN_SET('".$USER->department."',department) || FIND_IN_SET('".$USER->id."',show_to) ||  FIND_IN_SET('".$teamId."',team)) ORDER BY timecreated DESC";

	$messageSql = "SELECT COUNT(m.id) AS totalrecord
					FROM mdl_my_messages AS m
					LEFT JOIN mdl_type_status AS ts ON ts.type_id = m.id AND ts.user_id = ".$USER->id." AND ts.`type` = 0
					WHERE deleted = 0 AND STATUS = 1 ".$where." and ( FIND_IN_SET('".$USER->department."',department) || FIND_IN_SET('".$USER->id."',show_to) ||  FIND_IN_SET('".$teamId."',team))
					ORDER BY timecreated DESC";
	
	$messageRow = $DB->get_record_sql($messageSql);
	
	//$msgSql = "SELECT * from {$CFG->prefix}my_messages where deleted = 0 AND  status = 1 ".$where." and ( FIND_IN_SET('".$USER->department."',department) || FIND_IN_SET('".$USER->id."',show_to) ||  FIND_IN_SET('".$teamId."',team)) ORDER BY timecreated DESC";
	$msgSql = "SELECT m.*,ts.id AS readid,CONCAT(u.firstname,' ',u.lastname,' (',u.username,')') as user_name
				FROM mdl_my_messages AS m
				LEFT JOIN mdl_type_status AS ts ON ts.type_id = m.id AND ts.user_id = ".$USER->id." AND ts.`type` = 0
				LEFT JOIN mdl_user as u ON m.message_from = u.id
				WHERE m.deleted = 0 AND STATUS = 1 ".$where." and ( FIND_IN_SET('".$USER->department."',m.department) || FIND_IN_SET('".$USER->id."',m.show_to) ||  FIND_IN_SET('".$teamId."',m.team))
				ORDER BY m.timecreated DESC";
	$start=($page-1)*$perpage;
	$limi=" limit $start, $perpage ";
	$records = $DB->get_records_sql($msgSql."".$limi,$params);

	echo '<a class="addbutton" href=""><span>&nbsp;</span></a>';

	$parameter='';
	foreach($_GET as $key=>$para){
		if($key!='ord' && $key!='sort'){
			$parameter.="$key=$para&";
		}
	}
	echo '<div class="clear"></div>';
	if($_SESSION['update_msg']!=''){
			echo '<div class="update_msg">'.$_SESSION['update_msg'].'</div>';
			$_SESSION['update_msg']='';
	}
	echo '<div class="borderBlock">';
	echo '<div class="add_cms"><h2 class="icon_title">'.get_string("mymessages","messages").' ('.$messageRow->totalrecord.')</h2><div class="borderBlockSpace"><table class="table1" cellpadding="0" cellspacing="0" style="width: 100%;">
	<tr class="table1_head">	
	<td width="20%" class="align_left">'.get_string("message_title","messages").'</td>
	<td width="30%" class="align_left">'.get_string("message","messages").'</td>
	<td width="20%" class="align_left">'.get_string("received","messages").'</td>
	<td width="20%" class="align_left">'.get_string('message_sender','messages').'</td>
	<td width="10%" align="align_left">'.get_string("manage").'</td>
	</tr>';
	if($records){
		$rowCount=0;
		$totalRec=count($records);
		foreach($records as $key => $cms){
			if(++$rowCount % 2 == 0){
				$rowClass = ' class=table_odd';
			}else{
				$rowClass = '';
			}
			echo '<tr'.$rowClass.'>
			<td class="align_left">';
			echo $cms->message_title;
			echo '</td>';
			echo '<td class="align_left" >';
			if(strlen($cms->message_content) >100){
				echo substr($cms->message_content,0,100)." ...";
			}else{
				echo $cms->message_content;
			}
			if(isset($cms->readid) && $cms->readid != '' && $cms->readid != 0){
				$readStr = get_string("view_message","messages");
				$class = 'view-message';
			}else{
				$readStr = get_string("read_message","messages");
				$class = 'read-message';
			}
			echo '</td>';
			echo '<td class="align_left">'.date($CFG->customDefaultDateTimeFormat,$cms->timecreated).'</td>';
			echo '<td class="align_left">'.$cms->user_name.'</td>';
				echo '<td>';
				echo '<div class="adminiconsBar order"><a title="'.$readStr.'" class="read '.$class.'" href="'.$CFG->wwwroot.'/messages/read_message.php?id='.$cms->id.'">'.$readStr.'</a>';
			echo '</div></td>
			</tr>';
		}
	}else{
		echo '<tr>
		<td colspan="3" align="center">'.get_string("no_results").'</td>
		</tr>';
	}	
	echo '</table></div></div></div>';
	
	////// Define those key, which we need to remove first time //////
	$removeKeyArray = array('id','flag','perpage');	
	////// Getting common URL for the Search //////
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);	
	echo paging_bar($messageRow->totalrecord, $page, $perpage, $genURL);
	echo $OUTPUT->footer();
?>
<script language="javascript" type="application/javascript">
setInterval(function(){$('.update_msg').slideUp();},10000);
</script>