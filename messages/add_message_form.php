<?php
	/**
	* Message module - Add Message Form.
	* This file is included in add Message page
	* Date Creation - 12/06/2014
	* Date Modification : 13/06/2014
	* Last Modified By : Bhavana Kadwal
	*/
	require_once('../config.php');	
	global $CFG,$DB;
	require_once($CFG->libdir.'/formslib.php');
	
	class addcms_form extends moodleform{
		function definition() {
			//Defining global objects User and Configuration		
			global $USER, $CFG, $DB;
			//Form object
			$mform =& $this->_form;
			//data received to populate this form   
			$addcmsdata = $this->_customdata;
			if (!isset($addcmsdata->id)){ 
				$editoroptions = $this->_customdata['editoroptions'];
			}
			else{
				$editoroptions = $addcmsdata->editoroptions;
			}	
			if (!isset($addcmsdata->id)) {
				//$mform->addElement('header','create_message', get_string('add_message','messages'));
			}else{
				//$mform->addElement('header','create_message', get_string('edit_message','messages'));
			}

			/*$messageContent = array("text"=>$addcmsdata->message_content, 'format'=>1);
			$mform->addElement('editor','message_content', get_string('message','messages'), null, $editoroptions);
			$mform->setDefault('message_content', $messageContent);  
			$mform->setType('message_content', PARAM_RAW);*/
			$mform->addElement('html','<div class="filedset_outer ">');

			$mform->addElement('text','title', get_string('message_title','messages'),array('maxlength'=>100));      
			$mform->addRule('title', get_string('required'), 'required', null, 'server');
			
			$mform->addElement('textarea','message_content', get_string('message','messages'),array('style'=>"width: 100%; height: 205px;"));
			$mform->addRule('message_content', get_string('required'), 'required', null, 'server');

			//list of all teams.			
			$paramsTeam = array();
			/*if($USER->archetype!="admin"){
				$groupWhere = "where createdby='".$USER->id."'";
			}else{
				$groupWhere = "";
			}		
			$teamSql = "SELECT id, name FROM {$CFG->prefix}groups ".$groupWhere." order by name ASC";			
			$teamRow = $DB->get_records_sql($teamSql,$paramsTeam);*/
			$teamRow = getGroupsListing('id',0,'ASC',0,0,'','');
			$teamList = '';		
			if(strlen($addcmsdata->team)>0){$teams = $addcmsdata->team; $teamCheck = explode(",",$addcmsdata->team);}else{ $teams = ''; $teamCheck = array(); }
			
			foreach($teamRow as $teamValue){				
				if (in_array($teamValue->id,$teamCheck)){										
					$teamList .= '<div class="msg-department"><input type="checkbox" checked name="team_'.$teamValue->id.'" id="team_'.$deptValue->id.'" value="'.$teamValue->id.'"  /> '.ucfirst($teamValue->name).' ('.$teamValue->dept_title.')'.'</div>';
				}else{								
					$teamList .= '<div class="msg-department"><input type="checkbox"  name="team_'.$teamValue->id.'" id="team_'.$teamValue->id.'" value="'.$teamValue->id.'"  /> '.ucfirst($teamValue->name).' ('.$teamValue->dept_title.')'.'</div>';
				}
			}
			
			//list of all deparments members.			
			$paramsDepartment = array();			
			$departmentSql = "SELECT id, title FROM mdl_department where status=1 and deleted=0 order by title ASC";			
			$deptRow = $DB->get_records_sql($departmentSql,$paramsDepartment);
			
			$departmentList = '';			
			
			if(strlen($addcmsdata->department)>0){$departments = $addcmsdata->department; $departCheck = explode(",",$addcmsdata->department);}else{ $departments = ''; $departCheck = array(); }
			
			foreach($deptRow as $deptValue){				
				if (in_array($deptValue->id,$departCheck)){										
					$departmentList .= '<div class="msg-department"><input type="checkbox" checked name="department_'.$deptValue->id.'" id="department_'.$deptValue->id.'" value="'.$deptValue->id.'"  /> '.ucfirst($deptValue->title).'</div>';
				}else{								
					$departmentList .= '<div class="msg-department"><input type="checkbox"  name="department_'.$deptValue->id.'" id="department_'.$deptValue->id.'" value="'.$deptValue->id.'"  /> '.ucfirst($deptValue->title).'</div>';
				}
			}			

			//list of all members.			
			$paramsMembers = array();	
			if($USER->archetype!="admin"){
				$userWhere = " and createdby='".$USER->id."'";
			}else{
				$userWhere = "";
			}
			//$membersSql = "SELECT id, firstname, lastname,username FROM `{$CFG->prefix}user` WHERE deleted=0 ".$userWhere." AND suspended=0 and id<>$USER->id order by firstname ASC";			
			//$memberRow = $DB->get_records_sql($membersSql,$paramsMembers);
			$memberRow = get_users_listing('lastaccess','ASC',1,0,'','','',' u.suspended = 0',null,null);
			$membersList = '';
			if(strlen($addcmsdata->show_to)>0){$showTo = $addcmsdata->show_to;  $showToCheck = explode(",",$addcmsdata->show_to);}else{ $showTo = ''; $showToCheck = array(); }
			
			foreach($memberRow as $membersValue){					
				if (in_array($membersValue->id,$showToCheck)){					
					$membersList .= '<div class="msg-department"><input type="checkbox" checked value="'.$membersValue->id.'" name="show_to'.$membersValue->id.'" id="show_to'.$membersValue->id.'"/> '.ucfirst($membersValue->firstname).' '.ucfirst($membersValue->lastname).' ('.$membersValue->username.')'.'</div>';
				}else{
					$membersList .= '<div class="msg-department"><input type="checkbox" value="'.$membersValue->id.'" name="show_to'.$membersValue->id.'" id="show_to'.$membersValue->id.'"/> '.ucfirst($membersValue->firstname).' '.ucfirst($membersValue->lastname).' ('.$membersValue->username.')'.'</div>';
				}
			}			
			
			
			if($USER->archetype !="manager"){
				$tabMenu = '<a href="javascript:;" class="tabLink activeLink" id="cont-1">'.get_string('departments').'</a><a href="javascript:;" class="tabLink " id="cont-2">'.get_string('teams').'</a><a href="javascript:;" class="tabLink " id="cont-3">'.get_string('users').'</a>';
				$tabsBlocks = '<div class="tabcontent paddingAll" id="cont-1-1">
					<div class="departments">'.$departmentList.'</div>							
				</div>
				<div class="tabcontent paddingAll hide" id="cont-2-1">'.$teamList.'	
				</div>								  									  
				<div class="tabcontent paddingAll hide" id="cont-3-1">'.$membersList.'										
				</div>';
			}else{
				$tabMenu = '<a href="javascript:;" class="tabLink activeLink" id="cont-2">'.get_string('teams').'</a><a href="javascript:;" class="tabLink " id="cont-3">'.get_string('users').'</a>';
				$tabsBlocks = '<div class="tabcontent paddingAll" id="cont-2-1">'.$teamList.'	
				</div>								  									  
				<div class="tabcontent paddingAll hide" id="cont-3-1">'.$membersList.'										
				</div>' ;
			}
			
			$mform->addElement('html','<div class="fitem required fitem_ftextarea " id="fitem_id_notifyto"><div class="fitemtitle" id="yui_3_13_0_2_1405402028441_13"><label for="id_notifyto" id="yui_3_13_0_2_1405402028441_12">'.get_string('notify_to','messages').'</label></div><div class="felement ftextarea" id="yui_3_13_0_3_1405402028441_611"><div class="msg-tab-block">
				<div class="tab-msg-all">
					<div class="msg-tab-block" style="float:left;padding:1%;">
						<input type="checkbox" name="department_all" id="department_all"  > All
					</div>
				</div>
				<div class="tab-box">'.$tabMenu.'</div>'.$tabsBlocks.'</div></div></div>');
			$mform->addElement('html','<input type="hidden" value="'.$teams.'" name="team" id="team"><input type="hidden" name="department" id="department" value="'.$departments.'"/><input type="hidden" name="show_to" id="show_to" value="'.$showTo.'"><input type="hidden" name="toall" id="toall" value="">');
			/*$mform->addElement('html','<div class="msg-tab-html"><div class="msg-tab-label">'.get_string('notify_to','messages').'</div>
										<div class="msg-tab-block">
										<div class="tab-msg-all">
											<div class="msg-tab-block" style="float:left;padding:1%;">
												<input type="checkbox" name="department_all" id="department_all"  > All
											</div>
										</div>
									<div class="tab-box">
										<a href="javascript:;" class="tabLink activeLink" id="cont-1">'.get_string('departments').'</a> 
										<a href="javascript:;" class="tabLink " id="cont-2">'.get_string('teams').'</a> 
										<a href="javascript:;" class="tabLink " id="cont-3">'.get_string('users').'</a> 
									  </div>										
									  <div class="tabcontent paddingAll" id="cont-1-1"><div class="departments">'.$departmentList.'</div>																					
									  </div>
									  <div class="tabcontent paddingAll hide" id="cont-2-1">'.$teamList.'	
									  </div>								  									  
									  <div class="tabcontent paddingAll hide" id="cont-3-1">'.$membersList.'										
									  </div></div></div><input type="hidden" value="'.$teams.'" name="team" id="team"><input type="hidden" name="department" id="department" value="'.$departments.'"/><input type="hidden" name="show_to" id="show_to" value="'.$showTo.'"><input type="hidden" name="toall" id="toall" value="">');*/
			$mform->setDefault('message_content', $messageContent);  
			$mform->setType('message_content', PARAM_RAW);

            $showResetButton = true;
			if (isset($addcmsdata->id)) {
				$mform->addElement('hidden','id');
				$mform->setDefault('id', $addcmsdata->id);
				 $showResetButton = false;
			}
			
			
			$this->add_action_buttons(true, get_string('savechanges'), $showResetButton);
			$mform->addElement('html','</div>');			
		}
		function validation($data){
			global $USER, $CFG,$DB;
			$errors = array();
			$where  = '';
			if ($data['id']){
				$where  .= ' AND id != '.$data['id'];
			}
			if(isset($data['title']) && !empty($data['title'])){
				$messageExists = $DB->get_record_sql("SELECT id FROM mdl_my_messages WHERE LOWER(message_title) = '".strtolower($data['title'])."' ".$where);
				if(!empty($messageExists)){
					$errors['title'] = get_string('message_title_exists','messages', $data['title']);
				}
			}
			/*if(isset($data['startdate']) && !empty($data['startdate'])){
				$startdate = strtotime($data['startdate']);
				if($startdate < strtotime(date('Y/m/d'))){
					$announcement_sql = "SELECT startdate FROM {$CFG->prefix}my_messages where deleted='0' AND id = '".$data['id']."'";
					$announcement_startdate = $DB->get_record_sql($announcement_sql);
					if(strtotime(date('Y/m/d'))>$announcement_startdate->startdate){
					}else{
						$errors['startdate'] = 'Start date can\'t be less than Current date';
					}
				}
	    	}
			if(isset($data['expirydate']) && !empty($data['expirydate'])){
				$expirydate = strtotime($data['expirydate']);
				if($expirydate < $startdate){
					$errors['expirydate'] = 'End date can\'t be less than Start date';
				}
	    	}
			if($data['institute_list'] == '0'){
					$errors['institute_list'] = get_string('missinginstitute');
	    	}
			//print'<pre>';print_R($errors);die;*/
			return $errors;
		}
	}
?>