<?php
	/**
	* Message module - Message Library
	* For Database operation - Add Update
	* Date Creation - 12/06/2014
	* Date Modification : 13/06/2014
	* Last Modified By : Bhavana Kadwal
	*/
	require_once('../config.php');		
	global $USER,$CFG,$DB;	
	require_once($CFG->libdir."/dmllib.php");
	require_once($CFG->libdir."/deprecatedlib.php");
	require_once($CFG->libdir."/accesslib.php");
	
	function add_messsage($messageData)
	{
		global $USER, $DB, $CFG;
				
		$data = new stdClass();
		$data->message_content	= $messageData['message_content'];
		$data->message_text		= strip_tags($messageData['message_content']);
		$data->message_title	= addslashes($messageData['title']);
		$data->status=1;
		$data->deleted=0;
		$data->team = isset($messageData['team'])?$messageData['team']:'';
		$data->department = isset($messageData['department'])?$messageData['department']:'';
		$data->show_to = isset($messageData['show_to'])?$messageData['show_to']:'';
		$data->timecreated = time();
		$data->timemodified = time();
		$data->message_from = $USER->id;
		$data->createdby = $USER->id;
		$data->updatedby = $USER->id;
		$nid = $DB->insert_record('my_messages', $data);
		return $nid;
	}
	function update_messsage($messageData)
	{
		global $USER, $DB, $CFG;
		$data = new stdClass();
		$data->id = $messageData['id'];
		$data->status=1;
		$data->message_content = $messageData['message_content'];
		$data->message_title		= addslashes($messageData['title']);
		$data->message_text = strip_tags($messageData['message_content']);
		$data->team = isset($messageData['team'])?$messageData['team']:'';
		$data->department = isset($messageData['department'])?$messageData['department']:'';
		$data->show_to = isset($messageData['show_to'])?$messageData['show_to']:'';
		$data->timemodified = time();
		$data->updatedby = $USER->id;
		$nid = $DB->update_record('my_messages', $data);
		return $nid;
	}
	
?>