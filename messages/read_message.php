<?php
/**
	* Message module - Details of a message
	* Date Creation - 13/06/2014
	* Date Modification : 13/06/2014
	* Last Modified By : Bhavana Kadwal
*/
	require_once('../config.php');
	require_once('add_message_form.php');
	require_once('messagelib.php');
	
	global $USER,$CFG;

	//if userid not available, ask user to login
	if(empty($USER->id)){
		require_login();
	}
	$id = optional_param('id', 0, PARAM_INT);
	$context = get_context_instance(CONTEXT_SYSTEM);
	
	//check role assign univmanager
	if($USER->archetype!=$CFG->userTypeAdmin && $USER->archetype!=$CFG->userTypeStudent && $USER->archetype!=$CFG->userTypeManager)
	{
		redirect($CFG->wwwroot."/");
	}
	if(!$id){
		redirect($CFG->wwwroot."/");
	}
	//End role assing
	$strmymoodle = get_string('my_message','messages');	
	// Start setting up the page
	$header = $SITE->fullname.": ".get_string('my_message','messages');
	
	$returnto = optional_param('returnto', 0, PARAM_ALPHANUM);
	$params = array();
	$PAGE->set_context($context);
	$PAGE->set_url('/messages/mymessages.php', $params);
	$PAGE->set_pagelayout('general');
	$PAGE->set_pagetype('my-index');
	$PAGE->blocks->add_region('content');
	$PAGE->set_subpage($currentpage->id);
	$PAGE->set_title($header);
	$PAGE->set_heading($header);
	$PAGE->navbar->add(get_string('mymessages','messages'), new moodle_url('/messages/mymessages.php'));
	$PAGE->navbar->add(get_string('view_message','messages'));
	
	$get_cms = getMyMessages($id);
	$typeStatusRecord = $DB->get_record_sql("SELECT * FROM {$CFG->prefix}type_status WHERE type = 0 AND user_id = $USER->id AND type_id = $get_cms->id");
	if(!empty($typeStatusRecord)){
		$data = new stdClass();
		$data->id = $typeStatusRecord->id;
		$data->type_id=$get_cms->id;
		$data->user_id = $USER->id;
		$data->last_read  = time();
		$nid = $DB->update_record('type_status', $data);
	}else{
		$data = new stdClass();
		$data->type = 0;
		$data->type_id=$get_cms->id;
		$data->user_id = $USER->id;
		$data->read_time  = time();
		$data->last_read  = time();
		$nid = $DB->insert_record('type_status', $data);
	}
	$back_url=$CFG->wwwroot."/messages/index.php";
echo $OUTPUT->header();
echo getModuleHeaderHtml($get_cms, $CFG->messageModule);
echo '<input type="submit" name="cancel" value="'.get_string('backtomessages').'" id= "backtomessages"/>';
?>
<script>
$(document).ready(function(){
	$("#backtomessages").click(function(){
		window.location.href = "<?php echo $CFG->wwwroot?>/messages/mymessages.php";
	});
});
</script>
<?php
	echo $OUTPUT->footer();
?>