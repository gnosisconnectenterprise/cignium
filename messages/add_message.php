<?php
	/**
	* Message module - Add Message.
	* Date Creation - 12/06/2014
	* Date Modification : 13/06/2014
	* Last Modified By : Bhavana Kadwal
	*/
	require_once('../config.php');
	require_once('add_message_form.php');
	require_once('messagelib.php');
	
	global $USER,$CFG;

	//if userid not available, ask user to login
	if(empty($USER->id)){
		require_login();
	}
	
	$context = get_context_instance(CONTEXT_SYSTEM);
	
	$id = optional_param('id', 0, PARAM_INT); //message id
	
	//check role assign univmanager
	if($USER->archetype != $CFG->userTypeAdmin)
	{
		checkUserAccess("messages" , $id);
	}
	
	//End role assing
	$strmymoodle = get_string('add_message','messages');	
	
	$editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES, 'maxbytes'=>$CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true);
	
	
	// Start setting up the page
	if ($id) {
		$header = $SITE->fullname.": ".get_string('edit_message','messages');
	}else{
		$header = $SITE->fullname.": ".get_string('add_message','messages');
	}
	
	$returnto = optional_param('returnto', 0, PARAM_ALPHANUM);
	$params = array();
	$PAGE->set_context($context);
	$PAGE->set_url('/messages/index.php', $params);
	$PAGE->set_pagelayout('general');
	$PAGE->set_pagetype('my-index');
	$PAGE->blocks->add_region('content');
	$PAGE->set_subpage($currentpage->id);
	$PAGE->set_title($header);
	$PAGE->set_heading($header);
	$PAGE->navbar->add(get_string('manage_messages','messages'), new moodle_url('/messages/index.php'));
	if($id==0){
	$PAGE->navbar->add(get_string('add_message','messages'));
	}
	else{
	$PAGE->navbar->add(get_string('edit_message','messages'));
	}
	if($id != 0){
		$select = "SELECT * FROM {$CFG->prefix}my_messages WHERE id = $id";
		$get_cms = $DB->get_record_sql($select);
		$addcmsdata = new stdClass();
		$addcmsdata->message_content	= $get_cms->message_content;
		$addcmsdata->title		= $get_cms->message_title;
		$addcmsdata->id = $id;
		
		if(strlen($get_cms->team)>0){
			$addcmsdata->team = $get_cms->team; 			
		}else{
			$addcmsdata->team = '';
		}

		if(strlen($get_cms->department)>0){
			$addcmsdata->department = $get_cms->department; 			
		}else{
			$addcmsdata->department = '';
		}

		if(strlen($get_cms->show_to)>0){
			$addcmsdata->show_to = $get_cms->show_to; 			
		}else{
			$addcmsdata->show_to = '';
		}
		
		$attachmentoptions = array('subdirs'=>false, 'maxfiles'=>99, 'maxbytes'=>$maxbytes);
		$addcmsdata->editoroptions=$editoroptions;
		$addcmsdata->returnto=$returnto;
		$addcmsdata->attachmentoptions=$attachmentoptions;
		$addcms_form = new addcms_form('add_message.php',$addcmsdata);
		$addcms_form->set_data($addcmsdata);
	}else{
		$attachmentoptions = array('subdirs'=>false, 'maxfiles'=>99, 'maxbytes'=>$maxbytes);
		$addcms_form = new addcms_form(NULL, array('editoroptions'=>$editoroptions, 'returnto'=>$returnto, 'attachmentoptions'=>$attachmentoptions));
	}
	//$addcms_form = new addcms_form('add_message.php',$addcmsdata);
	$back_url=$CFG->wwwroot."/messages/index.php";
	if($addcms_form->is_cancelled()) {	
		redirect($back_url);
    }else if($cmsdata= $addcms_form->get_data()){
			if($cmsdata->id!=''){
				$cmsdata1 =  $_POST;
				$nid=update_messsage($cmsdata1);
				$_SESSION['update_msg']=get_string('update_msg','messages');
				redirect($back_url);	
			}else{
				$cmsdata1 =  $_POST;			
				$nid=add_messsage($cmsdata1);
				$_SESSION['update_msg']=get_string('insert_msg','messages');
				redirect($back_url);	
			}
			
		}
	echo $OUTPUT->header();
	echo '<div class="left_content">
			<div class="category_section">';
			$addcms_form->display(); 
	echo '</div></div>';

	echo $OUTPUT->footer();
?>
<script type="text/javascript">
  $(document).ready(function() {
    $(".tabLink").each(function(){
      $(this).click(function(){
        tabeId = $(this).attr("id");
        $(".tabLink").removeClass("activeLink");
        $(this).addClass("activeLink");
        $(".tabcontent").addClass("hide");
        $("#"+tabeId+"-1").removeClass("hide");					
        return false;	  
      });
    }); 


	$(document).on('blur', '#id_message_content', function(){

		   var cVal = $(this).val();
		   cVal = $.trim(cVal);
		   $(this).val(cVal);


		}); 
  });
  
      $(document).on("click", "#id_submitbutton", function(){

		    var id_name = $('#id_title').val();
			var id_message_content = $('#id_message_content').val();
			id_name =  $.trim(id_name);
			id_message_content =  $.trim(id_message_content);

			if( id_name != '' && id_message_content != ''){  
				
				$(this.form).submit(function(){
					 $("#id_submitbutton").prop('disabled', true);
				
				});
			}
	});
	
	
  
$('#cont-1-1 :checkbox').on('click',function(){		
	$('#department').val("");
	$("#cont-1-1 :checkbox").each(function () {     
		if ($(this).prop('checked')==true){ 			
			if($('#department').val()==""){
				var depart = ""
			}else{
				var depart = $('#department').val()+",";
			}	
			if($(this).attr("id")!="department_all"){
				$('#department').val( depart+$(this).val());
			}			
		}                  
	});	  
});
  
$('#cont-2-1 :checkbox').on('click',function(){	
	$('#team').val("");
	$("#cont-2-1 :checkbox").each(function () {     
		if ($(this).prop('checked')==true){ 			
			if($('#team').val()==""){
				var team = ""
			}else{
				var team = $('#team').val()+",";
			}			
			$('#team').val( team+$(this).val());			
		}                  
	});	
	
});

$('#cont-3-1 :checkbox').on('click',function(){		
	$('#show_to').val("");	
	$("#cont-3-1 :checkbox").each(function () {     
		if ($(this).prop('checked')==true){ 			
			if($('#show_to').val()==""){
				var show_to = ""
			}else{
				var show_to = $('#show_to').val()+",";
			}			
			$('#show_to').val( show_to+$(this).val());			
		}                  
    });	 
});

//onready state
$(document).on('ready',function(){	

$('#cont-3-1 :checkbox').each(function() { //loop through each checkbox
			   
			if($('#toall').val()==""){
				var depart = ""
			}else{
				var depart = $('#toall').val()+",";
			}
			
			$('#toall').val( depart+$(this).val());
			
		});

if($("#show_to").val()=="" || $('#toall').val()==$("#show_to").val()){
	$("#department_all").prop('checked',true);
}
	if($("#department_all").prop('checked')) { // check select status
		$('#show_to').val("");
		$('#cont-3-1 :checkbox').each(function() { //loop through each checkbox
			   
			if($('#show_to').val()==""){
				var all = ""
			}else{
				var all = $('#show_to').val()+",";
			}
			
			$('#show_to').val( all+$(this).val());
			
		});
		$(".tab-box, .tabcontent").hide();
	}else{
		$(".tab-box, .tabcontent").show();		
	}
});
//onready state/click
$("#department_all").on('click',function(){	
	if($("#department_all").prop('checked')) { // check select status
		$('#show_to').val("");
		$('#cont-3-1 :checkbox').each(function() { //loop through each checkbox
			   
			if($('#show_to').val()==""){
				var toall = ""
			}else{
				var toall = $('#show_to').val()+",";
			}			
			$('#show_to').val( toall+$(this).val());			
		});
		$(".tab-box, .tabcontent").hide();
	}else{
		$('#show_to').val("");
		$('#team').val("");
		$('#department').val("");
		$(".tab-box, .tabcontent").show();
		$(".tabcontent :checkbox").each(function() { 
			$(this).prop("checked", false);		
		});
	}
});
</script>