<?php
	/**
	* Message module - Delete a Message
	* Date Creation - 13/06/2014
	* Date Modification : 13/06/2014
	* Last Modified By : Bhavana Kadwal
	*/
	require_once('../config.php');
	
	global $USER,$CFG;
	//if userid not available, ask user to login
	if(empty($USER->id)){
		require_login();
	}
	$context = get_context_instance(CONTEXT_SYSTEM);

	//check role assign
	if($USER->archetype == $CFG->userTypeStudent){
		redirect($CFG->wwwroot."/");
	}
	
	// Start setting up the page
	$header = "$SITE->shortname";
	$params = array();
	$PAGE->set_context($context);
	$PAGE->set_url('/messages/index.php', $params);
	$PAGE->set_pagelayout('general');
	$PAGE->set_pagetype('my-index');
	$PAGE->blocks->add_region('content');
	$PAGE->set_subpage($currentpage->id);
	$PAGE->set_title($header);
	$PAGE->set_heading($header);
	$PAGE->navbar->add(get_string('manage_messages','messages'), new moodle_url('/messages/index.php'));
	$PAGE->navbar->add(get_string('delete_messages','messages'));
	
	$id = optional_param('id', 0, PARAM_INT);
	$action = optional_param('action', 0, PARAM_INT);
	if($id!=''){
		$select = "SELECT * FROM {$CFG->prefix}my_messages where deleted='0' and id='".$id."'";
		$get_cms = $DB->get_record_sql($select);
		if($get_cms=='')
		{
			redirect($CFG->wwwroot."/messages/index.php");
		}
	}
	
	if($action=='1')
	{
		// code below modified to delete record permanantly.
		$recordDeleteSql = "DELETE from {$CFG->prefix}my_messages where id='".$id."'";
		$updid = $DB->execute($recordDeleteSql);
		
		/*$newdiscussion = new stdClass();
		$newdiscussion->deleted=1;
		$newdiscussion->id=$id;
		$updid = $DB->update_record('my_messages', $newdiscussion);*/
		
		$_SESSION['update_msg']=get_string('record_deleted');
		redirect($CFG->wwwroot."/messages/index.php");
	}
	echo $OUTPUT->header();
	$message = $get_cms->message_content.'</br>';
	$message .= get_string("delete_msg");
	echo '<div class="box generalbox" id="notice">
			<p>'.$message.'</p>
			<div class="buttons">
				<div class="singlebutton">
					<form action="delete_message.php" method="post">
						<div>
							<input type="submit" value="Yes">
							<input type="hidden" value="'.$id.'" name="id">
							<input type="hidden" value="1" name="action">
						</div>
					</form>
				</div>
				<div class="singlebutton">
					<form action="index.php" method="post">
						<div>
							<input type="submit" value="No">
						</div>
					</form>
				</div>
			</div>
		</div>';
	
	echo $OUTPUT->footer();
?>