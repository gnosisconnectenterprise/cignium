<?php
require_once("config.php");
require_once("lib.php");
GLOBAL $conn;
$userRecordSql = "SELECT u.id,u.firstname,u.lastname,u.username,u.job_title,u.company,dm.departmentid, 
GROUP_CONCAT(IF(gd.id IS NULL,g.id,'') SEPARATOR ',') AS global_groupid, GROUP_CONCAT(IF(gd.id IS NOT NULL,g.id,'') SEPARATOR ',') AS groupid
,c.title as company_name,CONCAT_WS(' ',um.firstname,um.lastname) as inline_manager_name,CONCAT_WS(' ',up.firstname,up.lastname) as manager_name
FROM mdl_user u
LEFT JOIN mdl_role_assignments ra ON ra.userid = u.id AND ra.contextid = 1
LEFT JOIN mdl_department_members AS dm ON u.id = dm.userid
LEFT JOIN mdl_groups_members AS gm ON u.id = gm.userid
LEFT JOIN mdl_group_department AS gd ON gd.team_id = gm.groupid
LEFT JOIN mdl_groups AS g ON g.id = gm.groupid
LEFT JOIN mdl_department AS d ON dm.departmentid = d.id
LEFT JOIN mdl_company as c ON c.id = u.company
LEFT JOIN mdl_user as um ON u.inline_manager_id = um.id
LEFT JOIN mdl_user as up ON u.parent_id = up.id
WHERE ra.roleid IN (3,5) AND u.deleted = 0 AND dm.departmentid IS NOT NULL AND d.deleted = 0
GROUP BY u.id";

$userStatSql = "SELECT ca.user_id,IF(status_course IS NULL,'not started', IF(FIND_IN_SET('not started', status_course) && NOT FIND_IN_SET('incomplete', status_course) && NOT FIND_IN_SET('completed', status_course) && NOT FIND_IN_SET('passed', status_course) && NOT FIND_IN_SET('failed', status_course),'not started', IF(FIND_IN_SET('incomplete', status_course) || FIND_IN_SET('failed', status_course),'in progress', IF((FIND_IN_SET('completed', status_course) || FIND_IN_SET('passed', status_course)) && NOT FIND_IN_SET('incomplete', status_course) && NOT FIND_IN_SET('failed', status_course) && NOT FIND_IN_SET('not started', status_course),'completed','in progress')))) AS course_status,count(*) as enrol_count FROM ( SELECT ru.user_id,ru.course_id, GROUP_CONCAT(IF (rd.course_status IS NULL, 'not started',rd.course_status) SEPARATOR ',') AS status_course FROM mdl_reports_users_courses AS ru LEFT JOIN mdl_course AS c ON ru.course_id = c.id LEFT JOIN mdl_course_modules cm ON cm.course = c.id LEFT JOIN (SELECT * FROM ((SELECT s.id,s.course,s.name,18 AS module_id FROM mdl_scorm s WHERE s.is_active = 1)UNION(SELECT r.id,r.course,r.name,17 AS module_id FROM mdl_resource r WHERE r.is_active = 1)) as a) as ad ON ad.module_id = cm.module AND cm.instance = ad.id AND cm.course = ad.course LEFT JOIN mdl_reports_data AS rd ON ru.user_id = rd.user_id AND rd.module_id = cm.module AND rd.asset_id = cm.instance
WHERE c.deleted = 0 AND c.id IS NOT NULL AND c.coursetype_id != 2 AND ad.id IS NOT NULL GROUP BY ru.user_id,ru.course_id ORDER BY ru.user_id,ru.course_id ASC,cm.instance ASC) AS ca GROUP BY ca.user_id,course_status";

$result	= getSqlData($userStatSql);

$userArray = array();
if(mysqli_num_rows($result)>0){
	while ($stat = mysqli_fetch_object($result)){
		$userArray[$stat->user_id][str_replace(' ','_',$stat->course_status)] = $stat->enrol_count;
	}
}
$truncateSql = "TRUNCATE `mdl_report_learner_performance`";
$truncateData	= executeSql($truncateSql);
$userRecords	= getSqlData($userRecordSql);
$userData = array();
$insertStr = "INSERT INTO `mdl_report_learner_performance` (`user_id`, `first_name`, `last_name`, `user_name`, `job_title`, `company`, `department`, `group_id`,global_groupid, `enrolled`, `not_started`, `in_progress`, `completed`,company_name,manager_name,inline_manager_name) VALUES ";

$values = '';
$count = 1;
$insertChunk = 100;
if(mysqli_num_rows($userRecords)>0){
	while ($user = mysqli_fetch_object($userRecords)){
		if(isset($userArray[$user->id]['completed']))
			$user->completed = $userArray[$user->id]['completed'];
		else
			$user->completed = 0;
		if(isset($userArray[$user->id]['in_progress']))
			$user->in_progress = $userArray[$user->id]['in_progress'];
		else
			$user->in_progress = 0;
		if(isset($userArray[$user->id]['not_started']))
			$user->not_started = $userArray[$user->id]['not_started'];
		else
			$user->not_started = 0;
		$user->total_enrolled = 0;
		$user->total_enrolled = $user->completed + $user->in_progress + $user->not_started;
		$values .= "(".$user->id.",'".mysqli_real_escape_string($conn,$user->firstname)."', '".mysqli_real_escape_string($conn,$user->lastname)."', '".$user->username."', ".$user->job_title.", ".$user->company.", ".$user->departmentid.", '".$user->groupid."', '".$user->global_groupid."', ".$user->total_enrolled.", ".$user->not_started.", ".$user->in_progress.", ".$user->completed.",'".mysqli_real_escape_string($conn,trim($user->company_name))."','".mysqli_real_escape_string($conn,trim($user->manager_name))."','".mysqli_real_escape_string($conn,trim($user->inline_manager_name))."'),";
		if($count%$insertChunk == 0){
			$execSql = RTRIM($insertStr.$values,',');
			executeSql($execSql);
			$values = '';
		}
		$count++;
	}
	if($values != ''){
		$execSql = RTRIM($insertStr.$values,',');
		executeSql($execSql);
	}
}
echo 'done learner performance report';
?>