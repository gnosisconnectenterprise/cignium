<?php
require_once("config.php");
require_once("lib.php");
function getOverallRequestReport(){


	$queryAll = "TRUNCATE TABLE  `mdl_report_request_report`";
	executeSql($queryAll);

	$queryFields = "SELECT *";				 
	$query = " FROM 
		((
		SELECT dif_id, id, classid, course_id, fullname, name, program_id, request_type, user_id, sender_id, sender_parent, receiver_id, receiver_parent, request_status, request_text,  requested_on, reponse_on, startdate, enddate, no_of_seats, enrolmenttype, createdby, action_taken_by, declined_remarks, sender_name, sender_username, receiver_name, receiver_username, course_name, class_name, program_name,
		sender_teamtitle, sender_team_id, sender_departmenttitle, sender_department_id, receiver_teamtitle,receiver_team_id,receiver_departmenttitle,
		receiver_department_id, jobid, comid, job_title, company, is_sender_deleted, is_receiver_deleted, is_sender_dept_deleted, is_receiver_dept_deleted 
			FROM (SELECT CONCAT_WS('_', CAST(`s`.`id` AS CHAR CHARSET utf8), 
CAST(0 AS CHAR CHARSET utf8), CAST(`c`.`id` AS CHAR CHARSET utf8),`mu_sender`.`id`) AS `dif_id`,
`se`.`id` AS `id`,`s`.`id` AS `classid`,`c`.`id` AS `course_id`,`c`.`fullname` AS `fullname`,`s`.`name` AS `name`,0 AS `program_id`,'classroom' AS `request_type`,`mu_sender`.`id` AS `user_id`,`mu_sender`.`id` AS `sender_id`,`mu_sender`.`job_title` AS `jobid`,`mu_sender`.`company` AS `comid`,`jt`.`title` AS `job_title`,`com`.`title` AS `company`,`mu_sender`.`parent_id` AS `sender_parent`,`mu_sender`.`deleted` AS `is_sender_deleted`,`mu_receiver`.`deleted` AS `is_receiver_deleted`,`md_sender`.`deleted` AS `is_sender_dept_deleted`,`md_receiver`.`deleted` AS `is_receiver_dept_deleted`,`mu_receiver`.`id` AS `receiver_id`,`mu_receiver`.`parent_id` AS `receiver_parent`,`se`.`is_approved` AS `request_status`, CONCAT(CONCAT('Request for classroom (',' ',`c`.`fullname`,':',' ',`s`.`name`,')','##__TIME__## by ',`mu_sender`.`firstname`,' ',`mu_sender`.`lastname`),'(',`mu_sender`.`username`,')') AS `request_text`,`se`.`request_date` AS `requested_on`,`se`.`response_date` AS `reponse_on`,`s`.`startdate` AS `startdate`,`s`.`enddate` AS `enddate`,`s`.`no_of_seats` AS `no_of_seats`,`s`.`enrolmenttype` AS `enrolmenttype`,`s`.`createdby` AS `createdby`,`se`.`action_taken_by` AS `action_taken_by`,`se`.`declined_remarks` AS `declined_remarks`,concat(`mu_sender`.`firstname`,' ',`mu_sender`.`lastname`) AS `sender_name`,`mu_sender`.`username` AS `sender_username`,concat(`mu_receiver`.`firstname`,' ',`mu_receiver`.`lastname`) AS `receiver_name`,`mu_receiver`.`username` AS `receiver_username`,`c`.`fullname` AS `course_name`,`s`.`name` AS `class_name`,'' AS `program_name`,if(`mgm_sender`.`groupid`,group_concat(distinct `mg_sender`.`name` order by `mg_sender`.`name` ASC separator ','),'') AS `sender_teamtitle`,if(`mgm_sender`.`groupid`,group_concat(distinct `mg_sender`.`id` order by `mg_sender`.`name` ASC separator ','),'') AS `sender_team_id`,if(`mdm_sender`.`departmentid`,group_concat(distinct `md_sender`.`title` order by `md_sender`.`title` ASC separator ','),'') AS `sender_departmenttitle`,if(`mdm_sender`.`departmentid`,group_concat(distinct `md_sender`.`id` order by `md_sender`.`title` ASC separator ','),'') AS `sender_department_id`,if(`mgm_receiver`.`groupid`,group_concat(distinct `mg_receiver`.`name` order by `mg_receiver`.`name` ASC separator ','),'') AS `receiver_teamtitle`,if(`mgm_receiver`.`groupid`,group_concat(distinct `mg_receiver`.`id` order by `mg_receiver`.`name` ASC separator ','),'') AS `receiver_team_id`,if(`mdm_receiver`.`departmentid`,group_concat(distinct `md_receiver`.`title` order by `md_receiver`.`title` ASC separator ','),'') AS `receiver_departmenttitle`,if(`mdm_receiver`.`departmentid`,group_concat(distinct `md_receiver`.`id` order by `md_receiver`.`title` ASC separator ','),'') AS `receiver_department_id` from ((((((((((((((`mdl_scheduler` `s` left join `mdl_course` `c` on((`c`.`id` = `s`.`course`))) left join `mdl_scheduler_enrollment` `se` on((`se`.`scheduler_id` = `s`.`id`))) left join `mdl_user` `mu_sender` on((`mu_sender`.`id` = `se`.`userid`))) left join `mdl_user` `mu_receiver` on((`mu_receiver`.`id` = `se`.`action_taken_by`))) left join `mdl_job_title` `jt` on((`jt`.`id` = `mu_sender`.`job_title`))) left join `mdl_company` `com` on((`com`.`id` = `mu_sender`.`company`))) left join `mdl_groups_members` `mgm_sender` on((`mgm_sender`.`userid` = `se`.`userid`))) left join `mdl_groups` `mg_sender` on((`mgm_sender`.`groupid` = `mg_sender`.`id`))) left join `mdl_groups_members` `mgm_receiver` on((`mgm_receiver`.`userid` = `se`.`action_taken_by`))) left join `mdl_groups` `mg_receiver` on((`mgm_receiver`.`groupid` = `mg_receiver`.`id`))) left join `mdl_department_members` `mdm_sender` on((`mdm_sender`.`userid` = `se`.`userid`))) left join `mdl_department` `md_sender` on((`md_sender`.`id` = `mdm_sender`.`departmentid`))) left join `mdl_department_members` `mdm_receiver` on((`mdm_receiver`.`userid` = `se`.`action_taken_by`))) left join `mdl_department` `md_receiver` on((`md_receiver`.`id` = `mdm_receiver`.`departmentid`))) where (1 = 1) group by `s`.`id`,`se`.`userid`) as vcr 
		)
		UNION
		(
		SELECT dif_id, id, classid, course_id, fullname, name, program_id, request_type, user_id, sender_id, sender_parent, receiver_id, receiver_parent, request_status, request_text,  requested_on, reponse_on, startdate, enddate, no_of_seats, enrolmenttype, createdby, action_taken_by, declined_remarks, sender_name, sender_username, receiver_name, receiver_username, course_name, class_name, program_name,
		sender_teamtitle, sender_team_id, sender_departmenttitle, sender_department_id, receiver_teamtitle,receiver_team_id,receiver_departmenttitle,
		receiver_department_id, jobid, comid, job_title, company, is_sender_deleted, is_receiver_deleted, is_sender_dept_deleted, is_receiver_dept_deleted 
			
			FROM (SELECT CONCAT_WS('_', CAST(`c`.`id` AS CHAR CHARSET utf8), CAST(`c`.`program_id` AS CHAR CHARSET utf8), CAST(`c`.`course_id` AS CHAR CHARSET utf8),`c`.`user_id`) AS `dif_id`,`c`.`id` AS `id`,0 AS `classid`,`c`.`course_id` AS `course_id`,`mc`.`fullname` AS `fullname`,'' AS `name`,`c`.`program_id` AS `program_id`,`c`.`request_type` AS `request_type`,`c`.`user_id` AS `user_id`,`mu_sender`.`id` AS `sender_id`,`mu_sender`.`deleted` AS `is_sender_deleted`,`mu_receiver`.`deleted` AS `is_receiver_deleted`,`md_sender`.`deleted` AS `is_sender_dept_deleted`,`md_receiver`.`deleted` AS `is_receiver_dept_deleted`,`mu_sender`.`job_title` AS `jobid`,`mu_sender`.`company` AS `comid`,`jt`.`title` AS `job_title`,`com`.`title` AS `company`,`mu_sender`.`parent_id` AS `sender_parent`,`mu_receiver`.`id` AS `receiver_id`,`mu_receiver`.`parent_id` AS `receiver_parent`,`c`.`request_status` AS `request_status`,`c`.`request_text` AS `request_text`,`c`.`requested_on` AS `requested_on`,`c`.`reponse_on` AS `reponse_on`,1 AS `startdate`,2 AS `enddate`,0 AS `no_of_seats`,0 AS `enrolmenttype`,0 AS `createdby`,`c`.`recepient_id` AS `action_taken_by`, IF((`c`.`course_id` > 0),`mucm`.`remarks`,`pucm`.`remarks`) AS `declined_remarks`, CONCAT(`mu_sender`.`firstname`,' ',`mu_sender`.`lastname`) AS `sender_name`,`mu_sender`.`username` AS `sender_username`, CONCAT(`mu_receiver`.`firstname`,' ',`mu_receiver`.`lastname`) AS `receiver_name`,`mu_receiver`.`username` AS `receiver_username`,`mc`.`fullname` AS `course_name`,'' AS `class_name`,`mp`.`name` AS `program_name`, IF(`mgm_sender`.`groupid`, GROUP_CONCAT(DISTINCT `mg_sender`.`name`
ORDER BY `mg_sender`.`name` ASC SEPARATOR ','),'') AS `sender_teamtitle`, IF(`mgm_sender`.`groupid`, GROUP_CONCAT(DISTINCT `mg_sender`.`id`
ORDER BY `mg_sender`.`name` ASC SEPARATOR ','),'') AS `sender_team_id`, IF(`mdm_sender`.`departmentid`, GROUP_CONCAT(DISTINCT `md_sender`.`title`
ORDER BY `md_sender`.`title` ASC SEPARATOR ','),'') AS `sender_departmenttitle`, IF(`mdm_sender`.`departmentid`, GROUP_CONCAT(DISTINCT `md_sender`.`id`
ORDER BY `md_sender`.`title` ASC SEPARATOR ','),'') AS `sender_department_id`, IF(`mgm_receiver`.`groupid`, GROUP_CONCAT(DISTINCT `mg_receiver`.`name`
ORDER BY `mg_receiver`.`name` ASC SEPARATOR ','),'') AS `receiver_teamtitle`, IF(`mgm_receiver`.`groupid`, GROUP_CONCAT(DISTINCT `mg_receiver`.`id`
ORDER BY `mg_receiver`.`name` ASC SEPARATOR ','),'') AS `receiver_team_id`, IF(`mdm_receiver`.`departmentid`, GROUP_CONCAT(DISTINCT `md_receiver`.`title`
ORDER BY `md_receiver`.`title` ASC SEPARATOR ','),'') AS `receiver_departmenttitle`, IF(`mdm_receiver`.`departmentid`, GROUP_CONCAT(DISTINCT `md_receiver`.`id`
ORDER BY `md_receiver`.`title` ASC SEPARATOR ','),'') AS `receiver_department_id`
FROM ((((((((((((((((`mdl_course_request_log` `c`
LEFT JOIN `mdl_course` `mc` ON((`mc`.`id` = `c`.`course_id`)))
LEFT JOIN `mdl_programs` `mp` ON((`mp`.`id` = `c`.`program_id`)))
LEFT JOIN `mdl_user` `mu_sender` ON((`mu_sender`.`id` = `c`.`user_id`)))
LEFT JOIN `mdl_user` `mu_receiver` ON((`mu_receiver`.`id` = `c`.`recepient_id`)))
LEFT JOIN `mdl_job_title` `jt` ON((`jt`.`id` = `mu_sender`.`job_title`)))
LEFT JOIN `mdl_company` `com` ON((`com`.`id` = `mu_sender`.`company`)))
LEFT JOIN `mdl_groups_members` `mgm_sender` ON((`mgm_sender`.`userid` = `c`.`user_id`)))
LEFT JOIN `mdl_groups` `mg_sender` ON((`mgm_sender`.`groupid` = `mg_sender`.`id`)))
LEFT JOIN `mdl_groups_members` `mgm_receiver` ON((`mgm_receiver`.`userid` = `c`.`recepient_id`)))
LEFT JOIN `mdl_groups` `mg_receiver` ON((`mgm_receiver`.`groupid` = `mg_receiver`.`id`)))
LEFT JOIN `mdl_department_members` `mdm_sender` ON((`mdm_sender`.`userid` = `c`.`user_id`)))
LEFT JOIN `mdl_department` `md_sender` ON((`md_sender`.`id` = `mdm_sender`.`departmentid`)))
LEFT JOIN `mdl_department_members` `mdm_receiver` ON((`mdm_receiver`.`userid` = `c`.`recepient_id`)))
LEFT JOIN `mdl_department` `md_receiver` ON((`md_receiver`.`id` = `mdm_receiver`.`departmentid`)))
LEFT JOIN `mdl_user_course_mapping` `mucm` ON(((`mucm`.`courseid` = `c`.`course_id`) AND (`mucm`.`userid` = `c`.`user_id`))))
LEFT JOIN `mdl_program_user_mapping` `pucm` ON(((`pucm`.`program_id` = `c`.`program_id`) AND (`pucm`.`user_id` = `c`.`user_id`))))
WHERE (1 = 1)
GROUP BY `c`.`id`)  vor		 
		)
		) as c ";
	$query = $queryFields.$query;	
	
	$sql = "INSERT INTO mdl_report_request_report 
		(dif_id,id,classid,course_id,fullname,name, program_id, request_type, user_id, sender_id, sender_parent, receiver_id, receiver_parent, request_status, request_text,  requested_on, reponse_on, startdate, enddate, no_of_seats, enrolmenttype, createdby, action_taken_by, declined_remarks, sender_name, sender_username, receiver_name, receiver_username, course_name, class_name, program_name,sender_teamtitle, sender_team_id, sender_departmenttitle, sender_department_id, receiver_teamtitle,receiver_team_id,receiver_departmenttitle,receiver_department_id, jobid, comid, job_title, company, is_sender_deleted, is_receiver_deleted, is_sender_dept_deleted, is_receiver_dept_deleted) ($query)"; 
	
	

	echo executeSql($sql);
        
        echo "<br/>done request report";

}
getOverallRequestReport();

	
?>