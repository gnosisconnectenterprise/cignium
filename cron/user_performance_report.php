<?php
require_once("config.php");

require_once("lib.php");
GLOBAL $conn;
$userStatusSql = "SELECT c.id as course_id,c.credithours,r.user_id,cm.instance,c.fullname as course_name,ad.name as asset_name,IF(rd.course_status IS NOT NULL,rd.course_status,'not started') as asset_status,rd.score,rd.time_spent,rd.completion_date,rd.last_accessed FROM mdl_reports_users_courses r LEFT JOIN mdl_user u ON u.id = r.user_id LEFT JOIN mdl_department_members dm ON dm.userid = u.id LEFT JOIN mdl_course c ON r.course_id = c.id LEFT JOIN mdl_course_modules cm ON cm.course = c.id LEFT JOIN (SELECT * FROM ((SELECT s.id,s.course,s.name,18 AS module_id FROM mdl_scorm s WHERE s.is_active = 1)UNION(SELECT r.id,r.course,r.name,17 AS module_id FROM mdl_resource r WHERE r.is_active = 1)) as a) as ad ON ad.module_id = cm.module AND cm.instance = ad.id AND cm.course = ad.course LEFT JOIN mdl_reports_data AS rd ON r.user_id = rd.user_id AND rd.module_id = cm.module AND rd.asset_id = cm.instance WHERE c.deleted = 0 AND c.coursetype_id = 1 AND cm.id IS NOT NULL AND ad.name IS NOT NULL AND dm.userid IS NOT NULL";

$courseStatusSql = "SELECT c.id AS course_id,r.user_id,IF(GROUP_CONCAT(IF(rd.course_status IS NOT NULL,rd.course_status,'not started')) IS NOT NULL,GROUP_CONCAT(IF(rd.course_status IS NOT NULL,rd.course_status,'not started')),'not started') AS asset_status FROM mdl_reports_users_courses r LEFT JOIN mdl_course c ON r.course_id = c.id LEFT JOIN mdl_course_modules cm ON cm.course = c.id LEFT JOIN ( SELECT * FROM (( SELECT s.id,s.course,s.name,18 AS module_id FROM mdl_scorm s WHERE s.is_active = 1) UNION( SELECT r.id,r.course,r.name,17 AS module_id FROM mdl_resource r WHERE r.is_active = 1)) AS a) AS ad ON ad.module_id = cm.module AND cm.instance = ad.id AND cm.course = ad.course LEFT JOIN mdl_reports_data AS rd ON r.user_id = rd.user_id AND rd.module_id = cm.module AND rd.asset_id = cm.instance,( SELECT @a := 0) AS n WHERE c.deleted = 0 AND c.coursetype_id = 1 AND cm.id IS NOT NULL AND ad.name IS NOT NULL GROUP BY c.id,r.user_id";

$courseStatusArray = array();
$courseStatusData = getSqlData($courseStatusSql);

if(mysqli_num_rows($courseStatusData)>0){
	while ($courseStatus = mysqli_fetch_object($courseStatusData)){
		$key = $courseStatus->user_id.'_'.$courseStatus->course_id;
		$status_course = explode(',',$courseStatus->asset_status);
		if(empty($status_course)){
			$courseStat = 'not started';
		}elseif(in_array('not started', $status_course) && !in_array('incomplete', $status_course) && !in_array('completed', $status_course) && !in_array('passed', $status_course) && !in_array('failed', $status_course) ){
			$courseStat = 'not started';
		}elseif(in_array('incomplete', $status_course) || in_array('failed', $status_course)){
			$courseStat = 'in progress';
		}elseif((in_array('completed', $status_course) || in_array('passed', $status_course)) && !in_array('incomplete', $status_course)&& !in_array('not started', $status_course) && !in_array('failed', $status_course) ){
			$courseStat ='completed';
		}else{
			$courseStat = 'in progress';
		}
		$courseStatusArray[$key] = $courseStat;
	}
}
$userStatusData	= getSqlData($userStatusSql);
$count = 1;
$insertChunk = 20;
$truncateSql = "TRUNCATE `mdl_report_learner_performance_details`";
$truncateData	= executeSql($truncateSql);
$insertStr = "INSERT INTO `mdl_report_learner_performance_details` (`user_id`, `course_id`, `asset_id`, `course_name`, `asset_name`, `score`, `time_spent`, `completion_date`,`last_accessed`,`asset_status`,`course_status`,`credithours`) VALUES ";
$values = '';
echo '<pre>';
if(mysqli_num_rows($userStatusData)>0){
	while ($userStatus = mysqli_fetch_object($userStatusData)){
		$score = 0;
		$time_spent = 0;
		$completion_date = 0;
		$last_accessed = 0;
		if($userStatus->score)
			$score = $userStatus->score;
		if($userStatus->time_spent)
			$time_spent = $userStatus->time_spent;
		if($userStatus->completion_date)
			$completion_date = $userStatus->completion_date;

		if($userStatus->last_accessed)
			$last_accessed = $userStatus->last_accessed;

		$key = $userStatus->user_id.'_'.$userStatus->course_id;
		$values .= "(".$userStatus->user_id.",".$userStatus->course_id.", ".$userStatus->instance.", '".mysqli_real_escape_string($conn,$userStatus->course_name)."', '".mysqli_real_escape_string($conn,$userStatus->asset_name)."', ".$score.", '".$time_spent."', ".$completion_date.", ".$last_accessed.", '".$userStatus->asset_status."', '".$courseStatusArray[$key]."', '".$userStatus->credithours."'),";

		if($count%$insertChunk == 0){
			$execSql = RTRIM($insertStr.$values,',');
			executeSql($execSql);
			$values = '';
		}
		$count++;
	}
	if($values != ''){
		$execSql = RTRIM($insertStr.$values,',');
		executeSql($execSql);
	}
}
echo '<br/>done user performance report';
?>