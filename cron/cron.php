<?php
/*
 * cron file executed all crons in a single file
 * creatd on 6th oct 2016 by gnosis team
 */
$cron_messageStart = "cron started successfully on time:".time();
sendEmailToAdmin($cron_messageStart, '');
try {
    require_once("learner_performance_report.php");
} catch (Exception $ex) {
    $errormessage = $ex->getMessage();
    $cronFileName = "learner_performance_report.php";

    sendEmailToAdmin($errormessage, $cronFileName);
    exit;
}

try {

    require_once("user_performance_report.php");
} catch (Exception $ex) {
    $errormessage = $ex->getMessage();
    $cronFileName = "user_performance_report.php";
    sendEmailToAdmin($errormessage, $cronFileName);
    exit;
}

try {
    require_once("request_report.php");
} catch (Exception $ex) {
    $errormessage = $ex->getMessage();
    $cronFileName = "request_report.php";
    sendEmailToAdmin($errormessage, $cronFileName);
    exit;
}

try {
    require_once("credithours_report.php");
} catch (Exception $ex) {
    $errormessage = $ex->getMessage();
    $cronFileName = "credithours_report.php";
    sendEmailToAdmin($errormessage, $cronFileName);
    exit;
}

try {
    require_once("clasroom_course_report.php");
} catch (Exception $ex) {
    $errormessage = $ex->getMessage();
    $cronFileName = "clasroom_course_report.php";
    sendEmailToAdmin($errormessage, $cronFileName);
    exit;
}


try {
    require_once("online_course_usage_report.php");
} catch (Exception $ex) {
    $errormessage = $ex->getMessage();
    $cronFileName = "online_course_usage_report.php";
    sendEmailToAdmin($errormessage, $cronFileName);
    exit;
}

/*
 * update cron time in mdl_cron_job on the basis of element=report_data_sync
 */
$query	= "SELECT * FROM mdl_cron_job WHERE element = 'report_data_sync'";
$result	= getSqlData($query);
 $currenttime = time();
if(mysqli_num_rows($result)>0){
	while ($data = mysqli_fetch_object($result)){
		$updateSql = "UPDATE mdl_cron_job SET updated_on=".$currenttime." WHERE id=".$data->id;
                executeSql($updateSql);
	}
}else{
   
    $insertSql = "INSERT INTO mdl_cron_job (`element`,`value`,`created_on`,`updated_on`) values('report_data_sync',1,$currenttime,$currenttime)";
    executeSql($insertSql);
}

$cron_messageEnd = "Cron has been finished successfully on :".time();
sendEmailToAdmin($cron_messageEnd, '');

function sendEmailToAdmin($errormessage, $cronFileName) {

    include_once(dirname(dirname(__FILE__)) . '/email/phpmailer/class.phpmailer.php'); // path to the PHPMailer class
    include_once(dirname(dirname(__FILE__)) . '/email/phpmailer/class.smtp.php');

    date_default_timezone_set('America/New_York');

    $subject = "Cron failed at cignium";
    $currenttime = time();
    $message = "<b>Error Message :</b>" . addslashes($errormessage);
    $message .="<br/><br/><b>Cron File Name :</b>" . $cronFileName;
    $message .="<br/><br/><b>Date : </b>" . date('M d, Y h:i A T', $currenttime);

    $mail = new PHPMailer();
    $mail->IsSMTP();  // telling the class to use SMTP
    $mail->SMTPSecure = 'tls';
    $mail->Mailer = "smtp";

    $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
    $mail->Port = 587;
    $mail->SMTPAuth = true; // turn on SMTP authentication
    $mail->Username = "AKIAJMAB3VND3JQF7JQQ"; // SMTP username
    $mail->Password = "ApyAW81ULkxOW2WC2BQK4qz5s5p3ITsYFrOAT1TajQgg"; // SMTP password

    $fromAddress = "support@gnosisconnect.com";
    $name = "Learning Portal Administrator-cignium";

    $visitor_email = 'nabab.dhakar@compunnel.in';
    $mail->AddAddress($visitor_email, "Nabab dhakar");

    $mail->SetFrom($fromAddress, $name);

    $mail->Subject = $subject;

    $mail->isHTML(true);
    $mail->Encoding = 'quoted-printable';
    $mail->Body = ($message);
    $mail->AltBody = "\n$message\n";

    $mail->Send();
}
?>