<?php
require_once("config.php");
require_once("lib.php");
function getCreditHoursReport(){


	$queryAll = "TRUNCATE TABLE  `mdl_report_course_credit_hours`";
	executeSql($queryAll);
	//Online Course Credit Hour			 
	$query = "select `mrlpd`.`user_id` AS `user_id`,'learner' as user_type,`mu`.`firstname` AS `firstname`,
`mu`.`lastname` AS `lastname`, CONCAT(`mu`.`firstname`,' ',`mu`.`lastname`) AS `userfullname`,
`mu`.`username` AS `username`, `mdm`.`departmentid` AS `departmentid`,
GROUP_CONCAT(distinct IF(gd.id IS NOT NULL,g.id,'') SEPARATOR ',') AS groupid,
GROUP_CONCAT(distinct IF(gd.id IS NULL,g.id,'') SEPARATOR ',') AS global_groupid,
IF((`md`.`is_external` = 1), CONCAT('*',`md`.`title`),`md`.`title`) AS `departmenttitle`,
IF((CONCAT(`mu_manager`.`firstname`,' ',`mu_manager`.`lastname`) <> ''), CONCAT(`mu_manager`.`firstname`,' ',`mu_manager`.`lastname`),'') AS `usermanager`,
CONCAT_WS(' ',um.firstname,um.lastname) as inline_manager_name,
mrlpd.credithours AS `credithours`,`mc`.`id` AS `courseid`, `mc`.`fullname` AS `coursename`,
 `mc`.`coursetype_id` AS `coursetype_id`, `mrlpd`.`completion_date` AS `complition_time`,
 0 AS `class_id`,'' AS `class_name`,0 AS `class_instructor`, `mu`.`job_title` AS `jobid`,`mu`.`company` AS `comid`,`jt`.`title` AS `job_title`,
`com`.`title` AS `company`
FROM mdl_report_learner_performance_details as mrlpd
LEFT JOIN mdl_user as mu ON (mrlpd.user_id=mu.id)
LEFT JOIN `mdl_job_title` `jt` ON(`jt`.`id` = `mu`.`job_title`)
LEFT JOIN `mdl_company` `com` ON(`com`.`id` = `mu`.`company`)
LEFT JOIN `mdl_user` `mu_manager` ON(`mu`.`parent_id` = `mu_manager`.`id`)
LEFT JOIN mdl_user as um ON mu.inline_manager_id = um.id
LEFT JOIN `mdl_department_members` `mdm` ON(`mdm`.`userid` = `mu`.`id`)
LEFT JOIN `mdl_department` `md` ON(`md`.`id` = `mdm`.`departmentid`)
LEFT JOIN mdl_groups_members AS gm ON gm.userid = mrlpd.user_id and gm.is_active=1
LEFT JOIN mdl_group_department as gd ON gd.team_id = gm.groupid
LEFT JOIN mdl_groups AS g ON gm.groupid = g.id
LEFT JOIN `mdl_course` `mc` ON((`mrlpd`.`course_id` = `mc`.`id`) AND (`mc`.`id` <> '1') AND (`mc`.`publish` = '1') AND (`mc`.`deleted` = '0') AND (`mc`.`coursetype_id` = '1'))

where course_status='completed' and mrlpd.completion_date!=0 and mu.deleted=0 and md.deleted=0 group by course_id,mu.id  ";
	
	
	 $sql = "INSERT INTO mdl_report_course_credit_hours 
		(user_id,user_type,firstname,lastname,userfullname,username, departmentid,groupid,global_groupid, departmenttitle, usermanager,inline_manager_name, credithours, courseid, coursename, coursetype_id, complition_time, class_id,  class_name, class_instructor, jobid, comid, job_title, company) ($query)";

	echo executeSql($sql);
	
	// Classroom Learner Credit Hour
	$query = "SELECT `mse`.`userid` AS `user_id`,'learner' as user_type,`mu`.`firstname` AS `firstname`,
`mu`.`lastname` AS `lastname`,  CONCAT(`mu`.`firstname`,' ',`mu`.`lastname`) AS `userfullname`,
`mu`.`username` AS `username`, md.id as departmentid,
(select GROUP_CONCAT(IF(gd.id IS NOT NULL,mgm.groupid,'') SEPARATOR ',')  from mdl_groups_members as mgm LEFT JOIN mdl_group_department as gd ON(mgm.groupid=gd.team_id) where mgm.userid=mu.id and mgm.is_active=1) as groupid,
(select GROUP_CONCAT(IF(gd.id IS NULL,mgm.groupid,'') SEPARATOR ',')  from mdl_groups_members as mgm LEFT JOIN mdl_group_department as gd ON(mgm.groupid=gd.team_id) where mgm.userid=mu.id and mgm.is_active=1) as global_groupid,
IF((`md`.`is_external` = 1), CONCAT('*',`md`.`title`),`md`.`title`) AS `departmenttitle`,
IF((CONCAT(`mu_manager`.`firstname`,' ',`mu_manager`.`lastname`) <> ''), CONCAT(`mu_manager`.`firstname`,' ',`mu_manager`.`lastname`),'') AS `usermanager`,
CONCAT_WS(' ',um.firstname,um.lastname) as inline_manager_name,
 SUM(`mss`.`duration`) AS `credithours`,  `mc`.`id` AS `courseid`, `mc`.`fullname` AS `coursename`,
 `mc`.`coursetype_id` AS `coursetype_id`,  `ms`.`enddate` AS `complition_time`,   `ms`.`id` AS `class_id`,
 `ms`.`name` AS `class_name`, `ms`.`teacher` AS `class_instructor`,`mu`.`job_title` AS `jobid`, `mu`.`company` AS `comid`,
 `jt`.`title` AS `job_title`, `com`.`title` AS `company`
FROM `mdl_user` `mu`
LEFT JOIN `mdl_scheduler_enrollment` `mse` ON(`mu`.`id` = `mse`.`userid`)
LEFT JOIN `mdl_scheduler_slots` `mss` ON(`mse`.`scheduler_id` = `mss`.`schedulerid`)
LEFT JOIN `mdl_scheduler` `ms` ON(`ms`.`id` = `mss`.`schedulerid`)
LEFT JOIN `mdl_course` `mc` ON(`ms`.`course` = `mc`.`id`)
LEFT JOIN `mdl_user` `mu_manager` ON(`mu`.`parent_id` = `mu_manager`.`id`)
LEFT JOIN mdl_user as um ON mu.inline_manager_id = um.id
LEFT JOIN `mdl_job_title` `jt` ON(`jt`.`id` = `mu`.`job_title`)
LEFT JOIN `mdl_company` `com` ON(`com`.`id` = `mu`.`company`)
LEFT JOIN `mdl_department_members` `mdm` ON(`mdm`.`userid` = `mu`.`id`)
LEFT JOIN `mdl_department` `md` ON(`md`.`id` = `mdm`.`departmentid`)

WHERE ((`mse`.`userid` <> '') and mu.deleted=0 and md.deleted=0 AND (`mc`.`id` <> '') AND (`mc`.`id` <> '1') AND (`mc`.`publish` = '1') AND (`mc`.`deleted` = '0') AND (`mc`.`coursetype_id` = '2') AND (`mse`.`is_completed` = 1) AND (`ms`.`isclasscompleted` = 1))
GROUP BY `ms`.`id`,`mse`.`userid`
ORDER BY `ms`.`name` ";
	
	
	$sql = "INSERT INTO mdl_report_course_credit_hours
	(user_id,user_type,firstname,lastname,userfullname,username, departmentid,groupid,global_groupid, departmenttitle, usermanager,inline_manager_name, credithours, courseid, coursename, coursetype_id, complition_time, class_id,  class_name, class_instructor, jobid, comid, job_title, company) ($query)";
	echo executeSql($sql);
	
	// Classroom Instructor Credit Hour
	$query = "SELECT `mu`.`id` AS `user_id`,'instructor' as user_type,`mu`.`firstname` AS `firstname`,
`mu`.`lastname` AS `lastname`,CONCAT(`mu`.`firstname`,' ',`mu`.`lastname`) AS `userfullname`,
`mu`.`username` AS `username`,`mdm`.`departmentid` AS `departmentid`,
(select GROUP_CONCAT(IF(gd.id IS NOT NULL,mgm.groupid,'') SEPARATOR ',')  from mdl_groups_members as mgm LEFT JOIN mdl_group_department as gd ON(mgm.groupid=gd.team_id) where mgm.userid=mu.id and mgm.is_active=1) as groupid,
(select GROUP_CONCAT(IF(gd.id IS NULL,mgm.groupid,'') SEPARATOR ',')  from mdl_groups_members as mgm LEFT JOIN mdl_group_department as gd ON(mgm.groupid=gd.team_id) where mgm.userid=mu.id and mgm.is_active=1) as global_groupid,
IF((`md`.`is_external` = 1), CONCAT('*',`md`.`title`),`md`.`title`) AS `departmenttitle`,
IF((CONCAT(`mu_manager`.`firstname`,' ',`mu_manager`.`lastname`) <> ''), CONCAT(`mu_manager`.`firstname`,' ',`mu_manager`.`lastname`),'') AS `usermanager`,
CONCAT_WS(' ',um.firstname,um.lastname) as inline_manager_name,
SUM(`mss`.`duration`) AS `credithours`,`mc`.`id` AS `courseid`, `mc`.`fullname` AS `coursename`,
 `mc`.`coursetype_id` AS `coursetype_id`, `ms`.`enddate` AS `complition_time`, `ms`.`id` AS `class_id`,
`ms`.`name` AS `class_name`,`ms`.`teacher` AS `class_instructor`, `mu`.`job_title` AS `jobid`,`mu`.`company` AS `comid`,
`jt`.`title` AS `job_title`,`com`.`title` AS `company`
FROM `mdl_user` `mu`
LEFT JOIN `mdl_scheduler` `ms` ON(`ms`.`teacher` = `mu`.`id`)
LEFT JOIN `mdl_scheduler_slots` `mss` ON(`ms`.`id` = `mss`.`schedulerid`)
LEFT JOIN `mdl_course` `mc` ON(`ms`.`course` = `mc`.`id`)
LEFT JOIN `mdl_user` `mu_manager` ON(`mu`.`parent_id` = `mu_manager`.`id`)
LEFT JOIN mdl_user as um ON mu.inline_manager_id = um.id
LEFT JOIN `mdl_job_title` `jt` ON(`jt`.`id` = `mu`.`job_title`)
LEFT JOIN `mdl_company` `com` ON(`com`.`id` = `mu`.`company`)
LEFT JOIN `mdl_department_members` `mdm` ON(`mdm`.`userid` = `mu`.`id`)
LEFT JOIN `mdl_department` `md` ON(`md`.`id` = `mdm`.`departmentid`)

WHERE 1 = 1 and md.deleted=0 and mu.deleted=0 and  `mu`.`is_instructor` = 1 AND ms.isclasscompleted=1  AND `md`.`deleted` = '0' AND `mu`.`deleted` = '0' AND `mc`.`deleted` = '0' AND `mc`.`publish` = '1'


GROUP BY `ms`.`id`,`mu`.`id`
ORDER BY `ms`.`name`";
	
	
	$sql = "INSERT INTO mdl_report_course_credit_hours
	(user_id,user_type,firstname,lastname,userfullname,username, departmentid,groupid,global_groupid, departmenttitle, usermanager,inline_manager_name, credithours, courseid, coursename, coursetype_id, complition_time, class_id,  class_name, class_instructor, jobid, comid, job_title, company) ($query)";
	echo executeSql($sql);
	
	echo "<br/>done credithours report";

}
getCreditHoursReport();

	
?>