<?php

$config_path = dirname(dirname(__FILE__)) . '/config.php';
include_once($config_path);

if ($CFG->enable_auto_annual_enrollment_course == 1) {

    try {
        //Get all the courses for Auto annual enrollments
        $coursesListToBeEnrollments = getNewVersionCoursesForAutoEnrollments();
		
        //pr($coursesListToBeEnrollments);die;    
        if ($coursesListToBeEnrollments) {
            $output = enrolledParentCourseUsersIntoNewVersionCourse($coursesListToBeEnrollments);
        }
    } catch (Exception $ex) {
        $filename = time() . '_auto_enrollment_status_log_cron.csv';
        $file = $CFG->dirroot . "/user/download/user_log//" . $filename;
        $fp = fopen($file, 'w');
        // pr($errorLog);die;        
        fputcsv($fp, json_encode($ex));
        fclose($fp);
    }
    echo "Done";
}else{
    echo "Cron not executed";
}
?>