<?php
require_once("config.php");
require_once("lib.php");
GLOBAL $conn;

$userSlotSql = "SELECT c.id,c.fullname AS course_name,c.primary_instructor,mu_class_instructor.id as secondary_instructor,ms.id AS scheduler_id,mss.id AS ses_id,mu.id AS userid,ms.name AS classname,mss.sessionname AS sessionname,ms.isclasscompleted AS is_class_submitted,ms.submitted_by AS class_submitted_by,ms.submitted_on AS class_submitted_on,ms.teacher AS class_instructor,ms.isactive AS is_class_active,ms.startdate AS class_startdate,ms.enddate AS class_enddate,ms.isactive AS class_status,ms.enrolmenttype AS enrolmenttype,ms.createdby AS class_creator,ms.no_of_seats AS no_of_seats,
mse.is_approved AS is_approved,msa.attended AS attended,mse.is_completed AS is_completed,msa.grade AS grade,msa.score AS score,mss.starttime AS session_starttime,(mss.starttime + (mss.duration * 60)) AS session_endtime,mss.duration AS session_duration, CONCAT(mu.firstname,' ',mu.lastname) AS user_fullname,mu.username AS username, CONCAT(mu_class.firstname,' ',mu_class.lastname) AS class_submittedby_name,mu_class.username AS class_submittedby_username, CONCAT(mu_class_instructor.firstname,' ',mu_class_instructor.lastname) AS class_instructor_fullname, mu_class_instructor.username AS class_instructor_username,c.createdby as classroom_creator
FROM mdl_course c
LEFT JOIN mdl_scheduler ms ON c.id = ms.course
LEFT JOIN mdl_scheduler_enrollment mse ON (ms.id = mse.scheduler_id) AND mse.is_approved = 1
LEFT JOIN mdl_scheduler_slots mss ON (mss.schedulerid = mse.scheduler_id)
LEFT JOIN mdl_scheduler_appointment msa ON (msa.slotid = mss.id) AND (msa.studentid = mse.userid)
LEFT JOIN mdl_user mu ON (mu.id = mse.userid) AND (mu.deleted = '0')
LEFT JOIN mdl_department_members mdm ON (mdm.userid = mu.id)
LEFT JOIN mdl_department md ON (md.id = mdm.departmentid) AND (md.deleted = '0')  
LEFT JOIN mdl_user mu_class ON (mu_class.id = ms.submitted_by)
LEFT JOIN mdl_user mu_class_instructor ON (mu_class_instructor.id = ms.teacher) AND (mu_class_instructor.deleted = '0')
LEFT JOIN mdl_department_members ci_mdm ON (ci_mdm.userid = mu_class_instructor.id)
LEFT JOIN mdl_department ci_md ON (ci_md.id = ci_mdm.departmentid)
WHERE c.coursetype_id = 2";
$userSlotData	= getSqlData($userSlotSql);
$arrSchSlotData = array();
$truncateDetailsSql = "TRUNCATE `mdl_reports_classroom_details`";
$truncateData	= executeSql($truncateDetailsSql);
$insertStr = "INSERT INTO `mdl_reports_classroom_details` (`scheduler_id`, `session_id`, `userid`, `sessionname`, `attended`, `is_completed`, `grade`, `score`, `session_starttime`, `session_endtime`, `session_duration`, `user_fullname`, `username`) VALUES ";
$count = 1;
$insertChunk = 50;
$values = '';
if(mysqli_num_rows($userSlotData)>0){
	while ($userSlot = mysqli_fetch_object($userSlotData)){
		if(!array_key_exists($userSlot->id,$arrSchSlotData)){
			$arrSchSlotData[$userSlot->id] = array();
			$arrSchSlotData[$userSlot->id]['course_name'] = $userSlot->course_name;
			$arrSchSlotData[$userSlot->id]['classroom_creator'] = $userSlot->classroom_creator;
			$arrSchSlotData[$userSlot->id]['classes'] = array();
		}
		if($userSlot->scheduler_id != '' AND !array_key_exists($userSlot->scheduler_id,$arrSchSlotData[$userSlot->id]['classes'])){
			$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id] = array();
			$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['class_name'] = $userSlot->classname;
			$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['class_creator'] = $userSlot->class_creator;
			$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['secondary_instructor'] = $userSlot->secondary_instructor;
			$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['primary_instructor'] = $userSlot->primary_instructor;
			$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['is_class_submitted'] = $userSlot->is_class_submitted;
			$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['class_start'] = $userSlot->class_startdate;
			$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['class_end'] = $userSlot->class_enddate;

			$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['class_submitted_by'] = $userSlot->class_submitted_by;
			$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['class_submitted_on'] = $userSlot->class_submitted_on;
			$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['class_creator'] = $userSlot->class_creator;
			$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['no_of_seats'] = $userSlot->no_of_seats;
			$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['class_submittedby_name'] = $userSlot->class_submittedby_name;
			$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['class_submittedby_username'] = $userSlot->class_submittedby_username;
			$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['class_instructor_fullname'] = $userSlot->class_instructor_fullname;
			$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['class_instructor_username'] = $userSlot->class_instructor_username;

			$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['enr'] = 0;
			$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['ns'] = 0;
			$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['inc'] = 0;
			$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['com'] = 0;
		}
		if($userSlot->ses_id){
			$sessionData = array();
			if(!$userSlot->score){
				$userSlot->score = 0;
			}
			$sessionData['score'] = $userSlot->score;
			if(!$userSlot->attended){
				$userSlot->attended = 0;
			}
			$sessionData['grade'] = $userSlot->grade;
			$sessionData['attended'] = $userSlot->attended;
			if($userSlot->userid){
				if(!$userSlot->is_completed){
					$userSlot->is_completed = 0;
				}
				$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['users'][$userSlot->userid]['is_completed'] = $userSlot->is_completed;
				$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['users'][$userSlot->userid]['user_fullname'] = $userSlot->user_fullname;
				$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['users'][$userSlot->userid]['username'] = $userSlot->username;
				$arrSchSlotData[$userSlot->id]['classes'][$userSlot->scheduler_id]['users'][$userSlot->userid]['session'][$userSlot->ses_id] = $sessionData;
				$values .= "('".$userSlot->scheduler_id."', '".$userSlot->ses_id."', '".$userSlot->userid."', '".mysqli_real_escape_string($conn,$userSlot->sessionname)."', ".$userSlot->attended.", ".$userSlot->is_completed.", '".$userSlot->grade."', '".$userSlot->score."', '".$userSlot->session_starttime."', '".$userSlot->session_endtime."', '".$userSlot->session_duration."', '".mysqli_real_escape_string($conn,$userSlot->user_fullname)."', '".mysqli_real_escape_string($conn,$userSlot->username)."'),";
				if($count%$insertChunk == 0){
					$execSql = RTRIM($insertStr.$values,',');
					executeSql($execSql);
					$values = '';
				}
				$count++;
			}
		}
	}
	if($values != ''){
		$execSql = RTRIM($insertStr.$values,',');
		executeSql($execSql) ;
	}
}
$insertStr = '';
$execSql = '';
$values = '';
$count = 1;
$insertChunk = 20;
$truncateSql = "TRUNCATE `mdl_reports_classroom_report`";
$truncateData	= executeSql($truncateSql);
$insertStr = "INSERT INTO `mdl_reports_classroom_report` (`classroom_id`,`classroom_name`,`class_id`,`class_name`,`enrolled`,`no_show`,`incomplete`,`completed` ,`class_submitted`,`class_start`,`class_end`,primary_instructor,secondary_instructor,classroom_creator,class_creator,`class_submitted_by`,`class_submitted_on`,`no_of_seats`,`class_submittedby_name`,`class_submittedby_username`,`class_instructor_fullname`,`class_instructor_username`) VALUES ";
foreach($arrSchSlotData as $key=>$arrSchSlot){
	$classInfo = $arrSchSlot['classes'];
	if(isset($arrSchSlot['classes']) && !empty( $arrSchSlot['classes'])){
		foreach($classInfo as $classId=>$class){
			$completedCount = 0;
			$noShowCount = 0;
			$incompleteCount = 0;
			$enrolled = 0;
			if(isset($class['users']) && !empty($class['users'])){
				$enrolledUsers  = $class['users'];
				$enrolled = count($enrolledUsers);
				if($class['is_class_submitted'] == 1){
					foreach($enrolledUsers as $user){
						if($user['is_completed'] == 1){
							$completedCount++;
						}else{
							$isIncompleteflag = 0;
							foreach($user['session'] as $session){
								if($session['attended'] == 1){
									$isIncompleteflag = 1;
								}
							}
							if($isIncompleteflag == 1){
								$incompleteCount++;
							}else{
								$noShowCount++;
							}
						}
					}
				}
			}
			$values .= "( '".$key."', '".mysqli_real_escape_string($conn,$arrSchSlot['course_name'])."', '".$classId."', '".mysqli_real_escape_string($conn,$class['class_name'])."', '".$enrolled."', '".$noShowCount."', '".$incompleteCount."', '".$completedCount."', '".$class['is_class_submitted']."', '".$class['class_start']."', '".$class['class_end']."', '".$class['primary_instructor']."', '".$class['secondary_instructor']."', '".$arrSchSlot['classroom_creator']."', '".$class['class_creator']."', '".$class['class_submitted_by']."', '".$class['class_submitted_on']."', '".$class['no_of_seats']."', '".$class['class_submittedby_name']."', '".$class['class_submittedby_username']."', '".$class['class_instructor_fullname']."', '".$class['class_instructor_username']."'),";
			if($count%$insertChunk == 0){
				$execSql = RTRIM($insertStr.$values,',');
				executeSql($execSql);
				$values = '';
			}
			$count++;
		}
	}else{
		$values .= "( '".$key."', '".mysqli_real_escape_string($conn,$arrSchSlot['course_name'])."', 0, '', 0, 0, 0, 0, 0, '', '', 0, 0, '".$arrSchSlot['classroom_creator']."', 0, 0, '', 0, '', '', '', ''),";
		if($count%$insertChunk == 0){
			$execSql = RTRIM($insertStr.$values,',');
			executeSql($execSql);
			$values = '';
		}
		$count++;
	}
}
if($values != ''){
	$execSql = RTRIM($insertStr.$values,',');
	executeSql($execSql) ;
}

echo "<br/>done classroom course report";
?>