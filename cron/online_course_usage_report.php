<?php
set_time_limit(0);
require_once("config.php");
require_once("lib.php");
GLOBAL $conn;
$courseInformationSql = 'SELECT c.id,departmentid,gc.groupid as global_groupid,gcd.groupid as groupid,mpc.programid FROM mdl_course c LEFT JOIN ( SELECT dc.courseid, GROUP_CONCAT(dc.departmentid) AS departmentid FROM mdl_department_course dc LEFT JOIN mdl_department AS d ON d.id = dc.departmentid WHERE d.deleted = 0 GROUP BY courseid) AS dc ON dc.courseid = c.id LEFT JOIN ( SELECT mgc.courseid, GROUP_CONCAT(mgc.groupid) AS groupid FROM mdl_groups_course mgc LEFT JOIN mdl_group_department as gd ON gd.team_id = mgc.groupid LEFT JOIN mdl_groups as g ON mgc.groupid = g.id WHERE gd.id IS NULL AND g.id IS NOT NULL GROUP BY mgc.courseid) AS gc ON gc.courseid = c.id LEFT JOIN ( SELECT mgc.courseid, GROUP_CONCAT(mgc.groupid) AS groupid FROM mdl_groups_course mgc LEFT JOIN mdl_group_department as gd ON gd.team_id = mgc.groupid LEFT JOIN mdl_groups as g ON mgc.groupid = g.id WHERE gd.id IS NOT NULL AND g.id IS NOT NULL GROUP BY mgc.courseid) AS gcd ON gcd.courseid = c.id LEFT JOIN ( SELECT pc.courseid, GROUP_CONCAT(pc.programid) AS programid FROM mdl_program_course pc LEFT JOIN mdl_programs AS p ON pc.programid = p.id WHERE p.deleted = 0 GROUP BY courseid) AS mpc ON mpc.courseid = c.id WHERE c.deleted = 0 AND c.publish = 1 AND c.id != 1 AND c.coursetype_id = 1 GROUP BY c.id';
$courseArray = array();
$courseInformationData	= getSqlData($courseInformationSql);
if(mysqli_num_rows($courseInformationData)>0){
	while ($courseInformation = mysqli_fetch_object($courseInformationData)){
			$courseArray[$courseInformation->id]['programid'] = $courseInformation->programid;
			$courseArray[$courseInformation->id]['groupid'] = $courseInformation->groupid;
			$courseArray[$courseInformation->id]['global_groupid'] = $courseInformation->global_groupid;
			$courseArray[$courseInformation->id]['department'] = $courseInformation->departmentid;
	}
}
$courseSql ="SELECT c.id,c.fullname,uc.user_id, 
IF(uc.id IS NULL,'', IF(rll.id IS NULL,'', IF(rl.course_status IS NULL,'not started',rl.course_status))) AS course_status,
rll.user_id AS uid,rll.first_name as firstname,rll.department as departmentid
FROM mdl_course c
LEFT JOIN mdl_reports_users_courses uc ON uc.course_id = c.id
LEFT JOIN mdl_report_learner_performance as rll ON rll.user_id = uc.user_id
LEFT JOIN (
SELECT user_id,course_id,course_status
FROM mdl_report_learner_performance_details
GROUP BY user_id,course_id) rl ON rl.user_id = uc.user_id AND rl.course_id = uc.course_id
LEFT JOIN mdl_course_modules cm ON cm.course = c.id
LEFT JOIN (
SELECT *
FROM ((
SELECT s.id,s.course,s.name,18 AS module_id
FROM mdl_scorm s
WHERE s.is_active = 1) UNION(
SELECT r.id,r.course,r.name,17 AS module_id
FROM mdl_resource r
WHERE r.is_active = 1)) AS a) AS ad ON ad.module_id = cm.module AND cm.instance = ad.id AND cm.course = ad.course
WHERE c.id !=1 AND rll.id IS NOT NULL
AND c.deleted != 1 AND c.publish = 1 AND c.coursetype_id = 1 AND ad.name IS NOT NULL
GROUP BY c.id,uc.user_id
ORDER BY c.fullname ASC,uc.user_id ASC";
$truncateSql = "TRUNCATE `mdl_online_course_usage_details`";
$truncateData	= executeSql($truncateSql);
$userData = array();
$insertStr = "INSERT INTO `mdl_online_course_usage_details` (`course_id`, `fullname`, `department`, `groupid`, `global_groupid`,`programid`, `user_id`, `user_department`, `course_status`,`not_started`,completed,in_progress,blank_status) VALUES ";

$values = '';
$count = 1;
$insertChunk = 20;
$courseData	= getSqlData($courseSql);
if(mysqli_num_rows($courseData)>0){
	while ($course = mysqli_fetch_object($courseData)){
		if(array_key_exists($course->id,$courseArray)){
			$course->programid = $courseArray[$course->id]['programid'];
			$course->groupid = $courseArray[$course->id]['groupid'];
			$course->global_groupid = $courseArray[$course->id]['global_groupid'];
			$course->department = $courseArray[$course->id]['department'];
		}else{
			$course->programid = 0;
			$course->groupid = 0;
			$course->global_groupid = 0;
			$course->department = 0;
		}
		$not_started = 0;
		$completed = 0;
		$in_progress = 0;
		$blank_status = 0;
		switch($course->course_status){
			case 'not started':
				$not_started = 1;
			break;
			case 'in progress':
				$in_progress = 1;
			break;
			case 'completed':
				$completed = 1;
			break;
			default:
				$blank_status = 1;
		}
		$values .= "(".$course->id.",'".mysqli_real_escape_string($conn,$course->fullname)."', '".$course->department."', '".$course->groupid."', '".$course->global_groupid."', '".$course->programid."', '".$course->uid."', '".$course->departmentid."', '".$course->course_status."', '".$not_started."', '".$completed."', '".$in_progress."', '".$blank_status."'),";

		if($count%$insertChunk == 0){
			$execSql = RTRIM($insertStr.$values,',');
			executeSql($execSql);
			$values = '';
		}
		$count++;
	}
	if($values != ''){
		$execSql = RTRIM($insertStr.$values,',');
		executeSql($execSql);
	}
}
echo "<br/>done online course usage report";
?>