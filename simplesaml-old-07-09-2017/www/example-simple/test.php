<?php
//require_once (dirname(__FILE__) . '/../simplesaml/lib/_autoload.php');
require_once('../../lib/_autoload.php');
$as = new SimpleSAML_Auth_Simple('transishun-sp');

if (array_key_exists('logout', $_REQUEST)) {
	$as->logout(SimpleSAML_Utilities::selfURLNoQuery());

}

if (array_key_exists('login', $_REQUEST)) {

	$as->requireAuth();

}

$isAuth = $as->isAuthenticated();

$attributes = $as->getAttributes();

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Index Page</title>
</head>
<body>
<?php
if ($isAuth) {

	echo '<a href="?logout">Logout</a>';
?>  
    <h3>Welcome <strong>Authenticated User</strong>!</h3>
    <h4>Claim list:</h4>
<?php
echo '<pre>';
print_r($attributes);
echo '</pre>';

} else {
echo '<a href="?login">Login</a>';
}
?>
</body>
</html>