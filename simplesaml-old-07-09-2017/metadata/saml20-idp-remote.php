<?php
/**
 * SAML 2.0 remote IdP metadata for SimpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-idp-remote 
 */

/*
 * Guest IdP. allows users to sign up and register. Great for testing!
 */
$metadata['https://openidp.feide.no'] = array(
	'name' => array(
		'en' => 'Feide OpenIdP - guest users',
		'no' => 'Feide Gjestebrukere',
	),
	'description'          => 'Here you can login with your account on Feide RnD OpenID. If you do not already have an account on this identity provider, you can create a new one by following the create new account link and follow the instructions.',

	'SingleSignOnService'  => 'https://openidp.feide.no/simplesaml/saml2/idp/SSOService.php',
	'SingleLogoutService'  => 'https://openidp.feide.no/simplesaml/saml2/idp/SingleLogoutService.php',
	'certFingerprint'      => 'c9ed4dfb07caf13fc21e0fec1572047eb8a7a4cb'
);

$metadata['http://adfs.cignium.com/adfs/services/trust'] = array (
  'entityid' => 'http://adfs.cignium.com/adfs/services/trust',
  'contacts' => 
  array (
    0 => 
    array (
      'contactType' => 'support',
    ),
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://adfs.cignium.com/adfs/ls/',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://adfs.cignium.com/adfs/ls/',
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://adfs.cignium.com/adfs/ls/',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://adfs.cignium.com/adfs/ls/',
    ),
  ),
  'ArtifactResolutionService' => 
  array (
  ),
  'NameIDFormats' => 
  array (
    0 => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
    1 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
    2 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => false,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIC4jCCAcqgAwIBAgIQHZBAGaBvJYVHwftcVIOczzANBgkqhkiG9w0BAQsFADAtMSswKQYDVQQDEyJBREZTIEVuY3J5cHRpb24gLSBhZGZzLmNpZ25pdW0uY29tMB4XDTE2MDEyOTAzMjUzNloXDTE3MDEyODAzMjUzNlowLTErMCkGA1UEAxMiQURGUyBFbmNyeXB0aW9uIC0gYWRmcy5jaWduaXVtLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAO8SA7KlwzTZG7XtJChIWzrgydjZ40fmvkxIpeJAit9lVBSwa6SLuMyvkEIyXp0ktJI6XqzlRzsYFTrIN9EkCsiFNmt2puLHS91xSO5pcPld4UySdGbLuXi4fdng6afl4dLcrzFLJ+NgmVdeMQdXDKokc3YnPmqhT+4eDxvkCJDsAU9fS7aBttolQti8Ymwm2kL53+MUlau8z4tzJCF2pEDS5wEyrNZXFr+Ht5Gf9e5R/37S7vPVZekwvJZdsbUncKta6AZn7Tncf9uUr+qOyPpCqRDtqUTScr+0xMV5WL7saIqKQtomo/wMwwLatF7IoXGyYflaS13Gqt/A4nJmO1cCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAdosb5oMNxMYs9kQSYGfTLA/3VB3BxsDTvttMPCrcMYBhDXEJI4htAz5DkoAhyNE6M7h+FeDmr1T+jQF50i1Q+Il2WRbF9gK+wQLnk+ukrQYWG3rNAzeg5W/ewAY0+u2jaspBjFO+2vQ6j7oIqS5IslJkfoWdGAzZV4Tq68ADLuneVYRMoLPg0gE+/JYzlsHVNU9z1mUN1Jb8VUem/LOEqEflo9lF+Zhsg6vgtq2RdAD3q4j46EK7FyH2YHGxCiWy83MCBE/UEgw8Qv1NVSNSsolIbFq7Mwu0knGwCApGosiP63fh7F79wCmC71elUE5y77HvCAqaCRvZ4q8Paxn6vg==',
    ),
    1 => 
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIC3DCCAcSgAwIBAgIQHre1SWNPYoJMn/wqddCyQTANBgkqhkiG9w0BAQsFADAqMSgwJgYDVQQDEx9BREZTIFNpZ25pbmcgLSBhZGZzLmNpZ25pdW0uY29tMB4XDTE2MDEyOTAzMjUzOFoXDTE3MDEyODAzMjUzOFowKjEoMCYGA1UEAxMfQURGUyBTaWduaW5nIC0gYWRmcy5jaWduaXVtLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALVoSoKi2MyNQftMegZ+kOjExa5uA7r7xxTUZlFOJBQMGPs4opgIY0Hw5w4y+uEEZt5hOxeuYprZ5efBpeIrB4kpE7zQGMe23bHvKI7AdYR4w2ITIJ/xhJcfSwJwvW3JsBgdlejX6kUd+40ZzaLtdHKTEEZPBDO5qa2BAJMVYXdCE8LpKNkzwQkZjzA82U0SJXClhLCDTED6cAQ6+s2kXTXNfERueni2/CBazkU6pIAuV0gZRjLi6rAiCJnZeH5wWO5O16REZPjZB+bW7xiT+pVIpsGWK4zO0+2TIA29M0bTC984xd8C6QuGZg9HIguAwsKCX6rd3mkUBrppk4Hw8B8CAwEAATANBgkqhkiG9w0BAQsFAAOCAQEAZjdyom0btnTR5CZ7TfK26m8GRXmvkVZmY66zO4hnvtK+Lq+e3CAbz+1m8EC84n56cFq6Ff0aoS5sSXUSKladhZfMjr3qBMKUAH15OidR/+JMF6G0GwuEbsd7nanQOJMxEyjFamGMRgoDHtVpx4xi4k7lRek+in5wwaLZ+Admj2SjXuM6sP/pMexVaF026xiGmyJD8l+eiYRd2QrbQsz/p14OIIDMVjzAAKI7x/nwj2p6yx+G4ImXM7vJnp1mYujkWRiNTiRpEKC4sVbgBynWWJWeO4nFkRYYGwBBEN+Wed2q2pRDSfmUGH+/s52vldIvgU/CwGZ/Mm882Q5/p58uqA==',
    ),
  ),
);

$metadata['https://sts.windows.net/7427676f-ed2e-4126-b3b1-06279ba31283/'] = array (
  'entityid' => 'https://sts.windows.net/7427676f-ed2e-4126-b3b1-06279ba31283/',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://login.microsoftonline.com/7427676f-ed2e-4126-b3b1-06279ba31283/saml2',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://login.microsoftonline.com/7427676f-ed2e-4126-b3b1-06279ba31283/saml2',
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://login.microsoftonline.com/7427676f-ed2e-4126-b3b1-06279ba31283/saml2',
    ),
  ),
  'ArtifactResolutionService' => 
  array (
  ),
  'NameIDFormats' => 
  array (
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIDBTCCAe2gAwIBAgIQLplyYn9yyqlCiJ/PVTna6TANBgkqhkiG9w0BAQsFADAtMSswKQYDVQQDEyJhY2NvdW50cy5hY2Nlc3Njb250cm9sLndpbmRvd3MubmV0MB4XDTE3MDcyNzAwMDAwMFoXDTE5MDcyODAwMDAwMFowLTErMCkGA1UEAxMiYWNjb3VudHMuYWNjZXNzY29udHJvbC53aW5kb3dzLm5ldDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANVdwvIrsq1GP+xzMg1orDRi2uRhBJLwnM0LUKkx9zK9D+IcsNrrIoSbod0P3qKeY7mUd6Mvx01bFWw7y1ZGewWWePjOFIXDQEaKGIN0WXHWcpmrB21VuRgRr6RnrVvKVJlZ4gmFdkfVGY1pnRqTztjv9lK5DaTronHnhRLwGajrKHVnNvMqOVYbCJQFD6pCToeV56RlD9qePVVYSpZul5kLGkXIrrNbq1094Kd1v+07R6YifFw9Oi+YvIXQV8mzGazvLhnxJFB+TXPBwqjQODzogk6vzfz9DZGKxhKYDHh+KB01YWKuXofvDGqKZrC4mYAt7TZGa4tMCNwwS/F65a8CAwEAAaMhMB8wHQYDVR0OBBYEFJnvdZTJC0SLogTiajqLhDJabFtUMA0GCSqGSIb3DQEBCwUAA4IBAQAq9cwse+hSpZ/19bX54EftSJkpgAeV3RoVX/y+zCfH8hvOKYFKiNucx7k32KNGxnfaSkkMQ/xtJWxwQhFg93/n+YfjEg3bljW5tAQ3CgaB+h3h9EEDnUAHh7Tv3W4X4/hbqRa6NiTJWVUFRM7KDY3wwXaxttfyVAG6F9zmJZaqvsNFxrSnG+Pg+B1B+YtBYy0aeUoI7kSTx++WLtcKLlb+Ie5j26QOijsLCp/4vWi3OBuZptexgTmTCeQpCU7NLKiZZdN6076+lHJYYhTENjuoIP74KZnoJxBTHpp3iR0GpmR66ssCSL2LHBug3GmBaJ32EyC1AifOWLudp1M8/nn2',
    ),
    1 => 
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIDKDCCAhCgAwIBAgIQBHJvVNxP1oZO4HYKh+rypDANBgkqhkiG9w0BAQsFADAjMSEwHwYDVQQDExhsb2dpbi5taWNyb3NvZnRvbmxpbmUudXMwHhcNMTYxMTE2MDgwMDAwWhcNMTgxMTE2MDgwMDAwWjAjMSEwHwYDVQQDExhsb2dpbi5taWNyb3NvZnRvbmxpbmUudXMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQChn5BCs24Hh6L0BNitPrV5s+2/DBhaeytOmnghJKnqeJlhv3ZczShRM2Cp38LW8Y3wn7L3AJtolaSkF/joKN1l6GupzM+HOEdq7xZxFehxIHW7+25mG/WigBnhsBzLv1SR4uIbrQeS5M0kkLwJ9pOnVH3uzMGG6TRXPnK3ivlKl97AiUEKdlRjCQNLXvYf1ZqlC77c/ZCOHSX4kvIKR2uG+LNlSTRq2rn8AgMpFT4DSlEZz4RmFQvQupQzPpzozaz/gadBpJy/jgDmJlQMPXkHp7wClvbIBGiGRaY6eZFxNV96zwSR/GPNkTObdw2S8/SiAgvIhIcqWTPLY6aVTqJfAgMBAAGjWDBWMFQGA1UdAQRNMEuAEDUj0BrjP0RTbmoRPTRMY3WhJTAjMSEwHwYDVQQDExhsb2dpbi5taWNyb3NvZnRvbmxpbmUudXOCEARyb1TcT9aGTuB2Cofq8qQwDQYJKoZIhvcNAQELBQADggEBAGnLhDHVz2gLDiu9L34V3ro/6xZDiSWhGyHcGqky7UlzQH3pT5so8iF5P0WzYqVtogPsyC2LPJYSTt2vmQugD4xlu/wbvMFLcV0hmNoTKCF1QTVtEQiAiy0Aq+eoF7Al5fV1S3Sune0uQHimuUFHCmUuF190MLcHcdWnPAmzIc8fv7quRUUsExXmxSX2ktUYQXzqFyIOSnDCuWFm6tpfK5JXS8fW5bpqTlrysXXz/OW/8NFGq/alfjrya4ojrOYLpunGriEtNPwK7hxj1AlCYEWaRHRXaUIW1ByoSff/6Y6+ZhXPUe0cDlNRt/qIz5aflwO7+W8baTS4O8m/icu7ItE=',
    ),
  ),
);


