<?php
error_reporting(E_ALL);
require_once('../../lib/_autoload.php');

SimpleSAML_Logger::warning('The file example-simple/verysimple.php is deprecated and will be removed in future versions.');
$as = new SimpleSAML_Auth_Simple('transishun-sp');
echo $as->getLoginURL("https://cigniumuat.gnosisconnect.com/simplesaml/www/example-simple/user_authenticate.php?login"); die;
if (array_key_exists('login', $_REQUEST)) {
	$username = $_REQUEST['username'];
	$password = $_REQUEST['password'];
	
	$attributes = $as->login($username,$password);

}


/*
 * Retrieve the users attributes. We will list them if the user
 * is authenticated.
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Simple test</title>
</head>
<body>

<h1>Simple auth test</h1>

<?php
/* Show a logout message if authenticated or a login message if not. */
if ($isAuth) {
	echo '<p>You are currently authenticated. <a href="?logout">Log out</a>.</p>';
} else {
	echo '<p>You are not authenticated. <a href="?login">Log in</a>.</p>';
}
?>

<p>The following form makes it possible to test requiering authentication
in a POST handler. Try to submit the message while unauthenticated.</p>
<form method="post" action="?login">
<input type="text" autocomplete="off" placeholder="someone@example.com" spellcheck="false" class="text fullWidth" tabindex="1" value="" name="UserName" id="userNameInput">
<input type="password" autocomplete="off" placeholder="Password" class="text fullWidth" tabindex="2" name="Password" id="passwordInput">
<input type="submit" value="Post message" />
</form>

<?php

/* Print out the message if it is present. */
if ($message !== NULL) {
	echo '<h2>Message</h2>';
	echo '<p>' . htmlspecialchars($message) . '</p>';
}

/* Print out the attributes if the user is authenticated. */
if ($isAuth) {
	echo '<h2>Attributes</h2>';
	echo '<dl>';

	foreach ($attributes as $name => $values) {
		echo '<dt>' . htmlspecialchars($name) . '</dt>';
		echo '<dd><ul>';
		foreach ($values as $value) {
			echo '<li>' . htmlspecialchars($value) . '</li>';
		}
		echo '</ul></dd>';
	}

	echo '</dl>';
}

?>

</body>
</html>