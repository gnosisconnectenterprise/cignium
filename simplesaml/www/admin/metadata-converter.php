<?php
//require('../_include.php');
require('/var/www/cignium/simplesaml/www/_include.php');

//error_reporting(E_ALL); ini_set('display_errors', 1);
//echo '<pre>';
//print_r($_SERVER); die;
// make sure that the user has admin access rights
//SimpleSAML\Utils\Auth::requireAdmin();
$config = SimpleSAML_Configuration::getInstance();
$fileurl = 'https://login.microsoftonline.com/7427676f-ed2e-4126-b3b1-06279ba31283/federationmetadata/2007-06/federationmetadata.xml';
//if (!empty($_FILES['xmlfile']['tmp_name'])) {
if ($fileurl) {
    $xmldata = file_get_contents($fileurl);
} elseif (array_key_exists('xmldata', $_POST)) {
    $xmldata = $_POST['xmldata'];
}
if (!empty($xmldata)) {
    \SimpleSAML\Utils\XML::checkSAMLMessage($xmldata, 'saml-meta');
    $entities = SimpleSAML_Metadata_SAMLParser::parseDescriptorsString($xmldata);

    // get all metadata for the entities
    foreach ($entities as &$entity) {
        $entity = array(
            'shib13-sp-remote'  => $entity->getMetadata1xSP(),
            'shib13-idp-remote' => $entity->getMetadata1xIdP(),
            'saml20-sp-remote'  => $entity->getMetadata20SP(),
            'saml20-idp-remote' => $entity->getMetadata20IdP(),
        );
    }

    // transpose from $entities[entityid][type] to $output[type][entityid]
    $output = SimpleSAML\Utils\Arrays::transpose($entities);

    // merge all metadata of each type to a single string which should be added to the corresponding file
    foreach ($output as $type => &$entities) {
        $text = '';
        foreach ($entities as $entityId => $entityMetadata) {

            if ($entityMetadata === null) {
                continue;
            }

            // remove the entityDescriptor element because it is unused, and only makes the output harder to read
            unset($entityMetadata['entityDescriptor']);

            $text .= '$metadata['.var_export($entityId, true).'] = '.
                var_export($entityMetadata, true).";\n";
        }
        $entities = $text;
       
    }
} else {
    $xmldata = '';
    $output = array();
}
$newrecord  = '<?php ';
/*$newrecord .= '<?php' . PHP_EOL . PHP_EOL;
$newrecord .= 'require_once dirname(dirname(__FILE__))."/www/admin/metadata-converter.php";' . PHP_EOL . PHP_EOL; 

$newrecord .= '$metadata["https://openidp.feide.no"] = array(
	"name" => array(
		"en" => "Feide OpenIdP - guest users",
		"no" => "Feide Gjestebrukere",
	),
	"description"          => "Here you can login with your account on Feide RnD OpenID. If you do not already have an account on this identity provider, you can create a new one by following the create new account link and follow the instructions.",

	"SingleSignOnService"  => "https://openidp.feide.no/simplesaml/saml2/idp/SSOService.php",
	"SingleLogoutService"  => "https://openidp.feide.no/simplesaml/saml2/idp/SingleLogoutService.php",
	"certFingerprint"      => "c9ed4dfb07caf13fc21e0fec1572047eb8a7a4cb"
);'. PHP_EOL . PHP_EOL;*/
$newrecord .= $output['saml20-idp-remote']. PHP_EOL . PHP_EOL;
$newrecord .= " ?>";

$existingConfig = file_get_contents("/var/www/cignium/simplesaml/www/admin/federationconfig.php");

if(md5($existingConfig) != md5($newrecord)){
	//echo $newrecord;die;
    file_put_contents("/var/www/cignium/simplesaml/www/admin/federationconfig.php", $newrecord);
}else{
	//echo "Key matched";die;
}