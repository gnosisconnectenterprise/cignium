<?php
$metadata["https://openidp.feide.no"] = array(
	"name" => array(
		"en" => "Feide OpenIdP - guest users",
		"no" => "Feide Gjestebrukere",
	),
	"description"          => "Here you can login with your account on Feide RnD OpenID. If you do not already have an account on this identity provider, you can create a new one by following the create new account link and follow the instructions.",

	"SingleSignOnService"  => "https://openidp.feide.no/simplesaml/saml2/idp/SSOService.php",
	"SingleLogoutService"  => "https://openidp.feide.no/simplesaml/saml2/idp/SingleLogoutService.php",
	"certFingerprint"      => "c9ed4dfb07caf13fc21e0fec1572047eb8a7a4cb"
);
//ob_start();
require_once dirname(dirname(__FILE__))."/www/admin/metadata-converter.php";

require_once dirname(dirname(__FILE__))."/www/admin/federationconfig.php";
//$federationConfigs = ob_get_contents();
//ob_end_clean();




//echo $federationConfigs;
/*
$metadata['https://sts.windows.net/7427676f-ed2e-4126-b3b1-06279ba31283/'] = array (
  'entityid' => 'https://sts.windows.net/7427676f-ed2e-4126-b3b1-06279ba31283/',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://login.microsoftonline.com/7427676f-ed2e-4126-b3b1-06279ba31283/saml2',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://login.microsoftonline.com/7427676f-ed2e-4126-b3b1-06279ba31283/saml2',
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://login.microsoftonline.com/7427676f-ed2e-4126-b3b1-06279ba31283/saml2',
    ),
  ),
  'ArtifactResolutionService' => 
  array (
  ),
  'NameIDFormats' => 
  array (
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIDBTCCAe2gAwIBAgIQYbgOJ8Uror1IlEvjsPi7jzANBgkqhkiG9w0BAQsFADAtMSswKQYDVQQDEyJhY2NvdW50cy5hY2Nlc3Njb250cm9sLndpbmRvd3MubmV0MB4XDTE4MDUxMTAwMDAwMFoXDTIwMDUxMTAwMDAwMFowLTErMCkGA1UEAxMiYWNjb3VudHMuYWNjZXNzY29udHJvbC53aW5kb3dzLm5ldDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALz96rUhsQfjAe0JZni7J5oLq4t/3m27HwXSjZOVM+mDfNCQzTDvKvpmx6/VBmB+SAd/cMc/GfntWl/DQeb08Hgkd7UGzbV1rUOsxH15mpwFABjQA5iFTd5i8BqMVz6stNVTP/crS06cKMAom1VqAoouZvxi7VY8qKFwWTBo8IS0ps32dIqY5Xp+6vNy8WQGGEnVf5MxxrSShxw6zTMMyXzoJPrrxe9y0luJF+c5rRy4ET2PhpL6rtoNhJ+EoXaV0zdIjVeq2ecSMobbP9PBtqBdkQSRIo4RY+zQZMTHF9zUu2SpCKkb8kq4hIZkpGbEkahoPIJ/GlQtg6lbsEYopi8CAwEAAaMhMB8wHQYDVR0OBBYEFIwrggJsAwub9JGBkbpcqnwD052FMA0GCSqGSIb3DQEBCwUAA4IBAQAN9cz2xcZe76AxjQAOgaGGMrpowwmDht5ssS4SrwoL1gDvEP/pn4tTdYpPTP18EC7YMg925nbLmqNM0VJvO7AJr1I6G/HbmrCyyhvmZYZnAJVwqFwsPK2lJ1K0sjriL/g1UI0BofFsWBxBMqaDOp7+PTz27Ssn7UOo5ghKCMWaijNl+nsjfDtIJhKjISW8KduL5DO7Q+9R5ec/AyjheOCTmEij8V6nVBX642z9ujU9xOUaZZux9usuEHDhf7kqnOw/9/WyKluHoLhxFkTCV2Y12HabDtKo5iOP+ukjzNzZkRoo74Fi0tFB+nB24fdrd2TrxaGau/KXRu5QbXataOjz',
    ),
    1 => 
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIDBTCCAe2gAwIBAgIQE7nbxEiAlqhFdKnsKV+nuTANBgkqhkiG9w0BAQsFADAtMSswKQYDVQQDEyJhY2NvdW50cy5hY2Nlc3Njb250cm9sLndpbmRvd3MubmV0MB4XDTE4MDYyMTAwMDAwMFoXDTIwMDYyMTAwMDAwMFowLTErMCkGA1UEAxMiYWNjb3VudHMuYWNjZXNzY29udHJvbC53aW5kb3dzLm5ldDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAL1cxvfI3wPu1HFGZL6wnSHfcqX6DlvezLF1wbqUs6/DS5GhxtvHk6+9nKyNuCjznkEKql6bTIgnUbvTPr0UUKa3hezq/D9nkGu4qX8LVONVWkb53mjnN45lnVfOLh6VQ7J8/9Ut/ybbnhcn2a12vWiR0c6899TgtRp+i0bkT9dYl+3/wfP8+bBqmolwno7yojv/DxMz86mzhS7lW6mS9zzYtdsy6IHcF2P9XIac2TaP0efUpLrQY81wuTE3gUh2s6j7tqNH7aKK2PNAuxXtyGtZ9r+bg0gMrDXXVKJylQO9m4Z5J0vuz8xgCElLg3uJPOI8q/j0YrLe5rHy67ACg0MCAwEAAaMhMB8wHQYDVR0OBBYEFDnSNW3pMmrshl3iBAS4OSLCu/7GMA0GCSqGSIb3DQEBCwUAA4IBAQAFs3C5sfXSfoi7ea62flYEukqyVMhrDrpxRlvIuXqL11g8KEXlk8pS8gEnRtU6NBeHhMrhYSuiqj7/2jUT1BR3zJ2bChEyEpIgOFaiTUxq6tXdpWi/M7ibf8O/1sUtjgYktwJlSL6FEVAMFH82TxCoTWp2g5i2lmZQ7KxiKhG+Vl9nw1bPX57hkWWhR7Hpes0MbpGNZI2IEpZSjNG1IWPPOBcaOh4ed2WBQcLcaTuAaELlaxanQaC0B3029To80MnzpZuadaul3+jN7JQg0MpHdJJ8GMHAWe/IjXc0evJNhVUcKON41hzTu0R+Sze7xq1zGljQihJgcNpO9oReBUsX',
    ),
    2 => 
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIDKDCCAhCgAwIBAgIQBHJvVNxP1oZO4HYKh+rypDANBgkqhkiG9w0BAQsFADAjMSEwHwYDVQQDExhsb2dpbi5taWNyb3NvZnRvbmxpbmUudXMwHhcNMTYxMTE2MDgwMDAwWhcNMTgxMTE2MDgwMDAwWjAjMSEwHwYDVQQDExhsb2dpbi5taWNyb3NvZnRvbmxpbmUudXMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQChn5BCs24Hh6L0BNitPrV5s+2/DBhaeytOmnghJKnqeJlhv3ZczShRM2Cp38LW8Y3wn7L3AJtolaSkF/joKN1l6GupzM+HOEdq7xZxFehxIHW7+25mG/WigBnhsBzLv1SR4uIbrQeS5M0kkLwJ9pOnVH3uzMGG6TRXPnK3ivlKl97AiUEKdlRjCQNLXvYf1ZqlC77c/ZCOHSX4kvIKR2uG+LNlSTRq2rn8AgMpFT4DSlEZz4RmFQvQupQzPpzozaz/gadBpJy/jgDmJlQMPXkHp7wClvbIBGiGRaY6eZFxNV96zwSR/GPNkTObdw2S8/SiAgvIhIcqWTPLY6aVTqJfAgMBAAGjWDBWMFQGA1UdAQRNMEuAEDUj0BrjP0RTbmoRPTRMY3WhJTAjMSEwHwYDVQQDExhsb2dpbi5taWNyb3NvZnRvbmxpbmUudXOCEARyb1TcT9aGTuB2Cofq8qQwDQYJKoZIhvcNAQELBQADggEBAGnLhDHVz2gLDiu9L34V3ro/6xZDiSWhGyHcGqky7UlzQH3pT5so8iF5P0WzYqVtogPsyC2LPJYSTt2vmQugD4xlu/wbvMFLcV0hmNoTKCF1QTVtEQiAiy0Aq+eoF7Al5fV1S3Sune0uQHimuUFHCmUuF190MLcHcdWnPAmzIc8fv7quRUUsExXmxSX2ktUYQXzqFyIOSnDCuWFm6tpfK5JXS8fW5bpqTlrysXXz/OW/8NFGq/alfjrya4ojrOYLpunGriEtNPwK7hxj1AlCYEWaRHRXaUIW1ByoSff/6Y6+ZhXPUe0cDlNRt/qIz5aflwO7+W8baTS4O8m/icu7ItE=',
    ),
  ),
);*/