<?php
    require_once('../config.php');
    require_once($CFG->dirroot .'/course/lib.php');
    require_once($CFG->libdir .'/filelib.php');	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $CFG->siteTitle;?></title>

<link rel="shortcut icon" type="image/x-icon" href="<?php echo $CFG->wwwroot;?>/home/images/favicon.ico">
<link rel="stylesheet" type="text/css" href="index.css" />
<link href="<?php echo $CFG->wwwroot;?>/home/jquery.bxslider.css" rel="stylesheet" />
<script src="<?php echo $CFG->wwwroot;?>/home/js/jquery.min.js"></script>
<script src="<?php echo $CFG->wwwroot;?>/home/js/jquery.bxslider.min.js"></script>
</head>
<body>
<div class="portraitMessage">This website is viewable in landscape mode only.</div>
<div class="wrapper">
  <div class="homePage">
  	<div id="page">
            <a href="<?php echo $CFG->wwwroot;?>/docs/Human Resources Contacts.pdf" target="_blank" style="position: absolute; right:0; top:10px; background: rgb(67, 130, 196); color:#fff; border-radius:7px; padding:5px 10px;display:none;">Human Resources Contacts</a>
    	<h1>Welcome to <span>the Learning Portal</span></h1>
    	<div role="dialog" class="login-box" <?php echo $displayForm;?> >
              <div class="clear"></div>
              <?php
				$displayForm = "";
				$errmsg = "";
				$loginlinkShow = '';
				if($SESSION->loginerrormsg){
					$displayForm = 'style="display: block;"';
					$errmsg = $SESSION->loginerrormsg;
					$loginlinkShow = 'login';
					$SESSION->loginerrormsg = '';
				}
				
			  ?>
              <div class="logo-right" <?php echo $displayForm;?> >
                <div id="loginform">
                   <?php 
					if($CFG->isLdap==1){
					$authldap_skipntlmsso = optional_param('authldap_skipntlmsso', 0, PARAM_INT);
					$loginActionUrl = $CFG->wwwroot.'/login/index.php?authldap_skipntlmsso='.$authldap_skipntlmsso;
					$loginInfoText = '*User your windows credential to login.';
					} 
					else{
					$loginActionUrl = $CFG->wwwroot.'/login/index.php';
					$loginInfoText = "";
					}
					
					?>
                  <form action="<?php echo $loginActionUrl ?>" method="POST" id = "login-form">
                  
                    <?php 
					$loginTypeChecked = isset($_SESSION['sess_logintype'])?$_SESSION['sess_logintype']:1;
					echo getExternalDepartmentHtml(1, $loginTypeChecked);
					$_SESSION['sess_logintype'] = '';
					unset($_SESSION['sess_logintype']);
					?>
                  
                    <span class = 'username-error'><?php echo $errmsg;?></span>
                    <label for="login-email" class="hide_text">Login-Email</label>
                    <input name="username" type="text" value="Username" onblur="if(this.value==''){this.value='Username'}" onfocus="if(this.value=='Username'){this.value=''}" autocomplete="off" id = "login-email"/>
                    <div class="clear"></div>
                    <span class = 'password-error'></span>
                    <label for="login-password" class="hide_text">Login-password</label>
                    <input name="password" type="password" value="Password" onblur="if(this.value==''){this.value='Password'}" onfocus="if(this.value=='Password'){this.value=''}" autocomplete="off" id = "login-password"/>
                    <div class="clear"></div>
                    <label for="log-in" class="hide_text">login</label>
                    <input name="Login" type="submit" value="Login" id = "log-in"/>
                    <span class="f-left">
                    <div class="rememberpass">
                      <input type="checkbox" value="1" id="rememberusername" name="rememberusername">
                      <label for="rememberusername">Keep me signed in</label>
                    </div>
                   
                    </span>
                    <div class="clear"></div>
                    <?php if($CFG->isLdap==0){
                    echo '<span class="f-left forgot-password"> <a href="javascript:void(0)">Forgot Password?</a> </span>';
                    }
                    ?>
                     <div class="clear"></div>
                      <div class="login_info" style="margin-left: 10px;"> <?php echo $loginInfoText; ?></div>
                      
                     <?php echo getExternalDepartmentHtml(2, $loginTypeChecked);?>
                       
                  </form>
                  
                </div>
                <div class="clear"></div>
                <div id="forgot-password-form" style="display:none">
                  <form action="<?php echo $CFG->wwwroot;?>/login/forgot_password.php" method="POST" id = "forgot-passwordform">
                    <span class = 'email-error'></span>
                     <label for="forgot-passowrd-email" class="hide_text">forgot-passowrd-email</label>
                    <input name="email" type="text" value="Email / Username" onblur="if(this.value==''){this.value='Email / Username'}" onfocus="if(this.value=='Email / Username'){this.value=''}" autocomplete="off"  id = "forgot-passowrd-email"/>
                    <div class="clear"></div>
                    <label for="forgot-password-click" class="hide_text">forgot-password-click</label>
                    <input name="search" type="submit" value="Submit" id = "forgot-password-click"/>
                    <span class="f-left forgot-login-password"> <a href="javascript:void(0)">Click to Login</a> </span>
                  </form>
                </div>
                <a href="<?php echo $CFG->wwwroot;?>/docs/Human Resources Contacts.pdf" target="_blank" style="position: absolute;right:11px;bottom: -90px;background: rgb(239, 158, 62);color:#fff;border-radius:7px;padding:5px 10px;white-space: nowrap;">Human Resources Contacts</a>
              </div>
            </div>
    </div>
  </div>
  <footer>
    <div id="page">
     <!-- <div class="f-left">Copyright &copy; . All rights reserved.</div>-->
	  <div class="arrow"></div>
      <div class="f-right botm_foot">Powered By GnosisConnect LMS<span></span></div>
	</div>
      <!--<div class="arrow"></div>-->
      <!--<div class="f-right">
        <?php 
				$CMSPages = getFooterCMSPages(); 
				if(count($CMSPages) > 0){
				                   
                        foreach($CMSPages as $pages){
							if($pages->is_external == 1){
								$target = 'target = "_blank"';
								$targetLink = $pages->external_url;
							}else{
								$target = '';
								$targetLink = $CFG->wwwroot.'/cms/cms_content.php?pageid='.$pages->id;
							}
						?>
                            <a href="<?php echo $targetLink;?>" title="<?php echo $pages->page_name;?>" <?php echo $target;?>><?php echo $pages->page_name;?></a>
        <?php }   
                        
                 } ?>
                 </div>-->
                 
               <!--  
                 <?php 
				$CMSPages = getFooterCMSPages(); 
				if(count($CMSPages) > 0){
					$addCss = '';
					if($CFG->showPoweredBy == 0){
						$addCss = " pull-right";
					}
				?>
                    <div class="footer-menu <?php echo $addCss;?>">
                        <ul>
                        <?php 
						$OthersLi = '';
						$OthersLiStart = '';
						$OthersLiEnd = '';
						$cmsCount = 1;
						$totalCmsPages = count($CMSPages);
						if($totalCmsPages > 3){
							$OthersLiStart	= '<li class="linkPopup"><a href="javascript:void(0);"><span class="hide_text">Additional Items</span></a><ul>';
							$OthersLiEnd 	= '</ul></li>';
						}
						foreach($CMSPages as $pages){
								if($pages->is_external == 1){
									$target = 'target = "_blank"';
									$targetLink = $pages->external_url;
								}else{
									$target = '';
									$targetLink = $CFG->wwwroot.'/cms/cms_content.php?pageid='.$pages->id;
								}
								if($cmsCount>3){	
									$OthersLi .= '<li><a href="'.$targetLink.'" title="'.$pages->page_name.'" '.$target.'>'.$pages->page_name.'</a></li>';
								}else{
							?>
                            	<li><a href="<?php echo $targetLink;?>" title="<?php echo $pages->page_name;?>" <?php echo $target;?>><?php echo $pages->page_name;?></a></li>
                        	<?php
							}
							$cmsCount++;
						 } 
						 echo $OthersLiStart.$OthersLi.$OthersLiEnd;
						 ?>    
                        </ul>
                    </div>
                    
                <?php } ?>   
                 
                 -->
                 
      
  </footer>
  <div class="drop" title="Top"></div>
</div>


<script type="text/javascript">

$(document).ready(function(){
	if($('.username-error').length > 0 && $('.username-error').html() != '' && $('.password-error').length > 0 && $('.password-error').html() == ''){
	  $('.password-error').hide();
	}
	$('.bx-default-pager').remove();
	pageHeight();
});
function pageHeight(){
	var footerHeight = $('footer').height();
	var winHeight = $(window).height();
	var homePageHeight = winHeight-footerHeight;
	$('.homePage').css('height',homePageHeight);
}
$(window).resize(function() {
	pageHeight();
})

$(window).bind("load", function() {
	var hgt = $('.bx-viewport').css('height');
	$('.pos-fix').css('height',parseInt(hgt)+'px');
	$('.slider-five-bts-bx').fadeIn();
	

	if($('.username-error').length > 0 && $('.username-error').html() != ''){
	  $('.login-open').addClass('show');
	}

});

$('.callus-now a').click(function(){
	$('.callus-now-bx').toggle('slow');	
});

$('.drop').click(function(){
  $('html,body').animate({scrollTop:"0px"},'slow')
});
$(window).scroll(function(){
if($(this).scrollTop() >0){
	$('.drop').show();
}
else{
	$('.drop').hide();
}
});

$('a.login-open').click(function(){
	$(this).toggleClass('show');
	$('.login-box').toggle('slow');
	$(this).parent('.login-link').toggleClass('login');
	$(this).parent('.login-link').find('.right-down-arrow').toggleClass('right-close-arrow');
	$('.password-error').html();
	$('.password-error').hide();
	$('.username-error').html('');
	//$('.username-error').hide();
	$('.email-error').hide();
	var forgotEmail = $.trim($("#forgot-passowrd-email").val());
	var email = $.trim($("#login-email").val());
	var password = $.trim($("#login-password").val());
	if(email != '' && email.toLowerCase () != 'username' ){
		$("#login-email").val('Username');
	}
	if(forgotEmail != '' && forgotEmail != 'Email / Username' ){
		$("#forgot-passowrd-email").val('Email / Username');
	}
	if(password != '' && password.toLowerCase () != 'password' ){
		$("#login-password").val('Password');
	}
});

$('.forgot-password a').click(function(){
	$(this).parents('#loginform').hide('slow');
	$(this).parents('.login-box').find('#forgot-password-form').show('slow');
	$('.email-error').hide();
	var forgotEmail = $.trim($("#forgot-passowrd-email").val());
	$("#forgot-passowrd-email").val('Email / Username');
});

$('.forgot-login-password a').click(function(){
	$(this).parents('#forgot-password-form').hide('slow');
	$(this).parents('.login-box').find('#loginform').show('slow');
	var forgotEmail = $.trim($("#forgot-passowrd-email").val());
	var email = $.trim($("#login-email").val());
	var password = $.trim($("#login-password").val());
	$("#login-email").val('Username');
	$("#login-password").val('Password');
});

$(document).on("click","#forgot-password-click",function(event){
	event.preventDefault();
	var email = $.trim($("#forgot-passowrd-email").val());
	var err = 0;
	var msg = '';
	if(email == '' || email == 'Email / Username' ){
		msg = "<?php echo get_string('provide_email_username'); ?>";
		$('.email-error').html(msg);
		$('.email-error').show();
		return false;
	}
	$.ajax({
			url:'<?php echo $CFG->wwwroot;?>/login/forgot_password.php?email='+email,
			type:'POST',
			success:function(data){
				if(data == 0 || data == '0'){
					msg = "<?php echo get_string('provide_email_username'); ?>";
					$('.email-error').html(msg);
					$('.email-error').show();
					return false;
				}else if(data == 1 || data == '1'){
					msg = "<?php echo get_string('user_doesnot_exists'); ?>";
					$('.email-error').html(msg);
					$('.email-error').show();
					return false;
				}else if(data == 'SUCCESS'){
					msg = "<?php echo get_string('mail_have_been_sent'); ?>";
					$('.email-error').addClass('success');
					$('.email-error').html(msg);
					$('.email-error').show();
					return false;
				}else if(data == 'FAILED'){
					msg = "<?php echo get_string('error_sending_email'); ?>";
					$('.email-error').addClass('success');
					$('.email-error').html(msg);
					$('.email-error').show();
					return false;
				}
			}
	});
	return false;
});
$(document).on("click","#log-in",function(event){
	event.preventDefault();
	var email = $.trim($("#login-email").val());
	var password = $.trim($("#login-password").val());
	var err = 0;
	var msg = '';
	if(email == '' || email.toLowerCase () == 'username' ){
		msg = "<?php echo get_string('provide_username'); ?>";
		$('.username-error').html(msg);
		//$('.username-error').show();
		err = 1;
	}else{
		$('.username-error').html('');
		//$('.username-error').hide();
		err = 0;
	}

	if(password == '' || password.toLowerCase ()== 'password' ){
		msg = "<?php echo get_string('provide_password'); ?>";
		$('.password-error').html(msg);
		$('.password-error').show();
		err = 1;
	}else{
		$('.password-error').html();
		$('.password-error').hide();
		err = 0;
	}
	if(err == 1){
		return false;
	}else{
		$("#login-form").submit();
	}
});

$(".linkPopup > a").on('click',function(event){
	$(".contactIcon").next('ul').hide();
	$(this).next('ul').toggle();
	return false;
});
$(".contactIcon").on('click',function(event){
	$(".linkPopup > a").next('ul').hide();
	$(this).next('ul').toggle();
	return false;
});
$(document).on('click',function(event){
	$(".linkPopup > a,.contactIcon").next('ul').hide();
})


</script>
</body>
</html>
