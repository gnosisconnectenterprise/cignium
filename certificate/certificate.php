<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A4_embedded certificate type
 *
 * @package    mod
 * @subpackage certificate
 * @copyright  Mark Nelson <markn@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once("../config.php");
require_once("$CFG->libdir/pdflib.php");
require_once("$CFG->dirroot/certificate/lib.php");
$id = required_param('id', PARAM_INT);
$action = optional_param('action', '', PARAM_ALPHA);
$courseProgramList = get_learning_list($sort,2,$page,$perpage,'all',$where);
 $PAGE->set_title('Certificate');
$programme_arr = array();
$course_arr = array();
if($courseProgramList){
	foreach($courseProgramList as $key=>$programOrCourseArray){
		if($programOrCourseArray->program_id != 0){
		$programme_arr[] = $programOrCourseArray->program_id;
		}
		else{
		$course_arr[] = $programOrCourseArray->id;
		}
	}
}

if($action=='course'){
	$course = $DB->get_record('course',array('id'=>$id));
	if(!in_array($id,$course_arr)){
	redirect($CFG->wwwroot.'/my/learningpage.php?type=2');
	}
	$course->completiondate = date('M Y',time());
}
else if($action=='programme'){
	$course = $DB->get_record('programs',array('id'=>$id));
	if(!in_array($id,$programme_arr)){
		redirect($CFG->wwwroot.'/my/learningpage.php?type=2');
		}
	$course->fullname = $course->name;
	$course->completiondate = date('M Y',time());
}
else if($action=='class'){
	$course = $DB->get_record('scheduler',array('id'=>$id));
	$course_completion =  $DB->get_record('scheduler_enrollment',array('scheduler_id'=>$id,'userid'=>$USER->id,'is_completed'=>1));
	
	if(!$course_completion){
	
	redirect($CFG->wwwroot.'/my/learningpage.php?type=2');
		}
	else{
	$course->completiondate = date('M Y',$course_completion->timemodified);
	}
	
	$course->fullname = $course->name;
}
else{
	if($USER->archetype == $CFG->userTypeStudent){ 
	 redirect($CFG->wwwroot.'/my/learningpage.php?type=2');
	}else{
	 redirect($CFG->wwwroot.'/my/dashboard.php');
	}
}



$certificate = $DB->get_record('certificate',array('id'=>1));

$pdf = new PDF($certificate->orientation, 'mm', 'A4', true, 'UTF-8', false);
$pdf->SetTitle($certificate->name);
$pdf->SetProtection(array('modify'));
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetAutoPageBreak(false, 0);
$pdf->AddPage();
// Define variables
// Landscape
if ($certificate->orientation == 'L') {
    $x = 10;
    $y = 40;
    $sealx = 150;
    $sealy = 150;
    $sigx = 47;
    $sigy = 150;
    $custx = 47;
    $custy = 155;
    $wmarkx = 40;
    $wmarky = 31;
    $wmarkw = 212;
    $wmarkh = 148;
    $brdrx = 0;
    $brdry = 0;
    $brdrw = 297;
    $brdrh = 210;
    $codey = 175;
} else { // Portrait
    $x = 10;
    $y = 40;
    $sealx = 150;
    $sealy = 120;
    $sigx = 30;
    $sigy = 130;
    $custx = 30;
    $custy = 230;
    $wmarkx = 26;
    $wmarky = 58;
    $wmarkw = 158;
    $wmarkh = 170;
    $brdrx = 0;
    $brdry = 0;
    $brdrw = 210;
    $brdrh = 297;
    $codey = 250;
}

// Add images and lines
certificate_print_image($pdf, $certificate, CERT_IMAGE_BORDER, $brdrx, $brdry, $brdrw, $brdrh);

certificate_draw_frame($pdf, $certificate);

// Set alpha to semi-transparency
$pdf->SetAlpha(0.2);
certificate_print_image($pdf, $certificate, CERT_IMAGE_WATERMARK, $wmarkx, $wmarky, $wmarkw, $wmarkh);
$pdf->SetAlpha(1);
certificate_print_image($pdf, $certificate, CERT_IMAGE_SEAL, $sealx, $sealy, '', '');
certificate_print_image($pdf, $certificate, CERT_IMAGE_SIGNATURE, $sigx, $sigy, '', '');

// Add text
$pdf->SetTextColor(0, 0, 0);

certificate_print_text($pdf, $x, $y, 'C', 'freeserif', '', 40, get_string('title', 'certificate'));
$pdf->SetTextColor(0, 0, 0);

certificate_print_text($pdf, $x, $y + 30, 'C', 'freeserif', '', 15, get_string('certify', 'certificate'));
$pdf->SetTextColor(0, 0, 120);
certificate_print_text($pdf, $x, $y + 46, 'C', 'freeserif', '', 30, fullname($USER));
$pdf->SetTextColor(0, 0, 0);
certificate_print_text($pdf, $x, $y + 65, 'C', 'freeserif', '', 20, get_string('statement', 'certificate'));
certificate_print_text($pdf, $x, $y + 76, 'C', 'freeserif', '', 20, $course->fullname);
certificate_print_text($pdf, $x+36, $y + 125, 'L', 'freeserif', '', 10, get_string('signtext', 'certificate'));
certificate_print_text($pdf, $x, $y + 92, 'C', 'freesans', '', 14, strtoupper($course->completiondate));
//certificate_print_text($pdf, $x, $y + 102, 'C', 'freeserif', '', 10, 'Completiongrade');

//certificate_print_text($pdf, $x, $y + 112, 'C', 'freeserif', '', 10, certificate_get_outcome($certificate, $course));


//certificate_print_text($pdf, $x, $codey, 'C', 'freeserif', '', 10, certificate_get_code($certificate, $certrecord));

certificate_print_text($pdf, $custx, $custy, 'L', null, null, null, $certificate->customtext);
$pdf->Output($CFG->certificateFileName, 'I');
?>