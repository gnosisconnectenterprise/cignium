<?php

	/**
		* Custom module - Request Report page
		* Date Creation - 24/02/2015
		* Date Modification : 24/02/2015
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
	

	require_once("../config.php");
	require_once ($CFG->dirroot . '/mod/scheduler/lib.php');

	global $DB,$CFG,$USER;
	
	//$userrole =  getUserRole($USER->id);
	if( $USER->archetype == $CFG->userTypeStudent ) {
	//if(in_array($userrole, $CFG->customstudentroleid)){  // added by rajesh
	 redirect(new moodle_url('/my/learnerdashboard.php'));
	}
	
	require_login(); 
	$PAGE->set_pagelayout('globaladmin');
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('requestsreport'));
	//$PAGE->navbar->add(get_string('reports','multicoursereport'), new moodle_url($CFG->wwwroot.'/my/adminreportdashboard.php'));
	$PAGE->navbar->add(get_string('reports','multicoursereport'));
	$PAGE->navbar->add(get_string('requestsreport'));

	$sort    = optional_param('sort', 'fullname', PARAM_RAW);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 10, PARAM_INT);        // how many per page
	$export = optional_param('action', '', PARAM_ALPHANUM);
	
	$pageURL = '/my/request_report.phpp';
	checkPageIsNumeric($pageURL,$_REQUEST['page']);
				
	$sDate = '';
	$eDate = '';
		
	/*$sDate =  $CFG->reportSearchStartDate;
	$eDate = $CFG->reportSearchEndDate*/
	$sDepartment    = optional_param('department', '-1', PARAM_RAW);
	$sTeam          = optional_param('team', '-1', PARAM_RAW);  
	$sProgram       = optional_param('program', '-1', PARAM_RAW);     
	$sCourse        = optional_param('course', '-1', PARAM_RAW);       
	$sType          = optional_param('type', '-1', PARAM_RAW); 
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate       = optional_param('endDate', $eDate, PARAM_RAW);
	$sType          = optional_param('type', '-1', PARAM_RAW); 
	$selMode		= optional_param('sel_mode', '1' , PARAM_RAW);
	$userGroup      = optional_param('user_group', '-1', PARAM_RAW);
	$sUser          = optional_param('user', '-1', PARAM_RAW);
	$sClasses       = optional_param('classes', '-1', PARAM_RAW);
	
	
	$paramArray = array(
					'department' => $sDepartment,
					'team' => $sTeam,
					'program' => $sProgram,
					'course' => $sCourse,
					'type' => $sType,
					'startDate' => $sStartDate,
					'endDate' => $sEndDate,
					'sel_mode' => $selMode,
					'user_group' => $userGroup,
					'user' => $sUser,
					'classes' => $sClasses,
					'job_title' => optional_param('job_title', '-1', PARAM_RAW),
					'company' => optional_param('company', '-1', PARAM_RAW)
				  );
	
	$removeKeyArray = array();

  
	echo $OUTPUT->header(); 
	

	$globalRequestReport = getGlobalRequestReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
	$requestHTML = $globalRequestReport->requestHTML;
	echo $requestHTML;
	echo includeGraphFiles(get_string('requestsreport'));
	echo $OUTPUT->footer();
