<?php

$courseid = optional_param('course', SITEID, PARAM_INT);
$view = optional_param('view', 'day', PARAM_ALPHA);
$day = optional_param('cal_d', 0, PARAM_INT);
$mon = optional_param('cal_m', 0, PARAM_INT);
$year = optional_param('cal_y', 0, PARAM_INT);
$time = optional_param('time', 0, PARAM_INT);

$url = new moodle_url('/my/learnerdashboard.php');
if ($courseid != SITEID) {
    $url->param('course', $courseid);
}

if ($view !== 'upcoming') {
    $url->param('view', $view);
}

// If a day, month and year were passed then convert it to a timestamp. If these were passed
// then we can assume the day, month and year are passed as Gregorian, as no where in core
// should we be passing these values rather than the time. This is done for BC.
if (!empty($day) && !empty($mon) && !empty($year)) {
    if (checkdate($mon, $day, $year)) {
        $time = make_timestamp($year, $mon, $day);
    } else {
        $time = time();
    }
} else if (empty($time)) {
    $time = time();
}
$url->param('time', $time);

$PAGE->set_url($url);
$course = get_site();
$courses = calendar_get_default_courses();
$issite = true;

//require_course_login($course);

$calendar = new calendar_information(0, 0, 0, $time);
$calendar->prepare_for_view($course, $courses);
$strcalendar = get_string('calendar', 'calendar');

$renderer = $PAGE->get_renderer('core_calendar');
$calendar->add_sidecalendar_blocks($renderer, true, $view);
echo $renderer->start_layout();
echo html_writer::start_tag('div', array('class'=>'heightcontainer'));
switch($view) {
    case 'day':
        echo $renderer->show_day($calendar);
		break;
    break;
    case 'month':
        echo $renderer->show_month_detailed($calendar, $url);
	break;
    break;
    case 'upcoming':
        $defaultlookahead = CALENDAR_DEFAULT_UPCOMING_LOOKAHEAD;
        if (isset($CFG->calendar_lookahead)) {
            $defaultlookahead = intval($CFG->calendar_lookahead);
        }
        $lookahead = get_user_preferences('calendar_lookahead', $defaultlookahead);

        $defaultmaxevents = CALENDAR_DEFAULT_UPCOMING_MAXEVENTS;
        if (isset($CFG->calendar_maxevents)) {
            $defaultmaxevents = intval($CFG->calendar_maxevents);
        }
        $maxevents = get_user_preferences('calendar_maxevents', $defaultmaxevents);
        echo $renderer->show_upcoming_events($calendar, $lookahead, $maxevents);
    break;
}

//Link to calendar export page.
echo html_writer::end_tag('div');
echo $renderer->complete_layout();
?>