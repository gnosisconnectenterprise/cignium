<?php
require_once(dirname(__FILE__) . '/../config.php');
require_once($CFG->dirroot . '/my/lib.php');
Global $USER,$CFG,$DB;
 // its defined in event/event.php
$viewVal = optional_param('view', 'day', PARAM_ALPHA);
$timeVal = optional_param('time', 0, PARAM_INT);
$_SESSION['sess_add_event_from'] = '';
unset($_SESSION['sess_add_event_from']);
switch($USER->archetype){
	CASE $CFG->userTypeAdmin:
			require_once($CFG->dirroot .'/my/'.$CFG->pageAdminDashboard);
		break;
	CASE $CFG->userTypeManager:
			require_once($CFG->dirroot .'/my/'.$CFG->pageManagerDashboard);
		break;
	CASE $CFG->userTypeStudent:
			require_once($CFG->dirroot .'/my/'.$CFG->pageLearnerDashboard);
		break;
	default:
			redirect($CFG->wwwroot .'/course/index.php');
		break;
}

?>
<style>

/*.calendar-controls div.calender-view { display:none;}
.dashboard-calender .calender-view {margin: -31px 0 0 79px; width: 150px; }
.dashboard-calender .maincalendar .heightcontainer .calendar-controls .current { margin: 0 295px 0 0; width: 105px;} */

</style>

<script>


function loadCalendar(viewType, time, target){

    $('.eventlist').html('<div style="padding:10px"><?php echo get_string('loadingcalender', 'calendar');?></div>');
    var viewType = viewType;
	var timeVal = typeof(time) == 'undefined' ? 0 : time;
	var target = target;
	var calenderUrl = '<?php echo $CFG->wwwroot;?>/local/calender/<?php echo $CFG->pageAjaxCalendar;?>';

	if(viewType && timeVal){
	  dataVal = 'view='+viewType+'&time='+timeVal;
	}else if(viewType == '' && timeVal){
	  dataVal = 'time='+timeVal;
	}else if(viewType && timeVal == ''){
	  dataVal = 'view='+viewType;
	}else{
	  dataVal = '';
	}
	
	 if(dataVal!= ''){
	   calenderUrl = '<?php echo $CFG->wwwroot;?>/local/calender/<?php echo $CFG->pageAjaxCalendar;?>?'+dataVal;
	 }
		
    $.ajax({
	  
	    url:'<?php echo $CFG->wwwroot;?>/local/calender/<?php echo $CFG->pageAjaxCalendar;?>',
		type:'GET',
		data:dataVal,
		dataType:'html',
		success:function(data){

          if(data!=''){
            //var heightContainerData = $('.maincalendar', data);
			
			$('.calender-block').html(data);

				if(viewType == 'day'){

					  $('.commands a.edit').each(function(key, val){
		
						  $(this).attr('href', $(this).attr('href')+"&viewType="+viewType+'&timeVal='+timeVal);
						
					  });
				
					  if(target.length){
		
							$('html, body').animate({
								scrollTop: ($('[name = "'+target+'"]').offset().top - 100)
							},500);
						
					  }/*else{
						$('html, body').animate({
							scrollTop: ($('body').offset().top - 100)
						},500);
					  }*/
				}
		
		  }
		  
		  $('.calender-block').append('<input type="hidden" name="calender_url" id="calender_url" value="'+calenderUrl+'" />');
		  $('.eventlist.scroll-pane').jScrollPane();
			   
		}
	  
	  });
	  

}



$(document).ready(function(){

 
   $('.maincalendar').append('<input type="hidden" name="calender_url" id="calender_url" value="'+window.location+'" />');
   
   <?php  
    if(isset($_GET['view']) && $_GET['view'] == 'day'){ ?>
		
		if(window.location.hash.length > 0 ){

            var targetL = window.location.hash.slice(1); 
			var pos = $(this).offset();
			$('html, body').animate({
				scrollTop: ($('[name = "'+targetL+'"]').offset(pos).top - 100)
			},500);
		}
		
		
		
   <?php }  ?>
 
   <?php  
         if(isset($_GET['view']) && $_GET['view']!='' && isset($_GET['time']) && $_GET['time']!=''){ ?>

			  $('.commands a.edit').each(function(key, val){

				  $(this).attr('href', $(this).attr('href')+'&viewType=<?php echo $_GET['view'];?>&timeVal=<?php echo $_GET['time'];?>');
				
			  });
					  
   <?php }  ?>
				

    var re = /([^&=]+)=?([^&]*)/g;
		var decodeRE = /\+/g; // Regex for replacing addition symbol with a space
		var decode = function (str) {return decodeURIComponent( str.replace(decodeRE, " ") );};
		$.parseParams = function(query) {
				var params = {}, e;
				while ( e = re.exec(query) ) {
				var k = decode( e[1] ), v = decode( e[2] );
				if (k.substring(k.length - 2) === '[]') {
				k = k.substring(0, k.length - 2);
				(params[k] || (params[k] = [])).push(v);
				}
				else params[k] = v;
				}
				return params;
	};

	$(document).on('click', '.day-view', function(event){
	    
		event.preventDefault();
		$('.month-view').removeClass('active');
		$(this).addClass('active');
	    loadCalendar('day', 0, 0);
	
	});
	
	$(document).on('click', '.month-view', function(event){
	    
		event.preventDefault();
		$('.day-view').removeClass('active');
		$(this).addClass('active');
	    loadCalendar('month', 0, 0);
	
	});
	
	$(document).on('click', '.maincalendar .previous, .maincalendar .next', function(event){
	    
		event.preventDefault();
		var viewType = 'day';
		var url = $(this).attr('href');
		var jsonData = $.parseParams( url.split('?')[1] || '' );
		
	    viewType = jsonData.view;
	    time = jsonData.time.length > 0 ? jsonData.time: 0 ;
	    loadCalendar(viewType, time, 0);
	
	});
	
	$(document).on('click', '.calendar_event_classroom a:not(.delete, .edit),.calendar_event_course a:not(.delete, .edit), .calendar_event_global a:not(.delete, .edit), .calendar_event_user a:not(.delete, .edit), .maincalendar table.calendarmonth td div.day a,  .maincalendar table.calendarmonth td a.more-event', function(event){
	    
		
		var viewType = 'day';
		var url = $(this).attr('href');
		var jsonData = $.parseParams( url.split('?')[1] || '' );

        target = this.hash.slice(1);
	    viewType = jsonData.view;

        if(jsonData.time == undefined){
		  window.document.href=url;
		}else{
		 event.preventDefault(); 
	     time = jsonData.time.length > 0 ? jsonData.time: 0 ;
	     loadCalendar(viewType, time, target);
		}
	
	});
	
	$(document).on('click', '.calendar_event_classroom a.delete, .calendar_event_course a.delete, .calendar_event_global a.delete, .calendar_event_user a.delete', function(event){
	    
		event.preventDefault();
		var deleteMsg = '<?php echo get_string('confirmeventdelete', 'calendar'); ?>';
		if(confirm(deleteMsg)){
           var url = $(this).attr('href');
		   var deleteUrl = url.replace("event/delete.php","local/calender/delete_event.php");
		   var tableToBeDeleted = $(this).parents('table.event').attr('id');
		   $('#'+tableToBeDeleted).html('<tr><td class="deletingevent"><?php echo get_string('deletingevent', 'calendar'); ?></td></tr>');
		    $.ajax({
	  
				url:deleteUrl,
				type:'GET',
				data:'sesskey=<?php echo sesskey();?>',
				dataType:'html',
				success:function(data){
		          
				    if(data == 'success'){
					    $('#'+tableToBeDeleted).html('<tr><td class="eventdeletedsuccessfully"><?php echo get_string('eventdeletedsuccessfully', 'calendar'); ?></td></tr>');
					    $('#'+tableToBeDeleted).fadeOut(500, function() { $('#'+tableToBeDeleted).remove(); });
					    var calenderUrl = $('#calender_url').val();
						var viewType = 'day';
						var jsonData = $.parseParams( calenderUrl.split('?')[1] || '' );

						viewType = typeof(jsonData.view) == 'undefined' ? 0: jsonData.view ;
						time = typeof(jsonData.time) == 'undefined' ? 0: jsonData.time ;
						loadCalendar(viewType, time, 0);
					}else{
					  $('#'+tableToBeDeleted).html('<tr><td class="eventnotdeletedsuccessfully"><?php echo get_string('eventnotdeletedsuccessfully', 'calendar'); ?></td></tr>');
					}
					
		
					   
				}
		  
		  });
	  
		 	
		  
		}else{
		   return false;
		}
		
	
	});
	
	
	
});

loadCalendar('<?php echo $viewVal;?>', '<?php echo $timeVal;?>', 0);
</script>

<?php echo $OUTPUT->footer();?>

