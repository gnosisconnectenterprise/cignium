<?php
/*
Get all assets of a course
*/
/*
function getAssetInfoForCourse($courseIds,$lastaccessed = '',$skipExpired = 0){
	Global $DB,$CFG,$USER;
	$where = '';
	if($courseIds != ''){
		$where .= " AND c.courseid IN (".$courseIds.")"; 
	}
	if($lastaccessed == 'lastaccessed'){
		$where .=' AND (c.lastaccessed IS NULL || c.lastaccessed  = "")';
	}
	if($skipExpired == 1){
		$timeNow = strtotime('now');
		$where .= " AND ((mc.enddate + 86399) >=".$timeNow." AND ((c.end_date + 86399) >=".$timeNow." || c.end_date = 0 || c.end_date IS NULL))";
	}
	$query = " SELECT CONCAT_WS( '_', 'c', 0, CONVERT( c.courseid, char ) , CONVERT( c.instanceid, char ) , CONVERT( c.module, char ) ) AS difId, c . * FROM ((SELECT * FROM vw_user_full_resource_status) UNION ( SELECT * FROM vw_user_full_scorm_status)) AS c
	LEFT JOIN mdl_course as mc ON mc.id = c.courseid
	WHERE  c.userid = ".$USER->id.$where;
	$query .= " AND c.is_resource_reference_material = '0' AND IF( c.resource_type_id = 0,  '1 = 1', c.allow_resource_student_access = 1 ) ";
	$courseAssetList = $DB->get_records_sql($query);
	
	
	return $courseAssetList;
}*/

function getAssetInfoForCourse($courseIds,$lastaccessed = '',$skipExpired = 0){
	Global $DB,$CFG,$USER;
	$where = '';
	if($courseIds != ''){
		$where .= " AND c.courseid IN (".$courseIds.")"; 
	}
	if($lastaccessed == 'lastaccessed'){
		$where .=' AND (c.lastaccessed IS NULL || c.lastaccessed  = "")';
	}
	if($skipExpired == 1){
		$timeNow = strtotime('now');
		$where .= " AND ((mc.enddate + 86399) >=".$timeNow." AND ((c.end_date + 86399) >=".$timeNow." || c.end_date = 0 || c.end_date IS NULL))";
	}
	$query = " SELECT CONCAT_WS( '_', 'c', 0, CONVERT( c.courseid, char ) , CONVERT( c.instanceid, char ) , CONVERT( c.module, char ) ) AS difId, c . * FROM ((select 0 AS `program_id`,`c`.`id` AS `courseid`,`r`.`id` AS `instanceid`,`m`.`module` AS `module`,`map`.`userid` AS `userid`,`c`.`fullname` AS `fullname`,`c`.`summary` AS `description`,`c`.`learningobj` AS `learningobj`,`c`.`performanceout` AS `performanceout`,`r`.`name` AS `asset_name`,`r`.`is_weblink` AS `is_weblink`,`r`.`weblink_url` AS `weblink_url`,`r`.`intro` AS `asset_info`,if((`nl`.`id` > 0),'completed','not started') AS `combined_stat`,'organization' AS `organization`,`r`.`revision` AS `scoid`,`m`.`id` AS `moduleid`,`nl`.`timeaccess` AS `lastaccessed`,1000 AS `width`,700 AS `height`,`r`.`revision` AS `revision`,0 AS `score`,`r`.`resource_type` AS `resource_type_id`,(select `mdl_resource_type`.`name` from `mdl_resource_type` where (`mdl_resource_type`.`id` = `r`.`resource_type`) limit 1) AS `resource_type_name`,`r`.`is_reference_material` AS `is_resource_reference_material`,`r`.`allow_student_access` AS `allow_resource_student_access`,`r`.`createdby` AS `resource_createdby`,`nl`.`firstaccess` AS `completed_on`,(select count(0) from `mdl_launch_log` `mll` where ((`mll`.`userid` = `map`.`userid`) and (`mll`.`module` = 17) and (`mll`.`asset_id` = `r`.`id`))) AS `max_attempt`,(select count(0) from `mdl_launch_log` `mll` where ((`mll`.`userid` = `map`.`userid`) and (`mll`.`module` = 17) and (`mll`.`asset_id` = `r`.`id`))) AS `launch_count`,`map`.`end_date` AS `end_date` from (((((`mdl_user_course_mapping` `map` inner join `mdl_course` `c` on((`c`.`id` = `map`.`courseid`) AND map.userid=".$USER->id.")) left join `mdl_course_categories` `cc` on((`cc`.`id` = `c`.`category`))) left join `mdl_course_modules` `m` on(((`m`.`course` = `c`.`id`) and (`m`.`module` = 17)))) left join `mdl_resource` `r` on(((`r`.`course` = `c`.`id`) and (`r`.`id` = `m`.`instance`) and (`m`.`module` = 17)))) left join `mdl_user_noncourse_lastaccess` `nl` on(((`nl`.`courseid` = `c`.`id`) and (`nl`.`resource_id` = `r`.`id`) and (`m`.`module` = 17) and (`nl`.`userid` = `map`.`userid`)))) where ((`c`.`id` <> 1) and (`map`.`status` = 1) and (`map`.`program_user_ref_id` = 0) and (`m`.`module` = 17) and (`r`.`is_active` = 1) and (`c`.`is_active` = 1)) ) UNION (select 0 AS `program_id`,`c`.`id` AS `courseid`,`s`.`id` AS `instanceid`,`m`.`module` AS `module`,`map`.`userid` AS `userid`,`c`.`fullname` AS `fullname`,`c`.`summary` AS `description`,`c`.`learningobj` AS `learningobj`,`c`.`performanceout` AS `performanceout`,`s`.`name` AS `asset_name`,'0' AS `is_weblink`,'' AS `weblink_url`,`s`.`intro` AS `asset_info`,if((group_concat(if(isnull(`sst`.`value`),'not started',if((`sst`.`value` regexp '[0-9]+'),'incomplete',`sst`.`value`)) separator ' ') regexp 'passed|completed'),'completed',if((group_concat(if(isnull(`sst`.`value`),'not started',if((`sst`.`value` regexp '[0-9]+'),'incomplete',`sst`.`value`)) separator ' ') regexp 'not started'),'not started',if(isnull(group_concat(if(isnull(`sst`.`value`),'not started',if((`sst`.`value` regexp '[0-9]+'),'incomplete',`sst`.`value`)) separator ' ')),'','incomplete'))) AS `combined_stat`,`sco`.`organization` AS `organization`,`sco`.`id` AS `scoid`,`m`.`id` AS `moduleid`,max(`sst`.`timemodified`) AS `lastaccessed`,`s`.`width` AS `width`,`s`.`height` AS `height`,1 AS `revision`,group_concat(if((`sst`.`element` = 'cmi.core.score.raw'),`sst`.`value`,' ') separator '_') AS `score`,'0' AS `resource_type_id`,'0' AS `resource_type_name`,'0' AS `is_resource_reference_material`,'1' AS `allow_resource_student_access`,'0' AS `resource_createdby`,group_concat(if((`sst`.`value` regexp 'passed|completed'),cast(`sst`.`timemodified` as char charset utf8),' ') separator '_') AS `completed_on`,max(`sst`.`attempt`) AS `max_attempt`,(select count(0) from `mdl_launch_log` `mll` where ((`mll`.`userid` = `map`.`userid`) and (`mll`.`module` = 18) and (`mll`.`asset_id` = `s`.`id`))) AS `launch_count`,`map`.`end_date` AS `end_date` from ((((((`mdl_user_course_mapping` `map` inner join `mdl_course` `c` on((`c`.`id` = `map`.`courseid`) AND map.userid=".$USER->id.")) left join `mdl_course_categories` `cc` on((`cc`.`id` = `c`.`category`))) left join `mdl_course_modules` `m` on(((`m`.`course` = `c`.`id`) and (`m`.`module` = 18)))) left join `mdl_scorm` `s` on(((`s`.`course` = `c`.`id`) and (`s`.`id` = `m`.`instance`)))) left join `mdl_scorm_scoes_track` `sst` on(((`m`.`instance` = `sst`.`scormid`) and (`m`.`module` = 18) and (`map`.`userid` = `sst`.`userid`)))) left join `mdl_scorm_scoes` `sco` on(((`m`.`instance` = `sco`.`scorm`) and (`sco`.`id` = `sst`.`scoid`) and (`sco`.`scormtype` = 'sco')))) where ((`c`.`id` <> 1) and (`map`.`status` = 1) and (`map`.`program_user_ref_id` = 0) and (`m`.`module` = 18) and (`s`.`is_active` = 1) and (`c`.`is_active` = 1)) group by `m`.`instance`,`map`.`userid` )) AS c
	LEFT JOIN mdl_course as mc ON mc.id = c.courseid
	WHERE  c.userid = ".$USER->id.$where;
	$query .= " AND c.is_resource_reference_material = '0' AND IF( c.resource_type_id = 0,  '1 = 1', c.allow_resource_student_access = 1 ) ";
	$courseAssetList = $DB->get_records_sql($query);
	
	
	return $courseAssetList;
}

/*
Get combined course info for dashboard - similar to above function bt with `course_stat` and group by field
*/
function getAssetInfoForCourseDashboard($courseIds,$lastaccessed = '',$skipExpired = 0){
	Global $DB,$CFG,$USER;
	$where = '';
	if($courseIds != ''){
		$where .= " AND c.courseid IN (".$courseIds.")"; 
	}
	if($lastaccessed == 'lastaccessed'){
		$where .=' AND (c.lastaccessed IS NULL || c.lastaccessed  = "")';
	}
	if($skipExpired == 1){
		$timeNow = strtotime('now');
		$where .= " AND ((mc.enddate + 86399) >=".$timeNow." AND ((c.end_date + 86399) >=".$timeNow." || c.end_date = 0 || c.end_date IS NULL))";
	}
	$query = " SELECT CONCAT_WS( '_', 'c', 0, CONVERT( c.courseid, char ) , CONVERT( c.instanceid, char ) , CONVERT( c.module, char ) ) AS difId, c . *,IF((GROUP_CONCAT(c.combined_stat SEPARATOR ' ') REGEXP 'not started'), IF((GROUP_CONCAT(c.combined_stat SEPARATOR ' ') REGEXP 'incomplete'),'incomplete', IF((GROUP_CONCAT(c.combined_stat SEPARATOR ' ') REGEXP 'passed|completed'),'incomplete','not started')), IF((GROUP_CONCAT(c.combined_stat SEPARATOR ' ') REGEXP 'incomplete'),'incomplete', IF((GROUP_CONCAT(c.combined_stat SEPARATOR ' ') REGEXP 'passed|completed'),'completed',''))) AS `course_stat` FROM ((SELECT * FROM vw_user_full_resource_status) UNION ( SELECT * FROM vw_user_full_scorm_status)) AS c
	LEFT JOIN mdl_course as mc ON mc.id = c.courseid
	WHERE  c.userid = ".$USER->id.$where;
	$query .= " AND c.is_resource_reference_material = '0' AND IF( c.resource_type_id = 0,  '1 = 1', c.allow_resource_student_access = 1 )  GROUP BY c.courseid";
	$courseAssetList = $DB->get_records_sql($query);
	return $courseAssetList;
}
function getAssetInfoForProgram($programIds,$lastaccessed = '',$skipExpired = 0){
	Global $DB,$CFG,$USER;
	$where = '';
	if($programIds != ''){
		$where .= " AND c.program_id IN (".$programIds.")"; 
	}
	if($lastaccessed == 'lastaccessed'){
		$where .=' AND (c.lastaccessed IS NULL || c.lastaccessed  = "")';
	}
	if($skipExpired == 1){
		$timeNow = strtotime('now');
		$where .= " AND ((mc.enddate + 86399) >=".$timeNow." AND ((c.end_date + 86399) >=".$timeNow." || c.end_date = 0 || c.end_date IS NULL))";
	}
	$query = "SELECT CONCAT_WS('_','p',CONVERT(c.program_id,char),CONVERT(c.courseid,char),CONVERT(c.instanceid,char),CONVERT(c.module,char)) as difId,c.*,IF((GROUP_CONCAT(c.combined_stat SEPARATOR ' ') REGEXP 'not started'), IF((GROUP_CONCAT(c.combined_stat SEPARATOR ' ') REGEXP 'incomplete'),'incomplete', IF((GROUP_CONCAT(c.combined_stat SEPARATOR ' ') REGEXP 'passed|completed'),'incomplete','not started')), IF((GROUP_CONCAT(c.combined_stat SEPARATOR ' ') REGEXP 'incomplete'),'incomplete', IF((GROUP_CONCAT(c.combined_stat SEPARATOR ' ') REGEXP 'passed|completed'),'completed',''))) AS `course_stat` FROM (	( SELECT * FROM vw_user_full_program_resource ) UNION ( SELECT * FROM vw_user_full_program_scorm_status )) as c 
	LEFT JOIN mdl_course as mc ON mc.id = c.courseid
	WHERE c.userid = ".$USER->id.$where;
	$query .= " AND c.is_resource_reference_material = '0' AND IF( c.resource_type_id = 0,  '1 = 1', c.allow_resource_student_access = 1 ) AND mc.coursetype_id = 1 GROUP BY mc.id ";
	$programCourseList = $DB->get_records_sql($query);
	return $programCourseList;
}
function userAvailableCourses($userId=0){
	Global $DB,$CFG,$USER;
	if($userId == 0){
		$userId = $USER->id;
	}
	$query = "(SELECT concat_ws('_',cast(p.id as char charset utf8),cast(p.courseid as char charset utf8)) AS difId,p.*
			FROM vw_user_available_program_data AS p
			WHERE p.userid = ".$userId."
			) UNION 
			(
			SELECT concat_ws('_',cast(0 as char charset utf8),cast(c.courseid as char charset utf8)) AS difId,0 AS id,'name' AS name, 'p_description' AS program_desxcription,'pobj'
			as pobj,'pout' as pout,c.*
			FROM vw_user_available_course_data AS c
			WHERE c.userid = ".$userId."
			)";
	
	$availableList = $DB->get_records_sql($query);
	return $availableList;

}