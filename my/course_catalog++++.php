<?php
/**
	* Custom module - Learner Dashboard
	* Date Creation - 04/06/2014
	* Date Modification : 24/06/2014
	* Last Modified By : Bhavana Kadwal
*/
require_once(dirname(__FILE__) . '/../config.php');
global $CFG,$USER,$DB;
require_once($CFG->dirroot . '/my/lib.php');
require_once($CFG->dirroot.'/mod/scorm/locallib.php');
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot.'/calendar/lib.php');
require_once($CFG->dirroot . '/my/learninglib.php');

$isBelongToED = isLoginUserBelongsToED(); 

/*
if($USER->archetype == $CFG->userTypeManager){ 
	if($CFG->allowExternalDepartment == 1 && $isBelongToED){ 
	   if($CFG->openCatalogSection != 1){
	      
		  $key = isset($_GET['key'])?$_GET['key']:'';
		  if($key){
	        redirect($CFG->wwwroot .'/course/index.php?key='.$key);
		  }else{
		     redirect($CFG->wwwroot .'/');
		  }
	   }
	}
}*/

if($USER->archetype == $CFG->userTypeStudent){  
	if($isBelongToED){ 
	   if($CFG->openAvailableCourseSection != 1){
	      redirect($CFG->wwwroot .'/');
	   }
	}
}

require_login();
$groupids = getLearnerGroups();
if($groupids){
	$parentGroupCond = " OR gc.groupid IN(".$groupids.")";
	$parentGroupCond_prog = " OR pg.group_id IN(".$groupids.")";
	$parentGroupCond_prog_course = " OR pc.group_id IN(".$groupids.")";

}
else{
	$parentGroupCond = "";
	$parentGroupCond_prog = "";
	$parentGroupCond_prog_course = "";
}
$strmymoodle = get_string('myhome');
$header = "$SITE->shortname: $strmymoodle";
$context = get_context_instance(CONTEXT_SYSTEM);
$params = array();
$header = $SITE->fullname.": ".get_string('mylearningpage', 'learnercourse');
$PAGE->set_title($header);
$PAGE->set_heading($header);
$PAGE->set_pagelayout('mydashboard');
$stredit   = get_string('edit');
$strdelete = get_string('delete');
$strdeletecheck = get_string('deletecheck');
$strconfirm = get_string('confirm');
if($USER->archetype == $CFG->userTypeStudent){
	$PAGE->navbar->add(get_string('mylearningpage','menubar'));
}else{
	$PAGE->navbar->add(get_string('course_catalog','menubar'));
}
$pageURL = '/my/course_catalog.php';
checkPageIsNumeric($pageURL,$_REQUEST['page']);
echo $OUTPUT->header();
$page         = optional_param('page', 1, PARAM_INT);
$perpage      = optional_param('perpage', 10, PARAM_INT);
$page1 = $page - 1;
$page1 = $page1*$perpage;
$limit = '';
if($perpage != 0){
	$limit = "LIMIT $page1,$perpage";
}
$all = 0;
$program = 0;
$course = 0;
$classroom = 0;
if(isset($_REQUEST['all']) && $_REQUEST['all'] =='all'){
		$all = 1;
}else{
	if(isset($_REQUEST['program']) && $_REQUEST['program'] == 'program' ){
		$program = 1;
	}
	if(isset($_REQUEST['course']) && $_REQUEST['course'] == 'course'){
		$course = 1;
	}
	if(isset($_REQUEST['classroom']) && $_REQUEST['classroom'] == 'classroom'){
		$classroom = 1;
	}
}

$genURL = '/my/course_catalog.php?'.$_SERVER['QUERY_STRING'];
$removeKeyArray = array('perpage');
$paramArray = array(
					'all' => $_REQUEST['all'],
					'program' => $_REQUEST['program'],
					'course' => $_REQUEST['course'],
					'classroom' => $_REQUEST['classroom'],
					'key' => $_REQUEST['key']
				);
$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);

$searchText = '';
$searchTextField = '';
if(isset($_REQUEST['key']) && trim($_REQUEST['key']) !=''){
		$searchTextField = $_REQUEST['key'];
		$searchText = addslashes(strtolower(trim($_REQUEST['key'])));
}
$wheCourseChk = '';
if($course == 0 && $classroom == 1){
	$wheCourseChk = " AND c.coursetype_id = 2";
}else if($course == 1 && $classroom == 0){
	$wheCourseChk = " AND c.coursetype_id = 1";
}
if($USER->archetype != $CFG->userTypeStudent){

	$programSql = "(SELECT concat_ws('_',cast(p.id as char charset utf8),cast(0 as char charset utf8)) AS					difId,p.id as pid,0 as cid,p.name,'' AS fullname,p.performanceout as outcome,p.learningobj					as objective, p.description,f.contextid, f.component, f.filearea, f.filename, 0 as						coursetype_id, 0 AS criteria , 0 AS is_global, 0 AS coursecreatedby,0 as end_date,0 as credithours
					FROM mdl_programs AS p
					LEFT JOIN mdl_context AS ct ON ct.instanceid = p.id AND ct.contextlevel = 90
					LEFT JOIN mdl_files AS f ON f.contextid = ct.id AND f.component = 'program' AND f.filearea = 'programimage' AND f.filename !='' AND f.filename !='.'
					WHERE p.`status` = 1 AND p.deleted = 0 AND p.`publish` = 1)";
		
			
	/*$courseSql  = "(SELECT concat_ws('_',cast(0 as char charset utf8),cast(c.id as char charset utf8)) AS difId,0 as pid,c.id as cid,'' AS name,c.fullname as fullname,
					c.performanceout as outcome,c.learningobj as objective,c.summary as description,
					f.contextid, f.component, f.filearea, f.filename,c.coursetype_id, c.criteria, c.is_global, c.createdby AS coursecreatedby ,0 as  end_date,c.credithours as credithours
					FROM mdl_course as c
					LEFT JOIN mdl_context AS ct ON ct.instanceid = c.id AND ct.contextlevel = 50
					LEFT JOIN mdl_files as f On f.contextid = ct.id AND f.component = 'course' AND f.filearea = 'overviewfiles' AND f.filename !='' AND f.filename !='.'
					WHERE c.publish = 1 AND c.is_active = 1 AND c.id != 1  AND c.coursetype_id = 1)";*/
					
	if($USER->archetype == $CFG->userTypeManager){
	
				
		$allUserRoleDetails = getAllUserRoleDetails();
		if(count($allUserRoleDetails ) > 0 ){
		  $adminUserIdsArr = array(); 
		  foreach($allUserRoleDetails  as $arrRoles){
			if($arrRoles->roleid == 1){
			  $adminUserIdsArr[] = $arrRoles->id;
			}
		  }
		  $adminUserIds = 0;
		  $WhManager  = '';
		  if(count($adminUserIdsArr) > 0 ){
			$adminUserIds = implode(",", $adminUserIdsArr);
			$WhManager = " AND (dc.departmentid = '".$USER->department."' || c.createdby IN (".$adminUserIds."))";
		  }
		  
		  
		  
		}
		
	  $courseSql  = "(SELECT concat_ws('_',cast(0 as char charset utf8),cast(c.id as char charset utf8)) AS difId,0 as pid,c.id as cid,'' AS name,c.fullname as fullname,
					c.performanceout as outcome,c.learningobj as objective,c.summary as description,
					f.contextid, f.component, f.filearea, f.filename,c.coursetype_id, c.criteria, c.is_global, c.createdby AS coursecreatedby ,0 as  end_date,c.credithours as credithours
					FROM mdl_course as c
					LEFT JOIN mdl_department_course dc ON dc.courseid = c.id AND dc.is_active = 1
					LEFT JOIN mdl_context AS ct ON ct.instanceid = c.id AND ct.contextlevel = 50
					LEFT JOIN mdl_files as f On f.contextid = ct.id AND f.component = 'course' AND f.filearea = 'overviewfiles' AND f.filename !='' AND f.filename !='.'
					WHERE c.publish = 1 AND c.is_active = 1 AND c.id != 1  AND c.coursetype_id = 1 $WhManager )";
					
		$classroomSql  = "(	
						SELECT CONCAT_WS('_', CAST(0 AS CHAR CHARSET utf8), CAST(c.id AS CHAR CHARSET utf8)) AS difId,0 AS pid,c.id AS cid,'' AS name,c.fullname AS fullname, c.performanceout AS outcome,c.learningobj AS objective,c.summary AS description, c.criteria,c.is_global,c.createdby AS coursecreatedby, f.contextid,f.component, f.filearea, f.filename,coursetype_id,@enddatevar := MAX(s.enddate) AS end_date, GROUP_CONCAT(ss.duration separator '_') as credithours
						FROM mdl_course AS c
						LEFT JOIN mdl_scheduler as s ON s.course = c.id
						LEFT JOIN mdl_scheduler_enrollment as se ON se.scheduler_id = s.id AND se.userid = $USER->id
						LEFT JOIN mdl_scheduler_slots ss ON s.id = ss.schedulerid
						LEFT JOIN mdl_context AS ct ON ct.instanceid = c.id AND ct.contextlevel = 50
						LEFT JOIN mdl_files AS f ON f.contextid = ct.id AND f.component = 'course' AND f.filearea = 'overviewfiles' AND f.filename !='' AND f.filename !='.'
						WHERE c.publish = 1 AND c.id != 1 AND c.is_active = 1 AND (se.is_approved != 1 && se.is_approved != 2) AND c.coursetype_id = 2 AND (s.enddate + 86399) >=".strtotime('now')."
						GROUP BY c.id,se.userid)";
	}else{
	
	  $courseSql  = "(SELECT concat_ws('_',cast(0 as char charset utf8),cast(c.id as char charset utf8)) AS difId,0 as pid,c.id as cid,'' AS name,c.fullname as fullname,
					c.performanceout as outcome,c.learningobj as objective,c.summary as description,
					f.contextid, f.component, f.filearea, f.filename,c.coursetype_id, c.criteria, c.is_global, c.createdby AS coursecreatedby ,0 as  end_date,c.credithours as credithours
					FROM mdl_course as c
					LEFT JOIN mdl_context AS ct ON ct.instanceid = c.id AND ct.contextlevel = 50
					LEFT JOIN mdl_files as f On f.contextid = ct.id AND f.component = 'course' AND f.filearea = 'overviewfiles' AND f.filename !='' AND f.filename !='.'
					WHERE c.publish = 1 AND c.is_active = 1 AND c.id != 1  AND c.coursetype_id = 1)";
					
		$classroomSql  = "(SELECT concat_ws('_',cast(0 as char charset utf8),cast(c.id as char charset utf8)) AS					difId,0 as pid,c.id as cid,'' AS name,c.fullname as fullname,
					c.performanceout as outcome,c.learningobj as objective,c.summary as description,
					f.contextid, f.component, f.filearea, f.filename,c.coursetype_id, c.criteria, c.is_global, c.createdby AS coursecreatedby, 0 AS end_date , 0 as credithours
					FROM mdl_course as c
					LEFT JOIN mdl_context AS ct ON ct.instanceid = c.id AND ct.contextlevel = 50
					LEFT JOIN mdl_files as f On f.contextid = ct.id AND f.component = 'course' AND f.filearea = 'overviewfiles' AND f.filename !='' AND f.filename !='.'
					WHERE c.publish = 1 AND c.is_active = 1 AND c.id != 1  AND c.coursetype_id = 2)";
	}
	if($all == 1){
		$selectSql = $programSql.' UNION '.$courseSql.' UNION '.$classroomSql;
	}else{
		if( (($program == 1) && ($course == 1) && ($classroom == 1)) || ($program == 0 && $course == 0 && $classroom == 0 && $all == 0) ){
			$selectSql = $programSql.' UNION '.$courseSql.' UNION '.$classroomSql;
		}else{
			if($program == 1){
				$selectSql .= $programSql.' UNION ';
			}
			if($course == 1){
				$selectSql .= $courseSql.' UNION ';
			}
			if($classroom == 1){
				$selectSql .= $classroomSql.' UNION ';
			}
			$selectSql = substr($selectSql, 0, -6);
		}
	}
	$programCoursesSql = "SELECT ##SELECT_VALUE##
							FROM (
									##SELECT_SQL##
								) as catalog ";
	$programCoursesSql = str_replace("##SELECT_SQL##",$selectSql,$programCoursesSql);
	$departmentPrograms = $DB->get_records_sql("SELECT concat_ws('_',cast(p.id as char charset utf8),cast(0 as char charset utf8)) AS difId FROM mdl_programs AS p LEFT JOIN mdl_program_department AS pd ON pd.program_id = p.id AND pd.is_active = 1 LEFT JOIN mdl_department_members AS dm ON dm.departmentid = pd.department_id WHERE dm.userid = ".$USER->id."  AND p.`publish` = 1");
	$departmentCourses = $DB->get_records_sql("SELECT concat_ws('_',cast(0 as char charset utf8),cast(c.id as char charset utf8)) AS difId FROM mdl_course AS c LEFT JOIN mdl_department_course AS dc ON dc.courseid = c.id LEFT JOIN mdl_department_members AS dm ON dm.departmentid = dc.departmentid WHERE dc.is_active = 1 AND c.deleted = 0 AND c.is_active = 1 AND dm.userid = ".$USER->id);
	
}else{
	$userDepartment = $DB->get_record_sql("SELECT dm.departmentid
											FROM mdl_user u
											LEFT JOIN mdl_department_members as dm ON dm.userid = u.id
											WHERE u.id = ".$USER->id);
	$programSql = "(SELECT CONCAT_WS('_', CAST(p.id AS CHAR CHARSET utf8), CAST(0 AS CHAR CHARSET utf8)) AS					difId,p.id AS pid,0 AS cid,p.name,'' AS fullname, p.performanceout AS									outcome,p.learningobj AS objective,p.description, 0 as criteria, 0 as is_global , 0 as coursecreatedby, f.contextid, f.component, f.filearea,					f.filename, 0 as coursetype_id,''  as end_date, 0 as credithours
					FROM mdl_programs AS p
					LEFT JOIN mdl_program_department pd ON pd.program_id = p.id AND pd.is_active = 1
					LEFT JOIN mdl_program_user_mapping as pum ON pum.program_id = p.id AND pum.user_id = $USER->id AND pum.`status` = 1
					LEFT JOIN mdl_program_group pc ON pc.program_id = p.id AND pc.is_active = 1 AND pc.group_id IN (SELECT gm.groupid FROM mdl_groups_members AS gm WHERE gm.is_active = 1 AND gm.userid = $USER->id)
					LEFT JOIN mdl_context AS ct ON ct.instanceid = p.id AND ct.contextlevel = 90
					LEFT JOIN mdl_files AS f ON f.contextid = ct.id AND f.component = 'program' AND f.filearea = 'programimage' AND f.filename !='' AND f.filename !='.'
					WHERE p.`status` = 1 AND p.deleted = 0  AND p.`publish` = 1 AND pd.department_id = ".$userDepartment->departmentid." AND pum.user_id IS NULL AND pc.id IS NULL
				)";
	$programSql .= "UNION (SELECT CONCAT_WS('_', CAST(p.id AS CHAR CHARSET utf8), CAST(0 AS CHAR CHARSET utf8)) AS					difId,p.id AS pid,0 AS cid,p.name,'' AS fullname, 
				p.performanceout AS outcome,p.learningobj AS objective,p.description, 0 AS criteria, 0 as is_global, 
				0 AS coursecreatedby, f.contextid, f.component, f.filearea,	f.filename, 0 AS coursetype_id,pc.end_date as end_date, 0 as credithours
				FROM mdl_groups_members AS gm
				LEFT JOIN mdl_program_group pc ON (pc.group_id = gm.groupid ".$parentGroupCond_prog_course.") AND pc.is_active = 1
				LEFT JOIN mdl_programs AS p ON pc.program_id = p.id
				LEFT JOIN mdl_program_user_mapping AS pum ON pum.program_id = p.id AND pum.`status` = 1 AND pum.user_id = $USER->id
				LEFT JOIN mdl_context AS ct ON ct.instanceid = p.id AND ct.contextlevel = 90
				LEFT JOIN mdl_files AS f ON f.contextid = ct.id AND f.component = 'program' AND f.filearea = 'programimage' AND f.filename !='' AND f.filename !='.'
				WHERE p.publish = 1 AND pum.id IS NULL AND gm.userid = $USER->id
				AND gm.is_active = 1
				)";

	
   if($USER->archetype == $CFG->userTypeStudent && $CFG->allowExternalDepartment == 1 && $isBelongToED ){
   
      $courseSql  = "(SELECT CONCAT_WS('_', CAST(0 AS CHAR CHARSET utf8), CAST(c.id AS CHAR CHARSET utf8)) AS					difId,0 AS pid,c.id AS cid,'' AS name,c.fullname AS fullname, c.performanceout AS							outcome,c.learningobj AS objective,c.summary AS description, c.criteria,c.is_global,c.createdby AS coursecreatedby, f.contextid, f.component,					f.filearea, f.filename,coursetype_id,''  as end_date, c.credithours as credithours
					FROM mdl_course AS c
					LEFT JOIN mdl_department_course dc ON dc.courseid = c.id AND dc.is_active = 1
					LEFT JOIN mdl_user_course_mapping as ucm ON ucm.courseid = c.id AND ucm.`status` = 1 AND ucm.userid = $USER->id
					LEFT JOIN mdl_groups_course gc ON gc.courseid = c.id AND gc.is_active = 1 AND gc.groupid IN (SELECT gm.groupid FROM mdl_groups_members AS gm WHERE gm.is_active = 1 AND gm.userid = $USER->id)
					LEFT JOIN mdl_context AS ct ON ct.instanceid = c.id AND ct.contextlevel = 50
					LEFT JOIN mdl_files AS f ON f.contextid = ct.id AND f.component = 'course' AND f.filearea = 'overviewfiles' AND f.filename !='' AND f.filename !='.'
					";
	 $courseSql  .= " WHERE c.publish = 1 AND c.id != 1 AND c.is_active = 1 ";				
     if($CFG->CanExternalLearnerRequest == 1){
				
		  $courseSql  .= " AND (dc.departmentid = ".$userDepartment->departmentid." || (c.criteria = ".$CFG->courseCriteriaCompliance." AND (dc.departmentid = '".$USER->department."' || dc.id IS NULL)) || (c.is_global = ".$CFG->courseElectiveGlobal." AND (dc.departmentid = '".$USER->department."' || dc.id IS NULL)) ) AND ucm.id IS NULL  AND c.coursetype_id = 1 AND (gc.id IS NULL)";
					
				
	  }else{
		  $courseSql  .= "  AND (dc.departmentid = ".$userDepartment->departmentid." || (c.criteria = ".$CFG->courseCriteriaCompliance." AND (dc.departmentid = '".$USER->department."' )) || (c.is_global = ".$CFG->courseElectiveGlobal." AND (dc.departmentid = '".$USER->department."')) ) AND ucm.id IS NULL  AND c.coursetype_id = 1 AND (gc.id IS NULL) ";
			
	  }	
	  
	   $courseSql  .= ")"; 
	  
		$courseSql  .= " UNION (SELECT CONCAT_WS('_', CAST(0 AS CHAR CHARSET utf8), 
						CAST(c.id AS CHAR CHARSET utf8)) AS	difId,0 AS pid,c.id AS cid,'' AS name,c.fullname AS fullname, c.performanceout AS	outcome,c.learningobj AS objective,c.summary AS description,c.criteria,c.is_global,c.createdby AS coursecreatedby, f.contextid, f.component,f.filearea, f.filename,coursetype_id,gc.end_date  as end_date, c.credithours as credithours
						FROM mdl_groups_members as gm
						LEFT JOIN mdl_groups_course gc ON (gm.groupid = gc.groupid ".$parentGroupCond.") AND gc.is_active = 1
						LEFT JOIN mdl_course AS c ON gc.courseid = c.id AND gc.is_active = 1
						LEFT JOIN mdl_user_course_mapping AS ucm ON ucm.courseid = c.id AND ucm.`status` = 1 AND ucm.userid = ".$USER->id."
						LEFT JOIN mdl_context AS ct ON ct.instanceid = c.id AND ct.contextlevel = 50
						LEFT JOIN mdl_files AS f ON f.contextid = ct.id AND f.component = 'course' AND f.filearea = 'overviewfiles' AND f.filename !='' AND f.filename !='.'
						WHERE c.publish = 1 AND c.id != 1 AND c.is_active = 1 AND ucm.id IS NULL AND c.coursetype_id = 1 AND gm.userid = ".$USER->id." AND  gm.is_active = 1
						)";	
					
   }else{
   
	$courseSql  = "(SELECT CONCAT_WS('_', CAST(0 AS CHAR CHARSET utf8), CAST(c.id AS CHAR CHARSET utf8)) AS					difId,0 AS pid,c.id AS cid,'' AS name,c.fullname AS fullname, c.performanceout AS							outcome,c.learningobj AS objective,c.summary AS description, c.criteria,c.is_global,c.createdby AS coursecreatedby, f.contextid, f.component,					f.filearea, f.filename,coursetype_id,''  as end_date, c.credithours as credithours
					FROM mdl_course AS c
					LEFT JOIN mdl_department_course dc ON dc.courseid = c.id AND dc.is_active = 1
					LEFT JOIN mdl_user_course_mapping as ucm ON ucm.courseid = c.id AND ucm.`status` = 1 AND ucm.userid = $USER->id
					LEFT JOIN mdl_groups_course gc ON gc.courseid = c.id AND gc.is_active = 1 AND gc.groupid IN (SELECT gm.groupid FROM mdl_groups_members AS gm WHERE gm.is_active = 1 AND gm.userid = $USER->id)
					LEFT JOIN mdl_context AS ct ON ct.instanceid = c.id AND ct.contextlevel = 50
					LEFT JOIN mdl_files AS f ON f.contextid = ct.id AND f.component = 'course' AND f.filearea = 'overviewfiles' AND f.filename !='' AND f.filename !='.'
					WHERE c.publish = 1 AND c.id != 1 AND c.is_active = 1 AND (dc.departmentid = ".$userDepartment->departmentid." || (c.criteria = ".$CFG->courseCriteriaCompliance." AND (dc.departmentid = '".$USER->department."' || dc.id IS NULL)) || (c.is_global = ".$CFG->courseElectiveGlobal." AND (dc.departmentid = '".$USER->department."' || dc.id IS NULL)) ) AND ucm.id IS NULL  AND c.coursetype_id = 1 AND (gc.id IS NULL)
				)";
	$courseSql  .= " UNION (SELECT CONCAT_WS('_', CAST(0 AS CHAR CHARSET utf8), 
					CAST(c.id AS CHAR CHARSET utf8)) AS	difId,0 AS pid,c.id AS cid,'' AS name,c.fullname AS fullname, c.performanceout AS	outcome,c.learningobj AS objective,c.summary AS description,c.criteria,c.is_global,c.createdby AS coursecreatedby, f.contextid, f.component,f.filearea, f.filename,coursetype_id,gc.end_date  as end_date, c.credithours as credithours
					FROM mdl_groups_members as gm
					LEFT JOIN mdl_groups_course gc ON (gm.groupid = gc.groupid ".$parentGroupCond.") AND gc.is_active = 1
					LEFT JOIN mdl_course AS c ON gc.courseid = c.id AND gc.is_active = 1
					LEFT JOIN mdl_user_course_mapping AS ucm ON ucm.courseid = c.id AND ucm.`status` = 1 AND ucm.userid = ".$USER->id."
					LEFT JOIN mdl_context AS ct ON ct.instanceid = c.id AND ct.contextlevel = 50
					LEFT JOIN mdl_files AS f ON f.contextid = ct.id AND f.component = 'course' AND f.filearea = 'overviewfiles' AND f.filename !='' AND f.filename !='.'
					WHERE c.publish = 1 AND c.id != 1 AND c.is_active = 1 AND ucm.id IS NULL AND c.coursetype_id = 1 AND gm.userid = ".$USER->id." AND  gm.is_active = 1
					)";	
					
	}
					
	$classroomSql  = "(	
						SELECT CONCAT_WS('_', CAST(0 AS CHAR CHARSET utf8), CAST(c.id AS CHAR CHARSET utf8)) AS difId,0 AS pid,c.id AS cid,'' AS name,c.fullname AS fullname, c.performanceout AS outcome,c.learningobj AS objective,c.summary AS description, c.criteria,c.is_global,c.createdby AS coursecreatedby, f.contextid,f.component, f.filearea, f.filename,coursetype_id,@enddatevar := MAX(s.enddate) AS end_date, GROUP_CONCAT(ss.duration separator '_') as credithours
						FROM mdl_course AS c
						LEFT JOIN mdl_scheduler as s ON s.course = c.id
						LEFT JOIN mdl_scheduler_enrollment as se ON se.scheduler_id = s.id AND se.userid = $USER->id
						LEFT JOIN mdl_scheduler_slots ss ON s.id = ss.schedulerid
						LEFT JOIN mdl_context AS ct ON ct.instanceid = c.id AND ct.contextlevel = 50
						LEFT JOIN mdl_files AS f ON f.contextid = ct.id AND f.component = 'course' AND f.filearea = 'overviewfiles' AND f.filename !='' AND f.filename !='.'
						WHERE c.publish = 1 AND c.id != 1 AND c.is_active = 1 AND (se.is_approved != 1 && se.is_approved != 2) AND c.coursetype_id = 2 AND (s.enddate + 86399) >=".strtotime('now')."
						GROUP BY c.id,se.userid)";
	if($all == 1){
		$selectSql = $programSql.' UNION '.$courseSql.' UNION '.$classroomSql;
	}else{
		if( (($program == 1) && ($course == 1) && ($classroom == 1)) || ($program == 0 && $course == 0 && $classroom == 0 && $all == 0) ){
			$selectSql = $programSql.' UNION '.$courseSql.' UNION '.$classroomSql;
		}else{
			if($program == 1){
				$selectSql .= $programSql.' UNION ';
			}
			if($course == 1){
				$selectSql .= $courseSql.' UNION ';
			}
			if($classroom == 1){
				$selectSql .= $classroomSql.' UNION ';
			}
			$selectSql = substr($selectSql, 0, -6);
		}
	}
	$programCoursesSql = "SELECT ##SELECT_VALUE##
							FROM (
									##SELECT_SQL##
								) as catalog ";

	$programCoursesSql = str_replace("##SELECT_SQL##",$selectSql,$programCoursesSql);
	$departmentPrograms = $DB->get_records_sql("SELECT concat_ws('_',cast(p.id as char charset utf8),cast(0 as char charset utf8)) AS difId FROM mdl_programs p
	LEFT JOIN mdl_program_group pg ON (pg.program_id = p.id) AND pg.is_active = 1
	LEFT JOIN mdl_groups_members gm On gm.groupid = pg.group_id AND gm.is_active = 1
	WHERE (gm.userid = ".$USER->id." ".$parentGroupCond_prog.") AND p.`publish` = 1");
	$departmentCourses = $DB->get_records_sql("SELECT concat_ws('_',cast(0 as char charset utf8),cast(c.id as char charset utf8)) AS difId FROM mdl_course c
	LEFT JOIN mdl_groups_course gc ON (gc.courseid = c.id) AND gc.is_active = 1
	LEFT JOIN mdl_groups_members gm ON gm.groupid = gc.groupid AND gm.is_active = 1
	WHERE (gm.userid = ".$USER->id." $parentGroupCond )");
}
if($searchText != ''){
	$whereSql = "WHERE LOWER(catalog.name) LIKE '%".$searchText."%' || LOWER(catalog.fullname) LIKE '%".$searchText."%' || LOWER(catalog.outcome) LIKE '%".$searchText."%' || LOWER(catalog.objective) LIKE '%".$searchText."%' || LOWER(catalog.description) LIKE '%".$searchText."%'";
	$programCoursesSql .= $whereSql;
}
if($USER->original_archetype == $CFG->userTypeManager){
	$departmentPrograms2 = $DB->get_records_sql("SELECT concat_ws('_',cast(p.id as char charset utf8),cast(0 as char charset utf8)) AS difId FROM mdl_programs AS p LEFT JOIN mdl_program_department AS pd ON pd.program_id = p.id AND pd.is_active = 1 AND pd.department_id = $userDepartment->departmentid LEFT JOIN mdl_department_members AS dm ON dm.departmentid = pd.department_id WHERE dm.userid = ".$USER->id." AND p.`publish` = 1");
	$departmentCourses2 = $DB->get_records_sql("SELECT concat_ws('_',cast(0 as char charset utf8),cast(c.id as char charset utf8)) AS difId FROM mdl_course AS c LEFT JOIN mdl_department_course AS dc ON dc.courseid = c.id LEFT JOIN mdl_department_members AS dm ON dm.departmentid = dc.departmentid WHERE c.deleted = 0 AND c.is_active = 1 AND dm.userid = ".$USER->id);
	$departmentPrograms = array_merge($departmentPrograms,$departmentPrograms2);
	$departmentCourses = array_merge($departmentCourses,$departmentCourses2);
}

$dataSql = str_replace("##SELECT_VALUE##",'*',$programCoursesSql);
$dataCountSql = str_replace("##SELECT_VALUE##",'count(*) as cnt',$programCoursesSql);
//pr($dataSql);die;

$programCourses = $DB->get_records_sql($dataSql.' ORDER BY catalog.fullname ASC, catalog.name ASC '.$limit);
$programCoursesCount = $DB->get_record_sql($dataCountSql);
$programCoursesCount = $programCoursesCount->cnt;

	$paramArray = array();
	$filterArray = array();
	$outerDivStart = "<div class='tabsOuter'>";
	$outerDivEnd = "</div>";	
	//if($USER->archetype != $CFG->userTypeStudent){
		$headerFilterDiv = '<div id="common-search" class ="learning-search tabbedSec" >';
			$headerFilterDiv .= '<form method="get" action="/my/course_catalog.php" id="searchForm" name="searchForm">';
			$headerFilterDiv .=	'<div id="search-form">';
				$headerFilterDiv .= '<div class="filter" style = "display: block; float: left;">';
					$headerFilterDiv .= '<span>Filter By:</span><span><input type="checkbox" value="all" id="all" name="all" '.( (trim($_REQUEST['all']) == 'all') ? 'checked' : '').' />All</span>';
					$headerFilterDiv .= '<span><input type="checkbox" value="program" id="program" name="program" '.( (trim($_REQUEST['program']) == 'program') ? 'checked' : '').' />'.get_string("filter_program").'</span>';
					$headerFilterDiv .= '<span><input type="checkbox" value="course" id="course" name="course" '.( (trim($_REQUEST['course']) == 'course') ? 'checked' : '').' />'.get_string("filter_course").'</span>';
					$headerFilterDiv .= '<span><input type="checkbox" value="classroom" id="classroom" name="classroom" '.( (trim($_REQUEST['classroom']) == 'classroom') ? 'checked' : '').' />'.get_string("classroomcourse").'</span>';

				$headerFilterDiv .= '</div>';
				$headerFilterDiv .=	'<div class="search-input"><input type="text" value="'.( ($searchTextField != '') ? $searchTextField : '').'" onkeypress="submitSearchForm(event)" placeholder="Search" id="key" name="key"></div>';
			$headerFilterDiv .= '</div></form>
		<div class="search_clear_button"><input type="button" title="Search" onclick="document.searchForm.submit();" value="Search" name="search"><a href="javascript:void(0);" id="clear" title='.get_string("clear").' onClick="clearSearchBox();">'.get_string("clear").'</a>';
		$headerFilterDiv .='</div></div>';
	//}
	$courseTabs .= "<div class='tabLinks'>";
		if($USER->archetype != $CFG->userTypeStudent){
			//$courseTabs .= "<div class = 'table-head'><span><a href='#' class='cat_no_hover' title='".get_string('course_catalog','menubar')."' >".get_string('course_catalog','menubar')." (" .$programCoursesCount.")</a><span></div>";
			$courseTabs .= "<h2 class='icon_title'>".get_string('course_catalog','menubar')." (" .$programCoursesCount.")</h2>";
		}else{
			$courseTabs .= "<div><a href='".$CFG->wwwroot."/my/learningpage.php?type=1' title='".get_string('todo','learnercourse')."' class='$styTabs1'>".get_string('todo','learnercourse')."<span></span></a></div>";
			$courseTabs .= "<div><a href='".$CFG->wwwroot."/my/learningpage.php?type=2' title='".get_string('completed','learnercourse')."' class='$styTabs2' >".get_string('completed','learnercourse')."<span></span></a></div>";
			$courseTabs .= "<div><a href='".$CFG->wwwroot."/my/course_catalog.php' title='".get_string('available','learnercourse')."' class='current'>".get_string('available','learnercourse')."<span></span></a></div>";
		}
	$courseTabs .= "</div>";
	$html .= $outerDivStart.$courseTabs;
    $html .= $headerFilterDiv;
	$htmlDiv = '';
	$headerLegendDiv ='<div id="after-common-search"><div class="box"><span class="greenish"></span>Classroom</div><div class="box"><span class="redish"></span>Online Course</div><div class="box"><span class="bluish"></span>Program</div></div>';
	$html .= html_writer::start_tag('div', array('class'=>'borderBlockSpace'));
	if($_SESSION['update_msg']!=''){
		$html .= '<div class="clear"></div>';
		$html .= '<div class="bg-success text-success text-center '.$_SESSION['error_class'].'" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
		$html .= '<div class="clear"></div>';
		$_SESSION['update_msg']='';
		$_SESSION['error_class']='';
	}
	$html .= $headerLegendDiv;
	if (!empty($programCourses)) {
	    $cntProgramCourses =  count($programCourses) ;
		$i= 0;
		$k= 0;
		$data = new stdClass();
		$dataCourse = new stdClass();
		$currentTime = strtotime('now');

		foreach ($programCourses as $key=>$programOrCourseArray) {
			$i++;
			$courseTypeId = '';
			$classType = '';
			$enrolLink = '';
			
			$courseCriteria = $programOrCourseArray->criteria;
			$courseGlobalElective = $programOrCourseArray->is_global;
			$courseCreatedBy = $programOrCourseArray->coursecreatedby;
			$complianceDiv = '';
			$creditHoursDiv = "";
			if($programOrCourseArray->pid == 0){
				$courseProgramname = $programOrCourseArray->fullname;
				$programImgURL =  $CFG->courseDefaultImage;
				$courseTypeId = getCourseTypeIdByCourseId($programOrCourseArray->cid);
				$classType = 'border-left-red'; 
				if($courseTypeId == $CFG->courseTypeClassroom ){
				  $courseImgURL = $CFG->classroomDefaultImage;
				  $classType = 'border-left-green'; 
				}
				$complianceDiv = getCourseComplianceIcon($programOrCourseArray->cid);
				if($courseTypeId != $CFG->courseTypeClassroom){ 
					$creditHours = array_sum(explode('_',$programOrCourseArray->credithours));
					$creditHoursDiv = '<br /><strong>'.get_string('coursecredithours').'</strong>: '.setCreditHoursFormat($creditHours);
				}
			}else{
				$courseProgramname = $programOrCourseArray->name;
				$programImgURL = $CFG->programDefaultImage;
				$classType = 'border-left-blue'; 
			}
			//echo "$i != $cntProgramCourses <br>";
			$boxShadowClass = '';
			if($i < $cntProgramCourses ){
			  $boxShadowClass = 'box-shadow';
			}

			$classAlt = $i%2==0?'even-course':'odd-course';
			$imagePath = '';
			$refMaterial = array();
			$ky = $programOrCourseArray->difid;
			if($programOrCourseArray->contextid && $programOrCourseArray->component && $programOrCourseArray->filearea && $programOrCourseArray->filename){
				$programImgPath = $CFG->wwwroot.'/pluginfile.php/'.$programOrCourseArray->contextid.'/'.$programOrCourseArray->component.'/'.$programOrCourseArray->filearea.'/'.$programOrCourseArray->filename;
				
				if(isUrlExists($programImgPath)){
				  $programImgURL = $programImgPath;
				}
				
			}
			$programImg = '<img src="'.$programImgURL.'" alt="'.$courseProgramname.'" title="'.$courseProgramname.'" border="0" />';
			$imagePath = '<div class = "course-img" >'.$programImg.'</div>';
			
			$headingText = $courseProgramname;
			$summary = $programOrCourseArray->description;
			$learningObj = $programOrCourseArray->objective;

			$performanceOut = $programOrCourseArray->outcome;

			$pcLearningObj = $learningObj?('<br /><strong>'.get_string('learningobj','course').': </strong>'.$learningObj):'';
			$performanceOut = $performanceOut?('<br /><strong>'.get_string('performanceOut','course').': </strong>'.$performanceOut):'';
			$descriptionText = $summary?'<br /><strong>'.get_string('description','learnercourse').'</strong>: '.$summary:'';
			$descriptionText = $descriptionText.$pcLearningObj.$performanceOut.$creditHoursDiv;
			$htmlDiv = '<div class="no-course '.$classAlt.' '.$classType.' '.$boxShadowClass.'"   rel="'.$ky.'" id="'.$ky.'">';
			$htmlDiv .= $imagePath;
			$expiryDateDiv = "";
			$element = "";
			$elementId = 0;
			$showRequest = 0;
			if($courseCriteria != $CFG->courseCriteriaCompliance && $courseGlobalElective != $CFG->courseElectiveGlobal ){
				if($programOrCourseArray->pid != 0 && array_key_exists ($ky,$departmentPrograms)){
					if(!empty($programOrCourseArray->end_date)){
						if(($programOrCourseArray->end_date+86400) <= $currentTime){
							$element = "program";
							$elementId = $programOrCourseArray->pid;
							$showRequest = 1;
						}
						$expiryDateDiv .= '<br /><strong>'.get_string('enddate').': </strong>'.getDateFormat($programOrCourseArray->end_date, $CFG->customDefaultDateFormat);
					}else{
						$expiryDateDiv .= '<br /><strong>'.get_string('enddate').': </strong>'.get_string('any_end_date');
					}
				}elseif($courseTypeId != $CFG->courseTypeClassroom && array_key_exists ($ky,$departmentCourses)){
					if(!empty($programOrCourseArray->end_date)){
						if(($programOrCourseArray->end_date+86400) <= $currentTime){
							$element = "course";
							$elementId = $programOrCourseArray->cid;
							$showRequest = 1;
						}
						$expiryDateDiv .= '<br /><strong>'.get_string('enddate').': </strong>'.getDateFormat($programOrCourseArray->end_date, $CFG->customDefaultDateFormat);
					}else{
						$expiryDateDiv .= '<br /><strong>'.get_string('enddate').': </strong>'.get_string('any_end_date');
					}
				}
			}
			if($USER->archetype != $CFG->userTypeStudent || $USER->original_archetype == $CFG->userTypeManager){
				$expiryDateDiv = '';
			}
			$htmlDiv .= '<div class="pull-left left-content">';
			$htmlDiv .= '<div class="c-heading">'.$headingText.' '.$complianceDiv.'</div>';
			$htmlDiv .= '<div class="c-text">'.substr($descriptionText.$expiryDateDiv,6).'</div>';
			$htmlDiv .= '</div>';
			if($programOrCourseArray->pid == 0){ 
				if(array_key_exists ($ky,$departmentCourses)){
					if($USER->archetype != $CFG->userTypeStudent){
						$enrolLink = '<a href="javascript:;" class="already_enrolled" title = "'.get_string('enrolled').'"></a>';
					}else{
						if($USER->archetype != $CFG->userTypeAdmin){
							if($showRequest == 1 && $USER->original_archetype != $CFG->userTypeManager){
								$enrolLink = '<a href="#" class="requestforextention" element = "'.$element.'" element-name = "'.$courseProgramname.'" rel = "'.$elementId.'" title="'.get_string('requestforextention').'">'.get_string('requestforextention').'</a>';
							}else{
								$enrolLink = '<a href="javascript:;" data-url="'.$programOrCourseArray->cid.'" data-msg="'.get_string('areyousureyouwanttoenrolthiscourse','course', $courseProgramname).'" data-name = "'.$courseProgramname.'" class="enrollment-course" title="'.get_string('enrolment','learnercourse').'">'.get_string('enrolment','learnercourse').'</a>';
							}
						}
					}
				}else{
				
					if($USER->archetype != $CFG->userTypeAdmin){

					   if($courseCriteria == $CFG->courseCriteriaCompliance){ 
					    
						 $userDept = 0;
						 $users = array();
						 $usersIds = array();
					     if($courseCreatedBy){
							   $userDept = $DB->get_field_sql("select department from {$CFG->prefix}user where id = '".$courseCreatedBy."'");
							   if($userDept == 0){ // means superadmin created compliance course
									 if($USER->archetype == $CFG->userTypeStudent){
										 
									    if($CFG->allowExternalDepartment == 1 && $isBelongToED ){
									 
											if($CFG->CanExternalLearnerRequest == 1){
											   $enrolLink = '<a href="#" class="assign-course_available requestCourse" program-name = "'.$courseProgramname.'" rel = "'.$programOrCourseArray->cid.'" title="'.get_string('requestcourse','course').'">'.get_string('requestcourse','course').'</a>';
											 }else{
												  if($courseTypeId == $CFG->courseTypeOnline){
													 continue;
												  }
											 } 
											   
										}else{
										   $enrolLink = '<a href="javascript:;" data-url="'.$programOrCourseArray->cid.'" data-msg="'.get_string('areyousureyouwanttoenrolthiscourse','course', $courseProgramname).'" data-name = "'.$courseProgramname.'" class="enrollment-course" title="'.get_string('enrolment','learnercourse').'">'.get_string('enrolment','learnercourse').'</a>';
										}	   
										
									 }elseif($USER->archetype == $CFG->userTypeManager){
									   if($isBelongToED){
									      $enrolLink = '<a href="#" class="assign-course_available requestCourse" program-name = "'.$courseProgramname.'" rel = "'.$programOrCourseArray->cid.'" title="'.get_string('requestcourse','course').'">'.get_string('requestcourse','course').'</a>';
									   }else{
									     $enrolLink = '<a href="javascript:;" class="already_enrolled" title = "'.get_string('enrolled').'"></a>';
									   }
									 }
							   }else{ // means manager created compliance course
							   
									 $users = get_users_listing('name', 'asc', 1, 0, '', '', '','u.department="'.$userDept.'"', $paramArray=array(), $context);
									 if(count($users) > 0 ){
									   $usersIds = array_keys($users); // getting users of manager who created a compliance course
									 }
								
									 if(in_array($USER->id, $usersIds)){
									 
										  $enrolLink = '<a href="javascript:;" data-url="'.$programOrCourseArray->cid.'" data-msg="'.get_string('areyousureyouwanttoenrolthiscourse','course', $courseProgramname).'" data-name = "'.$courseProgramname.'" class="enrollment-course" title="'.get_string('enrolment','learnercourse').'">'.get_string('enrolment','learnercourse').'</a>';
									 }else{
									 
									    if($USER->archetype == $CFG->userTypeStudent){
										
											if($CFG->allowExternalDepartment == 1 && $isBelongToED ){
										 
												if($userDept != $USER->department && $courseTypeId == $CFG->courseTypeOnline){
												   continue;
												}
												
											}else{	
											  $enrolLink = '<a href="javascript:;" data-url="'.$programOrCourseArray->cid.'" data-msg="'.get_string('areyousureyouwanttoenrolthiscourse','course', $courseProgramname).'" data-name = "'.$courseProgramname.'" class="enrollment-course" title="'.get_string('enrolment','learnercourse').'">'.get_string('enrolment','learnercourse').'</a>';
											  
											}
											
									    }else{
									       $enrolLink = '<a href="#" class="assign-course_available requestCourse" program-name = "'.$courseProgramname.'" rel = "'.$programOrCourseArray->cid.'" title="'.get_string('requestcourse','course').'">'.get_string('requestcourse','course').'</a>';
										}
									 }
								}
						 }
						
					   }else{ 
					   
					
					      // its means course is elective, so here we will check whether it is global or not
						  if($courseGlobalElective == $CFG->courseElectiveGlobal){ 
								 $userDept = 0;
								 $users = array();
								 $usersIds = array();
								 if($courseCreatedBy){
									   $userDept = $DB->get_field_sql("select department from {$CFG->prefix}user where id = '".$courseCreatedBy."'");
									   if($userDept == 0){ // means superadmin created an elective global course
											 if($USER->archetype == $CFG->userTypeStudent){ 
											 
											   if(isset($USER->original_archetype) && $USER->original_archetype == $CFG->userTypeManager){
											     //$enrolLink = '<a href="javascript:;" data-url="'.$programOrCourseArray->cid.'" data-msg="'.get_string('areyousureyouwanttoenrolthiscourse','course', $courseProgramname).'" data-name = "'.$courseProgramname.'" class="enrollment-course" title="'.get_string('enrolment','learnercourse').'">'.get_string('enrolment','learnercourse').'</a>';
												  $enrolLink = '<a href="#" class="assign-course_available requestCourse" program-name = "'.$courseProgramname.'" rel = "'.$programOrCourseArray->cid.'" title="'.get_string('requestcourse','course').'">'.get_string('requestcourse','course').'</a>';
											   }else{
											      $enrolLink = '<a href="#" class="assign-course_available requestCourse" program-name = "'.$courseProgramname.'" rel = "'.$programOrCourseArray->cid.'" title="'.get_string('requestcourse','course').'">'.get_string('requestcourse','course').'</a>';
											   }
											   
											 }elseif($USER->archetype == $CFG->userTypeManager){
											 
											   if($isBelongToED){
												  $enrolLink = '<a href="#" class="assign-course_available requestCourse" program-name = "'.$courseProgramname.'" rel = "'.$programOrCourseArray->cid.'" title="'.get_string('requestcourse','course').'">'.get_string('requestcourse','course').'</a>';
											   }else{
											     $enrolLink = '<a href="javascript:;" class="already_enrolled" title = "'.get_string('enrolled').'"></a>';
											   }
											 }
									   }else{ // means manager created an elective global cours
											 $users = get_users_listing('name', 'asc', 1, 0, '', '', '','u.department="'.$userDept.'"', $paramArray=array(), $context);
											 if(count($users) > 0 ){
											   $usersIds = array_keys($users); // getting users of manager who created an elective global cours
											 }
										
											 if(in_array($USER->id, $usersIds)){
											 
												  $enrolLink = '<a href="javascript:;" data-url="'.$programOrCourseArray->cid.'" data-msg="'.get_string('areyousureyouwanttoenrolthiscourse','course', $courseProgramname).'" data-name = "'.$courseProgramname.'" class="enrollment-course" title="'.get_string('enrolment','learnercourse').'">'.get_string('enrolment','learnercourse').'</a>';
											 }else{
											 
												if($USER->archetype == $CFG->userTypeStudent){
												  $enrolLink = '<a href="#" class="assign-course_available requestCourse" program-name = "'.$courseProgramname.'" rel = "'.$programOrCourseArray->cid.'" title="'.get_string('requestcourse','course').'">'.get_string('requestcourse','course').'</a>';
												}else{
												   $enrolLink = '<a href="#" class="assign-course_available requestCourse" program-name = "'.$courseProgramname.'" rel = "'.$programOrCourseArray->cid.'" title="'.get_string('requestcourse','course').'">'.get_string('requestcourse','course').'</a>';
												}
											 }
										}
								 }
								
							   
						  }else{
						  
						     if($USER->archetype == $CFG->userTypeStudent && $CFG->allowExternalDepartment == 1 && $isBelongToED ){
							 
							    if($CFG->CanExternalLearnerRequest == 1){
								   $enrolLink = '<a href="#" class="assign-course_available requestCourse" program-name = "'.$courseProgramname.'" rel = "'.$programOrCourseArray->cid.'" title="'.get_string('requestcourse','course').'">'.get_string('requestcourse','course').'</a>';
								}else{
								  if($courseTypeId == $CFG->courseTypeOnline){
								     continue;
								  }
								}
							 }else{
						       $enrolLink = '<a href="#" class="assign-course_available requestCourse" program-name = "'.$courseProgramname.'" rel = "'.$programOrCourseArray->cid.'" title="'.get_string('requestcourse','course').'">'.get_string('requestcourse','course').'</a>';
							 }
						  }
					   }
					}
				}
				if($courseTypeId == $CFG->courseTypeClassroom && ($USER->archetype == $CFG->userTypeStudent || $USER->archetype == $CFG->userTypeManager)){
					$htmlDiv .= '<div class="pull-left right-icon cbox" rel="'.$ky.'" id="plus-minus-icon-'.$ky.'"><a href="javascript:void(0)" class = "getclassroomcontent" rel = "'.$ky.'"></a><img alt="" src="'.$CFG->wwwroot.'/theme/image.php?theme=gourmet&component=core&image=a%2Fadd_file"></div>';	
				}else{
					$htmlDiv .= '<div class="pull-left cbox"><div class="catalog_course_button">'.$enrolLink.'</div></div>';
				}

			}else{
				if(array_key_exists ($ky,$departmentPrograms)){
					if($USER->archetype != $CFG->userTypeStudent){
						$enrolLink = '<a href="#" class="already_enrolled" title = "'.get_string('enrolled').'"></a>';
					}else{
						if($USER->archetype != $CFG->userTypeAdmin){
							if($showRequest == 1 && $USER->original_archetype != $CFG->userTypeManager){
								$enrolLink = '<a href="#" class="requestforextention" element = "'.$element.'" element-name = "'.$courseProgramname.'" rel = "'.$elementId.'" title="'.get_string('requestforextention').'">'.get_string('requestforextention').'</a>';
								//$enrolLink = "Expired";
							}else{
								$enrolLink = '<a href="javascript:;" data-url="'.$programOrCourseArray->pid.'" data-msg="'.get_string('areyousureyouwanttoenrolthisprogram','course', $courseProgramname).'" data-name = "'.$courseProgramname.'" class="enrollment-program" title="'.get_string('enrolment','learnercourse').'">'.get_string('enrolment','learnercourse').'</a>';
							}
						}
					}
				}else{
					if($USER->archetype != $CFG->userTypeAdmin){
						$enrolLink = '<a href="#" class="assign-course_available requestProgram" rel = "'.$programOrCourseArray->pid.'"  program-name = "'.$courseProgramname.'" title="'.get_string('requestprogram','course').'">'.get_string('requestprogram','course').'</a>';
					}
				}
				$htmlDiv .= '<div class="pull-left cbox"><div class="catalog_course_button">'.$enrolLink.'</div></div>';

				$htmlDiv .= '<div class="pull-left right-icon cbox" rel="'.$ky.'" id="plus-minus-icon-'.$ky.'"><a href="javascript:void(0)"  data-class-type="'.$classType.'"  class = "getprogramcourse" rel = "'.$ky.'"></a><img alt="" src="'.$CFG->wwwroot.'/theme/image.php?theme=gourmet&component=core&image=a%2Fadd_file"></div>';	
			}
			$htmlDiv .= '</div>';
			$html .= $htmlDiv;
		}
	}else{
	    $html .= '<p style="margin-top: 20px;">'.get_string('no_results').'</p>';
	}
	  
  $html .= html_writer::end_tag('div');
  $html .= paging_bar($programCoursesCount, $page, $perpage, $genURL); 
  $html .= $outerDivEnd;
echo $html;
?>

<script>
$(document).ready(function(){
	$(document).on('click','#search-form .filter input',function(){
		$('#searchForm').submit();
	});
	$(".getprogramcourse").click(function(){
		var programId = $(this).attr('rel');
		var dataClassType = $(this).attr('data-class-type');	
		var keyId = programId;
		if($("#dbox"+keyId).attr('class') == '' || $("#dbox"+keyId).attr('class') == 'undefined' || $("#dbox"+keyId).attr('class') == undefined){
			$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
				type:'POST',
				data:'action=getProgramCourse&programId='+programId+"&getAsset=0&dataClassType="+dataClassType,
				success:function(data){
						$("#"+keyId).after(data);
						$("#dbox"+keyId).show();
				}
			});
		}else{
			$("#dbox"+keyId).toggle();
		}
		 var courseId = keyId;
		 $('.no-course').removeClass('selected');
		 $('.cbox').not('[rel="'+courseId+'"]').removeClass('minus');
		 var hasClass = $('#plus-minus-icon-'+courseId).hasClass('minus');
		 if(hasClass){
		   $('#plus-minus-icon-'+courseId).removeClass('minus');
		 }else{
		   $('#plus-minus-icon-'+courseId).addClass('minus');
		 }
		 
		 $('.d-box').not('#dbox'+courseId).hide();
		 
		 $(this).addClass('selected');	   
	});

	$(".getclassroomcontent").click(function(){
		var classroomId = $(this).attr('rel');	
		var keyId = classroomId;
		if($("#dbox"+keyId).attr('class') == '' || $("#dbox"+keyId).attr('class') == 'undefined' || $("#dbox"+keyId).attr('class') == undefined){
			$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
				type:'POST',
				data:'action=getClassroomContent&classroomId='+classroomId+"&getAsset=2",
				success:function(data){
						$("#"+keyId).after(data);
						$("#dbox"+keyId).show();
				}
			});
		}else{
			$("#dbox"+keyId).toggle();
		}
		 $('.no-course').removeClass('selected');
		 $('.cbox').not('[rel="'+keyId+'"]').removeClass('minus');
		 var hasClass = $('#plus-minus-icon-'+keyId).hasClass('minus');
		 if(hasClass){
		   $('#plus-minus-icon-'+keyId).removeClass('minus');
		 }else{
		   $('#plus-minus-icon-'+keyId).addClass('minus');
		 }
		 
		 $('.d-box').not('#dbox'+keyId).hide();
		 
		 $(this).addClass('selected');	   
	});

	$(".requestCourse").click(function(){
		var courseId = $(this).attr('rel');
		message = "<?php echo get_string('sentassigncourserequest', 'course')?>";
		if(confirm(message)){
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
					type:'POST',
					data:'action=checkCourseProgramRequest&courseId='+courseId+"&programId=0",
					success:function(data){
						if(data == 1){
							var txt = "<?php echo get_string('alreadyrequested');?>";
							var response = confirm(txt);
							if (response == true) {
								$.ajax({
									url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
									type:'POST',
									data:'action=RequestForCourse&courseId='+courseId+"&programId=0",
									success:function(data){
											window.location.reload();
											//alert("<?php echo $_SESSION['update_msg'];?>");
									}
								});
							}
						}else{
							if(data == 2){
								var txt = "<?php echo get_string('requestdeclined');?>";
								var response = confirm(txt);
								if (response == true) {
									$.ajax({
										url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
										type:'POST',
										data:'action=RequestForCourse&courseId='+courseId+"&programId=0",
										success:function(data){
											window.location.reload();
												//alert("<?php echo $_SESSION['update_msg'];?>");
										}
									});
								}
							}else{
								$.ajax({
									url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
									type:'POST',
									data:'action=RequestForCourse&courseId='+courseId+"&programId=0",
									success:function(data){
										window.location.reload();
											//alert("<?php echo $_SESSION['update_msg'];?>");
									}
								});
							}
						}
					}
			});
		}
	});
	
	$(".requestProgram").click(function(){
		var programId = $(this).attr('rel');
		message = "<?php echo get_string('sentassigncourserequest', 'course')?>";
		if(confirm(message)){
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
					type:'POST',
					data:'action=checkCourseProgramRequest&courseId=0&programId='+programId,
					success:function(data){
						if(data == 1){
							var txt = "<?php echo get_string('alreadyrequested');?>";
							if (confirm(txt)) {
								$.ajax({
									url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
									type:'POST',
									data:'action=RequestForCourse&courseId=0&programId='+programId,
									success:function(data){
										window.location.reload();
											//alert("<?php echo $_SESSION['update_msg'];?>");
									}
								});
							}
						}else{
							if(data == 2){
								var txt = "<?php echo get_string('alreadyrequested');?>";
								//var response = confirm(txt);
								if (confirm(txt)) {
									$.ajax({
										url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
										type:'POST',
										data:'action=RequestForCourse&courseId=0&programId='+programId,
										success:function(data){
											window.location.reload();
												//alert("<?php echo $_SESSION['update_msg'];?>");
										}
									});
								}
							}else{
								$.ajax({
									url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
									type:'POST',
									data:'action=RequestForCourse&courseId=0&programId='+programId,
									success:function(data){
										window.location.reload();
											//alert("<?php echo $_SESSION['update_msg'];?>");
									}
								});
							}
						}
					}
			});
		}
	});
	$(document).on('click',".request-for-class, .enroll-in-class",function(){
		var classId = $(this).attr('relclass');
		var userId = $(this).attr('reluser');
		if(classId != 'undefined' && classId != 'Undefined' && classId != undefined && userId != 'undefined' && userId != 'Undefined' && userId != undefined){
			var dataMsg = " Do you want to request for this class?";
			 if(confirm(dataMsg)){
				$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
					type:'POST',
					data:'action=enrolUserToClassroom&userId='+userId+'&classId='+classId,
					success:function(data){
						window.location.reload();
					}
				});
		  }
		}
	});
	$('.enrollment-program').click(function(){
		 var dataMsg = $(this).attr('data-msg');
		 var enrolId = $(this).attr('data-url');
		 var programName = $(this).attr('data-name');
		 if(enrolId){
		  if(confirm(dataMsg)){
				$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
					type:'POST',
					data:'action=enrolUserToProgram&programId='+enrolId,
					success:function(data){
						alert("<?php echo get_string('programhasbeenenroled', 'learnercourse');?>");
						window.location.reload();
					}
				});
		  }
		 }
	});
	$('.enrollment-course').click(function(){
		var dataMsg = $(this).attr('data-msg');
		var enrolId = $(this).attr('data-url');
		if(enrolId){  
		  if(confirm(dataMsg)){
			 $.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
					type:'POST',
					data:'action=enrolUserToCourse&courseId='+enrolId,
					success:function(data){
						alert("<?php echo get_string('coursehasbeenenroled', 'learnercourse');?>");
						window.location.reload();
					}
			});
		  }
		}
	});
	$(document).on('click',".enrol_user",function(){
		var txt = "Do you want to accept this request?";
		var response = confirm(txt);
		var userIds = "<?php echo $USER->id; ?>";
		if (response == true) {
				var enrolid = $(this).attr('relclass');
				var enrolUser = 1;
				$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
					type:'POST',
					data:"action=checkSeatEnrol&enrolid="+enrolid+"&enrolUser="+enrolUser,
					success:function(data){
							if(data == 'success'){
								$.ajax({
									url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
									type:'POST',
									data:"action=enrolUserInClassroom&userIds="+userIds+"&enrolid="+enrolid+"&enrolUser="+enrolUser,
									success:function(data){
											if(data == 'success'){
												window.location.reload();
											}else{
												alert("unable to process request. Please try again later.");
											}
									}
								});
							}else{
								if(data == 'error'){
									alert("unable to process request. Please try again later.");
								}else{
									var msg = '<?php echo get_string("errornoofinvite","scheduler","'+data+'");?>';
									alert(msg);
								}
							}
					}
				});
		}
	});

	$(".requestforextention").click(function(){
			var courseId = $(this).attr('rel');
			var programId = 0;
			var element = $(this).attr('element');
			var elementName = $(this).attr('element-name');
			if(element == 'course'){
				courseId = courseId;
				programId = 0;
			}else{
				if(element == 'program'){
					programId = courseId;	
					courseId = 0;
				}
			}
			message = "<?php echo get_string('request_for_extension')?>'"+elementName+"' ?";
			if(confirm(message)){
				$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
					type:'POST',
					data:'action=checkCourseProgramRequest&extension=1&courseId='+courseId+"&programId="+programId,
					success:function(data){
						if(data == 1){
							var txt = "<?php echo get_string('alreadyrequested');?>";
							var response = confirm(txt);
							if (response == true) {
								$.ajax({
									url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
									type:'POST',
									data:'action=RequestForCourse&extension=1&courseId='+courseId+"&programId="+programId,
									success:function(data){
											window.location.reload();
									}
								});
							}
						}else{
							if(data == 2){
								var txt = "<?php echo get_string('requestdeclined');?>";
								var response = confirm(txt);
								if (response == true) {
									$.ajax({
										url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
										type:'POST',
										data:'action=RequestForCourse&extension=1&courseId='+courseId+"&programId="+programId,
										success:function(data){
											window.location.reload();
										}
									});
								}
							}else{
								$.ajax({
									url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
									type:'POST',
									data:'action=RequestForCourse&extension=1&courseId='+courseId+"&programId="+programId,
									success:function(data){
										window.location.reload();
									}
								});
							}
						}
					}
				});
			}
		});
});
</script>
<?php
echo $OUTPUT->footer();
