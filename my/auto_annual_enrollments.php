<?php
	require_once(dirname(__FILE__) . '/../config.php'); 
	GLOBAL $CFG;
        
        $PAGE->set_url('/my/auto_annual_enrollments.php', array('id' => $id));
        $PAGE->set_pagelayout('classroompopup');

        echo $OUTPUT->header();
       // pr($CFG);
        
        ?>
<div class="borderBlockSpace">

<span id="loading_text">Please wait...</span></div>

<?php
    echo $OUTPUT->footer();
?>

<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script>
    
    $(document).ready(function(){
        
            $(function(){
                   var message = "Are you sure want to enroll previous version course(s) users into new version course(s).";
                    
                    if(confirm(message)){
                       
                        
                         //$('#loading').html('Syncing...');
                         
                        $.ajax({
                            url:'<?php echo $CFG->wwwroot;?>/cron/auto_annual_course_enrollment.php',
                            type:'POST',                          
                            data:'',                            
                            success:function(data){
                               console.log("Hello");
                                $('#loading_text').html('Process has been completed.');
                            }                 
    
                            
                        });
                    }
                
            });
        
    });
</script>
	