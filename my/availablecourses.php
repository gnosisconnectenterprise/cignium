<?php
	require_once(dirname(__FILE__) . '/../config.php'); 
	GLOBAL $CFG;
	require_once($CFG->dirroot . '/my/learninglib.php');
	global $USER;
	$userId = $USER->id;
	
	if($USER->archetype == $CFG->userTypeAdmin){
	   redirect($CFG->wwwroot .'/');
	}	

    require_login();

    $type         = optional_param('type', 1, PARAM_ALPHANUM);
    $page         = optional_param('page', 1, PARAM_INT);
    $perpage      = optional_param('perpage', 10, PARAM_INT);
	$courseEnrolId = optional_param('cid', '0', PARAM_INT);
	$programEnrolId = optional_param('pid', '0', PARAM_INT);
	$chkEnrolFlag = optional_param('enrolflag', '0', PARAM_INT);
	if($chkEnrolFlag == 1) {
		$toUser = new stdClass();
		$fromUser = new stdClass();
		if($programEnrolId){
			////// Getting Program Detail /////
			$programDetails = $DB->get_record_sql("SELECT id,name FROM mdl_programs WHERE id = ".$programEnrolId);
			if(count($programDetails) > 0){
				
				$userCreatorId = $USER->parent_id;
				$fromUser->email = $USER->email;
				$fromUser->firstname = $USER->firstname;
				$fromUser->lastname = $USER->lastname;
				$fromUser->maildisplay = true;
				$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
				$fromUser->id = $USER->id;
				$fromUser->firstnamephonetic = '';
				$fromUser->lastnamephonetic = '';
				$fromUser->middlename = '';
				$fromUser->alternatename = '';
		
				////// Getting To User /////
				$toUserQuery = "SELECT id, firstname, lastname, email, createdby FROM {$CFG->prefix}user WHERE id = '".$userCreatorId."'";

				$toUserDetails = $DB->get_record_sql($toUserQuery);
				
				$toUser->email = $toUserDetails->email;
				$toUser->firstname = $toUserDetails->firstname;
				$toUser->lastname = $toUserDetails->lastname;
				$toUser->maildisplay = true;
				$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
				$toUser->id = $userCreatorId;
				$toUser->firstnamephonetic = '';
				$toUser->lastnamephonetic = '';
				$toUser->middlename = '';
				$toUser->alternatename = '';
				$subject = 'Request for Assign Program ('.(($programDetails->id != '') ? $programDetails->id.' - ' : '').$programDetails->name.') to '.$USER->firstname.' '.$USER->lastname;
		
				$messageText = 'Dear '.$toUserDetails->firstname.','."\n\n".'Please assign the program ('.(($programDetails->id != '') ? $programDetails->id.' - ' : '').$programDetails->name.') to '.$USER->firstname.' '.$USER->lastname." (".$USER->username.")\n\n".'From,'."\n".$SITE->fullname;
				email_to_user($toUser, $fromUser, $subject, $messageText, $messageHtml, "", "", true);
		
				$_SESSION['update_msg'] = get_string('emailsentsuccess', 'course');
			} else {		
				$_SESSION['update_msg'] = get_string('emailsenterror', 'course');
			}			
		}elseif($courseEnrolId){
			////// Getting Course Detail /////
			$courseQuery = "SELECT id, fullname, idnumber FROM {$CFG->prefix}course WHERE id = '".$courseEnrolId."'";
			$courseDetails = $DB->get_record_sql($courseQuery);
			if(!empty($courseDetails)){
				$userCreatorId = $USER->parent_id;
				$fromUser->email = $USER->email;
				$fromUser->firstname = $USER->firstname;
				$fromUser->lastname = $USER->lastname;
				$fromUser->maildisplay = true;
				$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
				$fromUser->id = $USER->id;
				$fromUser->firstnamephonetic = '';
				$fromUser->lastnamephonetic = '';
				$fromUser->middlename = '';
				$fromUser->alternatename = '';

				////// Getting To User /////
				$toUserQuery = "SELECT id, firstname, lastname, email, createdby FROM {$CFG->prefix}user WHERE id = '".$userCreatorId."'";
				$toUserDetails = $DB->get_record_sql($toUserQuery);
				
				$toUser->email = $toUserDetails->email;
				$toUser->firstname = $toUserDetails->firstname;
				$toUser->lastname = $toUserDetails->lastname;
				$toUser->maildisplay = true;
				$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
				$toUser->id = $userCreatorId;
				$toUser->firstnamephonetic = '';
				$toUser->lastnamephonetic = '';
				$toUser->middlename = '';
				$toUser->alternatename = '';

				$subject = 'Request for Assign Course ('.(($courseDetails->idnumber != '') ? $courseDetails->idnumber.' - ' : '').$courseDetails->fullname.') to '.$USER->firstname.' '.$USER->lastname;

				$messageText = 'Dear '.$toUserDetails->firstname.','."\n\n".'Please assign the course ('.(($courseDetails->idnumber != '') ? $courseDetails->idnumber.' - ' : '').$courseDetails->fullname.') to '.$USER->firstname.' '.$USER->lastname." (".$USER->username.")\n\n".'From,'."\n".$SITE->fullname;

				email_to_user($toUser, $fromUser, $subject, $messageText, $messageHtml, "", "", true);

				$_SESSION['update_msg'] = get_string('emailsentsuccess', 'course');
			} else {
				$_SESSION['update_msg'] = get_string('emailsenterror', 'course');
			}
		}
		redirect($CFG->wwwroot."/my/availablecourses.php");
	}
	$paramArray = array(
	'type' => $type
    );
  
	
	$removeKeyArray = array('perpage');
	
	$pageURL = $_SERVER['PHP_SELF'];
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);

	/************************* Header Information start ***************************************/

    $header = $SITE->fullname.": ".get_string('mylearningpage', 'learnercourse');
    $PAGE->set_title($header);
	$PAGE->set_heading($header);
	$PAGE->set_pagelayout('mydashboard');
    $stredit   = get_string('edit');
    $strdelete = get_string('delete');
    $strdeletecheck = get_string('deletecheck');
    $strconfirm = get_string('confirm');
	$PAGE->navbar->add(get_string('mylearningpage','menubar'));
    echo $OUTPUT->header();
	
	/************************* Header Information start ***************************************/
	Global $DB,$CFG,$USER;
	$courseProgramList = userAvailableCourses();
	$programArray = array();
	foreach($courseProgramList as $assetInfo){
		if(array_key_exists($assetInfo->id,$programArray)){
			if(array_key_exists($assetInfo->courseid,$programArray[$assetInfo->id])){
				$programArray[$assetInfo->id][$assetInfo->courseid] = $assetInfo;
			}else{
				$programArray[$assetInfo->id][$assetInfo->courseid] = array();
				$programArray[$assetInfo->id][$assetInfo->courseid] = $assetInfo;
			}

		}else{
			$programArray[$assetInfo->id] = array();
			$programArray[$assetInfo->id][$assetInfo->courseid] = $assetInfo;
		}
	}
    $styTabs1 = '';
	$styTabs2  = '';
	$styTabs3 = '';
	$styTabs3 = "current";
	 
	$courseTabs = '';
	$html = '';
	$outerDivStart = "<div class='tabsOuter'>";
	$outerDivEnd = "</div>";
	
		$courseTabs .= "<div class='tabLinks'>";
			$courseTabs .= "<div><a href='".$CFG->wwwroot."/my/learningpage.php' title='".get_string('todo','learnercourse')."' class='$styTabs1'>".get_string('todo','learnercourse')."<span></span></a></div>";
			$courseTabs .= "<div><a href='".$CFG->wwwroot."/my/learningpage.php?type=2' title='".get_string('completed','learnercourse')."' class='$styTabs2' >".get_string('completed','learnercourse')."<span></span></a></div>";
			$courseTabs .= "<div><a href='".$CFG->wwwroot."/my/availablecourses.php' title='".get_string('available','learnercourse')."' class='$styTabs3'>".get_string('available','learnercourse')."<span></span></a></div>";
		$courseTabs .= "</div>";

	$html = $outerDivStart.$courseTabs;
	if($_SESSION['update_msg'] != ''){
		$html .= '<div class="clear"></div>';
		$html .= '<div class="update_msg">'.$_SESSION['update_msg'].'</div>';
		$html .= '<div class="clear"></div>';
		$_SESSION['update_msg']='';
	}
	$htmlDiv = '';
	$parsedArray = array();
	$html .= html_writer::start_tag('div', array('class'=>'no-overflow scroll-pane','style'=>"height: 800px;"));
	if (!empty($courseProgramList)) {
		$i= 0;
		$k = 0;
		$j = 0;
		$data = new stdClass();
		$dataCourse = new stdClass();
		foreach ($courseProgramList as $key=>$programOrCourseArray) {
			if(in_array($programOrCourseArray->id,$parsedArray)){
				continue;
			}else{
				array_push($parsedArray,$programOrCourseArray->id);
			}
			$i++;
			$courseDiv = '';
			$classAlt = ($k+$j)%2==0?'even-course':'odd-course';
			if($programOrCourseArray->id != 0){
					$data->id=$programOrCourseArray->id;
					$data->name = $programOrCourseArray->name;
					
					$imagePath = '<div class = "course-img" >'.getProgramImage($data).'</div>';
					if(!empty($programArray[$programOrCourseArray->id])){
						$j = 0;
						$courseDiv = '<div class="d-box" id="dbox'.$programOrCourseArray->difid.'">
										<div class="section-box">
											<div class="c-box" id="cbox8">';
						foreach($programArray[$programOrCourseArray->id] as $assetArr){
							$j++;
							$courseImagePath = '<div class = "course-img" >'.getCourseImage(array('id'=>$assetArr->courseid,'fullname'=>$assetArr->fullname)).'</div>';
							$classAlt2 = $j%2==0?'even-course':'odd-course';
							//$headingText1 = get_string('course','learnercourse').$j.': '.$assetArr->fullname;
							$headingText1 = $assetArr->fullname;

							$summary1 = $assetArr->description;

							$learningObj1 = $assetArr->objective;

							$performanceOut1 = $assetArr->outcomes;

							//$descriptionText1 = get_string('description','learnercourse').': '.$summary1;
							$descriptionText1 = $summary1?'<strong>'.get_string('description','learnercourse').'</strong>: '.$summary1:'';
							
							$pcLearningObj1 = $learningObj1?('<br /><strong>'.get_string('learningobj','course').': </strong> '.$learningObj1):'';
							$performanceOut1 = $performanceOut1?('<br /><strong> '.get_string('performanceOut','course').': </strong>'.$performanceOut1):'';
							$courseDiv .= '<div class="no-course '.$classAlt2.'" rel="'.$programOrCourseArray->difid.'">';
								//$courseDiv .= '<div class="pull-left left-icon"><span></span><img alt="" src="'.$CFG->wwwroot.'/theme/image.php?theme=gourmet&component=core&image=a%2Fview_icon_active"></div>';
								$courseDiv .= $courseImagePath;
								$courseDiv .= '<div class="pull-left left-content">';
									$courseDiv .= '<div class="c-heading">'.$headingText1.'</div>';
									$courseDiv .= '<div class="c-text">'.$descriptionText1.$pcLearningObj1.$performanceOut1.'</div>';
								$courseDiv .= '</div>';	
							$courseDiv .= '</div>';
						}
						$courseDiv.="</div></div></div>";
					}
					$htmlDiv = '';
					//$headingText = get_string('program','learnercourse').$i.': '.$programOrCourseArray->name;
					$headingText = $programOrCourseArray->name;
					$summary = $programOrCourseArray->program_description;
					$learningObj = $assetArr->objective;

					$performanceOut = $assetArr->outcomes;
					$pcLearningObj = $learningObj?('<br /><strong>'.get_string('learningobj','course').': </strong> '.$learningObj):'';
					$performanceOut = $performanceOut?('<br /><strong>'.get_string('performanceOut','course').': </strong> '.$performanceOut):'';
					$descriptionText = $summary?'<strong>'.get_string('description','learnercourse').'</strong>: '.$summary:'';
					$descriptionText .= $pcLearningObj.$performanceOut;
					if($programOrCourseArray->assignid == '' || $programOrCourseArray->assignid == null){
						$enrolLink = '<a href="'.$CFG->wwwroot.'/my/availablecourses.php?enrolflag=1&pid='.$programOrCourseArray->id.'" class="assign-course assign-program" program-name = "'.$programOrCourseArray->name.'" title="'.get_string('requestprogram','course').'">'.get_string('requestprogram','course').'</a>';
					}else{
						$enrolLink = '<a href="javascript:;" data-url="'.$programOrCourseArray->id.'" data-msg="'.get_string('areyousureyouwanttoenrolthisprogram','course', $programOrCourseArray->name).'" data-name = "'.$programOrCourseArray->name.'" class="enrollment-program" title="'.get_string('enrolment','learnercourse').'">'.get_string('enrolment','learnercourse').'</a>';
					}

					$htmlDiv = '<div class="no-course '.$classAlt.'" rel="'.$programOrCourseArray->difid.'">';
						//$htmlDiv .= '<div class="pull-left left-icon"><span></span><img alt="" src="'.$CFG->wwwroot.'/theme/image.php?theme=gourmet&component=core&image=a%2Fview_icon_active"></div>';
						$htmlDiv .= $imagePath;
						$htmlDiv .= '<div class="pull-left left-content" style="width:80%">';
							$htmlDiv .= '<div class="c-heading">'.$headingText.'</div>';
							$htmlDiv .= '<div class="c-text">'.$descriptionText.'</div>';
						$htmlDiv .= '</div>';
						$htmlDiv .= '<div class = "catalog_course_button">'.$enrolLink.'<div class="pull-left right-icon cbox" rel="'.$programOrCourseArray->difid.'" id="plus-minus-icon-'.$programOrCourseArray->difid.'"><a href="javascript:void(0)"></a><img alt="" src="'.$CFG->wwwroot.'/theme/image.php?theme=gourmet&component=core&image=a%2Fadd_file"></div></div>';
					$htmlDiv .= '</div>';
					$htmlDiv .= $courseDiv;
					$html .= $htmlDiv;
			}else{	
				if(!empty($programArray[$programOrCourseArray->id])){
					$k = 0;
					foreach($programArray[$programOrCourseArray->id] as $assetArr){
						$htmlDiv = '';
						$k++;
						$imagePath = '<div class = "course-img" >'.getCourseImage(array('id'=>$assetArr->courseid,'fullname'=>$assetArr->fullname)).'</div>';
						//$headingText = get_string('course','learnercourse').' '.$k.': '.$assetArr->fullname;
						$headingText = $assetArr->fullname;

						$summary = $assetArr->description;

						$learningObj = $assetArr->objective;

						$performanceOut = $assetArr->outcomes;

						$descriptionText = $summary?'<strong>'.get_string('description','learnercourse').'</strong>: '.$summary:'';
						$pcLearningObj = $learningObj?('<br /><strong>'.get_string('learningobj','course').': </strong> '.$learningObj):'';
						$performanceOut = $performanceOut?('<br /><strong>'.get_string('performanceOut','course').': </strong> '.$performanceOut):'';
						$htmlDiv = '<div class="no-course '.$classAlt.'" rel="'.$assetArr->difid.'">';
						if($assetArr->assignid == '' || $assetArr->assignid == null){
							$enrolLink = '<a href="'.$CFG->wwwroot.'/my/availablecourses.php?enrolflag=1&cid='.$assetArr->courseid.'" class="assign-course" program-name = "'.$assetArr->fullname.'" title="'.get_string('requestcourse','course').'">'.get_string('requestcourse','course').'</a>';
						}else{
							$enrolLink = '<a href="javascript:;" data-url="'.$assetArr->courseid.'" data-msg="'.get_string('areyousureyouwanttoenrolthiscourse','course', $assetArr->fullname).'" data-name = "'.$assetArr->fullname.'" class="enrollment-course" title="'.get_string('enrolment','learnercourse').'">'.get_string('enrolment','learnercourse').'</a>';
						}
						//$htmlDiv .= '<div class="pull-left left-icon"><span></span><img alt="" src="'.$CFG->wwwroot.'/theme/image.php?theme=gourmet&component=core&image=a%2Fview_icon_active"></div>';
						$htmlDiv .= $imagePath;
							$htmlDiv .= '<div class="pull-left left-content" style="width:84%">';
								$htmlDiv .= '<div class="c-heading">'.$headingText.'</div>';
								$htmlDiv .= '<div class="c-text">'.$descriptionText.$pcLearningObj.$performanceOut.'</div>';
							$htmlDiv .= '</div>';
							$htmlDiv .= '<div class="pull-left cbox"><div class="catalog_course_button">'.$enrolLink.'</div></div>';	
						$htmlDiv .= '</div>';
						$html .= $htmlDiv;
					}
				}else{
				}
			}
		}
	}else{
	    $html .= $OUTPUT->heading(get_string('no_results'));
	}
	  
  $html .= html_writer::end_tag('div');
  //$html .= paging_bar($courseProgramCount, $page, $perpage, $genURL); 
  $html .= $outerDivEnd;
echo $html;	
 ?>
 
 <script type="text/javascript">
 
 
 function setCourseLastAccess(userid, courseid, resourceid){
	
     
	   $.ajax({
	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=setCourseLastAccess&userid='+userid+"&courseid="+courseid+"&resourceid="+resourceid,
		dataType:'json',
		success:function(data){
		  
		   var success = data.success;
		     var error = data.error;
			   if(success == 1){
			      var course_resource_id = data.response;
				  $('#coursestatus_'+course_resource_id).addClass('downloaded');
			   }else{
				 alert(error);
			   }
		}
	  
	  });
	  $.ajax({
	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=setCourseBadge&userid='+userid+"&courseid="+courseid+"&resourceid="+resourceid,
		dataType:'json',
		success:function(data){  
		}
	  
	  });
	     
	
 }
	
 function showHideButton(){
      $('a.moreless-toggler').addClass('moreless-toggler moreless-less');
	  $('a.moreless-toggler').html('<?php echo get_string('showless','form');?>');
	  
	  $('div.fitem_fselect').addClass('fitem advanced fitem_fselect show');
	  $('div.fitem_fgroup').addClass('fitem advanced fitem_fgroup show');
	   
 }
 
 
 /*$(window).load(function () {
     <?php if(isset($SESSION->learnercourse_filtering['categoryname']) || isset($SESSION->learnercourse_filtering['timecreated'])){ ?>
	      //  showHideButton();
	<?php } ?>
 });*/
 
 $(document).ready(function(){
 
      $('.no-course').not('.section-box .c-box .odd-course, .section-box .c-box .even-course').click(function(){
	         var courseId = $(this).attr('rel');
			 $('.no-course').removeClass('selected');
			 $('.cbox').not('[rel="'+courseId+'"]').removeClass('minus');
			 var hasClass = $('#plus-minus-icon-'+courseId).hasClass('minus');
			 if(hasClass){
			   $('#plus-minus-icon-'+courseId).removeClass('minus');
			 }else{
			   $('#plus-minus-icon-'+courseId).addClass('minus');
			 }
			 
			 $('.d-box').not('#dbox'+courseId).hide();
			 if($('#dbox'+courseId).is(':visible')){
			    $('#dbox'+courseId).hide();
			 }else{
			   $('#dbox'+courseId).show();
			 }
			 
			 $(this).addClass('selected');
	  
	  });
	  
	  $('.enrollment-program').click(function(){

		 var dataMsg = $(this).attr('data-msg');
		 var enrolId = $(this).attr('data-url');
		 var programName = $(this).attr('data-name');
	     if(enrolId){
		  if(confirm(dataMsg)){
				$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
					type:'POST',
					data:'action=enrolUserToProgram&programId='+enrolId,
					success:function(data){
						alert("<?php echo get_string('programhasbeenenroled', 'learnercourse');?>");
						window.location.reload();
					}
				});
		  }
		 }
		
	  });
	  
	  $('.enrollment-course').click(function(){
	  
	    var dataMsg = $(this).attr('data-msg');
		var enrolId = $(this).attr('data-url');
	    
		if(enrolId){  
		  if(confirm(dataMsg)){
		     $.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
					type:'POST',
					data:'action=enrolUserToCourse&courseId='+enrolId,
					success:function(data){
						alert("<?php echo get_string('coursehasbeenenroled', 'learnercourse');?>");
						window.location.reload();
					}
			});
		  }
		}
	  
	  });
	  
 });


 </script>
 <?php	

 echo $OUTPUT->footer();
 