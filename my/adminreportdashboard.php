<?php
/**
	* Custom module - Admin Report Dashboard
	* Date Creation - 01/07/2014
	* Date Modification : 01/07/2014
	* Last Modified By : Rajesh Kumar
*/
require_once(dirname(__FILE__) . '/../config.php');
global $CFG,$USER;
require_once($CFG->dirroot . '/my/lib.php');
require_once($CFG->dirroot.'/mod/scorm/locallib.php');
require_once($CFG->dirroot.'/course/lib.php');

$userId = $USER->id;
//$userrole =  getUserRole($USER->id);
if($USER->archetype != $CFG->userTypeAdmin){  // added by rajesh 
   redirect($CFG->wwwroot .'/');
}

require_login();

$strmymoodle = get_string('myhome');
$header = "$SITE->shortname: $strmymoodle";
$context = get_context_instance(CONTEXT_SYSTEM);
$params = array();
$PAGE->set_context($context);
$PAGE->set_url('/my/'.$CFG->pageDashboard, $params);
$PAGE->set_pagelayout('adminreportdashboard');
$PAGE->set_pagetype('my-index');
$PAGE->blocks->add_region('content');
$PAGE->set_subpage($currentpage->id);
$PAGE->set_title($header);
$PAGE->set_heading($header);
$PAGE->navbar->add(get_string('reports',"adminreportdashboard"));
echo $OUTPUT->header();

$boxArr = array(get_string('coursereports',"adminreportdashboard")=>$CFG->wwwroot."/course/multicoursereport.php", get_string('userreports',"adminreportdashboard")=>$CFG->wwwroot."/user/multiuser_report.php");
$boximage = $CFG->wwwroot."/theme/gourmet/pix/images/course_img.png";

$sStartDate     = date("m-d-Y", mktime(0, 0, 0, 01, 01, (date('Y'))));
$sEndDate       = date('m-d-Y', strtotime("+1 years"));


list($month1, $day1, $year1) = explode('-', $sStartDate);
$sDateSelected = date("m/d/Y",mktime(0, 0, 0, $month1, $day1, $year1));
		   
list($month2, $day2, $year2) = explode('-', $sEndDate);
$eDateSelected = date("m/d/Y",mktime(0, 0, 0, $month2, $day2, $year2));

// search condition start here

$searchString = "";


if($sDateSelected){ 
	list($month, $day, $year) = explode('/', $sDateSelected);
	$sStartDateTime =  mktime(0, 0, 0, $month, $day, $year);
	$searchString .= " AND mc.startdate >= ($sStartDateTime) ";
}

if($eDateSelected){
   list($month, $day, $year) = explode('/', $eDateSelected);
   $sEndDateTime = mktime(0, 0, 0, $month, $day, $year);
   $searchString .= " AND mc.enddate <= ($sEndDateTime) ";
}
			
			
?>

<div role="main"><span id="maincontent"></span>
  

  <div class = "admin-dashboard content-upper" >
    
    <div class = "last-accessed-courses-report">
               <?php 
		   
		                   //$queryCount = "select count(mu.id) AS usercount FROM {$CFG->prefix}user mu WHERE mu.deleted = '0' $searchString";	
							$queryAll = "select mu.*, concat(mu.firstname,' ', mu.lastname) as fullname, if(mgm.groupid, GROUP_CONCAT(DISTINCT mg.name ORDER BY mg.name ASC),'---') as teamtitle, if(md.id, md.title,'---') as departmenttitle  FROM {$CFG->prefix}user mu LEFT JOIN {$CFG->prefix}groups_members mgm ON (mgm.userid = mu.id) LEFT JOIN {$CFG->prefix}groups mg ON (mg.id = mgm.groupid) LEFT JOIN {$CFG->prefix}department md ON (md.id = mu.department) WHERE mu.deleted = '0' GROUP BY mu.id ";	
							
							$allSystemUserAllArr = $DB->get_records_sql($queryAll);
							$allSystemUserCount = count($allSystemUserAllArr);
						
							$userHTML = '';
							
							$userCourseStatusArr = array();
							$totalAssignedCoursesAll = 0;
							$inProgressCoursesAll = 0;
							$completedCoursesAll = 0;
							$notStartedCoursesAll = 0;
							
							$IPUserId = array(); 
							$CLUserId = array();
							$NSUserId = array();  
						
							if($allSystemUserCount > 0){
							
							
								foreach($allSystemUserAllArr as $userId=>$data){
															 
									$courseStatus = getCourseIdByStatus($userId);
									$totalAssignedCourses = isset($courseStatus['allids'])?count($courseStatus['allids']):0;
									$inProgressCourses = isset($courseStatus['type'][1])?count($courseStatus['type'][1]):0;
									$completedCourses = isset($courseStatus['type'][2])?count($courseStatus['type'][2]):0;
									$notStartedCourses = isset($courseStatus['type'][3])?count($courseStatus['type'][3]):0;
								
									$userCourseStatusArr[$userId]['all'] = $totalAssignedCourses;
									$userCourseStatusArr[$userId]['1'] = $inProgressCourses;
									$userCourseStatusArr[$userId]['2'] = $completedCourses;
									$userCourseStatusArr[$userId]['3'] = $notStartedCourses;
									
									$totalAssignedCoursesAll += $totalAssignedCourses;
									   $inProgressCoursesAll +=  $inProgressCourses;
										$completedCoursesAll += $completedCourses;
									   $notStartedCoursesAll += $notStartedCourses;
									   
									   if($inProgressCourses ){ 
										 $IPUserId[] = $userId;
									   }
									   
									   if($completedCourses ){ 
										 $CLUserId[] = $userId;
									   }
									   
									   if($notStartedCourses ){ 
										 $NSUserId[] = $userId;
									   }
							  
									
								}
								
							
								$completedPercentageU = round(($completedCoursesAll*100)/$totalAssignedCoursesAll);
								$inProgressPercentageU = round(($inProgressCoursesAll*100)/$totalAssignedCoursesAll);
								$notStartedPercentageU = round(($notStartedCoursesAll*100)/$totalAssignedCoursesAll);
				
							    $userHTML .= '<div class = "single-report-start f-left half-space">
												<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('usereperformancereport','multiuserreport').'</span>
												  <div class="single-report-graph-left" id="UserChartContainer">'.get_string('loading','multiuserreport').'</div>
												  <div class="single-report-graph-right">
													<div class="course-count-heading">'.get_string('numberofcourses','multiuserreport').'</div>
													<div class="course-count">'.$totalAssignedCoursesAll.'</div>
													<div class="seperator">&nbsp;</div>
													<div class="course-status">
													  <div class="notstarted"><h6>'.get_string('notstarted','multiuserreport').'</h6><span>'.$notStartedCoursesAll.'</span></div>
													  <div class="clear"></div>
													  <div class="inprogress"><h6>'.get_string('inprogress','multiuserreport').'</h6><span>'.$inProgressCoursesAll.'</span></div>
													  <div class="clear"></div>
													  <div class="completed"><h6>'.get_string('completed','multiuserreport').'</h6><span>'.$completedCoursesAll.'</span></div>
													</div>
												  </div>
												</div>
<div class="viewmore" style="float:right"><a href="'.$CFG->wwwroot.'/user/multiuser_report.php" alt="'.get_string('viewmore','adminreportdashboard').'"  title="'.get_string('viewmore','adminreportdashboard').'" >'.get_string('viewmore','adminreportdashboard').'</a></div>
											  </div>';
											 
										
							} 
						
						
						
							$userHTML .=  '
											';
														
							echo $userHTML ;
							
			
			
			// search condition end here

			$query = "select  mc.id, mc.fullname, mc.summary ";
			$query .= ",  mcc.name as category";
			$query .= ", if(md.id, GROUP_CONCAT(DISTINCT  md.title ORDER BY md.title ASC),'---') as departmenttitle ";
			$query .= ", if(mg.id, GROUP_CONCAT(DISTINCT  mg.name ORDER BY mg.name ASC),'---') as teamtitle  ";
			$query .= ", if(mu.id, GROUP_CONCAT(DISTINCT  concat(mu.firstname, '<br>', mu.lastname) ORDER BY mg.name ASC),'---') as userfullname  ";
			$query .= "  FROM {$CFG->prefix}course mc  ";
			$query .= " LEFT JOIN {$CFG->prefix}course_categories mcc ON (mc.category = mcc.id)  ";
			$query .= " LEFT JOIN {$CFG->prefix}user_course_mapping mucm_dep ON (mucm_dep.courseid = mc.id AND mucm_dep.type = 'department') ";
			$query .= " LEFT JOIN {$CFG->prefix}department md ON (md.id = mucm_dep.typeid) ";
			$query .= " LEFT JOIN {$CFG->prefix}user_course_mapping mucm_group ON (mucm_group.courseid = mc.id AND mucm_group.type = 'group') ";
			$query .= " LEFT JOIN {$CFG->prefix}groups mg ON (mg.id = mucm_group.typeid) ";
			$query .= " LEFT JOIN {$CFG->prefix}user_course_mapping mucm_user ON (mucm_group.courseid = mc.id AND mucm_user.type = 'user') ";
			$query .= " LEFT JOIN {$CFG->prefix}user mu ON (mu.id = mucm_user.userid) ";
			$query .= "  WHERE mc.is_active = '1' AND mc.id != '1'  $searchString GROUP BY mc.id  ";		


			$courseAllArr = $DB->get_records_sql($query);	
			$courseCount = count($courseAllArr);

			$courseHTML = '';
			$userCourseStatusArr = array();
			$totalAssignedCoursesAll = 0;
			$inProgressCoursesAll = 0;
			$completedCoursesAll = 0;
			$notStartedCoursesAll = 0;

			$NSCourseIds = array();
			$IPCourseIds = array();
			$ClCourseIds = array();
			$ByTypeCourseIds = array();

			if(count($courseAllArr) > 0 ){
			
				foreach($courseAllArr as $courses){
				
			
					$courseId = $courses->id;
					$reportsArrPerUser = getSingleCourseReportPerUser($courseId, 'coursecount');
					$inProgressCourses = $reportsArrPerUser['UserCourseStatusCount']->IP;
					$completedCourses = $reportsArrPerUser['UserCourseStatusCount']->CL;
					$notStartedCourses = $reportsArrPerUser['UserCourseStatusCount']->NS;

				   
				 
					 
						$inProgressCourses = $reportsArrPerUser['UserCourseStatusCount']->IP;
						$completedCourses = $reportsArrPerUser['UserCourseStatusCount']->CL;
						$notStartedCourses = $reportsArrPerUser['UserCourseStatusCount']->NS;
				   
						$totalAssignedUserCnt = $inProgressCourses + $completedCourses + $notStartedCourses;
						
						$userCourseStatusArr[$courseId]['all'] = $totalAssignedUserCnt;
						$userCourseStatusArr[$courseId]['1'] = $inProgressCourses;
						$userCourseStatusArr[$courseId]['2'] = $completedCourses;
						$userCourseStatusArr[$courseId]['3'] = $notStartedCourses;
						
						$totalAssignedCoursesAll += $totalAssignedUserCnt;
						   $inProgressCoursesAll +=  $inProgressCourses;
							$completedCoursesAll += $completedCourses;
						   $notStartedCoursesAll += $notStartedCourses;
	
				}
					
				// pr($userCourseStatusArr);die;
				
				$completedPercentageC = round(($completedCoursesAll*100)/$totalAssignedCoursesAll);
				$inProgressPercentageC = round(($inProgressCoursesAll*100)/$totalAssignedCoursesAll);
				$notStartedPercentageC = round(($notStartedCoursesAll*100)/$totalAssignedCoursesAll);
				
							
			    $courseHTML .= '<div class = "single-report-start align-right half-space">
								<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('courseusagesreport','multicoursereport').$exportHTML.'</span>
								  <div class="single-report-graph-left" id="CourseChartContainer">'.get_string('loading','multicoursereport').'</div>
								  <div class="single-report-graph-right">
									<div class="course-count-heading">'.get_string('numberofusers','multicoursereport').'</div>
									<div class="course-count">'.$totalAssignedCoursesAll.'</div>
									<div class="seperator">&nbsp;</div>
									<div class="course-status">
									  <div class="notstarted"><h6>'.get_string('notstarted','multicoursereport').'</h6><span>'.$notStartedCoursesAll.'</span></div>
									  <div class="clear"></div>
									  <div class="inprogress"><h6>'.get_string('inprogress','multicoursereport').'</h6><span>'.$inProgressCoursesAll.'</span></div>
									  <div class="clear"></div>
									  <div class="completed"><h6>'.get_string('completed','multicoursereport').'</h6><span>'.$completedCoursesAll.'</span></div>
									</div>
								  </div>
								</div>
								<div class="viewmore" style="float:right"><a href="'.$CFG->wwwroot.'/course/multicoursereport.php" alt="'.get_string('viewmore','adminreportdashboard').'"  title="'.get_string('viewmore','adminreportdashboard').'" >'.get_string('viewmore','adminreportdashboard').'</a></div>
							  </div>
							  <div class="clear"></div>';
			} 
			
			
			echo $courseHTML;
							 
		   ?>

    </div>
  </div>
</div>

<script language="javascript" type="text/javascript">
 window.onload = function () {
			
					var chart = new CanvasJS.Chart("UserChartContainer",
					{
						title:{
							text: ""
						},
						theme: "theme2",
						data: [
						{        
							type: "doughnut",
							indexLabelFontFamily: "Garamond",       
							indexLabelFontSize: 12,
							startAngle:0,
							indexLabelFontColor: "dimgrey",       
							indexLabelLineColor: "darkgrey", 
							toolTipContent: "{y} %", 					
				
							dataPoints: [
							{  y: '<?php echo $notStartedPercentageU;?>', label: "<?php echo get_string('notstarted','multiuserreport');?> {y}%" },
							{  y: '<?php echo $inProgressPercentageU;?>', label: "<?php echo get_string('inprogress','multiuserreport');?> {y}%" },
							{  y: '<?php echo $completedPercentageU;?>', label: "<?php echo get_string('completed','multiuserreport');?> {y}%" },
				
							]
							
						}
						]
					});
					
					 chart.render();
					
					var chart = new CanvasJS.Chart("CourseChartContainer",
					{
						title:{
							text: ""
						},
						theme: "theme2",
						data: [
						{        
							type: "doughnut",
							indexLabelFontFamily: "Garamond",       
							indexLabelFontSize: 12,
							startAngle:0,
							indexLabelFontColor: "dimgrey",       
							indexLabelLineColor: "darkgrey", 
							toolTipContent: "{y} %", 					
				
							dataPoints: [
							{  y: '<?php echo $notStartedPercentageC;?>', label: "<?php echo get_string('notstarted','multiuserreport');?> {y}%" },
							{  y: '<?php echo $inProgressPercentageC;?>', label: "<?php echo get_string('inprogress','multiuserreport');?> {y}%" },
							{  y: '<?php echo $completedPercentageC;?>', label: "<?php echo get_string('completed','multiuserreport');?> {y}%" },
				
							]
							
						}
						]
					});
				
					 chart.render();
					 
				  }
				</script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/charts/canvaschart/canvasjs.min.js"></script>

<?php echo $OUTPUT->footer(); ?>
