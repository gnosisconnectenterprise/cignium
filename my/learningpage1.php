<?php
	require_once(dirname(__FILE__) . '/../config.php'); 
	GLOBAL $CFG;
	require_once($CFG->dirroot . '/my/learninglib.php');
	global $USER;
	$userId = $USER->id;
	
	if($USER->archetype == $CFG->userTypeAdmin){
	   redirect($CFG->wwwroot .'/');
	}	

    require_login();

    $type         = optional_param('type', 1, PARAM_ALPHANUM);
    $page         = optional_param('page', 1, PARAM_INT);
    $perpage      = optional_param('perpage', 10, PARAM_INT);
	
	
	$paramArray = array(
	'type' => $type
    );
  
	
	$removeKeyArray = array('perpage');
	
	$pageURL = $_SERVER['PHP_SELF'];
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);

	/************************* Header Information start ***************************************/

    $header = $SITE->fullname.": ".get_string('mylearningpage', 'learnercourse');
    $PAGE->set_title($header);
	$PAGE->set_heading($header);
	$PAGE->set_pagelayout('mydashboard');
    $stredit   = get_string('edit');
    $strdelete = get_string('delete');
    $strdeletecheck = get_string('deletecheck');
    $strconfirm = get_string('confirm');
	$PAGE->navbar->add(get_string('mylearningpage','menubar'));
    echo $OUTPUT->header();
	
	/************************* Header Information start ***************************************/
	Global $DB,$CFG,$USER;
	$courseProgramList = get_learning_list($sort,$type,$page,$perpage);
	$courseProgramCount = get_learning_list('count',$type,$page,$perpage);
	$programs = array();
	$courses = array();
	if (!empty($courseProgramList)) {
		foreach ($courseProgramList as $key=>$programOrCourseArray) {
			if($programOrCourseArray->program_id != 0){
				$programs[] = $programOrCourseArray->program_id;
			}else{
				$courses[] = $programOrCourseArray->id;
			}
		}
	}
	$programAssetInfo = array();
	$courseAssetInfo = array();
	$programArray = array();
	$courseArray = array();
	if(!empty($programs)){
		$programIds = implode(',',$programs);
		$programAssetInfo = getAssetInfoForProgram($programIds);
		foreach($programAssetInfo as $assetInfo){
			if(!empty($assetInfo)){
				if(array_key_exists($assetInfo->program_id,$programArray)){
					if(array_key_exists($assetInfo->courseid,$programArray[$assetInfo->program_id])){
						$programArray[$assetInfo->program_id][$assetInfo->courseid][] = $assetInfo;
					}else{
						$programArray[$assetInfo->program_id][$assetInfo->courseid][] = array();
						$programArray[$assetInfo->program_id][$assetInfo->courseid][] = $assetInfo;
					}

				}else{
					$programArray[$assetInfo->program_id] = array();
					$programArray[$assetInfo->program_id][$assetInfo->courseid][] = $assetInfo;
				}
			}
		}
	}
	unset($assetInfo);
	if(!empty($courses)){
		$courseIds = implode(',',$courses);
		$courseAssetInfo = getAssetInfoForCourse($courseIds);
		foreach($courseAssetInfo as $assetInfo){
			if(!empty($assetInfo)){
				if(array_key_exists($assetInfo->courseid,$courseArray)){
					if(array_key_exists($assetInfo->instanceid,$courseArray[$assetInfo->courseid])){
						$courseArray[$assetInfo->courseid][$assetInfo->instanceid] = $assetInfo;
					}else{
						$courseArray[$assetInfo->courseid][$assetInfo->instanceid] = array();
						$courseArray[$assetInfo->courseid][$assetInfo->instanceid] = $assetInfo;
					}

				}else{
					$courseArray[$assetInfo->courseid] = array();
					$courseArray[$assetInfo->courseid][$assetInfo->instanceid] = $assetInfo;
				}
			}
		}
	}
    $styTabs1 = '';
	$styTabs2  = '';
	$styTabs3 = '';
	if($type == 1){
		$styTabs1 = "current";
	}

	if($type == 2){
		$styTabs2 = "current";
	}

	if($type == 3){
		$styTabs3 = "current";
	}
	 
	$courseTabs = '';
	$html = '';
	//pr($courseArray);die;
	$outerDivStart = "<div class='tabsOuter'>";
	$outerDivEnd = "</div>";
	
		$courseTabs .= "<div class='tabLinks'>";
			$courseTabs .= "<div><a href='".$CFG->wwwroot."/my/learningpage.php?type=1' title='".get_string('todo','learnercourse')."' class='$styTabs1'>".get_string('todo','learnercourse')."</a></div>";
			$courseTabs .= "<div><a href='".$CFG->wwwroot."/my/learningpage.php?type=2' title='".get_string('completed','learnercourse')."' class='$styTabs2' >".get_string('completed','learnercourse')."</a></div>";
			$courseTabs .= "<div><a href='".$CFG->wwwroot."/my/availablecourses.php' title='".get_string('available','learnercourse')."' class='$styTabs3'>".get_string('available','learnercourse')."</a></div>";
		$courseTabs .= "</div>";
	$html = $outerDivStart.$courseTabs;
	$htmlDiv = '';
	$html .= html_writer::start_tag('div', array('class'=>'no-overflow'));
	if (!empty($courseProgramList)) {
		$i= 0;
		foreach ($courseProgramList as $key=>$programOrCourseArray) {
			$i++;
			$classAlt = $i%2==0?'even-course':'odd-course';
			$courseDiv = '';
			if($programOrCourseArray->program_id != 0){
				$headingText = get_string('program','learnercourse').$i.': '.$programOrCourseArray->program_name;
				$summary = strlen($programOrCourseArray->program_description)>150?substr($programOrCourseArray->program_description,0,150).'...':$programOrCourseArray->program_description;
				$descriptionText = '<strong>'.get_string('description','learnercourse').'</strong>'.': '.$summary;

				if(isset($programArray[$programOrCourseArray->program_id]) && !empty($programArray[$programOrCourseArray->program_id])){
					$courseDiv = '';
					$courseDiv .= "<div class='d-box no-box' id='dbox".$programOrCourseArray->program_id."_0' style='display: none;'><div class='section-box no-section-box'><div class='c-box' id='cbox8'>";
					$j=0;
					foreach($programArray[$programOrCourseArray->program_id] as $cKey=>$programCourseArray){
						$j++;
						$divKey = $programOrCourseArray->program_id.'_'.$cKey;
						$classAlt2 = $j%2==0?'even-course':'odd-course';
						$assetDiv = '';
						$assetDiv = "";
						if(!empty($programCourseArray)){
							$assetDiv .= "<div class='e-box e-box-inner' id='ebox0_".$divKey."' style='display: none;'><div class='section-box'><table class='admintable generaltable' id='learnercourses'>
								<thead>
								<tr>
								<th class='header c0 leftalign  w150' style='' scope='col'>".get_string('title','learnercourse')."</th>
								<th class='header c1 leftalign w350' style='' scope='col'>".get_string('description','learnercourse')."</th>
								<th class='header c2 leftalign' style='' scope='col'>".get_string('currentstatus','learnercourse')."</th>
								<th class='header c3 leftalign' style='' scope='col'>".get_string('score','learnercourse')."</th>
								<th class='header c4 leftalign' style='' scope='col'>".get_string('lastaccessed','learnercourse')."</th>
								<th class='header c5 lastcol centeralign' style='' scope='col'>".get_string('actions','learnercourse')."</th>
								</tr>
								</thead>
								<tbody>";
							foreach($programCourseArray as $assetArray){
								$headingText1 = get_string('course','learnercourse').$j.': '.$assetArray->fullname;
								$summary1 = $assetArray->description?(strlen(strip_tags($assetArray->description)) > 85?(substr(strip_tags($assetArray->description), 0, 85).' ....'):strip_tags($assetArray->description)):'';

								$learningObj1 = $assetArray->learningobj?(strlen(strip_tags($assetArray->learningobj)) > 85?(substr(strip_tags($assetArray->learningobj), 0, 85).' ....'):strip_tags($assetArray->learningobj)):'';

								$performanceOut1 = $assetArray->performanceout?(strlen(strip_tags($assetArray->performanceout)) > 85?(substr(strip_tags($assetArray->performanceout), 0, 85).' ....'):strip_tags($assetArray->performanceout)):'';

								$descriptionText1 = get_string('description','learnercourse').': '.$summary1;
								$pcLearningObj = $learningObj1?('<br /><strong>'.get_string('learningobj','course').': </strong>'.$learningObj1):'';
								$performanceOut = $performanceOut1?('<br /><strong>'.get_string('performanceOut','course').': </strong>'.$performanceOut1):'';

								if($assetArray->module == 17){
									$score = get_string('NA','learnercourse');
									$launchClass = "download_icon";
									if($assetArray->combined_stat == 'completed'){
										$launchClass .= " downloaded";
									}
									$context = context_module::instance($assetArray->moduleid);
									$fs = get_file_storage();
									$files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false);
									$file = reset($files);
									$path = '/'.$context->id.'/mod_resource/content/'.$assetArray->revision.$file->get_filepath().$file->get_filename();
									$fullurl = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
									$linkUrl = "<a href='javascript:;' onclick=\"setCourseLastAccess($USER->id, $programOrCourseArray->id, $assetArray->instanceid);window.open('".$fullurl."', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \" title='".get_string('download','learnercourse')."' class='".$launchClass."'   id='coursestatus_".$programOrCourseArray->id."_".$assetArray->instanceid."'>".get_string('download','learnercourse')."</a>";

								}else{
									$scores = explode('_',$assetArray->score);
									foreach($scores as $keys=>$scor){
										$scores[$keys] = (int)$scor;
									}
									$score = max($scores);
									$launchurl = $CFG->wwwroot.'/mod/scorm/player.php?a='.$assetArray->instanceid.'&cm='.$assetArray->moduleid.'&scoid='.$assetArray->scoid.'&currentorg='.$assetArray->organization.'&newattempt=on&display=popup&mode=normal';
									$linkUrl = "<a href=\"javascript:void(0);\" onclick=\"window.open('".$launchurl."', '', 'width=".$assetArray->width.",height=".$assetArray->height.",toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \"  title='".get_string('launch','learnercourse')."' class = \"launch_icon\">".get_string('launch','learnercourse')."</a>";
								}
								if($assetArray->lastaccessed){
									$remainAccessedTime = time() - $assetArray->lastaccessed;
									$lastAccessed = timePassed($remainAccessedTime);
								}else{
									 $lastAccessed = get_string('notaccessed','learnercourse');
								}
								$assetDiv .= "<tr class='r0 lastrow'>
									<td class='leftalign  w150 cell c0' style=''><a href='javascript:void(0);' onclick='' title='Introduction to Biology'>".$assetArray->asset_name."</a></td>
									<td class='leftalign w350 cell c1' style=''>".$assetArray->asset_info."</td>
									<td class='leftalign cell c2' style=''>".$assetArray->combined_stat."</td>
									<td class='leftalign cell c3' style=''>".$score."</td>
									<td class='leftalign  cell c4' style=''>".$lastAccessed."</td>
									<td class='centeralign cell c5 lastcol' style=''>".$linkUrl."</td>
									</tr>";
							}
							$assetDiv .= "</tbody></table></div></div>";

						$courseDiv .= '<div class="course '.$classAlt2.'" rel="'.$divKey.'">';
						$courseDiv .= '<div class="pull-left left-icon"><span></span><img alt="" src="'.$CFG->wwwroot.'/theme/image.php?theme=gourmet&component=core&image=a%2Fview_icon_active"></div>';
							$courseDiv .= '<div class="pull-left left-content">';
								$courseDiv .= '<div class="c-heading">'.$headingText1.'</div>';
								$courseDiv .= '<div class="c-text">'.$descriptionText1.$pcLearningObj.$performanceOut.'</div>';
							$courseDiv .= '</div>';
							$courseDiv .= '<div class="pull-left right-icon cbox plus-icon-inner" rel="'.$divKey.'" id="plus-minus-icon-'.$divKey.'"><a href="javascript:void(0)"></a><img alt="" src="'.$CFG->wwwroot.'/theme/image.php?theme=gourmet&component=core&image=a%2Fadd_file"></div>';	
						$courseDiv .= '</div>';
						$courseDiv .= $assetDiv;
						}
					}
					$courseDiv.="</div></div></div>";
				}
			}else{	
				$headingText = get_string('course','learnercourse').$i.': '.$programOrCourseArray->course_name;
				$summary = strlen($programOrCourseArray->course_description)>150?substr($programOrCourseArray->course_description,0,150).'...':$programOrCourseArray->course_description;

				$learningObj = $programOrCourseArray->learningobj?(strlen(strip_tags($programOrCourseArray->learningobj)) > 85?(substr(strip_tags($programOrCourseArray->learningobj), 0, 85).' ....'):strip_tags($programOrCourseArray->learningobj)):'';

				$performanceOut = $programOrCourseArray->performanceout?(strlen(strip_tags($programOrCourseArray->performanceout)) > 85?(substr(strip_tags($programOrCourseArray->performanceout), 0, 85).' ....'):strip_tags($programOrCourseArray->performanceout)):'';

				$pcLearningObj = $learningObj?('<br /><strong>'.get_string('learningobj','course').': </strong>'.$learningObj):'';
				$performanceOut = $performanceOut?('<br /><strong>'.get_string('performanceOut','course').': </strong>'.$performanceOut):'';
				$descriptionText = '<strong>'.get_string('description','learnercourse').'</strong>'.': '.$summary.$pcLearningObj.$performanceOut;
				if(isset($courseArray[$programOrCourseArray->id]) && !empty($courseArray[$programOrCourseArray->id])){
					$courseDiv .= "<div class='d-box' id='dbox0_".$programOrCourseArray->id."' style='display: none;'><div class='section-box'><div class='c-box' id='cbox8'><table class='admintable generaltable' id='learnercourses'>
						<thead>
						<tr>
						<th class='header c0 leftalign  w150' style='' scope='col'>".get_string('title','learnercourse')."</th>
						<th class='header c1 leftalign w350' style='' scope='col'>".get_string('description','learnercourse')."</th>
						<th class='header c2 leftalign' style='' scope='col'>".get_string('currentstatus','learnercourse')."</th>
						<th class='header c3 leftalign' style='' scope='col'>".get_string('score','learnercourse')."</th>
						<th class='header c4 leftalign' style='' scope='col'>".get_string('lastaccessed','learnercourse')."</th>
						<th class='header c5 lastcol centeralign' style='' scope='col'>".get_string('actions','learnercourse')."</th>
						</tr>
						</thead>
						<tbody>";
					foreach($courseArray[$programOrCourseArray->id] as $assetArr){
						if($assetArr->module == 17){
							$score = get_string('NA','learnercourse');
							$launchClass = "download_icon";
							if($assetArr->combined_stat == 'completed'){
								$launchClass .= " downloaded";
							}
							$context = context_module::instance($assetArr->moduleid);
							$fs = get_file_storage();
							$files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false);
							$file = reset($files);
							$path = '/'.$context->id.'/mod_resource/content/'.$assetArr->revision.$file->get_filepath().$file->get_filename();
							$fullurl = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
							$linkUrl = "<a href='javascript:;' onclick=\"setCourseLastAccess($USER->id, $programOrCourseArray->id, $assetArr->instanceid);window.open('".$fullurl."', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \" title='".get_string('download','learnercourse')."' class='".$launchClass."'   id='coursestatus_".$programOrCourseArray->id."_".$assetArr->instanceid."'>".get_string('download','learnercourse')."</a>";

						}else{
							$scores = explode('_',$assetArr->score);
							foreach($scores as $keys=>$scor){
								$scores[$keys] = (int)$scor;
							}
							$score = max($scores);
							$launchurl = $CFG->wwwroot.'/mod/scorm/player.php?a='.$assetArr->instanceid.'&cm='.$assetArr->moduleid.'&scoid='.$assetArr->scoid.'&currentorg='.$assetArr->organization.'&newattempt=on&display=popup&mode=normal';
							$linkUrl = "<a href=\"javascript:void(0);\" onclick=\"window.open('".$launchurl."', '', 'width=".$assetArr->width.",height=".$assetArr->height.",toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \"  title='".get_string('launch','learnercourse')."' class = \"launch_icon\">".get_string('launch','learnercourse')."</a>";
						}
						if($assetArr->lastaccessed){
							$remainAccessedTime = time() - $assetArr->lastaccessed;
							$lastAccessed = timePassed($remainAccessedTime);
						}else{
							 $lastAccessed = get_string('notaccessed','learnercourse');
						}
						$courseDiv .= "<tr class='r0 lastrow'>
							<td class='leftalign  w150 cell c0' style=''><a href='javascript:void(0);' onclick='' title='Introduction to Biology'>".$assetArr->asset_name."</a></td>
							<td class='leftalign w350 cell c1' style=''>".$assetArr->asset_info."</td>
							<td class='leftalign cell c2' style=''>".$assetArr->combined_stat."</td>
							<td class='leftalign cell c3' style=''>".$score."</td>
							<td class='leftalign  cell c4' style=''>".$lastAccessed."</td>
							<td class='centeralign cell c5 lastcol' style=''>".$linkUrl."</td>
							</tr>";
					}
					$courseDiv.="</tbody></table></div></div></div>";
				}else{
				}
				
			}
			$htmlDiv = '<div class="no-course '.$classAlt.'" rel="'.$key.'">';
				$htmlDiv .= '<div class="pull-left left-icon"><span></span><img alt="" src="'.$CFG->wwwroot.'/theme/image.php?theme=gourmet&component=core&image=a%2Fview_icon_active"></div>';
					$htmlDiv .= '<div class="pull-left left-content">';
						$htmlDiv .= '<div class="c-heading">'.$headingText.'</div>';
						$htmlDiv .= '<div class="c-text">'.$descriptionText.'</div>';
					$htmlDiv .= '</div>';
					$htmlDiv .= '<div class="pull-left right-icon cbox" rel="'.$key.'" id="plus-minus-icon-'.$key.'"><a href="javascript:void(0)"></a><img alt="" src="'.$CFG->wwwroot.'/theme/image.php?theme=gourmet&component=core&image=a%2Fadd_file"></div>';	
				$htmlDiv .= '</div>';
			$htmlDiv .= $courseDiv;
			$html .= $htmlDiv;
		}
	}else{
	    $html .= $OUTPUT->heading(get_string('no_results'));
	}
	  
  $html .= html_writer::end_tag('div');
  $html .= paging_bar($courseProgramCount, $page, $perpage, $genURL); 
  $html .= $outerDivEnd;
echo $html;	
 ?>
 
 <script type="text/javascript">
 
 
 function setCourseLastAccess(userid, courseid, resourceid){
	
     
	   $.ajax({
	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=setCourseLastAccess&userid='+userid+"&courseid="+courseid+"&resourceid="+resourceid,
		dataType:'json',
		success:function(data){
		  
		   var success = data.success;
		     var error = data.error;
			   if(success == 1){
			      var course_resource_id = data.response;
				  $('#coursestatus_'+course_resource_id).addClass('downloaded');
			   }else{
				 alert(error);
			   }
		}
	  
	  });
	  $.ajax({
	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=setCourseBadge&userid='+userid+"&courseid="+courseid+"&resourceid="+resourceid,
		dataType:'json',
		success:function(data){  
		}
	  
	  });
	     
	
 }
	
 function showHideButton(){
      $('a.moreless-toggler').addClass('moreless-toggler moreless-less');
	  $('a.moreless-toggler').html('<?php echo get_string('showless','form');?>');
	  
	  $('div.fitem_fselect').addClass('fitem advanced fitem_fselect show');
	  $('div.fitem_fgroup').addClass('fitem advanced fitem_fgroup show');
	   
 }
 
 
 /*$(window).load(function () {
     <?php if(isset($SESSION->learnercourse_filtering['categoryname']) || isset($SESSION->learnercourse_filtering['timecreated'])){ ?>
	      //  showHideButton();
	<?php } ?>
 });*/
 
 $(document).ready(function(){
 
      $('.no-course').click(function(){
	         var courseId = $(this).attr('rel');
			 $('.no-course').removeClass('selected');
			 $('.cbox').not('[rel="'+courseId+'"]').removeClass('minus');
			 var hasClass = $('#plus-minus-icon-'+courseId).hasClass('minus');
			 if(hasClass){
			   $('#plus-minus-icon-'+courseId).removeClass('minus');
			 }else{
			   $('#plus-minus-icon-'+courseId).addClass('minus');
			 }
			 
			 $('.d-box').not('#dbox'+courseId).hide();
			 
			 if($('#dbox'+courseId).is(':visible')){
			    $('#dbox'+courseId).hide();
			 }else{
			   $('#dbox'+courseId).show();
			 }
			 
			 $(this).addClass('selected');	         
	  
	  });
	  
	  
	  $('.no-box .course').click(function(){
			var courseId = "ebox0_"+$(this).attr('rel');
			//$('.e-box-inner').hide();
			$('#'+courseId).toggle();
			$(this).toggleClass('selected');			
			$(this).find('.plus-icon-inner').toggleClass('minus');
	  });
	  
 });


 </script>
 <?php	

 echo $OUTPUT->footer();
 