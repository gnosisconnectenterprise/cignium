<?php
/**
	* Custom module - Learner Dashboard
	* Date Creation - 04/06/2014
	* Date Modification : 24/06/2014
	* Last Modified By : Bhavana Kadwal
*/

require_once(dirname(__FILE__) . '/../config.php');
global $CFG,$USER,$DB;
require_once($CFG->dirroot . '/my/lib.php');
require_once($CFG->dirroot.'/mod/scorm/locallib.php');
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot.'/calendar/lib.php');
require_once($CFG->dirroot . '/my/learninglib.php');
require_once($CFG->dirroot . '/mod/scheduler/lib.php');

if($USER->archetype == $CFG->userTypeAdmin){  // added by rajesh 
   //redirect($CFG->wwwroot .'/');
}

require_login();

$strmymoodle = get_string('myhome');
$header = "$SITE->shortname: $strmymoodle";
$context = get_context_instance(CONTEXT_SYSTEM);
$params = array();
$PAGE->set_title($header);
$PAGE->set_heading($header);
$PAGE->set_pagelayout('mydashboard');
$stredit   = get_string('edit');
$strdelete = get_string('delete');
$strdeletecheck = get_string('deletecheck');
$strconfirm = get_string('confirm');
$PAGE->navbar->add(get_string('requests','menubar'));
echo $OUTPUT->header();
$page         = optional_param('page', 1, PARAM_INT);
$perpage      = optional_param('perpage', $CFG->perpage, PARAM_INT);
$requestParam      = optional_param('request', 1, PARAM_INT);
$key      = optional_param('key', '', PARAM_RAW);
$genURL = "/my/requests.php";
$page1 = $page - 1;
$page1 = $page1*$perpage;
$limit = '';
if($perpage != 0){
	$limit = " LIMIT $page1,$perpage";
}
$tableTd = '';
if($requestParam == 2){
	$styTabs2 = 'current';
	$tableTd .= "<td width='15%'>Request Type</td>";
	$tableTd .= "<td width='25%'>Request Details</td>";
	$tableTd .= "<td width='20%'>Requested By</td>";
	$tableTd .=  "<td width='15%'>Requested Date</td>";
	$tableTd .=  "<td width='15%'>Response Date</td>";
	$tableTd .=  "<td width='10%'>Status</td>";
	$colspan = 6;
}else{
	$styTabs1 = 'current';
	$tableTd .= "<td width='15%'>Request Type</td>";
	$tableTd .= "<td width='30%'>Request Details</td>";
	$tableTd .= "<td width='25%'>Requested By</td>";
	$tableTd .=  "<td width='15%'>Requested Date</td>";
	$tableTd .=  "<td width='15%'>Action</td>";
	$colspan = 5;
}
$searchText = '';
if(trim($key) != ''){
	$searchText = addslashes(strtolower(trim($key)));
}
$requests = getRequestdata($limit, $requestParam, 1, $searchText);
$requestsCount = getRequestdata($limit, $requestParam, 0, $searchText);

$outerDivStart = "<div class='tabsOuter'>";
$outerDivEnd = "</div>";
	$courseTabs .= "<div class='tabLinks'>";
		$courseTabs .= "<div><a href='".$CFG->wwwroot."/my/requests.php?request=1' title='Open' class='$styTabs1'>Open<span></span></a></div>";
		$courseTabs .= "<div><a href='".$CFG->wwwroot."/my/requests.php?request=2' title='Closed' class='$styTabs2' >Closed<span></span></a></div>";
	$courseTabs .= "</div>";

$paramArray = array('request'=>$requestParam,'key'=>$key);
$filterArray = array('request'=>$requestParam);
$html = genCommonSearchForm($paramArray, $filterArray, $genURL);
$html .= $outerDivStart.$courseTabs;
$html .= html_writer::start_tag('div', array('class'=>''));
$html .= '<div class = "borderBlockSpace">';

if($_SESSION['update_msg']!=''){
	$html .= '<div class="clear"></div>';
	$html .= '<div class="bg-success text-success text-center '.$_SESSION['error_class'].'" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
	$html .= '<div class="clear"></div>';
	$_SESSION['update_msg']='';
	$_SESSION['error_class']='';
}
	$tableHtml = '<table id = "requestTable" class="table1"><thead><tr>';
	$tableHtml .= $tableTd;
	$tableHtml .= '</tr></thead>';
	if(empty($requests)){
		$tableHtml .= '<tr><td colspan = "'.$colspan.'">'.get_string('no_results').'</td></tr>';
	}else{
		foreach($requests as $request){
			$actionItem = "";
			$numberOfSeats = $request->no_of_seats;
			$enrolmentType = $request->enrolmenttype;
			$createdBy = $request->createdby;
			$requestedBy = $request->firstname.' '.$request->lastname.'</br>('.$request->username.')';
			if($request->request_status == 0 || $request->request_status == 3){
				$requestText = '';
				$tdDate = $request->requested_on;
			}else{
				$requestText = date($CFG->DateFormatForRequests,$request->requested_on).' - ';
				$requestDate = $request->requested_on;
				$tdDate = $request->reponse_on;
			}
			if($request->request_type == 'classroom'){
				
				if($request->sessioncount > 1){
					$timeString = getClassDuration($request->startdate,$request->enddate);
					$request->fullname = '<a href = "'.$CFG->wwwroot.'/course/classroom_preview.php?id='.$request->course_id.'&classId='.$request->classid.'">'.$request->fullname.'</a>';
					$request->fullname .= '</br>'.$timeString;
				}else{
					$end_time =endTimeByDuration($request->starttime,$request->duration);
					$duration_text = date($CFG->customDefaultDateTimeFormat,$request->starttime).' to '.date($CFG->customDefaultTimeFormat1,$end_time);
					$request->fullname = '<a href = "'.$CFG->wwwroot.'/course/classroom_preview.php?id='.$request->course_id.'&classId='.$request->classid.'">'.$request->fullname.'</a>';
					$request->fullname .= '</br>'.$duration_text;
				}
				$reqId = $request->classid;
			}else{
				$requestText .= $request->request_text;
				$reqId = $request->id;
			}
			if($request->request_status == 0 || $request->request_status == 3){
					$actionItem .= "<a href = '#' title = '".get_string('accept_request')."' seat = '".$numberOfSeats."' enrolment-type = '".$enrolmentType."' creator = '".$createdBy."' request-type = '".$request->request_type."' enrol-user = '".$request->user_id."' relp = '".$request->program_id."' relc = '".$request->course_id."' rels = '".$reqId."' class = 'enrol_user accept' enrol = '1' >".get_string('accept_request')."</a>";
					$actionItem .= "<a href = '#' title = '".get_string('decline_request')."' request-type = '".$request->request_type."' enrol-user = '".$request->user_id."' relp = '".$request->program_id."' relc = '".$request->course_id."' rels = '".$reqId."' class = 'enrol_user decline' enrol = '0' >".get_string('decline_request')."</a>";
			}else{
				if($request->request_status == 1){
					$actionItem = get_string('accept_request_closed');
				}else{
					$actionItem = get_string('decline_request_closed');
					if(($request->user_id == $request->action_taken_by) && ($request->user_id != $USER->id) && $request->request_status == 2){
						$actionItem .= '</br>'."(".get_string('request_declined').")";
					}
				}
			}
			if($request->request_type == get_string('program_extension')){
				$reqTypeText = 'program extension';
			}elseif($request->request_type == get_string('course_extension')){
				$reqTypeText = 'online course extension';
			}else{
				if($request->request_type == 'program'){
					$reqTypeText = 'Program';
				}elseif(strtolower($request->request_type) == 'online course'){
					$reqTypeText = get_string('onlinecourse');//$request->request_type;
				}else{
					$reqTypeText = $request->request_type.' Course';
				}
			}
			$tableHtml .= '<tr id = "'.$request->program_id.'_'.$request->course_id.'">';
			$tableHtml .= "<td>".ucwords($reqTypeText)."</td>";
			$tableHtml .= "<td>".$request->fullname."</td>";
			$tableHtml .= "<td>".$requestedBy."</td>";
			if($requestParam == 2){
				$tableHtml .= "<td>".date($CFG->DateFormatForRequests,$requestDate)."</td>";
			}
			$tableHtml .= "<td>".date($CFG->DateFormatForRequests,$tdDate)."</td>";
			$tableHtml .= "<td>".$actionItem."</td>";
			$tableHtml .= '</tr>';
		}
	}
	$tableHtml .= "</table>";
	$html .= $tableHtml;
$html .= $outerDivEnd;
$html .= html_writer::end_tag('div');
$html .= $outerDivEnd;
echo $html;
$pageUrl = $genURL."?request=".$requestParam;
echo paging_bar($requestsCount, $page, $perpage, $pageUrl); 

?>
<a href="#end-date-div" id="inline-iframe" rel="inline-iframe" title="<?php print_string('allocation'); ?>">click</a>
<div class="end-date-div" id="end-date-div">
	<iframe src="" id="i-frame" width="100%" height="100%"></iframe>
</div>

<script>
$(document).ready(function(){
	$("#inline-iframe").colorbox({inline:true, width:"675px", height:"468px"});
	$(".enrol_user").click(function(){
		var type = $(this).attr('request-type');
		var enrolUser = $(this).attr("enrol");
		if(enrolUser == 1){
			var txt = "Do you want to accept this request?";
		}else{
			var txt = "Do you want to decline this request?";
		}
		if("<?php echo $USER->archetype;?>" == "<?php echo $CFG->userTypeManager;?>"){
			if(type == "classroom"){
				if(enrolUser == 1){
					var response = confirm(txt);
					var userId = $(this).attr('enrol-user');
					if (response == true) {
						var enrolid = $(this).attr('rels');
						$.ajax({
							url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
							type:'POST',
							data:"action=checkSeatEnrol&enrolid="+enrolid+"&enrolUser="+enrolUser,
							success:function(data){
								if(data == 'success'){
									$.ajax({
										url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
										type:'POST',
										data:"action=enrolUserInClassroom&enrolid="+enrolid+"&enrolUser="+enrolUser+"&userIds="+userId,
										success:function(data){
												if(data == 'success'){
													window.location.reload();
												}else{
													alert("unable to process request. Please try again later.");
												}
										}
									});
								}else{
									if(data == 'error'){
										alert("unable to process request. Please try again later.");
									}else{
										var msg = '<?php echo get_string("errornoofinvite","scheduler","'+data+'");?>';
										alert(msg);
									}
								}
							}
						});
					}
				}else{
					var enrolid = $(this).attr('rels');
					var userId = $(this).attr('enrol-user');
					var source = '<?php echo $CFG->wwwroot;?>/course/unenrolcourse.php?is_request=1&element=user&assigntype=classroom&assignId='+enrolid+'&elementList='+userId;
					$("#i-frame").attr('src',source);
					$(".cboxContent #end-date-div").attr("style",'display:block');
					$("#inline-iframe").trigger("click");
				}
			}else{
				if($(this).attr('relp') == 0 || $(this).attr('relp') == "0"){
					var courseId = $(this).attr('relc');
					var userId = $(this).attr('enrol-user');
					var request_type = $(this).attr('request-type');
					var	update = '';
					if(request_type == "<?php echo get_string('course_extension');?>"){
						update = '&action=update';
					}
					if($(this).attr('enrol') == 1 || $(this).attr('enrol') == "1"){
						var source = '<?php echo $CFG->wwwroot;?>/course/enrolcourse.php?is_request=1&element=user&assigntype=course&assignId='+courseId+'&elementList='+userId+update;
						$("#i-frame").attr('src',source);
						$(".cboxContent #end-date-div").attr("style",'display:block');
						$("#inline-iframe").trigger("click");
					}else{
						var source = '<?php echo $CFG->wwwroot;?>/course/unenrolcourse.php?is_request=1&element=user&assigntype=course&assignId='+courseId+'&elementList='+userId;
						$("#i-frame").attr('src',source);
						$(".cboxContent #end-date-div").attr("style",'display:block');
						$("#inline-iframe").trigger("click");
					}
				}else{
					var programId = $(this).attr('relp');
					var userId = $(this).attr('enrol-user');
					var request_type = $(this).attr('request-type');
					var	update = '';
					if(request_type == "<?php echo get_string('program_extension');?>"){
						update = '&action=update';
					}
					if($(this).attr('enrol') == 1 || $(this).attr('enrol') == "1"){
						var source = '<?php echo $CFG->wwwroot;?>/course/enrolcourse.php?is_request=1&element=user&assigntype=program&assignId='+programId+'&elementList='+userId+update;
						$("#i-frame").attr('src',source);
						$(".cboxContent #end-date-div").attr("style",'display:block');
						$("#inline-iframe").trigger("click");
					}else{
						var source = '<?php echo $CFG->wwwroot;?>/course/unenrolcourse.php?is_request=1&element=user&assigntype=program&assignId='+programId+'&elementList='+userId;
						$("#i-frame").attr('src',source);
						$(".cboxContent #end-date-div").attr("style",'display:block');
						$("#inline-iframe").trigger("click");
					}
				}
			}
		}else{
				if(type == "classroom"){
					var response = confirm(txt);
					if(response == true){
						var enrolid = $(this).attr('rels');
						var userId = $(this).attr("enrol-user");
						$.ajax({
							url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
							type:'POST',
							data:"action=checkSeatEnrol&enrolid="+enrolid+"&enrolUser="+enrolUser,
							success:function(data){
									if(data == 'success'){
										$.ajax({
											url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
											type:'POST',
											data:"action=enrolUserInClassroom&enrolid="+enrolid+"&enrolUser="+enrolUser+"&userIds"+userId,
											success:function(data){
													if(data == 'success'){
														window.location.reload();
													}else{
														alert("unable to process request. Please try again later.");
													}
											}
										});
									}else{
										if(data == 'error'){
											alert("unable to process request. Please try again later.");
										}else{
											var msg = '<?php echo get_string("errornoofinvite","scheduler","'+data+'");?>';
											alert(msg);
										}
									}
							}
						});
					}
				}else{
					if(enrolUser == 1){
						var response = confirm(txt);
						if(response == true){
							var courseId = $(this).attr("relc");
							var programId = $(this).attr("relp");
							var userId = $(this).attr("enrol-user");
							$.ajax({
								url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
								type:'POST',
								data:"action=enrolUserOnRequest&courseId="+courseId+"&programId="+programId+"&userId="+userId+"&enrolUser="+enrolUser,
								success:function(data){
										if(data == 'success'){
											window.location.reload();
										}else{
											alert("unable to process request. Please try again later");
										}
								}
							});
						}
					}else{
						if($(this).attr('relp') == 0 || $(this).attr('relp') == "0"){
							var courseId = $(this).attr('relc');
							var userId = $(this).attr('rels');
							var request_type = $(this).attr('request-type');
							var	update = '';
							if(request_type == "<?php echo get_string('course_extension');?>"){
								update = '&action=update';
							}
							var source = '<?php echo $CFG->wwwroot;?>/course/unenrolcourse.php?is_request=1&element=department&assigntype=course&assignId='+courseId+'&elementList='+userId;
							$("#i-frame").attr('src',source);
							$(".cboxContent #end-date-div").attr("style",'display:block');
							$("#inline-iframe").trigger("click");
						}else{
							var programId = $(this).attr('relp');
							var userId = $(this).attr('rels');
							var request_type = $(this).attr('request-type');
							var	update = '';
							if(request_type == "<?php echo get_string('program_extension');?>"){
								update = '&action=update';
							}
							var source = '<?php echo $CFG->wwwroot;?>/course/unenrolcourse.php?is_request=1&element=department&assigntype=program&assignId='+programId+'&elementList='+userId;
							$("#i-frame").attr('src',source);
							$(".cboxContent #end-date-div").attr("style",'display:block');
							$("#inline-iframe").trigger("click");
						}
					}
				}
		}
	});
});
</script>
<style>
#end-date-div{ display:none;}
#colorbox #end-date-div{display:block}
#inline-iframe{ position:absolute; left:-100000px;}
</style>
<?php
echo $OUTPUT->footer();
?>