<!-- bxSlider Javascript file -->
<?php
/**
	* Custom module - Admin Dashboard
	* Date Creation - 04/06/2014
	* Date Modification : 23/06/2014
	* Last Modified By : Rajesh Kumar
*/
require_once(dirname(__FILE__) . '/../config.php');
global $CFG,$USER;
require_once($CFG->dirroot . '/my/lib.php');
require_once($CFG->dirroot.'/mod/scorm/locallib.php');
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot.'/calendar/lib.php');

$userId = $USER->id;
//$userrole =  getUserRole($USER->id);
checkLogin();
if($USER->archetype == $CFG->userTypeStudent){  // added by rajesh 
   redirect($CFG->wwwroot .'/');
}

$calenderView = optional_param('view', 'day', PARAM_ALPHA);

$strmymoodle = get_string('myhome');
$header = "$SITE->shortname: $strmymoodle";
$context = get_context_instance(CONTEXT_SYSTEM);
$params = array();
$PAGE->set_context($context);
$PAGE->set_url('/my/'.$CFG->pageAdminDashboard, $params);
$PAGE->set_pagelayout('admindashboard');
$PAGE->set_pagetype('my-index');
$PAGE->blocks->add_region('content');
$PAGE->set_subpage($currentpage->id);
$PAGE->set_title($header);
$PAGE->set_heading($header);
echo $OUTPUT->header();


//$newCourseByUserForDashboard = getCourseByUserForDashboard();
?>
<!--<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/slider/style.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/slider/custom.js"></script>-->
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/bxslider/jquery.bxslider.css" />
<!--<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/bxslider/jquery.bxslider.js"></script>-->
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/bxslider/jquery.bxslider.min.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jscrollpane/jquery.jscrollpane.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jscrollpane/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jscrollpane/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jscrollpane/mwheelIntent.js"></script>


<div role="main"><span id="maincontent"></span>
  <div class = "admin-dashboard content-upper" style = "height: 200px;">
    <div class = "course-assign-status admin-course">
      <div class = "course_status_block course-assign-status-completed full"> <span class = "course_status_block_span course-assign-status-heading"><?php echo get_string('learners','admindashboard');?></span> <span class = "course_status_block_span course-assign-status-heading-value"><a href = "<?php echo $CFG->wwwroot;?>/admin/user.php?role=5" title="<?php echo get_string('learners','admindashboard');?>" ><?php 
	  $users = get_users_listing('usercount', '', 1, 0, '', '','','',array('is_active'=>1));
	  echo $users->usercount;?></a></span> </div>
      <div class = "course_status_block course-assign-status-inprogress half"> <span class = "course_status_block_span course-assign-status-heading"><?php echo get_string('departments','admindashboard');?></span> <span class = "course_status_block_span course-assign-status-heading-value"><a href = "<?php echo $CFG->wwwroot;?>/department/index.php" title="<?php echo get_string('departments','admindashboard');?>"><?php echo getDepartmentCountByUser();?></a></span> </div>
      <div class = "course_status_block course-assign-status-notstarted half"> <span class = "course_status_block_span course-assign-status-heading"><?php echo get_string('courses','admindashboard');?></span> <span class = "course_status_block_span course-assign-status-heading-value"><a href = "<?php echo $CFG->wwwroot;?>/course/index.php" title="<?php echo get_string('courses','admindashboard');?>"><?php echo countCourseListingResult(array('is_active'=>1));?></a></span> </div>
    </div>
    <div class = "last-accessed-courses slider">
      <div>
        <div class="bxslider">
          <?php
	 
	    $depArr = getDepartmentsForAdminDashboard();
		//pr($depArr);
		$depCnt = count($depArr);
		if($depCnt > 0){
		
			foreach($depArr as $deps){
			
				$picture = $deps->picture;
				$dname = $deps->name;
				$depLearnerCount = isset($deps->depLearners)?count($deps->depLearners):0;
				$depCoursesCount = isset($deps->depCourses)?count($deps->depCourses):0;
				$depsImage = getDepartmentImage($deps);
				
				?>
          <div class = "course_block">
            <div class = 'course_details_block'>
              <div class = 'course_data_block'>
                <div class = 'course_image_block'><?php echo $depsImage;?> </div>
                <div class = "course_data">
                  <div class="course_name word-wrap"><?php echo $dname && strlen($dname) > 18?substr($dname,0,18).'...':$dname;?></div>
                  <!--<div class="course_name"><?php //echo $dname;?></div>-->
                  <div class="course_status"></div>
                </div>
              </div>
            </div>
            <div class = "course_launch_link">
              <div class="leftlearner">
                <div><?php echo get_string('learners','admindashboard');?></div>
                <div class="teamcount"><?php echo $depLearnerCount?$depLearnerCount:0;?></div>
              </div>
              <div class="rightcourse">
                <div><?php echo get_string('courses','admindashboard');?></div>
                <div class="teamcount"><?php echo $depCoursesCount?$depCoursesCount:0;?></div>
              </div>
              <div class = "new-course-preview-link" style="float:left"> <a href="<?php echo $CFG->wwwroot;?>/department/index.php" class="course_launch launch_course" view = "normal" title="<?php echo get_string('clicktomanage','admindashboard');?>"><?php echo get_string('clicktomanage','admindashboard');?></a> </div>
            </div>
          </div>
          <?php
			}
		 	
	?>
          <div class = "course_block"><a href="<?php echo $CFG->wwwroot;?>/department/add_department.php"><img src="<?php echo $CFG->wwwroot;?>/theme/gourmet/pix/add-new-department.jpg" alt="<?php echo get_string("adddepartment","department");?>" title="<?php echo get_string("adddepartment","department");?>" border="0" /></a></div>
        </div>
        <?php }else{ ?>
        <!--div class = "no-reults"> <?php //echo get_string("no_results"); ?> </div-->
        <div class = "course_block"><a href="<?php echo $CFG->wwwroot;?>/department/add_department.php"><img src="<?php echo $CFG->wwwroot;?>/theme/gourmet/pix/add-new-department.jpg" alt="<?php echo get_string("adddepartment","department");?>" title="<?php echo get_string("adddepartment","department");?>" border="0" /></a></div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<?php
$dayView = "";
$weekView = "";
$monthView = "";
switch(strtolower($calenderView)){
Case 'month':
	$monthView = " active";
	break;
Case 'day':
	$dayView = " active";
	break;
Case 'week':
	$weekView = " active";
	break;
default:
	$monthView = " active";
	break;
}

$date = date("y-m-d")." 12:00:00";
$currDateTime = strtotime($date);
?>
<div class="content-lower pull-left">
  <div class="dashboard-lower-left pull-left">
      
      <div class="lower-right-bottom">
      <div class="block-header messages-icon"><span></span><?php echo get_string('messages','admindashboard');?><a href="<?php echo $CFG->wwwroot.'/messages/add_message.php';?>" class="message-setting pull-right" title = "<?php echo get_string('new_message','messages');?>"></a> <a href="<?php echo $CFG->wwwroot.'/messages/index.php';?>" class="add-message pull-right" title = "<?php echo get_string('manage_messages',"messages");?>"></a></div>
      <div class="block-content scroll-pane">
        <?php
									
				/*$msgSql = "SELECT * from {$CFG->prefix}my_messages where deleted = 0 AND  status = 1 and message_from='".$USER->id."' ORDER BY timecreated DESC limit 0,5";
				
				$messages = $DB->get_records_sql($msgSql);*/
				$groupSql = "SELECT groupid from {$CFG->prefix}groups_members where userid='".$userId."'";
				$groupRow = $DB->get_records_sql($groupSql);
				$groupId = array();
				$teamId = "";
				if(count($groupRow)>0){
					foreach($groupRow as $groupValue){		
						$groupId[] = $groupValue->groupid; 		
					}
					$teamId =  implode(",",$groupId);
				}	
								
				$msgSql = "SELECT * from {$CFG->prefix}my_messages where deleted = 0 AND  status = 1 and ( FIND_IN_SET('".$USER->department."',department) || FIND_IN_SET('".$userId."',show_to) ||  FIND_IN_SET('".$teamId."',team)) ORDER BY timecreated DESC limit 0,5";
				
				$messages = $DB->get_records_sql($msgSql);
				
				if(count($messages)>0){
					foreach($messages as $message){
					
					    //$message_content = $message->message_content?nl2br($message->message_content):$message->message_content;
						$message_content = $message->message_title?nl2br($message->message_title):$message->message_title;
						$user = $DB->get_record('user',array('id'=>$message->message_from));
					?>
                            <div class = 'message-block'>
                              <div class = 'message-sender-image'> <?php echo $OUTPUT->user_picture($user, $userpictureparams);?> </div>
                              <div class = 'message-content'>
                                <div class = "message-sender-info"> <?php echo  ucfirst($user->firstname)." ".ucfirst($user->lastname);
															echo '&nbsp;-&nbsp;';	
                                                            echo date($CFG->customDefaultDateTimeFormat, $message->timecreated); // user M in place of F for short month name
                                                    ?> </div>
                                <div class = "message-content-details"> <a href = "<?php echo $CFG->wwwroot.'/messages/read_message.php?id='.$message->id;?>">
                                  <?php
                                                            if(strlen($message_content) > 100){
                                                                echo substr($message_content,0,100)." ...";
                                                            }else{
                                                                echo $message_content;
                                                            }
                                                        ?>
                                  </a> </div>
                              </div>
                            </div>
        <?php
					}
			}else{ ?>
                      <div class = "no-reults"> <?php echo get_string("nomessagefound"); ?> </div>
        <?php } ?>
      </div>
    </div>
      
<!--    <div class="dashboard-calender">
      <div class="block-header dashboard-calender-icon"><span></span><?php echo get_string('calendar','admindashboard');?> </div>
        <div class = "calender-block">        
        <div style="padding:10px"> <?php echo get_string('loadingcalender', 'calendar');?></div>
        
      </div>
    </div>-->
  </div>
  <div class="dashboard-lower-right pull-right">
    <div class="lower-right-upper">
      <div class="block-header newcourse-icon"><span></span><?php echo get_string('newcourses','admindashboard');?> <a href="<?php echo $CFG->wwwroot.'/course/edit.php';?>" class="message-setting pull-right" title = "<?php echo get_string('addcourse','course');?>">Add</a>&nbsp; <a href="<?php echo $CFG->wwwroot.'/course/index.php';?>" class="add-message pull-right" title = "<?php echo get_string('managecourses');?>">Manage</a> &nbsp;<a href="<?php echo $CFG->wwwroot.'/my/course_catalog.php';?>" class="course_catalog pull-right" title = "<?php echo get_string('course_catalog','menubar');?>">Catalog</a></div>
      <div class="block-content">
        <div class="slider-new-courses">
        <?php echo getDashboardNewCoursesHtml();?>
          <?php /*if(count($newCourseByUserForDashboard) > 0 ){ ?>
          <div class="bxslider1">
            <?php
			
				foreach($newCourseByUserForDashboard as $newCourse){
					
				    $fullname = $newCourse->fullname;
					$newCourse->name = $newCourse->fullname;
					$newCourseImage = getCourseImage($newCourse);

				?>
            <a href="<?php echo $CFG->wwwroot;?>/course/view.php?id=<?php echo $newCourse->id;?>" class="newcoursebox">
            <div class="new-course-block" >
              <div class="new-course-image"> <?php echo $newCourseImage;?></div>
              <div class="new-course-details">
                <div class="new-course-name word-wrap"><?php echo $fullname && strlen($fullname) > 38?substr($fullname,0,38).'...':$fullname;?></div>
                <div class="new-course-category"><?php echo 'Category: '.$newCourse->categoryname;?></div>
                <?php

					//$fileStatus = '<a href="'.$CFG->wwwroot.'/course/edit.php?id='.$newCourse->id.'" class="course_launch launch_course"  view = "normal">'.get_string('clicktomanage','admindashboard').'</a>';
					$fileStatus = ''.get_string('clicktomanage','admindashboard').'';
					
							?>
                <div class = "new-course-preview-link"> <?php echo $fileStatus; ?> </div>
              </div>
            </div>
            </a>
            <?php 
				}
				?>
          </div>
          <?php }else{ ?>
          <div class = "no-reults"> <?php echo get_string("no_results"); ?> </div>
          <?php } */ ?>
        </div>
      </div>
    </div>
    
  </div>
</div>


<script>


$(document).ready(function(){

	var poptions = '';
	var course_url = '';
	var launch_url = '';
	$('.bxslider, .bxslider1').bxSlider({
		infiniteLoop: false,
		hideControlOnEnd: true,
		//displaySlideQty:3, 
		moveSlides: 1
	});
	

	
});



</script>