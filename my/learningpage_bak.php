<?php

	/**
		* Custom module - Learning page
		* Date Creation - 04/05/2014
		* Date Modification : 23/06/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/

	require_once(dirname(__FILE__) . '/../config.php'); 
	
	global $USER;
	$userId = $USER->id;
	
	if($USER->archetype == $CFG->userTypeAdmin){  // added by rajesh 
	   redirect($CFG->wwwroot .'/');
	}
	
	$module = 18;
	

    require_login();

    $type         = optional_param('type', 1, PARAM_ALPHANUM);
	$delete       = optional_param('delete', 0, PARAM_INT);
    $confirm      = optional_param('confirm', '', PARAM_ALPHANUM);   //md5 confirmation hash
    $sort         = optional_param('sort', 'id', PARAM_ALPHANUM);
    $dir          = optional_param('dir', 'DESC', PARAM_ALPHA);
    $page         = optional_param('page', 1, PARAM_INT);
    $perpage      = optional_param('perpage', 10, PARAM_INT);        // how many per page
	
	
	$paramArray = array(
	'type' => $type
    );
  
	
	$removeKeyArray = array('perpage');
	
	$pageURL = $_SERVER['PHP_SELF'];
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);

    $header = $SITE->fullname.": ".get_string('mylearningpage', 'learnercourse');
    $PAGE->set_title($header);
	$PAGE->set_heading($header);
	$PAGE->set_pagelayout('mydashboard');
    $stredit   = get_string('edit');
    $strdelete = get_string('delete');
    $strdeletecheck = get_string('deletecheck');
    $strconfirm = get_string('confirm');
	
	
	$PAGE->navbar->add(get_string('mylearningpage','menubar'));
	

    if (empty($CFG->loginhttps)) {
        $securewwwroot = $CFG->wwwroot;
    } else {
        $securewwwroot = str_replace('http:','https:',$CFG->wwwroot);
    }

    $returnurl = new moodle_url('/my/learningpage.php', array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'page'=>$page, 'type' => $type));

    // create the user filter form
	
    //$lcfiltering = new learnercourse_filtering();
	
	//$fff = getCourseIdByStatus($userId);
     //pr($fff);die;

    //$arr = getAssignedUserOfProgram($pid=1);
	//pr($arr);die;
    echo $OUTPUT->header();

    $columns = array('scormname', 'scormsummary', 'lastaccessed', 'timespent', 'score');

    foreach ($columns as $column) {
        $string[$column] = getLearnerCourseFieldName($column);
        if ($sort != $column) {
            $columnicon = "";
            if ($column == "timecreated") {
                $columndir = "DESC";
            } else {
                $columndir = "ASC";
            }
        } else {
            $columndir = $dir == "ASC" ? "DESC":"ASC";
            if ($column == "timecreated") {
                $columnicon = ($dir == "ASC") ? "sort_desc" : "sort_asc";
            } else {
                $columnicon = ($dir == "ASC") ? "sort_asc" : "sort_desc";
            }
            $columnicon = "<img class='iconsort' src=\"" . $OUTPUT->pix_url('t/' . $columnicon) . "\" alt=\"\" />";

        }
		
        if(in_array($column, array('scormname', 'scormsummary', 'lastaccessed', 'timespent', 'score'))){
          $$column = $string[$column];
		}else{
		  $$column = "<a href=\"learningpage.php?sort=$column&amp;dir=$columndir&amp;type=$type\">".$string[$column]."</a>$columnicon";
		}
    }

    //list($extrasql, $params) = $lcfiltering->get_sql_filter();
	
	$extrasql='';
	$params=array();
	$_SESSION['extrasql'] = '';
	//require_once("../local/searchForm.php");
	$extrasql =  $_SESSION['extrasql'];//die;
	
	
    //$learnercourses = getLearnerCoursesListing($module, $sort, $userId, $dir, $page*$perpage, $perpage, '', $extrasql, $params, $type);
	//$learnercoursecount = getLearnerCourses($module, $userId, $type);
	//$learnercoursesearchcount = count($learnercourses);
	   
   /* if ($extrasql !== '') {
        echo $OUTPUT->heading("$learnercoursesearchcount / $learnercoursecount ".get_string('learnercourse','learnercourse'));
        $learnercoursecount = $learnercoursesearchcount;
    } else {
        echo $OUTPUT->heading("$learnercoursecount ".get_string('learnercourse','learnercourse'));
    }*/

    //$strall = get_string('all');

    /*$baseurl = new moodle_url('/my/learningpage.php', array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'type' => $type));
    echo $OUTPUT->paging_bar($learnercoursecount, $page, $perpage, $baseurl);

    flush();
    */


     $isReport = false;
	 $courseStatusArr = getCourseIdByStatus($userId, '', '', $isReport);
	 //pr( $courseStatusArr);die;
           $courseCnt = isset($courseStatusArr['type'][$type])?count($courseStatusArr['type'][$type]):0;
	    arraySortByColumn($courseStatusArr['courses'], $sort, SORT_DESC);
		
		$courseSArr = array();
		if(count($courseStatusArr['courses']) > 0 ){
			foreach($courseStatusArr['courses'] as $key=>$valArr){
			   $courseSArr['courses'][$valArr['courseId']] =  $valArr;
			}
		}

		$courseArr = $courseStatusArr['type'][$type];
	
	 //$modulename = getModuleName($module); 
	 //$courseArr = getAssignedCourseForUser($module, $sort, $userId, $dir, $page, $perpage, '', $extrasql, $params, $type);
	 //$courseCnt = getAssignedCourseForUserCount($module, $userId, $type);
	  
	 //$baseurl = new moodle_url('/my/learningpage.php', array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'type' => $type));
	 
	

      //echo $OUTPUT->paging_bar($courseCnt, $page, $perpage, $baseurl);
	   //flush();
	

     $styTabs1 = '';
	 $styTabs2  = '';
	 $styTabs3 = '';
	 if($type == 1){
       $styTabs1 = "current";
     }
	 
	 if($type == 2){
       $styTabs2 = "current";
     }
	 
	 if($type == 3){
       $styTabs3 = "current";
     }
	 
	$courseTabs = '';
	$html = '';
	$outerDivStart = "<div class='tabsOuter'>";
	$outerDivEnd = "</div>";
	
	$courseTabs .= "<div class='tabLinks'>";

	$courseTabs .= "<div><a href='".$CFG->wwwroot."/my/learningpage.php?type=1' title='".get_string('inprogress','learnercourse')."' class='$styTabs1'>".get_string('inprogress','learnercourse')."</a></div>";
	$courseTabs .= "<div><a href='".$CFG->wwwroot."/my/learningpage.php?type=2' title='".get_string('completed','learnercourse')."' class='$styTabs2' >".get_string('completed','learnercourse')."</a></div>";
    $courseTabs .= "<div><a href='".$CFG->wwwroot."/my/learningpage.php?type=3' title='".get_string('notstarted','learnercourse')."' class='$styTabs3'>".get_string('notstarted','learnercourse')."</a></div>";
	$courseTabs .= "</div>";
		
		
	 $html = $outerDivStart.$courseTabs;
	 $htmlDiv = '';
	 $html .= html_writer::start_tag('div', array('class'=>'no-overflow'));
	 if ($courseCnt > 0) {

            $i=0;
			//pr( $courseArr);die;
			
			if($perpage){
			  $offset = $page - 1;
			  $offset = $offset*$perpage;
			  $courseArr = array_splice($courseArr, $offset, $perpage);
			}
				
			foreach ($courseArr as $courseId) {
			
				$i++;
				$classAlt = $i%2==0?'even-course':'odd-course';
				
				
				$courseformat = $courseSArr['courses'][$courseId]['courseformat'];
				$fullname = $courseSArr['courses'][$courseId]['fullname'];
				$summary = $courseSArr['courses'][$courseId]['summary'];
				$summary = strip_tags($summary);
				$summary = strlen($summary)>150?substr($summary,0,150).'...':$summary;
				
				
				/*$courseformat = $courses->courseformat;
				$fullname = $courses->fullname;
				$summary = strip_tags($courses->summary);
				$summary = strlen($summary)>150?substr($summary,0,150).'...':$summary;
				*/
				if($i == count($courseArr)){
				  $htmlDiv = '<div class="no-course '.$classAlt.'" rel="'.$courseId.'">';
				}else{
				  $htmlDiv = '<div class="no-course '.$classAlt.'" rel="'.$courseId.'">';
				}
				
				  $htmlDiv .= '<div class="pull-left left-icon"><span></span><img alt="" src="'.$CFG->wwwroot.'/theme/image.php?theme=gourmet&component=core&image=a%2Fview_icon_active"></div>';
				  $htmlDiv .= '<div class="pull-left left-content">';
				  $courseInc = $i;
					$htmlDiv .= '<div class="c-heading">'.get_string('course','learnercourse').$courseInc.': '.$fullname.'</div>';
					$htmlDiv .= '<div class="c-text">'.get_string('description','learnercourse').': '.$summary.'</div>';
				  $htmlDiv .= '</div>';
				  $htmlDiv .= '<div class="pull-left right-icon cbox" rel="'.$courseId.'" id="plus-minus-icon-'.$courseId.'"><a href="javascript:void(0)"></a><img alt="" src="'.$CFG->wwwroot.'/theme/image.php?theme=gourmet&component=core&image=a%2Fadd_file"></div>';
		
					if($courseformat == 'singleactivity'){
	
							    /*$courseDiv = '';
							    $courseDiv .= '<div class="d-box" id="dbox'.$courseId.'">';
				   
								$table = new html_table();
				
								$table->head = array ();
								$table->colclasses = array();
								$table->head[] = $scormname;
								$table->attributes['class'] = 'admintable generaltable';
								$table->colclasses[] = 'leftalign  w150';
								$table->head[] = $scormsummary;
								$table->colclasses[] = 'leftalign w350';
								//$table->head[] = $timespent;
								$table->head[] = get_string('currentstatus','learnercourse');
								$table->colclasses[] = 'leftalign';
								$table->head[] = $score;
								$table->colclasses[] = 'leftalign';
								$table->head[] = $lastaccessed;
								$table->colclasses[] = 'leftalign ';
								$table->head[] = get_string('actions','learnercourse');
								$table->colclasses[] = 'centeralign';
								//pr($learnercourses);
								$table->id = "learnercourses";
							
								$buttons = array();
								$lastcolumn = '';
								$curtime = time();
					
								$scormid = $courses->scormid;
								$scormtitle = $courses->scormname;
								$scormdiscription = strip_tags($courses->scormsummary);
								$categoryname = $courses->categoryname;
								$timecreated = $courses->ctimecreated;
								
								//$lastAccessedDT = getScormStatusForUser($scormid, $userId, 'lastaccessed');
								$lastAccessedTime = $courses->lastaccessed;
								if($lastAccessedTime){
									$remainAccessedTime = $curtime - $lastAccessedTime;
									$lastAccessed = timePassed($remainAccessedTime);
								}else{
									 $lastAccessed = get_string('notaccessed','learnercourse');
								}
								
								$timespentVal = $courses->timespent;
								
								if($timespentVal){
								   $timeSpentResult = convertCourseSpentTime($timespentVal);
								   $timeSpentResult = str_replace(get_string('ago','learnercourse'),'',$timeSpentResult);
								}else{
									$timeSpentResult = '---';
								}   
	
								$scoreResult = $courses->score!=''?($courses->score." ".get_string('points','learnercourse')):'---';
								$scormStatus = $courses->scormstatus;
								
								 if($scormStatus == ''){
								   $scormStatus = get_string('notstarted','learnercourse');
								 }elseif($scormStatus == 'incomplete'){
								   $scormStatus = get_string('inprogress','learnercourse');
								 }elseif(in_array($scormStatus, array('passed','completed'))){
								   $scormStatus = get_string('completed','learnercourse');  
								 }elseif($scormStatus == 'failed'){
								   $scormStatus = get_string('completed','learnercourse');  
								 }
								 
								
								$scormCourseModuleId = getScormCourseModuleId($scormid, $courseId);
								
								$scormLaunch = "<a href=\"../mod/scorm/view.php?id=$scormCourseModuleId\" class = \"launch_icon\">".get_string('launch','learnercourse')."</a>";
	
								$row = array ();
								$row[] = "<a href=\"../mod/scorm/view.php?id=$scormCourseModuleId\">".$scormtitle."</a>";
								$row[] = $scormdiscription;
								//$row[] = $timeSpentResult;
								$row[] = $scormStatus;
								$row[] = $scoreResult;
								$row[] = $lastAccessed;
								$row[] = $scormLaunch;
								$table->data[] = $row;
								
								if (!empty($table)) {
									//$courseDiv .= html_writer::start_tag('div', array('class'=>'no-overflow'));
									$courseDiv .= html_writer::table($table);
									//$courseDiv .= html_writer::end_tag('div');
								}
	
						
						$courseDiv .= '</div>';*/
						
					}elseif($courseformat == 'topics'){
					
					            $courseDiv = '';
							    $courseDiv .= '<div class="d-box" id="dbox'.$courseId.'">';
								
					            $sectionArr = getTopicsSection($courseId, $userId);
								
								$sectionHtml = '';
								
								//pr($sectionArr);
								if(count($sectionArr) > 0 ){
		
								    foreach($sectionArr as $sectionId=>$sections){
									
												$table = new html_table();
				
												$table->head = array ();
												$table->colclasses = array();
												$table->head[] = get_string('title','learnercourse');
												$table->attributes['class'] = 'admintable generaltable';
												$table->colclasses[] = 'leftalign  w150';
												$table->head[] = get_string('summary','learnercourse');
												$table->colclasses[] = 'leftalign w350';
												//$table->head[] = get_string('timespent','learnercourse');
												$table->head[] = get_string('currentstatus','learnercourse');
												$table->colclasses[] = 'leftalign';
												$table->head[] = get_string('score','learnercourse');
												$table->colclasses[] = 'leftalign';
												$table->head[] = get_string('lastaccessed','learnercourse');
												$table->colclasses[] = 'leftalign ';
												$table->head[] = get_string('actions','learnercourse');
												$table->colclasses[] = 'centeralign';
												//pr($learnercourses);
												$table->id = "learnercourses";
											
												$buttons = array();
												$lastcolumn = '';
												$curtime = time();
												
									
									            $sectionDiv = '';
									           $sectionName = $sections->sectionname;
											      $sequence = $sections->sequence;
										    $sectionSummary = $sections->summary;
										      $topicsCourse = $sections->courseModule;
										   
								                $sectionHtml .= '<div class="section-box" >'; // start section div
                                                //$sectionHtml .= '<div style="float:left;width:100%;padding:10px" class="sbox"  rel="'.$sectionId.'" id="sbox'.$sectionId.'"><a href="javascript:void(0)"></a><img alt="" src="'.$CFG->wwwroot.'/theme/image.php?theme=gourmet&component=core&image=a%2Fadd_file">'.$sectionName.'</div>';
												
			
											//pr($sectionArr);die;
											//$topicsCourse = getTopicsCourse($courseId, $userId, 18);
											//$nonCourseStatusForUser = getNonCourseStatusForUser($userId);
											//pr($nonCourseStatusForUser);
											
											if(count($sections->courseModule) > 0){
											
											
											  $sectionDiv .= '<div class="c-box" id="cbox'.$sectionId.'">';
											   foreach($sections->courseModule as $cmId=>$courseModule){

												  $topicsCourse = $courseModule['topicCourse'];
												  //pr($topicsCourse);
												  if(count($topicsCourse)>0){
														   foreach($topicsCourse as $topicsCourseArr){
														   //pr($topicsCourseArr);
																$coursetype = $topicsCourseArr->coursetype;
																
																if($coursetype == 'scorm'){
																
																		$scormid = $topicsCourseArr->scormid;
																		$scormtitle = $topicsCourseArr->scormname;
																		$scormdiscription = strip_tags($topicsCourseArr->scormsummary);
																		$categoryname = $topicsCourseArr->categoryname;
																		$timecreated = $topicsCourseArr->ctimecreated;
																		$timespentVal = $topicsCourseArr->timespent;
																		$coursesScore = $topicsCourseArr->score;
																		$scormStatus = $topicsCourseArr->scormstatus;
																		$lastAccessedTime = $topicsCourseArr->lastaccessed;
																		if($lastAccessedTime){
																			$remainAccessedTime = $curtime - $lastAccessedTime;
																			$lastAccessed = timePassed($remainAccessedTime);
																		}else{
																			 $lastAccessed = get_string('notaccessed','learnercourse');
																		}
										
																		if($timespentVal){
																		   $timeSpentResult = convertCourseSpentTime($timespentVal);
																		   $timeSpentResult = str_replace(get_string('ago','learnercourse'),'',$timeSpentResult);
																		}else{
																			$timeSpentResult = '---';
																		}   
											
																		$scoreResult = $coursesScore!=''?($coursesScore." ".get_string('points','learnercourse')):'---';
																		
																		
																		if($scormStatus == ''){
																		   $scormStatus = get_string('notstarted','learnercourse');
																		 }elseif($scormStatus == 'incomplete'){
																		   $scormStatus = get_string('inprogress','learnercourse');
																		 }elseif(in_array($scormStatus, array('passed','completed'))){
																		   $scormStatus = get_string('completed','learnercourse');  
																		 }elseif($scormStatus == 'failed'){
																		   $scormStatus = get_string('completed','learnercourse');  
																		 }
																		 
																		
																		$scormCourseModuleId = getScormCourseModuleId($scormid, $courseId);
																		$scodata = $DB->get_record_sql("SELECT * FROM mdl_scorm_scoes WHERE scorm = $scormid ORDER BY id DESC");
																		$scormdata = $DB->get_record('scorm',array('id'=>$scormid));
																		$launchurl = $CFG->wwwroot.'/mod/scorm/player.php?a='.$scormid.'&cm='.$scormCourseModuleId.'&scoid='.$scodata->id.'&currentorg='.$scodata->organization.'&newattempt=on&display=popup&mode=normal';
																		
																		//$scormLaunch = "<a href=\"../mod/scorm/view.php?id=$scormCourseModuleId\" class = \"launch_icon\">".get_string('launch','learnercourse')."</a>";

																		$scormLaunch = "<a href=\"javascript:void(0);\" onclick=\"window.open('".$launchurl."', '', 'width=".$scormdata->width.",height=".$scormdata->height.",toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \"  title='".get_string('launch','learnercourse')."' class = \"launch_icon\">".get_string('launch','learnercourse')."</a>";

																		$row = array ();
											
																		//$row[] = "<a href=\"../mod/scorm/view.php?id=$scormCourseModuleId\">".$scormtitle."</a>";
																		$row[] = "<a href=\"javascript:void(0);\" onclick=\"window.open('".$launchurl."', '', 'width=".$scormdata->width.",height=".$scormdata->height.",toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \" title='".$scormtitle."' >".$scormtitle."</a>";
																		$row[] = $scormdiscription;
																		//$row[] = $timeSpentResult;
																		$row[] = $scormStatus;
																		$row[] = $scoreResult;
																		$row[] = $lastAccessed;
																		$row[] = $scormLaunch;
																		$table->data[] = $row;
																		
																}elseif($coursetype == 'resource'){
																  
																  
																	$resourceCourseId = $topicsCourseArr->id;
																	$resourceid = $topicsCourseArr->resourceid;
																	$cmid = $topicsCourseArr->cmid;
																	
																	$resoursename = $topicsCourseArr->resoursename;
																	$resourcesummary = strip_tags($topicsCourseArr->resourcesummary);
																	$categoryname = $topicsCourseArr->categoryname;
																	$lastAccessedTime = $topicsCourseArr->lastaccessed;
																	
																	$lastaccessed = $lastaccessed?getDateFormat($lastaccessed, "d/m/Y"):'---';
																	if($lastAccessedTime){
																		$remainAccessedTime = $curtime - $lastAccessedTime;
																		$lastAccessed = timePassed($remainAccessedTime);
																	}else{
																		 $lastAccessed = get_string('notaccessed','learnercourse');
																	}
																						
																
																   $styDownload = '';
																   
																   $filedownloaded = isUserAccessedCourse($userId, $courseId, $resourceid);
																   
																   if($filedownloaded){
																	 $styDownload = "downloaded";
																   }
																   
																   if($styDownload){
																     $resourceStatus = get_string('downloaded','learnercourse');
																   }else{
																     $resourceStatus = get_string('notdownloaded','learnercourse');
																   }
																   
																
														
																	if (!$cm = get_coursemodule_from_id('resource', $cmid)) {
																		resource_redirect_if_migrated(0, $id);
																		print_error('invalidcoursemodule');
																	}
																	
																	
																	$resource = $DB->get_record('resource', array('id'=>$cm->instance), '*', MUST_EXIST);
																	$context = context_module::instance($cm->id);
																	$fs = get_file_storage();
																	$files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
														 
																	if (count($files) < 1) {
																	    $course = $DB->get_records('course',array('id'=>$courseId));
																		resource_print_filenotfound($resource, $cm, $course);
																		die;
																	} else {
																		$file = reset($files);
																		unset($files);
																	}
																	$path = '/'.$context->id.'/mod_resource/content/'.$resource->revision.$file->get_filepath().$file->get_filename();
																	$fullurl = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
														   
														   
														   
															
															
															$fileStatus = "<a href='javascript:;' onclick=\"setCourseLastAccess($userId, $courseId, $resourceid);window.open('".$fullurl."', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \" title='".get_string('download','learnercourse')."' class='download_icon ".$styDownload."'   id='coursestatus_".$courseId."_".$resourceid."'>".get_string('download','learnercourse')."</a>";
															
														   	      
															
																	$row = array();
																	
																	$row[] = "<a href='javascript:;' onclick=\"setCourseLastAccess($userId, $courseId, $resourceid);window.open('".$fullurl."', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \" title='".$resoursename."'>".$resoursename."</a>";
																	$row[] = $resourcesummary;
																	$row[] = $resourceStatus;
																	$row[] = get_string('NA','learnercourse');
																	$row[] = $lastAccessed;
																	$row[] = $fileStatus;
																	$table->data[] = $row;
																	
																}
																
															
														 } // end foreach for topicsCourse	
								
														 } // end if for topicsCourse
											      }
												  
												   
												   if (!empty($table)) {
														$sectionDiv .= html_writer::table($table);
												   }
												   
												   $sectionDiv .= '</div>';
												   $sectionHtml .= $sectionDiv;
											
											}

									        $sectionHtml .= '</div>'; // end section html
										
									} // end sectionArr foreach
									
									$courseDiv .= $sectionHtml; 
									
								} // end sectionArr if
								
								
	
						
						$courseDiv .= '</div>';
						
				    }
					
		
					//$htmlDiv .= $courseDiv.'</div>';
					$htmlDiv .= '</div>';
					$htmlDiv .= $courseDiv;
					
					$html .= $htmlDiv;
				
			}
			

	  }else{
	     //$html .= $OUTPUT->heading(get_string('nolearnercoursesfound','learnercourse'));
	     $html .= $OUTPUT->heading(get_string('no_results'));
	  }
	  
	  $html .= html_writer::end_tag('div');
      //$html .= $OUTPUT->paging_bar($courseCnt, $page, $perpage, $baseurl);
	  $html .= paging_bar($courseCnt, $page, $perpage, $genURL);
	 // $html .= $pagingHTML;
       
      
	  $html .= $outerDivEnd;
	  
	  echo $html;
	
    /*if (!empty($table)) {
        echo html_writer::start_tag('div', array('class'=>'no-overflow'));
        echo html_writer::table($table);
		if (count($courseArr) == 0) {
			echo $OUTPUT->heading(get_string('nolearnercoursesfound','learnercourse'));
        } 
        echo html_writer::end_tag('div');
        echo $OUTPUT->paging_bar($learnercoursecount, $page, $perpage, $baseurl);
       
    }*/
	
	
    //pr($SESSION->learnercourse_filtering);
	
 ?>
 
 <script type="text/javascript">
 
 
 function setCourseLastAccess(userid, courseid, resourceid){
	
     
	   $.ajax({
	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=setCourseLastAccess&userid='+userid+"&courseid="+courseid+"&resourceid="+resourceid,
		dataType:'json',
		success:function(data){
		  
		   var success = data.success;
		     var error = data.error;
			   if(success == 1){
			      var course_resource_id = data.response;
				  $('#coursestatus_'+course_resource_id).addClass('downloaded');
			   }else{
				 alert(error);
			   }
		}
	  
	  });
	  $.ajax({
	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=setCourseBadge&userid='+userid+"&courseid="+courseid+"&resourceid="+resourceid,
		dataType:'json',
		success:function(data){  
		}
	  
	  });
	     
	
 }
	
 function showHideButton(){
      $('a.moreless-toggler').addClass('moreless-toggler moreless-less');
	  $('a.moreless-toggler').html('<?php echo get_string('showless','form');?>');
	  
	  $('div.fitem_fselect').addClass('fitem advanced fitem_fselect show');
	  $('div.fitem_fgroup').addClass('fitem advanced fitem_fgroup show');
	   
 }
 
 
 $(window).load(function () {
     <?php if(isset($SESSION->learnercourse_filtering['categoryname']) || isset($SESSION->learnercourse_filtering['timecreated'])){ ?>
	      //  showHideButton();
	<?php } ?>
 });
 
 $(document).ready(function(){
 
      $('.no-course').click(function(){
	         var courseId = $(this).attr('rel');
			 $('.no-course').removeClass('selected');
			 $('.cbox').not('[rel="'+courseId+'"]').removeClass('minus');
			 var hasClass = $('#plus-minus-icon-'+courseId).hasClass('minus');
			 if(hasClass){
			   $('#plus-minus-icon-'+courseId).removeClass('minus');
			 }else{
			   $('#plus-minus-icon-'+courseId).addClass('minus');
			 }
			 
			//$('.d-box').hide();
			 
			 
			 $('.d-box').not('#dbox'+courseId).hide();
			 
			 if($('#dbox'+courseId).is(':visible')){
			    $('#dbox'+courseId).hide();
			 }else{
			   $('#dbox'+courseId).show();
			 }
			 
			 $(this).addClass('selected');
	         
			 
			 //$('.cbox').not('[rel="'+courseId+'"]').removeClass('minus');
			 //$('.cbox').toggleClass('minus');
			// $(window).scrollTop($('#dbox'+courseId).offset().top);
	  
	  });
	  
	   /*$('.sbox').click(function(){
	         var sectionId = $(this).attr('rel');
			 $('[id^="cbox"]').not('#cbox'+sectionId).hide();
	         $('#cbox'+sectionId).slideToggle();
			 //$(this).parent('.no-course').find('.right-icon').toggleClass('minus');
			// $(window).scrollTop($('#dbox'+courseId).offset().top);
	  
	  });*/
	  
 });


 </script>
 <?php	

 echo $OUTPUT->footer();
 