<?php
	require_once(dirname(__FILE__) . '/../config.php'); 
	GLOBAL $CFG;
	require_once($CFG->dirroot . '/my/learninglib.php');
	require_once($CFG->dirroot.'/mod/scheduler/lib.php'); 
	global $USER;
	$userId = $USER->id;
	
	if($USER->archetype == $CFG->userTypeAdmin){
	   redirect($CFG->wwwroot .'/');
	}	

    require_login();

    $type         = optional_param('type', 1, PARAM_ALPHANUM);
    $page         = optional_param('page', 1, PARAM_INT);
    $perpage      = optional_param('perpage', $CFG->perpage, PARAM_INT);
	
	$paramArr = array();
	 
	
	$removeKeyArray = array('perpage');
	
	/************************* Header Information start ***************************************/

    $header = $SITE->fullname.": ".get_string('mylearningpage', 'learnercourse');
    $PAGE->set_title($header);
	$PAGE->set_heading($header);
	$PAGE->set_pagelayout('mydashboard');
    $stredit   = get_string('edit');
    $strdelete = get_string('delete');
    $strdeletecheck = get_string('deletecheck');
    $strconfirm = get_string('confirm');
	$PAGE->navbar->add(get_string('mylearningpage','menubar'));
    echo $OUTPUT->header();
	
	/************************* Header Information start ***************************************/
	Global $DB,$CFG,$USER;
	$all = 0;
	$program = 0;
	$course = 0;
	$classroom = 0;
	$expired = 0;
	$active = 0;
        
        $course_cat_id = 0;
        if(isset($_REQUEST['course_cat_id']) && $_REQUEST['course_cat_id'] != ''){
                    $course_cat_id = $_REQUEST['course_cat_id'];
        }
                
	if(!isset($_REQUEST['expired']) && $type != 2){
		$active = 1;
	}
	/*if(isset($_REQUEST['all']) && $_REQUEST['all'] =='all'){
			$all = 1;
	}else{*/
		if(isset($_REQUEST['program']) && $_REQUEST['program'] == 'program' ){
			$program = 1;
		}
		if(isset($_REQUEST['course']) && $_REQUEST['course'] == 'course'){
			$course = 1;
		}
		if(isset($_REQUEST['classroom']) && $_REQUEST['classroom'] == 'classroom'){
			$classroom = 1;
		}
		if(isset($_REQUEST['expired']) && $_REQUEST['expired'] == 'all'){
			$all = 1;
			$paramArr['all'] = 1;
		}
		if(isset($_REQUEST['expired']) && $_REQUEST['expired'] == 'expired'){
			$expired = 1;
		}
		if(isset($_REQUEST['expired']) && $_REQUEST['expired'] == 'active'){
			$active = 1;
		}
		if(isset($_REQUEST['in_progress']) && $_REQUEST['in_progress'] == 'in_progress'){
			$in_progress = 1;
		}
		if(isset($_REQUEST['not_started']) && $_REQUEST['not_started'] == 'not_started'){
			$not_started = 1;
		}
		$paramArr['showClasses'] = 'all';
		$showClasses = 'all';
		$showClassStatus = 'all';
		if($active == 1){
			$showClassStatus = 'active';
		}elseif($expired == 1){
			$showClassStatus = 'expired';
		}
		if($in_progress == 1 && $not_started == 1){
			$showClasses = 'all';	
		}elseif($in_progress == 1){
			$paramArr['showClasses'] = 'in_progress';
			$showClasses = 'in_progress';
		}elseif($not_started == 1){
			$paramArr['showClasses'] = 'not_started';
			$showClasses = 'not_started';
		}
	/*}*/
	$paramArray = array(
					'all' => $_REQUEST['all'],
					'program' => $_REQUEST['program'],
					'course' => $_REQUEST['course'],
					'classroom' => $_REQUEST['classroom'],
					'expired' => $_REQUEST['expired'],
					'showClasses' => $showClasses,
					'key' => $_REQUEST['key'],
					'type' => $type,
                                        'course_cat_id' => $course_cat_id
				);
       
	$removeKeyArray = array('perpage');
	$pageURL = $_SERVER['PHP_SELF'];
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
	$searchText = '';
	$searchTextField = '';
	if(isset($_REQUEST['key']) && trim($_REQUEST['key']) !=''){
			$searchTextField = $_REQUEST['key'];
			$searchText = addslashes(strtolower(trim($_REQUEST['key'])));
	}
	$where = "";
	/*if($all == 1){
		$where = "";
	}else{*/
		/*if( (($program == 1) && ($course == 1) && ($classroom == 1) && ($expired == 1)&& ($active == 1)) || ($program == 0 && $course == 0 && $classroom == 0 && $expired == 0 && $active == 0 && $all == 0) ){
			$where = "";
		}else{*/
			$whereSql = '';
			if($program == 1){
				$whereSql .= " v.coursetype_id = 0 OR";
				$paramArr['program'] = 1;
			}
			if($course == 1){
				$whereSql .= " v.coursetype_id = 1 OR";
				$paramArr['course'] = 1;
			}
			if($classroom == 1){
				$whereSql .= " v.coursetype_id = 2 OR";
				$paramArr['classroom'] = 1;
			}
			$andClause = "";
			if($expired == 1){
				$paramArr['expired'] = 1;
			    // $andClause .= "AND (((c.enddate+86399) <=".strtotime('now').") || (end_date IS NOT NULL AND end_date != 0 AND (end_date+86399) <=".strtotime('now').")) "; 
				  $andClause .= "AND (((c.enddate+86399) <=".strtotime('now').") || (end_date IS NOT NULL AND end_date != 0 AND (end_date+86399) <=".strtotime('now').")) ";
			}
			
			
			if($active == 1){
				$paramArr['active'] = 1;
				//$andClause .= "AND (end_date IS NULL || end_date = 0 || (end_date+86399) >=".strtotime('now').") ";
				//$andClause .= "AND (((c.enddate+86399) >=".strtotime('now').") AND (end_date IS NULL || end_date = 0 || (end_date+86399) >=".strtotime('now').") )";
				
		    	 $andClause .= "AND ( if(v.program_id = 0 , ( c.enddate+86399 >=".strtotime('now')." AND (end_date IS NULL || end_date = 0 || end_date+86399 >=".strtotime('now').") ) , (end_date IS NULL || end_date = 0 || end_date+86399 >=".strtotime('now').") ) )";

			}
			
			if($course_cat_id!='' || $course_cat_id!=0)
                        {
                            $paramArr['course_cat_id'] = $course_cat_id;
                        }	
			
			if($whereSql != ''){
				$where .= "(";
				$selectSql = substr($whereSql, 0, -2);
				$where .= $selectSql.")";
				$where .= " ".$andClause;
			}else{
				$where .= substr($andClause, 4);
			}
		/*}*/
	/*}*/
	if($searchText != ''){
		if($where != ''){
			$where .= " AND ";
		}
		$paramArr['searchText'] = $searchText;
		$where .= " (LOWER(v.program_name) LIKE '%".$searchText."%' || LOWER(v.program_description) LIKE '%".$searchText."%' || LOWER(v.course_name) LIKE '%".$searchText."%' || LOWER(v.course_description) LIKE '%".$searchText."%' || LOWER(v.learningobj) LIKE '%".$searchText."%' || LOWER(v.performanceout) LIKE '%".$searchText."%')";
	}
        
        $courseProgramList = get_learning_list_optimized2($sort, $type, $page, $perpage, 'all', $paramArr);
	//$courseProgramList = get_learning_list($sort,$type,$page,$perpage,'all',$paramArr);
	$courseProgramCount = 0;//get_learning_list('count',$type,$page,$perpage,'all',$paramArr);
       // pr($courseProgramList);die;
        $styTabs1 = '';
	$styTabs2  = '';
	$styTabs3 = '';
	if($type == 1){
		$styTabs1 = "current";
	}

	if($type == 2){
		$styTabs2 = "current";
	}

	if($type == 3){
		$styTabs3 = "current";
	}
	 
	$courseTabs = '';
	$html = '';
	//pr($courseProgramList);die;
	$outerDivStart = "<div class='tabsOuter'>";
	$outerDivEnd = "</div>";
	$headerLegendDiv ='<div id="after-common-search"><div class="box"><span class="greenish"></span>'.get_string("classroomcourse").'</div><div class="box"><span class="redish"></span>'.get_string("filter_course").'</div><div class="box"><span class="bluish"></span>'.get_string("filter_program").'</div></div>';
	
		$courseTabs .= "<div class='tabLinks'>";
			$courseTabs .= "<div><a href='".$CFG->wwwroot."/my/learningpage.php?type=1' title='".get_string('todo','learnercourse')."' class='$styTabs1'>".get_string('todo','learnercourse')."<span></span></a></div>";
			$courseTabs .= "<div><a href='".$CFG->wwwroot."/my/learningpage.php?type=2' title='".get_string('completed','learnercourse')."' class='$styTabs2' >".get_string('completed','learnercourse')."<span></span></a></div>";
			 
			$isBelongToED = isLoginUserBelongsToED(); 
			if($CFG->allowExternalDepartment == 1 && $isBelongToED){
			   if($CFG->openAvailableCourseSection == 1 ){
			     $courseTabs .= "<div><a href='".$CFG->wwwroot."/my/course_catalog.php' title='".get_string('available','learnercourse')."' class='$styTabs3'>".get_string('available','learnercourse')."<span></span></a></div>";
			   }
			}else{
			     $courseTabs .= "<div><a href='".$CFG->wwwroot."/my/course_catalog.php' title='".get_string('available','learnercourse')."' class='$styTabs3'>".get_string('available','learnercourse')."<span></span></a></div>";
			}
		$courseTabs .= "</div>";
	$html = $outerDivStart.$courseTabs;
		$headerFilterDiv = '<div id="common-search" class ="learning-search tabbedSec"  style="clear: both;">';
				$headerFilterDiv .= '<form method="get" action="/my/learningpage.php" id="searchForm" name="searchForm">';
				$headerFilterDiv .=	'<div id="search-form">';
				$headerFilterDiv .=	'<input type = "hidden" name = "type" value = "'.$type.'">';
					$headerFilterDiv .= '<div class="filter learningPage"><div>';
						$headerFilterDiv .= '<span>Filter By:</span><!--span><input type="checkbox" value="all" id="all" name="all" '.( (trim($_REQUEST['all']) == 'all') ? 'checked' : '').' />All</span-->';
						if($type !=2){
						
						    $headerFilterDiv .= '<span><input type="radio" value="all" id="all" name="expired" '.( ($all == 1) ? 'checked' : '').' />All</span>';
							$headerFilterDiv .= '<span><input type="radio" value="expired" id="expired" name="expired" '.( ($expired == 1) ? 'checked' : '').' />Expired</span>';
							$headerFilterDiv .= '<span><input type="radio" value="active" id="active" name="expired" '.( ($active == 1) ? 'checked' : '').' />Active</span>';
							
						}
						$headerFilterDiv .= '<span><input type="checkbox" value="program" id="program" name="program" '.( (trim($_REQUEST['program']) == 'program') ? 'checked' : '').' />'.get_string("filter_program").'</span>';
						$headerFilterDiv .= '<span><input type="checkbox" value="course" id="course" name="course" '.( (trim($_REQUEST['course']) == 'course') ? 'checked' : '').' />'.get_string("filter_course").'</span>';
						$headerFilterDiv .= '<span><input type="checkbox" value="classroom" id="classroom" name="classroom" '.( (trim($_REQUEST['classroom']) == 'classroom') ? 'checked' : '').' />'.get_string("classroomcourse").'</span></div>';

						if($type !=2){
							$headerFilterDiv .= '<div class = "clear"></div><div class = "pull-left">';
							$headerFilterDiv .= '<span>Status:</span>';
							$headerFilterDiv .= '<span><input type="checkbox" value="in_progress" id="in_progress" name="in_progress" '.( ($in_progress == 1) ? 'checked' : '').' />'.get_string("course_in_progress").'</span>';
							$headerFilterDiv .= '<span><input type="checkbox" value="not_started" id="not_started" name="not_started" '.( ($not_started == 1) ? 'checked' : '').' />'.get_string("course_not_started").'</span>';
							$headerFilterDiv .= '</div>';
							
						}
                                                
                                                
                                             $headerFilterDiv .= getCourseCategoriesFilter($course_cat_id);

					$headerFilterDiv .= '</div>';
					$headerFilterDiv .=	'<div class="search-input"><input type="text" value="'.( ($searchTextField != '') ? $searchTextField : '').'" onkeypress="submitSearchForm(event)" placeholder="Search" id="key" name="key"></div>';
				$headerFilterDiv .= '</div></form>
			<div class="search_clear_button"><input type="button" title="Search" onclick="document.searchForm.submit();" value="Search" name="search"><a href="javascript:void(0);" id="clear" title='.get_string("clear").' onClick="clearSearchBox();">'.get_string("clear").'</a>';
			$headerFilterDiv .='</div></div>';
    $html .= $headerFilterDiv;
	$htmlDiv = '';
	
	
	$html .= html_writer::start_tag('div', array('class'=>'borderBlockSpace', 'style'=>"clear: both;"));
	if($_SESSION['update_msg']!=''){
		$html .= '<div class="clear"></div>';
		$html .= '<div class="bg-success text-success text-center '.$_SESSION['error_class'].'" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
		$html .= '<div class="clear"></div>';
		$_SESSION['update_msg']='';
		$_SESSION['error_class']='';
	}
	$html .= $headerLegendDiv;
	$cntProgramCourses =  count($courseProgramList) ;
	if (!empty($courseProgramList)) {
		$i= 0;
		$currentTime = strtotime('now');
		$k= 0;
		$data = new stdClass();
		$dataCourse = new stdClass();
		foreach ($courseProgramList as $key=>$programOrCourseArray) {
			$i++;
			$classAlt = $i%2==0?'even-course':'odd-course';
			$courseDiv = '';
			$imagePath = '';
			$refMaterial = array();
			
			$boxShadowClass = '';
			if($i < $cntProgramCourses ){
			  $boxShadowClass = 'box-shadow';
			}
			
			$classType = '';
			$rowType = "";
			$certificateLink = '';
			$courseStatusDiv = '<br />';
			if($programOrCourseArray->program_id != 0){
				//$certificateLink = '<a href="'.$CFG->wwwroot.'/certificate/certificate.php?id='.$programOrCourseArray->program_id.'&action=programme" target="_blank">Program</a>';
				$certificateLink = certificateLink($programOrCourseArray->program_id,'programme');
				$data->id=$programOrCourseArray->program_id;
				$data->name = $programOrCourseArray->program_name;
				$refMaterial = getProgramMaterials($data);
				$imagePath = '<div class = "course-img" >'.getProgramImage($data).'</div>';
				//$headingText = get_string('program','learnercourse').$i.': '.$programOrCourseArray->program_name;
				$headingText = $programOrCourseArray->program_name;
				//$summary = $programOrCourseArray->program_description;
				//$learningObj = $programOrCourseArray->learningobj;
				//$performanceOut = $programOrCourseArray->performanceout;
				
				
				$summary = strip_tags($programOrCourseArray->program_description);
				$learningObj = strip_tags($programOrCourseArray->learningobj);
				$performanceOut = strip_tags($programOrCourseArray->performanceout);
				$pcLearningObj = $learningObj?('<br /><strong>'.get_string('learningobj','course').': </strong>'.$learningObj):'';
				$performanceOut = $performanceOut?('<br /><strong>'.get_string('performanceOut','course').': </strong>'.$performanceOut):'';
				$descriptionText = $summary?'<br /><strong>'.get_string('description','learnercourse').':</strong> '.$summary:'';
				//$creditHours = array_sum(explode('_',$programOrCourseArray->credithours));
				//$creditHoursText = '<br/><strong>'.get_string('coursecredithours').'</strong>: '.setCreditHoursFormat($creditHours);
				$descriptionText = $descriptionText.$pcLearningObj.$performanceOut;
				$classType = 'border-left-blue'; 
				$rowType = "program";
				$programOrCourseArray->stat_scorm = trim($programOrCourseArray->stat_scorm);
				$programOrCourseArray->stat_resource = trim($programOrCourseArray->stat_resource);
				if(
					($programOrCourseArray->stat_scorm == '' && $programOrCourseArray->stat_resource == '')
				){
					$courseStatusDiv .= '<strong>'.get_string('status').': </strong>'.get_string('course_not_started');
				}elseif(
					($programOrCourseArray->stat_scorm == '' && !preg_match('/not started|in progress/', $programOrCourseArray->stat_resource)) ||
					($programOrCourseArray->stat_scorm == 'completed' && $programOrCourseArray->stat_resource == '') ||
					($programOrCourseArray->stat_scorm == 'completed' &&  !preg_match('/not started|in progress/', $programOrCourseArray->stat_resource)) ||
					($programOrCourseArray->stat_scorm == '' && $programOrCourseArray->stat_resource == 'completed') ||
					($programOrCourseArray->stat_resource == 'completed' &&  !preg_match('/not started|in progress/', $programOrCourseArray->stat_scorm))
					){
					$courseStatusDiv .= '<strong>'.get_string('status').': </strong>'.get_string('course_completed');
				}elseif(
					($programOrCourseArray->stat_scorm == '' && !preg_match('/completed|in progress/', $programOrCourseArray->stat_resource)) ||
					($programOrCourseArray->stat_scorm == 'not started' && $programOrCourseArray->stat_resource == '') ||
					($programOrCourseArray->stat_scorm == 'not started' &&  !preg_match('/completed|in progress/', $programOrCourseArray->stat_resource))	
				){
					$courseStatusDiv .= '<strong>'.get_string('status').': </strong>'.get_string('course_not_started').$statusDiv;	
				}else{
					$courseStatusDiv .= '<strong>'.get_string('status').': </strong>'.get_string('course_in_progress').$statusDiv;
				}
			}else{	
				$k++;
				$headingText = $programOrCourseArray->course_name;
				$summary = strip_tags($programOrCourseArray->course_description);
				//$learningObj = strip_tags($programOrCourseArray->learningobj);
                                $learningObj = $programOrCourseArray->learningobj;
				$performanceOut = strip_tags($programOrCourseArray->performanceout);
				$courseTypeId = getCourseTypeIdByCourseId($programOrCourseArray->id);
				$creditHoursText = "";
				$certificateLink = certificateLink($programOrCourseArray->id,'course');
				
				if($courseTypeId != $CFG->courseTypeClassroom){ 
					$creditHours = array_sum(explode('_',$programOrCourseArray->credithours));
					$creditHoursText = '<br /><strong>'.get_string('coursecredithours').':</strong> '.setCreditHoursFormat($creditHours);
				}
				$complianceDiv = getCourseComplianceIcon($programOrCourseArray->id);
				$primaryInstructor = '';
				$classroomCourseDuration = '';
				$classroomInstruction = '';
				$classroomCreatedBy = '';
				$classType = 'border-left-red'; 
				$rowType = "online";
				if($courseTypeId == $CFG->courseTypeClassroom ){ 
					//$certificateLink = 'Classroom';
					$certificateLink = '';
				    $classType = 'border-left-green'; 
					$rowType = "classroom";
					$classroomCreatedBy = getUsers($programOrCourseArray->course_createdby);
					$primaryInstructor = $programOrCourseArray->primary_instructor?getUsers($programOrCourseArray->primary_instructor):$classroomCreatedBy;
					$classroomInstruction = strip_tags($programOrCourseArray->classroom_instruction);
				    $classroomCourseDuration = strip_tags($programOrCourseArray->classroom_course_duration);
					
					$primaryInstructor = $primaryInstructor?('<br /><strong>'.get_string('primaryinstructor','course').':</strong>&nbsp; '.$primaryInstructor):'';
					$classroomInstruction = $classroomInstruction?('<br /><strong>'.get_string('classroominstruction','course').':</strong>&nbsp; '.$classroomInstruction):'';
					$classroomCourseDuration = $classroomCourseDuration?('<br /><strong>'.get_string('recommendationduration','course').':</strong>&nbsp; '.$classroomCourseDuration):'';
					$classroomCreatedBy = $classroomCreatedBy?('<br /><strong>'.get_string('classroomcreatedby','course').':</strong>&nbsp; '.$classroomCreatedBy):'';
                }
				
				$pcLearningObj = $learningObj?('<br /><strong>'.get_string('learningobj','course').': </strong>'.$learningObj):'';
				$performanceOut = $performanceOut?('<br /><strong>'.get_string('performanceOut','course').': </strong>'.$performanceOut):'';
				$descriptionText = $summary?'<br /><strong>'.get_string('description','learnercourse').':</strong> '.$summary:'';
				$descriptionText = $descriptionText.$pcLearningObj.$performanceOut.$classroomCreatedBy.$primaryInstructor.$creditHoursText;
				$imagePath = '<div class = "course-img" >'.getCourseImage(array('id'=>$programOrCourseArray->id,'fullname'=>$programOrCourseArray->course_name)).'</div>';	
				$programOrCourseArray->final_classrooom_stat = trim($programOrCourseArray->final_classrooom_stat);
				if($courseTypeId != $CFG->courseTypeClassroom){
					if(
						($programOrCourseArray->stat_scorm != 'not started' && $programOrCourseArray->stat_scorm != 'incomplete') && 
						($programOrCourseArray->stat_resource != 'not started' && $programOrCourseArray->stat_resource != 'incomplete') &&
						(
							(
								(in_array('completed',explode(' ',$programOrCourseArray->final_classrooom_stat)) || $programOrCourseArray->final_classrooom_stat == '') AND $programOrCourseArray->program_id == 0 
							) OR
							(
								(!in_array('incomplete',explode(' ',$programOrCourseArray->final_classrooom_stat)) || $programOrCourseArray->final_classrooom_stat == '') AND $programOrCourseArray->program_id != 0 
							)
						)
					){
						$courseStatusDiv .= '<strong>'.get_string('status').': </strong>'.get_string('course_completed');
					}else{
						//$statusDiv = ' </br>'.'Scorm = '.$programOrCourseArray->stat_scorm.' </br>'.'Res = '.$programOrCourseArray->stat_resource.' </br>'.'Class = '.$programOrCourseArray->final_classrooom_stat.' </br>';
						if(
							($programOrCourseArray->stat_scorm == 'not started' AND $programOrCourseArray->stat_resource == '' AND $programOrCourseArray->final_classrooom_stat == '') ||
							($programOrCourseArray->stat_scorm == '' AND $programOrCourseArray->stat_resource == 'not started' AND $programOrCourseArray->final_classrooom_stat == '') ||
							($programOrCourseArray->stat_scorm == 'not started' AND $programOrCourseArray->stat_resource == 'not started' AND $programOrCourseArray->final_classrooom_stat == '')
						){
							$courseStatusDiv .= '<strong>'.get_string('status').': </strong>'.get_string('course_not_started').$statusDiv;	
						}else{
							$courseStatusDiv .= '<strong>'.get_string('status').': </strong>'.get_string('course_in_progress').$statusDiv;
						}
					}
				}
				$courseExp = 0;
				if($courseTypeId != $CFG->courseTypeClassroom){
					$CourseEndDate = $DB->get_record('course',array('id'=>$programOrCourseArray->id));
					if($courseTypeId != $CFG->courseTypeClassroom && $CourseEndDate->enddate != ''){
						if(($CourseEndDate->enddate+86399) < strtotime('now')){
							$courseExp = 1;
							$courseStatusDiv .= ' ('.get_string('expired').')';
						}
					}
				}
			}
			$expired = "";
			$requestDiv = "";
			$expiryDateDiv = '<br />';
			if(!empty($programOrCourseArray->end_date) && $rowType != 'classroom'){
				$expiryDateDiv .= '<strong>'.get_string('enddate').': </strong>'.getDateFormat($programOrCourseArray->end_date, $CFG->customDefaultDateFormat);
			}elseif($rowType != 'classroom'){
				$expiryDateDiv .= '<strong>'.get_string('enddate').': </strong>'.get_string('any_end_date');
			}
			if($USER->original_archetype == $CFG->userTypeManager){
				//$expiryDateDiv = '';
			}
			if($programOrCourseArray->program_id != 0){
				$linktitle = "Show/Hide Course(s)";
			}else{
				if($courseTypeId == $CFG->courseTypeClassroom){
					$linktitle = "Show/Hide Class(es)";
				}else{
					$linktitle = "Show/Hide Course Material(s)";
				}
			}
			if(!empty($programOrCourseArray->end_date) && ($programOrCourseArray->end_date+86400) <= $currentTime && $type != 2){
				if($rowType != 'classroom'){
					$expired = "expired";
					$enrolLink = '<a href="#" class="requestforextention" program-name = "'.$headingText.'" rel = "'.$key.'" title="'.get_string('requestforextention').'">'.get_string('requestforextention').'</a>';
					if($programOrCourseArray->program_id == 0 && $courseExp == 1){
						$enrolLink = '';
					}
					if($USER->original_archetype != $CFG->userTypeManager){
						$requestDiv = '<div class="pull-left cbox toggle-button"><div	class="catalog_course_button">'.$enrolLink.'</div></div>';
						$requestDiv .= '<div class="pull-right right-icon cbox button-orange toggle-button" rel="'.$key.'" id="plus-minus-icon-'.$key.'"><a href="javascript:void(0)" title="'.$linktitle.'"></a><img alt="" src="'.$CFG->wwwroot.'/theme/image.php?theme=gourmet&component=core&image=a%2Fadd_file"></div>';
					}else{
						$requestDiv = '<div class="pull-right right-icon cbox button-orange toggle-button" rel="'.$key.'" id="plus-minus-icon-'.$key.'"><a href="javascript:void(0)" title="'.$linktitle.'"></a><img alt="" src="'.$CFG->wwwroot.'/theme/image.php?theme=gourmet&component=core&image=a%2Fadd_file"></div>';
					}
				}else{
					$expired = "";
					$requestDiv = '<div class="pull-right right-icon cbox button-orange toggle-button" rel="'.$key.'" id="plus-minus-icon-'.$key.'"><a href="javascript:void(0)" title="'.$linktitle.'"></a><img alt="" src="'.$CFG->wwwroot.'/theme/image.php?theme=gourmet&component=core&image=a%2Fadd_file"></div>';
				}
			}else{
				$requestDiv = '<div class="pull-right right-icon cbox button-orange toggle-button" rel="'.$key.'" id="plus-minus-icon-'.$key.'"><a href="javascript:void(0)" title="'.$linktitle.'"></a><img alt="" src="'.$CFG->wwwroot.'/theme/image.php?theme=gourmet&component=core&image=a%2Fadd_file"></div>';
			}
			//$courseStatusDiv = '';
			$htmlDiv = '<div class="no-course '.$classAlt.' '.$expired.' '.$classType.' '.$boxShadowClass.'" rel="'.$key.'" id="'.$key.'" data-class-type="'.$classType.'" >';
				$resLink = '';
				$resDiv = '';
				if(!empty($refMaterial)){
					$i = 1;
					$resLink = "<div class = 'res-title' style= 'float: left; width: 165px;'><strong>".get_string('reference_materials').":</strong> </div>";
					$resLink .= "<div class = 'res-content' style='float: left;'>";
					foreach($refMaterial as $res){
						//$name = get_string('reference_material').' '.$i.": ";submitSearchForm
						$name = $i.": ";
						$resLink .= '<span>'.$name.$res.'</br></span>';
						$i++;
					}
					$resLink .= '</div>';
					$resDiv = '<div class="resource-links">'.$resLink.'</div>';
				}
				
				   if($type !=2){
				   	$certificateLink = ''; 	
				   }
			
					$htmlDiv .= $imagePath;
					$htmlDiv .= '<div class="pull-left left-content">';
						$htmlDiv .= '<div class="c-heading"><a href="javascript:void(0)" title="Show/Hide Course Material(s)" style="text-decoration:underline;">'.$headingText.'</a> '.$complianceDiv.'</div>';
						
                                                 if($programOrCourseArray->category!=''){
                                                        $htmlDiv .= '<div class="c-text"><strong>'.get_string('coursecategory').': </strong>'.getCategoryNameById($programOrCourseArray->category).'<br></div>';
                                                 }
                                                
                                                $htmlDiv .= '<div class="c-text">'.substr($descriptionText.$courseStatusDiv.$expiryDateDiv,6).'</div>';
						$htmlDiv .= $resDiv;
					$htmlDiv .= '</div>';
					if($CFG->isCertificate==1){
					//$htmlDiv .=$certificateLink;
					}
					$htmlDiv .= $requestDiv;	
				$htmlDiv .= '</div>';
			$htmlDiv .= $courseDiv;
			$html .= $htmlDiv;
		}
	}else{
	  $html .= '<p style="margin-top: 20px;">'.get_string('no_results').'</p>';
	}
	  
  $html .= html_writer::end_tag('div');
  //$html .= paging_bar($courseProgramCount, $page, $perpage, $genURL); 
  $html .= $outerDivEnd;
echo $html;	
echo "<input type='hidden' id='showClasses' value='".$showClasses."'>";
echo "<input type='hidden' id='showClassStatus' value='".$showClassStatus."'>";
 ?>
<a href="#end-date-div" id="inline-iframe" rel="inline-iframe" title="<?php print_string('decline_request'); ?>">click</a>
<div class="end-date-div" id="end-date-div">
	<iframe src="" id="i-frame" width="100%" height="100%"></iframe>
</div>
 <script type="text/javascript">
 
   
 function setCourseLastAccess(userid, courseid, resourceid){
	
	   $.ajax({
	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=setCourseLastAccess&userid='+userid+"&courseid="+courseid+"&resourceid="+resourceid,
		dataType:'json',
		success:function(data){
		   var success = data.success;
		     var error = data.error;
			   if(success == 1){
			      var course_resource_id = data.response;
				  $('#coursestatus_'+course_resource_id).addClass('downloaded');
				   //location.reload();
                                   replacedbboxcontent(courseid);
                                   
			   }else{
				 //alert(error);
			   }
		}
	  
	  });
	  /*$.ajax({
	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=setCourseBadge&userid='+userid+"&courseid="+courseid+"&resourceid="+resourceid,
		dataType:'json',
		success:function(data){  
		}
	  
	  });*/
	     
	
 }
 
 function replacedbboxcontent(courseid){
                        
                        var expired = 0;
			if($(this).parent().hasClass('expired')){
				expired = 1;
			}
                        
                       
			var courseId = courseid;
                        courseId = 0+'_'+courseid;
			var showClasses = $("#showClasses").val();
			var showClassStatus = $("#showClassStatus").val();
			var type = "<?php echo $type;?>";
			var dataClassType = 'border-left-red';//$(this).parent().attr('data-class-type');
			//if($("#dbox"+courseId).attr('class') == '' || $("#dbox"+courseId).attr('class') == 'undefined' || $("#dbox"+courseId).attr('class') == undefined){
				if(courseId != 'undefined' && courseId != 'Undefined' && courseId != undefined){
					$('#fade').show();
					$.ajax({
						url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
						type:'POST',
						data:'action=getProgramCourse&programId='+courseId+"&getAsset=1&dataClassType="+dataClassType+'&type='+type+'&expired='+expired+'&showClasses='+showClasses+'&showClassStatus='+showClassStatus,
						success:function(data){
							//$("#"+courseId).after(data);
							//$("#dbox"+courseId).show();
                                                        $("#dbox"+courseId).replaceWith(data);
							$('#fade').hide();
						}
					});
				}
			//}
     
 }
     
	
 function showHideButton(){
      $('a.moreless-toggler').addClass('moreless-toggler moreless-less');
	  $('a.moreless-toggler').html('<?php echo get_string('showless','form');?>');
	  
	  $('div.fitem_fselect').addClass('fitem advanced fitem_fselect show');
	  $('div.fitem_fgroup').addClass('fitem advanced fitem_fgroup show');
	   
 }
 
 
 /*$(window).load(function () {
     <?php if(isset($SESSION->learnercourse_filtering['categoryname']) || isset($SESSION->learnercourse_filtering['timecreated'])){ ?>
	      //  showHideButton();
	<?php } ?>
 });*/
 
 $(document).ready(function(){
		$("#inline-iframe").colorbox({inline:true, width:"675px", height:"468px"});
		$(document).on('click','#search-form .filter input',function(){
			$('#searchForm').submit();
		});
		$(document).on("click",".decline",function(){
			var enrolid = $(this).attr('schedularid');
			var userId = $(this).attr('userid');
			var source = '<?php echo $CFG->wwwroot;?>/course/unenrolcourse.php?element=user&assigntype=classroom&assignId='+enrolid+'&elementList='+userId;
			$("#i-frame").attr('src',source);
			$(".cboxContent #end-date-div").attr("style",'display:block');
			$("#inline-iframe").trigger("click");
		});
		$(".requestforextention").click(function(){
			var courseId = $(this).attr('rel');
			var elementName = $(this).attr('program-name');
			var elementId = courseId.split("_")
			courseId = elementId["1"];
			var programId = elementId["0"];
			message = "<?php echo get_string('request_for_extension')?>'"+elementName+"' ?";
			if(confirm(message)){
				$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
					type:'POST',
					data:'action=checkCourseProgramRequest&extension=1&courseId='+courseId+"&programId="+programId,
					success:function(data){
						if(data == 1){
							var txt = "<?php echo get_string('alreadyrequested');?>";
							var response = confirm(txt);
							if (response == true) {
								$.ajax({
									url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
									type:'POST',
									data:'action=RequestForCourse&extension=1&courseId='+courseId+"&programId="+programId,
									success:function(data){
											window.location.reload();
									}
								});
							}
						}else{
							if(data == 2){
								var txt = "<?php echo get_string('requestdeclined');?>";
								var response = confirm(txt);
								if (response == true) {
									$.ajax({
										url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
										type:'POST',
										data:'action=RequestForCourse&extension=1&courseId='+courseId+"&programId="+programId,
										success:function(data){
											window.location.reload();
										}
									});
								}
							}else{
								$.ajax({
									url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
									type:'POST',
									data:'action=RequestForCourse&extension=1&courseId='+courseId+"&programId="+programId,
									success:function(data){
										window.location.reload();
									}
								});
							}
						}
					}
				});
			}
		});
      $('.toggle-button').click(function(){
			var expired = 0;
                      
			if($(this).parent().hasClass('expired')){
				expired = 1;
			}                        
			var courseId = $(this).parent().attr('rel');
			var showClasses = $("#showClasses").val();
			var showClassStatus = $("#showClassStatus").val();
			var type = "<?php echo $type;?>";
			var dataClassType = $(this).parent().attr('data-class-type');
			if($("#dbox"+courseId).attr('class') == '' || $("#dbox"+courseId).attr('class') == 'undefined' || $("#dbox"+courseId).attr('class') == undefined){
				if(courseId != 'undefined' && courseId != 'Undefined' && courseId != undefined){
					$('#fade').show();
                                        
					$.ajax({
						url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
						type:'POST',
						data:'action=getProgramCourse&programId='+courseId+"&getAsset=1&dataClassType="+dataClassType+'&type='+type+'&expired='+expired+'&showClasses='+showClasses+'&showClassStatus='+showClassStatus,
						success:function(data){
							$("#"+courseId).after(data);
							$("#dbox"+courseId).show();
						$('#fade').hide();
						}
					});
				}
			}else{
					$("#dbox"+courseId).toggle();
			}
			$('.no-course').removeClass('selected');
			$('.cbox').not('[rel="'+courseId+'"]').removeClass('minus');
			var hasClass = $('#plus-minus-icon-'+courseId).hasClass('minus');
			if(hasClass){
				$('#plus-minus-icon-'+courseId).removeClass('minus');
			}else{
				$('#plus-minus-icon-'+courseId).addClass('minus');
			}

			$('.d-box').not('#dbox'+courseId).hide();
			$(this).addClass('selected');
		});
	  
          $('.c-heading').click(function(){
			var expired = 0;
                      
			if($(this).parent().parent().hasClass('expired')){
				expired = 1;
			}                        
			var courseId = $(this).parent().parent().attr('rel');
                       // alert(courseId);
			var showClasses = $("#showClasses").val();
			var showClassStatus = $("#showClassStatus").val();
			var type = "<?php echo $type;?>";
			var dataClassType = $(this).parent().parent().attr('data-class-type');
			if($("#dbox"+courseId).attr('class') == '' || $("#dbox"+courseId).attr('class') == 'undefined' || $("#dbox"+courseId).attr('class') == undefined){
				if(courseId != 'undefined' && courseId != 'Undefined' && courseId != undefined){
					$('#fade').show();
                                        
					$.ajax({
						url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
						type:'POST',
						data:'action=getProgramCourse&programId='+courseId+"&getAsset=1&dataClassType="+dataClassType+'&type='+type+'&expired='+expired+'&showClasses='+showClasses+'&showClassStatus='+showClassStatus,
						success:function(data){
							$("#"+courseId).after(data);
							$("#dbox"+courseId).show();
						$('#fade').hide();
						}
					});
				}
			}else{
					$("#dbox"+courseId).toggle();
			}
			$('.no-course').removeClass('selected');
			$('.cbox').not('[rel="'+courseId+'"]').removeClass('minus');
			var hasClass = $('#plus-minus-icon-'+courseId).hasClass('minus');
			if(hasClass){
				$('#plus-minus-icon-'+courseId).removeClass('minus');
			}else{
				$('#plus-minus-icon-'+courseId).addClass('minus');
			}

			$('.d-box').not('#dbox'+courseId).hide();
			$(this).addClass('selected');
		});
          
	  $(document).on('click',".toggle-button-programcourse",function(){
			var courseId = "ebox0_"+$(this).attr('rel');
			$('#'+courseId).toggle();
			$(this).toggleClass('selected');			
			$(this).toggleClass('minus');
	  });
	  $(document).on("click",".attempt_div",function(){
		var currentElement = $(this).attr('id');
		var $this = $('#'+currentElement);
		$(this).toggleClass('addclass');
		$this.parent().parent().parent('tr').next('tr').toggleClass('show_row');
	  });
	
        $(document).on('change','#search-form .filter select',function(){		
            $('#searchForm').submit();
        });
 
 });


 </script>
<div id="fade" ></div>
 <?php	

 echo $OUTPUT->footer();
 