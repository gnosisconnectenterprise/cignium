<?php
/**
 * Custom module - Learner Dashboard
 * Date Creation - 04/06/2014
 * Date Modification : 24/06/2014
 * Last Modified By : Rajesh Kumar
 */
require_once(dirname(__FILE__) . '/../config.php');
global $CFG, $USER;
require_once($CFG->dirroot . '/my/lib.php');
require_once($CFG->dirroot . '/mod/scorm/locallib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/calendar/lib.php');
require_once($CFG->dirroot . '/my/learninglib.php');
$userId = $USER->id;
//$userrole =  getUserRole($userId);
checkLogin();


if ($USER->archetype == $CFG->userTypeAdmin) {  // added by rajesh 
    redirect($CFG->wwwroot . '/');
}
$calenderView = optional_param('view', 'day', PARAM_ALPHA);

$strmymoodle = get_string('myhome');
$header = "$SITE->shortname: $strmymoodle";
$context = get_context_instance(CONTEXT_SYSTEM);
$params = array();
$PAGE->set_context($context);
$PAGE->set_url('/my/learnerdashboard.php', $params);
$PAGE->set_pagelayout('learnerdashboard');
$PAGE->set_pagetype('my-index');
$PAGE->blocks->add_region('content');
$PAGE->set_subpage($currentpage->id);
$PAGE->set_title($header);
$PAGE->set_heading($header);

echo $OUTPUT->header();



$_SESSION['update_msg'] = '';
$isReport = false;


$courseData = get_learning_list_optimized2('', 5, 0, 0, 'all');
//$courseData = get_learning_list('',5,0,0,'all');



$notStartedCount = 0;
$completedCount = 0;
$inProgressCount = 0;
$courseArray = array();
$totalCourses = count($courseData);
if (!empty($courseData)) {
    foreach ($courseData as $courseRec) {
        $cData = array();
        if ($courseRec->program_id == 0) {
            $name = $courseRec->course_name;
            $id = $courseRec->id;
        } else {
            $name = $courseRec->program_name;
            $id = $courseRec->program_id;
        }
        $cData['courseId'] = $id;
        $cData['type'] = $courseRec->coursetype_id;
        $cData['fullname'] = $name;
        if ($courseRec->coursetype_id == 2) {
            $cData['classname'] = $courseRec->program_name;
            if ($courseRec->class_submit == 1) {
                $completedCount++;
                $cData['courseStatus'] = 'Completed';
                $courseArray[] = $cData;
            } else {
                if ($courseRec->class_start > strtotime('now') && $courseRec->class_submit != 1) {
                    $notStartedCount++;
                } else {
                    $inProgressCount++;
                    $cData['courseStatus'] = 'In Progress';
                    $courseArray[] = $cData;
                }
            }
        } else {
            if (
                    ($courseRec->stat_scorm == 'completed' && $courseRec->stat_resource == 'completed') ||
                    ($courseRec->stat_scorm == '' && $courseRec->stat_resource == 'completed') ||
                    ($courseRec->stat_scorm == 'completed' && $courseRec->stat_resource == '')
            ) {
                $completedCount++;
                $cData['courseStatus'] = 'Completed';
                $courseArray[] = $cData;
            } else {
                if (
                        ($courseRec->stat_scorm == '' && $courseRec->stat_resource == '') ||
                        ($courseRec->stat_scorm == 'not started' && $courseRec->stat_resource == '') ||
                        ($courseRec->stat_scorm == '' && $courseRec->stat_resource == 'not started') ||
                        ($courseRec->stat_scorm == 'not started' && $courseRec->stat_resource == 'not started')
                ) {
                    $notStartedCount++;
                } else {
                    $inProgressCount++;
                    $cData['courseStatus'] = 'In Progress';
                    $courseArray[] = $cData;
                }
            }
        }
    }
}


?>

<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/theme/gourmet/jquery/bxslider/jquery.bxslider.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot; ?>/theme/gourmet/jquery/bxslider/jquery.bxslider.js"></script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot; ?>/theme/gourmet/jquery/bxslider/jquery.bxslider.min.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot; ?>/theme/gourmet/jquery/jscrollpane/jquery.jscrollpane.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot; ?>/theme/gourmet/jquery/jscrollpane/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot; ?>/theme/gourmet/jquery/jscrollpane/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot; ?>/theme/gourmet/jquery/jscrollpane/mwheelIntent.js"></script>

<?php
////// Show Message after sending email //////
if ($_SESSION['update_msg'] != '') {
   
    $_SESSION['update_msg'] = '';
}
if ($_SESSION['error_msg'] != '') {
    
    $_SESSION['error_msg'] = '';
}
?>
<div role="main">
    <span id="maincontent"></span>

    <?php
    GLOBAL $DB, $USER;
    $i = 1;
    $j = 1;
    $lastAccessDateSortedArray = array();
    $notstarted = get_string('notstarted', 'learnercourse');
    ?>
    <div class = "content-upper" style = "height: 250px;">
        <div class = "course-assign-status">
            <div class = "course_status_block course-assign-status-completed full"> <span class = "course_status_block_span course-assign-status-heading"><?php echo get_string('completed', 'learnercourse'); ?></span> <span class = "course_status_block_span course-assign-status-heading-value"><a href = "<?php echo $CFG->wwwroot; ?>/my/learningpage.php?type=2"><?php echo $completedCount; ?></a></span> </div>
            <div class = "course_status_block course-assign-status-inprogress half"> <span class = "course_status_block_span course-assign-status-heading"><?php echo get_string('inprogress', 'learnercourse'); ?></span> <span class = "course_status_block_span course-assign-status-heading-value"><a href = "<?php echo $CFG->wwwroot; ?>/my/learningpage.php?type=1&in_progress=in_progress&expired=all"><?php echo $inProgressCount; ?></a></span> </div>
            <div class = "course_status_block course-assign-status-notstarted half"> <span class = "course_status_block_span course-assign-status-heading"><?php echo get_string('notstarted', 'learnercourse'); ?></span> <span class = "course_status_block_span course-assign-status-heading-value"><a href = "<?php echo $CFG->wwwroot; ?>/my/learningpage.php?not_started=not_started&type=1&expired=all"><?php echo $notStartedCount; ?></a></span> </div>
            <div class="course_total"><?php echo get_string('courses_assigned') . ':&nbsp;' . $totalCourses; ?><a href="<?php echo $CFG->wwwroot; ?>/my/learningpage.php" class="pull-right"><span><?php echo get_string('viewdetails', 'learnercourse'); ?></span></a></div>
        </div>
        <div class = "last-accessed-courses slider">
            <div>
<?php if (count($courseArray) > 0) { ?>
                    <div class="bxslider">
                    <?php
                    $courseObj = new stdClass();
                    foreach ($courseArray as $course) {
                        $courseId = $course['courseId'];
                        $fullName = $course['fullname'];
                        if ($course['type'] == 2) {
                            $fullName .= ' ' . $course['classname'];
                        }

                        $courseObj->id = $courseId;
                        $courseObj->fullname = $fullName;

                        $courseImg = getCourseImage($courseObj);
                        $courseStatus = $course['courseStatus'];
                        $ty = 1;
                        if ($courseStatus == 'Completed') {
                            $ty = 2;
                        }
                        $linkUrl = '<a href="' . $CFG->wwwroot . '/my/learningpage.php?type=' . $ty . '&expired=all&key=' . $course['fullname'] . '" class="course_launch launch_course"  view = "normal">' . get_string('clicktolaunch', 'learnercourse') . '</a>';
                        ?>
                            <div class = "course_block">
                                <div class = 'course_details_block'>
                                    <div class = 'course_data_block'>
                                        <div class = 'course_image_block'> <?php echo $courseImg; ?> </div>
                                        <div class = "course_data">
                                            <div class="course_name word-wrap"><?php echo $fullName && strlen($fullName) > 30 ? substr($fullName, 0, 30) . '...' : $fullName; ?></div>
                                           
                                            <div class="course_status"><?php echo $courseStatus; ?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "course_launch_link"><?php echo $linkUrl; ?></div>
                            </div>
    <?php } ?>
                    </div>
<?php } else { ?>
                    <div class = "no-reults"> <?php echo get_string("no_results"); ?> </div>
                    <?php } ?>
            </div>
        </div>
    </div>
                <?php
                $dayView = "";
                $weekView = "";
                $monthView = "";
                switch (strtolower($calenderView)) {
                    Case 'month':
                        $monthView = " active";
                        break;
                    Case 'day':
                        $dayView = " active";
                        break;
                    Case 'week':
                        $weekView = " active";
                        break;
                    default:
                        $dayView = " active";
                        break;
                }

                $date = date("y-m-d") . " 12:00:00";
                $currDateTime = strtotime($date);
                ?>
    <div class="content-lower pull-left">
        <div class="dashboard-lower-left pull-left">
            <div class="lower-right-bottom">
                <div class="block-header messages-icon"><span></span><?php echo get_string('messages', 'admindashboard'); ?><a href="<?php echo $CFG->wwwroot . '/messages/mymessages.php'; ?>" class="add-message pull-right" title = "<?php echo get_string('manage_messages', "messages"); ?>"></a></div>
                <div class="block-content scroll-pane">
<?php
$groupSql = "SELECT groupid from {$CFG->prefix}groups_members where userid='" . $userId . "'";
$groupRow = $DB->get_records_sql($groupSql);
$groupId = array();
$teamId = "";
if (count($groupRow) > 0) {
    foreach ($groupRow as $groupValue) {
        $groupId[] = $groupValue->groupid;
    }
    $teamId = implode(",", $groupId);
}

$msgSql = "SELECT * from {$CFG->prefix}my_messages where deleted = 0 AND  status = 1 and ( FIND_IN_SET('" . $USER->department . "',department) || FIND_IN_SET('" . $userId . "',show_to) ||  FIND_IN_SET('" . $teamId . "',team)) ORDER BY timecreated DESC limit 0,5";

$messages = $DB->get_records_sql($msgSql);

if (count($messages) > 0) {
    foreach ($messages as $message) {

       
        $message_content = $message->message_title ? nl2br($message->message_title) : $message->message_title;
        $user = $DB->get_record('user', array('id' => $message->message_from));
        ?>
                            <div class = 'message-block'>
                                <div class = 'message-sender-image'> <?php echo $OUTPUT->user_picture($user, $userpictureparams); ?> </div>
                                <div class = 'message-content'>
                                    <div class = "message-sender-info"> <?php
                            echo ucfirst($user->firstname) . " " . ucfirst($user->lastname);
                            echo '&nbsp;-&nbsp;';
                            echo getDateFormat($message->timecreated, $CFG->customDefaultDateTimeFormat); // user M in place of F for short month name
                            ?> </div>
                                    <div class = "message-content-details">
                                        <a href = "<?php echo $CFG->wwwroot . '/messages/read_message.php?id=' . $message->id; ?>">
                                        <?php
                                        if (strlen($message_content) > 100) {
                                            echo substr($message_content, 0, 100) . " ...";
                                        } else {
                                            echo $message_content;
                                        }
                                        ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                                            <?php
                                        }
                                    } else {
                                        ?>
                        <div class = "no-reults"> <?php echo get_string("nomessagefound"); ?> </div>
                    <?php } ?>
                </div>
            </div>
            
            
<!--            <div class="dashboard-calender">
                <div class="block-header dashboard-calender-icon"><span></span><?php echo get_string('calendar', 'learnercourse'); ?> </div>
   
                <div class = "calender-block">
                   
                    <div style="padding:10px"> <?php echo get_string('loadingcalender', 'calendar'); ?></div>
               
                </div>
            </div>-->
        </div>
        <div class="dashboard-lower-right pull-right">
            <div class="lower-right-upper">
                <div class="block-header newcourse-icon"><span></span><?php echo get_string('newcourses', 'learnercourse'); ?>
                    &nbsp;<a href="<?php echo $CFG->wwwroot . '/my/course_catalog.php'; ?>" class="course_catalog pull-right" title = "<?php echo get_string('course_catalog_learner', 'menubar'); ?>"><?php echo get_string('course_catalog_learner', 'menubar'); ?></a> </div>
                <div class="block-content">
                    <div class="slider-new-courses">
<?php echo getDashboardNewCoursesHtml(); ?>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

    <script language="javascript" type="application/javascript">
        

        setInterval(function(){$('.update_msg','.error_msg').slideUp();},10000);

        $(document).ready(function(){
        $(".enrollment-courseB").click(function(){
        var dataMsg = $(this).attr('data-msg');
        var enrolId = $(this).attr('data-url');
        if(enrolId){  
        if(confirm(dataMsg)){
        $.ajax({
        url:'<?php echo $CFG->wwwroot; ?>/local/ajax.php',
        type:'POST',
        data:'action=enrolUserToCourse&courseId='+enrolId,
        success:function(data){
        alert("<?php echo get_string('coursehasbeenenroled', 'learnercourse'); ?>");
        //window.location.reload();
        }
        });
        }
        }
        });


        var scormviewform = document.getElementById('scormviewform');
        var poptions = '';
        var course_url = '';
        var launch_url = '';
        $('.bxslider, .bxslider1').bxSlider({
        infiniteLoop: false,
        hideControlOnEnd: true,
        //displaySlideQty:3, 
        moveSlides: 1
        });

        $(document).on('click',".request-for-class, .enroll-in-class",function(event){
        event.preventDefault();
        var classId = $(this).attr('relclass');
        var userId = $(this).attr('reluser');
        if(classId != 'undefined' && classId != 'Undefined' && classId != undefined && userId != 'undefined' && userId != 'Undefined' && userId != undefined){
        var dataMsg = " Do you want to request for this class?";
        if(confirm(dataMsg)){
        $.ajax({
        url:'<?php echo $CFG->wwwroot; ?>/local/ajax.php',
        type:'POST',
        data:'action=enrolUserToClassroom&userId='+userId+'&classId='+classId,
        success:function(data){
        alert("<?php echo get_string('request_sent_success'); ?>");
        window.location.reload();
        }
        });
        }
        }
        });
        $(document).on('click',".enrol_user",function(event){
        event.preventDefault();
        var txt = "Do you want to accept this request?";
        var response = confirm(txt);
        var userIds = "<?php echo $USER->id; ?>";
        if (response == true) {
        var enrolid = $(this).attr('relclass');
        var enrolUser = 1;
        $.ajax({
        url:'<?php echo $CFG->wwwroot; ?>/local/ajax.php',
        type:'POST',
        data:"action=checkSeatEnrol&enrolid="+enrolid+"&enrolUser="+enrolUser,
        success:function(data){
        if(data == 'success'){
        $.ajax({
        url:'<?php echo $CFG->wwwroot; ?>/local/ajax.php',
        type:'POST',
        data:"action=enrolUserInClassroom&userIds="+userIds+"&enrolid="+enrolid+"&enrolUser="+enrolUser,
        success:function(data){
        if(data == 'success'){
        alert("<?php echo get_string('record_updated'); ?>");
        window.location.reload();
        }else{
        alert("unable to process request. Please try again later.");
        }
        }
        });
        }else{
        if(data == 'error'){
        alert("unable to process request. Please try again later.");
        }else{
        var msg = '<?php echo get_string("errornoofinvite", "scheduler", "'+data+'"); ?>';
        alert(msg);
        }
        }
        }
        });
        }
        });
        $(".requestCourse").click(function(event){
        event.preventDefault();
        message = "<?php echo get_string('sentassigncourserequest', 'course') ?>";
        var r = confirm(message);
        if (r == true) {
        var courseId = $(this).attr('rel');
        var courseType = $(this).attr('ctype');
        if(courseType == 'classroom'){
        window.location.href = $(this).attr('href');
        }else{
        $.ajax({
        url:'<?php echo $CFG->wwwroot; ?>/local/ajax.php',
        type:'POST',
        data:'action=checkCourseProgramRequest&courseId='+courseId+"&programId=0",
        success:function(data){
        if(data == 1){
        var txt = "<?php echo get_string('alreadyrequested'); ?>";
        var response = confirm(txt);
        if (response == true) {
        $.ajax({
        url:'<?php echo $CFG->wwwroot; ?>/local/ajax.php',
        type:'POST',
        data:'action=RequestForCourse&courseId='+courseId+"&programId=0",
        success:function(data){
        alert("<?php echo get_string('request_sent_success'); ?>");
        window.location.reload();
        //alert("<?php echo $_SESSION['update_msg']; ?>");
        }
        });
        }
        }else{
        if(data == 2){
        var txt = "<?php echo get_string('requestdeclined'); ?>";
        var response = confirm(txt);
        if (response == true) {
        $.ajax({
        url:'<?php echo $CFG->wwwroot; ?>/local/ajax.php',
        type:'POST',
        data:'action=RequestForCourse&courseId='+courseId+"&programId=0",
        success:function(data){
        alert("<?php echo get_string('request_sent_success'); ?>");
        window.location.reload();
        //	alert("<?php echo $_SESSION['update_msg']; ?>");
        }
        });
        }
        }else{
        $.ajax({
        url:'<?php echo $CFG->wwwroot; ?>/local/ajax.php',
        type:'POST',
        data:'action=RequestForCourse&courseId='+courseId+"&programId=0",
        success:function(data){
        alert("<?php echo get_string('request_sent_success'); ?>");
        window.location.reload();
        //alert("<?php echo $_SESSION['update_msg']; ?>");
        }
        });
        }
        }
        }
        });
        }
        }
        });
        });
    </script>