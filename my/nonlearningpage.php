<?php

	/**
		* Custom module - Non Learning page
		* Date Creation - 04/05/2014
		* Date Modification : 23/06/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
	
	require_once(dirname(__FILE__) . '/../config.php'); 
	global $USER;
	$userid = $USER->id;
	//$userrole =  getUserRole($userid);
	//if($userrole != 5){  // added by rajesh 
	 //  redirect($CFG->wwwroot .'/');
	//}
	if($USER->archetype != $CFG->userTypeStudent){  // added by rajesh 
	   redirect($CFG->wwwroot .'/course');
	}
	if(isset($_POST['removeall']) && !empty($_POST['removeall'])){
			$_POST['removeall'] = '';
			redirect($CFG->wwwroot .'/my/nonlearningpage.php');
	}
	$module = 17;
	

    require_login();

    $type         = optional_param('type', 0, PARAM_INT);
	$delete       = optional_param('delete', 0, PARAM_INT);
    $confirm      = optional_param('confirm', '', PARAM_ALPHANUM);   //md5 confirmation hash
    $sort         = optional_param('sort', 'id', PARAM_ALPHANUM);
    $dir          = optional_param('dir', 'DESC', PARAM_ALPHA);
    $page         = optional_param('page', 0, PARAM_INT);
    $perpage      = optional_param('perpage', 10, PARAM_INT);        // how many per page
	
    $header = $SITE->fullname.": ".get_string('mylearningpage', 'learnercourse');
    $PAGE->set_title($header);
	$PAGE->set_heading($header);
	$PAGE->set_pagelayout('mydashboard');
    $stredit   = get_string('edit');
    $strdelete = get_string('delete');
    $strdeletecheck = get_string('deletecheck');
    $strconfirm = get_string('confirm');

    if (empty($CFG->loginhttps)) {
        $securewwwroot = $CFG->wwwroot;
    } else {
        $securewwwroot = str_replace('http:','https:',$CFG->wwwroot);
    }

    $returnurl = new moodle_url('/my/nonlearningpage.php', array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'page'=>$page, 'type' => $type));

    // create the user filter form
	
	$fieldnames = array('resoursename'=>0, 'lastaccessed'=>1);
    //$lcfiltering = new learnercourse_filtering($fieldnames);
	
    echo $OUTPUT->header();

    $columns = array('resoursename', 'lastaccessed', 'rstatus');

    foreach ($columns as $column) {
        $string[$column] = getLearnerCourseFieldName($column);
        if ($sort != $column) {
            $columnicon = "";
            if ($column == "timecreated") {
                $columndir = "DESC";
            } else {
                $columndir = "ASC";
            }
        } else {
            $columndir = $dir == "ASC" ? "DESC":"ASC";
            if ($column == "timecreated") {
                $columnicon = ($dir == "ASC") ? "sort_desc" : "sort_asc";
            } else {
                $columnicon = ($dir == "ASC") ? "sort_asc" : "sort_desc";
            }
            $columnicon = "<img class='iconsort' src=\"" . $OUTPUT->pix_url('t/' . $columnicon) . "\" alt=\"\" />";

        }
		
        if(in_array($column, array())){
          $$column = $string[$column];
		}else{
		  $$column = "<a href=\"nonlearningpage.php?sort=$column&amp;dir=$columndir&amp;type=$type\">".$string[$column]."</a>$columnicon";
		}
    }

    //list($extrasql, $params) = $lcfiltering->get_sql_filter();
	
	$extrasql='';
	$params=array();
	$_SESSION['extrasql'] = '';
	//require_once("../local/searchForm.php");
	$extrasql =  $_SESSION['extrasql'];//die;
	//$extrasql='';
	

    $learnercourses = getAssignedCourseForUser($module, $sort, $userid, $dir, $page*$perpage, $perpage, '', $extrasql, $params, $type);
	//pr($learnercourses);
	$learnercoursecount = getAssignedCourseForUserCount($module, $userid, $type);
	//$learnercoursesearchcount = count($learnercourses);
	   
    /*if ($extrasql !== '') {
        echo $OUTPUT->heading("$learnercoursesearchcount / $learnercoursecount ".get_string('nonlearnercourse','learnercourse'));
        $learnercoursecount = $learnercoursesearchcount;
    } else {
        echo $OUTPUT->heading("$learnercoursecount ".get_string('nonlearnercourse','learnercourse'));
    }
*/
   // $strall = get_string('all');

    $baseurl = new moodle_url('/my/nonlearningpage.php', array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'type' => $type));
    //echo $OUTPUT->paging_bar($learnercoursecount, $page, $perpage, $baseurl);
	

    //flush();


        
        $table = new html_table();
        $table->head = array ();
        $table->colclasses = array();
		
	 if ($learnercoursecount) {
       
        $table->head[] = $resoursename;
        $table->attributes['class'] = 'admintable generaltable';
        $table->colclasses[] = 'leftalign';
		$table->head[] = $rstatus;
		$table->colclasses[] = 'leftalign';
       	$table->head[] = $lastaccessed;
        $table->colclasses[] = 'leftalign';
        $table->head[] = get_string('actions','learnercourse');
        $table->colclasses[] = 'centeralign';
        //pr($learnercourses);
        $table->id = "nonlearnercourses";
		
		
		
 
		$curtime = time();
        foreach ($learnercourses as $learnercourse) {
            $buttons = array();
            $lastcolumn = '';
			
			$courseid = $learnercourse->id;
			$resourceid = $learnercourse->resourceid;
			$cmid = $learnercourse->cmid;
			
			$resoursename = $learnercourse->resoursename;
			$categoryname = $learnercourse->categoryname;
			$rstatus = $learnercourse->rstatus;
			
			
			$lastaccessed = $lastaccessed?getDateFormat($lastaccessed, "d/m/Y"):'---';
		
		    $lastAccessedTime = $learnercourse->lastaccessed;
			if($lastAccessedTime){
				$remainAccessedTime = $curtime - $lastAccessedTime;
				$lastAccessed = timePassed($remainAccessedTime);
			}else{
				 $lastAccessed = get_string('notaccessed','learnercourse');;
			}
								
		
		   $styDownload = '';
		   
		   $filedownloaded = isUserAccessedCourse($userid, $courseid);
		   if($filedownloaded){
		     $styDownload = "downloaded";
		   }

			if (!$cm = get_coursemodule_from_id('resource', $cmid)) {
				resource_redirect_if_migrated(0, $id);
				print_error('invalidcoursemodule');
			}
			
			
			$resource = $DB->get_record('resource', array('id'=>$cm->instance), '*', MUST_EXIST);
	        $context = context_module::instance($cm->id);
			$fs = get_file_storage();
            $files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!

			if (count($files) < 1) {
				resource_print_filenotfound($resource, $cm, $course);
				die;
			} else {
				$file = reset($files);
				unset($files);
			}
			$path = '/'.$context->id.'/mod_resource/content/'.$resource->revision.$file->get_filepath().$file->get_filename();
			$fullurl = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
   
   
   
			$fileStatus = "<a href='javascript:;' onclick=\"setCourseLastAccess($userid, $courseid, $resourceid);window.open('".$fullurl."', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \" title='".get_string('download','learnercourse')."' class='download_icon ".$styDownload."'   id='coursestatus_".$courseid."_".$resourceid."'>".get_string('download','learnercourse')."</a>";
	
            $row = array();
            $row[] = $resoursename;
            $row[] = $rstatus;
			$row[] = $lastAccessed;
			$row[] = $fileStatus;
            $table->data[] = $row;
        }

   }
    // add filters
	
	
	
     //require_once("../local/searchForm.php");
    //$lcfiltering->display_active();
		


   /* added by rajesh to redirect the page otherwise filter were adding again and again while we refresh the page */	
    /*if(isset($_REQUEST['addfilter']) && !empty($_REQUEST['addfilter'])){
	    redirect($returnurl);
	}*/
	// end 
    
    	
    if (!empty($table)) {
        echo html_writer::start_tag('div', array('class'=>'no-overflow'));
        echo html_writer::table($table);
		if (!$learnercourses) {
          	echo $OUTPUT->heading(get_string('norecordsfound','learnercourse'));
		}
        echo html_writer::end_tag('div');
        echo $OUTPUT->paging_bar($learnercoursecount, $page, $perpage, $baseurl);
       
    }
	
	?>
    
   <script>
    
	function setCourseLastAccess(userid, courseid, resourceid){
	
     
	   $.ajax({
	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=setCourseLastAccess&userid='+userid+"&courseid="+courseid+"&resourceid="+resourceid,
		dataType:'json',
		success:function(data){
		  
		   var success = data.success;
		     var error = data.error;
			   if(success == 1){
			      var course_resource_id = data.response;
				  $('#coursestatus_'+course_resource_id).addClass('downloaded');
			   }else{
				 alert(error);
			   }
		}
	  
	  });
	     
	
	}
	
  </script>
    
   
    
    <?php  echo $OUTPUT->footer(); ?>
 