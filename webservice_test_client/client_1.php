<?php

/// SETUP - NEED TO BE CHANGED
$token = '5f2efae8c960fb84077811f83417729c';
$domainname = 'http://dev.cignium.gnosislms.com';

//$token = '061966443e0d6412f547439ff59ef125';//'c78e01ed83c2e6cdb89f2398eb75c0fb'; //for uat
//$domainname = 'https://cigniumuat.gnosisconnect.com';
//$domainname = 'https://cignium.gnosisconnect.com';

$functionname = 'core_user_create_update_users';
// REST RETURNED VALUES FORMAT
$restformat = 'json'; //Also possible in Moodle 2.2 and later: 'json'
                     //Setting it to 'json' will fail all calls on earlier Moodle version
//////// moodle_user_create_users ////////
/// PARAMETERS - NEED TO BE CHANGED IF YOU CALL A DIFFERENT FUNCTION
$user1 = array();
$user1['givenname'] = 'Nabab_ipwtest9feba1';
$user1['surname'] = 'IPW';
$user1['emailaddress'] = 'ipw_9feba1@abc.com';
$user1['co'] = 'United States';
$user1['department'] = 'test_team'; //team name
$user1['managerStatus'] = '';
$user1['uniqueid'] = 'ipw_ext9feb';
$user1['status'] = 1;
$user1['city'] = 'Noida sector-62';
//print_r($user1);die;

$user2 = array();
$user2['givenname'] = 'webserviceuserd';
$user2['surname'] = 'testlastname2';
$user2['emailaddress'] = 'amanda.lupi@tzinsurance.com';
$user2['co'] = 'United States';
$user2['department'] = 'test_team'; //team name
$user2['managerStatus'] = 'manager';
$user2['uniqueid'] = '20000033';
$user2['status'] = 1;
/*
$users3 = array();
$users3['givenname'] = "uidnew2";
$users3['surname'] = "new";
$users3['emailaddress'] = "uidnew2@test.com";
$users3['co'] = 'India';
$users3['department'] = 'test_team'; //team name
$users3['managerStatus'] = '';
$users3['uniqueid'] = 'uidnew2';
$users3['status'] = 1;*/
/*
$users4 = array();
$users4['givenname'] = "webserviceuser4";
$users4['surname'] = "testlast'name4";
$users4['emailaddress'] = "testemail4@moodle.com";
$users4['co'] = 'India';
$users4['department'] = 'test_team'; //team name
$users4['managerStatus'] = 'manager';
$users4['uniqueid'] = 'inf004';
$users4['status'] = 1;*/


$users = array($user1,$user2);
$params = array('users' => $users);
//echo "<pre/>";
//print_r($params);
/// REST CALL
header('Content-Type: text/plain');
$serverurl = $domainname . '/webservice/rest/server.php'. '?wstoken=' . $token . '&wsfunction='.$functionname;


require_once('./curl.php');
$curl = new curl;
//print_r($curl);die;
//if rest format == 'xml', then we do not add the param for backward compatibility with Moodle < 2.2
//echo json_encode($users);die;
$restformat = ($restformat == 'json')?'&moodlewsrestformat=' . $restformat:'';
//echo $serverurl . $restformat;die;
$resp = $curl->post($serverurl . $restformat, $params);
print_r($resp);