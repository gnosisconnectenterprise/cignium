<?php

require_once(dirname(__FILE__) . '/../config.php');
require_once($CFG->dirroot . '/local/user/selector/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->libdir . '/filelib.php');
require_once($CFG->dirroot . '/program/programlib.php');

global $DB,$CFG,$USER;

require_login();

$context = context_system::instance();

$programId = optional_param('program',0, PARAM_INT);

$programName = getPrograms($programId);
$returnurl = $CFG->wwwroot.'/program/index.php';
if(!$programId){
	 redirect($returnurl);
}


$PAGE->set_url('/program/assignusers.php', array('program'=>$programId));
$PAGE->set_pagelayout('admin');

/// Print header
$PAGE->set_title($SITE->fullname.": ". $programName);
$PAGE->set_heading($SITE->fullname);
$PAGE->navbar->add(get_string('manageprograms',"program"), new moodle_url('/program/index.php'));
$PAGE->navbar->add(get_string('allocation'));
echo $OUTPUT->header();


$programs = new program_assignment_selector('', array('programid' => $programId));
$programs->programTeamList();
$programs->programNonTeamList();

$programs->programDepartmentList();
$programs->programNonDepartmentList();

$programs->programUserList();
$programs->programNonUserList();

$program = $DB->get_record('programs', array('id'=>$programId), '*', MUST_EXIST);
$groupinforow = array();
$data = new stdClass();
$data->id=$program->id;
$data->name = $program->name;
$imagePath = getProgramImage($data);
//Start Display picture
$picturecell = new html_table_cell();
$picturecell->attributes['class'] = 'left side picture';
$picturecell->text ='<div class="felement fstatic program-image">'.$imagePath.'</div></div>';
$groupinforow[] = $picturecell;
//End Display picture

// Check if there is a description to display.
$program->description = file_rewrite_pluginfile_urls($program->description, 'pluginfile.php', $context->id, 'program', 'description', $program->id);
if (!isset($program->descriptionformat)) {
	$program->descriptionformat = FORMAT_MOODLE;
}

$options = new stdClass;
$options->overflowdiv = true;

$contentcell = new html_table_cell();
$contentcell->attributes['class'] = 'content';
$contentcell->text = '<b>'.$program->name.'</b></br>';
if($program->description != ''){
	$contentcell->text .= format_text($program->description, $program->descriptionformat, $options);
}
$groupinforow[] = $contentcell;

// Check if we have something to show.
if (!empty($groupinforow)) {
	$groupinfotable = new html_table();
	$groupinfotable->attributes['class'] = 'groupinfobox';
	$groupinfotable->data[] = new html_table_row($groupinforow);
	//echo html_writer::table($groupinfotable);
}

$headerHtml = getModuleHeaderHtml($program, $CFG->programModule);
echo $headerHtml;

/// Print the editing form
$courseHTML = '';
$courseHTML .= "<div class='tabsOuter borderBlockSpace'>";
	if($USER->archetype == $CFG->userTypeAdmin){
		$courseHTML .= "<div class='tabLinks'>";
		$courseHTML .= "</div>";
	}
echo $courseHTML;
/*
$block IS 'team_program'
$block IS 'non_team_program'
$block IS 'department_program'
$block IS 'non_department_program'
$block IS 'user_program'
$block IS 'non_user_program'
*/
$display = '';
if($USER->archetype != $CFG->userTypeAdmin){
	$display = 'style = "display:none"';
}


?>
<form id="assignform" method="post" action="<?php echo $CFG->wwwroot; ?>/program/assignusers.php?program=<?php echo $programId; ?>">
<input type = 'hidden' name = "formsubmit" value = "1">
<?php
echo '<fieldset class="" id="yui_3_13_0_2_1404228340264_14">';
echo '<div class="msg-tab-html">
		<!--<div class="msg-tab-label">'.get_string('enroll_to').'</div> -->
		<div class="msg-tab-block">
			<!-- <div class="tab-msg-all">
				<div class="msg-tab-block" style="float:left;padding:1%;">
					<input type="checkbox" name="department_all" id="department_all"  > All
				</div>
			</div> -->
		<div class="tab-box">';
$activeLink = 'activeLink';
if($USER->archetype == $CFG->userTypeAdmin){
	$activeLink = '';
	echo '<a href="javascript:;" class="tabLink activeLink" id="cont-1" '.$display.'>'.get_string('departments').'</a>';
}
echo '<a href="javascript:;" class="tabLink '.$activeLink.'" id="cont-2">'.get_string('teams').'</a><a href="javascript:;" class="tabLink " id="cont-3">'.get_string('users').'</a> 
		</div>';
$hide = '';
if($USER->archetype == $CFG->userTypeAdmin){
	echo '<div class="tabcontent paddingAll" id="cont-1-1"><div class="departments">';
	$hide = 'hide';
?>
<div id="addmembersform">
    <div>
    <input type="hidden" name="sesskey" value="<?php p(sesskey()); ?>" />

    <table class="assignTable" summary="">
    <tr>
	 <td id='potentialcell' class="potentialcell">
          <p>
            <label for="addselect"><?php echo get_string('coursenondepartment');?></label>
          </p>
         
		  <div id="department_select_wrapper" class="userselector">
			<select multiple="multiple" name = "department_program[]" size = '20'>
				<?php $programs->program_option_list('non_department_program'); ?>
			</select>
            <?php echo getEDAstric('lower-assign-filter', $display);?>
			<!--<div>
				<label for="addselect_searchtext"><?php echo get_string('search');?></label>
				<input type="text" value="" size="15" id="department_addselect_searchtext" name="addselect_searchtext">
				<input type="button" value="Clear" id="department_addselect_clearbutton" >
			</div>-->
            
              <div class="searchBoxDiv">
                <div id="search_non_dept_course"><div class="search-input" ><input class="searchBoxInput" type="text" value="" placeholder="Search" id="department_addselect_searchtext" name="addselect_searchtext" ></div></div>
                
                <div class="search_clear_button search_non_dept_course"><input type="button" title="Search" id="department_addselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="department_addselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
          
            </div>
            
		  </div>
          
      </td>
      <td id='buttonscell'>
        <div class="arrow_button">
            <input class="moveLeftButton" name="remove" id="department_remove" type="button" value="<?php echo get_string('remove').'&nbsp;'.$OUTPUT->rarrow(); ?>" title="<?php print_string('remove'); ?>" disabled = ""/><input class="moveRightButton" name="add" id="department_add" type="button" value="<?php echo $OUTPUT->larrow().'&nbsp;'.get_string('add'); ?>" title="<?php print_string('add'); ?>" disabled = ""/>
        </div>
      </td>
	   <td id='existingcell' class="potentialcell">
          <p>
            <label for="removeselect"><?php echo get_string('coursedepartment');?></label>
          </p>
          <div id="department_remove_wrapper" class="userselector">
			<select multiple="multiple" name = "add_department_program[]" size = '20'>
				<?php $programs->program_option_list('department_program'); ?>
			</select>
             <?php echo getEDAstric('lower-assign-filter', $display);?>
			<!--<div>
				<label for="removeselect_searchtext"><?php echo get_string('search');?></label>
				<input type="text" value="" size="15" id="department_removeselect_searchtext" name="removeselect_searchtext">
				<input type="button" value="Clear" id="department_removeselect_clearbutton" >
			</div>-->
            
            <div class="searchBoxDiv">
                <div id="search_dept_program"><div class="search-input" ><input class="searchBoxInput" type="text" value="" placeholder="Search" id="department_removeselect_searchtext" name="removeselect_searchtext" ></div></div>
                
                <div class="search_clear_button search_dept_program"><input type="button" title="Search" id="department_removeselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="department_removeselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
          
            </div>
            
		  </div>
        </td>
    </tr>
    </table>
    </div>
</div>
<?php
echo '</div>																					
  </div>';
}
echo ' <div class="tabcontent paddingAll '.$hide.'" id="cont-2-1">';
  ?>
<div id="addmembersform">
    <div>
    <input type="hidden" name="sesskey" value="<?php p(sesskey()); ?>" />

    <table class="assignTable" summary="">
    <tr>
	 <td id='potentialcell' class="potentialcell">
          <p>
            <label for="addselect"><?php echo get_string('coursenonteams')." (".get_string('department').")";?></label>
          </p>
		  <div id="team_addselect_wrapper" class="userselector">
			<select multiple="multiple" name = "addteam[]" size = '20'>
				<?php $programs->program_option_list('non_team_program'); ?>
			</select>
			<!--<div>
				<label for="addselect_searchtext"><?php echo get_string('search');?></label>
				<input type="text" value="" size="15" id="team_addselect_searchtext" name="addselect_searchtext">
				<input type="button" value="Clear" id="team_addselect_clearbutton" >
			</div>-->
            
               <div class="searchBoxDiv">
                <div id="search_non_team_program"><div class="search-input" ><input class="searchBoxInput" type="text" value="" placeholder="Search" id="team_addselect_searchtext" name="addselect_searchtext" ></div></div>
                
                <div class="search_clear_button search_non_team_program"><input type="button" title="Search" id="team_addselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="team_addselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
          
            </div>
            
		  </div>
      </td>
      <td id='buttonscell'>
        <div class="arrow_button">
            <input class="moveLeftButton" name="remove" id="team_remove" type="button" value="<?php echo get_string('remove').'&nbsp;'.$OUTPUT->rarrow(); ?>" title="<?php print_string('remove'); ?>" disabled = ""/><input class="moveRightButton" name="add" id="team_add" type="button" value="<?php echo $OUTPUT->larrow().'&nbsp;'.get_string('add'); ?>" title="<?php print_string('add'); ?>" disabled = ""/>
        </div>
      </td>
	   <td id='existingcell' class="potentialcell">
          <p>
            <label for="removeselect"><?php echo get_string('courseteams');?></label><a href="#" name="updateteam" id="updateTeam" class="updateDate"  title = "<?php echo get_string('update_date'); ?>">update</a>
          </p>
          <div id="team_removeselect_wrapper" class="userselector">
			<select multiple="multiple" name = "addteamprograms[]" size = '20'>
				<?php $programs->program_option_list('team_program'); ?>
			</select>
			<!--<div>
				<label for="removeselect_searchtext"><?php echo get_string('search');?></label>
				<input type="text" value="" size="15" id="team_removeselect_searchtext" name="removeselect_searchtext">
				<input type="button" value="Clear" id="team_removeselect_clearbutton" >
			</div>-->
             <div class="searchBoxDiv">
                <div id="search_team_program"><div class="search-input" ><input class="searchBoxInput" type="text" value="" placeholder="Search" id="team_removeselect_searchtext" name="removeselect_searchtext" ></div></div>
                
                <div class="search_clear_button search_team_program"><input type="button" title="Search" id="team_removeselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="team_removeselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
          
            </div>
            
		  </div>
        </td>
    </tr>
    </table>
    </div>
</div>
<?php
	echo '</div><div class="tabcontent paddingAll hide" id="cont-3-1">';
 ?>
<div id="addmembersform">
    <div>
    <input type="hidden" name="sesskey" value="<?php p(sesskey()); ?>" />

    <table class="assignTable" summary="">
    <tr>
	 <td id='potentialcell' class="potentialcell">
          <p>
            <label for="addselect"><?php echo get_string('coursenonuser')." (".get_string('username').")";?></label>
          </p>
		  <div id="user_addselect_wrapper" class="userselector">
			<select multiple="multiple" name = "adduser[]" size = '20'>
				<?php $programs->program_option_list('non_user_program'); ?>
			</select>
			      
		  </div>
          <fieldset>
                <legend><?php echo get_string('filter_legend');?></legend>
                 <div class="searchBoxDiv">
                    <!--label for="addselect_searchtext">'.get_string('search').'</label-->
                    <div id="search-form"><div class="search-input" ><input class="searchBoxInput" type="text" value="" placeholder="Search" id="user_addselect_searchtext" name="addselect_searchtext" ></div></div>
                    
                    <div class="search_clear_button  search-form-non-program-user"><input type="button" title="Search" id="user_addselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="user_addselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
                    
                </div>
                
                <div class="non-enrolled-program-user-filter-box">
                    <?php 
                    ob_start();
                    $filterBox = "non-enrolled-";
                    $filterBoxNonEnrolled = $filterBox;
                    $filter = 1;
                    require_once($CFG->dirroot . '/local/includes/non_enrolled_program_user_filter.php');
                    $SEARCHHTML = ob_get_contents();
                    ob_end_clean();
                    echo $SEARCHHTML;
                    ?>
                </div>
          </fieldset>
      </td>
      <td id='buttonscell'>
        <div class="arrow_button">
            <input class="moveLeftButton" name="remove" id="user_remove" type="button" value="<?php echo get_string('remove').'&nbsp;'.$OUTPUT->rarrow(); ?>" title="<?php print_string('remove'); ?>" disabled = ""/><input class="moveRightButton" name="add" id="user_add" type="button" value="<?php echo $OUTPUT->larrow().'&nbsp;'.get_string('add'); ?>" title="<?php print_string('add'); ?>" disabled = ""/>
        </div>
      </td>
	   <td id='existingcell' class="potentialcell">
          <p>
            <label for="removeselect"><?php echo get_string('courseuser');?></label><a href="#" name="updateuser" id="updateUser" class="updateDate"  title = "<?php echo get_string('update_date'); ?>">update</a>
          </p>
          <div id="user_removeselect_wrapper" class="userselector">
			<select multiple="multiple" name = "adduserprogram[]" size = '20'>
				<?php $programs->program_option_list('user_program'); ?>
			</select>
   
		  </div>
          
          <fieldset>
            <legend><?php echo get_string('filter_legend');?></legend>
             <div class="enrolled-program-user-filter-box">
           
                        <div class="searchBoxDiv">
                            <!--label for="removeselect_searchtext">'.get_string('search').'</label-->
                            <div id="search-form"><div class="search-input" ><input class="searchBoxInput" type="text" value="" placeholder="Search" id="user_removeselect_searchtext" name="removeselect_searchtext" ></div></div>
                            
                            <div class="search_clear_button search-form-program-user"><input type="button" title="Search" id="user_removeselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="user_removeselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
                                    
                                
                        </div>
                
						<?php 
                        ob_start();
                        $filterBox = "enrolled-";
                        $filterBoxEnrolled = $filterBox;
                        $filter = 2; 
                        require($CFG->dirroot . '/local/includes/enrolled_user_program_filter.php');
                        $SEARCHHTML = ob_get_contents();
                        ob_end_clean();
                        echo $SEARCHHTML;
                        ?>
			</div>
         </fieldset> 
        </td>
    </tr>
    </table>
    </div>
    
</div>
<?php
echo '</div></div></div>';
?>
</fieldset>


<!--<fieldset style="margin: 10px 0px 0px; text-align: center; clear: both;">
	<div >
		<div class="fitem fitem_actionbuttons fitem_fgroup" id="fgroup_id_buttonar">
			<div class="felement fgroup" >
                <input type="button" id="id_cancel" class=" btn-cancel" onclick="skipClientValidation = true; return true;" value="<?php echo get_string('cancel')?>" name="cancel">
				<input type="button" id="id_submitbutton" value="<?php echo get_string('savechanges')?>" name="submitbutton">
			</div>
		</div>
	</div>
</fieldset>-->
</form>
<a href="#end-date-div" id="inline-iframe" rel="inline-iframe" title="<?php print_string('allocation'); ?>">click</a>
<div class="end-date-div" id="end-date-div">
	<iframe src="" id="i-frame" width="100%" height="100%"></iframe>
</div>
<script>
function saveDepartmentData(assignElement,assignTo,assignId,elementList){
	if(assignElement != '' && assignTo != '' && assignId != '' && assignId != 0 && elementList != '' && elementList != 0){
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
				type:'POST',
				data:'action=saveCourseEnrolmentData&assignElement='+assignElement+'&assignTo='+assignTo+'&assignId='+assignId+'&elementList='+elementList+'&end_date=',
				success:function(data){
				}
		});
	}
}
function removeElementData(element,courseID,elementIds){
	if(elementIds != '' && elementIds != 0 && elementIds != ',' && courseID != '' && courseID != 0 && element != '' && element != 0 ){
		var queryString = 'action=saveCourseUnEnrolmentData&assignElement=program&assignTo='+element+'&assignId='+courseID+'&elementList='+elementIds;
		$.ajax({
				url		:	'<?php echo $CFG->wwwroot;?>/local/ajax.php',
				type	:	'POST',
				data	:	queryString,
				success	:	function(data){
				}
		});
	}
}
function sortOptions(optionsId){
	optionsId.sort(function(a,b) {
		if (a.text.toUpperCase() > b.text.toUpperCase()) return 1;
		else if (a.text.toUpperCase() < b.text.toUpperCase()) return -1;
		else return 0;
	});
	return optionsId;
}
$(document).ready(function(){
	$("#inline-iframe").colorbox({inline:true, width:"675px", height:"468px"});
	var userRole = "<?php echo $USER->archetype; ?>";
	var userManager = "<?php echo $CFG->userTypeManager; ?>";
	var programId = "<?php echo $programId; ?>";

/***********Search starts****************/
	$("#department_addselect_searchtext_btn").click(function(){
		var searchText = $('#department_addselect_searchtext').val();
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				data:'action=searchProgramNonDepartment&programid='+programId+'&search_text='+searchText,
				success:function(data){
					$('#department_select_wrapper').find('select').html(data);
				}
		});
	});
	$("#department_addselect_clearbutton").click(function(){
		var searchText = '';
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				data:'action=searchProgramNonDepartment&programid='+programId+'&search_text='+searchText,
				success:function(data){
					$('#department_select_wrapper').find('select').html(data);
					$("#department_addselect_searchtext").val("");
				}
		});
	});

	$("#department_removeselect_searchtext_btn").click(function(){
		var searchText = $('#department_removeselect_searchtext').val();
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				data:'action=searchProgramDepartment&programid='+programId+'&search_text='+searchText,
				success:function(data){
					$('#department_remove_wrapper').find('select').html(data);
				}
		});
	});
	$("#department_removeselect_clearbutton").click(function(){
		var searchText = '';
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				data:'action=searchProgramDepartment&programid='+programId+'&search_text='+searchText,
				success:function(data){
					$('#department_remove_wrapper').find('select').html(data);
					$("#department_removeselect_searchtext").val("");
				}
		});
	});

	//************** TEAM ***************
	$("#team_addselect_searchtext_btn").click(function(){
		var searchText = $('#team_addselect_searchtext').val();
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				data:'action=searchProgramNonTeam&programid='+programId+'&search_text='+searchText,
				success:function(data){
					$('#team_addselect_wrapper').find('select').html(data);
				}
		});
	});
	$("#team_addselect_clearbutton").click(function(){
		var searchText = '';
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				data:'action=searchProgramNonTeam&programid='+programId+'&search_text='+searchText,
				success:function(data){
					$('#team_addselect_wrapper').find('select').html(data);
					$("#team_addselect_searchtext").val("");
				}
		});
	});

	$("#team_removeselect_searchtext_btn").click(function(){
		var searchText = $('#team_removeselect_searchtext').val();
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				data:'action=searchProgramTeam&programid='+programId+'&search_text='+searchText,
				success:function(data){
					$('#team_removeselect_wrapper').find('select').html(data);
				}
		});
	});
	$("#team_removeselect_clearbutton").click(function(){
		var searchText = '';
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				data:'action=searchProgramTeam&programid='+programId+'&search_text='+searchText,
				success:function(data){
					$('#team_removeselect_wrapper').find('select').html(data);
					$("#team_removeselect_searchtext").val("");
				}
		});
	});

	//************** USERS ***************
	$("#user_addselect_searchtext_btn").click(function(){
		var searchText = $('#user_addselect_searchtext').val();
		
		var sel_mode = $("input[name=<?php echo $filterBoxNonEnrolled;?>selector]:checked").val();
		var user_group = loopSelected('<?php echo $filterBoxNonEnrolled;?>user_group');
		var department = loopSelected('<?php echo $filterBoxNonEnrolled;?>department');
		//var managers = loopSelected('<?php echo $filterBoxNonEnrolled;?>managers');
		var managers = '-1';
		var team = loopSelected('<?php echo $filterBoxNonEnrolled;?>team');
		//var roles = loopSelected('<?php echo $filterBoxNonEnrolled;?>user_roles');
		var roles = '-1';
		
		var job_title = loopSelected('<?php echo $filterBoxNonEnrolled;?>job_title');
	    var company = loopSelected('<?php echo $filterBoxNonEnrolled;?>company');
		
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				//data:'action=searchProgramNonUser&programid='+programId+'&search_text='+searchText,
				data:'action=searchProgramNonUser&programid='+programId+'&search_text='+searchText+'&sel_mode='+sel_mode+'&user_group='+user_group+'&department='+department+'&managers='+managers+'&team='+team+'&roles='+roles+'&job_title='+job_title+'&company='+company,
				success:function(data){
					$('#user_addselect_wrapper').find('select').html(data);
				}
		});
	});
	$("#user_addselect_clearbutton").click(function(){
		var searchText = '';
		
		var sel_mode = $("input[name=<?php echo $filterBoxNonEnrolled;?>selector]:checked").val();
		var user_group = loopSelected('<?php echo $filterBoxNonEnrolled;?>user_group');
		var department = loopSelected('<?php echo $filterBoxNonEnrolled;?>department');
		//var managers = loopSelected('<?php echo $filterBoxNonEnrolled;?>managers');
		var managers = '-1';
		var team = loopSelected('<?php echo $filterBoxNonEnrolled;?>team');
		//var roles = loopSelected('<?php echo $filterBoxNonEnrolled;?>user_roles');
		var roles = '-1';
		
		var job_title = loopSelected('<?php echo $filterBoxNonEnrolled;?>job_title');
	    var company = loopSelected('<?php echo $filterBoxNonEnrolled;?>company');
		
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				//data:'action=searchProgramNonUser&programid='+programId+'&search_text='+searchText,
				data:'action=searchProgramNonUser&programid='+programId+'&search_text='+searchText+'&sel_mode='+sel_mode+'&user_group='+user_group+'&department='+department+'&managers='+managers+'&team='+team+'&roles='+roles+'&job_title='+job_title+'&company='+company,
				success:function(data){
					$('#user_addselect_wrapper').find('select').html(data);
					$("#user_addselect_searchtext").val("");
				}
		});
	});

	$("#user_removeselect_searchtext_btn").click(function(){
		var searchText = $('#user_removeselect_searchtext').val();
		
		var sel_mode = $("input[name=<?php echo $filterBoxEnrolled;?>selector]:checked").val();
		var user_group = loopSelected('<?php echo $filterBoxEnrolled;?>user_group');
		var department = loopSelected('<?php echo $filterBoxEnrolled;?>department');
		//var managers = loopSelected('<?php echo $filterBoxEnrolled;?>managers');
		var managers = '-1';
		var team = loopSelected('<?php echo $filterBoxEnrolled;?>team');
		//var roles = loopSelected('<?php echo $filterBoxEnrolled;?>user_roles');
		var roles = '-1';
		
		var job_title = loopSelected('<?php echo $filterBoxEnrolled;?>job_title');
	    var company = loopSelected('<?php echo $filterBoxEnrolled;?>company');
		
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				//data:'action=searchProgramUser&programid='+programId+'&search_text='+searchText,
				data:'action=searchProgramUser&programid='+programId+'&search_text='+searchText+'&sel_mode='+sel_mode+'&user_group='+user_group+'&department='+department+'&managers='+managers+'&team='+team+'&roles='+roles+'&job_title='+job_title+'&company='+company,
				success:function(data){
					$('#user_removeselect_wrapper').find('select').html(data);
				}
		});
	});
	$("#user_removeselect_clearbutton").click(function(){
		var searchText = '';
		
		var sel_mode = $("input[name=<?php echo $filterBoxEnrolled;?>selector]:checked").val();
		var user_group = loopSelected('<?php echo $filterBoxEnrolled;?>user_group');
		var department = loopSelected('<?php echo $filterBoxEnrolled;?>department');
		//var managers = loopSelected('<?php echo $filterBoxEnrolled;?>managers');
		var managers = '-1';
		var team = loopSelected('<?php echo $filterBoxEnrolled;?>team');
		//var roles = loopSelected('<?php echo $filterBoxEnrolled;?>user_roles');
		var roles = '-1';
		
		var job_title = loopSelected('<?php echo $filterBoxEnrolled;?>job_title');
	    var company = loopSelected('<?php echo $filterBoxEnrolled;?>company');
		
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				//data:'action=searchProgramUser&programid='+programId+'&search_text='+searchText,
				data:'action=searchProgramUser&programid='+programId+'&search_text='+searchText+'&sel_mode='+sel_mode+'&user_group='+user_group+'&department='+department+'&managers='+managers+'&team='+team+'&roles='+roles+'&job_title='+job_title+'&company='+company,
				success:function(data){
					$('#user_removeselect_wrapper').find('select').html(data);
					$("#user_removeselect_searchtext").val("");
				}
		});
	});
/***********Search ends****************/


	$(".tabLink").each(function(){
      $(this).click(function(){
        tabeId = $(this).attr("id");
        $(".tabLink").removeClass("activeLink");
        $(this).addClass("activeLink");
        $(".tabcontent").addClass("hide");
        $("#"+tabeId+"-1").removeClass("hide");					
        return false;	  
      });
    });
	if(userRole != userManager){
		var fromDepartment = $('#department_select_wrapper select');
		var toDepartment = $('#department_remove_wrapper select');
	}

	var fromTeam = $('#team_addselect_wrapper select');
	var toTeam = $('#team_removeselect_wrapper select');

	var fromUser = $('#user_addselect_wrapper select');
	var toUser = $('#user_removeselect_wrapper select');

	//(document).on("change", "#department_select_wrapper select option",function(){
	$(document).on("change", "[name = 'department_program[]']",function(){
	
		$("#department_add").removeAttr("disabled");
	});
	
	//$(document).on("change", "#department_remove_wrapper select option",function(){
	$(document).on("change", "[name = 'add_department_program[]']",function(){
		$("#department_remove").removeAttr("disabled");
	});

	
	//$(document).on("change", "#team_addselect_wrapper select option",function(){
	$(document).on("change", "[name = 'addteam[]']",function(){
		$("#team_add").removeAttr("disabled");
	});
	
	//$(document).on("change", "#team_removeselect_wrapper select option",function(){
	$(document).on("change", "[name = 'addteamprograms[]']",function(){
		$("#team_remove").removeAttr("disabled");
	});
	
	//$(document).on("change", "#user_addselect_wrapper select option",function(){
	$(document).on("change", "[name = 'adduser[]']",function(){
		$("#user_add").removeAttr("disabled");
	});
	
	//$(document).on("change", "#user_removeselect_wrapper select option",function(){
	$(document).on("change", "[name = 'adduserprogram[]']",function(){
		if($(this).attr('class') == 'option-disable'){
			$("#user_remove").addAttr("disabled");
		}else{
			$("#user_remove").removeAttr("disabled");
		}
	});

	$(document).on("click","#department_add",function(){
			var departmentList = '';
			fromDepartment.find('option:selected').each(function(){
				departmentList += $(this).val()+',';
			});
			if(departmentList == '' || departmentList == ','){
				alert("<?php echo get_string('select_department_to_enrol');?>");
				return false;
			}
			departmentList = departmentList.substr(0, departmentList.length - 1);

			saveDepartmentData('program','department',programId,departmentList);

			fromDepartment.find('option:selected').appendTo(toDepartment.find('optgroup'));
			maintainSelectCount(fromDepartment,toDepartment,"<?php echo get_string('departments'); ?>");

			toDepartmentOptionOptions = sortOptions(toDepartment.find('option'));
			toDepartment.find('optgroup').empty();
			toDepartment.find('optgroup').append(toDepartmentOptionOptions);

			/*fromDepartment.find('option:selected').appendTo(toDepartment.find('optgroup'));
			maintainSelectCount(fromDepartment,toDepartment,"<?php echo get_string('departments'); ?>");*/
	});
	$(document).on("click","#department_remove",function(){
			var departmentList = '';
			toDepartment.find('option:selected').each(function(){
				departmentList += $(this).val()+',';
			});
			if(departmentList == '' || departmentList == ','){
				alert("<?php echo get_string('select_department_to_unenrol');?>");
				return false;
			}
			departmentList = departmentList.substr(0, departmentList.length - 1);
			var source = '<?php echo $CFG->wwwroot;?>/course/unenrolcourse.php?element=department&assigntype=program&assignId='+programId+'&elementList='+departmentList;
			$("#i-frame").attr('src',source);
			$(".cboxContent #end-date-div").attr("style",'display:block');
			$("#inline-iframe").trigger("click");
			/*removeElementData('department',programId,departmentList);
			toDepartment.find('option:selected').appendTo(fromDepartment.find('optgroup'));
			maintainSelectCount(fromDepartment,toDepartment,"<?php echo get_string('departments'); ?>");

			fromDepartmentOptionOptions = sortOptions(fromDepartment.find('option'));
			fromDepartment.find('optgroup').empty();
			fromDepartment.find('optgroup').append(fromDepartmentOptionOptions);*/

			//toDepartment.find('option:selected').appendTo(fromDepartment.find('optgroup'));
			//maintainSelectCount(fromDepartment,toDepartment,"<?php echo get_string('departments'); ?>");
	});
	$(document).on("click","#team_add",function(){
			var teamList = '';
			fromTeam.find('option:selected').each(function(){
				teamList += $(this).val()+',';
			});	
			if(teamList == '' || teamList == ','){
				alert("<?php echo get_string('select_team_to_enrol');?>");
				return false;
			}
			teamList = teamList.substr(0, teamList.length - 1);
			addRemove = 1;
			element = 'team';
			var source = '<?php echo $CFG->wwwroot;?>/course/enrolcourse.php?element=team&assigntype=program&assignId='+programId+'&elementList='+teamList;
			$("#i-frame").attr('src',source);
			$(".cboxContent #end-date-div").attr("style",'display:block');
			$("#inline-iframe").trigger("click");

			//fromTeam.find('option:selected').appendTo(toTeam.find('optgroup'));
			//maintainSelectCount(fromTeam,toTeam,"<?php echo get_string('teams'); ?>");
	});
	$(document).on("click","#updateTeam",function(){
			var teamList = '';
			toTeam.find('option:selected').each(function(){
				teamList += $(this).val()+',';
			});	
			if(teamList == '' || teamList == ','){
				alert("<?php echo get_string('select_team_to_update');?>");
				return false;
			}
			teamList = teamList.substr(0, teamList.length - 1);
			addRemove = 1;
			element = 'team';
			var source = '<?php echo $CFG->wwwroot;?>/course/enrolcourse.php?element=team&action=update&assigntype=program&assignId='+programId+'&elementList='+teamList;
			$("#i-frame").attr('src',source);
			$(".cboxContent #end-date-div").attr("style",'display:block');
			$("#inline-iframe").trigger("click");
	});
	$(document).on("click","#team_remove",function(){
		var teamList = '';
		toTeam.find('option:selected').each(function(){
			teamList += $(this).val()+',';
		});
		if(teamList == '' || teamList == ','){
			alert("<?php echo get_string('select_team_to_unenrol');?>");
			return false;
		}
		teamList = teamList.substr(0, teamList.length - 1);
		var source = '<?php echo $CFG->wwwroot;?>/course/unenrolcourse.php?element=team&assigntype=program&assignId='+programId+'&elementList='+teamList;
		$("#i-frame").attr('src',source);
		$(".cboxContent #end-date-div").attr("style",'display:block');
		$("#inline-iframe").trigger("click");

		//toTeam.find('option:selected').appendTo(fromTeam.find('optgroup'));
		//maintainSelectCount(fromTeam,toTeam,"<?php echo get_string('teams'); ?>");
	});
	$(document).on("click","#user_add",function(){
		var userList = '';
		fromUser.find('option:selected').each(function(){
			userList += $(this).val()+',';
		});
		if(userList == '' || userList == ','){
			alert("<?php echo get_string('select_user_to_enrol');?>");
			return false;
		}
		userList = userList.substr(0, userList.length - 1);
		var source = '<?php echo $CFG->wwwroot;?>/course/enrolcourse.php?element=user&assigntype=program&assignId='+programId+'&elementList='+userList;
		$("#i-frame").attr('src',source);
		$(".cboxContent #end-date-div").attr("style",'display:block');
		$("#inline-iframe").trigger("click");
			//fromUser.find('option:selected').appendTo(toUser.find('optgroup'));
			//maintainSelectCount(fromUser,toUser,"<?php echo get_string('users'); ?>");
	});
	$(document).on("click","#updateUser",function(){
			var userList = '';
			toUser.find('option:selected').each(function(){
				userList += $(this).val()+',';
			});
			if(userList == '' || userList == ','){
				alert("<?php echo get_string('select_user_to_update');?>");
				return false;
			}
			addRemove = 1;
			element = 'user';
			userList = userList.substr(0, userList.length - 1);
			var source = '<?php echo $CFG->wwwroot;?>/course/enrolcourse.php?element=user&action=update&assigntype=program&assignId='+programId+'&elementList='+userList;
			$("#i-frame").attr('src',source);
			$(".cboxContent #end-date-div").attr("style",'display:block');
			$("#inline-iframe").trigger("click");
	});
	$(document).on("click","#user_remove",function(){
		var err = 0;
		var userList = '';
		toUser.find( "option:selected" ).each(function() {
			if($(this).attr('class') == 'option-disable'){
				err = 1;
			}else{
				userList += $(this).val()+',';
			}
		});
		if(userList == '' || userList == ','){
			alert("<?php echo get_string('select_user_to_unenrol');?>");
			return false;
		}
		userList = userList.substr(0, userList.length - 1);
		var source = '<?php echo $CFG->wwwroot;?>/course/unenrolcourse.php?element=user&assigntype=program&assignId='+programId+'&elementList='+userList;
		$("#i-frame").attr('src',source);
		$(".cboxContent #end-date-div").attr("style",'display:block');
		$("#inline-iframe").trigger("click");
		/*if(err == 1){
			alert("<?php echo get_string('assignment_error');?>");
			toUser.find( "option" ).each(function() {
				//$(this).removeAttr('selected');
			});
		}
		maintainSelectCount(fromUser,toUser,"<?php echo get_string('users'); ?>");*/
	});
	$("#id_submitbutton").click(function(){
		if(userRole != userManager){
			fromDepartment.find('optgroup').find("option").each(function(){
				$(this).removeAttr("selected");
			});
		}
		fromTeam.find('optgroup').find("option").each(function(){
			$(this).removeAttr("selected");
		});
		fromUser.find('optgroup').find("option").each(function(){
			$(this).removeAttr("selected");
		});
		if($("#department_all").is(':checked')){
			if(userRole != userManager){
				fromDepartment.find('optgroup').find("option").each(function(){
					$(this).appendTo(toDepartment.find('optgroup'));
				});
			}
			fromTeam.find('optgroup').find("option").each(function(){
				$(this).appendTo(toTeam.find('optgroup'));
			});
			fromUser.find('optgroup').find("option").each(function(){
				$(this).appendTo(toUser.find('optgroup'));
			});
		}
		if(userRole != userManager){
			toDepartment.find('option').prop('selected', true);
		}
		toTeam.find('option').prop('selected', true);
		toUser.find('option').prop('selected', true);
		$("#assignform").submit();
	});
	$("#department_all").click(function(){
		if($("#department_all").is(':checked')){
			$(".tab-box").hide();
			$(".tabcontent").hide();
		}else{
			$(".tab-box").show();
			$(".tabcontent").show();
		}
	});
	$('#id_cancel').click(function(){
		window.location.href = '<?php echo $CFG->wwwroot; ?>/program/index.php';
	});
});
</script>
<style>
#end-date-div{ display:none;}
#colorbox #end-date-div{display:block}
#inline-iframe{ position:absolute; left:-100000px;}
</style>
<?php
echo '</div>';
echo $OUTPUT->footer();