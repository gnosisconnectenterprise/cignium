<?php
	require_once(dirname(__FILE__) . '/../config.php');
	require_once($CFG->dirroot . '/local/user/selector/lib.php');
	require_once($CFG->dirroot . '/course/lib.php');
	require_once($CFG->libdir . '/filelib.php');
    require_once('programlib.php');
	
	global $USER;
	require_login();
	if($USER->archetype != $CFG->userTypeAdmin){ // if user is not manager redirect to home page.
		redirect($CFG->wwwroot."/");
	}

	$programId = required_param('program', PARAM_INT);
	$cancel  = optional_param('cancel', false, PARAM_BOOL);

	$program = $DB->get_record('programs', array('id'=>$programId), '*', MUST_EXIST);

	//Start Assignment Post Data
	if (optional_param('add', false, PARAM_BOOL) && confirm_sesskey()) {
		assignUserToProgram($programId,$_REQUEST['addcourse']);
		redirect($CFG->wwwroot."/program/".$CFG->pageProgramAssignUsers."?program=".$programId);
	}
	if (optional_param('remove', false, PARAM_BOOL) && confirm_sesskey()) {
		removeUserFromProgram($programId,$_REQUEST['removecourse']);
		redirect($CFG->wwwroot."/program/".$CFG->pageProgramAssignUsers."?program=".$programId);
	}
	//End Assignment Post Data

	$context = context_system::instance();
	$returnUrl = $CFG->wwwroot.'/program/index.php';

	if ($cancel) {
		redirect($returnUrl);
	}
	//Start Setting Page data
	$PAGE->set_url('/program/'.$CFG->pageProgramAssignUsers, array('program'=>$programId));
	$PAGE->set_pagelayout('globaladmin');
	$PAGE->navbar->add(get_string('manageprograms',"program"), new moodle_url('/program/index.php'));
	$PAGE->navbar->add(get_string('assignusers', 'program'));
	$PAGE->set_title("$SITE->fullname: ".get_string('assignusers', 'program'));
	$PAGE->set_heading($SITE->fullname);
	echo $OUTPUT->header();
	//End Setting Page data

	//Start Set Array of assigned and unassigned data
	$users = new program_users_selector('', array('programId' => $programId)); 
	$users->get_non_program_users();
	$users->get_program_users();
	//End Set Array of assigned and unassigned data

	$groupinforow = array();

	// Check if there is a picture to display.
	if($program->picture != ''){
		$view =  $CFG->dirroot.'/theme/gourmet/pix/program/'.$program->picture;
		$view1 =  $CFG->wwwroot.'/theme/gourmet/pix/program/'.$program->picture;
	}else{
		$view =  $CFG->dirroot.'/theme/gourmet/pix/program-img.jpg';
		$view1 =  $CFG->wwwroot.'/theme/gourmet/pix/program-img.jpg';
	}
	//Start Display picture
	$picturecell = new html_table_cell();
	$picturecell->attributes['class'] = 'left side picture';
	$picturecell->text ='<div class="felement fstatic"><img width="64" height="64" class="userpicture defaultuserpic" src="'.$view1.'"></div></div>';
	$groupinforow[] = $picturecell;
	//End Display picture

	// Check if there is a description to display.
	$program->description = file_rewrite_pluginfile_urls($program->description, 'pluginfile.php', $context->id, 'program', 'description', $program->id);
	if (!isset($program->descriptionformat)) {
		$program->descriptionformat = FORMAT_MOODLE;
	}

	$options = new stdClass;
	$options->overflowdiv = true;

	$contentcell = new html_table_cell();
	$contentcell->attributes['class'] = 'content';
	$contentcell->text = '<b>'.$program->name.'</b></br>';
	if($program->description != ''){
		$contentcell->text .= format_text($program->description, $program->descriptionformat, $options);
	}
	$groupinforow[] = $contentcell;

	// Check if we have something to show.
	if (!empty($groupinforow)) {
		$groupinfotable = new html_table();
		$groupinfotable->attributes['class'] = 'groupinfobox';
		$groupinfotable->data[] = new html_table_row($groupinforow);
		echo html_writer::table($groupinfotable);
	}

	/// Print the editing form
	?>

	<div id="addmembersform">
		<form id="assignform" method="post" action="<?php echo $CFG->wwwroot; ?>/program/<?php echo $CFG->pageProgramAssignUsers; ?>?program=<?php echo $programId; ?>">
			<div>
				<input type="hidden" name="sesskey" value="<?php p(sesskey()); ?>" />
				<table class="generaltable generalbox groupmanagementtable boxaligncenter" summary="">
					<tr>
						<td id='potentialcell'>
						  <p><label for="addselect"><?php echo get_string('nonprogramusers','program');?></label></p>
						  <div id="addselect_wrapper" class="userselector">
							<select multiple="multiple" name = "addcourse[]" size = '20'>
								<?php 
									$users->user_option_list(1);
									// This function will print users options
								?>
							</select>
							<div>
								<label for="addselect_searchtext"><?php echo get_string('search');?></label>
								<input type="text" value="" size="15" id="addselect_searchtext" name="addselect_searchtext">
								<input type="button" value="Clear" id="addselect_clearbutton" >
							</div>
						  </div>
						</td>
						<td id='buttonscell'>
							<p class="arrow_button">
								<input name="add" id="add" type="submit" value="<?php echo $OUTPUT->larrow().'&nbsp;'.get_string('add'); ?>" title="<?php print_string('add'); ?>" disabled = ""/>
								<input name="remove" id="remove" type="submit" value="<?php echo get_string('remove').'&nbsp;'.$OUTPUT->rarrow(); ?>" title="<?php print_string('remove'); ?>" disabled = ""/>
							</p>
						</td>
						<td id='existingcell'>
							  <p><label for="removeselect"><?php echo get_string('programusers','program');?></label></p>
							  <div id="removeselect_wrapper" class="userselector">
								<select multiple="multiple" name = "removecourse[]" size = '20'>
									<?php $users->user_option_list(2); ?>
								</select>
								<div>
									<label for="removeselect_searchtext"><?php echo get_string('search');?></label>
									<input type="text" value="" size="15" id="removeselect_searchtext" name="removeselect_searchtext">
									<input type="button" value="Clear" id="removeselect_clearbutton" >
								</div>
							 </div>
						</td>
					</tr>
					<tr><td colspan="3" id='backcell'>
						<input type="submit" name="cancel" value="<?php print_string('backtoprograms', 'program'); ?>" />
					</td></tr>
				</table>
			</div>
		</form>
	</div>
	<script>
	$(document).ready(function(){
		//Unassigned course search
		$("#addselect_searchtext").keyup(function(){
			var searchText = $(this).val();
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/group/searchcourses.php',
					type:'POST',
					data:'action=searchNonProgramUser&programId=<?php echo $programId; ?>&search_text='+searchText,
					success:function(data){
						$('#addselect_wrapper').find('select').html(data);
					}
			});
		});

		//Assigned course search
		$("#addselect_clearbutton").click(function(){
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/group/searchcourses.php',
					type:'POST',
					data:'action=searchNonProgramUser&programId=<?php echo $programId; ?>',
					success:function(data){
						$('#addselect_wrapper').find('select').html(data);
						$("#addselect_searchtext").val('');
					}
			});
		});

		//unassigned course clear search
		$("#removeselect_clearbutton").click(function(){
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/group/searchcourses.php',
					type:'POST',
					data:'action=searchProgramUser&programId=<?php echo $programId; ?>',
					success:function(data){
						$('#removeselect_wrapper').find('select').html(data);
						$("#removeselect_searchtext").val('');
					}
			});
		});
		//assigned course clear search
		$("#removeselect_searchtext").keyup(function(){
			var searchText = $(this).val();
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/group/searchcourses.php',
					type:'POST',
					data:'action=searchProgramUser&programId=<?php echo $programId; ?>&search_text='+searchText,
					success:function(data){
						$('#removeselect_wrapper').find('select').html(data);
					}
			});
		});
		//enable add icon
		$(document).on("click", "#addselect_wrapper select option",function(){
			$("#add").removeAttr("disabled");
		});
		//enable remove icon
		$(document).on("click", "#removeselect_wrapper select option",function(){

			$("#remove").removeAttr("disabled");
		});
	});
	</script>
	<?php
	echo $OUTPUT->footer();