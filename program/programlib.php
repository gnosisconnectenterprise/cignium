<?php
	defined('MOODLE_INTERNAL') || die();
	############################## Start Program Functions ##########################
	
	/**
	 * Returns the display name of a field in the program table. Works for most fields that are commonly displayed to programs.
	 * @param string $field Field name, e.g. 'pname'
	 * @return string Text description taken from language file, e.g. 'Name'
	 */
	
	function getProgramFieldName($field) {
		// Some fields have language strings which are not the same as field name.
		switch ($field) {
		
			
			case 'name' : {
				return get_string('name','program');
			}
			case 'description' : {
				return get_string('description','program');
			}
		   
			case 'timecreated' : {
				return get_string('timecreated');
			}
			case 'createdby' : {
				 return get_string('createdby');
			}
			case 'timemodified' : {
				 return get_string('timemodified');
			}
			case 'modifiedby' : {
				 return get_string('modifiedby');
			}
			case 'status' : {
				 return get_string('status');
			}
			
		}
		// Otherwise just use the same lang string.
		return get_string($field);
	}
	
	/**
	 * Return filtered (if provided) list of programs in site
	 *
	 * @param string $sort An SQL field to sort by
     * @param string $dir The sort direction ASC|DESC
	 * @param int $page The page or records to return
	 * @param int $perpage The number of records to return per page
	 * @param string $search A simple string to search for
	 * @param string $extraSelect An additional SQL select statement to append to the query
	 * @param array $extraParams Additional parameters to use for the above $extraSelect
	 * @return object Array of program records
	 */
	 
	 
	 
	function getProgramsListing($sort='mp.id', $dir='ASC', $page=0, $perpage=0,
							   $search='', $extraSelect='', array $paramArray=null) {
		global $DB, $CFG,$USER;
		$searchString = "AND ";
		if($paramArray['ch'] != '') {
			if($paramArray['ch'] == 'OTH'){
				$searchString .= "(mp.name REGEXP '^[^a-zA-Z]') AND";
			}else{
				$searchString .= "(mp.name like '".$paramArray['ch']."%') AND ";
			}
		}
		$searchKeyAll = true;
		if( ($paramArray['name']==1) || ($paramArray['des']==1) )
			$searchKeyAll = false;

		$paramArray['key'] = addslashes($paramArray['key']);
		if($searchKeyAll && ($paramArray['key'] != '')) {
			$searchString .= " (mp.name like '%".$paramArray['key']."%' OR strip_tags(mp.description) like '%".$paramArray['key']."%' ) AND";

		} else if ($paramArray['key'] != '') {
			$searchKeyAllString .= "(";
			if( $paramArray['name']==1 )
				$searchKeyAllString .= " mp.name like '%".$paramArray['key']."%' OR";
			if( $paramArray['des']==1 )
				$searchKeyAllString .= " strip_tags(mp.description) like '%".$paramArray['key']."%' OR";

			$searchKeyAllString = substr($searchKeyAllString, 0, -2);
			$searchKeyAllString .= ") AND";
			$searchString .= $searchKeyAllString;
		}
		$searchString .= " 1=1";
		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$searchString .= '';
				break;
			CASE $CFG->userTypeManager:					
				   $extraCondForRole = " AND (r.id = 1 OR r.archetype = '".$CFG->userTypeAdmin."')"; 
				   $adminRoleUsers = getAllUserRoleDetails($extraCondForRole);
				   $adminRoleUserIds = 0;
				   $adminRoleUsersArr = array();
				   if(count($adminRoleUsers) > 0 ){
					  $adminRoleUsersArr = array_keys($adminRoleUsers);
					  $adminRoleUserIds = implode(",",$adminRoleUsersArr);
				   }
	   			
					$searchString .= " AND (mp.id IN (SELECT p.program_id FROM mdl_user as u
								LEFT JOIN mdl_department_members as dm ON dm.userid = u.id
								LEFT JOIN mdl_program_department as p ON p.department_id = dm.departmentid AND p.is_active = 1
								WHERE u.id = $USER->id)) AND if(mp.createdby IN (".$adminRoleUserIds."), mp.status IN (1), mp.status IN (0,1)) ";
				break;
		}
		if($sort == 'programCount'){
			$sort = '';
			$limit = '';
			$programCount = $DB->get_record_sql("SELECT count(DISTINCT(mp.id)) as programcount FROM {$CFG->prefix}programs mp WHERE 1 = 1 AND mp.deleted = '0'   $searchString ");
			return $programCount->programcount;
		}else{
			$sort = 'mp.name';
			$dir = 'ASC';
			if ($sort) {
				$sort = $sort;
				$sort = " ORDER BY $sort $dir";
			}
			$page = $page - 1;
			$page = $page*$perpage;
			$limit = '';
			if($perpage != 0){
				$limit = "LIMIT $page,$perpage";
			}
			$query = "SELECT mp.id, mp.*, CONCAT(mu.firstname, '', mu.lastname) as createdbyname FROM {$CFG->prefix}programs as mp LEFT JOIN {$CFG->prefix}user mu ON (mu.id = mp.createdby) WHERE 1 = 1 AND mp.deleted = '0' $searchString $sort $limit";
			$results =  $DB->get_records_sql($query);
			return $results;
		}
	
	}
	
	/**
	 *  To check, if program is exist for the same name
	 *  @param string $name is name of the program
	 *  @return bool true, if exist or return false
	*/
	
	function isProgramAlreadyExist($name) {
	
		global $DB;
		
		if($name){
			$data = $DB->get_records('programs', array('name' => $name), 'name ASC');
			foreach ($data as $program) {
				if (trim($program->name) == trim($name) && $program->deleted == 0  && $program->archive == 0) {
					return $program->id;
				}
			}
		}
		return false;
	}
	
	/**
	 *  To check, if program id is exist for the same id
	 *  @param string $idnumber is id of the program
	 *  @return bool true, if exist or return false
	*/
	
	function isProgramIdAlreadyExist($idnumber) {
	
		global $DB;
		
		if($idnumber){
			$data = $DB->get_records('programs', array('idnumber' => $idnumber), 'idnumber ASC');
			foreach ($data as $program) {
				if (trim($program->idnumber) == trim($idnumber) && $program->deleted == 0  && $program->archive == 0) {
					return $program->id;
				}
			}
		}
		return false;
	}
	
	
	/**
	 * Add a new program
	 *
	 * @param stdClass $data program properties
	 * @param stdClass $editForm
	 * @param array $editOrOptions editor content
	 * @return id of program or false if error
	 */
	 
	function createProgram($data, $editForm = false, $editOrOptions = false) {
	
		global $CFG, $DB, $USER;

		$data->timecreated  = time();
		$data->timemodified = $data->timecreated;
		$data->createdby = $USER->id;
		$data->updatedby = $USER->id;
		$data->name         = trim($data->name);
		$programMaterial = $data->programmaterial_filemanager;
		$programImage = $data->programimage_filemanager;
		
		if ($editOrOptions) {
			// summary text is updated later, we need context to store the files first
			$data->description = '';
			$data->description_format = FORMAT_HTML;
			$data->suggesteduse = '';
			$data->suggesteduseformat = FORMAT_HTML;
			$data->learningobj = '';
			$data->learning_objformat = FORMAT_HTML;
			$data->performanceout = '';
			$data->performanceoutformat = FORMAT_HTML;
		}
		$data->id = $DB->insert_record('programs', $data);
		
		$maxContextId = $DB->get_field_sql("SELECT max(id) FROM {$CFG->prefix}context");
		$contextLevel = 90;
		$depth = 3;
		$maxContextId = $maxContextId + 1;
		$contextPath = "/1/".$depth."/".$maxContextId;
		$component = 'program';
		
		$query = "INSERT INTO {$CFG->prefix}context set contextlevel = '".$contextLevel."' , instanceid = '".$data->id."', path = '".$contextPath."', depth = '".$depth."'";
		executeSql($query);
			
		$programId = $data->id;
		$contextLevel = 90; 
	    $query = "SELECT * FROM {$CFG->prefix}context WHERE contextlevel = '".$contextLevel."' AND instanceid = '".$programId."' ";
	    $contextProgram = $DB->get_record_sql($query);
		  
		if ($filesOptions = programOverviewFilesOptions($programId)) {
		 
		  $data = file_postupdate_standard_filemanager($data, 'programimage', $filesOptions, $contextProgram, 'program', 'programimage', 0);
		  
		}

		$data = file_postupdate_standard_filemanager($data, 'programmaterial', array('subdirs'=>0), $contextProgram, 'program', 'programmaterial', 0);
	
		
		
		if ($editOrOptions) {
				// Save the files used in the summary editor and store
				$data = file_postupdate_standard_editor($data, 'description', $editOrOptions, $contextProgram, 'program', 'description', 0);
				$data = file_postupdate_standard_editor($data, 'suggesteduse', $editOrOptions, $contextProgram, 'program', 'suggesteduse', 0);
				$data = file_postupdate_standard_editor($data, 'learningobj', $editOrOptions, $contextProgram, 'program', 'learningobj', 0);
				$data = file_postupdate_standard_editor($data, 'performanceout', $editOrOptions, $contextProgram, 'program', 'performanceout', 0);
	
				$DB->set_field('programs', 'description', $data->description, array('id'=>$programId));
				$DB->set_field('programs', 'descriptionformat', $data->description_format, array('id'=>$programId));
				$DB->set_field('programs', 'suggesteduse', $data->suggesteduse, array('id'=>$programId));
				$DB->set_field('programs', 'suggesteduseformat', $data->suggesteduseformat, array('id'=>$programId));
				$DB->set_field('programs', 'learningobj', $data->learningobj, array('id'=>$programId));
				$DB->set_field('programs', 'learningobjformat', $data->learning_objformat, array('id'=>$programId));
				$DB->set_field('programs', 'performanceout', $data->performanceout, array('id'=>$programId));
				$DB->set_field('programs', 'performanceoutformat', $data->performanceoutformat, array('id'=>$programId));
       }
		$program = $DB->get_record('programs', array('id'=>$data->id));
		if($USER->archetype == $CFG->userTypeManager){
			$userDepartment = $DB->get_record('department_members',array('userid'=>$USER->id,'is_active'=>1));
			if (!empty($userDepartment)){
				$assigned = assignProgramToDepartment($program->id, $userDepartment->departmentid);
			}
		}
		return $program->id;
	}
	
	/**
	 * Update program
	 *
	 * @param stdClass $data program properties (with magic quotes)
	 * @param stdClass $editForm
	 * @param array $editOrOptions
	 * @return bool true or exception
	 */
	 

	function updateProgram($data, $editForm = false, $editOrOptions = false) {
		global $CFG, $DB, $USER;

		
		$data->timemodified = time();
		$data->updatedby = $USER->id;
		$data->name         = trim($data->name);
		
		$context = context_system::instance();
		if ($editOrOptions) {
			$data = file_postupdate_standard_editor($data, 'description', $editOrOptions, $context, 'program', 'description', 0);
			$data = file_postupdate_standard_editor($data, 'suggesteduse', $editOrOptions, $context, 'program', 'suggesteduse', 0);
			$data = file_postupdate_standard_editor($data, 'learningobj', $editOrOptions, $context, 'program', 'learningobj', 0);
			$data = file_postupdate_standard_editor($data, 'performanceout', $editOrOptions, $context, 'program', 'performanceout', 0);
		}
	
		if(isset($data->changeownership) && $data->changeownership !=0){
			$data->createdby = $data->changeownership;
		}
		
		$data->is_certificate = isset($data->is_certificate)?$data->is_certificate:0;
		
		$programMaterial = $data->programmaterial_filemanager;
		$programImage = $data->programimage_filemanager;
		
		$DB->update_record('programs', $data);
		$programId = $data->id;
		$contextLevel = 90; 
	    $query = "SELECT * FROM {$CFG->prefix}context WHERE contextlevel = '".$contextLevel."' AND instanceid = '".$programId."' ";
	    $contextProgram = $DB->get_record_sql($query);
		if ($filesOptions = programOverviewFilesOptions($programId)) {
		    $data = file_postupdate_standard_filemanager($data, 'programimage', $filesOptions, $contextProgram, 'program', 'programimage', 0);
		}
		
        $data = file_postupdate_standard_filemanager($data, 'programmaterial', array('subdirs'=>0), $contextProgram, 'program', 'programmaterial', 0);
		return true;
	}
	
	
	  
	  
	  /**
	 * Delete a program best effort, first removing members and links with courses .
	 * Removes program avatar too.
	 *
	 * @param mixed $programId The id of program to delete or full program object
	 * @return bool True if deletion was successful, false otherwise
	 */
	

	function deleteProgram($programId) {
		global $CFG, $DB, $USER;
	
		if ($programId){
		
		    $program = new stdClass();
			$program->id = $programId;
			$program->deleted = 1;	
			
			if (!$updated = $DB->update_record('programs', $program)) {
				return true;
			}
			
		    $query = "UPDATE {$CFG->prefix}user_course_mapping set status = '1' WHERE type = 'program' AND typeid = '".$programId."'";
			executeSql($query);
		}

	
		return true;
	}
	
	/**
	 * Returns options to use in course overviewfiles filemanager
	 *
	 * @param null|stdClass|course_in_list|int $course either object that has 'id' property or just the course id;
	 *     may be empty if course does not exist yet (course create form)
	 * @return array|null array of options such as maxfiles, maxbytes, accepted_types, etc.
	 *     or null if overviewfiles are disabled
	 */
	 
 
  function programOverviewFilesOptions($program) {
    global $CFG, $DB;
    if (empty($CFG->programOverviewFilesLimit)) {
        return null;
    }

    $accepted_types = preg_split('/\s*,\s*/', trim($CFG->programOverviewFilesExt), -1, PREG_SPLIT_NO_EMPTY);
    if (in_array('*', $accepted_types) || empty($accepted_types)) {
        $accepted_types = '*';
    } else {
        // Since config for $CFG->programOverviewFilesExt is a text box, human factor must be considered.
        // Make sure extensions are prefixed with dot unless they are valid typegroups
        foreach ($accepted_types as $i => $type) {
            if (substr($type, 0, 1) !== '.') {
                require_once($CFG->libdir. '/filelib.php');
                if (!count(file_get_typegroup('extension', $type))) {
                    // It does not start with dot and is not a valid typegroup, this is most likely extension.
                    $accepted_types[$i] = '.'. $type;
                    $corrected = true;
                }
            }
        }
        if (!empty($corrected)) {
            set_config('programOverviewFilesExt', join(',', $accepted_types));
        }
    }
    $options = array(
        'maxfiles' => $CFG->programOverviewFilesLimit,
        'maxbytes' => $CFG->maxbytes,
        'subdirs' => 0,
        'accepted_types' => $accepted_types
    );
    if (!empty($program->id)) {
	
	  $contextLevel = 90; 
	  $query = "SELECT * FROM {$CFG->prefix}context WHERE contextlevel = '".$contextLevel."' AND instanceid = '".$program->id."' ";
	  $context = $DB->get_record_sql($query);
      $options['context'] = $context;
	  
	  
    } else if (is_int($program) && $program > 0) {
	  
	  $contextLevel = 90; 
	  $query = "SELECT * FROM {$CFG->prefix}context WHERE contextlevel = '".$contextLevel."' AND instanceid = '".$program."' ";
	  $context = $DB->get_record_sql($query);
	  $options['context'] = $context;
    }
	
	
     return $options;
	
   }
   /**
	 * Lists users in a group based on their role on the course.
	 * Returns false if there's an error or there are no users in the group.
	 * Otherwise returns an array of role ID => role data, where role data includes:
	 * (role) $id, $shortname, $name
	 * $users: array of objects for each user which include the specified fields
	 * Users who do not have a role are stored in the returned array with key '-'
	 * and pseudo-role details (including a name, 'No role'). Users with multiple
	 * roles, same deal with key '*' and name 'Multiple roles'. You can find out
	 * which roles each has by looking in the $roles array of the user object.
	 *
	 * @param int $groupId
	 * @param int $courseid Course ID (should match the group's course)
	 * @param string $fields List of fields from user table prefixed with u, default 'u.*'
	 * @param string $sort SQL ORDER BY clause, default (when null passed) is what comes from users_order_by_sql.
	 * @param string $extrawheretest extra SQL conditions ANDed with the existing where clause.
	 * @param array $whereorsortparams any parameters required by $extrawheretest (named parameters).
	 * @return array Complex array as described above
	 */
	 
	function programMembersByRole($programId=0, $courseId = 0, $fields='u.*',
		$sort=null, $extrawheretest='', $whereorsortparams=array()) {
		global $DB;
	
		$relatedctxsql = '';
		$relatedctxparams = '';
		$context = context_system::instance();
		if($courseId){
		
			// Retrieve information about all users and their roles on the course or
			// parent ('related') contexts
			//$context = context_course::instance($courseId);
		
			// We are looking for all users with this role assigned in this context or higher.
			//list($relatedctxsql, $relatedctxparams) = $DB->get_in_or_equal($context->get_parent_context_ids(true), SQL_PARAMS_NAMED, 'relatedctx');
	
		}
		
		
		if ($extrawheretest) {
			$extrawheretest = ' AND ' . $extrawheretest;
		}
	
		if (is_null($sort)) {
			list($sort, $sortparams) = users_order_by_sql('u');
			$whereorsortparams = array_merge($whereorsortparams, $sortparams);
		}
		
		$extrasql = '';
		$sql = "SELECT r.id AS roleid, u.id AS userid, $fields
				  FROM {program_members} gm
				  JOIN {user} u ON u.id = gm.userid
			 LEFT JOIN {role_assignments} ra ON (ra.userid = u.id $extrasql)
			 LEFT JOIN {role} r ON r.id = ra.roleid";
			 
		 $sql .= "  WHERE 1 = 1 and ra.roleid = 5";
		 if($programId){
		  $sql .= " AND gm.programid=:mprogramid";
		 }
			 
		 $sql .= $extrawheretest." ORDER BY r.sortorder, $sort";
		  if($programId){
			$whereorsortparams = array_merge($whereorsortparams, array('mprogramid' => $programId));
		  }
		$rs = $DB->get_recordset_sql($sql, $whereorsortparams);
		return programsCalculateRolePeople($rs, $context);
	}
	
	
	/**
	 * results of a database query that includes a list of users and possible
	 * roles on a course.
	 *
	 * @param moodle_recordset $rs The record set (may be false)
	 * @param int $context ID of course context
	 * @return array 
	 */
	 
	function programsCalculateRolePeople($rs, $context) {
		global $CFG, $DB;
	
		if (!$rs) {
			return array();
		}
	
		$allroles = array(); 
		if($context){
		  $allroles = role_fix_names(get_all_roles($context), $context);
		}
	
		// Array of all involved roles
		$roles = array();
		// Array of all retrieved users
		$users = array();
		// Fill arrays
		foreach ($rs as $rec) {
			// Create information about user if this is a new one
			if (!array_key_exists($rec->userid, $users)) {
				// User data includes all the optional fields, but not any of the
				// stuff we added to get the role details
				$userdata = clone($rec);
				unset($userdata->roleid);
				unset($userdata->roleshortname);
				unset($userdata->rolename);
				unset($userdata->userid);
				$userdata->id = $rec->userid;
	
				// Make an array to hold the list of roles for this user
				$userdata->roles = array();
				$users[$rec->userid] = $userdata;
			}
			// If user has a role...
			if (!is_null($rec->roleid)) { 
				// Create information about role if this is a new one
				if (!array_key_exists($rec->roleid, $roles)) { 
					$role = count($allroles) > 0?$allroles[$rec->roleid]:0;
					$roledata = new stdClass();
					$roledata->id        = $role->id;
					$roledata->shortname = $role->shortname;
					$roledata->name      = $role->localname;
					$roledata->users = array();
					$roles[$roledata->id] = $roledata;
				}
				// Record that user has role
				$users[$rec->userid]->roles[$rec->roleid] = $roles[$rec->roleid];
			}
		}
		
		$rs->close();
	
		// Return false if there weren't any users
		if (count($users) == 0) {
			return false;
		}
	
		// Add pseudo-role for multiple roles
		$roledata = new stdClass();
		$roledata->name = get_string('multipleroles','role');
		$roledata->users = array();
		$roles['*'] = $roledata;
	
		$roledata = new stdClass();
		$roledata->name = get_string('noroles','role');
		$roledata->users = array();
		$roles[0] = $roledata;
	
		// Now we rearrange the data to store users by role
		foreach ($users as $userId=>$userdata) {
			$rolecount = count($userdata->roles);
			if ($rolecount == 0) {
				// does not have any roles
				$roleid = 0;
			} else if($rolecount > 1) {
				$roleid = '*';
			} else {
				$userrole = reset($userdata->roles);
				$roleid = 5;
			}
			
			$roles[$roleid]->users[$userId] = $userdata;
		}
	
		// Delete roles not used
		foreach ($roles as $key=>$roledata) {
			if (count($roledata->users)===0) {
				unset($roles[$key]);
			}
		}
	
		// Return list of roles containing their users
		return $roles;
	}
	  /**
	 * This function add courses to program
	 * @global object
	 * @param int $programId program Id
	 * @param array $courses course Id array
	 * @return nothing
	 * assign course to program
	 */
	function assignCourseToProgram($programId,$courses){
		global $DB,$USER,$CFG;
		
		if(!empty($courses)){
			$time = time();
			$programLastOrder = $DB->get_record_sql('SELECT count(*) as last_order FROM mdl_program_course as pc WHERE pc.is_active = 1 AND  pc.programid = '.$programId);
			if(!empty($programLastOrder)){
				$order = $programLastOrder->last_order + 1;
			}else{
				$order = 1;
			}
			foreach($courses as $course){
				$programCourse = $DB->get_record('program_course',array('courseid'=>$course,'programid'=>$programId));
				
				if(empty($programCourse)){
					$programCourseTb = new stdClass;
					$programCourseTb->courseid = $course;
					$programCourseTb->programid = $programId;
					$programCourseTb->is_active = 1;
					$programCourseTb->timecreated = $time;
					$programCourseTb->timemodified = $time;
					$programCourseTb->course_order = $order;
					$programCourseTb = $DB->insert_record('program_course', $programCourseTb);
				}else{
					if($programCourse->is_active == 0){
						$programCourseTb = new stdClass;
						$programCourseTb->id = $programCourse->id;
						$programCourseTb->is_active = 1;
						$programCourseTb->course_order = $order;
						$programCourseTb = $DB->update_record('program_course', $programCourseTb);
					}
				}
				$order++;
			}
		}
	}
	/**
	 * This function removes courses to program
	 * @global object
	 * @param int $programId program Id
	 * @param array $courses course Id array
	 * @return nothing
	 */
	function removeCourseFromProgram($programId,$courses){
		global $DB,$USER,$CFG;
		if(!empty($courses)){
			$time = time();
			foreach($courses as $course){
				$record = $DB->get_record('program_course', array("courseid" => $course,"programid" => $programId));
				if(!empty($record)){
					$userEnrolCourse = new stdClass;
					$userEnrolCourse->is_active = 0;
					$userEnrolCourse->id = $record->id;
					$last_id = $DB->update_record('program_course',$userEnrolCourse);
					$DB->execute("UPDATE mdl_program_course SET course_order = course_order - 1 WHERE programid = ".$programId." AND course_order >".$record->course_order);
				}
			}
		}
	}

	/**
	 * This function removes courses to program
	 * @global object
	 * @param int $programId program Id
	 * @param array $users user Id array
	 * @return nothing
	 */
	function removeUserFromProgram($programId,$users){
		global $DB,$USER,$CFG;
		if(!empty($users)){
			$time = time();
			$programCourses =  $DB->get_records('program_course',array('programid'=>$programId));
			foreach($users as $user){
			
				if(!empty($programCourses)){
					foreach($programCourses as $programCourse){
					
						$record = $DB->get_record('user_course_mapping', array("courseid" => $programCourse->courseid,  "userid" => $user,"typeid" => $programId,"type"=>'program'));
						if(!empty($record)){
							$userEnrolCourse = new stdClass;
							$userEnrolCourse->status = 0;
							$userEnrolCourse->id = $record->id;
							$last_id = $DB->update_record('user_course_mapping',$userEnrolCourse);
						}
						
					}	
				
				}
				
				
				$record = $DB->get_record('program_members', array("userid" => $user,"programid" => $programId));
				if(!empty($record)){
					$DB->delete_records_select('program_members','id='.$record->id);
				}
						
			}
			
		}
	}
	
	
	 /**
	 * This function deactivate's a program's department
	 * @global object $USER,$CFG,$DB
	 * @param int $programId program Id
	 * @param array $department Deparment Id array
	 * @param Text $remarks decline remarks
	 * @return nothing
	 */
	function deactivateProgramDepartments($programId = 0, $department = 0,$remarks = ''){
		global $USER,$CFG,$DB;
		if($programId != 0){
			$where = '';
			if($department != 0){
				$where = ' AND department_id = '.$department;
			}
			$query = "SELECT id,department_id FROM mdl_program_department WHERE program_id = ".$programId.$where;
			$departments = $DB->get_records_sql($query);
			if(!empty($departments)){
				foreach($departments as $department){
					$mappingData = new stdClass;
					$mappingData->is_active = 0;
					$mappingData->id = $department->id;
					$mappingData->remarks = $remarks;
					$last_id = $DB->update_record('program_department',$mappingData);
					add_to_log(1, 'Unenrol department in program', 'remove', '/program/assignusers.php?program='.$programId.'&departmentid='.$department->id, "Unenrol department in program", 0, $USER->id);
				}
			}
		}
	}
	/**
	 * This function deactivate's a program's team
	 * @global object $USER,$CFG,$DB
	 * @param int $programId program Id
	 * @param array $group Group Id array
	 * @param Text $remarksText decline remarks
	 * @return nothing
	 */
	function deactivateProgramGroups($programId = 0, $group = 0,$remarksText = ""){
		global $USER,$CFG,$DB;
		if($programId != 0){
			$where = '';
			if($group != 0){
				$where = ' AND group_id = '.$group;
			}
			$query = "SELECT id,group_id FROM mdl_program_group WHERE program_id = ".$programId.$where;
			$groups = $DB->get_records_sql($query);
			if(!empty($groups)){
				foreach($groups as $group){
					$mappingData = new stdClass;
					$mappingData->is_active = 0;
					$mappingData->remarks = $remarksText;
					$mappingData->id = $group->id;
					$last_id = $DB->update_record('program_group',$mappingData);
					add_to_log(1, 'Unenrol group from program', 'remove', 'program/assignusers.php?program='.$programId.'&groupid='.$group->id, "Unenrol group from program", 0, $USER->id);
				}
			}
		}
	}
	/**
	 * This function deactivate's a user from team
	 * @global object $USER,$CFG,$DB
	 * @param int $programId program Id
	 * @param int $userId User Id array
	 * @param Text $remarksText decline remarks
	 * @return nothing
	 */
	function deactivateProgramUsers($programId = 0, $userId = 0,$remarksText = ""){
		global $USER,$CFG,$DB;
		if($programId != 0){
			$where = '';
			if($userId != 0){
				$where = ' AND typeid = '.$userId;
			}
			$query = "SELECT id,typeid FROM mdl_program_user_mapping WHERE type = 'user' AND program_id = ".$programId.$where;
			$users = $DB->get_records_sql($query);
			if(!empty($users)){
				foreach($users as $user){
					$mappingData = new stdClass;
					$mappingData->status = 0;
					$mappingData->id = $user->id;
					$mappingData->remarks = $remarksText;
					$last_id = $DB->update_record('program_user_mapping',$mappingData);
					add_to_log(1, 'Unenrol user from program', 'remove', 'program/assignusers.php?program='.$programId.'&userid='.$userId, "Unenrol user from program", 0, $USER->id);
				}
			}
		}
	}
	function assignProgramToDepartment($programId = 0, $department = 0){
		global $USER,$CFG,$DB;
		$time = time();
		$query = "SELECT pam.id,pam.is_active FROM mdl_program_department as pam WHERE pam.program_id = '".$programId."' AND pam.department_id = ".$department;
		$departmentExists = $DB->get_record_sql($query);
		if(empty($departmentExists)){
			$mappingData = new stdClass;
			$mappingData->program_id = $programId;
			$mappingData->department_id = $department;
			$mappingData->is_active = 1;
			$programMapId = $DB->insert_record('program_department', $mappingData);
		}else{
			if($departmentExists->is_active == 0){
				$mappingData = new stdClass;
				$mappingData->is_active = 1;
				$mappingData->id = $departmentExists->id;
				$lastId = $DB->update_record('program_department',$mappingData);
			}
			$programMapId = $departmentExists->id;
		}
		add_to_log(1, 'Enrol department in program', 'add', '/program/assignusers.php?program='.$programId.'&departmentid='.$department, "Enrol department in program", 0, $USER->id);
	}

	function assignProgramToGroup($programId = 0, $group = 0,$endDate = ""){
		global $USER,$CFG,$DB;
		$time = time();

		$query = "SELECT pam.id,pam.is_active FROM mdl_program_group as pam WHERE pam.program_id = '".$programId."' AND pam.group_id = ".$group;
		$groupExists = $DB->get_record_sql($query);
		if(empty($groupExists)){
			$mappingData->program_id = $programId;
			$mappingData->group_id = $group;
			$mappingData->is_active = 1;
			$mappingData->end_date = $endDate;
			$mappingData->createdby = $USER->id;
			$mappingData->updatedby = $USER->id;
			$programMapId = $DB->insert_record('program_group', $mappingData);
		}else{
			$mappingData = new stdClass;
			$mappingData->is_active = 1;
			$mappingData->id = $groupExists->id;
			$mappingData->updatedby = $USER->id;
			$mappingData->end_date = $endDate;
			$lastId = $DB->update_record('program_group',$mappingData);

			$programMapId = $groupExists->id;
		}
		add_to_log(1, 'Enrol group in program', 'add', '/program/assignusers.php?program='.$programId.'&groupid='.$group, "Enrol group in program", 0, $USER->id);
	}
	function assignProgramToUser($programId = 0, $user = 0,$endDate){
		global $USER,$CFG,$DB;
		$time = time();
		$query = "SELECT pam.id,pam.status FROM mdl_program_user_mapping as pam WHERE pam.type = 'user' AND pam.program_id = '".$programId."' AND pam.typeid = ".$user;
		$userExists = $DB->get_record_sql($query);
		if(empty($userExists)){
			$mappingData = new stdClass;
			$mappingData->program_id = $programId;
			$mappingData->typeid = $user;
			$mappingData->user_id = $user;
			$mappingData->type = 'user';
			$mappingData->status = 1;
			$mappingData->end_date = $endDate;
			$mappingData->createdby = $USER->id;
			$mappingData->updatedby = $USER->id;
			$mappingData->created_on = time();
			$mappingData->updated_on = time();
			$programMapId = $DB->insert_record('program_user_mapping', $mappingData);
		}else{
			$mappingData = new stdClass;
			$mappingData->status = 1;
			$mappingData->id = $userExists->id;
			$mappingData->end_date = $endDate;
			$mappingData->updatedby = $USER->id;
			$mappingData->updated_on = time();
			$lastId = $DB->update_record('program_user_mapping',$mappingData);
			$programMapId = $userExists->id;
		}
		add_to_log(1, 'Enrol user in program', 'add', '/program/assignusers.php?program='.$programId.'&userid='.$user, "Enrol user in program", 0, $USER->id);
		$programCoursesSql = 'SELECT p.courseid as id FROM mdl_program_course AS p WHERE p.programid = '.$programId;
		$programCourses = $DB->get_records_sql($programCoursesSql);

		if(!empty($programCourses)){
			foreach($programCourses as $course){
				$enrolId = CheckCourseEnrolment($course->id);
				$userEnrolId = enrolUserToCourse($enrolId,$user,$course->id);
			}
		}
	}
	
	function setProgramActiveFlag($pid, $flagCheck) {
		global $DB;
		$data = new stdClass();
		$data->status = $flagCheck;
		$data->id = $pid;
		$nid = $DB->update_record('programs', $data);
		return true;
	}
	/*
	 * This function set program course order and resets all selection elements.
	 * @param int $programId program Id
	 * @param $CourseId course id
	 * @param $order new course order in program
	 * return boolean true/false on success/failure  
	 * */
	function setProgramCourseOrder($programId,$CourseId,$order){
		GLOBAL $CFG,$DB,$USER;
		$programCurrentOrder = $DB->get_record_sql("SELECT pc.id,pc.course_order FROM mdl_program_course as pc WHERE pc.is_active = 1 AND pc.programid = ".$programId."  AND pc.courseid =".$CourseId);	
		if(!empty($programCurrentOrder)){
			$DB->execute("UPDATE mdl_program_course SET course_order = ".$order." WHERE id = ".$programCurrentOrder->id);
			/*if($programCurrentOrder->course_order > $order){
				$DB->execute("UPDATE mdl_program_course SET course_order = course_order + 1 WHERE programid = ".$programId." AND course_order >=".$order);
			}else{
				$DB->execute("UPDATE mdl_program_course SET course_order = course_order - 1 WHERE programid = ".$programId." AND course_order <=".$order);
			}
			resetProgramCoursePref($programId);*/
			return true;
		}else{
			return false;
		}
	}
	/*
	 * This function reset all the course preferences for a program.
	 * @param int $programId program Id
	 * return boolean true/false on success/failure  
	 * */
	function resetProgramCoursePref($programId){
		GLOBAL $CFG,$DB,$USER;
		$searchArray = array('programid'=>$programId);
		$programMapData = $DB->get_records('program_course',$searchArray);
		if(!empty($programMapData)){
			$deleteSql = "DELETE FROM mdl_program_course_criteria WHERE program_map_id IN (SELECT pc.id FROM mdl_program_course pc WHERE pc.programid = ".$programId.")";
			$DB->execute($deleteSql);
			return true;
		}
		return true;
	}
	/*
	 * This function insert program course creteria in DB
	 * @param int $programId program id
	 * @param int $courseId course id for which criteria is being set
	 * @param array $onCompletionCourse course id on completion
	 * return boolean true/false 
	 * */
	function savecourseCriteria($programId,$courseId,$onCompletionCourse = array()){
		GLOBAL $CFG,$USER,$DB;
		$searchArray = array('programid'=>$programId,'courseid'=>$courseId,'is_active'=>1);
		$programMapData = $DB->get_records('program_course',$searchArray);
		if(!empty($programMapData)){
			$deleteSql = "DELETE FROM mdl_program_course_criteria WHERE program_map_id IN (SELECT pc.id FROM mdl_program_course pc WHERE pc.programid = ".$programId.")";
			$DB->execute($deleteSql);
			if(!empty($onCompletionCourse)){
				$data = new stdClass();
				foreach($onCompletionCourse as $completionCourse){
					$data->program_map_id = $programMapData->id;
					$data->course_id = $completionCourse;
					$nid = $DB->insert_record('program_course_criteria', $data);
				}
			}
			return true;
		}
		return false;
	}
	/*
	* This function get program course creteria
	* @param int $programId program id
	* @param int $courseId course id
	* return array $programCourseCriteria
	* */
	function getProgramCourseCriteria($programId,$courseId=0){
		GLOBAL $CFG,$USER,$DB;
		$programCourseCriteria = array();
		$where = '';
		if($courseId != 0){
			$where .= " AND pc.courseid = ".$courseId;
		}
		$sql = "SELECT pc.id, GROUP_CONCAT(pcc.course_id SEPARATOR ',') AS criteria_id,pc.programid,pc.courseid,pc.course_order,c.fullname FROM mdl_program_course AS pc LEFT JOIN mdl_program_course_criteria as pcc ON pc.id = pcc.program_map_id LEFT JOIN mdl_course as c ON pc.courseid = c.id WHERE c.is_active = 1 AND pc.programid = $programId AND pc.is_active = 1".$where." GROUP BY pc.courseid ORDER BY pc.course_order";
		$programMapData = $DB->get_records_sql($sql);
		if(!empty($programMapData)){
			return $programMapData;
		}
		return $programCourseCriteria;
	}
########################################### End Program Functions ##########################
