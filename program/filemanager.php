<?php

	require_once('../config.php');
	require_once('../local/program/program_form1.php');

	require_login();
	$PAGE->set_url('/program/addprogram.php');
	$strprograms = get_string('addnewprogram','program');

	
	$PAGE->set_title($SITE->fullname.": ".$strprograms);
	$PAGE->set_heading($SITE->fullname . ': '.$strprograms);
	$PAGE->set_pagelayout('globaladmin');
	navigation_node::override_active_url(new moodle_url('/program/index.php'));
	$returnUrl = $CFG->wwwroot.'/program/index.php';
	$context = context_system::instance();
	$editoroptions = array('maxfiles'=>EDITOR_UNLIMITED_FILES, 'subdirs' => false, 'trust'=>false, 'context'=>$context, 'noclean'=>true);
	$editform = new program_form(null, array('editoroptions'=>$editoroptions));

	if ($editform->is_cancelled()) {
		redirect($returnUrl);

	} elseif ($data = $editform->get_data()) {

		$programId = 673;
		$contextLevel = 150; 
	    $query = "SELECT * FROM {$CFG->prefix}context WHERE contextlevel = '".$contextLevel."' AND instanceid = '".$programId."' ";
	    $contextProgram = $DB->get_record_sql($query);
		$component = 'test';
		if ($filesOptions = programOverviewFilesOptions($programId)) {
		     $data = file_postupdate_standard_filemanager($data, 'testimage', $filesOptions, $contextProgram, $component, 'testimage', 0);
		}
/*
		$maxContextId = $DB->get_field_sql("SELECT max(id) FROM {$CFG->prefix}context");
		$contextLevel = 150;
		$depth = 3;
		$maxContextId = $maxContextId + 1;
		$contextPath = "/1/".$depth."/".$maxContextId;
		$component = 'test';
		$data->id = $maxContextId;
		$query = "INSERT INTO {$CFG->prefix}context set contextlevel = '".$contextLevel."' , instanceid = '".$data->id."', path = '".$contextPath."', depth = '".$depth."'";
		executeSql($query);
			
		$programId = $data->id;
		$contextLevel = 150; 
	    $query = "SELECT * FROM {$CFG->prefix}context WHERE contextlevel = '".$contextLevel."' AND instanceid = '".$programId."' ";
	    $contextProgram = $DB->get_record_sql($query);
		  
		if ($filesOptions = programOverviewFilesOptions($programId)) {
		  $data = file_postupdate_standard_filemanager($data, 'testimage', $filesOptions, $contextProgram, $component, 'testimage', 0);
		}*/
	}else{
		$contextLevel = 150; 
		$programId = 673;
		$query = "SELECT * FROM {$CFG->prefix}context WHERE contextlevel = '".$contextLevel."' AND instanceid = '".$programId."' ";
		$programContext = $DB->get_record_sql($query);
		$program->id = $programContext->id;
		if ($filesOptions = programOverviewFilesOptions($program)) {
		   file_prepare_standard_filemanager($program, 'testimage', $filesOptions, $programContext, 'test', 'testimage', 0);
		}
		$editform->set_data($program);
	}

	$manageprograms = get_string('manageprograms','program');
	$PAGE->navbar->add($manageprograms, new moodle_url('/program/index.php'));
	$PAGE->navbar->add($strprograms);
	
	//$allocationType  = getProgramCourseAllocationType($id, $CFG->programType );


	/// Print header
	echo $OUTPUT->header();
	$editform->display();

	function programOverviewFilesOptions($program){
		global $CFG, $DB;
		if (empty($CFG->programOverviewFilesLimit)) {
			return null;
		}

		$accepted_types = preg_split('/\s*,\s*/', trim($CFG->programOverviewFilesExt), -1, PREG_SPLIT_NO_EMPTY);
		if (in_array('*', $accepted_types) || empty($accepted_types)) {
			$accepted_types = '*';
		} else {
			// Since config for $CFG->programOverviewFilesExt is a text box, human factor must be considered.
			// Make sure extensions are prefixed with dot unless they are valid typegroups
			foreach ($accepted_types as $i => $type) {
				if (substr($type, 0, 1) !== '.') {
					require_once($CFG->libdir. '/filelib.php');
					if (!count(file_get_typegroup('extension', $type))) {
						// It does not start with dot and is not a valid typegroup, this is most likely extension.
						$accepted_types[$i] = '.'. $type;
						$corrected = true;
					}
				}
			}
			if (!empty($corrected)) {
				set_config('programOverviewFilesExt', join(',', $accepted_types));
			}
		}
		$options = array(
			'maxfiles' => $CFG->programOverviewFilesLimit,
			'maxbytes' => $CFG->maxbytes,
			'subdirs' => 0,
			'accepted_types' => $accepted_types
		);
		if (!empty($program->id)) {
		
		  $contextLevel = 150; 
		  $query = "SELECT * FROM {$CFG->prefix}context WHERE contextlevel = '".$contextLevel."' AND instanceid = '".$program->id."' ";
		  $context = $DB->get_record_sql($query);
		  $options['context'] = $context;
		  
		  
		} else if (is_int($program) && $program > 0) {
		  
		  $contextLevel = 150; 
		  $query = "SELECT * FROM {$CFG->prefix}context WHERE contextlevel = '".$contextLevel."' AND instanceid = '".$program."' ";
		  $context = $DB->get_record_sql($query);
		  $options['context'] = $context;
		}
		
		
		 return $options;
	}
?>


<!--  for program setting  -->


<script>
$(document).ready(function(){


	var cnt = 0;
	$("#mform1").find('.collapsible').each(function(){
		if(cnt != 0){
			$(this).addClass('collapsed');
		}
		cnt++;
	});
	var userRole = "<?php echo $USER->archetype; ?>";
	var userManager = "<?php echo $CFG->userTypeManager; ?>";
	var allocationType = "<?php echo $allocationType; ?>";
	
	
	if(allocationType == 1){
	  $('.self-allocation-box').addClass('hide');
	}else if(allocationType == 2){
	  $('.self-allocation-box').removeClass('hide');
	} 
	
	$(document).on('click','[name = "allocation_type"]', function(){
	    var allocationType = $(this).val();
		if(allocationType == 1){
		  $('.self-allocation-box').addClass('hide');
		}else if(allocationType == 2){
		  $('.self-allocation-box').removeClass('hide');
		} 
	});
	
	$(".tabLink").each(function(){
      $(this).click(function(){
        tabeId = $(this).attr("id");
        $(".tabLink").removeClass("activeLink");
        $(this).addClass("activeLink");
        $(".tabcontent").addClass("hide");
        $("#"+tabeId+"-1").removeClass("hide");					
        return false;	  
      });
    }); 
	
	
	
	$("#addselect_searchtext").keyup(function(){
		var searchText = $(this).val();
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/user/searchcourses.php',
				type:'POST',
				data:'action=searchNonCourse&courseid=<?php echo $courseid; ?>&search_text='+searchText,
				success:function(data){
					$('#addselect_wrapper').find('select').html(data);
				}
		});
	});
	$("#addselect_clearbutton").click(function(){
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/user/searchcourses.php',
				type:'POST',
				data:'action=searchNonCourse&courseid=<?php echo $courseid; ?>',
				success:function(data){
					$('#addselect_wrapper').find('select').html(data);
					$("#addselect_searchtext").val('');
				}
		});
	});
	$("#removeselect_clearbutton").click(function(){
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/user/searchcourses.php',
				type:'POST',
				data:'action=searchUserCourse&courseid=<?php echo $courseid; ?>',
				success:function(data){
					$('#removeselect_wrapper').find('select').html(data);
					$("#removeselect_searchtext").val('');
				}
		});
	});
	$("#removeselect_searchtext").keyup(function(){
		var searchText = $(this).val();
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/user/searchcourses.php',
				type:'POST',
				data:'action=searchUserCourse&courseid=<?php echo $courseid; ?>&search_text='+searchText,
				success:function(data){
					$('#removeselect_wrapper').find('select').html(data);
				}
		});
	});
	if(userRole != userManager){
		var fromDepartment = $('#department_select_wrapper select');
		var toDepartment = $('#department_remove_wrapper select');
	}

	var fromTeam = $('#team_addselect_wrapper select');
	var toTeam = $('#team_removeselect_wrapper select');

	var fromUser = $('#user_addselect_wrapper select');
	var toUser = $('#user_removeselect_wrapper select');

	$(document).on("click", "#department_select_wrapper select option",function(){
		$("#department_add").removeAttr("disabled");
	});
	$(document).on("click", "#department_remove_wrapper select option",function(){
		$("#department_remove").removeAttr("disabled");
	});

	$(document).on("click", "#team_addselect_wrapper select option",function(){
		$("#team_add").removeAttr("disabled");
	});
	$(document).on("click", "#team_removeselect_wrapper select option",function(){
		$("#team_remove").removeAttr("disabled");
	});
	$(document).on("click", "#user_addselect_wrapper select option",function(){
		$("#user_add").removeAttr("disabled");
	});
	$(document).on("click", "#user_removeselect_wrapper select option",function(){
		if($(this).attr('class') == 'option-disable'){
			$("#user_remove").addAttr("disabled");
		}else{
			$("#user_remove").removeAttr("disabled");
		}
	});

	$(document).on("click","#department_add",function(){
			fromDepartment.find('option:selected').appendTo(toDepartment.find('optgroup'));
	});
	$(document).on("click","#team_add",function(){
			fromTeam.find('option:selected').appendTo(toTeam.find('optgroup'));
	});
	$(document).on("click","#user_add",function(){
			fromUser.find('option:selected').appendTo(toUser.find('optgroup'));
	});

	$(document).on("click","#department_remove",function(){
			toDepartment.find('option:selected').appendTo(fromDepartment.find('optgroup'));
	});
	$(document).on("click","#team_remove",function(){
			toTeam.find('option:selected').appendTo(fromTeam.find('optgroup'));
	});
	$(document).on("click","#user_remove",function(){
		var err = 0;
		toUser.find( "option:selected" ).each(function() {
			if($(this).attr('class') == 'option-disable'){
				err = 1;
			}else{
				$(this).appendTo(fromUser.find('optgroup'));
			}
		});
		if(err == 1){
			alert("<?php echo get_string('assignment_error');?>");
			toUser.find( "option" ).each(function() {
				//$(this).removeAttr('selected');
			});
		}
	});
	$("#id_submitbutton").click(function(){
		if(userRole != userManager){
			fromDepartment.find('optgroup').find("option").each(function(){
				$(this).removeAttr("selected");
			});
		}
		fromTeam.find('optgroup').find("option").each(function(){
			$(this).removeAttr("selected");
		});
		fromUser.find('optgroup').find("option").each(function(){
			$(this).removeAttr("selected");
		});
		if($("#department_all").is(':checked')){
			if(userRole != userManager){
				fromDepartment.find('optgroup').find("option").each(function(){
					$(this).appendTo(toDepartment.find('optgroup'));
				});
			}
			fromTeam.find('optgroup').find("option").each(function(){
				$(this).appendTo(toTeam.find('optgroup'));
			});
			fromUser.find('optgroup').find("option").each(function(){
				$(this).appendTo(toUser.find('optgroup'));
			});
		}
		if(userRole != userManager){
			toDepartment.find('option').prop('selected', true);
		}
		toTeam.find('option').prop('selected', true);
		toUser.find('option').prop('selected', true);
		$("#assignform").submit();
	});
	$("#department_all").click(function(){
		if($("#department_all").is(':checked')){
			$(".tab-box").hide();
			$(".tabcontent").hide();
		}else{
			$(".tab-box").show();
			$(".tabcontent").show();
		}
	});
	$('#id_cancel').click(function(){
		window.location.href = '<?php echo $CFG->wwwroot; ?>/program/index.php';
	});
	
		
	/*$(document).on('click', '.fp-upload-btn', function(){
  
		  if($("input:file").length == 0 ){
		  
			  $('.moodle-dialogue-base:eq(3)').hide();
		  }
		
	
	});
	

	$(document).on('click', '.fp-btn-add a', function(){

	  $('.moodle-dialogue-base:eq(3)').show();
	  $('.moodle-dialogue-base:eq(3) .moodle-dialogue').css({'left':'470px'});

	});*/
});
</script>

<!--  end program setting  -->
<?php
	echo $OUTPUT->footer();
