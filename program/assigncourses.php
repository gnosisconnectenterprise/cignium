<?php
	require_once(dirname(__FILE__) . '/../config.php');
	require_once($CFG->dirroot . '/local/user/selector/lib.php');
	require_once($CFG->dirroot . '/course/lib.php');
	require_once($CFG->libdir . '/filelib.php');
    require_once('programlib.php');
	
	global $USER;
	require_login();

	if($USER->archetype == $CFG->userTypeStudent){ // if user is not manager redirect to home page.
		redirect($CFG->wwwroot."/");
	}

	$programId = required_param('program', PARAM_INT);
	$cancel  = optional_param('cancel', false, PARAM_BOOL);

	$program = $DB->get_record('programs', array('id'=>$programId), '*', MUST_EXIST);

	//Start Assignment Post Data
	if (optional_param('add', false, PARAM_BOOL) && confirm_sesskey()) {
		assignCourseToProgram($programId,$_REQUEST['addcourse']);
		redirect($CFG->wwwroot."/program/".$CFG->pageProgramAssignCourses."?program=".$programId);
	}
	if (optional_param('remove', false, PARAM_BOOL) && confirm_sesskey()) {
		removeCourseFromProgram($programId,$_REQUEST['removecourse']);
		redirect($CFG->wwwroot."/program/".$CFG->pageProgramAssignCourses."?program=".$programId);
	}
	//End Assignment Post Data

	$context = context_system::instance();
	$returnUrl = $CFG->wwwroot.'/program/index.php';

	if ($cancel) {
		redirect($returnUrl);
	}
	//Start Setting Page data
	$PAGE->set_url('/program/'.$CFG->pageProgramAssignCourses, array('program'=>$programId));
	$PAGE->set_pagelayout('globaladmin');
	$PAGE->navbar->add(get_string('manageprograms',"program"), new moodle_url('/program/index.php'));
	$PAGE->navbar->add(get_string('assigncourses', 'program'));
	$PAGE->set_heading($SITE->fullname);
	
	//Start Set Array of assigned and unassigned data
	$courses = new program_courses_selector('', array('programId' => $programId));
	$courses->get_non_program_courses();
	$courses->get_program_courses();
	$programCourses = getProgramCourseCriteria($programId);
	$orderedCourses = array();
	foreach($programCourses as $pCourse){
		$orderedCourses[$pCourse->course_order] = $pCourse;
	}
	ksort($orderedCourses);
	//End Set Array of assigned and unassigned data

	$groupinforow = array();

	// Check if there is a picture to display.
	if($program->picture != ''){
		$view =  $CFG->dirroot.'/theme/gourmet/pix/program/'.$program->picture;
		$view1 =  $CFG->wwwroot.'/theme/gourmet/pix/program/'.$program->picture;
	}else{
		$view =  $CFG->dirroot.'/theme/gourmet/pix/program-img.jpg';
		$view1 =  $CFG->wwwroot.'/theme/gourmet/pix/program-img.jpg';
	}
	$data = new stdClass();
	$data->id=$program->id;
	$data->name = $program->name;
	$imagePath = getProgramImage($data);
	$PAGE->set_title("$SITE->fullname: ".get_string('assigncoursesfor', 'program'). " ".$data->name);
	
	echo $OUTPUT->header();
	//End Setting Page data

	
	//Start Display picture
	$picturecell = new html_table_cell();
	$picturecell->attributes['class'] = 'left side picture';
	$picturecell->text ='<div class="felement fstatic program-image">'.$imagePath.'</div></div>';
	$groupinforow[] = $picturecell;
	//End Display picture

	// Check if there is a description to display.
	$program->description = file_rewrite_pluginfile_urls($program->description, 'pluginfile.php', $context->id, 'program', 'description', $program->id);
	if (!isset($program->descriptionformat)) {
		$program->descriptionformat = FORMAT_MOODLE;
	}

	$options = new stdClass;
	$options->overflowdiv = true;

	$contentcell = new html_table_cell();
	$contentcell->attributes['class'] = 'content';
	$contentcell->text = '<b>'.$program->name.'</b></br>';
	if($program->description != ''){
		$contentcell->text .= format_text($program->description, $program->descriptionformat, $options);
	}
	$groupinforow[] = $contentcell;

	// Check if we have something to show.
	if (!empty($groupinforow)) {
		$groupinfotable = new html_table();
		$groupinfotable->attributes['class'] = 'groupinfobox';
		$groupinfotable->data[] = new html_table_row($groupinforow);
		//echo html_writer::table($groupinfotable);
	}

	$headerHtml = getModuleHeaderHtml($program, $CFG->programModule);
	echo $headerHtml;
	if($_SESSION['update_msg']!=''){
		echo '<div class="clear"></div>';
		echo '<div class="bg-success text-success text-center" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
		echo '<div class="clear"></div>';
		$_SESSION['update_msg']='';
	}
	/// Print the editing form
	?>

	<div id="addmembersform" class="borderBlock borderBlockSpace">
		<form id="assignform" method="post" action="<?php echo $CFG->wwwroot; ?>/program/<?php echo $CFG->pageProgramAssignCourses; ?>?program=<?php echo $programId; ?>">
			<div>
				<input type="hidden" name="sesskey" value="<?php p(sesskey()); ?>" />
				<table class="assignTable">
        <tr>
          <td id='potentialcell' class="potentialcell">
						  <p><label for="addselect"><?php echo get_string('nonteamcourses','group');?></label></p>
						  <div id="addselect_wrapper" class="userselector">
							<select multiple="multiple" name = "addcourse[]" size = '20'>
								<?php 
									$courses->course_option_list(1);
									// This function will print courses options
								?>
							</select>
                             <div class="search-noncourse-box searchBoxDiv" >
                                <div class="search-input" ><input type="text" class="searchBoxInput" value="" placeholder="Search" id="addselect_searchtext" name="addselect_searchtext" ></div>
                                <div class="search_clear_button"><input type="button" title="Search" id="addselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="addselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
                      
                            </div>
                            
						  </div>
						</td>
						<td id='buttonscell'>
							<div class="arrow_button">
								<input class="moveLeftButton" name="remove" id="remove" type="submit" value="<?php echo get_string('remove').'&nbsp;'.$OUTPUT->rarrow(); ?>" title="<?php print_string('remove'); ?>" disabled = ""/><input class="moveRightButton" name="add" id="add" type="submit" value="<?php echo $OUTPUT->larrow().'&nbsp;'.get_string('add'); ?>" title="<?php print_string('add'); ?>" disabled = ""/>
							</div>
						</td>
						<td id='existingcell'  class="potentialcell">
							  <p><label for="removeselect"><?php echo get_string('teamcourses','group');?></label></p>
							  <div id="removeselect_wrapper" class="userselector">
								<select multiple="multiple" name = "removecourse[]" size = '20'>
									<?php $courses->course_option_list(2); ?>
								</select>
                                  <div  class="search-course-box searchBoxDiv" >
                                    <div class="search-input" ><input type="text" class="searchBoxInput" value="" placeholder="Search" id="removeselect_searchtext" name="removeselect_searchtext" ></div>
                                    <div class="search_clear_button"><input type="button" title="Search" id="removeselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="removeselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
                          
                                  </div>
                
							 </div>
						</td>
					</tr>
                  </table>
                  
                  <fieldset class="borderBlock margin_top nav_sequence">
                  	  <legend><?php echo get_string('pre_requisite_heading','program');?></legend>
                      <div class="borderBlockSpace">
                      	<div class="row disableDrag">
                            <div class="col"><?php echo get_string('pre_requisite_heading_courses','program');?></div>
                            <div class="col"><?php echo get_string('pre_requisite_heading_order','program');?></div>
                            <div class="col"><?php echo get_string('pre_requisite_heading_pased','program');?></div>
                        </div>
                        <?php
                        if(empty($orderedCourses)){                        	
                        }else{
							$rowOrder = 1;
							foreach($orderedCourses as $course){
								?>
		                        <div class="row dragRow" id = "<?php echo $course->courseid; ?>" pid = "<?php echo $programId; ?>" order="<?php echo $rowOrder; ?>">
		                            <div class="col"><?php echo $course->fullname; ?></div>
		                            <div class="col"><a title="Drag" class="dragdropIcon" href="#">Drag Drop</a></div>
		                            <div class="col">
		                            <?php
		                             	$displayRow = '';
			                            if($rowOrder === 1){
			                            	$displayRow = "style='display:none;'";
			                            }
			                        ?>
		                            	<div class="customListBox" <?php echo $displayRow; ?>>
		                                	<ul>
		                                	<?php 
			                                	$courseSelected = array();
			                                	if(!empty($course->criteria_id)){
													$courseSelected = explode(',',$course->criteria_id);
			                               		}
		                                		$courseList = array_slice($orderedCourses, 0, $rowOrder-1);
			                                	foreach($courseList as $courseLess){
													$checked = '';
													if(!empty($courseSelected) && in_array($courseLess->courseid,$courseSelected)){
														$checked = ' checked="checked"';
													}
												?>
												<li class="listitem"><label><input class = "coursesCre" type="checkbox" name="coursesCre[]" value="<?php echo $courseLess->courseid;?>" <?php echo $checked;?> /><span><?php echo $courseLess->fullname;?></span></label></li>
												<?php 
												}
		                                	?>
		                                    </ul>
		                                </div>
		                            </div>
		                        </div>
		                        <?php 
		                        $rowOrder++;
							}
						} ?>
                      </div>
                      <div class="borderBlockSpace text-center padding_none paddBottom">
                      	<input type="reset" value="Reset" name="reset" id="resetButton">
                      	<input type="button" value="Save" name="cancel" id="saveButton">
                      </div>
                  </fieldset>
                  
                  
					<div id='backcell'><input type="submit" name="cancel" style="margin-top:10px;margin-bottom:0;"  value="<?php print_string('back'); ?>" /></div>
				
			</div>
		</form>
	</div>
	<script>
	$(document).ready(function(){
		//Unassigned course search
		$("#addselect_searchtext_btn").click(function(){
			var searchText = $('#addselect_searchtext').val();
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/group/searchcourses.php',
					type:'POST',
					data:'action=searchNonProgramCourse&programId=<?php echo $programId; ?>&search_text='+searchText,
					success:function(data){
						$('#addselect_wrapper').find('select').html(data);
					}
			});
		});

		//Assigned course search
		$("#addselect_clearbutton").click(function(){
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/group/searchcourses.php',
					type:'POST',
					data:'action=searchNonProgramCourse&programId=<?php echo $programId; ?>',
					success:function(data){
						$('#addselect_wrapper').find('select').html(data);
						$("#addselect_searchtext").val('');
					}
			});
		});

		//unassigned course clear search
		$("#removeselect_clearbutton").click(function(){
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/group/searchcourses.php',
					type:'POST',
					data:'action=searchProgramCourse&programId=<?php echo $programId; ?>',
					success:function(data){
						$('#removeselect_wrapper').find('select').html(data);
						$("#removeselect_searchtext").val('');
					}
			});
		});
		//assigned course clear search
		$("#removeselect_searchtext_btn").click(function(){
			var searchText = $('#removeselect_searchtext').val();
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/group/searchcourses.php',
					type:'POST',
					data:'action=searchProgramCourse&programId=<?php echo $programId; ?>&search_text='+searchText,
					success:function(data){
						$('#removeselect_wrapper').find('select').html(data);
					}
			});
		});
		//enable add icon
		$(document).on("change", "#addselect_wrapper select",function(){
			$("#add").removeAttr("disabled");
		});
		//enable remove icon
		$(document).on("change", "#removeselect_wrapper select",function(){
			$("#remove").removeAttr("disabled");
		});

		var DivStart = "<div class = 'customListBox'><ul>";
		var DivEnd = "</ul></div>";
		var ulLi = '<li class="listitem"><label><input class = "coursesCre" type="checkbox" name="coursesCre[]" value="##COURSEID##" /><span>##COURSNAME##</span></label></li>';
		$( ".nav_sequence .borderBlockSpace" ).sortable({
            items: "> div:not(.disableDrag)",
			handle: ".dragdropIcon",
			placeholder: "sortable-placeholder",
			containment: ".nav_sequence .borderBlockSpace",
			axis: "y",
			stop: function( event, ui ) {
				var courseId = ui.item.attr('id');
				var programId = ui.item.attr('pid');
				var order = ui.item.attr('order');
				$('.customListBox').show();
				var elementHtml = '';
				$( ".dragRow" ).each(function( i ) {
					var completeEle = '';
					if(i == 0){
						$(this).find('.customListBox').remove();
					}else{
						var ele = ulLi;
						ele = ele.replace('##COURSEID##',$(this).prev().attr('id'));
						ele = ele.replace('##COURSNAME##',$(this).prev().find(".col:first-child").html());
						elementHtml += ele;
						completeEle += DivStart+elementHtml+DivEnd;
						$(this).find(".col:last-child").html(completeEle);
					}
				});
				$(".coursesCre").removeAttr('checked');
			}
          });
        $("#resetButton").click(function(){
        	$.ajax({
			    url: '<?php echo $CFG->wwwroot;?>/local/ajax.php?action=resetProgramCriteria&programId='+programId,
			    type: 'POST',
			    success: function(data){
						window.location.reload();
				}
			});
        });
        $("#saveButton").click(function(){
			var elementArray = {};
			var programId = <?php echo $programId?>;
			$( ".dragRow" ).each(function( i ) {
				var arrayIndex = $(this).attr('id');
				elementArray[i] = {};
				elementArray[i]['order'] = i + 1;
				elementArray[i]['courseId'] = arrayIndex;
				elementArray[i]['courses'] = {};
				if(i != 0){
					var eleCount = 0;
					$(this).find(".col:last-child").find('.listitem').find('.coursesCre').each(function(j){
						if($(this).is(":checked")){
							elementArray[i]['courses'][j] = $(this).val();
							eleCount++;
						}
					});
				}
			});
			$.ajax({
			    url: '<?php echo $CFG->wwwroot;?>/local/ajax.php?action=saveProgramCriteria&programId='+programId,
			    type: 'POST',
			    data: {elem: JSON.stringify(elementArray)},
			    success: function(data){
						window.location.reload();
				}
			});
        });
	});
	
	
	
	
	</script>
	<?php
	echo $OUTPUT->footer();