<?php

    require_once('../config.php');
	GLOBAL $CFG,$USER,$DB;
	require_once($CFG->dirroot . '/local/user/selector/lib.php');
    require_once($CFG->dirroot . '/program/programlib.php');

	require_login();

    if($USER->archetype == $CFG->userTypeStudent){ // if user is not manager redirect to home page.
		redirect($CFG->wwwroot."/");
	}

    $delete       = optional_param('delete', 0, PARAM_INT);
    $confirm      = optional_param('confirm', '', PARAM_ALPHANUM);   //md5 confirmation hash
    $sort         = optional_param('sort', 'mp.id', PARAM_ALPHANUM);
    $dir          = optional_param('dir', 'DESC', PARAM_ALPHA);
    $page         = optional_param('page', 1, PARAM_INT);
    $perpage      = optional_param('perpage', $CFG->perpage, PARAM_INT);        // how many per page
	$status       = optional_param('status', 0, PARAM_INT);

  
    $PAGE->set_title($SITE->fullname.": ".get_string('manageprograms', 'program'));
	$manageprograms = get_string('manageprograms','program');
	$PAGE->navbar->add($manageprograms);
	
    $stredit   = get_string('edit');
    $strdelete = get_string('delete');
    $strdeletecheck = get_string('deletecheck');
    $strassign   = get_string('assignusers', 'program');    	
    $strconfirm = get_string('confirm');
    $assigncourses = get_string('assigncourses', 'program');

    if (empty($CFG->loginhttps)) {
        $securewwwroot = $CFG->wwwroot;
    } else {
        $securewwwroot = str_replace('http:','https:',$CFG->wwwroot);
    }

    $returnurl = new moodle_url('/program/index.php', array('sort' => $sort, 'dir' => $dir));
    checkPageIsNumeric($returnurl,$_REQUEST['page']);
   
    $paramArray = array(
					'ch' => optional_param('ch', '', PARAM_ALPHA),
					'key' => optional_param('key', '', PARAM_RAW),
					'name' => optional_param('name', '', PARAM_RAW),
					'des' => optional_param('des', '', PARAM_RAW)
					);
	$filterArray = array(							
						'name'=>get_string('name'),
						'des'=>get_string('description')
				   );
				   
    if ($delete and confirm_sesskey()) {              // Delete a selected program, after confirmation
		$programid = $delete;
        redirect(new moodle_url('/local/program/delete.php', array_merge($paramArray, array('programs'=>$programid,'sesskey'=>sesskey(),'confirm'=>1))));
    } 
	
	
	
	
	if($status == 1){	
	    $flagCheck = optional_param('flag', '', PARAM_ALPHANUM);
		if( ($flagCheck == '0') || ($flagCheck == '1') ){
			$pid = optional_param('program', '', PARAM_INT);		
			setProgramActiveFlag($pid, (int)$flagCheck);
			$_SESSION['update_msg'] = get_string('record_updated');
			redirect($genURL);
		}
	}
		

    echo $OUTPUT->header();
	

	$pageURL = '/program/index.php';
	echo genCommonSearchForm($paramArray, $filterArray, $pageURL);
    $extracolumns = array();
    $columns = array_merge(array('name', 'description'),$extracolumns,
            array('timecreated'));

    foreach ($columns as $column) {
        $string[$column] = getProgramFieldName($column);
        if ($sort != $column) {
            $columnicon = "";
            if ($column == "timecreated") {
                $columndir = "DESC";
            } else {
                $columndir = "ASC";
            }
        } else {
            $columndir = $dir == "ASC" ? "DESC":"ASC";
            if ($column == "timecreated") {
                $columnicon = ($dir == "ASC") ? "sort_desc" : "sort_asc";
            } else {
                $columnicon = ($dir == "ASC") ? "sort_asc" : "sort_desc";
            }
            $columnicon = "<img class='iconsort' src=\"" . $OUTPUT->pix_url('t/' . $columnicon) . "\" alt=\"\" />";

        }
        //$$column = "<a href=\"managegroups.php?sort=$column&amp;dir=$columndir\">".$string[$column]."</a>$columnicon";
        $$column = $string[$column];
    }

	$extrasql = '';
	$params = array();
    $programs = getProgramsListing($sort, $dir, $page, $perpage, '', $extrasql, $paramArray);
    $programCount = getProgramsListing('programCount', $dir, $page, $perpage, '', $extrasql, $paramArray);

    $baseurl = new moodle_url('/program/index.php');
	$removeKeyArray = array('id','action','sta','sort');
	$pageURL = '/program/index.php';
		////// Getting common URL for the Search List //////
	 $genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
	
	
	// Start listing table
	$tableHtml = '<table class = "table1"><thead><tr>';
	$tableHtml .= "<td width = '25%' align='align_left' >$name</td>";		
	$tableHtml .= "<td width = '15%' align='align_left' >".get_string('programid','program')."</td>";	
	$tableHtml .= "<td width = '10%' align='align_left' >".get_string('noofusers')."</td>";
	$tableHtml .= "<td width = '10%' align='align_left' >".get_string('noofcourses')."</td>";
	//$tableHtml .= "<td width = '14%' align='align_left' >$timecreated</td>";	
	//$tableHtml .= "<td width = '15%' align='align_left' >$createdby</td>";	
	$tableHtml .= "<td width = '30%' align='align_left' >".get_string('manage')."</td>";	
	$tableHtml .= '</tr></thead>';
	
	   $extraCondForRole = " AND (r.id = 1 OR r.archetype = '".$CFG->userTypeAdmin."')"; 
	   $adminRoleUsers = getAllUserRoleDetails($extraCondForRole);
	   $adminRoleUserIds = 0;
	   $adminRoleUsersArr = array();
	   if(count($adminRoleUsers) > 0 ){
		  $adminRoleUsersArr = array_keys($adminRoleUsers);
		  $adminRoleUserIds = implode(",",$adminRoleUsersArr);
	   }
					
    if (!$programs) {
		$tableHtml .= "<tr><td colspan = '5'>".get_string('no_results')."</td></tr>";		
        $table = NULL;
    } else {
        foreach ($programs as $program) {
		
		    $cntTotalUsers = 0;
			$cntTotalCourses = 0;
		    $programs = new program_assignment_selector('', array('programid' => $program->id));
			$programUserList = $programs->getProgramUserList();
			$cntTotalUsers = count($programUserList);
			
			$pCourses = new program_courses_selector('', array('programId' => $program->id));
			$programCourses = $pCourses->getProgramCourses();
			$cntTotalCourses = count($programCourses);
	
            $buttons = array();
            $lastcolumn = '';
			$tableHtml .= "<tr>";
			
			$pLaunch = '<a href="'.$CFG->wwwroot.'/course/program_preview.php?pid='.$program->id.'" class="view" title="'.get_string('previewlink','course').'">'.get_string('previewlink','course').'</a>';	
			$buttons[] = $pLaunch;
			/*if($USER->archetype == $CFG->userTypeAdmin || $program->createdby == $USER->id){
			// edit button
				$buttons[] = html_writer::link(new moodle_url($securewwwroot.'/program/addprogram.php', array('id'=>$program->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>$stredit, 'class'=>'iconsmall')), array('title'=>$stredit, 'class'=>'edit'));
			}*/

			

			/*if($USER->archetype == $CFG->userTypeAdmin || $program->createdby == $USER->id){
				$buttons[] = html_writer::link(new moodle_url($securewwwroot.'/program/'.$CFG->pageTeamAssigncourses, array('program'=>$program->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/group'), 'alt'=>$assigncourses, 'class'=>'iconsmall')), array('title'=>$assigncourses, 'class'=>'assign-courses'));
				// delete button
				$buttons[] = html_writer::link(new moodle_url($returnurl, array_merge($paramArray,array('delete'=>$program->id, 'sesskey'=>sesskey()))), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/delete'), 'alt'=>$strdelete, 'class'=>'iconsmall')), array('title'=>$strdelete, 'class'=>'delete delete_program'));
			}*/
			
			
			 if($USER->archetype == $CFG->userTypeAdmin){

               
					if($program->publish == 1){
	                     
						 
						 if($program->status == 1) { 
						      $buttons[] = html_writer::link(new moodle_url($securewwwroot.'/program/addprogram.php', array('id'=>$program->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>$stredit, 'class'=>'iconsmall')), array('title'=>$stredit, 'class'=>'edit'));
							 $buttons[] = html_writer::link(new moodle_url($securewwwroot.'/program/assignusers.php', array('program'=>$program->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/group'), 'alt'=>$strassign, 'class'=>'iconsmall')), array('title'=>$strassign, 'class'=>'assign-group'));
							 $buttons[] = html_writer::link(new moodle_url($securewwwroot.'/program/'.$CFG->pageTeamAssigncourses, array('program'=>$program->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/group'), 'alt'=>$assigncourses, 'class'=>'iconsmall')), array('title'=>$assigncourses, 'class'=>'assign-courses'));
							 $buttons[] = html_writer::link(new moodle_url($securewwwroot.'/program/index.php?status=1&flag=0', array('program'=>$program->id)), get_string('deactivatelink','course'), array('title'=>get_string('deactivatelink','course'), 'class'=>'disable'));
						 }else{
						 
						     $buttons[] = html_writer::link('javascript:;', '', array('title'=>$stredit, 'class'=>'edit-disable'));
							 $buttons[] = html_writer::link('javascript:;', '', array('title'=>$strassign, 'class'=>'assign-group-disabled'));
							 $buttons[] = html_writer::link('javascript:;', '', array('title'=>$assigncourses, 'class'=>'assign-courses disable'));
							 $buttons[] = html_writer::link(new moodle_url($securewwwroot.'/program/index.php?status=1&flag=1', array('program'=>$program->id)), get_string('activatelink','program'), array('title'=>get_string('activatelink','program'), 'class'=>'enable'));
							 
						 }	
						
					}else{
					
					    $buttons[] = html_writer::link(new moodle_url($securewwwroot.'/program/addprogram.php', array('id'=>$program->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>$stredit, 'class'=>'iconsmall')), array('title'=>$stredit, 'class'=>'edit'));
						$buttons[] = html_writer::link(new moodle_url('javascript:;'), '', array('title'=>$strassign, 'class'=>'assign-group-disabled'));
						$buttons[] = html_writer::link(new moodle_url($securewwwroot.'/program/'.$CFG->pageTeamAssigncourses, array('program'=>$program->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/group'), 'alt'=>$assigncourses, 'class'=>'iconsmall')), array('title'=>$assigncourses, 'class'=>'assign-courses'));
						$buttons[] = '<a href="#" class="publish-program" rel = "'.$program->id.'" title="'.get_string('publish','course').'">'.get_string('publish','course').'</a>';
						$buttons[] = html_writer::link(new moodle_url($returnurl, array_merge($paramArray,array('delete'=>$program->id, 'sesskey'=>sesskey()))), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/delete'), 'alt'=>$strdelete, 'class'=>'iconsmall')), array('title'=>$strdelete, 'class'=>'delete delete_program'));
						
					}
				
			}else{
			
				
				if($program->publish == 1){

                   if(count($adminRoleUsersArr) > 0 && in_array($program->createdby, $adminRoleUsersArr)){
					  $buttons[] = html_writer::link(new moodle_url($securewwwroot.'/program/assignusers.php', array('program'=>$program->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/group'), 'alt'=>$strassign, 'class'=>'iconsmall')), array('title'=>$strassign, 'class'=>'assign-group'));
					  $buttons[] = html_writer::link(new moodle_url($securewwwroot.'/program/'.$CFG->pageTeamAssigncourses, array('program'=>$program->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/group'), 'alt'=>$assigncourses, 'class'=>'iconsmall')), array('title'=>$assigncourses, 'class'=>'assign-courses'));
				   }else{
				   
				        if($program->status == 1) { 
							 $buttons[] = html_writer::link(new moodle_url($securewwwroot.'/program/addprogram.php', array('id'=>$program->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>$stredit, 'class'=>'iconsmall')), array('title'=>$stredit, 'class'=>'edit'));
							 $buttons[] = html_writer::link(new moodle_url($securewwwroot.'/program/assignusers.php', array('program'=>$program->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/group'), 'alt'=>$strassign, 'class'=>'iconsmall')), array('title'=>$strassign, 'class'=>'assign-group'));
							 $buttons[] = html_writer::link(new moodle_url($securewwwroot.'/program/'.$CFG->pageTeamAssigncourses, array('program'=>$program->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/group'), 'alt'=>$assigncourses, 'class'=>'iconsmall')), array('title'=>$assigncourses, 'class'=>'assign-courses'));
							 $buttons[] = html_writer::link(new moodle_url($securewwwroot.'/program/index.php?status=1&flag=0', array('program'=>$program->id)), get_string('deactivatelink','course'), array('title'=>get_string('deactivatelink','course'), 'class'=>'disable'));	
						}else{
						     $buttons[] = html_writer::link('javascript:;', '', array('title'=>$stredit, 'class'=>'edit-disable'));
							 $buttons[] = html_writer::link('javascript:;', '', array('title'=>$strassign, 'class'=>'assign-group-disabled'));
							 $buttons[] = html_writer::link('javascript:;', '', array('title'=>$assigncourses, 'class'=>'assign-courses disable'));
							 $buttons[] = html_writer::link(new moodle_url($securewwwroot.'/program/index.php?status=1&flag=1', array('program'=>$program->id)), get_string('activatelink','program'), array('title'=>get_string('activatelink','program'), 'class'=>'enable'));
						}
						
				   }
				    
				}else{
				   if(count($adminRoleUsersArr) > 0 && in_array($program->createdby, $adminRoleUsersArr)){
				      $buttons[] = html_writer::link('javascript:;', '', array('title'=>$stredit, 'class'=>'edit-disable'));
					  $buttons[] = html_writer::link(new moodle_url('javascript:;'), '', array('title'=>$strassign, 'class'=>'assign-group-disabled'));
					  $buttons[] = html_writer::link(new moodle_url($securewwwroot.'/program/'.$CFG->pageTeamAssigncourses, array('program'=>$program->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/group'), 'alt'=>$assigncourses, 'class'=>'iconsmall')), array('title'=>$assigncourses, 'class'=>'assign-courses'));
					  $buttons[] = '<a href="#" class="publish-program-disabled" title="'.get_string('publish','course').'">'.get_string('publish','course').'</a>';
					  $buttons[] = html_writer::link('javascript:;','', array('title'=>$strdelete, 'class'=>'delete-disabled delete_program'));
					}else{
					    $buttons[] = html_writer::link(new moodle_url($securewwwroot.'/program/addprogram.php', array('id'=>$program->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>$stredit, 'class'=>'iconsmall')), array('title'=>$stredit, 'class'=>'edit'));	
					    $buttons[] = html_writer::link(new moodle_url('javascript:;'), '', array('title'=>$strassign, 'class'=>'assign-group-disabled'));
					    $buttons[] = html_writer::link(new moodle_url($securewwwroot.'/program/'.$CFG->pageTeamAssigncourses, array('program'=>$program->id)), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/group'), 'alt'=>$assigncourses, 'class'=>'iconsmall')), array('title'=>$assigncourses, 'class'=>'assign-courses'));
						$buttons[] = '<a href="#" class="publish-program"  rel = "'.$program->id.'" title="'.get_string('publish','course').'">'.get_string('publish','course').'</a>';                  $buttons[] = html_writer::link(new moodle_url($returnurl, array_merge($paramArray,array('delete'=>$program->id, 'sesskey'=>sesskey()))), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/delete'), 'alt'=>$strdelete, 'class'=>'iconsmall')), array('title'=>$strdelete, 'class'=>'delete delete_program'));
					}
					
				}
			}
			
			//start program image
	
			
			$programImagePath = getProgramImage($program);
			//$pImagePath =  $CFG->wwwroot.'/theme/gourmet/pix/program-img.jpg';
			//$programImagePath = '<img src="'.$pImagePath.'" alt="'.$program->name.'" title="'.$program->name.'" border="0" />';

			//end program image

			//$tableHtml .= "<td>".$program->name."</td>";
			$tableHtml .= '<td class="align_left">'.$programImagePath . '<span class="f-left">'.$program->name.'</span></td>';
			if(strlen($program->description) >200){
				$description = substr($program->description,0,200)." ...";
			}else{
				$description = $program->description;
			}
			//$description = $program->description;
			//$tableHtml .= "<td>".nl2br($description)."</td>";
			$tableHtml .= "<td>".($program->idnumber?$program->idnumber:getMDash())."</td>";
			$tableHtml .= "<td>".$cntTotalUsers."</td>";
			$tableHtml .= "<td>".$cntTotalCourses."</td>";
			//$tableHtml .= "<td>".getDateFormat($program->timecreated, $CFG->customDefaultDateFormat)."</td>";
			$tableHtml .= "<td class='adminiconsBar'>".implode(' ', $buttons)."</td>";
			$tableHtml .= "</tr>";
        }
    }
	$tableHtml .= "</table>";
	// End listing table

	//if( $USER->archetype == $CFG->userTypeAdmin ) { 
		 echo '<div class = "add-program-button"><a class="button-link add-program" href="'.$securewwwroot.'/program/addprogram.php" ><i></i><span>'.get_string("addnewprogram","program").'</span></a></div>';
    //}
	//// Record update Message
	if($_SESSION['update_msg']!=''){
		echo '<div class="clear"></div>';
		echo '<div class="bg-success text-success text-center" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
		echo '<div class="clear"></div>';
		$_SESSION['update_msg']='';
	}
	/// Record update message ends
    if (!empty($tableHtml)) {
		echo '<div class = "borderBlock">';
		echo "<h2 class='icon_title'><span class='program'></span>".get_string('programs','program')." (".$programCount.")</h2>";
		echo html_writer::start_tag('div', array('class'=>'borderBlockSpace'));
        echo html_writer::start_tag('div', array('class'=>'no-overflow'));
        echo $tableHtml;
        echo html_writer::end_tag('div');
        echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		//Pring paging bar
        echo paging_bar($programCount, $page, $perpage, $genURL);
    }

    echo $OUTPUT->footer();
    ?>
    
    
<script>


$(document).ready(function(){


	$(".delete_program").click(function(){
		
		var slotid = $(this).attr("rel");
		var txt = "<?php echo get_string('programdeleteconfirm')?>";
		
		var response = confirm(txt);
		
		if (response == true) {
		return true;
		}
		return false;
	});
	
	

	
});

</script>
    