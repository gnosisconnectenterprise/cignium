<?php

	require_once('../config.php'); 
    require_once('programlib.php');
	require_once('../local/program/program_form.php');

	require_login();

	if($USER->archetype == $CFG->userTypeStudent) {// if user is not manager redirect to home page.
		redirect($CFG->wwwroot."/");
	}


	// get url variables
	$id       = optional_param('id', 0, PARAM_INT);
	$confirm  = optional_param('confirm', 0, PARAM_BOOL);

	if ($id) {
		if (!$program = $DB->get_record('programs', array('id'=>$id))) {
			print_error('invalidprogramid');
		}
		
	} else {

		$program = new stdClass();

	}

	if ($id) {
		$PAGE->set_url('/program/addprogram.php', array('id'=>$id));
		$strprograms = get_string('editprogram','program');
	} else {
		$PAGE->set_url('/program/addprogram.php');
		$strprograms = get_string('addnewprogram','program');
	}


	
	$PAGE->set_title($SITE->fullname.": ".$strprograms);
	$PAGE->set_heading($SITE->fullname . ': '.$strprograms);
	$PAGE->set_pagelayout('globaladmin');
	navigation_node::override_active_url(new moodle_url('/program/index.php'));
	$returnUrl = $CFG->wwwroot.'/program/index.php';
	$context = context_system::instance();
	
	
	
    
	if (!empty($program->id)) { 
	
		 $contextLevel = 90; 
		 $query = "SELECT * FROM {$CFG->prefix}context WHERE contextlevel = '".$contextLevel."' AND instanceid = '".$program->id."' ";
		 $programContext = $DB->get_record_sql($query);
		 
		 // Prepare the description editor: We do support files for program descriptions
		  $editoroptions = array('maxfiles'=>EDITOR_UNLIMITED_FILES, 'subdirs' => false, 'trust'=>false, 'context'=>$context, 'noclean'=>true);
	
		  $editoroptions['subdirs'] = false;
		  $editoroptions['context'] = $context;
		  //$program = file_prepare_standard_editor($program, 'description', $editoroptions, $context, 'program', 'description', null);
		  $program = file_prepare_standard_editor($program, 'description', $editoroptions, $context, 'program', 'description', null);
		  $program = file_prepare_standard_editor($program, 'suggesteduse', $editoroptions, $context, 'program', 'suggesteduse', null);
		  $program = file_prepare_standard_editor($program, 'learningobj', $editoroptions, $context, 'program', 'learningobj', null);
		  $program = file_prepare_standard_editor($program, 'performanceout', $editoroptions, $context, 'program', 'performanceout', null);
		  
       file_prepare_standard_filemanager($program, 'programmaterial', array('subdirs'=>0), $programContext, 'program', 'programmaterial', 0);
        if ($filesOptions = programOverviewFilesOptions($program)) {
		  file_prepare_standard_filemanager($program, 'programimage', $filesOptions, $programContext, 'program', 'programimage', 0);
		}
		
   
	} else { 
		 
		$editoroptions['subdirs'] = false;
		$editoroptions['context'] = $context;
		$program = file_prepare_standard_editor($program, 'description', $editoroptions, null, 'program', 'description', null);
		$program = file_prepare_standard_editor($program, 'suggesteduse', $editoroptions, null, 'program', 'suggesteduse', null);
		$program = file_prepare_standard_editor($program, 'learningobj', $editoroptions, null, 'program', 'learningobj', null);
		$program = file_prepare_standard_editor($program, 'performanceout', $editoroptions, null, 'program', 'performanceout', null);
		
		if ($filesOptions = programOverviewFilesOptions($program)) {
		  //file_prepare_standard_filemanager($program, 'programmaterial', $filesOptions, $programContext, 'program', 'programmaterial', 0);
		  //file_prepare_standard_filemanager($program, 'programimage', $filesOptions, $programContext, 'program', 'programimage', 0);
		  file_prepare_standard_filemanager($program, 'programmaterial', $filesOptions, 'program', 'programmaterial', 0);
		  file_prepare_standard_filemanager($program, 'programimage', $filesOptions, 'program', 'programimage', 0);
		}
		
	}

	/// First create the form
	$editform = new program_form(null, array('editoroptions'=>$editoroptions,'programdata'=>$program));
	$editform->set_data($program);

	if ($editform->is_cancelled()) {
		redirect($returnUrl);

	} elseif ($data = $editform->get_data()) {
	   
		if ($data->id) { 
			
			updateProgram($data, $editform, $editoroptions);
			$_SESSION['update_msg'] = get_string('record_updated');
			
		} else {
			
			$id = createProgram($data, $editform, $editoroptions);
			$data->id = $id;
			$_SESSION['update_msg'] = get_string('record_added');
			$returnUrl = $CFG->wwwroot.'/program/index.php';
		}
		
		// adding and updating course setings
		//addUpdateProgramCourseSettings($data, $CFG->programType);
		//pr($_REQUEST);die;
		// Assigning deprtment, team and users to course under course setting 
		/*if(isset($_REQUEST['formsubmit']) && $_REQUEST['formsubmit'] == 1){
		 SaveCourseMappingData($_REQUEST);
		}*/
		// end 

		redirect($returnUrl);
	}

	$manageprograms = get_string('manageprograms','program');
	$PAGE->navbar->add($manageprograms, new moodle_url('/program/index.php'));
	$PAGE->navbar->add($strprograms);
	
	//$allocationType  = getProgramCourseAllocationType($id, $CFG->programType );


	/// Print header
	echo $OUTPUT->header();
	$editform->display();
?>


<!--  for program setting  -->


<script>
$(document).ready(function(){


	var cnt = 0;
	$("#mform1").find('.collapsible').each(function(){
		if(cnt != 0){
			$(this).addClass('collapsed');
		}
		cnt++;
	});
	var userRole = "<?php echo $USER->archetype; ?>";
	var userManager = "<?php echo $CFG->userTypeManager; ?>";
	var allocationType = "<?php echo $allocationType; ?>";
	
	
	if(allocationType == 1){
	  $('.self-allocation-box').addClass('hide');
	}else if(allocationType == 2){
	  $('.self-allocation-box').removeClass('hide');
	} 
	
	$(document).on('click','[name = "allocation_type"]', function(){
	    var allocationType = $(this).val();
		if(allocationType == 1){
		  $('.self-allocation-box').addClass('hide');
		}else if(allocationType == 2){
		  $('.self-allocation-box').removeClass('hide');
		} 
	});
	
	$(".tabLink").each(function(){
      $(this).click(function(){
        tabeId = $(this).attr("id");
        $(".tabLink").removeClass("activeLink");
        $(this).addClass("activeLink");
        $(".tabcontent").addClass("hide");
        $("#"+tabeId+"-1").removeClass("hide");					
        return false;	  
      });
    }); 
	
	
	
	/*$("#addselect_searchtext").keyup(function(){
		var searchText = $(this).val();
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/user/searchcourses.php',
				type:'POST',
				data:'action=searchNonCourse&courseid=<?php echo $courseid; ?>&search_text='+searchText,
				success:function(data){
					$('#addselect_wrapper').find('select').html(data);
				}
		});
	});
	$("#addselect_clearbutton").click(function(){
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/user/searchcourses.php',
				type:'POST',
				data:'action=searchNonCourse&courseid=<?php echo $courseid; ?>',
				success:function(data){
					$('#addselect_wrapper').find('select').html(data);
					$("#addselect_searchtext").val('');
				}
		});
	});
	$("#removeselect_clearbutton").click(function(){
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/user/searchcourses.php',
				type:'POST',
				data:'action=searchUserCourse&courseid=<?php echo $courseid; ?>',
				success:function(data){
					$('#removeselect_wrapper').find('select').html(data);
					$("#removeselect_searchtext").val('');
				}
		});
	});
	$("#removeselect_searchtext").keyup(function(){
		var searchText = $(this).val();
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/user/searchcourses.php',
				type:'POST',
				data:'action=searchUserCourse&courseid=<?php echo $courseid; ?>&search_text='+searchText,
				success:function(data){
					$('#removeselect_wrapper').find('select').html(data);
				}
		});
	});
	if(userRole != userManager){
		var fromDepartment = $('#department_select_wrapper select');
		var toDepartment = $('#department_remove_wrapper select');
	}

	var fromTeam = $('#team_addselect_wrapper select');
	var toTeam = $('#team_removeselect_wrapper select');

	var fromUser = $('#user_addselect_wrapper select');
	var toUser = $('#user_removeselect_wrapper select');

	$(document).on("click", "#department_select_wrapper select option",function(){
		$("#department_add").removeAttr("disabled");
	});
	$(document).on("click", "#department_remove_wrapper select option",function(){
		$("#department_remove").removeAttr("disabled");
	});

	$(document).on("click", "#team_addselect_wrapper select option",function(){
		$("#team_add").removeAttr("disabled");
	});
	$(document).on("click", "#team_removeselect_wrapper select option",function(){
		$("#team_remove").removeAttr("disabled");
	});
	$(document).on("click", "#user_addselect_wrapper select option",function(){
		$("#user_add").removeAttr("disabled");
	});
	$(document).on("click", "#user_removeselect_wrapper select option",function(){
		if($(this).attr('class') == 'option-disable'){
			$("#user_remove").addAttr("disabled");
		}else{
			$("#user_remove").removeAttr("disabled");
		}
	});

	$(document).on("click","#department_add",function(){
			fromDepartment.find('option:selected').appendTo(toDepartment.find('optgroup'));
	});
	$(document).on("click","#team_add",function(){
			fromTeam.find('option:selected').appendTo(toTeam.find('optgroup'));
	});
	$(document).on("click","#user_add",function(){
			fromUser.find('option:selected').appendTo(toUser.find('optgroup'));
	});

	$(document).on("click","#department_remove",function(){
			toDepartment.find('option:selected').appendTo(fromDepartment.find('optgroup'));
	});
	$(document).on("click","#team_remove",function(){
			toTeam.find('option:selected').appendTo(fromTeam.find('optgroup'));
	});
	$(document).on("click","#user_remove",function(){
		var err = 0;
		toUser.find( "option:selected" ).each(function() {
			if($(this).attr('class') == 'option-disable'){
				err = 1;
			}else{
				$(this).appendTo(fromUser.find('optgroup'));
			}
		});
		if(err == 1){
			alert("<?php echo get_string('assignment_error');?>");
			toUser.find( "option" ).each(function() {
				//$(this).removeAttr('selected');
			});
		}
	});
	$("#id_submitbutton").click(function(){
		if(userRole != userManager){
			fromDepartment.find('optgroup').find("option").each(function(){
				$(this).removeAttr("selected");
			});
		}
		fromTeam.find('optgroup').find("option").each(function(){
			$(this).removeAttr("selected");
		});
		fromUser.find('optgroup').find("option").each(function(){
			$(this).removeAttr("selected");
		});
		if($("#department_all").is(':checked')){
			if(userRole != userManager){
				fromDepartment.find('optgroup').find("option").each(function(){
					$(this).appendTo(toDepartment.find('optgroup'));
				});
			}
			fromTeam.find('optgroup').find("option").each(function(){
				$(this).appendTo(toTeam.find('optgroup'));
			});
			fromUser.find('optgroup').find("option").each(function(){
				$(this).appendTo(toUser.find('optgroup'));
			});
		}
		if(userRole != userManager){
			toDepartment.find('option').prop('selected', true);
		}
		toTeam.find('option').prop('selected', true);
		toUser.find('option').prop('selected', true);
		$("#assignform").submit();
		
	});
	
	$("#department_all").click(function(){
		if($("#department_all").is(':checked')){
			$(".tab-box").hide();
			$(".tabcontent").hide();
		}else{
			$(".tab-box").show();
			$(".tabcontent").show();
		}
	});*/
	$('#id_cancel').click(function(){
		window.location.href = '<?php echo $CFG->wwwroot; ?>/program/index.php';
	});
	 $(document).on('blur', '#id_name, #id_idnumber', function(){

		   var cVal = $(this).val();
		   cVal = $.trim(cVal);
		   $(this).val(cVal);


		}); 
		
	
	$(document).on("click", "#id_submitbutton", function(){
	
			var id_name = $('#id_name').val();
			id_name =  $.trim(id_name);

			if( id_name != '' ){  
				$(this.form).submit(function(){
				
					 $("#id_submitbutton").prop('disabled', 'disabled');
				
				});
			}
	});	
		
	/*$(document).on('click', '.fp-upload-btn', function(){
  
		  if($("input:file").length == 0 ){
		  
			  $('.moodle-dialogue-base:eq(3)').hide();
		  }
		
	
	});
	

	$(document).on('click', '.fp-btn-add a', function(){

	  $('.moodle-dialogue-base:eq(3)').show();
	  $('.moodle-dialogue-base:eq(3) .moodle-dialogue').css({'left':'470px'});

	});*/
});
</script>

<!--  end program setting  -->
<?php
	echo $OUTPUT->footer();
