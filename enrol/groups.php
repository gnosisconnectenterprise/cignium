<?php

    require_once('../config.php');
    require_once($CFG->dirroot.'/local/group/filters/lib.php');
	//require_once($CFG->dirroot.'/local/group/lib.php');
	
	
	checkLogin('managegroups');
	//$userrole =  getUserRole($USER->id);
    
    $courseid     = optional_param('courseid', 0, PARAM_INT);
	$delete       = optional_param('delete', 0, PARAM_INT);
    $confirm      = optional_param('confirm', '', PARAM_ALPHANUM);   //md5 confirmation hash
    $sort         = optional_param('sort', 'id', PARAM_ALPHANUM);
    $dir          = optional_param('dir', 'DESC', PARAM_ALPHA);
    $page         = optional_param('page', 0, PARAM_INT);
    $perpage      = optional_param('perpage', 30, PARAM_INT);        // how many per page


    if($courseid){
	  $isExist = $DB->get_field_sql("select id from {$CFG->prefix}course where id = '".$courseid."'");
	  if(!$isExist){
	     redirect( new moodle_url("/"));
	  }
	}else{
	   redirect( new moodle_url("/"));
	}
	
    $PAGE->set_title($SITE->fullname.": ".get_string('enrolledgroups', 'course'));
    $stredit   = get_string('edit');
    $strdelete = get_string('delete');
    $strdeletecheck = get_string('deletecheck');
    $strassign   = get_string('assignusers', 'group');    	
    $strconfirm = get_string('confirm');
	
	

    if (empty($CFG->loginhttps)) {
        $securewwwroot = $CFG->wwwroot;
    } else {
        $securewwwroot = str_replace('http:','https:',$CFG->wwwroot);
    }

    $returnurl = new moodle_url('/enrol/groups.php?courseid='.$courseid, array('courseid'=>$courseid,'sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'page'=>$page));

    if ($delete and confirm_sesskey()) {              // Delete a selected group, after confirmation
        
		$groupid = $delete;
        redirect(new moodle_url('/local/group/delete.php', array('groups'=>$groupid)));
		
    } 

    // create the user filter form
	$formurl = "groups.php?courseid=".$courseid;
    $gfiltering = new group_filtering(null, $formurl, null);
    echo $OUTPUT->header();
   
    $extracolumns = array();
    $columns = array_merge(array('name', 'description'),$extracolumns,
            array('timecreated'));

    foreach ($columns as $column) {
        $string[$column] = getGroupFieldName($column);
        if ($sort != $column) {
            $columnicon = "";
            if ($column == "timecreated") {
                $columndir = "DESC";
            } else {
                $columndir = "ASC";
            }
        } else {
            $columndir = $dir == "ASC" ? "DESC":"ASC";
            if ($column == "timecreated") {
                $columnicon = ($dir == "ASC") ? "sort_desc" : "sort_asc";
            } else {
                $columnicon = ($dir == "ASC") ? "sort_asc" : "sort_desc";
            }
            $columnicon = "<img class='iconsort' src=\"" . $OUTPUT->pix_url('t/' . $columnicon) . "\" alt=\"\" />";

        }
        $$column = "<a href=\"/enrol/groups.php?courseid=".$courseid."&sort=$column&amp;dir=$columndir\">".$string[$column]."</a>$columnicon";
    }

    list($extrasql, $params) = $gfiltering->get_sql_filter();
	
    $groups = getGroupsListing($sort, 0, $dir, $page*$perpage, $perpage, '', $extrasql, $params);
			
	$groupcount = getGroups(false, 0);
	$groupsearchcount = getGroups(false, 0, '', null, "", '', '', '*', $extrasql, $params);
	   

    if ($extrasql !== '') {
        echo $OUTPUT->heading("$groupsearchcount / $groupcount ".get_string('ggroup','group'));
        $groupcount = $groupsearchcount;
    } else {
        echo $OUTPUT->heading("$groupcount ".get_string('ggroup','group'));
    }

    $strall = get_string('all');

    $baseurl = new moodle_url('/enrol/groups.php?courseid='.$courseid, array('courseid' => $courseid, 'sort' => $sort, 'dir' => $dir, 'perpage' => $perpage));
    echo $OUTPUT->paging_bar($groupcount, $page, $perpage, $baseurl);

    flush();


    if (!$groups) {
        $match = array();
        echo $OUTPUT->heading(get_string('nogroupsfound','group'));

        $table = NULL;

    } else {

        $table = new html_table();
        $table->head = array ();
        $table->colclasses = array();
        $table->head[] = $name;
        $table->attributes['class'] = 'admintable generaltable';
        $table->colclasses[] = 'leftalign';
        foreach ($extracolumns as $field) {
            $table->head[] = ${$field};
            $table->colclasses[] = 'leftalign';
        }
        $table->head[] = $description;
        $table->colclasses[] = 'leftalign';
        $table->head[] = $timecreated;
        $table->colclasses[] = 'leftalign';
		$table->head[] = get_string('assigendcourses', 'course');
        $table->colclasses[] = 'centeralign';
        $table->head[] = get_string('gaction','group');
        $table->colclasses[] = 'centeralign';

        $table->id = "groups";
        foreach ($groups as $group) {
            $buttons = array();
            $lastcolumn = '';

			if( $USER->archetype == $CFG->userTypeAdmin ) {
            //if(in_array($userrole, $CFG->custommanagerroleid)){ 
			
			       // assign groups button
				   
				   $isCourseIdExist = isCourseGroupExist($group->id, $courseid);
				   $assignCoursesForGroup = getAssignCoursesForGroup($group->id, 2);
				   
				   if($isCourseIdExist){
				       $buttons[] = '<input type="button" value="'.get_string('unenrolgp','course').'" class="groupenrol"  rel="'.$group->id.'">';
				   }else{
				       $buttons[] = '<input type="button" value="'.get_string('enrolgp','course').'" class="groupenrol"  rel="'.$group->id.'">';
				   }
                 

             }
			
            $row = array ();
            $row[] = "<a href=\"../group/assignusers.php?group=$group->id\">".$group->name."</a>";
            foreach ($extracolumns as $field) {
                $row[] = $group->{$field};
            }
            $row[] = nl2br($group->description);
            $row[] = date("Y-m-d", $group->timecreated);
			$row[] = "<div class='g".$group->id."'>".$assignCoursesForGroup."</div>";
            $row[] = implode(' ', $buttons);
            $table->data[] = $row;
        }
    }

    // add filters
	
    $gfiltering->display_add();
    $gfiltering->display_active();
	

   /* added by rajesh to redirect the page otherwise filter were adding again and again while we refresh the page */	
    if(isset($_REQUEST['addfilter']) && !empty($_REQUEST['addfilter'])){
	    redirect($returnurl);
	}
	// end 

    
    if (!empty($table)) {
        echo html_writer::start_tag('div', array('class'=>'no-overflow'));
		echo html_writer::start_tag('div', array('style'=>'float:right'));
	    echo $buttonhtml = '<input type="button" value="'.get_string('backtomanagecourses','course').'" name="backtomanagecourses" onclick="window.location.href=\''.$CFG->wwwroot.'/course/index.php\'" >';
	    echo html_writer::end_tag('div');
        echo html_writer::table($table);
		echo html_writer::start_tag('div', array('style'=>'float:right'));
	    echo $buttonhtml = '<input type="button" value="'.get_string('backtomanagecourses','course').'" name="backtomanagecourses" onclick="window.location.href=\''.$CFG->wwwroot.'/course/index.php\'" >';
	    echo html_writer::end_tag('div');
        echo html_writer::end_tag('div');
        echo $OUTPUT->paging_bar($groupcount, $page, $perpage, $baseurl);
      
    }
	
	 
	
	?>
    
    
    <script type="text/javascript">
      $(document).ready(function(){
	  
	  
	   $(".groupenrol").on('click',function(){
	   
	        var obj =  $(this);
	        var groupid = $(this).attr('rel');
			var btnVal = $(this).val();
			 
			if(btnVal == '<?php echo get_string('enrolgp','course');?>'){ 
				$.ajax({
				   url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
				   type:'GET',
				   data:'action=assigngroup&id=<?php echo $courseid;?>&groupid='+groupid+'&sesskey=<?php echo sesskey();?>',
				   dataType:'json',
				   success: function(data){
					 
					  var success = data.success;
					  var error = data.error;
					  if(success == true){
					      obj.val('<?php echo get_string('unenrolgp','course');?>');
					      var assignedcourses = data.response;
						  var groupid = data.groupid;
						  $('.g'+groupid).html(assignedcourses);
					  }else{
						alert(error);
					  }
				   }
				});
			}else if(btnVal == '<?php echo get_string('unenrolgp','course');?>'){ 
			
			    $.ajax({
				   url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
				   type:'GET',
				   data:'action=unassigngroup&id=<?php echo $courseid;?>&groupid='+groupid+'&sesskey=<?php echo sesskey();?>',
				   dataType:'json',
				   success: function(data){
				      var success = data.success;
					  var error = data.error;
					  if(success == true){
						  obj.val('<?php echo get_string('enrolgp','course');?>');
						  var assignedcourses = data.response;
						  var groupid = data.groupid;
						  $('.g'+groupid).html(assignedcourses);
					  }else{
						alert(error);
					  }
					 
				   }
				});
			}
	   
	   });
	   
	  
	  });
    </script>
    
    <?php

    echo $OUTPUT->footer();



