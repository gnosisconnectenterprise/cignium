<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file processes AJAX enrolment actions and returns JSON
 *
 * The general idea behind this file is that any errors should throw exceptions
 * which will be returned and acted upon by the calling AJAX script.
 *
 * @package    core_enrol
 * @copyright  2010 Sam Hemelryk
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
	* Common module - All Ajax Hits at this page
	* Date Creation - 01/06/2014
	* Date Modification : 27/10/2014
*/

//define('AJAX_SCRIPT', true);

require('../config.php');
global $USER,$DB,$CFG;
// Must have the sesskey

$action  = required_param('action', PARAM_ALPHANUMEXT);

checkLogin();
$outcome = new stdClass();
$outcome->success = true;
$outcome->response = new stdClass();
$outcome->error = '';


switch ($action) {
  
	case 'assigngroup':
	    
		$groupid = required_param('groupid', PARAM_INT);
		$courseid = required_param('id', PARAM_INT);
		$outcome = enrolUser($groupid, $courseid);
        break;
		
	case 'unassigngroup':
	
        $groupid = required_param('groupid', PARAM_INT);
		$courseid = required_param('id', PARAM_INT);
		$outcome = unEnrolUser($groupid, $courseid);
		break;
		
	case 'deleteMyDoc':
	
        $id = required_param('id', PARAM_INT);
		$outcome = deleteMyDocuments($id);
		break;	
		
	case 'setCourseLastAccess':
	
        $userid = required_param('userid', PARAM_INT);
		$courseid = required_param('courseid', PARAM_INT);
		$resourceid = required_param('resourceid', PARAM_INT);
		$outcome = setCourseLastAccess($userid, $courseid, $resourceid);
		break;
		
	case 'deleteCourseImage':
	
        $courseId = required_param('courseId', PARAM_INT);
		$outcome = deleteCourseImage($courseId);
		break;	
	case 'deleteDepartmentImage':
        $departmentId = required_param('departmentId', PARAM_INT);
		$outcome = deleteDepartmentImage($departmentId);
		break;	

	case 'deleteGroupImage':
        $groupId = required_param('groupId', PARAM_INT);
		$outcome = deleteGroupImage($groupId);
		break;	

	case 'checkCurrentActionsHeader':
		$outcome = checkCurrentActionsHeader();
		break;	

	case 'setCourseBadge':
		$userid = required_param('userid', PARAM_INT);
		$courseid = required_param('courseid', PARAM_INT);
		$resourceid = required_param('resourceid', PARAM_INT);
		$outcome = setCourseBadge($userid,$courseid);
		break;	
		
	case 'getUserListByDnT':
	
		$department = required_param('department', PARAM_RAW);
		$team = required_param('team', PARAM_RAW);
		$user = required_param('user', PARAM_RAW);
		$job_title = optional_param('job_title', '-1', PARAM_RAW);
		$company = optional_param('company', '-1', PARAM_RAW);
		$searchOwner = optional_param('search_owner', 0, PARAM_INT);
		$sel_mode = optional_param('sel_mode', 1, PARAM_INT);
		$departmentArr = explode("@",$department);
		$teamArr = explode("@",$team);
		$userArr = explode("@",$user);
		$jobTitleArr = explode("@",$job_title);
		$companyArr = explode("@",$company);
		$isReport = true;
		$outcome = getUserListByDnT($departmentArr, $teamArr, $userArr, 1, $isReport, 0, $jobTitleArr, $companyArr,$sel_mode,$searchOwner);
		break;
		
	case 'getTeamListByDepartment':
		$department = required_param('department', PARAM_RAW);
		$team = required_param('team', PARAM_RAW);
		$isReport = optional_param('isReport', true, PARAM_BOOL);
		$departmentArr = explode("@",$department);
		$teamArr = explode("@",$team);
		$return = 1;
		$outcome = getTeamListByDepartment($departmentArr, $teamArr, $return , $isReport );
		break;	
	case 'getProgramListByDnT':
		$department = required_param('department', PARAM_RAW);
		$team = required_param('team', PARAM_RAW);		
		$program = required_param('program', PARAM_RAW);
		$departmentArr = explode("@",$department);
		$teamArr = explode("@",$team);		
		$programArr = explode("@",$program);
		$isReport = true;
		$outcome = getProgramListByDnT($departmentArr, $teamArr, $programArr, $isReport);
		break;	
	case 'getCourseListByDnT':
	    $courseTypeId = optional_param('course_type_id', $CFG->courseTypeOnline, PARAM_RAW);
		$department = required_param('department', PARAM_RAW);
		$team = required_param('team', PARAM_RAW);	
        $program = required_param('program', PARAM_RAW);		
		$course = required_param('course', PARAM_RAW);
		$departmentArr = explode("@",$department);
		$teamArr = explode("@",$team);	
        $programArr = explode("@",$program);		
		$courseArr = explode("@",$course);
		$return = 1;
		$isReport = true;
		//$outcome = getCourseListByDnT($departmentArr, $teamArr, $courseArr);
		$outcome = getCourseListByDnT($departmentArr, $teamArr, $programArr, $courseArr, $return , $isReport, $courseTypeId );
		break;	
	case 'getDepartmentManagerList':
		GLOBAL $USER;
		$departmentId = required_param('departmentId', PARAM_RAW);
		$userRole = required_param('userRole', PARAM_RAW);
		$userId = optional_param('userId',0, PARAM_RAW);
		$departmentManagerArray = getUsersList(array('manager'), $departmentId);
		if($USER->archetype != $CFG->userTypeManager){
			$option = '<option value = "0">'.get_string('select_department_manager').'</option>';
		}
		if(!empty($departmentManagerArray)){
			foreach($departmentManagerArray as $departmentManager){
				$selected = '';
				if($userId != $departmentManager->id){
						if($USER->archetype == $CFG->userTypeManager && $departmentManager->id == $USER->id){
							$selected = 'selected';
						}
						$option .= '<option value = "'.$departmentManager->id.'" '.$selected.'>'.$departmentManager->firstname.' '.$departmentManager->lastname.'</option>';
				}
			}
		}else{
			$option = '<option value = "0">'.get_string('select_department_manager').'</option>';
		}
		echo $option;die;
		break;	
	case 'enrolUserToProgram':
		require_once($CFG->dirroot . '/program/programlib.php');
		GLOBAL $USER;
		$programId = required_param('programId', PARAM_RAW);
		$programEnddate = $DB->get_record_sql("SELECT MAX(pg.end_date) AS enddate FROM mdl_program_group as pg LEFT JOIN mdl_groups_members AS gm ON gm.groupid = pg.group_id LEFT JOIN mdl_groups AS g ON g.id = gm.groupid AND gm.is_active = 1 WHERE pg.program_id = ".$programId." AND pg.is_active = 1 AND gm.userid = ".$USER->id);
		$endDate = "";
		if(!empty($programEnddate)){
			$endDate = $programEnddate->enddate;	
		}
		$success = assignProgramToUser($programId, $USER->id,$endDate);
		break;	
	case 'enrolUserToCourse':
		GLOBAL $USER,$DB;
		$courseId = required_param('courseId', PARAM_RAW);
		$enrolId  = CheckCourseEnrolment($courseId);
		$enrolledId = enrolUserToCourse($enrolId,$USER->id,$courseId);
		$courseEnddate = $DB->get_record_sql("SELECT MAX(gc.end_date) as enddate FROM mdl_groups_course AS gc LEFT JOIN mdl_groups_members as gm ON gm.groupid = gc.groupid LEFT JOIN mdl_groups as g ON g.id = gm.groupid AND gm.is_active = 1 WHERE gc.courseid = ".$courseId." AND gc.is_active = 1  AND gm.userid = ".$USER->id);
		$endDate = "";
		if(!empty($courseEnddate)){
			$endDate = $courseEnddate->enddate;	
		}
		$courseIdArr = array($courseId);
		$userMapId = assignProgramCourseToUsers(0,$courseId,$USER->id,'user',$USER->id,$endDate);
		userEnrolMailToCourses($courseIdArr,$USER->id,$endDate,0);
		break;	
	case 'getDepartmentTeamList':
		GLOBAL $USER;
		$departmentId = required_param('departmentId', PARAM_RAW);
		$departmentTeams = getDepartmentTeams($departmentId);
		$option = '<option value = "0">'.get_string('select_team').'</option>';
		if(!empty($departmentTeams)){
			foreach($departmentTeams as $departmentTeam){
				$option .= '<option value = "'.$departmentTeam->id.'" '.$selected.'>'.$departmentTeam->name.'</option>';
			}
		}
		echo $option;die;
		break;	
	case 'getRightPanelCalendar':

		$calenderData = getRightPanelCalendar();
		echo $calenderData;die;
		break;	
	case 'getDepartmentPrimaryManager':
		GLOBAL $DB,$CFG,$USER;
		$departmentId = required_param('departmentId', PARAM_RAW);
		$currentUser = required_param('currentUser', PARAM_RAW);
		$calenderData = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname
											FROM mdl_user AS u
											LEFT JOIN mdl_department_members AS dm ON dm.userid = u.id
											WHERE dm.is_active = 1 AND u.deleted = 0 AND u.is_primary = 1 AND dm.departmentid = $departmentId");
		if($calenderData && $calenderData->id != $currentUser){
			echo $calenderData->firstname." ".$calenderData->lastname;
		}else{
			echo '';
		}die;
		break;	
	case 'getUserType':
		GLOBAL $DB,$CFG,$USER;
		if($USER->archetype == $CFG->userTypeStudent){
			echo "noAccess";die;
		}
		echo "Access";die;
		break;	

	case 'getProgramCourse':
		GLOBAL $DB,$CFG,$USER;
		require_once($CFG->dirroot.'/my/learninglib.php'); 
		require_once($CFG->dirroot.'/mod/scheduler/lib.php'); 
		$programIdDiv = optional_param('programId',0, PARAM_RAW);
		$getAsset = optional_param('getAsset',0, PARAM_INT);
		$classType = optional_param('dataClassType','', PARAM_RAW);
		$selectionType = optional_param('type','', PARAM_RAW);
		$expired = optional_param('expired','', PARAM_RAW);
		$showClasses = optional_param('showClasses','all', PARAM_RAW);
		$showClassStatus = optional_param('showClassStatus','all', PARAM_RAW);
		$courseExpiredText = 1;
		$programId = explode('_',$programIdDiv);
		$courseId = $programId[1];
		$programId = $programId[0];
		$ky = $programIdDiv;
		$certificateLink = "";
		if($selectionType==2 && $CFG->isCertificate==1)
		{
			if($programId!=0){
			$certificateLink = certificateLink($programId,'programme');
			}
			else{
				$courseTypeId = getCourseTypeIdByCourseId($courseId);
				if($courseTypeId == $CFG->courseTypeClassroom){
					$certificateLink == "";
				}
				else{
				$certificateLink = certificateLink($courseId,'course');
				}
			}
		}
		$courseDiv = '<div class="d-box no-box '.$classType.'" id="dbox'.$programIdDiv.'">
				
							<div class="section-box">
								<div class="c-box" id="cbox8">';
		if($programId != 0){
			$courseDiv .= getProgramAsset($programId,$getAsset,$expired,$selectionType);
		}else{
			$courseTypeId = getCourseTypeIdByCourseId($courseId);
			$CourseEndDate = $DB->get_record('course',array('id'=>$courseId));
			if($courseTypeId != $CFG->courseTypeClassroom && $CourseEndDate->enddate != ''){
				if(($CourseEndDate->enddate+86399) < strtotime('now')){
					$expired = 1;
					$courseExpiredText = 0;
				}
			}

			if($courseTypeId == $CFG->courseTypeClassroom && $getAsset == 1){
				$sectionArr = getTopicsSection($courseId, $USER->id, '', $courseTypeId);
				if(count($sectionArr) > 0 ){
					foreach($sectionArr as $sectionId=>$sections){
						if(count($sections->courseModule) > 0){
							$topicsCourseNew = array();
							foreach($sections->courseModule as $cmId=>$courseModule){

								$topicsCourse = $courseModule['topicCourse'];
								if(count($topicsCourse)>0){
									foreach($topicsCourse as $topics){
										//if($topics->allow_student_access == 1){
											$topicsCourseNew[$topics->resource_type_id]['name'] = $topics->resource_type;
											$topicsCourseNew[$topics->resource_type_id]['details'][] = $topics;
										//}
									}
								}
							}

						}

					}

				}
				$assetdata = array();
				$j = 0;
				$courseDetails = $DB->get_record("course",array('id'=>$courseId));
				if(!empty($topicsCourseNew)){
					foreach($topicsCourseNew as $topicAsset){
						foreach($topicAsset['details'] as $topicsCourseArr){
							$assetdata[$j] = $topicsCourseArr;
							$assetdata[$j]->module = 17;
							$assetdata[$j]->description = $topicsCourseArr->resourcesummary;
							$assetdata[$j]->allow_student_access = $topicsCourseArr->allow_student_access;
							$assetdata[$j]->asset_info = $topicsCourseArr->resourcesummary;
							$assetdata[$j]->asset_name = $topicsCourseArr->resoursename;
							$assetdata[$j]->resource_type_name = $topicsCourseArr->resource_type;
							$assetdata[$j]->coursetype_id = $courseDetails->coursetype_id;
							$assetdata[$j]->primary_instructor = $courseDetails->primary_instructor;
							$assetdata[$j]->classroom_instruction = $courseDetails->classroom_instruction;
							$assetdata[$j]->classroom_course_duration = $courseDetails->classroom_course_duration;
							$assetdata[$j]->course_createdby = $courseDetails->createdby;
							$assetdata[$j]->resource_createdby = $topicsCourseArr->rcreatedby;
							$assetdata[$j]->learningobj = $courseDetails->learningobj;
							$assetdata[$j]->performanceout = $courseDetails->performanceout;
							$j++;
						}
					}
				}
			}else{
				$assetdata = getAssetInfoForCourse($courseId);
                                //pr($assetdata);die;
			}
			if(($courseTypeId == $CFG->courseTypeClassroom) || !empty($assetdata)){
				$courseDiv .= getOnlyAssetHtml($assetdata,$courseTypeId,$courseId,$selectionType,$expired,0,$showClasses,$showClassStatus);
			}else{
				$courseDiv .= "No Record(s) found";
			}
		}
		$userRole = $DB->get_record_sql("SELECT ra.roleid,u.id,r.name FROM mdl_role as r LEFT JOIN mdl_role_assignments AS ra ON ra.roleid = r.id LEFT JOIN mdl_user AS u ON u.id = ra.userid WHERE ra.userid = ".$USER->id);
		$outputDiv = '';
		if(!empty($userRole) && $userRole->name == $CFG->userTypeManager && $courseExpiredText == 1 && $expired == 1){
			$outputDiv = "<div class='f-left'><span>".get_string('manager_expired_string')."</span></div>";
		}
		$courseDiv.="</div>".$outputDiv."</div></div>";
		echo $courseDiv;die;
		break;
	case 'getClassroomContent':
		GLOBAL $DB,$CFG,$USER;
		require_once($CFG->dirroot.'/mod/scheduler/lib.php'); 
		$classroomIdDivId = required_param('classroomId', PARAM_RAW);
		$classroomId = explode('_',$classroomIdDivId);
		$classroomId = $classroomId[1];
		$getAsset = optional_param('getAsset',2, PARAM_INT);
		$courseDiv = '<div class="d-box border-left-green" id="dbox'.$classroomIdDivId.'">
							<div class="section-box">
								<div class="c-box" id="cbox8">';
		$courseDiv .= getClassesPreviewHtmlForLearner($classroomId,1,$getAsset);
		$courseDiv.="</div></div></div>";
		echo $courseDiv;die;
		break;
	case 'enrolUserToClassroom':
		GLOBAL $DB,$CFG,$USER;
		require_once($CFG->dirroot.'/mod/scheduler/lib.php'); 
		$userId		= optional_param('userId' ,0 ,PARAM_INT);
		$classId	= optional_param('classId',0, PARAM_INT);
		$enrol = assignUserToClass($classId, array($userId));
		$mailto = requestForClass($userId,$classId);
		echo '1';
		die;
		break;
	case 'RequestForCourse':
		GLOBAL $DB,$CFG,$USER;
		$programEnrolId = optional_param('programId',0, PARAM_INT);
		$courseEnrolId = optional_param('courseId',0, PARAM_INT);
		$extension = optional_param('extension',0, PARAM_INT);

		if($programEnrolId){
			$programDetails = $DB->get_record_sql("SELECT id,name FROM mdl_programs WHERE id = ".$programEnrolId);
			if(count($programDetails) > 0){
				requestForCourse($USER->id,0,$programEnrolId,$extension);
				$_SESSION['update_msg'] = get_string('request_sent_success');
				echo '1';die;
			} else {
				$_SESSION['update_msg'] = get_string('error_in_update');
				echo '0';die;
			}			
		}elseif($courseEnrolId){
			////// Getting Course Detail /////
			$courseQuery = "SELECT id, fullname, idnumber FROM {$CFG->prefix}course WHERE id = '".$courseEnrolId."'";
			$courseDetails = $DB->get_record_sql($courseQuery);
			if(!empty($courseDetails)){
				requestForCourse($USER->id,$courseEnrolId,0,$extension);
				$_SESSION['update_msg'] = get_string('request_sent_success');
				//$_SESSION['update_msg'] = get_string('emailsentsuccess', 'course');
				echo '1';die;
			} else {
				//$_SESSION['update_msg'] = get_string('emailsenterror', 'course');
				$_SESSION['update_msg'] = get_string('error_in_update');
				echo '0';die;
			}
		}
		$_SESSION['update_msg'] = get_string('error_in_update');
		echo '0';die;
		break;
	case 'checkCourseProgramRequest':
		GLOBAL $DB,$CFG,$USER;
		$programEnrolId = optional_param('programId',0, PARAM_INT);
		$courseEnrolId = optional_param('courseId',0, PARAM_INT);
		$extension = optional_param('extension',0, PARAM_INT);
		if($programEnrolId){
			$status = getLogdata($programEnrolId, 0, $USER->id, 0,$extension);
			$statusDeactive = getLogdata($programEnrolId, 0, $USER->id, 2,$extension);
		}elseif($courseEnrolId){
			$status = getLogdata(0, $courseEnrolId, $USER->id, 0,$extension);
			$statusDeactive = getLogdata(0, $courseEnrolId, $USER->id, 2,$extension);
		}
		if(!empty($status)){
			echo '1';
		}else{
			if($statusDeactive){
				echo '2';
			}else{
				echo '0';
			}
		}
		die;
		break;
	case 'enrolUserOnRequest':
		GLOBAL $DB,$CFG,$USER;
		require_once($CFG->dirroot . '/program/programlib.php');
		$programEnrolId = optional_param('programId',0, PARAM_INT);
		$courseEnrolId = optional_param('courseId',0, PARAM_INT);
		$userId = optional_param('userId',0, PARAM_INT);
		$enrolUser = optional_param('enrolUser',0, PARAM_INT);
		$end_date = optional_param('end_date',0, PARAM_RAW);
		$remarksText = $_REQUEST['remarks_text'];
		if($end_date == 0){
			$end_date = "";
		}else{
			$end_date = strtotime($end_date);
		}
		$userRole = $DB->get_record_sql("SELECT r.name FROM mdl_role r LEFT JOIN mdl_role_assignments as ra On ra.roleid = r.id LEFT JOIN mdl_user u ON u.id = ra.userid AND ra.contextid = 1 WHERE u.id = ".$userId);
		$userRole = $userRole->name;
		$userDepartment = $DB->get_record_sql("SELECT d.departmentid FROM mdl_department_members d WHERE d.userid = ".$userId);

		if($programEnrolId){
			$status = getLogdata($programEnrolId, 0, $userId, 0);
		}elseif($courseEnrolId){
			$status = getLogdata(0, $courseEnrolId, $userId, 0);
		}
		if($status){
			$statusId = 0;
			$requestStatus = 'Pending';
			if($enrolUser == 1){
				$statusId = 1;
				$requestStatus = 'Accepted';
			}elseif($enrolUser == 0){
				$statusId = 2;
				$requestStatus = 'Declined';
			}
			$success = saveLogdata($programEnrolId, $courseEnrolId, $userId, 0,"", $USER->id, 1,'',1,$statusId);
			if($statusId == 1){
				if($programEnrolId){
					if(strtolower($userRole) == strtolower($CFG->userTypeStudent)){
						$userenrolsuccess = assignProgramToUser($programEnrolId, $userId,$end_date);
					}else{
						$userenrolsuccess = assignProgramToDepartment($programEnrolId, $userDepartment->departmentid);
					}
					CourseRequestAccepted($userId,0,$programEnrolId,$end_date);
				}elseif($courseEnrolId){
					if(strtolower($userRole) == strtolower($CFG->userTypeStudent)){
						$enrolId  = CheckCourseEnrolment($courseEnrolId);
						$enrolledId = enrolUserToCourse($enrolId,$userId,$courseEnrolId);
						$userenrolsuccess = assignProgramCourseToUsers(0,$courseEnrolId,$userId,'user',$userId,$end_date);
					}else{
						$courses = array();
						$courses[] = $courseEnrolId;
						$userenrolsuccess = assignCourseToDepartment($userDepartment->departmentid,$courses,0);
					}
					CourseRequestAccepted($userId,$courseEnrolId,0,$end_date);
				}
			}else{
				if($programEnrolId){
					//$enroll = assignProgramToUser($programEnrolId, $userId);
					//$unenroll = deactivateProgramUsers($programEnrolId, $userId,$remarksText);
					CourseRequestDeclined($userId,0,$programEnrolId,$remarksText);
				}elseif($courseEnrolId){
					//$enroll = assignProgramCourseToUsers(0,$courseEnrolId,$userId,'user',$userId);
					//$unenroll = deactivateUserMappingData(0,$courseEnrolId,$userId,'user',$userId,$remarksText);
					CourseRequestDeclined($userId,$courseEnrolId,0,$remarksText);
				}
			
			}
			if(!empty($success)){
				if($programEnrolId){
					////// Getting Program Detail /////
					$programDetails = $DB->get_record_sql("SELECT id,name FROM mdl_programs WHERE id = ".$programEnrolId);
					if(count($programDetails) > 0){
						
						$userCreatorId = $USER->parent_id;
						$fromUser->email = $USER->email;
						$fromUser->firstname = $USER->firstname;
						$fromUser->lastname = $USER->lastname;
						$fromUser->maildisplay = true;
						$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
						$fromUser->id = $USER->id;
						$fromUser->firstnamephonetic = '';
						$fromUser->lastnamephonetic = '';
						$fromUser->middlename = '';
						$fromUser->alternatename = '';
				
						////// Getting To User /////
						$toUserQuery = "SELECT id, firstname, lastname, email, createdby FROM {$CFG->prefix}user WHERE id = '".$userId."'";

						$toUserDetails = $DB->get_record_sql($toUserQuery);
						
						$toUser->email = $toUserDetails->email;
						$toUser->firstname = $toUserDetails->firstname;
						$toUser->lastname = $toUserDetails->lastname;
						$toUser->maildisplay = true;
						$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
						$toUser->id = $userId;
						$toUser->firstnamephonetic = '';
						$toUser->lastnamephonetic = '';
						$toUser->middlename = '';
						$toUser->alternatename = '';
						$subject = 'Request for Assign Program ('.(($programDetails->id != '') ? $programDetails->id.' - ' : '').$programDetails->name.') is '.$requestStatus;

						$messageText = 'Dear '.$toUserDetails->firstname.','."\n\n".'Your request to assign program ('.(($programDetails->id != '') ? $programDetails->id.' - ' : '').$programDetails->name.') is '.$requestStatus.' From,'."\n".$USER->firstname.' '.$USER->lastname." (".$USER->username.")";

						//email_to_user($toUser, $fromUser, $subject, $messageText, $messageHtml, "", "", true);
					}	
				}elseif($courseEnrolId){
					////// Getting Course Detail /////
					$courseQuery = "SELECT id, fullname, idnumber FROM {$CFG->prefix}course WHERE id = '".$courseEnrolId."'";
					$courseDetails = $DB->get_record_sql($courseQuery);
					if(!empty($courseDetails)){
						$userCreatorId = $userId;
						$fromUser->email = $USER->email;
						$fromUser->firstname = $USER->firstname;
						$fromUser->lastname = $USER->lastname;
						$fromUser->maildisplay = true;
						$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
						$fromUser->id = $USER->id;
						$fromUser->firstnamephonetic = '';
						$fromUser->lastnamephonetic = '';
						$fromUser->middlename = '';
						$fromUser->alternatename = '';

						////// Getting To User /////
						$toUserQuery = "SELECT id, firstname, lastname, email, createdby FROM {$CFG->prefix}user WHERE id = '".$userCreatorId."'";
						$toUserDetails = $DB->get_record_sql($toUserQuery);
						
						$toUser->email = $toUserDetails->email;
						$toUser->firstname = $toUserDetails->firstname;
						$toUser->lastname = $toUserDetails->lastname;
						$toUser->maildisplay = true;
						$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
						$toUser->id = $userCreatorId;
						$toUser->firstnamephonetic = '';
						$toUser->lastnamephonetic = '';
						$toUser->middlename = '';
						$toUser->alternatename = '';
						$subject = 'Request for Assign Course ('.(($courseDetails->idnumber != '') ? $courseDetails->idnumber.' - ' : '').$courseDetails->fullname.') is '.$requestStatus;

						$messageText = 'Dear '.$toUserDetails->firstname.','."\n\n".'Your request to assign course ('.(($courseDetails->idnumber != '') ? $courseDetails->idnumber.' - ' : '').$courseDetails->fullname.') is '.$requestStatus.' From,'."\n".$USER->firstname.' '.$USER->lastname." (".$USER->username.")";
						//email_to_user($toUser, $fromUser, $subject, $messageText, $messageHtml, "", "", true);
					}
				}
				$_SESSION['update_msg'] = get_string('record_updated');
				echo "success";die;
			}else{
				$_SESSION['update_msg'] = get_string('error_in_update');
				echo "error";die;
			}
		}else{
			$_SESSION['update_msg'] = get_string('error_in_update');
			echo "error";die;
		}
		break;
	case 'enrolUserInClassroom':
		GLOBAL $DB,$CFG,$USER;
		require_once($CFG->dirroot . '/mod/scheduler/lib.php');
		$enrolid = optional_param('enrolid',0, PARAM_INT);
		$enrolUser = optional_param('enrolUser',0, PARAM_INT);
		$userIds = optional_param('userIds',0, PARAM_RAW);
		$userIdsArray = explode(',',$userIds);
		if(isset($_REQUEST['remarks_text'])){
			$remarksText = $_REQUEST['remarks_text'];
		}else{
			$remarksText = '';
		}
		$err = 0;
		if($enrolid){
			foreach($userIdsArray as $userArray){
				$schData = $DB->get_record('scheduler_enrollment', array('scheduler_id' => $enrolid,'userid'=>$userArray));
				$schEnrolId = $schData->id;
				
				$data = new stdClass();
				if($enrolUser == 1){
					$statusId = 1;
					$description = "Accepted";
				}elseif($enrolUser == 0){
					$statusId = 2;
					$description = "Declined";
				}
				$data->id			= $schEnrolId;
				$data->is_approved	= $statusId;
				$data->declined_remarks	= $remarksText;
				$data->response_date= time();
				$data->timemodified= time();
				$data->action_taken_by= $USER->id;
				if($DB->update_record('scheduler_enrollment', $data)){

					add_to_log(1, 'scheduler', 'Unassign user from class', "Unassign user from class", $userArray, $enrolid);
					$scheduler = $DB->get_record('scheduler_enrollment', array('id' => $schEnrolId));
					$schedulerData = $DB->get_record('scheduler', array('id' => $scheduler->scheduler_id));
					if($enrolUser == 1){
						$course = $DB->get_record('course',array('id' => $schedulerData->course));
						$schedulerSlots = $DB->get_records_sql('SELECT ss.id,ss.sessionname,ss.starttime,ss.duration FROM mdl_scheduler_slots as ss WHERE ss.schedulerid = '.$schedulerData->id);
						if(!empty($schedulerSlots)){
							foreach($schedulerSlots as $Slots){
								$title = $course->fullname.':'.$Slots->sessionname;
								$description = $course->summary;
								$eventAdded = addClassEvent($title,$description,$scheduler->userid,$Slots->id,$Slots->starttime,$Slots->duration);
							}
						}
						requestAccepted($scheduler->userid,$schedulerData->id);
					}else{
						requestDeclined($scheduler->userid,$schedulerData->id);
						deleteEventForClass($schedulerData->id,$scheduler->userid);
					}
					$_SESSION['update_msg'] = get_string('record_updated');
				}else{
					$_SESSION['update_msg'] = get_string('error_in_update');
					$err++;
				}
			}
		}else{
			$_SESSION['update_msg'] = get_string('error_in_update');
			$err++;
		}
		if($err != 0){
			echo 'error';
		}else{
			echo 'success';
		}
		die;
		break;
	case 'publishCourse':
		GLOBAL $DB,$CFG,$USER;
		$courseId = optional_param('courseId',0, PARAM_INT);
		if($courseId){
			$published = publishCourse($courseId, 1);
			if($published == 0){
				echo 'noasset';
			}else{
				echo 'success';
			}
		}else{
			echo 'error';
		}
		die;
		break;
	case 'publishProgram':
		GLOBAL $DB,$CFG,$USER;
		$programId = optional_param('programId',0, PARAM_INT);
		if($programId){
			$published = publishProgram($programId, 1);
			if($published == 0){
				echo 'nocourses';
			}else{
				echo 'success';
			}
		}else{
			echo 'error';
		}
		die;
		break;	
	case 'checkSeatEnrol':
		GLOBAL $DB,$CFG,$USER;
		$enrolid = optional_param('enrolid',0, PARAM_INT);
		$enrolUser = optional_param('enrolUser',0, PARAM_INT);
		
		if($enrolid){
			$scheduler = $DB->get_record('scheduler', array('id' => $enrolid));
			$schedulerSeats = $DB->get_record_sql('	SELECT count(*) as cnt
												FROM mdl_scheduler_enrollment se
												WHERE se.scheduler_id = '.$enrolid.' AND se.is_approved = 1');
			if(!empty($scheduler)){
				$schedulerData = $scheduler;
				if(!empty($schedulerData)){
					if($schedulerData->no_of_seats != 0){
						if(!empty($schedulerSeats)){
							if($schedulerData->no_of_seats <= $schedulerSeats->cnt){
								echo $schedulerData->no_of_seats;
							}else{
								echo 'success';
							}
						}else{
							echo 'success';
						}
					}else{
						echo 'success';
					}
				}else{
					echo 'error';	
				}
			}else{
				echo 'success';
			}
		}else{
			echo 'error';
		}
		die;
		break;
	case 'getClasses':
		GLOBAL $DB,$CFG,$USER;
		$course = optional_param('courses',0, PARAM_RAW);		
		$classesSelected = optional_param('classes',0, PARAM_RAW);		
		$courses = explode("@",$course);
		$classesSelected = explode("@",$classesSelected);
		$selectedC = in_array('-1', $classesSelected)?'selected="selected"':'';
		$select = '<option value="-1" '.$selectedC.' >'.get_string('allclasses','classroomreport').'</option>';
		$where = '';
		if($USER->archetype == $CFG->userTypeAdmin){
			if(in_array('-1', $courses)){
				$query = "SELECT s.id,s.name,s.course FROm mdl_scheduler as s WHERE 1 = 1 ORDER BY s.name ASC";
			}else{
				$courseIds = count($courses) > 0 ?implode(",", $courses):0;
				if($courseIds != 0){
					$where = ' AND s.course IN ('.$courseIds.')';
				}
				$query = "SELECT s.id,s.name,s.course FROm mdl_scheduler as s WHERE 1 = 1".$where." ORDER BY s.name ASC";
			}
		}else{
			if(in_array('-1', $courses)){
				$query = "	SELECT s.id,s.name,s.course
							FROM mdl_scheduler AS s
							WHERE s.isactive = 1 AND 
							(s.teacher = ".$USER->id." || 
							s.course IN (SELECT c.id
							FROM mdl_course c
							WHERE (c.primary_instructor = ".$USER->id." || c.createdby = ".$USER->id." ) AND c.coursetype_id = 2)
							)"." ORDER BY s.name ASC";
			}else{
				$courseIds = count($courses) > 0 ?implode(",", $courses):0;
				if($courseIds != 0){
					$where = ' AND s.course IN ('.$courseIds.')';
				}
				$query = "	SELECT s.id,s.name,s.course
							FROM mdl_scheduler AS s
							WHERE 1 = 1 AND 
							(s.teacher = ".$USER->id." || 
							s.course IN (SELECT c.id
							FROM mdl_course c
							WHERE (c.primary_instructor = ".$USER->id." || c.createdby = ".$USER->id." ) AND c.coursetype_id = 2)
							)".$where." ORDER BY s.name ASC";
			}
		}
		$classes = $DB->get_records_sql($query);
		//pr($classes)
		foreach($classes as $class){
			$selectedC = in_array($class->id, $classesSelected)?'selected="selected"':'';
			$select .= '<option value="'.$class->id.'" '.$selectedC.'>'.$class->name.'</option>';
		}
		$outcome->success = true;
		$outcome->response = $select;
		break;
		case 'getClassesWithoutAll':
			GLOBAL $DB,$CFG,$USER;
			$course = optional_param('courses',0, PARAM_RAW);
			$classesSelected = optional_param('classes',0, PARAM_RAW);
			 if($course){
				$classArr = getClassesOfClassroom($course);
				/* if(count($classArr) > 0 ){
					$clKArr = array_keys($classArr);
					$classesSelected = $clKArr[0];
				} */
					
			} 
			$courses = explode("@",$course);
			$classesSelected = explode("@",$classesSelected);
			$select .= '<optgroup label="'.get_string('classname','classroomreport').'">';
			foreach($classArr as $class){
				
				$selectedC = in_array($class->id, $classesSelected)?'selected="selected"':'';
				$select .= '<option value="'.$class->id.'" '.$selectedC.'>'.$class->name.'</option>';
			}
			$select .= '</optgroup>';
			$outcome->success = true;
			$outcome->response = $select;
			break;
		case 'getManagersByDepartment':
		GLOBAL $DB,$CFG,$USER;
		$department = optional_param('department',0, PARAM_RAW);	
		$manager = optional_param('manager',0, PARAM_RAW);	
		$managersSelected = explode("@",$manager);

		$departments = explode("@",$department);
		$classesSelected = explode("@",$departments);
		//$selectedC = in_array('-1', $classesSelected)?'selected="selected"':'';
		$selectedC = in_array('-1', $managersSelected)?'selected="selected"':'';
		$select = '<option value="-1" '.$selectedC.' >'.get_string('allmanagers','user').'</option>';
		$where = '';
		if(in_array('-1', $departments)|| in_array('0', $departments)){
			$query = "	SELECT u.id,CONCAT(u.firstname,' ',u.lastname,' (',u.username,')') as name
						FROM mdl_user u
						LEFT JOIN mdl_department_members AS dm ON u.id = dm.userid
						LEFT JOIN mdl_role_assignments as ra ON ra.userid = u.id
						LEFT JOIN mdl_role as r ON r.id = ra.roleid
						WHERE r.name = 'manager' AND dm.is_active = 1 AND u.deleted = 0  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
		}else{
			$courseIds = count($departments) > 0 ?implode(",", $departments):0;
			if($courseIds != 0){
				$where = ' AND dm.departmentid IN ('.$courseIds.')';
			}
			$query = "	SELECT u.id,CONCAT(u.firstname,' ',u.lastname,' (',u.username,')') as name
						FROM mdl_user u
						LEFT JOIN mdl_department_members AS dm ON u.id = dm.userid
						LEFT JOIN mdl_role_assignments as ra ON ra.userid = u.id
						LEFT JOIN mdl_role as r ON r.id = ra.roleid
						WHERE r.name = 'manager' AND dm.is_active = 1 AND u.deleted = 0".$where."  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
		}
		$managers = $DB->get_records_sql($query);
		foreach($managers as $manager){
			$selectedC = in_array($manager->id, $managersSelected)?'selected="selected"':'';
			$select .= '<option value="'.$manager->id.'" '.$selectedC.'>'.$manager->name.'</option>';
		}
		$outcome->success = true;
		$outcome->response = $select;
		break;
		
	case 'getUserListByDnUserType':
		$department = required_param('department', PARAM_RAW);
		$team = required_param('team', PARAM_RAW);
		$checkInstructor = required_param('checkInstructor', PARAM_INT);
		$job_title = optional_param('job_title', '-1', PARAM_RAW);
		$company = optional_param('company', '-1', PARAM_RAW);
		$user = required_param('user', PARAM_RAW);
		$selMode       = optional_param('sel_mode', 1, PARAM_RAW);
		$sUserType     = optional_param('userType', '-1', PARAM_RAW);
		
		$sDepartmentArr = explode("@",$department);
		$sTeamArr = explode("@",$team);
		$sUserArr = explode("@",$user);
		$sUserTypeArr = explode("@",$sUserType);
		$sJobTitleArr = explode("@",$job_title);
		$sCompanyArr = explode("@",$company);
	
		$isReport = true;
		
		//$outcome = getUserListByDnT($departmentArr, $teamArr, $userArr, 1, $isReport, $checkInstructor, $jobTitleArr, $companyArr);

		   
	   if($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2 ){

		 $checkInstructor = 0;
		 $outcome = getUserListByDnT($sDepartmentArr, $sTeamArr, $sUserArr, 1, $isReport, $checkInstructor, $sJobTitleArr, $sCompanyArr, $selMode );
		 
	   }elseif(in_array($CFG->userTypeInstructor, $sUserTypeArr)){
		  $checkInstructor = 1;
		  $outcome = getUserListByDnT($sDepartmentArr, $sTeamArr, $sUserArr, 1, $isReport, $checkInstructor, $sJobTitleArr, $sCompanyArr, $selMode );
	   }elseif(in_array($CFG->userTypeStudent, $sUserTypeArr)){
		   $checkInstructor = 0;
		   $outcome = getUserListByDnT($sDepartmentArr, $sTeamArr, $sUserArr, 1, $isReport, $checkInstructor, $sJobTitleArr, $sCompanyArr, $selMode );
	   }
			
	
		
		break;

	case 'saveCourseEnrolmentData':
		GLOBAL $USER,$CFG,$DB;
		$assignElement = optional_param('assignElement',0, PARAM_RAW);
		if($assignElement == "program"){
			require_once($CFG->dirroot . '/program/programlib.php');
		}
                
                
                try{
		$assignTo = optional_param('assignTo',0, PARAM_RAW);
		$assignId = optional_param('assignId',0, PARAM_RAW);
		$elementList = optional_param('elementList',0, PARAM_RAW);
		$endDate = optional_param('end_date',0, PARAM_RAW);
                
                $cc_emails = optional_param('cc_emails','', PARAM_RAW);
                
		$update = optional_param('update','add', PARAM_RAW);
		$updateMail = 0;
		if($update == 'update'){
			$updateMail = 1;
		}
		$elementIdArr = explode(",",$elementList);
		if($endDate == ""){
			$outcome->response = "";
		}else{
			$endDate = strtotime($endDate);
			$outcome->response = date($CFG->customDefaultDateFormat,$endDate);
		}
		if($assignElement == 'user' && $assignTo == 'course'){
			$out = assignCourseToUser($assignId,$elementIdArr,1,$endDate,$updateMail,$cc_emails);
			$outcome->success = 'success';
		}elseif($assignElement == 'course' && $assignTo == 'user'){
			$courseIdArr = array($assignId);
                     
			foreach($elementIdArr as $elementId){
				$out = assignCourseToUser($elementId,$courseIdArr,1,$endDate,$updateMail,$cc_emails);
			}
			$outcome->success = 'success';
                        
		}elseif($assignElement == 'course' && $assignTo == 'team'){
			$courseIdArr = array($assignId);	
			foreach($elementIdArr as $elementId){
				$out = addCourseToGroup($elementId,$courseIdArr,$endDate,$updateMail);
			}
			$outcome->success = 'success';
		}elseif($assignElement == 'team' && $assignTo == 'course'){
			$out = addCourseToGroup($assignId,$elementIdArr,$endDate,$updateMail);
			$outcome->success = 'success';
		}elseif($assignElement == 'department' && $assignTo == 'course'){
			$out = assignCourseToDepartment($assignId,$elementIdArr);
			$departmentManagers = getUsersList(array($CFG->userTypeManager),$assignId);
			if(!empty($departmentManagers) && $CFG->courseenrolDeptMail == 1){
				$department = $DB->get_record('department', array('id'=>$assignId));
				foreach($departmentManagers as $manager){
					departmentEnrolMailToCourses($manager->id,$elementIdArr,$assignId,$department->title);
				}
			}else{
				add_to_log(1, 'department', 'add course to department', 'department/assigncourses.php?department='.$assignId, "add course to department", 0, $USER->id);
			}
			$outcome->success = 'success';
		}elseif($assignElement == 'course' && $assignTo == 'department'){
			$courseIdArr = array($assignId);
			foreach($elementIdArr as $department){
				$out = assignCourseToDepartment($department,$courseIdArr);
				$departmentManagers = getUsersList(array($CFG->userTypeManager),$department);
				if(!empty($departmentManagers) && $CFG->courseenrolDeptMail == 1){
					$departmentDetails = $DB->get_record('department', array('id'=>$department));
					foreach($departmentManagers as $manager){
						departmentEnrolMailToCourses($manager->id,$courseIdArr,$department,$departmentDetails->title);
					}
				}else{
					add_to_log(1, 'department', 'add course to department', 'department/assigncourses.php?department='.$department, "add course to department", 0, $USER->id);
				}
			}
			$outcome->success = 'success';
		}elseif($assignElement == 'program' && $assignTo == 'department'){
			foreach($elementIdArr as $department){
				$out = assignProgramToDepartment($assignId,$department);
				if($CFG->courseenrolDeptMail == 1){
					sendEnrolMailToDepartment($department,0,$assignId);
				}
			}
			$outcome->success = 'success';
		}elseif($assignElement == 'program' && $assignTo == 'team'){
			foreach($elementIdArr as $team){
				$out = assignProgramToGroup($assignId,$team,$endDate);
				if($CFG->courseenrolTeamMail == 1){
					sendEnrolMailToTeam($team,0,$assignId,$endDate,$updateMail);
				}
			}
			$outcome->success = 'success';
		}elseif($assignElement == 'program' && $assignTo == 'user'){
			foreach($elementIdArr as $programUser){
				$out = assignProgramToUser($assignId,$programUser,$endDate);
				courseEnrolMail(0,$programUser,0,0,$assignId,$endDate,$updateMail);
			}
			$outcome->success = 'success';
		}else{
			$outcome->success = 'error';
		}
                if(isset($SESSION->custom_email_content)){
                            $SESSION->custom_email_content = '';
                             unset($SESSION->custom_email_content);
                }
		break;
                
}catch(EXCEPTION $ex){ pr($ex);}
			
	case 'saveCourseUnEnrolmentData':
		GLOBAL $USER,$CFG,$DB;

		$assignElement = optional_param('assignElement',0, PARAM_RAW);
		if($assignElement == "program"){
			require_once($CFG->dirroot . '/program/programlib.php');
		}
		$assignTo = optional_param('assignTo',0, PARAM_RAW);
		$assignId = optional_param('assignId',0, PARAM_RAW);
		$elementList = optional_param('elementList',0, PARAM_RAW);
		$endDate = optional_param('end_date',0, PARAM_RAW);
		$remarksText = $_REQUEST['remarks_text'];

		$elementIdArr = explode(",",$elementList);
		$outcome->response = "";
		if($assignElement == 'user' && $assignTo == 'course'){
			foreach($elementIdArr as $elementId){
				$out = deactivateUserMappingData(0,$elementId,$assignId,'user',$assignId,$remarksText);
			}
			userCoursesUnenrolMail($elementIdArr,$assignId,$remarksText);
			$outcome->success = 'success';
		}elseif($assignElement == 'course' && $assignTo == 'user'){
			$courseIdArr = array($assignId);	
			foreach($elementIdArr as $elementId){
				$out = deactivateUserMappingData(0,$assignId,$elementId,'user',$elementId,$remarksText);
				userCoursesUnenrolMail($courseIdArr,$elementId,$remarksText);
			}
			$outcome->success = 'success';
		}elseif($assignElement == 'course' && $assignTo == 'team'){
			$courseIdArr = array($assignId);	
			foreach($elementIdArr as $elementId){
				$out = removeCourseFromGroup($elementId,$courseIdArr,$remarksText);
				if($CFG->courseenrolTeamMail == 1){
					sendCoursesUnEnrolMailToTeam($elementId,$courseIdArr,$remarksText);
				}
			}
			$outcome->success = 'success';
		}elseif($assignElement == 'team' && $assignTo == 'course'){
			$out = removeCourseFromGroup($assignId,$elementIdArr,$remarksText);
			if($CFG->courseenrolTeamMail == 1){
				sendCoursesUnEnrolMailToTeam($assignId,$elementIdArr,$remarksText);
			}
			$outcome->success = 'success';
		}elseif($assignElement == 'department' && $assignTo == 'course'){
			$out = removeCourseFromDepartment($assignId,$elementIdArr,$remarksText);
			$departmentManagers = getUsersList(array($CFG->userTypeManager),$assignId);
			if(!empty($departmentManagers) && $CFG->courseenrolDeptMail == 1){
				$departmentDetails = $DB->get_record('department', array('id'=>$assignId));
				foreach($departmentManagers as $manager){
					departmentCoursesUnenrolMail($elementIdArr,$manager->id,$department,$departmentDetails->title,$remarksText);
				}
			}else{
				add_to_log(1, 'department', 'add course to department', 'department/assigncourses.php?department='.$assignId, "add course to department", 0, $USER->id);
			}
			$outcome->success = 'success';
		}elseif($assignElement == 'course' && $assignTo == 'department'){
			$courseIdArr = array($assignId);
			foreach($elementIdArr as $department){
				$out = removeCourseFromDepartment($department,$courseIdArr,$remarksText);
				$departmentManagers = getUsersList(array($CFG->userTypeManager),$department);
				if(!empty($departmentManagers) && $CFG->courseenrolDeptMail == 1){
					$departmentDetails = $DB->get_record('department', array('id'=>$department));
					foreach($departmentManagers as $manager){
						departmentCoursesUnenrolMail($courseIdArr,$manager->id,$department,$departmentDetails->title,$remarksText);
					}
				}else{
					add_to_log(1, 'department', 'add course to department', 'department/assigncourses.php?department='.$department, "add course to department", 0, $USER->id);
				}
			}
			$outcome->success = 'success';
		}elseif($assignElement == 'program' && $assignTo == 'department'){
			foreach($elementIdArr as $department){
				$out = deactivateProgramDepartments($assignId, $department,$remarksText);
				if($CFG->courseenrolDeptMail == 1){
					sendUnEnrolMailToDepartment($department,0,$assignId,$remarksText);
				}
			}
			$outcome->success = 'success';
		}elseif($assignElement == 'program' && $assignTo == 'team'){
			foreach($elementIdArr as $team){
				$out = deactivateProgramGroups($assignId, $team,$remarksText);
				if($CFG->courseenrolTeamMail == 1){
					sendUnEnrolMailToTeam($team,0,$assignId,$remarksText);
				}
			}
			$outcome->success = 'success';
		}elseif($assignElement == 'program' && $assignTo == 'user'){
			foreach($elementIdArr as $programUser){
				$out = deactivateProgramUsers($assignId, $programUser,$remarksText);
				courseUnenrolMail(0,$programUser,0,0,$assignId,$remarksText);
			}
			$outcome->success = 'success';
		}else{
			$outcome->success = 'error';
		}
		break;
	case 'getCourseNClassroomListByDnT':

		$department = required_param('department', PARAM_RAW);
		$team = required_param('team', PARAM_RAW);	
        $program = required_param('program', PARAM_RAW);		
		$course = required_param('course', PARAM_RAW);
		$departmentArr = explode("@",$department);
		$teamArr = explode("@",$team);	
        $programArr = explode("@",$program);		
		$courseArr = explode("@",$course);
		$isReport = true;
		//$outcome = getCourseListByDnT($departmentArr, $teamArr, $courseArr);
		$outcome = getCourseNClassroomListByDnT($departmentArr, $teamArr, $programArr, $courseArr, $isReport );
		break;	
	case 'getManagerUsers':
		GLOBAL $CFG,$DB,$USER;
		$id = required_param('id', PARAM_RAW);
		$paramArray = Array('managers' => $id,'sel_mode' => 1,'is_active' => -1);
		$managersUser = get_users_listing('u.firstname', '', 1, 0, '', '', '','',$paramArray, '');
		if(!empty($managersUser)){
			echo '0';die;
		}
		echo '1';die;
		break;
		
	case 'getLearnerClasses':
		GLOBAL $DB,$CFG,$USER;
		$course = optional_param('courses',0, PARAM_RAW);		
		$classesSelected = optional_param('classes',0, PARAM_RAW);
		$userId = optional_param('userid',0, PARAM_INT);
		$clsCnt = optional_param('clsCnt',0, PARAM_INT);
		$courses = explode("@",$course);
		$classesSelected = explode("@",$classesSelected);
		$selectedC = in_array('-1', $classesSelected)?'selected="selected"':'';
		$select = '<option value="-1" '.$selectedC.' >'.get_string('allclasses','classroomreport').'</option>';
		$where = '';
		
		$classes = array();
		if($userId && $clsCnt > 0 ){
		
		  if(count($courses) > 0 && $courses[0] != '-1' ){ 
		    $courseIds = implode(",", $courses);
			$where .= " AND gce.id = '".$courseIds."'";
		  }
		  $classesOfClassroom = refineClassroomCourseData($userId, $where);
		 
		  $classes =  isset($classesOfClassroom['approved_class_details']['class']) && count($classesOfClassroom['approved_class_details']['class']) > 0 ?$classesOfClassroom['approved_class_details']['class']:array();

		}

		foreach($classes as $class){
			$selectedC = in_array($class['classId'], $classesSelected)?'selected="selected"':'';
			$select .= '<option value="'.$class['classId'].'" '.$selectedC.'>'.$class['orgClassName'].'</option>';
		}
		$outcome->success = true;
		$outcome->response = $select;
		break;		
	case 'getDepartmentParentTeams':
		GLOBAL $DB,$CFG,$USER;
		$deptId = optional_param('department',0, PARAM_RAW);
		$sel_mode = optional_param('sel_mode',0, PARAM_RAW);
		$searchParamArray = array();
		$searchParamArray['department'] = $deptId;
		$searchParamArray['sel_mode'] = $sel_mode;
		$teamList = getParentsListing($searchParamArray);
		$select = '<option value="-1" >'.get_string('select_all_parent').'</option>';
		if(!empty($teamList)){
			foreach($teamList as $team){
				$select .= '<option value="'.$team->id.'">'.$team->name.'</option>';
			}
		}
		$outcome->success = true;
		$outcome->response = $select;
		break;
	case 'saveProgramCriteria':
			GLOBAL $DB,$CFG,$USER;
			require_once($CFG->dirroot . '/program/programlib.php');
			$programId = $_REQUEST['programId'];
			$coursesData = json_decode($_REQUEST['elem']);
			resetProgramCoursePref($programId);
			$programCurrentOrder = $DB->get_records_sql("SELECT pc.courseid,pc.id FROM mdl_program_course as pc WHERE pc.is_active = 1 AND pc.programid = ".$programId);
			$record = array();
			try{
				if(!empty($programCurrentOrder) && !empty($coursesData)){
					foreach($coursesData as $programCourse){
						$DB->execute("UPDATE mdl_program_course SET course_order = ".$programCourse->order." WHERE programid = ".$programId." AND courseid = ".$programCourse->courseId);
						$courseId = $programCourse->courseId;
						$programCourses = $programCourse->courses;
						if(isset($programCurrentOrder[$courseId])){
							$mapId = $programCurrentOrder[$courseId]->id;
							if(!empty($programCourses)){
								foreach($programCourses as $pCourse){
									$courserec = new stdClass;
									$courserec->program_map_id = $mapId;
									$courserec->course_id = $pCourse;
									$DB->insert_record('program_course_criteria', $courserec);
								}
							}
						}
					}
				}
				$_SESSION['update_msg'] = get_string('record_updated');
				$outcome->success = true;
			}catch (Exception $e) {
				$_SESSION['update_msg'] = get_string('error_in_update');
				$outcome->success = false;
			    $outcome->error = true;
			}
			break;
		case 'resetProgramCriteria':
			GLOBAL $DB,$CFG,$USER;
			require_once($CFG->dirroot . '/program/programlib.php');
			$programId = $_REQUEST['programId'];
			resetProgramCoursePref($programId);
			$_SESSION['update_msg'] = get_string('program_reset_successfully');
			$outcome->success = true;
			break;
                case 'setValueInSession':
			GLOBAL $DB,$CFG,$USER,$SESSION;
                        $SESSION->custom_email_content='';
                       /* if(isset($SESSION->custom_email_content)){
                            unset($SESSION->custom_email_content);
                        }*/
			$custom_email_content = $_REQUEST['custom_email_content'];			
			$SESSION->custom_email_content = $custom_email_content;
			$outcome->success = true;
			break;
    default:
        throw new enrol_ajax_exception('unknowajaxaction');
}

echo json_encode($outcome);
die();