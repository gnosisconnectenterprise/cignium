<?php
//define('AJAX_SCRIPT', true);
require('../config.php');

$courseid = required_param('courseid', PARAM_INT);

$site = get_site();
$PAGE->set_url('/local/upload_csv_users.php', array('id' => $courseid));
$PAGE->set_pagelayout('classroompopup');
$PAGE->set_title("$site->fullname: $stradduserstogroup");
$PAGE->set_heading($site->fullname);

echo $OUTPUT->header();


$downloadLink =1;
        echo '<div class="borderBlockSpace">';
         //echo '<div><a href="#" id="closepopup">Close</a></div>';
        echo '<div class="uploaduserresults"></div>';
	echo "<div class = 'upload-user-div' style='display: block;'>";
		
		echo '<form name="upload_user" id="upload_user" method="POST" enctype="multipart/form-data">';
      
                echo '<div class="custom_email">
                    <label for="custom_email_content">Custom Email Content</label>
                    <span class="element customtxtarea">                    
                        <textarea rows="3" cols="100" name="custom_email_content" id="custom_email_content"></textarea>
                    </span>
                </div>';
                $placeholder = get_string('cc_emails_placeholder');
                echo '<div class="custom_email" style="display:none;">
                    <label for="custom_email_content">'.get_string('cc_emails', 'course').'</label>
                    <span class="element customtxtarea" style="padding-left:39px;">                    
                        <textarea rows="1" cols="100" name="cc_emails" id="cc_emails" placeholder="'.$placeholder.'"></textarea>
                    </span>
                </div>';
                
                echo '<div id="user_listOuter">
                    <input type = "file" name = "user_list" id = "user_list"></div>
					<input type = "hidden" name = "upload_type" value = "1">
                                        <input type = "hidden" name = "courseid" id = "courseid" value = "'.$courseid.'">
                                        <input type = "hidden" name = "action" id = "action" value = "csv_user_upload_into_course">
					<input type = "button" name = "upload" value = "upload" id = "upload-submit">';
                echo '<a href="'.$securewwwroot.'/local/download.php?download='.$downloadLink.'" class="download-sample custom_email-sample">'.get_string('download_sample').'</a>';
                echo '</form>';
	echo '</div>';
        echo '</div>';
        
        
echo $OUTPUT->footer();
?>
<script>
    var emaillistError=0;
    $('#upload-submit').click(function(){
        
                        checkEmails();
            
                        if(emaillistError==1){
                            return false;
                        }
                        
                        var cc_emails = $("#cc_emails").val();
			var filename = $("#user_list").val();
			if($.trim(filename) == ''){
				alert('Please select file');
				return false;
			}
                        var file_data = $('#user_list').prop('files')[0];
                        var action_data = $("#action").val();
                        var course_id = $("#courseid").val();

                        var form_data = new FormData();                  
                        form_data.append('user_list', file_data);
                        form_data.append('action', action_data);
                        form_data.append('courseid', course_id);
                        form_data.append('cc_emails', cc_emails);
                        var custom_email_content = $("#custom_email_content").val();
                        form_data.append('custom_email_content', custom_email_content);
                        $.ajax({
                                url: '<?php echo $CFG->wwwroot;?>/local/upload_users_into_course.php', // point to server-side PHP script 
                                contentType: false,
                                processData: false,
                                data: form_data,                         
                                type: 'post',
                                success: function(data){
                                    $("#user_list").val('');
                                    //alert(data); // display response from the PHP script, if any                                    
                                    $('.uploaduserresults').html(data);                                   
                                }
                        });
			//$("#assignform").submit();
		});
                $("#closepopup").click(function(){
                  parent.window.location.reload();
                   // parent.$.colorbox.close();                    
                });
               
                </script>
                
                <script language="javascript">
function checkEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;  

  return(regex.test(email));

}

function checkEmails(){
	var emails = document.getElementById("cc_emails").value;
       
       if(emails != ""){
           var emailArray = emails.split(",");
            var invEmails = "";
            for(i = 0; i <= (emailArray.length - 1); i++){
                    if(checkEmail(emailArray[i].trim())){
                            //Do what ever with the email.
                    }else{
                            invEmails += emailArray[i] + "\n";
                    }
            }
            if(invEmails != ""){
                    emaillistError=1;                   
                    alert("Invalid emails:\n" + invEmails);                
                    return false;
            }else{
                emaillistError=0;
            }
       }
	
        
        
}
</script>



