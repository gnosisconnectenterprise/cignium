<?php

require_once('../../config.php');
require_once($CFG->dirroot.'/calendar/event_form.php');
require_once($CFG->dirroot.'/calendar/lib.php');
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot.'/calendar/renderer.php');

$eventid = required_param('id', PARAM_INT);
$sesskey = required_param('sesskey', PARAM_RAW);
$confirm = true;
$repeats = false;
$courseid = optional_param('course', 0, PARAM_INT);




$event = calendar_event::load($eventid);

/**
 * We are going to be picky here, and require that any event types other than
 * group and site be associated with a course. This means any code that is using
 * custom event types (and there are a few) will need to associate thier event with
 * a course
 */
if ($event->eventtype !== 'user' && $event->eventtype !== 'site') {
    $courseid = $event->courseid;
}

$course = $DB->get_record('course', array('id'=>$courseid));

if (!$course) {
    $PAGE->set_context(context_system::instance()); //TODO: wrong
}

// Check the user has the required capabilities to edit an event
if (!calendar_edit_event_allowed($event)) {
    print_error('nopermissions');
}

// Count the repeats, do we need to consider the possibility of deleting repeats
$event->timedurationuntil = $event->timestart + $event->timeduration;
$event->count_repeats();

// If there are repeated events then add a Delete Repeated button
if (!empty($event->eventrepeats) && $event->eventrepeats > 0) {
    $repeats = true;
}

if ($confirm) {
	triggerEventMail($eventid,'delete');
    // Confirm the session key to stop CSRF
    if (!confirm_sesskey()) {
        print_error('confirmsesskeybad');
    }
    // Delete the event and possibly repeats
    $event->delete($repeats);
    echo "success";
    die();
}