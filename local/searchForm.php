<?php
global $CFG;
session_start();
?>
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/style/jquery-ui.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-ui.js"></script>

<?php 

//pr($columns);
$containfield = '';
$categoryfield = '';
$datefield = '';

if(count($columns) > 0 ){

	foreach($columns as $fieldname){
	
		 switch ($fieldname) { 
			case 'resoursename':    $containfield = 'mr.name';break;
			case 'fullname':    $containfield = 'mc.fullname';break;
			case 'category':     $categoryfield = 'mcc.id';break;
			case 'ctimecreated':    $datefield = 'mc.timecreated';break;
			case 'lastaccessed':    $datefield = 'mula.timeaccess';break;
			case 'timecreated':    $datefield = 'mc.timecreated';break;
		 }
	 
	}		
}
	

          $searchcontain = '';
		 $searchcategory = '';

     $startdate_day = date("d");
   $startdate_month = date("m");
    $startdate_year = date("Y");
   
       $enddate_day = date("d");
     $enddate_month = date("m");
      $enddate_year = date("Y");
	  
	  $startdatetime = 0;
	    $enddatetime = 0;
	  
if(isset($_POST['addfilter']) && !empty($_POST['addfilter'])){

   $extrasql  = ''; 

     
           $searchcontain = $_POST['contain'];
		  $searchcategory = $_POST['categoryname'];
     $startdate_day = $_POST['startdate_day'];
   $startdate_month = $_POST['startdate_month'];
    $startdate_year = $_POST['startdate_year'];
	 
	 if($searchcategory){
	   $extrasql .= " AND $categoryfield = '$searchcategory'";
	 }
	
   
       $enddate_day = $_POST['enddate_day'];
     $enddate_month = $_POST['enddate_month'];
      $enddate_year = $_POST['enddate_year'];

	$search_start_date = '';
	$search_end_date = '';
	$showMoreLessDateCheck = $_POST['showMoreLess'];
	
	if($containfield){
	   $extrasql .= " AND $containfield like '%$searchcontain%'";
	}
	
	if($showMoreLessDateCheck == 1){
		$search_start_date = $_POST['start_date'];
		$search_end_date = $_POST['end_date'];
		if($search_end_date != ''){
		
			$endDateTime = strtotime($search_end_date);
			
			if($datefield){
			  $extrasql .= " AND $datefield <= $endDateTime";
			}
		}
		if($search_start_date != ''){
		
		   $startDateTime = strtotime($search_start_date);
		   
		   if($datefield){
		     $extrasql .= " AND $datefield >= $startDateTime";
		   }
		}
	}
	
    $_SESSION['extrasql'] = $extrasql;
	

}elseif(isset($_POST['removeall']) && !empty($_POST['removeall'])){
}
$dis = '0';
$classShow = '';
if($search_start_date != '' || $search_end_date != '' || $searchcategory!=''){
	$dis = "1";
	$classShow = 'show';
}



?>

<form class="mform" id="mform2" accept-charset="utf-8" method="post" autocomplete="off">
  <div style="display: none;">
    <input type="hidden" value="<?php echo isset($_REQUEST['type'])?$_REQUEST['type']:1;?>" name="type" >
    <input type="hidden" value="<?php echo $dis; ?>" name="showMoreLess" id = "showMoreLess">
    <input type="hidden" value="<?php echo sesskey();?>" name="sesskey" >
  </div>
  <fieldset id="id_newfilter" class="clearfix collapsible containsadvancedelements">
  <legend class="ftoggler" ><a href="javascript:;" class="fheader" role="button" aria-controls="id_newfilter" aria-expanded="true"><?php echo get_string('newfilter','filters');?></a></legend>
  <div class="fcontainer clearfix" >
    <div class="fitem fitem_fgroup " id="fgroup_id_resoursename_grp">
      <div class="fitemtitle">
        <div class="fgrouplabel">
          <label><?php echo get_string('namecontain','search');?> </label>
        </div>
      </div>
      <fieldset class="felement fgroup">
      <label for="id_resoursename" class="accesshide">&nbsp;</label>
      <input type="text" id="id_contain" value="<?php echo $searchcontain;?>" name="contain">
      </fieldset>
    </div>
    <?php if($categoryfield){
	
	  $learnercourses_category = getLearnerCoursesCategory();
	
	?>
        <div aria-live="polite" class="fitem advanced fitem_fselect <?php echo $classShow; ?>" id="fitem_id_categoryname">
          <div class="fitemtitle">
            <label for="id_categoryname"><?php echo get_string('category','search');?></label>
          </div>
          <div class="felement fselect">
            <select id="id_categoryname" name="categoryname">
              <option selected="selected" value=""><?php echo get_string('anyvalue','search');?></option>
              <?php if(count($learnercourses_category)>0){
			  foreach($learnercourses_category as $lc){
			    $catid = $lc->id;
				$catname = $lc->name;
				$categoryselected = '';
				$categoryselected = $searchcategory == $catid?'selected="selected"':'';
			  ?>
              <option value="<?php echo $catid;?>"  <?php echo $categoryselected;?> ><?php echo $catname;?></option>
              <?php }} ?>
            </select>
          </div>
        </div>
	<?php } ?>
    <div class="fitem advanced fitem_fgroup <?php echo $classShow; ?>" >
      <div class="fitemtitle">
        <div class="fgrouplabel">
          <label><?php echo get_string('lastaccessed','search');?> </label>
        </div>
      </div>
	  <fieldset class="felement fgroup">
		<div class = "fromDate" style = "clear: both; height: 35px;">
			<?php echo get_string('from','search');?>
			<span class="fdate_selector">
				<input type="text" id="start_date" value="<?php echo $search_start_date;?>" name="start_date" readonly = "true">
			</span>
		</div>
		<div class = "tillDate" style = "clear: both; height: 35px;">
			<?php echo get_string('till','search');?>
			<span class="fdate_selector">
				<input type="text" id="end_date" value="<?php echo $search_end_date;?>" name="end_date" readonly = "true">
			</span>
		</div>
	  </fieldset>
     
    </div>
    <div class="fitem fitem_actionbuttons fitem_fsubmit" id="fitem_id_addfilter">
      <div class="felement fsubmit">
        <input type="submit" id="id_addfilter" value="<?php echo get_string('addfilter','filters');?>" name="addfilter">
      </div>
    </div>
    <div class="fitem fitem_actionbuttons fitem_fsubmit" id="fitem_id_removeall">
      <div class="felement fsubmit">
        <input type="submit" id="id_removeall" value="<?php echo get_string('removeall','filters');?>" name="removeall">
      </div>
    </div>
    <div class="fitem moreless-actions">
<?php if($dis == 1){?>
      <div class="felement"><a href="javascript:;" class="moreless-toggler moreless-less" ><?php echo get_string('showless','form');?></a></div>
<?php }else{
?>
	<div class="felement"><a href="javascript:;" class="moreless-toggler" ><?php echo get_string('showmore','form');?></a></div>
<?php 
}?>
    </div>
  </div>
  </fieldset>
</form>
<script>
    
jQuery(document).ready(function($) {
	$('#start_date').datepicker();
	$('#end_date').datepicker();
});
 
	 function doSetFilter(disabled, showhide){
	 
	     /* $('#startdate_day').attr('disabled',disabled);
		  $('#startdate_month').attr('disabled',disabled);
		  $('#startdate_year').attr('disabled',disabled);
		  
		  $('#enddate_day').attr('disabled',disabled);
		  $('#enddate_month').attr('disabled',disabled);
		  $('#enddate_year').attr('disabled',disabled);
		  */
		  if(showhide == true){
		  
		  
		         $('a.moreless-toggler').removeClass('moreless-less');
				 $('a.moreless-toggler').html('<?php echo get_string('showmore','form');?>');
				  $('.advanced').removeClass('show');
			 
	      }else{
		 
				  $('a.moreless-toggler').addClass('moreless-toggler moreless-less');
				  $('a.moreless-toggler').html('<?php echo get_string('showless','form');?>');
				  $('.advanced').addClass('fitem advanced fitem_fgroup show');
		  
		  }
		  
		  
		  
	 }
	 
	 
	 /*$(window).load(function () {
		 <?php if(isset($SESSION->learnercourse_filtering['categoryname']) || isset($SESSION->learnercourse_filtering[$datefield])){ ?>
			     doSetFilter(false, false);
		<?php }else{?>
		         //doSetFilter(true, true);
		<?php } ?>
		
	 });*/
	 
	 $(document).on('click', 'a.moreless-toggler', function(){
			  var showMoreLess = 0;
			  showMoreLess = $("#showMoreLess").val();
			  if(showMoreLess == 0){
				  $("#showMoreLess").val(1);
				  hasMoreless = false;
			  }else{
				$("#showMoreLess").val(0);
				hasMoreless = true;
			  }
			  //var hasMoreless = $(this).hasClass('moreless-less');
			   //alert(hasMoreless);
			  if(hasMoreless == true){
				 doSetFilter(true, true);
			  }else{
				 doSetFilter(false, false);
			  }
		
	});

	
	
	
    </script>
