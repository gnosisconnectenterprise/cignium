<?php
require_once('../config.php');
require_once($CFG->dirroot . '/local/user/selector/lib.php');


$searchText = optional_param('search_text', '', PARAM_RAW);
$action = optional_param('action', 0, PARAM_ALPHA);

$startDate = optional_param('start_date', '', PARAM_RAW);
$endDate = optional_param('end_date', '', PARAM_RAW);
 
 
if($action == "searchCourseNonDepartment"){
	$courseid = optional_param('courseid',0, PARAM_INT);
	$courses = new courses_assignment_selector($searchText, array('courseid' => $courseid));
	$courses->courseNonDepartmentList();
	$courses->course_option_list('non_department_course');
	die;
}elseif($action == "searchCourseDepartment"){
	$courseid = optional_param('courseid',0, PARAM_INT);
	$courses = new courses_assignment_selector($searchText, array('courseid' => $courseid));
	$courses->courseDepartmentList();
	$courses->course_option_list('department_course');
	die;
}elseif($action == "searchCourseNonTeam"){
	$courseid = optional_param('courseid',0, PARAM_INT);
	$courses = new courses_assignment_selector($searchText, array('courseid' => $courseid));
	$courses->courseNonTeamList();
	$courses->course_option_list('non_team_course');
	die;
}elseif($action == "searchCourseTeam"){
	$courseid = optional_param('courseid',0, PARAM_INT);
	$courses = new courses_assignment_selector($searchText, array('courseid' => $courseid));
	$courses->courseTeamList();
	$courses->course_option_list('team_course');
	die;
}elseif($action == "searchCourseNonUser"){
	$courseid = optional_param('courseid',0, PARAM_INT);
	$sel_mode = optional_param('sel_mode', 1, PARAM_RAW);
	$user_group = optional_param('user_group', '', PARAM_RAW);
	$department = optional_param('department', '', PARAM_RAW);
        $team =  optional_param('team', '', PARAM_RAW);
	$managers = optional_param('managers', '', PARAM_RAW);
	$roles = optional_param('roles', '', PARAM_RAW);
	$job_title = optional_param('job_title', '', PARAM_RAW);
        $report_to = optional_param('report_to', '', PARAM_RAW);
	$company = optional_param('company', '', PARAM_RAW);
        
        $country = optional_param('country', '', PARAM_RAW);//add country filter on 15th feb 2016
        $is_manager_yes = optional_param('is_manager_yes', '', PARAM_RAW);
	
	$courses = new courses_assignment_selector($searchText, array('courseid' => $courseid, 'selMode' => $sel_mode, 'userGroup' => $user_group, 'department' => $department, 'team' => $team, 'managers' => $managers, 'roles' => $roles, 'job_title' => $job_title, 'report_to' => $report_to, 'company' => $company,'country' => $country,'startdate' => $startDate,'enddate' => $endDate,'is_manager_yes'=>$is_manager_yes));
	$courses->courseNonUserList();
	$courses->course_option_list('non_user_course');
	die;
}elseif($action == "courseUserList"){
	$courseid = optional_param('courseid',0, PARAM_INT);
	$sel_mode = optional_param('sel_mode', 1, PARAM_RAW);
	$user_group = optional_param('user_group', '', PARAM_RAW);
	$department = optional_param('department', '', PARAM_RAW);
    $team =  optional_param('team', '', PARAM_RAW);
	$managers = optional_param('managers', '', PARAM_RAW);
	$roles = optional_param('roles', '', PARAM_RAW);
	$job_title = optional_param('job_title', '', PARAM_RAW);
        $report_to = optional_param('report_to', '', PARAM_RAW);
	$company = optional_param('company', '', PARAM_RAW);
        
        //code add country filter on 15th feb 2016
        $country = optional_param('country', '', PARAM_RAW);
	$is_manager_yes = optional_param('is_manager_yes', '', PARAM_RAW);
        
        //code edit for aadd country filter on 15th feb 2016
	$courses = new courses_assignment_selector($searchText, array('courseid' => $courseid, 'selMode' => $sel_mode, 'userGroup' => $user_group, 'department' => $department, 'team' => $team, 'managers' => $managers, 'roles' => $roles, 'job_title' => $job_title, 'report_to' => $report_to, 'company' => $company,'country'=>$country,'is_manager_yes'=>$is_manager_yes ));
	$courses->courseUserList();
	$courses->course_option_list('user_course');
	die;
}elseif($action == "searchProgramNonDepartment"){
	$programid = optional_param('programid',0, PARAM_INT);
	$courses = new program_assignment_selector($searchText, array('programid' => $programid));
	$courses->programNonDepartmentList();
	$courses->program_option_list('non_department_program');
	die;
}elseif($action == "searchProgramDepartment"){
	$programid = optional_param('programid',0, PARAM_INT);
	$courses = new program_assignment_selector($searchText, array('programid' => $programid));
	$courses->programDepartmentList();
	$courses->program_option_list('department_program');
	die;
}elseif($action == "searchProgramNonTeam"){
	$programid = optional_param('programid',0, PARAM_INT);
	$courses = new program_assignment_selector($searchText, array('programid' => $programid));
	$courses->programNonTeamList();
	$courses->program_option_list('non_team_program');
	die;
}elseif($action == "searchProgramTeam"){
	$programid = optional_param('programid',0, PARAM_INT);
	$courses = new program_assignment_selector($searchText, array('programid' => $programid));
	$courses->programTeamList();
	$courses->program_option_list('team_program');
	die;
}elseif($action == "searchProgramNonUser"){
	$programid = optional_param('programid',0, PARAM_INT);
	$sel_mode = optional_param('sel_mode', 1, PARAM_RAW);
	$user_group = optional_param('user_group', '', PARAM_RAW);
	$department = optional_param('department', '', PARAM_RAW);
    $team =  optional_param('team', '', PARAM_RAW);
	$managers = optional_param('managers', '', PARAM_RAW);
	$roles = optional_param('roles', '', PARAM_RAW);
	$job_title = optional_param('job_title', '', PARAM_RAW);
	$company = optional_param('company', '', PARAM_RAW);
        
        $country = optional_param('country', '', PARAM_RAW); //code added to add country filter on 15th feb 2016
	//echo $country;
        
        //code edit to add country filter on 15th feb 2016
        $courses = new program_assignment_selector($searchText, array('programid' => $programid, 'selMode' => $sel_mode, 'userGroup' => $user_group, 'department' => $department, 'team' => $team, 'managers' => $managers, 'roles' => $roles, 'job_title' => $job_title, 'company' => $company, 'country'=>$country ));
	$courses->programNonUserList();
	$courses->program_option_list('non_user_program');
	die;
}elseif($action == "searchProgramUser"){
	$programid = optional_param('programid',0, PARAM_INT);
	$sel_mode = optional_param('sel_mode', 1, PARAM_RAW);
	$user_group = optional_param('user_group', '', PARAM_RAW);
	$department = optional_param('department', '', PARAM_RAW);
    $team =  optional_param('team', '', PARAM_RAW);
	$managers = optional_param('managers', '', PARAM_RAW);
	$roles = optional_param('roles', '', PARAM_RAW);
	$job_title = optional_param('job_title', '', PARAM_RAW);
	$company = optional_param('company', '', PARAM_RAW);
        
         $country = optional_param('country', '', PARAM_RAW); //code added to add country filter on 15th feb 2016
	//echo $country;
         
         //code edit to add country fiter on 15th feb 2016
	$courses = new program_assignment_selector($searchText, array('programid' => $programid, 'selMode' => $sel_mode, 'userGroup' => $user_group, 'department' => $department, 'team' => $team, 'managers' => $managers, 'roles' => $roles, 'job_title' => $job_title, 'company' => $company, 'country'=>$country ));
	$courses->programUserList();
	$courses->program_option_list('user_program');
	die;
}elseif($action == "searchOpenCourseNonUser"){
    require_once($CFG->dirroot.'/mod/scheduler/lib.php');
	$courseid = optional_param('courseid',0, PARAM_INT);
	$sel_mode = optional_param('sel_mode', 1, PARAM_RAW);
	$user_group = optional_param('user_group', '', PARAM_RAW);
	$department = optional_param('department', '', PARAM_RAW);
    $team =  optional_param('team', '', PARAM_RAW);
	$managers = optional_param('managers', '', PARAM_RAW);
	$roles = optional_param('roles', '', PARAM_RAW);
	$job_title = optional_param('job_title', '', PARAM_RAW);
	$company = optional_param('company', '', PARAM_RAW);
        
        //Code added to add country filter, on 16th feb 2016
        $country = optional_param('country', '', PARAM_RAW);
	
        //Code edit to add country filer, on 16th feb 2016
	$courses = new class_assignment_selector($searchText, array('courseid' => $courseid, 'selMode' => $sel_mode, 'userGroup' => $user_group, 'department' => $department, 'team' => $team, 'managers' => $managers, 'roles' => $roles, 'job_title' => $job_title, 'company' => $company,'country'=>$country ));
	
	$courses->opencourseUserList();
	$courses->opencourseNonUserList();
	$courses->course_option_list('non_user_course');
	die;
}elseif($action == "courseOpenUserList"){
    require_once($CFG->dirroot.'/mod/scheduler/lib.php');
	$courseid = optional_param('courseid',0, PARAM_INT);
	$sel_mode = optional_param('sel_mode', 1, PARAM_RAW);
	$user_group = optional_param('user_group', '', PARAM_RAW);
	$department = optional_param('department', '', PARAM_RAW);
    $team =  optional_param('team', '', PARAM_RAW);
	$managers = optional_param('managers', '', PARAM_RAW);
	$roles = optional_param('roles', '', PARAM_RAW);
	$job_title = optional_param('job_title', '', PARAM_RAW);
	$company = optional_param('company', '', PARAM_RAW);
        
        //Code added to add country filter, on 16th feb 2016
        $country = optional_param('country', '', PARAM_RAW);
        
        //Code edited to add country filter, on 16th feb 2016
	$courses = new class_assignment_selector($searchText, array('courseid' => $courseid, 'selMode' => $sel_mode, 'userGroup' => $user_group, 'department' => $department, 'team' => $team, 'managers' => $managers, 'roles' => $roles, 'job_title' => $job_title, 'company' => $company, 'country'=>$country ));
	$courses->opencourseUserList();
	$courses->course_option_list('user_course');
	die;
}elseif($action == "searchOpenNonDepartment"){
    require_once($CFG->dirroot.'/mod/scheduler/lib.php');
	$courseid = optional_param('courseid',0, PARAM_INT);
	$courses = new class_assignment_selector($searchText, array('courseid' => $courseid));
	$courses->courseDepartmentList();
	$courses->courseNonDepartmentList();
	$courses->course_option_list('non_department_course');
	die;
}elseif($action == "searchOpenDepartment"){
    require_once($CFG->dirroot.'/mod/scheduler/lib.php');
	$courseid = optional_param('courseid',0, PARAM_INT);
	$courses = new class_assignment_selector($searchText, array('courseid' => $courseid));
	$courses->courseDepartmentList();
	$courses->course_option_list('department_course');
	die;
}elseif($action == "searchOpenNonTeam"){
    require_once($CFG->dirroot.'/mod/scheduler/lib.php');
	$courseid = optional_param('courseid',0, PARAM_INT);
	$courses = new class_assignment_selector($searchText, array('courseid' => $courseid));
	$courses->courseTeamList();
	$courses->courseNonTeamList();
	$courses->course_option_list('non_team_course');
	die;
}elseif($action == "searchOpenTeam"){
    require_once($CFG->dirroot.'/mod/scheduler/lib.php');
	$courseid = optional_param('courseid',0, PARAM_INT);
	$courses = new class_assignment_selector($searchText, array('courseid' => $courseid));
	$courses->courseTeamList();
	$courses->course_option_list('team_course');
	die;
}
?>