<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Code for ajax user selectors.
 *
 * @package   user
 * @copyright 1999 onwards Martin Dougiamas  http://dougiamas.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * The default size of a user selector.
 */
define('USER_SELECTOR_DEFAULT_ROWS', 20);

/**
 * Base class for user selectors.
 *
 * In your theme, you must give each user-selector a defined width. If the
 * user selector has name="myid", then the div myid_wrapper must have a width
 * specified.
 */
abstract class user_selector_base {
    /** @var string The control name (and id) in the HTML. */
    protected $name;
    /** @var array Extra fields to search on and return in addition to firstname and lastname. */
    protected $extrafields;
    /** @var object Context used for capability checks regarding this selector (does
     * not necessarily restrict user list) */
    protected $accesscontext;
    /** @var boolean Whether the conrol should allow selection of many users, or just one. */
    protected $multiselect = true;
    /** @var int The height this control should have, in rows. */
    protected $rows = USER_SELECTOR_DEFAULT_ROWS;
    /** @var array A list of userids that should not be returned by this control. */
    protected $exclude = array();
    /** @var array|null A list of the users who are selected. */
    protected $selected = null;
    /** @var boolean When the search changes, do we keep previously selected options that do
     * not match the new search term? */
    protected $preserveselected = false;
    /** @var boolean If only one user matches the search, should we select them automatically. */
    protected $autoselectunique = false;
    /** @var boolean When searching, do we only match the starts of fields (better performance)
     * or do we match occurrences anywhere? */
    protected $searchanywhere = false;
    /** @var mixed This is used by get selected users */
    protected $validatinguserids = null;

    /**  @var boolean Used to ensure we only output the search options for one user selector on
     * each page. */
    private static $searchoptionsoutput = false;

    /** @var array JavaScript YUI3 Module definition */
    protected static $jsmodule = array(
                'name' => 'user_selector',
                'fullpath' => '/user/selector/module.js',
                'requires'  => array('node', 'event-custom', 'datasource', 'json', 'moodle-core-notification'),
                'strings' => array(
                    array('previouslyselectedusers', 'moodle', '%%SEARCHTERM%%'),
                    array('nomatchingusers', 'moodle', '%%SEARCHTERM%%'),
                    array('none', 'moodle')
                ));

    /** @var int this is used to define maximum number of users visible in list */
    public $maxusersperpage = 100;

    // Public API ==============================================================

    /**
     * Constructor. Each subclass must have a constructor with this signature.
     *
     * @param string $name the control name/id for use in the HTML.
     * @param array $options other options needed to construct this selector.
     * You must be able to clone a userselector by doing new get_class($us)($us->get_name(), $us->get_options());
     */
    public function __construct($name, $options = array()) {  
        global $CFG, $PAGE;

        // Initialise member variables from constructor arguments.
        $this->name = $name;

        // Use specified context for permission checks, system context if not
        // specified
         $this->accesscontext = context_system::instance();
  
        if (isset($options['extrafields'])) {
            $this->extrafields = $options['extrafields'];
        } else if (!empty($CFG->showuseridentity) &&
                has_capability('moodle/site:viewuseridentity', $this->accesscontext)) {
            $this->extrafields = explode(',', $CFG->showuseridentity);
        } else {
            $this->extrafields = array();
        }
        if (isset($options['exclude']) && is_array($options['exclude'])) {
            $this->exclude = $options['exclude'];
        }
        if (isset($options['multiselect'])) {
            $this->multiselect = $options['multiselect'];
        }

        // Read the user prefs / optional_params that we use.
        $this->preserveselected = $this->initialise_option('userselector_preserveselected', $this->preserveselected);
        $this->autoselectunique = $this->initialise_option('userselector_autoselectunique', $this->autoselectunique);
        $this->searchanywhere = $this->initialise_option('userselector_searchanywhere', $this->searchanywhere);

        if (!empty($CFG->maxusersperpage)) {
            $this->maxusersperpage = $CFG->maxusersperpage;
        }
    }

    /**
     * All to the list of user ids that this control will not select. For example,
     * on the role assign page, we do not list the users who already have the role
     * in question.
     *
     * @param array $arrayofuserids the user ids to exclude.
     */
    public function exclude($arrayofuserids) {
        $this->exclude = array_unique(array_merge($this->exclude, $arrayofuserids));
    }

    /**
     * Clear the list of excluded user ids.
     */
    public function clear_exclusions() {
        $this->exclude = array();
    }

    /**
     * @return array the list of user ids that this control will not select.
     */
    public function get_exclusions() {
        return clone($this->exclude);
    }

    /**
     * @return array of user objects. The users that were selected. This is a more sophisticated version
     * of optional_param($this->name, array(), PARAM_INT) that validates the
     * returned list of ids against the rules for this user selector.
     */
    public function get_selected_users() {
        // Do a lazy load.
        if (is_null($this->selected)) {
            $this->selected = $this->load_selected_users();
        }
		
        return $this->selected;
    }

    /**
     * Convenience method for when multiselect is false (throws an exception if not).
     * @return object the selected user object, or null if none.
     */
    public function get_selected_user() {
        if ($this->multiselect) {
            throw new moodle_exception('cannotcallusgetselecteduser');
        }
        $users = $this->get_selected_users();
		
        if (count($users) == 1) {
            return reset($users);
        } else if (count($users) == 0) {
            return null;
        } else {
            throw new moodle_exception('userselectortoomany');
        }
    }

    /**
     * If you update the database in such a way that it is likely to change the
     * list of users that this component is allowed to select from, then you
     * must call this method. For example, on the role assign page, after you have
     * assigned some roles to some users, you should call this.
     */
    public function invalidate_selected_users() {
        $this->selected = null;
    }

    /**
     * Output this user_selector as HTML.
     * @param boolean $return if true, return the HTML as a string instead of outputting it.
     * @return mixed if $return is true, returns the HTML as a string, otherwise returns nothing.
     */
    public function display($return = false) {
        global $PAGE;

        // Get the list of requested users.
        $search = optional_param($this->name . '_searchtext', '', PARAM_RAW);
        if (optional_param($this->name . '_clearbutton', false, PARAM_BOOL)) {
            $search = '';
        }
		
        $groupedusers = $this->find_users($search);
		ksort($groupedusers,SORT_DESC);
        // Output the select.
        $name = $this->name;
        $multiselect = '';
        if ($this->multiselect) {
            $name .= '[]';
            $multiselect = 'multiple="multiple" ';
        }
		
        $output = '<div class="userselector" id="' . $this->name . '_wrapper">' . "\n" .
                '<select name="' . $name . '" id="' . $this->name . '" ' .
                $multiselect . 'size="' . $this->rows . '">' . "\n";

        // Populate the select.
        $output .= $this->output_options($groupedusers, $search);

        // Output the search controls.
       /* $output .= "</select>\n<div class = 'searchBoxDiv'>\n";
        $output .= '<input type="text" name="' . $this->name . '_searchtext" id="' .
                $this->name . '_searchtext" size="15" value="' . s($search) . '" />';
        $output .= '<input type="submit" name="' . $this->name . '_searchbutton" id="' .
                $this->name . '_searchbutton" value="' . $this->search_button_caption() . '" />';
        $output .= '<input type="submit" name="' . $this->name . '_clearbutton" id="' .
                $this->name . '_clearbutton" value="' . get_string('clear') . '" />';*/
				
		$output .= "</select>\n<div class = 'searchBoxDiv'>\n";
        $output .= ' <div class="search-input" >
						<input type="text" value="" placeholder="Search" id="' .
                $this->name . '_searchtext" name="' . $this->name . '_searchtext" >
					</div>
					<div class="search_clear_button">
						<input type="button" name="' . $this->name . '_searchbutton" title="Search" id="' . $this->name . '_searchbutton" value="Search" name="search">
						<a title="Clear" id="' .$this->name . '_clearbutton"  href="javascript:void(0);">Clear</a>
					</div>';

        // And the search options.
        $optionsoutput = false;
        if (!user_selector_base::$searchoptionsoutput) {
            $output .= print_collapsible_region_start('', 'userselector_options',
                    get_string('searchoptions'), 'userselector_optionscollapsed', true, true);
            $output .= $this->option_checkbox('preserveselected', $this->preserveselected, get_string('userselectorpreserveselected'));
            $output .= $this->option_checkbox('autoselectunique', $this->autoselectunique, get_string('userselectorautoselectunique'));
            $output .= $this->option_checkbox('searchanywhere', $this->searchanywhere, get_string('userselectorsearchanywhere'));
            $output .= print_collapsible_region_end(true);

            $PAGE->requires->js_init_call('M.core_user.init_user_selector_options_tracker', array(), false, self::$jsmodule);
            user_selector_base::$searchoptionsoutput = true;
        }
        $output .= "</div>\n</div>\n\n";
        $output .= "<style> #userselector_options { display:none} </style>";
        // Initialise the ajax functionality.
        $output .= $this->initialise_javascript($search);

        // Return or output it.
        if ($return) {
            return $output;
        } else {
            echo $output;
        }
		
		
    }

    /**
     * The height this control will be displayed, in rows.
     *
     * @param integer $numrows the desired height.
     */
    public function set_rows($numrows) {
        $this->rows = $numrows;
    }

    /**
     * @return integer the height this control will be displayed, in rows.
     */
    public function get_rows() {
        return $this->rows;
    }

    /**
     * Whether this control will allow selection of many, or just one user.
     *
     * @param boolean $multiselect true = allow multiple selection.
     */
    public function set_multiselect($multiselect) {
        $this->multiselect = $multiselect;
    }

    /**
     * @return boolean whether this control will allow selection of more than one user.
     */
    public function is_multiselect() {
        return $this->multiselect;
    }

    /**
     * @return string the id/name that this control will have in the HTML.
     */
    public function get_name() {
        return $this->name;
    }

    /**
     * Set the user fields that are displayed in the selector in addition to the
     * user's name.
     *
     * @param array $fields a list of field names that exist in the user table.
     */
    public function set_extra_fields($fields) {
        $this->extrafields = $fields;
    }

    // API for sublasses =======================================================

    /**
     * Search the database for users matching the $search string, and any other
     * conditions that apply. The SQL for testing whether a user matches the
     * search string should be obtained by calling the search_sql method.
     *
     * This method is used both when getting the list of choices to display to
     * the user, and also when validating a list of users that was selected.
     *
     * When preparing a list of users to choose from ($this->is_validating()
     * return false) you should probably have an maximum number of users you will
     * return, and if more users than this match your search, you should instead
     * return a message generated by the too_many_results() method. However, you
     * should not do this when validating.
     *
     * If you are writing a new user_selector subclass, I strongly recommend you
     * look at some of the subclasses later in this file and in admin/roles/lib.php.
     * They should help you see exactly what you have to do.
     *
     * @param string $search the search string.
     * @return array An array of arrays of users. The array keys of the outer
     *      array should be the string names of optgroups. The keys of the inner
     *      arrays should be userids, and the values should be user objects
     *      containing at least the list of fields returned by the method
     *      required_fields_sql(). If a user object has a ->disabled property
     *      that is true, then that option will be displayed greyed out, and
     *      will not be returned by get_selected_users.
     */
    public abstract function find_users($search);

    /**
     *
     * Note: this function must be implemented if you use the search ajax field
     *       (e.g. set $options['file'] = '/admin/filecontainingyourclass.php';)
     * @return array the options needed to recreate this user_selector.
     */
    protected function get_options() {
        return array(
            'class' => get_class($this),
            'name' => $this->name,
            'exclude' => $this->exclude,
            'extrafields' => $this->extrafields,
            'multiselect' => $this->multiselect,
            'accesscontext' => $this->accesscontext,
        );
    }

    // Inner workings ==========================================================

    /**
     * @return boolean if true, we are validating a list of selected users,
     *      rather than preparing a list of uesrs to choose from.
     */
    protected function is_validating() {
        return !is_null($this->validatinguserids);
    }

    /**
     * Get the list of users that were selected by doing optional_param then
     * validating the result.
     *
     * @return array of user objects.
     */
    protected function load_selected_users() {
        // See if we got anything.
        if ($this->multiselect) {
            $userids = optional_param_array($this->name, array(), PARAM_INT);
        } else if ($userid = optional_param($this->name, 0, PARAM_INT)) {
            $userids = array($userid);
        }
		
        // If there are no users there is nobody to load
        if (empty($userids)) {
            return array();
        }

        // If we did, use the find_users method to validate the ids.
        $this->validatinguserids = $userids;
        $groupedusers = $this->find_users('');
		
        $this->validatinguserids = null;

        // Aggregate the resulting list back into a single one.
        $users = array();
        foreach ($groupedusers as $group) {
            foreach ($group as $user) {
                if (!isset($users[$user->id]) && empty($user->disabled) && in_array($user->id, $userids)) {
                    $users[$user->id] = $user;
                }
            }
        }

        // If we are only supposed to be selecting a single user, make sure we do.
        if (!$this->multiselect && count($users) > 1) {
            $users = array_slice($users, 0, 1);
        }

        return $users;
    }

    /**
     * @param string $u the table alias for the user table in the query being
     *      built. May be ''.
     * @return string fragment of SQL to go in the select list of the query.
     */
    protected function required_fields_sql($u) {
        // Raw list of fields.
        $fields = array('id');
        // Add additional name fields
		
        $fields = array_merge($fields, get_all_user_name_fields(), $this->extrafields);

        // Prepend the table alias.
        if ($u) {
            foreach ($fields as &$field) {
                $field = $u . '.' . $field;
            }
        }
        return implode(',', $fields);
    }

    /**
     * @param string $search the text to search for.
     * @param string $u the table alias for the user table in the query being
     *      built. May be ''.
     * @return array an array with two elements, a fragment of SQL to go in the
     *      where clause the query, and an array containing any required parameters.
     *      this uses ? style placeholders.
     */
    protected function search_sql($search, $u) {
        return users_search_sql($search, $u, $this->searchanywhere, $this->extrafields,
                $this->exclude, $this->validatinguserids);
    }

    /**
     * Used to generate a nice message when there are too many users to show.
     * The message includes the number of users that currently match, and the
     * text of the message depends on whether the search term is non-blank.
     *
     * @param string $search the search term, as passed in to the find users method.
     * @param int $count the number of users that currently match.
     * @return array in the right format to return from the find_users method.
     */
    protected function too_many_results($search, $count) {
        if ($search) {
            $a = new stdClass;
            $a->count = $count;
            $a->search = $search;
            return array(get_string('toomanyusersmatchsearch', '', $a) => array(),
                    get_string('pleasesearchmore') => array());
        } else {
            return array(get_string('toomanyuserstoshow', '', $count) => array(),
                    get_string('pleaseusesearch') => array());
        }
    }

    /**
     * Output the list of <optgroup>s and <options>s that go inside the select.
     * This method should do the same as the JavaScript method
     * user_selector.prototype.handle_response.
     *
     * @param array $groupedusers an array, as returned by find_users.
     * @return string HTML code.
     */
    protected function output_options($groupedusers, $search) {
    	
        $output = '';

        // Ensure that the list of previously selected users is up to date.
        $this->get_selected_users();

        // If $groupedusers is empty, make a 'no matching users' group. If there is
        // only one selected user, set a flag to select them if that option is turned on.
        $select = false;
        if (empty($groupedusers)) {	
            if (!empty($search)) {
                $groupedusers = array(get_string('nomatchingusers', '', $search) => array());
            } else {
                $groupedusers = array(get_string('none') => array());
            }
        } else if ($this->autoselectunique && count($groupedusers) == 1 &&
                count(reset($groupedusers)) == 1) {
            $select = true;
            if (!$this->multiselect) {
                $this->selected = array();
            }
        }

        // Output each optgroup.
        foreach ($groupedusers as $groupname => $users) {
            $output .= $this->output_optgroup($groupname, $users, $select);
        }

        // If there were previously selected users who do not match the search, show them too.
        if ($this->preserveselected && !empty($this->selected)) {
            $output .= $this->output_optgroup(get_string('previouslyselectedusers', '', $search), $this->selected, true);
        }

        // This method trashes $this->selected, so clear the cache so it is
        // rebuilt before anyone tried to use it again.
        $this->selected = null;

        return $output;
    }

    /**
     * Output one particular optgroup. Used by the preceding function output_options.
     *
     * @param string $groupname the label for this optgroup.
     * @param array $users the users to put in this optgroup.
     * @param boolean $select if true, select the users in this group.
     * @return string HTML code.
     */
    protected function output_optgroup($groupname, $users, $select) {
		GLOBAL $DB,$USER,$CFG;
        if (!empty($users)) {
            $output = '  <optgroup label="' . ucfirst($groupname) . ' (' . count($users) . ')">' . "\n";
            foreach ($users as $user) {
                $attributes = '';
				if($this->name == "removeselect" AND $USER->archetype != $CFG->userTypeAdmin && $this->isGlobal == 1 && $user->createdby != $USER->id){
					$attributes .= ' disabled="disabled"';
				}
                if (!empty($user->disabled)) {
                    $attributes .= ' disabled="disabled"';
                } else if ($select || isset($this->selected[$user->id])) {
                    $attributes .= ' selected="selected"';
                }
                unset($this->selected[$user->id]);
                $output .= '    <option' . $attributes . ' value="' . $user->id . '">' .
                        $this->output_user($user) . "</option>\n";
                if (!empty($user->infobelow)) {
                    // 'Poor man's indent' here is because CSS styles do not work
                    // in select options, except in Firefox.
                    $output .= '    <option disabled="disabled" class="userselector-infobelow">' .
                            '&nbsp;&nbsp;&nbsp;&nbsp;' . s($user->infobelow) . '</option>';
                }
            }
        } else {
            $output = '  <optgroup label="' . htmlspecialchars($groupname) . '">' . "\n";
            $output .= '    <option disabled="disabled">&nbsp;</option>' . "\n";
        }
        $output .= "  </optgroup>\n";
        return $output;
    }

    /**
     * Convert a user object to a string suitable for displaying as an option in the list box.
     *
     * @param object $user the user to display.
     * @return string a string representation of the user.
     */
    public function output_user($user) {
        $out = fullname($user);

        if ($this->extrafields) {
            $displayfields = array();
            foreach ($this->extrafields as $field) {
                $displayfields[] = $user->{$field};
            }
            $out .= ' (' . implode(', ', $displayfields) . ')';
        }
        return $out;
    }

    /**
     * @return string the caption for the search button.
     */
    protected function search_button_caption() {
        return get_string('search');
    }

    // Initialise one of the option checkboxes, either from
    // the request, or failing that from the user_preferences table, or
    // finally from the given default.
    private function initialise_option($name, $default) {
        $param = optional_param($name, null, PARAM_BOOL);
        if (is_null($param)) {
            return get_user_preferences($name, $default);
        } else {
            set_user_preference($name, $param);
            return $param;
        }
    }

    // Output one of the options checkboxes.
    private function option_checkbox($name, $on, $label) {
        if ($on) {
            $checked = ' checked="checked"';
        } else {
            $checked = '';
        }
        $name = 'userselector_' . $name;
        $output = '<p><input type="hidden" name="' . $name . '" value="0" />' .
                // For the benefit of brain-dead IE, the id must be different from the name of the hidden form field above.
                // It seems that document.getElementById('frog') in IE will return and element with name="frog".
                '<input type="checkbox" id="' . $name . 'id" name="' . $name . '" value="1"' . $checked . ' /> ' .
                '<label for="' . $name . 'id">' . $label . "</label></p>\n";
        user_preference_allow_ajax_update($name, PARAM_BOOL);
        return $output;
    }

    /**
     * @param boolean $optiontracker if true, initialise JavaScript for updating the user prefs.
     * @return any HTML needed here.
     */
    protected function initialise_javascript($search) {
        global $USER, $PAGE, $OUTPUT;
        $output = '';

        // Put the options into the session, to allow search.php to respond to the ajax requests.
        $options = $this->get_options();
        $hash = md5(serialize($options));
        $USER->userselectors[$hash] = $options;

        // Initialise the selector.
        $PAGE->requires->js_init_call('M.core_user.init_user_selector', array($this->name, $hash, $this->extrafields, $search), false, self::$jsmodule);
        return $output;
    }
}

// User selectors for managing group members ==================================

/**
 * Base class to avoid duplicating code.
 */
abstract class groups_user_selector_base extends user_selector_base {
    protected $groupid;
    protected $courseid;
    protected $isGlobal;

    /**
     * @param string $name control name
     * @param array $options should have two elements with keys groupid and courseid.
     */
    public function __construct($name, $options) {  

        global $CFG,$DB;
		
        parent::__construct($name, $options);
        $this->groupid = $options['groupid'];
        $this->courseid = $options['courseid'];
		$isGlobalArray = $DB->get_record_sql("SELECT id FROM mdl_group_department WHERE team_id = ".$this->groupid);
		$this->isGlobal = 0;
		if(empty($isGlobalArray)){
			$this->isGlobal = 1;
		}
        require_once($CFG->dirroot . '/group/lib.php');
    }

    protected function get_options() {
        $options = parent::get_options();
        $options['groupid'] = $this->groupid;
        $options['courseid'] = $this->courseid;
        return $options;
    }

    /**
     * @param array $roles array in the format returned by groups_calculate_role_people.
     * @return array array in the format find_users is supposed to return.
     */
    protected function convert_array_format($roles, $search) {
        if (empty($roles)) {
            $roles = array();
        }
        $groupedusers = array();
        foreach ($roles as $role) {
           /* if ($search) {
                $a = new stdClass;
                $a->role = $role->name;
                $a->search = $search;
                $groupname = get_string('matchingsearchandrole', '', $a);
            } else {*/
                $groupname = $role->name;
            //}
            $groupedusers[$groupname] = $role->users;
            foreach ($groupedusers[$groupname] as &$user) {
                unset($user->roles);
                $user->fullname = fullname($user);
                if (!empty($user->component)) {
                    $user->infobelow = get_string('addedby', 'group',
                        get_string('pluginname', $user->component));
                }
            }
        }
        return $groupedusers;
    }
}

/**
 * User selector subclass for the list of users who are in a certain group.
 * Used on the add group memebers page.
 */
class group_members_selector extends groups_user_selector_base {
    public function find_users($search) {  
        list($wherecondition, $params) = $this->search_sql($search, 'u');
	
        list($sort, $sortparams) = users_order_by_sql('u', $search, $this->accesscontext);

        $roles = customGroupsGetMembersByRole($this->groupid, $this->courseid,
                $this->required_fields_sql('u') . ', gm.component',
                $sort, $wherecondition, array_merge($params, $sortparams));
      
		return $roles;
        return $this->convert_array_format($roles, $search);
    }
}

/**
 * User selector subclass for the list of users who are not in a certain group.
 * Used on the add group members page.
 */
class group_non_members_selector extends groups_user_selector_base {
    /**
     * An array of user ids populated by find_users() used in print_user_summaries()
     */
    private $potentialmembersids = array();

    public function output_user($user) {
       // return parent::output_user($user) . ' (' . $user->numgroups . ')';
		 return parent::output_user($user);
    }

    /**
     * Returns the user selector JavaScript module
     * @return array
     */
    public function get_js_module() {
        return self::$jsmodule;
    }

    /**
     * Creates a global JS variable (userSummaries) that is used by the group selector
     * to print related information when the user clicks on a user in the groups UI.
     *
     * Used by /group/clientlib.js
     *
     * @global moodle_database $DB
     * @global moodle_page $PAGE
     * @param int $courseid
     */
    public function print_user_summaries($courseid) {
        global $DB, $PAGE;

        $usersummaries = array();

        // Get other groups user already belongs to
        $usergroups = array();
        $potentialmembersids = $this->potentialmembersids;
        if( empty($potentialmembersids)==false ) {
            list($membersidsclause, $params) = $DB->get_in_or_equal($potentialmembersids, SQL_PARAMS_NAMED, 'pm');
            $sql = "SELECT u.id AS userid, g.*
                    FROM {user} u
                    JOIN {groups_members} gm ON u.id = gm.userid
                    JOIN {groups} g ON gm.groupid = g.id
                    WHERE u.id $membersidsclause AND g.courseid = :courseid ";
            $params['courseid'] = $courseid;
            $rs = $DB->get_recordset_sql($sql, $params);
            foreach ($rs as $usergroup) {
                $usergroups[$usergroup->userid][$usergroup->id] = $usergroup;
            }
            $rs->close();

            foreach ($potentialmembersids as $userid) {
                if (isset($usergroups[$userid])) {
                    $usergrouplist = html_writer::start_tag('ul');
                    foreach ($usergroups[$userid] as $groupitem) {
                        $usergrouplist .= html_writer::tag('li', format_string($groupitem->name));
                    }
                    $usergrouplist .= html_writer::end_tag('ul');
                } else {
                    $usergrouplist = '';
                }
                $usersummaries[] = $usergrouplist;
            }
        }

        $PAGE->requires->data_for_js('userSummaries', $usersummaries);
    }

    public function find_users($search) {
        global $DB,$USER,$CFG;
        // Get list of allowed roles.
		$context = context_system::instance();
		
        if ($validroleids = groups_get_possible_roles($context)) {
            list($roleids, $roleparams) = $DB->get_in_or_equal($validroleids, SQL_PARAMS_NAMED, 'r');
        } else {
            $roleids = " = -1";
            $roleparams = array();
        }


        // We want to query both the current context and parent contexts.
        list($relatedctxsql, $relatedctxparams) = $DB->get_in_or_equal($context->get_parent_context_ids(true), SQL_PARAMS_NAMED, 'relatedctx');

        // Get the search condition.
        list($searchcondition, $searchparams) = $this->search_sql($search, 'u');
			$roleChk = "and r.name IN( 'learner','manager')";
			switch($USER->archetype){
				CASE $CFG->userTypeAdmin:
						$searchcondition .= '';
					break;
				CASE $CFG->userTypeManager:
															
						$searchcondition .=	" AND u.id IN(SELECT DISTINCT(u.id)
												FROM mdl_user AS u
												LEFT JOIN mdl_department_members AS dm ON dm.userid = u.id
												WHERE dm.departmentid IN (
												SELECT departmentid
												FROM mdl_department_members
												WHERE userid = $USER->id) )";
					break;
			}
			$groupDepartment = $DB->get_record_sql("SELECT gd.id,gd.department_id FROM mdl_group_department as gd WHERE gd.team_id = ".$this->groupid." AND gd.is_active = 1");
			$fields = "SELECT r.id AS roleid, u.id AS userid,
							  " . $this->required_fields_sql('u') . ", u.username,
							  (SELECT count(igm.groupid)
								 FROM mdl_groups_members igm
								 JOIN mdl_groups ig ON igm.groupid = ig.id
								WHERE igm.userid = u.id AND ig.courseid = :courseid) AS numgroups";
			if(!empty($groupDepartment)){
				$sql = "   FROM mdl_user u
					  LEFT JOIN mdl_role_assignments ra ON (ra.userid = u.id)
					  LEFT JOIN mdl_role r ON r.id = ra.roleid
					  LEFT JOIN mdl_groups_members gm ON (gm.userid = u.id AND gm.groupid = :groupid AND gm.is_active = 1)
					  LEFT JOIN mdl_department_members as dm ON dm.userid = u.id
					  LEFT JOIN mdl_group_department as gd ON gd.department_id = dm.departmentid
						  WHERE u.deleted = 0 AND u.suspended = 0 $roleChk
								AND gm.id IS NULL
								AND $searchcondition AND gd.team_id = ".$this->groupid;
			}else{				
				$sql = "	FROM mdl_user u
					LEFT JOIN mdl_role_assignments ra ON (ra.userid = u.id)
					LEFT JOIN mdl_role r ON r.id = ra.roleid
					LEFT JOIN mdl_groups_members gm ON (gm.userid = u.id AND gm.groupid = :groupid AND gm.is_active = 1)
					WHERE u.deleted = 0 AND u.suspended = 0 $roleChk AND gm.id IS NULL AND $searchcondition";
			}
			list($sort, $sortparams) = users_order_by_sql('u', $search, $this->accesscontext);
			$orderby = "ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname";
	
			$params = array_merge($searchparams, $roleparams, $relatedctxparams);
			$params['courseid'] = $this->courseid;
			$params['groupid']  = $this->groupid;
		
		
        if (!$this->is_validating()) {
            $potentialmemberscount = $DB->count_records_sql("SELECT COUNT(DISTINCT u.id) $sql", $params);
            if ($potentialmemberscount > $this->maxusersperpage) {
               // return $this->too_many_results($search, $potentialmemberscount);
            }
        }
        $rs = $DB->get_recordset_sql("$fields $sql $orderby", array_merge($params, $sortparams));
        $roles =  groups_calculate_role_people($rs, $context);

        //don't hold onto user IDs if we're doing validation
        if (empty($this->validatinguserids) ) {
            if($roles) {
                foreach($roles as $k=>$v) {
                    if($v) {
                        foreach($v->users as $uid=>$userobject) {
                            $this->potentialmembersids[] = $uid;
                        }
                    }
                }
            }
        }

        return $this->convert_array_format($roles, $search);
    }
}



// start assign courses to group data
class non_group_courses_selector {
	protected $groupid;
	protected $isGlobal;
	protected $name;
	protected $nonGroupCourses;
	protected $GroupCourses;
	protected $groupCoursesOtherList;
	public function __construct($name,$options) {  
        $this->groupid = $options['groupid'];
        $this->isGlobal = $options['isGlobal'];
        $this->name = $name;
    }
	public function get_non_group_courses(){
		global $CFG,$DB,$USER;
		$where = ' AND c.id != 1 ';
		if($this->name != '')
		{
			$where .= "AND (fullname LIKE '%".$this->name."%')";
		}
		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$where .= '';
				break;
			CASE $CFG->userTypeManager:
																
					$where .=	' AND (c.id IN(SELECT dc.courseid
								FROM mdl_department_course AS dc
								LEFT JOIN mdl_department AS d ON d.id = dc.departmentid
								LEFT JOIN mdl_department_members as dm ON dm.departmentid = d.id
								WHERE dm.userid = '.$USER->id.' AND dc.is_active = 1) )';
				break;
		}
		$activeIds = getActiveCourses();
		if($activeIds != ''){
			$where .= " AND c.id IN (".$activeIds.")";
		}
		$where .= " AND c.coursetype_id='1'";
		$Query = "	SELECT distinct(c.id),c.fullname,0 as createdby FROM mdl_course as c LEFT JOIN mdl_groups_course as gc ON gc.courseid = c.id AND gc.is_active = 1 AND gc.groupid = ".$this->groupid." WHERE c.publish = 1 AND gc.id IS NULL AND c.is_active = 1 AND c.id != 1 ".$where. " ORDER BY c.fullname ASC";
		$nonGroupCourse = $DB->get_records_sql($Query);
		$this->nonGroupCourses = $nonGroupCourse;
	}
	public function get_group_courses(){
		global $CFG,$DB, $USER;
		$where = ' AND c.id != 1 ';
		$whereForOthers = '';
		if($this->name != ''){
			$where .= " AND (c.fullname LIKE '%".$this->name."%')";
		}
		
		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$where .= '';
				break;
			CASE $CFG->userTypeManager:
				$groupCourses = fetchGroupsCourseIds($USER->id,$this->groupid);
				$ownerWhere = '';
				if(!empty($groupCourses)){
					$coursesList =  implode(',',$groupCourses);
					$ownerWhere = ' OR gc.courseid IN ('.$coursesList.')';
				}
				$where	.= " AND (gc.courseid IN (SELECT dc.courseid FROM mdl_department_course AS dc WHERE dc.departmentid IN( ".$USER->department.") AND dc.is_active = 1) $ownerWhere ) AND c.coursetype_id = '".$CFG->courseTypeOnline."'";
				break;
		}
																
		$Query = "SELECT DISTINCT(c.id),CONCAT(c.fullname,IF(((gc.end_date = '') || (gc.end_date = 0) || gc.end_date IS NULL),'', FROM_UNIXTIME(gc.end_date,' - ".$CFG->customDefaultDateFormatForDB."'))) as fullname,gc.updatedby as createdby FROM mdl_groups_course as gc LEFT JOIN mdl_course as c ON gc.courseid = c.id WHERE gc.is_active = 1 AND c.is_active = 1 ".$where." AND gc.groupid = ".$this->groupid." ORDER BY c.fullname ASC";
		$GroupCourse = $DB->get_records_sql($Query);
		$this->GroupCourses = $GroupCourse;
		
	}
	
	public function getGroupCourses(){
	   $this->get_group_courses();
	   if($USER->archetype == $CFG->userTypeManager){
	     return $this->GroupCourses;
	   }else{
	     return $this->groupCoursesOtherList;
	   }
	}
	
	public function course_option_list($block){
		GLOBAL $USER,$CFG;
		if($block == 1){
			$courses = $this->nonGroupCourses;
		}else{
			$courses = $this->GroupCourses;
		}
		$option = '';
		if(!empty($courses)){
			$option .= '<optgroup label="Courses ('.count($courses).')">';
			foreach($courses as $course){
				$disabled = '';
				if($block != 1 AND $USER->archetype != $CFG->userTypeAdmin && $this->isGlobal == 1 && $course->createdby != $USER->id){
					$disabled = 'disabled';
				}
				$option .= "<option value = '".$course->id."' ".$disabled.">".$course->fullname."</option>";
			}
		}else{
			$option .= '<optgroup label="None">';
		}
		
		if($USER->archetype == $CFG->userTypeManager){
			if(!empty($courseOthers)){
				foreach($courseOthers as $courseOther){
					$option .= "<option value = '".$courseOther->id."' class = 'disable-grey' disabled >".$courseOther->fullname."</option>";
				}
			}
		}
		
		$option .= '</optgroup>';
		echo $option;
	}
}
// end assign courses to group data

class user_courses_selector {
	protected $user;
	protected $name;
	protected $userid;
	protected $departmentId;
	protected $nonUserCourses;
	protected $userCourses;
	public function __construct($name,$options) {  
        $this->userid = $options['userid'];
        $this->departmentId = $options['departmentId'];
        $this->name = addslashes($name);
    }
	public function get_non_user_courses(){
		global $CFG,$DB,$USER;
		$where = " AND id != 1 ";
		if($this->name != '')
		{
			$where .= "AND ( fullname LIKE '%".$this->name."%')";
		}
		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$where .= '';
				break;
			CASE $CFG->userTypeManager:
					/*$where .=	' AND (id IN(SELECT dc.courseid
																FROM mdl_department_course AS dc
																LEFT JOIN mdl_department AS d ON d.id = dc.departmentid
																LEFT JOIN mdl_department_members as dm ON dm.departmentid = d.id
																WHERE dc.is_active = 1 AND dm.is_active = 1 AND dm.userid = '.$USER->id.') OR createdby = '.$USER->id.')';*/
																
					$where .=	' AND (id IN(SELECT dc.courseid
																FROM mdl_department_course AS dc
																LEFT JOIN mdl_department AS d ON  d.deleted = 0 AND d.id = dc.departmentid 
																LEFT JOIN mdl_department_members as dm ON dm.departmentid = d.id
																WHERE dc.is_active = 1 AND dm.is_active = 1 AND dm.userid = '.$USER->id.') )';
				break;
		}
		$activeIds = getActiveCourses();
		if($activeIds != ''){
			$where .= " AND id IN (".$activeIds.")";
		}
		$where .= " AND coursetype_id='1'";
		$query = "SELECT DISTINCT(courseid) FROM mdl_user_course_mapping WHERE status = 1 AND userid = ".$this->userid;
		$Query = "SELECT DISTINCT(id),fullname,0 as createdby FROM mdl_course WHERE publish = 1 AND is_active = 1 AND id != '' AND criteria !=1 AND id NOT IN ($query) ".$where. " ORDER BY fullname ASC";
		$nonUserCourses = $DB->get_records_sql($Query);
		$this->nonUserCourses = $nonUserCourses;
	}
	public function get_user_courses(){
		global $CFG,$DB,$USER;
		$where = ' AND c.id != 1 ';
		if($this->name != '')
		{
			$where .= " AND (c.fullname LIKE '%".$this->name."%')";
		}
		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$where .= '';
				break;
			CASE $CFG->userTypeManager:
					/*$where .=	' AND (c.id IN(SELECT dc.courseid
																FROM mdl_department_course AS dc
																LEFT JOIN mdl_department AS d ON d.id = dc.departmentid
																LEFT JOIN mdl_department_members as dm ON dm.departmentid = d.id
																WHERE dm.userid = '.$USER->id.') OR c.createdby = '.$USER->id.')';*/
				break;
		}
		$query = "SELECT DISTINCT(c.id),CONCAT(c.fullname,IF(((cm.end_date = '') || (cm.end_date = 0) || cm.end_date IS NULL),'', FROM_UNIXTIME(cm.end_date,' - ".$CFG->customDefaultDateFormatForDB."'))) as fullname,cm.updatedby as createdby FROM mdl_user_course_mapping as cm LEFT JOIN mdl_course as c ON c.id = cm.courseid";
		$query .= " WHERE cm.status = 1 AND c.id != '' AND cm.userid = ".$this->userid.$where;
		$query .= "ORDER BY LOWER(c.fullname) ASC";
		$userCourses = $DB->get_records_sql($query);
		$this->userCourses = $userCourses;
	}
	
	public function getUserCourses(){
	  $this->get_user_courses();
	  return $this->userCourses;
	}
	
	public function course_option_list($block){
		GLOBAL $USER,$CFG;
		if($block == 1){
			$courses = $this->nonUserCourses;
		}else{
			$courses = $this->userCourses;
		}
		$option = '';
		if(!empty($courses)){
			$option .= '<optgroup label="Courses ('.count($courses).')">';
			foreach($courses as $course){
				$disabled = '';
				if($block != 1 && $this->departmentId != $USER->department && $USER->archetype != $CFG->userTypeAdmin && $course->createdby != $USER->id){
					$disabled = 'disabled';
				}
				$option .= "<option value = '".$course->id."' $disabled>".$course->fullname."</option>";
			}
		}else{
			$option .= '<optgroup label="None">';
		}
		$option .= '</optgroup>';
		echo $option;
	}
}


class program_users_selector {
	protected $programId;
	protected $name;
	protected $nonProgramUsers;
	protected $programUsers;
	public function __construct($name,$options) {  
        $this->programId = $options['programId'];
        $this->name = addslashes($name);
    }
	public function get_non_program_users(){
		global $CFG,$DB;
		$where = " AND 1 = 1 ";
		if($this->name != '')
		{
			$where .= "AND ( firstname LIKE '%".$this->name."%' || lastname LIKE '%".$this->name."%')";
		}


		$query = "SELECT DISTINCT(userid) FROM mdl_program_members WHERE 1 = 1 AND programid = ".$this->programId;
		$Query = "SELECT DISTINCT(id),CONCAT(firstname,' ', lastname,' (',username,')')  AS name FROM mdl_user WHERE deleted = '0'  AND id != '' AND id NOT IN ($query) ".$where. " ORDER BY name ASC";
		$nonProgramUsers = $DB->get_records_sql($Query);
		$this->nonProgramUsers = $nonProgramUsers;
	}
	public function get_program_users(){
		global $CFG,$DB;
		$where = ' AND 1 = 1 ';
		if($this->name != '')
		{
			$where .= "AND ( u.firstname LIKE '%".$this->name."%' || u.lastname LIKE '%".$this->name."%')";
		}
		$query = "SELECT DISTINCT(u.id),CONCAT(u.firstname,' ', u.lastname,' (',u.username,')')  AS name FROM mdl_program_members as pm LEFT JOIN mdl_user as u ON u.id = pm.userid";
		$query .= " WHERE pm.programid != '' AND pm.programid = ".$this->programId.$where;
		$programUsers = $DB->get_records_sql($query);
		$this->programUsers = $programUsers;
	}
	public function user_option_list($block){
		if($block == 1){
			$users = $this->nonProgramUsers;
		}else{
			$users = $this->programUsers;
		}
		$option = '';
		if(!empty($users)){
			$option .= '<optgroup label="'.get_string('members','program').' ('.count($users).')">';
			foreach($users as $user){
				$option .= "<option value = '".$user->id."'>".$user->name."</option>";
			}
		}else{
			$option .= '<optgroup label="None">';
		}
		$option .= '</optgroup>';
		echo $option;
	}
}

class courses_assignment_selector{
	protected $courseId;
	protected $name;
	
	public $selMode;
	public $userGroup;
	public $department;
	public $team;
	public $managers;
	public $roles;
    public $job_title;
	public $company;
    public $country; //add country filter on 15th feb 2016


	protected $courseTeamList;
	protected $courseNonTeamList;
	protected $courseOthersTeamList;

	protected $courseDepartmentList;
	protected $courseNonDepartmentList;
	protected $courseOthersDepartmentList;

	protected $courseUserList;
	protected $courseNonUserList;
	protected $courseOthersUserList;

	public function __construct($name,$options) {  
        $this->courseId = isset($options['courseid'])?$options['courseid']:0;
		$this->selMode = isset($options['selMode'])?$options['selMode']:'';
		$this->userGroup = isset($options['userGroup'])?$options['userGroup']:'';
		$this->department = isset($options['department'])?$options['department']:'';
		$this->team = isset($options['team'])?$options['team']:'';
		$this->managers = isset($options['managers'])?$options['managers']:'';
		$this->roles = isset($options['roles'])?$options['roles']:'';
		$this->job_title = isset($options['job_title'])?$options['job_title']:'';
                $this->report_to = isset($options['report_to'])?$options['report_to']:'';
		$this->company = isset($options['company'])?$options['company']:'';
                $this->country = isset($options['country'])?$options['country']:''; //add country filter on 15th feb 2016
                $this->is_manager_yes = isset($options['country'])?$options['is_manager_yes']:'';
                
                $this->name = isset($name)?strtolower(addslashes($name)):'';
                $this->startDate = isset($options['startdate'])?$options['startdate']:'';
		$this->endDate = isset($options['enddate'])?$options['enddate']:'';
    }
	public function courseTeamList(){
		global $CFG,$DB,$USER;
		$where = '';
		if($this->name != '')
		{
			$where = " AND (LOWER(g.name) LIKE '%".$this->name."%')";
		}
		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$where .= '';
				break;
			CASE $CFG->userTypeManager:
					//$where .=	' AND createdby ='. $USER->id;
					//$whereForOthers .=	' AND createdby !='. $USER->id;
					
					 $dGroups = getDepartmentGroups($USER->department);
					 $dGroupsStr = 0;
					 if(count($dGroups) > 0 ){
					   $dGroupsIds = array_keys($dGroups);
					   $dGroupsStr = implode(",", $dGroupsIds);
					 }
					
					 $where .=	' AND g.id IN ('.$dGroupsStr.')';
					 $whereForOthers .=	' AND g.id IN (0)';
				 
				break;
		}
		$query = "SELECT DISTINCT(g.id), CONCAT(IF((d.title IS NULL),'".get_string("global_team_identifier")."',''),g.name,IF((d.title IS NULL), IF(((gc.end_date = '') || (gc.end_date = 0) || gc.end_date IS NULL),'', FROM_UNIXTIME(gc.end_date,' - ".$CFG->customDefaultDateFormatForDB."')), CONCAT('(',d.title,')', IF(((gc.end_date = '') || (gc.end_date = 0) || gc.end_date IS NULL),'', FROM_UNIXTIME(gc.end_date,' - ".$CFG->customDefaultDateFormatForDB."'))))) AS name,d.id as dep_id ,gc.updatedby as createdby FROM mdl_groups_course as gc LEFT JOIN mdl_groups as g ON g.id = gc.groupid LEFT JOIN mdl_group_department as gd ON gd.team_id = g.id LEFT JOIN mdl_department d ON d.deleted = 0 AND d.id = gd.department_id WHERE gc.is_active = 1 AND g.id IS NOT NULL AND gc.courseid = ".$this->courseId.$where." ORDER BY LOWER(CONCAT(IF((d.title IS NULL),'".get_string("global_team_identifier")."',''),g.name, IF((d.title IS NULL),'', CONCAT('(',d.title,')')))) ASC";
		$courseTeamList = $DB->get_records_sql($query);
		$this->courseTeamList = $courseTeamList;

		$othersTeams = "SELECT DISTINCT(g.id),CONCAT(IF((d.title IS NULL),'".get_string("global_team_identifier")."',''),g.name,IF((d.title IS NULL), IF(((gc.end_date = '') || (gc.end_date = 0) || gc.end_date IS NULL),'', FROM_UNIXTIME(gc.end_date,' - ".$CFG->customDefaultDateFormatForDB."')), CONCAT('(',d.title,')', IF(((gc.end_date = '') || (gc.end_date = 0) || gc.end_date IS NULL),'', FROM_UNIXTIME(gc.end_date,' - ".$CFG->customDefaultDateFormatForDB."'))))) AS name FROM mdl_groups_course as gc LEFT JOIN mdl_groups as g ON g.id = gc.groupid LEFT JOIN mdl_group_department as gd ON gd.team_id = g.id LEFT JOIN mdl_department d ON d.deleted = 0 AND d.id = gd.department_id WHERE gc.is_active = 1 AND g.id IS NOT NULL AND gc.courseid = ".$this->courseId.$whereForOthers." ORDER BY LOWER(CONCAT(IF((d.title IS NULL),'".get_string("global_team_identifier")."',''),g.name, IF((d.title IS NULL),'', CONCAT('(',d.title,')')))) ASC";	
		$courseOthersTeamList = $DB->get_records_sql($othersTeams);
		$this->courseOthersTeamList = $courseOthersTeamList;
	}
	public function courseNonTeamList(){
		global $CFG,$DB,$USER;
		$where = '';
		if($this->name != '')
		{
			$where = " AND (LOWER(g.name) LIKE '%".$this->name."%')";
		}
		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$where .= ' ';
				break;
			CASE $CFG->userTypeManager:
					//$where .=	' AND createdby ='. $USER->id;
					 $dGroups = getDepartmentGroups($USER->department);
					 $dGroupsStr = 0;
					 if(count($dGroups) > 0 ){
					   $dGroupsIds = array_keys($dGroups);
					   $dGroupsStr = implode(",", $dGroupsIds);
					 }
					
					 $where .=	' AND g.id IN('.$dGroupsStr.')';
				 
				break;
		}
		
		$where .= ' AND g.is_active = 1 ';

		$query = "SELECT g.id,CONCAT(IF((d.title IS NULL),'".get_string("global_team_identifier")."',''),g.name,IF((d.title IS NULL),'',CONCAT('(',d.title,')'))) as name FROM mdl_groups AS g LEFT JOIN mdl_groups_course gc ON (g.id = gc.groupid AND gc.is_active = 1 AND gc.courseid = ".$this->courseId.") LEFT JOIN mdl_group_department as gd ON gd.team_id = g.id LEFT JOIN mdl_department d ON d.deleted = 0 AND d.id = gd.department_id WHERE gc.groupid IS NULL AND g.id IS NOT NULL $where ORDER BY LOWER(CONCAT(IF((d.title IS NULL),'".get_string("global_team_identifier")."',''),g.name, IF((d.title IS NULL),'', CONCAT('(',d.title,')')))) ASC";
		$courseNonTeamList = $DB->get_records_sql($query);
		$this->courseNonTeamList = $courseNonTeamList;
	}
	public function courseUserList(){
		global $CFG,$DB,$USER;
		$where = '';
		$whereForOthers = '';
		if($this->name != '')
		{
			$where = " AND (LOWER(u.firstname) LIKE '%".$this->name."%' || LOWER(u.lastname) LIKE '%".$this->name."%' || LOWER(u.username) LIKE '%".$this->name."%' || LOWER(CONCAT(u.firstname,' ',u.lastname)) LIKE '%".$this->name."%')";
		}
                
                //Code Added to add country filter on 15th feb 2016
                if($this->country !='' && $this->country !='-1'){
                    $countryExplode = explode('@',$this->country);
                    $countryCSV = '';
                    foreach($countryExplode as $countrycode){
                        $countryCSV .="'".$countrycode."',";
                    }
                    $countryCSV = trim($countryCSV,',');
                    $where .=" AND u.country IN(".$countryCSV.")";
                }
                //end code of country filter
                
                if($this->is_manager_yes !='' && $this->is_manager_yes !='-1'){
                    $isManagerExplode = explode('@',$this->is_manager_yes);                  
                   
                    $is_manager_yesCSV = implode(',',$isManagerExplode);
                    $where .=" AND u.is_manager_yes IN(".$is_manager_yesCSV.")";
                }
		
                if($this->report_to != '' && $this->report_to != '-1' && count($this->report_to) > 0){
			 $id_explode = explode("@",$this->report_to);
			 $List = getLeadersNameList();

			 $leaders_name = '';
			 foreach($id_explode as $id){
							 $leaders_name .= '"'.$List[$id].'",';
			 }
			 $leaders_name = trim($leaders_name,',');
			 $where .= " AND u.report_to IN ($leaders_name)";
                }
		$where .= ($this->selMode!= '')?getSelectFilterCondition($this):'';

		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$where .= ' AND (r.name = "'.$CFG->userTypeManager.'" OR r.name = "'.$CFG->userTypeStudent.'")';
				break;
			CASE $CFG->userTypeManager:
					$groupOwnerTeamIds = fetchGroupsList();
					$departmentUserSelector = "";
					$departmentUserSelectorOther = "";
					if(!empty($groupOwnerTeamIds)){
						$groupsList =  implode(',',$groupOwnerTeamIds);
						$departmentUserSelect = "u.id IN (SELECT gm.userid FROM mdl_groups_members as gm WHERE gm.is_active = 1 AND gm.groupid IN (".$groupsList."))";
						//$departmentUserSelector = " OR ".$departmentUserSelect;
						$departmentUserSelectorOther = " AND ".$departmentUserSelect;
					}else{
						$departmentUserSelector = "";
						$departmentUserSelectorOther = "";
					}
					$where .=  " AND (u.id IN(SELECT DISTINCT(u.id)
															FROM mdl_user AS u
															LEFT JOIN mdl_department_members AS dm ON dm.userid = u.id
															WHERE dm.departmentid IN (
															SELECT departmentid
															FROM mdl_department_members
															WHERE userid = $USER->id) ) $departmentUserSelector)";
					if(!empty($groupOwnerTeamIds)){									
						$whereForOthers .=  " AND u.id NOT IN(SELECT DISTINCT(u.id)
															FROM mdl_user AS u
															LEFT JOIN mdl_department_members AS dm ON dm.userid = u.id
															WHERE dm.departmentid IN (
															SELECT departmentid
															FROM mdl_department_members
															WHERE userid = $USER->id) ) AND (r.name = '".$CFG->userTypeManager."' OR r.name = '".$CFG->userTypeStudent."') $departmentUserSelectorOther";
					}else{
						$whereForOthers .=  " AND u.id IN (0)";
					}
					
				break;
		}

		$query = "	SELECT u.id,CONCAT(u.firstname,' ', u.lastname,' (',u.username,')',IF(((uc.end_date = '') || (uc.end_date = 0) || uc.end_date IS NULL),'', FROM_UNIXTIME(uc.end_date,' - ".$CFG->customDefaultDateFormatForDB."')))  AS name,uc.type FROM mdl_user as u LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id LEFT JOIN mdl_role AS r ON ra.roleid = r.id LEFT JOIN mdl_user_course_mapping AS uc ON u.id = uc.userid LEFT JOIN mdl_department_members as dm ON dm.userid = u.id AND dm.is_active = 1
					LEFT JOIN mdl_department as d ON dm.departmentid = d.id WHERE u.deleted = 0 AND u.suspended = 0 AND uc.courseid = $this->courseId AND uc.type = 'user' AND uc.status = 1  AND d.`status` = 1 AND d.deleted = 0 ";
		$orderBy = " ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname";
		$Select = $query.$where.$orderBy;
		$courseUserList = $DB->get_records_sql($Select);
		$this->courseUserList = $courseUserList;

		//$queryOthers = $query." WHERE cm.status = 1 AND u.deleted = 0 AND u.id != '' AND cm.courseid = ".$this->courseId.$whereForOthers." ORDER BY u.firstname ASC";
		$queryOthers = $query.$whereForOthers.$orderBy;
		$courseOthersUserList = $DB->get_records_sql($queryOthers);
		$this->courseOthersUserList = $courseOthersUserList;
	}
	
	public function getCourseUsers(){
	   $this->courseUserList();
	   if($USER->archetype == $CFG->userTypeManager){
	     return $this->courseUserList;
	   }else{
	     return $this->courseOthersUserList;
	   }

	}
	
	public function courseNonUserList(){
		global $CFG,$DB,$USER;
		$where = '';
		if($this->name != '')
		{
			$where = " AND (LOWER(u.firstname) LIKE '%".$this->name."%' || LOWER(u.lastname) LIKE '%".$this->name."%' || LOWER(u.username) LIKE '%".$this->name."%' || LOWER(CONCAT(u.firstname,' ',u.lastname)) LIKE '%".$this->name."%')";
		}
		//Add country filter on 15th feb 2016
                if($this->country !='' && $this->country !='-1'){
                    $countryExplode = explode('@',$this->country);
                    $countryCSV = '';
                    foreach($countryExplode as $countrycode){
                        $countryCSV .="'".$countrycode."',";
                    }
                    $countryCSV = trim($countryCSV,',');
                    $where .=" AND u.country IN(".$countryCSV.")";
                }
                
                //Add country filter on 15th feb 2016
                if($this->is_manager_yes !='' && $this->is_manager_yes !='-1'){
                    $isManagerExplode = explode('@',$this->is_manager_yes);                  
                   
                    $is_manager_yesCSV = implode(',',$isManagerExplode);
                    $where .=" AND u.is_manager_yes IN(".$is_manager_yesCSV.")";
                }
                
                if($this->report_to != '' && $this->report_to != '-1' && count($this->report_to) > 0){
			 $id_explode = explode("@",$this->report_to);
			 $List = getLeadersNameList();

			 $leaders_name = '';
			 foreach($id_explode as $id){
				$leaders_name .= '"'.$List[$id].'",';
			 }
			 $leaders_name = trim($leaders_name,',');
			 $where .= " AND u.report_to IN ($leaders_name)";
                }
                
                //end code of country filter
                
		$where .= ($this->selMode!= '')?getSelectFilterCondition($this):'';
		
		$where .= ($this->selMode!= '')?getSelectFilterCondition($this):'';
               
                $sd=$this->startDate;
		if($sd != "")
		{
			$ed=empty($this->endDate)?date('Y-m-d'):$this->endDate;
			if(strtotime($sd) > strtotime($ed))
			{
				$tmpdate = $ed;
				$ed = $sd;
				$sd = $tmpdate;
			}

			$sd = strtotime($sd);
			$ed = strtotime("$ed 23:59:59");		
			$where .= " AND ( u.dateofbirth BETWEEN $sd AND $ed )";
		}
		elseif($sd=="" && $this->endDate != '')
		{
			$ed=$this->endDate;			
			$ed = strtotime("$ed 23:59:59");		
			$where .= " AND ( u.dateofbirth <= $ed )";
		
		}
		
		//$notInSubQuery = "SELECT DISTINCT(userid) FROM mdl_user_course_mapping WHERE status = 1 AND courseid = ".$this->courseId." ";
		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$where .= ' AND (r.name = "'.$CFG->userTypeManager.'" OR r.name = "'.$CFG->userTypeStudent.'")';
				break;
			CASE $CFG->userTypeManager:
					//$where .=	' AND u.id IN(Select u.id FROM mdl_user as u WHERE u.parent_id = '.$USER->id.' OR u.createdby = '.$USER->id.') AND r.name = "'.$CFG->userTypeStudent.'"';
					/*$where .=  " AND u.id IN(SELECT DISTINCT(u.id)
															FROM mdl_user AS u
															LEFT JOIN mdl_department_members AS dm ON dm.userid = u.id
															WHERE dm.departmentid IN (
															SELECT departmentid
															FROM mdl_department_members
															WHERE userid = $USER->id) OR u.createdby = $USER->id) AND r.name = '".$CFG->userTypeStudent."'";*/
					$groupOwnerTeamIds = fetchGroupsList();	
					$departmentUserSelector = "";
					if(!empty($groupOwnerTeamIds)){
						$groupsList =  implode(',',$groupOwnerTeamIds);
						$departmentUserSelect = "u.id IN (SELECT gm.userid FROM mdl_groups_members as gm WHERE gm.is_active = 1 AND gm.groupid IN (".$groupsList."))";
						//$departmentUserSelector = " OR ".$departmentUserSelect;
					}else{
						$departmentUserSelector = "";
					}						
					$where .=  " AND (u.id IN(SELECT DISTINCT(u.id)
															FROM mdl_user AS u
															LEFT JOIN mdl_department_members AS dm ON dm.userid = u.id
															WHERE dm.departmentid IN (
															SELECT departmentid
															FROM mdl_department_members
															WHERE userid = $USER->id)) $departmentUserSelector)"; // AND r.name = '".$CFG->userTypeStudent."'
				break;
		}
		//$query = "SELECT DISTINCT(u.id),CONCAT(u.firstname, ' ', u.lastname) AS name FROM mdl_user as u LEFT JOIN mdl_role_assignments as ra ON ra.userid = u.id LEFT JOIN {$CFG->prefix}role as r ON r.id = ra.roleid WHERE u.suspended = 0 AND u.deleted = 0 AND u.id NOT IN ($notInSubQuery) $where ORDER BY u.firstname ASC";

		$query = "SELECT DISTINCT(u.id),CONCAT(u.firstname,' ', u.lastname,' (',u.username,')') AS name
					FROM mdl_user AS u
					LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id
					LEFT JOIN mdl_role AS r ON r.id = ra.roleid
					LEFT JOIN mdl_user_course_mapping as ucm ON ( ucm.userid = u.id AND ucm.`type` = 'user' AND ucm.`status` = 1 AND ucm.courseid = $this->courseId)
					LEFT JOIN mdl_department_members as dm ON dm.userid = u.id AND dm.is_active = 1
					LEFT JOIN mdl_department as d ON dm.departmentid = d.id
					WHERE ucm.id IS NULL AND u.suspended = 0 AND u.deleted = 0 AND d.`status` = 1 AND d.deleted = 0 $where
					ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname";
		//echo $query;die;
		$courseNonUserList = $DB->get_records_sql($query);
		$this->courseNonUserList = $courseNonUserList;
	}
	public function courseDepartmentList(){
		global $CFG,$DB;
		$where = '';
		if($this->name != '')
		{
			$where = " AND (LOWER(d.title) LIKE '%".$this->name."%')";
		}
		
		$extDeptIdentifier = get_string('external_department_identifier', 'department');
        $where .= externalCheckForDepartment();
		$query = "SELECT d.id,(if(d.is_external = 1, concat('".$extDeptIdentifier ."', d.title), d.title)) AS name, d.is_external FROM mdl_department as d LEFT JOIN mdl_department_course as cm ON d.id = cm.departmentid WHERE d.deleted = 0 AND cm.courseid = ".$this->courseId." AND cm.is_active = 1 $where ORDER BY d.title ASC";

		$courseDepartmentList = $DB->get_records_sql($query);
		$this->courseDepartmentList = $courseDepartmentList;
	}
	public function courseNonDepartmentList(){
		global $CFG,$DB;
		$where = '';
		if($this->name != '')
		{
			$where = " AND (LOWER(d.title) LIKE '%".$this->name."%')";
		}
		
		$extDeptIdentifier = get_string('external_department_identifier', 'department');
		$where .= externalCheckForDepartment();
		$query = "SELECT d.id,(if(d.is_external = 1, concat('".$extDeptIdentifier ."', d.title), d.title)) AS name, d.is_external FROM mdl_department AS d LEFT JOIN mdl_department_course dc ON (d.id = dc.departmentid AND dc.is_active = 1 AND dc.courseid = ".$this->courseId.") WHERE d.deleted = 0 AND d.status = 1 AND dc.departmentid IS NULL $where ORDER BY d.title ASC";

		$courseNonDepartmentList = $DB->get_records_sql($query);
		$this->courseNonDepartmentList = $courseNonDepartmentList;
	}
	public function course_option_list($block = 'team_course', $return = false){
		global $USER,$CFG,$DB;
		$courseOthers = array();
		$chkOwner = 0;
		if($block == 'team_course'){
			$courses = $this->courseTeamList;
			$courseOthers = $this->courseOthersTeamList;
			$chkOwner = 1;
			$header = get_string('teams');
		}elseif($block == 'non_team_course'){
			$courses = $this->courseNonTeamList;
			$header = get_string('teams');
		}elseif($block == 'department_course'){
			$courses = $this->courseDepartmentList;
			$courseOthers = $courses;
			$header = get_string('departments');
		}elseif($block == 'non_department_course'){
			$courses = $this->courseNonDepartmentList;
			$header = get_string('departments');
		}elseif($block == 'user_course'){
			$courses = $this->courseUserList;
			$courseOthers = $this->courseOthersUserList;
			$header = get_string('users');
		}elseif($block == 'non_user_course'){
			$courses = $this->courseNonUserList;
			$header = get_string('users');
		}
		$option = '';
		if(!empty($courses)){
			$courseCount = count($courses);
			if($USER->archetype == $CFG->userTypeManager){
				$courseCount += count($courseOthers);
			}
			$option .= '<optgroup label="'.$header.' ('.$courseCount.')">';
			foreach($courses as $course){
				$disable = '';
				if($block == 'user_course'){
					if($course->type != 'user'){
						$disable = "class = 'option-disable'";
					}
				}
				if($chkOwner == 1 && $USER->archetype != $CFG->userTypeAdmin && $course->dep_id != $USER->department && $course->createdby != $USER->id){
					$disable = "disabled = 'disabled'";
				}
				$option .= "<option value = '".$course->id."' ".$disable." >".$course->name."</option>";
			}
		}else{
			$option .= '<optgroup label="None">';
		}
		if($USER->archetype == $CFG->userTypeManager){
			if(!empty($courseOthers)){
				foreach($courseOthers as $courseOther){
					$option .= "<option value = '".$courseOther->id."' disabled = 'disabled' >".$courseOther->name."</option>";
				}
			}
		}
		$option .= '</optgroup>';
		if($return){
		  return $option;
		}else{
		  echo $option;
		}
	}
}

class department_courses_selector {
	protected $departmentId;
	protected $name;
	protected $nonDepartmentCourses;
	protected $DepartmentCourses;
	public function __construct($name,$options) {  
        $this->departmentId = $options['departmentId'];
        $this->name = addslashes($name);
    }
	public function get_non_department_courses(){
		global $CFG,$DB;
		$where = ' AND c.id != 1 ';
		if($this->name != '')
		{
			$where .= "AND (fullname LIKE '%".$this->name."%')";
		}
		$where .= " AND c.coursetype_id='1'";
		$activeIds = getActiveCourses();
		if($activeIds != ''){
			$where .= " AND c.id IN (".$activeIds.")";
		}
		$Query = "	SELECT DISTINCT(c.id),fullname,dc.is_active
					FROM mdl_course as c
					LEFT JOIN mdl_department_course as dc ON (dc.courseid = c.id AND dc.is_active = 1 AND dc.departmentid = ".$this->departmentId.")
					WHERE c.publish = 1 AND dc.id IS NULL AND c.id != 1 AND c.is_active = 1 ".$where. "
					ORDER BY fullname ASC";
		$nonDepartmentCourses = $DB->get_records_sql($Query);
		$this->nonDepartmentCourses = $nonDepartmentCourses;
	}
	public function get_department_courses(){
		global $CFG,$DB;
		$where = ' AND c.id != 1 ';
		if($this->name != '')
		{
			$where .= " AND (c.fullname LIKE '%".$this->name."%')";
		}
		$Query = "SELECT DISTINCT(c.id),c.fullname FROM mdl_department_course as gc LEFT JOIN mdl_course as c ON gc.courseid = c.id WHERE gc.is_active = 1 AND c.is_active = 1".$where." AND gc.departmentid = ".$this->departmentId." ORDER BY c.fullname ASC";
		$DepartmentCourses = $DB->get_records_sql($Query);
		$this->DepartmentCourses = $DepartmentCourses;
	}
	
	public function getAssignedDepartmentCourses(){ 
		global $CFG,$DB;
		$this->get_department_courses();
		return $this->DepartmentCourses;
	}
	
	public function course_option_list($block){
		if($block == 1){
			$courses = $this->nonDepartmentCourses;
		}else{
			$courses = $this->DepartmentCourses;
		}
		$option = '';
		if(!empty($courses)){
			$option .= '<optgroup label="Courses ('.count($courses).')">';
			foreach($courses as $course){
				$option .= "<option value = '".$course->id."'>".$course->fullname."</option>";
			}
		}else{
			$option .= '<optgroup label="None">';
		}
		$option .= '</optgroup>';
		echo $option;
	}
	
	
}


class program_courses_selector {
	protected $programId;
	protected $name;
	protected $nonProgramCourses;
	protected $DepartmentCourses;
	protected $DepartmentCoursesOtherList;
	
	public function __construct($name,$options) {  
        $this->programId = $options['programId'];
        $this->name = addslashes($name);
    }
	public function get_non_program_courses(){
		global $CFG,$DB,$USER;
		$where = ' AND id != 1 ';
		if($this->name != '')
		{
			$where .= "AND (fullname LIKE '%".$this->name."%')";
		}
		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$where .= '';
				break;
			CASE $CFG->userTypeManager:
					/*$where .=	' AND (id IN(SELECT dc.courseid
																FROM mdl_department_course AS dc
																LEFT JOIN mdl_department AS d ON d.id = dc.departmentid
																LEFT JOIN mdl_department_members as dm ON dm.departmentid = d.id
																WHERE dc.is_active = 1 AND dm.is_active = 1 AND dm.userid = '.$USER->id.') OR createdby = '.$USER->id.')';*/
																
					$where .=	' AND (id IN(SELECT dc.courseid
																FROM mdl_department_course AS dc
																LEFT JOIN mdl_department AS d ON d.deleted = 0 AND d.id = dc.departmentid
																LEFT JOIN mdl_department_members as dm ON dm.departmentid = d.id
																WHERE dc.is_active = 1 AND dm.is_active = 1 AND dm.userid = '.$USER->id.'))';
				break;
		}
		$activeIds = getActiveCourses();
		if($activeIds != ''){
			$where .= " AND id IN (".$activeIds.")";
		}
		$Query = "SELECT id,fullname FROM mdl_course WHERE publish = 1 AND is_active = 1 AND id NOT IN (SELECT DISTINCT(courseid) FROM mdl_program_course WHERE programid = ".$this->programId." AND is_active = 1) ".$where. " ORDER BY fullname ASC";
		
		$nonDepartmentCourses = $DB->get_records_sql($Query);
		$this->nonDepartmentCourses = $nonDepartmentCourses;
	}
	public function get_program_courses(){
		global $CFG,$DB,$USER;
		$where = ' AND c.id != 1 ';
		$whereForOthers = '';
		if($this->name != ''){
			$where .= " AND (c.fullname LIKE '%".$this->name."%')";
		}
		
		
		
		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$where .= '';
				break;
			CASE $CFG->userTypeManager:
																
					$where .=	' AND (pc.courseid IN(SELECT dc.courseid
																FROM mdl_department_course AS dc
																LEFT JOIN mdl_department AS d ON d.deleted = 0 AND d.id = dc.departmentid
																LEFT JOIN mdl_department_members as dm ON dm.departmentid = d.id
																WHERE dc.is_active = 1 AND dm.is_active = 1 AND dm.userid = '.$USER->id.'))';
																
					$whereForOthers .=	' AND (pc.courseid NOT IN(SELECT dc.courseid
																FROM mdl_department_course AS dc
																LEFT JOIN mdl_department AS d ON d.deleted = 0 AND d.id = dc.departmentid
																LEFT JOIN mdl_department_members as dm ON dm.departmentid = d.id
																WHERE dc.is_active = 1 AND dm.is_active = 1 AND dm.userid = '.$USER->id.'))';
				break;
		}
		
		
		
		$Query = "SELECT DISTINCT(c.id),c.fullname,pc.course_order FROM mdl_program_course as pc LEFT JOIN mdl_course as c ON pc.courseid = c.id WHERE pc.is_active = 1 AND c.is_active = 1".$where." AND pc.programid = ".$this->programId." ORDER BY c.fullname ASC";
		$DepartmentCourses = $DB->get_records_sql($Query);
		$this->DepartmentCourses = $DepartmentCourses;
		
		$queryOthers = "SELECT DISTINCT(c.id),c.fullname,pc.course_order FROM mdl_program_course as pc LEFT JOIN mdl_course as c ON pc.courseid = c.id WHERE pc.is_active = 1 AND c.is_active = 1".$whereForOthers." AND pc.programid = ".$this->programId." ORDER BY c.fullname ASC";
		$DepartmentCoursesOtherList = $DB->get_records_sql($queryOthers);
		$this->DepartmentCoursesOtherList = $DepartmentCoursesOtherList;

	}
	
	public function getProgramCourses(){
	  $this->get_program_courses();
	  if($USER->archetype == $CFG->userTypeManager){
	     return $this->DepartmentCourses;
	  }else{
	     return $this->DepartmentCoursesOtherList;
	  }
	}
	
	public function getNonProgramCourses(){
	  $this->get_non_program_courses();
	  return $this->nonDepartmentCourses;
	}
	
	public function course_option_list($block){
	
	    global $USER,$CFG,$DB;
		if($block == 1){
			$courses = $this->nonDepartmentCourses;
		}else{
			$courses = $this->DepartmentCourses;
			$courseOthers = $this->DepartmentCoursesOtherList;
		}
		$option = '';
		if(!empty($courses)){
			if($USER->archetype == $CFG->userTypeManager){
				$optionCount = count($courses) + count($courseOthers);
			}else{
				$optionCount = count($courses);
			}
			$option .= '<optgroup label="Courses ('.$optionCount.')">';
			foreach($courses as $course){
				$option .= "<option value = '".$course->id."'>".$course->fullname."</option>";
			}
		}else{
			if(!empty($courseOthers)){
				$optionCount = count($courseOthers);
				$option .= '<optgroup label="Courses ('.$optionCount.')">';
			}else{
				$option .= '<optgroup label="None">';
			}
		}
		
		if($USER->archetype == $CFG->userTypeManager){
			if(!empty($courseOthers)){
				foreach($courseOthers as $courseOther){
					$option .= "<option value = '".$courseOther->id."' disabled = 'disabled' >".$courseOther->fullname."</option>";
				}
			}
		}
		
		$option .= '</optgroup>';

	    echo $option;
		
	}
}
class program_assignment_selector{
	protected $ProgramId;
	protected $name;
	
	public $selMode;
	public $userGroup;
	public $department;
	public $team;
	public $managers;
	public $roles;
    public $job_title;
	public $company;
   	public $country; //add filter for country on 15th feb 2016

	

	protected $programTeamList;
	protected $programNonTeamList;
	protected $programOthersTeamList;

	protected $programDepartmentList;
	protected $programNonDepartmentList;
	protected $programOthersDepartmentList;

	protected $programUserList;
	protected $programNonUserList;
	protected $programOthersUserList;

	public function __construct($name,$options) {  
        $this->programId = $options['programid'];
		
		$this->selMode = isset($options['selMode'])?$options['selMode']:'';
		$this->userGroup = isset($options['userGroup'])?$options['userGroup']:'';
		$this->department = isset($options['department'])?$options['department']:'';
		$this->team = isset($options['team'])?$options['team']:'';
		$this->managers = isset($options['managers'])?$options['managers']:'';
		$this->roles = isset($options['roles'])?$options['roles']:'';
		$this->job_title = isset($options['job_title'])?$options['job_title']:'';
		$this->company = isset($options['company'])?$options['company']:'';
                
                //code added to add country filter on 15th feb 2016
                $this->country = isset($options['country'])?$options['country']:'';
				
        $this->name = strtolower(addslashes(trim($name)));
    }
	public function programTeamList(){
		global $CFG,$DB,$USER;
		$where = '';
		if($this->name != ''){
			$where .= " AND (LOWER(g.name) LIKE '%".$this->name."%' || LOWER(d.title) LIKE '%".$this->name."%')";
		}
		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$where .= '';
				break;
			CASE $CFG->userTypeManager:
					//$where .=	' AND g.createdby ='. $USER->id;				 
					// $whereForOthers .=	' AND g.createdby !='. $USER->id;
					
					 $dGroups = getDepartmentGroups($USER->department);
					 $dGroupsStr = 0;
					 if(count($dGroups) > 0 ){
					   $dGroupsIds = array_keys($dGroups);
					   $dGroupsStr = implode(",", $dGroupsIds);
					 }
					
					 $where .=	' AND g.id IN ('.$dGroupsStr.')';
					 $whereForOthers .=	' AND g.id NOT IN ('.$dGroupsStr.')';
					  
				break;
		}
		
		$Query = "	SELECT DISTINCT(g.id), CONCAT(IF((d.title IS NULL),'".get_string("global_team_identifier")."',''),g.name,IF((d.title IS NULL), IF(((pg.end_date = '') || (pg.end_date = 0) || pg.end_date IS NULL),'', FROM_UNIXTIME(pg.end_date,' - ".$CFG->customDefaultDateFormatForDB."')), CONCAT('(',d.title,')', IF(((pg.end_date = '') || (pg.end_date = 0) || pg.end_date IS NULL),'', FROM_UNIXTIME(pg.end_date,' - ".$CFG->customDefaultDateFormatForDB."'))))) AS name,pg.updatedby as createdby,d.id as dep_id FROM mdl_groups as g
					LEFT JOIN mdl_program_group as pg ON g.id = pg.group_id LEFT JOIN mdl_group_department as gd ON gd.team_id = g.id LEFT JOIN mdl_department d ON d.deleted = 0 AND d.id = gd.department_id
					WHERE pg.program_id = $this->programId AND pg.is_active = 1";

		$query  = $Query.$where." ORDER by CONCAT(IF((d.title IS NULL),'".get_string("global_team_identifier")."',''),g.name,IF((d.title IS NULL), IF(((pg.end_date = '') || (pg.end_date = 0) || pg.end_date IS NULL),'', FROM_UNIXTIME(pg.end_date,' - ".$CFG->customDefaultDateFormatForDB."')), CONCAT('(',d.title,')', IF(((pg.end_date = '') || (pg.end_date = 0) || pg.end_date IS NULL),'', FROM_UNIXTIME(pg.end_date,' - ".$CFG->customDefaultDateFormatForDB."'))))) ASC";
		//echo $query;die;
		$programTeamList = $DB->get_records_sql($query);
		$this->programTeamList = $programTeamList;

		$othersTeams  = $Query.$whereForOthers;
		$programOthersTeamList = $DB->get_records_sql($othersTeams);
		$this->programOthersTeamList = $programOthersTeamList;
	}
	public function programNonTeamList(){
		global $CFG,$DB,$USER;
		$where = '';
		if($this->name != ''){
			$where .= " AND (LOWER(g.name) LIKE '%".$this->name."%' || LOWER(d.title) LIKE '%".$this->name."%')";
		}

		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$where .= '';
				break;
			CASE $CFG->userTypeManager:
			
			     //$where .=	' AND g.createdby ='. $USER->id; 
			     $dGroups = getDepartmentGroups($USER->department);
				 $dGroupsStr = 0;
				 if(count($dGroups) > 0 ){
				   $dGroupsIds = array_keys($dGroups);
				   $dGroupsStr = implode(",", $dGroupsIds);
				 }
				
				 $where .=	' AND g.id IN('.$dGroupsStr.')';
				 break;
		}
		$where .= " AND g.is_active = 1 ";
		
		$Query = "SELECT g.id,CONCAT(IF((d.title IS NULL),'".get_string("global_team_identifier")."',''),g.name,IF((d.title IS NULL),'',CONCAT('(',d.title,')'))) as name FROM mdl_groups g LEFT JOIN mdl_program_group pg ON (g.id = pg.group_id AND pg.program_id = ".$this->programId." AND  pg.is_active = 1) LEFT JOIN mdl_group_department as gd ON gd.team_id = g.id LEFT JOIN mdl_department d ON d.id = gd.department_id WHERE pg.group_id IS NULL".$where." ORDER by CONCAT(IF((d.title IS NULL),'".get_string("global_team_identifier")."',''),g.name,IF((d.title IS NULL),'',CONCAT('(',d.title,')'))) ASC";

		$programNonTeamList = $DB->get_records_sql($Query);
		$this->programNonTeamList = $programNonTeamList;
	}
	public function programUserList(){
		global $CFG,$DB,$USER;
		$where = '';
		if($this->name != '')
		{
			$where = " AND (LOWER(u.firstname) LIKE '%".$this->name."%' || LOWER(u.lastname) LIKE '%".$this->name."%' || LOWER(u.username) LIKE '%".$this->name."%' || LOWER(CONCAT(u.firstname,' ',u.lastname)) LIKE '%".$this->name."%')";
		}
		
		//Code Added to add country filter on 15th feb 2016
                if($this->country !='' && $this->country !='-1'){
                    $countryExplode = explode('@',$this->country);
                    $countryCSV = '';
                    foreach($countryExplode as $countrycode){
                        $countryCSV .="'".$countrycode."',";
                    }
                    $countryCSV = trim($countryCSV,',');
                    $where .=" AND u.country IN(".$countryCSV.")";
                }
                //end code of country filter

                
		$where .= ($this->selMode!= '')?getSelectFilterCondition($this):'';
		
		$whereForOthers = '';
		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$where .= ' AND (r.name = "'.$CFG->userTypeManager.'" OR r.name = "'.$CFG->userTypeStudent.'")';
				break;
			CASE $CFG->userTypeManager:
					/*$where .=  " AND u.id IN(SELECT DISTINCT(u.id)
											FROM mdl_user AS u
											LEFT JOIN mdl_department_members AS dm ON dm.userid = u.id
											WHERE dm.departmentid IN (
											SELECT departmentid
											FROM mdl_department_members
											WHERE userid = $USER->id) OR u.createdby = $USER->id) AND r.name = '".$CFG->userTypeStudent."'";
					$whereForOthers .=  " AND u.id NOT IN(SELECT DISTINCT(u.id)
															FROM mdl_user AS u
															LEFT JOIN mdl_department_members AS dm ON dm.userid = u.id
															WHERE dm.departmentid IN (
															SELECT departmentid
															FROM mdl_department_members
															WHERE userid = $USER->id) OR u.createdby = $USER->id) AND (r.name = '".$CFG->userTypeManager."' OR r.name = '".$CFG->userTypeStudent."')";*/
															
				$where .=  " AND u.id IN(SELECT DISTINCT(u.id)
											FROM mdl_user AS u
											LEFT JOIN mdl_department_members AS dm ON dm.userid = u.id
											WHERE dm.departmentid IN (
											SELECT departmentid
											FROM mdl_department_members
											WHERE userid = $USER->id) )"; //AND r.name = '".$CFG->userTypeStudent."'
											
				$whereForOthers .=  " AND u.id NOT IN(SELECT DISTINCT(u.id)
															FROM mdl_user AS u
															LEFT JOIN mdl_department_members AS dm ON dm.userid = u.id
															WHERE dm.departmentid IN (
															SELECT departmentid
															FROM mdl_department_members
															WHERE userid = $USER->id)) AND (r.name = '".$CFG->userTypeManager."' OR r.name = '".$CFG->userTypeStudent."')";
				break;
		}
		$query = "SELECT u.id, CONCAT(u.firstname,' ', u.lastname,' (',u.username,')',IF(((p.end_date = '') || (p.end_date = 0) || p.end_date IS NULL),'', FROM_UNIXTIME(p.end_date,' - ".$CFG->customDefaultDateFormatForDB."')))  AS name,				p.type FROM mdl_user as u
					LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id
					LEFT JOIN mdl_role AS r ON ra.roleid = r.id
					LEFT JOIN mdl_program_user_mapping AS p ON u.id = p.user_id
					LEFT JOIN mdl_department_members as dm ON dm.userid = u.id AND dm.is_active = 1
					LEFT JOIN mdl_department as d ON dm.departmentid = d.id
					WHERE u.deleted = 0 AND u.suspended = 0 AND p.program_id = $this->programId AND p.type = 'user' AND p.`status` = 1  AND d.`status` = 1 AND d.deleted = 0 ";
		$orderBy = " ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname";
		$select = $query.$where.$orderBy;
		$programUserList = $DB->get_records_sql($select);
		$this->programUserList = $programUserList;

		$queryOthers = $query.$whereForOthers.$orderBy;
		$programOthersUserList = $DB->get_records_sql($queryOthers);
		$this->programOthersUserList = $programOthersUserList;
		
	}
	public function programNonUserList(){
		global $CFG,$DB,$USER; 
		$where = '';
		
		if($this->name != '')
		{
			$where = " AND (LOWER(u.firstname) LIKE '%".$this->name."%' || LOWER(u.lastname) LIKE '%".$this->name."%' || LOWER(u.username) LIKE '%".$this->name."%' || LOWER(CONCAT(u.firstname,' ',u.lastname)) LIKE '%".$this->name."%')";
		}
		
//Code Added to add country filter on 15th feb 2016
                if($this->country !='' && $this->country !='-1'){
                    $countryExplode = explode('@',$this->country);
                    $countryCSV = '';
                    foreach($countryExplode as $countrycode){
                        $countryCSV .="'".$countrycode."',";
                    }
                    $countryCSV = trim($countryCSV,',');
                    $where .=" AND u.country IN(".$countryCSV.")";
                }
                //end code of country filter
		
		$where .= ($this->selMode!= '')?getSelectFilterCondition($this):'';
		
		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$where .= ' AND (r.name = "'.$CFG->userTypeManager.'" OR r.name = "'.$CFG->userTypeStudent.'")';
				break;
			CASE $CFG->userTypeManager:
					/*$where .=  " AND u.id IN(SELECT DISTINCT(u.id)
											FROM mdl_user AS u
											LEFT JOIN mdl_department_members AS dm ON dm.userid = u.id
											WHERE dm.departmentid IN (
											SELECT departmentid
											FROM mdl_department_members
											WHERE userid = $USER->id) OR u.createdby = $USER->id) AND r.name = '".$CFG->userTypeStudent."'";*/
											
					$where .=  " AND u.id IN(SELECT DISTINCT(u.id)
											FROM mdl_user AS u
											LEFT JOIN mdl_department_members AS dm ON dm.userid = u.id
											WHERE dm.departmentid IN (
											SELECT departmentid
											FROM mdl_department_members
											WHERE userid = $USER->id) )"; // AND r.name = '".$CFG->userTypeStudent."'
				break;
		}
		$query = "	SELECT u.id, CONCAT(u.firstname,' ', u.lastname,' (',u.username,')')  AS name,				p.type
					FROM mdl_user AS u
					LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id
					LEFT JOIN mdl_role AS r ON ra.roleid = r.id
					LEFT JOIN mdl_program_user_mapping AS p ON (u.id = p.user_id AND p.program_id = $this->programId AND p.status = 1 AND p.type = 'user')
					LEFT JOIN mdl_department_members as dm ON dm.userid = u.id AND dm.is_active = 1
                    LEFT JOIN mdl_department as d ON dm.departmentid = d.id
					WHERE p.id IS NULL AND u.deleted = 0 AND u.suspended = 0  AND d.`status` = 1 AND d.deleted = 0 $where
					ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
		$programNonUserList = $DB->get_records_sql($query);
		$this->programNonUserList = $programNonUserList;
		
	}
	
	public function getProgramUserList(){
	  $this->programUserList();
	  if($USER->archetype == $CFG->userTypeManager){
	    $programUsers = $this->programUserList;
	  }else{
	    $programUsers = $this->programOthersUserList;
	  }
	  return $programUsers;
	}
	
	public function getProgramNonUserList(){
	  $this->programNonUserList();
	  return $this->programNonUserList;
	}
	
	public function programDepartmentList(){
		global $CFG,$DB;
		$where = '';
		if($this->name != ''){
			$where .= " AND (LOWER(d.title) LIKE '%".$this->name."%')";
		}
		
		$where .= externalCheckForDepartment();
        $extDeptIdentifier = get_string('external_department_identifier', 'department');		
		$query = "	SELECT d.id,(if(d.is_external = 1, concat('".$extDeptIdentifier."', d.title), d.title)) as name,d.is_external FROM mdl_department as d
					LEFT JOIN mdl_program_department as pd ON d.id = pd.department_id
					WHERE pd.program_id = $this->programId AND pd.is_active = 1".$where;
		
		$programDepartmentList = $DB->get_records_sql($query);
		$this->programDepartmentList = $programDepartmentList;
	}
	public function programNonDepartmentList(){
		global $CFG,$DB;
		$where = '';
		if($this->name != ''){
			$where .= " AND (LOWER(d.title) LIKE '%".$this->name."%')";
		}

        $where .= externalCheckForDepartment();
		$extDeptIdentifier = get_string('external_department_identifier', 'department');		
		$query = "	SELECT d.id,(if(d.is_external = 1, concat('".$extDeptIdentifier."', d.title), d.title)) as name,d.is_external
					FROM mdl_department d
					LEFT JOIN mdl_program_department pd ON (d.id = pd.department_id AND pd.program_id = ".$this->programId." AND pd.is_active = 1)
					WHERE pd.department_id IS NULL AND d.deleted = 0 AND d.`status` = 1 $where ORDER BY d.title ASC";
		$programNonDepartmentList = $DB->get_records_sql($query);
		$this->programNonDepartmentList = $programNonDepartmentList;
	}
	public function program_option_list($block = 'team_program', $return = false){
		global $USER,$CFG,$DB;
		$programOthers = array();
		$chkOwner = 0;
		if($block == 'team_program'){
			$programs = $this->programTeamList;
			$programOthers = $this->programOthersTeamList;
			$header = get_string('teams');
			$chkOwner = 1;
		}elseif($block == 'non_team_program'){
			$programs = $this->programNonTeamList;
			$header = get_string('teams');
		}elseif($block == 'department_program'){
			$programs = $this->programDepartmentList;
			$programOthers = $programs;
			$header = get_string('departments');
		}elseif($block == 'non_department_program'){
			$programs = $this->programNonDepartmentList;
			$header = get_string('departments');
		}elseif($block == 'user_program'){
			$programs = $this->programUserList;
			$programOthers = $this->programOthersUserList;
			$header = get_string('users');
		}elseif($block == 'non_user_program'){
			$programs = $this->programNonUserList;
			$header = get_string('users');
		}
		$option = '';
		if(!empty($programs)){
			$programsCount = count($programs);
			if($USER->archetype != $CFG->userTypeAdmin){
				//$programsCount += count($programOthers);
			}
			$option .= '<optgroup label="'.$header.' ('.$programsCount.')">';
			foreach($programs as $program){
				$disable = '';
				if($block == 'user_program'){
					if($program->type != 'user'){
						$disable = "class = 'option-disable'";
					}
				}
				if($chkOwner == 1 && $USER->archetype != $CFG->userTypeAdmin && $program->dep_id != $USER->department && $program->createdby != $USER->id){
					$disable = "disabled = 'disabled'";
				}
				$option .= "<option value = '".$program->id."' ".$disable." >".$program->name."</option>";
			}
		}else{
			$option .= '<optgroup label="None">';
		}
		if($USER->archetype == $CFG->userTypeManager){
			if(!empty($programOthers)){
				foreach($programOthers as $programOther){
					//$option .= "<option value = '".$programOther->id."' disabled = 'disabled' >".$programOther->name."</option>";
				}
			}
		}
		$option .= '</optgroup>';
		
		if($return){
		  return $option;
		}else{
		  echo $option;
		}
		
	}
}
