<?php
require_once('../../config.php');
require_once($CFG->dirroot . '/local/user/selector/lib.php');

$userid = required_param('userid', PARAM_INT);
$searchText = optional_param('search_text', '', PARAM_RAW);
$action = optional_param('action', 0, PARAM_ALPHA);
if($action == "searchNonCourse"){
	
	$courses = new user_courses_selector($searchText, array('userid' => $userid));
	$courses->get_non_user_courses();
	$courses->course_option_list(1);
}
if($action == "searchUserCourse"){
	
	$courses = new user_courses_selector($searchText, array('userid' => $userid));
	$courses->get_user_courses();
	$courses->course_option_list(2);
}
?>