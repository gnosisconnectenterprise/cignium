<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * A form for the creation and editing of programs.
 *
 * @copyright 2006 The Open University, N.D.Freear AT open.ac.uk, J.White AT open.ac.uk
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package   core_program
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot.'/lib/formslib.php');

// for course setting only
/*require_once($CFG->dirroot . '/local/user/selector/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->libdir . '/filelib.php');*/
// end

/**
 * Program form class
 *
 * @copyright 2006 The Open University, N.D.Freear AT open.ac.uk, J.White AT open.ac.uk
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package   core_program
 */
class program_form extends moodleform {

    /**
     * Definition of the form
     */
    function definition () {
			global $USER, $CFG, $OUTPUT, $PAGE;
			$mform =& $this->_form;
			$editoroptions = $this->_customdata['editoroptions'];
			$programData = $this->_customdata['programdata'];		
			$mform->addElement('header', 'programimage', get_string('programimage', 'program'));
			if ($filesOptions = programOverviewFilesOptions($programData)) {
				$mform->addElement('filemanager', 'testimage_filemanager', get_string('uploadimage', 'program'), null, $filesOptions);
			}		
			$showResetButton = true;
			if(!empty($programData->id)){
			  $showResetButton = false;
			}
			$this->add_action_buttons(true, null, $showResetButton);

    }

    /**
     * Form validation
     *
     * @param array $data
     * @param array $files
     * @return array $errors An array of errors
     */
    function validation($data, $files) {
        global $COURSE, $DB, $CFG;
        return $errors;
    }

    /**
     * Get editor options for this form
     *
     * @return array An array of options
     */
    function get_editor_options() {
        return $this->_customdata['editoroptions'];
    }
}
