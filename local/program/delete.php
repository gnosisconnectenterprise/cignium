<?php

require_once('../../config.php');
require_once($CFG->dirroot.'/program/programlib.php');

// Get and check parameters

$programids = required_param('programs', PARAM_INT);
$confirm = optional_param('confirm', 0, PARAM_BOOL);

 $paramArray = array(
					'ch' => optional_param('ch', '', PARAM_ALPHA),
					'key' => optional_param('key', '', PARAM_RAW),
					'name' => optional_param('name', '', PARAM_RAW),
					'des' => optional_param('des', '', PARAM_RAW)
					);
					
$PAGE->set_url('/local/program/delete.php', array('programs'=>$programids));
$PAGE->set_pagelayout('globaladmin');

$manageprograms = get_string('manageprograms','program');
$PAGE->navbar->add($manageprograms, new moodle_url('/program/index.php'));
$PAGE->navbar->add(get_string('deleteprogram','program'));


// Make sure all programs are OK 
$programidarray = explode(',',$programids);
$programnames = array();
foreach($programidarray as $programid) {
    if (!$program = $DB->get_record('programs', array('id' => $programid))) {
        print_error('invalidprogramid');
    }
    $programnames[] = format_string($program->name);
}

$queryString = http_build_query($paramArray);

if($queryString){
  $queryString = '?'.$queryString;
}
$returnurl = $CFG->wwwroot.'/program/index.php'.$queryString;

if(count($programidarray)==0) {
    print_error('errorselectsome','program',$returnurl);
}

if ($confirm ) {
    if (!confirm_sesskey() ) {
        print_error('confirmsesskeybad','error',$returnurl);
    }

    foreach($programidarray as $programid) {
        deleteProgram($programid);
    }
	$_SESSION['update_msg']=get_string('record_deleted');
    redirect($returnurl);
} else {
    $PAGE->set_title(get_string('deleteselectedprogram', 'program'));
    $PAGE->set_heading($SITE->fullname . ': '. get_string('deleteselectedprogram', 'program'));
    echo $OUTPUT->header();
    $optionsyes = array('programs'=>$programids, 'sesskey'=>sesskey(), 'confirm'=>1);
    if(count($programnames)==1) {
        $message=get_string('deleteprogramconfirm', 'program', $programnames[0]);
    } else {
        $message=get_string('deleteprogramsconfirm', 'program').'<ul>';
        foreach($programnames as $programname) {
            $message.='<li>'.$programname.'</li>';
        }
        $message.='</ul>';
    }
    $formcontinue = new single_button(new moodle_url('delete.php', $optionsyes), get_string('yes'), 'post');
    $formcancel = new single_button(new moodle_url($returnurl, array()), get_string('no'), 'get');
    echo $OUTPUT->confirm($message, $formcontinue, $formcancel);
    echo $OUTPUT->footer();
}
