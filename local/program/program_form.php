<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * A form for the creation and editing of programs.
 *
 * @copyright 2006 The Open University, N.D.Freear AT open.ac.uk, J.White AT open.ac.uk
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package   core_program
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot.'/lib/formslib.php');
require_once($CFG->dirroot.'/program/programlib.php');

// for course setting only
/*require_once($CFG->dirroot . '/local/user/selector/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->libdir . '/filelib.php');*/
// end

/**
 * Program form class
 *
 * @copyright 2006 The Open University, N.D.Freear AT open.ac.uk, J.White AT open.ac.uk
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package   core_program
 */
class program_form extends moodleform {

    /**
     * Definition of the form
     */
    function definition () {
			global $USER, $CFG, $OUTPUT, $PAGE;
			
			
			$mform =& $this->_form;
			$editoroptions = $this->_customdata['editoroptions'];
			$programData = $this->_customdata['programdata'];
			$mform->addElement('html','<div class="filedset_outer ">');
			$mform->addElement('header', 'general', get_string('general', 'form'));
			
			/*if(!empty($programData->id)){Created On
				if($programData->picture != ''){
					$view =  $CFG->dirroot.'/theme/gourmet/pix/program/'.$programData->picture;
					$view1 =  $CFG->wwwroot.'/theme/gourmet/pix/program/'.$programData->picture;
					//echo $view;die;
					if(file_exists($view) && $programData->picture !=''){
						$mform->addElement('html','<div class="fitem"><div class="fitemtitle"><label for="id_image">'.get_string("current_image").'</label></div>');
						$mform->addElement('html','<div class="felement fstatic"><img width="64" height="64" class="userpicture defaultuserpic" src="'.$view1.'">');
						//$mform->addElement('html','<div class="felement fimage">');
						$mform->addElement('html','<a class = "removeimage" rel = "'.$programData->id.'"><span id="'.$programData->id.'" rel="login">'.get_string("delete").'</span></a>');
						$mform->addElement('html','</div></div>');
					}
				}
			}*/
			
			$mform->addElement('text','name', get_string('name', 'program'),'maxlength="254" size="50" rows="10" cols="5"');
			//$mform->addHelpButton('name', 'fullnameprogram');
			$mform->addRule('name', get_string('required'), 'required', null, 'client');
			$mform->setType('name', PARAM_TEXT);
			
			$mform->addElement('text','idnumber', get_string('programid', 'program'),'maxlength="100"  size="10"');
			$mform->setType('idnumber', PARAM_RAW);
			if($CFG->isCertificate==1){
				$mform->addElement('checkbox', 'is_certificate', get_string('iscertificaterequired'),'');
				$mform->setDefault('is_certificate', 1);
			}
			$mform->addElement('editor', 'description_editor', get_string('description', 'program'), null, $editoroptions);
			//$mform->addHelpButton('description_editor', 'programsummary');
			$mform->setType('description_editor', PARAM_RAW);
			
			/*$mform->addElement('textarea', 'description', get_string('description', 'program'), 'maxlength="254" style="width:650px;height:200px;" ');
			$mform->setType('description', PARAM_RAW);*/
			
			$mform->addElement('editor','suggesteduse_editor', get_string('suggested_use'), null, $editoroptions);
			//$mform->addHelpButton('suggested_use', 'coursesummary');
			$mform->setType('suggesteduse_editor', PARAM_RAW);
			$summaryfields = 'suggesteduse_editor';
	
	
			$mform->addElement('editor','learningobj_editor', get_string('learning_obj'), null, $editoroptions);
			//$mform->addHelpButton('learning_obj', 'coursesummary');
			$mform->setType('learningobj_editor', PARAM_RAW);
			$summaryfields = 'learningobj_editor';

			$mform->addElement('editor','performanceout_editor', get_string('performance_out'), null, $editoroptions);
			//$mform->addHelpButton('performance_out', 'coursesummary');
			$mform->setType('performanceout_editor', PARAM_RAW);
			$summaryfields = 'performanceout_editor';
			if($programData->id){
			$created_on = getDateFormat($programData->timecreated, $CFG->customDefaultDateFormat);
			$mform->addElement('html','<div class="fitem "><div class="fitemtitle"><div class="fstaticlabel"><label>'.get_string("timecreated","program").'</label></div></div><div class="felement fstatic">'.$created_on.'</div></div>');
			}
			
			$mform->addElement('header', 'referencematerial', get_string('referencematerial', 'program'));
			//if ($filesOptions = programOverviewFilesOptions($programData)) {
				$mform->addElement('filemanager', 'programmaterial_filemanager', get_string('uploadreferencematerial', 'program'), null, array('subdirs'=>0));
				//$mform->addHelpButton('programmaterial_filemanager', 'uploadreferencematerial', 'programmaterial');
			//}
			
			
			 $mform->addElement('header', 'programimage', get_string('programimage', 'program'));
			 if ($filesOptions = programOverviewFilesOptions($programData)) {
			   $mform->addElement('filemanager', 'programimage_filemanager', get_string('uploadimage', 'program'), null, $filesOptions);
			 }
			 //$mform->addHelpButton('programimage_filemanager', 'programimage');
			
			if($USER->archetype == $CFG->userTypeAdmin && (isset($programData->id) || !empty($programData->id))){
				global $DB;
				$mform->addElement('header','ownership', get_string('others'));
				$userDepartment = $DB->get_record('department_members',array('userid'=>$programData->createdby,'is_active'=>1));
				$userRole = array($CFG->userTypeManager,$CFG->userTypeAdmin);
				if(empty($userDepartment)){
					$userList = getDepartmentManagerAdmin(0);
				}else{
					$userList = getDepartmentManagerAdmin($userDepartment->departmentid);
				}
				//$changeownership = array(0=>get_string('select_owner'));
				if(!empty($userList)){
					foreach($userList as $user){
						$changeownership[$user->id] = $user->firstname.' '.$user->lastname;
					}
				}
				$mform->addElement('select', 'changeownership', get_string('programownership'), $changeownership);
				$mform->getElement('changeownership')->setSelected($programData->createdby);
			}
			$mform->addElement('hidden','id');
			$mform->setType('id', PARAM_INT);
			
			
			######################### For course setting only  #########################################
			
			
			//$mform->addElement('header', 'courseformathdr', get_string('allocationsettings', 'plugin'));
			//require_once("program_allocation_settings.php");
			
			######################### End for course setting only  ######################################### 
		    
			
			$showResetButton = true;
			if(!empty($programData->id)){
			  $showResetButton = false;
			}
			$this->add_action_buttons(true, null, $showResetButton);
			$mform->addElement('html','</div>');
    }

    /**
     * Form validation
     *
     * @param array $data
     * @param array $files
     * @return array $errors An array of errors
     */
    function validation($data, $files) {
        global $COURSE, $DB, $CFG;

        $errors = parent::validation($data, $files);
       if(trim($data['name']) == ''){
			$errors['name'] = get_string('required');
	   }
        /*if ($data['id'] and $program = $DB->get_record('programs', array('id'=>$data['id']))) {
            if (core_text::strtolower($program->name) != core_text::strtolower(trim($data['name']))) {
                if (isProgramAlreadyExist(trim($data['name']))) {
                    $errors['name'] = get_string('programnameexists', 'program', $data['name']);
                }
            }

        } else if (isProgramAlreadyExist(trim($data['name']))) {
            $errors['name'] = get_string('programnameexists', 'program', $data['name']);
        } */
		
		if ($data['id'] and $program = $DB->get_record('programs', array('id'=>$data['id']))) {
	
            if (core_text::strtolower($program->idnumber) != core_text::strtolower(trim($data['idnumber']))) { 
                if (isProgramIdAlreadyExist(trim($data['idnumber']))) {
                    $errors['idnumber'] = get_string('programidnumberexists', 'program', $data['idnumber']);
                }
            }

        } else if (isProgramIdAlreadyExist(trim($data['idnumber']))) {
            $errors['idnumber'] = get_string('programidnumberexists', 'program', $data['idnumber']);
        }
		
		if(!empty($_FILES) && $_FILES["program_image"]["error"] == 0){
				$imgname = $_FILES["program_image"]["tmp_name"];
				$size = getimagesize($imgname);
				if($size[0]>300 || $size[1]>300){
					$errors['program_image'] = get_string('filedimloginprogramdepartment','program');
				}
				$filecheck = basename($_FILES["program_image"]['name']);
				$ext = substr($filecheck, strrpos($filecheck, '.') + 1);
				if ($_FILES["program_image"]['name'] != '' && ($ext == "jpg" || $ext == "gif" || $ext == "png") && ($_FILES["program_image"]["type"] == "image/jpeg" || $_FILES["program_image"]["type"] == "image/gif" || $_FILES["program_image"]["type"] == "image/png")){
				}
				else{
					$errors["program_image"] = get_string('supportedfiletype','program');
				}
		}
        return $errors;
    }

    /**
     * Get editor options for this form
     *
     * @return array An array of options
     */
    function get_editor_options() {
        return $this->_customdata['editoroptions'];
    }
}
