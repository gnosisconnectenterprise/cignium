<?php 

   	defined('MOODLE_INTERNAL') || die();
	
		
	/**
	 *  This function is using for downloading a file 
	 *  @param string $filepath is path of file to be downloaded
	 *  @return download the file 
	*/
	

	function forceDownload($filePath){
	
	 if($filePath){
		$file = $filePath; //file location
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.basename($file).'"');
		header('Content-Length: ' . filesize($file));
		readfile($file);
		die;
	 }
	
	}
	
	/**
	 * get course name or all results
	 *
	 * @param mixed $courseId is course id
	 * @param int $courseTypeId is course type id
	 * @param int $return it is form of result
	 * @param string $sort is sorting field
	 * @param string $dir is sorting order
	 * @return string $result course name or all results (if $courseId is null)
	 */
	 	 
	function getCourses($courseId='', $courseTypeId = '', $return = 1, $sort='fullname', $dir = 'ASC'){
	
		global $DB, $CFG;
		$result = '';
		$courseTypeId = $courseTypeId?$courseTypeId:$CFG->courseTypeOnline;
		$fullNameField = extractMDashInSql('fullname');
		if(is_array($courseId)){
		  $courseIds = count($courseId) > 0 ? implode(",",$courseId) : 0;
		  if($courseIds){
		    if($return == 1){
			  $query = "select GROUP_CONCAT($fullNameField ORDER BY fullname ASC SEPARATOR ', ' ) FROM {$CFG->prefix}course WHERE id IN (".$courseIds.") " ;
		      $result = $DB->get_field_sql($query);
			}elseif($return == 2){
			
			   $query = "SELECT mc.*, mcc.name as categoryname, $fullNameField AS fullname FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}course_categories mcc on (mc.category = mcc.id) where mc.is_active='1' AND mc.publish='1' AND mc.category!='0' AND mc.coursetype_id = '".$courseTypeId."' AND mc.id NOT IN (1) AND mc.deleted = '0' ORDER BY mc.{$sort} $dir";
			
			   $result = $DB->get_records_sql($query);
			}
		  }
		}else{
			if($courseId){
			   $result = $DB->get_field_sql("select $fullNameField AS fullname FROM {$CFG->prefix}course  WHERE id IN (".$courseId.") ORDER BY $sort $dir ");
			}else{
			   $result = $DB->get_records_sql("select *,$fullNameField AS fullname FROM {$CFG->prefix}course  WHERE coursetype_id = '".$courseTypeId."' AND id NOT IN (1) AND deleted = '0' ORDER BY $sort $dir ");
			}
        }
		
		return $result;
	}
	
	
		
	/**
	 * get course category name or all results
	 *
	 * @param mixed $catId is course category id
	 * @return string $result course category name or all results (if $catId is null)
	 */
	 
	 
	function getCourseCategory($catId=''){
	
		global $DB, $CFG;
		$result = '';
		
		if(is_array($catId)){
		  $catIds = count($catId) > 0 ? implode(",",$catId) : 0;
		  if($catIds){
		    $result = $DB->get_field_sql("select GROUP_CONCAT(name ORDER BY name ASC SEPARATOR ', ' ) FROM {$CFG->prefix}course_categories WHERE id IN (".$catIds.") ");
		  }
		}else{
			if($catId){
			   $result = $DB->get_field_sql("select name FROM {$CFG->prefix}course_categories  WHERE id IN (".$catId.") ORDER BY name ASC ");
			}else{
			   $result = $DB->get_records_sql("select * FROM {$CFG->prefix}course_categories ORDER BY name ASC ");
			}
        }
		
		return $result;
	}
	
	
	
	/**
	 * This function is using for getting learner course
	 * @global object
	 * @param int $moduleId id of module
	 * @param string $sort A SQL snippet for the sorting criteria to use
	 * @param int $userId id of user
	 * @param string $page The page or records to return
	 * @param string $recordsPerPage The number of records to return per page
	 * @param string $search A simple string to search for
	 * @param string $extraSelect An additional SQL select statement to append to the query
	 * @param array $extraParams Additional parameters to use for the above $extraSelect
	 * @param int $type is course status like not started/in progress/ completed
	 * @param int $courseTypeId is a type of course like online or classroom course
	 * @return array of learner course
	 */
	
	
	
	function getAssignedCourseForUser($moduleId, $sort='id', $userId = 0, $dir='ASC', $page=1, $recordsPerPage=0,
							   $search='', $extraSelect='', array $extraParams=null, $courseTypeId='') {
		global $DB, $CFG;
		$select = " 1 = 1";
		$courseTypeId = $courseTypeId?$courseTypeId:$CFG->courseTypeOnline;
	
		$params = array();
		$records = array();
	
		$offset = $page - 1;
		$offset = $offset*$recordsPerPage;
		$limit = '';
		if($recordsPerPage != 0){
			$limit = " LIMIT $offset, $recordsPerPage";
		}


	
	   if($moduleId){
	
			   if (!empty($search)) {
			
					$search = trim($search);
					$select .= " AND (mc.idnumber = :search1 OR mc.fullname = :search2 OR mc.shortname = :search3 OR mc.summary = :search4 OR mcc.name = :search5 )";
					$params['search1'] = "%$search%";
					$params['search2'] = "%$search%";
					$params['search3'] = "$search";
					$params['search4'] = "$search";
					$params['search5'] = "$search";
					
				}
				
				//echo $extraSelect."<br>";
			
				if ($extraSelect) {
					$select .= " $extraSelect";
					//$params = $params + (array)$extraParams;
				}
				
				if ($sort) {
					$sort = " ORDER BY $sort $dir";
				}
				
				
				//pr($arr);
				if($userId){
				   
				   $courseStatusArr = getCourseIdByStatus($userId);
				   
				   $courseIdsArr = $courseStatusArr['allids'];
				 
				   if(count($courseIdsArr) > 0 ){
					 
                    
					 $courseIds = implode(",", $courseIdsArr);
					 $select = " AND mc.id IN ($courseIds)";
					 
					 if($type){
					 
					    if(count($courseStatusArr['type'][$type]) > 0){
					  
							$courseIds = implode(",", $courseStatusArr['type'][$type]);
							$select = " AND mc.id IN ($courseIds)";

							 if($moduleId == 18){	
						
									$query = "SELECT mc.*, mc.format as courseformat, mss.launch, mss.id as scormid, mss.name as scormname, mss.intro as scormsummary, mss.timeopen as scormtimeopen, mss.timeclose as scormtimeclose, mss.width, mss.height, mss.options, mcc.id as catid, mcc.name as categoryname, mc.timecreated as ctimecreated ";
									
									$element = 'cmi.core.score.raw';
									$query .= "  ,(SELECT  MAX(value) FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' ) WHERE msst.userid = '".$userId."' AND element = '".$element."' AND ms.id = mss.id AND mc.coursetype_id = '".$courseTypeId."' LIMIT 1)  as score";
									
									
									$element = 'cmi.core.total_time';
									$query .= "  ,(SELECT  value FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' ) WHERE msst.userid = '".$userId."' AND  element = '".$element."' AND ms.id = mss.id  AND mc.coursetype_id = '".$courseTypeId."' ORDER BY msst.id DESC LIMIT 1 )  as timespent";
									
									$element = "'cmi.core.lesson_status','cmi.completion_status'";
									$query .= "  ,(SELECT  value FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' )  WHERE msst.userid = '".$userId."' AND  element in (".$element.") AND ms.id = mss.id AND mc.coursetype_id = '".$courseTypeId."'  ORDER BY msst.id DESC LIMIT 1 )  as scormstatus";
									
									//$element = 'x.start.time';
									$query .= "  ,(  if((SELECT  msst.timemodified FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' ) WHERE msst.userid = '".$userId."' AND  element = 'cmi.core.score.raw' AND ms.id = mss.id  AND mc.coursetype_id = '".$courseTypeId."' ORDER BY msst.id DESC LIMIT 1)!='', 
									
									(SELECT  msst.timemodified FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' ) WHERE msst.userid = '".$userId."' AND  element = 'cmi.core.score.raw' AND ms.id = mss.id  AND mc.coursetype_id = '".$courseTypeId."' ORDER BY msst.id DESC LIMIT 1),
									
									(SELECT  value FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' ) WHERE msst.userid = '".$userId."' AND  element = 'x.start.time' AND ms.id = mss.id  AND mc.coursetype_id = '".$courseTypeId."' ORDER BY msst.id DESC LIMIT 1)
									 )
									
									 )  as lastaccessed";
									
									
									$query .= "  FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm mss ON (mc.id = mss.course) LEFT JOIN {$CFG->prefix}course_categories mcc ON (mc.category = mcc.id)";

									$query .= "  LEFT JOIN {$CFG->prefix}course_modules as mcm  ON (mcm.course = mc.id AND mcm.module = '$moduleId')";

									$query .= "  WHERE 1 = 1 AND mc.coursetype_id = '".$courseTypeId."' ";
									$query .= $select;
									
									
									$element = "'cmi.core.lesson_status','cmi.completion_status'";

									$query .= "  AND mc.format = 'topics' ";
									$query .= "  AND mcm.module = '$moduleId' ";
									$query .= " GROUP BY mc.id $sort $limit";
	
								}elseif($moduleId == '17'){
								
									 $downloaded = get_string('downloaded','learnercourse');
									 $notdownloaded = get_string('notdownloaded','learnercourse');
									 
									 $query = "SELECT mc.*, mc.format as courseformat, mr.id as resourceid, mr.name as resoursename, mr.intro as resourcesummary, mcc.id as catid, mcc.name as categoryname, mc.timecreated as ctimecreated,  mcm.id as cmid , mula.timeaccess as lastaccessed, if(mula.timeaccess!='', '$downloaded', '$notdownloaded') as rstatus " ;

									 $query .= "  FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}resource mr ON (mc.id = mr.course) LEFT JOIN {$CFG->prefix}course_categories mcc ON (mc.category = mcc.id) LEFT JOIN {$CFG->prefix}course_modules mcm ON (mc.id = mcm.course)  LEFT JOIN {$CFG->prefix}user_noncourse_lastaccess mula ON (mc.id = mula.courseid AND mr.id = mula.resource_id AND mula.userid = '".$userId."') WHERE 1 = 1 $select  AND mcm.module = '$moduleId' ";
									 
									 //if($type == ''){
									  // $query .= "  AND mc.format = 'singleactivity' ";
									   $query .= "  AND mc.format = 'topics' ";
									   $query .= "  AND mc.coursetype_id = '".$courseTypeId."' ";
									// }
									 
									 $query .= "   $sort $limit";
										   
								}
								
								
				
								//$records = $DB->get_records_sql($query, $params, $page, $recordsPerPage);
								$records = $DB->get_records_sql($query);
								
						
								
						}
					 }
					  
				   }
				}
								

		
		}
		
		//pr($records);die;
		return $records;
	
	}
	
	/**
	 * This function is using for getting count of learner course 
	 * @global object
	 * @param int $moduleId id of module
	 * @param int $userId id of user
	 * @param int $type is course status like not started/in progress/ completed
	 * @param int $courseTypeId is a type of course like online or classroom course
	 * @return int count of learner course
	 */
	
	
	function getAssignedCourseForUserCount($module, $userId = 0, $type, $courseTypeId='') {
	
	   global $DB, $CFG;
	
	   $count = 0;
	   $select = " 1 = 1";
	   $records = array();
	   $courseTypeId = $courseTypeId?$courseTypeId:$CFG->courseTypeOnline;
		
	   if($module) {
	   
	   
				 if($userId){
					   
					   $courseStatusArr = getCourseIdByStatus($userId);
					   $courseIdsArr = $courseStatusArr['allids'];
					   if(count($courseIdsArr) > 0 ){
						 
						   $courseIds = implode(",", $courseIdsArr);
						   $select = " AND mc.id IN ($courseIds)";
		
					 
						   if($type){
						 
							  if(count($courseStatusArr['type'][$type]) > 0){
							  
									$courseIds = implode(",", $courseStatusArr['type'][$type]);
									$select = " AND mc.id IN ($courseIds)";
										 if($module == 18){	
							
											$element = "'cmi.core.lesson_status','cmi.completion_status'";
											$query = "SELECT mc.* ";
											$query .= "  FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm mss ON (mc.id = mss.course) LEFT JOIN {$CFG->prefix}course_categories mcc ON (mc.category = mcc.id)";
												$query .= "  LEFT JOIN {$CFG->prefix}course_modules as mcm  ON (mcm.course = mc.id AND mcm.module = '$module')";
												$query .= "  WHERE 1 = 1 $select ";
												$query .= "  AND mc.coursetype_id = '".$courseTypeId."' ";
												$query .= "  AND mc.format = 'topics' ";
													  //echo $query;die;
													  
												$records = $DB->get_records_sql($query);
											
											 
											 }else if($module == 17){ // for non course matterial
											 
												 $query = "SELECT mc.* " ;
												
												 $query .= "  FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}resource mr ON (mc.id = mr.course) LEFT JOIN {$CFG->prefix}course_categories mcc ON (mc.category = mcc.id) LEFT JOIN {$CFG->prefix}course_modules mcm ON (mc.id = mcm.course)  LEFT JOIN {$CFG->prefix}user_noncourse_lastaccess mula ON (mc.id = mula.courseid AND mr.id = mula.resource_id AND mula.userid = '".$userId."') WHERE 1 = 1 $select  AND mcm.module = '$module' ";
										 
												 //if($type == ''){
												   //$query .= "  AND mc.format = 'singleactivity' ";
												   $query .= "  AND mc.format = 'topics' ";
												   $query .= "  AND mc.coursetype_id = '".$courseTypeId."' ";
												 //}
												 
												
										 
											   // for resource
								 
											   $records = $DB->get_records_sql($query); 
								
										  }
						  
						           }
						       }
					   }
				 }
 
		} 
		
		$count = count($records);
		return $count;
	  
	}
	
	
	
	function getNonCourseStatusForUser($userId){
	
		 global $DB, $CFG;
		 $moduleId = 17;
		 $completed = $CFG->completed;
		 $notstarted = $CFG->notstarted;
		 $query = "SELECT mr.id, mc.id as courseid, if(mula.timeaccess!='','$completed','$notstarted') as resourse_status" ;
		 $query .= "  FROM {$CFG->prefix}resource mr LEFT JOIN {$CFG->prefix}course mc ON (mc.id = mr.course) LEFT JOIN {$CFG->prefix}course_categories mcc ON (mc.category = mcc.id) LEFT JOIN {$CFG->prefix}course_modules mcm ON (mc.id = mcm.course)  LEFT JOIN {$CFG->prefix}user_noncourse_lastaccess mula ON (mc.id = mula.courseid AND mr.id = mula.resource_id AND mula.userid = '".$userId."')  WHERE mcm.module = '$moduleId' AND mc.format = 'topics'";
					 
		$records = $DB->get_records_sql($query);
	
		return $records;
					 
	
	}
	
	/**
	 * This function is using for getting topic section for learner
	 * @param int $courseId id of course
	 * @param int $userId id of current user
	 * @param int $type is course status like not started/in progress/ completed
	 * @param int $courseTypeId is a type of course like online or classroom course
	 * @return object array $topicsCourseArr
	 */
	
	function getTopicsSection($courseId, $userId, $type='', $courseTypeId = ''){
	
	  global $DB, $CFG;
	  $sectionArr = array();
	  $courseArr = array();
	  $courseTypeId = $courseTypeId?$courseTypeId:$CFG->courseTypeOnline;
	  if($courseId && $userId ){
	
			   $querySection = "SELECT mcs.id as sectionid, mcs.section as section, if(mcs.name!='', mcs.name, concat('Topic',' ',mcs.section)) as sectionname, mcs.summary as summary, mcs.sequence " ;
			   $querySection .= "  FROM {$CFG->prefix}course_sections mcs LEFT JOIN {$CFG->prefix}course mc ON (mcs.course = mc.id) WHERE mcs.visible = '1' AND mcs.section!='0'  AND mcs.sequence!=''  AND mcs.course = '$courseId' ";
			   $querySection .= "  AND mc.coursetype_id = '".$courseTypeId."' ";
			   $querySection .= "  ORDER BY mcs.section ASC ";
				   
               //echo $querySection;
			   $sectionArr = $DB->get_records_sql($querySection);
			
			   if(count($sectionArr) > 0){
			   
					foreach($sectionArr as $sectionid=>$sections){
			   
						 $courseArr[$sectionid]->section = $sections->section;
						 $courseArr[$sectionid]->sectionname = $sections->sectionname;
						 $courseArr[$sectionid]->summary = $sections->summary;
						 $courseArr[$sectionid]->sequence = $sections->sequence;
				 
				         if($sections->sequence && $sectionid){
							 $queryCM = "SELECT mcm.* " ;
							 $queryCM .= " FROM {$CFG->prefix}course_modules mcm ";
							 $queryCM .= " WHERE 1 = 1";
							 $queryCM .= " AND mcm.id IN (".$sections->sequence.") ";
							 $queryCM .= " AND mcm.section = '".$sectionid."' ";
							 $queryCM .= " AND mcm.course = '".$courseId."' ";
							 $queryCM .= " AND mcm.visible = '1' ";
							 
							 $cmArr = $DB->get_records_sql($queryCM);
							 if(count($cmArr) > 0 ){ 
							
								 foreach($cmArr as $cm){
									 
										 $cmId = $cm->id;
									 $moduleId = $cm->module;
									 $instance = $cm->instance;
									  $section = $cm->section;
		
									 $courseArr[$sectionid]->courseModule[$cmId]['cmId'] = $cmId;
									 $courseArr[$sectionid]->courseModule[$cmId]['courseId'] = $courseId;
									 $courseArr[$sectionid]->courseModule[$cmId]['moduleId'] = $moduleId;
									 $courseArr[$sectionid]->courseModule[$cmId]['instance'] = $instance;
									 $courseArr[$sectionid]->courseModule[$cmId]['section'] = $section;	
									 
			
									//echo "$courseId, $userId, $type, $moduleId, $instance<br>";
									 $topicCourseArr = getTopicsCourse($courseId, $userId, $type, $moduleId, $instance, $cmId, $courseTypeId);
									 $courseArr[$sectionid]->courseModule[$cmId]['topicCourse'] = count($topicCourseArr)>0?$topicCourseArr:array();
			
								 } // end inner foreach
							 } // end outer if
						  } //end of $sections->sequence && $sectionid if
				   } // end outer foreach
			   } // end outer if
	   }  // end outermost if
	
	   return $courseArr;
	  
	}
	
	/**
	 * This function is using for getting topic course for learner
	 * @param int $courseId id of course
	 * @param int $userId id of current user
	 * @param int $type is course status like not started/in progress/ completed
	 * @param int $moduleId id of module
	 * @param int $instance is id of scorm or resource
	 * @param int $courseTypeId is a type of course like online or classroom course
	 * @return object array $topicsCourseArr
	 */
	
	function getTopicsCourse($courseId, $userId, $type='', $moduleId, $instance = '', $cmId = '', $courseTypeId = ''){
	
	  global $DB, $CFG;
	  $topicsCourseArr = array();
	  $courseTypeId = $courseTypeId?$courseTypeId:$CFG->courseTypeOnline;
	  
	  if($courseId && $userId && $moduleId){
	  
		 $module = $moduleId; 
		 if($module == 18){ // scrom
	
			$select = " mss.course IN ($courseId)";
			if($instance){
			  $select .= " AND mss.id IN ($instance)";
			}
			$sort = ' ORDER BY mc.id DESC';
	 
			$query = "SELECT ";
			 $query .= "mss.id,mss.revision, mc.id as courseid, mc.fullname as fullname, mc.summary as summary, mc.format as courseformat, mss.id as scormid, mss.launch , mss.name as scormname, mss.intro as scormsummary, mss.timeopen as scormtimeopen, mss.timeclose as scormtimeclose, mcc.id as catid, mcc.name as categoryname, mc.timecreated as ctimecreated, 'scorm' as coursetype ";
			
			$query .= " , (  if((SELECT  msst.timemodified FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' ) WHERE msst.userid = '".$userId."' AND  element = 'cmi.core.score.raw' AND ms.id = mss.id AND mc.coursetype_id = '".$courseTypeId."' ORDER BY msst.id DESC LIMIT 1)!='', 
			
			(SELECT  msst.timemodified FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' ) WHERE msst.userid = '".$userId."' AND  element = 'cmi.core.score.raw' AND ms.id = mss.id  AND mc.coursetype_id = '".$courseTypeId."' ORDER BY msst.id DESC LIMIT 1),
			
			(SELECT  msst.value FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' ) WHERE msst.userid = '".$userId."' AND  element = 'x.start.time' AND ms.id = mss.id  AND mc.coursetype_id = '".$courseTypeId."' ORDER BY msst.id DESC LIMIT 1)
			 )
			
			 )  as lastaccessed ";
			 
			$element = 'cmi.core.score.raw';
			$query .= "  ,(SELECT  MAX(value) FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' ) WHERE msst.userid = '".$userId."' AND element = '".$element."' AND ms.id = mss.id  AND mc.coursetype_id = '".$courseTypeId."' LIMIT 1)  as score";
			
			
			$element = 'cmi.core.total_time';
			$query .= "  ,(SELECT  value FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' ) WHERE msst.userid = '".$userId."' AND  element = '".$element."' AND ms.id = mss.id AND mc.coursetype_id = '".$courseTypeId."'  ORDER BY msst.id DESC LIMIT 1 )  as timespent";
			
			/*$element = "'cmi.core.lesson_status','cmi.completion_status'";
			$query .= "  ,(SELECT  value FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' )  WHERE msst.userid = '".$userId."' AND  element in (".$element.") AND ms.id = mss.id  ORDER BY msst.id DESC LIMIT 1 )  as scormstatus";*/

  

            $element = "'cmi.core.lesson_status','cmi.completion_status'";
			$query .= " , (  if((SELECT value FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' )  WHERE msst.userid = '".$userId."' AND  element in (".$element.") AND ms.id = mss.id AND mc.coursetype_id = '".$courseTypeId."'  ORDER BY msst.id DESC LIMIT 1 )!='', 
			
			(SELECT  value FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' )  WHERE msst.userid = '".$userId."' AND  element in (".$element.") AND ms.id = mss.id  AND mc.coursetype_id = '".$courseTypeId."' ORDER BY msst.id DESC LIMIT 1 ),
			
			(if((SELECT  msst.timemodified FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' ) WHERE msst.userid = '".$userId."' AND  element = 'x.start.time' AND ms.id = mss.id  AND mc.coursetype_id = '".$courseTypeId."' ORDER BY msst.id DESC LIMIT 1)!='', 'incomplete','')
			 ))
			
			 )  as scormstatus ";

			
			//$element = 'x.start.time';
			
			
			if($type){
			
				$query .= "  FROM {$CFG->prefix}scorm mss LEFT JOIN {$CFG->prefix}course mc ON (mc.id = mss.course)  LEFT JOIN {$CFG->prefix}course_categories mcc ON (mc.category = mcc.id)  WHERE $select AND mss.id!='' AND mc.format = 'topics' AND mss.is_active = '1'";
			
			  $query .= "  AND mc.coursetype_id = '".$courseTypeId."' ";	
				
			  $query .= " $sort " ;
				
				
			}else{
	
			  $query .= "  FROM  {$CFG->prefix}scorm mss LEFT JOIN {$CFG->prefix}course mc ON (mc.id = mss.course) LEFT JOIN {$CFG->prefix}course_categories mcc ON (mc.category = mcc.id)  WHERE $select AND mss.id!='' AND mc.format = 'topics' AND mss.is_active = '1'  ";
			  $query .= "  AND mc.coursetype_id = '".$courseTypeId."' ";	
			  $query .= $sort;	
			  
			}		
			
			//echo $query;die;
			
			
			$scromArr = $DB->get_records_sql($query);
			$scromArr->coursetype = 'scorm';
			sort($scromArr);
			$topicsCourseArr = $scromArr;
			
			//pr($scromArr);die;
			
		 }elseif($module == 17){  // resource
		 
			   $select = " mr.course IN ($courseId)";
			   if($instance){
				 $select .= " AND mr.id IN ($instance)";
			   }

			   if($cmId){
			       $select .= " AND mcm.id IN ($cmId)";
			   }
			   $query = "SELECT mr.id,mr.revision,mr.allow_student_access,  mc.id as courseid, mc.fullname as fullname, mc.summary as summary, mc.format as courseformat, mr.id as resourceid, mr.name as resoursename, mr.intro as resourcesummary, mcc.id as catid, mcc.name as categoryname, mc.timecreated as ctimecreated,  mcm.id as cmid , mula.timeaccess as lastaccessed, 'resource' as coursetype, mr.resource_type as resource_type_id, (SELECT name FROM {$CFG->prefix}resource_type WHERE id = mr.resource_type) as resource_type, mr.createdby as rcreatedby" ;
			   $query .= "  FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}resource mr ON (mc.id = mr.course) LEFT JOIN {$CFG->prefix}course_categories mcc ON (mc.category = mcc.id) LEFT JOIN {$CFG->prefix}course_modules mcm ON (mc.id = mcm.course)  LEFT JOIN {$CFG->prefix}user_noncourse_lastaccess mula ON (mc.id = mula.courseid AND mr.id = mula.resource_id  AND mula.userid = '".$userId."')  WHERE $select  AND mcm.module = '$module' AND mc.format = 'topics'  AND mr.is_active = '1' ";
			   
			   if($courseTypeId == $CFG->courseTypeClassroom){
			      $query .= "  AND mc.coursetype_id = '".$courseTypeId."' AND mr.is_reference_material = '0' "; // we need only assets in case of classroom course, reference material not required
			   }else{
			      $query .= "  AND mc.coursetype_id = '".$courseTypeId."' ";
			   }	
			   $query .= $sort;	
				   
			   //echo $query;die;	   
			   $resourceArr = $DB->get_records_sql($query);
			   $resourceArr->coursetype = 'file';
			   sort($resourceArr);
			   $topicsCourseArr = $resourceArr;
				
		 }
	   
	  }
	  
	  return $topicsCourseArr;
	  
	}
		
	/**
	 * get group members
	 *
	 * @param int groupId id of group
	 * @return object array of group members
	 */
	 
	function getGroupMembers($groupId){
	
		global $DB, $CFG, $USER;
		$groupsMembers = array();
		$searchcondition = '';
		if($groupId){
			
				$roleChk = "and ra.roleid IN( ".$CFG->studentTypeId.",".$CFG->managerTypeId.")";
				
				 switch($USER->archetype){
						CASE $CFG->userTypeAdmin:
								$searchcondition .= '';
							break;
						CASE $CFG->userTypeManager:
								
								$roleChk = "and ra.roleid = ".$CFG->studentTypeId;
			
								if($groupId){
									  $groupDepts = getGroupDepartments($groupId);
									  $groupDeptsStr = 0;
									  if(count($groupDepts) > 0){
											$groupDeptsIds = array_keys($groupDepts); 
											$groupDeptsStr = implode(",", $groupDeptsIds);
											$searchcondition .=	" AND u.id IN(SELECT DISTINCT(uu.id)
																						FROM {$CFG->prefix}user AS uu
																						LEFT JOIN {$CFG->prefix}department_members AS dm ON dm.userid = uu.id
																						WHERE dm.is_active = '1' AND uu.deleted = '0' AND dm.departmentid IN (".$groupDeptsStr.")) ";
									  }
								  
								}
									
							break;
				   }
		
		
					$query = "SELECT DISTINCT(u.id) AS userid
							  FROM {$CFG->prefix}groups_members gm
							  JOIN {$CFG->prefix}user u ON u.id = gm.userid
						 LEFT JOIN {$CFG->prefix}role_assignments ra ON (ra.userid = u.id )
						 LEFT JOIN {$CFG->prefix}role r ON r.id = ra.roleid";
						 
					 $query .= "  WHERE 1 = 1 AND u.deleted = '0' AND gm.is_active = 1 ".$roleChk.$searchcondition;
					 if($groupId){
					   $query .= " AND gm.groupid = '".$groupId."'";
					 }
			
		             $groupsMembers = $DB->get_records_sql($query);
					 
		}
		
		return $groupsMembers;
	}
	
	/**
	 * get department members
	 *
	 * @param int deptId id of department
	 * @return object array of department members
	 */
	 
	function getDepartmentMembers($deptId = '', $isReport = false){
	
		global $DB, $CFG, $USER;
		$depMembers = array();
		$dMemArr = array();
		$depMembersArr = array();
		
		
		$excludedUsers = count($CFG->excludedUsers) > 0 ? implode(",",$CFG->excludedUsers) : 0;
		$userIdArray = array();
			
		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
				$roleChk = " AND mra.roleid IN( ".$CFG->studentTypeId.",".$CFG->managerTypeId.")";
			break;
			CASE $CFG->userTypeManager:
				$roleChk = " AND mra.roleid IN( ".$CFG->studentTypeId.")";
			break;
		}

		$query = "select mdm.id, mdm.userid, mdm.departmentid from {$CFG->prefix}department_members mdm 
						 LEFT JOIN {$CFG->prefix}user mu ON (mdm.userid = mu.id)
						 LEFT JOIN {$CFG->prefix}role_assignments mra ON (mra.userid = mu.id )
						 LEFT JOIN {$CFG->prefix}role mr ON mr.id = mra.roleid";
		
		$query .= "  WHERE 1 = 1 AND mu.deleted = '0' AND mu.suspended=0 AND mu.id NOT IN (".$excludedUsers.") ".$roleChk;			
		if($deptId){
		 $query .= "   AND mdm.departmentid = '".$deptId."'";	
		}	
		
		if(!$isReport){
		 $query .= "  AND mdm.is_active = '1'  ";
		}
		//echo $query ;die;
		$depMembers = $DB->get_records_sql($query);
		
		if(count($depMembers) > 0 ){
		  foreach($depMembers as $arr){
			$dMemArr[$arr->departmentid][] = $arr->userid;
		  }
		}
		
		if($deptId){
		  $depMembersArr[$deptId] =  isset($dMemArr[$deptId])?$dMemArr[$deptId]:array();
		}else{
		  $depMembersArr =  $dMemArr;
		}

		return $depMembersArr;
	}
	
	/**
	 * To check, if course group exist
	 *
	 * @param int $groupId id of group
	 * @param int $courseId id of course
	 * @return int courseid, if exist, else zero
	 */
	 
	 
	 
	function isCourseGroupExist($groupId, $courseId){
	
		global $DB, $CFG;
		$isCourseGroupExist = 0;
		if($groupId && $courseId){
		   $isCourseGroupExist = $DB->get_field_sql("select courseid from {$CFG->prefix}groups_course where groupid = '".$groupId."' AND courseid = '".$courseId."' ");
		}
		
		return $isCourseGroupExist;
	}
	
	
	/**
	 * get assigned users for course
	 * @param int $courseId id of course
	 * @return $assignUsersForCourse return assigned users of course
	 */
	 
	 
	function getAssignUsersForCourse($courseId){
	
		global $DB, $CFG;
		$assignUsersForCourse = 0;
		if($courseId){
		   
		   $enrolArr = $DB->get_record('enrol',array('courseid'=>$courseId,'enrol'=>'manual'));
		   $enrolId = $enrolArr->id;
		   
		   if($enrolId){
              $userArr = $DB->get_records('user_enrolments',array('enrolid'=>$enrolId));
			  $assignUsersForCourse = count($userArr);
		   }
		}
		
		
		
		return $assignUsersForCourse;
	}
	
	/**
	 * get assigned courses for group
	 *
	 * @param mixed $groupId id of group
	 * @param int $return specifies what you want to return
	 * @return if 1, return course object else return comma seperated course 
	 */
	 
	 
	function getAssignCoursesForGroup($groupId, $return=1, $isReport = false){
	
		global $DB, $CFG, $USER;
		$assignCoursesForGroupRec = array();
		$assignCoursesForGroup = getMDash();
		$groupIds = 0;
		
		if(is_array($groupId) && count($groupId) > 0 ){
		   $groupIds = implode(",", $groupId);
		}else{
		   $groupIds = $groupId;
		}
		
		if($groupIds){
		
		    /*$query = "select mc.id, mgc.courseid, mgc.groupid, mc.fullname as name from {$CFG->prefix}groups_course mgc INNER JOIN {$CFG->prefix}course mc ON (mgc.courseid = mc.id)  where mgc.groupid = '".$groupId."' ORDER BY name";
		   $assignCoursesForGroupRec = $DB->get_records_sql($query);*/

		   switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$where .= '';
				break;
			CASE $CFG->userTypeManager:
																
				$where .=	" AND (gc.courseid IN(SELECT dc.courseid
																FROM mdl_department_course AS dc
																LEFT JOIN mdl_department AS d ON d.id = dc.departmentid
																LEFT JOIN mdl_department_members as dm ON dm.departmentid = d.id
																WHERE 1 = 1 AND d.deleted = '0' ";
							 if(!$isReport){									
							   $where .=	" AND dc.is_active = 1 AND dm.is_active = 1 ";
							 }  
							 
							$where .=	' AND dm.userid = '.$USER->id.'))';

				break;
		}
																
		$Query = "SELECT DISTINCT(c.id), c.fullname as name, gc.courseid, gc.groupid FROM mdl_groups_course as gc LEFT JOIN mdl_course as c ON gc.courseid = c.id WHERE  1 = 1 ";
		$Query .= " AND c.deleted = '0' ";
		
		if(!$isReport){
		  $Query .= "AND gc.is_active = 1 AND c.is_active = 1 ";
		}
		$Query .= $where." AND gc.groupid IN ( ".$groupId." ) ORDER BY c.fullname ASC";
		$assignCoursesForGroupRec = $DB->get_records_sql($Query);
		
		 
		   if($return == 2){
			   if(count($assignCoursesForGroupRec) > 0){
				 foreach($assignCoursesForGroupRec as $rows){
				   $assignCoursesForGroupArr[] = $rows->name;
				 }
				
				 if(count($assignCoursesForGroupArr) > 0 ){
				   $assignCoursesForGroup = implode(", ", $assignCoursesForGroupArr);
				 }
			   }
		   }else{
			 $assignCoursesForGroup = $assignCoursesForGroupRec;
		   }
		}
		
		
		
		return $assignCoursesForGroup;
	}
	
	/**
	 * get assigned programs for group
	 *
	 * @param mixed $groupId id of group
	 * @param int $return specifies what you want to return
	 * @return if 1, return program object else return comma seperated programs 
	 */
	 
	 
	function getAssignProgramsForGroup($groupId, $return=1, $isReport = false){
	
		global $DB, $CFG, $USER;
		$assignProgramsForGroupRec = array();
		$assignProgramsForGroup = getMDash();
		$groupIds = 0;
		if(is_array($groupId) && count($groupId) > 0 ){
		   $groupIds = implode(",", $groupId);
		}else{
		   $groupIds = $groupId;
		}
		if($groupIds){

		   switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$where .= '';
				break;
			CASE $CFG->userTypeManager:
																
				$where .=	" AND (pg.program_id IN(SELECT pd.program_id
																FROM mdl_program_department AS pd
																LEFT JOIN mdl_department AS d ON d.id = pd.department_id
																LEFT JOIN mdl_department_members as dm ON dm.departmentid = d.id
																WHERE 1 = 1 AND d.deleted = '0' ";
							 if(!$isReport){									
							   $where .=	' AND pd.is_active = 1 AND dm.is_active = 1 ';
							 }  
							 
							$where .=	' AND dm.userid = '.$USER->id.'))';

				break;
		}
																
		$Query = "SELECT DISTINCT(p.id), p.name, pg.program_id, pg.group_id FROM mdl_program_group as pg LEFT JOIN mdl_programs as p ON pg.program_id = p.id WHERE  1 = 1 AND p.deleted = '0'";
		
		if(!$isReport){
		  $Query .= "AND pg.is_active = 1 AND p.status = 1 ";
		}
		$Query .= $where." AND pg.group_id IN ( ".$groupIds." ) ORDER BY p.name ASC";
		$assignProgramsForGroupRec = $DB->get_records_sql($Query);
		
		 
		   if($return == 2){
			   if(count($assignProgramsForGroupRec) > 0){
				 foreach($assignProgramsForGroupRec as $rows){
				   $assignProgramsForGroupArr[] = $rows->name;
				 }
				
				 if(count($assignProgramsForGroupArr) > 0 ){
				   $assignProgramsForGroup = implode(", ", $assignProgramsForGroupArr);
				 }
			   }
		   }else{
			 $assignProgramsForGroup = $assignProgramsForGroupRec;
		   }
		}
		
		
		
		return $assignProgramsForGroup;
	}
	
	/**
	 * get assigned courses for department
	 *
	 * @param int $deptId id of department
	 * @param int $return specifies what you want to return
	 * @return if 1, return course object else return comma seperated course 
	 */
	 
	 
	function getAssignCoursesForDepartment($deptId, $return=1){
	
		global $DB, $CFG;
		$assignCoursesForDeptRec = array();
		$assignCoursesForDept = getMDash();
		if($deptId){
		
		   $query = "select mc.id, mdc.courseid, mdc.departmentid, mc.fullname as name from {$CFG->prefix}department_course mdc INNER JOIN {$CFG->prefix}course mc ON (mdc.courseid = mc.id)  where mdc.departmentid = '".$deptId."' AND mdc.is_active = '1' AND mc.deleted = '0' ORDER BY name";
		   $assignCoursesForDeptRec = $DB->get_records_sql($query);
		 
			   if($return == 2){
				   if(count($assignCoursesForDeptRec) > 0){
					 foreach($assignCoursesForDeptRec as $rows){
					   $assignCoursesForDeptArr[] = $rows->name;
					 }
					
					 if(count($assignCoursesForDeptArr) > 0 ){
					   $assignCoursesForDept = implode(", ", $assignCoursesForDeptArr);
					 }
				   }
			   }else{
				 $assignCoursesForDept = $assignCoursesForDeptRec;
			   }
		}
		
		
		
		return $assignCoursesForDept;
	}
	
	
	
	/**
	 * get assigned groups for course
	 *
	 * @param int $courseId id of course
	 * @param int $return specifies what you want to return
	 * @return if 1, return group object else return comma seperated groups 
	 */
	 
	 
	function getAssignGroupsForCourse($courseId, $return=1){
		
		global $DB, $CFG;
		$assignGroupsForCourseRec = array();
		$assignGroupsForCourse = getMDash();
		if($courseId){
		
	           $query = "select mg.id, mg.name from {$CFG->prefix}groups_course mgc INNER JOIN {$CFG->prefix}groups mg ON (mgc.groupid = mg.id)  where mgc.courseid = '".$courseId."'  AND mgc.is_active = '1' ORDER BY mg.name";
		       $assignGroupsForCourseRec = $DB->get_records_sql($query);
		 
			   if($return == 1){
			      $assignGroupsForCourse = $assignGroupsForCourseRec;
			   }elseif($return == 2){
			       if(count($assignGroupsForCourseRec) > 0){
					 foreach($assignGroupsForCourseRec as $rows){
					   $assignGroupsForCourseArr[] = $rows->name;
					 }
					 
					 if(count($assignGroupsForCourseArr) > 0 ){
					   $assignGroupsForCourse = implode(", ", $assignGroupsForCourseArr);
					 }
				   }
			   }elseif($return == 3){
			     $assignGroupsForCourse = array_keys($assignGroupsForCourseRec);
			   }
		}
		
		
		
		return $assignGroupsForCourse;
	}
	
	/**
	 * get assigned programs for course
	 *
	 * @param int $programId id of program
	 * @param int $return specifies what you want to return
	 * @return if 1, return group object else return comma seperated groups 
	 */
	 
	 
	function getAssignGroupsForProgram($programId, $return=1){
		
		global $DB, $CFG;
		$assignGroupsForProgramRec = array();
		$assignGroupsForProgram = getMDash();
		if($programId){
		
	           $query = "select mg.id, mg.name from {$CFG->prefix}program_group mpg INNER JOIN {$CFG->prefix}groups mg ON (mpg.group_id = mg.id)  where mpg.program_id = '".$programId."'  AND mpg.is_active = '1' ORDER BY mg.name";
		       $assignGroupsForProgramRec = $DB->get_records_sql($query);
		 
			   if($return == 1){
			      $assignGroupsForProgram = $assignGroupsForProgramRec;
			   }elseif($return == 2){
			       if(count($assignGroupsForProgramRec) > 0){
					 foreach($assignGroupsForProgramRec as $rows){
					   $assignGroupsForProgramArr[] = $rows->name;
					 }
					 
					 if(count($assignGroupsForProgramArr) > 0 ){
					   $assignGroupsForProgram = implode(", ", $assignGroupsForProgramArr);
					 }
				   }
			   }elseif($return == 3){
			     $assignGroupsForProgram = array_keys($assignGroupsForProgramRec);
			   }
		}
		
		
		
		return $assignGroupsForProgram;
	}
	
	/**
	 * get assigned groups of users
	 *
	 * @param int $userId id of userid of course
	 * @param int $return specifies what you want to return
	 * @return if 1, return group object else return comma seperated groups 
	 */
	 
	 
	function getAssignGroupsForUser($userId, $return=1){
	
		
		global $DB, $CFG;
		$assignGroupsForUserRec = array();
		$assignGroupsForUser = getMDash();
		if($userId){
		
	   $query = "select mg.id, mg.name from {$CFG->prefix}groups_members mgm LEFT JOIN {$CFG->prefix}groups mg ON (mgm.groupid = mg.id)  where mgm.userid = '".$userId."' AND mgm.is_active = '1'  ORDER BY mg.name";
		   $assignGroupsForUserRec = $DB->get_records_sql($query);
		 
			   if($return == 1){
				   $assignGroupsForUser = $assignGroupsForUserRec;
			   }elseif($return == 2){
				   if(count($assignGroupsForUserRec) > 0){
					 foreach($assignGroupsForUserRec as $rows){
					   $assignGroupsForUserArr[] = $rows->name;
					 }
					 
					 if(count($assignGroupsForUserArr) > 0 ){
					   $assignGroupsForUser = implode(", ", $assignGroupsForUserArr);
					 }
				   }
			  }elseif($return == 3){
			     $assignGroupsForUser = array_keys($assignGroupsForUserRec);
			   }
		}
		
		
		
		return $assignGroupsForUser;
	}
	
		
	/**
	 * This function is using to count total no. of department for an user 
	 * @global object
	 * @param int $userId is created by id of department
	 * @param int $learner counter of department
	 * @return int $count total no. of department for an user 
	 */
	 
	function getDepartmentCountByUser($userId){
		global $DB, $CFG;
		$count = 0;
		$query = "SELECT count(*) as count FROM {$CFG->prefix}department WHERE deleted='0' AND status = '1'";
		if($userId){
		   $query .= " AND createdby = '".$userId."'";
		}
		$records = $DB->get_record_sql($query);
		$count = $records->count;
		return $count;
		
	}
	
	/**
	 * This function is using to count total no. of learner for an admin/manager role 
	 * @global object
	 * @param int $count counter of learner
	 * @return int $count total no. of learner for an user 
	 */
	 
	function getLearnerCountByUser($userId=0){
		global $DB, $CFG;
		$count = 0;
		//$query = "SELECT count(*) as count FROM {$CFG->prefix}user mu INNER JOIN {$CFG->prefix}role_assignments mra ON (mu.id = mra.userid) where mu.deleted='0' AND mra.contextid = 1 AND mra.roleid = 5";
		$context = get_context_instance(CONTEXT_SYSTEM);
		$userscount = get_users_listing('usercountwithoutsuspended', '', '', '', '', '', '', '' , array(), $context);
		$count = $userscount->usercount;
		return $count;
		
	}
	
	/**
	 * This function is using to count total no. of course for an user , if return is 'count' else will return  total courses
	 * @global object
	 * @param int $return is type of return records ( it can be count or list)
	 * @param int $count counter of course
	 * @return int $count total no. of course for an user 
	 */
	 
	function getCourseCountByUser($return = 'count', $on = ''){
		global $DB, $CFG, $USER;
		$count = 0;
		if($return == 'count'){
		  $query = "SELECT count(*) as count FROM {$CFG->prefix}course WHERE 1 = 1 AND category!='0' AND id NOT IN (1) ";
		}elseif($return = 'list'){
		  $query = "SELECT * FROM {$CFG->prefix}course WHERE 1 = 1 AND category!='0' AND id NOT IN (1)  ";
		}

		   
	   if( $USER->archetype == $CFG->userTypeAdmin) {
		    $query .= " ";
			if($on == 'event'){
			   $query .= " AND is_active='1'  AND publish = 1  AND deleted = '0' AND coursetype_id='1'";
			}
	   }elseif( $USER->archetype == $CFG->userTypeManager) {
		   $mCourseIdArr = getManagerCourse();
		   if(count($mCourseIdArr) > 0 ){
			 $mCourseIdStr = implode(",", $mCourseIdArr); 
			 $query .= " AND id IN ($mCourseIdStr) ";
		   }else{
			   $query .= " AND id IN (0) ";
		   }
		    
		   $query .= " AND is_active='1'  AND publish = 1  AND deleted = '0' AND coursetype_id='1'";
			
	   }elseif( $USER->archetype == $CFG->userTypeStudent) {
			 $query .= " AND is_active='1'  AND publish = 1 AND deleted = '0' ";
	   }

		
		
		if($return == 'count'){
		  $records = $DB->get_record_sql($query);
		  $count = $records->count;
		  return $count;
		}elseif($return == 'list'){ 
		  $records = $DB->get_records_sql($query);
		  return $records;
		}
		
	}
	
	/**
	 * This function is using to count total no. of team for an user 
	 * @global object
	 * @param int $count counter of team
	 * @return int $count total no. of team for an user 
	 */
	 
	function getTeamCountByUser(){
		global $DB, $CFG, $USER;
		$count = 0;
		
		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$query = "SELECT count(*) as count FROM {$CFG->prefix}groups WHERE 1 = 1 ";
					//$query .= " AND deleted = '0'";
					$records = $DB->get_record_sql($query);
					$count = $records->count;
		
			break;
			CASE $CFG->userTypeManager:

						 $dGroups = getDepartmentGroups($USER->department);
						 $count = count($dGroups);
					  

						
				break;
		}
		
		return $count;
		
	}
	
	/**
	 * This function is using to get teams of user 
	 * @global object
	 * @return array $records
	 */
	 
	function getTeamsByUser(){

		global $DB, $CFG, $USER;
		$records = array();
		
		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$query = "SELECT count(*) as count FROM {$CFG->prefix}groups WHERE 1 = 1 ";
					//$query .= " AND deleted = '0'";
					$records = $DB->get_record_sql($query);
			
			break;
			CASE $CFG->userTypeManager:

					$records = getDepartmentGroups($USER->department);

				break;
		}
		

		
		return $records;
		
	}
	
	
	
	/**
	 * This function is using to get teams, their learner and courses for admin dashboard display
	 * @global object
	 * @param int $userId is created by id of course
	 * @return array $teamDashboard
	 */
	 
	function getTeamsForAdminDashboard($userId){
	
		global $DB, $CFG;
		$teamDashboard = array();
		$teamsArr = getTeamsByUser();

        $cntTeam = count($teamsArr);
        if($cntTeam > 0 ){
		
			foreach($teamsArr as $teams){
			
			  $groupId = $teams->id;
			  $name = $teams->name;
			  $description = $teams->description;
			  $picture = $teams->picture;
			  
			  // getting team learners
			  $teamLearner = getGroupMembers($groupId);
			
              // getting course assigned to team
			  $teamCourses = getAssignCoursesForGroup($groupId, $return=1);
			  
			  $teamDashboard[$groupId]->name = $name;
			  $teamDashboard[$groupId]->id = $groupId;
			  $teamDashboard[$groupId]->description = $description;
			  $teamDashboard[$groupId]->picture = $picture;
			  $teamDashboard[$groupId]->teamLearner = $teamLearner;
			  $teamDashboard[$groupId]->teamCourses = $teamCourses;

			  
			}
		}
			
		return $teamDashboard;
		
	}
	
	
	/**
	 * This function is using to get departments of user 
	 * @global object
	 * @param int $userId is created by id of department
	 * @return array $records
	 */
	 
	function getDepartmentsByUser($userId){
		global $DB, $CFG;
		$count = 0;
		$query = "SELECT * FROM {$CFG->prefix}department WHERE deleted = '0' AND status = '1'";
		if($userId){
		   $query .= " AND createdby = '".$userId."'";
		}
		$query .= " ORDER BY title ASC";
		$records = $DB->get_records_sql($query);
		return $records;
		
	}
	
		/**
	 * This function is using to get departments of user 
	 * @global object
	 * @param int $userId is created by id of department
	 * @return array $records
	 */
	 
	function getDepartmentOfUsers(){
		global $DB, $CFG;
		$count = 0;
		$query = "SELECT id, department FROM {$CFG->prefix}user WHERE 1 = 1 ";
		$query .= " ORDER BY id ASC";
		$records = $DB->get_records_sql($query);
		return $records;
		
	}
	
	
	
	/**
	 * This function is using to get departments, their learner and courses for admin dashboard display
	 * @global object
	 * @param int $userId is created by id of department
	 * @return array $depDashboard
	 */
	 
	function getDepartmentsForAdminDashboard($userId = 0){
	
		global $DB, $CFG;
		$depDashboard = array();
		$depArr = getDepartmentsByUser($userId);
		
        $cntDep = count($depArr);
        if($cntDep > 0 ){
		
			foreach($depArr as $dep){
			
			  $deptId = $dep->id;
			  $name = $dep->title;
			  $description = $dep->description;
			  $picture = $dep->picture;
			  
			  // getting department learners
			  $depLearners = getDepartmentMembers($deptId);
			
              // getting course assigned to team
			  $depCourses = getAssignCoursesForDepartment($deptId, $return=1);
			  
			  $depDashboard[$deptId]->id = $deptId;
			  $depDashboard[$deptId]->name = $name;
			  $depDashboard[$deptId]->title = $name;
			  $depDashboard[$deptId]->description = $description;
			  $depDashboard[$deptId]->picture = $picture;
			  $depDashboard[$deptId]->depLearners = $depLearners[$deptId];
			  $depDashboard[$deptId]->depCourses = $depCourses;

			  
			}
		}
			
		return $depDashboard;
		
	}
	
	
	
	/**
	 * This function is using to get all compliance and global course ids
	 * @global object
	 * @param int $type is type of course , it can be either "compliance", "global", "mixed" 
	 * @return array $cngArrForEnroll return compliace and global course id array, if type is defined then it will return accordingly
	 */
	 
function getComplianceAndGlobalCourses($type=''){
	
	   global $DB, $CFG,$USER;
	   
	   $cngArrForEnroll = array();
	   $cngArr = array();
	   $allUserRoleDetails = getAllUserRoleDetails();
	   $departmentOfUsers = getDepartmentOfUsers();
	   $isBelongToED = isLoginUserBelongsToED();
	   //pr($allUserRoleDetails);die;
		//pr($USER);die;
		
	    $queryForNonEnrolledCourseInDep = "select c.id from mdl_course AS c where c.id NOT IN (select distinct courseid from mdl_department_course where is_active = 1) AND c.criteria = ".$CFG->courseCriteriaCompliance." AND c.deleted = '0' AND c.publish = '1' AND c.is_active = '1'";
	    $nonEnrolledCourseInDepRec = $DB->get_records_sql($queryForNonEnrolledCourseInDep);
	
		
	    $nonEnrolledCourseInDep = array();
	    if(count($nonEnrolledCourseInDepRec) >0 ){
	    	$nonEnrolledCourseInDep = array_keys($nonEnrolledCourseInDepRec);
	    }
		

		
		if($USER->archetype == $CFG->userTypeStudent && $CFG->allowExternalDepartment == 1 && $isBelongToED ){
		 
			 $courseSql  = "SELECT c.id, c.createdby, c.criteria, c.is_global 
						FROM mdl_course AS c
						LEFT JOIN mdl_department_course dc ON dc.courseid = c.id AND dc.is_active = 1
						LEFT JOIN mdl_user_course_mapping as ucm ON ucm.courseid = c.id AND ucm.`status` = 1 AND ucm.userid = $USER->id
						LEFT JOIN mdl_groups_course gc ON gc.courseid = c.id AND gc.is_active = 1 AND gc.groupid IN (SELECT gm.groupid FROM mdl_groups_members AS gm WHERE gm.is_active = 1 AND gm.userid = $USER->id)";
			 $courseSql  .= " WHERE c.publish = 1 AND c.id != 1 AND c.is_active = 1 ";	
		 
			 if($CFG->CanExternalLearnerRequest == 1){
					
				  $courseSql  .= " AND (dc.departmentid = ".$USER->department." || (c.criteria = ".$CFG->courseCriteriaCompliance." AND (dc.departmentid = '".$USER->department."' || dc.id IS NULL)) || (c.is_global = ".$CFG->courseElectiveGlobal." AND (dc.departmentid = '".$USER->department."' || dc.id IS NULL)) ) ";
				  $courseSql  .= " AND ucm.id IS NULL  AND c.coursetype_id = 1 AND (gc.id IS NULL)";
							
						
			  }else{
				  $courseSql  .= "  AND (dc.departmentid = ".$USER->department." || (c.criteria = ".$CFG->courseCriteriaCompliance." AND dc.departmentid = '".$USER->department."') || (c.is_global = ".$CFG->courseElectiveGlobal." AND dc.departmentid = '".$USER->department."') ) ";
				  $courseSql  .= " AND ucm.id IS NULL  AND c.coursetype_id = 1 AND gc.id IS NULL ";
					
			  }	
		  
		}else{
		  
		  if($USER->archetype == $CFG->userTypeStudent){
		  
		     $courseSql  = "SELECT c.id, c.createdby, c.criteria, c.is_global 
						FROM mdl_course AS c
						LEFT JOIN mdl_department_course dc ON dc.courseid = c.id AND dc.is_active = 1
						LEFT JOIN mdl_user_course_mapping as ucm ON ucm.courseid = c.id AND ucm.`status` = 1 AND ucm.userid = $USER->id
						LEFT JOIN mdl_groups_course gc ON gc.courseid = c.id AND gc.is_active = 1 AND gc.groupid IN (SELECT gm.groupid FROM mdl_groups_members AS gm WHERE gm.is_active = 1 AND gm.userid = $USER->id)";
			 $courseSql  .= " WHERE c.publish = 1 AND c.id != 1 AND c.is_active = 1 ";	
			 
			  $courseSql  .= " AND (dc.departmentid = ".$USER->department." || (c.criteria = ".$CFG->courseCriteriaCompliance." AND (dc.departmentid = '".$USER->department."' || dc.id IS NULL)) || (c.is_global = ".$CFG->courseElectiveGlobal." AND (dc.departmentid = '".$USER->department."' || dc.id IS NULL)) ) AND ucm.id IS NULL  AND c.coursetype_id = 1 AND (gc.id IS NULL)";
			  
			 
		  }else{
		      $courseSql  = "SELECT c.id, c.createdby, c.criteria, c.is_global FROM mdl_course AS c WHERE (c.criteria = ".$CFG->courseCriteriaCompliance." OR c.is_global = ".$CFG->courseElectiveGlobal." ) AND c.deleted = 0 AND c.is_active = 1 AND c.publish = '1'";
		  }
		
		}
	    
		 
		  
		$complianceAndGlobalCourse = $DB->get_records_sql($courseSql);
		 
		
		
	  
	
		if(!empty($complianceAndGlobalCourse)){
			foreach($complianceAndGlobalCourse as $arr){
				
				$courseId = $arr->id;
				$criteria = $arr->criteria;
				$is_global = $arr->is_global;
				$cCreatedBy = $arr->createdby;
				
				if($criteria == $CFG->courseCriteriaCompliance){
					
					if($USER->archetype == $CFG->userTypeManager ){
						if($cCreatedBy && ( ($cCreatedBy == $USER->id) || (isset($allUserRoleDetails[$cCreatedBy]) && $allUserRoleDetails[$cCreatedBy]->name == $CFG->userTypeAdmin))){
						  $cngArr['compliance'][] = $arr->id;
						}
					}elseif($USER->archetype == $CFG->userTypeStudent ){
					
					    if($CFG->allowExternalDepartment == 1 && $isBelongToED){
						
						    if($cCreatedBy && isset($departmentOfUsers[$cCreatedBy]) && $departmentOfUsers[$cCreatedBy]->department == $USER->department){
							  $cngArr['compliance'][] = $arr->id;
							}
							
						}else{
	
						if( ( ( $cCreatedBy && isset($departmentOfUsers[$cCreatedBy]) && $departmentOfUsers[$cCreatedBy]->department == $USER->department) || (isset($allUserRoleDetails[$cCreatedBy]) && $allUserRoleDetails[$cCreatedBy]->name == $CFG->userTypeAdmin)) || (count($nonEnrolledCourseInDep) > 0 && in_array($courseId, $nonEnrolledCourseInDep))){
							  $cngArr['compliance'][] = $arr->id;
							}
						}
					}
				}else{
				   
				   if($is_global == $CFG->courseElectiveGlobal){
				   
				     if($USER->archetype == $CFG->userTypeManager ){
						if( ($cCreatedBy && ($cCreatedBy == $USER->id)) || (isset($departmentOfUsers[$cCreatedBy]) && $departmentOfUsers[$cCreatedBy]->department == $USER->department) || (isset($allUserRoleDetails[$cCreatedBy]) && $allUserRoleDetails[$cCreatedBy]->name == $CFG->userTypeAdmin)){
						  $cngArr['global'][] = $arr->id;
						}
					 }elseif($USER->archetype == $CFG->userTypeStudent ){
					
					    if($CFG->allowExternalDepartment == 1 && $isBelongToED){
						
						    if($cCreatedBy && isset($departmentOfUsers[$cCreatedBy]) && $departmentOfUsers[$cCreatedBy]->department == $USER->department){
							 $cngArr['global'][] = $arr->id;
							}
							
						}else{
							if($cCreatedBy && ( (isset($departmentOfUsers[$cCreatedBy]) && $departmentOfUsers[$cCreatedBy]->department == $USER->department) || (isset($allUserRoleDetails[$cCreatedBy]) && $allUserRoleDetails[$cCreatedBy]->name == $CFG->userTypeAdmin))){
						      $cngArr['global'][] = $arr->id;
						    }
						}
					}else{
	
						if($cCreatedBy && ( (isset($departmentOfUsers[$cCreatedBy]) && $departmentOfUsers[$cCreatedBy]->department == $USER->department) || (isset($allUserRoleDetails[$cCreatedBy]) && $allUserRoleDetails[$cCreatedBy]->name == $CFG->userTypeAdmin))){
						  $cngArr['global'][] = $arr->id;
						}
					 }
					
				   }
				
				}	
				
			}
			//pr($cngArr['compliance']);die;
			if($type == 'compliance'){
			 $cngArrForEnroll =  $cngArr['compliance'];
			}elseif($type == 'global'){
			 $cngArrForEnroll =  $cngArr['global'];
			}elseif($type == 'mixed'){
				 if(count($cngArr['compliance']) > 0 && count($cngArr['global']) > 0 ){
				  $cngArrForEnroll = array_merge($cngArr['compliance'], $cngArr['global']);
				 }elseif(count($cngArr['compliance']) > 0 && count($cngArr['global']) == 0 ){
				  $cngArrForEnroll = $cngArr['compliance'];
				 }elseif(count($cngArr['compliance']) == 0 && count($cngArr['global']) > 0 ){
				  $cngArrForEnroll = $cngArr['global'];
				 } 
			}else{
			 $cngArrForEnroll = $cngArr;
			}
		}
		
		return $cngArrForEnroll;
	
	}
	
	
	/**
	 * This function is using to get all new courses for admin dashboard
	 * @global object
	 * @param int $userId is created by id of course
	 * @param int $courseTypeId is a type of course like online or classroom course
	 * @return array $records
	 */
	 
	function getCourseByUserForDashboard($userId, $courseTypeId = ''){
		global $DB, $CFG,$USER;
		$records = array();
		
		 
		$complianceCourses = getComplianceAndGlobalCourses('mixed');

		$whExtra = "c.id IN (0)"; 
		if(count($complianceCourses) > 0 ){
		  $whExtra = "c.id IN (".implode(",",$complianceCourses).")";
		}
		if($USER->archetype == $CFG->userTypeAdmin){
		
			//$courseTypeId = $courseTypeId?$courseTypeId:$CFG->courseTypeOnline;
			$query = "SELECT mc.*, mcc.name as categoryname,0 as classid FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}course_categories mcc on (mc.category = mcc.id) where mc.is_active='1' AND mc.publish='1' AND mc.category!='0' AND mc.deleted = '0'";
			
			if($userId){
			   $query .= " AND mc.createdby = '".$userId."'";
			}
			if($courseTypeId != ''){
				$query .= "  AND mc.coursetype_id = '".$courseTypeId."' ";
			}
			
			$query .= " ORDER BY mc.timecreated DESC LIMIT 5";
			$records = $DB->get_records_sql($query);
			return $records;
			
		}elseif($USER->archetype == $CFG->userTypeManager){
		
		    $allUserRoleDetails = getAllUserRoleDetails();
			if(count($allUserRoleDetails ) > 0 ){
			  $adminUserIdsArr = array(); 
			  foreach($allUserRoleDetails  as $arrRoles){
			    if($arrRoles->roleid == 1){
			      $adminUserIdsArr[] = $arrRoles->id;
				}
			  }
			  $adminUserIds = 0;
			  $WhManager  = '';
			  if(count($adminUserIdsArr) > 0 ){
				$adminUserIds = implode(",", $adminUserIdsArr);
				$WhManager = " AND (dc.departmentid = '".$USER->department."' || c.createdby IN (".$adminUserIds."))";
			  }
			}
			//pr($adminUserIds);die;
		
			$courseSql  = "(SELECT CONCAT_WS('_', CAST(0 AS CHAR CHARSET utf8), CAST(c.id AS CHAR CHARSET utf8)) AS difId,0 AS pid,c.*,
			mcc.name as categoryname,0 as classid
							FROM mdl_course AS c 
							LEFT JOIN {$CFG->prefix}course_categories mcc on (c.category = mcc.id) 
							LEFT JOIN mdl_department_course dc ON dc.courseid = c.id AND dc.is_active = 1
							LEFT JOIN mdl_user_course_mapping as ucm ON ucm.courseid = c.id AND ucm.`status` = 1 AND ucm.userid = $USER->id
							WHERE c.publish = 1 AND c.id != 1 AND c.is_active = 1 AND c.deleted = '0' AND ucm.id IS NULL  AND c.coursetype_id = 1 AND (c.enddate + 86399) >=".strtotime('now')." $WhManager  )";
					
							
			$classroomSql  = "(	
									SELECT CONCAT_WS('_', CAST(0 AS CHAR CHARSET utf8), CAST(c.id AS CHAR CHARSET utf8)) AS difId,0 AS pid,c.*,mcc.name as categoryname,s.id as classid
									FROM mdl_course AS c
									LEFT JOIN {$CFG->prefix}course_categories mcc on (c.category = mcc.id) 
									LEFT JOIN mdl_scheduler as s ON s.course = c.id
									WHERE c.publish = 1 AND c.id != 1 AND c.is_active = 1 AND c.deleted = '0' AND c.coursetype_id = 2 AND s.isactive = 1 AND s.enrolmenttype = 0  AND (s.enddate + 86399) >=".strtotime('now');
			$classroomSql  .= "		AND (s.teacher = ".$USER->id." || s.course IN (SELECT cc.id FROM mdl_course cc WHERE cc.coursetype_id = 2 AND (cc.primary_instructor = ".$USER->id." || cc.createdby = ".$USER->id." )))";
			$classroomSql  .= ")";
			$selectSql = $courseSql.' UNION '.$classroomSql;
			$programCoursesSql = "SELECT *
							FROM (
									##SELECT_SQL##
								) as catalog ORDER BY catalog.timecreated DESC
								LIMIT 0,5";

			$programCoursesSql = str_replace("##SELECT_SQL##",$selectSql,$programCoursesSql);
			$records = $DB->get_records_sql($programCoursesSql);
			return $records;
			
		}elseif($USER->archetype == $CFG->userTypeStudent){
			$courseSql  = "(SELECT 0 AS pid, c.*, mcc.name as categoryname, '' as classname,0 as classid 
							FROM mdl_course AS c 
							LEFT JOIN {$CFG->prefix}course_categories mcc on (c.category = mcc.id) 
							LEFT JOIN mdl_department_course dc ON dc.courseid = c.id AND dc.is_active = 1
							LEFT JOIN mdl_user_course_mapping as ucm ON ucm.courseid = c.id AND ucm.`status` = 1 AND ucm.userid = $USER->id
							WHERE c.publish = 1 AND c.id != 1 AND c.is_active = 1 AND c.deleted = '0' AND (dc.departmentid = '".$USER->department."' || ".$whExtra.")  AND ucm.id IS NULL  AND c.coursetype_id = 1 AND (c.enddate + 86399) >=".strtotime('now')."
						)";
			$courseSql  .= " UNION (SELECT 0 AS pid, c.*, mcc.name as categoryname, '' as classname,0 as classid 
						FROM mdl_groups_members as gm
						LEFT JOIN mdl_groups_course gc ON (gm.groupid = gc.groupid ".$parentGroupCond.") AND gc.is_active = 1
						LEFT JOIN mdl_course AS c ON gc.courseid = c.id AND gc.is_active = 1
								LEFT JOIN {$CFG->prefix}course_categories mcc on (c.category = mcc.id) 
						LEFT JOIN mdl_user_course_mapping AS ucm ON ucm.courseid = c.id AND ucm.`status` = 1 AND ucm.userid = ".$USER->id."
						LEFT JOIN mdl_context AS ct ON ct.instanceid = c.id AND ct.contextlevel = 50
						LEFT JOIN mdl_files AS f ON f.contextid = ct.id AND f.component = 'course' AND f.filearea = 'overviewfiles' AND f.filename !='' AND f.filename !='.'
						WHERE c.publish = 1 AND c.id != 1 AND c.is_active = 1 AND ucm.id IS NULL AND c.coursetype_id = 1 AND gm.userid = ".$USER->id." AND  gm.is_active = 1 AND (c.enddate + 86399) >=".strtotime('now')."
						)";
			
			$classroomSql  = "(	SELECT 0 AS pid, c.*, mcc.name as categoryname, s.name as classname,s.id as classid 
								FROM mdl_course AS c
								LEFT JOIN {$CFG->prefix}course_categories mcc on (c.category = mcc.id) 
								LEFT JOIN mdl_scheduler as s ON s.course = c.id
								LEFT JOIN mdl_scheduler_enrollment as se ON se.scheduler_id = s.id AND se.userid = $USER->id
								WHERE c.publish = 1 AND c.id != 1 AND c.is_active = 1 AND c.deleted = '0' AND se.is_approved NOT IN (1,2) AND c.coursetype_id = 2  AND s.enrolmenttype = 0 AND (s.enddate + 86399) >=".strtotime('now')." AND (se.is_approved != 1 && se.is_approved != 2)
								GROUP BY c.id, s.id, se.userid)";
			$selectSql = $courseSql.' UNION '.$classroomSql;
			
			$fields = " @a:=@a+1 `serial_number`, pid, categoryname, classname, id, is_global, category, sortorder, fullname, shortname, idnumber, summary, summaryformat, suggesteduse, suggesteduseformat, learningobj, learningobjformat, performanceout, format, startdate, enddate, visible, timecreated, timemodified, is_active, deleted, keywords, author, coursetype_id, primary_instructor, classroom_instruction, classroom_course_duration, publish, criteria, credithours, createdby, modifiedby,classid";
			$programCoursesSql = "SELECT $fields FROM (
									##SELECT_SQL##
								) as catalog , (SELECT @a:= 0) AS X ORDER BY catalog.timecreated DESC
								LIMIT 0,5";

			$programCoursesSql = str_replace("##SELECT_SQL##",$selectSql,$programCoursesSql);		
			$records = $DB->get_records_sql($programCoursesSql);
			return $records;
		}
		
	}
	
	function getAllUserRoleDetails($extraCond=''){
		global $USER,$DB;

        $query = "SELECT u.id,r.name,ra.roleid 
													FROM mdl_user AS u
													LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id
													LEFT JOIN mdl_role AS r ON r.id = ra.roleid
													WHERE 1 = 1 AND u.deleted = 0 AND ra.roleid IS NOT NULL";
		if($extraCond){
		  $query .= $extraCond;
		}											
		$userRoleDetail = $DB->get_records_sql($query);
		return $userRoleDetail;
	}
	
	/**
	 * This function is using to get all new courses for dashboard
	 * @global object
	 * @return html $html
	 */
	 
	 
	function getDashboardNewCoursesHtml(){
		global $DB, $CFG, $USER;
		
		$html = '';
		$userId = $USER->id;
	
	    $cngCourses = getComplianceAndGlobalCourses();
		$complianceCourses = $cngCourses['compliance'];
		$globalCourses = $cngCourses['global'];
		
		//pr($cngCourses);die;
		$newCoursesSortedArray = getCourseByUserForDashboard();
		//pr($newCoursesSortedArray);die;
		$userCourseId = getAssignedCourseIdForUser($userId);
		
		$isBelongToED = isLoginUserBelongsToED(); 
		
		if(count($newCoursesSortedArray) > 0 ){ 
		  
			$html = '<div class="bxslider1">';
			
			
			if($USER->archetype == $CFG->userTypeManager || (isset($USER->original_archetype) && $USER->original_archetype == $CFG->userTypeManager)) {
			  $managerCourse = getManagerCourse();
			}elseif($USER->archetype == $CFG->userTypeStudent) {
			  $depCourses = getDepartmentCourse($USER->department);
			  
			}
		
			foreach($newCoursesSortedArray as $newCourse){

			$courseId = $newCourse->id;
			$classId = $newCourse->classid;
			$reluser = $USER->id;
			$fullName = $newCourse->fullname;
			$className = $newCourse->classname;
			$categoryName = $newCourse->categoryname;
			$cCreatedBy = $newCourse->createdby;
			
			$newCourseImage = getCourseImage($newCourse);
			$complianceDiv = getCourseComplianceIcon($courseId);
			$a->name = $fullName;
			$dataMsg = get_string('areyousureyouwanttoenrolthiscourse','course', $a->name);
			$clickToManage = get_string('clicktomanage','admindashboard');
			$enrollUser = get_string('enroluser','course');
			$requestCourse = get_string('requestcourse','course');
			$launchCourse = get_string('launchcourse','course');
			$courseTypeId = getCourseTypeIdByCourseId($newCourse->id);
			$classProgram = '';
			$relclass = "";
			if($courseTypeId == $CFG->courseTypeClassroom ){
				$classProgram = ' differentiate';
				$relclass = " relclass = '".$classId."' reluser = '".$reluser."'";
			}
			
			$linkUrl = '<a href="javascript:;" class="newcoursebox'.$classProgram.'" '.$relclass.'>';
			$linkUrlLabel = '';
						  
			if($USER->archetype == $CFG->userTypeAdmin ){
			      /*$linkUrl = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$newCourse->id.'" class="newcoursebox'.$classProgram.'" title="'.$clickToManage.'">';*/
				  $linkUrl = '<a href="'.$CFG->wwwroot.'/course/index.php?key='.$fullName.'" class="newcoursebox'.$classProgram.'" title="'.$clickToManage.'">';
			      $linkUrlLabel = $clickToManage;
			}elseif($USER->archetype == $CFG->userTypeManager ){

				if( in_array($courseId, $managerCourse ) ) {
				  /*$linkUrl = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$newCourse->id.'" class="newcoursebox '.$classProgram.'" title="'.$clickToManage.'">';*/
				  $linkUrl = '<a href="'.$CFG->wwwroot.'/course/index.php?key='.$fullName.'" class="newcoursebox'.$classProgram.'" title="'.$clickToManage.'">';
				  $linkUrlLabel = $clickToManage;
				}else{
				
				  if($USER->original_archetype == $CFG->userTypeAdmin){
						/*  $linkUrl = '<a href="javascript:;" data-url="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&enrolflag=1&from='.$_SERVER['PHP_SELF'].'" data-url="'.$courseId.'"  data-msg="'.$dataMsg.'" class="enrollment-courseB" title="'.$enrollUser.'">';*/
						  $linkUrl = '<a href="javascript:;" data-url="'.$courseId.'"  data-msg="'.$dataMsg.'" class="enrollment-courseB'.$classProgram.'" title="'.$enrollUser.'">';
						  $linkUrlLabel = $enrollUser;
				  }else{
						
					 if($CFG->allowExternalDepartment == 1 && $isBelongToED){
					 
					        if($courseTypeId == $CFG->courseTypeClassroom ){
							    $linkUrl = '<a href="'.$CFG->wwwroot.'/course/index.php?key='.$fullName.'" class="newcoursebox'.$classProgram.'" title="'.$clickToManage.'">';
								$linkUrlLabel = $clickToManage;
							}else{
							  $linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" class="assign-course requestCourse'.$classProgram.'" rel = "'.$courseId.'" title="'.$requestCourse.'">';
						 
							  $linkUrlLabel = $requestCourse;
							}
							
					 }else{	
						
							 if(!empty($complianceCourses) && (in_array($courseId, $complianceCourses))){
								 /*$linkUrl = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$newCourse->id.'" class="newcoursebox '.$classProgram.'" title="'.$clickToManage.'">';*/
								 //$linkUrl = '<a href="'.$CFG->wwwroot.'/course/index.php?key='.$fullName.'" class="newcoursebox'.$classProgram.'" title="'.$clickToManage.'">';
								 $linkUrl = '<a href="'.$CFG->wwwroot.'/my/course_catalog.php?course=course&key='.$fullName.'" class="newcoursebox '.$classProgram.'" rel = "'.$courseId.'" title="'.$clickToManage.'">';
								 $linkUrlLabel = $clickToManage;
							 }elseif(!empty($globalCourses) && (in_array($courseId, $globalCourses))){
								 /*$linkUrl = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$newCourse->id.'" class="newcoursebox '.$classProgram.'" title="'.$clickToManage.'">';*/
								//$linkUrl = '<a href="'.$CFG->wwwroot.'/course/index.php?key='.$fullName.'" class="newcoursebox'.$classProgram.'" title="'.$clickToManage.'">';
								//$linkUrlLabel = $clickToManage;
								
								$linkUrl = '<a href="'.$CFG->wwwroot.'/my/course_catalog.php?course=course&key='.$fullName.'" class="newcoursebox '.$classProgram.'" rel = "'.$courseId.'" title="'.$clickToManage.'">';
								 $linkUrlLabel = $clickToManage;
		
							 }else{
							 
								if($courseTypeId == $CFG->courseTypeClassroom ){
								    $linkUrl = '<a href="'.$CFG->wwwroot.'/course/index.php?key='.$fullName.'" class="newcoursebox'.$classProgram.'" title="'.$clickToManage.'">';
									$linkUrlLabel = $clickToManage;
								}else{
								  $linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" class="assign-course requestCourse'.$classProgram.'" rel = "'.$courseId.'" title="'.$requestCourse.'">';
							 
								  $linkUrlLabel = $requestCourse;
								}
							 }
                       }
				  }	
				  
				}
			}elseif($USER->archetype == $CFG->userTypeStudent ){

				$assignGroupsForUser = getAssignGroupsForUser($userId, 3);
				$requestClass = "request-for-class ";
				if($USER->original_archetype == $CFG->userTypeAdmin || $USER->original_archetype == $CFG->userTypeManager){
					$requestClass = "enrol_user";
				}
				if( in_array($courseId, $userCourseId ) ) {
					  $linkUrl = '<a href="'.$CFG->wwwroot.'/course/courselaunch.php?id='.$courseId.'" title="'.$launchCourse.'" class = "launch-course-dashboard assign-course">';
					  $linkUrlLabel = $launchCourse;
				}else if(!empty($complianceCourses) && (in_array($courseId, $complianceCourses))){
					 if($classId != 0){
						 $linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" rel = "'.$courseId.'" class="assign-course '.$requestClass.$classProgram.'" title="'.$requestCourse.'" '.$relclass.'>';
						 $linkUrlLabel = $requestCourse;
					 }else{
						$linkUrl = '<a href="javascript:;" data-url="'.$courseId.'"  data-msg="'.get_string('areyousureyouwanttoenrolthiscourse','course', $a->name).'" class="enrollment-courseB'.$classProgram.'" title="'.$enrollUser.'">';
						$linkUrlLabel = $enrollUser;
					 }
												 
				}elseif(!empty($globalCourses) && (in_array($courseId, $globalCourses))){
					 if($classId != 0){
						 $linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" rel = "'.$courseId.'" class="assign-course '.$requestClass.$classProgram.'" title="'.$requestCourse.'" '.$relclass.'>';
						 $linkUrlLabel = $requestCourse;
					 }else{					
						$linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" ctype = "online" rel = "'.$courseId.'" class="assign-course requestCourse'.$classProgram.'" title="'.$requestCourse.'">';
						$linkUrlLabel = $requestCourse;
					 }
													
				}else{
				
				        $assignedGroupsForCourse = getAssignGroupsForCourse($courseId, 3);
						
						$isUserAndCourseInSameTeam = false;
						
						if(count($assignGroupsForUser) > 0 && count($assignedGroupsForCourse) > 0){

							foreach($assignGroupsForUser as $assignedGroup){
							   if(in_array($assignedGroup, $assignedGroupsForCourse)){
								  $isUserAndCourseInSameTeam = true;
								  break;
							   }
							}
							if( $isUserAndCourseInSameTeam) { 
								 if($classId != 0){
									 $linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" rel = "'.$courseId.'" class="assign-course '.$requestClass.$classProgram.'" title="'.$requestCourse.'" '.$relclass.'>';
									 $linkUrlLabel = $requestCourse;
								 }else{
									$linkUrl = '<a href="javascript:;" data-url="'.$courseId.'"  data-msg="'.get_string('areyousureyouwanttoenrolthiscourse','course', $a->name).'" class="enrollment-courseB'.$classProgram.'" title="'.$enrollUser.'">';
									$linkUrlLabel = $enrollUser;
								 }
							 }else{
							 
								if($USER->original_archetype == $CFG->userTypeAdmin){
									  $linkUrl = '<a href="javascript:;"  data-url="'.$courseId.'" data-msg="'.get_string('areyousureyouwanttoenrolthiscourse','course', $a->name).'" class="enrollment-courseB'.$classProgram.'" title="'.$enrollUser.'">';
									  $linkUrlLabel = $enrollUser;
								}else{
								
									 if( in_array($courseId, $managerCourse ) ) {
										 //$linkUrl = '<a href="'.$CFG->wwwroot.'/course/courselaunch.php?id='.$courseId.'" title="'.$launchCourse.'" class = "launch-course-dashboard assign-course">';
										 //$linkUrlLabel = $launchCourse;
										  if($classId != 0){
											 $linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" rel = "'.$courseId.'" class="assign-course '.$requestClass.$classProgram.'" title="'.$requestCourse.'" '.$relclass.'>';
											 $linkUrlLabel = $requestCourse;
										 }else{
											$linkUrl = '<a href="javascript:;"  data-url="'.$courseId.'" data-msg="'.get_string('areyousureyouwanttoenrolthiscourse','course', $a->name).'" class="enrollment-courseB'.$classProgram.'" title="'.$enrollUser.'">';
											$linkUrlLabel = $enrollUser;
										 }
									  
									 }else{
										 if($classId != 0){
											 $linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" rel = "'.$courseId.'" class="assign-course '.$requestClass.$classProgram.'" title="'.$requestCourse.'" '.$relclass.'>';
											 $linkUrlLabel = $requestCourse;
										 }else{
											 $linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" rel = "'.$courseId.'" class="assign-course requestCourse'.$classProgram.'" title="'.$requestCourse.'">';
											 $linkUrlLabel = $requestCourse;
										 }
									 }
								}
							 } 

														
						 }elseif(in_array($courseId, $depCourses)){
				
							if($USER->original_archetype == $CFG->userTypeAdmin || $USER->original_archetype == $CFG->userTypeManager){
								 if($classId != 0){
									 $linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" rel = "'.$courseId.'" class="assign-course '.$requestClass.$classProgram.'" title="'.$requestCourse.'" '.$relclass.'>';
									 $linkUrlLabel = $requestCourse;
								 }else{
									$linkUrl = '<a href="javascript:;" data-url="'.$courseId.'"  data-msg="'.get_string('areyousureyouwanttoenrolthiscourse','course', $a->name).'" ctype = "online" class="enrollment-courseB'.$classProgram.'" title="'.$enrollUser.'">';
									$linkUrlLabel = $enrollUser;
								 }
							}else{
								if($classId != 0){
									 $linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" rel = "'.$courseId.'" class="assign-course '.$requestClass.$classProgram.'" title="'.$requestCourse.'" '.$relclass.'>';
									 $linkUrlLabel = $requestCourse;
								 }else{
									$linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" rel = "'.$courseId.'" class="assign-course requestCourse'.$classProgram.'" title="'.$requestCourse.'">';
									$linkUrlLabel = $requestCourse;
								 }
							}
							
						 }else{

								  if($USER->original_archetype == $CFG->userTypeManager){
								  
									  if($CFG->allowExternalDepartment == 1 && $isBelongToED){
									  
											if( in_array($courseId, $managerCourse ) ) {
												 if($classId != 0){
													 $linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" rel = "'.$courseId.'" class="assign-course '.$requestClass.$classProgram.'" title="'.$requestCourse.'" '.$relclass.'>';
													 $linkUrlLabel = $requestCourse;
												 }else{
													$linkUrl = '<a href="javascript:;" data-url="'.$courseId.'"  data-msg="'.get_string('areyousureyouwanttoenrolthiscourse','course', $a->name).'" ctype = "online" class="enrollment-courseB'.$classProgram.'" title="'.$enrollUser.'">';
													$linkUrlLabel = $enrollUser;
												 }
											}else{
												
												if($courseTypeId == $CFG->courseTypeClassroom ){
													
													$linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" rel = "'.$courseId.'" class="assign-course '.$requestClass.$classProgram.'" title="'.$requestCourse.'" '.$relclass.'>';
													$linkUrlLabel = $requestCourse;
													/*$linkUrl = '<a href="'.$CFG->wwwroot.'/my/course_catalog.php?classroom=classroom&key='.$fullName.'" class="newcoursebox'.$classProgram.'" title="'.$clickToManage.'">';	$linkUrlLabel = $clickToManage;*/
													
												}else{
										
													if($CFG->CanExternalLearnerRequest == 1){
														if($classId != 0){
															 $linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" rel = "'.$courseId.'" class="assign-course '.$requestClass.$classProgram.'" title="'.$requestCourse.'" '.$relclass.'>';
															 $linkUrlLabel = $requestCourse;
														 }else{
															$linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" ctype = "online" rel = "'.$courseId.'" class="assign-course requestCourse'.$classProgram.'" title="'.$requestCourse.'">';
															$linkUrlLabel = $requestCourse;
														 }
													}else{
													  continue;
													}
												
												}	
										 }
									  }else{
										   if($classId != 0){
											 $linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" rel = "'.$courseId.'" class="assign-course '.$requestClass.$classProgram.'" title="'.$requestCourse.'" '.$relclass.'>';
											 $linkUrlLabel = $requestCourse;
										 }else{
											$linkUrl = '<a href="javascript:;" data-url="'.$courseId.'"  data-msg="'.get_string('areyousureyouwanttoenrolthiscourse','course', $a->name).'" ctype = "online" class="enrollment-courseB'.$classProgram.'" title="'.$enrollUser.'">';
											$linkUrlLabel = $enrollUser;
										 }
									  }
								  }else{
										
										if($courseTypeId == $CFG->courseTypeClassroom ){
											$linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" rel = "'.$courseId.'" class="assign-course '.$requestClass.$classProgram.'" title="'.$requestCourse.'"	'.$relclass.'>';
											$linkUrlLabel = $requestCourse;
											/*$linkUrl = '<a href="'.$CFG->wwwroot.'/my/course_catalog.php?classroom=classroom&key='.$fullName.'" class="newcoursebox'.$classProgram.'" title="'.$clickToManage.'">';		    $linkUrlLabel = $clickToManage;*/
											
										}else{
										   
											if($CFG->allowExternalDepartment == 1 && $isBelongToED){
															 
												if($CFG->CanExternalLearnerRequest == 1){
													if($classId != 0){
														 $linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" rel = "'.$courseId.'" class="assign-course '.$requestClass.$classProgram.'" title="'.$requestCourse.'" '.$relclass.'>';
														 $linkUrlLabel = $requestCourse;
													 }else{
														 $linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" ctype = "online" rel = "'.$courseId.'" class="assign-course requestCourse'.$classProgram.'" title="'.$requestCourse.'">';
														$linkUrlLabel = $requestCourse;
													 }
												}else{
												  continue;
												}
												
										   }else{
												if($classId != 0){
													 $linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" rel = "'.$courseId.'" class="assign-course '.$requestClass.$classProgram.'" title="'.$requestCourse.'" '.$relclass.'>';
													 $linkUrlLabel = $requestCourse;
												 }else{
													$linkUrl = '<a href="'.$CFG->wwwroot.'/course/course_catalog.php?cid='.$courseId.'&mailflag=1&from='.$_SERVER['PHP_SELF'].'" ctype = "online" rel = "'.$courseId.'" class="assign-course requestCourse'.$classProgram.'" title="'.$requestCourse.'">';
													$linkUrlLabel = $requestCourse;
												 }
											}
												  
										  
										}
									 
									 
								  }	 
						 
						    }
						
				}
				
			}
			
		
			$html .= $linkUrl;
			$html .= '<div class="new-course-block">';
				$html .= '<div class="new-course-image">'.$newCourseImage.'</div>';
				$html .= '<div class="new-course-details">';
				if($courseTypeId == $CFG->courseTypeClassroom && $USER->archetype == $CFG->userTypeStudent){
					$html .= '<div class="new-course-name word-wrap">'.($fullName && strlen($fullName.$className) > 34?substr($className."-".$fullName,0,34).'...':($className."-".$fullName)).$complianceDiv.'</div>';
				}else{
					$html .= '<div class="new-course-name word-wrap">'.($fullName && strlen($fullName) > 34?substr($fullName,0,34).'...':$fullName).$complianceDiv.'</div>';
				}
					$html .= '<div class="new-course-category">'.get_string('category','learnercourse').': '.$categoryName.'</div>';
					$html .= '<div class = "new-course-preview-link">'.$linkUrlLabel.' </div>';
				$html .= '</div>';
			$html .= '</div>';
			$html .= '</a>';
	   }
	   $html .= '</div>';
	  }else{
		  $html .= '<div class = "no-reults">'.get_string("no_results").'</div>';
	  }
		  
	  return $html; 
		
	}
	

		
	/**
	 * This function is using for getting a course ids by status (like by "Not started", "In Progress" Or "Completed" ) per user 
	 * @global object
	 * @param int $userId id of user 
	 * @param int $assignedCourseId is optional course id for getting record for this course id only
	 * @param bool $isReport, if true then it will return also unassigned or deleted course id for this user
	 * @param int $courseTypeId is a type of course like online or classroom course
	
	 * @return array $courseStatus course status of all course ids of particular user
	 */
	
	function getCourseIdByStatus($userId, $assignedCourseId = '', $searchString='', $isReport = true, $courseTypeId=''){
	
	  global $DB, $CFG;
	  $sectionArr = array();
	  $courseStatus = array();
	  $courseStatus1 = array();
	  $courseStatus2 = array();
	  $courseTypeId = $courseTypeId?$courseTypeId:$CFG->courseTypeOnline;
	  if($userId){

			  // Topics Course
			   //$querySection = "SELECT mcs.id as sectionid, mcs.section as section, if(mcs.name!='', mcs.name, concat('Topic',' ',mcs.section)) as sectionname, mcs.summary as summary, mcs.sequence, mcs.course as courseid " ;
			  // $querySection .= "  FROM {$CFG->prefix}course_sections mcs LEFT JOIN {$CFG->prefix}course mc ON (mcs.course = mc.id) WHERE mcs.visible = '1' AND mcs.section!='0'  ORDER BY mcs.section ASC";
			  
			   $querySection = "SELECT mcs.id as sectionid, mcs.section as section, if(mcs.name!='', mcs.name, concat('Topic',' ',mcs.section)) as sectionname, mcs.summary as summary, mcs.sequence, mcs.course as courseid, mcc.name as categoryname " ;
			   $querySection .= ", mc.fullname, mc.summary, mc.format as courseformat ";
			  // $querySection .= ", if(md.id, GROUP_CONCAT(DISTINCT  md.title ORDER BY md.title ASC),'".getMDash()."') as departmenttitle ";
			  // $querySection .= ", if(mg.id, GROUP_CONCAT(DISTINCT  mg.name ORDER BY mg.name ASC),'".getMDash()."') as teamtitle  ";
			  // $querySection .= ", if(mu.id, GROUP_CONCAT(DISTINCT  concat(mu.firstname, '<br>', mu.lastname) ORDER BY mg.name ASC),'".getMDash()."') as userfullname  ";
			   $querySection .= "  FROM {$CFG->prefix}course_sections mcs ";
			   $querySection .= " LEFT JOIN {$CFG->prefix}course mc ON (mcs.course = mc.id) ";
			   $querySection .= " LEFT JOIN {$CFG->prefix}course_categories mcc ON (mc.category = mcc.id) ";
			   /*$querySection .= " LEFT JOIN {$CFG->prefix}user_course_mapping mucm_dep ON (mucm_dep.courseid = mc.id AND mucm_dep.type = 'department') ";
			   $querySection .= " LEFT JOIN {$CFG->prefix}department md ON (md.id = mucm_dep.typeid) ";
			   $querySection .= " LEFT JOIN {$CFG->prefix}user_course_mapping mucm_group ON (mucm_group.courseid = mc.id AND mucm_group.type = 'group') ";
			   $querySection .= " LEFT JOIN {$CFG->prefix}groups mg ON (mg.id = mucm_group.typeid) ";
			   $querySection .= " LEFT JOIN {$CFG->prefix}user_course_mapping mucm_user ON (mucm_group.courseid = mc.id AND mucm_user.type = 'user') ";
			   $querySection .= " LEFT JOIN {$CFG->prefix}user mu ON (mu.id = mucm_user.userid) ";*/
			   $querySection .= " WHERE 1 = 1 AND mcs.section!='0' ";
			   if(!$isReport){
			      $querySection .= " AND mcs.visible = '1' AND mc.is_active = '1' ";
			   }
			   $querySection .= " AND mc.coursetype_id = '".$courseTypeId."' ";
			   $querySection .= " AND mc.id != '1'  AND mc.deleted = '0' AND mc.publish = '1'  $searchString  ";
			   
			   $querySection .= " GROUP BY mc.id ";	
			   //$querySection .= " ORDER BY mcs.section ASC ";	
			   $querySection .= " ORDER BY mc.fullname ASC ";	
				   
               //echo $querySection."<br>";die;
			   $sectionArr = $DB->get_records_sql($querySection);
			
			   if(count($sectionArr) > 0){
			   
			        $userCourses = getAssignedCourseIdForUser($userId, $isReport);
					foreach($sectionArr as $sectionid=>$sections){
				 
				         if($sections->sequence && $sectionid){
						 
						     $courseId = $sections->courseid;
							 $courseName = $sections->fullname;
							 $courseSummary = $sections->summary;
							 $courseFormat = $sections->courseformat;
							 $categoryName = $sections->categoryname;
						
							 
							 //$departmentTitle = $sections->departmenttitle;
							 //$teamTitle = $sections->teamtitle;
							 //$AssignedUserFullName = $sections->userfullname;
							 
							 
							 if(!in_array($courseId, $userCourses)){ // if user is not enrol in this course then loop will continue without going ahead
							   continue;
							 }
							 
							 $queryCM = "SELECT mcm.* " ;
							 $queryCM .= " FROM {$CFG->prefix}course_modules mcm ";
							 $queryCM .= " WHERE 1 = 1";
							 $queryCM .= " AND mcm.id IN (".$sections->sequence.") ";
							 $queryCM .= " AND mcm.section = '".$sectionid."' ";
							 $queryCM .= " AND mcm.course = '".$courseId."' ";
							 //$queryCM .= " AND mcm.module in(17,18)";
							 if(!$isReport){
							   $queryCM .= " AND mcm.visible = '1' ";
							 }
							 
							 $cmArr = $DB->get_records_sql($queryCM);
							 
					
							 if(count($cmArr) > 0 ){ 
							     
								 foreach($cmArr as $cm){
									 
										 $cmId = $cm->id;
									 $moduleId = $cm->module;
									 $instance = $cm->instance;
									  $section = $cm->section;
									
									 $module = $moduleId; 
									 
									 if($module == 18 && $instance){ // scrom
								        
										$select = " mss.course IN ($courseId)";
										$select .= " AND mss.id IN ($instance)";
										$sort = ' ORDER BY mc.id DESC';
								 
										 $query = "SELECT ";
										 $query .= "mss.id, mc.id as courseid, mc.fullname as fullname, mc.summary as summary, mc.format as courseformat, mss.id as scormid, mss.launch , mss.name as scormname, mss.intro as scormsummary, mss.timeopen as scormtimeopen, mss.timeclose as scormtimeclose, mcc.id as catid, mcc.name as categoryname, mc.timecreated as ctimecreated, 'scorm' as coursetype ";
										
										$query .= " , (  if((SELECT  msst.timemodified FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' ) WHERE msst.userid = '".$userId."' AND  element = 'cmi.core.score.raw' AND ms.id = mss.id  AND mc.coursetype_id = '".$courseTypeId."' AND mc.deleted = '0' AND mc.publish = '1'  ORDER BY msst.id DESC LIMIT 1)!='', 
										
										(SELECT  msst.timemodified FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' ) WHERE msst.userid = '".$userId."' AND  element = 'cmi.core.score.raw' AND ms.id = mss.id AND mc.coursetype_id = '".$courseTypeId."' AND mc.deleted = '0' AND mc.publish = '1' ORDER BY msst.id DESC LIMIT 1),
										
										(SELECT  msst.value FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' ) WHERE msst.userid = '".$userId."' AND  element = 'x.start.time' AND ms.id = mss.id  AND mc.coursetype_id = '".$courseTypeId."' AND mc.deleted = '0' ORDER BY msst.id DESC LIMIT 1)
										 )
										
										 )  as lastaccessed ";
										 
										$element = 'cmi.core.score.raw';
										$query .= "  ,(SELECT  MAX(value) FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' ) WHERE msst.userid = '".$userId."' AND element = '".$element."' AND ms.id = mss.id AND mc.coursetype_id = '".$courseTypeId."' AND mc.deleted = '0' LIMIT 1)  as score";
										
										
										$element = 'cmi.core.total_time';
										$query .= "  ,(SELECT  value FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' ) WHERE msst.userid = '".$userId."' AND  element = '".$element."' AND ms.id = mss.id AND mc.coursetype_id = '".$courseTypeId."' AND mc.deleted = '0' ORDER BY msst.id DESC LIMIT 1 )  as timespent";
										
										$element = "'cmi.core.lesson_status','cmi.completion_status'";
										//$query .= "  ,(SELECT  value FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' )  WHERE msst.userid = '".$userId."' AND  element in (".$element.") AND ms.id = mss.id  ORDER BY msst.id DESC LIMIT 1 )  as scormstatus";
										
										
										$query .= " , (  if((SELECT value FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' )  WHERE msst.userid = '".$userId."' AND  element in (".$element.") AND ms.id = mss.id  AND mc.coursetype_id = '".$courseTypeId."' AND mc.deleted = '0' ORDER BY msst.id DESC LIMIT 1 )!='', 
										
										(SELECT  value FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' )  WHERE msst.userid = '".$userId."' AND  element in (".$element.") AND ms.id = mss.id AND mc.coursetype_id = '".$courseTypeId."'  AND mc.deleted = '0' ORDER BY msst.id DESC LIMIT 1 ),
										
										(if((SELECT  msst.timemodified FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}scorm ms ON (mc.id = ms.course) LEFT JOIN {$CFG->prefix}scorm_scoes_track msst ON (ms.id = msst.scormid ) LEFT JOIN {$CFG->prefix}scorm_scoes ms_sco ON (ms_sco.id = msst.scoid AND ms_sco.scorm = msst.scormid AND ms_sco.scormtype = 'sco' ) WHERE msst.userid = '".$userId."' AND  element = 'x.start.time' AND ms.id = mss.id AND mc.coursetype_id = '".$courseTypeId."' AND mc.deleted = '0' ORDER BY msst.id DESC LIMIT 1)!='', 'incomplete','')
										 ))
										
										 )  as scormstatus ";
										 
										//$element = 'x.start.time';
									
										  $query .= "  FROM  {$CFG->prefix}scorm mss LEFT JOIN {$CFG->prefix}course mc ON (mc.id = mss.course) ";
										  $query .= "  LEFT JOIN {$CFG->prefix}course_categories mcc ON (mc.category = mcc.id) ";
										  $query .= "  WHERE $select AND mss.id!='' AND mc.format = 'topics' ";
										  if(!$isReport){
											 $query .= " AND mss.is_active = 1";
										  }else{
										     $query .= " AND mss.is_active = 1";
										  }
										  $query .= " AND mc.coursetype_id = '".$courseTypeId."' AND mc.deleted = '0' ";
										  $query .= "$sort";
										  
										//echo $query."<br>";
										
										
										$scromArr = $DB->get_records_sql($query);
										if(count($scromArr) > 0){ 
										   
										   $z=0;
										   foreach($scromArr as $arr){ 
										        
												 $z++; 
										         $scormStatus = $arr->scormstatus;
												 $coursesScore = $arr->score;
												 $lastaccessed = $arr->lastaccessed;
												 //$coursename = $arr->fullname;
												 //$summary = $arr->summary;
												 //$courseformat = $arr->courseformat;
												 $courseStatus2[$courseId]['courseInstance'][$instance."_18"]['type'] = 'scorm';
												 $courseStatus2[$courseId]['courseInstance'][$instance."_18"]['courseid'] = $courseId;
												 $courseStatus2[$courseId]['courseInstance'][$instance."_18"]['instanceid'] = $arr->id;
												 $courseStatus2[$courseId]['courseInstance'][$instance."_18"]['title'] = $arr->scormname;

												 $courseStatus2[$courseId]['courseInstance'][$instance."_18"]['info'] = $arr->scormsummary;
												 //$scoreResult = $coursesScore!=''?($coursesScore." ".get_string('points','learnercourse')):getMDash();
												 $courseStatus2[$courseId]['courseInstance'][$instance."_18"]['score'] = $coursesScore;
												 $courseStatus2[$courseId]['courseInstance'][$instance."_18"]['lastaccessed'] = $lastaccessed;
												
															  
												  if($z == 1){
													   if(isset($courseStatus2[$courseId]['lastaccessed']) && $courseStatus2[$courseId]['lastaccessed']){
														 if($lastaccessed > $courseStatus2[$courseId]['lastaccessed']){
															$courseStatus2[$courseId]['lastaccessed'] = $lastaccessed;
															$courseStatus2[$courseId]['score'] = $coursesScore;
														 }
													   }else{
														$courseStatus2[$courseId]['lastaccessed'] = $lastaccessed;
														$courseStatus2[$courseId]['score'] = $coursesScore;
													   }
													   
												
													   $courseStatus2[$courseId]['courseId'] = $courseId;
													   $courseStatus2[$courseId]['coursename'] = $courseName;
													   $courseStatus2[$courseId]['fullname'] = $courseName;
													   $courseStatus2[$courseId]['summary'] = $courseSummary;	
													   $courseStatus2[$courseId]['courseformat'] = $courseFormat;
													   $courseStatus2[$courseId]['categoryname'] = $categoryName;	
													   //$courseStatus2[$courseId]['departmentTitle'] = $departmentTitle;	
													   //$courseStatus2[$courseId]['teamTitle'] = $teamTitle;	
													   
													   //$courseStatus2[$courseId]['AssignedUserFullName'] = $AssignedUserFullName;			
												  } 
												 
												
											     if($scormStatus == ''){
												   $courseStatus2[$courseId]['courseInstance'][$instance."_18"]['status'] = $CFG->notstarted;
												   $courseStatus2[$courseId]['courseStatusArr'][] = 3;  
												 }elseif(in_array($scormStatus ,array('incomplete','failed'))){
												   $courseStatus2[$courseId]['courseInstance'][$instance."_18"]['status'] = $CFG->inprogress;
												   $courseStatus2[$courseId]['courseStatusArr'][] = 1;  
												 }elseif(in_array($scormStatus, array('passed','completed'))){  
												   $courseStatus2[$courseId]['courseInstance'][$instance."_18"]['status'] = $CFG->completed;  
												   $courseStatus2[$courseId]['courseStatusArr'][] = 2;  
												 }elseif($scormStatus == 'failed'){
												   $courseStatus2[$courseId]['courseInstance'][$instance."_18"]['status'] = $CFG->completed;
												   $courseStatus2[$courseId]['courseStatusArr'][] = 2;    
												 }
												 
										   }
										   
										  
										   
										  
										 
										}
										
										//pr($courseStatus2);die;
										
									 }elseif($module == 17 && $instance){  // resource
									
																   
										   $instanceArr = explode(",", $instance);
									
										   if(count($instanceArr) > 0 ){
										   
										     $y = 0;
										     foreach($instanceArr as $iid){
											     
												  $y++;
											    									  
											      $query = "SELECT mr.*, mc.fullname, mc.summary, mc.format as courseformat from {$CFG->prefix}resource mr LEFT JOIN {$CFG->prefix}course mc ON (mc.id = mr.course) WHERE mr.course IN ($courseId) AND mr.id = '$iid'  ";
												  if(!$isReport){
												     $query .= " AND mr.is_active = '1'";
												  }else{
												    $query .= " AND mr.is_active = '1'";
												  }
												 $query .= " AND mc.coursetype_id = '".$courseTypeId."' AND mc.deleted = '0' ";
												 //echo $query."<br>";
												  $resourceArr = $DB->get_records_sql($query);
												  sort($resourceArr);
												  
												  if(count($resourceArr) > 0){
												 
													  $courseStatus2[$courseId]['courseInstance'][$instance."_17"]['type'] = 'resource';
													  $courseStatus2[$courseId]['courseInstance'][$instance."_17"]['courseid'] = $courseId;
													  $courseStatus2[$courseId]['courseInstance'][$instance."_17"]['instanceid'] = $instance;
													  $courseStatus2[$courseId]['courseInstance'][$instance."_17"]['score'] = '';
									  
													  $filedownloaded = isUserAccessedCourse($userId, $courseId, $iid, $tableFields = 'timeaccess');
																			  
													  if(isset($filedownloaded->timeaccess) && $filedownloaded->timeaccess){  
														 $courseStatus2[$courseId]['courseInstance'][$instance."_17"]['status'] = get_string('downloaded','learnercourse');
														 $courseStatus2[$courseId]['courseStatusArr'][] = 2; 
														 $timeaccess = $filedownloaded->timeaccess;
														 $courseStatus2[$courseId]['courseInstance'][$instance."_17"]['title'] = $resourceArr[0]->name;
														 $courseStatus2[$courseId]['courseInstance'][$instance."_17"]['info'] = $resourceArr[0]->intro;
														   
													  }else{
														 $courseStatus2[$courseId]['courseInstance'][$instance."_17"]['status'] = get_string('notdownloaded','learnercourse');
														 $courseStatus2[$courseId]['courseStatusArr'][] = 3;  
														 $timeaccess = 0; 
														 $courseStatus2[$courseId]['courseInstance'][$instance."_17"]['title'] = $resourceArr[0]->name;
														 $courseStatus2[$courseId]['courseInstance'][$instance."_17"]['info'] = $resourceArr[0]->intro;
													  }
													  
													  $courseStatus2[$courseId]['courseInstance'][$instance."_17"]['lastaccessed'] = $timeaccess;
													  
													  if($y == 1){
														   if(isset($courseStatus2[$courseId]['lastaccessed']) && $courseStatus2[$courseId]['lastaccessed']){
															 if($timeaccess > $courseStatus2[$courseId]['lastaccessed']){
																$courseStatus2[$courseId]['lastaccessed'] = $timeaccess;
																$courseStatus2[$courseId]['score'] = getMDash();
															 }
														   }else{
															$courseStatus2[$courseId]['lastaccessed'] = $timeaccess;
															$courseStatus2[$courseId]['score'] = '';
														   }
														   
														   /*$courseStatus2[$courseId]['coursename'] = $resourceArr[0]->fullname;	
														   $courseStatus2[$courseId]['summary'] = $resourceArr[0]->summary;	
														   $courseStatus2[$courseId]['courseformat'] = $resourceArr[0]->courseformat;	*/
										
														   $courseStatus2[$courseId]['courseId'] = $courseId;
														   $courseStatus2[$courseId]['coursename'] = $courseName;
														   $courseStatus2[$courseId]['fullname'] = $courseName;
														   $courseStatus2[$courseId]['summary'] = $courseSummary;	
														   $courseStatus2[$courseId]['courseformat'] = $courseFormat;
														   $courseStatus2[$courseId]['categoryname'] = $categoryName;	
														   //$courseStatus2[$courseId]['departmentTitle'] = $departmentTitle;	
														  // $courseStatus2[$courseId]['teamTitle'] = $teamTitle;	
														  // $courseStatus2[$courseId]['AssignedUserFullName'] = $AssignedUserFullName;	
													  } 
												  
												 }
												 
												 
											  }  
									      
										  }   
										  
										  
											
									 }
			
								 } // end inner foreach
								 
								
							   
							 } // end count($cmArr) if
							 
							 
							 
						 } // end of $sections->sequence && $sectionid if
						 
						 	 
				   } // end outer foreach
				  
	
			   } // end outer if
	   }  // end outermost if



	  if(count($courseStatus2) > 0 ){
	   
	     $courseStatus2['type'][1] = array();
		 $courseStatus2['type'][2] = array();
		 $courseStatus2['type'][3] = array();
	     foreach($courseStatus2 as $cid=>$vArr){
		 
		     if(is_int($cid)){
				   if(!in_array(1,$vArr['courseStatusArr']) && !in_array(3,$vArr['courseStatusArr']) && in_array(2, $vArr['courseStatusArr'])){
					  $courseStatus2[$cid]['courseStatus'] = $CFG->completed; // Completed
					  $courseStatus2['type'][2][] = $cid;
				   }elseif(!in_array(1,$vArr['courseStatusArr']) && !in_array(2,$vArr['courseStatusArr']) && in_array(3,$vArr['courseStatusArr'])){
					  $courseStatus2[$cid]['courseStatus'] = $CFG->notstarted; // Not started
					  $courseStatus2['type'][3][] = $cid;
				   }else{
					  $courseStatus2[$cid]['courseStatus'] = $CFG->inprogress; // In progress
					  $courseStatus2['type'][1][] = $cid;
				   }
				   
				   $courseStatus2['allids'][] = $cid;
				   sort($courseStatus2['allids']);
				   
			}	   
  
			  
	     }
		 
	  }	  
 
 

       if(count($courseStatus2) > 0 ){
	   
	      foreach($courseStatus2 as $k=>$csArr){
		 
		   if(is_int($k)){
		      $courseStatus['courses'][$k] = $csArr;
		   }
		    
		  }
	      $courseStatus['allids'] = $courseStatus2['allids'];
		  $courseStatus['type'][1] = $courseStatus2['type'][1];
		  $courseStatus['type'][2] = $courseStatus2['type'][2];
		  $courseStatus['type'][3] = $courseStatus2['type'][3];
	   }
	  
	   if($assignedCourseId){ 
	      return $courseStatus['courses'][$assignedCourseId];
	   }
	   //pr($courseStatus);die;

	   return $courseStatus;
	  
	}
	

	/**
	 * This function is using for getting all users by department, team
	 * @global object
	 * @param array $departmentArr array of selected deparmtnet id 
	* @param array $teamArr array of selected team id
	* @param array $jobTitleArr array of selected job title id 
	* @param array $companyArr array of selected company id
	* @param array $selectedUserArr array of selected user id 
	 * @param bool $isReport if true then records will not check deleted, is_active condition
	 * @return string the dropdrown html for users.
	 */
		
	function getUserListByDnT($deptArr, $teamArr, $selectedUserArr, $return = 1, $isReport = false, $checkInstructor = 0, $jobTitleArr = array('-1'), $companyArr = array('-1'), $selMode = 1,$searchOwner = 0){
		global $DB, $CFG, $USER;
		
		    $userIdArr = array();
			$groupsMembers = array();
			$searchcondition = '';
            
			$roleChk = ''; 
			/*switch($USER->archetype){
				CASE $CFG->userTypeAdmin:
					$roleChk = "AND ra.roleid IN( ".$CFG->studentTypeId.",".$CFG->managerTypeId.")";
				break;
				CASE $CFG->userTypeManager:
				      $roleChk = "AND ra.roleid IN( ".$CFG->studentTypeId.",".$CFG->managerTypeId.")";
				break;
			}*/
			$users = array();
			if($USER->archetype == $CFG->userTypeManager || $USER->archetype == $CFG->userTypeStudent) {	
				$users = fetchGroupsUserIds(0,1);
			}
			$queryUser = "SELECT u.id, CONCAT(u.firstname, ' ', u.lastname) AS fullname
								  FROM {$CFG->prefix}user u
								  LEFT JOIN {$CFG->prefix}role_assignments ra ON (ra.userid = u.id )
								  LEFT JOIN {$CFG->prefix}role r ON r.id = ra.roleid 
			LEFT JOIN mdl_department_members as mdm ON u.id = mdm.userid
LEFT JOIN mdl_department as md ON md.id = mdm.departmentid ";
			$userIdArray = array();
			
			
			if($selMode == 2){
			//123
				if($searchOwner != 1){
					$userIdArray = getOpenTeamsUser($teamArr);
				}else{
					$userIdArray = getOpenTeamsOwners($teamArr);	
				}
				
				/*if(!empty($users)){
					$userIdArray = array_merge($userIdArray, $users);
				}*/
				if(count($userIdArray) > 0) {				
					$userIds = implode(",", array_unique($userIdArray));			  
					$searchcondition = " AND u.id IN (".$userIds.")";
				} else {
					$searchcondition = " AND u.id IN (0)";
				}
					
			}else{
				if( ($deptArr[0] == '-1') && ($teamArr[0] == '-1') ) {
					if($USER->archetype == $CFG->userTypeManager) {
						$deptArr = array($USER->department);
						
						$userIdArray = array();
						foreach($deptArr as $deptKey => $deptVal) {
							if($searchOwner == 1){
								$query = "SELECT g.group_owner FROM mdl_groups g LEFT JOIN mdl_group_department gd ON gd.team_id = g.id WHERE gd.id IS NOT NULL AND gd.department_id = ".$deptVal." AND gd.is_active = 1";
							}else{
								$query = "SELECT dm.userid
										  FROM {$CFG->prefix}department_members dm
										  WHERE dm.departmentid =  '".$deptVal."' AND dm.is_active = 1";

							}
									 
							$groupsMembers = $DB->get_records_sql($query);
							if(count($groupsMembers) > 0) {
								$userIdArray = array_merge($userIdArray, array_keys($groupsMembers));
							}
						}
						
						$userArr = array();
						if(count($userIdArray) > 0) {
							if(!empty($users)){
								$userIdArray = array_merge($userIdArray, $users);
							}	
							$userIds = implode(",", array_unique($userIdArray));
							$searchcondition = " AND u.id IN (".$userIds.")";
						} else {
							$searchcondition = " AND u.id IN (0)";
						}
					}elseif($USER->archetype == $CFG->userTypeStudent){
						if(!empty($users)){
							$userIds = implode(",", array_unique($users));
							$searchcondition = " AND u.id IN (".$userIds.")";
						}	
					}else{
						if($searchOwner == 1){
							$query = "SELECT g.group_owner FROM mdl_groups g LEFT JOIN mdl_group_department gd ON gd.team_id = g.id WHERE gd.id IS NOT NULL  AND gd.is_active = 1";
							$groupsMembers = $DB->get_records_sql($query);
							if(count($groupsMembers) > 0) {
								$userIds = array();
								foreach($groupsMembers as $gMember){
									$userIds[] = $gMember->group_owner;
								}
								$userIds = implode(",",$userIds);
								$searchcondition .= " AND u.id IN (".$userIds.")";
							}
						}	
					}
				} else if( ($deptArr[0] != '-1') && ($teamArr[0] != '-1') ) {				
					foreach($deptArr as $deptKey => $deptVal) {
						foreach($teamArr as $teamKey => $teamVal) {
							$query = "SELECT gm.userid
										  FROM {$CFG->prefix}groups_members gm
										  LEFT JOIN {$CFG->prefix}group_department gd ON ( gm.groupid = gd.team_id )
										  WHERE gm.is_active = 1 AND gd.is_active = 1 AND gm.groupid =  '".$teamVal."'
												AND gd.department_id =  '".$deptVal."'";
									 
							$groupsMembers = $DB->get_records_sql($query);
							if(count($groupsMembers) > 0) {
								$userIdArray = array_merge($userIdArray, array_keys($groupsMembers));
							}
						}
					}
					
					$userArr = array();
					if(count($userIdArray) > 0) {				
						$userIds = implode(",", array_unique($userIdArray));			  
						$searchcondition = " AND u.id IN (".$userIds.")";
					} else {
						$searchcondition = " AND u.id IN (0)";
					}
				} else if( ($deptArr[0] != '-1') && ($teamArr[0] == '-1') ) {
					$userIdArray = array();
					foreach($deptArr as $deptKey => $deptVal) {
						if($searchOwner == 1){
							$query = "SELECT g.group_owner FROM mdl_groups g LEFT JOIN mdl_group_department gd ON gd.team_id = g.id WHERE gd.id IS NOT NULL AND gd.department_id = ".$deptVal." AND gd.is_active = 1";
						}else{
							$query = "SELECT dm.userid
									  FROM {$CFG->prefix}department_members dm
									  WHERE dm.departmentid =  '".$deptVal."' AND dm.is_active = 1";

						}
						$groupsMembers = $DB->get_records_sql($query);
						if(count($groupsMembers) > 0) {
							$userIdArray = array_merge($userIdArray, array_keys($groupsMembers));
						}
					}
					
					$userArr = array();
					if(count($userIdArray) > 0) {				
						$userIds = implode(",", array_unique($userIdArray));			  
						$searchcondition = " AND u.id IN (".$userIds.")";
					} else {
						$searchcondition = " AND u.id IN (0)";
					}
				} else if( ($deptArr[0] == '-1') && ($teamArr[0] != '-1') ) {
					$userIdArray = array();
					foreach($teamArr as $teamKey => $teamVal) {
						$query = "SELECT gm.userid
									  FROM {$CFG->prefix}groups_members gm
									  WHERE gm.is_active = 1 AND gm.groupid =  '".$teamVal."'";
								 
						$groupsMembers = $DB->get_records_sql($query);
						if(count($groupsMembers) > 0) {
							$userIdArray = array_merge($userIdArray, array_keys($groupsMembers));
						}
					}
					
					$userArr = array();
					if(count($userIdArray) > 0) {				
						$userIds = implode(",", array_unique($userIdArray));			  
						$searchcondition = " AND u.id IN (".$userIds.")";
					} else {
						$searchcondition = " AND u.id IN (0)";
					}
				}
			}	
			if($searchOwner == 1){
				$searchcondition .= " AND u.id IN (SELECT g.group_owner FROM mdl_groups g) ";
			}
			//echo $searchcondition;die;
			if($jobTitleArr[0] != '-1' && $CFG->showJobTitle == 1 ) {
					if(count($jobTitleArr) > 0) {				
						$jobIds = implode(",", $jobTitleArr);			  
						$searchcondition .= " AND u.job_title IN (".$jobIds.")";
					}
			}
			
			if($companyArr[0] != '-1' && $CFG->showCompany == 1 ) {
					if(count($companyArr) > 0) {				
						$companyIds = implode(",", $companyArr);			  
						$searchcondition .= " AND u.company IN (".$companyIds.")";
					}
			}
				
				
	
			$deleteDepartments = $DB->get_field_sql("select group_concat(id) as dep_ids from mdl_department where deleted = 1");
			if(!empty($deleteDepartments)){
				$queryUser .= "  WHERE 1 = 1 AND u.deleted = '0' and u.department NOT IN (".$deleteDepartments.") ".$roleChk.$searchcondition;
			}else{
				$queryUser .= "  WHERE 1 = 1 AND u.deleted = '0' ".$roleChk.$searchcondition;
			}
		
			//$queryUser .= "  WHERE 1 = 1 AND u.deleted = '0' ".$roleChk.$searchcondition;
			if($checkInstructor){
			   $queryUser .= " AND u.is_instructor = '1'";
			}
			$queryUser .= " AND u.id NOT IN (".implode(',',$CFG->excludedUsers).")";
			if($isReport == false){
				$queryUser .= " AND u.suspended = 0";
			}
			$queryUser .= " AND mdm.id IS NOT NULL AND md.id IS NOT NULL";
			$queryUser .= " ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname";
			$userArr = $DB->get_records_sql($queryUser);			
			
			/////// User $userArr to generate the Drop down list				
			if($return == 1){				
				
				$selectedU = in_array('-1', $selectedUserArr)?'selected="selected"':'';
				$select = '<option value="-1" '.$selectedU.' >'.get_string('allusers','multiuserreport').'</option>';
				if(count($userArr) > 0 ){
				  foreach($userArr as $arr){
					$selectedU = in_array($arr->id, explode(",",implode(",",$selectedUserArr)))?"selected='selected'":'';
					$select .= '<option value="'.$arr->id.'" '.$selectedU.'>'.$arr->fullname.'</option>';
				  }
				}else{
				  //$selectedU = in_array(0, $selectedUserArr)?'selected="selected"':'';
				 // $select .= '<option '.$selectedU.' value="0">'.get_string('nouserfound','multiuserreport').'</option>';
				}
				
				if($error == ''){
				 $outcome->success = true;
				 $outcome->response = $select;
				}
				
				return $outcome;
			
		   }elseif($return == 2){
		      if(count($userArr) > 0 ){
		       $userIdArr = array_keys($userArr); 
			  }
		      return $userIdArr;
		   }else{
		     return false;
		   } 
    }
	
	
	
	
	/**
	 * This function is using for getting all courses by department, team
	 * @global object
	 * @param array $departmentArr array of selected deparmtnet id 
	* @param array $teamArr array of selected team id 
	* @param array $programArr array of selected program id 
	* @param array $selectedCourseArr array of selected course id 
	* @param int  $return return type of result
	* @param bool $isReport if we getting result in report section then it will be true or else false
	* @param int $courseTypeId is course type id
	 * @return string the dropdrown html for courses.
	 */


     function getCourseListByDnT($departmentArr, $teamArr, $programArr, $selectedCourseArr, $return = 1, $isReport = false, $courseTypeId = ''){

            global $CFG, $DB, $USER;

			$courseIds = 0;
			$courseIdArr = array();
			$courseIdArr1 = array();
			$courseIdArr2 = array();
			$courseIdArr3 = array();

		    $courseTypeId = $courseTypeId?$courseTypeId:$CFG->courseTypeOnline;
			
			if(count($teamArr) > 0){
			
			   $query = "SELECT DISTINCT courseid FROM {$CFG->prefix}groups_course WHERE 1 = 1 ";
			   if(!$isReport){
			     $query .= " AND is_active = '1'";
			   }
			   
			   $teamIds = 0;
			   if($teamArr[0] == '-1'){
			         /*$tArrRec = getTeams();
					 if(count($tArrRec)>0){
					  $tArr = array_keys($tArrRec);
					 }
					 $teamIds = count($tArr) > 0?implode(",", $tArr):0;*/
			   }else{
					 $teamIds = count($teamArr) > 0?implode(",", $teamArr):0;
			   }
			   
			   $query .= " AND groupid IN ($teamIds) ";
			   $records = $DB->get_records_sql($query);
			   $courseIdArr1 = array_keys($records);
			}
			
			//pr($courseIdArr1);die;
			
			if(count($departmentArr) > 0){
			
				$query = "select DISTINCT courseid FROM {$CFG->prefix}department_course WHERE 1 = 1 ";
				if(!$isReport){
			     $query .= " AND is_active = '1'";
			    }
				$departmentIds = 0;
				if($departmentArr[0] == '-1'){
				
				   /*if( $USER->archetype == $CFG->userTypeManager) { // in the case of manager, we will get the manager department ids and these ids will be used to get courses by department  
					 $deptArrRec = getDepartments('', $isReport);
					 if(count($deptArrRec)>0){
					  $deptArr = array_keys($deptArrRec);
					 }
					 $departmentIds = count($deptArr) > 0 ?implode(",", $deptArr):0;
				   }*/
				   	
				}else{
				    $departmentIds = count($departmentArr) > 0 ?implode(",", $departmentArr):0;
 			    }
				
				$query .= " AND departmentid IN ($departmentIds) ";
				
			    $records = $DB->get_records_sql($query);
			    $courseIdArr2 = array_keys($records);	
			}
			
			//pr($courseIdArr2);die;
			
			if(count($programArr) > 0){
			
				$query = "select DISTINCT courseid FROM {$CFG->prefix}program_course WHERE 1 = 1 ";
				if(!$isReport){
			     $query .= " AND is_active = '1'";
			    }
				$programIds = 0;
				if($programArr[0] == '-1'){
					 /*$progArrRec = getPrograms('','', $isReport);
					 if(count($progArrRec)>0){
					  $progArr = array_keys($progArrRec);
					 }
					 $programIds = count($progArr) > 0 ?implode(",", $progArr):0;
					 */
				}else{
				    $programIds = count($programArr) > 0 ?implode(",", $programArr):0;
 			    }
				
				$query .= " AND programid IN ($programIds) ";
				
			    $records = $DB->get_records_sql($query);
			    $courseIdArr3 = array_keys($records);	
			}
			
			//pr($courseIdArr3);die;
			
			
			
			$fullnameField = extractMDashInSql('mc.fullname');	 
			$query = "select mc.id, $fullnameField  as fullname FROM {$CFG->prefix}course mc WHERE 1 = 1 AND coursetype_id = '".$courseTypeId."'";
			$query .= "  AND mc.deleted = '0' AND mc.publish = '1'";	
			if(!$isReport){
			    // $query .= " AND mc.is_active = '1' AND mc.deleted = '0' ";
				 $query .= " AND mc.is_active = '1' ";	
			}
			if( $USER->archetype == $CFG->userTypeManager) { // in the case of manager, we will get the manager department ids and these ids will be used to get courses by department  
			    $managerCourseIdArr = getManagerCourse($isReport);
				$groupCourses = fetchGroupsCourseIds(0,0,1);
				if(!empty($groupCourses)){
					$managerCourseIdArr = array_merge($managerCourseIdArr,$groupCourses);
				}
				$managerCourseIds = count($managerCourseIdArr) > 0 ? implode(",", $managerCourseIdArr) : 0 ;
				$query .= ' AND mc.id IN ('.$managerCourseIds.') ';
		    }elseif( $USER->archetype == $CFG->userTypeStudent && $USER->is_instructor == 1) { 
			
			// in the case of student instructor, we will get the student instructor department ids and these ids will be used to get classroom courses by department  
			    $studentCourseIdArr = genCourseListingResult();
				$ownerArray = fetchGroupsCourseIds($USER->id,0,1);
				$studentCourseIdArr = count($studentCourseIdArr) > 0 ? array_keys($studentCourseIdArr) : array();
				if(!empty($ownerArray)){
					if(count($studentCourseIdArr)> 0){
						$studentCourseIdArr = array_merge($studentCourseIdArr,$ownerArray);
					}else{
						$studentCourseIdArr = $ownerArray;
					}
				}
				$studentCourseIds = count($studentCourseIdArr) > 0 ? implode(",", $studentCourseIdArr) : 0 ;
				$query .= ' AND mc.id IN ('.$studentCourseIds.') ';
		    }
			
			
			if($programArr[0] != '-1'){ 
			  $courseIdArr = $courseIdArr3;
			}else{
			  
			  if($teamArr[0] != '-1'){
			    $courseIdArr = $courseIdArr1;
			  }else{
			     if($departmentArr[0] != '-1'){
				    $courseIdArr = $courseIdArr2;
				 }
			  }
			}
			
         

			//$courseIdArr = array_merge($courseIdArr1, $courseIdArr2, $courseIdArr3);
			$courseIdArr = array_unique($courseIdArr);	
            //pr($courseIdArr);die;
			$query .= " AND mc.id != 1 ";
			if(count($courseIdArr) > 0){
			  $courseIdArr = array_unique($courseIdArr);
			  $courseIds = implode(",", $courseIdArr);
			  $query .= " AND mc.id in ($courseIds) ";
			  $query .= " AND coursetype_id = '".$courseTypeId."' ";
			  $query .= " ORDER BY mc.fullname ASC";
			  $courseArr = $DB->get_records_sql($query);
			}else{
			  
			  if($departmentArr[0] == '-1' && $teamArr[0] == '-1'){
			      $query .= " ORDER BY mc.fullname ASC";
			      $courseArr = $DB->get_records_sql($query);
			  }
			  		
			}
			//echo  $query;die;
			//pr($courseArr );die;
			if($return == 1){
			
				$selectedC = in_array('-1', $selectedCourseArr)?'selected="selected"':'';
				if($courseTypeId == $CFG->courseTypeClassroom){
				  $select = '<option value="-1" '.$selectedC.' >'.get_string('allcourses','classroomreport').'</option>';
				}else{
				  $select = '<option value="-1" '.$selectedC.' >'.get_string('allcourses','multiuserreport').'</option>';
				}
				if(count($courseArr) > 0 ){
				  foreach($courseArr as $arr){
					$selectedC = in_array($arr->id, explode(",",implode(",",$selectedCourseArr)))?"selected='selected'":'';
					$select .= '<option value="'.$arr->id.'" '.$selectedC.'>'.$arr->fullname.'</option>';
				  }
				}else{
				  //$selectedC = in_array(0, $selectedCourseArr)?'selected="selected"':'';
				  //$select .= '<option '.$selectedC.' value="0">'.get_string('nocoursefound','multiuserreport').'</option>';
				}
				
				if($error == ''){
				 $outcome->success = true;
				 $outcome->response = $select;
				}
				return $outcome;
				
		   }elseif($return == 2){
		      if(count($courseArr) > 0 ){
			    $courseIdArr = array_keys($courseArr);
			  }
		      return $courseIdArr;
		   }elseif($return == 3){
		      return $courseArr;
		   }else{
             return false;		   
		   }	 
			   
    }
	
	/**
	 * This function is using for getting all courses by department, team and classroom courses
	 * @global object
	 * @param array $departmentArr array of selected deparmtnet id 
	* @param array $teamArr array of selected team id 
	* @param array $programArr array of selected program id 
	* @param array $selectedCourseArr array of selected course id 
	* @param bool $isReport if we getting result in report section then it will be true or else false
	 * @return string the dropdrown html for courses.
	 */


     function getCourseNClassroomListByDnT($departmentArr, $teamArr, $programArr, $selectedCourseArr, $isReport = false){

            global $CFG, $DB, $USER;

			$courseIds = 0;
			$courseIdArr = array();
			$courseIdArr1 = array();
			$courseIdArr2 = array();
			$courseIdArr3 = array();

		    $courseTypeId = $CFG->courseTypeOnline;
			$return = 3;
			
			
			$onlineCourseArr = getCourseListByDnT($departmentArr, $teamArr, $programArr, $selectedCourseArr, $return , $isReport ) ;
			$totalClassroomRecords = getClassesOfClassroomDetails();
			//pr($onlineCourseArr);die;
			//pr($totalClassroomRecords);die;
			
			
			$selectedC = in_array('-1', $selectedCourseArr)?'selected="selected"':'';
			$HTML = '<option value="-1" '.$selectedC.' >'.get_string('allcourses','multiuserreport').'</option>';
			
			$onlineCHTML = '<optgroup label="'.get_string('onlinecourse').'">';
			if(count($onlineCourseArr) > 0 ){
			  $select = '';
			  foreach($onlineCourseArr as $arr){
				$selectedC = in_array($arr->id, explode(",",implode(",",$selectedCourseArr)))?"selected='selected'":'';
				$select .= '<option value="'.$arr->id.'" '.$selectedC.'>'.$arr->fullname.'</option>';
			  }
			  $onlineCHTML .= $select;
			}
			$onlineCHTML .= '</optgroup>';
			
			$classroomCHTML = '<optgroup label="'.get_string('classroomcourse').'">';
			if(isset($totalClassroomRecords['classroom']) && count($totalClassroomRecords['classroom']) > 0 ){
			  $select = '';
			  foreach($totalClassroomRecords['classroom'] as $oarr){
				$selectedC = in_array($oarr['course_id'], explode(",",implode(",",$selectedCourseArr)))?"selected='selected'":'';
				$select .= '<option value="'.$oarr['course_id'].'" '.$selectedC.'>'.$oarr['course_name'].'</option>';
			  }
			  $classroomCHTML .= $select;
			}
			$classroomCHTML .= '</optgroup>';
			$HTML .= $onlineCHTML.$classroomCHTML;
			
			if($error == ''){
			 $outcome->success = true;
			 $outcome->response = $HTML;
			}
			
			return $outcome;
				
			   
    }
	
	
	/**
	 * This function is using for getting all programs by department, team
	 * @global object
	 * @param array $departmentArr array of selected deparmtnet id 
	* @param array $teamArr array of selected team id 
	* @param array $selectedProgramArr array of selected program id 
	 * @return string the dropdrown html for programs.
	 */
	
	function getProgramListByDnT($departmentArr, $teamArr, $selectedProgramArr, $isReport = false){

            global $CFG, $DB, $USER;
		
			$programIds = 0;
			$programIdArr = array();
			$programIdArr1 = array();
			$programIdArr2 = array();
			
			if($departmentArr[0] == '-1' && $teamArr[0] == '-1'){
			
				if( $USER->archetype == $CFG->userTypeManager) { // in the case of manager, we will get the manager department ids and these ids will be used to get courses by department  
					 $programArr = getManagerProgram(3, $isReport);
			 
			    }else{
				  
				     $query = "select DISTINCT mp.id, mp.name FROM {$CFG->prefix}programs mp LEFT JOIN {$CFG->prefix}program_department as mpd ON (mp.id = mpd.program_id) WHERE 1 = 1 ";
					 $query .= "  AND mp.publish = '1' AND mp.deleted = '0' ";
					 if(!$isReport){
						$query .= " AND mp.status = '1' AND mpd.is_active = '1'";
					 }
					
					$query .= " ORDER BY mp.name ASC";
			        $programArr = $DB->get_records_sql($query);
				
				}
				 
			}elseif(($departmentArr[0] == '-1' && $teamArr[0] != '-1')  || ($departmentArr[0] != '-1' && $teamArr[0] != '-1') ){
			
			
				$Query = "SELECT DISTINCT(p.id), p.name, pg.program_id, pg.group_id FROM mdl_programs as p LEFT JOIN mdl_program_group as pg ON (p.id = pg.program_id ) WHERE  1 = 1 ";
		        $Query .= " AND p.publish = '1' AND p.deleted = '0' ";
				if(!$isReport){
				  $Query .= "AND pg.is_active = 1 AND p.status = 1 ";
				}
				
				$groupIds = count($teamArr) > 0 ?implode(",", $teamArr):0;
				
				$Query .= " AND pg.group_id IN ( ".$groupIds." ) ORDER BY p.name ASC";
				$programArr = $DB->get_records_sql($Query);
			
			
			}elseif($departmentArr[0] != '-1' && $teamArr[0] == '-1'){
			
			   $query = "select DISTINCT mp.id, mp.name FROM {$CFG->prefix}programs mp LEFT JOIN {$CFG->prefix}program_department as mpd ON (mp.id = mpd.program_id) WHERE 1 = 1 ";
			    $query .= " AND mp.publish = '1' AND mp.deleted = '0' ";
				if(!$isReport){
			     $query .= " AND mp.status = '1' AND mpd.is_active = '1'";
			   }
			   
			   $departmentIds = count($departmentArr) > 0 ?implode(",", $departmentArr):0;
			   $query .= " AND mpd.department_id IN (".$departmentIds.")";
			   $query .= " ORDER BY mp.name ASC";
			   $programArr = $DB->get_records_sql($query);
			}
			
			
	        $selectedP = in_array('-1', $selectedProgramArr)?'selected="selected"':'';
			$select = '<option value="-1" '.$selectedP.' >'.get_string('allprograms','multiuserreport').'</option>';
			if(count($programArr) > 0 ){
			  foreach($programArr as $arr){
			    $selectedP = in_array($arr->id, explode(",",implode(",",$selectedProgramArr)))?"selected='selected'":'';
				$select .= '<option value="'.$arr->id.'" '.$selectedP.'>'.$arr->name.'</option>';
			  }
			}else{
			  //$selectedP = in_array(0, $selectedProgramArr)?'selected="selected"':'';
			  //$select .= '<option '.$selectedP.' value="0">'.get_string('noprogramfound','multiuserreport').'</option>';
			}
			
			if($error == ''){
			 $outcome->success = true;
			 $outcome->response = $select;
			}
			   
	
		
	   return $outcome;
	   

	
    }
	
	

     /*
	 * Below method is used to download any file in csv.
	 * You just only pass the file location with filename.
	 */
	function exportCSV($filepath, $remove=true){ 
		 				
		//ini_set('display_errors', 1);
  	    //error_reporting(E_ALL);
			
		if (file_exists($filepath)){
		
			header('Content-Description: File Transfer');
			header('Content-Type:text/csv');
			header('Content-Disposition: attachment; filename='.basename($filepath));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . filesize($filepath));
			ob_clean();
			flush();
			readfile($filepath);
			if($remove){
				unlink($filepath);
			}
			exit;
		}
	}
	
	/**
	 * get team name or all results
	 *
	 * @param mixed $teamId is team id
	  * @return string $result team name or all records (if $teamId is null)
	 */
	 
	 
	function getTeams($teamId=''){
	
		global $DB, $CFG, $USER;
		$result = '';
		/*$extraCond = '';
		if( $USER->archetype == $CFG->userTypeManager) {
			$extraCond =  " AND createdby = '".$USER->id."'";
		}*/

		if(is_array($teamId)){
		  $teamIds = count($teamId) > 0?implode(",",$teamId):0;
		  if($teamIds){
		    $result = $DB->get_field_sql("select GROUP_CONCAT(name ORDER BY name ASC SEPARATOR ', ') FROM {$CFG->prefix}groups WHERE id IN (".$teamIds.")");
		  }
		}else{
			if($teamId){
			   $result = $DB->get_field_sql("select name FROM {$CFG->prefix}groups WHERE id IN (".$teamId.") ORDER BY name ASC ");
			}else{
			        if( $USER->archetype == $CFG->userTypeManager || $USER->archetype == $CFG->userTypeStudent) {
						$department = $USER->department;
						$result = getDepartmentGroups($department);
					}elseif( $USER->archetype == $CFG->userTypeAdmin) {
			            $result = $DB->get_records_sql("select * FROM {$CFG->prefix}groups WHERE  1 = 1 ORDER BY name ASC ");
					}

			}
        }
			
		
		return $result;
	}
	/**
	 * get team name or all results
	 *
	 * @param mixed $teamId is team id
	  * @return string $result team name or all records (if $teamId is null)
	 */
	 
	 
	function getUserGroup($teamId=''){
	
		global $DB, $CFG, $USER;
		$result = '';
		/*$extraCond = '';
		if( $USER->archetype == $CFG->userTypeManager) {
			$extraCond =  " AND createdby = '".$USER->id."'";
		}*/

		if(is_array($teamId)){
		  $teamIds = count($teamId) > 0?implode(",",$teamId):0;
		  if($teamIds){
		    $result = $DB->get_field_sql("select GROUP_CONCAT(name ORDER BY name ASC SEPARATOR ', ') FROM {$CFG->prefix}groups WHERE id IN (".$teamIds.")");
		  }
		}else{
			if($teamId){
			   $result = $DB->get_field_sql("select name FROM {$CFG->prefix}groups WHERE id IN (".$teamId.") ORDER BY name ASC ");
			}else{
			        if( $USER->archetype == $CFG->userTypeManager || $USER->archetype == $CFG->userTypeStudent) {
						$department = $USER->department;
						$result = getDepartmentGroups($department);
					}elseif( $USER->archetype == $CFG->userTypeAdmin) {
			            $result = $DB->get_records_sql("select * FROM {$CFG->prefix}groups WHERE  1 = 1 ORDER BY name ASC ");
					}

			}
        }
		return $result;
	}
	
		/**
	 * get department name or all results
	 *
	 * @param mixed $deptId is department id
	 * @return string $result department name or all records (if $deptId is null)
	 */
	 
	 
	function getDepartments($deptId='', $isReport = false){
	
		global $DB, $CFG, $USER;
		$result = '';
		/*$extraCond = '';
		if( $USER->archetype == $CFG->userTypeManager) {
			$query = "SELECT GROUP_CONCAT(departmentid) as departmentids FROM {$CFG->prefix}department_members WHERE userid = '".$USER->id."' ";
			if(!$isReport){
			  $query .= "  AND is_active = '1'";
			}
			$managerDepartments = $DB->get_field_sql($query);
			if($managerDepartments){
			  $extraCond =  " AND id IN (".$managerDepartments.")";
			}
		}elseif( $USER->archetype == $CFG->userTypeStudent) {
			$extraCond =  " AND id IN (".$USER->department.")";
		}*/
		
		$extDeptIdentifier = get_string('external_department_identifier', 'department');
		
		if(is_array($deptId)){
		  $deptIds = count($deptId) > 0 ? implode(",",$deptId) : 0;
		  if($deptIds){
		    
			if($CFG->allowExternalDepartment == 1){
			  $query = "select GROUP_CONCAT( (if(is_external = 1, concat('".$extDeptIdentifier."',title), title)) ORDER BY title ASC SEPARATOR ', ') FROM {$CFG->prefix}department WHERE id IN (".$deptIds.")  AND deleted='0' ";
			}else{
			
			   $where = externalCheckForDepartment();
			   $query = "select GROUP_CONCAT( (if(is_external = 1, concat('".$extDeptIdentifier."',title), title)) ORDER BY title ASC SEPARATOR ', ') FROM {$CFG->prefix}department WHERE id IN (".$deptIds.")  AND deleted='0' $where  ";
			  
			}
			
			if(!$isReport){
			  $query .= " AND status = '1' ";
			}
			
		    $result = $DB->get_field_sql($query);
		  }
		}else{
		
			if($deptId){ 
			    $query = "select if(is_external = 1, concat('".$extDeptIdentifier."',title), title) as title FROM {$CFG->prefix}department  WHERE id IN (".$deptId.")  AND deleted='0'  ";
				if(!$isReport){
				  $query .= "  AND status = '1'";
				}
				$query .= "  ORDER BY title ASC ";
			    $result = $DB->get_field_sql($query);
			}else{
			  
			    $where = externalCheckForDepartment();
				
				if($USER->archetype == $CFG->userTypeManager){
					$query = "SELECT d.id, if(d.is_external = 1, concat('".$extDeptIdentifier."',d.title), d.title) as title, d.createdby, d.description, d.is_external, d.status, d.deleted, d.picture, d.timecreated, d.timemodified, d.display_order FROM {$CFG->prefix}department_members as dm LEFT JOIN {$CFG->prefix}department as d on d.id = dm.departmentid WHERE userid = ".$USER->id." AND d.deleted = 0 $where ";

				}elseif( $USER->archetype == $CFG->userTypeStudent) {
				
				 $query = "SELECT d.id, if(d.is_external = 1, concat('".$extDeptIdentifier."',d.title), d.title) as title, d.createdby, d.description, d.is_external, d.status, d.deleted, d.picture, d.timecreated, d.timemodified, d.display_order FROM {$CFG->prefix}department_members as dm LEFT JOIN {$CFG->prefix}department as d on d.id = dm.departmentid WHERE userid = ".$USER->id." AND d.deleted = 0 $where ";
				 
				}else{
					$query = "SELECT d.id, if(d.is_external = 1, concat('".$extDeptIdentifier."',d.title), d.title) as title, d.createdby, d.description, d.is_external, d.status, d.deleted, d.picture, d.timecreated, d.timemodified, d.display_order FROM {$CFG->prefix}department as d WHERE d.deleted = 0 $where ";
					
				}

			    //$query = "select id, if(is_external = 1, concat('".$extDeptIdentifier."',title), title) as title, createdby, description, is_external, status, deleted, picture, timecreated, timemodified, display_order FROM {$CFG->prefix}department  WHERE 1 = 1  AND deleted='0'  $extraCond $where ";
				if(!$isReport){
				  $query .= "  AND d.status = '1' ";
				}
				
				$query .= "  order by d.title ASC ";
			    $result = $DB->get_records_sql($query);
			}
        }
		//pr($result);	die;	
		return $result;
	}
	
		/**
	 *  To check, if department is exist for the same name
	 *  @param string $title is title of the department
	 *  @return bool true, if exist or return false
	*/
	
	function isDepartmentAlreadyExist($title, $id = 0) {
	
		global $DB;
		$where  = '';
		if($id != 0){
			$where  = ' AND id != '.$id;
		}
		$query = 'SELECT id FROM mdl_department d WHERE d.deleted = 0 AND LOWER(d.title) = "'.addslashes(strtolower(trim($title))).'"'.$where;
		$data = $DB->get_record_sql($query);

		if(!empty($data)){
			return $data->id;
		}
		else{
			return false;
		}
	}
	
	function isUserAlreadyExist($username, $id = 0) {
	
		global $DB;
		$where  = '';
		if($id != 0){
			$where  = ' AND id != '.$id;
		}
		$query = 'SELECT id FROM mdl_user as u WHERE u.deleted != 1 AND LOWER(u.username) = "'.addslashes(strtolower(trim($username))).'" '.$where;
		$data = $DB->get_record_sql($query);

		if(!empty($data)){
			return $data->id;
		}
		else{
			return false;
		}
	}
	/**
	 * get program name or all results
	 *
	 * @param mixed $pgId is program id
	 * @return string $result program name or all records (if $pgId is null)
	 */	 
	 
	function getPrograms($pgId='', $isReport=false, $return = 2, $checkCond = 1){
	
		global $DB, $CFG, $USER;
		$result = '';
		

		if(is_array($pgId)){
		  $pgIds = count($pgId) > 0 ? implode(",",$pgId) : 0;
		  if($pgIds){
		   
			$query = "select GROUP_CONCAT(name ORDER BY name ASC SEPARATOR ', ') FROM {$CFG->prefix}programs WHERE id IN (".$pgIds.") ";
			
			if($checkCond == 1){
				$query .= " AND publish = '1' AND deleted = '0'";
				if(!$isReport){
				 $query .= " AND status = '1' ";
				}
			}
		    $result = $DB->get_field_sql($query);
		  }
		}else{
			if($pgId){  
			   $query = "select name FROM {$CFG->prefix}programs  WHERE id IN (".$pgId.") ";
			   if($checkCond == 1){
				   $query .= " AND publish = '1' AND deleted = '0'";
				   if(!$isReport){
					 $query .= " AND status = '1' ";
				   }
			   }
			   $result = $DB->get_field_sql($query);
			}else{
			
			        $query = "select * FROM {$CFG->prefix}programs  WHERE 1 = 1 ";
					
					if($checkCond == 1){
						$query .= " AND publish = '1' AND deleted = '0'";
						if(!$isReport){
								 $query .= " AND status = '1' ";
						}
					}
					
					
				    if( $USER->archetype == $CFG->userTypeManager) {
						$result = getManagerProgram($return, $isReport); 
					}elseif( $USER->archetype == $CFG->userTypeStudent) {
						$learnerProgramsArr = getAssignedProgramIdForUser($USER->id);
						$learnerPrograms = count($learnerProgramsArr) > 0 ? implode(",", $learnerProgramsArr) : 0;
						if($learnerPrograms){
						  $extraCond .=  " AND id IN (".$learnerPrograms.")";
						}
						
						$query .= $extraCond;
						$query .= "  ORDER BY name ASC ";
						$result = $DB->get_records_sql($query);
						
					}else{
						$query .= "  ORDER BY name ASC ";
						$result = $DB->get_records_sql($query);
					} 

			       
			}
        }
				
		return $result;
	}
	
	
	/**
	 * get user full name or all results
	 *
	 * @param mixed $userId is user id
	 * @return string $result user full name or all records (if $userId is null)
	 */
	 
	 
	function getUsers($userId, $needUserName=0){
	
		global $DB, $CFG;
		$result = '';

		if(is_array($userId)){
		  $userIds = count($userId) > 0 ? implode(",",$userId) : 0;
		  if($userIds){
		    $result = $DB->get_field_sql("select GROUP_CONCAT(concat(firstname,' ',lastname)  ORDER BY firstname ASC  SEPARATOR ', ') FROM {$CFG->prefix}user WHERE id IN (".$userIds.") AND deleted = '0'");
		  }
		}else{
			if($userId){
			    if($needUserName){
			      $result = $DB->get_field_sql("select concat(firstname,' ',lastname, ' (', username, ')') FROM {$CFG->prefix}user  WHERE id IN (".$userId.") AND deleted = '0' ORDER BY firstname ASC ");
			   }else{
			      $result = $DB->get_field_sql("select concat(firstname,' ',lastname) FROM {$CFG->prefix}user  WHERE id IN (".$userId.") AND deleted = '0' ORDER BY firstname ASC ");
			   }
			}else{
			   $result = $DB->get_records_sql("select * FROM {$CFG->prefix}user  WHERE deleted = '0' ORDER BY firstname ASC ");
			}
        }
			
		
		return $result;
	}
	
	
	function exportPDF($file='', $html='', $html1='', $pdfTitle='') {
	   // ini_set('display_errors', 1);
  	   // error_reporting(E_ALL);
		//global $USER, $DB, $CFG, $SITE, $OUTPUT, $PAGE;
		global $CFG,$SITE;
	
	    ob_start();

		
		require_once($CFG->dirroot .'/local/lib/tcpdf/config/tcpdf_config.php');
		require_once($CFG->dirroot .'/local/lib/tcpdf/config/lang/eng.php');

		require_once($CFG->dirroot .'/local/lib/pdflib.php');
        $pdf = new pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor($SITE->fullname);
       // $pdf->SetTitle(get_string('courseusagesreport','multicoursereport'));
        //$pdf->SetSubject(get_string('courseusagesreport','multicoursereport'));
      
		
		$copyright = get_string('copyright','multicoursereport').' <a href="'.$CFG->wwwroot.'" target="_blank">'.$SITE->fullname.'</a> '.get_string('allrightsreserved','multicoursereport');
		$pdfHeaderTitle = get_string('courseusagesreport','multicoursereport');
		if(!empty($pdfTitle)) {
			$pdf->SetTitle($pdfTitle);
			$pdf->SetSubject($pdfTitle);
			$pdfHeaderTitle = $pdfTitle;
		}
		
		$pdfCreatedDate = date($CFG->customDefaultDateTimeFormatForPrint);
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $pdfHeaderTitle, $pdfCreatedDate, array(0, 0, 0), array(0, 0, 0));
		// set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        // remove default header/footer
        $pdf->setPrintHeader(true);
        $pdf->setPrintFooter(true);
        $pdf->SetDisplayMode('real', 'SinglePage', 'UseNone');
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetMargins('10', '30', '10', true);
        //set margins
        $pdf->SetHeaderMargin('5');
        $pdf->SetFooterMargin(15);
        //set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        //set some language-dependent strings
        $pdf->setLanguageArray($l);
		$pdf->SetFont('helvetica', '', 10, '', 'false');
        $pdf->setFontSubsetting(false);
		$pdf->AddPage();
  	    $pdf->writeHTML($html, false, false, true, false, '');
	
        if($html1){
		
			$newhtml = explode('~~~', $html1, 3);
		
			if(sizeof($newhtml) > 0) {
				$i=0;
				foreach($newhtml AS $key => $val) {
					$val = str_replace('~~~', '', $val);
					if(!empty($val)) {
						//if($i%2==0)
						if($key==0)
							$pdf->AddPage();
						$pdf->writeHTML('<p>&nbsp;</p>'.$val, false, false, true, false, '');
					}
					$i++;
				}
			}
		}
       
		//$pdf->Output($file, 'I');
		$pdf->Output($file, 'D');
		die();
		//pdf generator ends
	}
	
	
	
	 /**
	 * This function is using for getting a Course Usages Report
	 * @global object
	 * @param array $paramArray is search array
	 * @param array $removeKeyArray contain search key that you don't want to add in address bar
	 * @param string $sort is sorting field
	 * @param string $dir is sorting order
	 * @param int $page current page
	 * @param int $perpage is number of records per page
	 * @param string $export is action variable for PDF download, CSV download, and Print Course Usages Report
	 * @return array $courseUsagesReport return CSV , PDF and Report Content
	 */
	
	
	function getCourseUsagesReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export=''){

	       //ini_set('display_errors', 1);
  	       // error_reporting(E_ALL);
		
	        global $DB, $CFG, $USER;
			$courseUsagesReport = new stdClass();
			$isReport = true;
			
			$offset = $page - 1;
			$offset = $offset*$perpage;
			$limit = '';
			if($perpage != 0){
				$limit = "LIMIT $offset, $perpage";
			}

			////// Getting common URL for the Search //////
			$pageURL = $_SERVER['PHP_SELF'];
			$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
			
			$printUrl = '/course/multicoursereportprint.php';
			$genPrintURL = genParameterizedURL($paramArray, $removeKeyArray, $printUrl);				  
	  
	        if($USER->archetype == $CFG->userTypeAdmin ){
	          $sDepartment    = $paramArray['department'];
			}else{
			  $sDepartment    = $USER->department;
			}
			$sTeam          = $paramArray['team'];
			$sCourse        = $paramArray['course'];
			$sProgram       = $paramArray['program'];
			$sType          = $paramArray['type'];
			$sStartDate     = $paramArray['startDate'];
			$sEndDate       = $paramArray['endDate'];
			
			$userGroup       = $paramArray['user_group'];
			/*$sDateSelected = '';
			if($sStartDate){
				list($month1, $day1, $year1) = explode('-', $sStartDate);
				$sDateSelected = date("m/d/Y",mktime(0, 0, 0, $month1, $day1, $year1));
			}		   
			
			$eDateSelected = '';
			if($sEndDate){
				list($month2, $day2, $year2) = explode('-', $sEndDate);
				$eDateSelected = date("m/d/Y",mktime(0, 0, 0, $month2, $day2, $year2));
			}*/
			
			$sDateSelected = '';
			if($sStartDate){
				$sDateArr = getFormattedTimeStampOfDate($sStartDate);
			    $sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

			}		   
			
			$eDateSelected = '';
			if($sEndDate){
			   $eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
			   $eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
			}
			

			$sDepartmentArr = explode("@",$sDepartment);
			$sTeamArr = explode("@",$sTeam);
			$sCourseArr = explode("@",$sCourse);
			$sProgramArr = explode("@",$sProgram);
			$sTypeArr = explode("@",$sType);
			
			$userGroupArr = explode("@",$userGroup);
			
			if($paramArray['sel_mode'] == 2){
				$sTeamArr = $userGroupArr;
			}
		
			$sDepartmentName = $sDepartment=='-1'?(get_string('all','multicoursereport')):getDepartments($sDepartmentArr, $isReport);
			if($USER->archetype != $CFG->userTypeAdmin ){
				$sDepartmentName = getDepartments($USER->department, $isReport);
			}
			$sTeamName = $sTeam=='-1'?(get_string('all','multicoursereport')):getTeams($sTeamArr);
			$sCourseName = $sCourse=='-1'?(get_string('all','multicoursereport')):getCourses($sCourseArr);
			$sProgramName = $sProgram=='-1'?(get_string('all','multicoursereport')):getPrograms($sProgramArr);
			
			$sUserGroupName = $userGroup=='-1'?(get_string('all','multiuserreport')):getUserGroup($userGroupArr);
			
			$searchStringLimit = '';
			if($sType == '-1'){
			  $sTypeName = get_string('all','multicoursereport');
			}else{
			
			  $repTypeArr = $CFG->courseStatusArr;
			  $sTypeArr2 = str_replace(array(3, 1, 2), $repTypeArr, $sTypeArr); 
			  $sTypeName = implode(", ", $sTypeArr2);
			}
			
			// search condition start here
			
			$searchString = "";
			if(count($sCourseArr) > 0 &&  !in_array($sCourseArr[0] , array(0, '-1'))){
				$sCourseArr = implode(",", $sCourseArr);
				$searchString .= " AND mc.id IN ($sCourseArr) ";
				
				if($_REQUEST["sel_mode"] == 2){
					$userIdArr = getOpenTeamsUser($sTeamArr);
				}else{
					$userIdArr = getUserListByDnT($sDepartmentArr, array('-1'), '', 2, $isReport, 0);
				}
				$sUserStr = isset($userIdArr) && count($userIdArr) > 0?implode(",", $userIdArr):0;
				$searchString .= " AND fecdu.enrolled_user IN ($sUserStr) ";
				
			}else{
				if($_REQUEST["sel_mode"] == 2){
					$courseIdArr = getOpenTeamsCourse($sTeamArr);
				}else{
					$courseIdArr = getCourseListByDnT($sDepartmentArr, $sTeamArr, $sProgramArr, $sCourseArr, 2, $isReport);
				}
				$sCourseArr = count($courseIdArr) > 0 ? implode(",", $courseIdArr) : 0;
				$searchString .= " AND mc.id IN ($sCourseArr) ";
			
				
				
				if($USER->archetype != $CFG->userTypeAdmin ){
					if($_REQUEST["sel_mode"] == 2){
						$userIdArr = getOpenTeamsUser($sTeamArr);
					}else{
						$userIdArr = getUserListByDnT($sDepartmentArr, array('-1'), '', 2, $isReport, 0);
					}
					
					$sUserStr = isset($userIdArr) && count($userIdArr) > 0?implode(",", $userIdArr):0;
					$searchString .= " AND fecdu.enrolled_user IN ($sUserStr) ";
				}
				
			}
			
		
			
			if($sDateSelected && $eDateSelected){ 

				
				$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
                $sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
			   
				$searchString .= " AND ((fecdu.f_min_activity_lastaccessed >= $sStartDateTime && fecdu.f_min_activity_lastaccessed <= $sEndDateTime ) || (fecdu.f_max_activity_lastaccessed >= $sStartDateTime && fecdu.f_max_activity_lastaccessed <= $sEndDateTime ) )";

			}elseif($sDateSelected && $eDateSelected == ''){ 
			
				$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
			    $searchString .= " AND (fecdu.f_min_activity_lastaccessed >= ($sStartDateTime) || fecdu.f_max_activity_lastaccessed >= ($sStartDateTime) )";
				
			}elseif($sDateSelected =='' && $eDateSelected){ 
			  
			   $sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
			   $searchString .= " AND (fecdu.f_min_activity_lastaccessed <= ($sEndDateTime) || fecdu.f_max_activity_lastaccessed <= ($sEndDateTime) )";
			}
			
			
			// search condition end here
			
			$recordArr = getUserAndCourseReports($searchString, $page, $perpage, $sTypeArr);
			$reportArr = $recordArr['reportArr']['courses'];
			$limitReportArr = $recordArr['limitReportArr'];
			$limitCount = $recordArr['limitCount'];

			$courseHTML = '';
			$style = !empty($export)?"style=''":'';
			$courseHTML .= "<div class='tabsOuter borderBlockSpace' ".$style.">";
			$courseHTML .= '<div class="clear"></div>';
            $exportHTML = '';
            if(empty($export)){
			
				ob_start();
				require_once($CFG->dirroot . '/local/includes/multicoursereportsearch.php');
				$SEARCHHTML = ob_get_contents();
				ob_end_clean();
				
				$courseHTML .= $SEARCHHTML;

				$exportHTML .= '<div class="exports_opt_box"> ';
				$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','multicoursereport').'" href="'.$genPrintURL.'&action=exportcsv">'.get_string('downloadcsv','multicoursereport').'</a> ';
				$exportHTML .= '<span class="seperater">&nbsp;</span>';
				$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','multicoursereport').'" id="exportpdf1" href="'.$genPrintURL.'&action=exportpdf">'.get_string('downloadpdf','multicoursereport').'</a> ';
				$exportHTML .= '<span class="seperater">&nbsp;</span>';
				$exportHTML .= '<a rel="'.$genPrintURL.'&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','multicoursereport').'" class="print_icon">'.get_string('print','multicoursereport').'</a>';
				$exportHTML .= '</div>';
			
			}
			
			$courseHTML .= '<div class="userprofile view_assests">';
			
			$userCourseStatusArr = array();
			$totalAssignedCoursesAll = 0;
			$inProgressCoursesAll = 0;
			$completedCoursesAll = 0;
			$notStartedCoursesAll = 0;
			
			$inProgressCoursesAllExploded = false;
			$completedCoursesAllExploded = false;
			$notStartedCoursesAllExploded = false;
	
		
			$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
			$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
			$sDateSelectedForPrintCSV = getDateFormatForPrint($sDateSelected, 'csv');
			$eDateSelectedForPrintCSV = getDateFormatForPrint($eDateSelected, 'csv');
				
				
			if(!empty($export) && $export == 'print'){
				
					$reportName = get_string('courseusagesreport','multicoursereport');
					$courseHTML .= getReportPrintHeader($reportName);

					$courseHTML .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="generaltable margin_bottom">';
						$courseHTML .= '<tr>';
							if($paramArray['sel_mode'] == 2){
								$courseHTML .= '<td colspan = "2" ><strong>'.get_string('global_team').':</strong> '.$sUserGroupName.'</td>';
							}else{
								$courseHTML .= '<td><strong>'.get_string('department','multicoursereport').':</strong> '.$sDepartmentName.'</td>';
								$courseHTML .= '<td><strong>'.get_string('team','multicoursereport').':</strong>  '.$sTeamName.'</td>';
							}
						$courseHTML .= '</tr>';
						$courseHTML .= '<tr>';
							$courseHTML .= '<td width="500"><strong>'.get_string('program','multicoursereport').':</strong> '.$sProgramName.'</td>';
							$courseHTML .= '<td width="500"><strong>'.get_string('course','multicoursereport').':</strong> '.$sCourseName.'</td>';
						$courseHTML .= '</tr>';
						$courseHTML .= '<tr>';
							$courseHTML .= '<td colspan="2"><strong>'.get_string('statusreport').':</strong> '.$sTypeName.'</td>';
						$courseHTML .= '</tr>';
						$courseHTML .= '<tr>';
							$courseHTML .= '<td width="50%" ><strong>'.get_string('lastaccesseddatefrom').':</strong> '.$sDateSelectedForPrint.'</td>';
							$courseHTML .= '<td width="50%" ><strong>'.get_string('lastaccesseddateto').':</strong>  '.$eDateSelectedForPrint.'</td>';				
						$courseHTML .= '</tr>';
					$courseHTML .= '</table>';
					//$courseHTML .= '<span '.$CFG->pdfSpanAttribute.'>&nbsp;<br /><strong>'.get_string('courseusagesreport','multicoursereport').'</strong></span><br /><br />';
			}
			
			
			
			if(count($reportArr) > 0 ){
			
				
				 foreach($reportArr as $data){
				 
				   $ip_count  = isset($data->IP_COUNT)?$data->IP_COUNT:0;
				   $cl_count  = isset($data->CL_COUNT)?$data->CL_COUNT:0;
				   $ns_count  = isset($data->NS_COUNT)?$data->NS_COUNT:0;
				   
				   $inProgressCoursesAll += $ip_count;
				   $completedCoursesAll += $cl_count;
				   $notStartedCoursesAll += $ns_count;
				
				}
				
				$totalAssignedCoursesAll = $inProgressCoursesAll + $completedCoursesAll + $notStartedCoursesAll;
				
				$completedPercentage = numberFormat(($completedCoursesAll*100)/$totalAssignedCoursesAll);
				$inProgressPercentage = numberFormat(($inProgressCoursesAll*100)/$totalAssignedCoursesAll);
				$notStartedPercentage = numberFormat(($notStartedCoursesAll*100)/$totalAssignedCoursesAll);
				
				if(in_array(3, $sTypeArr)){
				   $notStartedCoursesAllExploded = true;
				}
				
				if(in_array(1, $sTypeArr)){
				   $inProgressCoursesAllExploded = true;
				}
				
				if(in_array(2, $sTypeArr)){
				   $completedCoursesAllExploded = true;
				}	
				
				if($totalAssignedCoursesAll){
	
					 $courseHTML .= '<div class = "single-report-start" id="watch">
											<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('courseusagesreport','multicoursereport').$exportHTML.'</span>
											  <div class="single-report-graph-left" id="chartContainer">'.get_string('loading','multicoursereport').'</div>
											  <div class="single-report-graph-right">
												<div class="course-count-heading">'.get_string('numberofusers','multicoursereport').'</div>
												<div class="course-count">'.$totalAssignedCoursesAll.'</div>
												<div class="seperator">&nbsp;</div>
												<div class="course-status">
												  <div class="notstarted"><h6>'.get_string('notstarted','multicoursereport').'</h6><span>'.$notStartedCoursesAll.'</span></div>
												  <div class="clear"></div>
												  <div class="inprogress"><h6>'.get_string('inprogress','multicoursereport').'</h6><span>'.$inProgressCoursesAll.'</span></div>
												  <div class="clear"></div>
												  <div class="completed"><h6>'.get_string('completed','multicoursereport').'</h6><span>'.$completedCoursesAll.'</span></div>
	
												</div>
											  </div>
											</div>
										  </div>
										  <div class="clear"></div>';
									  
				}					  
				  
				  				  
			} 
			
			
			
			$courseHTML .= '<div class=""><div class=""><table cellspacing="0" cellpadding="0" border="0" width="100%" class="generaltable"><tbody>'; 
			
			$courseHTML .= '<tr>';
			$courseHTML .=  '<th width="30%">'.get_string('coursetitle','multicoursereport').'</th>';
			$courseHTML .=  '<!--th width="15%">'.get_string('category','multicoursereport').'</th-->';
		
			$courseHTML .=  '<th width="15%">'.get_string('enrolled','multicoursereport').'</th>';
			$courseHTML .=  '<th width="15%">'.get_string('notstarted','multicoursereport').'</th>';
			$courseHTML .=  '<th width="15%">'.get_string('inprogress','multicoursereport').'</th>';
			$courseHTML .=  '<th width="15%">'.get_string('completed','multicoursereport').'</th>';
			$courseHTML .=  '<!--th width="14%">'.get_string('viewdetails','multicoursereport').'</th-->';
			$courseHTML .=  '</tr>';
			
			$reportContentCSV = '';
			
			if($paramArray['sel_mode'] == 2){
				$reportContentCSV .= get_string('global_team').",".$sUserGroupName."\n";
			}else{
				$reportContentCSV .= get_string('department','multicoursereport').",".$sDepartmentName."\n".get_string('team','multicoursereport').",".$sTeamName ."\n";
			}
			
			$reportContentCSV .= get_string('lastaccesseddatefrom').",".$sDateSelectedForPrintCSV."\n".get_string('lastaccesseddateto').",".$eDateSelectedForPrintCSV."\n";							  
			$reportContentCSV .= get_string('program','multicoursereport').",".$sProgramName."\n".get_string('course','multicoursereport').",".$sCourseName ."\n";
			$reportContentCSV .= get_string('statusreport').",".$sTypeName ."\n";
			$reportContentCSV .= get_string('numberofusers','multicoursereport').",".$totalAssignedCoursesAll."\n";
			$reportContentCSV .= get_string('notstarted','multicoursereport').",".$notStartedCoursesAll." (".floatval($notStartedPercentage)."%)\n";
			$reportContentCSV .= get_string('inprogress','multicoursereport').",".$inProgressCoursesAll." (".floatval($inProgressPercentage)."%)\n";
			$reportContentCSV .= get_string('completed','multicoursereport').",".$completedCoursesAll." (".floatval($completedPercentage)."%)\n";
			
			$reportContentCSV .= get_string('courseusagesreport','multicoursereport')."\n";
		    $reportContentCSV .= get_string('coursetitle','multicoursereport').",".get_string('enrolled','multicoursereport').",".get_string('notstarted','multicoursereport').",".get_string('inprogress','multicoursereport').",".get_string('completed','multicoursereport')."\n";
			
		
			
			
			
			$reportContentPDF = '';
			$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="generaltable">';
				$reportContentPDF .= '<tr>';
					if($paramArray['sel_mode'] == 2){
						$reportContentPDF .= '<td colspan = "2" ><strong>'.get_string('global_team').':</strong> '.$sUserGroupName.'</td>';
					}else{
						$reportContentPDF .= '<td><strong>'.get_string('department','multicoursereport').':</strong> '.$sDepartmentName.'</td>';
						$reportContentPDF .= '<td><strong>'.get_string('team','multicoursereport').':</strong>  '.$sTeamName.'</td>';
					}
				$reportContentPDF .= '</tr>';
				$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td><strong>'.get_string('program','multicoursereport').':</strong> '.$sProgramName.'</td>';
					$reportContentPDF .= '<td><strong>'.get_string('course','multicoursereport').':</strong> '.$sCourseName.'</td>';
				$reportContentPDF .= '</tr>';
				$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td colspan="2"><strong>'.get_string('statusreport').':</strong> '.$sTypeName.'</td>';
				$reportContentPDF .= '</tr>';
				$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td><strong>'.get_string('lastaccesseddatefrom').':</strong> '.$sDateSelectedForPrint.'</td>';
					$reportContentPDF .= '<td><strong>'.get_string('lastaccesseddateto').':</strong>  '.$eDateSelectedForPrint.'</td>';				
				$reportContentPDF .= '</tr>';
				
				$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td><strong>'.get_string('numberofusers','multicoursereport').':</strong> '.$totalAssignedCoursesAll.'</td>';
					$reportContentPDF .= '<td><strong>'.get_string('notstarted','multicoursereport').':</strong>  '.$notStartedCoursesAll.' ('.floatval($notStartedPercentage).'%)</td>';				
				$reportContentPDF .= '</tr>';
	
				$reportContentPDF .= '<tr>';
				    $reportContentPDF .= '<td><strong>'.get_string('inprogress','multicoursereport').':</strong>  '.$inProgressCoursesAll.' ('.floatval($inProgressPercentage).'%)</td>';				$reportContentPDF .= '<td><strong>'.get_string('completed','multicoursereport').':</strong>  '.$completedCoursesAll.' ('.floatval($completedPercentage).'%)</td>';							
				$reportContentPDF .= '</tr>';
					
				
			   // $reportContentPDF .= '<tr><br /><td width="100%"  ><span '.$CFG->pdfSpanAttribute.'><strong>'.get_string('courseusagesreport','multicoursereport').'</strong></span><br /></td></tr>';
				 
			 $reportContentPDF .= '</table>';
			 
			 $reportContentPDF .= getGraphImageHTML(get_string('courseusagesreport','multicoursereport'));
			 
			
			 $reportContentPDF .= '<table '.$CFG->pdfTableStyle.'>';
			 $reportContentPDF .= '<tr '.$CFG->pdfTableHeaderStyle.'>';
			 $reportContentPDF .= '<td ><strong>'.get_string('coursetitle','multicoursereport').'</strong></td>';
			 $reportContentPDF .= '<td ><strong> '.get_string('enrolled','multicoursereport').'</strong></td>';
			 $reportContentPDF .= '<td ><strong>'.get_string('notstarted','multicoursereport').'</strong></td>';
			 $reportContentPDF .= '<td ><strong>'.get_string('inprogress','multicoursereport').'</strong></td>';
			 $reportContentPDF .= '<td ><strong>'.get_string('completed','multicoursereport').'</strong></td>';
			 $reportContentPDF .= '</tr>';

			 

			if($limitCount> 0 ){
			
				
			  $i = 0;
			  foreach($limitReportArr as $courses){
									 
					$courseId = $courses->courseid;
					$courseName = $courses->fullname;
					$in_progress_count  = isset($courses->IP_COUNT)?$courses->IP_COUNT:0;
				    $completed_count  = isset($courses->CL_COUNT)?$courses->CL_COUNT:0;
				    $not_started_count  = isset($courses->NS_COUNT)?$courses->NS_COUNT:0;
				    $totalAssignedUserCnt = $in_progress_count + $completed_count + $not_started_count;
					$complianceDiv = getCourseComplianceIcon($courseId);
					
					$courseHTML .=  '<tr>';
					if(!empty($export)){
					    $courseHTML .=  '<td>'.$courseName.$complianceDiv.'</td>';
						$courseHTML .=  '<td>'.$totalAssignedUserCnt.'</td>';
						$courseHTML .=  '<td>'.$not_started_count.'</td>';
						$courseHTML .=  '<td>'.$in_progress_count.'</td>';
						$courseHTML .=  '<td>'.$completed_count.'</td>';
					}else{
					    $extraParams = "&startDate=".$sStartDate."&endDate=".$sEndDate;
					    $courseHTML .=  '<td><a href="'.$CFG->wwwroot.'/course/coursereportdetails.php?cid='.$courseId.$extraParams.'"  >'.$courseName.$complianceDiv.'</a></td>';
						$courseHTML .=  '<td><a href="'.$CFG->wwwroot.'/course/coursereportdetails.php?cid='.$courseId.$extraParams.'"  >'.$totalAssignedUserCnt.'</a></td>';
						$courseHTML .=  '<td><a href="'.$CFG->wwwroot.'/course/coursereportdetails.php?cid='.$courseId.$extraParams.'&type=3"  >'.$not_started_count.'</a></td>';
						$courseHTML .=  '<td><a href="'.$CFG->wwwroot.'/course/coursereportdetails.php?cid='.$courseId.$extraParams.'&type=1"  >'.$in_progress_count.'</a></td>';
						$courseHTML .=  '<td><a href="'.$CFG->wwwroot.'/course/coursereportdetails.php?cid='.$courseId.$extraParams.'&type=2"  >'.$completed_count.'</a></td>';
					}
					$courseHTML .=  '</tr>';
					
					$reportContentCSV .= $courseName.",".$totalAssignedUserCnt.",".$not_started_count.",".$in_progress_count.",".$completed_count."\n";
					//$reportContentCSV[11] = array($courseName, $totalAssignedUserCnt, $not_started_count, $in_progress_count, $completed_count);
					$reportContentPDF .= '<tr '.$CFG->pdfTableRowAttribute.' >';
					$reportContentPDF .= '<td>'.$courseName.'</td>';
					$reportContentPDF .= '<td >'.$totalAssignedUserCnt.'</td>';
					$reportContentPDF .= '<td >'.$not_started_count.'</td>';
					$reportContentPDF .= '<td >'.$in_progress_count.'</td>';
					$reportContentPDF .= '<td >'.$completed_count.'</td>';
					$reportContentPDF .= '</tr>';
					
					$i++;

				}
				
				$courseHTML .=  '<script language="javascript" type="text/javascript">';
				$courseHTML .=  '	$(document).ready(function(){
								
												$("#printbun").bind("click", function(e) {
													var url = $(this).attr("rel");
													window.open(url, "'.get_string('courseusagesreport','multicoursereport').'", "'.$CFG->printWindowParameter.'");
												});
								
						             }); ';

				$courseHTML .=  ' window.onload = function () {
					
										var chart = new CanvasJS.Chart("chartContainer",
										{
											title:{
												text: ""
											},
											theme: "theme2",
											data: [
											{        
												type: "doughnut",
												indexLabelFontFamily: "Arial",       
												indexLabelFontSize: 12,
												startAngle:0,
												indexLabelFontColor: "dimgrey",       
												indexLabelLineColor: "darkgrey", 
												toolTipContent: "{y}%", 					

				
												dataPoints: [
												{  y: '.$notStartedPercentage.', label: "'.get_string('notstarted','singlereport').' ('.$notStartedCoursesAll.')", exploded: "'.$notStartedCoursesAllExploded.'" },
												{  y: '.$inProgressPercentage.', label: "'.get_string('inprogress','singlereport').' ('.$inProgressCoursesAll.')", exploded: "'.$inProgressCoursesAllExploded.'"  },
												{  y: '.$completedPercentage.', label: "'.get_string('completed','singlereport').' ('.$completedCoursesAll.')", exploded: "'.$completedCoursesAllExploded.'"  },
									
												]
												
												/*dataPoints: [
												{  y: '.$notStartedPercentage.', label: "'.get_string('notstarted','singlereport').' {y}%" },
												{  y: '.$inProgressPercentage.', label: "'.get_string('inprogress','singlereport').' {y}%" },
												{  y: '.$completedPercentage.', label: "'.get_string('completed','singlereport').' {y}%" },
									
												]*/
											}
											]
										});';
					
						if($notStartedPercentage || $inProgressPercentage || $completedPercentage){					
						  $courseHTML .=  '	chart.render(); ';
						}
												 
						$courseHTML .=  '  }
									</script>
									<script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js"></script>';
				
			}else{
			            $courseHTML .=  '<tr><td colspan="5" >'.get_string('norecordfound','singlecoursereport').'</td></tr>';
			      $reportContentCSV .= get_string('norecordfound','multicoursereport')."\n";
			      $reportContentPDF .= get_string('norecordfound','multicoursereport')."\n";
			}
			
			$reportContentPDF .= '</table>';
			
			$courseHTML .= '</tbody></table></div></div></div></div>';
	
            if(empty($export)){ 
				$courseHTML .= paging_bar($limitCount, $page, $perpage, $genURL);
				
				/*if(strstr($_SERVER['REQUEST_URI'], 'course/multicoursereport.php')){
				  $courseHTML .= '<div id="backcell"><input type = "button" value = "'.get_string('back','multiuserreport').'" '.$styleSheet.' onclick="location.href=\''.$CFG->wwwroot.'/course/multicoursereport.php\';"></div>'; 
				}*/
		     }
			$courseHTML .= '';
			
			$courseUsagesReport->courseHTML = $courseHTML;
			$courseUsagesReport->reportContentCSV = $reportContentCSV;
			$courseUsagesReport->reportContentPDF = $reportContentPDF;
			
			return $courseUsagesReport;

      }
	  
	  
	 /**
	 * This function is using for getting a user performance report
	 * @global object
	 * @param array $paramArray is search array
	 * @param array $removeKeyArray contain search key that you don't want to add in address bar
	 * @param string $sort is sorting field
	 * @param string $dir is sorting order
	 * @param int $page current page
	 * @param int $perpage is number of records per page
	 * @param string $export is action variable for PDF download, CSV download, and Print an user performance report
	  * @return array $userPerformanceReport return CSV , PDF and Report Content
	 */
	  
	  
	  function UserPerformanceReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export=''){
		
		  //ini_set('display_errors', 1);
		  //error_reporting(E_ALL);
				
			global $DB, $CFG, $USER;
			$isReport = true;
			$userPerformanceReport = new stdClass();
			
			////// Getting common URL for the Search //////
			$pageURL = $_SERVER['PHP_SELF'];
			$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
			
			$actionUrl = '/user/multiuser_report_print.php';
			$genActionURL = genParameterizedURL($paramArray, $removeKeyArray, $actionUrl);

					
			$sDepartment    = $paramArray['department'];
			$sTeam          = $paramArray['team'];
			$sUser          = $paramArray['user'];
			$sStartDate     = $paramArray['startDate'];
			$sEndDate       = $paramArray['endDate'];
			$sType			= $paramArray['type'];

			$sUserGroup     = $paramArray['user_group'];
			
			$sJobTitle          = $paramArray['job_title'];
			$sCompany          = $paramArray['company'];
			$sJobTitleArr = explode("@",$sJobTitle);
			$sCompanyArr = explode("@",$sCompany);
			$sJobTitleName = $sJobTitle=='-1'?(get_string('all','multiuserreport')):getJobTitles($sJobTitleArr);
			$sCompanyName = $sCompany=='-1'?(get_string('all','multiuserreport')):getCompany($sCompanyArr);

			$sDepartmentArr = explode("@",$sDepartment);
			$sTeamArr = explode("@",$sTeam);
			$sUserArr = explode("@",$sUser);
			$sTypeArr = explode("@",$sType);

			$sUserGroupArr = explode("@",$sUserGroup);
			if($paramArray['sel_mode'] == 2){
				$sTeamArr = $sUserGroupArr;
			}
			$sDepartmentName = $sDepartment=='-1'?(get_string('all','multiuserreport')):getDepartments($sDepartmentArr, $isReport);
			if($USER->archetype != $CFG->userTypeAdmin ){
				$sDepartmentName = getDepartments($USER->department, $isReport);
			}
			$sTeamName = $sTeam=='-1'?(get_string('all','multiuserreport')):getTeams($sTeamArr);
			$sUserFullName = $sUser=='-1'?(get_string('all','multiuserreport')):getUsers($sUserArr);
			
			$sUserGroupName = $sUserGroup=='-1'?(get_string('all','multiuserreport')):getUserGroup($sUserGroupArr);
			$searchString = "";
			$searchDString = "";
			$searchTString = "";
	
			
			$sDateSelected = '';
			if($sStartDate){
				$sDateArr = getFormattedTimeStampOfDate($sStartDate);
			    $sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

			}		   
			
			$eDateSelected = '';
			if($sEndDate){
			   $eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
			   $eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
			}
	
			
			
			if(count($sUserArr) > 0 && !in_array($sUserArr[0] , array(0, '-1'))){
				$sUserStr = isset($sUserArr) && count($sUserArr) > 0?implode(",", $sUserArr):0;
				$searchString .= " AND mu.id IN ($sUserStr) ";
			}else{
				if($_REQUEST["sel_mode"] == 2){
					$userIdArr = getOpenTeamsUser($sTeamArr);
				}else{
					$userIdArr = getUserListByDnT($sDepartmentArr, $sTeamArr, $sUserArr, 2, $isReport, 0, $sJobTitleArr, $sCompanyArr);
				}
				$sUserStr = isset($userIdArr) && count($userIdArr) > 0?implode(",", $userIdArr):0;
				$searchString .= " AND mu.id IN ($sUserStr) ";
			}
			
			if($sDateSelected && $eDateSelected){ 
			
				$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
				$sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
			   
				$searchString .= " AND ((fecdu.f_min_activity_lastaccessed >= $sStartDateTime && fecdu.f_min_activity_lastaccessed <= $sEndDateTime ) || (fecdu.f_max_activity_lastaccessed >= $sStartDateTime && fecdu.f_max_activity_lastaccessed <= $sEndDateTime ) )";

			}elseif($sDateSelected && $eDateSelected == ''){ 
				
				$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
			    $searchString .= " AND (fecdu.f_min_activity_lastaccessed >= ($sStartDateTime) || fecdu.f_max_activity_lastaccessed >= ($sStartDateTime) )";
				
			}elseif($sDateSelected =='' && $eDateSelected){ 
			  
			   $sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
			   $searchString .= " AND (fecdu.f_min_activity_lastaccessed <= ($sEndDateTime) || fecdu.f_max_activity_lastaccessed <= ($sEndDateTime) )";
			}
			
			$havingClause = '';
			if($sType == '-1'){
			  $sTypeName = get_string('all','multiuserreport');
			}else{
			
			  $repTypeArr = $CFG->courseStatusArr;
			  $sTypeArr2 = str_replace(array(3, 1, 2), $repTypeArr, $sTypeArr); 
			  $sTypeName = implode(", ", $sTypeArr2);
              
			  if(count($sTypeArr) == 2 ){
				  if(in_array(1, $sTypeArr) && in_array(2, $sTypeArr)){
				    $havingClause .= " HAVING (sum(if( fecdu.f_course_status  = 1, 1 , 0)) > 0 OR sum(if( fecdu.f_course_status  = 2, 1 , 0)) > 0 ) ";
				  }elseif(in_array(1, $sTypeArr) && in_array(3, $sTypeArr)){
				    $havingClause .= " HAVING (sum(if( fecdu.f_course_status  = 1, 1 , 0)) > 0 OR sum(if( fecdu.f_course_status  = 3, 1 , 0)) > 0 ) ";
				  }elseif(in_array(2, $sTypeArr) && in_array(1, $sTypeArr)){
				    $havingClause .= " HAVING (sum(if( fecdu.f_course_status  = 2, 1 , 0)) > 0 OR sum(if( fecdu.f_course_status  = 1, 1 , 0)) > 0 ) ";
				  }elseif(in_array(2, $sTypeArr) && in_array(3, $sTypeArr)){
				    $havingClause .= " HAVING (sum(if( fecdu.f_course_status  = 2, 1 , 0)) > 0 OR sum(if( fecdu.f_course_status  = 3, 1 , 0)) > 0 ) ";
				  }elseif(in_array(3, $sTypeArr) && in_array(1, $sTypeArr)){
				    $havingClause .= " HAVING (sum(if( fecdu.f_course_status  = 3, 1 , 0)) > 0 OR sum(if( fecdu.f_course_status  = 1, 1 , 0)) > 0 ) ";
				  }elseif(in_array(3, $sTypeArr) && in_array(2, $sTypeArr)){
				    $havingClause .= " HAVING (sum(if( fecdu.f_course_status  = 3, 1 , 0)) > 0 OR sum(if( fecdu.f_course_status  = 2, 1 , 0)) > 0 ) ";
				  }
			  }elseif(count($sTypeArr)  == 1){
				   if(in_array(1, $sTypeArr) ){
			          $havingClause .= " HAVING sum(if( fecdu.f_course_status  = 1, 1 , 0)) > 0 ";
				   }elseif(in_array(2, $sTypeArr)){
				      $havingClause .= " HAVING sum(if( fecdu.f_course_status  = 2, 1 , 0)) > 0 ";
				   }elseif(in_array(3, $sTypeArr)){
				      $havingClause .= " HAVING sum(if( fecdu.f_course_status  = 3, 1 , 0)) > 0 ";
				   }
			  }
	
			  
			  
			}
			
			if($CFG->showJobTitle == 1 && count($sJobTitleArr) > 0 && !in_array($sJobTitleArr[0] , array(0, '-1'))){
				$sJobTitleStr = count($sJobTitleArr) > 0 ? implode(",", $sJobTitleArr) : 0;
				$searchString .= " AND mu.job_title IN ($sJobTitleStr) ";
			}
			
			if($CFG->showCompany == 1 &&  count($sCompanyArr) > 0 && !in_array($sCompanyArr[0] , array(0, '-1'))){
				$sCompanyStr = count($sCompanyArr) > 0 ? implode(",", $sCompanyArr) : 0;
				$searchString .= " AND mu.company IN ($sCompanyStr) ";
			}
				
				
		    $offset = $page - 1;
			$offset = $offset*$perpage;
			$limit = '';
			if($perpage != 0){
				$limit = "LIMIT $offset, $perpage";
			}
				
			
			$excludedUsers = count($CFG->excludedUsers) > 0 ? implode(",",$CFG->excludedUsers) : 0;

			/*if($CFG->showInlineManagerNCompany == 1){
			  $inlineManagerField = " ,concat(inline_manager.firstname,' ', inline_manager.lastname) as inline_manager_name ";
			}*/
			
			$queryFields = "SELECT mu.id, mu.username, concat(mu.firstname,' ', mu.lastname) as fullname, fecdu.enrolled_user,
						 sum(if( fecdu.f_course_status  = 1, 1 , 0)) as IP_COUNT, 
						 sum(if( fecdu.f_course_status  = 2, 1 , 0)) as CL_COUNT, 
						 sum(if( fecdu.f_course_status  = 3, 1 , 0)) as NS_COUNT, 
						 count(fecdu.f_course_status) as TT_COUNT $inlineManagerField ";
						 
			$queryFieldsAll = "SELECT fecdu.enrolled_user, 
			 sum(if( fecdu.f_course_status  = 1, 1 , 0)) as IP_COUNT, 
			 sum(if( fecdu.f_course_status  = 2, 1 , 0)) as CL_COUNT, 
			 sum(if( fecdu.f_course_status  = 3, 1 , 0)) as NS_COUNT, 
			 count(fecdu.f_course_status) as TT_COUNT ";
			 
			$queryFieldsCount = "SELECT mu.id ";
						 
			$query = " FROM mdl_user mu ";
			$query .= "LEFT JOIN vw_final_enrolled_course_details_of_users fecdu ON (fecdu.enrolled_user = mu.id) ";
			//$query .= "LEFT JOIN vws_final_course_status_of_user fecdu ON (fecdu.enrolled_user = mu.id) ";
			$query .= " WHERE 1 = 1 AND mu.id NOT IN (".$excludedUsers.") ";
			$query .= " AND mu.deleted = '0' ";

            $queryAll = $queryFieldsAll.$query;
			$queryAll .= " $searchString ";
			$queryAll .= " GROUP BY mu.id ".$havingClause;
			$queryAll .= " ORDER BY $sort $dir";
			
			$queryLimit = $queryFields.$query;
		    $queryLimit .= " $searchString ";
		    $queryLimit .= " GROUP BY mu.id ".$havingClause;
		    $queryLimit .= " ORDER BY $sort $dir";
			 if($perpage){
			   $queryLimit .= " $limit";
			}

			
			$queryLimitCount = $queryFieldsCount.$query;
		    $queryLimitCount .= " $searchString ";
		    $queryLimitCount .= " GROUP BY mu.id ".$havingClause;
		 	
            //echo date("Y-m-d H:i:s")."<br>";
	        $limitUserCountArr = $DB->get_records_sql($queryLimitCount);
			$limitUserCount = count($limitUserCountArr);
            //echo date("Y-m-d H:i:s")."<br>";
			$limitedUserArr = $DB->get_records_sql($queryLimit);
	        //echo date("Y-m-d H:i:s")."<br>";
			$allUserArr = $DB->get_records_sql($queryAll);
			$allUserCount = count($allUserArr);
			//echo date("Y-m-d H:i:s")."<br>";die;
						
			$userHTML = '';
			$style = !empty($export)?"style=''":'';
            $userHTML .= "<div class='tabsOuter borderBlockSpace' ".$style."  >";
			$userHTML .= '<div class="clear"></div>';
			
			
			if(empty($export)){
				ob_start(); 
				require_once($CFG->dirroot . '/local/includes/multiuserreportsearch.php');
				$userHTML .= ob_get_contents();
				ob_end_clean();
			    $exportHTML = ''; 
			    if($allUserCount > 0 ){

					$exportHTML .= '<div class="exports_opt_box"> ';
					$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','multiuserreport').'" href="'.$genActionURL.'&perpage=0&&action=exportcsv">'.get_string('downloadcsv','multiuserreport').'</a> ';
					$exportHTML .= '<span class="seperater">&nbsp;</span>';
					$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','multiuserreport').'" id="exportpdf1" href="'.$genActionURL.'&perpage=0&&action=exportpdf">'.get_string('downloadpdf','multiuserreport').'</a> ';
					$exportHTML .= '<span class="seperater">&nbsp;</span>';
					$exportHTML .= '<a rel="'.$genActionURL.'&perpage=0&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','multiuserreport').'" class="print_icon">'.get_string('print','multiuserreport').'</a>';
					$exportHTML .= '</div>';
			   }		
			}
				
			$userHTML .= '<div class="userprofile view_assests">';
				
			$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
			$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
			$sDateSelectedForPrintCSV = getDateFormatForPrint($sDateSelected, 'csv');
			$eDateSelectedForPrintCSV = getDateFormatForPrint($eDateSelected, 'csv');

		   $inProgressCoursesAllExploded = false;
		   $completedCoursesAllExploded = false;
		   $notStartedCoursesAllExploded = false;
		   
		 
		
		   if($allUserCount > 0 ){

				foreach($allUserArr as $userArr){
				
				   $totalAssignedCoursesAll += $userArr->tt_count;
				   $notStartedCoursesAll += $userArr->ns_count;
				   $inProgressCoursesAll += $userArr->ip_count;
				   $completedCoursesAll += $userArr->cl_count;
				
				}
				
				
				$completedPercentage = numberFormat(($completedCoursesAll*100)/$totalAssignedCoursesAll);
				$inProgressPercentage = numberFormat(($inProgressCoursesAll*100)/$totalAssignedCoursesAll);
				$notStartedPercentage = numberFormat(($notStartedCoursesAll*100)/$totalAssignedCoursesAll);
				
				
				$completedPercentage = $completedPercentage?$completedPercentage:0;
				$inProgressPercentage = $inProgressPercentage?$inProgressPercentage:0;
				$notStartedPercentage = $notStartedPercentage?$notStartedPercentage:0;	
				
				
				if(in_array(1, $sTypeArr)){
				   $inProgressCoursesAllExploded = true;
				}
				
				if(in_array(2, $sTypeArr)){
				   $completedCoursesAllExploded = true;
				}
				
				if(in_array(3, $sTypeArr)){
				   $notStartedCoursesAllExploded = true;
				}
				
		   
			   if(!empty($export) && $export == 'print'){
			   	
			   	$reportName = get_string('usereperformancereport','multiuserreport');
			   	$userHTML .= getReportPrintHeader($reportName);
			   	
							$userHTML .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="generaltable margin_bottom">';
								$userHTML .= '<tr>';
								if($_REQUEST["sel_mode"] == 2){
									$userHTML .= '<td colspan = "2" ><strong>'.get_string('global_team').':</strong> '.$sUserGroupName.'</td>';
								}else{
									$userHTML .= '<td><strong>'.get_string('department','multiuserreport').':</strong> '.$sDepartmentName.'</td>';
									$userHTML .= '<td><strong>'.get_string('team','multiuserreport').':</strong>  '.$sTeamName.'</td>';
								}
								$userHTML .= '</tr>';
								
								if($CFG->showJobTitle == 1 && $CFG->showCompany == 1){
									$userHTML .= '<tr>';
										$userHTML .= '<td ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
										$userHTML .= '<td><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
									$userHTML .= '</tr>';
								}elseif($CFG->showJobTitle == 1 && $CFG->showCompany == 0){
									$userHTML .= '<tr>';
										$userHTML .= '<td colspan="2" ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
									$userHTML .= '</tr>';
								}elseif($CFG->showJobTitle == 0 && $CFG->showCompany == 1){
									$userHTML .= '<tr>';
										$userHTML .= '<td colspan="2" ><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
									$userHTML .= '</tr>';
								}
							
								$userHTML .= '<tr>';
									$userHTML .= '<td><strong>'.get_string('user','multiuserreport').':</strong> '.$sUserFullName.'</td>';
									$userHTML .= '<td><strong>'.get_string('statusreport').':</strong> '.$sTypeName.'</td>';
								$userHTML .= '</tr>';
								$userHTML .= '<tr>';
									$userHTML .= '<td width="50%"><strong>'.get_string('lastaccesseddatefrom').':</strong> '.$sDateSelectedForPrint.'</td>';
									$userHTML .= '<td width="50%"><strong>'.get_string('lastaccesseddateto').':</strong>  '.$eDateSelectedForPrint.'</td>';
								$userHTML .= '</tr>';
							$userHTML .= '</table>';
							//$userHTML .= '<span '.$CFG->pdfSpanAttribute.'>&nbsp;<br /><strong>'.get_string('usereperformancereport','multiuserreport').'</strong></span><br /><br />';
				   }
 						 
		
			}
			
			if(empty($export) || (!empty($export) && $export == 'print')){
					if($totalAssignedCoursesAll > 0){
										
						 $userHTML .= '<div class = "single-report-start" id="watch">
										<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('usereperformancereport','multiuserreport').$exportHTML.'</span>
										  <div class="single-report-graph-left" id="chartContainer">'.get_string('loading','multiuserreport').'</div>
										  <div class="single-report-graph-right">
											<div class="course-count-heading">'.get_string('numberofcourses','multiuserreport').'</div>
											<div class="course-count">'.$totalAssignedCoursesAll.'</div>
											<div class="seperator">&nbsp;</div>
											<div class="course-status">
											  <div class="notstarted"><h6>'.get_string('notstarted','multiuserreport').'</h6><span>'.$notStartedCoursesAll.'</span></div>
											  <div class="clear"></div>
											  <div class="inprogress"><h6>'.get_string('inprogress','multiuserreport').'</h6><span>'.$inProgressCoursesAll.'</span></div>
											  <div class="clear"></div>
											  <div class="completed"><h6>'.get_string('completed','multiuserreport').'</h6><span>'.$completedCoursesAll.'</span></div>
											</div>
										  </div>
										</div>
									  </div>
									 <div class="clear"></div>';
									 
					}else{
						/* $userHTML .= '<div class = "single-report-start-nograph">
										<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('usereperformancereport','multiuserreport').$exportHTML.'</span></div>
									 <div class="clear"></div>
									 </div>';*/
					}	
						   
					
					$userHTML .= '<div class=""><div class=""><table cellspacing="0" cellpadding="0" border="0" width="100%" class="generaltable"><tbody>';
					
					/*if($CFG->showInlineManagerNCompany == 1){
					
					          $userHTML .= '<tr>
									   <th width="18%">'.get_string('fullname','multiuserreport').'</th>
									   <th width="16%">'.get_string('username').'</th>
									   <th width="18%">'.get_string('inlinemanager').'</th>
									   <th width="12%">'.get_string('enrolled','multiuserreport').'</th>
									   <th width="12%">'.get_string('notstarted','multiuserreport').'</th>
									   <th width="12%">'.get_string('inprogress','multiuserreport').'</th>
									   <th width="12%">'.get_string('completed','multiuserreport').'</th>
						         </tr>';
								 
					}else{*/
					           $userHTML .= '<tr>
									   <th width="30%">'.get_string('fullname','multiuserreport').'</th>
									   <th width="20%">'.get_string('username').'</th>
									   <th width="15%">'.get_string('enrolled','multiuserreport').'</th>
									   <th width="12%">'.get_string('notstarted','multiuserreport').'</th>
									   <th width="12%">'.get_string('inprogress','multiuserreport').'</th>
									   <th width="12%">'.get_string('completed','multiuserreport').'</th>
						         </tr>';
			       /*}*/
		   
			 }
			 
			
		
			 if($allUserCount > 0 ){
			
			
			        if((!empty($export) && $export != 'print')){
						$reportContentCSV = '';
						
						if($export == 'exportcsv'){
							if($_REQUEST["sel_mode"] == 2){
								$reportContentCSV .= get_string('global_team').",".$sUserGroupName."\n";
							}else{
								$reportContentCSV .= get_string('department','multiuserreport').",".$sDepartmentName."\n".get_string('team','multiuserreport').",".$sTeamName ."\n";
							}
							if($CFG->showJobTitle == 1){
							  $reportContentCSV .= get_string('job_title','user').",".$sJobTitleName."\n";
							}
							if($CFG->showCompany == 1){
							  $reportContentCSV .= get_string('company','user').",".$sCompanyName."\n";
							}
									
							$reportContentCSV .= get_string('user','multiuserreport').",".$sUserFullName."\n";
							$reportContentCSV .= get_string('statusreport').",".$sTypeName."\n";
							$reportContentCSV .= get_string('lastaccesseddatefrom').",".$sDateSelectedForPrintCSV."\n".get_string('lastaccesseddateto').",".$eDateSelectedForPrintCSV."\n";
							
							$reportContentCSV .= get_string('numberofcourses','multiuserreport').",".$totalAssignedCoursesAll."\n";
							$reportContentCSV .= get_string('notstarted','multiuserreport').",".$notStartedCoursesAll." (".floatval($notStartedPercentage)."%)\n";
							$reportContentCSV .= get_string('inprogress','multiuserreport').",".$inProgressCoursesAll." (".floatval($inProgressPercentage)."%)\n";
							$reportContentCSV .= get_string('completed','multiuserreport').",".$completedCoursesAll." (".floatval($completedPercentage)."%)\n";
					
							$reportContentCSV .= get_string('usereperformancereport','multiuserreport')."\n";
							
							/*if($CFG->showInlineManagerNCompany == 1){
							   $reportContentCSV .= get_string('fullname','multiuserreport').",".get_string('username').",".get_string('inlinemanager').",".get_string('enrolled','multiuserreport').",".get_string('notstarted','multiuserreport').",".get_string('inprogress','multiuserreport').",".get_string('completed','multiuserreport')."\n";
							}else{*/
							  $reportContentCSV .= get_string('fullname','multiuserreport').",".get_string('username').",".get_string('enrolled','multiuserreport').",".get_string('notstarted','multiuserreport').",".get_string('inprogress','multiuserreport').",".get_string('completed','multiuserreport')."\n";
							/*}*/
							
						}
						$reportContentPDF = '';
						
						if($export == 'exportpdf'){
								$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="generaltable">';
									$reportContentPDF .= '<tr>';
										if($_REQUEST["sel_mode"] == 2){
											$reportContentPDF .= '<td colspan = "2" ><strong>'.get_string('global_team').':</strong> '.$sUserGroupName.'</td>';
										}else{
			
			
											$reportContentPDF .= '<td><strong>'.get_string('department','multiuserreport').':</strong> '.$sDepartmentName.'</td>';
											$reportContentPDF .= '<td><strong>'.get_string('team','multiuserreport').':</strong>  '.$sTeamName.'</td>';
										}
									$reportContentPDF .= '</tr>';
									
									if($CFG->showJobTitle == 1 && $CFG->showCompany == 1){
										$reportContentPDF .= '<tr>';
											$reportContentPDF .= '<td ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
											$reportContentPDF .= '<td><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
										$reportContentPDF .= '</tr>';
									}elseif($CFG->showJobTitle == 1 && $CFG->showCompany == 0){
										$reportContentPDF .= '<tr>';
											$reportContentPDF .= '<td colspan="2" ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
										$reportContentPDF .= '</tr>';
									}elseif($CFG->showJobTitle == 0 && $CFG->showCompany == 1){
										$reportContentPDF .= '<tr>';
											$reportContentPDF .= '<td colspan="2" ><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
										$reportContentPDF .= '</tr>';
									}
										
									$reportContentPDF .= '<tr>';
										$reportContentPDF .= '<td><strong>'.get_string('user','multiuserreport').':</strong> '.$sUserFullName.'</td>';
										$reportContentPDF .= '<td><strong>'.get_string('statusreport').':</strong> '.$sTypeName.'</td>';
									$reportContentPDF .= '</tr>';
									$reportContentPDF .= '<tr>';
										$reportContentPDF .= '<td><strong>'.get_string('lastaccesseddatefrom').':</strong> '.$sDateSelectedForPrint.'</td>';
										$reportContentPDF .= '<td><strong>'.get_string('lastaccesseddateto').':</strong>  '.$eDateSelectedForPrint.'</td>';	
									$reportContentPDF .= '</tr>';
									
								$reportContentPDF .= '<tr>';
									$reportContentPDF .= '<td><strong>'.get_string('numberofcourses','multiuserreport').':</strong> '.$totalAssignedCoursesAll.'</td>';
									$reportContentPDF .= '<td><strong>'.get_string('notstarted','multiuserreport').':</strong>  '.$notStartedCoursesAll.' ('.floatval($notStartedPercentage).'%)</td>';				
								$reportContentPDF .= '</tr>';
					
								$reportContentPDF .= '<tr>';
									$reportContentPDF .= '<td><strong>'.get_string('inprogress','multiuserreport').':</strong>  '.$inProgressCoursesAll.' ('.floatval($inProgressPercentage).'%)</td>';				$reportContentPDF .= '<td><strong>'.get_string('completed','multiuserreport').':</strong>  '.$completedCoursesAll.' ('.floatval($completedPercentage).'%)</td>';							
								$reportContentPDF .= '</tr>';
							
								// $reportContentPDF .= '<tr><br /><td width="100%"  ><span '.$CFG->pdfSpanAttribute.'><strong>'.get_string('usereperformancereport','multiuserreport').'</strong></span><br /></td></tr>';	
								$reportContentPDF .= '</table>';
								
								$reportContentPDF .= getGraphImageHTML(get_string('usereperformancereport','multiuserreport'));
								
								
								 $reportContentPDF .= '<table '.$CFG->pdfTableStyle.'>';
					            
								/*if($CFG->showInlineManagerNCompany == 1){
									$reportContentPDF .= '<tr '.$CFG->pdfTableHeaderStyle.'>';
									$reportContentPDF .= '<td ><strong>'.get_string('fullname','multiuserreport').'</strong></td>';
									$reportContentPDF .= '<td ><strong> '.get_string('username').'</strong></td>';
									$reportContentPDF .= '<td ><strong> '.get_string('inlinemanager').'</strong></td>';
									$reportContentPDF .= '<td ><strong> '.get_string('enrolled','multiuserreport').'</strong></td>';
									$reportContentPDF .= '<td ><strong>'.get_string('notstarted','multiuserreport').'</strong></td>';
									$reportContentPDF .= '<td ><strong>'.get_string('inprogress','multiuserreport').'</strong></td>';
									$reportContentPDF .= '<td ><strong>'.get_string('completed','multiuserreport').'</strong></td>';
									$reportContentPDF .= '</tr>';
								}else{*/
									$reportContentPDF .= '<tr '.$CFG->pdfTableHeaderStyle.'>';
									$reportContentPDF .= '<td ><strong>'.get_string('fullname','multiuserreport').'</strong></td>';
									$reportContentPDF .= '<td ><strong> '.get_string('username').'</strong></td>';
									$reportContentPDF .= '<td ><strong> '.get_string('enrolled','multiuserreport').'</strong></td>';
									$reportContentPDF .= '<td ><strong>'.get_string('notstarted','multiuserreport').'</strong></td>';
									$reportContentPDF .= '<td ><strong>'.get_string('inprogress','multiuserreport').'</strong></td>';
									$reportContentPDF .= '<td ><strong>'.get_string('completed','multiuserreport').'</strong></td>';
									$reportContentPDF .= '</tr>';
								 /*}*/
								 
							}	 
				  }
				  	 
				  $curtime = time();
				  
				  //pr($limitedUserArr);die;
				  foreach($limitedUserArr as $data){

						$assestHTML = '';
						$userId = $data->id;				 
						$inProgressCourses = $data->ip_count;
						$completedCourses = $data->cl_count;
						$notStartedCourses = $data->ns_count;
						$totalAssignedCourses = $data->tt_count;
					
					    $fullName = $data->fullname;	
						$userName = $data->username;
						/*if($CFG->showInlineManagerNCompany == 1){
						  $inlineManager = $data->inline_manager_name?$data->inline_manager_name:(!empty($export) && $export == 'exportcsv'?getMDashForCSV():getMDash());
						}*/
						$user->id = $userId;
		
						//$imagevalue = $OUTPUT->user_picture($user, array('size'=>64));
						
										
						
						if((!empty($export) && $export == 'print')){
						    $userHTML .=  '<tr>';
							$userHTML .=  '<td>'.$fullName.'</td>';
							$userHTML .=  '<td>'.$userName.'</td>';
							/*if($CFG->showInlineManagerNCompany == 1){
							  $userHTML .=  '<td>'.$inlineManager.'</td>';
							}*/
							$userHTML .=  '<td>'.$totalAssignedCourses.'</td>';
							$userHTML .=  '<td>'.$notStartedCourses.'</td>';
							$userHTML .=  '<td>'.$inProgressCourses.'</td>';
							$userHTML .=  '<td>'.$completedCourses.'</td>';
							$userHTML .=  '</tr>';
						}else{
						    $userHTML .=  '<tr>';
						    $userHTML .=  '<td><a href="'.$CFG->wwwroot.'/user/multiuser_report_details.php?uid='.$userId.'&startDate='.$sStartDate.'&endDate='.$sEndDate.'" >'.$fullName.'</a></td>';
							$userHTML .=  '<td><a href="'.$CFG->wwwroot.'/user/multiuser_report_details.php?uid='.$userId.'" >'.$userName.'</a></td>';
							/*if($CFG->showInlineManagerNCompany == 1){
							  $userHTML .=  '<td>'.$inlineManager.'</td>';
							}*/
							$userHTML .=  '<td><a href="'.$CFG->wwwroot.'/user/multiuser_report_details.php?uid='.$userId.'" >'.$totalAssignedCourses.'</a></td>';
							$userHTML .=  '<td><a href="'.$CFG->wwwroot.'/user/multiuser_report_details.php?uid='.$userId.'&type=3" >'.$notStartedCourses.'</a></td>';
							$userHTML .=  '<td><a href="'.$CFG->wwwroot.'/user/multiuser_report_details.php?uid='.$userId.'&type=1" >'.$inProgressCourses.'</a></td>';
							$userHTML .=  '<td><a href="'.$CFG->wwwroot.'/user/multiuser_report_details.php?uid='.$userId.'&type=2" >'.$completedCourses.'</a></td>';
						    $userHTML .=  '</tr>';
						}
						
						
						if((!empty($export) && $export != 'print')){
						
						    /*if($CFG->showInlineManagerNCompany == 1){
							  $reportContentCSV .= $fullName.",".$userName.",".$inlineManager.",".$totalAssignedCourses.",".$notStartedCourses.",".$inProgressCourses.",".$completedCourses."\n";
					        }else{*/
							  $reportContentCSV .= $fullName.",".$userName.",".$totalAssignedCourses.",".$notStartedCourses.",".$inProgressCourses.",".$completedCourses."\n";
							/*}*/
							
							$reportContentPDF .= '<tr '.$CFG->pdfTableRowAttribute.' >';
							$reportContentPDF .= '<td>'.$fullName.'</td>';
							$reportContentPDF .= '<td>'.$userName.'</td>';
							/*if($CFG->showInlineManagerNCompany == 1){
							  $reportContentPDF .=  '<td>'.$inlineManager.'</td>';
							}*/
							$reportContentPDF .= '<td >'.$totalAssignedCourses.'</td>';
							$reportContentPDF .= '<td >'.$notStartedCourses.'</td>';
							$reportContentPDF .= '<td >'.$inProgressCourses.'</td>';
							$reportContentPDF .= '<td >'.$completedCourses.'</td>';
							$reportContentPDF .= '</tr>';
						}
		
					
					}
					
					if((!empty($export) && $export != 'print')){
					  $reportContentPDF .= '</table>';
					}
					
					if(empty($export) || (!empty($export) && $export == 'print')){
					
							$userHTML .=  '<script language="javascript" type="text/javascript">';
							$userHTML .=  '	$(document).ready(function(){
											
													$("#printbun").bind("click", function(e) {
														var url = $(this).attr("rel");
														window.open(url, "'.get_string('usereperformancereport','multiuserreport').'", "'.$CFG->printWindowParameter.'");
													});
											
											 }); ';
			
							$userHTML .=  ' window.onload = function () {
								
													var chart = new CanvasJS.Chart("chartContainer",
													{
														title:{
															text: ""
														},
														theme: "theme2",
														data: [
														{        
															type: "doughnut",
															indexLabelFontFamily: "Arial",       
															indexLabelFontSize: 12,
															startAngle:0,
															indexLabelFontColor: "dimgrey",       
															indexLabelLineColor: "darkgrey", 
															toolTipContent: "{y}%", 					
						
															
															dataPoints: [
															{  y: '.$notStartedPercentage.', label: "'.get_string('notstarted','singlereport').' ('.$notStartedCoursesAll.')", exploded:"'.$notStartedCoursesAllExploded.'" },
															{  y: '.$inProgressPercentage.', label: "'.get_string('inprogress','singlereport').' ('.$inProgressCoursesAll.')", exploded:"'.$inProgressCoursesAllExploded.'"  },
															{  y: '.$completedPercentage.', label: "'.get_string('completed','singlereport').' ('.$completedCoursesAll.')", exploded:"'.$completedCoursesAllExploded.'"  },
												
															]
															
													
														}
														]
													});';
							
								if($notStartedPercentage || $inProgressPercentage || $completedPercentage){					
								  $userHTML .=  '	chart.render(); ';
								}
														 
								$userHTML .=  '  }
												</script>
												<script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js"></script>';
										
					}
						
				}else{
				
				    if(empty($export) || (!empty($export) && $export == 'print')){
					  $userHTML .=  '<tr><td colspan="7" style="border-bottom:1px solid #b9b9b9!important;">'.get_string('norecordfound','multiuserreport').'</td></tr>';
					}
					
					if((!empty($export) && $export != 'print')){
					  $reportContentCSV .= get_string('norecordfound','multiuserreport')."\n";
					  $reportContentPDF .= get_string('norecordfound','multiuserreport')."\n";
					}
				}
				
			 if(empty($export) || (!empty($export) && $export == 'print')){	
				 $userHTML .= '</tbody></table></div></div></div></div>';
			
				 if(empty($export)){
				  $userHTML .= paging_bar($limitUserCount, $page, $perpage, $genURL);
				 } 
				
			 }
			$userPerformanceReport->userHTML = $userHTML;
			$userPerformanceReport->reportContentCSV = $reportContentCSV;
			$userPerformanceReport->reportContentPDF = $reportContentPDF;
		
		   return $userPerformanceReport;
	  }
	  
	  
	   /**
	 * This function is using for getting a user report
	 * @global object
	 * @param array $paramArray is search array
	 * @param array $removeKeyArray contain search key that you don't want to add in address bar
	 * @param string $sort is sorting field
	 * @param string $dir is sorting order
	 * @param int $page current page
	 * @param int $perpage is number of records per page
	 * @param string $export is action variable for PDF download, CSV download, and Print an user report
	  * @return array $userReport return CSV , PDF and Report Content
	 */
	  
	  
 
	  function getUserReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export=''){
		
		  //ini_set('display_errors', 1);
		  //error_reporting(E_ALL);
				
			global $DB, $CFG,$USER;
			$isReport = true;
			
			$userReport = new stdClass();
			
			////// Getting common URL for the Search //////
			$pageURL = $_SERVER['PHP_SELF'];
			$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
			
			$actionUrl = '/user/user_report_print.php';
			$genActionURL = genParameterizedURL($paramArray, $removeKeyArray, $actionUrl);

					
			$sDepartment    = $paramArray['department'];
			$sTeam          = $paramArray['team'];
			$sUser          = $paramArray['user'];
			$sStatus       = $paramArray['status'];
			$sStartDate     = $paramArray['startDate'];
			$sEndDate       = $paramArray['endDate'];
			
			$sUserGroup		= $paramArray['user_group'];
			
			$sJobTitle          = $paramArray['job_title'];
			$sCompany          = $paramArray['company'];
			$sJobTitleArr = explode("@",$sJobTitle);
			$sCompanyArr = explode("@",$sCompany);
			$sJobTitleName = $sJobTitle=='-1'?(get_string('all','multiuserreport')):getJobTitles($sJobTitleArr);
			$sCompanyName = $sCompany=='-1'?(get_string('all','multiuserreport')):getCompany($sCompanyArr);
			
			$sDepartmentArr = explode("@",$sDepartment);
			$sTeamArr = explode("@",$sTeam);
			$sUserArr = explode("@",$sUser);
			

			$sUserGroupArr = explode("@",$sUserGroup);

			if($_REQUEST["sel_mode"] == 2){
				$sTeamArr = $sUserGroupArr;
			}
			
			$sStatusArr = explode("@",$sStatus);
			
			$sDepartmentName = $sDepartment=='-1'?(get_string('all','multiuserreport')):getDepartments($sDepartmentArr, $isReport);
			if($USER->archetype != $CFG->userTypeAdmin ){
				$sDepartmentName = getDepartments($USER->department, $isReport);
			}
			$sTeamName = $sTeam=='-1'?(get_string('all','multiuserreport')):getTeams($sTeamArr);
			$sUserFullName = $sUser=='-1'?(get_string('all','multiuserreport')):getUsers($sUserArr);

			$sUserGroupName = $sUserGroup=='-1'?(get_string('all','multiuserreport')):getUserGroup($sUserGroupArr);
			//$sUserGroupName = $sUserGroup=='-1'?(get_string('all','multiuserreport')):getTeams($sUserGroupArr);
			
			
			if($sStatus == '-1'){
			  $sStatusName = get_string('all','multiuserreport');
			}else{
			  
			  $repStatusArr = array(0 => get_string('statusactive','multiuserreport'), 1 => get_string('statusdeactive','multiuserreport'));
			  $sStatusArr2 = str_replace(array(0,1), $repStatusArr, $sStatusArr); 
			  $sStatusName = count($sStatusArr2) > 0 ? implode(", ", $sStatusArr2) : 0;
			}
			

			$searchString = "";
			$searchDString = "";
			$searchTString = "";
			
	
			
		    $sDateSelected = '';
			if($sStartDate){
				$sDateArr = getFormattedTimeStampOfDate($sStartDate);
			    $sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

			}		   
			
			$eDateSelected = '';
			if($sEndDate){
			   $eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
			   $eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
			}
			
			
			
			//$searchStringGraph = '';
			if(count($sUserArr) > 0 && !in_array($sUserArr[0] , array(0, '-1'))){
				$sUserStr = count($sUserArr) > 0 ? implode(",", $sUserArr) : 0;
				$searchString .= " AND id IN ($sUserStr) ";
				//$searchStringGraph .= " AND mu.id IN ($sUserStr) ";
			}else{
				if($_REQUEST["sel_mode"] == 2){
					$usersArr = getOpenTeamsUser($sTeamArr);
				}else{
					$usersArr = getUserListByDnT($sDepartmentArr, $sTeamArr, $sUserArr, 2, $isReport, 0, $jobTitleArr, $companyArr);
				}
				$managerUsersIdSql = '';
				if(count($usersArr) > 0 ){
				  $usersId = implode(",", $usersArr);
				  $searchString .= " AND id IN ($usersId)";
				  //$searchStringGraph .= " AND mu.id IN ($usersId)";
				}else{
				   $searchString .= " AND id IN (0)";
				   //$searchStringGraph .= " AND mu.id IN (0)";
				}
				
				
			}
			
			if($CFG->showJobTitle == 1 && count($sJobTitleArr) > 0 && !in_array($sJobTitleArr[0] , array(0, '-1'))){
				$sJobTitleStr = count($sJobTitleArr) > 0 ? implode(",", $sJobTitleArr) : 0;
				$searchString .= " AND jobid IN ($sJobTitleStr) ";
			}
			
			if($CFG->showCompany == 1 &&  count($sCompanyArr) > 0 && !in_array($sCompanyArr[0] , array(0, '-1'))){
				$sCompanyStr = count($sCompanyArr) > 0 ? implode(",", $sCompanyArr) : 0;
				$searchString .= " AND comid IN ($sCompanyStr) ";
			}
			
			
			
            if($sDateSelected){ 
				/*list($month, $day, $year) = explode('/', $sDateSelected);
				$sStartDateTime =  mktime(0, 0, 0, $month, $day, $year);*/
				$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
				$searchString .= " AND timecreated >= ($sStartDateTime) ";
				//$searchStringGraph .= " AND mu.timecreated >= ($sStartDateTime) ";
			}
			
			if($eDateSelected){
			  /* list($month, $day, $year) = explode('/', $eDateSelected);
			   $sEndDateTime = mktime(0, 0, 0, $month, $day, $year);*/
			   $sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
			   $searchString .= " AND timecreated <= ($sEndDateTime) ";
			   //$searchStringGraph .= " AND mu.timecreated <= ($sEndDateTime) ";
			}

			$searchStatusString = '';
			if(count($sStatusArr) > 0 &&  !in_array($sStatusArr[0] , array('-1'))){
				 $sStatusStr = implode(",", $sStatusArr);
				 $searchStatusString .= " AND suspended IN ($sStatusStr)";
			}
				
				
		    $offset = $page - 1;
			$offset = $offset*$perpage;
			$limit = '';
			if($perpage != 0){
				$limit = "LIMIT $offset, $perpage";
			}
			

			$excludedUsers = count($CFG->excludedUsers) > 0 ? implode(",",$CFG->excludedUsers) : 0;

			$queryAll = " SELECT * ";
			$queryAll .= " FROM vw_user_details WHERE 1 = 1";
			$queryAll .= " AND id NOT IN (".$excludedUsers.") ";
			$queryAll .= " AND deleted = '0' ";
			$queryAll .= " $searchString ";
			$queryAll .= " $searchStatusString ";
			$queryAll .= " ORDER BY $sort $dir";
			$queryLimit = $queryAll;
			if($perpage){
			   $queryLimit .= " $limit";
			}
			
			$allSystemUserArr = $DB->get_records_sql($queryLimit);	
			$allSystemUserCount = 0;
			if(count($allSystemUserArr) > 0 ){
			    $allSystemUserAllArr = $DB->get_records_sql($queryAll);
			    $allSystemUserCount = count($allSystemUserAllArr);
		    }
			
			$queryDUsers = "SELECT count(suspended) from vw_user_details mu where suspended = 1 AND deleted = '0' AND id NOT IN ($excludedUsers) $searchString group by suspended";
			 
			$queryAUsers = "SELECT count(suspended) from vw_user_details mu where suspended = 0 AND deleted = '0' AND id NOT IN ($excludedUsers) $searchString group by suspended ";
			 
			$deactivatedUser = $DB->get_field_sql($queryDUsers);
			$activateUser = $DB->get_field_sql($queryAUsers);
			
			$deactivatedUser = $deactivatedUser?$deactivatedUser:0;
			$activateUser = $activateUser?$activateUser:0;
			
			$totalUsers = $deactivatedUser + $activateUser;

			$userHTML = '';
			$style = !empty($export)?"style=''":'';
            $userHTML .= "<div class='tabsOuter borderBlockSpace' ".$style."  >";
			$userHTML .= '<div class="clear"></div>';
			
			
			if(empty($export)){
				ob_start();
				require_once($CFG->dirroot . '/local/includes/userreportsearch.php');
				$userHTML .= ob_get_contents();
				ob_end_clean();
			
				$exportHTML = '<div class="exports_opt_box"> ';
				$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','multiuserreport').'" href="'.$genActionURL.'&perpage=0&&action=exportcsv">'.get_string('downloadcsv','multiuserreport').'</a> ';
				$exportHTML .= '<span class="seperater">&nbsp;</span>';
				$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','multiuserreport').'" id="exportpdf1" href="'.$genActionURL.'&perpage=0&&action=exportpdf">'.get_string('downloadpdf','multiuserreport').'</a> ';
				$exportHTML .= '<span class="seperater">&nbsp;</span>';
				$exportHTML .= '<a rel="'.$genActionURL.'&perpage=0&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','multiuserreport').'" class="print_icon">'.get_string('print','multiuserreport').'</a>';
				$exportHTML .= '</div>';
			}
				
			$userHTML .= '<div class="userprofile view_assests">';

			$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
			$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
			$sDateSelectedForPrintCSV = getDateFormatForPrint($sDateSelected, 'csv');
			$eDateSelectedForPrintCSV = getDateFormatForPrint($eDateSelected, 'csv');
		
			if($allSystemUserCount > 0){

				$deactivatedUserExploded = false;
				$activateUserExploded = false;
		
				if(in_array(1, $sStatusArr)){
				   $deactivatedUserExploded = true;
				}
				
				if(in_array(0, $sStatusArr)){
				   $activateUserExploded = true;
				}
				
				
			 $deactivatedUserPercentage = numberFormat(($deactivatedUser*100)/$totalUsers);
			 $activateUserPercentage = numberFormat(($activateUser*100)/$totalUsers);

	
			  if(!empty($export) && $export == 'print'){
			  	
					  	$reportName = get_string('userreport','multiuserreport');
					  	$userHTML .= getReportPrintHeader($reportName);
			  	
						$userHTML .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="generaltable margin_bottom">';
							if($_REQUEST["sel_mode"] == 2){
								$userHTML .= '<tr>';
									$userHTML .= '<td  ><strong>'.get_string('global_team').':</strong> '.$sUserGroupName.'</td>';
									$userHTML .= '<td  ><strong>'.get_string('user','multiuserreport').':</strong> '.$sUserFullName.'</td>';
								$userHTML .= '</tr>';
								$userHTML .= '<tr>';
									$userHTML .= '<td colspan="2" ><strong>'.get_string('status','multiuserreport').':</strong> '.$sStatusName.'</td>';
								$userHTML .= '</tr>';
							}else{
								$userHTML .= '<tr>';
									$userHTML .= '<td width="500"><strong>'.get_string('department','multiuserreport').':</strong> '.$sDepartmentName.'</td>';
									$userHTML .= '<td width="500"><strong>'.get_string('team','multiuserreport').':</strong>  '.$sTeamName.'</td>';
								$userHTML .= '</tr>';
								
								$userHTML .= '<tr>';
								$userHTML .= '<td><strong>'.get_string('user','multiuserreport').':</strong> '.$sUserFullName.'</td>';
								$userHTML .= '<td ><strong>'.get_string('status','multiuserreport').':</strong> '.$sStatusName.'</td>';
							    $userHTML .= '</tr>';
							}
						
						   if($CFG->showJobTitle == 1 && $CFG->showCompany == 1){
								$userHTML .= '<tr>';
									$userHTML .= '<td ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
									$userHTML .= '<td><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
								$userHTML .= '</tr>';
							}elseif($CFG->showJobTitle == 1 && $CFG->showCompany == 0){
							    $userHTML .= '<tr>';
									$userHTML .= '<td colspan="2" ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
								$userHTML .= '</tr>';
							}elseif($CFG->showJobTitle == 0 && $CFG->showCompany == 1){
							    $userHTML .= '<tr>';
									$userHTML .= '<td colspan="2" ><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
								$userHTML .= '</tr>';
							}
							
							$userHTML .= '<tr>';
								$userHTML .= '<td><strong>'.get_string('startdate','multiuserreport').':</strong> '.$sDateSelectedForPrint.'</td>';
								$userHTML .= '<td><strong>'.get_string('enddate','multiuserreport').':</strong>  '.$eDateSelectedForPrint.'</td>';
							$userHTML .= '</tr>';
							
							//$userHTML .= getReportEDAstricFor("print", $sDepartment);
						$userHTML .= '</table>';
						//$userHTML .= '<span '.$CFG->pdfSpanAttribute.'>&nbsp;<br /><strong>'.get_string('userreport','multiuserreport').'</strong></span><br /><br />';
			   }
							
							 
				$userHTML .= '<div class = "single-report-start" id="watch">
						<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('userreport','multiuserreport').$exportHTML.'</span>
						  <div class="single-report-graph-left" id="chartContainer">'.get_string('loading','multiuserreport').'</div>
						  <div class="single-report-graph-right">
							<div class="course-count-heading">'.get_string('graphusers','multiuserreport').'</div>
							<div class="course-count">'.$totalUsers.'</div>
							<div class="seperator">&nbsp;</div>
							<div class="course-status">
							  <div class="publish-active"><h6>'.get_string('statusactive','multicoursereport').'</h6><span>'.$activateUser.'</span></div>
							  <div class="clear"></div>
							  <div class="publish-deactive"><h6>'.get_string('statusdeactive','multicoursereport').'</h6><span>'.$deactivatedUser.'</span></div>
							</div>
						  </div>
						</div>
					  </div>
					  <div class="clear"></div>';			 
			} 
		
			$userHTML .= '<div class="clear"></div>';
			$userHTML .= '<div class=""><table class="generaltable" cellspacing="0" cellpadding="0" border="0" width="100%"><tbody>';
			
			$userHTML .= '<tr>';
			$userHTML .= '<th width="21%">'.get_string('fullname','multiuserreport').'</th>';
			$userHTML .= '<th width="15%">'.get_string('username','multiuserreport').'</th>';
			$userHTML .= '<th width="25%">'.get_string('contactdetails','multiuserreport').'</th>';
			$userHTML .= '<th width="15%">'.get_string('manager','multiuserreport').'</th>';
			$userHTML .= '<th width="13%">'.get_string('department','multiuserreport').'</th>';
			$userHTML .= '<th width="12%">'.get_string('team','multiuserreport').'</th>';
			$userHTML .= '</tr>';
			
		   
			$reportContentCSV = '';
			if($_REQUEST["sel_mode"] == 2){
				$reportContentCSV .= get_string('global_team').",".$sUserGroupName."\n";
			}else{
				$reportContentCSV .= get_string('department','multiuserreport').",".$sDepartmentName."\n".get_string('team','multiuserreport').",".$sTeamName ."\n";
				
			}
			$reportContentCSV .= get_string('user','multiuserreport').",".$sUserFullName."\n";
			if($CFG->showJobTitle == 1){
			  $reportContentCSV .= get_string('job_title','user').",".$sJobTitleName."\n";
			}
			if($CFG->showCompany == 1){
			  $reportContentCSV .= get_string('company','user').",".$sCompanyName."\n";
			}
			$reportContentCSV .= get_string('status','multiuserreport').",".$sStatusName."\n";
			$reportContentCSV .= get_string('daterangefrom','multicoursereport').",".$sDateSelectedForPrintCSV."\n".get_string('daterangeto','multicoursereport').",".$eDateSelectedForPrintCSV."\n";
			//$reportContentCSV .= getReportEDAstricFor("csv", $sDepartment);
			$reportContentCSV .= get_string('graphusers','multiuserreport').",".$totalUsers."\n";
			$reportContentCSV .= get_string('statusactive','multiuserreport').",".$activateUser." (".floatval($activateUserPercentage)."%)\n";
			$reportContentCSV .= get_string('statusdeactive','multiuserreport').",".$deactivatedUser." (".floatval($deactivatedUserPercentage)."%)\n";
			$reportContentCSV .= get_string('userreport','multiuserreport')."\n";
			$reportContentCSV .= get_string('fullname','multiuserreport').",".get_string('username','multiuserreport').",".get_string('contactdetails','multiuserreport').",".get_string('manager','multiuserreport').",".get_string('department','multiuserreport').",".get_string('team','multiuserreport')."\n";
			
			
			$reportContentPDF = '';
			
			$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="generaltable">';
				
				if($_REQUEST["sel_mode"] == 2){
					$reportContentPDF .= '<tr>';
					   $reportContentPDF .= '<td  width="50%" ><strong>'.get_string('global_team').':</strong> '.$sUserGroupName.'</td>';
					   $reportContentPDF .= '<td  width="50%" ><strong>'.get_string('user','multiuserreport').':</strong> '.$sUserFullName.'</td>';
					$reportContentPDF .= '</tr>';
					
					$reportContentPDF .= '<tr>';
					   $reportContentPDF .= '<td  width="100%" colspan="2"><strong>'.get_string('status','multiuserreport').':</strong> '.$sStatusName.'</td>';
					$reportContentPDF .= '</tr>';
					
				}else{
				
					$reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td width="50%" ><strong>'.get_string('department','multiuserreport').':</strong> '.$sDepartmentName.'</td>';
						$reportContentPDF .= '<td width="50%" ><strong>'.get_string('team','multiuserreport').':</strong>  '.$sTeamName.'</td>';
					$reportContentPDF .= '</tr>';	
					$reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td  width="50%" ><strong>'.get_string('user','multiuserreport').':</strong> '.$sUserFullName.'</td>';
						$reportContentPDF .= '<td  width="50%" ><strong>'.get_string('status','multiuserreport').':</strong> '.$sStatusName.'</td>';
					$reportContentPDF .= '</tr>';
			
				}
				
				if($CFG->showJobTitle == 1 && $CFG->showCompany == 1){
					$reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
						$reportContentPDF .= '<td><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
					$reportContentPDF .= '</tr>';
				}elseif($CFG->showJobTitle == 1 && $CFG->showCompany == 0){
					$reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td colspan="2" ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
					$reportContentPDF .= '</tr>';
				}elseif($CFG->showJobTitle == 0 && $CFG->showCompany == 1){
					$reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td colspan="2" ><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
					$reportContentPDF .= '</tr>';
				}
							
					
				$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td width="50%" ><strong>'.get_string('daterangefrom','multicoursereport').':</strong> '.$sDateSelectedForPrint.'</td>';
					$reportContentPDF .= '<td width="50%"  ><strong>'.get_string('daterangeto','multicoursereport').':</strong>  '.$eDateSelectedForPrint.'</td>';	
				$reportContentPDF .= '</tr>';
				
				$reportContentPDF .= '<tr>';
				   $reportContentPDF .= '<td  width="100%" colspan="2"><strong>'.get_string('graphusers','multiuserreport').':</strong> '.$totalUsers.'</td>';
				$reportContentPDF .= '</tr>';
				$reportContentPDF .= '<tr>';
				   $reportContentPDF .= '<td  width="50%" ><strong>'.get_string('statusactive','multicoursereport').':</strong> '.$activateUser.' ('.floatval($activateUserPercentage).'%)</td>';
				   $reportContentPDF .= '<td  width="50%" ><strong>'.get_string('statusdeactive','multicoursereport').':</strong> '.$deactivatedUser.' ('.floatval($deactivatedUserPercentage).'%)</td>';
				$reportContentPDF .= '</tr>';
				
			   //$reportContentPDF .= getReportEDAstricFor("pdf", $sDepartment);
	
			   //$reportContentPDF .= '<tr><br /><td width="100%"  ><span '.$CFG->pdfSpanAttribute.'><strong>'.get_string('userreport','multiuserreport').'</strong></span><br /></td></tr>';	
			 $reportContentPDF .= '</table>';
			 
			 $reportContentPDF .= getGraphImageHTML(get_string('userreport','multiuserreport'));
			 
			 $reportContentPDF .= '<table '.$CFG->pdfTableStyle.'>';
             $reportContentPDF .= '<tr '.$CFG->pdfTableHeaderStyle.' >';
			 $reportContentPDF .= '<td ><strong>'.get_string('fullname','multiuserreport').'</strong></td>';
			 $reportContentPDF .= '<td ><strong> '.get_string('username','multiuserreport').'</strong></td>';
			 $reportContentPDF .= '<td  ><strong>'.get_string('contactdetails','multiuserreport').'</strong></td>';
			 $reportContentPDF .= '<td ><strong>'.get_string('manager','multiuserreport').'</strong></td>';
			 $reportContentPDF .= '<td ><strong>'.get_string('department','multiuserreport').'</strong></td>';
			 $reportContentPDF .= '<td ><strong>'.get_string('team','multiuserreport').'</strong></td>';
			 $reportContentPDF .= '</tr>';
			 

			//$totalLearnerInDept = 0;	
			if(count($allSystemUserArr) > 0 ){
				
				  //$curtime = strtotime(date("Y-m-d"));
				  $curtime = time();
				  //pr($allSystemUserArr);
				  //pr($deptsArr);die;
				  $mDash = getMDash();
			      $mDashCSV = getMDashForCSV();
				  foreach($allSystemUserArr as $userId=>$data){
					    $totalLearnerInDept = 0;
						$assestHTML = '';
				        $fullName = $data->fullname;
						$userName = $data->username?$data->username:getMDash();
						$userNameCSV = $data->username?$data->username:getMDashForCSV();
						
						$jobTitleHtml = '';
						$companyHtml = '';
						if($CFG->showJobTitle == 1 && $data->job_title){ 
						  $jobTitleHtml .= $data->job_title;
						}
						if($CFG->showCompany == 1 && $data->company){
							$companyHtml = $data->company;
						}
			
						$contactDetails = ($jobTitleHtml?$jobTitleHtml."<br>":'').$data->email.($data->phone1?"<br>".$data->phone1:'').($data->phone2?"<br>".$data->phone2:'').($companyHtml?"<br>".$companyHtml:'');
						$contactDetailsCSV = $jobTitleHtml.'|'.$data->email.($data->phone1?"|".$data->phone1:'').($data->phone2?"|".$data->phone2:'').'|'.$companyHtml;
						$userManager = $data->usermanager?$data->usermanager:$mDash;
						$userManagerCSV = $data->usermanager?$data->usermanager:$mDashCSV;
						$departmentTitle = $data->departmenttitle?$data->departmenttitle:$mDash;
						$departmentTitleCSV = $data->departmenttitle?$data->departmenttitle:$mDashCSV;
						$departmentIdStr = $data->department_id;
						$teamTitle = $data->teamtitle?(str_replace(",",",<br>",$data->teamtitle)):$mDash;
						$teamTitleCSV =  $data->teamtitle?(str_replace(",","|", $data->teamtitle)):$mDashCSV;
						$createdById = $data->createdby;
						$user->id = $userId;
						
		
						//$imagevalue = $OUTPUT->user_picture($user, array('size'=>64));
						
										
						$userHTML .=  '<tr>';
						if(!empty($export)){
						 $userHTML .=  '<td>'.$fullName.'</td>';
						 $userHTML .=  '<td>'.$userName.'</td>';
						}else{
						  
						   
						  $pBack =  strstr($_SERVER['REQUEST_URI'], 'user/user_report.php')?3:1;
						  $userHTML .=  '<td><a href="'.$CFG->wwwroot.'/user/multiuser_report_details.php?uid='.$userId.'&back='.$pBack.'" >'.$fullName.'</a></td>';
						  $userHTML .=  '<td><a href="'.$CFG->wwwroot.'/user/multiuser_report_details.php?uid='.$userId.'&back='.$pBack.'" >'.$userName.'</a></td>';
						}
						//$userHTML .=  '<td>'.$fullName.'</td>';
						
						$userHTML .=  '<td class="email-word-wrap">'.$contactDetails.'</td>';
						$userHTML .=  '<td>'.$userManager.'</td>';
						$userHTML .=  '<td>'.$departmentTitle.'</td>';
						$userHTML .=  '<td nowrap="true">'.$teamTitle.'</td>';
						$userHTML .=  '</tr>';
						
						$reportContentCSV .= $fullName.",".$userNameCSV.",".$contactDetailsCSV.",".$userManagerCSV.",".$departmentTitleCSV.",".$teamTitleCSV."\n";
				
						$reportContentPDF .= '<tr '.$CFG->pdfTableRowAttribute.' >';
						$reportContentPDF .= '<td>'.$fullName.'</td>';
						$reportContentPDF .= '<td >'.$userName.'</td>';
						$reportContentPDF .= '<td >'.$contactDetails.'</td>';
						$reportContentPDF .= '<td >'.$userManager.'</td>';
						$reportContentPDF .= '<td >'.$departmentTitle.'</td>';
						$reportContentPDF .= '<td >'.$teamTitle.'</td>';
						$reportContentPDF .= '</tr>';
		
					
					}

					$reportContentPDF .= '</table>';
					
					$userHTML .=  '<script language="javascript" type="text/javascript">';
					$userHTML .=  '	$(document).ready(function(){
									
											$("#printbun").bind("click", function(e) {
												var url = $(this).attr("rel");
												window.open(url, "'.get_string('userereport','multiuserreport').'", "'.$CFG->printWindowParameter.'");
											});
									
									 }); ';
				      
					  //$totalLearnerInDept = $departmentLearners['totalLearnerInDept'];

			 
	                 // if($totalLearnerInDept > 0 ){
					  if($allSystemUserCount > 0 ){
					        $graphtLabelHtml = '';
							$userHTML .=  ' window.onload = function () {

													var chart = new CanvasJS.Chart("chartContainer", {
																  theme: "theme2",//theme1
																  title:{
																	  /*text: "'.get_string('departmentvsuserchart', 'multiuserreport').'"*/
																 },
																  data: [              
																  {
																	  // Change type to "bar", "splineArea", "area", "spline", "pie","column",  "doughnut" etc.
																	  type: "doughnut",
																	  indexLabelFontFamily: "Arial",       
																	  indexLabelFontSize: 12,
																	  startAngle:0,
																	  indexLabelFontColor: "dimgrey",       
																	  indexLabelLineColor: "darkgrey", 
																	  toolTipContent: "{y}%",
																	  //indexLabelPlacement: "inside",
																	  '; 
																	  
			  
																    $userHTML .=  '  dataPoints: [ ';
								
																	  $graphtLabelHtml .= '{  y: '.$activateUserPercentage.', label: "'.get_string('statusactive','multiuserreport').' ('.$activateUser.')", exploded: "'.$activateUserExploded.'" },';
																	  $graphtLabelHtml .= '{  y: '.$deactivatedUserPercentage.', label: "'.get_string('statusdeactive','multiuserreport').' ('.$deactivatedUser.')", exploded: "'.$deactivatedUserExploded.'" },';
																	
																						
																	  $userHTML .= 	$graphtLabelHtml;		
																	  $userHTML .=  '  ]
																  }
																  ]
															  });
													
															  chart.render();' ;
				  
								
													
																	 
								$userHTML .=  '  } ';
								
								$userHTML .=  ' </script>
										<script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js">';
										
						       $userHTML .=  '</script>';
						 }
						
										
						
				}else{
					$userHTML .=  '<tr><td colspan="6" style="border-bottom:1px solid #b9b9b9!important;">'.get_string('norecordfound','multiuserreport').'</td></tr>';
					$reportContentCSV .= get_string('norecordfound','multiuserreport')."\n";
					$reportContentPDF .= get_string('norecordfound','multiuserreport')."\n";
				}
			$userHTML .= '</tbody></table></div></div></div>';
		
		    if(empty($export)){
			  $userHTML .= paging_bar($allSystemUserCount, $page, $perpage, $genURL);
			  //$userHTML .= '<input type = "button" value = "'.get_string('back','multiuserreport').'" onclick="location.href=\''.$CFG->wwwroot.'/my/adminreportdashboard.php\';">';
			 } 
            $userHTML .= '';
			
			$userReport->userHTML = $userHTML;
			$userReport->reportContentCSV = $reportContentCSV;
			$userReport->reportContentPDF = $reportContentPDF;
		
		   return $userReport;
	  }
	  
	 
	   
	  /**
	 * This function is using for getting a single learner report
	 * @global object
	 * @param array $paramArray is search array
	 * @param array $removeKeyArray contain search key that you don't want to add in address bar
	 * @param string $sort is sorting field
	 * @param string $dir is sorting order
	 * @param int $page current page
	 * @param int $perpage is number of records per page
	 * @param string $export is action variable for PDF download, CSV download, and Print a single learner report
	  * @return array $learnerReport return CSV , PDF and Report Content
	 */
	  
	  
	  function getLearnerReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export=''){
		
	        global $DB, $CFG, $USER, $SITE;
			$loginUserId = $USER->id;
			$isReport = true;
			$learnerReport = new stdClass();
			
			$offset = $page - 1;
			$offset = $offset*$perpage;
			$limit = '';
			if($perpage != 0){
				$limit = "LIMIT $offset, $perpage";
			}

			////// Getting common URL for the Search //////
			$pageURL = $_SERVER['PHP_SELF'];
			$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
			
			$printUrl = '/user/single_report_print.php';
			$genPrintURL = genParameterizedURL($paramArray, $removeKeyArray, $printUrl);
				  
	        $userId    = $paramArray['uid'];
	        $sDepartment    = $paramArray['department'];
			$sTeam          = $paramArray['team'];
			$sCourse        = $paramArray['course'];
			$sType          = $paramArray['type'];
			$sStartDate     = $paramArray['startDate'];
			$sEndDate       = $paramArray['endDate'];
			$sBack       = $paramArray['back'];
			
			$sCid       = $paramArray['cid'];
			
			$isUserDeleted = isUserDeleted($userId);
			if($isUserDeleted){
				redirect('/');
			}

			$sDateSelected = '';
			if($sStartDate){
				$sDateArr = getFormattedTimeStampOfDate($sStartDate);
			    $sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

			}		   
			
			$eDateSelected = '';
			if($sEndDate){
			   $eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
			   $eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
			}
			
			$sDepartmentArr = explode("@",$sDepartment);
			$sTeamArr = explode("@",$sTeam);
			$sCourseArr = explode("@",$sCourse);
			$sTypeArr = explode("@",$sType);
			
			
			$sDepartmentName = $sDepartment=='-1'?(get_string('all','singlereport')):getDepartments($sDepartmentArr, $isReport);
			if($USER->archetype != $CFG->userTypeAdmin ){
				$sDepartmentName = getDepartments($USER->department, $isReport);
			}
		
			$sTeamName = $sTeam=='-1'?(get_string('all','singlereport')):getTeams($sTeamArr);
			$sCourseName = $sCourse=='-1'?(get_string('all','singlereport')):getCourses($sCourseArr);
			
			if($sType == '-1'){
			  $sTypeName = get_string('all','singlereport');
			}else{
			
			  $repTypeArr = $CFG->courseStatusArr;
			  $sTypeArr2 = str_replace(array(3, 1, 2), $repTypeArr, $sTypeArr); 
			  $sTypeName = count($sTypeArr2) > 0 ? implode(", ", $sTypeArr2) : '';
			}
			

			
			// search condition start here
			
			$searchString = "";



		   if($sDateSelected && $eDateSelected){ 
			
				
				$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
				$sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
			   
				$searchString .= " AND ((fecdu.f_min_activity_lastaccessed >= $sStartDateTime && fecdu.f_min_activity_lastaccessed <= $sEndDateTime ) || (fecdu.f_max_activity_lastaccessed >= $sStartDateTime && fecdu.f_max_activity_lastaccessed <= $sEndDateTime ) )";

			}elseif($sDateSelected && $eDateSelected == ''){ 
				$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
			    $searchString .= " AND (fecdu.f_min_activity_lastaccessed >= ($sStartDateTime) || fecdu.f_max_activity_lastaccessed >= ($sStartDateTime) )";
				
			}elseif($sDateSelected =='' && $eDateSelected){ 
			   $sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
			   $searchString .= " AND (fecdu.f_min_activity_lastaccessed <= ($sEndDateTime) || fecdu.f_max_activity_lastaccessed <= ($sEndDateTime) )";
			}
			
		
			
			$userName = getUsers($userId, 1);
			
			// search condition end here
			
			############
			
		   $fullnameField = extractMDashInSql('fecdu.fullname');	
		   $queryFields = "SELECT fecdu.courseid, fecdu.enrolled_user,  $fullnameField as fullname, fecdu.f_course_status,fecdu.completion_date,
						 sum(if( fecdu.f_course_status  = 1, 1 , 0)) as IP_COUNT, 
						 sum(if( fecdu.f_course_status  = 2, 1 , 0)) as CL_COUNT, 
						 sum(if( fecdu.f_course_status  = 3, 1 , 0)) as NS_COUNT, 
						 count(fecdu.f_course_status) as TT_COUNT  ";
						 
			$query = " FROM vw_final_enrolled_course_details_of_users fecdu ";
			
			$query .= " WHERE 1 = 1 AND enrolled_user='".$userId."' ";

            $queryAll = $queryFields.$query;
			$queryAll .= " $searchString ";
			$queryAll .= " group by fecdu.courseid ";
			
			$queryAll .= " ORDER BY fullname ASC ";
			
			$queryLimit = $queryFields.$query;
		    $queryLimit .= " $searchString ". $limitSearchString;
			$queryLimit .= " group by fecdu.courseid ";
		    $queryLimit .= " ORDER BY fullname ASC";
			
			$queryLimitCount = $queryFields.$query;
		    $queryLimitCount .= " $searchString ". $limitSearchString;
			$queryLimitCount .= " group by fecdu.courseid ";
			
		    if($perpage){
			   $queryLimit .= " $limit";
			}

            //first we will check the user enrollment to avoid to run below view , if there is no enrollemnt for this user
            $sql = "SELECT count(id) FROM mdl_user_enrolments WHERE userid='".$userId."'";
			$haveAnyEnrollment = $DB->count_records_sql($sql);
            
			$limitCourseCount = 0;
			$allCourseCount = 0;
			if($haveAnyEnrollment > 0 ){ 
			
	            $limitCourseCountArr = $DB->get_records_sql($queryLimitCount);
				$limitCourseCount = count($limitCourseCountArr);
				if($limitCourseCount > 0 ){
	
					$limitedCourseArr = $DB->get_records_sql($queryLimit);	
					$allCourseArr = $DB->get_records_sql($queryAll);
					$allCourseCount = count($allCourseArr);
					
				}
			}
			
			##########
			
			

			$HTML = '';
			$pagename = basename($pageURL); 
			
			if(empty($export) && $USER->archetype != $CFG->userTypeStudent){
			
			  if($sCid){
			  }else{
			    $HTML .= '<select class="reportTypeSelect" name="" pagename="'.$pagename.'" id="reporttype" rel="'.$userId.'"><option value="'.$CFG->courseTypeOnline.'">'.get_string('onlinecourse').'</option><option value="'.$CFG->courseTypeClassroom.'">'.get_string('classroomcourse').'</option></select>';
			  }	
			}
			
			
			if(empty($export) || (!empty($export) && $export == 'print')){
				$style = (!empty($export) || strstr($_SERVER['REQUEST_URI'], 'user/multiuser_report_details.php'))?"style=''":'';
				$HTML .= "<div class='tabsOuter borderBlockSpace' ".$style." >";
				
				$HTML .= '<div class="clear"></div>';
			}
			
			
            $exportHTML = '';
            if(empty($export)){
				
				$exportHTML .= '<div class="exports_opt_box"> ';
				$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','singlereport').'" href="'.$genPrintURL.'&action=exportcsv">'.get_string('downloadcsv','singlereport').'</a> ';
				$exportHTML .= '<span class="seperater">&nbsp;</span>';
				$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','singlereport').'" id="exportpdf1" href="'.$genPrintURL.'&action=exportpdf">'.get_string('downloadpdf','singlereport').'</a> ';
				$exportHTML .= '<span class="seperater">&nbsp;</span>';
				$exportHTML .= '<a rel="'.$genPrintURL.'&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','singlereport').'" class="print_icon">'.get_string('print','singlereport').'</a>';
				$exportHTML .= '</div>';

			}
			
			
			$HTML .= '<div class="clear"></div>';
				if(empty($export) && !strstr($_SERVER['REQUEST_URI'], 'user/multiuser_report_details.php') ){
					ob_start();
					######### this tab already have admin role condition,  ######### 
					#########  so we don't have to put any condition here  ######### 
					#########  to show this tab only in admin panel        ######### 
					
					$user->id = $userId;
					include_once('user_tabs.php');
					
					$HTMLTabs .= ob_get_contents();
					ob_end_clean();
					
					$HTML .= $HTMLTabs;

				 }
			
			$HTML .= '<div class="clear"></div>';
			
			if(empty($export) || (!empty($export) && $export == 'print')){
			
			  $HTML .= '<div class="">';
			
			}
			
			if(empty($export)){
					
				if( $USER->archetype == $CFG->userTypeStudent || (strstr($_SERVER['REQUEST_URI'], 'user/multiuser_report_details.php'))) {
					ob_start();
					require_once($CFG->dirroot . '/local/includes/multiuser_report_details_search.php');
					$SEARCHHTML = ob_get_contents();
					ob_end_clean();
					$HTML .= $SEARCHHTML;
				}
					
			}
			
			$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
			$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
			$sDateSelectedForPrintCSV = getDateFormatForPrint($sDateSelected, 'csv');
			$eDateSelectedForPrintCSV = getDateFormatForPrint($eDateSelected, 'csv');
				
			if((!empty($export) && $export == 'print')){

			 $reportName = get_string('overallcourseprogressreport','singlereport');
			 $HTML .= getReportPrintHeader($reportName);
				
			 $HTML .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="generaltable margin_bottom">';
				$HTML .= '<tr>';
					$HTML .= '<td colspan="2"><strong>'.get_string('user','singlereport').':</strong> '.$userName.'</td>';
				$HTML .= '</tr>';
				
				$HTML .= '<tr>';
						$HTML .= '<td width="50%"><strong>'.get_string('lastaccesseddatefrom').':</strong> '.$sDateSelectedForPrint.'</td>';
						$HTML .= '<td width="50%"><strong>'.get_string('lastaccesseddateto').':</strong>  '.$eDateSelectedForPrint.'</td>';				
					$HTML .= '</tr>';
					
			 $HTML .= '</table>';
			
			}

			$totalAssignedCoursesAll = 0;
			$inProgressCoursesAll = 0;
			$completedCoursesAll = 0;
			$notStartedCoursesAll = 0;
			
			
		    $inProgressCoursesAllExploded = false;
		    $completedCoursesAllExploded = false;
		    $notStartedCoursesAllExploded = false;
			

			if($allCourseCount > 0){
			
			
					foreach($allCourseArr as $dataArr){
					
					   $totalAssignedCoursesAll += $dataArr->tt_count;
					   $notStartedCoursesAll += $dataArr->ns_count;
					   $inProgressCoursesAll += $dataArr->ip_count;
					   $completedCoursesAll += $dataArr->cl_count;
					
					}
					
					
					$completedPercentage = numberFormat(($completedCoursesAll*100)/$totalAssignedCoursesAll);
					$inProgressPercentage = numberFormat(($inProgressCoursesAll*100)/$totalAssignedCoursesAll);
					$notStartedPercentage = numberFormat(($notStartedCoursesAll*100)/$totalAssignedCoursesAll);
					
					
					$completedPercentage = $completedPercentage?$completedPercentage:0;
					$inProgressPercentage = $inProgressPercentage?$inProgressPercentage:0;
					$notStartedPercentage = $notStartedPercentage?$notStartedPercentage:0;	
					
					
					if(in_array(1, $sTypeArr)){
					   $inProgressCoursesAllExploded = true;
					}
					
					if(in_array(2, $sTypeArr)){
					   $completedCoursesAllExploded = true;
					}
					
					if(in_array(3, $sTypeArr)){
					   $notStartedCoursesAllExploded = true;
					}
				
			
			       if(empty($export) || (!empty($export) && $export == 'print')){ 
					   $HTML .= '<div class = "single-report-start" id="watch">
										<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('overallcourseprogressreport','singlereport').$exportHTML.'</span>
										  <div class="single-report-graph-left" id="chartContainer">'.get_string('loading','singlereport').'</div>
										  <div class="single-report-graph-right">
											<div class="course-count-heading">'.get_string('numberofcourses','singlereport').'</div>
											<div class="course-count">'.$totalAssignedCoursesAll.'</div>
											<div class="seperator">&nbsp;</div>
											<div class="course-status">
											  <div class="notstarted"><h6>'.get_string('notstarted','singlereport').'</h6><span>'.$notStartedCoursesAll.'</span></div>
											  <div class="clear"></div>
											  <div class="inprogress"><h6>'.get_string('inprogress','singlereport').'</h6><span>'.$inProgressCoursesAll.'</span></div>
											  <div class="clear"></div>
											  <div class="completed"><h6>'.get_string('completed','singlereport').'</h6><span>'.$completedCoursesAll.'</span></div>
											</div>
										  </div>
										</div>
								 </div>
								 <div class="clear"></div>';
					}			 
	       } 
			
			if(empty($export) || (!empty($export) && $export == 'print')){

				$HTML .= '<div class = "" >';
				$HTML .= '<table class="table1 generaltable" cellpadding="0" border="0" cellspacing="0">
				<tr class="table1_head">
				<th width="20%">'.get_string("coursetitle","singlereport").'</th>
				<th width="13%">'.get_string("status","singlereport").'</th>
				<th width="12%">'.get_string("score","singlereport").'</th>
				<th width="15%">'.get_string("assettimespent","singlereport").'</th>
				<th width="20%">'.get_string("comletiondate").'</th>
				<th width="20%">'.get_string("lastaccessed","singlereport").'</th>
				</tr>';
			}
			
			$reportContentCSV = '';
			$reportContentPDF = '';
			
			if($limitCourseCount > 0){
			
					if((!empty($export) && $export != 'print')){
		
					
						$reportContentCSV .= get_string('user','singlereport').",".$userName."\n";
						$reportContentCSV .= get_string('numberofcourses','singlereport').",".$totalAssignedCoursesAll."\n";
						$reportContentCSV .= get_string('notstarted','singlereport').",".$notStartedCoursesAll." (".floatval($notStartedPercentage)."%)\n";
						$reportContentCSV .= get_string('inprogress','singlereport').",".$inProgressCoursesAll ." (".floatval($inProgressPercentage)."%)\n";
						$reportContentCSV .= get_string('completed','singlereport').",".$completedCoursesAll ." (".floatval($completedPercentage)."%)\n";
						$reportContentCSV .= get_string('lastaccesseddatefrom').",".$sDateSelectedForPrintCSV."\n".get_string('lastaccesseddateto').",".$eDateSelectedForPrintCSV."\n\n";
						$reportContentCSV .= get_string('overallcourseprogressreport','singlereport')."\n";
						
						$reportContentCSV .= get_string('coursetitle','singlereport').",".get_string('status','singlereport').",".get_string('score','singlereport').",".get_string('assettimespent','singlereport').",".get_string('comletiondate').",".get_string('lastaccessed','singlereport')."\n";
						
					}
		
					if(!empty($export) && !strstr($_SERVER['REQUEST_URI'], 'user/single_report_print.php')){
					
						if((!empty($export) && $export != 'print')){
		
							$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0"  class="generaltable"><tr><td colspan="2"></td></tr>';
								$reportContentPDF .= '<tr>';
									$reportContentPDF .= '<td><strong>'.get_string('user','singlereport').':</strong> '.$userName.'</td>';
								$reportContentPDF .= '</tr>';
								$reportContentPDF .= '<tr>';
									$reportContentPDF .= '<td><strong>'.get_string('department','singlereport').':</strong> '.$sDepartmentName.'</td>';
									$reportContentPDF .= '<td><strong>'.get_string('team','singlereport').':</strong>  '.$sTeamName.'</td>';
								$reportContentPDF .= '</tr>';
								$reportContentPDF .= '<tr>';
									$reportContentPDF .= '<td><strong>'.get_string('course','singlereport').':</strong> '.$sCourseName.'</td>';
									$reportContentPDF .= '<td><strong>'.get_string('statusreport').':</strong> '.$sTypeName.'</td>';
								$reportContentPDF .= '</tr>';
								$reportContentPDF .= '<tr>';
									$reportContentPDF .= '<td><strong>'.get_string('lastaccesseddatefrom').':</strong> '.$sDateSelectedForPrint.'</td>';
									$reportContentPDF .= '<td><strong>'.get_string('lastaccesseddateto').':</strong>  '.$eDateSelectedForPrint.'</td>';				
								$reportContentPDF .= '</tr>';
								
								
								
								$reportContentPDF .= '<tr>';
									$reportContentPDF .= '<td><strong>'.get_string('numberofcourses','singlereport').':</strong> '.$totalAssignedCoursesAll.'</td>';
									$reportContentPDF .= '<td><strong>'.get_string('notstarted','singlereport').':</strong>  '.$notStartedCoursesAll.' ('.floatval($notStartedPercentage).'%)</td>';				
								$reportContentPDF .= '</tr>';
					
								$reportContentPDF .= '<tr>';
								$reportContentPDF .= '<td><strong>'.get_string('inprogress','singlereport').':</strong>  '.$inProgressCoursesAll.' ('.floatval($inProgressPercentage).'%)</td>';
								$reportContentPDF .= '<td><strong>'.get_string('completed','singlereport').':</strong>  '.$completedCoursesAll.' ('.floatval($completedPercentage).'%)</td>';							
								$reportContentPDF .= '</tr>';
								
							 $reportContentPDF .= '</table>';
							 
							 $reportContentPDF .= getGraphImageHTML(get_string('overallcourseprogressreport','singlereport'));
						  }						  
					 }else{
					 
					   if((!empty($export) && $export != 'print')){
		
								$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="generaltable">';
								
								if(isset($_GET['cid']) && $_GET['cid']){
									$courseName = getCourses($_GET['cid']); 
									
									$reportContentPDF .= '<tr>';
										$reportContentPDF .= '<td colspan="2" ><strong>'.get_string('user','singlereport').':</strong> '.$userName.'</td>';
									$reportContentPDF .= '</tr>';
									
								}else{
									$reportContentPDF .= '<tr>';
										$reportContentPDF .= '<td colspan="2" ><strong>'.get_string('user','singlereport').':</strong> '.$userName.'</td>';
									$reportContentPDF .= '</tr>';
								}
								
								
								$reportContentPDF .= '<tr>';
									$reportContentPDF .= '<td><strong>'.get_string('numberofcourses','singlereport').':</strong> '.$totalAssignedCoursesAll.'</td>';
									$reportContentPDF .= '<td><strong>'.get_string('notstarted','singlereport').':</strong>  '.$notStartedCoursesAll.' ('.floatval($notStartedPercentage).'%)</td>';				
								$reportContentPDF .= '</tr>';
					
								$reportContentPDF .= '<tr>';
								$reportContentPDF .= '<td><strong>'.get_string('inprogress','singlereport').':</strong>  '.$inProgressCoursesAll.' ('.floatval($inProgressPercentage).'%)</td>';				                    $reportContentPDF .= '<td><strong>'.get_string('completed','singlereport').':</strong>  '.$completedCoursesAll.' ('.floatval($completedPercentage).'%)</td>';							
								 $reportContentPDF .= '</tr>';
								 
								 $reportContentPDF .= '<tr>';
										$reportContentPDF .= '<td><strong>'.get_string('lastaccesseddatefrom').':</strong> '.$sDateSelectedForPrint.'</td>';
										$reportContentPDF .= '<td><strong>'.get_string('lastaccesseddateto').':</strong>  '.$eDateSelectedForPrint.'</td>';				
									$reportContentPDF .= '</tr>';
									

								 $reportContentPDF .= '</table>';
								 
								 $reportContentPDF .= getGraphImageHTML(get_string('overallcourseprogressreport','singlereport'));
						 }		 
					 }	 
					
					
					 if((!empty($export) && $export != 'print')){
		
						 $reportContentPDF .= '<table '.$CFG->pdfTableStyle.'>';
						 $reportContentPDF .= '<tr '.$CFG->pdfTableHeaderStyle.'>';
						 $reportContentPDF .= '<td><strong>'.get_string('coursetitle','singlereport').'</strong></td>';
						 $reportContentPDF .= '<td><strong> '.get_string('status','singlereport').'</strong></td>';
						 $reportContentPDF .= '<td><strong>'.get_string('score','singlereport').'</strong></td>';
						 $reportContentPDF .= '<td ><strong>'.get_string('assettimespent','singlereport').'</strong></td>';
						 $reportContentPDF .= '<td ><strong>'.get_string('comletiondate').'</strong></td>';
						 $reportContentPDF .= '<td ><strong>'.get_string('lastaccessed','singlereport').'</strong></td>';
						 $reportContentPDF .= '</tr>';
					 }	
			 
			 } 
			

			$htmlSpace = getSpace();
			$htmlSpaceCSV = getSpace('csv');

			if($limitCourseCount > 0){
			
				$queryAsset = "select @a:=@a+1 `serial_number`, ast.* from vw_combined_resource_scorm_details_course ast , (SELECT @a:= 0) AS X where ast.enrolled_user='".$userId."' group by ast.courseid, ast.activity_id, ast.activity_type ORDER BY ast.activity_name ASC";
				$cAssetArr = $DB->get_records_sql($queryAsset);
				
				
				$assetArr = array();
				if(count($cAssetArr) > 0 ){
					foreach($cAssetArr as $rows){
						$assetArr[$rows->courseid][] = $rows;
					}
				}
			
				$rowCount=0;
				$curtime = time();
				foreach($limitedCourseArr as $courses){
			
					$rowCount++;
					
					$courseId = $courses->courseid;
					$classmMain = 'evenR';
				
					$courseInstanceArr = $courseId && isset($assetArr[$courseId]) && count($assetArr[$courseId]) > 0 ? $assetArr[$courseId]:array();
					
					$cfullname = $courses->fullname;
			        $courseStatus = isset($courses->f_course_status)?$CFG->courseStatusArr[$courses->f_course_status]:getMDash();
					$courseStatusCSV = isset($courses->f_course_status)?$CFG->courseStatusArr[$courses->f_course_status]:getMDashForCSV();
					
					$complianceDiv = getCourseComplianceIcon($courseId);
					
					if(empty($export) || (!empty($export) && $export == 'print')){
						$HTML .= '<tr class="'.$classmMain.'" id="course-'.$courseId.'">
	
						<td class="align_left" >'.$cfullname.$complianceDiv.'</td>
						<td class="align_left" >'.$courseStatus.'</td>
						<td class="align_left" >'.getSpace().'</td>
						<td class="align_left" >'.getSpace().'</td>
						<td class="align_left" >'.getSpace().'</td>
						<td class="align_left" >'.getSpace().'</td>
						
						</tr>
						<tr>
						<td colspan="6" class="borderLeftRight padding_none">';
					}
					
					
					if((!empty($export) && $export != 'print')){
						$reportContentCSV .= $cfullname.",".$courseStatusCSV.",".(getSpace('csv')).",".(getSpace('csv')).",".(getSpace('csv')).",".(getSpace('csv'))."\n";
						
						$reportContentPDF .= '<tr '.$CFG->pdfTableEvenRowStyle.' >';
						
						$reportContentPDF .= '<td>'.$cfullname.'</td>';
						$reportContentPDF .= '<td>'.$courseStatus.'</td>';
						$reportContentPDF .= '<td>'.getSpace().'</td>';
						$reportContentPDF .= '<td>'.getSpace().'</td>';
						$reportContentPDF .= '<td>'.getSpace().'</td>';
						$reportContentPDF .= '<td>'.getSpace().'</td>';
						$reportContentPDF .= '</tr>';
						   
					 }   
				
				    
                    if(empty($export) || (!empty($export) && $export == 'print')){
						$assestHTML = '';
						$assestHTML .= '<div class="a-box" id="abox'.$courseId.'" >';
                    }
					
					if(count($courseInstanceArr) > 0 ){
					
					   $x=0;
					   foreach($courseInstanceArr as $rows){
					   
						$x++;
						
						$classEvenOdd = $x%2==0 ?'even':'odd';
						$classPDFEvenOdd = $x%2==0 ?$CFG->pdfInnerTableEvenRowStyle:$CFG->pdfInnerTableOddRowStyle;
						
						
                        if(empty($export) || (!empty($export) && $export == 'print')){
						
						   $assestHTML .=  '<div class="'.$classEvenOdd.'" >';
						
						}
				
							$courseType = $rows->activity_type;
						    $userMappingStatus = $rows->user_mapping_status;
							$completionDate = $rows->completion_date;
							if($completionDate){
								$completionDateTime = getDateFormat($completionDate, $CFG->customDefaultDateTimeFormat);
								$completionDateTimeCSV = getDateFormat($completionDate, $CFG->customDefaultDateTimeFormatForCSV);
			
							}else{
								 $completionDateTime = getMDash();
								 $completionDateTimeCSV = getMDashForCSV();
							}
							
							if($courseType == $CFG->scormModule){
							
									
									$scormId = $rows->activity_id;
									$scormTitle = $rows->activity_name;
									$scormDiscription = strip_tags($rows->activity_info);
									$coursesScore = $rows->activity_score;
									$scormStatus = isset($rows->fstatus)?$CFG->courseStatusArr[$rows->fstatus]:getMDash();
									$scormSummary =  strip_tags($rows->activity_info);
									$lastAccessedTime = $rows->activity_lastaccessed;
									$timeSpent = $rows->timespent?(convertCourseSpentTime($rows->timespent)):0;
									if($lastAccessedTime){
									    $lastAccessed = getDateFormat($lastAccessedTime, $CFG->customDefaultDateTimeFormat);
					                    $lastAccessedCSV = getDateFormat($lastAccessedTime, $CFG->customDefaultDateTimeFormatForCSV);
					
									}else{
										 $lastAccessed = get_string('notaccessed','learnercourse');
										 $lastAccessedCSV = get_string('notaccessed','learnercourse');
									}
									
	
									$score = (int)$coursesScore;
									

									$scoreResult = ($score)?$score:getMDash();
									$scoreResultCSV = ($score)?$score:getMDashForCSV();
									
									
                                    if(empty($export) || (!empty($export) && $export == 'print')){
	
										$assestHTML .=  '<div style="width:20%" class = "break-all" >'.$scormTitle.'</div>';
										$assestHTML .=  '<div style="width:13%">'.$scormStatus.'</div>';
										$assestHTML .=  '<div style="width:12%">'.$scoreResult.'</div>';
										$assestHTML .=  '<div style="width:15%">'.$timeSpent.'</div>';
										$assestHTML .=  '<div style="width:20%">'.$completionDateTime.'</div>';
										$assestHTML .=  '<div style="width:20%">'.$lastAccessed.'</div>';
									}
									
									if((!empty($export) && $export != 'print')){
										$reportContentCSV .= $htmlSpaceCSV.$scormTitle.",".$scormStatus.",".$scoreResultCSV.",".$timeSpent.",".$completionDateTimeCSV.",".$lastAccessedCSV."\n";
										
										$reportContentPDF .= '<tr '.$classPDFEvenOdd.'>';
										$reportContentPDF .= '<td>'.$htmlSpace.$scormTitle.'</td>';
										$reportContentPDF .= '<td >'.$scormStatus.'</td>';
										$reportContentPDF .= '<td >'.$scoreResult.'</td>';
										$reportContentPDF .= '<td >'.$timeSpent.'</td>';
										$reportContentPDF .= '<td>'.$completionDateTime.'</td>';
										$reportContentPDF .= '<td>'.$lastAccessed.'</td>';
										$reportContentPDF .= '</tr>';
									}
									
							}elseif($courseType == $CFG->resourceModule){
							  
									
								$resourceCourseId = $rows->courseid;
								$resourceId = $rows->activity_id;
								$resourseName = $rows->activity_name;
								$resourceSummary = strip_tags($rows->activity_info);
								$lastAccessedTime = $rows->activity_lastaccessed;
								$resourceStatus = isset($rows->fstatus)?$CFG->courseStatusArr[$rows->fstatus]:getMDash();
								
								$timeSpent = getMDash();
								$timeSpentCSV = getMDashForCSV();
								
								if($lastAccessedTime){
									$lastAccessed = getDateFormat($lastAccessedTime, $CFG->customDefaultDateTimeFormat);
									$lastAccessedCSV = getDateFormat($lastAccessedTime, $CFG->customDefaultDateTimeFormatForCSV);
				
								}else{
									 $lastAccessed = get_string('notaccessed','learnercourse');
									 $lastAccessedCSV = get_string('notaccessed','learnercourse');
								}
								
								if(empty($export) || (!empty($export) && $export == 'print')){
								
									$assestHTML .=  '<div style="width:20%" class = "break-all">'.$resourseName.'</div>';
									$assestHTML .=  '<div style="width:13%">'.$resourceStatus.'</div>';
									$assestHTML .=  '<div style="width:12%">'.getMDash().'</div>';
									$assestHTML .=  '<div style="width:15%">'.$timeSpent.'</div>';
									$assestHTML .=  '<div style="width:20%">'.$completionDateTime.'</div>';
									$assestHTML .=  '<div style="width:20%">'.$lastAccessed.'</div>';
								}
								
								if((!empty($export) && $export != 'print')){
								
									$reportContentCSV .= "  ".$resourseName.",".$resourceStatus.",".(getMDashForCSV()).",".$timeSpentCSV.",".$completionDateTimeCSV.",".$lastAccessedCSV."\n";
										
									$reportContentPDF .= '<tr '.$classPDFEvenOdd.' >';
									$reportContentPDF .= '<td>'.$htmlSpace.$resourseName.'</td>';
									$reportContentPDF .= '<td >'.$resourceStatus.'</td>';
									$reportContentPDF .= '<td >'.getMDash().'</td>';
									$reportContentPDF .= '<td >'.$timeSpent.'</td>';
									$reportContentPDF .= '<td>'.$completionDateTime.'</td>';
									$reportContentPDF .= '<td>'.$lastAccessed.'</td>';
									$reportContentPDF .= '</tr>';
									
								}	

							}
							
						
						if(empty($export) || (!empty($export) && $export == 'print')){
						 $assestHTML .=  '</div>';
						} 
						  
					 } // end foreach for courseInstanceArr	
					

			
		             if(empty($export) || (!empty($export) && $export == 'print')){
					    $assestHTML .= '</div>';
					  }	
											
						
				 } // end courseInstanceArr if

					
					if(empty($export) || (!empty($export) && $export == 'print')){
					 $HTML .=  $assestHTML;
				     $HTML .= '</td></tr>';
					} 

				}
	
	            if(empty($export) || (!empty($export) && $export == 'print')){ 
					$HTML .=  '<script language="javascript" type="text/javascript">';
					
	
					$HTML .=  ' window.onload = function () {
					
										var chart = new CanvasJS.Chart("chartContainer",
										{
											title:{
												text: ""
											},
											theme: "theme2",
											data: [
											{        
												type: "doughnut",
												indexLabelFontFamily: "Arial",       
												indexLabelFontSize: 12,
												startAngle:0,
												indexLabelFontColor: "dimgrey",       
												indexLabelLineColor: "darkgrey", 
												toolTipContent: "{y}%", 					
									
												dataPoints: [
												{  y: '.$notStartedPercentage.', label: "'.get_string('notstarted','singlereport').' ('.$notStartedCoursesAll.')", exploded: "'.$notStartedCoursesAllExploded.'" },
												{  y: '.$inProgressPercentage.', label: "'.get_string('inprogress','singlereport').' ('.$inProgressCoursesAll.')", exploded: "'.$inProgressCoursesAllExploded.'" },
												{  y: '.$completedPercentage.', label: "'.get_string('completed','singlereport').' ('.$completedCoursesAll.')", exploded: "'.$completedCoursesAllExploded.'" },
									
												]

											}
											]
										}); ';
					
						if($notStartedPercentage || $inProgressPercentage || $completedPercentage){					
						  $HTML .=  '	chart.render(); ';
						}
												 
						$HTML .=  '  }';
							$HTML .=  ' 		</script>
									<script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js"></script>';
									
				}					
				
			}else{
			
			      if(empty($export) || (!empty($export) && $export == 'print')){
				    $HTML .=  '<tr><td colspan="6" align="center" >'.get_string('no_results').'</td></tr>';
				  }	
				  
				  if((!empty($export) && $export != 'print')){
				   $reportContentCSV .= get_string('norecordfound','singlereport')."\n";
				   $reportContentPDF .= get_string('norecordfound','singlereport')."\n";
				  }
			}
			
			
			
			if((!empty($export) && $export != 'print')){
			  $reportContentPDF .= '</table>';
			}
			
			if(empty($export) || (!empty($export) && $export == 'print')){
			  $HTML .= '</tbody></table></div></div></div>';
			
				
				$HTML .=  '	<script>$(document).ready(function(){
				
									 $("#reporttype").change(function(){
		
											var userid = $(this).attr("rel");
											var pagename = $(this).attr("pagename");
											var ctype = $(this).val();
											if(pagename=="single_report.php"){
											  url = "'.$CFG->wwwroot.'/course/classroom_course_report.php?back='.($sBack?$sBack:1).'&userid="+userid+"&ctype=2";	
											}else if(pagename=="multiuser_report_details.php"){
											  url = "'.$CFG->wwwroot.'/course/classroom_course_report.php?back='.($sBack?$sBack:2).'&userid="+userid+"&ctype=2";	
											}
											window.location.href = url;
									});
									
									$("#printbun").bind("click", function(e) {
										var url = $(this).attr("rel");
										window.open(url, "'.get_string('overallcourseprogressreport','singlereport').'", "'.$CFG->printWindowParameter.'");
									});
				
								 });</script> ';
							 
			}
				
			$styleSheet ='';
            if(empty($export)){ 
				$HTML .= paging_bar($limitCourseCount, $page, $perpage, $genURL);
				
				if(strstr($_SERVER['REQUEST_URI'], 'user/multiuser_report_details.php')){
				
					 if($sCid){

					    if($sBack == 1){
						   $HTML .= '<div id="backcell"><input type = "button" value = "'.get_string('back','multiuserreport').'" '.$styleSheet.' onclick="location.href=\''.$CFG->wwwroot.'/course/coursereportdetails.php?cid='.$sCid.'&back='.$sBack.'\';"></div>';
						}else if($sBack == 3){
						   $HTML .= '<div id="backcell"><input type = "button" value = "'.get_string('back','multiuserreport').'" '.$styleSheet.' onclick="location.href=\''.$CFG->wwwroot.'/user/user_report.php\';"></div>';
						}else if($sBack == 2){
						       $HTML .= '<div id="backcell"><input type = "button" value = "'.get_string('back','multiuserreport').'" '.$styleSheet.' onclick="location.href=\''.$CFG->wwwroot.'/course/coursereportdetails.php?cid='.$sCid.'&startDate='.$sStartDate.'&endDate='.$sEndDate.'\';"></div>';
							
						}else if($sBack == 5){
						       $HTML .= '<div id="backcell"><input type = "button" value = "'.get_string('back','multiuserreport').'" '.$styleSheet.' onclick="location.href=\''.$CFG->wwwroot.'/course/singlecoursereport.php?cid='.$sCid.'\';"></div>';
							
						}else{
						   $HTML .= '<div id="backcell"><input type = "button" value = "'.get_string('back','multiuserreport').'" '.$styleSheet.' onclick="location.href=\''.$CFG->wwwroot.'/user/multiuser_report.php?startDate='.$sStartDate.'&endDate='.$sEndDate.'\';"></div>';
						}
					 }else{
						if($sBack == 1){
						   $HTML .= '<div id="backcell"><input type = "button" value = "'.get_string('back','multiuserreport').'" '.$styleSheet.' onclick="location.href=\''.$CFG->wwwroot.'/user/user_report.php\';"></div>';
						}else if($sBack == 3){
						   $HTML .= '<div id="backcell"><input type = "button" value = "'.get_string('back','multiuserreport').'" '.$styleSheet.' onclick="location.href=\''.$CFG->wwwroot.'/user/user_report.php\';"></div>';
						}else{
						   $HTML .= '<div id="backcell"><input type = "button" value = "'.get_string('back','multiuserreport').'" '.$styleSheet.' onclick="location.href=\''.$CFG->wwwroot.'/user/multiuser_report.php?startDate='.$sStartDate.'&endDate='.$sEndDate.'\';"></div>';
						}
					 }	
				}
			}

				
			$learnerReport->HTML = $HTML;
			$learnerReport->reportContentCSV = $reportContentCSV;
			$learnerReport->reportContentPDF = $reportContentPDF;
			
			return $learnerReport;
	  }
	  
		  
	  /**
	 * This function is using for getting a single course v/s multiple learner report
	 * @global object
	 * @param array $paramArray is search array
	 * @param array $removeKeyArray contain search key that you don't want to add in address bar
	 * @param string $sort is sorting field
	 * @param string $dir is sorting order
	 * @param int $page current page
	 * @param int $perpage is number of records per page
	 * @param string $export is action variable for PDF download, CSV download, and Print a single course v/s multiple learner report
	  * @return array $courseReportPerLearner return CSV , PDF and Report Content
	 */
	  
     function getCourseVsMultipleUserReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export=''){
		
		  //ini_set('display_errors', 1);
  	      //error_reporting(E_ALL);
		
	        global $DB, $CFG, $USER, $SITE;
			$loginUserId = $USER->id;
			$isReport = true;
			
			$sType = $paramArray['type'];
			$sBack = $paramArray['back'];
			
			$sTypeArr = explode("@",$sType);

			$sStartDate     = $paramArray['startDate'];
			$sEndDate       = $paramArray['endDate'];

		/*	$sDateSelected = '';
			if($sStartDate){
				list($month1, $day1, $year1) = explode('-', $sStartDate);
				$sDateSelected = date("m/d/Y",mktime(0, 0, 0, $month1, $day1, $year1));
			}		   
			
			$eDateSelected = '';
			if($sEndDate){
				list($month2, $day2, $year2) = explode('-', $sEndDate);
				$eDateSelected = date("m/d/Y",mktime(0, 0, 0, $month2, $day2, $year2));
			}*/
			
			$sDateSelected = '';
			if($sStartDate){
				$sDateArr = getFormattedTimeStampOfDate($sStartDate);
			    $sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

			}		   
			
			$eDateSelected = '';
			if($sEndDate){
			   $eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
			   $eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
			}
			
			
			$offset = $page - 1;
			$offset = $offset*$perpage;
			$limit = '';
			if($perpage != 0){
				$limit = "LIMIT $offset, $perpage";
			}

			////// Getting common URL for the Search //////
			$pageURL = $_SERVER['PHP_SELF'];
			$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
			
			$printUrl = '/course/singlecoursereportprint.php';
			$genPrintURL = genParameterizedURL($paramArray, $removeKeyArray, $printUrl);
				  
	        $cid    = $paramArray['cid'];
			$courseName = getCourses($cid);
			
	        $where = '';
			
			
			
		    if($USER->archetype == $CFG->userTypeAdmin){
			   $where .= '';
		    }elseif($USER->archetype == $CFG->userTypeManager){
				$teamUser = fetchGroupsUserIds($USER->id,1);
				if(!empty($teamUser)){
					$groupsList =  implode(',',$teamUser);
					$where = " AND (gcru.departmentid = ".$USER->department." OR enrolled_user IN ($groupsList)) ";
				}else{
					$where .= ' AND (gcru.departmentid = "'.$USER->department.'")';
				}
		    }elseif($USER->archetype == $CFG->userTypeStudent){
				$teamUser = fetchGroupsUserIds($USER->id,1);
				if(!empty($teamUser)){
					$groupsList =  implode(',',$teamUser);
					$where = " AND (enrolled_user IN ($groupsList)) ";
				}else{
					$where .= ' AND (gcru.departmentid = "'.$USER->department.'")';
				}
			}
			
			
			if($sDateSelected && $eDateSelected){ 
				/*list($month, $day, $year) = explode('/', $sDateSelected);
				$sStartDateTime =  mktime(0, 0, 0, $month, $day, $year);
				
				list($month, $day, $year) = explode('/', $eDateSelected);
			    $sEndDateTime = mktime(23, 59, 59, $month, $day, $year);*/
				
				$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
                $sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
			   
				$where .= " AND ((gcru.f_min_activity_lastaccessed >= $sStartDateTime && gcru.f_min_activity_lastaccessed <= $sEndDateTime ) || (gcru.f_max_activity_lastaccessed >= $sStartDateTime && gcru.f_max_activity_lastaccessed <= $sEndDateTime ) )";

			}elseif($sDateSelected && $eDateSelected == ''){ 
				/*list($month, $day, $year) = explode('/', $sDateSelected);
				$sStartDateTime =  mktime(0, 0, 0, $month, $day, $year);*/
				
				$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;

			    $where .= " AND (gcru.f_min_activity_lastaccessed >= ($sStartDateTime) || gcru.f_max_activity_lastaccessed >= ($sStartDateTime) )";
				
			}elseif($sDateSelected =='' && $eDateSelected){ 
			   /*list($month, $day, $year) = explode('/', $eDateSelected);
			   $sEndDateTime = mktime(23, 59, 59, $month, $day, $year);*/
			   $sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
			   $where .= " AND (gcru.f_min_activity_lastaccessed <= ($sEndDateTime) || gcru.f_max_activity_lastaccessed <= ($sEndDateTime) )";
			}
			
							
			$fullnameField = extractMDashInSql('gcru.fullname');				
            $query = "SELECT *, $fullnameField AS fullname FROM vw_get_course_report_of_users gcru";
			$query .= " WHERE 1 = 1";
			$query .= " AND gcru.courseid = '".$cid."' ";
			$query .= $where;
			$query .= " AND gcru.user_deleted = '0'";
			$query .= " GROUP BY gcru.enrolled_user ";
			$query .= " ORDER BY gcru.ufirstname ASC ";
			$queryLimit = $query.$limit;
			$reportsArr = $DB->get_records_sql($query);
			$limitReportsArr = $DB->get_records_sql($queryLimit);
			$userCount = count($reportsArr);
            
			$in_progress_count = 0;
			$completed_count = 0;
			$not_started_count = 0;
			
			$completedPercentage = 0;
			$inProgressPercentage = 0;
			$notStartedPercentage = 0;
			
			if($userCount > 0 ){

                foreach($reportsArr as $arr){
				
				    if($arr->f_course_status == 1){
					 $in_progress_count++;
					}elseif($arr->f_course_status == 2){
					 $completed_count++;
					}elseif($arr->f_course_status == 3){
					 $not_started_count++;
					}
				}
			
			}
			
			
			$completedPercentage = numberFormat(($completed_count*100)/$userCount);
			$inProgressPercentage = numberFormat(($in_progress_count*100)/$userCount);
			$notStartedPercentage = numberFormat(($not_started_count*100)/$userCount);
			
			$in_progress_count_exploded = in_array(1, $sTypeArr)?true:false;
			$completed_count_exploded = in_array(2, $sTypeArr)?true:false;
			$not_started_count_exploded = in_array(3, $sTypeArr)?true:false;
			
			$courseHTML = '';
			$exportHTML = '';
            if(empty($export)){

				$exportHTML .= '<div class="exports_opt_box"> ';
				$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','singlereport').'" href="'.$genPrintURL.'&action=exportcsv">'.get_string('downloadcsv','singlereport').'</a> ';
				$exportHTML .= '<span class="seperater">&nbsp;</span>';
				$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','singlereport').'" id="exportpdf1" href="'.$genPrintURL.'&action=exportpdf">'.get_string('downloadpdf','singlereport').'</a> ';
				$exportHTML .= '<span class="seperater">&nbsp;</span>';
				$exportHTML .= '<a rel="'.$genPrintURL.'&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','singlereport').'" class="print_icon">'.get_string('print','singlereport').'</a>';
				$exportHTML .= '</div>';
			
			}
			
			$style = (!empty($export) || strstr($_SERVER['REQUEST_URI'], 'course/coursereportdetails.php'))?"style=''":'';
			
			$courseHTML .= "<div class='tabsOuter' ".$style." >";
			if(empty($export) && strstr($_SERVER['REQUEST_URI'], 'course/singlecoursereport.php')){
				
				$courseHTML .= "<div class='tabLinks'>";
				ob_start();
				$course->id = $cid;
				include_once('course_tabs.php');
				$HTMLTabs = ob_get_contents();
				ob_end_clean();
				$courseHTML .= $HTMLTabs;
				$courseHTML .= "</div>";
			}
			
			$courseHTML .= "<div class='borderBlockSpace'>";
			
			if(empty($export) && !strstr($_SERVER['REQUEST_URI'], 'course/singlecoursereport.php')){
				ob_start();
				require_once($CFG->dirroot . '/local/includes/course_report_details_search.php');
				$SEARCHHTML = ob_get_contents();
				ob_end_clean();
				$courseHTML .= $SEARCHHTML;
			}
				
			$courseHTML .= '<div class="clear"></div>';
			$courseHTML .= '<div class="userprofile view_assests">';
			
            $sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
			$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
			$sDateSelectedForPrintCSV = getDateFormatForPrint($sDateSelected, 'csv');
			$eDateSelectedForPrintCSV = getDateFormatForPrint($eDateSelected, 'csv');

			if(!empty($export)){
				$reportName = get_string('overalluserprogressreport','singlecoursereport');
				$courseHTML .= getReportPrintHeader($reportName);
				$courseHTML .= '<table border="0" cellpadding="0" cellspacing="0"  class="generaltable margin_bottom">';
					$courseHTML .= '<tr>';
						$courseHTML .= '<td colspan="2"><strong>'.get_string('course','singlecoursereport').':</strong> '.$courseName.'</td>';
					$courseHTML .= '</tr>';
					
					$courseHTML .= '<td width="50%"><strong>'.get_string('lastaccesseddatefrom').':</strong> '.$sDateSelectedForPrint.'</td>';
						$courseHTML .= '<td width="50%" ><strong>'.get_string('lastaccesseddateto').':</strong>  '.$eDateSelectedForPrint.'</td>';
					$courseHTML .= '</tr>';
				 $courseHTML .= '</table>';
			}
			if($userCount > 0){
			
				   $courseHTML .= ' <div class = "single-report-start" id="watch">
						<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('overalluserprogressreport','singlecoursereport').$exportHTML.'</span>
						  <div class="single-report-graph-left" id="chartContainer">'.get_string('loading','singlecoursereport').'</div>
						  <div class="single-report-graph-right">
							<div class="course-count-heading">'.get_string('numberofusers','singlecoursereport').'</div>
							<div class="course-count">'.$userCount.'</div>
							<div class="seperator">&nbsp;</div>
							<div class="course-status">
							  <div class="notstarted"><h6>'.get_string('notstarted','singlecoursereport').'</h6><span>'.$not_started_count.'</span></div>
							  <div class="clear"></div>
							  <div class="inprogress"><h6>'.get_string('inprogress','singlecoursereport').'</h6><span>'.$in_progress_count.'</span></div>
							  <div class="clear"></div>
							  <div class="completed"><h6>'.get_string('completed','singlecoursereport').'</h6><span>'.$completed_count.'</span></div>
							</div>
						  </div>
						</div>
					  </div>
					  <div class="clear"></div>';
	       } 
			
			
			
			
            $courseHTML .= '<div class="">';
			$courseHTML .= '<table  class="generaltable" cellpadding="0" border="0" cellspacing="0"></tbody>';
			$courseHTML .= '<tr>';
			
			if($CFG->showInlineManagerNCompany == 1 && !empty($export) && $export == 'print'){
			  $courseHTML .=  '<th width="16%">'.get_string('fullname','singlecoursereport').'</th>';
			  $courseHTML .=  '<th width="15%">'.get_string('username').'</th>';
			  $courseHTML .=  '<th width="13%">'.get_string('company').'</th>';
			  $courseHTML .=  '<th width="13%">'.get_string('inlinemanager').'</th>';
			  $courseHTML .=  '<th width="13%">'.get_string('status','singlecoursereport').'</th>';
			  //$courseHTML .=  '<th width="25%">'.get_string('score','singlecoursereport').'</th>';
			  $courseHTML .=  '<th width="15%">'.get_string('comletiondate').'</th>';
			  $courseHTML .=  '<th width="15%">'.get_string('lastaccessed','singlecoursereport').'</th>';
			}else{
			  $courseHTML .=  '<th width="23%">'.get_string('fullname','singlecoursereport').'</th>';
			  $courseHTML .=  '<th width="22%">'.get_string('username').'</th>';
			  $courseHTML .=  '<th width="15%">'.get_string('status','singlecoursereport').'</th>';
			  //$courseHTML .=  '<th width="25%">'.get_string('score','singlecoursereport').'</th>';
			  $courseHTML .=  '<th width="20%">'.get_string('comletiondate').'</th>';
			  $courseHTML .=  '<th width="20%">'.get_string('lastaccessed','singlecoursereport').'</th>';
			}
			
			$courseHTML .= '</tr>';
			
			

			$reportContentCSV = '';
			$reportContentCSV .= get_string('course','singlecoursereport').",".$courseName."\n";
			$reportContentCSV .= get_string('numberofusers','singlecoursereport').",".$userCount."\n";
			$reportContentCSV .= get_string('notstarted','singlecoursereport').",".$not_started_count." (".floatval($notStartedPercentage)."%)\n";
			$reportContentCSV .= get_string('inprogress','singlecoursereport').",".$in_progress_count." (".floatval($inProgressPercentage)."%)\n";
			$reportContentCSV .= get_string('completed','singlecoursereport').",".$completed_count." (".floatval($completedPercentage)."%)\n";
			
            $reportContentCSV .= get_string('lastaccesseddatefrom').",".$sDateSelectedForPrintCSV."\n".get_string('lastaccesseddateto').",".$eDateSelectedForPrintCSV."\n";
			$reportContentCSV .= get_string('overalluserprogressreport','singlecoursereport')."\n";
		   // $reportContentCSV .= get_string('fullname','singlecoursereport')." (".get_string('username')."),".get_string('status','singlecoursereport').",".get_string('score','singlecoursereport').",".get_string('lastaccessed','singlecoursereport')."\n";
						
			if($CFG->showInlineManagerNCompany == 1){
			   $reportContentCSV .= get_string('fullname','singlecoursereport').",".get_string('username').",".get_string('company').",".get_string('inlinemanager').",".get_string('status','singlecoursereport').",".get_string('comletiondate').",".get_string('lastaccessed','singlecoursereport')."\n";
			}else{
			   $reportContentCSV .= get_string('fullname','singlecoursereport').",".get_string('username').",".get_string('status','singlecoursereport').",".get_string('comletiondate').",".get_string('lastaccessed','singlecoursereport')."\n";
			}
			
			$reportContentPDF = '';
			$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0"  class="generaltable">';
				$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td><strong>'.get_string('course','singlecoursereport').':</strong> '.$courseName.'</td>';
					$reportContentPDF .= '<td><strong>'.get_string('numberofusers','singlecoursereport').':</strong> '.$userCount.'</td>';
				$reportContentPDF .= '</tr>';
				$reportContentPDF .= '<tr>';
				$reportContentPDF .= '<td><strong>'.get_string('notstarted','singlecoursereport').':</strong> '.$not_started_count.' ('.floatval($notStartedPercentage).'%)</td>';
				$reportContentPDF .= '<td><strong>'.get_string('inprogress','singlecoursereport').':</strong> '.$in_progress_count.' ('.floatval($inProgressPercentage).'%)</td>';
				$reportContentPDF .= '</tr>';
				
				$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td><strong>'.get_string('lastaccesseddatefrom').':</strong> '.$sDateSelectedForPrint.'</td>';
					$reportContentPDF .= '<td><strong>'.get_string('lastaccesseddateto').':</strong>  '.$eDateSelectedForPrint.'</td>';	
				$reportContentPDF .= '</tr>';
							
				$reportContentPDF .= '<tr>';
				$reportContentPDF .= '<td colspan="2"><strong>'.get_string('completed','singlecoursereport').':</strong> '.$completed_count.' ('.floatval($completedPercentage).'%)</td>';
				$reportContentPDF .= '</tr>';
				
				//$reportContentPDF .= '<tr><br /><td width="100%"  ><span '.$CFG->pdfSpanAttribute.'><strong>'.get_string('overalluserprogressreport','singlecoursereport').'</strong></span><br /></td></tr>';
			 $reportContentPDF .= '</table>';
			 
			 if(!empty($export) && $export == 'exportpdf' && strstr($_SERVER['REQUEST_URI'], 'course/singlecoursereportprint.php')){
			 	$reportContentPDF .= getGraphImageHTML(get_string('overalluserprogressreport','singlecoursereport'));
			 }else{
			 	$reportContentPDF .= getGraphImageHTML(get_string('viewreportdetails','multicoursereport'));
			 } 
			 
			 
			 
			 $reportContentPDF .= '<table '.$CFG->pdfTableStyle.'>';
			 $reportContentPDF .= '<tr '.$CFG->pdfTableHeaderStyle.' >';
			 $reportContentPDF .= '<td ><strong>'.get_string('fullname','singlecoursereport').'</strong></td>';
			 $reportContentPDF .= '<td ><strong>'.get_string('username').'</strong></td>';
			 if($CFG->showInlineManagerNCompany == 1){
			   $reportContentPDF .= '<td ><strong>'.get_string('company').'</strong></td>';
			   $reportContentPDF .= '<td ><strong>'.get_string('inlinemanager').'</strong></td>';
			 }
			 $reportContentPDF .= '<td><strong> '.get_string('status','singlecoursereport').'</strong></td>';
			// $reportContentPDF .= '<td><strong>'.get_string('score','singlecoursereport').'</strong></td>';
			 $reportContentPDF .= '<td><strong>'.get_string('comletiondate').'</strong></td>';
			 $reportContentPDF .= '<td><strong>'.get_string('lastaccessed','singlecoursereport').'</strong></td>';
			 $reportContentPDF .= '</tr>';
			 

            $htmlSpace = getSpace();
			$htmlSpaceCSV = getSpace('csv');
			//pr($limitReportsArr);die;
			
		    $extraParams = "startDate=".$sStartDate."&endDate=".$sEndDate;
			if($userCount > 0){
			
				$curtime = time();
		        $i=0;
				foreach($limitReportsArr as $data){
		  
					$i++;
					//$classEvenOddR = $i%2==0 ?'evenR':'oddR';
					$classEvenOddR = 'oddR';
					$courseStatus = $data->f_course_status?$CFG->courseStatusArr[$data->f_course_status]:'';
					if($CFG->showInlineManagerNCompany == 1){
						$inlineManager = $data->inline_manager_name?$data->inline_manager_name:(!empty($export) && $export == 'exportcsv'?getMDashForCSV():getMDash());
						$inlineCompany = $data->company?$data->company:(!empty($export) && $export == 'exportcsv'?getMDashForCSV():getMDash());
					}
					//$courseScore = '';
				    //$lastAccessed = getMDash();
					
					$completionDate = $data->completion_date;
					if($completionDate){
						$completionDateTime = getDateFormat($completionDate, $CFG->customDefaultDateTimeFormat);
						$completionDateTimeCSV = getDateFormat($completionDate, $CFG->customDefaultDateTimeFormatForCSV);
	
					}else{
						 $completionDateTime = getMDash();
						 $completionDateTimeCSV = getMDashForCSV();
					}
					
					$lastAccessedTime = $data->f_max_activity_lastaccessed;
					if($lastAccessedTime){
						$lastAccessed = getDateFormat($lastAccessedTime, $CFG->customDefaultDateTimeFormat);
						$lastAccessedCSV = getDateFormat($lastAccessedTime, $CFG->customDefaultDateTimeFormatForCSV);
	
					}else{
						 $lastAccessed = get_string('notaccessed','learnercourse');
						 $lastAccessedCSV = get_string('notaccessed','learnercourse');
					}
	
					$userId = $data->enrolled_user;
					$fullName = $data->userfullname;
					$userName = $data->username;
					//$imagevalue = $OUTPUT->user_picture($user, array('size'=>64));

					$courseHTML .=  '<tr class="'.$classEvenOddR.'" >';
					if(!empty($export)){
					  $courseHTML .=  '<td>'.$fullName.'</td>';
					  $courseHTML .=  '<td>'.$userName.'</td>';
					}else{
					  $userDetailsLink = $CFG->wwwroot."/user/multiuser_report_details.php?uid=".$userId."&cid=".$cid."&back=".$sBack.'&'.$extraParams;
					  $courseHTML .=  '<td><a href="'.$userDetailsLink.'">'.$fullName.'</a></td>';
					  $courseHTML .=  '<td><a href="'.$userDetailsLink.'" >'.$userName.'</a></td>';
					}
					
					if($CFG->showInlineManagerNCompany == 1 && !empty($export) && $export == 'print' ){
					  $courseHTML .=  '<td>'.$inlineCompany.'</td>';
					  $courseHTML .=  '<td>'.$inlineManager.'</td>';
					}
					$courseHTML .=  '<td>'.$courseStatus.'</td>';
					//$courseHTML .=  '<td>'.($courseScore?$courseScore:getMDash()).'</td>';
					$courseHTML .=  '<td>'.$completionDateTime.'</td>';
					$courseHTML .=  '<td>'.$lastAccessed.'</td>';
					$courseHTML .=  '</tr>';

					//$courseHTML .=  '<tr><td colspan="5">';
					
					//$reportContentCSV .= $fullName." (".$userName."),".$courseStatus.",".($courseScore?$courseScore:getMDashForCSV()).",".(getMDashForCSV())."\n";
					
					if($CFG->showInlineManagerNCompany == 1){
					  $reportContentCSV .= $fullName.",".$userName.",".$inlineCompany.",".$inlineManager.",".$courseStatus.",".$completionDateTimeCSV.",".$lastAccessedCSV."\n";
					}else{
					  $reportContentCSV .= $fullName.",".$userName.",".$courseStatus.",".$completionDateTimeCSV.",".$lastAccessedCSV."\n"; 
					}
					$reportContentPDF .= '<tr '.$CFG->pdfTableRowAttribute.' >';
					$reportContentPDF .= '<td>'.$fullName.'</td>';
					$reportContentPDF .= '<td>'.$userName.'</td>';
					if($CFG->showInlineManagerNCompany == 1){ 
					   $reportContentPDF .=  '<td>'.$inlineCompany.'</td>';
					   $reportContentPDF .=  '<td>'.$inlineManager.'</td>';
					}
					$reportContentPDF .= '<td>'.$courseStatus.'</td>';
					//$reportContentPDF .= '<td>'.($courseScore?$courseScore:getMDash()).'</td>';
					$reportContentPDF .= '<td>'.$completionDateTime.'</td>';
					$reportContentPDF .= '<td>'.$lastAccessed.'</td>';
					$reportContentPDF .= '</tr>';
					   
					$assestHTML = '';
					$courseHTML .=  $assestHTML;
				    // $courseHTML .= '</td></tr>';

				}
	
	
	           //if($notStartedPercentage || $inProgressPercentage || $completedPercentage){
				$courseHTML .=  '<script language="javascript" type="text/javascript">';
				$courseHTML .=  '	$(document).ready(function(){
								
								$("#printbun").bind("click", function(e) {
									var url = $(this).attr("rel");
									window.open(url, "'.get_string('coursevsmultipleuserreport','singlecoursereport').'", "'.$CFG->printWindowParameter.'");
								});
								
						     }); ';

                
				$courseHTML .=  ' window.onload = function () {
					
										var chart = new CanvasJS.Chart("chartContainer",
										{
											title:{
												text: ""
											},
											theme: "theme2",
						                   
											data: [
											{        
												type: "doughnut",
												indexLabelFontFamily: "Arial",       
												indexLabelFontSize: 12,
												startAngle:0,
												indexLabelFontColor: "dimgrey",       
												indexLabelLineColor: "darkgrey", 
												toolTipContent: "{y}%", 					
									
																					
												dataPoints: [
												{  y: '.$notStartedPercentage.', label: "'.get_string('notstarted','singlereport').' ('.$not_started_count.')", exploded:"'.$not_started_count_exploded.'" },
												{  y: '.$inProgressPercentage.', label: "'.get_string('inprogress','singlereport').' ('.$in_progress_count.')", exploded:"'.$in_progress_count_exploded.'" },
												{  y: '.$completedPercentage.', label: "'.get_string('completed','singlereport').' ('.$completed_count.')", exploded:"'.$completed_count_exploded.'" },
									
												]
												
												/*dataPoints: [
												{  y: '.$notStartedPercentage.', label: "'.get_string('notstarted','singlereport').' {y}%" },
												{  y: '.$inProgressPercentage.', label: "'.get_string('inprogress','singlereport').' {y}%" },
												{  y: '.$completedPercentage.', label: "'.get_string('completed','singlereport').' {y}%" },
									
												]*/
											}
											]
											
										});';
					
				if($notStartedPercentage || $inProgressPercentage || $completedPercentage){					
				  $courseHTML .=  '	chart.render(); ';
				}
										 
				$courseHTML .=  '  }
									</script>
									<script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js"></script>';
									
				//}					
				
			}else{
				  $courseHTML .=  '<tr><td colspan="5" align="center" >'.get_string('no_results').'</td></tr>';
				  $reportContentCSV .= get_string('norecordfound','singlecoursereport')."\n";
				  $reportContentPDF .= get_string('norecordfound','singlecoursereport')."\n";
			}
			
			$reportContentPDF .= '</table>';
			
			$courseHTML .= '</tbody></table></div></div></div></div>';
	
            if(empty($export)){ 
			
				 $courseHTML .= paging_bar($userCount, $page, $perpage, $genURL);
				 $styleSheet = $userCount?'':'style=""';
				 if(strstr($_SERVER['REQUEST_URI'], 'course/coursereportdetails.php')){
				 
				  if($sBack == 1){
				     $backUrl = $CFG->wwwroot.'/course/course_report.php';
				  }else{
				     $backUrl = $CFG->wwwroot.'/course/multicoursereport.php?'.$extraParams;
				  } 
				 
				  $courseHTML .= '<div id="backcell"><input type = "button" value = "'.get_string('back','multicoursereport').'" '.$styleSheet.' onclick="location.href=\''.$backUrl.'\';"></div>';
				 }else{
				  $urlCancel = new moodle_url($CFG->wwwroot.'/course/courseview.php', array('id'=>$cid));
				  $courseHTML .= '<div id="backcell"><input type = "button" value = "'.get_string('cancel','singlecoursereport').'" '.$styleSheet.' onclick="location.href=\''.$urlCancel.'\';"></div>';
				}
			}
			
			$courseHTML .= '';
			$courseHTML .=  '';
				
			$courseVsMultipleUserReport->courseHTML = $courseHTML;
			$courseVsMultipleUserReport->reportContentCSV = $reportContentCSV;
			$courseVsMultipleUserReport->reportContentPDF = $reportContentPDF;
			
			return $courseVsMultipleUserReport;
	  }
	  
	
	  
	  /**
	 * This function is using for getting a Course Report
	 * @global object
	 * @param array $paramArray is search array
	 * @param array $removeKeyArray contain search key that you don't want to add in address bar
	 * @param string $sort is sorting field
	 * @param string $dir is sorting order
	 * @param int $page current page
	 * @param int $perpage is number of records per page
	 * @param string $export is action variable for PDF download, CSV download, and Print Course Report
	 * @return array $courseReport return CSV , PDF and Report Content
	 */
	
	
	function getCourseReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export=''){
	  
	        //ini_set('display_errors', 1);
  	        //error_reporting(E_ALL);
		
	        global $DB, $CFG, $USER;
			$courseReport = new stdClass();
			$isReport = true;
			$offset = $page - 1;
			$offset = $offset*$perpage;
			$limit = '';
			if($perpage != 0){
				$limit = "LIMIT $offset, $perpage";
			}


			////// Getting common URL for the Search //////
			$pageURL = '/course/'.$CFG->pageCourseReport;
			$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
			
			$printUrl = '/course/'.$CFG->pageCourseReportPrint;
			$genPrintURL = genParameterizedURL($paramArray, $removeKeyArray, $printUrl);

			$sCategory      = $paramArray['category'];
			$sActive       = $paramArray['active'];
			$sStartDate     = $paramArray['startDate'];
			$sEndDate       = $paramArray['endDate'];

			
			$sDateSelected = '';
			if($sStartDate){
				$sDateArr = getFormattedTimeStampOfDate($sStartDate);
			    $sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

			}		   
			
			$eDateSelected = '';
			if($sEndDate){
			   $eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
			   $eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
			}

			$sCategoryArr = explode("@",$sCategory);
			$sActiveArr = explode("@",$sActive);
			
			$sCategoryName = $sCategory=='-1'?(get_string('all','multicoursereport')):getCourseCategory($sCategoryArr);

			
			
			if($sActive == '-1'){
			  $sActiveName = get_string('all','multicoursereport');
			}else{
			
			  
			  $repActiveArr = array(0 => get_string('statusdeactive','multicoursereport'), 1 => get_string('statusactive','multicoursereport'), 2 => get_string('statusunpublished','multicoursereport'));
			  $sActiveArr2 = str_replace(array(0,1,2), $repActiveArr, $sActiveArr); 
			  $sActiveName = count($sActiveArr2) > 0 ? implode(", ", $sActiveArr2) : 0;
			}
			
			// search condition start here
			
			$searchString = "";
			$subSearchString = "";

			if( $USER->archetype == $CFG->userTypeManager) {
			
			    $managerCourseIdArr = getManagerCourse($isReport);
				$groupCourses = fetchGroupsCourseIds(0,0,1);
				$managerCourseIdArr = array_merge($managerCourseIdArr, $groupCourses);
				$managerCourseIds = count($managerCourseIdArr) > 0 ? implode(",", $managerCourseIdArr) : 0 ;
				$searchString .= ' AND mc.id IN ('.$managerCourseIds.') ';
				$subSearchString .= ' AND msc.id IN ('.$managerCourseIds.') ';
				
			   
			}else{
				if( $USER->archetype == $CFG->userTypeStudent) {
					$groupCourses = fetchGroupsCourseIds(0,0,1);
					$managerCourseIds = count($groupCourses) > 0 ? implode(",", $groupCourses) : 0 ;
					$searchString .= ' AND mc.id IN ('.$managerCourseIds.') ';
					$subSearchString .= ' AND msc.id IN ('.$managerCourseIds.') ';
				}
			}
			
			if(count($sCategoryArr) > 0 && !in_array($sCategoryArr[0] , array(0, '-1'))){
				$sCategoryStr = implode(",", $sCategoryArr);
				$searchString .= " AND mc.category IN ($sCategoryStr) ";
				$subSearchString .= " AND msc.category IN ($sCategoryStr) ";
			}
			
			
			
			$sActiveArrr = $sActiveArr;
			$searchStatusString = '';
			if(count($sActiveArrr) > 0 &&  !in_array($sActiveArrr[0] , array('-1'))){
                 
				 
				 if(count($sActiveArrr) == 3){
				 
				 }else{
				 
					 if(in_array(2, $sActiveArrr)){ 
					    foreach($sActiveArrr as $kk=>$sActiveVal){
						  if($sActiveVal == 2){ 
						    unset($sActiveArrr[$kk]);
						  }
						}  
						$searchStatusString .= " AND mc.publish IN (0) ";
							
						if(count($sActiveArrr) > 0 ){
							$sActiveStr = implode(",", $sActiveArrr);
							$searchStatusString .= "  AND mc.is_active IN ($sActiveStr)";
						}
					 }else{
						 $sActiveStr = implode(",", $sActiveArrr);
						 $searchStatusString .= " AND mc.publish IN (1) AND mc.is_active IN ($sActiveStr)";
					 }
				 
				 }
				 
					
			}
			

			if($sDateSelected){ 
				$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
				$searchString .= " AND mc.timecreated >= ($sStartDateTime) ";
				$subSearchString .= " AND msc.timecreated >= ($sStartDateTime) ";
			}
			
			if($eDateSelected){
			   $sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
			   $searchString .= " AND mc.timecreated <= ($sEndDateTime) ";
			   $subSearchString .= " AND msc.timecreated <= ($sEndDateTime) ";
			}
			
			// search condition end here
            $fullnameField = extractMDashInSql('mc.fullname');	
			$query = "select  mc.id, $fullnameField as fullname, mc.summary, mc.timecreated ";
			$query .= ",mcc.name as category";
			$query .= ",concat(mu_created.firstname, ' ', mu_created.lastname) as createdbyname, mu_created.username as createdby_username";
            $query .= ",mc.publish, mc.is_active";
			$query .= ", (SELECT count(msc.id) FROM {$CFG->prefix}course msc WHERE msc.is_active = '1' AND msc.publish = '1'  AND msc.deleted = '0' AND msc.coursetype_id = '".$CFG->courseTypeOnline."' AND msc.id!='1'  $subSearchString) AS publish_and_active";
			$query .= ", (SELECT count(msc.id) FROM {$CFG->prefix}course msc WHERE msc.is_active = '0' AND msc.publish = '1'  AND msc.deleted = '0' AND msc.coursetype_id = '".$CFG->courseTypeOnline."' AND msc.id!='1'  $subSearchString) AS publish_and_deactive";
			$query .= ", (SELECT count(msc.id) FROM {$CFG->prefix}course msc WHERE msc.publish = '0'  AND msc.deleted = '0' AND msc.id!='1' AND msc.coursetype_id = '".$CFG->courseTypeOnline."' $subSearchString) AS unpublished";
			$query .= " FROM {$CFG->prefix}course mc  ";
			$query .= " LEFT JOIN {$CFG->prefix}course_categories mcc ON (mc.category = mcc.id)  ";
			$query .= " LEFT JOIN {$CFG->prefix}user mu_created ON (mu_created.id = mc.createdby) ";
			$query .= " WHERE 1 = 1  ";
			$query .= " AND mc.deleted = '0'  ";	
			$query .= "   AND mc.coursetype_id = '".$CFG->courseTypeOnline."'";
	
			$query .= "   AND mc.id != '1' ";
			$query .=    $searchString;
			$queryCount = $query."  GROUP BY mc.id ";
			$query .=    $searchStatusString;
			$query .= "  GROUP BY mc.id ";
			$query .= " ORDER BY $sort ";
			
				
			$queryLimit = $query.$limit;

			$courseAllArr = $DB->get_records_sql($query);	
			$courseCount = count($courseAllArr);
			$courseArr = $DB->get_records_sql($queryLimit);	

			
			$courseCountRes = $DB->get_records_sql($queryCount);	
			$courseCountAll = count($courseCountRes);
			
			$courseHTML = '';
			$style = !empty($export)?"style=''":'';
			$courseHTML .= "<div class='tabsOuter' ".$style."><div class='borderBlockSpace'>";
			$courseHTML .= '<div class="clear"></div>';
            $exportHTML = '';
            if(empty($export)){
			
				ob_start();
				require_once($CFG->dirroot . '/local/includes/coursereportsearch.php');
				$SEARCHHTML = ob_get_contents();
				ob_end_clean();
				
				$courseHTML .= $SEARCHHTML;

				$exportHTML .= '<div class="exports_opt_box" > ';
				$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','multicoursereport').'" href="'.$genPrintURL.'&action=exportcsv">'.get_string('downloadcsv','multicoursereport').'</a> ';
				$exportHTML .= '<span class="seperater">&nbsp;</span>';
				$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','multicoursereport').'" id="exportpdf1" href="'.$genPrintURL.'&action=exportpdf">'.get_string('downloadpdf','multicoursereport').'</a> ';
				$exportHTML .= '<span class="seperater">&nbsp;</span>';
				$exportHTML .= '<a rel="'.$genPrintURL.'&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','multicoursereport').'" class="print_icon">'.get_string('print','multicoursereport').'</a>';
				$exportHTML .= '</div>';
			
			}
			
			$courseHTML .= '<div class="userprofile view_assests">';
			
			$publishAndActive = 0;
			$publishAndDeactive = 0;
			$unpublishAndDeactive = 0;
			$unpublishAndActive = 0;
            $publishAndActiveExploded = false;
			$publishAndDeactiveExploded = false;
			$unpublishedExploded = false;

			$reportContentPDF = '';
			if($courseCount > 0 ){

				foreach($courseAllArr as $courses){

					   $publishAndActive = $courses->publish_and_active;
					   $publishAndDeactive = $courses->publish_and_deactive;
					   $unpublished = $courses->unpublished;
					   break;
				}
				
		
				if(in_array(1, $sActiveArr)){
				   $publishAndActiveExploded = true;
				}
				
				if(in_array(0, $sActiveArr)){
				   $publishAndDeactiveExploded = true;
				}
				
				if(in_array(2, $sActiveArr)){
				   $unpublishedExploded = true;
				}
					
				$publishAndActivePercentage = numberFormat(($publishAndActive*100)/$courseCountAll);
				$publishAndDeactivePercentage = numberFormat(($publishAndDeactive*100)/$courseCountAll);
				$unpublishedPercentage = numberFormat(($unpublished*100)/$courseCountAll);
				
			    
				$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
				$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
				$sDateSelectedForPrintCSV = getDateFormatForPrint($sDateSelected, 'csv');
				$eDateSelectedForPrintCSV = getDateFormatForPrint($eDateSelected, 'csv');
				
			    if(!empty($export) && $export == 'print'){
			    	
			    	    $reportName = get_string('coursereport','multicoursereport');
			    	    $courseHTML .= getReportPrintHeader($reportName);
						$courseHTML .= '<table border="0" cellpadding="0" cellspacing="0"  class="generaltable margin_bottom">';
							$courseHTML .= '<tr>';
												$courseHTML .= '<td width="500"><strong>'.get_string('category','multicoursereport').':</strong> '.$sCategoryName.'</td>';
												$courseHTML .= '<td width="500"><strong>'.get_string('status','multicoursereport').':</strong>  '.$sActiveName.'</td>';
							$courseHTML .= '</tr>';
							$courseHTML .= '<tr>';
												$courseHTML .= '<td><strong>'.get_string('daterangefrom','multicoursereport').':</strong> '.$sDateSelectedForPrint.'</td>';
												$courseHTML .= '<td><strong>'.get_string('daterangeto','multicoursereport').':</strong>  '.$eDateSelectedForPrint.'</td>';		
							$courseHTML .= '</tr>';
						$courseHTML .= '</table>';
						
			    }
							
			    $courseHTML .= '<div class = "single-report-start" id="watch"   >
								<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('coursereport','multicoursereport').$exportHTML.'</span>
								  <div class="single-report-graph-left" id="chartContainer">'.get_string('loading','multicoursereport').'</div>
								  <div class="single-report-graph-right">
									<div class="course-count-heading">'.get_string('numberofcourses','multicoursereport').'</div>
									<div class="course-count">'.$courseCountAll.'</div>
									<div class="seperator">&nbsp;</div>
									<div class="course-status">
									  <div class="publish-active"><h6>'.get_string('statusactive','multicoursereport').'</h6><span>'.$publishAndActive.'</span></div>
									  <div class="clear"></div>
									  <div class="publish-deactive"><h6>'.get_string('statusdeactive','multicoursereport').'</h6><span>'.$publishAndDeactive.'</span></div>
									  <div class="clear"></div>
									  <div class="unpublish-deactive"><h6>'.get_string('statusunpublished','multicoursereport').'</h6><span>'.$unpublished.'</span></div>
									</div>
								  </div>
								</div>
							  </div>
							  <div class="clear"></div>';
							  			  
			} 

			$courseHTML .= '<div class=""><div class=""><table cellspacing="0" cellpadding="0" border="0" class="generaltable"><tbody>'; 
			$courseHTML .= '<tr>';
			$courseHTML .=  '<th width="30%">'.get_string('coursetitle','multicoursereport').'</th>';
			$courseHTML .=  '<th width="15%">'.get_string('category','multicoursereport').'</th>';
			$courseHTML .=  '<th width="25%">'.get_string('dateofcreation','multicoursereport').'</th>';
			$courseHTML .=  '<th width="15%">'.get_string('status','multicoursereport').'</th>';
			$courseHTML .=  '<th width="15%">'.get_string('createdby','multicoursereport').'</th>';
			$courseHTML .=  '</tr>';
			$reportContentCSV .= get_string('category','multicoursereport').",".$sCategoryName."\n".get_string('status','multicoursereport').",".$sActiveName."\n";
			$reportContentCSV .= get_string('daterangefrom','multicoursereport').",".$sDateSelectedForPrintCSV."\n".get_string('daterangeto','multicoursereport').",".$eDateSelectedForPrintCSV."\n";
			$reportContentCSV .= get_string('numberofcourses','multicoursereport').",".$courseCountAll."\n".get_string('statusactive','multicoursereport').",".$publishAndActive." (".floatval($publishAndActivePercentage)."%)\n";
			$reportContentCSV .= get_string('statusdeactive','multicoursereport').",".$publishAndDeactive." (".floatval($publishAndDeactivePercentage)."%)\n".get_string('statusunpublished','multicoursereport').",".$unpublished." (".floatval($unpublishedPercentage)."%)\n";
			$reportContentCSV .= get_string('coursereport','multicoursereport')."\n";
		    $reportContentCSV .= get_string('coursetitle','multicoursereport').",".get_string('category','multicoursereport').",".get_string('dateofcreation','multicoursereport').",".get_string('status','multicoursereport').",".get_string('createdby','multicoursereport')."\n";
			$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" class="generaltable">';
				$reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td width="50%" ><strong>'.get_string('category','multicoursereport').':</strong> '.$sCategoryName.'</td>';
						$reportContentPDF .= '<td width="50%" ><strong>'.get_string('status','multicoursereport').':</strong>  '.$sActiveName.'</td>';
				$reportContentPDF .= '</tr>';
				$reportContentPDF .= '<tr>';
					    $reportContentPDF .= '<td width="50%"  ><strong>'.get_string('daterangefrom','multicoursereport').':</strong> '.$sDateSelectedForPrint.'</td>';
					    $reportContentPDF .= '<td width="50%" ><strong>'.get_string('daterangeto','multicoursereport').':</strong>  '.$eDateSelectedForPrint.'</td>';	
				$reportContentPDF .= '</tr>';
				$reportContentPDF .= '<tr>';
					    $reportContentPDF .= '<td width="50%"  ><strong>'.get_string('numberofcourses','multicoursereport').':</strong> '.$courseCountAll.'</td>';
					    $reportContentPDF .= '<td width="50%" ><strong>'.get_string('statusactive','multicoursereport').':</strong>  '.$publishAndActive.' ('.floatval($publishAndActivePercentage).'%)</td>';	
				$reportContentPDF .= '</tr>';
				$reportContentPDF .= '<tr>';
					    $reportContentPDF .= '<td width="50%"  ><strong>'.get_string('statusdeactive','multicoursereport').':</strong> '.$publishAndDeactive.' ('.floatval($publishAndDeactivePercentage).'%)</td>';
					    $reportContentPDF .= '<td width="50%" ><strong>'.get_string('statusunpublished','multicoursereport').':</strong>  '.$unpublished.' ('.floatval($unpublishedPercentage).'%)</td>';	
				$reportContentPDF .= '</tr>';
			 $reportContentPDF .= '</table>';
			 $reportContentPDF .= getGraphImageHTML(get_string('coursereport','multicoursereport')); 
			 $reportContentPDF .= '<table '.$CFG->pdfTableStyle.' >';
			 $reportContentPDF .= '<tr '.$CFG->pdfTableHeaderStyle.'>';
		     $reportContentPDF .= '<td  width="30%" ><strong>'.get_string('coursetitle','multicoursereport').'</strong></td>';
			 $reportContentPDF .= '<td  width="15%"><strong> '.get_string('category','multicoursereport').'</strong></td>';
			 $reportContentPDF .= '<td  width="25%" ><strong>'.get_string('dateofcreation','multicoursereport').'</strong></td>';
			 $reportContentPDF .= '<td  width="15%"><strong>'.get_string('status','multicoursereport').'</strong></td>';
			 $reportContentPDF .= '<td  width="15%" ><strong>'.get_string('createdby','multicoursereport').'</strong></td>';
			 $reportContentPDF .= '</tr>';
			if(count($courseArr) > 0 ){
			  $i = 0;
			  $format = $CFG->customDefaultDateFormat;
			  $formatCsv = $CFG->customDefaultDateFormatForCSV;
			  foreach($courseArr as $courses){
					$courseId = $courses->id;
					$courseName = $courses->fullname;
					$summary = $courses->summary;
					$category = $courses->category;
					$timeCreated = $courses->timecreated;
					$timeCreatedFormat = $timeCreated?getDateFormat($timeCreated, $format):getMDash();
					$timeCreatedFormatCSV = $timeCreated?getDateFormat($timeCreated, $formatCsv):getMDashForCSV();
					$courseImg = getCourseImage($course);
					$is_active = $courses->is_active;
					$publish = $courses->publish;
					$complianceDiv = getCourseComplianceIcon($courseId);

					if($publish == 1){
					
					   if($is_active == 1){
					     $cStatus = get_string('statusactive', 'multicoursereport');
					   }elseif($is_active == 0){
					     $cStatus = get_string('statusdeactive', 'multicoursereport');
					   }

					}elseif($publish == 0){
					  $cStatus = get_string('statusunpublished', 'multicoursereport');
					}
					
					$createdBy = $courses->createdbyname;
					$courseHTML .=  '<tr>';
					if(empty($export)){
					  $courseHTML .=  '<td><a href="'.$CFG->wwwroot.'/course/'.$CFG->pageCourseReportDetails.'?cid='.$courseId.'&back=1" >'.$courseName.$complianceDiv.'</a></td>';
					}else{
					   $courseHTML .=  '<td>'.$courseName.$complianceDiv.'</td>';
					}
					$courseHTML .=  '<td>'.$category.'</td>';
					$courseHTML .=  '<td>'.$timeCreatedFormat.'</td>';
					$courseHTML .=  '<td>'.$cStatus.'</td>';
					$courseHTML .=  '<td>'.$createdBy.'</td>';
					$courseHTML .=  '</tr>';
					$reportContentCSV .= $courseName.",".$category.",".$timeCreatedFormatCSV.",".$cStatus.",".$createdBy."\n";
					$reportContentPDF .= '<tr '.$CFG->pdfTableRowAttribute.' >';
					$reportContentPDF .= '<td>'.$courseName.'</td>';
					$reportContentPDF .= '<td >'.$category.'</td>';
					$reportContentPDF .= '<td >'.$timeCreatedFormat.'</td>';
					$reportContentPDF .= '<td >'.$cStatus.'</td>';
					$reportContentPDF .= '<td >'.$createdBy.'</td>';
					$reportContentPDF .= '</tr>';
					$i++;
				}
				
				$courseHTML .=  '<script language="javascript" type="text/javascript">';
				$courseHTML .=  '	$(document).ready(function(){
								
												$("#printbun").bind("click", function(e) {
													var url = $(this).attr("rel");
													window.open(url, "'.get_string('coursereport','multicoursereport').'", "'.$CFG->printWindowParameter.'");
												});
								
						             }); ';
                
				$courseHTML .=  ' window.onload = function () {
					
										var chart = new CanvasJS.Chart("chartContainer",
										{
											title:{
												text: ""
											},
											theme: "theme2",
											data: [
											{        
												type: "doughnut",
												indexLabelFontFamily: "Arial",       
												indexLabelFontSize: 12,
												startAngle:0,
												indexLabelFontColor: "dimgrey",       
												indexLabelLineColor: "darkgrey", 
												toolTipContent: "{y}%", 					
	
					 
												dataPoints: [
												{  y: '.$publishAndActivePercentage.', label: "'.get_string('statusactive','multicoursereport').' ('.$publishAndActive.')", exploded : "'.$publishAndActiveExploded.'"  },
												{  y: '.$publishAndDeactivePercentage.', label: "'.get_string('statusdeactive','multicoursereport').' ('.$publishAndDeactive.')", exploded : "'.$publishAndDeactiveExploded.'"  },
												{  y: '.$unpublishedPercentage.', label: "'.get_string('statusunpublished','multicoursereport').' ('.$unpublished.')", exploded : "'.$unpublishedExploded.'"  },
									
												]
			
											}
											]
										}); ';
					
				if(count($courseArr) > 0){					
				  $courseHTML .=  '	chart.render(); ';
				}
										 
				$courseHTML .=  '  }
									  
									  
									</script>
									
									<script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js"></script>';
				
				
				
			}else{
			      $courseHTML .=  '<tr><td colspan="6" style="border-bottom:1px solid #b9b9b9!important;" class="no_record" >'.get_string('norecordfound','singlecoursereport').'</td></tr>';
			      $reportContentCSV .= get_string('norecordfound','multicoursereport')."\n";
			      $reportContentPDF .= get_string('norecordfound','multicoursereport')."\n";
			}
			
			$reportContentPDF .= '</table>';
			
			$courseHTML .= '</tbody></table></div></div></div></div></div>';

	
            if(empty($export)){ 
				$courseHTML .= paging_bar($courseCount, $page, $perpage, $genURL);
		    }
			$courseHTML .= '';
			
			$courseReport->courseHTML = $courseHTML;
			$courseReport->reportContentCSV = $reportContentCSV;
			$courseReport->reportContentPDF = $reportContentPDF;
			
			return $courseReport;

    }
	
	
    
	########################################### Start Program Functions ##########################
	
	/**
	 * This function is using for getting the course id for a particular manager
	 * @global object
	 * @param string $extraSql is extra condition for course table query
	 * @return array of course ids
	 */
	 
	function getManagerCourse($isReport = false){
	
	  global $CFG, $DB, $USER;
      $managerCourseIds = array();
	  
	  if( $USER->archetype == $CFG->userTypeManager || (isset($USER->original_archetype) && $USER->original_archetype == $CFG->userTypeManager)) {
	  
			$query = '';
			$query .= " SELECT DISTINCT mcc.id FROM {$CFG->prefix}course mcc";
			$query .= " WHERE 1 = 1 ";
			$query .= " AND (mcc.id IN(SELECT dc.courseid
													FROM {$CFG->prefix}department_course AS dc
													LEFT JOIN {$CFG->prefix}department AS d ON d.id = dc.departmentid
													LEFT JOIN {$CFG->prefix}department_members as dm ON dm.departmentid = d.id
													WHERE dm.userid = '".$USER->id."' AND d.deleted = '0' ";
			if(!$isReport){										
			 $query .= " AND dc.is_active = 1 ";
			}
			$query .= " )) ";
			
			if(!$isReport){
			  $query .= " AND mcc.is_active = '1' ";
			}
			$query .= "AND mcc.id NOT IN (1)";
			$query .= " AND mcc.deleted = '0' ";
			//$query .= $extraSql;
			
			$managerCourseIdArr = $DB->get_records_sql($query);
			if(count($managerCourseIdArr) > 0 ){
			  $managerCourseIds = array_keys($managerCourseIdArr);
			}

	   }
	 
	   return $managerCourseIds;
	}
	
	/**
	 * This function is using for getting the course id for a particular department
	 * @global object
	 * @param string $extraSql is extra condition for course table query
	 * @return array of course ids
	 */
	 
	function getDepartmentCourse($depId, $isReport = false){
	
	  global $CFG, $DB, $USER;
      $depCourseIds = array();
	  
	  if($depId){
		$query = '';
		$query .= " SELECT DISTINCT mcc.id FROM {$CFG->prefix}course mcc";
		$query .= " WHERE 1 = 1 ";
		$query .= " AND (mcc.id IN(SELECT dc.courseid
												FROM {$CFG->prefix}department_course AS dc
												LEFT JOIN {$CFG->prefix}department AS d ON d.id = dc.departmentid
												WHERE d.id = '".$depId."' AND d.deleted = '0' AND dc.is_active = '1'";
	
		$query .= " )) ";
		
		if(!$isReport){
		  $query .= " AND mcc.is_active = '1' ";
		}
		$query .= "AND mcc.id NOT IN (1)";
		$query .= " AND mcc.deleted = '0' ";
		//$query .= $extraSql;
		//echo $query;die;
		$depCourseIdArr = $DB->get_records_sql($query);
		if(count($depCourseIdArr) > 0 ){
		  $depCourseIds = array_keys($depCourseIdArr);
		}

	   }
	   return $depCourseIds;
	}
	
	/**
	 * This function is using for getting the program id for a particular manager
	 * @global object
	 * @param string $extraSql is extra condition for program table query
	 * @return array of program ids
	 */
	 
	function getManagerProgram( $return = 1, $isReport = false){
	
	  global $CFG, $DB, $USER;
      $managerProgramIds = array();
	  $curtime = time();
	  if( $USER->archetype == $CFG->userTypeManager) {
	  
			$query = '';
			if($return == 3){
			  $query .= " SELECT DISTINCT * FROM {$CFG->prefix}programs mpp";
			}else{
			  $query .= " SELECT DISTINCT mpp.id FROM {$CFG->prefix}programs mpp";
			}
			$query .= " WHERE 1 = 1 ";
			/*$query .= " AND (mpp.id IN(SELECT mpd.program_id
													FROM {$CFG->prefix}program_department AS mpd
													LEFT JOIN {$CFG->prefix}department AS d ON d.id = mpd.department_id
													LEFT JOIN {$CFG->prefix}department_members as dm ON dm.departmentid = d.id
													WHERE dm.userid = '".$USER->id."' AND mpd.is_active = 1) OR mpp.createdby = '".$USER->id."') ";*/
			$query .= " AND (mpp.id IN(SELECT mpd.program_id
													FROM {$CFG->prefix}program_department AS mpd
													LEFT JOIN {$CFG->prefix}department AS d ON d.id = mpd.department_id
													LEFT JOIN {$CFG->prefix}department_members as dm ON dm.departmentid = d.id
			
													WHERE dm.userid = '".$USER->id."' AND d.deleted = '0' ";
			if(!$isReport){	
			  $query .= " AND mpd.is_active = 1";
			}  
			$query .= " )) ";
			
			$query .= " AND mpp.deleted = '0' ";
													
			if(!$isReport){										
			  $query .= " AND mpp.status = '1' ";
			}
			//$query .= " AND mpp.publish = '1' ";
			//$query .= " AND mpp.archive = '0' ";
			//$query .= " AND mpp.startdate >= $curtime ";
			//$query .= " AND mpp.enddate <= $curtime ";
			
			$managerProgramIdArr = $DB->get_records_sql($query);
			
			if($return == 1 ){
			  if(count($managerProgramIdArr) > 0 ){
			    $managerProgramIds = array_keys($managerProgramIdArr);
			  }
			}elseif($return == 2 ){
			  return $managerProgramIdArr;
			}elseif($return == 3 ){
			  return $managerProgramIdArr;
			}
			

	   }
	 
	   return $managerProgramIds;
	}
	

	/**
	 * This function is using for getting the user id for a particular manager
	 * @global object
	 * @param string $extraSql is extra condition for user table query
	 * @return array of user ids
	 */
	 
	function getManagerUser($extraSql = ''){
	
	  global $CFG, $DB, $USER;
      $managerUserIds = array();
	  $managerDefaultString = '';
	  if( $USER->archetype == $CFG->userTypeManager || $USER->archetype == $CFG->userTypeStudent) {
	  
	      $userArr = get_users_listing($sort='lastaccess', $dir='ASC', $page=1, $recordsperpage=0,
                           $search='', $firstinitial='', $lastinitial='', $extraselect='',
                            $paramArray=array('is_active'=>'-1','is_report'=>1), $extracontext = '');
		  $managerUserIds = array_keys($userArr);					
	  
	   }
	   
	   return $managerUserIds;
	}
	
	function getAssignedUsersOfProgramAndViceVersa($typeId, $type = ''){
	   
	    global $DB, $CFG, $USER; 
	    $userIds = array();
		$assignedUsersOfProgram = array();
		$assignedProgramsOfUser = array();
		$assignedData = array();
		
		$query = "select * FROM {$CFG->prefix}programs  WHERE deleted='0' AND status = '1' ORDER BY name ASC"; 
		$result = $DB->get_records_sql($query);
		foreach($result as $arr){
		
			$pgid = $arr->id;
            $query = ''; 
			
			$query .= " SELECT mpam.typeid FROM {$CFG->prefix}program_assignment_mapping mpam WHERE mpam.program_id	= '".$pgid."' AND mpam.status = '1' AND mpam.type = 'user'";
		
			$userIdArr = $DB->get_records_sql($query);
			$userIdArr = array_keys($userIdArr);
			sort($userIdArr);
			$userIdArr = array_unique($userIdArr);
					
			if(count($userIdArr ) > 0 ){
				foreach($userIdArr  as $userId){
				  $programUsers[$pgid][] = $userId;
				}
			}
			
			if(count($userIdArr ) > 0 ){
				foreach($userIdArr  as $userId){
				  $userPrograms[$userId][] = $pgid;
				}
			}
			
		}	
			
		
		if($typeId){
		
		    if($type == 'program' && isset($programUsers[$typeId]) && count($programUsers[$typeId]) > 0 ){
			   return $assignedUsersOfProgram = $programUsers[$typeId];
			}else if($type == 'user' && isset($programUsers[$typeId]) && count($programUsers[$typeId]) > 0 ){
			   return $assignedProgramsOfUser = $userPrograms[$typeId];
			}
			
		}else{
		   $assignedUsersOfProgram = $programUsers;
		   $assignedProgramsOfUser = $userPrograms;
		   $assignedData['assignedUsersOfProgram'] = $assignedUsersOfProgram;
		   $assignedData['assignedProgramsOfUser'] = $assignedProgramsOfUser;
		   return $assignedData;
		}
		
		
	}
	
	
	/**
	 * This function is using for getting the progran or course/program allocation type for particular course/program
	 * @global object
	 * @param string $instanceId is course or program id
	 * @param string $instanceType is either course or program
	 * @return int $allocationType is a allocation type of course/program
	 */
	 
	function getProgramCourseAllocationType($instanceId, $instanceType){
	
	 global $CFG, $DB;
	 
	    $defaultAllocationType = 1; // default allocation type
	    if($instanceId){
		  $query = "SELECT allocation_type FROM {$CFG->prefix}program_course_settings WHERE instance_id = '".$instanceId."' AND instance_type='".$instanceType."'";
		  $allocationType  = $DB->get_field_sql($query);
		}
		
		$allocationType = $allocationType?$allocationType:$defaultAllocationType;
		
		return $allocationType;
		
	}
	  /**
	 * This function is using for getting a program course
	 * @global object
	 * @param object $pid id of a program
	 * @return $programCourses a program course
	 */
	
	function getProgramCourses($pid){
	  global $DB, $CFG;
	  $programCourses = array();
	  
	   if($pid){
	     //$query = "SELECT mc.* FROM {$CFG->prefix}program_course mpc LEFT JOIN {$CFG->prefix}course mc ON (mc.id = mpc.courseid) WHERE mpc.programid = '".$pid."' AND mpc.is_active= '1' AND mc.is_active = '1' AND mc.id NOT IN (1) AND mc.deleted = '0' ORDER BY mpc.course_order ";
	     $query = "SELECT mc.*,GROUP_CONCAT(mcp.fullname SEPARATOR ',') AS criteria_course_name
			FROM mdl_program_course mpc
			LEFT JOIN mdl_program_course_criteria as pcc ON mpc.id = pcc.program_map_id  
			LEFT JOIN mdl_course mc ON (mc.id = mpc.courseid)
			LEFT JOIN mdl_course mcp ON (mcp.id = pcc.course_id) WHERE mpc.programid = '".$pid."' AND mpc.is_active= '1' AND mc.is_active = '1' AND mc.id NOT IN (1) AND mc.deleted = '0' GROUP BY mpc.courseid ORDER BY mpc.course_order";
	     $programCourses = $DB->get_records_sql($query);
	  }
	  
	  return $programCourses;
	}
	
	  /**
	 * This function is using for getting a program image
	 * @global object
	 * @param object $program object of a program
	 * @return html of program image, if exists otherwise it will return default image
	 */
	 
	 
	  function getProgramImage($program){
	  
	    global $DB, $CFG;
	  
	     $programImgURL = $CFG->programDefaultImage;
		 $programImgPath = '';
		 $programName = '';
		 $programType = $CFG->programType;
		 $programImageFileArea = $CFG->programImageFileArea;
	     if(count($program) > 0){
		  
		    $programId = $program->id;
			$programName = $program->name;
	        $programContextId = $DB->get_field('context','id',array('instanceid'=>$programId,'contextlevel'=> 90));
			if($programContextId){
			
				$programImage = $DB->get_record_sql("Select  contextid, component, contextid, filearea, filename from {$CFG->prefix}files where contextid = ".$programContextId." and component = '".$programType."' and filearea = '".$programImageFileArea."' and filename !='' and filename !='.'");

				if(count($programImage) > 0 && $programImage->contextid && $programImage->component && $programImage->filearea && $programImage->filename){
				
					$programImgPath = $CFG->wwwroot.'/pluginfile.php/'.$programImage->contextid.'/'.$programImage->component.'/'.$programImage->filearea.'/'.$programImage->filename;
					
					if(isUrlExists($programImgPath)){
					  $programImgURL = $programImgPath;
					}
					
				}
			}

	    }		
		
		$programImg = '<img src="'.$programImgURL.'" alt="'.$programName.'" title="'.$programName.'" border="0" />';
		
		return $programImg;
			
	  
	  }
	  
	 	  
	 /**
	 * This function is using for getting a program materials
	 * @global object
	 * @param object $program object of a program
	 * @return  program materials
	 */
	 
	 
	  function getProgramMaterials($program){
	  
	    global $DB, $CFG;
	  
	     $programMaterialArr = '';
		 $programMaterialPath = '';
		 $programName = '';
		 $programType = $CFG->programType;
		 $programMaterialFileArea = $CFG->programMaterialFileArea;
	     if(count($program) > 0){
		  
		    $programId = $program->id;
			$programName = $program->name;
	        $programContextId = $DB->get_field('context','id',array('instanceid'=>$programId,'contextlevel'=> 90));
			if($programContextId){
				$records = $DB->get_records_sql("Select  * from {$CFG->prefix}files where contextid = ".$programContextId." and component = '".$programType."' and filearea = '".$programMaterialFileArea."' and filename !='' and filename !='.'");
				
				if(count($records) > 0){
				
						 foreach($records as $programMaterial){
						 
							  if( $programMaterial->contextid && $programMaterial->component && $programMaterial->contextid && $programMaterial->filename){
								$programMaterialPath = $CFG->wwwroot.'/pluginfile.php/'.$programMaterial->contextid.'/'.$programMaterial->component.'/'.$programMaterial->filearea.'/'.$programMaterial->filename;
								//if(isUrlExists($programMaterialPath)){
								  $programMaterialLink = '<a href="'.$programMaterialPath.'" alt="'.$programName.'" title="'.$programName.'" border="0" target="_blank">'.$programMaterial->filename.'</a>';
								  $programMaterialArr[] = $programMaterialLink;
								//}
								
							 }		
							
						 }	
				}
			}

	    }		
		
		return $programMaterialArr;
			
	  
	  }
	  
	  function isUserAbleToEnrollment($typeId, $type){
	   
	    global $DB, $CFG, $USER;
        
		$userId = $USER->id;
		$userAbleToEnrollment = 0; // if course is not in user team, so user will not be able to enrol
	    if($USER->archetype == $CFG->userTypeStudent){

			$assignGroupsForUser = getAssignGroupsForUser($userId, 3);
			
		    if($type == $CFG->courseType && $typeId){
			
			    $courseId = $typeId;
			    $userCourseId = getAssignedCourseIdForUser($userId);
			 
				if( in_array($courseId, $userCourseId ) ) {
				   $userAbleToEnrollment = 2; // already enrolled
				}else{

					 if(count($assignGroupsForUser) > 0){
					
							$isUserAndCourseInSameTeam = false;
							$assignedGroupsForCourse = getAssignGroupsForCourse($courseId, 3);
	
							foreach($assignGroupsForUser as $assignedGroup){
							   if(in_array($assignedGroup, $assignedGroupsForCourse)){
								  $isUserAndCourseInSameTeam = true;
								  break;
							   }
							}
						
							if( $isUserAndCourseInSameTeam) {
								$userAbleToEnrollment = 1; // course is in user team, so user will be able to enrol
							}
						
					  }
			    }
				
		    }elseif($type == $CFG->programType && $typeId){
			
			   $programId = $typeId;
			   $userProgramId = getAssignedProgramIdForUser($userId);
			   
			   if( in_array($programId, $userProgramId ) ) {
			     $userAbleToEnrollment = 2; // already enrolled
			   }else{
			   
				   if(count($assignGroupsForUser) > 0){
										
						$isUserAndProgramInSameTeam = false;
						$assignGroupsForProgram = getAssignGroupsForProgram($programId, 3);
		
						foreach($assignGroupsForUser as $assignedGroup){
						   if(in_array($assignedGroup, $assignGroupsForProgram)){
							  $isUserAndProgramInSameTeam = true;
							  break;
						   }
						}
						
						if( $isUserAndProgramInSameTeam) {
		                   $userAbleToEnrollment = 1; // course is in user team, so user will be able to enrol     
						}
					
				   }
				   
				}	   
			  
			}
			
		}	
		
		return $userAbleToEnrollment;
	  
	  }
	  
	  /**
	 * This function is using for getting all teams by department
	 * @global object
	 * @param array $departmentArr array of selected deparmtnet id 
	 * @param array $selectedTeamArr array of selected user id 
	 * @return string the dropdrown html for teams.
	 */
		
	function getTeamListByDepartment($departmentArr, $selectedTeamArr, $return = 1, $isReport = false){

            global $CFG, $DB, $USER;

            $departmentsGroupsIdsSTR = 0;
			$departmentsGroupsArr = array();
			
			if($departmentArr[0] == '-1'){
			   
			   switch($USER->archetype){
					CASE $CFG->userTypeAdmin:
					     $query = "SELECT id FROM {$CFG->prefix}department WHERE 1 = 1 AND deleted = '0' ";
						 if(!$isReport){
						   $query .= " AND status = '1' ";
						 }
						 $query .= " order by title ASC";
					     $records = $DB->get_records_sql($query);
						 $departmentArr = array_keys($records);
					break;
					CASE $CFG->userTypeManager:
						 $departmentArr = array($USER->department);
					break;
					CASE $CFG->userTypeStudent:
						 $departmentArr = array($USER->department);
					break;
				}
			 
			}

			if(count($departmentArr) > 0){
				$departmentsGroupsArr = getDepartmentGroups($departmentArr, $isReport);
			}
			
			
			if($return == 1){
               
				$selectedT = in_array('-1', $selectedTeamArr)?'selected="selected"':'';
				$select = '<option value="-1" '.$selectedT.' >'.get_string('allteams','multiuserreport').'</option>';
				if(count($departmentsGroupsArr) > 0 ){
				  foreach($departmentsGroupsArr as $arr){
					$selectedT = in_array($arr->id, explode(",",implode(",",$selectedTeamArr)))?"selected='selected'":'';
					$select .= '<option value="'.$arr->id.'" '.$selectedT.'>'.$arr->name.'</option>';
				  }
				}else{
				 // $selectedT = in_array(0, $selectedTeamArr)?'selected="selected"':'';
				  //$select .= '<option '.$selectedT.' value="0">'.get_string('noteamfound','multiuserreport').'</option>';
				}
				
				if($error == ''){
				 $outcome->success = true;
				 $outcome->response = $select;
				}
				
				return $outcome;
			
		   }elseif($return == 2){ 
		      return $departmentsGroupsArr;
		   }else{
		     return false;
		   }	
	   

	
    }
	
	function getRightPanelCalendar(){
	
	  
	   //ini_set('display_errors', 1);
	   //error_reporting(E_ALL);
	   global $CFG, $OUTPUT;
	  
	   require_once($CFG->dirroot."/blocks/moodleblock.class.php");
	   require_once($CFG->dirroot."/local/blocks/calendar_month/block_calendar_month.php");
	   
	   $objBCM = new block_calendar_month();
	   $calenderData  = $objBCM->get_content()->text;
	   return $calenderData;
	
	}
	  
	  
	  
	
	
	########################################### End Program Functions ##########################
	
 /**
	 * This function is using for getting a department image
	 * @global object
	 * @param object $department object of a department
	 * @return html of department image, if exists otherwise it will return default image
	 */
	 
	 
	  function getDepartmentImage($department){
	  
	    global $DB, $CFG;
	  
	     $programImgURL = $CFG->departmentDefaultImage;
		 $programImgPath = '';
		 $programName = '';
	     if(count($department) > 0){
		    $departmentId = $department->id;
			$programName = $department->title;
	        $departmentContextId = $DB->get_field('context','id',array('instanceid'=>$departmentId,'contextlevel'=> 150));
			if($departmentContextId){
			
				$programImage = $DB->get_record_sql("Select  contextid, component, contextid, filearea, filename from {$CFG->prefix}files where contextid = ".$departmentContextId." and component = 'test' and filearea = 'testimage' and filename !='' and filename !='.'");
				if(count($programImage) > 0 && $programImage->contextid && $programImage->component && $programImage->filearea && $programImage->filename){
				
					$programImgPath = $CFG->wwwroot.'/pluginfile.php/'.$programImage->contextid.'/'.$programImage->component.'/'.$programImage->filearea.'/'.$programImage->filename;
					//if(isUrlExists($programImgPath)){
					  $programImgURL = $programImgPath;
					//}
					
				}
			}

	    }		
		
		$programImg = '<img src="'.$programImgURL.'" alt="'.$programName.'" title="'.$programName.'" border="0" />';
		
		return $programImg;
			
	  
	  }
	  function getTeamImage($department){
	  
	    global $DB, $CFG;
	     $programImgURL = $CFG->teamDefaultImage;
		 $programImgPath = '';
		 $programName = '';
	     if(count($department) > 0){
		    $departmentId = $department->id;
			$programName = $department->name;
	        $departmentContextId = $DB->get_field('context','id',array('instanceid'=>$departmentId,'contextlevel'=> 140));
			if($departmentContextId){
			
				$programImage = $DB->get_record_sql("Select  contextid, component, contextid, filearea, filename from {$CFG->prefix}files where contextid = ".$departmentContextId." and component = 'team' and filearea = 'teamimage' and filename !='' and filename !='.'");
				if(count($programImage) > 0 && $programImage->contextid && $programImage->component && $programImage->filearea && $programImage->filename){
				
					$programImgPath = $CFG->wwwroot.'/pluginfile.php/'.$programImage->contextid.'/'.$programImage->component.'/'.$programImage->filearea.'/'.$programImage->filename;
					//if(isUrlExists($programImgPath)){
					  $programImgURL = $programImgPath;
					//}
					
				}
			}

	    }		
		
		$programImg = '<img src="'.$programImgURL.'" alt="'.$programName.'" title="'.$programName.'" border="0" />';
		
		return $programImg;
			
	  
	  }
	  
	  
	  function getUserManager(){
	  
	         global $DB, $CFG, $USER;
	  
	      	 $excludedUsers = count($CFG->excludedUsers) > 0 ? implode(",",$CFG->excludedUsers) : 0;
				 
			 $queryAll = "select  mu.id, concat(mu.firstname,' ', mu.lastname) as fullname,mu.email, mu.username";
			// $queryAll .= " , concat(mu.firstname,' ', mu.lastname) as fullname, mu.username, mu.email, mu.phone1, mu.phone1, mu.createdby";
			// $queryAll .= " , if(mgm.groupid, GROUP_CONCAT(DISTINCT mg.name ORDER BY mg.name ASC),'".getMDash()."') as teamtitle";
			// $queryAll .= " , if(mdm.departmentid, GROUP_CONCAT(DISTINCT md.title ORDER BY md.title ASC),'".getMDash()."') as departmenttitle";
			// $queryAll .= " , if(mdm.departmentid, GROUP_CONCAT(DISTINCT mdm.departmentid ORDER BY md.title ASC),'') as department_id";
			 $queryAll .= " , if(mu_manager.id!=''";
			 $queryAll .= " , mu_manager.id,0) as usermanagerid ";
			 $queryAll .= " , if(concat(mu_manager.firstname,' ', mu_manager.lastname)!=''";
			 $queryAll .= " , concat(mu_manager.firstname,' ', mu_manager.lastname),'".getMDash()."') as usermanager ";
			 $queryAll .= " FROM {$CFG->prefix}user mu ";
			 $queryAll .= " LEFT JOIN {$CFG->prefix}user mu_manager ON (mu.parent_id = mu_manager.id) ";
			 $queryAll .= " LEFT JOIN {$CFG->prefix}groups_members mgm ON (mgm.userid = mu.id) ";
			 $queryAll .= " LEFT JOIN {$CFG->prefix}groups mg ON (mg.id = mgm.groupid) ";
			 $queryAll .= " LEFT JOIN {$CFG->prefix}department_members mdm ON (mdm.userid = mu.id) ";
			 $queryAll .= " LEFT JOIN {$CFG->prefix}department md ON (md.id = mdm.departmentid) ";
			 $queryAll .= " WHERE 1 = 1 AND mu.deleted = '0' ";
			 if(!$isReport){
			   $queryAll .= "  ";
			 }
			 $queryAll .= " AND mu.id NOT IN (".$excludedUsers.") ";
			 $queryAll .= " $searchString ";
			 $queryAll .= " GROUP BY mu.id ";
			 $queryAll .= " ORDER BY mu.id ASC";
			
			 $result = $DB->get_records_sql($queryAll);
			 
			 return  $result;

	  
	  }
	  
	   /**
	 * This function is using for getting a number format for a number
	 * @global object
	 * @param float $number is number that we are going to format with giving decimal place
	 * @return float $num is formatted number with giving decimal place
	 */
	  
	  function numberFormat($number, $place = 1){

	     $num = $number;
	     if($number){
		   $num = number_format($number, $place, '.', '');
		 }
		 
		 return $num;
	  }
	  
	  
	    /**
	 * This function is using for getting all completed course details of an user
	 * @global object
	 * @param int $userId is user id
	  * @return array $completedCoursesArr
	 */
	 
	  function getAllCompletedCourseOfUser2($userId){
	      
		 global $DB, $CFG;
	     $completedCoursesArr = array();
		
		 $result = getCourseStatusByUser($whereChk='', '', '');
		 if(count($result) > 0 ){
		 
		   foreach($result as $userId=>$arr){
		   
		      $completedCoursesArr[$userId] = isset($arr['course_details_by_status'][2])?$arr['course_details_by_status'][2]:array();
		   
		   }
		 }
		 //pr($completedCoursesArr);die;
		
		 return $completedCoursesArr;  
	  }
	  
	  function getAllCompletedCourseOfUser($userId){
	      
		global $DB, $CFG;
	    $completedCoursesArr = array();
		 
		if($userId){  
			

			$courseProgramList = get_learning_list('', 2, 1, 0, 'all', $extraCond='', $userId);

			//pr($courseProgramList);die;
			
			//pr($courseProgramList);
										
			if(count($courseProgramList) > 0 ){
						$courseIdArr = array();
						foreach($courseProgramList as $key=>$programList){
							  $listArr = explode('_',$key);
							 $courseId = $listArr[1];
							$programId = $listArr[0];
							
							if($programId){
							  $programCourses =  $DB->get_records_sql("SELECT c.id, c.fullname, credithours FROM mdl_course as c LEFT JOIN mdl_program_course as pc ON pc.courseid = c.id AND pc.is_active = 1 LEFT JOIN mdl_context AS ct ON ct.instanceid = c.id AND ct.contextlevel = 50 LEFT JOIN mdl_files as f On f.contextid = ct.id AND f.component = 'course' AND f.filearea = 'overviewfiles' AND f.filename !='' AND f.filename !='.' WHERE 1 = 1 AND c.deleted = '0' AND pc.programid = ".$programId);
							  if(count($programCourses) > 0 ){
								foreach($programCourses as $courseId=>$arr){
								  $courseIdArr[] = $courseId;

								}
							  }
							}else{
							  $courseIdArr[] = $courseId;
							}
						}
					
						$courseIdArr = array_unique($courseIdArr);
						
						$totalCourses = count($courseIdArr);
						if($totalCourses > 0 ){
						  $courseIds = implode(",", $courseIdArr);
						  $completedCoursesArr =  $DB->get_records_sql("SELECT c.id, c.fullname, credithours FROM `mdl_course` as c WHERE 1 = 1 AND c.deleted = '0' AND c.id IN (".$courseIds.")");
						}
					
						
			  }	
		  }	
		  
		  return $completedCoursesArr;  
	  }
	  
	  
	   /**
	 * This function is using for getting a user credit hours report
	 * @global object
	 * @param array $paramArray is search array
	 * @param array $removeKeyArray contain search key that you don't want to add in address bar
	 * @param string $sort is sorting field
	 * @param string $dir is sorting order
	 * @param int $page current page
	 * @param int $perpage is number of records per page
	 * @param string $export is action variable for PDF download, CSV download, and Print an user report
	  * @return array $userCHReport return CSV , PDF and Report Content
	 */
	  
	  
	  function getUserCreditHoursReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export=''){
		
		     //ini_set('display_errors', 1);
		    //error_reporting(E_ALL);
				
			global $DB, $CFG,$USER;
			$isReport = true;
			if($CFG->isCignium  == 'Y'){
				if($USER->department == _THEME_COLOR_TRANZACT_DEPARTMENT){
					$CFG->creditReportColor = _THEME_COLOR_TRANZACT;
				}elseif($USER->department == _THEME_COLOR_TRANZACTIS_DEPARTMENT){
					$CFG->creditReportColor = _THEME_COLOR_TRANZACTIS;
				}elseif($USER->department == _THEME_COLOR_CIGNIUM_DEPARTMENT){
					$CFG->creditReportColor = _THEME_COLOR_CIGNIUM;
				}
			}
			
			$userCHReport = new stdClass();
			
			////// Getting common URL for the Search //////
			$pageURL = $_SERVER['PHP_SELF'];
			$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
			

			$linkToDetailsURL = genParameterizedURL(array_merge($paramArray, array('uid'=>':userId','back'=>1)), $removeKeyArray, '/user/user_course_credithours_report.php');
			
			$actionUrl = '/user/user_credithours_report_print.php';
			$genActionURL = genParameterizedURL($paramArray, $removeKeyArray, $actionUrl);

					
			$sDepartment    = $paramArray['department'];
			$sTeam          = $paramArray['team'];
			$sUser          = $paramArray['user'];
			$sStartDate     = $paramArray['startDate'];
			$sEndDate       = $paramArray['endDate'];
			$sUserType       = $paramArray['userType'];
			
			
			$sJobTitle          = $paramArray['job_title'];
			$sCompany          = $paramArray['company'];
			$sJobTitleArr = explode("@",$sJobTitle);
			$sCompanyArr = explode("@",$sCompany);
			$sJobTitleName = $sJobTitle=='-1'?(get_string('all','multiuserreport')):getJobTitles($sJobTitleArr);
			$sCompanyName = $sCompany=='-1'?(get_string('all','multiuserreport')):getCompany($sCompanyArr);
			
			
			$sDepartmentArr = explode("@",$sDepartment);
			$sTeamArr = explode("@",$sTeam);
			$sUserArr = explode("@",$sUser);
			$sUserTypeArr = explode("@",$sUserType);

			$sDepartmentName = $sDepartment=='-1'?(get_string('all','multiuserreport')):getDepartments($sDepartmentArr, $isReport);
			if($USER->archetype != $CFG->userTypeAdmin ){
				$sDepartmentName = getDepartments($USER->department, $isReport);
			}
			
			$sUserGroup     = $paramArray['user_group'];
			$sUserGroupArr = explode("@",$sUserGroup);
			if($paramArray['sel_mode'] == 2){
				$sTeamArr = $sUserGroupArr;
			}
			
			$sTeamName = $sTeam=='-1'?(get_string('all','multiuserreport')):getTeams($sTeamArr);
			$sUserFullName = $sUser=='-1'?(get_string('all','multiuserreport')):getUsers($sUserArr);
			$sUserGroupName = $sUserGroup=='-1'?(get_string('all','multiuserreport')):getUserGroup($sUserGroupArr);
			
			if($sUserTypeArr[0] == '-1' || count($sUserTypeArr) > 2 ){
			  $sUserTypeName = get_string('all','multiuserreport');
			}else{
			  $sUserTypeName = implode(", ",array_map('ucfirst', $sUserTypeArr));
			}
		
			$searchString = "";
			$searchDString = "";
			$searchTString = "";
			
			$sDateSelected = '';
			if($sStartDate){
				$sDateArr = getFormattedTimeStampOfDate($sStartDate);
			    $sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

			}		   
			
			$eDateSelected = '';
			if($sEndDate){
			   $eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
			   $eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
			}
			
			
			
			if(count($sUserArr) > 0 && !in_array($sUserArr[0] , array(0, '-1'))){
				$sUserStr = count($sUserArr) > 0 ? implode(",", $sUserArr) : 0;
				$searchString .= " AND id IN ($sUserStr) ";
			}else{
			
			    /*$checkInstructor = $sUserType == $CFG->userTypeInstructor?1:0;
				if($_REQUEST["sel_mode"] == 2){ 
				   $usersArr = getOpenTeamsUser($sTeamArr);
				}else{
			       $usersArr = getUserListByDnT($sDepartmentArr, $sTeamArr, $sUserArr, 2, $isReport, $checkInstructor, $sJobTitleArr, $sCompanyArr);
				}*/
				

			   if($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2 ){

				 $checkInstructor = 0;
				 $usersArr = getUserListByDnT($sDepartmentArr, $sTeamArr, $sUserArr, 2, $isReport, $checkInstructor, $sJobTitleArr, $sCompanyArr, $_REQUEST["sel_mode"]);
				 $usersArr = count($usersArr)>0?array_unique($usersArr):array();
				 
			   }elseif(in_array($CFG->userTypeInstructor, $sUserTypeArr)){
				  $checkInstructor = 1;
				  $usersArr = getUserListByDnT($sDepartmentArr, $sTeamArr, $sUserArr, 2, $isReport, $checkInstructor, $sJobTitleArr, $sCompanyArr, $_REQUEST["sel_mode"]);
			   }elseif(in_array($CFG->userTypeStudent, $sUserTypeArr)){
				   $checkInstructor = 0;
				   $usersArr = getUserListByDnT($sDepartmentArr, $sTeamArr, $sUserArr, 2, $isReport, $checkInstructor, $sJobTitleArr, $sCompanyArr, $_REQUEST["sel_mode"]);
			   }

				$managerUsersIdSql = '';
				if(count($usersArr) > 0 ){
				  sort($usersArr);
				  $usersId = implode(",", $usersArr);
				  $searchString .= " AND id IN ($usersId)";
				}else{
				   $searchString .= " AND id IN (0)";
				}

			}
			
			if($CFG->showJobTitle == 1 && count($sJobTitleArr) > 0 && !in_array($sJobTitleArr[0] , array(0, '-1'))){
				$sJobTitleStr = count($sJobTitleArr) > 0 ? implode(",", $sJobTitleArr) : 0;
				$searchString .= " AND jobid IN ($sJobTitleStr) ";
			}
			
			if($CFG->showCompany == 1 &&  count($sCompanyArr) > 0 && !in_array($sCompanyArr[0] , array(0, '-1'))){
				$sCompanyStr = count($sCompanyArr) > 0 ? implode(",", $sCompanyArr) : 0;
				$searchString .= " AND comid IN ($sCompanyStr) ";
			}
			
			
		    $searchStringI = '';
			$searchStringL  = '';
			if($sDateSelected){ 
				$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
				$searchStringI .= " AND (class_endtime >= ($sStartDateTime) || (class_endtime is null))";
				$searchStringL .= " AND (complition_time >= ($sStartDateTime) || (complition_time is null))";
			}
			
			if($eDateSelected){
			   $sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
			   $searchStringI .= " AND (class_endtime <= ($sEndDateTime) || (class_endtime is null))";
			   $searchStringL .= " AND (complition_time <= ($sEndDateTime) || (complition_time is null))";
			}
				
				
		    $offset = $page - 1;
			$offset = $offset*$perpage;
			$limit = '';
			if($perpage != 0){
				$limit = "LIMIT $offset, $perpage";
			}
			
			if($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2 ){
			  $recordArr = getOverallCourseCreditReportForLnI($searchString, $searchStringI, $searchStringL, $paramArray, $sort, $dir);
			}else{
			  $recordArr = getOverallCourseCreditReport($sUserType, $searchString, $searchStringI, $searchStringL, $paramArray, $sort, $dir);
			}
			//pr($recordArr );die;
			$creditHoursArr = $recordArr['creditHoursArr'];
			$allUserArr = $recordArr['allUserArr'];
			
			//pr($allUserArr);die;
			$allSystemUserCount = count($allUserArr['users']);	

			$userHTML = '';
			$style = !empty($export)?"style=''":'';
			
			if(empty($export) || (!empty($export) && $export == 'print')){
				$userHTML .= "<div class='tabsOuter borderBlockSpace'>";
				$userHTML .= '<div class="clear"></div>';
			}
			
			//$totalCreditHoursInDept = $creditHoursArr['totalCreditHoursInDept'];
			$onlineGraphCreditHours = isset($creditHoursArr[1])?$creditHoursArr[1]:0;
		    $classroomGraphCreditHours = isset($creditHoursArr[2])?$creditHoursArr[2]:0;
			$totalGraphCreditHours = $onlineGraphCreditHours + $classroomGraphCreditHours;	
			
			$OCCreditHourPercentage = numberFormat(($onlineGraphCreditHours*100)/$totalGraphCreditHours);
			$CCCreditHourPercentage = numberFormat(($classroomGraphCreditHours*100)/$totalGraphCreditHours);
				
			$OCreditHours = setCreditHoursFormat($onlineGraphCreditHours);
			$CCreditHours = setCreditHoursFormat($classroomGraphCreditHours);
			

			if(empty($export)){
				ob_start();
				require_once($CFG->dirroot . '/local/includes/usercredithoursreportsearch.php');
				$userHTML .= ob_get_contents();
				ob_end_clean();
			
				$exportHTML = '<div class="exports_opt_box"> ';
				$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','multiuserreport').'" href="'.$genActionURL.'&perpage=0&&action=exportcsv">'.get_string('downloadcsv','multiuserreport').'</a> ';
				$exportHTML .= '<span class="seperater">&nbsp;</span>';
				$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','multiuserreport').'" id="exportpdf1" href="'.$genActionURL.'&perpage=0&&action=exportpdf">'.get_string('downloadpdf','multiuserreport').'</a> ';
				$exportHTML .= '<span class="seperater">&nbsp;</span>';
				$exportHTML .= '<a rel="'.$genActionURL.'&perpage=0&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','multiuserreport').'" class="print_icon">'.get_string('print','multiuserreport').'</a>';
				$exportHTML .= '</div>';
			}
			
			if(empty($export) || (!empty($export) && $export == 'print')){
			  $userHTML .= '<div class="userprofile view_assests">';
			}
			
			$userCourseStatusArr = array();
			$totalAssignedCoursesAll = 0;
			$inProgressCoursesAll = 0;
			$completedCoursesAll = 0;
			$notStartedCoursesAll = 0;
			
			
			$IPUserId = array(); 
			$CLUserId = array();
			$NSUserId = array();  
			
			$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
			$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
			$sDateSelectedForPrintCSV = getDateFormatForPrint($sDateSelected, 'csv');
			$eDateSelectedForPrintCSV = getDateFormatForPrint($eDateSelected, 'csv');
			

			if($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2 ){
				$headerLabelGraph = get_string('usercredithoursreport');
			}elseif(in_array($CFG->userTypeInstructor, $sUserTypeArr)){
				$headerLabelGraph = get_string('instructorcredithoursreport');
				// $graphTitle = get_string('departmentvscredithoursofinstructor');
			}elseif(in_array($CFG->userTypeStudent, $sUserTypeArr)){
				$headerLabelGraph = get_string('learnercredithoursreport');
				//$graphTitle = get_string('departmentvscredithoursoflearner');
			}
			
			
			if($allSystemUserCount > 0){
			
			  if(!empty($export) && $export == 'print'){
			  
			  	$reportName = $headerLabelGraph;
			  	$userHTML .= getReportPrintHeader($reportName);
			  	
						   $userHTML .= '<table border="0" cellpadding="0" cellspacing="0" class="generaltable margin_bottom" >';
						
						   if($_REQUEST["sel_mode"] == 2){
								$userHTML .= '<tr>';
									$userHTML .= '<td><strong>'.get_string('usertype').':</strong> '.$sUserTypeName.'</td>';
									$userHTML .= '<td><strong>'.get_string('user','multiuserreport').':</strong> '.$sUserFullName.'</td>';
								$userHTML .= '</tr>';
								
								$userHTML .= '<tr>';
									$userHTML .= '<td colspan="2"><strong>'.get_string('global_team').':</strong> '.$sUserGroupName.'</td>';
								$userHTML .= '</tr>';
								
						   }else{
						   
						        $userHTML .= '<tr>';
									$userHTML .= '<td><strong>'.get_string('usertype').':</strong> '.$sUserTypeName.'</td>';
									$userHTML .= '<td colspan="2"><strong>'.get_string('user','multiuserreport').':</strong> '.$sUserFullName.'</td>';
								$userHTML .= '</tr>';
								
								 $userHTML .= '<tr>';
								    $userHTML .= '<td><strong>'.get_string('department','multiuserreport').':</strong> '.$sDepartmentName.'</td>';
									$userHTML .= '<td><strong>'.get_string('team','multiuserreport').':</strong>  '.$sTeamName.'</td>';
								 $userHTML .= '</tr>';
								 
						   }

							if($CFG->showJobTitle == 1 && $CFG->showCompany == 1){
								$userHTML .= '<tr>';
									$userHTML .= '<td ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
									$userHTML .= '<td><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
								$userHTML .= '</tr>';
							}elseif($CFG->showJobTitle == 1 && $CFG->showCompany == 0){
							    $userHTML .= '<tr>';
									$userHTML .= '<td colspan="2" ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
								$userHTML .= '</tr>';
							}elseif($CFG->showJobTitle == 0 && $CFG->showCompany == 1){
							    $userHTML .= '<tr>';
									$userHTML .= '<td colspan="2" ><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
								$userHTML .= '</tr>';
							}
							
			
							$userHTML .= '<tr>';
								$userHTML .= '<td><strong>'.get_string('credithourscomletiondate').':</strong> '.$sDateSelectedForPrint.'</td>';
								$userHTML .= '<td><strong>'.get_string('credithourscomletiondateto').':</strong>  '.$eDateSelectedForPrint.'</td>';
							$userHTML .= '</tr>';
						

						$userHTML .= '</table>';
						//$userHTML .= '<span '.$CFG->pdfSpanAttribute.'>&nbsp;<br /><strong>'.get_string('userreport','multiuserreport').'</strong></span><br /><br />';
			   }
			   
				    
			 
			    if(empty($export) || (!empty($export) && $export == 'print')){
					if( $totalGraphCreditHours){
								
					   $userHTML .= '<div class = "single-report-start" id="watch">
									<div class = "single-report-graph-box"> <span class="main-heading">'.$headerLabelGraph.$exportHTML.'</span>
									  <div class="single-report-graph-left" id="chartContainer">'.get_string('loading','multiuserreport').'</div>
										<div class="single-report-graph-right">
										<div class="course-count-heading">'.get_string('coursecredithourswithtime').'</div>
										<div class="course-count">'.(setCreditHoursFormat($totalGraphCreditHours)).'</div>
										<div class="seperator">&nbsp;</div>
										<div class="course-status">';
											if(in_array($CFG->userTypeStudent, $sUserTypeArr) || ($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2)){	
												$userHTML .= '<div class="publish-deactive"><h6>'.get_string('onlinecourse').'</h6><span>'.(setCreditHoursFormat($onlineGraphCreditHours)).'</span></div>
														  <div class="clear"></div><div class="publish-active"><h6>'.get_string('classroomcourse').'</h6><span>'.(setCreditHoursFormat($classroomGraphCreditHours)).'</span></div>';	  
											   
											}else{
												 $userHTML .= '<div class="publish-active"><h6>'.get_string('classroomcourse').'</h6><span>'.(setCreditHoursFormat($classroomGraphCreditHours)).'</span></div>';
											}
							$userHTML .= '
										</div>
									  </div>
								  </div>
								 </div><div class="clear"></div>';
					}else{
						  $userHTML .= '<div class = "single-report-start-nograph">
											<div class = "single-report-graph-box"> <span class="main-heading">'.$headerLabelGraph.$exportHTML.'</span></div>
										 <div class="clear"></div>
										 </div>';
					}
					
				 }				 
			}
			
			$reportContentPDF = '';
			$reportContentCSV = '';
			if(empty($export) || (!empty($export) && $export == 'print')){ 
		
				$userHTML .= '<div class="clear"></div>';
				$userHTML .= '<div class=""><table cellspacing="0" cellpadding="0" border="0" class="generaltable"><tbody>';
			
			}elseif((!empty($export) && $export == 'exportpdf')){
			
			    $reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0"  class="generaltable">';
			
			    if($_REQUEST["sel_mode"] == 2){
					$reportContentPDF .= '<tr>';
					    $reportContentPDF .= '<td><strong>'.get_string('usertype').':</strong> '.$sUserTypeName.'</td>';
						$reportContentPDF .= '<td ><strong>'.get_string('user','multiuserreport').':</strong> '.$sUserFullName.'</td>';
					$reportContentPDF .= '</tr>';
					
				   $reportContentPDF .= '<tr>';
				    $reportContentPDF .= '<td colspan="2" ><strong>'.get_string('global_team').':</strong> '.$sUserGroupName.'</td>';
				   $reportContentPDF .= '</tr>';
				 
			    }else{
					 $reportContentPDF .= '<tr>';
					    $reportContentPDF .= '<td><strong>'.get_string('usertype').':</strong> '.$sUserTypeName.'</td>';
						$reportContentPDF .= '<td ><strong>'.get_string('user','multiuserreport').':</strong> '.$sUserFullName.'</td>';
					 $reportContentPDF .= '</tr>';
					
					 $reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td><strong>'.get_string('department','multiuserreport').':</strong> '.$sDepartmentName.'</td>';
						$reportContentPDF .= '<td><strong>'.get_string('team','multiuserreport').':</strong>  '.$sTeamName.'</td>';
					 $reportContentPDF .= '</tr>';
			    }
						   
				
				if($CFG->showJobTitle == 1 && $CFG->showCompany == 1){
					$reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
						$reportContentPDF .= '<td><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
					$reportContentPDF .= '</tr>';
				}elseif($CFG->showJobTitle == 1 && $CFG->showCompany == 0){
					$reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td colspan="2" ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
					$reportContentPDF .= '</tr>';
				}elseif($CFG->showJobTitle == 0 && $CFG->showCompany == 1){
					$reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td colspan="2" ><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
					$reportContentPDF .= '</tr>';
				}
				
									 
			

						
				$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td  width="50%"  ><strong>'.get_string('credithourscomletiondate').':</strong> '.$sDateSelectedForPrint.'</td>';
					$reportContentPDF .= '<td  width="50%"  ><strong>'.get_string('credithourscomletiondateto').':</strong>  '.$eDateSelectedForPrint.'</td>';	
				$reportContentPDF .= '</tr>';
				
							
				 if(in_array($CFG->userTypeStudent, $sUserTypeArr) || ($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2)){				  
					$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td><strong>'.get_string('coursecredithourswithtime').':</strong> '.(setCreditHoursFormat($totalGraphCreditHours)).'</td>'; 	
					$reportContentPDF .= '<td><strong>'.get_string('onlinecourse').':</strong> '.$OCreditHours.' ('.floatval($OCCreditHourPercentage).'%)</td>';
					$reportContentPDF .= '</tr>';
					$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td colsapn="2" ><strong>'.get_string('classroomcourse').':</strong> '.$CCreditHours.' ('.floatval($CCCreditHourPercentage).'%)</td>'; 					 $reportContentPDF .= '</tr>';
					
				}else{	
						
					$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td><strong>'.get_string('coursecredithourswithtime').':</strong> '.(setCreditHoursFormat($totalGraphCreditHours)).'</td>'; 	
					$reportContentPDF .= '<td><strong>'.get_string('classroomcourse').':</strong> '.$CCreditHours.' ('.floatval($CCCreditHourPercentage).'%)</td>';
					$reportContentPDF .= '</tr>';
			
				}	
				
				if( $totalGraphCreditHours == 0){
				  $reportContentPDF .= '<tr><br /><td width="100%"  ><span '.$CFG->pdfSpanAttribute.'><strong>'.$headerLabelGraph.'</strong></span><br /></td></tr>';
				}
				
				$reportContentPDF .= '</table>';
				
				if( $totalGraphCreditHours){
				   $reportContentPDF .= getGraphImageHTML($headerLabelGraph);
				}
				
				$reportContentPDF .= '<table '.$CFG->pdfTableStyle.' >';
				
			}elseif((!empty($export) && $export == 'exportcsv')){
			
				$reportContentCSV .= get_string('usertype').",".$sUserTypeName."\n";
				if($_REQUEST["sel_mode"] == 2){
					$reportContentCSV .= get_string('global_team').",".$sUserGroupName."\n";
				}else{
					$reportContentCSV .= get_string('department','multiuserreport').",".$sDepartmentName."\n".get_string('team','multiuserreport').",".$sTeamName ."\n";
					
				}
				
				$reportContentCSV .= get_string('user','multiuserreport').",".$sUserFullName."\n";
				if($CFG->showJobTitle == 1){
				  $reportContentCSV .= get_string('job_title','user').",".$sJobTitleName."\n";
				}
				if($CFG->showCompany == 1){
				  $reportContentCSV .= get_string('company','user').",".$sCompanyName."\n";
				}
				$reportContentCSV .= get_string('credithourscomletiondate').",".$sDateSelectedForPrintCSV."\n".get_string('credithourscomletiondateto').",".$eDateSelectedForPrintCSV."\n";
				
				$reportContentCSV .= get_string('coursecredithourswithtime').",".(setCreditHoursFormat($totalGraphCreditHours))."\n";	
				
				 if(in_array($CFG->userTypeStudent, $sUserTypeArr) || ($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2)){				  
					$reportContentCSV .= get_string('onlinecourse').",".$OCreditHours." (".floatval($OCCreditHourPercentage)."%)\n";
					$reportContentCSV .= get_string('classroomcourse').",".$CCreditHours." (".floatval($CCCreditHourPercentage)."%)\n";
				}else{	
					$reportContentCSV .= get_string('classroomcourse').",".$CCreditHours." (".floatval($CCCreditHourPercentage)."%)\n";
				}	
				
				$reportContentCSV .= $headerLabelGraph."\n";
			}
			

			if(in_array($CFG->userTypeStudent, $sUserTypeArr) || ($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2)){		
			
					
					if(empty($export) || (!empty($export) && $export == 'print')){
					   
						$userHTML .= '<tr>';
							$userHTML .= '<th width="15%" rowspan="2" >'.get_string('fullname','multiuserreport').'</th>';
							$userHTML .= '<th width="15%" rowspan="2" >'.get_string('username').'</th>';
							$userHTML .= '<th width="25%" colspan="3" style="text-align:center;height:35px;">'.get_string('noofcourses').'</th>';
							$userHTML .= '<th width="25%" colspan="3" class = "blueHighlight" style="text-align:center;height:35px;background-color:'.$CFG->creditReportColor.' !important">'.get_string('coursecredithourswithtime').'</th>';
							$userHTML .= '<th width="10%" rowspan="2">'.get_string('manager','multiuserreport').'</th>';
							$userHTML .= '<th width="10%" rowspan="2">'.get_string('department','multiuserreport').'</th>';
						$userHTML .= '</tr>';
						$userHTML .= '<tr>';
							$userHTML .= '<th>'.get_string('onlinecourse').'</th>';
							$userHTML .= '<th>'.get_string('classroomcourse').'</th>';
							$userHTML .= '<th>'.get_string('total').'</th>';
							$userHTML .= '<th>'.get_string('onlinecourse').'</th>';
							$userHTML .= '<th>'.get_string('classroomcourse').'</th>';
							$userHTML .= '<th class = "orangeHighlight" >'.get_string('total').'</th>';
						$userHTML .= '</tr>';
						
					}elseif((!empty($export) && $export == 'exportpdf')){	
						
						  $reportContentPDF .= '<tr >';
								$reportContentPDF .= '<td style="background-color:#ededef;" rowspan="2" ><strong>'.get_string('fullname','multiuserreport').'</strong></td>';
								$reportContentPDF .= '<td style="background-color:#ededef" rowspan="2" ><strong>'.get_string('username').'</strong></td>';
								$reportContentPDF .= '<td style="background-color:#ededef;text-align:center" colspan="3"  ><strong>'.get_string('noofcourses').'</strong></td>';
								$reportContentPDF .= '<td style="background-color:'.$CFG->creditReportColor.';color:#fff;text-align:center" colspan="3" ><strong>'.get_string('coursecredithourswithtime').'</strong></td>';
								$reportContentPDF .= '<td style="background-color:#ededef" rowspan="2"  ><strong>'.get_string('manager','multiuserreport').'</strong></td>';
								$reportContentPDF .= '<td style="background-color:#ededef" rowspan="2" nowrap ><strong>'.get_string('department','multiuserreport').'</strong></td>';
							
							$reportContentPDF .= '</tr>';
							$reportContentPDF .= '<tr>';
								$reportContentPDF .= '<td style="background-color:#ededef"  ><strong>'.get_string('onlinecourse').'</strong></td>';
								$reportContentPDF .= '<td style="background-color:#ededef" nowrap ><strong>'.get_string('classroomcourse').'</strong></td>';
								$reportContentPDF .= '<td style="background-color:#ededef"  ><strong>'.get_string('total').'</strong></td>';
								$reportContentPDF .= '<td style="background-color:#ededef"  ><strong>'.get_string('onlinecourse').'</strong></td>';
								$reportContentPDF .= '<td style="background-color:#ededef" nowrap ><strong>'.get_string('classroomcourse').'</strong></td>';
								$reportContentPDF .= '<td style="background-color:#fc8428"  ><strong>'.get_string('total').'</strong></td>';
							$reportContentPDF .= '</tr>';
							
					  }elseif((!empty($export) && $export == 'exportcsv')){	
					  
					    $reportContentCSV .= get_string('fullname','multiuserreport').",".get_string('username').",,".get_string('noofcourses').",,,".get_string('coursecredithourswithtime').",,".get_string('manager','multiuserreport').",".get_string('department','multiuserreport')."\n";
					    $reportContentCSV .= ",,".get_string('onlinecourse').",".get_string('classroomcourse').",".get_string('total').",".get_string('onlinecourse').",".get_string('classroomcourse').",".get_string('total')."\n";
						
					  }

			}else{
			
			    if(empty($export) || (!empty($export) && $export == 'print')){
				
					 $userHTML .= '<tr>';
						  $userHTML .= '<th width="19%">'.get_string('fullname','multiuserreport').'</th>';
						  $userHTML .= '<th width="19%">'.get_string('username').'</th>';
						  $userHTML .= '<th width="11%">&nbsp;'.get_string('noofcourses').'</th>';
						  $userHTML .= '<th width="13%">&nbsp;'.get_string('coursecredithourswithtime').'</th>';
						  $userHTML .= '<th width="19%">'.get_string('manager','multiuserreport').'</th>';
						  $userHTML .= '<th width="19%">'.get_string('department','multiuserreport').'</th>';
					 $userHTML .= '</tr>';
				 
				}elseif((!empty($export) && $export == 'exportpdf')){
					 $reportContentPDF .= '<tr>';
						 $reportContentPDF .= '<td style="background-color:#ededef"><strong>'.get_string('fullname','multiuserreport').'</strong></td>';
						 $reportContentPDF .= '<td style="background-color:#ededef" ><strong>'.get_string('username').'</strong></td>';
						 $reportContentPDF .= '<td style="background-color:#ededef" ><strong>'.get_string('noofcourses').'</strong></td>';
						 $reportContentPDF .= '<td style="background-color:#ededef" ><strong>'.get_string('coursecredithourswithtime').'</strong></td>';
						 $reportContentPDF .= '<td style="background-color:#ededef"><strong>'.get_string('manager','multiuserreport').'</strong></td>';
						 $reportContentPDF .= '<td style="background-color:#ededef"><strong>'.get_string('department','multiuserreport').'</strong></td>';
					   //$reportContentPDF .= '<td style="background-color:#ededef"><strong>'.get_string('team','multiuserreport').'</strong></td>';
					 $reportContentPDF .= '</tr>';
				}elseif((!empty($export) && $export == 'exportcsv')){	
				   $reportContentCSV .= get_string('fullname','multiuserreport').",".get_string('username').",".get_string('coursecredithourswithtime').",".get_string('noofcourses').get_string('manager','multiuserreport').",".get_string('department','multiuserreport')."\n";
				 }


			}
			
		
			//$totalLearnerInDept = 0;
			//pr($allUserArr['users']);die;
			
			$allSystemUserArr = array();	
		
				  
			if($allSystemUserCount > 0 ){

				  $curtime = time();
				  $courseIdArr = array();
				  $mDash = getMDash();
			      $mDashCSV = getMDashForCSV();

				   if($perpage){
					$offset = $page - 1;
					$offset = $offset*$perpage;
					$allSystemUserArr = array_splice($allUserArr['users'], $offset, $perpage);
					//$allSystemUserCount = count($allSystemUserArr);
				  }else{
				    $allSystemUserArr = $allUserArr['users'];

				  }
				  
				 //pr( $allSystemUserArr);die;
				  foreach($allSystemUserArr as $data){
					
						$assestHTML = '';
						$userIid = $data->userid;
						
				        $fullName = $data->userfullname;
						$userName = $data->username;
						
						$onlineCreditHours = isset($data->online_credithours) && $data->online_credithours >=0?$data->online_credithours:$mDash;
						$onlineTotalCourses = isset($data->online_course_count) && $data->online_course_count >=0?$data->online_course_count:0;
						
						$classroomCreditHours = isset($data->classroom_credithours) && $data->classroom_credithours >=0?$data->classroom_credithours:$mDash;
						$classroomTotalCourses = isset($data->classroom_course_count) && $data->classroom_course_count >=0?$data->classroom_course_count:0;
						
						
						$creditHours = isset($data->online_credithours) && $data->online_credithours >=0?($data->online_credithours + $data->classroom_credithours):$mDash;
						$totalCourses = isset($data->online_course_count) && $data->online_course_count >=0?($data->online_course_count + $data->classroom_course_count):0;
						
						
						$onlineCreditHoursCSV = isset($data->online_credithours) && $data->online_credithours >=0?$data->online_credithours:$mDashCSV;
						$onlineTotalCoursesCSV = isset($data->online_course_count) && $data->online_course_count >=0?$data->online_course_count:0;
						
						$classroomCreditHoursCSV = isset($data->classroom_credithours) && $data->classroom_credithours >=0?$data->classroom_credithours:$mDashCSV;
						$classroomTotalCoursesCSV = isset($data->classroom_course_count) && $data->classroom_course_count >=0?$data->classroom_course_count:0;
						
						$creditHoursCSV = isset($data->online_credithours) && $data->online_credithours >=0?($data->online_credithours + $data->classroom_credithours):$mDashCSV;
						$totalCoursesCSV = isset($data->online_course_count) && $data->online_course_count >=0?($data->online_course_count + $data->classroom_course_count):0;
						
						$userManager = $data->usermanager?$data->usermanager:$mDash;
						$departmentTitle = $data->departmenttitle?$data->departmenttitle:$mDash;
						$userManagerCSV = $data->usermanager?$data->usermanager:$mDashCSV;
						$departmentTitleCSV = $data->departmenttitle?$data->departmenttitle:$mDashCSV;
						$departmentId = $data->departmentid;
						
						//$teamTitle = str_replace(",",",<br>",$data->teamtitle);
						//$createdById = $data->createdby;
						$user->id = $userIid;
						
						$creditHours = setCreditHoursFormat($creditHours);
						$creditHoursCSV = setCreditHoursFormat($creditHoursCSV);
						$onlineCreditHoursCSV = setCreditHoursFormat($onlineCreditHoursCSV);
						$classroomCreditHoursCSV = setCreditHoursFormat($classroomCreditHoursCSV);
						$onlineCreditHours = setCreditHoursFormat($onlineCreditHours);
						$classroomCreditHours = setCreditHoursFormat($classroomCreditHours);
						
		
						//$imagevalue = $OUTPUT->user_picture($user, array('size'=>64));
						
									
						
						if(in_array($CFG->userTypeStudent, $sUserTypeArr) || ($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2)){		
						
							 
							  if(empty($export) || (!empty($export) && $export == 'print')){
									$userHTML .=  '<tr>';
								  
										 if(!empty($export)){
										  $userHTML .=  '<td>'.$fullName.'</td>';
										  $userHTML .=  '<td>'.$userName.'</td>';
										 }else{
										
										  $linkUrlDetails = str_replace(array(":userId"), array($userIid), $linkToDetailsURL);
										  $userHTML .=  '<td><a href="'.$linkUrlDetails.'" >'.$fullName.'</a></td>';
										  $userHTML .=  '<td><a href="'.$linkUrlDetails.'" >'.$userName.'</a></td>';
										 }
	
											 $userHTML .= '<td>'.$onlineTotalCourses.'</td>';
											 $userHTML .= '<td>'.$classroomTotalCourses.'</td>';
											 $userHTML .= '<td>'.$totalCourses.'</td>';
											 $userHTML .= '<td>'.$onlineCreditHours.'</td>';
											 $userHTML .= '<td>'.$classroomCreditHours.'</td>';
											 $userHTML .= '<td>'.$creditHours.'</td>';
											 $userHTML .=  '<td>'.$userManager.'</td>';
											 $userHTML .=  '<td>'.$departmentTitle.'</td>';
									  $userHTML .=  '</tr>';
								}elseif((!empty($export) && $export == 'exportpdf')){
									  $reportContentPDF .= '<tr '.$CFG->pdfTableRowAttribute.' >';
										 $reportContentPDF .= '<td>'.$fullName.'</td>';
										 $reportContentPDF .= '<td>'.$userName.'</td>';
										 $reportContentPDF .= '<td>'.$onlineTotalCourses.'</td>';
										 $reportContentPDF .= '<td>'.$classroomTotalCourses.'</td>';
										 $reportContentPDF .= '<td>'.$totalCourses.'</td>';
										 $reportContentPDF .= '<td>'.$onlineCreditHours.'</td>';
										 $reportContentPDF .= '<td>'.$classroomCreditHours.'</td>';
										 $reportContentPDF .= '<td>'.$creditHours.'</td>';
										 $reportContentPDF .= '<td>'.$userManager.'</td>';
										 $reportContentPDF .= '<td>'.$departmentTitle.'</td>';
										//$reportContentPDF .= '<td >'.$teamTitle.'</td>';
									  $reportContentPDF .= '</tr>';  	
								}elseif((!empty($export) && $export == 'exportcsv')){
								     $reportContentCSV .= $fullName.",".$userName.",".$onlineTotalCoursesCSV.",".$classroomTotalCoursesCSV.",".$totalCoursesCSV.",".$onlineCreditHoursCSV.",".$classroomCreditHoursCSV.",".$creditHoursCSV.",".$userManagerCSV.",".$departmentTitleCSV ."\n"; 
								}	

						}else{

							  if(empty($export) || (!empty($export) && $export == 'print')){
									 $userHTML .=  '<tr>';
										 if(!empty($export)){
										  $userHTML .=  '<td>'.$fullName.'</td>';
										  $userHTML .=  '<td>'.$userName.'</td>';
										 }else{
										
										  $linkUrlDetails = str_replace(array(":userId"), array($userIid), $linkToDetailsURL);
										  $userHTML .=  '<td><a href="'.$linkUrlDetails.'" >'.$fullName.'</a></td>';
										  $userHTML .=  '<td><a href="'.$linkUrlDetails.'" >'.$userName.'</a></td>';
										 }
										 $userHTML .=  '<td>'.$totalCourses.'</td>';
										 $userHTML .=  '<td>'.$creditHours.'</td>';
										 $userHTML .=  '<td>'.$userManager.'</td>';
										 $userHTML .=  '<td>'.$departmentTitle.'</td>';
									 $userHTML .=  '</tr>';
							  }elseif((!empty($export) && $export == 'exportpdf')){ 

								 $reportContentPDF .= '<tr '.$CFG->pdfTableRowAttribute.' >';
									 $reportContentPDF .= '<td>'.$fullName.'</td>';
									 $reportContentPDF .= '<td>'.$userName.'</td>';
									 $reportContentPDF .= '<td>'.$totalCourses.'</td>';
									 $reportContentPDF .= '<td>'.$creditHours.'</td>';
									 $reportContentPDF .= '<td>'.$userManager.'</td>';
									 $reportContentPDF .= '<td>'.$departmentTitle.'</td>';
									//$reportContentPDF .= '<td >'.$teamTitle.'</td>';
								  $reportContentPDF .= '</tr>';
							  }elseif((!empty($export) && $export == 'exportcsv')){
							     $reportContentCSV .= $fullName.",".$userName.",".$totalCoursesCSV.",".$creditHoursCSV.",".$userManagerCSV.",".$departmentTitleCSV ."\n";
							  }
							 
							 
						}	

					}
                    
					if((!empty($export) && $export == 'exportpdf')){ 
					  $reportContentPDF .= '</table>';
					}
					
					if(empty($export) || (!empty($export) && $export == 'print')){
						$userHTML .=  '<script language="javascript" type="text/javascript">';
						$userHTML .=  '	$(document).ready(function(){
										
												$("#printbun").bind("click", function(e) {
													var url = $(this).attr("rel");
													window.open(url, "'.$headerLabelGraph.'", "'.$CFG->printWindowParameter.'");
												});
												
									
										 });
										 
										  ';

					  
						  if($totalGraphCreditHours > 0 ){
								$graphtLabelHtml = '';
								$userHTML .=  ' window.onload = function () {
	
														var chart = new CanvasJS.Chart("chartContainer", {
																	  theme: "theme2",//theme1
																	  title:{
																		  /*text: "'.$graphTitle.'" */
																	 },
																	  data: [              
																	  {
																		  // Change type to "bar", "splineArea", "area", "spline", "pie","column",  "doughnut" etc.
																		  type: "doughnut",
																		  indexLabelFontFamily: "Arial",       
																		  indexLabelFontSize: 12,
																		  startAngle:0,
																		  indexLabelFontColor: "dimgrey",       
																		  indexLabelLineColor: "darkgrey", 
																		  toolTipContent: "{y}%",
																		  //indexLabelPlacement: "inside",
																		  '; 
																		  
																	
																		  $userHTML .=  '  dataPoints: [ ';
																							  
																						   if(in_array($CFG->userTypeStudent, $sUserTypeArr) || ($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2)){			
																							 
																								$graphtLabelHtml .= '{  y: '.$CCCreditHourPercentage.', label: "'.get_string('classroomcourse').' ('.$CCreditHours.')", exploded: false },';
																								$graphtLabelHtml .= '{  y: '.$OCCreditHourPercentage.', label: "'.get_string('onlinecourse').' ('.$OCreditHours.')", exploded: false },';
																							  
																							}else{
																								$graphtLabelHtml .= '{  y: '.$CCCreditHourPercentage.', label: "'.get_string('classroomcourse').' ('.$CCreditHours.')", exploded: false },';
																							} 
																							
																		  $userHTML .= 	$graphtLabelHtml;		
																		  $userHTML .=  '  ]
																	  }
																	  ]
																  });
														
																  chart.render();' ;
					  
									
														
																		 
									$userHTML .=  '  } ';
									$userHTML .=  '</script>';	
									$userHTML .=  '<script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js"></script>';
											
								 
							 }else{
							   
							   $userHTML .=  '</script>';	
							 
							 }
						 
					}	 

				}else{
				    if(empty($export) || (!empty($export) && $export == 'print')){
					   $userHTML .=  '<tr><td colspan="10" style="border-bottom:1px solid #b9b9b9!important;">'.get_string('norecordfound','multiuserreport').'</td></tr>';
					}elseif((!empty($export) && $export == 'exportpdf')){ 
					   $reportContentPDF .= get_string('norecordfound','multiuserreport')."\n";
					}elseif((!empty($export) && $export == 'exportcsv')){ 
					   $reportContentCSV .= get_string('norecordfound','multiuserreport')."\n";
					}
				}
				
			if(empty($export) || (!empty($export) && $export == 'print')){	
			  $userHTML .= '</tbody></table></div></div></div>';
			}
		
		    if(empty($export)){
			  $userHTML .= paging_bar($allSystemUserCount, $page, $perpage, $genURL);
			 } 
			
			$userCHReport->userHTML = $userHTML;
			$userCHReport->reportContentCSV = $reportContentCSV;
			$userCHReport->reportContentPDF = $reportContentPDF;
		
		   return $userCHReport;
	  }
	  
	   /**
	 * This function is using for getting credit hours format in hours
	 * @param int $creditHours in minutes
	 * @param int $sep is a seperator
	  * @return array $creditHoursFormatted return formatted credit hours in minute to hours
	 */
	 
	  function setCreditHoursFormat($creditHours, $sep=":"){
	     $creditHoursFormatted = $creditHours;
	     if($creditHours>=0){ 
		     $creditHoursInSec = $creditHours*60;
			 $creditHoursFormatted = sprintf("%02d%s%02d", floor($creditHoursInSec/3600), $sep, ($creditHoursInSec/60)%60); 
		 }
		 
		 return $creditHoursFormatted;
	  }
	  
	  
  
	  
	   /**
	 * This function is using for getting a single learner course credit hour report
	 * @global object
	 * @param array $paramArray is search array
	 * @param array $removeKeyArray contain search key that you don't want to add in address bar
	 * @param string $sort is sorting field
	 * @param string $dir is sorting order
	 * @param int $page current page
	 * @param int $perpage is number of records per page
	 * @param string $export is action variable for PDF download, CSV download, and Print a single learner report
	  * @return array $userCCHReport return CSV , PDF and Report Content
	 */
	  
	  
	  function getUserCourseCreditHoursReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export=''){
		
		  //ini_set('display_errors', 1);
  	        //error_reporting(E_ALL);
		
	        global $DB, $CFG, $USER, $SITE;
			$loginUserId = $USER->id;
			$isReport = true;
			$userCCHReport = new stdClass();
			
			$offset = $page - 1;
			$offset = $offset*$perpage;
			$limit = '';
			if($perpage != 0){
				$limit = "LIMIT $offset, $perpage";
			}

			////// Getting common URL for the Search //////
			$pageURL = $_SERVER['PHP_SELF'];
			$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
			
			$printUrl = '/user/user_course_credithours_report_print.php';

			$genPrintURL = genParameterizedURL($paramArray, $removeKeyArray, $printUrl);
				  
	        $userId    = $paramArray['uid'];
	        $sDepartment    = $paramArray['department'];
			//$sTeam          = $paramArray['team'];
			//$sCourse        = $paramArray['course'];
			//$sType          = $paramArray['type'];
			$sStartDate     = $paramArray['startDate'];
			$sEndDate       = $paramArray['endDate'];
			$sBack       = $paramArray['back'];
			$sUserType       = $paramArray['userType'];
			$sUser       = $paramArray['user'];
			
			$isUserDeleted = isUserDeleted($userId);
			if($isUserDeleted){
				redirect("/");
			}
			
			$sUserTypeArr = explode("@",$sUserType);
			//pr($sUserTypeArr);die;
			if($sUserTypeArr[0] == '-1' || count($sUserTypeArr) > 2 ){
			  $sUserTypeName = get_string('all','multiuserreport');
			}else{
			  $sUserTypeName = implode(", ",array_map('ucfirst', $sUserTypeArr));
			}
		
			
	
			$sDateSelected = '';
			if($sStartDate){
				$sDateArr = getFormattedTimeStampOfDate($sStartDate);
			    $sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

			}		   
			
			$eDateSelected = '';
			if($sEndDate){
			   $eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
			   $eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
			}
			
			$sStartDateTime = '';
			$sEndDateTime = '';
			if($sDateSelected){ 
				//list($month, $day, $year) = explode('/', $sDateSelected);
				//$sStartDateTime =  mktime(0, 0, 0, $month, $day, $year);
				$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
			}
			
			if($eDateSelected){
			   //list($month, $day, $year) = explode('/', $eDateSelected);
			   //$sEndDateTime = mktime(23, 59, 59, $month, $day, $year);
			   $sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
			}
			
			
			$curTime = time();	
			$searchStringI ='';
			$searchStringL ='';
			if($sDateSelected){ 
				//list($month, $day, $year) = explode('/', $sDateSelected);
				//$sStartDateTime =  mktime(0, 0, 0, $month, $day, $year);
				$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
				
				if($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2 ){
				
				  //$searchStringI .= " AND class_starttime > $sStartDateTime ";
				  $searchStringI .= " AND (class_starttime >= ($sStartDateTime) || (class_starttime is null))";
				  $searchStringL .= " AND (complition_time >= ($sStartDateTime) || (complition_time is null))";	
				  
				}elseif(in_array($CFG->userTypeInstructor, $sUserTypeArr)){
				  //$searchStringI .= " AND class_endtime > $sEndDateTime ";
				  $searchStringI .= " AND (class_endtime >= ($sStartDateTime) || (class_endtime is null))";
			    }elseif(in_array($CFG->userTypeStudent, $sUserTypeArr)){
				   $searchStringL .= " AND (complition_time >= ($sStartDateTime) || (complition_time is null))";	
			    }
				/*if($sUserType == $CFG->userTypeInstructor){
				  $searchString .= " AND class_endtime < $sStartDateTime ";
				  $searchString .= " AND (class_endtime >= ($sStartDateTime) || (class_endtime is null))";
				}else{
				  $searchString .= " AND (complition_time >= ($sStartDateTime) || (complition_time is null))";				
				}*/
			}
			
			
			if($eDateSelected){
			  // list($month, $day, $year) = explode('/', $eDateSelected);
			  // $sEndDateTime = mktime(23, 59, 59, $month, $day, $year);
			   $sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
			   
			   if($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2 ){
				
				  //$searchStringI .= " AND class_endtime < $sEndDateTime ";
				  $searchStringI .= " AND (class_endtime <= ($sEndDateTime) || (class_endtime is null))";
				  $searchStringL .= " AND (complition_time <= ($sEndDateTime) || (complition_time is null))";
				  
			    }elseif(in_array($CFG->userTypeInstructor, $sUserTypeArr)){
				   //$searchStringI .= " AND class_endtime < $sEndDateTime ";
				   $searchStringI .= " AND (class_endtime <= ($sEndDateTime) || (class_endtime is null))";
			    }elseif(in_array($CFG->userTypeStudent, $sUserTypeArr)){
				   $searchStringL .= " AND (complition_time <= ($sEndDateTime) || (complition_time is null))";	
			    }
	
			}
				
				

			$sDepartmentArr = explode("@",$sDepartment);
			$userName = getUsers($userId, 1);
					
			if($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2 ){
			  $recordArr = getOverallUserCourseCreditReportForLnI($searchStringI,$searchStringL, $paramArray, $sort, $dir);
			}else{
			  $recordArr = getOverallUserCourseCreditReport($sUserType, $searchStringI, $searchStringL, $paramArray, $sort, $dir);
			  //pr($recordArr);die;
			}
			
			
			$crediHoursForGraph = $recordArr['crediHoursForGraph'];
			$allCourses = $recordArr['allCourses'];
			$allCoursesCnt = $recordArr['allCoursesCnt'];
			
			//pr($allCourses);die;
			$onlineCreditHours = isset($crediHoursForGraph[1])?$crediHoursForGraph[1]:0;
		    $classroomCreditHours = isset($crediHoursForGraph[2])?$crediHoursForGraph[2]:0;
		    $totalCreditHours = $onlineCreditHours + $classroomCreditHours;	
			
            $OCCreditHourPercentage = numberFormat(($onlineCreditHours*100)/$totalCreditHours);
			$CCCreditHourPercentage = numberFormat(($classroomCreditHours*100)/$totalCreditHours);
			
			$OCreditHours = setCreditHoursFormat($onlineCreditHours);
			$CCreditHours = setCreditHoursFormat($classroomCreditHours);
			//pr($crediHoursForGraph);die;
			//$allCourses = getAllCompletedCourseOfUser($userId);
			//$allCoursesCnt = count($allCourses);
			
		

			$HTML = '';
			
			if(empty($export) || (!empty($export) && $export == 'print')){
				$style = (!empty($export) || strstr($_SERVER['REQUEST_URI'], 'user/user_course_credithours_report.php'))?"style=''":'';
				$HTML .= "<div class='tabsOuter borderBlockSpace' ".$style." >";
				$HTML .= '<div class="clear"></div>';
			}
			
            $exportHTML = '';
            if(empty($export)){
				
				ob_start();
				require_once($CFG->dirroot . '/local/includes/usercoursecredithoursreportsearch.php');
				$HTML .= ob_get_contents();
				ob_end_clean();
				
				if($allCoursesCnt > 0 ){
					$exportHTML .= '<div class="exports_opt_box"> ';
					$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','singlereport').'" href="'.$genPrintURL.'&action=exportcsv">'.get_string('downloadcsv','singlereport').'</a> ';
					$exportHTML .= '<span class="seperater">&nbsp;</span>';
					$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','singlereport').'" id="exportpdf1" href="'.$genPrintURL.'&action=exportpdf">'.get_string('downloadpdf','singlereport').'</a> ';
					$exportHTML .= '<span class="seperater">&nbsp;</span>';
					$exportHTML .= '<a rel="'.$genPrintURL.'&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','singlereport').'" class="print_icon">'.get_string('print','singlereport').'</a>';
					$exportHTML .= '</div>';
				}
				
				
			
			}
			
		   if(empty($export) || (!empty($export) && $export == 'print')){	 
			
			    $HTML .= '<div class="clear"></div>';
				if(empty($export) && !strstr($_SERVER['REQUEST_URI'], 'user/user_course_credithours_report.php') ){
					ob_start();
					######### this tab already have admin role condition,  ######### 
					#########  so we don't have to put any condition here  ######### 
					#########  to show this tab only in admin panel        ######### 
					
					$user->id = $userId;
					include_once('user_tabs.php');
					
					$HTMLTabs .= ob_get_contents();
					ob_end_clean();
					
					$HTML .= $HTMLTabs;
					
					$HTML .= "</div>";
				 }
			
				$HTML .= '<div class="clear"></div>';
				$HTML .= '<div class="userprofile view_assests">';
				
			}
			
			$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
			$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
			$sDateSelectedForPrintCSV = getDateFormatForPrint($sDateSelected, 'csv');
			$eDateSelectedForPrintCSV = getDateFormatForPrint($eDateSelected, 'csv');
			
			if($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2 ){
				$headerLabelGraph = get_string('usercredithoursreport');
				$graphTitle = '';
			}elseif(in_array($CFG->userTypeInstructor, $sUserTypeArr)){
				$headerLabelGraph = get_string('overallinstructorcoursecredithoursreport');
				$graphTitle = '';
			}elseif(in_array($CFG->userTypeStudent, $sUserTypeArr)){
				$headerLabelGraph = get_string('overalllearnercoursecredithoursreport');
				$graphTitle = '';
			}
			
			if((!empty($export) && $export == 'print')){
				
			$reportName = $headerLabelGraph;
			$HTML .= getReportPrintHeader($reportName);
				
				
			 $HTML .= '<table border="0" cellpadding="0" cellspacing="0"  class="generaltable margin_bottom">';
				$HTML .= '<tr>';
					$HTML .= '<td><strong>'.get_string('user','singlereport').':</strong> '.$userName.'</td>';
					$HTML .= '<td><strong>'.get_string('usertype').':</strong> '.$sUserTypeName.'</td>';
				$HTML .= '</tr>';
				
				$HTML .= '<tr>';
					$HTML .= '<td  width="500" ><strong>'.get_string('credithourscomletiondate').':</strong> '.$sDateSelectedForPrint.'</td>';
					$HTML .= '<td  width="500" ><strong>'.get_string('credithourscomletiondateto').':</strong>  '.$eDateSelectedForPrint.'</td>';				
			    $HTML .= '</tr>';
			
			 $HTML .= '</table>';
			
			}

			$userCourseStatusArr = array();
			$totalAssignedCoursesAll = 0;
			$inProgressCoursesAll = 0;
			$completedCoursesAll = 0;
			$notStartedCoursesAll = 0;

			$NSCourseIds = array();
			$IPCourseIds = array();
			$ClCourseIds = array();
			$ByTypeCourseIds = array();


	        if(empty($export) || (!empty($export) && $export == 'print')){
			
				if($allCoursesCnt > 0 && $totalCreditHours > 0 ){
				
					  $HTML .= '<div class = "single-report-start" id="watch">
									<div class = "single-report-graph-box"> <span class="main-heading">'.$headerLabelGraph.$exportHTML.'</span>
									   <div class="single-report-graph-left" id="chartContainer">'.get_string('loading','multiuserreport').'</div>
										<div class="single-report-graph-right">
										<div class="course-count-heading">'.get_string('coursecredithourswithtime').'</div>
										<div class="course-count">'.(setCreditHoursFormat($totalCreditHours)).'</div>
										<div class="seperator">&nbsp;</div>
										<div class="course-status">';
										
										if(in_array($CFG->userTypeStudent, $sUserTypeArr) || ($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2)){		
										 $HTML .= ' <div class="publish-deactive"><h6>'.get_string('onlinecourse').'</h6><span>'.$OCreditHours.'</span></div>
										  <div class="clear"></div>
										  <div class="publish-active"><h6>'.get_string('classroomcourse').'</h6><span>'.$CCreditHours.'</span></div>';		
										 
										} else{
										   $HTML .= '<div class="publish-active"><h6>'.get_string('classroomcourse').'</h6><span>'.$CCreditHours.'</span></div>';
										}
										$HTML .= '</div>
									  </div>
									</div>
									
								</div>
								<div class="clear"></div>';
								 
					  
			   }else{
				 
					  $HTML .= '<div class = "single-report-start-nograph" id="watch" >
										<div class = "single-report-graph-box"> <span class="main-heading">'.$headerLabelGraph.$exportHTML.'</span>
										  
										</div>
								 </div>
								 <div class="clear"></div>';
			   
			   } 
		   
		   }
			
			
			if(empty($export) || (!empty($export) && $export == 'print')){
				
				$styleSheet = $allCoursesCnt?'':'style=""';  
				$HTML .= '<div class = "clear">';
				$HTML .= '<table class="generaltable" cellpadding="0" border="0" cellspacing="0" >
				<tr class="table1_head">
				<th width="30%">'.get_string("coursetitle","singlereport").'</th>
				<th width="15%">'.get_string("coursecredithourswithtime").'</th>
				<th width="15%">'.get_string('coursetype','course').'</th>
				<th width="20%">'.get_string("completiondate").'</th>
				</tr>';
			}
            
			$reportContentCSV = '';
			if((!empty($export) && $export == 'exportcsv')){
				$reportContentCSV .= get_string('user','singlereport').",".$userName."\n";
				$reportContentCSV .= get_string('usertype').",".$sUserTypeName."\n";
				$reportContentCSV .= get_string('credithourscomletiondate').",".$sDateSelectedForPrintCSV."\n".get_string('credithourscomletiondateto').",".$eDateSelectedForPrintCSV."\n";
			}
			
			
			$reportContentPDF = '';
			
			if((!empty($export) && $export == 'exportpdf')){
				$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" class="generaltable"><tr><td colspan="4"></td></tr>';
					$reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td width="50%" ><strong>'.get_string('user','singlereport').':</strong> '.$userName.'</td>';
						$reportContentPDF .= '<td width="50%" ><strong>'.get_string('usertype').':</strong> '.$sUserTypeName.'</td>';
					$reportContentPDF .= '</tr>';
				$reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td  width="50%" ><strong>'.get_string('credithourscomletiondate').':</strong> '.$sDateSelectedForPrint.'</td>';
						$reportContentPDF .= '<td  width="50%" ><strong>'.get_string('credithourscomletiondateto').':</strong>  '.$eDateSelectedForPrint.'</td>';				
				$reportContentPDF .= '</tr>';
			}
			
			if((!empty($export) && $export == 'exportcsv')){
			  $reportContentCSV .= get_string('coursecredithourswithtime').",".(setCreditHoursFormat($totalCreditHours))."\n";			
			}
			
			
			
			if(in_array($CFG->userTypeStudent, $sUserTypeArr) || ($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2)){	
			
			    if((!empty($export) && $export == 'exportpdf')){
					$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td><strong>'.get_string('coursecredithourswithtime').':</strong> '.(setCreditHoursFormat($totalCreditHours)).'</td>'; 	
					$reportContentPDF .= '<td><strong>'.get_string('onlinecourse').':</strong> '.$OCreditHours.' ('.floatval($OCCreditHourPercentage).'%)</td>';
					$reportContentPDF .= '</tr>';
					$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td colsapn="2" ><strong>'.get_string('classroomcourse').':</strong> '.$CCreditHours.' ('.floatval($CCCreditHourPercentage).'%)</td>';
					$reportContentPDF .= '</tr>';
				}
				
				if((!empty($export) && $export == 'exportcsv')){
				    $reportContentCSV .= get_string('onlinecourse').",".$OCreditHours." (".floatval($OCCreditHourPercentage)."%)\n";
				    $reportContentCSV .= get_string('classroomcourse').",".$CCreditHours." (".floatval($CCCreditHourPercentage)."%)\n";
				}
					  
				
			}else{	
					
				if((!empty($export) && $export == 'exportpdf')){	
					$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td><strong>'.get_string('coursecredithourswithtime').':</strong> '.(setCreditHoursFormat($totalCreditHours)).'</td>'; 	
					$reportContentPDF .= '<td><strong>'.get_string('classroomcourse').':</strong> '.$CCreditHours.' ('.floatval($CCCreditHourPercentage).'%)</td>';
					$reportContentPDF .= '</tr>';
				}
				
				if((!empty($export) && $export == 'exportcsv')){
				  $reportContentCSV .= get_string('classroomcourse').",".$CCreditHours." (".floatval($CCCreditHourPercentage)."%)\n";
				}
						
			}	
			
			if((!empty($export) && $export == 'exportcsv')){
			  $reportContentCSV .= $headerLabelGraph."\n";
		      $reportContentCSV .= get_string('coursetitle','singlereport').",".get_string("coursecredithourswithtime").",".get_string('coursetype','course').",".get_string('completiondate')."\n";
			}
			
			if((!empty($export) && $export == 'exportpdf')){
				
				if( $totalCreditHours == 0){
				   $reportContentPDF .= '<tr><br /><td width="100%"  ><span '.$CFG->pdfSpanAttribute.'><strong>'.$headerLabelGraph.'</strong></span><br /></td></tr>';
				}
				 $reportContentPDF .= '</table>';
				 
				 if( $totalCreditHours){
				 	$reportContentPDF .= getGraphImageHTML($headerLabelGraph);
				 }
				 
				 $reportContentPDF .= '<table '.$CFG->pdfTableStyle .'>';
				 $reportContentPDF .= '<tr '.$CFG->pdfTableHeaderStyle.'>';
				 $reportContentPDF .= '<td><strong>'.get_string('coursetitle','singlereport').'</strong></td>';
				 $reportContentPDF .= '<td><strong> '.get_string('coursecredithourswithtime').'</strong></td>';
				 $reportContentPDF .= '<td><strong> '.get_string('coursetype','course').'</strong></td>';
				 $reportContentPDF .= '<td><strong>'.get_string('completiondate').'</strong></td>';
				 $reportContentPDF .= '</tr>';
			}
			// pr($allCourses);die;
			
						  
			if($allCoursesCnt > 0){
			
				$rowCount=0;
				$crCnt = 0;
				$curtime = time();
			
				if($perpage){
				  $offset = $page - 1;
				  $offset = $offset*$perpage;
				 
				  $allCourses = array_slice($allCourses, $offset, $perpage, true);
				}
        
		
		        //pr($allCourses);die;
                //$j = 0; 
				foreach($allCourses as $courses){
			
					
					
					$creditHours = 0;
					
					//$classmMain = 'evenR';
					$courseId = $courses->courseid;
					$cfullname = $courses->coursename;
					
					$courseTypeId = $courses->coursetype_id;
					$courseType = $courses->coursetype;
								
					$completionDate = $courseTypeId == $CFG->courseTypeClassroom?'':(getDateFormat($courses->complition_time, $CFG->customDefaultDateFormat));
					$completionDateCSV = $courseTypeId == $CFG->courseTypeClassroom?'':(getDateFormat($courses->complition_time, $CFG->customDefaultDateFormatForCSV));
					$complianceDiv = getCourseComplianceIcon($courseId);
					
					$creditHours = $courses->credithours?$courses->credithours:0;
					$creditHoursGraph = $courses->credithours?$courses->credithours:0;
					$creditHours = $courseTypeId == $CFG->courseTypeClassroom?'':(setCreditHoursFormat($creditHours));
					$creditHoursCSV = $creditHours;

				
					if($courseTypeId == $CFG->courseTypeClassroom){

						$crCnt++;
						$bgColor = $crCnt > 1 ?'style=""':'';
						
						
					   $classHTML = '';
					   $classHTMLPDF = '';
					   $classContentCSV = '';
					   //pr($allCourses[$courseId]);die;
					   if(isset($allCourses[$courseId]->class_details) && count($allCourses[$courseId]->class_details) > 0 ){

							   foreach($allCourses[$courseId]->class_details as $ccuArr){   
							       $rowCount++; 
							       $className = $ccuArr['class_name'];
								   $classCreditHours = $ccuArr['credithours']?$ccuArr['credithours']:0;
								   $classType = getMDash();
								   $classTypeCSV = getMDashForCSV();
							   
								   $classCompletionDate = getDateFormat($ccuArr['complition_time'], $CFG->customDefaultDateFormat);
					               $classCompletionDateCSV = getDateFormat($ccuArr['complition_time'], $CFG->customDefaultDateFormatForCSV);
					
								   $classCreditHours = setCreditHoursFormat($classCreditHours);
								   $classCreditHoursCSV = $classCreditHours;
								   
							     //  $j++;
								   //$altClass = $j%2==0?'even':'odd';
								   //$altClassPDF = $j%2==0?$CFG->pdfInnerTableOddRowStyle:$CFG->pdfInnerTableEvenRowStyle;
								   
								   $classmMain = $rowCount%2 == 0?'evenR':'oddR';
					               $classmMainPDF = $rowCount%2 == 0?$CFG->pdfTableOddRowStyle:$CFG->pdfTableEvenRowStyle;
						
								 
									if(empty($export) || (!empty($export) && $export == 'print')){		 
										$classHTML .= '<tr class="'.$classmMain.'" >
										<td '.$bgColor.' >'.$cfullname.' - '.$className.'</td>
										<td '.$bgColor.'  >'.$classCreditHours.'</td>
										<td '.$bgColor.'  >'.$courseType.'</td>
										<td '.$bgColor.'  >'.$classCompletionDate.'</td>
										
										</tr>';			 
									}elseif((!empty($export) && $export == 'exportpdf')){		 
									   $classHTMLPDF .= '<tr '.$classmMainPDF.' >
											<td>'.$cfullname.' - '.$className.'</td>
											<td>'.$classCreditHours.'</td>
											<td>'.$courseType.'</td>
											<td>'.$classCompletionDate.'</td>
											</tr>';
								    }elseif((!empty($export) && $export == 'exportcsv')){				
									   $classContentCSV .= $cfullname.' - '.$className.",".$classCreditHoursCSV.",".$courseType.",".($classCompletionDateCSV?$classCompletionDateCSV:getMDashForCSV())."\n";				
									}
											 
								}
						 }
							
							if(empty($export) || (!empty($export) && $export == 'print')){
							  $HTML .= $classHTML;
							}elseif((!empty($export) && $export == 'exportpdf')){	
							  $reportContentPDF .= $classHTMLPDF;
							}elseif((!empty($export) && $export == 'exportcsv')){	
							  $reportContentCSV .= $classContentCSV;
							}
						 
					}else{
					        $rowCount++;
                            $classmMain = $rowCount%2 == 0?'evenR':'oddR';
					        $classmMainPDF = $rowCount%2 == 0?$CFG->pdfTableOddRowStyle:$CFG->pdfTableEvenRowStyle;
							
							
							
							if(empty($export) || (!empty($export) && $export == 'print')){
								$HTML .= '<tr class="'.$classmMain.'">
											<td>'.$cfullname.$complianceDiv.'</td>
											<td>'.$creditHours.'</td>
											<td>'.$courseType.'</td>
											<td>'.$completionDate.'</td>
									  </tr>';
							}elseif((!empty($export) && $export == 'exportpdf')){	
								$reportContentPDF .= '<tr '.$classmMainPDF.' >';
								$reportContentPDF .= '<td>'.$cfullname.'</td>';
								$reportContentPDF .= '<td>'.$creditHours.'</td>';
								$reportContentPDF .= '<td>'.$courseType.'</td>';
								$reportContentPDF .= '<td>'.($completionDate?$completionDate:getMDash()).'</td>';
								$reportContentPDF .= '</tr>';
							}elseif((!empty($export) && $export == 'exportcsv')){	
							    $reportContentCSV .= $cfullname.",".$creditHoursCSV.",".$courseType.",".($completionDateCSV?$completionDateCSV:getMDashForCSV())."\n";
							}

					 }

				}
	
			}else{
			      if(empty($export) || (!empty($export) && $export == 'print')){
				    $HTML .=  '<tr><td colspan="4" align="center" >'.get_string('no_results').'</td></tr>';
				  }elseif((!empty($export) && $export == 'exportpdf')){	
				    $reportContentPDF .= get_string('norecordfound','singlereport')."\n";
				  }elseif((!empty($export) && $export == 'exportcsv')){
				    $reportContentCSV .= get_string('norecordfound','singlereport')."\n";
				  }

			}
			
			if((!empty($export) && $export == 'exportpdf')){	
			  $reportContentPDF .= '</table>';
			}
			
			if(empty($export) || (!empty($export) && $export == 'print')){
			  $HTML .= '</tbody></table></div></div></div>';
	 
			   $HTML .=  '<script language="javascript" type="text/javascript">';
					$HTML .=  '	$(document).ready(function(){
									
											$("#printbun").bind("click", function(e) {
												var url = $(this).attr("rel");
												window.open(url, "'.$headerLabelGraph.'", "'.$CFG->printWindowParameter.'");
											});
											
								
									 });
									 
									
									 
									  ';
                      		 
	                  if($totalCreditHours > 0 ){

					        $graphtLabelHtml = '';
							$HTML .=  ' window.onload = function () {

													var chart = new CanvasJS.Chart("chartContainer", {
																  theme: "theme2",//theme1
																  title:{
																	  text: "'.$graphTitle.'"              
																 },
																  data: [              
																  {
																	  // Change type to "bar", "splineArea", "area", "spline", "pie","column",  "doughnut" etc.
																	  type: "doughnut",
																	  indexLabelFontFamily: "Arial",       
																	  indexLabelFontSize: 12,
																	  startAngle:0,
																	  indexLabelFontColor: "dimgrey",       
																	  indexLabelLineColor: "darkgrey", 
																	  toolTipContent: "{y}%",
																	  //indexLabelPlacement: "inside",
																	  '; 
																	  
																
																	  $HTML .=  '  dataPoints: [ ';
																						  
																					  // pr($creditHoursArr); 
																					  // pr($deptsArr);die; 
																					  
																					 if(in_array($CFG->userTypeStudent, $sUserTypeArr) || ($sUserTypeArr[0] == '-1' || count($sUserTypeArr) >= 2)){	
																					 	$graphtLabelHtml .= '{  y: '.$CCCreditHourPercentage.', label: "'.get_string('classroomcourse').' ('.$CCreditHours.')", exploded: false },';
																					    $graphtLabelHtml .= '{  y: '.$OCCreditHourPercentage.', label: "'.get_string('onlinecourse').' ('.$OCreditHours.')", exploded: false },';
																					  }else{
																					    
																					     $graphtLabelHtml .= '{  y: '.$CCCreditHourPercentage.', label: "'.get_string('classroomcourse').' ('.$CCreditHours.')", exploded: false },';
																					  }
																					    
																						 
																						
																	  $HTML .= 	$graphtLabelHtml;		
																	  $HTML .=  '  ]
																  }
																  ]
															  });
													
															  chart.render();' ;
				  
								
													
																	 
								$HTML .=  '  } ';
								$HTML .=  '</script>';	
								$HTML .=  '<script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js"></script>';
										
						     
						 }else{
						   
						   $HTML .=  '</script>';	
						 
						 }
			}	
					 				 
            if(empty($export)){ 
				$HTML .= paging_bar($allCoursesCnt, $page, $perpage, $genURL);
				if( $USER->archetype != $CFG->userTypeStudent ) {
			       $HTML .= '<div id="backcell"><input type = "button" value = "'.get_string('back','multiuserreport').'" '.$styleSheet.' onclick="location.href=\''.$CFG->wwwroot.'/user/user_credithours_report.php?startDate='.$sStartDate.'&endDate='.$sEndDate.'&department='.$sDepartment.'&user='.$sUser.'&userType='.$sUserType.'\';"></div>';
				}
			}
			
				
			$userCCHReport->HTML = $HTML;
			$userCCHReport->reportContentCSV = $reportContentCSV;
			$userCCHReport->reportContentPDF = $reportContentPDF;
			
			return $userCCHReport;
	  }
	  
	  function getCourseComplianceIcon($courseId){
	  
	  		 				
		//ini_set('display_errors', 1);
  	    //error_reporting(E_ALL);
			global $CFG;
			
			$complianceDiv = '';
			if($courseId){
			   $cArr = getCourseCriteriaIdAndIsGlobalByCourseId($courseId);
			   $isGlobal = $cArr->is_global;
			   $courseCriteria = $cArr->criteria;
			   if($courseCriteria == $CFG->courseCriteriaCompliance && $isGlobal == 0){ 
					  $complianceIcon = '<img src="'.$CFG->complianceIcon.'" class="compliance-img" alt="'.get_string('coursecompliance').'" width="25" height="25" title="'.get_string('coursecompliance').'" border="0" />';
					  //$complianceDiv = '<div class = "compliance-img" >'.$complianceIcon.'</div>';
					  $complianceDiv = $complianceIcon;
				}elseif($courseCriteria != $CFG->courseCriteriaCompliance && $isGlobal == 1){ 
					  //$complianceIcon = '<img src="'.$CFG->globalElectiveIcon.'" class="global-elective-img" alt="'.get_string('globalelectivecourse').'" width="25" height="25" title="'.get_string('globalelectivecourse').'" border="0" />';
					  //$complianceDiv = '<div class = "compliance-img" >'.$complianceIcon.'</div>';
					  //$complianceDiv = $complianceIcon;
				}
			}
			
			return $complianceDiv;
				
	  }
	  
	 function getAllUserCourseList($sort, $type, $page, $perpage,$selectionType = 'all',$whereChk = '', $cType = ''){
	
		global $DB,$CFG,$USER;
		if($perpage != 0) {
			$offset = ($page - 1) * $perpage;
			$courseQuery .= " LIMIT " . $offset . ", " . $perpage;
		}
		//$userId =  $userId?$userId:$USER->id;
		 $where = " WHERE 1 = 1  ";
		if($cType){
		  $where .= " AND coursetype_id IN(0,".$cType.")";
        }
		
		
		
		if($type == 1){
			$where .= " AND (
							(
							FIND_IN_SET('incomplete', REPLACE(final_classrooom_stat,' ',',')) ||
							final_classrooom_stat = ''
							) AND
							stat_scorm != 'completed' AND
							stat_resource != 'completed'
						)";
		}elseif($type == 2){
			$where .= " AND (
						FIND_IN_SET('completed', REPLACE(final_classrooom_stat,' ',',')) ||
						stat_scorm = 'completed' ||
						stat_resource = 'completed'
						)";
		}elseif($type == 3){
			$where .= " AND (
						FIND_IN_SET('not started', REPLACE(final_classrooom_stat,' ',',')) ||
						stat_scorm = 'not started' ||
						stat_resource = 'not started'
						)";
			
		}
			
		if($selectionType !='all'){
			//$where .= " AND program_id = 0";
		}
		if($whereChk != ''){
			$where .= $whereChk;
		}
		if($sort == 'count'){
			$fields = 'count(*) as listcount';
			$query = "SELECT ".$fields." FROM vw_user_complete_learning_data_rr ".$where;
			$courseProgramList = $DB->get_record_sql($query);
			return $courseProgramList->listcount;
		}else{
			$fields = '@serial:= @serial+1 AS `serial_number`, program_id, id, program_name, program_description, user_id, course_name, course_description, learningobj, performanceout, stat_scorm, stat_resource , scorm_stat_new, resourse_stat_new , classrooom_stat, coursetype_id, primary_instructor, classroom_instruction, classroom_course_duration,	course_createdby, final_classrooom_stat, user_id';
			$query = "SELECT ".$fields." FROM vw_user_complete_learning_data_rr,  (SELECT @serial :=0) AS serial ".$where.$courseQuery;
			$query .= "ORDER BY course_name ASC";
			$courseProgramList = $DB->get_records_sql($query);
			return $courseProgramList;
		}
	}
	
	
	
	function getUserStatusByCourse($whereChk=''){
	
		    global $DB,$CFG,$USER;
		    $courseProgramList = getAllUserCourseList('', '', 1, 0, 'all', $whereChk, $CFG->courseTypeOnline);
			//pr($courseProgramList);die;
			$cUReorts = array();
			$mUsers = getManagerUser();
			//pr($mUsers);die;
			if(count($courseProgramList) > 0 ){
			   foreach($courseProgramList as $cPArr){
						  $courseId = $cPArr->id;
							$userId = $cPArr->user_id;
							
							if($USER->archetype == $CFG->userTypeAdmin){
							}elseif($USER->archetype == $CFG->userTypeManager){
							
							  if(!in_array($userId, $mUsers)){
							    continue;
							  }
							}elseif($USER->archetype == $CFG->userTypeStudent){
							    if( $userId != $USER->id){
								  continue;
								}
							}
							
						$courseName = $cPArr->course_name;
						$courseDesc = $cPArr->course_description;
						 //$statScorm = $cPArr->stat_scorm;
					  //$statResource = $cPArr->stat_resource;
					     $statScorm = trim($cPArr->scorm_stat_new);
					  $statResource = trim($cPArr->resourse_stat_new);
				   $courseCreatedBy = $cPArr->course_createdby;
				   
				   $NS = 0;
				   $IP = 0;
				   $CL = 0;
				   if($statScorm && $statResource){
				      if($statScorm == 'completed' && $statResource == 'completed'){
					       if(isset($cUReorts[$courseId]['course_status'][2])){
						     if(!in_array($userId, $cUReorts[$courseId]['course_status'][2])){
					           $CL++;
							   $cUReorts[$courseId]['course_status'][2][] = $userId;
							 }
						   }else{
						     $CL++;
							 $cUReorts[$courseId]['course_status'][2][] = $userId;
						   }
					  }else{
						 if($statScorm == 'not started' && $statResource == 'not started'){
						    if(isset($cUReorts[$courseId]['course_status'][3])){
							  if(!in_array($userId, $cUReorts[$courseId]['course_status'][3])){
							    $NS++;
							    $cUReorts[$courseId]['course_status'][3][] = $userId;
							  }
							}else{
							  $NS++;
							  $cUReorts[$courseId]['course_status'][3][] = $userId;
							}
						 }else{
						   if(isset($cUReorts[$courseId]['course_status'][1])){
	   						   if(!in_array($userId, $cUReorts[$courseId]['course_status'][1])){
								 $IP++;
								 $cUReorts[$courseId]['course_status'][1][] = $userId;
							   }
						   }else{
						     $IP++;
							 $cUReorts[$courseId]['course_status'][1][] = $userId;
						   }
						 }
					  }	  
				   }elseif($statScorm == '' && $statResource){
				   
				      if($statResource == 'completed'){
					       if(isset($cUReorts[$courseId]['course_status'][2])){
					          if(!in_array($userId, $cUReorts[$courseId]['course_status'][2])){						   
					            $CL++;
							    $cUReorts[$courseId]['course_status'][2][] = $userId;
							  }	
						   }else{
						     $CL++;
							 $cUReorts[$courseId]['course_status'][2][] = $userId;
						   }
					  }else{
						 if($statResource == 'not started'){
							  if(isset($cUReorts[$courseId]['course_status'][3])){
							    if(!in_array($userId, $cUReorts[$courseId]['course_status'][3])){
								  $NS++;
								  $cUReorts[$courseId]['course_status'][3][] = $userId;
								}
							  }	else{
							    $NS++;
								$cUReorts[$courseId]['course_status'][3][] = $userId;
							  }
						 }else{
							if(isset($cUReorts[$courseId]['course_status'][1])){
							   if(!in_array($userId, $cUReorts[$courseId]['course_status'][1])){
					             $IP++;
							     $cUReorts[$courseId]['course_status'][1][] = $userId;
							   }
						    }else{
							  $IP++;
							  $cUReorts[$courseId]['course_status'][1][] = $userId;
							}
						 }
					  }
					  
				   }elseif($statScorm && $statResource == ''){
				   
				      if($statScorm == 'completed'){
					        if(isset($cUReorts[$courseId]['course_status'][2])){
							
								if(!in_array($userId, $cUReorts[$courseId]['course_status'][2])){
								 $CL++;
								 $cUReorts[$courseId]['course_status'][2][] = $userId;
							   }
						   
					       }else{
						     $CL++;
							 $cUReorts[$courseId]['course_status'][2][] = $userId;
						   }
					  }else{
						 if($statScorm == 'not started'){
							  if(isset($cUReorts[$courseId]['course_status'][3])){
							    if(!in_array($userId, $cUReorts[$courseId]['course_status'][3])){							  
								  $NS++;
								  $cUReorts[$courseId]['course_status'][3][] = $userId;
								}
							  }	else{
							    $NS++;
								$cUReorts[$courseId]['course_status'][3][] = $userId;
							  }
						 }else{
							if(isset($cUReorts[$courseId]['course_status'][1])){
							  if(!in_array($userId, $cUReorts[$courseId]['course_status'][1])){
					           $IP++;
							   $cUReorts[$courseId]['course_status'][1][] = $userId;
						      }
					         
						   }else{
						     $IP++;
							 $cUReorts[$courseId]['course_status'][1][] = $userId;
						   }
						 }
					  }
				   }elseif($statScorm == '' && $statResource == ''){
						  if(isset($cUReorts[$courseId]['course_status'][3])){
						     if(!in_array($userId, $cUReorts[$courseId]['course_status'][3])){
							   $NS++;
							   $cUReorts[$courseId]['course_status'][3][] = $userId;
							 }
						  }	else{
						    $NS++;
							$cUReorts[$courseId]['course_status'][3][] = $userId;
						  }
				   }
				   
				   
				   $cUReorts[$courseId]['UserCourseStatusCount']->NS += $NS;
				   $cUReorts[$courseId]['UserCourseStatusCount']->IP += $IP;
				   $cUReorts[$courseId]['UserCourseStatusCount']->CL += $CL;
			   }
			   
			  
			} 
			//pr( $cUReorts);die;
			return $cUReorts;
	}
	
	
	function getCourseStatusByUser($whereChk='', $type='', $cType=''){
	
		    global $DB,$CFG,$USER;
		    $courseProgramList = getAllUserCourseList('', $type, 1, 0, 'all', $whereChk, $cType);
			$cUReorts = array();
			$mUsers = getManagerUser();
			if(count($courseProgramList) > 0 ){
			   foreach($courseProgramList as $cPArr){
						  
					    $courseTypeId = $cPArr->coursetype_id;
						if($cType && $cType != $courseTypeId){
						 continue;
						}	
						
					      $courseId = $cPArr->id;
							$userId = $cPArr->user_id;
							
							if($USER->archetype == $CFG->userTypeAdmin){
							}elseif($USER->archetype == $CFG->userTypeManager){
							
							  if(!in_array($userId, $mUsers)){
							    continue;
							  }
							}elseif($USER->archetype == $CFG->userTypeStudent){
								if(!in_array($userId, $mUsers)){
									continue;
								}
							}
						$courseName = $cPArr->course_name;
						$courseDesc = $cPArr->course_description;
						 $statScorm = trim($cPArr->scorm_stat_new);
					  $statResource = trim($cPArr->resourse_stat_new);
		       $finalClassrooomStat = $cPArr->final_classrooom_stat;
				   $courseCreatedBy = $cPArr->course_createdby;
				  $courseCreditHour = $cPArr->credithours;
				   
				   $NS = 0;
				   $IP = 0;
				   $CL = 0;
				  // if($userId == 114 && $courseId == 203){
				    //echo "$statScorm && $statResource";die;
				   
				   $cUReorts[$userId]['course_details'][$courseId]['id'] = $courseId;
				   $cUReorts[$userId]['course_details'][$courseId]['name'] = $courseName;
				   $cUReorts[$userId]['course_details'][$courseId]['summary'] = $courseDesc;
				   $cUReorts[$userId]['course_details'][$courseId]['credithours'] = $courseCreditHour;
				   $cUReorts[$userId]['course_details'][$courseId]['coursetype_id'] = $courseTypeId;
				   
				   
			       
				   $cctype=0;
				   
				   if($courseTypeId == $CFG->courseTypeClassroom){
				   
						  if($finalClassrooomStat == 'completed'){
								
								$cctype = 2;
								$cUReorts[$userId]['course_status'][$courseId] = $cctype;
								$cUReorts[$userId]['course_details'][$courseId]['status'] = $cctype;
								 if(isset($cUReorts[$userId]['course_id_by_status'][$cctype])){
						            if(!in_array($courseId, $cUReorts[$userId]['course_id_by_status'][$cctype])){
									   $CL++;
								       $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
									}
								 }else{
								   $CL++;
								   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
								 }	
						  }else{
							 if($statResource == 'incomplete'){
								
								$cctype = 1;
								$cUReorts[$userId]['course_status'][$courseId] = $cctype;
								$cUReorts[$userId]['course_details'][$courseId]['status'] = $cctype;
								
								if(isset($cUReorts[$userId]['course_id_by_status'][$cctype])){
						            if(!in_array($courseId, $cUReorts[$userId]['course_id_by_status'][$cctype])){
									   $IP++;
								       $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
									}
								 }else{
								   $IP++;
								   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
								 }	
							 }else{
							 
								$cctype = 3;
								$cUReorts[$userId]['course_status'][$courseId] = $cctype;
								$cUReorts[$userId]['course_details'][$courseId]['status'] = $cctype;
								if(isset($cUReorts[$userId]['course_id_by_status'][$cctype])){
						            if(!in_array($courseId, $cUReorts[$userId]['course_id_by_status'][$cctype])){
									   $NS++;
								       $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
									}
								}else{
								   $NS++;
								   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
								}	

							 }
						  }	 
								 
				   }else{ 
						   if($statScorm && $statResource){ 
							  if($statScorm == 'completed' && $statResource == 'completed'){
									$cctype = 2;
									$cUReorts[$userId]['course_status'][$courseId] = $cctype;
									$cUReorts[$userId]['course_details'][$courseId]['status'] = $cctype;
									if(isset($cUReorts[$userId]['course_id_by_status'][$cctype])){
										if(!in_array($courseId, $cUReorts[$userId]['course_id_by_status'][$cctype])){
										   $CL++;
										   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
										}
									}else{
									   $CL++;
									   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
									}	
									
							  }else{
								 if($statScorm == 'not started' && $statResource == 'not started'){
									$cctype = 3;
									$cUReorts[$userId]['course_status'][$courseId] = $cctype;
									$cUReorts[$userId]['course_details'][$courseId]['status'] = $cctype;
									if(isset($cUReorts[$userId]['course_id_by_status'][$cctype])){
										if(!in_array($courseId, $cUReorts[$userId]['course_id_by_status'][$cctype])){
										   $NS++;
										   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
										}
									}else{
									   $NS++;
									   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
									}	
								 }else{
									$cctype = 1;
									$cUReorts[$userId]['course_status'][$courseId] = $cctype;
									$cUReorts[$userId]['course_details'][$courseId]['status'] = $cctype;
									if(isset($cUReorts[$userId]['course_id_by_status'][$cctype])){
										if(!in_array($courseId, $cUReorts[$userId]['course_id_by_status'][$cctype])){
										   $IP++;
										   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
										}
									}else{
									   $IP++;
									   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
									}
								 }
							  }	  
						   }elseif($statScorm == '' && $statResource){
						   
							  if($statResource == 'completed'){
									$cctype = 2;
									$cUReorts[$userId]['course_status'][$courseId] = $cctype;
									$cUReorts[$userId]['course_details'][$courseId]['status'] = $cctype;
									if(isset($cUReorts[$userId]['course_id_by_status'][$cctype])){
										if(!in_array($courseId, $cUReorts[$userId]['course_id_by_status'][$cctype])){
										   $CL++;
										   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
										}
									}else{
									   $CL++;
									   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
									}
							  }else{
								 if($statResource == 'not started'){
									$cctype = 3;
									$cUReorts[$userId]['course_status'][$courseId] = $cctype;
									$cUReorts[$userId]['course_details'][$courseId]['status'] = $cctype;
									if(isset($cUReorts[$userId]['course_id_by_status'][$cctype])){
										if(!in_array($courseId, $cUReorts[$userId]['course_id_by_status'][$cctype])){
										   $NS++;
										   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
										}
									}else{
									   $NS++;
									   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
									}
								 }elseif($statResource == 'incomplete'){
								    $cctype = 1;
									$cUReorts[$userId]['course_status'][$courseId] = $cctype;
									$cUReorts[$userId]['course_details'][$courseId]['status'] = $cctype;
									if(isset($cUReorts[$userId]['course_id_by_status'][$cctype])){
										if(!in_array($courseId, $cUReorts[$userId]['course_id_by_status'][$cctype])){
										   $IP++;
										   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
										}
									}else{
									   $IP++;
									   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
									}
								 }else{
									$cctype = 3;
									$cUReorts[$userId]['course_status'][$courseId] = $cctype;
									$cUReorts[$userId]['course_details'][$courseId]['status'] = $cctype;
									if(isset($cUReorts[$userId]['course_id_by_status'][$cctype])){
										if(!in_array($courseId, $cUReorts[$userId]['course_id_by_status'][$cctype])){
										   $NS++;
										   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
										}
									}else{
									   $NS++;
									   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
									}
								 }
							  }
							  
						   }elseif($statScorm && $statResource == ''){
						   
							  if($statScorm == 'completed'){
									$cctype = 2;
									$cUReorts[$userId]['course_status'][$courseId] = $cctype;
									$cUReorts[$userId]['course_details'][$courseId]['status'] = $cctype;
									if(isset($cUReorts[$userId]['course_id_by_status'][$cctype])){
										if(!in_array($courseId, $cUReorts[$userId]['course_id_by_status'][$cctype])){
										   $CL++;
										   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
										}
									}else{
									   $CL++;
									   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
									}
							  }else{
								 if($statScorm == 'not started'){
									$cctype = 3;
									$cUReorts[$userId]['course_status'][$courseId] = $cctype;
									$cUReorts[$userId]['course_details'][$courseId]['status'] = $cctype;
									if(isset($cUReorts[$userId]['course_id_by_status'][$cctype])){
										if(!in_array($courseId, $cUReorts[$userId]['course_id_by_status'][$cctype])){
										   $NS++;
										   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
										}
									}else{
									   $NS++;
									   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
									}
								 }elseif($statScorm == 'incomplete'){
								    $cctype = 1;
									$cUReorts[$userId]['course_status'][$courseId] = $cctype;
									$cUReorts[$userId]['course_details'][$courseId]['status'] = $cctype;
									if(isset($cUReorts[$userId]['course_id_by_status'][$cctype])){
										if(!in_array($courseId, $cUReorts[$userId]['course_id_by_status'][$cctype])){
										   $IP++;
										   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
										}
									}else{
									   $IP++;
									   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
									}
									
								 }else{
									$cctype = 3;
									$cUReorts[$userId]['course_status'][$courseId] = $cctype;
									$cUReorts[$userId]['course_details'][$courseId]['status'] = $cctype;
									if(isset($cUReorts[$userId]['course_id_by_status'][$cctype])){
										if(!in_array($courseId, $cUReorts[$userId]['course_id_by_status'][$cctype])){
										   $NS++;
										   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
										}
									}else{
									   $NS++;
									   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
									}
								 }
							  }
						   }elseif($statScorm == '' && $statResource == ''){
							 $cctype = 3;
							 $cUReorts[$userId]['course_status'][$courseId] = $cctype;
							 $cUReorts[$userId]['course_details'][$courseId]['status'] = $cctype;
							 if(isset($cUReorts[$userId]['course_id_by_status'][$cctype])){
								if(!in_array($courseId, $cUReorts[$userId]['course_id_by_status'][$cctype])){
								   $NS++;
								   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;
								}
							  }else{
							   $NS++;
							   $cUReorts[$userId]['course_id_by_status'][$cctype][] = $courseId;

							  }
						   }
				     }
					 
					 $cUReorts[$userId]['course_details_by_status'][$cctype][$courseId]['id'] = $courseId;
					 $cUReorts[$userId]['course_details_by_status'][$cctype][$courseId]['name'] = $courseName;
					 $cUReorts[$userId]['course_details_by_status'][$cctype][$courseId]['summary'] = $courseDesc;
					 $cUReorts[$userId]['course_details_by_status'][$cctype][$courseId]['credithours'] = $courseCreditHour;
					 $cUReorts[$userId]['course_details_by_status'][$cctype][$courseId]['coursetype_id'] = $courseTypeId;
							
				   //}
				   
				   $cUReorts[$userId]['UserCourseStatusCount']->NS += $NS;
				   $cUReorts[$userId]['UserCourseStatusCount']->IP += $IP;
				   $cUReorts[$userId]['UserCourseStatusCount']->CL += $CL;
			   }
			   
			  
			} 
			return $cUReorts;
	}
	
		/**
	 * This function is using for getting a particluar course report per users
	 * @global object
	 * @param int $courseId id of course 
	 * @param string $sort is sorting field
	 * @param string $dir is sorting order
	 * @param int $page current page
	 * @param int $perpage is number of records per page
	 * @param array $paramArray is search array
	 * @return array $reports getting a particluar course report per users
	 */
	
	function getSingleCourseReportPerUser($courseId, $sort='mu.id', $dir='ASC', $page=1, $perpage=10, array $paramArray=null) {
	   global $DB, $CFG;

        require_once($CFG->dirroot . '/my/learninglib.php');
	    $reports = array();
	    $searchString = "";
		if($paramArray['ch'] != '') {
			if($paramArray['ch'] == 'OTH')
				$searchString .= " AND (mu.firstname REGEXP '^[^a-zA-Z]' OR mu.lastname REGEXP '^[^a-zA-Z]') ";
			else
				$searchString .= " AND ( mu.firstname like '%".$paramArray['ch']."%' OR mu.lastname like '%".$paramArray['ch']."%' OR (concat(mu.firstname, ' ', mu.lastname) like '%".$paramArray['ch']."%' ) ) ";
		}

		$searchKeyAll = true;
		if( $paramArray['nm'] == 1 )
			$searchKeyAll = false;

		$paramArray['key'] = addslashes($paramArray['key']);
		if($searchKeyAll && $paramArray['key'] != '') {
			$searchString .= " AND ( mu.firstname like '%".$paramArray['key']."%' OR mu.lastname like '%".$paramArray['key']."%' OR (concat(mu.firstname, ' ', mu.lastname) like '%".$paramArray['key']."%' ) ) ";
		} else if ($paramArray['key'] != '') {
			if( $paramArray['nm']==1 ){
				$searchString .= " AND ( mu.firstname like '%".$paramArray['key']."%' OR mu.lastname like '%".$paramArray['key']."%' OR (concat(mu.firstname, ' ', mu.lastname) like '%".$paramArray['key']."%' ) ) ";
			}
		}

	   if($courseId){

	                $extraCondForUserCourseList = " AND id = '". $courseId."'";
			        $courseProgramList = getCourseStatusByUser($extraCondForUserCourseList,'', $CFG->courseTypeOnline);
					$userIdArr = count($courseProgramList) > 0 ? array_keys($courseProgramList): 0; 
					$userIds = is_array($userIdArr) && count($userIdArr) > 0 ? implode(",", $userIdArr): 0; 
				    $sortstr = '';
				    if ($sort) {
		                 
						 if($sort != 'usercount' && $sort != 'coursecount'){
						   $sortstr = "  ORDER BY $sort $dir";
						 }  
					}
				  
					$query = " SELECT mu.id, mu.username, mu.email, concat(mu.firstname, ' ', mu.lastname) as fullname FROM {$CFG->prefix}user as mu ";
					$query .= " WHERE 1 = 1 ";
					$query .= " AND deleted = '0' ";
					$query .= " AND mu.id IN ($userIds)" ;
					$query .= $searchString ;
					$query .= " $sortstr ";
					
					$courseUsers = $DB->get_records_sql($query);
					//pr($query);die;
				    if($sort == 'usercount'){
		
						$NS = 0;
						$IP = 0;
						$CL = 0;
						if(count($courseUsers) > 0){
						  
						  $usercount = 0;
						  foreach($courseUsers as $users){
						
							$userId = $users->id;
							//$courseStatusByUser = getCourseIdByStatus($userId, $courseId);
							$courseStatus = isset($courseProgramList[$userId]['course_status'][$courseId])?$courseProgramList[$userId]['course_status'][$courseId]:'';
							
							 if($courseStatus){
									if(isset($paramArray['type']) && !in_array($paramArray['type'][0], array(0, '-1'))){
									 
										  //$type = $paramArray['type'];
										  $typeArr = explode("@", $paramArray['type']);
											
										  foreach($typeArr as $type){
												
												if($courseStatus == $type){
													 $usercount++;
												}
												
											}
											
									 }else{
										 $usercount++;
									 }
							  }
							 
						  }
						  
						  	$reports = $usercount;
						
						}
						
						
				   }elseif($sort == 'coursecount'){

						   $NS = 0;
						   $IP = 0;
						   $CL = 0;
						   
						   if(count($courseUsers) > 0){
	 
				              foreach($courseUsers as $users){

								 $userId = $users->id; 
								 $courseStatus = isset($courseProgramList[$userId]['course_status'][$courseId])?$courseProgramList[$userId]['course_status'][$courseId]:'';

							      if($courseStatus){
									 
									 if($courseStatus == 3){
									   $NS++;
									 }elseif($courseStatus == 1){
									   $IP++;
									 }elseif($courseStatus == 2){
									   $CL++;
									 }
									 
								   }	 
						      }
							  
							  $reports['UserCourseStatusCount']->NS = $NS;
							  $reports['UserCourseStatusCount']->IP = $IP;
							  $reports['UserCourseStatusCount']->CL = $CL;
						  }
						
					}else{
							   
						   $NS = 0;
						   $IP = 0;
						   $CL = 0;
						   //pr($courseUsers);
						   if(count($courseUsers) > 0){
							  //pr($courseProgramList);die;
				              foreach($courseUsers as $users){

								 $userId = $users->id;
								 $userName = $users->username;
								 $email = $users->email;
								 $fullName = $users->fullname;
								 
								 $courseStatus = isset($courseProgramList[$userId])?$courseProgramList[$userId]['course_status'][$courseId]:'';
								                     
					
								$cAssetArr =  getAssetInfoByUserCourse($userId, $courseId);
								//pr($cAssetArr);die;
							
							     if($courseStatus){
										 if(isset($paramArray['type']) && !in_array($paramArray['type'][0], array(0, '-1'))){
										 
											$typeArr = explode("@", $paramArray['type']);
											//pr( $typeArr);die;
											foreach($typeArr as $type){

												//echo "$courseStatus == $type == $userId <br>";
												if($courseStatus == $type){
												
													 $reports[$userId]->userId = $userId;
													 $reports[$userId]->userName = $userName;
													 $reports[$userId]->email = $email;
													 $reports[$userId]->fullName = $fullName;
													 $reports[$userId]->courseInfo['courseStatus'] =  $courseStatus;
													 $reports[$userId]->courseInfo['courseInstance'] =  $cAssetArr;
											 
												}
											}
											
											
											
										 }else{
											 $reports[$userId]->userId = $userId;
											 $reports[$userId]->userName = $userName;
											 $reports[$userId]->email = $email;
											 $reports[$userId]->fullName = $fullName;
											 $reports[$userId]->courseInfo['courseStatus'] =  $courseStatus;
											 $reports[$userId]->courseInfo['courseInstance'] =  $cAssetArr;
										 }
								   }
								 
						      }

						  }
						
					}
	
		
		}
		//die;
		//pr($reports);die;
		return $reports;
	
	}	
	
	function getAssetInfoByUserCourse($userId, $courseIds='',$lastaccessed = ''){
		global $DB,$CFG,$USER;
		$where = '';
		if($courseIds != ''){
			$where .= " AND c.courseid IN (".$courseIds.")"; 
		}
		if($lastaccessed == 'lastaccessed'){
			$where .=' AND (c.lastaccessed IS NULL || c.lastaccessed  = "")';
		}
		
		$fieldsPS = "program_id, courseid, instanceid, module, userid, fullname, description, learningobj, performanceout, asset_name, asset_info, completed_stat as combined_stat, organization, scoid, moduleid, lastaccessed, width, height, revision, score, resource_type_id, resource_type_name, is_resource_reference_material, allow_resource_student_access, resource_createdby,'' as completed_on";
		
		$fieldsPR = "program_id, courseid, instanceid, module, userid, fullname, description, learningobj, performanceout, asset_name, asset_info, combined_stat, organization, scoid, moduleid, lastaccessed, width, height, revision, score, resource_type_id, resource_type_name, is_resource_reference_material, allow_resource_student_access, resource_createdby,'' as completed_on";
	
		$query = " SELECT CONCAT_WS( '_', 'c', 0, CONVERT( c.courseid, char ) , CONVERT( c.instanceid, char ) , CONVERT( c.module, char ) ) AS difId, c . * FROM ((SELECT * FROM vw_user_full_resource_status) UNION ( SELECT * FROM vw_user_full_scorm_status) UNION ( SELECT $fieldsPS FROM vw_user_full_program_scorm_status) UNION ( SELECT $fieldsPR FROM vw_user_full_program_resource)) AS c WHERE  c.userid = ".$userId.$where;
		$query .= " AND c.is_resource_reference_material = '0' AND IF( c.resource_type_id = 0,  '1 = 1', c.allow_resource_student_access = 1 ) ORDER BY c.asset_name ASC";
		
		//echo $query;die;
		$courseAssetList = $DB->get_records_sql($query);
		return $courseAssetList;
	}
	function getOpenTeamsUser($teamId){
		GLOBAL $CFG,$DB,$USER;
		if($teamId[0] == '-1' || $teamId[0] == '' || $teamId[0] == '0'){
			$userList = $DB->get_records_sql("SELECT gm.userid FROM mdl_groups_members as gm LEFT JOIN mdl_group_department as gd ON gd.team_id = gm.groupid WHERE gd.id IS NULL AND gm.is_active  = 1");
		}else{
			$teamId = implode(',',$teamId);
			$userList = $DB->get_records_sql("SELECT gm.userid FROM mdl_groups_members as gm LEFT JOIN mdl_group_department as gd ON gd.team_id = gm.groupid WHERE gd.id IS NULL AND gm.groupid IN ($teamId) AND gm.is_active  = 1 ");
		}
		
		$users = array();
		if(!empty($userList)){
			foreach($userList as $user){
				$users[] = $user->userid;
			}
		}
		return $users;
	}
	/*
	Get all owners under teams
	*/
	function getOpenTeamsOwners($teamId){
		GLOBAL $CFG,$DB,$USER;
		if($teamId[0] == '-1' || $teamId[0] == '' || $teamId[0] == '0'){
			$userList = $DB->get_records_sql("SELECT g.group_owner as userid FROM mdl_groups g LEFT JOIN mdl_group_department gd ON gd.team_id = g.id WHERE gd.id IS NULL");
		}else{
			$teamId = implode(',',$teamId);
			$userList = $DB->get_records_sql("SELECT g.group_owner as userid FROM mdl_groups g LEFT JOIN mdl_group_department gd ON gd.team_id = g.id WHERE gd.id IS NULL AND g.id IN ($teamId) AND g.is_active  = 1 ");
		}
		
		$users = array();
		if(!empty($userList)){
			foreach($userList as $user){
				$users[] = $user->userid;
			}
		}
		return $users;
	}
	function getOpenTeamsCourse($teamId){
		GLOBAL $CFG,$DB,$USER;
		if($teamId[0] == '-1' || $teamId[0] == '' || $teamId[0] == '0'){
			$courseList = $DB->get_records_sql("SELECT gc.courseid FROM mdl_groups_course AS gc LEFT JOIN mdl_group_department AS gd ON gd.team_id = gc.groupid WHERE gd.id IS NULL");
		}else{
			$teamId = implode(',',$teamId);
			$courseList = $DB->get_records_sql("SELECT gc.courseid FROM mdl_groups_course AS gc LEFT JOIN mdl_group_department AS gd ON gd.team_id = gc.groupid WHERE gd.id IS NULL AND gc.groupid IN ($teamId)");
		}
		$courses = array();
		if(!empty($courseList)){
			foreach($courseList as $course){
				$courses[] = $course->courseid;
			}
		}
		return $courses;
	}
	
	function isClassAlreadyExist($title,$courseid, $id = 0) {
	
		global $DB;
		$where  = '';
		if($id != 0){
			$where  = ' AND s.id != '.$id;
		}
		 $query = 'SELECT id FROM mdl_scheduler s WHERE s.course='.$courseid.' AND LOWER(s.name) = "'.addslashes(strtolower(trim($title))).'"'.$where;
		
		$data = $DB->get_record_sql($query);
	
		if(!empty($data)){
			return $data->id;
		}
		else{
			return false;
		}
	}
	
	function makeElementDisabled($classPrefix='', $width=0, $height=0, $top=0, $left=0){
  
	  global $CFG;
	  $html = '
	  <style>
		div.trans-'.$classPrefix.'-box{
			left: '.$left.'px;
			top: '.$top.'px; 
			margin: 0;
			position: absolute;
			opacity: 0.6;
			filter:alpha(opacity=60); /* For IE8 and earlier */
			
		}
		
		div.trans-'.$classPrefix.'-box img{
		   width:'.$width.'px;
		   height:'.$height.'px;
		}
		</style>
		<div class="trans-'.$classPrefix.'-box"><img  src="'.$CFG->wwwroot.'/theme/gourmet/pix/transp.png" /></div>';
		echo $html;
	
	}


	/**
	 * This function is using for getting the user records
	 * @global object
	 * @param string $extraSql is extra condition for user table query
	 * @return array of user array
	 */
	 
	function getUserListing($paramArray = array()){
	
	  global $CFG, $DB, $USER;

	  
	   $userArr = get_users_listing($sort='lastaccess', $dir='ASC', $page=1, $recordsperpage=0,
                           $search='', $firstinitial='', $lastinitial='', $extraselect='',
                            $paramArray, $extracontext = '');
			
	   
	   return $userArr;
	}
	
	/**
	 * This function is using for check access rights to any module
	 * @global object
	 * @param string $area is area name that we need to check whehter it can be accessed or not
	 * @return bool if can access then return true else false
	 */
	function canUserAccessArea($area){
	
	  global $CFG, $USER;
	  $bool = false;
	  switch($area){
			CASE 'post-blog':
					if($USER->archetype == $CFG->userTypeAdmin || ($USER->archetype == $CFG->userTypeManager && $CFG->isManagerCanPostBlog == 1 ) || ($USER->archetype == $CFG->userTypeStudent && $CFG->isLearnerCanPostBlog == 1) ){
					  $bool = true;
					}
					break;
			CASE 'post-comment-on-blog':
					if($USER->archetype == $CFG->userTypeAdmin || ($USER->archetype == $CFG->userTypeManager && $CFG->isManagerCanPostComment == 1 ) || ($USER->archetype == $CFG->userTypeStudent && $CFG->isLearnerCanPostComment == 1) ){
					  $bool = true;
					}
					break;		
	 }
			
	 return $bool;			
							
	}
	
	/**
	 * This function is using to send an email to admin about comment appoval
	  * @global object
	 * @param array $args is an array of comment details
	 */
	function sendCommentApprovalEmail($args){
	
	    //ini_set('display_errors', 1);
  	    //error_reporting(E_ALL);
		global $CFG, $USER, $SITE, $DB;
		  
		  //pr($args);die;
		  
		if(is_object($args)){
		
			$userListing = getUsers();
			
			$commentId = $args->id;
			$postId = $args->itemid;
			$userId = $args->userid;
			$timeCreated = $args->timecreated;
			$dateTime = getDateFormat($timeCreated, $CFG->customDefaultDateFormat);
			$fullName = $args->fullname;
			
			$timeCreated = $args->timecreated;
			$comment = strip_tags($args->content);
			
			$userDetails = $userListing[$userId];
			$userName = $userDetails->username;
				if($postId){
					$postDetails = $DB->get_record_sql("SELECT mp.subject, mp.summary, mp.userid, mu.username, concat(mu.firstname, ' ', mu.firstname) as userfullname, mu.firstname, mu.lastname, mu.email FROM {$CFG->prefix}post mp LEFT JOIN {$CFG->prefix}user mu ON (mp.userid = mu.id) where mp.id = '".$postId."'");
					$postSubject = $postDetails->subject;
					$postSummary = $postDetails->summary;
					$postCreatorId = $postDetails->userid;
					$postCreatorUserName = $postDetails->username;
					$postCreatorFirstName = $postDetails->firstname;
					$postCreatorLastName = $postDetails->lastname;
					$postCreatorFullName = $postDetails->userfullname;
					$postCreatorEmail = $postDetails->email;
				}
	
				$link = $CFG->wwwroot."/blog/index.php?entryid=".$postId."&showcomment=1#showcomment";
			
			
			$emailContent = array(
								'name'			=> $fullName ,
								'postSubject'	=> $postSubject,
								'postCreatorFullName' => $postCreatorFullName,
								'comment'		=> $comment,
								'datetime'		=> $dateTime,
								'link'		=> $link,
								'sitename'      => html_entity_decode(format_string($SITE->fullname)),
								'siteTitleForBlog'		=> $CFG->siteTitleForBlog );
	
			$emailContent = (object)$emailContent;
			$emailText = get_string('comment_admin_approval_to_admin','email',$emailContent);
			$emailText .= get_string('email_footer','email');
			$emailText .= get_string('from_email','email');
			
			$subject = get_string('comment_admin_approval_subject_to_admin','email',$emailContent);
			
			$fromUser->email = $USER->email;
			$fromUser->firstname = $USER->firstname;
			$fromUser->lastname = $USER->lastname;
			$fromUser->maildisplay = true;
			$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
			$fromUser->id = $USER->id;
			$fromUser->firstnamephonetic = '';
			$fromUser->lastnamephonetic = '';
			$fromUser->middlename = '';
			$fromUser->alternatename = '';
	
			$toUser->email = $postCreatorEmail ;
			$toUser->firstname = $postCreatorFirsttName;
			$toUser->lastname = $postCreatorLastName;
			$toUser->maildisplay = true;
			$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
			$toUser->id = $postCreatorId;
			$toUser->firstnamephonetic = '';
			$toUser->lastnamephonetic = '';
			$toUser->middlename = '';
			$toUser->alternatename = '';
		    $emailTextHtml = text_to_html($emailText, false, false, true);
	
			//email_to_user($toUser, $fromUser, $subject, $emailText, $emailTextHtml, "", "", true);		
			
		}					
	}
	
	
	
	function getGlobalRequestData($getData = 1,$searchText = ''){
	GLOBAL $DB,$USER,$CFG;
	$where = '';
	$where2 = '';
	//echo $searchText;die;
	if($USER->archetype == $CFG->userTypeAdmin){
		//$where2 .= " AND c.request_type != 'classroom' AND c.request_type != 'Course Extension'";
		//$userIdCheck = ' u.parent_id = '.$USER->id;
		$userIdCheck = '1=1';
		/*if($statusId == 2){
			$where = "WHERE (c.request_status = 1 || c.request_status = 2) ";
			$orderBy = "ORDER BY c.reponse_on DESC ";
		}else{
			$where = " WHERE (c.request_status = 0)";
			$orderBy = "ORDER BY c.requested_on DESC ";
		}*/

		//$where = " WHERE 1 = 1 AND c.is_sender_deleted = '0'  AND c.is_sender_dept_deleted = '0' AND if(c.receiver_id, 'c.is_receiver_deleted = 0 AND c.is_receiver_dept_deleted = 0', '1=1' ) ";
		$where = " WHERE 1 = 1 AND c.is_sender_deleted = '0'  AND c.is_sender_dept_deleted = '0'";
		$orderBy = "ORDER BY c.requested_on DESC ";
		$where .= $where2;
		if($searchText != ''){
			$where .= $searchText;
		}
		
		
		$programCoursesSql = "SELECT ##SELECT_VALUE## FROM 
						(
								(
									SELECT dif_id, id, classid, course_id, fullname, name, program_id, request_type, user_id, sender_id, sender_parent, receiver_id, receiver_parent, request_status, request_text,  requested_on, reponse_on, startdate, enddate, no_of_seats, enrolmenttype, createdby, action_taken_by, declined_remarks, sender_name, sender_username, receiver_name, receiver_username, course_name, class_name, program_name,
									sender_teamtitle, sender_team_id, sender_departmenttitle, sender_department_id, receiver_teamtitle,receiver_team_id,receiver_departmenttitle,
									receiver_department_id, jobid, comid, job_title, company, is_sender_deleted, is_receiver_deleted, is_sender_dept_deleted, is_receiver_dept_deleted  FROM vw_classroom_request 
									
								)
						    UNION
								(
									SELECT dif_id, id, classid, course_id, fullname, name, program_id, request_type, user_id, sender_id, sender_parent, receiver_id, receiver_parent, request_status, request_text,  requested_on, reponse_on, startdate, enddate, no_of_seats, enrolmenttype, createdby, action_taken_by, declined_remarks, sender_name, sender_username, receiver_name, receiver_username, course_name, class_name, program_name,
									sender_teamtitle, sender_team_id, sender_departmenttitle, sender_department_id, receiver_teamtitle,receiver_team_id,receiver_departmenttitle,
									receiver_department_id, jobid, comid, job_title, company, is_sender_deleted, is_receiver_deleted, is_sender_dept_deleted, is_receiver_dept_deleted  from vw_online_request  vor
								)
							) as c ";

		
		
	}else{
		$userIdCheck = '(sender_parent = '.$USER->id.' || sender_id = '.$USER->id.')';
		//$userIdCheck = '(mu_sender.parent_id = '.$USER->id.' || mu_sender.id = '.$USER->id.')';
		/*if($statusId == 2){
			$where = "WHERE (c.request_status = 1 || c.request_status = 2) ";
			$orderBy = "ORDER BY c.reponse_on DESC ";
		}else{
			//$where = " WHERE (c.request_status = 0)";
			$where = 'WHERE ((c.request_status = 0) || (c.request_status = 3 AND c.user_id = '.$USER->id.'))';
			$orderBy = "ORDER BY c.requested_on DESC ";
		}*/
		
		$where = " WHERE 1 = 1 AND c.is_sender_deleted = '0'  AND c.is_sender_dept_deleted = '0' AND if(c.receiver_id, 'c.is_receiver_deleted = 0 AND c.is_receiver_dept_deleted = 0', '1=1' ) ";
		$where = " WHERE 1 = 1 AND c.is_sender_deleted = '0'  AND c.is_sender_dept_deleted = '0' ";
		$orderBy = "ORDER BY requested_on DESC ";
		if($searchText != ''){
			$where .= $searchText;
		}
		

		$programCoursesSql = "SELECT ##SELECT_VALUE## FROM 
						(
								(
									SELECT dif_id, id, classid, course_id, fullname, name, program_id, request_type, user_id, sender_id, sender_parent, receiver_id, receiver_parent, request_status, request_text,  requested_on, reponse_on, startdate, enddate, no_of_seats, enrolmenttype, createdby, action_taken_by, declined_remarks, sender_name, sender_username, receiver_name, receiver_username, course_name, class_name, program_name,
									sender_teamtitle, sender_team_id, sender_departmenttitle, sender_department_id, receiver_teamtitle,receiver_team_id,receiver_departmenttitle,
									receiver_department_id, jobid, comid, job_title, company, is_sender_deleted, is_receiver_deleted, is_sender_dept_deleted, is_receiver_dept_deleted FROM vw_classroom_request WHERE $userIdCheck 
									
								)
						    UNION
								(
									SELECT dif_id, id, classid, course_id, fullname, name, program_id, request_type, user_id, sender_id, sender_parent, receiver_id, receiver_parent, request_status, request_text,  requested_on, reponse_on, startdate, enddate, no_of_seats, enrolmenttype, createdby, action_taken_by, declined_remarks, sender_name, sender_username, receiver_name, receiver_username, course_name, class_name, program_name,
									sender_teamtitle, sender_team_id, sender_departmenttitle, sender_department_id, receiver_teamtitle,receiver_team_id,receiver_departmenttitle,
									receiver_department_id, jobid, comid, job_title, company, is_sender_deleted, is_receiver_deleted, is_sender_dept_deleted, is_receiver_dept_deleted from vw_online_request  vor
									WHERE vor.action_taken_by = $USER->id 
								)
							) as c ";
		}
		
		$where .= " AND request_status IS NOT NULL ";
		$programCoursesSql .= $where;
		if($getData == 1){
			$dataSql = str_replace("##SELECT_VALUE##",'*',$programCoursesSql);
		   // echo $dataSql.$orderBy.$limit;die;
			$requests = $DB->get_records_sql($dataSql.$orderBy.$limit);
			return $requests;
		}else{
			$dataCountSql = str_replace("##SELECT_VALUE##",'count(*) as cnt',$programCoursesSql);
			$requestsCount = $DB->get_record_sql($dataCountSql);
			$requestsCount = $requestsCount->cnt;
			return $requestsCount;
		}
	}

	// function get request report data with cron table
	function getGlobalRequestData_cron($getData = 1,$searchText = '',$page,$perpage,$alldata=0,$sTypeArrIds=array()){
		GLOBAL $DB,$USER,$CFG;
		$where = '';
		$where2 = '';
		$offset = $page - 1;
		$offset = $offset*$perpage;
		$limit = '';
		if($perpage != 0){
			$limit = "LIMIT $offset, $perpage";
		}
		$sTypeArrIdsStr = '';
		if($sTypeArrIds){
			$sTypeArrIdsStr = implode(',',$sTypeArrIds);
			$sTypeArrIdsStr = " AND request_status IN($sTypeArrIdsStr)";
		}
		
		if($USER->archetype == $CFG->userTypeAdmin){
			
			$userIdCheck = '';			
	
			//$where = " WHERE 1 = 1 AND is_sender_deleted = '0'  AND is_sender_dept_deleted = '0' AND if(receiver_id, 'is_receiver_deleted = 0 AND is_receiver_dept_deleted = 0', '1=1' ) ";
			$where = " WHERE 1 = 1 AND is_sender_deleted = '0'  AND is_sender_dept_deleted = '0'  ";
			$orderBy = "ORDER BY requested_on DESC ";
			$where .= $where2;
			$where .= $sTypeArrIdsStr;
			if($searchText != ''){
				$where .= $searchText;
			}	
			$programCoursesSql = "select ##SELECT_VALUE## from mdl_report_request_report";
			
		}else{
			$userIdCheck = ' AND  (sender_parent = '.$USER->id.' || sender_id = '.$USER->id.')';
			
			//$where = " WHERE 1 = 1 AND is_sender_deleted = '0'  AND is_sender_dept_deleted = '0' AND if(receiver_id, 'is_receiver_deleted = 0 AND is_receiver_dept_deleted = 0', '1=1' ) ";
			$where = " WHERE 1 = 1 AND is_sender_deleted = '0'  AND is_sender_dept_deleted = '0' ";
			
			$orderBy = " ORDER BY requested_on DESC ";
			if($searchText != ''){
				$where .= $searchText;
			}
	
			$programCoursesSql = "select ##SELECT_VALUE## from mdl_report_request_report ";
			
		}
		
		$where .= " AND request_status IS NOT NULL ";
		$where .= $userIdCheck;
		$programCoursesSql .= $where;
                
               // echo $programCoursesSql;die;
		if($getData == 1){
				$programCoursesSql .= $sTypeArrIdsStr;
				//echo $programCoursesSql;die;
				$dataSql = str_replace("##SELECT_VALUE##",'*',$programCoursesSql);
				//echo $dataSql.$orderBy.$limit;die;
				$requests = $DB->get_records_sql($dataSql.$orderBy.$limit);
			//echo count($requests);die;
				return $requests;
			}else if($alldata==1){
				$dataCountSql = str_replace("##SELECT_VALUE##",'count(*) as cnt',$programCoursesSql);
				//echo $dataCountSql;die;
				$requestsCount = $DB->get_record_sql($dataCountSql);
				$requestsCount = $requestsCount->cnt;
				
				return $requestsCount;
			}else{
				$programCoursesSql .= $sTypeArrIdsStr;
				$programCoursesSql = $programCoursesSql.' Group by request_status';
				$dataCountSql = str_replace("##SELECT_VALUE##",'request_status,count(id) as cnt',$programCoursesSql);
				//echo $dataCountSql;die;
				$requests = $DB->get_records_sql($dataCountSql);
				return $requests;
				}
			
			}
	
	 /**
	 * This function is using for getting a global request Report
	 * @global object
	 * @param array $paramArray is search array
	 * @param array $removeKeyArray contain search key that you don't want to add in address bar
	 * @param string $sort is sorting field
	 * @param string $dir is sorting order
	 * @param int $page current page
	 * @param int $perpage is number of records per page
	 * @param string $export is action variable for PDF download, CSV download, and Print Course Usages Report
	 * @return array $globalRequestReport return CSV , PDF and Report Content
	 */
	
			function getGlobalRequestReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export=''){
				 
				//ini_set('display_errors', 1);
				// error_reporting(E_ALL);
			
				global $DB, $CFG, $USER;
				$globalRequestReport = new stdClass();
				$isReport = true;
					
				$offset = $page - 1;
				$offset = $offset*$perpage;
				$limit = '';
				if($perpage != 0){
					$limit = "LIMIT $offset, $perpage";
				}
			
				////// Getting common URL for the Search //////
				$pageURL = $_SERVER['PHP_SELF'];
				$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
					
				$printUrl = '/my/'.$CFG->pageGlobalRequestReportPrint;
				$genPrintURL = genParameterizedURL($paramArray, $removeKeyArray, $printUrl);
				 
				$sDepartment    = $paramArray['department'];
				$sTeam          = $paramArray['team'];
				$sCourse        = $paramArray['course'];
				$sProgram       = $paramArray['program'];
				$sType          = $paramArray['type'];
				$sStartDate     = $paramArray['startDate'];
				$sEndDate       = $paramArray['endDate'];
				$sUserGroup      = $paramArray['user_group'];
				$sUser      = $paramArray['user'];
				$sClasses      = $paramArray['classes'];
					
				$sJobTitle          = $paramArray['job_title'];
				$sCompany          = $paramArray['company'];
				$sJobTitleArr = explode("@",$sJobTitle);
				$sCompanyArr = explode("@",$sCompany);
				$sJobTitleName = $sJobTitle=='-1'?(get_string('all','multiuserreport')):getJobTitles($sJobTitleArr);
				$sCompanyName = $sCompany=='-1'?(get_string('all','multiuserreport')):getCompany($sCompanyArr);
			
			
					
				$sDateSelected = '';
				if($sStartDate){
					$sDateArr = getFormattedTimeStampOfDate($sStartDate);
					$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';
			
				}
					
				$eDateSelected = '';
				if($sEndDate){
					$eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
					$eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
				}
			
				//$sStartDate = date("m/d/Y",strtotime($sStartDate));
				//$sEndDate = date("m/d/Y",strtotime($sEndDate));
			
				$sDepartmentArr = explode("@",$sDepartment);
				$sTeamArr = explode("@",$sTeam);
				$sCourseArr = explode("@",$sCourse);
				$sProgramArr = explode("@",$sProgram);
				$sTypeArr = explode("@",$sType);
				$sUserGroupArr = explode("@",$sUserGroup);
				$sUserArr = explode("@",$sUser);
				$sClassesArr = explode("@",$sClasses);
			
				if($paramArray['sel_mode'] == 2){
					$sTeamArr = $sUserGroupArr;
				}
				//$extraCond = " AND id = '". $courseId."' AND user_id = '". $userId."'";
				//$cUReorts = getUserStatusByCourse($extraCond='');
				//pr($cUReorts);die;
				$sDepartmentName = $sDepartment=='-1'?(get_string('all','multicoursereport')):getDepartments($sDepartmentArr, $isReport);
				if($USER->archetype != $CFG->userTypeAdmin ){
					$sDepartmentName = getDepartments($USER->department, $isReport);
				}
				$sTeamName = $sTeam=='-1'?(get_string('all','multicoursereport')):getTeams($sTeamArr);
				$sCourseName = $sCourse=='-1'?(get_string('all','multicoursereport')):getCourses($sCourseArr);
				$sProgramName = $sProgram=='-1'?(get_string('all','multicoursereport')):getPrograms($sProgramArr);
				$sUserGroupName = $sUserGroup=='-1'?(get_string('all','multiuserreport')):getUserGroup($sUserGroupArr);
				$sClassName = $sClasses=='-1'?(get_string('all','multicoursereport')):getClasses($sClassesArr);
				$sUserName = $sUser=='-1'?(get_string('all','multicoursereport')):getUsers($sUserArr);
					
					
				// search condition start here
			
				if($sType == '-1'){
					$sTypeName = get_string('all','multicoursereport');
				}else{
					$sTypeArrNames = str_replace(array(1, 2, 3), array(get_string('openrequestreport'), get_string('accept_request_closed'), get_string('decline_request_closed')), $sTypeArr);
						
					$sTypeArrIds = array();
					if(count($sTypeArr) > 0 ){
						foreach($sTypeArr as $sT){
							if($sT == 1){
								$sTypeArrIds[] = 0;
								$sTypeArrIds[] = 3;
							}elseif($sT == 2){
								$sTypeArrIds[] = 1;
							}elseif($sT == 3){
								$sTypeArrIds[] = 2;
							}
						}
					}
					//pr($sTypeArrIds);die;
					$sTypeIds = '0,1,2,3'; // 0 & 3 =>open , 1=>accepted , 2=>declined
					$sTypeName = implode(", ", $sTypeArrNames);
					$sTypeIds = count($sTypeArrIds) > 0 ? implode(",", $sTypeArrIds):$sTypeIds;
					//$searchString .= " AND request_status IN ($sTypeIds) ";
				}
					
					
				$senderDepartmentId = $request->sender_department_id?$request->sender_department_id:0;
				$receiverTeamTitle = $request->receiver_teamtitle?$request->receiver_teamtitle:getMDash();
				$receiverTeamTitleCSV = $request->receiver_teamtitle?$request->receiver_teamtitle:getMDashForCSV();
				$receiverTeamId = $request->receiver_team_id?$request->receiver_team_id:0;
				$receiverDepartmentTitle = $request->receiver_departmenttitle?$request->receiver_departmenttitle:getMDash();
				$receiverDepartmentTitleCSV = $request->receiver_departmenttitle?$request->receiver_departmenttitle:getMDashForCSV();
				$receiverDepartmentId = $request->receiver_department_id?$request->receiver_department_id:0;
					
					
			
				if(count($sDepartmentArr) > 0 &&  !in_array($sDepartmentArr[0] , array(0, '-1'))){
					$sDepartmentStr = implode(",", $sDepartmentArr);
					$searchString .= " AND sender_department_id IN ($sDepartmentStr) ";
				}
					
				if(count($sTeamArr) > 0 &&  !in_array($sTeamArr[0] , array(0, '-1'))){
					$TeamSearchStringArr = array();
					foreach($sTeamArr as $sTeamVal){
						$TeamSearchStringArr[] = " FIND_IN_SET ($sTeamVal,sender_team_id) ";
					}
					if(count($TeamSearchStringArr) > 0 ){
						$TeamSearchString = implode(" OR ", $TeamSearchStringArr);
						$searchString .= " AND ($TeamSearchString) ";
					}
				}
					
					
				if(count($sUserGroupArr) > 0 &&  !in_array($sUserGroupArr[0] , array(0, '-1'))){
					$TeamSearchStringArr = array();
					foreach($sUserGroupArr as $sTeamVal){
						$TeamSearchStringArr[] = " FIND_IN_SET ($sTeamVal,sender_team_id) ";
					}
					if(count($TeamSearchStringArr) > 0 ){
						$TeamSearchString = implode(" OR ", $TeamSearchStringArr);
						$searchString .= " AND ($TeamSearchString) ";
					}
				}
					
					
				if(count($sProgramArr) > 0 &&  !in_array($sProgramArr[0] , array(0, '-1'))){
					$sProgramStr = implode(",", $sProgramArr);
					$searchString .= " AND program_id IN ($sProgramStr) ";
				}
					
					
				if(count($sUserArr) > 0 &&  !in_array($sUserArr[0] , array(0, '-1'))){
					$sUserStr = implode(",", $sUserArr);
					$searchString .= " AND sender_id IN ($sUserStr) ";
				}
					
				if(count($sCourseArr) > 0 &&  !in_array($sCourseArr[0] , array(0, '-1'))){
					$sCourseStr = implode(",", $sCourseArr);
					$searchString .= " AND course_id IN ($sCourseStr) ";
				}else{
					/*if($_REQUEST["sel_mode"] == 2){
					 $courseIdArr = getOpenTeamsCourse($sTeamArr);
					}else{
					$courseIdArr = getCourseListByDnT($sDepartmentArr, $sTeamArr, $sProgramArr, $sCourseArr, 2, $isReport);
					}
					$sCourseArr = count($courseIdArr) > 0 ? implode(",", $courseIdArr) : 0;
					$searchString .= " AND mc.id IN ($sCourseArr) ";
					$searchStringView .= " AND mc_id IN ($sCourseArr) ";*/
				}
					
				if(count($sClassesArr) > 0 &&  !in_array($sClassesArr[0] , array(0, '-1'))){
					$sClassesStr = implode(",", $sClassesArr);
					$searchString .= " AND classid IN ($sClassesStr) ";
				}
					
				if($CFG->showJobTitle == 1 && count($sJobTitleArr) > 0 && !in_array($sJobTitleArr[0] , array(0, '-1'))){
					$sJobTitleStr = count($sJobTitleArr) > 0 ? implode(",", $sJobTitleArr) : 0;
					$searchString .= " AND jobid IN ($sJobTitleStr) ";
				}
					
				if($CFG->showCompany == 1 &&  count($sCompanyArr) > 0 && !in_array($sCompanyArr[0] , array(0, '-1'))){
					$sCompanyStr = count($sCompanyArr) > 0 ? implode(",", $sCompanyArr) : 0;
					$searchString .= " AND comid IN ($sCompanyStr) ";
				}
					
					
				if($sDateSelected){
					/*list($month, $day, $year) = explode('/', $sDateSelected);
					 $sStartDateTime =  mktime(0, 0, 0, $month, $day, $year);*/
					$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
					$searchString .= " AND requested_on >= ($sStartDateTime) ";
				}
					
				if($eDateSelected){
					/*list($month, $day, $year) = explode('/', $eDateSelected);
					 $sEndDateTime = mktime(0, 0, 0, $month, $day, $year);*/
					$sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
					$searchString .= " AND requested_on <= ($sEndDateTime) ";
				}
					
				// search condition end here
					
				$requestArr = getGlobalRequestData(1, $searchString);
				$requestCount = count($requestArr);
				//pr($requestArr );die;
					
					
					
				$openReq = 0;
				$acceptReq = 0;
				$declineReq = 0;
				$totalReq = 0;
					
				$requestArrN = array();
				if($requestCount > 0){
			
					foreach($requestArr as $k=>$reqArr){
						$reqStatus = $reqArr->request_status;
						if($reqStatus == 0 || $reqStatus == 3){
							$openReq++;
						}elseif($reqStatus == 1){
							$acceptReq++;
						}elseif($reqStatus == 2){
							$declineReq++;
						}
						$totalReq++;
							
						if(count($sTypeArrIds) > 0 && in_array($reqStatus, $sTypeArrIds)){
							$requestArrN[$k] = $reqArr;
						}
					}
						
				}
					
				//pr($requestArrN);die;
					
				$openReqPercentage = 0;
				$acceptReqPercentage = 0;
				$declineReqPercentage = 0;
				$openReqExploded = false;
				$acceptReqExploded = false;
				$declineReqExploded = false;
					
				$openReqPercentage = numberFormat(($openReq*100)/$totalReq);
				$acceptReqPercentage = numberFormat(($acceptReq*100)/$totalReq);
				$declineReqPercentage = numberFormat(($declineReq*100)/$totalReq);
					
				if(in_array(1, $sTypeArr)){
					$openReqExploded = true;
				}
					
				if(in_array(2, $sTypeArr)){
					$acceptReqExploded = true;
				}
					
				if(in_array(3, $sTypeArr)){
					$declineReqExploded = true;
				}
					
					
					
					
				//pr($requestArr );die;
				$requestHTML = '';
				$style = !empty($export)?"style=''":'';
				$requestHTML .= "<div class='tabsOuter borderBlockSpace' ".$style.">";
				$requestHTML .= '<div class="clear"></div>';
				$exportHTML = '';
				if(empty($export)){
						
					ob_start();
					require_once($CFG->dirroot . '/local/includes/global_request_report_search.php');
					$SEARCHHTML = ob_get_contents();
					ob_end_clean();
			
					$requestHTML .= $SEARCHHTML;
			
					$exportHTML .= '<div class="exports_opt_box"> ';
					$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','multicoursereport').'" href="'.$genPrintURL.'&action=exportcsv">'.get_string('downloadcsv','multicoursereport').'</a> ';
					$exportHTML .= '<span class="seperater">&nbsp;</span>';
					$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','multicoursereport').'" id="exportpdf1" href="'.$genPrintURL.'&action=exportpdf">'.get_string('downloadpdf','multicoursereport').'</a> ';
					$exportHTML .= '<span class="seperater">&nbsp;</span>';
					$exportHTML .= '<a rel="'.$genPrintURL.'&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','multicoursereport').'" class="print_icon">'.get_string('print','multicoursereport').'</a>';
					$exportHTML .= '</div>';
						
				}
					
				$requestHTML .= '<div class="userprofile view_assests">';
					
					
				if(count($requestArrN) > 0 ){
					$requestCount  = count($requestArrN);
					$requestArr = $requestArrN;
				}
					
				if($requestCount > 0 ){
			
					$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
					$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
					$sDateSelectedForPrintCSV = getDateFormatForPrint($sDateSelected, 'csv');
					$eDateSelectedForPrintCSV = getDateFormatForPrint($eDateSelected, 'csv');
			
			
					if(!empty($export) && $export == 'print'){
			
						$reportName = get_string('requestsreport');
						$requestHTML .= getReportPrintHeader($reportName);
			
						$requestHTML .= '<table border="0" cellpadding="0" cellspacing="0" class="generaltable margin_bottom">';
						$requestHTML .= '<tr>';
						if($paramArray['sel_mode'] == 2){
							$requestHTML .= '<td colspan = "2" ><strong>'.get_string('global_team').':</strong> '.$sUserGroupName.'</td>';
						}else{
							$requestHTML .= '<td><strong>'.get_string('department','multicoursereport').':</strong> '.$sDepartmentName.'</td>';
							$requestHTML .= '<td><strong>'.get_string('team','multicoursereport').':</strong>  '.$sTeamName.'</td>';
						}
						$requestHTML .= '</tr>';
						$requestHTML .= '<tr>';
						$requestHTML .= '<td width="500"><strong>'.get_string('program','multicoursereport').':</strong> '.$sProgramName.'</td>';
						$requestHTML .= '<td width="500"><strong>'.get_string('course','multicoursereport').':</strong> '.$sCourseName.'</td>';
						$requestHTML .= '</tr>';
						$requestHTML .= '<tr>';
						$requestHTML .= '<td><strong>'.get_string('class','multicoursereport').':</strong> '.$sClassName.'</td>';
						$requestHTML .= '<td><strong>'.get_string('user','multicoursereport').':</strong> '.$sUserName.'</td>';
						$requestHTML .= '</tr>';
							
						if($CFG->showJobTitle == 1 && $CFG->showCompany == 1){
							$requestHTML .= '<tr>';
							$requestHTML .= '<td ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
							$requestHTML .= '<td><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
							$requestHTML .= '</tr>';
						}elseif($CFG->showJobTitle == 1 && $CFG->showCompany == 0){
							$requestHTML .= '<tr>';
							$requestHTML .= '<td colspan="2" ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
							$requestHTML .= '</tr>';
						}elseif($CFG->showJobTitle == 0 && $CFG->showCompany == 1){
							$requestHTML .= '<tr>';
							$requestHTML .= '<td colspan="2" ><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
							$requestHTML .= '</tr>';
						}
							
						$requestHTML .= '<tr>';
						$requestHTML .= '<td colspan="2"><strong>'.get_string('statusreport').':</strong> '.$sTypeName.'</td>';
						$requestHTML .= '</tr>';
						$requestHTML .= '<tr>';
						$requestHTML .= '<td><strong>'.get_string('daterangefrom','multicoursereport').':</strong> '.$sDateSelectedForPrint.'</td>';
						$requestHTML .= '<td><strong>'.get_string('daterangeto','multicoursereport').':</strong>  '.$eDateSelectedForPrint.'</td>';
						$requestHTML .= '</tr>';
						$requestHTML .= '</table>';
						//$requestHTML .= '<span '.$CFG->pdfSpanAttribute.'>&nbsp;<br /><strong>'.get_string('courseusagesreport','multicoursereport').'</strong></span><br /><br />';
					}
			
					$openReqPercentage = numberFormat(($openReq*100)/$totalReq);
					$acceptReqPercentage = numberFormat(($acceptReq*100)/$totalReq);
					$declineReqPercentage = numberFormat(($declineReq*100)/$totalReq);
						
					$requestHTML .= '<div class = "single-report-start" id="watch">
								<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('requestsreport').$exportHTML.'</span>
								  <div class="single-report-graph-left" id="chartContainer">'.get_string('loading','multicoursereport').'</div>
								  <div class="single-report-graph-right">
									<div class="course-count-heading">'.get_string('numberofrequest').'</div>
									<div class="course-count">'.$totalReq.'</div>
									<div class="seperator">&nbsp;</div>
									<div class="course-status">
									  <div class="notstarted"><h6>'.get_string('openrequestreport').'</h6><span>'.$openReq.'</span></div>
									  <div class="clear"></div>
									  <div class="inprogress"><h6>'.get_string('accept_request_closed').'</h6><span>'.$acceptReq.'</span></div>
									  <div class="clear"></div>
									  <div class="completed"><h6>'.get_string('decline_request_closed').'</h6><span>'.$declineReq.'</span></div>
									</div>
								  </div>
								</div>
							  </div>
							  <div class="clear"></div>';
			
				}
					
					
					
				$requestHTML .= '<div class=""><div class=""><table cellspacing="0" cellpadding="0" border="0" class="generaltable"><tbody>';
					
				$requestHTML .= '<tr>';
				$requestHTML .=  '<th width="15%">'.get_string('requesttypereport').'</th>';
				//$requestHTML .=  '<th width=15%">'.get_string('requesttextreport').'</th>';
				$requestHTML .=  '<th width="20%">'.get_string('requestforreport').'</th>';
				$requestHTML .=  '<th width="11%">'.get_string('requestbyreport').'</th>';
				$requestHTML .=  '<th width="11%">'.get_string('requestprocesseddbyreport').'</th>';
				$requestHTML .=  '<th width="12%">'.get_string('requesteddatereport').'</th>';
				$requestHTML .=  '<th width="12%">'.get_string('responsedatereport').'</th>';
				$requestHTML .=  '<th width="10%">'.get_string('requeststatusreport').'</th>';
				$requestHTML .=  '<th width="9%">'.get_string('decline_remarks').'</th>';
					
					
				$requestHTML .=  '</tr>';
					
				$reportContentCSV = '';
				$reportContentCSV .= get_string('daterangefrom','multicoursereport').",".$sDateSelectedForPrintCSV."\n".get_string('daterangeto','multicoursereport').",".$eDateSelectedForPrintCSV."\n";
				if($paramArray['sel_mode'] == 2){
					$reportContentCSV .= get_string('global_team').",".$sUserGroupName."\n";
				}else{
					$reportContentCSV .= get_string('department','multicoursereport').",".$sDepartmentName."\n".get_string('team','multicoursereport').",".$sTeamName ."\n";
				}
				$reportContentCSV .= get_string('program','multicoursereport').",".$sProgramName."\n".get_string('course','multicoursereport').",".$sCourseName ."\n";
				$reportContentCSV .= get_string('class','multicoursereport').",".$sClassName."\n".get_string('user','multicoursereport').",".$sUserName ."\n";
				if($CFG->showJobTitle == 1){
					$reportContentCSV .= get_string('job_title','user').",".$sJobTitleName."\n";
				}
				if($CFG->showCompany == 1){
					$reportContentCSV .= get_string('company','user').",".$sCompanyName."\n";
				}
				$reportContentCSV .= get_string('statusreport').",".$sTypeName ."\n";
				$reportContentCSV .= get_string('numberofrequest').",".$totalReq."\n".get_string('openrequestreport').",".$openReq." (".floatval($openReqPercentage)."%)" ."\n";
				$reportContentCSV .= get_string('accept_request_closed').",".$acceptReq." (".floatval($acceptReqPercentage)."%)"."\n".get_string('decline_request_closed').",".$declineReq." (".floatval($declineReqPercentage)."%)" ."\n";
				$reportContentCSV .= get_string('requestsreport')."\n";
				//$reportContentCSV .= get_string('requesttypereport').",".get_string('requesttextreport').",".get_string('requestforreport').",".get_string('requestbyreport').",".get_string('requestprocesseddbyreport').",".get_string('requesteddatereport').",".get_string('responsedatereport').",".get_string('requeststatusreport').",".get_string('decline_remarks')."\n";
			
				$reportContentCSV .= get_string('requesttypereport').",".get_string('requestforreport').",".get_string('requestbyreport').",".get_string('requestprocesseddbyreport').",".get_string('requesteddatereport').",".get_string('responsedatereport').",".get_string('requeststatusreport').",".get_string('decline_remarks')."\n";
					
					
				$reportContentPDF = '';
				$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" class="generaltable">';
				$reportContentPDF .= '<tr>';
				if($paramArray['sel_mode'] == 2){
					$reportContentPDF .= '<td colspan = "2" ><strong>'.get_string('global_team').':</strong> '.$sUserGroupName.'</td>';
				}else{
					$reportContentPDF .= '<td><strong>'.get_string('department','multicoursereport').':</strong> '.$sDepartmentName.'</td>';
					$reportContentPDF .= '<td><strong>'.get_string('team','multicoursereport').':</strong>  '.$sTeamName.'</td>';
				}
				$reportContentPDF .= '</tr>';
				$reportContentPDF .= '<tr>';
				$reportContentPDF .= '<td><strong>'.get_string('program','multicoursereport').':</strong> '.$sProgramName.'</td>';
				$reportContentPDF .= '<td><strong>'.get_string('course','multicoursereport').':</strong> '.$sCourseName.'</td>';
				$reportContentPDF .= '</tr>';
				$reportContentPDF .= '<tr>';
				$reportContentPDF .= '<td><strong>'.get_string('class','multicoursereport').':</strong> '.$sClassName.'</td>';
				$reportContentPDF .= '<td><strong>'.get_string('user','multicoursereport').':</strong> '.$sUserName.'</td>';
				$reportContentPDF .= '</tr>';
			
				if($CFG->showJobTitle == 1 && $CFG->showCompany == 1){
					$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
					$reportContentPDF .= '<td><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
					$reportContentPDF .= '</tr>';
				}elseif($CFG->showJobTitle == 1 && $CFG->showCompany == 0){
					$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td colspan="2" ><strong>'.get_string('job_title','user').':</strong> '.$sJobTitleName.'</td>';
					$reportContentPDF .= '</tr>';
				}elseif($CFG->showJobTitle == 0 && $CFG->showCompany == 1){
					$reportContentPDF .= '<tr>';
					$reportContentPDF .= '<td colspan="2" ><strong>'.get_string('company','user').':</strong> '.$sCompanyName.'</td>';
					$reportContentPDF .= '</tr>';
				}
			
				$reportContentPDF .= '<tr>';
				$reportContentPDF .= '<td colspan="2"><strong>'.get_string('statusreport').':</strong> '.$sTypeName.'</td>';
				$reportContentPDF .= '</tr>';
				$reportContentPDF .= '<tr>';
				$reportContentPDF .= '<td><strong>'.get_string('daterangefrom','multicoursereport').':</strong> '.$sDateSelectedForPrint.'</td>';
				$reportContentPDF .= '<td><strong>'.get_string('daterangeto','multicoursereport').':</strong>  '.$eDateSelectedForPrint.'</td>';
				$reportContentPDF .= '</tr>';
			
				$reportContentPDF .= '<tr>';
				$reportContentPDF .= '<td><strong>'.get_string('numberofrequest').':</strong> '.$totalReq.'</td>';
				$reportContentPDF .= '<td><strong>'.get_string('openrequestreport').':</strong>  '.$openReq.' ('.floatval($openReqPercentage).'%)</td>';
				$reportContentPDF .= '</tr>';
			
				$reportContentPDF .= '<tr>';
				$reportContentPDF .= '<td><strong>'.get_string('accept_request_closed').':</strong>  '.$acceptReq.' ('.floatval($acceptReqPercentage).'%)</td>';
				$reportContentPDF .= '<td><strong>'.get_string('decline_request_closed').':</strong> '.$declineReq.' ('.floatval($declineReqPercentage).'%)</td>';
				$reportContentPDF .= '</tr>';
			
				//$reportContentPDF .= '<tr><br /><td width="100%"  ><span '.$CFG->pdfSpanAttribute.'><strong>'.get_string('requestsreport').'</strong></span><br /></td></tr>';
					
				$reportContentPDF .= '</table>';
			
				$reportContentPDF .= getGraphImageHTML(get_string('requestsreport'));
					
					
				$reportContentPDF .= '<table '.$CFG->pdfTableStyle.'>';
				$reportContentPDF .= '<tr '.$CFG->pdfTableHeaderStyle.'>';
				$reportContentPDF .=  '<td width="15%"><strong>'.get_string('requesttypereport').'</strong></td>';
				//$reportContentPDF .=  '<td width="15%"><strong>'.get_string('requesttextreport').'</strong></td>';
				$reportContentPDF .=  '<td width="20%"><strong>'.get_string('requestforreport').'</strong></td>';
				$reportContentPDF .=  '<td width="10%"><strong>'.get_string('requestbyreport').'</strong></td>';
				$reportContentPDF .=  '<td width="10%"><strong>'.get_string('requestprocesseddbyreport').'</strong></td>';
				$reportContentPDF .=  '<td width="12%"><strong>'.get_string('requesteddatereport').'</strong></td>';
				$reportContentPDF .=  '<td width="12%"><strong>'.get_string('responsedatereport').'</strong></td>';
				$reportContentPDF .=  '<td width="10%"><strong>'.get_string('requeststatusreport').'</strong></td>';
				$reportContentPDF .=  '<td width="10%"><strong>'.get_string('decline_remarks').'</strong></td>';
					
				$reportContentPDF .=  '</tr>';
					
			
				if($requestCount > 0 ){
					//pr($requestArr );die;
					$i = 0;
						
					if($perpage){
						$offset = $page - 1;
						$offset = $offset*$perpage;
						$requestGlobalArr = array_splice($requestArr, $offset, $perpage);
			
					}else{
						$requestGlobalArr = $requestArr;
					}
			
					foreach($requestGlobalArr as $request){
			
						$format = $CFG->DateFormatForRequests;
						$formatCsv = $CFG->customDefaultDateFormatForCSV;
							
						$formatDateTime = $CFG->customDefaultDateTimeFormat;
						$formatDateTimeCsv = $CFG->customDefaultDateTimeFormatForCSV;
			
						$requestType = $request->request_type;
						$requestStatus = $request->request_status;
						$programId = $request->program_id;
						$courseId = $request->course_id;
						$numberOfSeats = $request->no_of_seats;
						$enrolmentType = $request->enrolmenttype;
						$requestText = $request->request_text;
						$createdBy = $request->createdby;
						$actionTakenBy = $request->action_taken_by;
						$senderName = $request->sender_name;
						$senderUsername = $request->sender_username;
						$receiverName = $request->receiver_name;
						$receiverUsername = $request->receiver_username;
						$courseName = $request->course_name;
						$classId = $request->classid;
						$className = $request->class_name;
						$prograName = $request->program_name;
							
						$sessionList = array();
						$sessionListArr = array();
						if($classId && !isset($sessionListArr[$classId])){
							$sessionList = classSessionList ( $classId , 'classid');
							$sessionListArr[$classId] = $sessionList;
						}
						$sessionName = '';
						$sessionNameCSV = '';
						if(isset($sessionListArr[$classId]) && count($sessionListArr[$classId]) > 0 ){
								
							$sessionNameCSV .= ",".get_string('classsessions').",,,,,,\n";
							if(empty($export) || (!empty($export) && $export !== 'exportcsv')){
								$sessionName .= "<br>".get_string('classsessions');
							}
							foreach($sessionListArr[$classId] as $session){
			
								if(!empty($export) && $export === 'exportcsv'){
									$sessionDuration = getClassroomSessionDateDuration($session->starttime, $session->duration, 'minutes', 'csv');
									$sessionNameCSV .= ",".$session->sessionname.": ". $sessionDuration.",,,,,,\n";
								}else{
									$sessionDuration = getClassroomSessionDateDuration($session->starttime, $session->duration);
									$sessionName .= "<br><strong>".$session->sessionname."</strong>: ". $sessionDuration;
								}
			
							}
						}
							
						$requestFor = $courseName?($className?$courseName.": ".$className.($sessionName):$courseName):($prograName?$prograName:getMDash());
						$requestedOn = $request->requested_on;
						$reponseOn = $request->reponse_on;
						$startDate = $request->startdate;
						$endDate = $request->enddate;
							
						$senderTeamTitle = $request->sender_teamtitle?$request->sender_teamtitle:getMDash();
						$senderTeamTitleCSV = $request->sender_teamtitle?$request->sender_teamtitle:getMDashForCSV();
						$senderTteamId = $request->sender_team_id?$request->sender_team_id:0;
						$senderDepartmentTitle = $request->sender_departmenttitle?$request->sender_departmenttitle:getMDash();
						$senderDepartmentTitleCSV = $request->sender_departmenttitle?$request->sender_departmenttitle:getMDashForCSV();
						$senderDepartmentId = $request->sender_department_id?$request->sender_department_id:0;
						$receiverTeamTitle = $request->receiver_teamtitle?$request->receiver_teamtitle:getMDash();
						$receiverTeamTitleCSV = $request->receiver_teamtitle?$request->receiver_teamtitle:getMDashForCSV();
						$receiverTeamId = $request->receiver_team_id?$request->receiver_team_id:0;
						$receiverDepartmentTitle = $request->receiver_departmenttitle?$request->receiver_departmenttitle:getMDash();
						$receiverDepartmentTitleCSV = $request->receiver_departmenttitle?$request->receiver_departmenttitle:getMDashForCSV();
						$receiverDepartmentId = $request->receiver_department_id?$request->receiver_department_id:0;
							
						$requestDate = $requestedOn?getDateFormat($requestedOn, $format):getMDash();
						$responseDate = ($requestStatus == 0 || $requestStatus == 3)?getMDash():($reponseOn?getDateFormat($reponseOn, $format):getMDash());
							
						$requestDateCSV = $requestedOn?getDateFormat($requestedOn, $formatCsv):getMDashForCSV();
						$responseDateCSV = ($requestStatus == 0 || $requestStatus == 3)?getMDashForCSV():($reponseOn?getDateFormat($reponseOn, $formatCsv):getMDashForCSV());
							
						$requestDateTime = $requestedOn?getDateFormat($requestedOn, $formatDateTime):getMDash();
						$responseDateTime = ($requestStatus == 0 || $requestStatus == 3)?getMDash():($reponseOn?getDateFormat($reponseOn, $formatDateTime):getMDash());
							
						$requestDateTimeCSV = $requestedOn?getDateFormat($requestedOn, $formatDateTimeCsv):getMDashForCSV();
						$responseDateTimeCSV = ($requestStatus == 0 || $requestStatus == 3)?getMDashForCSV():($reponseOn?getDateFormat($reponseOn, $formatDateTimeCsv):getMDashForCSV());
							
						$startDateTime = $startDate?getDateFormat($startDate, $formatDateTime):getMDash();
						$endDateTime = $endDate?getDateFormat($endDate, $formatDateTime):getMDash();
							
						$startDateTimeCSV = $startDate?getDateFormat($startDate, $formatDateTimeCsv):getMDashForCSV();
						$endDateTimeCSV = $endDate?getDateFormat($endDate, $formatDateTimeCsv):getMDashForCSV();
							
							
						if($requestStatus == 0 || $requestStatus == 3){
							$requestText = '';
							$requestTextCSV = '';
							$tdDate = $requestedOn;
						}else{
							$requestText = $requestDate.' - ';
							$requestTextCSV = $requestDateCSV.' - ';
							$tdDate = $reponseOn;
						}
						if($request->request_type == 'classroom'){
							$timeString = $startDateTime." - ".$endDateTime;
							$timeStringCSV = $startDateTime." - ".$endDateTime;
							$timeString = '';
							$timeStringCSV = '';
							$requestText .= str_replace('##__TIME__##',$timeString,$request->request_text);
							$requestTextCSV .= str_replace('##__TIME__##',$timeStringCSV,$request->request_text);
							if(($request->user_id == $actionTakenBy) && ($request->user_id != $USER->id) && $requestStatus == 2){
								$requestText .= date($CFG->DateFormatForRequests,$tdDate)." - ".get_string('request_declined_by_learner');
								$requestTextCSV .= date($formatCsv,$tdDate)." - ".get_string('request_declined_by_learner');
							}
							$reqId = $request->classid;
						}else{
							$requestText .= $request->request_text;
							$requestTextCSV .= $request->request_text;
							$reqId = $request->id;
						}
						if($requestStatus == 0 || $requestStatus == 3){
							//$actionItem = get_string('openrequestreport');
							$actionItem = $requestStatus == 0?(get_string('openrequestreport')." (".get_string('managerforapproval','classroomreport').")"):(get_string('openrequestreport')." (".get_string('totalinviteduser','classroomreport').")");
						}else{
							if($requestStatus == 1){
								$actionItem = get_string('accept_request_closed');
							}else{
								$actionItem = get_string('decline_request_closed');
							}
						}
						if($request->request_type == get_string('program_extension')){
							$reqTypeText = 'program extension';
						}elseif($request->request_type == get_string('course_extension')){
							$reqTypeText = 'online course extension';
						}else{
							$reqTypeText = $request->request_type=='course'?get_string('onlinecourse'):($request->request_type == 'classroom'?get_string('classroomcourse'):$request->request_type);
						}
							
							
						$declinedRemarks = $request->declined_remarks?$request->declined_remarks:getMDash();
						$declinedRemarksCSV = $request->declined_remarks?$request->declined_remarks:getMDashForCSV();
							
			
			
						$requestHTML .=  '<tr>';
						$requestHTML .= '<tr id = "'.$request->program_id.'_'.$request->course_id.'">';
						$requestHTML .= "<td>".ucwords($reqTypeText)."</td>";
						//$requestHTML .= "<td class='email-word-wrap' >".$requestText."</td>";
						$requestHTML .= "<td>".$requestFor."</td>";
						$requestHTML .= "<td>".($senderUsername?($senderName."<br>(".$senderUsername.")"):(getMDash()))."</td>";
						$requestHTML .= "<td>".($receiverUsername?($receiverName."<br>(".$receiverUsername.")"):(getMDash()))."</td>";
						$requestHTML .= "<td>".$requestDate."</td>";
						$requestHTML .= "<td>".$responseDate."</td>";
						$requestHTML .= "<td>".$actionItem."</td>";
						$requestHTML .= "<td >".$declinedRemarks."</td>";
						$requestHTML .= '</tr>';
							
						//$reportContentCSV .= ucwords($reqTypeText).",".$requestTextCSV.",".$requestFor.",".($senderUsername?($senderName." (".$senderUsername.")"):(getMDashForCSV())).",".($receiverUsername?($receiverName." (".$receiverUsername.")"):(getMDashForCSV())).",".$requestDateCSV.",".$responseDateCSV.",".$actionItem.",".$declinedRemarksCSV."\n";
						$reportContentCSV .= ucwords($reqTypeText).",".$requestFor.",".($senderUsername?($senderName." (".$senderUsername.")"):(getMDashForCSV())).",".($receiverUsername?($receiverName." (".$receiverUsername.")"):(getMDashForCSV())).",".$requestDateCSV.",".$responseDateCSV.",".$actionItem.",".$declinedRemarksCSV."\n";
							
						if($sessionNameCSV){
							$reportContentCSV .= $sessionNameCSV;
						}
							
						$reportContentPDF .= '<tr '.$CFG->pdfTableRowAttribute.' >';
						$reportContentPDF .= '<td>'.ucwords($reqTypeText).'</td>';
						//$reportContentPDF .= '<td style="word-break: break-all !important;" >'.$requestText.'</td>';
						$reportContentPDF .= '<td >'.$requestFor.'</td>';
						$reportContentPDF .= "<td style='word-break: break-all !important;' >".($senderUsername?($senderName."<br>(".$senderUsername.")"):(getMDash()))."</td>";
						$reportContentPDF .= "<td style='word-break: break-all !important;' >".($receiverUsername?($receiverName."<br>(".$receiverUsername.")"):(getMDash()))."</td>";
						$reportContentPDF .= '<td>'.$requestDate.'</td>';
						$reportContentPDF .= '<td>'.$responseDate.'</td>';
						$reportContentPDF .= '<td>'.$actionItem.'</td>';
						$reportContentPDF .= '<td>'.$declinedRemarks.'</td>';
						$reportContentPDF .= '</tr>';
							
						$i++;
			
					}
			
					$requestHTML .=  '<script language="javascript" type="text/javascript">';
					$requestHTML .=  '	$(document).ready(function(){
			
												$("#printbun").bind("click", function(e) {
													var url = $(this).attr("rel");
													window.open(url, "'.get_string('requestsreport').'", "'.$CFG->printWindowParameter.'");
												});
			
						             }); ';
			
					$requestHTML .=  ' window.onload = function () {
			
										var chart = new CanvasJS.Chart("chartContainer",
										{
											title:{
												text: ""
											},
											theme: "theme2",
											data: [
											{
												type: "doughnut",
												indexLabelFontFamily: "Arial",
												indexLabelFontSize: 12,
												startAngle:0,
												indexLabelFontColor: "dimgrey",
												indexLabelLineColor: "darkgrey",
												toolTipContent: "{y}%",
			
			
												dataPoints: [
												{  y: '.$openReqPercentage.', label: "'.get_string('openrequestreport').' ('.$openReq.')", exploded: "'.$openReqExploded.'" },
												{  y: '.$acceptReqPercentage.', label: "'.get_string('accept_request_closed').' ('.$acceptReq.')", exploded: "'.$acceptReqExploded.'"  },
												{  y: '.$declineReqPercentage.', label: "'.get_string('decline_request_closed').' ('.$declineReq.')", exploded: "'.$declineReqExploded.'"  },
					
												]
			
											}
											]
										});';
						
					if($openReqPercentage || $acceptReqPercentage || $declineReqPercentage){
						$requestHTML .=  '	chart.render(); ';
					}
						
					$requestHTML .=  '  }
									</script>
									<script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js"></script>';
			
				}else{
					$requestHTML .=  '<tr><td colspan="9" >'.get_string('norecordfound','singlecoursereport').'</td></tr>';
					$reportContentCSV .= get_string('norecordfound','multicoursereport')."\n";
					$reportContentPDF .= get_string('norecordfound','multicoursereport')."\n";
				}
					
				$reportContentPDF .= '</table>';
					
				$requestHTML .= '</tbody></table></div></div></div></div>';
			
				if(empty($export)){
					$requestHTML .= paging_bar($requestCount, $page, $perpage, $genURL);
					//$requestHTML .= '<input type = "button" value = "'.get_string('back','multicoursereport').'" onclick="location.href=\''.$CFG->wwwroot.'/my/adminreportdashboard.php\';">';
				}
				$requestHTML .= '';
					
				$globalRequestReport->requestHTML = $requestHTML;
				$globalRequestReport->reportContentCSV = $reportContentCSV;
				$globalRequestReport->reportContentPDF = $reportContentPDF;
					
				return $globalRequestReport;
			
			}
	
	  function getAstricHtml($string="Required"){
	     $html = '<span><img alt="Required field" src="'.$CFG->wwwroot.'/theme/image.php/gourmet/core/1426073186/req"></span> '.$string;
		 return $html;
	  }

	 function getUserListingForUserReport($page,$perpage,$sort,$dir,$searchString,$searchStatusString,$allCount=0){
		  	global $DB,$CFG;
		  	$offset = $page - 1;
		  	$offset = $offset*$perpage;
		  	$limit = '';
		  	if($perpage != 0){
		  		$limit = " LIMIT $offset, $perpage";
		  	}
		  		
		  	$excludedUsers = count($CFG->excludedUsers) > 0 ? implode(",",$CFG->excludedUsers) : 0;
		  	$sql = "";
		  	if($allCount==1){
		  		$sql .= "SELECT `mu`.`id` as id ";
		  		$sql .= " FROM (((((((((`mdl_user` `mu`
						LEFT JOIN `mdl_user` `mu_manager` ON((`mu`.`parent_id` = `mu_manager`.`id`)))
						LEFT JOIN `mdl_groups_members` `mgm` ON((`mgm`.`userid` = `mu`.`id` AND mgm.is_active=1)))
						LEFT JOIN `mdl_groups` `mg` ON((`mg`.`id` = `mgm`.`groupid`)))
						LEFT JOIN `mdl_department_members` `mdm` ON((`mdm`.`userid` = `mu`.`id`)))
						LEFT JOIN `mdl_department` `md` ON((`md`.`id` = `mdm`.`departmentid`)))
						LEFT JOIN `mdl_group_department` `gd` ON(((`gd`.`team_id` = `mg`.`id`) AND (`gd`.`is_active` = 1))))
						LEFT JOIN `mdl_job_title` `jt` ON((`jt`.`id` = `mu`.`job_title`)))
						LEFT JOIN `mdl_company` `com` ON((`com`.`id` = `mu`.`company`)))
		  				LEFT JOIN mdl_user as um ON ((mu.inline_manager_id = um.id)))
						WHERE ((1 = 1) AND (`mu`.`deleted` = 0)   AND (`md`.`deleted` = 0))";
		  		$sql .= "AND `mu`.`id` NOT IN(".$excludedUsers.")";
		  		$sql .= " $searchString ";
		  		$sql .= " $searchStatusString ";
		  		$sql .=  " GROUP BY `mu`.`id`
							ORDER BY `mu`.`firstname`";		  		
		  		
		  	}
		  	else{
		  		$sql .= "SELECT `mu`.`id` AS `id`,mu.is_manager_yes,`mu`.`city` AS `city`,mu.report_to,CONCAT(`um`.`firstname`,' ',`um`.`lastname`) AS `inlinemanager_name`,`mu`.`username` AS `username`,mu.user_identifier,mu.phone1 as phone1,`mu`.`email` AS `email`,`mu`.`department` AS `department`,`mu`.`firstaccess` AS `firstaccess`,`mu`.`lastaccess` AS `lastaccess`,`mu`.`lastlogin` AS `lastlogin`,`mu`.`currentlogin` AS `currentlogin`,`mu`.`timecreated` AS `timecreated`,`mu`.`updatedby` AS `updatedby`,`mu`.`timemodified` AS `timemodified`,`mu`.`parent_id` AS `parent_id`,`mu`.`is_instructor` AS `is_instructor`,`mu`.`is_primary` AS `is_primary`, CONCAT(`mu`.`firstname`,' ',`mu`.`lastname`) AS `fullname`, IF(`mgm`.`groupid`, GROUP_CONCAT(DISTINCT CONCAT(IF(ISNULL(`mg`.`name`),'', IF(ISNULL(`gd`.`id`), CONCAT('*',`mg`.`name`),`mg`.`name`)))
		  		
						ORDER BY `mg`.`name` ASC SEPARATOR ','),'') AS `teamtitle`, IF(`mdm`.`departmentid`, GROUP_CONCAT(DISTINCT IF((`md`.`is_external` = 1), CONCAT('*',`md`.`title`),`md`.`title`)
						ORDER BY `md`.`title` ASC SEPARATOR ', '),'') AS `departmenttitle`, IF(`mdm`.`departmentid`, GROUP_CONCAT(DISTINCT `mdm`.`departmentid`
						ORDER BY `md`.`title` ASC SEPARATOR ','),'') AS `department_id`, IF((CONCAT(`mu_manager`.`firstname`,' ',`mu_manager`.`lastname`) <> ''), CONCAT(`mu_manager`.`firstname`,' ',`mu_manager`.`lastname`),'') AS `usermanager`,`mu`.`job_title` AS `jobid`,`mu`.`company` AS `comid`,`jt`.`title` AS `job_title`,`com`.`title` AS `company`";
		  		$sql .= " FROM (((((((((`mdl_user` `mu`
						LEFT JOIN `mdl_user` `mu_manager` ON((`mu`.`parent_id` = `mu_manager`.`id`)))
						LEFT JOIN `mdl_groups_members` `mgm` ON((`mgm`.`userid` = `mu`.`id` AND mgm.is_active=1 AND mgm.is_active=1)))
						LEFT JOIN `mdl_groups` `mg` ON((`mg`.`id` = `mgm`.`groupid`)))
						LEFT JOIN `mdl_department_members` `mdm` ON((`mdm`.`userid` = `mu`.`id`)))
						LEFT JOIN `mdl_department` `md` ON((`md`.`id` = `mdm`.`departmentid`)))
						LEFT JOIN `mdl_group_department` `gd` ON(((`gd`.`team_id` = `mg`.`id`) AND (`gd`.`is_active` = 1))))
						LEFT JOIN `mdl_job_title` `jt` ON((`jt`.`id` = `mu`.`job_title`)))
						LEFT JOIN `mdl_company` `com` ON((`com`.`id` = `mu`.`company`)))
		  				LEFT JOIN mdl_user as um ON ((mu.inline_manager_id = um.id)))
						WHERE ((1 = 1) AND (`mu`.`deleted` = 0)  AND (`md`.`deleted` = 0))";
		  		$sql .= " $searchString ";
		  		$sql .= " $searchStatusString ";
		  		$sql .= "AND `mu`.`id` NOT IN(".$excludedUsers.")";
		  		
		  		$sql .=  " GROUP BY `mu`.`id`
						ORDER BY `mu`.`firstname`";
		  		$sql .= $limit;
		  	//echo $sql;die;	
		  	}
		 //echo $sql;die;
		
		  	$userRec = $DB->get_records_sql($sql);
		
		  	return $userRec;
	  }
	  function getUserListingForUserReportCount($searchString,$suspended=0){
		  	global $DB,$CFG;
		  	$excludedUsers = count($CFG->excludedUsers) > 0 ? implode(",",$CFG->excludedUsers) : 0;
		  	$sql = "SELECT count(distinct(mu.id)) FROM ((((((((`mdl_user` `mu`
					LEFT JOIN `mdl_user` `mu_manager` ON((`mu`.`parent_id` = `mu_manager`.`id`)))
					LEFT JOIN `mdl_groups_members` `mgm` ON((`mgm`.`userid` = `mu`.`id`)))
					LEFT JOIN `mdl_groups` `mg` ON((`mg`.`id` = `mgm`.`groupid`)))
					LEFT JOIN `mdl_department_members` `mdm` ON((`mdm`.`userid` = `mu`.`id`)))
					LEFT JOIN `mdl_department` `md` ON((`md`.`id` = `mdm`.`departmentid`)))
					LEFT JOIN `mdl_group_department` `gd` ON(((`gd`.`team_id` = `mg`.`id`) AND (`gd`.`is_active` = 1))))
					LEFT JOIN `mdl_job_title` `jt` ON((`jt`.`id` = `mu`.`job_title`)))
					LEFT JOIN `mdl_company` `com` ON((`com`.`id` = `mu`.`company`)))
					WHERE ((1 = 1) AND (`mu`.`deleted` = 0) AND (`md`.`deleted` = 0))";
		  	 
		  	$sql .= " $searchString ";
		  	$sql .= "AND `mu`.`id` NOT IN(".$excludedUsers.")";
		  	$sql .= "AND mu.suspended='".$suspended."'";
			$sql .= " GROUP BY  mu.suspended";
	
		 
		  	 $userRec = $DB->get_field_sql($sql);
		  	return $userRec;
	  }