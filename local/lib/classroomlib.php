<?php 

	defined('MOODLE_INTERNAL') || die();
		
	/**
	 * get course type name or all results
	 *
	 * @param mixed $courseTypeId is course type id
	  * @return string $result course type  name or all records (if $courseTypeId is null)
	 */
	 
	 
	function getCourseType($courseTypeId=''){
	
		global $DB, $CFG, $USER;
		$result = '';

		if(is_array($courseTypeId)){
		  $courseTypeIds = count($courseTypeId) > 0?implode(",",$courseTypeId):0;
		  if($courseTypeIds){
		    $result = $DB->get_field_sql("select GROUP_CONCAT(coursetype ORDER BY coursetype ASC SEPARATOR ', ') FROM {$CFG->prefix}coursetype WHERE id IN (".$courseTypeIds.")");
		  }
		}else{
			if($courseTypeId){
			   $result = $DB->get_field_sql("select coursetype FROM {$CFG->prefix}coursetype WHERE id IN (".$courseTypeId.") ORDER BY coursetype ASC ");
			}else{

			   $result = $DB->get_records_sql("select * FROM {$CFG->prefix}coursetype WHERE  1 = 1 ORDER BY coursetype ASC ");
			}
        }
			
		
		return $result;
	}
	
	/**
	 * get course type id by course id
	 *
	 * @param mixed $courseId is course id
	  * @return string $courseTypeId course type id 
	 */
	 
	 
	function getCourseTypeIdByCourseId($courseId=''){
	
		global $DB, $CFG, $USER;
		$courseTypeId = '';


		if($courseId){
		   $query = "select coursetype_id FROM {$CFG->prefix}course WHERE id IN (".$courseId.") ";
		   $courseTypeId = $DB->get_field_sql($query);
		}

			
		
		return $courseTypeId;
	}
	
	/**
	 * get course type name by course id
	 *
	 * @param mixed $courseId is course id
	  * @return string $courseTypeName course type  name 
	 */
	 
	 
	function getCourseTypeNameByCourseId($courseId=''){
	
		global $DB, $CFG, $USER;
		$courseTypeName = '';


		if($courseId){
		   $courseTypeName = $DB->get_field_sql("select coursetype FROM {$CFG->prefix}course mc INNER JOIN {$CFG->prefix}coursetype mct ON (mc.coursetype_id = mct.id )  WHERE mc.id IN (".$courseId.") ");
		}

		return $courseTypeName;
	}
	
	/**
	 * get course type name by course id
	 *
	 * @param mixed $courseId is course id
	 * @param mixed $fieldName is course field name
	  * @return string $courseTypeName course type  name 
	 */
	 
	 
	function getCourseFieldByCourseId($courseId='', $fieldName = 'id'){
	
		global $DB, $CFG, $USER;
		$courseFieldName = '';


		if($courseId){
		     $courseFieldName = $DB->get_field_sql("select ".$fieldName." FROM {$CFG->prefix}course WHERE id IN (".$courseId.") ");
		}

		return $courseFieldName;
	}
	
	
	/**
	 * get instructor full name or all results
	 *
	 * @param mixed $userId is instructor id
	 * @return string $result instructor full name or all records (if $userId is null)
	 */
	 
	 
	function getInstructor($userId){
	
		global $DB, $CFG, $USER;
		$result = '';

		if(is_array($userId)){
		  $userIds = count($userId) > 0 ? implode(",",$userId) : 0;
		  if($userIds){
		    $result = $DB->get_field_sql("select GROUP_CONCAT(concat(firstname,' ',lastname)  ORDER BY firstname ASC  SEPARATOR ', ') as fullname FROM {$CFG->prefix}user WHERE id IN (".$userIds.") AND deleted = '0' AND is_instructor = '1' ");
		  }
		}else{
			if($userId){
			   $result = $DB->get_field_sql("select concat(firstname,' ',lastname) as fullname FROM {$CFG->prefix}user WHERE id IN (".$teamId.") AND deleted = '0' AND is_instructor = '1' ORDER BY firstname ASC ");
			}else{
			    
			    if($CFG->allowExternalDepartment == 1 && $USER->archetype != $CFG->userTypeAdmin ){
					$where =   " AND d.id = '".$USER->department."'" ;
				}

				$sql = "select u.*, concat(u.firstname,' ',u.lastname,' (',u.username,')') as fullname 
				FROM {$CFG->prefix}user as u 
				LEFT JOIN {$CFG->prefix}department_members as dm ON (u.id=dm.userid) 
				LEFT JOIN {$CFG->prefix}department as d ON (dm.departmentid=d.id)
				WHERE u.deleted = '0' AND u.is_instructor = '1' AND d.status=1 AND d.deleted=0  $where ";
				$sql .= " ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC ";
				
			  // $result = $DB->get_records_sql("select *, concat(firstname,' ',lastname,' (',username,')') as fullname FROM {$CFG->prefix}user WHERE deleted = '0' AND is_instructor = '1'  ORDER BY firstname ASC ");
				 $result = $DB->get_records_sql($sql);
			  // pr($result);
			  // die;
			}
        }
			
		
		return $result;
	}
	
	/**
	 * get all primary instructors of any classroom 
	 *
	 * @param mixed $courseId is course  id
	  * @return mixed $result all primary instructors of classroom 
	 */
	 
	 
	function getPrimaryInstructorsOfClassroom($courseId){
	
		global $DB, $CFG, $USER;
		$result = array();

        $query = "select mc.id, mc.primary_instructor, mc.createdby, concat(u.firstname, ' ', u.lastname) as primary_instructor_name, u.username as primary_instructor_username, concat(uc.firstname, ' ', uc.lastname) as createdby_name, uc.username as createdby_username FROM {$CFG->prefix}course mc LEFT JOIN {$CFG->prefix}user as u ON (u.id = mc.primary_instructor AND u.deleted = '0' ) LEFT JOIN {$CFG->prefix}user as uc ON (uc.id = mc.createdby  AND uc.deleted = '0' ) WHERE 1 = 1 ";
		$query .= "  AND mc.deleted = '0'  ";
        if(is_array($courseId)){
		
			  $courseIds = count($courseId) > 0 ? implode(",",$courseId) : 0;
			  if($courseIds){
               $query .= " AND mc.id IN (".$courseIds.")" ;
			  }
		
		}else{
		
		     if($courseId){
	            $query .= " AND mc.id IN (".$courseId.")" ;
			 }
			 
		}  
		

		//$query .= " AND mc.primary_instructor!='' AND mc.createdby!='' ";
		$query .= "  GROUP BY mc.id ";
		//echo $query."<br>";die;
		$result = $DB->get_records_sql($query);
		
		return $result;
	}
	
	/**
	 * get all secondory instructors of any classroom 
	 *
	 * @param mixed $courseId is course  id
	  * @return mixed $result all secondory instructors of classroom 
	 */
	 
	 
	function getSecondaryInstructorsOfClassroom($courseId){
	
		global $DB, $CFG, $USER;
		$result = array();

        if(is_array($courseId)){
		
			  $courseIds = count($courseId) > 0 ? implode(",",$courseId) : 0;
			  if($courseIds){
			
				$query = "select sch.id as scheduler_id, sch.course, u.id as si_id, concat(u.firstname, ' ', u.lastname) as fullname, u.username FROM {$CFG->prefix}scheduler sch LEFT JOIN {$CFG->prefix}user as u ON (u.id = sch.teacher) WHERE sch.course IN (".$courseIds.") AND sch.teacher!=''  ";
				//echo $query."<br>";
				$result = $DB->get_records_sql($query);
			 }
		
		}else{
		
		     if($courseId){
			
				$query = "select sch.id as scheduler_id, sch.course, u.id as si_id, concat(u.firstname, ' ', u.lastname) as fullname, u.username FROM {$CFG->prefix}scheduler sch LEFT JOIN {$CFG->prefix}user as u ON (u.id = sch.teacher) WHERE sch.course IN (".$courseId.") AND sch.teacher!=''  ";
				//echo $query."<br>";
				$result = $DB->get_records_sql($query);
			 }else{
			 
			    $query = "select sch.id as scheduler_id, sch.course, u.id as si_id, concat(u.firstname, ' ', u.lastname) as fullname, u.username FROM {$CFG->prefix}scheduler sch LEFT JOIN {$CFG->prefix}user as u ON (u.id = sch.teacher) WHERE sch.teacher!='' ";
				$query .= "  AND u.deleted = '0' ";
				//echo $query."<br>";
				$result = $DB->get_records_sql($query);
				
			 }
			 
		}  
		
		

		return $result;
	}
	
	/**
	 * get secondory instructor of any class/schedule 
	 *
	 * @param mixed $classId is class/scheduler  id
	  * @return array $result secondory instructor of any class/schedule 
	 */
	 
	 
	function getSecondaryInstructorOfClass($classId){
	
		global $DB, $CFG, $USER;
		$result = array();

        if(is_array($classId)){
		
			  $classIds = count($classId) > 0 ? implode(",",$classId) : 0;
			  if($classIds){
			
				$query = "select sch.id, sch.createdby as class_creator, sch.course, sch.id, u.id as secondary_instructor, concat(u.firstname, ' ', u.lastname) as fullname, u.username, mc.primary_instructor, mc.createdby as classroom_creator  FROM {$CFG->prefix}scheduler sch LEFT JOIN {$CFG->prefix}course as mc ON (mc.id = sch.course) LEFT JOIN {$CFG->prefix}user as u ON (u.id = sch.teacher) WHERE sch.id IN (".$classIds.") AND sch.teacher!=''  ";
				//echo $query."<br>";
				$result = $DB->get_records_sql($query);
			 }
		
		}else{
		
		     if($classId){
			
				$query = "select sch.id, sch.createdby as class_creator, sch.course, u.id  as secondary_instructor, concat(u.firstname, ' ', u.lastname) as fullname, u.username, mc.primary_instructor, mc.createdby as classroom_creator FROM {$CFG->prefix}scheduler sch LEFT JOIN {$CFG->prefix}course as mc ON (mc.id = sch.course) LEFT JOIN {$CFG->prefix}user as u ON (u.id = sch.teacher) WHERE sch.id IN (".$classId.") AND sch.teacher!='' ";
				//echo $query."<br>";
				$result = $DB->get_record_sql($query);
			 }else{
			 
			    $query = "select sch.id, sch.createdby as class_creator, sch.course, u.id  as secondary_instructor, concat(u.firstname, ' ', u.lastname) as fullname, u.username, mc.primary_instructor, mc.createdby as classroom_creator FROM {$CFG->prefix}scheduler sch LEFT JOIN {$CFG->prefix}course as mc ON (mc.id = sch.course) LEFT JOIN {$CFG->prefix}user as u ON (u.id = sch.teacher) WHERE sch.teacher!='' ";
				$query .= "  AND mc.deleted = '0'  AND u.deleted = '0' ";
				//echo $query."<br>";
				$result = $DB->get_records_sql($query);
				
			 }
			 
		}  
		
		

		return $result;
	}
	
	/**
	 * get instructor classroom
	 *
	 * @param mixed $userId is instructor id
	 * @param int $return specifies in what form you want to return the result
	 * @return array $result course details
	 */
	 
	 
	function getInstructorClassroom($userId, $return=1){
	 
	    global $USER,$CFG,$DB;
	
	    
		if($return == 1){ // only course id
		     $courseSelectQuery = "SELECT c.id";
		}else if($return == 2){ // full course details
		
		     $courseSelectQuery = "SELECT c.id, c.category, c.fullname, c.idnumber , c.keywords, c.is_active, ca.name as cat_name, c.createdby, c.publish, c.coursetype_id, ct.coursetype";
					   
		}
		
		$courseQuery = $courseSelectQuery.
		               " FROM {$CFG->prefix}course c ".
					   " LEFT JOIN {$CFG->prefix}course_categories ca ON c.category = ca.id" . 
					   " LEFT JOIN {$CFG->prefix}coursetype ct ON c.coursetype_id = ct.id  WHERE c.coursetype_id = '".$CFG->courseTypeClassroom."' AND c.deleted = '0' " ;
					   
		
		if($USER->archetype == $CFG->userTypeAdmin){
		    $courseQuery .=   " " ;
		}elseif($USER->archetype == $CFG->userTypeManager){
		    $courseQuery .=   "  AND ( (c.primary_instructor = '".$userId."' AND c.is_active = '1' AND c.publish = '1' ) OR c.createdby = '".$userId."')" ;
		}elseif($USER->archetype == $CFG->userTypeStudent){
		  //$courseQuery .=   "  AND ( (c.primary_instructor = '".$userId."' AND c.is_active = '1' AND c.publish = '1' ) OR c.createdby = '".$userId."')" ;
		  $courseQuery .=   "  AND ( (c.primary_instructor = '".$userId."' AND c.is_active = '1' AND c.publish = '1' ) )" ;
		}
		
		
		$courseQuery .= " ORDER BY c.fullname ASC";		 
					   
		$courseDetails = $DB->get_records_sql($courseQuery);
		
		return $courseDetails;
	
	}
	
	/**
	 * get secondary instructor classroom
	 *
	 * @param mixed $userId is instructor id
	 * @param int $return specifies in what form you want to return the result
	 * @return array $result course details
	 */
	 
	 
	function getSecondaryInstructorClassroom($userId, $return=1){
	 
	    global $USER,$CFG,$DB;
	
	    
		if($return == 1){ // only course id
		     $courseSelectQuery = "SELECT c.id";
		}else if($return == 2){ // full course details
		
		     $courseSelectQuery = "SELECT c.id, c.category, c.fullname, c.idnumber , c.keywords, c.is_active, ca.name as cat_name, c.createdby, c.publish, c.coursetype_id, ct.coursetype";
					   
		}
		
		$courseQuery = $courseSelectQuery.
		               " FROM {$CFG->prefix}course c ".
					   " LEFT JOIN {$CFG->prefix}scheduler sch ON sch.course = c.id" .
					   " LEFT JOIN {$CFG->prefix}course_categories ca ON c.category = ca.id" . 
					   " LEFT JOIN {$CFG->prefix}coursetype ct ON c.coursetype_id = ct.id  " .
					   " WHERE c.coursetype_id = '".$CFG->courseTypeClassroom."' AND c.deleted = '0' AND c.id NOT IN (1)" ;
					   
		
		if($USER->archetype == $CFG->userTypeAdmin){
		    $courseQuery .=   "  AND c.id IN (0) " ; // we don't want classroom course in case of admin, because we will already get it by another method 
		}elseif($USER->archetype == $CFG->userTypeManager){
		    $courseQuery .=   " AND c.is_active = '1' AND c.publish = '1' AND sch.teacher = '".$userId."'" ;
		}elseif($USER->archetype == $CFG->userTypeStudent){
		  $courseQuery .=   " AND c.is_active = '1' AND c.publish = '1' AND sch.teacher = '".$userId."'" ;
		}
		
		
		$courseQuery .= "  GROUP BY c.id ORDER BY c.fullname ASC";		 
					   
		$courseDetails = $DB->get_records_sql($courseQuery);
		
		return $courseDetails;
	
	}
	
	function isSecondaryInstructorOfClass($userId, $classId){
	 
	    global $USER,$CFG,$DB;
	
	    $bool = false; 
	    if($userId &&  $classId){
			$courseSelectQuery = "SELECT c.id";
			$courseQuery = $courseSelectQuery.
						   " FROM {$CFG->prefix}course c ".
						   " LEFT JOIN {$CFG->prefix}scheduler sch ON sch.course = c.id" .
						   " LEFT JOIN {$CFG->prefix}course_categories ca ON c.category = ca.id" . 
						   " LEFT JOIN {$CFG->prefix}coursetype ct ON c.coursetype_id = ct.id  " .
						   " WHERE c.coursetype_id = '".$CFG->courseTypeClassroom."' AND c.deleted = '0'  AND c.id NOT IN (1)" ;
						   
			
			if($USER->archetype == $CFG->userTypeAdmin){
				$courseQuery .=   "  AND c.id IN (0) " ; // we don't want classroom course in case of admin, because we will already get it by another method 
			}elseif($USER->archetype == $CFG->userTypeManager){
				$courseQuery .=   " AND c.is_active = '1' AND c.publish = '1' AND sch.teacher = '".$userId."' AND sch.id = '".$classId."' " ;
			}elseif($USER->archetype == $CFG->userTypeStudent){
			  $courseQuery .=   " AND c.is_active = '1' AND c.publish = '1' AND sch.teacher = '".$userId."' AND sch.id = '".$classId."'" ;
			}
			
			
			$courseQuery .= "  GROUP BY c.id ORDER BY c.fullname ASC limit 1";		 
						   
			$bool = $DB->get_field_sql($courseQuery);
		}
		return $bool;
	
	}
	
	
	/**
	 * get resource type name or all results
	 *
	 * @param mixed $resourceTypeId is resource type id
	  * @return string $result resource type  name or all records (if $resourceTypeId is null)
	 */
	 
	 
	function getResourceType($resourceTypeId=''){
	
		global $DB, $CFG, $USER;
		$result = '';

		if(is_array($resourceTypeId)){
		  $resourceTypeIds = count($resourceTypeId) > 0?implode(",",$resourceTypeId):0;
		  if($resourceTypeIds){
		    $result = $DB->get_field_sql("select GROUP_CONCAT(name ORDER BY name ASC SEPARATOR ', ') FROM {$CFG->prefix}resource_type WHERE id IN (".$resourceTypeIds.") AND is_active = '1' AND deleted = '0' ");
		  }
		}else{
			if($resourceTypeId){
			   $result = $DB->get_field_sql("select name FROM {$CFG->prefix}resource_type WHERE id IN (".$resourceTypeId.") AND is_active = '1' AND deleted = '0'");
			}else{
			   $result = $DB->get_records_sql("select * FROM {$CFG->prefix}resource_type WHERE 1 = 1 AND is_active = '1' AND deleted = '0' ORDER BY CASE WHEN id != 6 THEN '' ELSE 6 END, name ");
			}
        }

		return $result;
	}
	
	
	/**
	 * get reference materials of class
	 * @param int $classId is class/scheduler id
	 * @param int $resourceId is resource id
	 * @return array $result reference materials details
	 */
	 
	 
	function getReferenceMaterialsOfClass($classId, $resourceId = ''){
	 
	    global $USER,$CFG,$DB;
	    $refMatDetails = array();
	    if($classId){

		   $refMatQuery = "SELECT mrm.id, mrm.scheduler_id, mrm.resource_id, sch.teacher, mc.fullname as coursename, mc.primary_instructor, mc.createdby as classroom_creator, mr.course, mr.name, mr.intro, mr.resource_type, mr.is_reference_material, mr.allow_student_access, mr.is_active, mr.revision, '".ucfirst($CFG->resourceModule)."' as modtype, rt.name as resoursetype_name, rt.id as resource_type_id, concat(mu.firstname, ' ', mu.lastname) as refmat_createdbyname, mr.createdby as refmat_createdby, mr.modifiedby as refmat_modifiedby";

		   $refMatQuery .=
						   " FROM {$CFG->prefix}class_reference_material mrm ".
						   " INNER JOIN {$CFG->prefix}scheduler sch ON sch.id = mrm.scheduler_id" .
						   " INNER JOIN {$CFG->prefix}resource mr ON mr.id = mrm.resource_id" .
						   " INNER JOIN {$CFG->prefix}user mu ON mu.id = mr.createdby" .
						   " INNER JOIN {$CFG->prefix}resource_type rt ON mr.resource_type = rt.id" .
						   " INNER JOIN {$CFG->prefix}course mc ON mc.id = mr.course" .
						   " WHERE mrm.scheduler_id = '".$classId."' AND mc.deleted = '0' AND mc.id NOT IN (1)" ;
						   
			
			
			if($USER->archetype == $CFG->userTypeAdmin){
				$refMatQuery .=   " " ; 
			}elseif($USER->archetype == $CFG->userTypeManager){
				$refMatQuery .=   " AND 1=1 " ;
			}elseif($USER->archetype == $CFG->userTypeStudent){
			  $refMatQuery .=   " AND mc.is_active = '1' AND mc.publish = '1' " ;
			}
			
			if($resourceId){			   
		   	  $refMatQuery .= " AND mr.id = '".$resourceId."'";	
			  $refMatQuery .= "  ORDER BY mr.name ASC";	
			  $refMatDetails = $DB->get_record_sql($refMatQuery);	   
			}else{
			  $refMatQuery .= "  ORDER BY mr.name ASC";
			  $refMatDetails = $DB->get_records_sql($refMatQuery);
			}
			
			
			
					   
		    
		}
		
		return $refMatDetails;
	
	}
	
	
	/**
	 * get primary, secondry and created by of classroom by course id
	 * @param mixed $courseId is classroom
	 * @param int $return specifies in what form you want to return the result
	 * @return array $result 
	 */
	
	function getAllTypeOfUsersOfClassroom($courseId, $return = 1){
	
	     global $CFG;
		 $userId = array();
		 $userIdWithDetails = array();
	     if($courseId){
		 
			    $primaryInstructorsOfClassroom = getPrimaryInstructorsOfClassroom($courseId);
			    $secondaryInstructorsOfClassroom = getSecondaryInstructorsOfClassroom($courseId);
			   
				if(count($primaryInstructorsOfClassroom) > 0 ){
				  foreach($primaryInstructorsOfClassroom as $rows){
					$userId[] = $rows->primary_instructor;
					$userId[] = $rows->createdby;
					$userIdWithDetails['primary_instructor'][] = $rows->primary_instructor;
					$userIdWithDetails['createdby'][] = $rows->createdby;
				  }
				}
				
				if(count($secondaryInstructorsOfClassroom) > 0 ){
				  foreach($secondaryInstructorsOfClassroom as $rows){
					$userId[] = $rows->si_id;
					$userIdWithDetails['secondary_instructor'][] = $rows->si_id;
				  }
				}
					

			   //pr($userId);
		       //pr($primaryInstructorsOfClassroom);
			   //pr($secondaryInstructorsOfClassroom);die;
		 }
		 
		  if($return == 1){	
		  	
			 if(count($userId) > 0 ){
			   $userId = array_unique($userId);
			 }
			  sort($userId);
			  return $userId;
			  
			}elseif($return == 2){	
			   return $userIdWithDetails;
			}
		 
		 
		 
	}
	
		
	/**
	 *  This function is using for getting date format for classroom session 
	 *  @param int $timeStamp the timestamp in UTC.
	 *  @param int $duration in minutes
	 *  @param int $durationType it can be secs/minutes/hours etc
	 *  @return string the formatted date/time.
	*/
	
	function getClassroomSessionDateDuration($timeStamp, $duration=60, $durationType = 'minutes', $for = ''){
	 
	  global $CFG;
	  $dateDuration = $timeStamp;
	  if($timeStamp){
	    $nDash = $for == 'csv'?getNDashForCSV():getNDash();
		$dateFormat = $for == 'csv'?$CFG->customDefaultDateFormatForCSV:$CFG->customDefaultDateFormat;
		$dateDuration = getDateFormat($timeStamp, $dateFormat." ".$CFG->customDefaultTimeFormat1)." ".$nDash." ".getDateFormat(strtotime("+ ".$duration." ".$durationType." ", $timeStamp), $CFG->customDefaultTimeFormat1);
	  }
	  
	  return $dateDuration;
	}
	
	/**
	 *  This function is using for checking access of reference material action
	 *  @param int $classId is class/scheduler id
	 *  @param int $resourceId is resource id
	 *  @return bool $bool
	*/
	
	function haveReferenceMaterialAccess($classId, $resourceId){
	 
	  global $CFG, $USER;
	  $bool = false;
	 
	  if($classId && $resourceId){
	    
		   $refMatArr = getReferenceMaterialsOfClass($classId, $resourceId);
		   //pr($refMatArr);die;
		   $refMatCreatedBy = $refMatArr->refmat_createdby;
				
		   if($USER->archetype == $CFG->userTypeAdmin ){
				 $bool = true;
		   }elseif($USER->archetype == $CFG->userTypeManager ){
				
				$secondaryInstructorOfClass = getSecondaryInstructorOfClass($classId);

				if( in_array($refMatCreatedBy, array($USER->id, $secondaryInstructorOfClass->secondary_instructor)) ){
				   $bool = true;
				}

		   }elseif($USER->archetype == $CFG->userTypeStudent ){ 
				if($refMatCreatedBy == $USER->id){
				   $bool = true;
				}
		
		   }
			
	  }
	  
	  return $bool;
	  
	}
	
	/**
	 *  This function is using for checking access of resource action
	 *  @param int $resourceId is resource id
	  *  @return bool $bool
	*/
	
	function haveResourceAccess($resourceId){
	 
	  global $DB, $CFG, $USER;
	  $bool = false;

	  if($resourceId){
	    
		   $resourceArr = $DB->get_record('resource',array('id'=>$resourceId));
		   $resourceCreatedBy = $resourceArr->createdby;

		   if($USER->archetype == $CFG->userTypeAdmin ){
				 $bool = true;
		   }elseif($USER->archetype == $CFG->userTypeManager ){
				
				if( in_array($resourceCreatedBy, array($USER->id)) ){
				   $bool = true;
				}

		   }elseif($USER->archetype == $CFG->userTypeStudent ){ 
				if($resourceCreatedBy == $USER->id){
				   $bool = true;
				}
		
		   }
			
	  }
	  
	  return $bool;
	  
	}
	
	/**
	 *  This function is using for checking access of resource asset action
	 *  @param int $resourceId is resource id
	  *  @return bool $bool
	*/
	
	function haveResourceAssetAccess($resourceId){
	 
	  global $DB, $CFG, $USER;
	  $bool = false;

	  if($resourceId){
	    
		   $resourceArr = $DB->get_record('resource',array('id'=>$resourceId));
		   $resourceCreatedBy = $resourceArr->createdby;
		   $courseId = $resourceArr->course;
		   
		   
			$primaryInstructors = array();
			$primaryInstructorsOfClassroomArr = getPrimaryInstructorsOfClassroom($courseId);
			$course = $DB->get_record('course',array('id'=> $courseId));
			if(count($primaryInstructorsOfClassroomArr) > 0 ){
			  foreach($primaryInstructorsOfClassroomArr as $rows){
				$primaryInstructors[] = $rows->primary_instructor;
			  }
			}


		   if($USER->archetype == $CFG->userTypeAdmin ){
				 $bool = true;
		   }elseif($USER->archetype == $CFG->userTypeManager ){
				
				if( (in_array($USER->id, $primaryInstructors) && $course->createdby != $resourceCreatedBy ) || ($course->createdby == $USER->id)){ 
				   $bool = true;
				}else{
				   $bool = false;
				}
		

		   }elseif($USER->archetype == $CFG->userTypeStudent ){ 
				if( (in_array($USER->id, $primaryInstructors) && $course->createdby != $resourceCreatedBy ) || ($course->createdby == $USER->id)){ 
				   $bool = true;
				}else{
				   $bool = false;
				}
		
		   }
			
	  }
	  
	  return $bool;
	  
	}
	
	/**
	 *  This function is using for checking access of course action
	 *  @param int $courseId is course id
	  *  @return int $accessType 0=>No Access 1=>Full Access  2=>View All Assest(s), View All Class(s), View All Session(s) of Class(s), View User Enrollment,  Manage Assets,                                    Manage Reference Material 3=>View All Assest(s), View Own Class(s) Only, View Session(s) of Own Class(s) Only, View User Enrollment of Own                                    Classes, Manage Own Reference Material 
	  
	    $CFG->classroomNoAccess = 0;
		$CFG->classroomFullAccess = 1;
		$CFG->classroomMediumAccess = 2;
		$CFG->classroomLowAccess = 3;
	*/
	
	function haveClassroomCourseAccess($courseId){
	 
	  global $DB, $CFG, $USER;
	  $accessType = $CFG->classroomNoAccess; 

	  if($courseId){
	    
		   $courseArr = $DB->get_record('course',array('id'=>$courseId));
		   $courseCreatedBy = $courseArr->createdby;
		   $primaryInstructor = $courseArr->primary_instructor?$courseArr->primary_instructor:$courseCreatedBy;

		   $secondaryInstructorOfClassroomArr = getSecondaryInstructorsOfClassroom($courseId);
		   $secondaryInstructorIds = array();
		   if(count($secondaryInstructorOfClassroomArr) > 0 ){
				  foreach($secondaryInstructorOfClassroomArr as $rows){
					$secondaryInstructorIds[] = $rows->si_id;
				  }
		   }

		   if($USER->archetype == $CFG->userTypeAdmin){

				$accessType = $CFG->classroomFullAccess;
					
			}elseif($USER->archetype == $CFG->userTypeManager){
				
				if( $courseCreatedBy == $USER->id){
                  $accessType = $CFG->classroomFullAccess;
				}else{
				
					if($primaryInstructor == $USER->id){
					    $accessType = $CFG->classroomMediumAccess;
					}else{
					  if(in_array($USER->id, $secondaryInstructorIds)){
					    $accessType = $CFG->classroomLowAccess;
					  }
					}
                }					
					
			}elseif($USER->archetype == $CFG->userTypeStudent ){
			  
				  if($primaryInstructor == $USER->id){
					$accessType = $CFG->classroomMediumAccess;
				  }else{
					  if(in_array($USER->id, $secondaryInstructorIds)){
						 $accessType = $CFG->classroomLowAccess;
					  }
				  }

			}
			
	  }
	  
	  return $accessType;
	  
	}
	
	
	/**
	 * To check whether classroom has asset(s) or not
	  *  @param int $courseId is course id
	 * @return bool $haveAsset if have then return true else false
	 */
	 
	 
	function isClassroomHaveAsset($courseId){
	 
	    global $USER,$CFG,$DB;
	    $haveAsset = false;
	    if($courseId){

		       $query = " SELECT id FROM {$CFG->prefix}resource WHERE course = '".$courseId."' AND is_reference_material = '0' AND is_active = '1'" ;
			   $records = $DB->get_records_sql($query);
			   $totalhaveAssetAsset = count($records);
			   if($totalhaveAssetAsset > 0 ){
			     $haveAsset = $totalhaveAssetAsset;
			   }
 
		}
		
		return $haveAsset;

	
	}
	
	/**
	 * To check whether classroom has atleast one "haveAsset" Asset 
	  *  @param int $courseId is course id
	 * @return bool $specific if have then return true else false
	 */
	 
	 
	function isClassroomHaveSpecificAsset($courseId){
	 
	    global $USER,$CFG,$DB;
	    $specific = false;
	    if($courseId){

		   $rTQuery = "SELECT id FROM {$CFG->prefix}resource_type WHERE `specific` = '1'";
		   $rTRes = $DB->get_records_sql($rTQuery);
		   $rTIdArr = array_keys($rTRes);
           if(count($rTIdArr) > 0){
		   
		       $rTIds = implode(",", $rTIdArr);
		       $query = " SELECT id FROM {$CFG->prefix}resource WHERE course = '".$courseId."' AND resource_type IN (".$rTIds.") AND is_reference_material = '0' AND is_active = '1'" ;
						   
			   $records = $DB->get_records_sql($query);
			   $totalSpecificAsset = count($records);
			   if($totalSpecificAsset > 0 ){
			     $specific = $totalSpecificAsset;
			   }
						   
		   }else{
		     $specific = true;
		   }
		   
 
		}
		
		return $specific;

	
	}
	
	/**
	 * To check whether resource classroom has "Specific" Asset 
	  *  @param int $resourceId is resource id
	 * @return bool $specific if have then return true else false
	 */
	 
	 
	function isResourceHaveSpecificAsset($resourceId){
	 
	    global $USER,$CFG,$DB;
	    $specific = false;
	    if($resourceId){

		   $rTQuery = "SELECT id FROM {$CFG->prefix}resource_type WHERE `specific` = '1'";
		   $rTRes = $DB->get_records_sql($rTQuery);
		   $rTIdArr = array_keys($rTRes);
           if(count($rTIdArr) > 0){
		   
		       $rTIds = implode(",", $rTIdArr);
		       $query = " SELECT id FROM {$CFG->prefix}resource WHERE id = '".$resourceId."' AND resource_type IN (".$rTIds.") AND is_reference_material = '0'" ;
						   
			   $records = $DB->get_records_sql($query);

			   if(count($records) > 0 ){
			     $specific = true;
			   }
						   
		   }else{
		     $specific = true;
		   }
		   
 
		}
		
		return $specific;
	
	}
	
	/**
	 * To get "Specific" Asset of classroom resource
	  *  @param string $field is field of table
	 * @return string $specific  return "Specific" Asset of classroom resource
	 */
	 
	 
	function getClassroomSpecificAsset($field = 'name'){
	 
	    global $USER,$CFG,$DB;
	    $specific = '';
	    $query = "SELECT group_concat($field) as name FROM {$CFG->prefix}resource_type WHERE `specific` = '1'";
	    $specific = $DB->get_field_sql($query);
		return $specific;
	
	}
	
	
	
	
	/**
	 * To check whether classroom has any user enrollment
	 *  @param int $courseId is course id
	 * @return bool $haveEnrollment if have then return true else false
	 */
	 
	 
	function isClassroomHaveAnyEnrollment($courseId){
	 
	    global $USER,$CFG,$DB;
	    $haveEnrollment = false;
	    if($courseId){

		   $query = "SELECT id FROM {$CFG->prefix}scheduler WHERE course = '".$courseId."'";
		   $records = $DB->get_records_sql($query);
		   $idArr = array_keys($records);
           if(count($idArr) > 0){
		       $haveEnrollment = isClassHaveAnyEnrollment($idArr);
		   }
		   
 
		}
		
		return $haveEnrollment;
	
	}
	
	/**
	 * To check whether class has any user enrollment
	 *  @param mixed $classId is class/scheduler id
	 * @return bool $haveEnrollment if have then return true else false
	 */
	 
	 
	function isClassHaveAnyEnrollment($classId){
	 
	    global $USER,$CFG,$DB;
	    $haveEnrollment = false;
		$ids = 0;
	    if(is_array($classId)){
		   $ids = count($classId) > 0 ? implode(",",$classId) : 0;
		}else{
		    $ids = $classId;  
		}
		
		   $query = " SELECT id, userid FROM {$CFG->prefix}scheduler_enrollment WHERE scheduler_id IN (".$ids.") ";
		   $query .= " AND is_approved IN (3,0,1)" ;
		   $records = $DB->get_records_sql($query);
		   if(count($records) > 0 ){
			 $haveEnrollment = true;
		   }
		   
		return $haveEnrollment;
	
	}
	
	
	/**
	 * To get the user enrollment details for a particular class
	 *  @param int $classId is class/scheduler id
	 *  @param int $userId is user id
	 *  @return array $records
	 */
	 
	 
	function getUserEnrollmentDetailsInClass($classId, $userId = ''){
	 
	    global $USER,$CFG,$DB;
	    $records = array();
		$ids = 0;
	    if(is_array($classId)){
		   $ids = count($classId) > 0 ? implode(",",$classId) : 0;
		}else{
		    $ids = $classId;  
		}
		
	    if($userId){
		   $query = " SELECT mse.scheduler_id, mse.*, ms.course, ms.name, ms.enrolmenttype, ms.teacher, ms.startdate as class_startdate, ms.enddate as class_enddate, ms.no_of_seats, ms.isactive as is_class_active, ms.createdby as class_createdby, ms.isclasscompleted as is_class_submitted, concat(mu.firstname, ' ', mu.lastname) as leranerfullname, ms.submitted_by, ms.submitted_on, concat(mu_submitted.firstname, ' ', mu_submitted.lastname) as class_submittedby_name, mu_submitted.username as class_submittedby_username  FROM {$CFG->prefix}scheduler_enrollment mse LEFT JOIN {$CFG->prefix}scheduler ms ON (mse.scheduler_id = ms.id) LEFT JOIN {$CFG->prefix}user mu ON (mu.id = mse.userid) LEFT JOIN {$CFG->prefix}user mu_submitted ON (mu.id = ms.submitted_by)  ";
		   $query .= " WHERE  1 = 1 ";
		   $query .= " AND mu.deleted = '0' ";
		   $query .= " AND mse.scheduler_id IN (".$ids.")  AND mse.userid = '".$userId."'" ;
	    }else{
	       $query = " SELECT mse.*, ms.course, ms.name, ms.enrolmenttype, ms.teacher, ms.startdate as class_startdate, ms.enddate as class_enddate, ms.no_of_seats, ms.isactive as is_class_active, ms.createdby as class_createdby, ms.isclasscompleted as is_class_submitted, concat(mu.firstname, ' ', mu.lastname) as leranerfullname, ms.submitted_by, ms.submitted_on, concat(mu_submitted.firstname, ' ', mu_submitted.lastname) as class_submittedby_name, mu_submitted.username as class_submittedby_username  FROM {$CFG->prefix}scheduler_enrollment mse LEFT JOIN {$CFG->prefix}scheduler ms ON (mse.scheduler_id = ms.id) LEFT JOIN {$CFG->prefix}user mu ON (mu.id = mse.userid) LEFT JOIN {$CFG->prefix}user mu_submitted ON (mu.id = ms.submitted_by) ";
		   $query .= " WHERE  1 = 1 ";
		   $query .= " AND mu.deleted = '0' ";
		   $query .= " AND mse.scheduler_id IN (".$ids.")  ";
		   $query .= " ORDER BY mu.firstname" ;
		   
	    }
	    $records = $DB->get_records_sql($query);
	    if(count($records) > 0 ){
		  $records = $records;
	    } else{
		  $records[$classId] = array();
	    }
		   
		return $records;
	
	}
	
	
	/**
	 * To get the status of class for a particular learner
	 *  @param mixed $classId is class/scheduler id
	 *  @param mixed $userId is leraner id
	 *  @return html $userStatusInClass
	 */
	 
	 
	function getClassStatusForLearner($classId, $userId){
	 
	    global $USER, $CFG, $DB;
		
		$userStatusInClass = '';
		if($classId && $userId){
		
			$userEnrollmentDetailsInClass = getUserEnrollmentDetailsInClass($classId, $userId);
				
			    if(isset($userEnrollmentDetailsInClass[$classId]) && count($userEnrollmentDetailsInClass[$classId]) > 0){ // user recevies the email for enroll or already enrolled in this class and waiting for  manager approval(if manager of class is not the manager of user), if manager of class is same as manager of user then do not need to approve by manager.
					//echo "$classId, $userId<br>";
					  if($userEnrollmentDetailsInClass[$classId]->is_class_submitted == $CFG->classCompleted){
						  //$userStatusInClass = '<a href="javascript:;" class="completd-in-class">'.get_string('classcompleted','scheduler').'</a>';
						   if($userEnrollmentDetailsInClass[$classId]->is_completed == $CFG->classCompleted){
								$userStatusInClass = get_string('classcompleted','scheduler');
						   }else{
								$userStatusSql = "SELECT sa.* FROM mdl_scheduler as s LEFT JOIN mdl_scheduler_slots as ss ON s.id = ss.schedulerid LEFT JOIN mdl_scheduler_appointment as sa ON ss.id = sa.slotid WHERE sa.studentid = $userId AND s.id = $classId AND sa.attended = 1";
								$userStatusDetails = $DB->get_records_sql($userStatusSql);
								if(!empty($userStatusDetails)){
									$userStatusInClass = get_string('class_incomplete','classroomreport');
								}else{
									$userStatusInClass = get_string('classnoshow','classroomreport');
								}
						   }
					  }elseif($userEnrollmentDetailsInClass[$classId]->is_class_submitted == $CFG->classInCompleted && $userEnrollmentDetailsInClass[$classId]->is_completed == $CFG->classCompleted){
						$userStatusInClass = get_string('classapproved','scheduler');
					  }elseif($userEnrollmentDetailsInClass[$classId]->is_completed == $CFG->classInCompleted || trim($userEnrollmentDetailsInClass[$classId]->is_completed) == ''){
						 //$userStatusInClass = '<a href="javascript:;" class="incompletd-in-class">'.get_string('classincompleted','scheduler').'</a>';
						 $userStatusInClass = get_string('classincompleted','scheduler');
						 if($userEnrollmentDetailsInClass[$classId]->is_approved == $CFG->requestForClass){
							 if($USER->archetype != $CFG->userTypeManager && $USER->original_archetype != $CFG->userTypeManager){
								 $userStatusInClass = '<a href="javascript:;" class="enroll-in-class" relclass = "'.$classId.'" reluser = "'.$userId.'" title = "'.get_string('requestclass','scheduler').'" >'.get_string('requestclass','scheduler').'</a>';
							 }else{
								$userStatusInClass = '<a href="javascript:;" class="enrol_user accept" relclass = "'.$classId.'" reluser = "'.$userId.'" rels = "'.$userEnrollmentDetailsInClass[$classId]->id.'" title = "Accept" >Accept</a>';
							 }
						 }elseif($userEnrollmentDetailsInClass[$classId]->is_approved == $CFG->waitingForManagerApprovalForClass){
							 if($USER->archetype != $CFG->userTypeManager && $USER->original_archetype != $CFG->userTypeManager){
								//$userStatusInClass = '<a href="javascript:;" class="waiting-approval-in-class">'.get_string('waitingformanagerapproval','scheduler').'</a>';
								$userStatusInClass = get_string('waitingformanagerapproval','scheduler');
							 }else{
								$userStatusInClass = '<a href="javascript:;" class="enrol_user accept" relclass = "'.$classId.'" reluser = "'.$userId.'" rels = "'.$userEnrollmentDetailsInClass[$classId]->id.'" title = "Accept" >Accept</a>';
							 }
						 }elseif($userEnrollmentDetailsInClass[$classId]->is_approved == $CFG->classApproved){
							 //$userStatusInClass = '<a href="javascript:;" class="enrollment-approved-in-class">'.get_string('classapproved','scheduler').'</a>';
							 $userStatusInClass = get_string('classapproved','scheduler');
						 }elseif($userEnrollmentDetailsInClass[$classId]->is_approved == $CFG->classDeclined){
							$userStatusInClass = get_string('classdeclined','scheduler');
						 }
					  }
				}else{ // means classroom is enroll only through program, so we will diaply this class and user can send an open invite to his manager to enroll in same class
					
					// user is not enroll in this class, so he will request for this class
					$classEndDate = $userEnrollmentDetailsInClass[$classId]->class_enddate;
					if($classEndDate < $curtime){
					   //echo $schedule->name."<br>";
					   continue; // because end date has been passed for this class, so class will not be visible for this user, because in this case we will show only upcoming classes             
					  
					}
					if($USER->archetype != $CFG->userTypeManager && $USER->original_archetype != $CFG->userTypeManager){
						$userStatusInClass = '<a href="javascript:;" class="request-for-class" relclass = "'.$classId.'" reluser = "'.$userId.'" >'.get_string('requestclass','scheduler').'</a>';
					}else{
						$userStatusInClass = '<a href="javascript:;" class="enrol_user accept" relclass = "'.$classId.'" reluser = "'.$userId.'" rels = "'.$userEnrollmentDetailsInClass[$classId]->id.'"  title = "Accept" >Accept</a>';
					}
				}
	    }
	    return $userStatusInClass;
	}
	
	
	
	
	/**
	 * To delete resource of a course
	 *  @param mixed $cmId is course module id
     *  @param mixed $courseId is course id
	 *  @param mixed $resourceId is resource id
	  * @return bool $delete if deleted then return true else false
	 */
	function deleteResource($courseId, $cmId, $resourceId){
	
	  global $DB, $CFG, $USER;
	  $delete = false;
	  if($courseId && $cmId && $resourceId){
	     
			 $courseTypeId = getCourseTypeIdByCourseId($courseId);
			 if($courseTypeId == $CFG->courseTypeClassroom ){
			 
			     //$haveResourceAccess = haveResourceAccess($resourceId);
				$haveResourceAccess = true;
				$haveEnrollment = isClassroomHaveAnyEnrollment($courseId);
				if($haveResourceAccess && !$haveEnrollment){
				//echo "$resourceId, $courseId";die;
					  $cm = get_coursemodule_from_id(null, $cmId, $courseId, false, MUST_EXIST);
					  if(isset($cm->id) && !empty($cm->id)){
						$modContext = context_module::instance($cm->id);
						require_capability('moodle/course:manageactivities', $modContext);
						course_delete_module($cm->id);
						$delete = true;  
					  }
					  
				}	  
			}else{

				$haveEnrollment = getAssignUsersForCourse($courseId);

				if(!$haveEnrollment){
				//echo "$resourceId, $courseId";die;
					  $cm = get_coursemodule_from_id(null, $cmId, $courseId, false, MUST_EXIST);
					  if(isset($cm->id) && !empty($cm->id)){
						$modContext = context_module::instance($cm->id);
						require_capability('moodle/course:manageactivities', $modContext);
						course_delete_module($cm->id);
						$delete = true;  
					  }
					  
				}	
				
			}		  
	  }
	  
	  return $delete;
	  
	}
	
	
	function deleteRefrenceMaterial($courseId, $cmId, $resourceId,$classId){
	
		global $DB, $CFG, $USER;
		$delete = false;
		if($courseId && $cmId && $resourceId){
	
			$courseTypeId = getCourseTypeIdByCourseId($courseId);
			if($courseTypeId == $CFG->courseTypeClassroom ){
	
				//$haveResourceAccess = haveResourceAccess($resourceId);
				$haveResourceAccess = true;
				$haveEnrollment = isClassHaveAnyEnrollment($classId);
				if($haveResourceAccess && !$haveEnrollment){
					//echo "$resourceId, $courseId";die;
					$cm = get_coursemodule_from_id(null, $cmId, $courseId, false, MUST_EXIST);
					if(isset($cm->id) && !empty($cm->id)){
						$modContext = context_module::instance($cm->id);
						require_capability('moodle/course:manageactivities', $modContext);
						course_delete_module($cm->id);
						$delete = true;
					}
						
				}
			}else{
	
				$haveEnrollment = getAssignUsersForCourse($courseId);
	
				if(!$haveEnrollment){
					//echo "$resourceId, $courseId";die;
					$cm = get_coursemodule_from_id(null, $cmId, $courseId, false, MUST_EXIST);
					if(isset($cm->id) && !empty($cm->id)){
						$modContext = context_module::instance($cm->id);
						require_capability('moodle/course:manageactivities', $modContext);
						course_delete_module($cm->id);
						$delete = true;
					}
						
				}
	
			}
		}
		 
		return $delete;
		 
	}
	
	/**
	 * To delete scrom of a course
	 *  @param mixed $cmId is course module id
     *  @param mixed $courseId is course id
	 *  @param mixed $scromId is scrom id
	  * @return bool $delete if deleted then return true else false
	 */
	function deleteScrom($courseId, $cmId, $scromId){
	
	  global $DB, $CFG, $USER;
	  $delete = false;
	  if($courseId && $cmId && $scromId){
	     
			 $courseTypeId = getCourseTypeIdByCourseId($courseId);
			 if($courseTypeId == $CFG->courseTypeOnline ){
			 
				$haveEnrollment = getAssignUsersForCourse($courseId);

				if(!$haveEnrollment){
				      //echo "$scromId, $courseId";die;
					  $cm = get_coursemodule_from_id(null, $cmId, $courseId, false, MUST_EXIST);
					  if(isset($cm->id) && !empty($cm->id)){
						$modContext = context_module::instance($cm->id);
						require_capability('moodle/course:manageactivities', $modContext);
						course_delete_module($cm->id);
						$delete = true;  
					  }
				}	
				
			}		  
	  }
	  
	  return $delete;
	  
	}
	
	function enrolIntoOpenCourse($classId,$enrolIds,$module,$actionType='add'){
		global $DB, $CFG, $USER;
		$newIds = array();
		$time = time();
		$removed_ids = "";
		if($actionType=='add'){
		foreach($enrolIds as $enrolId){
			$newIds[] = $enrolId;
			$isexist = $DB->record_exists('scheduler_open_for',array('classid'=>$classId,'enrolid'=>$enrolId,'enrolfor'=>$module));
			if(!$isexist){
				$record = new stdClass();
				$record->classid= $classId;
				$record -> enrolfor = $module;
				$record ->enrolid =$enrolId;
				$record ->createddate = $time;
				$DB->insert_record('scheduler_open_for',$record);
			}
		}
		}
	if($actionType=='remove'){
		foreach($enrolIds as $enrolId){
			
			$isexist = $DB->get_record('scheduler_open_for',array('classid'=>$classId,'enrolid'=>$enrolId,'enrolfor'=>$module));
			if($isexist){
				$DB->delete_records('scheduler_open_for',array('id'=>$isexist->id));
			}
		}
	}
		/* if($newIds){
			$removed_ids = implode(',',$newIds);
			$removed_ids = " AND enrolid NOT IN (".$removed_ids.")";
		}
		$sql = "select * from mdl_scheduler_open_for where classid = ".$classId.$removed_ids." AND enrolfor = '".$module."'";
		$result_set = $DB->get_records_sql($sql);
	
		if($result_set){
			foreach($result_set as $result){
	
	
				$DB->delete_records('scheduler_open_for',array('id'=>$result->id));
	
			}
		} */
		
		
	
	
	
	}
	
	
	function sendOpenInvitetoUsers($classId,$departments,$teams,$users,$all=false){
		global $DB, $CFG, $USER;
		$all_users = array();
		$scheduler_rec = $DB->get_record_sql('SELECT s.*,c.fullname FROM mdl_scheduler as s LEFT JOIN mdl_course as c ON c.id = s.course WHERE s.id = '.$classId);
		$time = time();
		if(!$all){
			
			foreach($departments as $deptId){
				if($deptId!=""){
				$query = "SELECT mdm.userid as userid
				FROM mdl_user AS u
				LEFT JOIN mdl_department_members AS mdm ON u.id = mdm.userid
				WHERE u.confirmed = 1 AND u.deleted = 0 AND u.suspended = 0 AND mdm.departmentid=".$deptId;
	
				$deptUsers = $DB->get_records_sql($query);
	
				foreach($deptUsers as $user){
					$all_users[] = $user->userid;
	
				}
	
				$records = $DB->get_record('scheduler_open_for',array('enrolid'=>$deptId,'enrolfor'=>'department','classid'=>$classId));
				$update_rec = new stdClass();
				$update_rec->id = $records->id;
				$update_rec->email_status = 1;
				$update_rec->email_sent_date = $time;
				$DB->update_record('scheduler_open_for',$update_rec);
				}
			}
	
			foreach($teams as $teamId){
				if($teamId!=""){
				$query = "SELECT mdm.userid as userid
				FROM mdl_user AS u
				LEFT JOIN mdl_groups_members AS mdm ON u.id = mdm.userid
				WHERE u.confirmed = 1 AND u.deleted = 0 AND u.suspended = 0 AND mdm.groupid=".$teamId;
	
				$teamUsers = $DB->get_records_sql($query);
				//$teamUsers = $DB->get_records('groups_members',array('groupid'=>$teamId));
				foreach($teamUsers as $user){
					$all_users[] = $user->userid;
				}
				$records = $DB->get_record('scheduler_open_for',array('enrolid'=>$teamId,'enrolfor'=>'team','classid'=>$classId));
				$update_rec = new stdClass();
				$update_rec->id = $records->id;
				$update_rec->email_status = 1;
				$update_rec->email_sent_date = $time;
				$DB->update_record('scheduler_open_for',$update_rec);
				}
			}
			foreach($users as $userId){
				if($userId!=""){
				$all_users[] = $userId;
				$records = $DB->get_record('scheduler_open_for',array('enrolid'=>$userId,'enrolfor'=>'user','classid'=>$classId));
				$update_rec = new stdClass();
				$update_rec->id = $records->id;
				$update_rec->email_status = 1;
				$update_rec->email_sent_date = $time;
				$DB->update_record('scheduler_open_for',$update_rec);
				}
			}
	
			foreach($all_users as $user){
				$enrol_rec = $DB->get_record('scheduler_enrollment',array('scheduler_id'=>$classId,'userid'=>$user));
				if(!$enrol_rec){
					$insert_rec = new stdClass();
					$insert_rec->scheduler_id = $classId;
					$insert_rec->userid = $user;
					$insert_rec->is_approved = 3;
					$insert_rec->assigned_by = $USER->id;
					$insert_rec->timecreated = $time;
					$insert_rec->request_date = $time;
					$insert_rec->response_date = $time;
					$enrolment_id = $DB->insert_record('scheduler_enrollment',$insert_rec);
					emailForSendOpenInvitation($user,$scheduler_rec,$enrolment_id);
				}
				else{
					$update_rec = new stdClass();
					$update_rec->id = $enrol_rec->id;
					$update_rec->email_status = 1;
					$update_rec->email_sent_date = $time;
					$update_rec->timemodified = $time;
	
					$DB->update_record('scheduler_enrollment',$update_rec);
					emailForSendOpenInvitation($user,$scheduler_rec,$enrol_rec->id);
				}
			}
	
	
		}
		else{
			$excludedUsers = implode(',',$CFG->excludedUsers);
			$query = "SELECT * FROM mdl_user as u WHERE u.confirmed = 1 AND u.deleted = 0 AND u.suspended = 0 AND u.id NOT IN (".$excludedUsers.")";
			$allUsers = $DB->get_records_sql($query);
	
			foreach($allUsers as $user){
				$enrol_rec = $DB->get_record('scheduler_enrollment',array('scheduler_id'=>$classId,'userid'=>$user->id));
				if(!$enrol_rec){
					$insert_rec = new stdClass();
					$insert_rec->scheduler_id = $classId;
					$insert_rec->userid = $user->id;
					$insert_rec->is_approved = 3;
					$insert_rec->assigned_by = $USER->id;
					$insert_rec->timecreated = $time;
					$insert_rec->email_status = 1;
					$insert_rec->email_sent_date = $time;
					$insert_rec->request_date = $time;
					$insert_rec->response_date = $time;
					$enrolment_id = $DB->insert_record('scheduler_enrollment',$insert_rec);
					emailForSendOpenInvitation($user,$scheduler_rec,$enrolment_id,'all');
				}
				else{
					$update_rec = new stdClass();
					$update_rec->id = $enrol_rec->id;
					$update_rec->email_status = 1;
					$update_rec->email_sent_date = $time;
					$update_rec->timemodified = $time;
						
					$DB->update_record('scheduler_enrollment',$update_rec);
					emailForSendOpenInvitation($user,$scheduler_rec,$enrol_rec->id,'all');
				}
			}
	
		}
	
	}
	
	function is_checked($key,$value,$type){
		if($key==$value){
			if($type=="checkbox"){
				return "checked='checked'";
			}
			else if($type=="select"){
				return "selected='selected'";
			}
	
		}
		return;
	}
	
	function getUserClassByUser($userId,$courseId,$checkEnrol,$selectionType = 0,$isProgram,$showClasses = 'all',$showClassStatus = 'all'){
		global $CFG, $DB,$USER;
		$where = "";
		if($userId != 0){
			$where = " AND se.userid = ".$userId." ";
		}
		if($isProgram != 1){
			if($selectionType == 1){
				if($showClasses == 'not_started'){
					$where .= " AND s.startdate > ".strtotime('now')." ";
				}elseif($showClasses == 'in_progress'){
					$where .= " AND s.startdate < ".strtotime('now')." ";
				}
				if($showClassStatus == 'expired'){
					$where .= " AND s.enddate+86399 < ".strtotime('now')." ";
				}elseif($showClassStatus == 'active'){
					$where .= " AND s.enddate+86399 > ".strtotime('now')." ";
				}
				$where .= " AND s.isactive = 1 AND isclasscompleted !=1";
			}elseif($selectionType == 2){	
				//$where .= "AND se.is_completed = 1 ";
				$where .= " AND s.isactive = 1 AND isclasscompleted = 1";
			}else{
				$where .= " AND s.isactive = 1 AND s.enddate > ".time()." ";
			}
		}
		if($checkEnrol == 1){
			$classSql = "SELECT s.*,se.is_approved,se.userid,IF((se.is_completed=1),se.timemodified,0) AS completed_on FROM mdl_scheduler s LEFT JOIN mdl_scheduler_enrollment as se ON se.scheduler_id = s.id WHERE s.course = ".$courseId." AND se.is_approved = 1".$where;
		}else if($checkEnrol == 2){

			if($USER->archetype == $CFG->userTypeManager){
				$classSql = "SELECT s.*,se.is_approved,se.userid FROM mdl_scheduler s LEFT JOIN mdl_scheduler_enrollment as se ON se.scheduler_id = s.id WHERE s.course = ".$courseId." ".$where;
			}else{
				$classSql = "SELECT s.*,se.is_approved,se.userid FROM mdl_scheduler s LEFT JOIN mdl_scheduler_enrollment AS se ON se.scheduler_id = s.id ".$where." WHERE s.course = ".$courseId." AND se.is_approved != 1 AND se.is_approved != 2";
			}
		}else{
			$classSql = "SELECT s.*,se.is_approved,se.userid FROM mdl_scheduler s LEFT JOIN mdl_scheduler_enrollment as se ON se.scheduler_id = s.id ".$where." WHERE s.course = ".$courseId;
		}
		//echo $classSql;die;
		$schedulerList = $DB->get_records_sql($classSql);
		return $schedulerList;
	}
	/**
	 * To get the html of detail of classes for particular course while we are login by learner and viewing "my learning page"
     *  @param mixed $courseId is course id
	  * @return html $$classHtml return detail of classes for particular course 
	 */
	 
	function getClassesPreviewHtmlForLearner($courseId,$hideElement= 0,$checkEnrol = 0,$selectionType = 0,$isProgram = 0,$showClasses = 'all',$showClassStatus = 'all'){
	  
	  global $CFG, $USER, $DB;
	 // Class Box Start
						
		$classHtml = '';				  
		$classHtml .= '<div class="class-outer-box" >'; // start class-outer-box
		$schedulerList = getUserClassByUser($USER->id,$courseId,$checkEnrol,$selectionType,$isProgram,$showClasses,$showClassStatus);
		
		$curtime = strtotime('now');
		if(count($schedulerList) > 0 ){
		
			$classHtml .= '<div class="classlabel" >'.get_string('classlabel','learnercourse').': </div>'; 
			$classDiv = '';
			$c=0;
			foreach($schedulerList as $schedule){

				if($schedule->startdate >= $curtime && $checkEnrol != 2){
					$userStatusInClass = '<a href = "javascript:void(0);" class="enrol_user decline" userid = "'.$USER->id.'" schedularId = "'.$schedule->id.'" title = "'.get_string("decline_request").'" >'.get_string("decline_request").'</a>';
					$showAssetsAndRefMat = 0;
				}else{
					$userStatusInClass = getClassStatusForLearner($schedule->id, $USER->id);
					$showAssetsAndRefMat = 1;
				}
				$certificateLink = "";
				if(strip_tags($userStatusInClass)=='Completed' and  $CFG->isCertificate==1 and $selectionType==2){
					$isCertificateEnabled = isCertificateEnabled($schedule->id,'class');
					if($isCertificateEnabled==1){
					$certificateLink = certificateLink($schedule->id,'class',$schedule->completed_on);
					}
				}
				
				$classDiv .= '<div class="class-box" style="display:block !important;">'; // start class box
				
				$classDiv .= '<table class="admintable generaltable" >';
				
				$classDiv .= '<thead>
									<tr>
										<th scope="col" style="" class="header c0 leftalign  w150">'.get_string('classname','scheduler').'</th>
										<th scope="col" style="" class="header c1 leftalign w150">'.get_string('instructor','scheduler').'</th>
										<th scope="col" style="" class="header c2 lastcol centeralign w250">'.get_string('class_date_label').'</th>
										<th scope="col" style="" class="header c2 lastcol centeralign w250">'.get_string('coursecredithours').'</th>
										<th scope="col" style="" class="header c3 lastcol centeralign w100">'.get_string('no_of_seats','scheduler').'</th>
										<th scope="col" style="" class="header c4 lastcol centeralign w150">'.get_string('enroloptions','scheduler').'</th>
										
										<th scope="col" style="" class="header c6 lastcol centeralign w100">'.get_string('classstatus','scheduler').'</th>
									</tr>
								 </thead>';
								 
				$classDiv .= '<tbody>';	
				 
				$course_module = getSchedulerModuleId($schedule->id,$courseId);
				$classSessionList = getClassSessionList($schedule->id);
				
				if($hideElement == 1){
					$referenceMaterialsOfClass = array();
				}else{
					$referenceMaterialsOfClass = getReferenceMaterialsOfClass($schedule->id);
				}
				
				$sessionListDiv = "";
				$creditHourCount = 0;
				if(count($classSessionList) > 0 ){ // start classSessionList if
				
						   $sessionListDiv .= '<tr class="r'.$c.'" >
								<td style="" class="leftalign cell c0" colspan="8">';

							$sessionListDiv .= '<div class="session-box" >'; // start session box
							$sessionListDiv .= '<div class="sessionlabel" style="float:left">'.get_string('sessionlabel','scheduler').': </div>';
							
							
							$sessionListDiv .= '<table class="admintable generaltable" >';
			
							$sessionListDiv .= '<thead>
											<tr>
												<th scope="col" style="" class="header c0 leftalign  w600">'.get_string('sessionname','scheduler').'</th>
												<th scope="col" style="" class="header c1 leftalign duration">'.get_string('duration_date').'</th>
											 </tr>
											 </thead>';
												 
							 $sessionListDiv .= '<tbody>';		
								 
							$sessDiv = '';
							
							foreach($classSessionList as $sessionList){  // start classSessionList foreach
								$creditHourCount += $sessionList->duration;
								$sessionDurationText = getClassroomSessionDateDuration($sessionList->starttime, $sessionList->duration); 
								$webAccessLink = $sessionList->web_access_link;
								if(empty($sessionList->web_access_link)){
									$webAccessLink = getMDash();
								}
								$location_info = $sessionList->location;
								if(empty($sessionList->location)){
									$location_info = getMDash();
								}
								$sessDiv .= '<tr class="t'.$c.'">
													<td style="" class="leftalign  w600 cell c0">'.$sessionList->sessionname.'<div><strong >'.get_string("location","scheduler").':</strong> '.$location_info.'<br/><strong >'.get_string("webaccesslink","scheduler").':</strong> '.$webAccessLink.'</div></td>
													<td style="" class="leftalign w150 cell c1">'.$sessionDurationText.'</td>
											 </tr>';

								
							} // end classSessionList foreach
							
							$sessionListDiv .= $sessDiv.'</tbody>';	
							$sessionListDiv .= '</table>';	

							$sessionListDiv .= '</div>'; // end session box
							$sessionListDiv .= '</td> </tr>';
							
				 }  // end classSessionList if

				//$duration_text = getDateFormat($schedule->startdate, $CFG->customDefaultDateFormat).' to '.getDateFormat($schedule->enddate, $CFG->customDefaultDateFormat); 
				$duration_text = getClassDuration($schedule->startdate, $schedule->enddate);
				################################################## Session Will Display Here ####################################################
				$classDiv .= '<tr class="r'.$c.'">
								<td style="" class="leftalign  w150 cell c0">'.$schedule->name.'</td>
								<td style="" class="leftalign w150 cell c1">'.fullname(getClassInstructor($schedule)).'</td>
								<td style="" class="centeralign w250 cell c2 lastcol">'.$duration_text.'</td>
								<td style="" class="centeralign w250 cell c2 lastcol">'.setCreditHoursFormat($creditHourCount).'</td>
								<td style="" class="centeralign w100 cell c3 lastcol">'.$schedule->no_of_seats.'</td>
								<td style="" class="centeralign w150 cell c4 lastcol">'.(isset($schedule->enrolmenttype) && $schedule->enrolmenttype == 1?$CFG->classroomInviteText:$CFG->classroomOpenText).'</td>
								
								<td style="" class="centeralign w100 cell c6 lastcol">'.$userStatusInClass.'</td>
							  </tr>';
				$webAccessLink = $schedule->web_access_link;
				if(empty($schedule->web_access_link)){
					$webAccessLink = getMDash();
				}
				$preRequisite = $schedule->pre_requisite;
				if(empty($schedule->pre_requisite)){
					$preRequisite = getMDash();
				}
				if($certificateLink){
				$classDiv .= '<tr><td colspan="8">'.$certificateLink.'</td></tr>';
				}
				$classDiv .= '<tr class="r'.$c.'">
								<td style="" class="leftalign  w150 cell c0" colspan = "8"><strong>'.get_string('pre_requisite','scheduler').':</strong>&nbsp;'.$preRequisite.'
								</td>
							  </tr>';
			
				$classDiv .= $sessionListDiv;
				 
				################################################## End Session Display Here ########################################################### 
				 
				################################################## Reference Material Will Display Here ###############################################
				
				
				//pr($referenceMaterialsOfClass);die;
				$referenceMaterialsNew = array();
				if(count($referenceMaterialsOfClass) > 0){
				   foreach($referenceMaterialsOfClass as $referenceMaterials){
					 $referenceMaterialsNew[$referenceMaterials->resource_type_id]['name'] = $referenceMaterials->resoursetype_name;
					 $referenceMaterialsNew[$referenceMaterials->resource_type_id]['details'][] = $referenceMaterials;
				   }
				}
				//pr($referenceMaterialsNew);die;	
				if(count($referenceMaterialsNew) > 0 && $showAssetsAndRefMat == 1){
		
				   $classDiv .= '<tr class="r'.$c.'" >
								   <td style="background-color: white ! important;" class="leftalign cell c0" colspan="8" >';
								
				   $classDiv .= '<div class="refmat-box" >';
				   $classDiv .= '<div class="reference-material-label">'.get_string('referencemateriallabel','course').': </div>';
				   $classDiv .= '<div class="" ><table cellspacing="0" cellpadding="0" border="0" width="100%">';
				   $i = 0;
				   foreach($referenceMaterialsNew as $referenceMaterialsArr){
				   
					  $i++;
					  $resourceType = $referenceMaterialsArr['name'];
					  $resourceTypeDetails = $referenceMaterialsArr['details'];
					  
					  if(count($resourceTypeDetails) > 0 ){
					  
						//$classDiv .= '<div class="" >'; 
						$classDiv .= '<tr><td><!--('.$i.')--> '.$resourceType.' </td><td>'; 
						$rCDetailsDiv = '';
						foreach($resourceTypeDetails as $rtdArr){
						
						     $resourseName = $rtdArr->name;
							 $allowStudentAccess = $rtdArr->allow_student_access == 1?"Yes":'No';
							 $assetIconClass = '';
							 $courseModuleId = $DB->get_field_sql("select id from {$CFG->prefix}course_modules where instance = '".$rtdArr->resource_id."' AND module = '".$CFG->resourceModuleId."' AND course = '".$courseId."'  ");
							 $cm = get_coursemodule_from_id('resource', $courseModuleId);
							 
							 $referenceMaterialsIconClass = '';			
							 if($cm){	
												   
								 $resource = $DB->get_record('resource', array('id'=>$cm->instance));
								
								 if(count($resource) > 0 ){
								
									$context = context_module::instance($cm->id);
									$fs = get_file_storage();
									$files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
						
									if (count($files) < 1) {
										//resource_print_filenotfound($resource, $cm, $course);
										//die;
									} else {
										$file = reset($files);
										unset($files);
									}
									$path = '/'.$context->id.'/mod_resource/content/'.$resource->revision.$file->get_filepath().$file->get_filename();
									$fullurl = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
									$resourseTypes = end(explode('.',$path));
									$referenceMaterialsIconClass = getFileClass($resourseTypes);
									$linkUrl = "<div class='classroom-refmat-details' ><a href='javascript:;' onclick=\"window.open('".$fullurl."', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \" title='".$resourseName."'   ><span class = '".$referenceMaterialsIconClass."' style='height:30px'></span>".$resourseName."</a></div>";
								}
							}
							
							
							$rCDetailsDiv .= $linkUrl;
						}
						$classDiv .= $rCDetailsDiv.'</td></tr>'; 
					  }
				   }
				   $classDiv .= '</table></div>'; 
				   $classDiv .= '</div>';
				   $classDiv .= '</div>';
				   $classDiv .= '</td> </tr>';
				}elseif($showAssetsAndRefMat == 0 && count($referenceMaterialsNew) > 0){
					$classDiv .= '<tr class="r'.$c.'" >
							   <td style="background-color: white ! important;" class="leftalign cell c0" colspan="8" >';
							
					$classDiv .= '<div class="refmat-box" >';
					$classDiv .= '<div class="reference-material-label">'.get_string('referencemateriallabel','course').': </div>';
					$classDiv .= '<div class="" ><table cellspacing="0" cellpadding="0" border="0" width="100%">';
					$i = 0;
					$classDiv .= '<tr><td colspan = "2">'.get_string('cannotAccessReferenceMaterial').'</td>';
					$classDiv .= '</table></div>'; 
					$classDiv .= '</div>';
					$classDiv .= '</div>';
					$classDiv .= '</td> </tr>';
				}
						 
				 
				 $c++;


				$classDiv .= '</tbody>';	
				$classDiv .= '</table>';	
				
				$classDiv .= '</div>'; // end class box
			
			} // end classArr foreach
			
			
			
		}else{
		
		    $classHtml .= '<div class="classlabel" >'.get_string('classlabel','learnercourse').': </div>'; 
			$classDiv .= '<div class="class-box" style="display:block !important;">'; // start class box
			$classDiv .= '<table class="admintable generaltable" >';
				$classDiv .= '<thead>
									<tr>
										<th scope="col" style="" class="header c0 leftalign  w150">'.get_string('classname','scheduler').'</th>
										<th scope="col" style="" class="header c1 leftalign w150">'.get_string('instructor','scheduler').'</th>
										<th scope="col" style="" class="header c2 lastcol centeralign w250">'.get_string('class_date_label').'</th>
										<th scope="col" style="" class="header c3 lastcol centeralign w100">'.get_string('no_of_seats','scheduler').'</th>
										<th scope="col" style="" class="header c4 lastcol centeralign w150">'.get_string('enroloptions','scheduler').'</th>
										<th scope="col" style="" class="header c5 lastcol centeralign w100">'.get_string('location','scheduler').'</th>
										<th scope="col" style="" class="header c6 lastcol centeralign w100">'.get_string('classstatus','scheduler').'</th>
									</tr>
								 </thead>';
								 
					$classDiv .= '<tbody>';	
						$classDiv .= '<tr>';	
							$classDiv .= '<td colspan = "7">';	
							$classDiv .= get_string('norecordfound','course');	
							$classDiv .= '</td>';	
						$classDiv .= '</tr>';	
					$classDiv .= '</tbody>';	
				$classDiv .= '</table>';	
			$classDiv .= '</div>';
		} // end classArr if

         $classHtml .= $classDiv.'</div>';  // end class-outer-box
						
		// Class Box End 

						
	    return $classHtml;
	}
	
	/**
	 * To get the html of detail of classes for particular course while we are login by any user and viewing "classroom preview" of course
     *  @param mixed $courseId is course id
	 *  @param mixed $classId is class id , it is optional
	  * @return html $$classHtml return detail of classes for particular course 
	 */
	 
	 
	function getClassesPreviewHtmlForUser($courseId, $classId=''){
	  
	  global $CFG, $USER, $DB;
	
	 // Class Box Start
						
		$classHtml = '';				  
		
		
		$schedulerList = classroomSchedulerList($courseId);
		$schedulerListArr = $schedulerList;
	    if($classId){
		  $schedulerListArr = array();
		  $schedulerListArr[$classId] = $schedulerList[$classId];
		}
		//pr($schedulerListArr);die;
		if(count($schedulerListArr) > 0 ){
			$allSessionArr = classSessionListOfCourse($courseId);
			$classHtml .= '<div class="class-outer-box" >'; // start class-outer-box 
			$classHtml .= '<div class="classlabel" >'.get_string('classlabel','learnercourse').': </div>'; 
			$classDiv = '';
			$c=0;
			foreach($schedulerListArr as $schedule){

                
				$classDiv .= '<div class="class-box" style="display:block !important;">'; // start class box
				$classDiv .= '<table class="admintable generaltable" >';
				
				$classDiv .= '<thead>
									<tr>
										<th scope="col" style="" class="header c0 leftalign  w250">'.get_string('classname','scheduler').'</th>
										<th scope="col" style="" class="header c1 leftalign w150">'.get_string('instructor','scheduler').'</th>
										<th scope="col" style="" class="header c2 lastcol centeralign w250">'.get_string('class_date_label').'</th>
										<th scope="col" style="" class="header c2 lastcol centeralign w250">'.get_string('coursecredithours').'</th>
										<th scope="col" style="" class="header c3 lastcol centeralign w100">'.get_string('no_of_seats','scheduler').'</th>
										<th scope="col" style="" class="header c4 lastcol centeralign w150">'.get_string('enroloptions','scheduler').'</th>
										
									</tr>
								 </thead>';
								 
				$classDiv .= '<tbody>';	
				 
				$course_module = getSchedulerModuleId($schedule->id,$courseId);				
				$classSessionList = $allSessionArr[$schedule->id];
				$referenceMaterialsOfClass = getReferenceMaterialsOfClass($schedule->id);
				
				//$duration_text = getDateFormat($schedule->startdate, $CFG->customDefaultDateFormat).' to '.getDateFormat($schedule->enddate, $CFG->customDefaultDateFormat); 
				$duration_text = getClassDuration($schedule->startdate, $schedule->enddate);
				################################################## Session Will Display Here ####################################################
				$sessionListDiv = "";
				$creditHourCount = 0;
				//pr($classSessionList);
				if(count($classSessionList) > 0 ){ // start classSessionList if
				
						   $sessionListDiv .= '<tr class="r'.$c.'" >
								<td class="leftalign cell c0" colspan="7">';

							$sessionListDiv .= '<div class="session-box" >'; // start session box
							$sessionListDiv .= '<div class="sessionlabel" style="float:left">'.get_string('sessionlabel','scheduler').': </div>';
							
							
							$sessionListDiv .= '<table class="admintable generaltable" >';
			
							$sessionListDiv .= '<thead>
											<tr>
												<th scope="col" style="" class="header c0 leftalign  w250">'.get_string('sessionname','scheduler').'</th>
												<th scope="col" style="" class="header c1 leftalign duration">'.get_string('duration_date').'</th>
											 </tr>
											 </thead>';
												 
							 $sessionListDiv .= '<tbody>';		
								 
							$sessDiv = '';
							foreach($classSessionList as $sessionList){  // start classSessionList foreach
								$creditHourCount += $sessionList->duration;
								$sessionDurationText = getClassroomSessionDateDuration($sessionList->starttime, $sessionList->duration); 
								$webAccessLink = $sessionList->web_access_link;
								if(empty($sessionList->web_access_link)){
									$webAccessLink = getMDash();
								}
								$location_info = $sessionList->location;
								if(empty($sessionList->location)){
									$location_info = getMDash();
								}
								$sessDiv .= '<tr class="t'.$c.'">
										<td style="" class="leftalign  w300 cell c0">'.$sessionList->sessionname.'<div><strong >'.get_string("location","scheduler").':</strong> '.$location_info.'<br><strong >'.get_string("webaccesslink","scheduler").':</strong> '.$webAccessLink.'</div></td>
												
													<td style="" class="leftalign w150 cell c1">'.$sessionDurationText.'</td>
											 </tr>';

								
							} // end classSessionList foreach
							
							$sessionListDiv .= $sessDiv.'</tbody>';	
							$sessionListDiv .= '</table>';	

							$sessionListDiv .= '</div>'; // end session box
							$sessionListDiv .= '</td> </tr>';
							
				 }  // end classSessionList if

				$classDiv .= '<tr class="r'.$c.'">
								<td style="" class="leftalign  w250 cell c0">'.$schedule->name.'</td>
								<td style="" class="leftalign w150 cell c1">'.fullname(getClassInstructor($schedule)).'</td>
								<td style="" class="centeralign w250 cell c2 lastcol">'.$duration_text.'</td>
								<td style="" class="centeralign w250 cell c2 lastcol">'.setCreditHoursFormat($creditHourCount).'</td>
								<td style="" class="centeralign w100 cell c3 lastcol">'.($schedule->no_of_seats?$schedule->no_of_seats:getMDash()).'</td>
								<td style="" class="centeralign w150 cell c4 lastcol">'.(isset($schedule->enrolmenttype) && $schedule->enrolmenttype == 1?$CFG->classroomInviteText:$CFG->classroomOpenText).'</td>
								
							  </tr>';
				/* $webAccessLink = $schedule->web_access_link;
				if(empty($schedule->web_access_link)){
					$webAccessLink = getMDash();
				} */
				$preRequisite = $schedule->pre_requisite;
				if(empty($schedule->pre_requisite)){
					$preRequisite = getMDash();
				}
				$classDiv .= '<tr class="r'.$c.'">
								<td style="" class="leftalign  w150 cell c0" colspan = "7"><strong>'.get_string('pre_requisite','scheduler').':</strong>&nbsp;'.$preRequisite.'
								</td>
							  </tr>';
				$classDiv .= $sessionListDiv;
				 
				 
				################################################## End Session Display Here ########################################################### 
				 
				################################################## Reference Material Will Display Here ###############################################
				
				
				//pr($referenceMaterialsOfClass);die;
				$referenceMaterialsNew = array();
				if(count($referenceMaterialsOfClass) > 0 ){
				   foreach($referenceMaterialsOfClass as $referenceMaterials){
					 $referenceMaterialsNew[$referenceMaterials->resource_type_id]['name'] = $referenceMaterials->resoursetype_name;
					 $referenceMaterialsNew[$referenceMaterials->resource_type_id]['details'][] = $referenceMaterials;
				   }
				}
				//pr($referenceMaterialsNew);die;	
				if(count($referenceMaterialsNew) > 0){
		
				   $classDiv .= '<tr class="r'.$c.'" >
								   <td style="background-color: white ! important; border-top:none" class="leftalign cell c0" colspan="7" >';
								
				   $classDiv .= '<div class="refmat-box" >';
				   $classDiv .= '<div class="reference-material-label">'.get_string('referencemateriallabel','course').': </div>';
				   $classDiv .= '<div class="" >';
				   $i = 0;
				   foreach($referenceMaterialsNew as $referenceMaterialsArr){
				   
					  $i++;
					  
					  $resourceType = $referenceMaterialsArr['name'];
					  $resourceTypeDetails = $referenceMaterialsArr['details'];
					  
					  if(count($resourceTypeDetails) > 0 ){
					    
						$classDiv .= '<div class="" ><table cellspacing="0" cellpadding="0" border="0" width="100%"><tr>'; 
						$classDiv .= '<td><!--('.$i.') -->'.$resourceType.' </td><td>'; 
						$rCDetailsDiv = '';
						foreach($resourceTypeDetails as $rtdArr){
						
						     $resourseName = $rtdArr->name;
							 $allowStudentAccess = $rtdArr->allow_student_access == 1?"Yes":'No';
							 $assetIconClass = '';
							 $courseModuleId = $DB->get_field_sql("select id from {$CFG->prefix}course_modules where instance = '".$rtdArr->resource_id."' AND module = '".$CFG->resourceModuleId."' AND course = '".$courseId."'  ");
							 $cm = get_coursemodule_from_id('resource', $courseModuleId);
							 
							 $referenceMaterialsIconClass = '';			
							 if($cm){	
												   
								 $resource = $DB->get_record('resource', array('id'=>$cm->instance));
								
								 if(count($resource) > 0 ){
								
									$context = context_module::instance($cm->id);
									$fs = get_file_storage();
									$files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
						
									if (count($files) < 1) {
										$linkUrl = "<div class='classroom-refmat-details'><a href='javascript:;' onclick='alert(\"".get_string('clamdeletedfile')."\")' \" title='".$resourseName."'   ><span class = '".$referenceMaterialsIconClass."' style='height:30px'></span>".$resourseName."</a></div>";
									} else {
										$file = reset($files);
										unset($files);
										$path = '/'.$context->id.'/mod_resource/content/'.$resource->revision.$file->get_filepath().$file->get_filename();
										$fullurl = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
										$resourseTypes = end(explode('.',$path));
										$referenceMaterialsIconClass = getFileClass($resourseTypes);
										$linkUrl = "<div class='classroom-refmat-details'><a href='javascript:;' onclick=\"window.open('".$fullurl."', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \" title='".$resourseName."'   ><span class = '".$referenceMaterialsIconClass."' style='height:30px'></span>".$resourseName."</a></div>";
									}
									
								}
							}
							
							
							$rCDetailsDiv .= $linkUrl;
						}
						$classDiv .= $rCDetailsDiv.'</td></tr></table></div>'; 
					  }
				   }
				   $classDiv .= '</div>';
				   $classDiv .= '</div>';
				   $classDiv .= '</td> </tr>';
				}	
				
							   
				
				 
				 ################################################## End Reference Material Display Here ###############################################
				 
				 
				 $c++;


				$classDiv .= '</tbody>';	
				$classDiv .= '</table>';	
				
				$classDiv .= '</div>'; // end class box
			
			} // end classArr foreach
			
			
			$classHtml .= $classDiv.'</div>';  // end class-outer-box
		} // end classArr if

        
						
		// Class Box End 

						
	    return $classHtml;
	}
	
		/**
	 * To check whether course has been published or not
	 *  @param mixed $courseId is course id
	 * @return bool $havePublished if have then return true else false
	 */
	 
	 
	function isCoursePublished($courseId){
	 
	   global $USER,$CFG,$DB;
	   $havePublished = false;
       
	   if($courseId){	
		   $query = "SELECT id, publish FROM {$CFG->prefix}course WHERE id = ".$courseId;
		   $records = $DB->get_record_sql($query);
		   $havePublished = $records->publish;
	   }   
	   return $havePublished;
	
	}

	
	//function to close colorbox popup
	function closeColorboxpopup(){
		echo '<script>parent.$.fn.colorbox.close();</script>';
	}
	
	//send declined email to user
	function declinedEmailRequestFromClass($userid,$classid){
		global $DB,$USER,$CFG;
		$scheduler = $DB->get_record('scheduler',array('id'=>$classid));
	
		$toUserQuery = "SELECT id, firstname, lastname, email, createdby FROM {$CFG->prefix}user WHERE id = '".$userid."'";
	
		$toUserDetails = $DB->get_record_sql($toUserQuery);
		$fromUser->email = $USER->email;
		$fromUser->firstname = $USER->firstname;
		$fromUser->lastname = $USER->lastname;
		$fromUser->maildisplay = true;
		$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$fromUser->id = $USER->id;
		$fromUser->firstnamephonetic = '';
		$fromUser->lastnamephonetic = '';
		$fromUser->middlename = '';
		$fromUser->alternatename = '';
		$subject = 'Request Declined from ".$scheduler->name." Class by '. $USER->firstname.' '.$USER->lastname ;
	
		$messageText = 'Dear '.$toUserDetails->firstname.','."\n\n".'Your request has been declined';
		email_to_user($toUser, $fromUser, $subject, $messageText, $messageHtml, "", "", true);
	}

	//send declined email to user
	function AcceptanceEmailRequestFromClass($userid,$classid){
		global $DB,$USER,$CFG;
		$scheduler = $DB->get_record('scheduler',array('id'=>$classid));
	
		$toUserQuery = "SELECT id, firstname, lastname, email, createdby FROM {$CFG->prefix}user WHERE id = '".$userid."'";
	
		$toUserDetails = $DB->get_record_sql($toUserQuery);
		$fromUser->email = $USER->email;
		$fromUser->firstname = $USER->firstname;
		$fromUser->lastname = $USER->lastname;
		$fromUser->maildisplay = true;
		$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$fromUser->id = $USER->id;
		$fromUser->firstnamephonetic = '';
		$fromUser->lastnamephonetic = '';
		$fromUser->middlename = '';
		$fromUser->alternatename = '';
		$subject = 'Request Accepted from ".$scheduler->name." Class by '. $USER->firstname.' '.$USER->lastname ;
	
		$messageText = 'Dear '.$toUserDetails->firstname.','."\n\n".'Your request has been Accepted';
		email_to_user($toUser, $fromUser, $subject, $messageText, $messageHtml, "", "", true);
	}

	//function to send invitation to manager of user
	
	function emailForSendInvitation($userid,$scheduleRec,$all_users,$enrolment_id,$skipmanagerapproval=0){
		global $DB,$USER,$CFG;
	
		if($all_users[$userid]->usermanagerid==3 || $all_users[$userid]->usermanagerid==0)
		{
			$managerid = $userid;
		}
		else{
			$managerid = $all_users[$userid]->usermanagerid;
		}
	
	
		$toUserQuery = "SELECT id, firstname, lastname, email, createdby FROM {$CFG->prefix}user WHERE id = '".$managerid."'";
		$toUser = $DB->get_record_sql($toUserQuery);
		$fromUser->email = $USER->email;
		$fromUser->firstname = $USER->firstname;
		$fromUser->lastname = $USER->lastname;
		$fromUser->maildisplay = true;
		$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$fromUser->id = $USER->id;
		$fromUser->firstnamephonetic = '';
		$fromUser->lastnamephonetic = '';
		$fromUser->middlename = '';
		$fromUser->alternatename = '';
		$classDetails = $DB->get_record_sql("SELECT s.id,s.course,s.name,s.startdate,s.enddate,se.is_approved,se.declined_remarks,s.location FROM mdl_scheduler AS s LEFT JOIN mdl_scheduler_enrollment as se ON se.scheduler_id = s.id WHERE s.id = ".$scheduleRec->id." AND se.userid = ".$userid);
		$events = $DB->get_records_sql("SELECT ss.id,e.id as eid FROM mdl_scheduler_slots ss LEFT JOIN mdl_event as e ON ss.id = e.sessionid WHERE ss.isactive = 1 AND ss.schedulerid = ".$scheduleRec->id." AND e.userid = ".$userid);
		$emailFile = '';
		$emailFileName = '';
		$sessionsListing = sessionListForEmails($scheduleRec->id);
		$sessionsListingManager = "\n\r"."\n\r";
		if($sessionsListing != ''){
			$sessionsListingManager = $sessionsListing;
		}
		if(!empty($events)){
			$update = 0;
			$eventArr = array();
			foreach($events as $event){
				$eventArr[] = $event->eid;
			}
			$emailFile = generateIcsFile($eventArr,0);
			$emailFileName = basename($emailFile);
		}
		$courseDetails = $DB->get_record_sql("SELECT c.id,c.fullname,c.summary FROM mdl_course as c WHERE c.id =  ".$classDetails->course);
		$site = get_site();
		$emailContent = array(
								'name'			=>$toUser->firstname.' '.$toUser->lastname,
								'user_name'		=>$all_users[$userid]->fullname,
								'coursename'	=>$courseDetails->fullname.': '.$classDetails->name,
								'datetime'		=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
								'starttime'		=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
								'location'		=>$classDetails->location,
								'lmsLink'		=> $CFG->emailLmsLink,
								'lmsName'		=> $CFG->emailLmsName,
								'sitename'		=> html_entity_decode(format_string($site->fullname)),
								'sessionsListing'=> $sessionsListingManager,
								'link'			=> $CFG->wwwroot
						);
		$emailContent = (object)$emailContent;
		$messageText = get_string('user_enrol_invite','email',$emailContent);
		$messageText .= get_string('email_footer','email');
		$messageText .= get_string('from_email','email');
		$subject = get_string('request_subject_class_manager','email',$emailContent);
		$messageHtml = text_to_html($messageText, false, false, true);
		
		$toUser->email = $toUser->email;
		$toUser->firstname = $toUser->firstname;
		$toUser->lastname = $toUser->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 1; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $toUser->id;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		if($skipmanagerapproval==0 && $managerid != $userid && $classDetails->is_approved != 1)
		{
			email_to_user($toUser, $fromUser, $subject, $messageText, $messageHtml, "", "", true);
		}
		$locationText = '';
		if(trim($classDetails->location) != ''){
			$locationText = "<li>".get_string('location','scheduler').': '.$classDetails->location."</li>";
		}
		$summary = '';
		if($CFG->showCourseDescriptionInMail == 1){
			$summary = getCourseSummary($courseDetails);
		}
		$emailContentForLearner = array(
								'name'			=>$all_users[$userid]->fullname,
								'coursename'	=>$courseDetails->fullname.': '.$classDetails->name,
								'datetime'		=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
								'starttime'		=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
								'classduration'	=> getClassDuration($classDetails->startdate, $classDetails->enddate),
								'lmsLink'		=> $CFG->emailLmsLink,
								'lmsName'		=> $CFG->emailLmsName,
								'summary'		=> $summary,
								'sessionsListing'=> $sessionsListing,
								'location'		=>$locationText);
		$emailContentForLearner = (object)$emailContentForLearner;
		if($skipmanagerapproval==0  && $managerid != $userid && $classDetails->is_approved != 1)
		{
			$messageTextForLearner = get_string('user_enrol_invite_for_learner','email',$emailContentForLearner);
		}else{
			$messageTextForLearner = get_string('skip_user_enrol_invite_for_learner','email',$emailContentForLearner);
		}
		if($skipmanagerapproval == 0){
			$emailFile = '';
			$emailFileName = '';
		}
		if($skipmanagerapproval==0){
			$subjectForLearner = get_string('request_subject_invite_only','email',$emailContentForLearner);
		}elseif($emailFile != ''){
			$subjectForLearner = get_string('request_subject_invite','email',$emailContentForLearner);
			$messageTextForLearner .= get_string('skip_user_enrol_invite_for_learner_outlook_info','email',$emailContentForLearner);
		}else{
			$subjectForLearner = get_string('request_subject_invite','email',$emailContentForLearner);
			$messageTextForLearner .=  get_string('skip_user_enrol_invite_for_learner_info','email',$emailContentForLearner);
		}
		$messageTextForLearner .= get_string('email_footer','email');
		$messageTextForLearner .= get_string('from_email','email');
		$messageHtmlForLearner = text_to_html($messageTextForLearner, false, false, true);
		$userFname = explode(' ',$all_users[$userid]->fullname);
		$toLearnerUser->email = $all_users[$userid]->email;
		$toLearnerUser->firstname = $userFname[0];
		$toLearnerUser->lastname = $userFname[1];
		$toLearnerUser->maildisplay = true;
		$toLearnerUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toLearnerUser->id = $all_users[$userid]->id;
		$toLearnerUser->firstnamephonetic = '';
		$toLearnerUser->lastnamephonetic = '';
		$toLearnerUser->middlename = '';
		$toLearnerUser->alternatename = '';
		if($CFG->blockApprovalMailtoLearner == 0){
			email_to_user($toLearnerUser, $fromUser, $subjectForLearner, $messageTextForLearner, $messageHtmlForLearner, $emailFile, $emailFileName, true);
		}
	}
	function sendUnassignInvitation($classid,$userid){
		global $DB,$USER,$CFG;

		$toUserQuery = "SELECT id, firstname, lastname, email, createdby,parent_id FROM {$CFG->prefix}user WHERE id = '".$userid."'";
		$toUser = $DB->get_record_sql($toUserQuery);

		$toUserQuery = "SELECT id, firstname, lastname, email, createdby,parent_id FROM {$CFG->prefix}user WHERE id = '".$toUser->parent_id."'";
		$toUserManager = $DB->get_record_sql($toUserQuery);

		$classDetails = $DB->get_record_sql("SELECT s.id,s.course,s.name,s.startdate,s.enddate,se.declined_remarks,location FROM mdl_scheduler AS s LEFT JOIN mdl_scheduler_enrollment as se ON se.scheduler_id = s.id WHERE s.id = ".$classid." AND se.userid = ".$userid);
		$courseDetails = $DB->get_record_sql("SELECT c.id,c.fullname FROM mdl_course as c WHERE c.id =  ".$classDetails->course);
		$locationText = '';
		if(trim($classDetails->location) != ''){
			$locationText = "<li>".get_string('location','scheduler').': '.$classDetails->location."</li>";
		}
		$emailContentForLearner = array(
								'name'			=>$toUser->firstname.' '.$toUser->lastname,
								'manager'		=>$toUserManager->firstname.' '.$toUserManager->lastname,
								'coursename'	=>$courseDetails->fullname.': '.$classDetails->name,
								'reason'		=>$classDetails->declined_remarks,
								'datetime'		=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
								'starttime'		=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
								'location'		=>$locationText);
		$emailContentForLearner = (object)$emailContentForLearner;
		$messageTextForLearner = get_string('user_enrol_denied_compliance','email',$emailContentForLearner);
		$messageTextForLearner .= get_string('email_footer_classroom','email');
		$messageTextForLearner .= get_string('from_email','email');
		$subjectForLearner = get_string('course_unenrol_subject','email',$emailContentForLearner);
		$messageHtmlForLearner = text_to_html($messageTextForLearner, false, false, true);
		$messageHtmlForLearner = '';
		$toUser->email = $toUser->email;
		$toUser->firstname = $toUser->firstname;
		$toUser->lastname = $toUser->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $userid;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		email_to_user($toUser, $fromUser, $subjectForLearner, $messageTextForLearner, $messageHtmlForLearner, "", "", true);

		$userRole = $DB->get_record_sql("SELECT r.name FROM mdl_role r LEFT JOIN mdl_role_assignments ra ON ra.roleid = r.id WHERE ra.userid = ".$toUserManager->id);

		if($userRole->name != $CFG->userTypeAdmin){
			$emailContentForManager = array(
									'name'			=>$toUserManager->firstname.' '.$toUserManager->lastname,
									'user_name'		=>$toUser->firstname.' '.$toUser->lastname,
									'coursename'	=>$courseDetails->fullname.': '.$classDetails->name,
									'reason'		=>$classDetails->declined_remarks,
									'datetime'		=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
									'starttime'		=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
									'location'		=>$locationText);
			$emailContentForManager = (object)$emailContentForManager;
			$messageTextForManager = get_string('manager_user_enrol_denied_compliance','email',$emailContentForManager);
			$messageTextForManager .= get_string('email_footer','email');
			$messageTextForManager .= get_string('from_email','email');
			$subjectForManager = get_string('manager_course_unenrol_subject','email',$emailContentForManager);
			$toManagerUser->email = $toUserManager->email;
			$toManagerUser->firstname = $toUserManager->firstname;
			$toManagerUser->lastname = $toUserManager->lastname;
			$toManagerUser->maildisplay = true;
			$toManagerUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
			$toManagerUser->id = $toUserManager->id;
			$toManagerUser->firstnamephonetic = '';
			$toManagerUser->lastnamephonetic = '';
			$toManagerUser->middlename = '';
			$toManagerUser->alternatename = '';
			email_to_user($toManagerUser, $fromUser, $subjectForManager, $messageTextForManager,"", "", "", true);
		}
	}
	
	
	
	  
	  function getClassroomReportDetails($cid, $classId ='', $extraSearch = ''){
			
		  global $DB, $CFG, $USER;
		  
		 
		  
		  if($cid){

            if($classId == ''){
		      $classId = $DB->get_field_sql("select id FROM mdl_scheduler where course = '".$cid."' order by name asc limit 1"); 
		    }
		  
			if($USER->archetype != $CFG->userTypeAdmin){
			  $extraSearch .= " AND (gce.primary_instructor = ".$USER->id." || gce.class_creator = ".$USER->id." || gce.class_instructor = ".$USER->id.")";
			}
			
			if($cid){
			  $extraSearch .= " AND gce.id = '".$cid."' ";
			}
			
			if($classId){
			  $extraSearch .= " AND gce.scheduler_id = '".$classId."' ";
			}
			
			$fullnameField = extractMDashInSql('gce.fullname');	 
			$query = "SELECT @a := @a +1 AS serial, gce.* FROM vw_get_class_enrollments gce ";
			$query .= " , (SELECT @a:=0) AS x";
			$query .= " WHERE gce.id NOT IN (1) AND gce.coursetype_id = ".$CFG->courseTypeClassroom." ".$extraSearch;
			$query .= " GROUP BY gce.scheduler_id, gce.session_id, gce.userid ";
			$query .= " ORDER BY gce.classname, gce.user_fullname";
		  
		    $records = $DB->get_records_sql($query);

		    $dateFormat = $CFG->customDefaultDateFormat;

			$enrolledUsersInClass = array();	
			if(count($records) > 0 ){
 
				  $i=0;
				  foreach($records as $enrollDetailsArr){
       
	                   $i++;
				       $totalInvitedUser++;
					   $courseName = $enrollDetailsArr->fullname;
					   $className = $enrollDetailsArr->classname;
					   $sessionName = $enrollDetailsArr->sessionname;
					   $primaryInstructor = $enrollDetailsArr->primary_instructor;
					   $courseDeleted = $enrollDetailsArr->course_deleted;
					   $coursePublish = $enrollDetailsArr->course_publish;
					   $userFullname = $enrollDetailsArr->user_fullname;
					   $userName = $enrollDetailsArr->username;
					   
					   $courseTypeId = $enrollDetailsArr->coursetype_id;
					   $courseId = $enrollDetailsArr->id;
					   $enrollClassId = $enrollDetailsArr->scheduler_id;
					   
					   
					   $sessionId = $enrollDetailsArr->session_id;
					   $enrollUserId = $enrollDetailsArr->userid;
					   
					   $isUserApproved = $enrollDetailsArr->is_approved?$enrollDetailsArr->is_approved:0;
					   $classStartDate = $enrollDetailsArr->class_startdate;
					   $classEndDate = $enrollDetailsArr->class_enddate;
					   $classDuration = getClassDuration($classStartDate, $classEndDate);
					   $isClassCompleted = $enrollDetailsArr->is_completed?$enrollDetailsArr->is_completed:0;
					   $isClassSubmitted = $enrollDetailsArr->is_class_submitted?$enrollDetailsArr->is_class_submitted:0;
					   $classInstructor = $enrollDetailsArr->class_instructor;
					   $classInstructorFullName = $enrollDetailsArr->class_instructor_fullname;
					   $classInstructorUserName = $enrollDetailsArr->class_instructor_username;
					   $classStatus = $enrollDetailsArr->class_status;
					   $enrolmentType = $enrollDetailsArr->enrolmenttype;
					   $classCreator = $enrollDetailsArr->class_creator;
					   $submittedBy = $enrollDetailsArr->class_submitted_by;
					   $classSubmittedByName = $enrollDetailsArr->class_submittedby_name;
					   $classSubmittedByUsername = $enrollDetailsArr->class_submittedby_username;
					   $classSubmittedOn = $enrollDetailsArr->class_submitted_on;
					   $isClassActive = $enrollDetailsArr->is_class_active;
					   $noOfSeats = $enrollDetailsArr->no_of_seats;
					   $sessionAttended = $enrollDetailsArr->attended?$enrollDetailsArr->attended:0;
					   $sessionGrade = $enrollDetailsArr->grade;
					   $sessionScore = $enrollDetailsArr->score;
					   $sessionStartTime = $enrollDetailsArr->session_starttime;
					   $sessionEndTime = $enrollDetailsArr->session_endtime;
					   $sessionDuration = getClassDuration($sessionStartTime, $sessionEndTime);
					   
			           
					   $enrolledUsersInClass['classroom'][$courseId]['courseId'] = $courseId;
					   $enrolledUsersInClass['classroom'][$courseId]['courseName'] = $courseName;
					   $enrolledUsersInClass['classroom'][$courseId]['courseDeleted'] = $courseDeleted;
					   $enrolledUsersInClass['classroom'][$courseId]['coursePublish'] = $coursePublish;
					   $enrolledUsersInClass['classroom'][$courseId]['courseTypeId'] = $courseTypeId;
					   $enrolledUsersInClass['classroom'][$courseId]['primaryInstructor'] = $primaryInstructor;

					   
					   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['classId'] = $enrollClassId;
					   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['className'] = $className;
					   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['classStartDate'] = $classStartDate;
					   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['classEndDate'] = $classEndDate;
					   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['classDuration'] = $classDuration;
					   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['isClassSubmitted'] = $isClassSubmitted;

					   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['classInstructor'] = $classInstructor;
					   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['classInstructorFullName'] = $classInstructorFullName;
					   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['classInstructorUserName'] = $classInstructorUserName;
					   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['submittedBy'] = $submittedBy;
					   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['classSubmittedByName'] = $classSubmittedByName;
					   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['classSubmittedByUsername'] = $classSubmittedByUsername;
					   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['classSubmittedOn'] = $classSubmittedOn;
					   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['noOfSeats'] = $noOfSeats;
					   
				
					   if($enrollUserId){
						   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['users'][$enrollUserId]['userFullname'] = $userFullname;
						   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['users'][$enrollUserId]['userName'] = $userName;
						   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['users'][$enrollUserId]['isUserApproved'] = $isUserApproved;
						   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['users'][$enrollUserId]['isClassCompleted'] = $isClassCompleted;
						   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['users'][$enrollUserId]['sessions'][$sessionId]['sessionId'] = $sessionId;
						   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['users'][$enrollUserId]['sessions'][$sessionId]['sessionName'] = $sessionName;
						   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['users'][$enrollUserId]['sessions'][$sessionId]['sessionAttended'] = $sessionAttended;
						   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['users'][$enrollUserId]['sessions'][$sessionId]['sessionGrade'] = $sessionGrade;
						   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['users'][$enrollUserId]['sessions'][$sessionId]['sessionScore'] = $sessionScore;
						   $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['users'][$enrollUserId]['sessions'][$sessionId]['sessionDuration'] = $sessionDuration;
						   
					   }else{
					       $enrolledUsersInClass['classroom'][$courseId]['class'][$enrollClassId]['users'] = array();
					   }
					   
					
					   
				  }


				  if(isset($enrolledUsersInClass['classroom'][$cid]['class'][$classId]['users']) && count($enrolledUsersInClass['classroom'][$cid]['class'][$classId]['users']) > 0 ){
				    $totalInvitedUser = count($enrolledUsersInClass['classroom'][$cid]['class'][$classId]['users']);
					$enrolledUsersInClass['classroom'][$cid]['class'][$classId]['invited'] = $totalInvitedUser; 
					
					
				    foreach($enrolledUsersInClass['classroom'][$cid]['class'][$classId]['users'] as $uid => $userArr){
					
					   $totalCLUser = 0;
					   $totalApUser = 0;
					   $totalDecUser = 0;
					   $totalWUser = 0;

						if($userArr['isUserApproved'] == $CFG->classApproved){
						
						  $enrolledUsersInClass['classroom'][$courseId]['class'][$classId]['approved']['users'][$uid] = $userArr;
						  $enrolledUsersInClass['classroom'][$cid]['class'][$classId]['approved']['total'] += 1;
						  
						  $enrolledUsersInClass['classroom'][$courseId]['class'][$classId]['approved']['users'][$uid]['class_status'] = get_string('classnoshow','classroomreport');
						  if($isClassSubmitted == 0){
						     $enrolledUsersInClass['classroom'][$cid]['class'][$classId]['approved']['noshow'] += 1;
							 $enrolledUsersInClass['classroom'][$cid]['class'][$classId]['approved']['incomplete'] = 0;
							 $enrolledUsersInClass['classroom'][$cid]['class'][$classId]['approved']['completed'] = 0;
							 
							// if(!isset($enrolledUsersInClass['classroom'][$courseId]['class'][$classId]['approved']['users'][$uid]['class_status'])){
							    $enrolledUsersInClass['classroom'][$courseId]['class'][$classId]['approved']['users'][$uid]['class_status'] = get_string('classnoshow','classroomreport');
							// }
						  }elseif($isClassSubmitted == 1){
						
						      if($userArr['isClassCompleted'] == 1){
								$enrolledUsersInClass['classroom'][$cid]['class'][$classId]['approved']['completed'] += 1;
								
								//if(!isset($enrolledUsersInClass['classroom'][$courseId]['class'][$classId]['approved']['users'][$uid]['class_status'])){
								  $enrolledUsersInClass['classroom'][$courseId]['class'][$classId]['approved']['users'][$uid]['class_status'] = get_string('completed','classroomreport');
								//}
							  }else{
								  if(isset($userArr['sessions']) && count($userArr['sessions']) > 0 ){
									  foreach($userArr['sessions'] as $sessions){
										 if($sessions['sessionAttended'] == 1){
											$enrolledUsersInClass['classroom'][$cid]['class'][$classId]['approved']['incomplete'] += 1;
											
											//if(!isset($enrolledUsersInClass['classroom'][$courseId]['class'][$classId]['approved']['users'][$uid]['class_status'])){
											$enrolledUsersInClass['classroom'][$courseId]['class'][$classId]['approved']['users'][$uid]['class_status'] = get_string('inprogress','classroomreport');
											//}
											
										 }
									  }
								  }	  
								  
								  $enrolledUsersInClass['classroom'][$cid]['class'][$classId]['approved']['noshow'] = $enrolledUsersInClass['classroom'][$cid]['class'][$classId]['approved']['total'] - ($enrolledUsersInClass['classroom'][$cid]['class'][$classId]['approved']['completed'] + $enrolledUsersInClass['classroom'][$cid]['class'][$classId]['approved']['incomplete']);
							  }
						  }
						  
						  
						}elseif($userArr['isUserApproved'] == $CFG->classDeclined){
						  $enrolledUsersInClass['classroom'][$cid]['class'][$classId]['declined'] += 1;

						}elseif($userArr['isUserApproved'] == $CFG->waitingForManagerApprovalForClass){
						  $enrolledUsersInClass['classroom'][$courseId]['class'][$classId]['waiting'] += 1;
						}elseif($userArr['isUserApproved'] == $CFG->requestForClass){
						  $enrolledUsersInClass['classroom'][$cid]['class'][$classId]['request'] += 1;
						}
					}
				  }

			}
		  }
		  
		  if($classId){
			  $classSessions = $DB->get_records("scheduler_slots", array('schedulerid'=>$classId)); 
			  $enrolledUsersInClass['classid'] = $classId;
			  $enrolledUsersInClass['classSessions'] = $classSessions;
		  }
		  
		  return $enrolledUsersInClass;

	 }
	 
	 
	 function refineClassroomCourseData($userId, $extraSearch = '', $export=''){
			
		  global $DB, $CFG, $USER;
		  
		  
		  if($userId){

			$fullnameField = extractMDashInSql('gce.fullname');	 
			$query = "SELECT @a := @a +1 AS serial, gce.* FROM vw_get_class_enrollments gce ";
			$query .= " , (SELECT @a:=0) AS x";
			$query .= " WHERE gce.id NOT IN (1) AND gce.coursetype_id = ".$CFG->courseTypeClassroom." ".$extraSearch;
			$query .= "  AND gce.userid ='".$userId."'";
			$query .= "  AND gce.is_approved ='1' ";
			$query .= " GROUP BY gce.scheduler_id, gce.session_id, gce.userid ";
			$query .= " ORDER BY gce.fullname, gce.attended, gce.classname, gce.user_fullname ASC";
		  
		    $records = $DB->get_records_sql($query);

		    $dateFormat = $CFG->customDefaultDateFormat;

			$enrolledUsersInClass = array();	
			if(count($records) > 0 ){
 
				  $i=0;
				  foreach($records as $enrollDetailsArr){
       
	                   $i++;
				       $totalInvitedUser++;
					   $courseName = $enrollDetailsArr->fullname;
					   $className = $enrollDetailsArr->classname;
					   $sessionName = $enrollDetailsArr->sessionname;
					   $primaryInstructor = $enrollDetailsArr->primary_instructor;
					   $courseDeleted = $enrollDetailsArr->course_deleted;
					   $coursePublish = $enrollDetailsArr->course_publish;
					   $userFullname = $enrollDetailsArr->user_fullname;
					   $userName = $enrollDetailsArr->username;
					   
					   $courseTypeId = $enrollDetailsArr->coursetype_id;
					   $courseId = $enrollDetailsArr->id;
					   $enrollClassId = $enrollDetailsArr->scheduler_id;
					   
					   
					   $sessionId = $enrollDetailsArr->session_id;
					   $enrollUserId = $enrollDetailsArr->userid;
					   
					   $isUserApproved = $enrollDetailsArr->is_approved?$enrollDetailsArr->is_approved:0;
					   $classStartDate = $enrollDetailsArr->class_startdate;
					   $classEndDate = $enrollDetailsArr->class_enddate;
					   
					   $isClassCompleted = $enrollDetailsArr->is_completed?$enrollDetailsArr->is_completed:0;
					   $isClassSubmitted = $enrollDetailsArr->is_class_submitted?$enrollDetailsArr->is_class_submitted:0;
					   $classInstructor = $enrollDetailsArr->class_instructor;
					   $classInstructorFullName = $enrollDetailsArr->class_instructor_fullname;
					   $classInstructorUserName = $enrollDetailsArr->class_instructor_username;
					   $classStatus = $enrollDetailsArr->class_status;
					   $enrolmentType = $enrollDetailsArr->enrolmenttype;
					   $classCreator = $enrollDetailsArr->class_creator;
					   $submittedBy = $enrollDetailsArr->class_submitted_by;
					   $classSubmittedByName = $enrollDetailsArr->class_submittedby_name;
					   $classSubmittedByUsername = $enrollDetailsArr->class_submittedby_username;
					   $classSubmittedOn = $enrollDetailsArr->class_submitted_on;
					   $isClassActive = $enrollDetailsArr->is_class_active;
					   $noOfSeats = $enrollDetailsArr->no_of_seats;
					   $sessionAttended = $enrollDetailsArr->attended?$enrollDetailsArr->attended:0;
					   $sessionGrade = $enrollDetailsArr->grade;
					   $sessionScore = $enrollDetailsArr->score;
					   
					   $sessionStartTime = $enrollDetailsArr->session_starttime;
					   $sessionEndTime = $enrollDetailsArr->session_endtime;
					   $sessionDuration = $enrollDetailsArr->session_duration;
					   
					   if(empty($export) || $export == 'exportpdf' || $export == 'print'){
						  $sessionDurationText = getClassroomSessionDateDuration($sessionStartTime, $sessionDuration, 'minutes');
						  $sessionName = $sessionName."<br>(". $sessionDurationText.")";
					   }else{
					      $sessionDurationText = getClassroomSessionDateDuration($sessionStartTime, $sessionDuration, 'minutes', 'csv'); 
						  $sessionName = $sessionName." (". $sessionDurationText.")";
					   }
					   
					   if(empty($export) || $export == 'exportpdf' || $export == 'print'){
					      $classDuration = getClassDuration($classStartDate, $classEndDate);
					   }else{
					      $classDuration = getClassDuration($classStartDate, $classEndDate, 'csv');
					   }
					   

					   $enrolledUsersInClass['courses'][$courseId]['courseId'] = $courseId;
					   $enrolledUsersInClass['courses'][$courseId]['courseName'] = $courseName;
					   $enrolledUsersInClass['courses'][$courseId]['courseDeleted'] = $courseDeleted;
					   $enrolledUsersInClass['courses'][$courseId]['coursePublish'] = $coursePublish;
					   $enrolledUsersInClass['courses'][$courseId]['courseTypeId'] = $courseTypeId;
					   $enrolledUsersInClass['courses'][$courseId]['primaryInstructor'] = $primaryInstructor;

					   
					   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['classId'] = $enrollClassId;
					   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['orgClassName'] = $className;
					   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['className'] = $courseName." - ".$className;
					   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['classStartDate'] = $classStartDate;
					   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['classEndDate'] = $classEndDate;
					   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['classDuration'] = $classDuration;
					   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['isClassSubmitted'] = $isClassSubmitted;

					   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['classInstructor'] = $classInstructor;
					   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['classInstructorFullName'] = $classInstructorFullName;
					   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['classInstructorUserName'] = $classInstructorUserName;
					   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['submittedBy'] = $submittedBy;
					   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['classSubmittedByName'] = $classSubmittedByName;
					   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['classSubmittedByUsername'] = $classSubmittedByUsername;
					   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['classSubmittedOn'] = $classSubmittedOn;
					   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['noOfSeats'] = $noOfSeats;
					   

					   if($enrollUserId){
					   
					
						    $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['users'][$enrollUserId]['userFullname'] = $userFullname;
						    $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['users'][$enrollUserId]['userName'] = $userName;
						    $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['users'][$enrollUserId]['isUserApproved'] = $isUserApproved;
						    $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['users'][$enrollUserId]['isClassCompleted'] = $isClassCompleted;
						    $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['users'][$enrollUserId]['sessions'][$sessionId]['sessionId'] = $sessionId;
						    $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['users'][$enrollUserId]['sessions'][$sessionId]['sessionName'] = $sessionName;
						    $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['users'][$enrollUserId]['sessions'][$sessionId]['sessionAttended'] = $sessionAttended;
						    $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['users'][$enrollUserId]['sessions'][$sessionId]['sessionGrade'] = $sessionGrade;
						    $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['users'][$enrollUserId]['sessions'][$sessionId]['sessionScore'] = $sessionScore;
						   

						   if($isUserApproved == $CFG->classApproved){


								   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['approved']['users'][$enrollUserId] =  $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['users'][$enrollUserId];
								   
								  if(!isset( $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['users'][$enrollUserId])){
								     $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['approved']['total'] += 1;
									 $enrolledUsersInClass['classroom']['total_enrolled'] += 1;
								  }
								  
								  if(!isset( $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['approved']['users'][$enrollUserId]['class_status'])){
								  
								    $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['approved']['users'][$enrollUserId]['class_status'] = get_string('classnoshow','classroomreport');
								  }
								  
								  
								  if($isClassSubmitted == 0){
								  
								   if(!isset( $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['class_status'])){
									    $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['noshow'] += 1;
									    $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['incomplete'] = 0;
									    $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['completed'] = 0;
									    $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['approved']['users'][$enrollUserId]['class_status'] = get_string('classnoshow','classroomreport');
									    $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['class_status'] = get_string('classnoshow','classroomreport'); 
									   
									   $enrolledUsersInClass['classroom']['total_noshow'] += 1;
									 }
									 
					
								  }elseif($isClassSubmitted == 1){
								
									  if($isClassCompleted == 1){
												
										
										    if(!isset( $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['class_status'])){
											   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['approved']['completed'] += 1;
											   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['approved']['users'][$enrollUserId]['class_status'] = get_string('completed','classroomreport');
											   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['class_status'] = get_string('completed','classroomreport');
											  $enrolledUsersInClass['classroom']['total_completed'] += 1;
											}
											
									  }else{
										  if($sessionAttended == 1){
										  
																
												if(!isset( $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['class_status'])){
												   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['approved']['incomplete'] += 1;
												   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['approved']['users'][$enrollUserId]['class_status'] = get_string('inprogress','classroomreport');
												 
												   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['class_status'] = get_string('inprogress','classroomreport');
												  $enrolledUsersInClass['classroom']['total_incomplete'] += 1;
												}
												

										  } else{
										  
																 
											  if(!isset( $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['class_status'])){
												   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['approved']['noshow'] += 1;
												   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['approved']['users'][$enrollUserId]['class_status'] = get_string('classnoshow','classroomreport');
												   $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['class_status'] = get_string('classnoshow','classroomreport'); 
												  $enrolledUsersInClass['classroom']['total_noshow'] += 1;
											 }
 
											  
										  }
										  
										 
									  }
								  }
								
						   }else{
						       continue; 
							   if($isUserApproved == $CFG->classDeclined){
							   
							
								  if(!isset( $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['users'][$enrollUserId])){
									 $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['declined'] += 1;
								  }
		
							   }elseif($isUserApproved == $CFG->waitingForManagerApprovalForClass){
							   
						
								   if(!isset( $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['users'][$enrollUserId])){
									 $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['waiting'] += 1;
								   }
								  
							   }elseif($isUserApproved == $CFG->requestForClass){
				
								  
								  if(!isset( $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['users'][$enrollUserId])){
									 $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['request'] += 1;
								  }
							   }
						    } 
						   
					   }else{
					       
						    $enrolledUsersInClass['approved_class_details']['class'][$enrollClassId]['users'] = array();
					   }
					   
					
					   
				  }

			}
		  }
		  
		
		  //pr($enrolledUsersInClass['approved_class_details']);die;
		  
		  return $enrolledUsersInClass;

	 }
	 


	   /**
	 * This function is using for getting a single classroom course v/s multiple learner report
	 * @global object
	 * @param array $paramArray is search array
	 * @param array $removeKeyArray contain search key that you don't want to add in address bar
	 * @param string $sort is sorting field
	 * @param string $dir is sorting order
	 * @param int $page current page
	 * @param int $perpage is number of records per page
	 * @param string $export is action variable for PDF download, CSV download, and Print a single course v/s multiple learner report
	  * @return array $classroomCourseReportPerLearner return CSV , PDF and Report Content
	 */
	  
	  function getClassroomVsMultipleUserReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export='', $reportsArr, $classArr){		
		  //ini_set('display_errors', 1);
  	      //error_reporting(E_ALL);
		
	        global $DB, $CFG, $USER, $SITE;
			$loginUserId = $USER->id;
			$isReport = true;
			//$userRole =  getUserRole($loginUserId);
			$classroomCourseReportPerLearner = new stdClass();
			
			$sType = $paramArray['type'];
			$sBack = $paramArray['back'];
			$sClassId = $paramArray['class_id'];
		    $sTypeArr = explode("@",$sType);
			
			$offset = $page - 1;
			$offset = $offset*$perpage;
			$limit = '';
			if($perpage != 0){
				$limit = "LIMIT $offset, $perpage";
			}

			////// Getting common URL for the Search //////
			$pageURL = $_SERVER['PHP_SELF'];
			$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
			
			$printUrl = '/course/'.$CFG->pageSingleClassroomPrintReport;
			$genPrintURL = genParameterizedURL($paramArray, $removeKeyArray, $printUrl);
				  
	        $cid    = $paramArray['cid'];
			$courseName = getCourses($cid);
		   
		    //$reportsArr = getUserEnrollStatusOfClassroom($cid);
			
			
			
			if(!empty($sTypeArr) && $sTypeArr[0] != -1 && count($sTypeArr) < 3){
			   
			   $extraSearchArr = array();
			   if(in_array(0, $sTypeArr)){
			     $extraSearchArr[] = '  (gce.is_approved = 1 AND gce.is_class_submitted = 1 AND (gce.attended = 1 AND (gce.is_completed <> 1 OR isnull(gce.is_completed)))) ';
			   }
			   
			   if(in_array(1, $sTypeArr)){
			     $extraSearchArr[] = ' (gce.is_approved = 1 AND gce.is_class_submitted = 1 AND gce.is_completed = 1) ';
			   }
			   
			   if(in_array(2, $sTypeArr)){
			     $extraSearchArr[] = '
				 
				 ifnull(gce.attended,0) = 
( select max(ifnull(attended,0)) FROM vw_session_info_of_classroom where userid = gce.userid and scheduler_id = gce.scheduler_id and id = gce.id)
                  AND 
				  (gce.is_approved = 1 AND ( ( gce.attended <> 1 AND (gce.is_completed <> 1 OR isnull(gce.is_completed)) AND gce.is_class_submitted = 1) OR (gce.is_class_submitted = 0) ))';
				 
				 
			   }
			   
			   if(count($extraSearchArr) > 0 ){
			        $extraSearch .= " AND (";
			        $extraSearch .= implode(" OR ", $extraSearchArr);
					$extraSearch .= " ) ";
					
			   }

			}
			
			
		    $reportsArr = getClassroomReportDetails($cid, $sClassId, $extraSearch);
			
			$classId = $sClassId?$sClassId:(isset($reportsArr['classid'])?$reportsArr['classid']:0);
			
			$extraSearchGraph = " AND id = '".$cid."'";
            $classesOfClassroom = getClassroomReportDetails($cid, $classId, $extraSearchGraph);
		    
			
			//pr($classesOfClassroom['classroom'][$cid]['class'][$classId]);die;

			
			$is_class_submitted = isset($classesOfClassroom['classroom'][$cid]['class'][$classId]['isClassSubmitted'])?$classesOfClassroom['classroom'][$cid]['class'][$classId]['isClassSubmitted']:0;
			
			$no_show_count = isset($classesOfClassroom['classroom'][$cid]['class'][$classId]['approved']['noshow'])?$classesOfClassroom['classroom'][$cid]['class'][$classId]['approved']['noshow']:0;
			
			$in_completed_count = isset($classesOfClassroom['classroom'][$cid]['class'][$classId]['approved']['incomplete'])?$classesOfClassroom['classroom'][$cid]['class'][$classId]['approved']['incomplete']:0;
			$completed_count = isset($classesOfClassroom['classroom'][$cid]['class'][$classId]['approved']['completed'])?$classesOfClassroom['classroom'][$cid]['class'][$classId]['approved']['completed']:0;
			$userCount =  isset($classesOfClassroom['classroom'][$cid]['class'][$classId]['approved']['total'])?$classesOfClassroom['classroom'][$cid]['class'][$classId]['approved']['total']:0;
			
			$totalInviteUser = isset($classesOfClassroom['classroom'][$cid]['class'][$classId]['invited'])?$classesOfClassroom['classroom'][$cid]['class'][$classId]['invited']:0;

			$waitingUser = isset($classesOfClassroom['classroom'][$cid]['class'][$classId]['waiting'])?$classesOfClassroom['classroom'][$cid]['class'][$classId]['waiting']:0;
			$declineUser = isset($classesOfClassroom['classroom'][$cid]['class'][$classId]['declined'])?$classesOfClassroom['classroom'][$cid]['class'][$classId]['declined']:0;
			$approvedUser = $userCount;
			
			$noShowPercentage = numberFormat(($no_show_count*100)/$userCount);
			$inCompletedPercentage = numberFormat(($in_completed_count*100)/$userCount);
			$completedPercentage = numberFormat(($completed_count*100)/$userCount);
			
			//pr($sTypeArr);die;
			
		
			$no_show_count_exploded = in_array(2, $sTypeArr)?true:false;
			$in_completed_count_exploded = in_array(0, $sTypeArr)?true:false;
			$completed_count_exploded = in_array(1, $sTypeArr)?true:false;

			 
			$courseHTML = '';
			$exportHTML = '';
            if(empty($export) && $userCount > 0){
			
				$exportHTML .= '<div class="exports_opt_box "> ';
				$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','classroomreport').'" href="'.$genPrintURL.'&action=exportcsv">'.get_string('downloadcsv','classroomreport').'</a> ';
				$exportHTML .= '<span class="seperater">&nbsp;</span>';
				$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','classroomreport').'" id="exportpdf1" href="'.$genPrintURL.'&action=exportpdf">'.get_string('downloadpdf','classroomreport').'</a> ';
				$exportHTML .= '<span class="seperater">&nbsp;</span>';
				$exportHTML .= '<a rel="'.$genPrintURL.'&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','classroomreport').'" class="print_icon">'.get_string('print','classroomreport').'</a>';
				$exportHTML .= '</div>';
			
			}
			
			$style = (!empty($export) || strstr($_SERVER['REQUEST_URI'], 'course/'.$CFG->pageMultiClassroomReportDetails))?"style=''":'';
			$courseHTML .= "<div class='tabsOuter attendance-performance' ".$style." >";
			if(empty($export) && strstr($_SERVER['REQUEST_URI'], 'course/'.$CFG->pageSingleClassroomReport)){
				$courseHTML .= "<div class='tabLinks'>";
				ob_start();
				$course->id = $cid;
				include_once('course_tabs.php');
				$HTMLTabs = ob_get_contents();
				ob_end_clean();
				$courseHTML .= $HTMLTabs;
				$courseHTML .= "</div>";
			}
			
			$courseHTML .= '<div class="clear"></div>';
			$courseHTML .= '<div class="borderBlockSpace">';
			
			$classDetails = array();
			
			if(!empty($export) && $export == 'print'){
				$reportName = get_string('overalluserprogressreport','classroomreport');
			    $courseHTML .= getReportPrintHeader($reportName);
			}
			
			$sClassName ='';
			if($classId){
			 include_once($CFG->wwwpath."/mod/scheduler/lib.php");
			 $classDetails = getClassDetailsFromModuleId($classId);
			 $headerHtml = getModuleHeaderHtml($classDetails, $CFG->classModule);
			 $courseHTML .= $headerHtml;
			 $sClassName = isset($classDetails->classname) && $classDetails->classname ? $classDetails->classname :'';
			}

			
			if($sType == '-1'){
			  $sTypeName = get_string('all','classroomreport');
			}else{
			  $repTypeArr = array(0=>get_string('inprogress','classroomreport'),1=>get_string('completed','classroomreport'), get_string('classnoshow','classroomreport'));
			  $sTypeArr2 = str_replace(array(0, 1, 2), $repTypeArr, $sTypeArr); 
			  $sTypeName = implode(", ", $sTypeArr2);
			}
			
			if(!empty($export)){

				 $courseHTML .= '<table border="0" cellpadding="0" cellspacing="0" class="generaltable margin_bottom" >';
					$courseHTML .= '<tr>';
						$courseHTML .= '<td width="50%"><strong>'.get_string('name','scheduler').':</strong> '.$sClassName.'</td>';
						$courseHTML .= '<td width="50%"><strong>'.get_string('status','classroomreport').':</strong> '.$sTypeName.'</td>';
					$courseHTML .= '</tr>';
					
				 $courseHTML .= '</table>';
			     //$courseHTML .= '<span '.$CFG->pdfSpanAttribute.'>&nbsp;<br /><strong>'.get_string('coursevsmultipleuserreport','classroomreport').'</strong></span><br /><br />';
			}
			
			$SEARCHHTML = '';
			if(empty($export)){
				GLOBAL $DB;
							
				ob_start();
				require_once($CFG->dirroot . '/local/includes/classroom_report_details_search.php');
				$SEARCHHTML = ob_get_contents();
				ob_end_clean();
				
				
				$courseHTML .= $SEARCHHTML;
				
			}

					
			if($userCount > 0){
	
					   $courseHTML .= ' <div class = "single-report-start" id="watch">
							<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('overalluserprogressreport','classroomreport').$exportHTML.'</span>';
						$courseHTML .= '<div class="single-report-graph-left" id="chartContainer">'.get_string('loading','classroomreport').'</div>
							  <div class="single-report-graph-right">
								<div class="course-count-heading">'.get_string('numberofusers','classroomreport').'</div>
								<div class="course-count">'.$userCount.'</div>
								<div class="seperator">&nbsp;</div>
								<div style="float:left;width:100%">'.get_string('totalinviteduser','classroomreport').': '.$totalInviteUser.', '. get_string('managerforapproval','classroomreport').': '.$waitingUser.', '.get_string('approveduser','classroomreport').': '.$approvedUser.', '.get_string('declineduser','classroomreport').': '.$declineUser.' </div>
								<div class="seperator">&nbsp;</div>
								<div class="course-status">
								  <div class="noshow"><h6>'.get_string('classnoshow','classroomreport').'</h6><span>'.$no_show_count.'</span></div>
								  <div class="clear"></div>
								  <div class="inprogress"><h6>'.get_string('inprogress','classroomreport').'</h6><span>'.$in_completed_count.'</span></div>
								  <div class="clear"></div>
								  <div class="completed"><h6>'.get_string('completed','classroomreport').'</h6><span>'.$completed_count.'</span></div>
								</div>
							  </div>
							</div>
						  </div>
						  <div class="clear"></div>';
		   } 
			
			
			
            $courseHTML .= '<div class="">';
			
			require_once ($CFG->dirroot . '/mod/scheduler/classroom_grade_reports.php');
			
			$courseHTML .= '</div></div></div>';
	
			if($userCount > 0 && (empty($export) || (!empty($export) && $export == 'print'))){
				$courseHTML .=  '<script language="javascript" type="text/javascript">';
				$courseHTML .=  '	$(document).ready(function(){
								
										$("#printbun").bind("click", function(e) {
											var url = $(this).attr("rel");
											window.open(url, "'.get_string('coursevsmultipleuserreport','classroomreport').'", "'.$CFG->printWindowParameter.'");
										});
								
							     });</script> ';
	            
				 
				        $courseHTML .=  '<script language="javascript" type="text/javascript">';
						$courseHTML .=  ' window.onload = function () {
							
												var chart = new CanvasJS.Chart("chartContainer",
												{
													title:{
														text: ""
													},
													theme: "theme2",
													data: [
													{        
														type: "doughnut",
														indexLabelFontFamily: "Arial",       
														indexLabelFontSize: 12,
														startAngle:0,
														indexLabelFontColor: "dimgrey",       
														indexLabelLineColor: "darkgrey", 
														toolTipContent: "{y}%", 					
											
																							
														dataPoints: [
														
														{  y: '.$noShowPercentage.', label: "'.get_string('classnoshow','classroomreport').' ('.$no_show_count.')", exploded:"'.$no_show_count_exploded.'" },
														{  y: '.$inCompletedPercentage.', label: "'.get_string('inprogress','classroomreport').' ('.$in_completed_count.')", exploded:"'.$in_completed_count_exploded.'" },
														{  y: '.$completedPercentage.', label: "'.get_string('completed','classroomreport').' ('.$completed_count.')", exploded:"'.$completed_count_exploded.'" },
											
														]
														
													
													}
													]
												});';
								
					
					    $courseHTML .=  '	chart.render(); ';

													 
						$courseHTML .=  '  }
												</script>
												<script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js"></script>';
	
			}		  
            if(empty($export)){ 
			
				//$courseHTML .= paging_bar($userCount, $page, $perpage, $genURL);
				
				 if(strstr($_SERVER['REQUEST_URI'], 'course/'.$CFG->pageMultiClassroomReportDetails)){
				 
				  if($sBack == 1){
				     $backUrl = $CFG->wwwroot.'/course/course_report.php';
				  }else{
				     $backUrl = $CFG->wwwroot.'/course/'.$CFG->pageMultiClassroomReport;
				  } 
				 
				  $courseHTML .= '<div id="backcell"><input type = "button" value = "'.get_string('back','multicoursereport').'" onclick="location.href=\''.$backUrl.'\';"></div>';
				 }else{
				  $urlCancel = new moodle_url($CFG->wwwroot.'/course/courseview.php', array('id'=>$cid));
				  $courseHTML .= '<div id="backcell"><input type = "button" value = "'.get_string('cancel','classroomreport').'" onclick="location.href=\''.$urlCancel.'\';"></div>';
				}
			}
			
			$courseHTML .= '';
			$courseHTML .=  '';
				
			$courseVsMultipleUserReport->courseHTML = $courseHTML;
			$courseVsMultipleUserReport->reportContentCSV = $reportContentCSV;
			$courseVsMultipleUserReport->reportContentPDF = $reportContentPDF;
			
			return $courseVsMultipleUserReport;
	  }
	  
	  
	  function getClassroomCourseReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export='', $reportsArr, $classArr){		
		  //ini_set('display_errors', 1);
  	     // error_reporting(E_ALL);
		
	        global $DB, $CFG, $USER, $SITE;
			$loginUserId = $USER->id;
			$isReport = true;
			//$userRole =  getUserRole($loginUserId);
			$classroomCourseReport = new stdClass();
			
			$sType = $paramArray['type'];
			$sCourse = $paramArray['course'];
			$sClasses = $paramArray['classes'];
			$cType = $paramArray['ctype'];
			$sBack = $paramArray['back'];
			$sUserId = $paramArray['userid'];
		    $sCourseArr = explode("@",$sCourse);
			$sClassesArr = explode("@",$sClasses);
			$sTypeArr = explode("@",$sType);
			

			$isUserDeleted = isUserDeleted($sUserId);
			if($isUserDeleted){
				redirect('/');
			}
			

			$sStartDate     = $paramArray['startDate'];
			$sEndDate       = $paramArray['endDate'];


			$sDateSelected = '';
			if($sStartDate){
				$sDateArr = getFormattedTimeStampOfDate($sStartDate);
			    $sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

			}		   
			
			$eDateSelected = '';
			if($sEndDate){
			   $eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
			   $eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
			}


			$sClassroomName = $sCourse=='-1'?(get_string('all','classroomreport')):getCourses($sCourseArr);
			$sClassName = $sClasses=='-1'?(get_string('all','classroomreport')):getClasses($sClassesArr);

			$offset = $page - 1;
			$offset = $offset*$perpage;
			$limit = '';
			if($perpage != 0){
				$limit = "LIMIT $offset, $perpage";
			}

			////// Getting common URL for the Search //////
			$pageURL = $_SERVER['PHP_SELF'];
			$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
			
			$printUrl = '/course/classroom_course_report_print.php';
			$genPrintURL = genParameterizedURL($paramArray, $removeKeyArray, $printUrl);
				  
			$extraSearch = '';
            $where = '';
			
			if(!empty($sTypeArr) && $sTypeArr[0] != -1 && count($sTypeArr) < 3){
			   
			   $extraSearchArr = array();
			   if(in_array(0, $sTypeArr)){
			     $extraSearchArr[] = '  (gce.is_approved = 1 AND gce.is_class_submitted = 1 AND (gce.attended = 1 AND (gce.is_completed <> 1 OR isnull(gce.is_completed)))) ';
			   }
			   
			   if(in_array(1, $sTypeArr)){
			     $extraSearchArr[] = ' (gce.is_approved = 1 AND gce.is_class_submitted = 1 AND gce.is_completed = 1) ';
			   }
			   
			   if(in_array(2, $sTypeArr)){
			     $extraSearchArr[] = '
				 
				 ifnull(gce.attended,0) = 
( select max(ifnull(attended,0)) FROM vw_session_info_of_classroom where userid = gce.userid and scheduler_id = gce.scheduler_id and id = gce.id)
                  AND 
				  (gce.is_approved = 1 AND ( ( gce.attended <> 1 AND (gce.is_completed <> 1 OR isnull(gce.is_completed)) AND gce.is_class_submitted = 1) OR (gce.is_class_submitted = 0) ))';
				 
				 
			   }
			   
			   if(count($extraSearchArr) > 0 ){
			        $extraSearch .= " AND (";
			        $extraSearch .= implode(" OR ", $extraSearchArr);
					$extraSearch .= " ) ";
					
			   }

			}
			
			
		
			
			if($sDateSelected && $eDateSelected){ 
				
				
				$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
                $sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
			   
				$extraSearch .= " AND ((gce.class_startdate >= $sStartDateTime && gce.class_startdate <= $sEndDateTime ) || (gce.class_enddate >= $sStartDateTime && gce.class_enddate <= $sEndDateTime ) )";
				$where .= " AND ((gce.class_startdate >= $sStartDateTime && gce.class_startdate <= $sEndDateTime ) || (gce.class_enddate >= $sStartDateTime && gce.class_enddate <= $sEndDateTime ) )";

			}elseif($sDateSelected && $eDateSelected == ''){ 
				
				$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
			    $extraSearch .= " AND (gce.class_startdate >= ($sStartDateTime) || gce.class_enddate >= ($sStartDateTime) )";
				$where .= " AND (gce.class_startdate >= ($sStartDateTime) || gce.class_enddate >= ($sStartDateTime) )";
				
			}elseif($sDateSelected =='' && $eDateSelected){ 
			
			   $sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
			   $extraSearch .= " AND (gce.class_startdate <= ($sEndDateTime) || gce.class_enddate <= ($sEndDateTime) )";
			   $where .= " AND (gce.class_startdate <= ($sEndDateTime) || gce.class_enddate <= ($sEndDateTime) )";
			}
			
			
			if(count($sCourseArr) > 0 && $sCourseArr[0] != '-1' ){ 
			 $courseIds = implode(",", $sCourseArr);
			 $extraSearch .= " AND gce.id = '".$courseIds."'";
			 $where .= " AND gce.id = '".$courseIds."'";
			}
			
			if(count($sClassesArr) > 0 && $sClassesArr[0] != '-1' ){ 
			 $classIds = implode(",", $sClassesArr);
			 $extraSearch .= " AND gce.scheduler_id = '".$classIds."'";
			 $where .= " AND gce.scheduler_id = '".$classIds."'";
			}

           
		    $reportsArr = refineClassroomCourseData($sUserId, $extraSearch, $export);
			
			if($perpage){
			  $offset = $page - 1;
			  $offset = $offset*$perpage;
			  $courseData = array_slice($reportsArr['approved_class_details']['class'], $offset, $perpage, true);
			}else{
			  $courseData = $reportsArr['approved_class_details']['class'];
			}
        
		
			//pr($courseData);die;
			
            $classesOfClassroom = refineClassroomCourseData($sUserId, $where);
		    //pr($classesOfClassroom);die;
			$no_show_count = isset($classesOfClassroom['classroom']['total_noshow'])?$classesOfClassroom['classroom']['total_noshow']:0;
			$in_completed_count = isset($classesOfClassroom['classroom']['total_incomplete'])?$classesOfClassroom['classroom']['total_incomplete']:0;
			$completed_count = isset($classesOfClassroom['classroom']['total_completed'])?$classesOfClassroom['classroom']['total_completed']:0;
			$userCount =  $no_show_count + $in_completed_count + $completed_count;;
			
			
			$noShowPercentage = numberFormat(($no_show_count*100)/$userCount);
			$inCompletedPercentage = numberFormat(($in_completed_count*100)/$userCount);
			$completedPercentage = numberFormat(($completed_count*100)/$userCount);

			$no_show_count_exploded = in_array(2, $sTypeArr)?true:false;
			$in_completed_count_exploded = in_array(0, $sTypeArr)?true:false;
			$completed_count_exploded = in_array(1, $sTypeArr)?true:false;

			 
			$courseHTML = '';
			$exportHTML = '';

			$courseHTML .= '<div class="clear"></div>';
			if($USER->archetype != 'learner' && empty($export)) {
				$cSelected1 = $cType == 1?"selected='selected'":"";
				$cSelected2 = $cType == 2?"selected='selected'":"";
				$courseHTML .= '<select name="" backto="'.$sBack.'" id="reporttype" rel="'.$sUserId.'" class="reportTypeSelect" ><option value="'.$CFG->courseTypeOnline.'" '.$cSelected1.'>'.get_string('onlinecourse').'</option><option value="'.$CFG->courseTypeClassroom.'" '.$cSelected2.' >'.get_string('classroomcourse').'</option></select>';
			
			}
	
			$style = '';
			
			if(empty($export) || $export == 'print'){
			   $courseHTML .= "<div class='tabsOuter' ".$style." >";
			}

			if( $USER->archetype != $CFG->userTypeStudent && empty($export)) {
		
				if($sBack == 1){
				
					$user->id = $sUserId;
					ob_start();
					include_once($CFG->dirroot.'/user/user_tabs.php');
					$HTMLTabs .= ob_get_contents();
					ob_end_clean();
					$courseHTML .= $HTMLTabs;
					
				}
			}
			
			$SEARCHHTML = '';
			if(empty($export)){
				GLOBAL $DB;
							
				//if($sBack != 1){			
					ob_start();
					require_once($CFG->dirroot . '/local/includes/classroom_course_report_search.php');
					$SEARCHHTML = ob_get_contents();
					ob_end_clean();
				//}
				

				$exportHTML .= '<div class="exports_opt_box "> ';
				$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','classroomreport').'" href="'.$genPrintURL.'&action=exportcsv">'.get_string('downloadcsv','classroomreport').'</a> ';
				$exportHTML .= '<span class="seperater">&nbsp;</span>';
				$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','classroomreport').'" id="exportpdf1" href="'.$genPrintURL.'&action=exportpdf">'.get_string('downloadpdf','classroomreport').'</a> ';
				$exportHTML .= '<span class="seperater">&nbsp;</span>';
				$exportHTML .= '<a rel="'.$genPrintURL.'&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','classroomreport').'" class="print_icon">'.get_string('print','classroomreport').'</a>';
				$exportHTML .= '</div>';
				
			}
			
			
			if(!empty($export)){
			
				if($sType == '-1'){
				  $sTypeName = get_string('all','classroomreport');
				}else{
				  $repTypeArr = array(0=>get_string('inprogress','classroomreport'),1=>get_string('completed','classroomreport'), get_string('classnoshow','classroomreport'));
				  $sTypeArr2 = str_replace(array(0, 1, 2), $repTypeArr, $sTypeArr); 
				  $sTypeName = implode(", ", $sTypeArr2);
				}
				
				 $userName = getUsers($sUserId, 1);
			     $reportContentPDF = '';
				 $reportContentCSV = '';
				 
				 $sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
				 $eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
				 $sDateSelectedForPrintCSV = getDateFormatForPrint($sDateSelected, 'csv');
				 $eDateSelectedForPrintCSV = getDateFormatForPrint($eDateSelected, 'csv');
				
				 $printHtml = '';
			     if($export == 'print'){
			     	
			     	 $reportName = get_string('overallclassroomreport','scheduler');
			     	 $printHtml .= getReportPrintHeader($reportName);
			     	
					 $printHtml .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="generaltable margin_bottom">';
						
						$printHtml .= '<tr>';
							$printHtml .= '<td width="50%"><strong>'.get_string('user','singlereport').':</strong> '.$userName.'</td>';
							$printHtml .= '<td width="50%"><strong>'.get_string('course','classroomreport').':</strong> '.$sClassroomName.'</td>';
						$printHtml .= '</tr>';
						$printHtml .= '<tr>';
							$printHtml .= '<td><strong>'.get_string('classname','classroomreport').':</strong>  '.$sClassName.'</td>';
							$printHtml .= '<td><strong>'.get_string('status','classroomreport').':</strong> '.$sTypeName.'</td>';
						$printHtml .= '</tr>';
						$printHtml .= '<tr>';
							$printHtml .= '<td><strong>'.get_string('classstartfrom').':</strong> '.$sDateSelectedForPrint.'</td>';
							$printHtml .= '<td><strong>'.get_string('classstartto').':</strong>  '.$eDateSelectedForPrint.'</td>';		
						$printHtml .= '</tr>';
					 $printHtml .= '</table>';
						 
				
				  }elseif($export == 'exportpdf'){

				 	$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" width="100%">';
					
					$reportContentPDF .= '<tr>';
							$reportContentPDF .= '<td width="50%"><strong>'.get_string('user','singlereport').':</strong> '.$userName.'</td>';
							$reportContentPDF .= '<td width="50%"><strong>'.get_string('course','classroomreport').':</strong> '.$sClassroomName.'</td>';
					$reportContentPDF .= '</tr>';
						
					$reportContentPDF .= '<tr>';
							$reportContentPDF .= '<td><strong>'.get_string('classname','classroomreport').':</strong>  '.$sClassName.'</td>';
							$reportContentPDF .= '<td><strong>'.get_string('status','classroomreport').':</strong> '.$sTypeName.'</td>';
					$reportContentPDF .= '</tr>';
					$reportContentPDF .= '<tr>';
							$reportContentPDF .= '<td><strong>'.get_string('classstartfrom').':</strong> '.$sDateSelectedForPrint.'</td>';
							$reportContentPDF .= '<td><strong>'.get_string('classstartto').':</strong>  '.$eDateSelectedForPrint.'</td>';	
					$reportContentPDF .= '</tr>';
						
					$reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td><strong>'.get_string('numberofcourses','classroomreport').':</strong> '.$userCount.'</td>';
						$reportContentPDF .= '<td><strong>'.get_string('classnoshow','classroomreport').':</strong> '.$no_show_count.' ('.floatval($noShowPercentage).'%)</td>';
					$reportContentPDF .= '</tr>';
		
					$reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td><strong>'.get_string('inprogress','classroomreport').':</strong> '.$in_completed_count.' ('.floatval($inCompletedPercentage).'%)</td>';
						$reportContentPDF .= '<td><strong>'.get_string('completed','classroomreport').':</strong> '.$completed_count.' ('.floatval($completedPercentage).'%)</td>';;
					$reportContentPDF .= '</tr>';
					
					//$reportContentPDF .= '<tr><br /><td width="100%"  ><span '.$CFG->pdfSpanAttribute.'><strong>'.get_string('courseusagesreport','classroomreport').'</strong></span><br /></td></tr>';
		
				    $reportContentPDF .= '</table>';
				    
				    $reportContentPDF .= getGraphImageHTML(get_string('overallclassroomreport','scheduler'));
					
					$reportContentPDF .= '<table '.$CFG->pdfTableStyle.' id="table1"><tr '.$CFG->pdfTableHeaderStyle.'>';
					$reportContentPDF .= '<td><strong>'.get_string('title').'</strong></td>';
					$reportContentPDF .= "<td><strong>" . get_string ( 'attended', 'scheduler' ) . "</strong></td>";
					$reportContentPDF .= "<td><strong>" . get_string ( 'grade', 'scheduler' ) . "</strong></td>";
					$reportContentPDF .= "<td><strong>" . get_string ( 'score', 'scheduler' ) . "</strong></td>";
					$reportContentPDF .= "<td><strong>" . get_string ( 'classcompletedstatus', 'scheduler' ) . "</strong></td>";
					$reportContentPDF .= '</tr>';
					
				
		 
				  }elseif($export == 'exportcsv'){
				  
				        $reportContentCSV .= get_string('user','singlereport').",".$userName."\n";
						$reportContentCSV .= get_string('course','classroomreport').",".$sClassroomName."\n";
						$reportContentCSV .= get_string('classname','classroomreport').",".$sClassName."\n";
						$reportContentCSV .= get_string('status','classroomreport').",".$sTypeName."\n";
						$reportContentCSV .= get_string('classstartfrom').",".$sDateSelectedForPrintCSV."\n";
						$reportContentCSV .= get_string('classstartto').",".$eDateSelectedForPrintCSV."\n";
						$reportContentCSV .= get_string('numberofcourses','classroomreport').",".$userCount."\n";
						$reportContentCSV .= get_string('classnoshow','classroomreport').",".$no_show_count." (".floatval($noShowPercentage)."%)\n";
						$reportContentCSV .= get_string('inprogress','classroomreport').",".$in_completed_count." (".floatval($inCompletedPercentage)."%)\n";
						$reportContentCSV .= get_string('completed','classroomreport').",".$completed_count." (".floatval($completedPercentage)."%)\n";
						$reportContentCSV .= get_string('overallclassroomreport','scheduler')."\n";
						
						$reportContentCSV .= get_string('title').','.get_string ( 'attended', 'scheduler' ).','.get_string ( 'grade', 'scheduler' ).','. get_string ( 'score', 'scheduler' ).','.get_string ( 'classcompletedstatus', 'scheduler' )."\n";
							
				  }
			     
			}
			
			$graphHtml = '';
			
			 if($userCount > 0 && (empty($export) || (!empty($export) && $export == 'print')) ){

				$graphHtml .= ' <div class = "single-report-start" id="watch">
						<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('overallclassroomreport','scheduler').$exportHTML.'</span>';
					$graphHtml .= '<div class="single-report-graph-left" id="chartContainer">'.get_string('loading','classroomreport').'</div>
						  <div class="single-report-graph-right" style="margin-bottom: 25px;">
							<div class="course-count-heading">'.get_string('numberofcourses','classroomreport').'</div>
							<div class="course-count">'.$userCount.'</div>
							<div class="seperator">&nbsp;</div>
							
							<div class="seperator">&nbsp;</div>
							<div class="course-status">
								  <div class="noshow"><h6>'.get_string('classnoshow','classroomreport').'</h6><span>'.$no_show_count.'</span></div>
								  <div class="clear"></div>
								  <div class="inprogress"><h6>'.get_string('inprogress','classroomreport').'</h6><span>'.$in_completed_count.'</span></div>
								  <div class="clear"></div>
								  <div class="completed"><h6>'.get_string('completed','classroomreport').'</h6><span>'.$completed_count.'</span></div>
							</div>
						  </div>
						</div>
					  </div>
					  <div class="clear"></div>';
			
			}
			
		
			if(empty($export) || $export == 'print'){
				$courseHTML .= '<div class="clear"></div>';
				$courseHTML .= '<div class="borderBlockSpace">'; 
				$courseHTML .= $SEARCHHTML;
				$courseHTML .= $printHtml;
				$courseHTML .= $graphHtml;
				$courseHTML .= '<table class = "generaltable" id="table1"><tr class=""><th  width="50%">Title</th>';
				$courseHTML .= "<th width = '12%' align='align_left' >" . get_string ( 'attended', 'scheduler' ) . "</th>";
				$courseHTML .= "<th width = '12%' align='align_left' >" . get_string ( 'grade', 'scheduler' ) . "</th>";
				$courseHTML .= "<th width = '12%' align='align_left' >" . get_string ( 'score', 'scheduler' ) . "</th>";
				$courseHTML .= "<th width = '14%' align='align_left' >" . get_string ( 'classcompletedstatus', 'scheduler' ) . "</th>";	
				$courseHTML .= '</tr>';	
			}
			
			if (count($courseData) > 0 ) {

				foreach ( $courseData as $class_id => $class_rec ) {				
		
		            $className = $class_rec['className']." (".$class_rec['classDuration'].")";
		            if(empty($export) || $export == 'print'){
						$courseHTML .= "<tr class='evenR'><td class='align_left'  width = '14%'><span class='f-left'>" . $className . "</span></td>";
						$courseHTML .= "<td></td>";
						$courseHTML .= "<td></td>";
						$courseHTML .= "<td></td>";
						$courseHTML .= '<td>'.$class_rec['class_status'].'</td>';
						$courseHTML .= "</tr>";	
					}elseif($export == 'exportpdf'){
					
					    $reportContentPDF .= "<tr ".$CFG->pdfTableEvenRowStyle."><td width = '14%'><span>" . $className  . "</span></td>";
						$reportContentPDF .= "<td></td>";
						$reportContentPDF .= "<td></td>";
						$reportContentPDF .= "<td></td>";
						$reportContentPDF .= '<td>'.$class_rec['class_status'].'</td>';
						$reportContentPDF .= "</tr>";
				
					}elseif($export == 'exportcsv'){
					   $reportContentCSV .=  $className .', , , ,'.$class_rec['class_status']."\n";
					}
						
			
					$courseHTML .= "<tr><td colspan='5' class='borderLeftRight'>";
						$courseHTML .= "<div class='a-box'>";
							$i=0;
							if(isset($class_rec['users'][$sUserId]['sessions']) && count($class_rec['users'][$sUserId]['sessions']) > 0 ){
								foreach ( $class_rec['users'][$sUserId]['sessions'] as $session ) {
									$class = $i%2!=0?'even':'odd';
									$classSession = $i%2!=0?$CFG->pdfInnerTableOddRowStyle:$CFG->pdfInnerTableEvenRowStyle;
									$i++;
									$courseHTML .= "<div class='".$class."' >
									<div style='width:50%'>" . $session['sessionName'] . "</div>";
									
									if($class_rec['class_status'] == 'No Show'){
									
									    if(empty($export) || $export == 'print'){
											$courseHTML .= "<div style='width:12%'>".getMDash()."</div>";
											$courseHTML .= "<div style='width:12%'>".getMDash()."</div>";
											$courseHTML .= "<div style='width:12%'>".getMDash()."</div>";
										}elseif($export == 'exportpdf'){
										    $reportContentPDF .= "<tr ".$classSession."><td>" .getSpace().$session['sessionName']. "</td>";
											$reportContentPDF .= "<td>".getMDash()."</td>";
											$reportContentPDF .= "<td>".getMDash()."</td>";
											$reportContentPDF .= "<td>".getMDash()."</td>";
											$reportContentPDF .= "<td></td></tr>";
										}elseif($export == 'exportcsv'){
										    $reportContentCSV .= getSpace('csv').$session['sessionName'].','.getMDashForCSV().','.getMDashForCSV().','.getMDashForCSV()."\n";
										}
								
										
									}else{
									
									    if(empty($export) || $export == 'print'){
											$courseHTML .= "<div style='width:12%'>".($session['sessionAttended'] == 1?'Yes':'No')."</div>";
											$courseHTML .= "<div style='width:12%'>".($session['sessionGrade']!=''?$session['sessionGrade']:getMDash())."</div>";
											$courseHTML .= "<div style='width:12%'>".($session['sessionScore']!=''?$session['sessionScore']:getMDash())."</div>";
										}elseif($export == 'exportpdf'){
											$reportContentPDF .= "<tr ".$classSession."><td>" .getSpace(). $session['sessionName'] . "</td>";
											$reportContentPDF .= "<td>".($session['sessionAttended'] == 1?'Yes':'No')."</td>";
											$reportContentPDF .= "<td>".($session['sessionGrade']!=''?$session['sessionGrade']:getMDash())."</td>";
											$reportContentPDF .= "<td>".($session['sessionScore']!=''?$session['sessionScore']:getMDash())."</td>";
											$reportContentPDF .= "<td></td></tr>";
										}elseif($export == 'exportcsv'){
										    $reportContentCSV .= getSpace('csv').$session['sessionName'].','.($session['sessionAttended'] == 1?'Yes':'No').','.($session['sessionGrade']!=''?$session['sessionGrade']:getMDashForCSV()).','.($session['sessionScore']!=''?$session['sessionScore']:getMDashForCSV())."\n";
							           }
									}
									$courseHTML .= "<div  style='width:14%'></div>";
									$courseHTML .= "</div>";			
									
								}
							}else{
								$courseHTML .= get_string('norecordfound','scheduler');
								$reportContentPDF .= get_string('norecordfound','scheduler');
								$reportContentCSV .= get_string('norecordfound','scheduler')."\n";
							}
						
						$courseHTML .= "</div>";
					$courseHTML .= "</td></tr>";			
				
				}
				  
			}else{
			   $courseHTML .= '<tr><td colspan="5">'.get_string('norecordfound','scheduler').'</td></tr>';
			   $reportContentPDF .= '<tr><td colspan="5">'.get_string('norecordfound','scheduler').'</td></tr>';
			   $reportContentCSV .= get_string('norecordfound','scheduler')."\n";
			}
					
			if(empty($export) || $export == 'print'){
			  $courseHTML .= "</table>";	
			  $courseHTML .="</div></div>";
			}elseif($export == 'exportpdf'){	
			  $reportContentPDF .= "</table>";
			}
			
			if( (empty($export) || $export == 'print')){
				
					$courseHTML .=  '<script>	$(document).ready(function(){
													
														$("#reporttype").change(function(){ 
															var userid = $(this).attr("rel");
															var backto = $(this).attr("backto");
															
															if(backto == 2){
															  url = "'.$CFG->wwwroot.'/user/multiuser_report_details.php?uid="+userid+"&course=1";			
															}else if(backto == 1){
															  url = "'.$CFG->wwwroot.'/user/single_report.php?uid="+userid;		
															}else if(backto == 3){
															  url = "'.$CFG->wwwroot.'/user/multiuser_report_details.php?uid="+userid+"&back=3";		
															}else{
															  url = "'.$CFG->wwwroot.'/reports/user_performance_report.php?uid="+userid+"&back=3";		
															}
															  		
															window.location.href = url;
														});
															
														$("#printbun").bind("click", function(e) {
															var url = $(this).attr("rel");
															window.open(url, "'.get_string('courseusagesreport','classroomreport').'", "'.$CFG->printWindowParameter.'");
														});	
												 }); 
										</script>';
												
					if($userCount > 0 ){							
					   $courseHTML .=  ' <script>
					   		                 window.onload = function () {
						
													var chart = new CanvasJS.Chart("chartContainer",
													{
														title:{
															text: ""
														},
														theme: "theme2",
														data: [
														{
															type: "doughnut",
															indexLabelFontFamily: "Arial",
															indexLabelFontSize: 12,
															startAngle:0,
															indexLabelFontColor: "dimgrey",
															indexLabelLineColor: "darkgrey",
															toolTipContent: "{y}%",
								
															dataPoints: [
													
															{  y: '.$noShowPercentage.', label: "'.get_string('classnoshow','classroomreport').' ('.$no_show_count.')", exploded:"'.$no_show_count_exploded.'" },
															{  y: '.$inCompletedPercentage.', label: "'.get_string('inprogress','classroomreport').' ('.$in_completed_count.')", exploded:"'.$in_completed_count_exploded.'" },
															{  y: '.$completedPercentage.', label: "'.get_string('completed','classroomreport').' ('.$completed_count.')", exploded:"'.$completed_count_exploded.'" },
												
															]
		
					
														}
														]
													}); ';
						
					  
						  $courseHTML .=  '	chart.render(); ';
						  $courseHTML .=  '  }';
						  
						  $courseHTML .=  '</script><script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js"></script>';
						  
				     }
					
				
				
				
				if(count($reportsArr['approved_class_details']['class']) > 0 && empty($export)) {
					$courseHTML .= paging_bar(count($reportsArr['approved_class_details']['class']), $page, $perpage, $genURL);
				}
				
				if(empty($export)){	
					if($USER->archetype == 'learner'){
					}else{
						if($sBack == 2){
						  $courseHTML .= '<div id="backcell" ><input type = "button" value = "'.get_string('back').'" onclick="location.href=\''.$CFG->wwwroot.'/user/multiuser_report.php\';"></div>';							                        }elseif($sBack == 3){
						  $courseHTML .= '<div id="backcell" ><input type = "button" value = "'.get_string('back').'" onclick="location.href=\''.$CFG->wwwroot.'/user/user_report.php\';"></div>';			
						}elseif($sBack == 1){
						  //$courseHTML .= '<div id="backcell" ><input type = "button" value = "'.get_string('back').'" onclick="location.href=\''.$CFG->wwwroot.'/admin/user.php\';"</div>';		
						}
	
					}
				}	

			}
				
			$classroomCourseReport->courseHTML = $courseHTML;
			$classroomCourseReport->reportContentCSV = $reportContentCSV;
			$classroomCourseReport->reportContentPDF = $reportContentPDF;
			
			return $classroomCourseReport;
	  }
	  
	  
	
	  /**
	 * get user enroll status  of classroom 
	 * @param int $courseId is course id
	 * @return array $enrolledUsers user enroll status  of classroom 
	 */
	 
	 
	function getUserEnrollStatusOfClassroom($courseId){
	
		global $DB, $CFG;
		$enrolledUsers = array();
		if($courseId){
			
			$classesOfClassroom = getClassesOfClassroom($courseId);
			$classIds = array();
			$enrolledUsers = array();	
			if(count($classesOfClassroom) > 0 ){
			  $classIds = array_keys($classesOfClassroom);
			  $enrollDetails = getUserEnrollmentDetailsInClass($classIds);
			  //pr($enrollDetails);die;
			  if(count($enrollDetails) > 0 ){
				 
				  $totalEnrollUser = 0; 
				  $totalCompletedUser = 0;
				  $totalInCompletedUser = 0;
				  foreach($enrollDetails as $enrollDetailsArr){
				       $totalEnrollUser++;
					   $enrollUserId = $enrollDetailsArr->userid;
					   $enrollClassId = $enrollDetailsArr->scheduler_id;
					   $isClassCompleted = $enrollDetailsArr->is_completed;
					   $isClassSubmitted = $enrollDetailsArr->is_class_submitted;
					   
					   $classGrade = $enrollDetailsArr->grade;
					   $userId = $enrollDetailsArr->userid;
					   $leranerFullName = $enrollDetailsArr->leranerfullname;
					   $isClassroomCompleted = 0;
					   
					   $enrolledUsers['users'][$enrollUserId]['is_classroom_completed'] = $isClassroomCompleted;
					   if($isClassCompleted == 1){
						 $isClassroomCompleted = 1;
						 $enrolledUsers['users'][$enrollUserId]['is_classroom_completed'] = $isClassroomCompleted;
						 $totalCompletedUser++;
					   }else{
					     $totalInCompletedUser++;
					   }
					   
					   if($classGrade == 0){
						 $classroomGrade = 0;
					   }else{
					     $classroomGrade = $classGrade;
					   }
					   
					   $enrolledUsers['users'][$enrollUserId]['userid'] = $userId;
					   $enrolledUsers['users'][$enrollUserId]['learnername'] = $leranerFullName;
					   $enrolledUsers['users'][$enrollUserId]['classroom_grade'] = $classroomGrade;
					   $enrolledUsers['users'][$enrollUserId]['class'][$enrollClassId] = $enrollDetailsArr;
					   
				  }
				  
				  $totalEnrollUser = count($enrolledUsers['users']);
				  $enrolledUsers['completed_enroll_user'] = $totalCompletedUser;
				  $enrolledUsers['incompleted_enroll_user'] = $totalEnrollUser - $totalCompletedUser;
				  $enrolledUsers['total_enroll_user'] = $totalEnrollUser;
				  
			  }
			}
			
		}	
		return  $enrolledUsers;
	}
	
	
	//function to get single date if start date and end date are same
	//@$c_startdate : class start date
	//@$c_startdate : class end date
	
	function getClassDuration($c_startdate,$c_enddate, $format = ''){
		global  $CFG;
		$enddate = $c_startdate+86399;
		$mDash = $format == 'csv'?getMDashForCSV():getMDash();
		$nDash = $format == 'csv'?getNDashForCSV():getNDash();
		$nDash = " ".$nDash." ";
		if($c_enddate<=$enddate && $c_enddate>$c_startdate){
		    if($format == 'csv'){
			  $duration_text = $c_startdate?getDateFormat($c_startdate, $CFG->customDefaultDateFormatForCSV):$mDash;
			}else{
			  $duration_text = $c_startdate?getDateFormat($c_startdate, $CFG->customDefaultDateFormat):$mDash;
			}
		}else{
		
		   if($format == 'csv'){
			  $duration_text = $c_startdate && $c_enddate? (getDateFormat($c_startdate, $CFG->customDefaultDateFormatForCSV).$nDash.getDateFormat($c_enddate, $CFG->customDefaultDateFormatForCSV)):$mDash;
			}else{
			  $duration_text = $c_startdate && $c_enddate? (getDateFormat($c_startdate, $CFG->customDefaultDateFormat).$nDash.getDateFormat($c_enddate, $CFG->customDefaultDateFormat)):$mDash;
			}
		}
		return $duration_text;
	}
	


	 /**
	 * get user enroll status  of classroom per class
	 * @param int $courseId is course id
	 * @return array $enrolledUsers user enroll status  of classroom 
	 */
	 
	 
	function getUserEnrollStatusOfClassroomPerClass($courseId){
	
		global $DB, $CFG, $USER;
		$enrolledUsers = array();
		$dateFormat = $CFG->customDefaultDateFormat;
		
		if($courseId){
			
			$classesOfClassroom = getClassesOfClassroom($courseId);
			
			$classIds = array();
			$enrolledUsersInClass = array();	
			if(count($classesOfClassroom) > 0 ){
			  $classIds = array_keys($classesOfClassroom);
			  $enrollDetails = getUserEnrollmentDetailsInClass($classIds);
			 // pr($enrollDetails);die;
			
			  if(count($enrollDetails) > 0 ){
				 
				  $totalEnrollUser = 0; 
				  $totalCompletedUser = 0;
				  $totalApprovedUser = 0;
				  $totalDeclinedUser = 0;
				  $totalWaitingUser = 0;
				  $totalRequestForClassUser = 0;
				  foreach($enrollDetails as $enrollDetailsArr){
			
				       $totalEnrollUser++;
					   $isUserApproved = $enrollDetailsArr->is_approved;
					   
					   $classStartDate = $enrollDetailsArr->class_startdate;
					   $classEndDate = $enrollDetailsArr->class_enddate;
					   $classDuration = getClassDuration($classStartDate, $classEndDate);
					   $enrollUserId = $enrollDetailsArr->userid;
					   $enrollClassId = $enrollDetailsArr->scheduler_id;
					   $className = $enrollDetailsArr->name;
					   $isClassCompleted = $enrollDetailsArr->is_completed;
					   $isClassSubmitted = $enrollDetailsArr->is_class_submitted;
					   $teacher = $enrollDetailsArr->teacher;
					   $course = $enrollDetailsArr->course;
					   $submitted_by = $enrollDetailsArr->submitted_by;
					   $class_submittedby_name = $enrollDetailsArr->class_submittedby_name;
					   $class_submittedby_username = $enrollDetailsArr->class_submittedby_username;
					   $submitted_on = $enrollDetailsArr->submitted_on;
					   $no_of_seats = $enrollDetailsArr->no_of_seats;
					   
					   
					   $classGrade = $enrollDetailsArr->grade;
					   $userId = $enrollDetailsArr->userid;

					   if($isClassCompleted == 1){
						 $totalCompletedUser++;
						 if($isUserApproved == $CFG->classApproved){
					       $totalApprovedUser++;
						 }
					   }else{
					     
						 if($isUserApproved == $CFG->classApproved){
					       $totalApprovedUser++;
						 }elseif($isUserApproved == $CFG->classDeclined){
					       $totalDeclinedUser++;
						 }elseif($isUserApproved == $CFG->waitingForManagerApprovalForClass){
					       $totalWaitingUser++;
						 }elseif($isUserApproved == $CFG->requestForClass){
					       $totalRequestForClassUser++;
						 }
					   }
					   
					   $enrolledUsersInClass['class'][$enrollClassId]['classid'] = $enrollClassId;
					   $enrolledUsersInClass['class'][$enrollClassId]['classname'] = $className;
					   $enrolledUsersInClass['class'][$enrollClassId]['class_start_date'] = $classStartDate;
					   $enrolledUsersInClass['class'][$enrollClassId]['class_end_date'] = $classEndDate;
					   $enrolledUsersInClass['class'][$enrollClassId]['class_duration'] = $classDuration;
					   $enrolledUsersInClass['class'][$enrollClassId]['is_class_submitted'] = $isClassSubmitted;
					   $enrolledUsersInClass['class'][$enrollClassId]['course'] = $course;
					   $enrolledUsersInClass['class'][$enrollClassId]['teacher'] = $teacher;
					   $enrolledUsersInClass['class'][$enrollClassId]['submitted_by'] = $submitted_by;
					   $enrolledUsersInClass['class'][$enrollClassId]['class_submittedby_name'] = $class_submittedby_name;
					   $enrolledUsersInClass['class'][$enrollClassId]['class_submittedby_username'] = $class_submittedby_username;
					   $enrolledUsersInClass['class'][$enrollClassId]['submitted_on'] = $submitted_on;
					   $enrolledUsersInClass['class'][$enrollClassId]['no_of_seats'] = $no_of_seats;
					   $enrolledUsersInClass['class'][$enrollClassId]['users'][$userId] = $enrollDetailsArr;
					   $courseModule = getSchedulerModuleId($enrollClassId, $courseId);
				       $classSessionList = classSessionList($courseModule->id);
					   $enrolledUsersInClass['class'][$enrollClassId]['sessions'] = $classSessionList;
					   
				  }
				  

				  arraySortByColumn($enrolledUsersInClass['class'], 'classname');
				  $enrolledUsersInClass2 = array();
				  if(count($enrolledUsersInClass['class']) > 0 ){
				   foreach($enrolledUsersInClass['class'] as $key=>$valArr){
				      $enrolledUsersInClass2['class'][$valArr['classid']] = $enrolledUsersInClass['class'][$key];
				   }
				   unset($enrolledUsersInClass['class']);
				   $enrolledUsersInClass = $enrolledUsersInClass2;
				  }
				  //pr($enrolledUsersInClass['class']);
				  
				  if(isset($enrolledUsersInClass['class']) && count($enrolledUsersInClass['class']) > 0 ){
				    foreach($enrolledUsersInClass['class'] as $classIid=>$rows){
					
					  $totalInCUser = count($rows['users']);
					  $totalCLUser = 0;
					  $totalApUser = 0;
					  $totalDecUser = 0;
					  $totalWUser = 0;
					  if(isset($rows['users']) && $totalInCUser > 0 ){
					          
							  foreach($rows['users'] as $userId=>$userArr){
							  
								   $isCUserApproved = $userArr->is_approved;
								   $isUClassCompleted = $userArr->is_completed;
								  
								   if($isCUserApproved == $CFG->classApproved){
									 $totalApUser++;
									 if($isUClassCompleted == $CFG->classCompleted){
									   $totalCLUser++;
									 }
								   }else{
									 
									 if($isCUserApproved == $CFG->classApproved){
									   $totalApUser++;
									 }elseif($isCUserApproved == $CFG->classDeclined){
									   $totalDecUser++;
									 }elseif($isCUserApproved == $CFG->waitingForManagerApprovalForClass){
									   $totalWUser++;
									 }
								   }

							  }

					  }

					  $enrolledUsersInClass['class'][$classIid]['user_details']['total_invite_user'] = $totalInCUser;
					  $enrolledUsersInClass['class'][$classIid]['user_details']['waiting_user'] = $totalWUser;
					  $enrolledUsersInClass['class'][$classIid]['user_details']['declined_user'] = $totalDecUser;
					  $enrolledUsersInClass['class'][$classIid]['user_details']['approved_user']['total_approved'] = $totalApUser;
					  $enrolledUsersInClass['class'][$classIid]['user_details']['approved_user']['completed_user'] = $totalCLUser;
					  $enrolledUsersInClass['class'][$classIid]['user_details']['approved_user']['incompleted_user'] = $totalApUser - $totalCLUser;
					}
				  }
				  //pr($enrolledUsersInClass);die;
				  $totalInviteUser = $totalCompletedUser + $totalApprovedUser + $totalDeclinedUser + $totalWaitingUser + $totalRequestForClassUser;
				  $enrolledUsersInClass['all_invite_user'] = $totalInviteUser;
				  $enrolledUsersInClass['all_requesting_user'] = $totalRequestForClassUser;
				  $enrolledUsersInClass['all_completed_user'] = $totalCompletedUser;
				  $enrolledUsersInClass['all_waiting_user'] = $totalWaitingUser;
				  $enrolledUsersInClass['all_approved_user'] = $totalApprovedUser;
				  $enrolledUsersInClass['all_declined_user'] = $totalDeclinedUser;
				  
				  
				  
			  }			  
			  
			  
			         if(count($classIds) > 0 ){
						
							 foreach( $classIds as  $classId){
							 
							 
									   if(isset($enrolledUsersInClass['class'][$classIid])){
										   $classArr = $DB->get_record('scheduler', array('id'=>$classId));
										   
										   $className = $classArr->name;
										   $classStartDate = $classArr->startdate;
										   $classEndDate = $classArr->enddate;
										   $classDuration = getClassDuration($classStartDate, $classEndDate);	
										   $course = $classArr->course;
										   $teacher = $classArr->teacher;  
										   $submitted_by = $classArr->submitted_by;
										   $class_submittedby_name = $classArr->class_submittedby_name;
										   $class_submittedby_username = $classArr->class_submittedby_username;
										   $submitted_on = $classArr->submitted_on; 
										   $no_of_seats = $classArr->no_of_seats;
										   $enrolledUsersInClass['class'][$classId]['classid'] = $classId;
										   $enrolledUsersInClass['class'][$classId]['classname'] = $className;
										   $enrolledUsersInClass['class'][$classId]['class_start_date'] = $classStartDate;
										   $enrolledUsersInClass['class'][$classId]['class_end_date'] = $classEndDate;
										   $enrolledUsersInClass['class'][$classId]['class_duration'] = $classDuration;
										   $enrolledUsersInClass['class'][$classId]['course'] = $course;
					                       $enrolledUsersInClass['class'][$classId]['teacher'] = $teacher;
										   $enrolledUsersInClass['class'][$classId]['submitted_by'] = $submitted_by;
										   $enrolledUsersInClass['class'][$classId]['class_submittedby_name'] = $class_submittedby_name;
										   $enrolledUsersInClass['class'][$classId]['class_submittedby_username'] = $class_submittedby_username;
										   $enrolledUsersInClass['class'][$classId]['submitted_on'] = $submitted_on;
										   $enrolledUsersInClass['class'][$classId]['no_of_seats'] = $no_of_seats;
										   $enrolledUsersInClass['class'][$classId]['users'] = array();
										   $courseModule = getSchedulerModuleId($classId, $courseId);
										   $classSessionList = classSessionList($courseModule->id);
										   $enrolledUsersInClass['class'][$classId]['sessions'] = $classSessionList;
										   
									  }	   
							 } 
					 }		
					 
			}
			
		}	
		//pr($enrolledUsersInClass);die;
		return  $enrolledUsersInClass;
	}
	
	 /**
	 * get class list of classroom
	 * @param int $courseId is course id
	 * @return array $result class list of classroom
	 */
	 
	function getClassesOfClassroom($courseId){
	
	
		global $DB, $CFG;
		$result = array();
        include_once($CFG->wwwpath."/mod/scheduler/lib.php");
		if($courseId){
		  $result = classroomSchedulerList($courseId);
		}
		return $result;
	}
	
	
	
	//function send open invitation
	
	function emailForSendOpenInvitation($userid,$scheduleRec,$enrolment_id,$type=false){
		global $DB,$USER,$CFG;
		//pr($all_users);
		if($CFG->SendOpenInvite == 0){
			return true;
		}
		if($type=='all'){
			$toUser = $userid;
		}
		else{
			$toUserQuery = "SELECT id, firstname, lastname, email, createdby FROM {$CFG->prefix}user WHERE id = '".$userid."'";
	
			$toUser = $DB->get_record_sql($toUserQuery);
		}
		$fromUser->email = $USER->email;
		$fromUser->firstname = $USER->firstname;
		$fromUser->lastname = $USER->lastname;
		$fromUser->maildisplay = true;
		$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$fromUser->id = $USER->id;
		$fromUser->firstnamephonetic = '';
		$fromUser->lastnamephonetic = '';
		$fromUser->middlename = '';
		$fromUser->alternatename = '';	
		$locationText = '';
		if(trim($scheduleRec->location) != ''){
			$locationText = "<li>".get_string('location','scheduler').': '.$scheduleRec->location."</li>";
		}
		$sessionsListing = sessionListForEmails($scheduleRec->id);
		$emailContent = array(
					'name'			=> $toUser->firstname." ".$toUser->lastname,
					'coursename'	=> $scheduleRec->fullname.": ".$scheduleRec->name,
					'datetime'		=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
					'starttime'		=> getDateFormat($scheduleRec->startdate, $CFG->customDefaultDateFormat),
					'classduration'	=> getClassDuration($scheduleRec->startdate, $scheduleRec->enddate),
					'lmsLink'		=> $CFG->emailLmsLink,
					'lmsName'		=> $CFG->emailLmsName,
					'sessionsListing'=> $sessionsListing,
					'location'		=> $locationText);
		$emailContent = (object)$emailContent;
		$emailText = get_string("open_class_invitation",'email',$emailContent);
		$emailText .= $courseText;
		$emailText .= get_string('email_footer','email');
		$emailText .= get_string('from_email','email',$emailContent);
		$subject	= get_string('open_class_invitation_subject','email',$emailContent);
		email_to_user($toUser, $fromUser, $subject, $emailText, '', "", "", true);
	}
	
	
	//Delete Event for class
	
	function deleteEventForClass($classId,$userId){
		global $DB;
		$query = 'DELETE FROM mdl_event WHERE userid = '.$userId.' AND sessionid IN (SELECT ss.id FROM mdl_scheduler_slots as ss WHERE ss.schedulerid = '.$classId.')';
	
		$DB->execute($query);
		
		return;
	}
	
	//add class event
	//@title : title of event
	//@duration : in minutes
	function addClassEvent($title,$description,$userid,$sessionid,$starttime,$duration){
		global $DB,$USER,$CFG;
		$unix_time_duration = $duration*60;
		$event_rec = new stdClass();
		$event_rec->name = $title;
		$event_rec->description = $description;
		$event_rec->format = 1;
		//	$event_rec->courseid = $courseid;
		$event_rec->userid = $userid;
		$event_rec->modulename =0;
		$event_rec->sessionid = $sessionid;
		$event_rec->eventtype = 'user';
		$event_rec->timestart = $starttime;
		$event_rec->timeduration = $unix_time_duration;
		$event_rec->timemodified = time();
		$createdBy = '';
		if($sessionid){
		  $createdBy = $DB->get_field_sql("select ms.createdby FROM mdl_scheduler ms INNER JOIN mdl_scheduler_slots mss ON (ms.id = mss.schedulerid)  where mss.id = '". $sessionid."'");
		}
		$event_rec->createdby = $createdBy?$createdBy:$USER->id;
	
		$DB->insert_record('event',$event_rec);
		return;
	}
	
	// function to update event by session
	
function updateClassEvent($slotid,$starttime,$duration,$title){
		global $DB,$USER,$CFG;
		$unix_time_duration = $duration*60;
		
		
		//$update_event->sequence = 'sequence + 1';
		
		 $sql = "UPDATE mdl_event SET name=?, timestart=?,timeduration=?,sequence = sequence + 1 where sessionid=?";

$params = array(addslashes($title), $starttime, $unix_time_duration, $slotid);
		$DB->execute($sql,$params);
		
		return;
	}
	
	
	
	function getClassesOfClassroomDetails($extraSearch = ''){
			
		  global $DB, $CFG, $USER;
			  
		  if($USER->archetype != $CFG->userTypeAdmin){
			$extraSearch .= " AND (cdc.primary_instructor = ".$USER->id." || cdc.class_creator = ".$USER->id." || cdc.class_instructor = ".$USER->id.")";
		  }
	      $fullnameField = extractMDashInSql('cdc.fullname');	 
		  $query = "SELECT @a := @a +1 AS serial, cdc.id, $fullnameField AS fullname,cdc.course_startdate, cdc.course_enddate,cdc.primary_instructor, cdc.classname,cdc.class_startdate,cdc.class_enddate,cdc.class_status,cdc.enrolmenttype,cdc.class_creator,cdc.class_instructor,cdc.scheduler_id,cdc.userid,cdc.is_approved,cdc.attended,cdc.is_completed, cdc.is_class_submitted,cdc.enrolled_user_count,cdc.inprogress_user_count,cdc.completed_user_count,cdc.noshow_user_count FROM vw_class_info_of_classroom cdc ";
		 // $query .= " LEFT JOIN vw_class_details_of_classroom cdc ON (mc.id = cdc.id), (SELECT @a:=0) AS x";
		  $query .= " , (SELECT @a:=0) AS x";
		  $query .= " WHERE cdc.id NOT IN (1) AND cdc.coursetype_id = ".$CFG->courseTypeClassroom." ".$extraSearch;
		  $query .= " GROUP BY cdc.scheduler_id ORDER BY cdc.fullname, cdc.classname ASC ";
		  
		  $records = $DB->get_records_sql($query);
		  $classesOfClassroom = array();
		  if(count($records) > 0 ){
			$overall_enrolled_user_count = 0;
			foreach($records as $arr){
			   $classesOfClassroom['classroom'][$arr->id]['course_id'] = $arr->id;
			   $classesOfClassroom['classroom'][$arr->id]['course_name'] = $arr->fullname;
			   if($arr->scheduler_id){
				 $classesOfClassroom['classroom'][$arr->id]['class'][] = $arr;
			   }
			   $enrolled_user_count = $arr->enrolled_user_count?$arr->enrolled_user_count:0;
			   $classesOfClassroom['classroom'][$arr->id]['total_enrolled_user_count'] += $enrolled_user_count;
			   $overall_enrolled_user_count += $enrolled_user_count;
			}
			$classesOfClassroom['overall_enrolled_user_count'] = $overall_enrolled_user_count;
		  }
		  
		  return $classesOfClassroom;

	 }
	 
	   /**
	 * This function is using for getting a classroom Usages Report
	 * @global object
	 * @param array $paramArray is search array
	 * @param array $removeKeyArray contain search key that you don't want to add in address bar
	 * @param string $sort is sorting field
	 * @param string $dir is sorting order
	 * @param int $page current page
	 * @param int $perpage is number of records per page
	 * @param string $export is action variable for PDF download, CSV download, and Print Course Usages Report
	 * @return array $classroomUsagesReport return CSV , PDF and Report Content
	 */
	
	
	  
	  function getClassroomUsagesReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export=''){
	  
	       //ini_set('display_errors', 1);
  	       // error_reporting(E_ALL);
		
	        global $DB, $CFG, $USER;
			$classroomUsagesReport = new stdClass();
			$isReport = true;
			
			$offset = $page - 1;
			$offset = $offset*$perpage;
			$limit = '';
			if($perpage != 0){
				$limit = "LIMIT $offset, $perpage";
			}

			////// Getting common URL for the Search //////
			$pageURL = $_SERVER['PHP_SELF'];
			$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
			
			$printUrl = '/course/'.$CFG->pageMultiClassroomPrintReport;
			$genPrintURL = genParameterizedURL($paramArray, $removeKeyArray, $printUrl);
			$sCourse        = $paramArray['course'];
			$sclasses        = $paramArray['classes'];
			$sStartDate     = $paramArray['startDate'];
			$sEndDate       = $paramArray['endDate'];
			
			
			$sType          = $paramArray['type'];
			$sTypeArr = explode("@",$sType);

			if($sType == '-1'){
			  $sTypeName = get_string('all','classroomreport');
			}else{
							  
			  $repTypeArr = array(0=>get_string('inprogress','classroomreport'),1=>get_string('completed','classroomreport'), get_string('classnoshow','classroomreport'));
			  $sTypeArr2 = str_replace(array(0, 1, 2), $repTypeArr, $sTypeArr); 
			  $sTypeName = implode(", ", $sTypeArr2);
			}

			$sDateSelected = '';
			if($sStartDate){
				$sDateArr = getFormattedTimeStampOfDate($sStartDate);
			    $sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

			}		   
			
			$eDateSelected = '';
			if($sEndDate){
			   $eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
			   $eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
			}
			
			
			$sCourseArr = explode("@",$sCourse);
			$sclassesArr = explode("@",$sclasses);
			$sClassroomName = $sCourse=='-1'?(get_string('all','classroomreport')):getCourses($sCourseArr);
			$sClassName = $sclasses=='-1'?(get_string('all','classroomreport')):getClasses($sclassesArr);
			
		    $extraSearch = '';
		
			
			if(!empty($sCourseArr) && $sCourseArr[0] != -1){
					$extraSearch .= ' AND cdc.id IN ('.implode(",",$sCourseArr).')';
			}
			if(!empty($sclassesArr) && $sclassesArr[0] != -1){
				$extraSearch .= ' AND cdc.scheduler_id IN ('.implode(",",$sclassesArr).')';
			}

	
			
			if($sDateSelected && $eDateSelected){ 
			
				$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
                $sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
			   
				$extraSearch .= " AND ((cdc.class_startdate >= $sStartDateTime && cdc.class_startdate <= $sEndDateTime ) || (cdc.class_enddate >= $sStartDateTime && cdc.class_enddate <= $sEndDateTime ) )";
				$where .= " AND ((cdc.class_startdate >= $sStartDateTime && cdc.class_startdate <= $sEndDateTime ) || (cdc.class_enddate >= $sStartDateTime && cdc.class_enddate <= $sEndDateTime ) )";

			}elseif($sDateSelected && $eDateSelected == ''){ 
				$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
			    $extraSearch .= " AND (cdc.class_startdate >= ($sStartDateTime) || cdc.class_enddate >= ($sStartDateTime) )";
				$where .= " AND (cdc.class_startdate >= ($sStartDateTime) || cdc.class_enddate >= ($sStartDateTime) )";
				
			}elseif($sDateSelected =='' && $eDateSelected){ 
			   $sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
			   $extraSearch .= " AND (cdc.class_startdate <= ($sEndDateTime) || cdc.class_enddate <= ($sEndDateTime) )";
			   $where .= " AND (cdc.class_startdate <= ($sEndDateTime) || cdc.class_enddate <= ($sEndDateTime) )";
			}
			
		    $totalClassroomRecords = getClassesOfClassroomDetails($extraSearch); // do not want sTypeArr extra search here
			$overallEnrolledUserCount = isset($totalClassroomRecords['overall_enrolled_user_count'])?$totalClassroomRecords['overall_enrolled_user_count']:0;
			
			
			if(!empty($sTypeArr) && $sTypeArr[0] != -1 && count($sTypeArr) < 3){
			   
			   $extraSearchArr = array();
			   if(in_array(0, $sTypeArr)){
			     $extraSearchArr[] = '  (cdc.is_approved = 1 AND cdc.is_class_submitted = 1 AND (cdc.attended = 1 AND (cdc.is_completed <> 1 OR isnull(cdc.is_completed)))) ';
			   }
			   
			   if(in_array(1, $sTypeArr)){
			     $extraSearchArr[] = ' (cdc.is_approved = 1 AND cdc.is_class_submitted = 1 AND cdc.is_completed = 1) ';
			   }
			   
			   if(in_array(2, $sTypeArr)){
			     $extraSearchArr[] = ' (cdc.is_approved = 1 AND ( ( cdc.attended <> 1 AND (cdc.is_completed <> 1 OR isnull(cdc.is_completed)) AND cdc.is_class_submitted = 1) OR (cdc.is_class_submitted = 0) ))';
			   }
			   
			   if(count($extraSearchArr) > 0 ){
			        $extraSearch .= " AND (";
			        $extraSearch .= implode(" OR ", $extraSearchArr);
					$extraSearch .= " ) ";
					
			   }

			}
			//echo $extraSearch;die;

			
			$classesOfClassroom = getClassesOfClassroomDetails($extraSearch);
			$classesOfClassroom2 = $classesOfClassroom;
			//pr($classesOfClassroom);die;
			 //$query .= $whereCourseIn;
			
            $classroomCnt = count($classesOfClassroom['classroom']);
						
			
			$courseHTML = '';
			$style = !empty($export)?"style=''":'';
			$courseHTML .= "<div class='tabsOuter borderBlockSpace' ".$style.">";
			$courseHTML .= '<div class="clear"></div>';
            $exportHTML = '';
            if(empty($export)){
			
				ob_start();
				require_once($CFG->dirroot . '/local/includes/'.$CFG->pageMultiClassroomReportSearch);
				$SEARCHHTML = ob_get_contents();
				ob_end_clean();
				
				$courseHTML .= $SEARCHHTML;

				$exportHTML .= '<div class="exports_opt_box "> ';
				$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','classroomreport').'" href="'.$genPrintURL.'&action=exportcsv">'.get_string('downloadcsv','classroomreport').'</a> ';
				$exportHTML .= '<span class="seperater">&nbsp;</span>';
				$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','classroomreport').'" id="exportpdf1" href="'.$genPrintURL.'&action=exportpdf">'.get_string('downloadpdf','classroomreport').'</a> ';
				$exportHTML .= '<span class="seperater">&nbsp;</span>';
				$exportHTML .= '<a rel="'.$genPrintURL.'&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','classroomreport').'" class="print_icon">'.get_string('print','classroomreport').'</a>';
				$exportHTML .= '</div>';
			
			}
			
			$sDateSelectedForPrint = getDateFormatForPrint($sDateSelected);
			$eDateSelectedForPrint = getDateFormatForPrint($eDateSelected);
			$sDateSelectedForPrintCSV = getDateFormatForPrint($sDateSelected, 'csv');
			$eDateSelectedForPrintCSV = getDateFormatForPrint($eDateSelected, 'csv');
			
			if(!empty($export) && $export == 'print'){
				
				$reportName = get_string('courseusagesreport','classroomreport');
				$courseHTML .= getReportPrintHeader($reportName);
				
				$courseHTML .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" class="generaltable margin_bottom">';
					$courseHTML .= '<tr>';
										$courseHTML .= '<td width="500"><strong>'.get_string('course','classroomreport').':</strong> '.$sClassroomName.'</td>';
										$courseHTML .= '<td width="500"><strong>'.get_string('classname','classroomreport').':</strong>  '.$sClassName.'</td>';
					$courseHTML .= '</tr>';
					$courseHTML .= '<tr>';
										$courseHTML .= '<td colspan="2"><strong>'.get_string('status','classroomreport').':</strong> '.$sTypeName.'</td>';	
					$courseHTML .= '</tr>';
					$courseHTML .= '<tr>';
										$courseHTML .= '<td><strong>'.get_string('classstartfrom').':</strong> '.$sDateSelectedForPrint.'</td>';
										$courseHTML .= '<td><strong>'.get_string('classstartto').':</strong>  '.$eDateSelectedForPrint.'</td>';		
					$courseHTML .= '</tr>';
					
					
					
				$courseHTML .= '</table>';
				//$courseHTML .= '<span '.$CFG->pdfSpanAttribute.'>&nbsp;<br /><strong>'.get_string('coursereport','multicoursereport').'</strong></span><br /><br />';
		    }
			
			$courseHTML .= '<div class="userprofile view_assests">';
			
			$noshowCountGraph = 0;
			$incompleteCountGraph = 0;
			$completeCountGrpah = 0;
			$enrolledCountGraph = 0;
			
			if($classroomCnt > 0 && $overallEnrolledUserCount > 0 ){
			
				if(count($totalClassroomRecords['classroom']) > 0 ){
				
					foreach($totalClassroomRecords['classroom'] as $gcArr){

						if(isset($gcArr['class']) && count($gcArr['class'])>0){
							foreach($gcArr['class'] as $schedule){
								//$noshowCountGraph += $schedule->noshow_user_count?$schedule->noshow_user_count:0;
								$incompleteCountGraph += $schedule->inprogress_user_count?$schedule->inprogress_user_count:0;
								$completeCountGrpah += $schedule->completed_user_count?$schedule->completed_user_count:0;
								$enrolledCountGraph += $schedule->enrolled_user_count?$schedule->enrolled_user_count:0;
								
								$noshowCountGraph = $enrolledCountGraph - ($incompleteCountGraph+$completeCountGrpah);
							}
							
						}
		
									
					}
				}
				
				$nsgPercentage = $enrolledCountGraph?(numberFormat(($noshowCountGraph*100)/$enrolledCountGraph)):0;
				$icgPercentage = $enrolledCountGraph?(numberFormat(($incompleteCountGraph*100)/$enrolledCountGraph)):0;
				$ccgPercentage = $enrolledCountGraph?(numberFormat(($completeCountGrpah*100)/$enrolledCountGraph)):0;
				  
				if($enrolledCountGraph){					  
					$courseHTML .= '<div class = "single-report-start" id="watch">
						<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('classroomusagesreport','classroomreport').$exportHTML.'</span>
						  <div class="single-report-graph-left" id="chartContainer">'.get_string('loading','classroomreport').'</div>
						  <div class="single-report-graph-right">
							<div class="course-count-heading">'.get_string('totalassignedusers','classroomreport').'</div>
							<div class="course-count">'.$enrolledCountGraph.'</div>
							<div class="seperator">&nbsp;</div>
							<div class="course-status">
							  <div class="noshow"><h6>'.get_string('classnoshow','classroomreport').'</h6><span>'.$noshowCountGraph.'</span></div>
							  <div class="clear"></div>
							  <div class="inprogress"><h6>'.get_string('inprogress','classroomreport').'</h6><span>'.$incompleteCountGraph.'</span></div>
							  <div class="clear"></div>
							  <div class="completed"><h6>'.get_string('completed','classroomreport').'</h6><span>'.$completeCountGrpah.'</span></div>
							</div>
						  </div>
						</div>
					  </div>
					  <div class="clear"></div>';
				}	  

									  
			}
			
			
			$courseHTML .= '<div><div class=""><table cellspacing="0" cellpadding="0" border="0" width="100%" class="table1"><tbody>'; 
			
			$courseHTML .= '<tr>';
			$courseHTML .=  '<th width="40%">'.get_string('coursetitle','classroomreport').'</th>';
			$courseHTML .=  '<th width="15%">'.get_string('enrolled','classroomreport').'</th>';
			$courseHTML .=  '<th width="15%">'.get_string('classnoshow','classroomreport').'</th>';
			$courseHTML .=  '<th width="15%">'.get_string('inprogress','classroomreport').'</th>';
			$courseHTML .=  '<th width="15%">'.get_string('completed','classroomreport').'</th>';
			$courseHTML .=  '</tr>';

			$reportContentCSV .= get_string('course','classroomreport').",".$sClassroomName."\n".get_string('classname','classroomreport').",".$sClassName."\n";
			$reportContentCSV .= get_string('status','classroomreport').",".$sTypeName."\n";
			$reportContentCSV .= get_string('classstartfrom').",".$sDateSelectedForPrintCSV."\n".get_string('classstartto').",".$eDateSelectedForPrintCSV."\n";
			$reportContentCSV .= get_string('enrolled','multicoursereport').",".$enrolledCountGraph."\n";
			$reportContentCSV .= get_string('classnoshow','classroomreport').",".$noshowCountGraph." (".floatval($nsgPercentage)."%)\n";
			$reportContentCSV .= get_string('inprogress','classroomreport').",".$incompleteCountGraph." (".floatval($icgPercentage)."%)\n";
			$reportContentCSV .= get_string('completed','multicoursereport').",".$completeCountGrpah." (".floatval($ccgPercentage)."%)\n";
			$reportContentCSV .= get_string('classroomusagesreport','classroomreport')."\n";
			
			$reportContentCSV .= get_string('coursetitle','classroomreport').",".get_string('totalassignedusers','classroomreport').",".get_string('inprogress','classroomreport').",".get_string('completed','classroomreport')."\n";

			$reportContentPDF = '';
				
			$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" width="100%">';
				$reportContentPDF .= '<tr>';
									$reportContentPDF .= '<td width="50%" ><strong>'.get_string('course','classroomreport').':</strong> '.$sClassroomName.'</td>';
									$reportContentPDF .= '<td width="50%"><strong>'.get_string('classname','classroomreport').':</strong>  '.$sClassName.'</td>';
				$reportContentPDF .= '</tr>';
				
				$reportContentPDF .= '<tr>';
										$reportContentPDF .= '<td colspan="2"><strong>'.get_string('status','classroomreport').':</strong> '.$sTypeName.'</td>';	
				$reportContentPDF .= '</tr>';
					
				$reportContentPDF .= '<tr>';
									$reportContentPDF .= '<td width="50%"><strong>'.get_string('classstartfrom').':</strong> '.$sDateSelectedForPrint.'</td>';
									$reportContentPDF .= '<td width="50%"><strong>'.get_string('classstartto').':</strong>  '.$eDateSelectedForPrint.'</td>';		
				$reportContentPDF .= '</tr>';
				$reportContentPDF .= '<tr>';
									$reportContentPDF .= '<td width="50%"><strong>'.get_string('totalassignedusers','multicoursereport').':</strong> '.$enrolledCountGraph.'</td>';
									$reportContentPDF .= '<td width="50%"><strong>'.get_string('classnoshow','classroomreport').':</strong> '.$noshowCountGraph.' ('.floatval($nsgPercentage).'%)</td>';		 
				$reportContentPDF .= '</tr>';
				$reportContentPDF .= '<tr>';
				                $reportContentPDF .= '<td width="50%"><strong>'.get_string('inprogress','classroomreport').':</strong>  '.$incompleteCountGraph.' ('.floatval($icgPercentage).'%)</td>';	
				                $reportContentPDF .= '<td width="50%"><strong>'.get_string('completed','multicoursereport').':</strong> '.$completeCountGrpah.' ('.floatval($ccgPercentage).'%)</td>';	
				                      
				$reportContentPDF .= '</tr>';
				
		   //$reportContentPDF .= '<tr><br /><td width="100%"  ><span '.$CFG->pdfSpanAttribute.'><strong>'.get_string('classroomusagesreport','classroomreport').'</strong></span><br /></td></tr>';
			$reportContentPDF .= '</table>';
			

			$reportContentPDF .= getGraphImageHTML(get_string('classroomusagesreport','classroomreport'));
				
				
			// $reportContentPDF .= '<span '.$CFG->pdfMainHeadingFontStyle.'>&nbsp;<br /><strong>'.get_string('classroomusagesreport','classroomreport').'</strong></span><br /><br />';
			 $reportContentPDF .= '<table '.$CFG->pdfTableStyle.'>';

			 $reportContentPDF .= '<tr '.$CFG->pdfTableHeaderStyle.'>';
			 $reportContentPDF .= '<td><strong>'.get_string('coursetitle','classroomreport').'</strong></td>';
			 $reportContentPDF .= '<td><strong> '.get_string('enrolled','classroomreport').'</strong></td>';
			 $reportContentPDF .= '<td><strong>'.get_string('classnoshow','classroomreport').'</strong></td>';
			 $reportContentPDF .= '<td><strong>'.get_string('inprogress','classroomreport').'</strong></td>';
			 $reportContentPDF .= '<td><strong>'.get_string('completed','classroomreport').'</strong></td>';
			
			 $reportContentPDF .= '</tr>';
			
			
			
			
			if($classroomCnt > 0 ){
			  	
			  if($perpage){
				$offset = $page - 1;
				$offset = $offset*$perpage;
				$allClassroomArr = array_splice($classesOfClassroom2['classroom'], $offset, $perpage);
			  }else{
				$allClassroomArr = $classesOfClassroom['classroom'];
			  }
					  
			  $i = 0;
			  
			
			  foreach($allClassroomArr as $courses){
					$i++;
					//$classEvenOddR = $i%2==0 ?'evenR':'oddR';
					//$pdfStyleEvenOddR = $i%2==0 ?$CFG->pdfTableEvenRowStyle:$CFG->pdfTableOddRowStyle;
					$pdfStyleEvenOddR = $CFG->pdfTableEvenRowStyle;
				   
					$classEvenOddR = 'evenR';				 
					$courseId = $courses['course_id'];
					$courseName = $courses['course_name'];
					$courseHTML .=  '<tr class="'.$classEvenOddR.'" >';
					if(!empty($export)){
					   $courseHTML .=  '<td>'.$courseName.'</td>';
					}else{
					  $courseHTML .=  '<td><a href="'.$CFG->wwwroot.'/course/'.$CFG->pageMultiClassroomReportDetails.'?cid='.$courseId.'">'.$courseName.'</a></td>';
					}
					$courseHTML .=  '<td></td>';
					$courseHTML .=  '<td></td>';
					$courseHTML .=  '<td></td>';
					$courseHTML .=  '<td></td>';
					$courseHTML .=  '</tr>';
					
					$reportContentCSV .= $courseName.",,,\n";
					$reportContentPDF .=  '<tr '.$pdfStyleEvenOddR.'>';
					$reportContentPDF .=  '<td colspan="5">'.$courseName.'</td>';
					$reportContentPDF .=  '</tr>';	
					
					
					$courseHTML .=  '<tr><td colspan="5" class="borderLeftRight">';
						$courseHTML .=  '<div id="abox'.$courseId.'" class="a-box">';
					$totalEnrolledCount = 0;	
					if(isset($courses['class']) && count($courses['class'])>0){
						$j = 0;
						
						foreach($courses['class'] as $schedule){
							$j++;
							$classId = $schedule->scheduler_id;
							$className = $schedule->classname;
							$incompleteCount = $schedule->inprogress_user_count?$schedule->inprogress_user_count:0;
							$completeCount = $schedule->completed_user_count?$schedule->completed_user_count:0;
							//$noshowCount = $schedule->noshow_user_count?$schedule->noshow_user_count:0;
							$enrolledCount = $schedule->enrolled_user_count?$schedule->enrolled_user_count:0;
							
							$noshowCount = $enrolledCount - ($incompleteCount+$completeCount);
							 							
							$classEvenOdd = $j%2==0 ?'even':'odd';
							$pdfStyleEvenOddClass = $j%2==0 ?$CFG->pdfInnerTableOddRowStyle:$CFG->pdfInnerTableEvenRowStyle;
							$courseHTML .=  '<div class="'.$classEvenOdd.'">';
								if(!empty($export)){
								  $courseHTML .= '<div style="width:">'.$className.'</div>';
								}else{
								   $courseHTML .= '<div style="width:40%"><a href="'.$CFG->wwwroot.'/course/'.$CFG->pageMultiClassroomReportDetails.'?cid='.$courseId.'&class_id='.$classId.'" >'.$className.'</a></div>';
								}
											
											$courseHTML .= '<div style="width:15%">'.$enrolledCount.'</div>';
											$courseHTML .= '<div style="width:15%">'.$noshowCount.'</div>';
											$courseHTML .= '<div style="width:15%">'.$incompleteCount.'</div>';
											$courseHTML .= '<div style="width:15%">'.$completeCount.'</div>';
											
									$courseHTML .= '</div>';
										
							$reportContentCSV .= getSpace('csv').$className.",".$enrolledCount.",".$noshowCount.",".$incompleteCount.",".$completeCount."\n";

							$reportContentPDF .=  '<tr '.$pdfStyleEvenOddClass.'>';
							$reportContentPDF .=  '<td>'.getSpace().$className.'</td>';
							$reportContentPDF .=  '<td>'.$enrolledCount.'</td>';
							$reportContentPDF .=  '<td>'.$noshowCount.'</td>';
							$reportContentPDF .=  '<td>'.$incompleteCount.'</td>';
							$reportContentPDF .=  '<td>'.$completeCount.'</td>';
							
							$reportContentPDF .=  '</tr>';
			
						}
						
					}else{
						$courseHTML .= get_string('no_results');
						$reportContentCSV .= get_string('no_results')."\n";
						$reportContentPDF .=  '<tr><td colspan = "5">'.get_string('no_results').'</td></tr>';
					}

					
					$courseHTML .=  '</div>';
					$courseHTML .=  '</td></tr>';
				}
				
				
				
				if($enrolledCountGraph){
					$courseHTML .=  '<script language="javascript" type="text/javascript">';
					$courseHTML .=  '	$(document).ready(function(){
									
													$("#printbun").bind("click", function(e) {
														var url = $(this).attr("rel");
														window.open(url, "'.get_string('classroomusagesreport','classroomreport').'", "'.$CFG->printWindowParameter.'");
													});
									
										 }); ';
	
					$courseHTML .=  ' window.onload = function () {
						var chart = new CanvasJS.Chart("chartContainer", {
								theme: "theme2",//theme1, theme2
								title:{
									  text: ""    },
								data: [
										{
										  // Change type to "bar", "splineArea", "area", "spline", "pie","column",  "doughnut" etc.
										  type: "doughnut",
										  indexLabelFontFamily: "Arial",       
										  indexLabelFontSize: 12,
										  startAngle:0,
										  indexLabelFontColor: "dimgrey",       
										  indexLabelLineColor: "darkgrey", 
										  toolTipContent: "{y}%",
										  //indexLabelPlacement: "inside",
										  '; 	

									
									     $courseHTML .=  '		dataPoints: [ ';					  
										  $IPExploded = count($sTypeArr) > 0 && in_array(0, $sTypeArr)?true:false;
					                      $compExploded = count($sTypeArr) > 0 && in_array(1, $sTypeArr)?true:false;
										  $noshowExploded = count($sTypeArr) > 0 && in_array(2, $sTypeArr)?true:false;
										 
										  $graphtLabelHtml .= '{  y: '.$nsgPercentage.', label: "'.get_string('classnoshow','classroomreport').' ('.$noshowCountGraph.')", exploded: "'.$noshowExploded.'" },';	 
										  $graphtLabelHtml .= '{  y: '.$icgPercentage.', label: "'.get_string('inprogress','classroomreport').' ('.$incompleteCountGraph.')", exploded: "'.$IPExploded.'" },';
										  $graphtLabelHtml .= '{  y: '.$ccgPercentage.', label: "'.get_string('completed','classroomreport').' ('.$completeCountGrpah.')", exploded: "'.$compExploded.'" },';	 
										 
										  $courseHTML .= 	$graphtLabelHtml;		
										  $courseHTML .=  '  ]
						  }
						  ]
					 });';
						
							//if($inProgressPercentage || $completedPercentage){					
							  $courseHTML .=  '	chart.render(); ';
							//}
													 
							$courseHTML .=  '  }
										</script>
										<script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js"></script>';
				}						
				
			}else{
			      $courseHTML .=  '<tr><td colspan="5" > '.get_string('norecordfound','singlecoursereport').'</td></tr>';
			}
			
			$courseHTML .= '</tbody></table></div></div></div></div>';
	
            if(empty($export)){ 
				$courseHTML .= paging_bar($classroomCnt, $page, $perpage, $genURL);
				//$courseHTML .= '<input type = "button" value = "'.get_string('back','classroomreport').'" onclick="location.href=\''.$CFG->wwwroot.'/my/adminreportdashboard.php\';">';   
				   }
			$courseHTML .= '';
			$reportContentPDF .= '</table>';
			
			$classroomUsagesReport->courseHTML = $courseHTML;
			$classroomUsagesReport->reportContentCSV = $reportContentCSV;
			$classroomUsagesReport->reportContentPDF = $reportContentPDF;
			//echo $enrollmentPercentage11;die;
			return $classroomUsagesReport;

      }
	
	  
	  /**
	 * get class name or all results
	 *
	 * @param mixed $classId is course id
	 * @param string $sort is sorting field
	 * @param string $dir is sorting order
	 * @return string $result class name or all results (if $classId is null)
	 */
	 
	 
	function getClasses($classId='', $sort='name', $dir = 'ASC'){
	
		global $DB, $CFG;
		$result = '';

		if(is_array($classId)){
		  $classIds = count($classId) > 0 ? implode(",",$classId) : 0;
		  if($classIds){
		      $result = $DB->get_field_sql("select GROUP_CONCAT(name ORDER BY name ASC SEPARATOR ', ' ) FROM {$CFG->prefix}scheduler WHERE id IN (".$classIds.") ");
		  }
		}else{
			if($classId){
			   $result = $DB->get_field_sql("select name FROM {$CFG->prefix}scheduler  WHERE id IN (".$classId.") ORDER BY $sort $dir ");
			}else{
			   $result = $DB->get_records_sql("select * FROM {$CFG->prefix}scheduler  WHERE isactive = '1'  ORDER BY $sort $dir ");
			}
        }
		
		return $result;
	}
	
	
      function getDeclinedUsersReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export=''){   
      
       	global $DB, $CFG, $USER;
      	$classroomUsagesReport = new stdClass();
      	$isReport = true;
      		
      	$offset = $page - 1;
      	$offset = $offset*$perpage;
      	$limit = '';
      	if($perpage != 0){
      		$limit = " LIMIT $offset, $perpage";
      	}
      
      	////// Getting common URL for the Search //////
      	$pageURL = $_SERVER['PHP_SELF'];
      	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
      		
      	$printUrl = '/course/'.$CFG->pageDeclinedLearnerReportPrint;
      	$genPrintURL = genParameterizedURL($paramArray, $removeKeyArray, $printUrl);
      	$sCourse        = $paramArray['course'];
      	$sclasses        = $paramArray['classes'];
      	
      	$sType          = $paramArray['type'];
      	$sStartDate     = $paramArray['startDate'];
      	$sEndDate       = $paramArray['endDate'];
      	$sStatus		= $paramArray['status'];
      	$sDateSelected = '';
      	if($sStartDate){
      		list($month1, $day1, $year1) = explode('-', $sStartDate);
      		$sDateSelected = date("m/d/Y",mktime(0, 0, 0, $month1, $day1, $year1));
      	}
      		
      	$eDateSelected = '';
      	if($sEndDate){
      		list($month2, $day2, $year2) = explode('-', $sEndDate);
      		$eDateSelected = date("m/d/Y",mktime(0, 0, 0, $month2, $day2, $year2));
      	}
      
      
      	$sCourseArr = explode("@",$sCourse);
      	$sclassesArr = explode("@",$sclasses);
      	$sTypeArr = explode("@",$sType);
      	$sStatusArr = explode("@",$sStatus);
      	$whereClassroomIn = '';
      	$whereCourseIn = '';
      	$whereClassIn = '';
      	$whereCourseInchk = '';
      	$whereClassInchk = '';
      	$statusWhere = '';
		
		$classroomId = 0;
		$classId  = 0;
		$classroomArr = getClassroomCourseListingResult();
		if(count($classroomArr) > 0 ){
		 $cKArr = array_keys($classroomArr);
		 $classroomId = $cKArr[0];
		 
		}
		
		if($classroomId){
		  $classArr = getClassesOfClassroom($classroomId);
		  if(count($classArr) > 0 ){
			 $clKArr = array_keys($classArr);
			 $classId = $clKArr[0];
			
		  }
		 
		}

		
      		
		if(!empty($sclassesArr) && $sclassesArr[0] != -1){
			
			$whereCourseIn .= ' AND se.scheduler_id IN ('.implode(",",$sclassesArr).')';
			$classIdForHeader = implode(",",$sclassesArr);
		}else{
		   $whereCourseIn .= ' AND se.scheduler_id IN ('.$classId.')';
		   $classIdForHeader = $classId;
		}
		
      	if($sType == '-1'){
      		$sTypeName = get_string('all','classroomreport');
      	}else{
      			
      		$repTypeArr = $CFG->courseStatusArr;
      		$sTypeArr2 = str_replace(array(3, 1, 2), $repTypeArr, $sTypeArr);
      		$sTypeName = implode(", ", $sTypeArr2);
      	}
		
      	if($sStatusArr[0]!='-1'){
      		$statusWhere .= ' AND se.is_approved IN ('.implode(",",$sStatusArr).')';
			$sTypeStr = isset($sStatusArr) && $sStatusArr[0] != '' && count($sStatusArr) > 0 ? implode(", ", $sStatusArr):get_string('all','classroomreport');
			$sStatusName = str_replace(array(3,0,1,2), array(get_string('totalinviteduser','classroomreport'), get_string('managerforapproval','classroomreport'), get_string('approveduser','classroomreport'), get_string('declineduser','classroomreport')), $sTypeStr);
      	}else{
		  $sStatusName = get_string('all','classroomreport');
		}
      	// search condition start here
      		
      	$searchString = "";
      		
      		
      	if($sDateSelected){
      		list($month, $day, $year) = explode('/', $sDateSelected);
      		$sStartDateTime =  mktime(0, 0, 0, $month, $day, $year);
      		$searchString .= " AND s.startdate >= ($sStartDateTime) ";
      	}
      		
      	if($eDateSelected){
      		list($month, $day, $year) = explode('/', $eDateSelected);
      		$sEndDateTime = mktime(0, 0, 0, $month, $day, $year);
      		$searchString .= " AND s.enddate <= ($sEndDateTime) ";
      	}
      	 $query = "	SELECT se.id,u.id,u.firstname,se.request_date,se.response_date,u.lastname,se.is_approved, concat(u.firstname, ' ', u.lastname) as fullname, u.username 
						FROM mdl_scheduler_enrollment AS se 
						LEFT JOIN mdl_user u ON (se.userid = u.id)
						WHERE 1=1 "; 
		$query .= " AND u.deleted = '0' ";
		$query .= $whereCourseIn;   	

      		
      	//$query .= $searchString;
      	//$query .= " AND mc.createdby = '".$USER->id."'";    
		
		
		$graphQuery = $query." ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname";
		$query .= $statusWhere;
		$queryLimit = $query.$limit;
		$query .= "  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ";
      	$classAllArr = $DB->get_records_sql($query);
      
      	$classCount = count($classAllArr);
      	$classArr = $DB->get_records_sql($queryLimit);
      	//pr($classArr);die;
      	$courseIDsCheck = array();
      	
		$classAllForGraphArr = $DB->get_records_sql($graphQuery);
     	$typeOfUsers = typeOfUser($classAllForGraphArr);
   
   //pr($typeOfUsers);die;
      	$courseHTML = '';
      	$style = !empty($export)?"style=''":'';
      	$courseHTML .= "<div class='tabsOuter borderBlockSpace' ".$style.">";
      	$courseHTML .= '<div class="clear"></div>';
      	$exportHTML = '';
      	$reportContentCSV= '';
      	$reportContentPDF= '';
      	if(empty($export)){
      			
      		ob_start();
      		require_once($CFG->dirroot . '/local/includes/'.$CFG->declinedLearnerClassroomReportSearch); 
      		$SEARCHHTML = ob_get_contents();
      		ob_end_clean();
      
      		$courseHTML .= $SEARCHHTML;
            
			if(count($classArr) > 0 ){
				$exportHTML .= '<div class="exports_opt_box "> ';
				$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','classroomreport').'" href="'.$genPrintURL.'&action=exportcsv">'.get_string('downloadcsv','classroomreport').'</a> ';
				$exportHTML .= '<span class="seperater">&nbsp;</span>';
				$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','classroomreport').'" id="exportpdf1" href="'.$genPrintURL.'&action=exportpdf">'.get_string('downloadpdf','classroomreport').'</a> ';
				$exportHTML .= '<span class="seperater">&nbsp;</span>';
				$exportHTML .= '<a rel="'.$genPrintURL.'&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','classroomreport').'" class="print_icon">'.get_string('print','classroomreport').'</a>';
				$exportHTML .= '</div>';
			}
      			
      	}
      		
      	$courseHTML .= '<div class="userprofile view_assests">';
      			
        $graphCnt = count($classAllForGraphArr);
		
		
		if($classIdForHeader!=0){
			$class_details = getClassDetailsFromModuleId($classIdForHeader);
		  
			//$headerHtml = getModuleHeaderHtml($class_details, $CFG->classModule);
			$instructorName = fullname(getClassInstructor($class_details));
			$classDuration = getClassDuration($class_details->startdate, $class_details->enddate);
			$classDurationCSV = getClassDuration($class_details->startdate, $class_details->enddate, 'csv');
		}	
	
	    if(!empty($export) && $export == 'print'){
			$courseHTML .= '<table border="0" cellpadding="0" cellspacing="0" width="100%">';
				$courseHTML .= '<tr>';
						$courseHTML .= '<td width="50%" ><strong>'.get_string('course','classroomreport').':</strong> '.$class_details->classroomname.'</td>';
						$courseHTML .= '<td width="50%" ><strong>'.get_string('name','scheduler').':</strong>  '.$class_details->classname.'</td>';
				$courseHTML .= '</tr>';
				
				$courseHTML .= '<tr>';
						$courseHTML .= '<td width="100%" colspan="2" ><strong>'.get_string('status','multicoursereport').':</strong> '.$sStatusName.'</td>';
				$courseHTML .= '</tr>';
				
			

			 $courseHTML .= '</table>';
		}
      	if(count($classAllArr) > 0 ){
      		$reportContentPDF .= "";
         	$courseHTML .= '<div class = "single-report-start">
										<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('classrequestreport','classroomreport').$exportHTML.'</span>
										  <div class="single-report-graph-left" id="chartContainer">'.get_string('loading','multicoursereport').'</div>
										  <div class="single-report-graph-right">
											<div class="course-count-heading">'.get_string('numberofusers','multicoursereport').'</div>
											<div class="course-count">'.$graphCnt.'</div>
											<div class="seperator">&nbsp;</div>
											<div class="course-status">													
											  <div class="inviteduser"><h6>'.get_string('totalinviteduser','classroomreport').'</h6><span>'.$typeOfUsers["invited"].'</span></div>
											  <div class="clear"></div>
											  <div class="watingforapproval"><h6>'.get_string('managerforapproval','classroomreport').'</h6><span>'.$typeOfUsers["wating"].'</span></div>
											  <div class="clear"></div>
											  <div class="approveduser"><h6>'.get_string('approveduser','classroomreport').'</h6><span>'.$typeOfUsers["approved"].'</span></div>
											  <div class="clear"></div>
											  <div class="declineduser"><h6>'.get_string('declineduser','classroomreport').'</h6><span>'.$typeOfUsers["declined"].'</span></div>
											</div>
										  </div>
										</div>
									  </div>
									  <div class="clear"></div>';
      	}
      	
      	//if($classIdForHeader!=0){
						
			//$courseHTML .= $headerHtml;
			
			$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" width="100%">';
				$reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td width="50%" ><strong>'.get_string('course','classroomreport').':</strong> '.$class_details->classroomname.'</td>';
						$reportContentPDF .= '<td width="50%" ><strong>'.get_string('name','scheduler').':</strong>  '.$class_details->classname.'</td>';
				$reportContentPDF .= '</tr>';
	
				$reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td width="50%" ><strong>'.get_string('status','multicoursereport').':</strong> '.$sStatusName.'</td>';
						$reportContentPDF .= '<td width="50%" ><strong>'.get_string('numberofusers','multicoursereport').':</strong>  '.$graphCnt.'</td>';
				$reportContentPDF .= '</tr>';

			    $reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td width="50%" ><strong>'.get_string('totalinviteduser','classroomreport').':</strong> '.$typeOfUsers["invited"].' ('.floatval($typeOfUsers["invitedper"]).'%)</td>';
						$reportContentPDF .= '<td width="50%" ><strong>'.get_string('managerforapproval','classroomreport').':</strong>  '.$typeOfUsers["wating"].' ('.floatval($typeOfUsers["watingper"]).'%)</td>';
				$reportContentPDF .= '</tr>';
				
				$reportContentPDF .= '<tr>';
						$reportContentPDF .= '<td width="50%" ><strong>'.get_string('approveduser','classroomreport').':</strong> '.$typeOfUsers["approved"].' ('.floatval($typeOfUsers["approvedper"]).'%)</td>';
						$reportContentPDF .= '<td width="50%" ><strong>'.get_string('declineduser','classroomreport').':</strong>  '.$typeOfUsers["declined"].' ('.floatval($typeOfUsers["declinedper"]).'%)</td>';
				$reportContentPDF .= '</tr>';
	
				$reportContentPDF .= '<tr><br /><td width="100%"  ><span '.$CFG->pdfSpanAttribute.'><strong>'.get_string('classrequestreport','classroomreport').'</strong></span><br /></td></tr>';	
			 $reportContentPDF .= '</table>';

												
			$reportContentCSV = '';
			$reportContentCSV .= get_string('course','classroomreport').",".$class_details->classroomname."\n".get_string('name','scheduler').",".$class_details->classname."\n";
			//$reportContentCSV .= get_string('instructor','scheduler').",".$instructorName."\n".get_string('duration','scheduler').",".$classDurationCSV."\n";
			$reportContentCSV .= get_string('status','multicoursereport').",".$sStatusName."\n".get_string('numberofusers','multicoursereport').",".$graphCnt."\n";
			$reportContentCSV .= get_string('totalinviteduser','classroomreport').",".$typeOfUsers["invited"]." (".floatval($typeOfUsers["invitedper"])."%)\n".get_string('managerforapproval','classroomreport').",".$typeOfUsers["wating"]." (".floatval($typeOfUsers["watingper"]).")% \n";
			$reportContentCSV .= get_string('approveduser','classroomreport').",".$typeOfUsers["approved"]." (".floatval($typeOfUsers["approvedper"]).")\n".get_string('declineduser','classroomreport').",".$typeOfUsers["declined"]." (".floatval($typeOfUsers["declinedper"])."%)\n";
			$reportContentCSV .= get_string('classrequestreport','classroomreport')."\n";
			
			
			
      	//}
      	
      	
      	//$reportContentPDF .=$headerHtml;
      	
      	$courseHTML .= '<div><div class=""><table cellspacing="0" cellpadding="0" border="0" width="100%" class="generaltable"><tbody>';
      	$reportContentPDF .='<div><div class="borderBlock borderBlockSpace"><table cellspacing="0" cellpadding="0" border="0" width="100%"  class="table1"><tbody>';
      	$courseHTML .= '<tr class="table1_head">';
      	$courseHTML .=  '<th width="22%">'.get_string('name').'</th>';
		$courseHTML .=  '<th width="23%">'.get_string('username').'</th>';
      	$courseHTML .=  '<th width="20%">'.get_string('requestdate').'</th>';
      	$courseHTML .=  '<th width="20%">'.get_string('responsedate').'</th>';
      	$courseHTML .=  '<th width="15%">'.get_string('status').'</th>';
      	$courseHTML .=  '</tr>';
		$reportContentPDF .= '<table '.$CFG->pdfTableStyle.' >';
		$reportContentPDF .= '<tr '.$CFG->pdfTableHeaderStyle.'>';
      	$reportContentPDF .=  '<td><strong>'.get_string('name').'</strong></td>';
		$reportContentPDF .=  '<td><strong>'.get_string('username').'</strong></td>';
      	$reportContentPDF .=  '<td><strong>'.get_string('requestdate').'</strong></td>';
      	$reportContentPDF .=  '<td><strong>'.get_string('responsedate').'</strong></td>';
      	$reportContentPDF .=  '<td><strong>'.get_string('status').'</strong></td>';
      	$reportContentPDF .=  '</tr>';
      	$reportContentCSV .= get_string('name').",".get_string('username').",".get_string('requestdate').",".get_string('responsedate').",".get_string('status')."\n";
      
      
      	if(count($classArr) > 0 ){
      		$i = 0;
      		foreach($classArr as $classRec){
      			$request_date = '';
      			$response_date = '';
      			
      			
      			if(in_array($classRec->is_approved,array(0,1,2,3))){
      				$request_date = getDateFormat($classRec->request_date, $CFG->customDefaultDateFormat);
      				$request_date_exl = getDateFormat($classRec->request_date, $CFG->customDefaultDateFormatForCSV);
      				
      			}
      			if(in_array($classRec->is_approved,array(1,2))){
      				$response_date =getDateFormat($classRec->response_date, $CFG->customDefaultDateFormat);
      				$response_date_exl = getDateFormat($classRec->response_date, $CFG->customDefaultDateFormatForCSV);
      			}
      			else{
      				$response_date = getMDash();
					$response_date_exl = getMDashForCSV();
      			}
      			
      			$courseHTML .= '<tr class="printRowSec">';
      			$courseHTML .=  '<td>'.$classRec->fullname.'</td>';
				$courseHTML .=  '<td>'.$classRec->username.'</td>';
      			$courseHTML .=  '<td>'.$request_date.'</td>';
      			$courseHTML .=  '<td>'.$response_date.'</td>';
      			$courseHTML .=  '<td>'.getUserStatusInClass($classRec->is_approved).'</td>';
      			$courseHTML .=  '</tr>';
      			
      			$reportContentPDF .='<tr '.$CFG->pdfTableRowAttribute.' >';
      			$reportContentPDF .=  '<td>'.$classRec->fullname.'</td>';
				$reportContentPDF .=  '<td>'.$classRec->username.'</td>';
      			$reportContentPDF .=  '<td>'.$request_date.'</td>';
      			$reportContentPDF .=  '<td>'.$response_date.'</td>';
      			$reportContentPDF .=  '<td>'.getUserStatusInClass($classRec->is_approved).'</td>';
      			$reportContentPDF .=  '</tr>';
      			$reportContentCSV .= $classRec->fullname.",".$classRec->username.",".$request_date_exl.",".$response_date_exl.",".getUserStatusInClass($classRec->is_approved)."\n";
      			
      		}
      		
      		
      
      	}else{
      		$courseHTML .=  '<tr><td colspan="5" > '.get_string('norecordfound','singlecoursereport').'</td></tr>';
      		$reportContentPDF .= get_string('norecordfound','singlecoursereport');
      	}
      	$courseHTML .=  '<script language="javascript" type="text/javascript">';
      	$courseHTML .=  '	$(document).ready(function(){
      	
												$("#printbun").bind("click", function(e) {
													var url = $(this).attr("rel");
													window.open(url, "'.get_string('classroomusagesreport','classroomreport').'", "'.$CFG->printWindowParameter.'");
												});
      	
						             }); ';
      	
      	$courseHTML .=  ' window.onload = function () {
					var chart = new CanvasJS.Chart("chartContainer", {
							theme: "theme2",//theme1
							title:{
								  text: ""         },
							data: [
									{
									  // Change type to "bar", "splineArea", "area", "spline", "pie","column",  "doughnut" etc.
									  type: "doughnut",
									  indexLabelFontFamily: "Arial",
									  indexLabelFontSize: 12,
									  startAngle:0,
									  indexLabelFontColor: "dimgrey",
									  indexLabelLineColor: "darkgrey",
									  toolTipContent: "{y}%",
									  //indexLabelPlacement: "inside",
									  ';
									  
									  if($sStatusArr[0]!='-1'){
      		$statusWhere .= ' AND se.is_approved IN ('.implode(",",$sStatusArr).')';
      	}
		
      	$courseHTML .=  '		dataPoints: [ {  y: '.$typeOfUsers["invitedper"].', label: "'.get_string('totalinviteduser','classroomreport').' ('.$typeOfUsers["invited"].')", exploded: '.((in_array(3, $sStatusArr) )?'true':'false').' },
												{  y: '.$typeOfUsers["watingper"].', label: "'.get_string('managerforapproval','classroomreport').' ('.$typeOfUsers["wating"].')", exploded: '.((in_array(0, $sStatusArr) )?'true':'false').'  },
												{  y: '.$typeOfUsers["approvedper"].', label: "'.get_string('approveduser','classroomreport').' ('.$typeOfUsers["approved"].')", exploded: '.((in_array(1, $sStatusArr) )?'true':'false').'  },
												{  y: '.$typeOfUsers["declinedper"].', label: "'.get_string('declineduser','classroomreport').' ('.$typeOfUsers["declined"].')", exploded: '.((in_array(2, $sStatusArr) )?'true':'false').'  }
									]
					  }
					  ]
				 });';
      	
      	//if($inProgressPercentage || $completedPercentage){
      	$courseHTML .=  '	chart.render(); ';
      	//}
      	 
      	$courseHTML .=  '  }
									</script>
									<script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js"></script>';
      	$courseHTML .= '</tbody></table></div></div></div></div>';
      	$reportContentPDF .= '</tbody></table></div></div>';
      
      	if(empty($export) && count($classArr) > 0) {
      		$courseHTML .= paging_bar($classCount, $page, $perpage, $genURL);
      		//$courseHTML .= '<input type = "button" value = "'.get_string('back','classroomreport').'" onclick="location.href=\''.$CFG->wwwroot.'/my/adminreportdashboard.php\';">';
      	}
      	$courseHTML .= '';
      		
      	$classroomUsagesReport->courseHTML = $courseHTML;
      	$classroomUsagesReport->reportContentCSV = $reportContentCSV;
      	$classroomUsagesReport->reportContentPDF = $reportContentPDF;
      	return $classroomUsagesReport;
      
      }
      
 function typeOfUser($allRec){
 	$invitedUser = 0;
 	$approvedUser = 0;
 	$watingUser = 0;
 	$declinedUser = 0;
 	$totalClasses = count($allRec);
 	if($totalClasses>0){
	 	foreach($allRec as $rec){
		 	if($rec->is_approved==0){
		 		$watingUser = $watingUser+1;
		 	}
		 	if($rec->is_approved==1){
		 		$approvedUser = $approvedUser+1;
		 	}
		 	if($rec->is_approved==2){
		 		$declinedUser = $declinedUser+1;
		 	}
		 	if($rec->is_approved==3){
		 		$invitedUser = $invitedUser+1;
		 	}
	 		
	 	}
 	}
 	
	 $userStatus = array();
	 $userStatus['invited'] = $invitedUser;
	 $userStatus['wating'] = $watingUser;
	 $userStatus['approved'] = $approvedUser;
	 $userStatus['declined'] = $declinedUser;
	 $userStatus['invitedper'] = numberFormat(($invitedUser/$totalClasses)*100);
	 $userStatus['watingper'] = numberFormat(($watingUser/$totalClasses)*100);
	 $userStatus['approvedper'] = numberFormat(($approvedUser/$totalClasses)*100);
	 $userStatus['declinedper'] =numberFormat(($declinedUser/$totalClasses)*100);
	 return $userStatus;
 }
 
 function getUserStatusInClass($status){
 	$statusText = '';
 	if($status==3){
 		$statusText = get_string('totalinviteduser','classroomreport');
 	}
 	if($status==1){
 		$statusText = get_string('approveduser','classroomreport');
 	}
 	if($status==2){
 		$statusText = get_string('declineduser','classroomreport');
 	}
 	if($status==0){
 		$statusText = get_string('managerforapproval','classroomreport');
 	}
 	return $statusText;
 }
 
 
 function getClassroomCourseListingResult(){
 
		global $USER,$CFG,$DB;

		
		$classroomInstructorWhere = " AND c.coursetype_id = '".$CFG->courseTypeClassroom."' AND c.id IN (0) AND c.deleted = '0' ";
		$classroomSecondaryInstructorWhere = " AND c.coursetype_id = '".$CFG->courseTypeClassroom."' AND c.id IN (0) AND c.deleted = '0' ";
        $instructorCourses = getInstructorClassroom($USER->id); 
		$secondaryInstructorCourses = getSecondaryInstructorClassroom($USER->id); 
		// pr($secondaryInstructorCourses);die;
		
		$instructorCoursesIds = 0;
		if(!empty($instructorCourses) && count($instructorCourses) > 0 ){
		   $instructorCoursesIdArr = array_keys($instructorCourses);
		   $instructorCoursesIds = implode(",", $instructorCoursesIdArr);
		   $classroomInstructorWhere = " AND c.coursetype_id = '".$CFG->courseTypeClassroom."' AND c.id IN (".$instructorCoursesIds.") AND c.deleted = '0' ";
		}
		
		$secondaryInstructorCoursesIds = 0;
		if(!empty($secondaryInstructorCourses) && count($secondaryInstructorCourses) > 0 ){
		   $secondaryInstructorCoursesIdArr = array_keys($secondaryInstructorCourses);
		   $secondaryInstructorCoursesIds = implode(",", $secondaryInstructorCoursesIdArr);
		   $classroomSecondaryInstructorWhere = " AND c.coursetype_id = '".$CFG->courseTypeClassroom."' AND c.id IN (".$secondaryInstructorCoursesIds.") AND c.deleted = '0' ";
		}
		
	   $searchString .= " c.id != 1 AND c.deleted = '0' ";				   
	   $classroomInstructorQuery = "SELECT c.id, c.category, c.fullname as fullname, c.idnumber , c.keywords, c.is_active, ca.name as cat_name, c.createdby, c.publish, c.coursetype_id, c.primary_instructor, ct.coursetype,c.timemodified FROM {$CFG->prefix}course c" . 
					   " LEFT JOIN {$CFG->prefix}course_categories ca ON c.category = ca.id" . 
					   " LEFT JOIN {$CFG->prefix}coursetype ct ON c.coursetype_id = ct.id" . 
					   ( ($searchString != '') ? " WHERE " . $searchString : "") .$classroomInstructorWhere.
					   " ORDER BY c.fullname ASC";
					   
	   $classroomSecondaryInstructorQuery = "SELECT c.id, c.category, c.fullname as fullname, c.idnumber , c.keywords, c.is_active, ca.name as cat_name, c.createdby, c.publish, c.coursetype_id, c.primary_instructor, ct.coursetype,c.timemodified FROM {$CFG->prefix}course c" . 
					   " LEFT JOIN {$CFG->prefix}course_categories ca ON c.category = ca.id" . 
					   " LEFT JOIN {$CFG->prefix}coursetype ct ON c.coursetype_id = ct.id" . 
					   ( ($searchString != '') ? " WHERE " . $searchString : "") .$classroomSecondaryInstructorWhere.
					   " ORDER BY c.fullname ASC";			   
					   
					   				   
	    $courseQuery = 	"($classroomInstructorQuery) UNION ($classroomSecondaryInstructorQuery) ORDER BY fullname";
		$courseDetails = $DB->get_records_sql($courseQuery);
		return $courseDetails;
	}	
	
	
	
	
	/**
	 * get course criteria name or all results
	 *
	 * @param mixed $courseCriteriaId is course criteria id
	  * @return string $result course criteria  name or all records (if $courseCriteriaId is null)
	 */
	 
	 
	function getCourseCriteria($courseCriteriaId=''){
	
		global $DB, $CFG, $USER;
		$result = '';

		if(is_array($courseCriteriaId)){
		   $resultArr = array();
		   foreach($courseCriteriaId as $ccid){
		       $resultArr[] = $CFG->courseCriteriaArr[$ccid];
		   }
		   if(count($resultArr) > 0 ){
		      $result = implode(",", $resultArr);
		   }
		}else{
			if($courseCriteriaId){
			   $result = $CFG->courseCriteriaArr[$courseCriteriaId];
			}else{
			   $result = $CFG->courseCriteriaArr;
			}
        }
			
		
		return $result;
	}
	
	/**
	 * get course criteria id by course id
	 *
	 * @param mixed $courseId is course id
	  * @return string $courseCriteriaId course criteria id 
	 */
	 
	 
	function getCourseCriteriaIdByCourseId($courseId=''){
	
		global $DB, $CFG, $USER;
		$courseCriteriaId = '';


		if($courseId){
		   $query = "select criteria FROM {$CFG->prefix}course WHERE id IN (".$courseId.") ";
		   $courseCriteriaId = $DB->get_field_sql($query);
		}

			
		
		return $courseCriteriaId;
	}
	
		/**
	 * get course criteria id and is_global by course id
	 *
	 * @param mixed $courseId is course id
	  * @return string $courseCriteriaAndIsGlobal course criteria id and is_global field of course
	 */
	 
	 
	function getCourseCriteriaIdAndIsGlobalByCourseId($courseId=''){
	
		global $DB, $CFG, $USER;
		$courseCriteriaAndIsGlobal = '';


		if($courseId){
		   $query = "select criteria, is_global FROM {$CFG->prefix}course WHERE id IN (".$courseId.") ";
		   $courseCriteriaAndIsGlobal = $DB->get_record_sql($query);
		}

			
		
		return $courseCriteriaAndIsGlobal;
	}
	
	/**
	 * get course criteria name by course id
	 *
	 * @param mixed $courseId is course id
	  * @return string $courseCriteriaName course criteria  name 
	 */
	 
	 
	function getCourseCriteriaNameByCourseId($courseId=''){
	
		global $DB, $CFG, $USER;
		$courseCriteriaName = '';


		if($courseId){
		   $courseCriteriaId = getCourseCriteriaIdByCourseId($courseId);
		   $courseCriteriaName = $CFG->courseCriteriaArr[$courseCriteriaId];
		}

		return $courseCriteriaName;
	}
	
	// create duration in HH::MM Format
	
	function durationArray($startfromzero=false){
		global $CFG;
		
		$duration_arr = array();
		$minute_value = 0;
		for($hours=$CFG->durationStart; $hours<=$CFG->durationEnd; $hours++) // the interval for hours is '1'
		{
			for($mins=0; $mins<60; $mins+= $CFG->slotOfDuration) // the interval for mins is '30'
			{
				if($startfromzero && $hours==0 && $mins==0){
					continue;
				}
				$minute_value = $hours*60+$mins;
				$duration_arr[$minute_value] = str_pad($hours,2,'0',STR_PAD_LEFT).':'.str_pad($mins,2,'0',STR_PAD_LEFT);
				if($hours==$CFG->durationEnd){
					break;
				}
		
			}
			
		}
		
		return $duration_arr;
	}
	function emailForSessionAddUpdate($userid,$schId,$mode = 'add'){
		global $DB,$USER,$CFG;
		$classDetails = $DB->get_record_sql("SELECT s.id,s.course,ss.sessionname as name,ss.starttime,s.startdate,s.enddate,se.declined_remarks,s.location,u.firstname,u.lastname,u.email,u.id as uid FROM mdl_scheduler AS s LEFT JOIN mdl_scheduler_enrollment AS se ON se.scheduler_id = s.id LEFT JOIN mdl_scheduler_slots ss ON s.id = ss.schedulerid LEFT JOIN mdl_user as u ON u.id = se.userid WHERE ss.id = ".$schId." AND se.userid = ".$userid);
		if($classDetails->starttime > strtotime('now')){
			$events = $DB->get_records_sql("SELECT ss.id,e.id as eid FROM mdl_scheduler_slots ss LEFT JOIN mdl_event as e ON ss.id = e.sessionid WHERE ss.isactive = 1 AND ss.id = ".$schId." AND e.userid = ".$userid);
			$emailFile = '';
			$emailFileName = '';
			$addUpdate = 0;
			$append = '';
			if($mode == 'update'){
				$addUpdate = 1;	
				$append = '_update';
			}
			if(!empty($events)){
				$update = 0;
				$eventArr = array();
				foreach($events as $event){
					$eventArr[] = $event->eid;
				}
				$emailFile = generateIcsFile($eventArr,$addUpdate);
				$emailFileName = basename($emailFile);
			}
			$courseDetails = $DB->get_record_sql("SELECT c.id,c.fullname FROM mdl_course as c WHERE c.id =  ".$classDetails->course);
			$site = get_site();
			$classDetails->user_name = getUserNameFormat($classDetails);
			$emailContent = array(
									'name'			=>$classDetails->user_name,
									'eventname'		=>$courseDetails->fullname.': '.$classDetails->name,
									'eventtime'		=> getDateFormat($classDetails->starttime, $CFG->customDefaultDateTimeFormat),
									'lmsLink'		=> $CFG->emailLmsLink,
									'lmsName'		=> $CFG->emailLmsName,
									'coursename'	=> $courseDetails->fullname
								);
			$emailContent = (object)$emailContent;
			$messageTextForLearner = get_string('classroom_event_body'.$append,'email',$emailContent);
			if($emailFile != ''){
				$messageTextForLearner .= get_string('add_to_outlook','email');
			}
			$messageTextForLearner .= get_string('email_footer','email');
			$messageTextForLearner .= get_string('from_email','email');
			$subjectForLearner = get_string('classroom_event_subject','email');
			$messageHtmlForLearner = text_to_html($messageTextForLearner, false, false, true);
			$toLearnerUser->email = $classDetails->email;
			$toLearnerUser->firstname =  $classDetails->firstname;
			$toLearnerUser->lastname =  $classDetails->lastname;
			$toLearnerUser->maildisplay = true;
			$toLearnerUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
			$toLearnerUser->id = $classDetails->uid;
			$toLearnerUser->firstnamephonetic = '';
			$toLearnerUser->lastnamephonetic = '';
			$toLearnerUser->middlename = '';
			$toLearnerUser->alternatename = '';
			email_to_user($toLearnerUser, $fromUser, $subjectForLearner, $messageTextForLearner, $messageHtmlForLearner, $emailFile, $emailFileName, true);
		}
	}	
	function emailForSessionAddUpdateToTeacher($userid,$schId,$mode = 'add'){
		global $DB,$USER,$CFG;
		$classDetails = $DB->get_record_sql("SELECT s.id,s.course,ss.sessionname AS name,ss.starttime,s.startdate,s.enddate,s.location,u.firstname,u.lastname,u.email,u.id AS uid FROM mdl_scheduler AS s LEFT JOIN mdl_scheduler_slots ss ON s.id = ss.schedulerid LEFT JOIN mdl_user AS u ON u.id = s.teacher WHERE ss.id = ".$schId." AND s.teacher = ".$userid);
		$events = $DB->get_records_sql("SELECT ss.id,e.id as eid FROM mdl_scheduler_slots ss LEFT JOIN mdl_event as e ON ss.id = e.sessionid WHERE ss.isactive = 1 AND ss.id = ".$schId." AND e.userid = ".$userid);
		if($classDetails->starttime > strtotime('now')){
			$emailFile = '';
			$emailFileName = '';
			$addUpdate = 0;
			$append = '';
			if($mode == 'update'){
				$addUpdate = 1;	
				$append = '_update';
			}
			if(!empty($events)){
				$update = 0;
				$eventArr = array();
				foreach($events as $event){
					$eventArr[] = $event->eid;
				}
				$emailFile = generateIcsFile($eventArr,$addUpdate);
				$emailFileName = basename($emailFile);
			}
			$courseDetails = $DB->get_record_sql("SELECT c.id,c.fullname FROM mdl_course as c WHERE c.id =  ".$classDetails->course);
			$site = get_site();
			$classDetails->user_name = getUserNameFormat($classDetails);
			$emailContent = array(
									'name'			=>$classDetails->user_name,
									'eventname'		=>$courseDetails->fullname.': '.$classDetails->name,
									'eventtime'		=> getDateFormat($classDetails->starttime, $CFG->customDefaultDateTimeFormat),
									'lmsLink'		=> $CFG->emailLmsLink,
									'lmsName'		=> $CFG->emailLmsName,
									'coursename'	=> $courseDetails->fullname
								);
			$emailContent = (object)$emailContent;
			$messageTextForLearner = get_string('classroom_event_body'.$append,'email',$emailContent);
			if($emailFile != ''){
				$messageTextForLearner .= get_string('add_to_outlook','email');
			}
			$messageTextForLearner .= get_string('email_footer','email');
			$messageTextForLearner .= get_string('from_email','email');
			$subjectForLearner = get_string('classroom_event_subject','email');
			$messageHtmlForLearner = text_to_html($messageTextForLearner, false, false, true);
			$toLearnerUser->email = $classDetails->email;
			$toLearnerUser->firstname =  $classDetails->firstname;
			$toLearnerUser->lastname =  $classDetails->lastname;
			$toLearnerUser->maildisplay = true;
			$toLearnerUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
			$toLearnerUser->id = $classDetails->uid;
			$toLearnerUser->firstnamephonetic = '';
			$toLearnerUser->lastnamephonetic = '';
			$toLearnerUser->middlename = '';
			$toLearnerUser->alternatename = '';
			email_to_user($toLearnerUser, $fromUser, $subjectForLearner, $messageTextForLearner, $messageHtmlForLearner, $emailFile, $emailFileName, true);
		}
	}
	function emailForClassAddUpdateToTeacher($userid,$classId,$mode = 'add'){
		global $DB,$USER,$CFG;
		$classDetails = $DB->get_record_sql("SELECT s.id,s.course,s.name,s.startdate,s.enddate, s.location,createdby FROM mdl_scheduler as s WHERE s.id = ".$classId);
		$userDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email FROM mdl_user u WHERE u.id = ".$userid);
		$userDetails->user_name = getUserNameFormat($userDetails);
		$courseDetails = $DB->get_record_sql("SELECT c.id,c.fullname,c.summary FROM mdl_course as c WHERE c.id =  ".$classDetails->course);
		$emailFile = '';
		$emailFileName = '';
		$addUpdate = 0;
		$append = '';
		if($mode == 'update'){
			$addUpdate = 1;	
			$append = '_update';
		}elseif($mode == 'delete'){
			$addUpdate = 2;	
			$append = '_delete';
		}
		$sessionsListing = '';
		if($mode == 'add'){
			$schedulerSlots = $DB->get_records_sql('SELECT ss.id,ss.sessionname,ss.starttime,ss.duration FROM mdl_scheduler_slots as ss WHERE ss.schedulerid = '.$classId);
			if(!empty($schedulerSlots)){
				$sessionsListing .= get_string('sessions_text','email');
				$sessionsListing .= "<ul>";
				foreach($schedulerSlots as $Slots){
					$title = $courseDetails->fullname.':'.$Slots->sessionname;
					$description = $courseDetails->summary;
					$eventAdded = addClassEvent($title,$description,$userid,$Slots->id,$Slots->starttime,$Slots->duration);
					$end_time =endTimeByDuration($Slots->starttime,$Slots->duration);
					$duration_text = date($CFG->customDefaultDateTimeFormat,$Slots->starttime).' to '.date($CFG->customDefaultTimeFormat1,$end_time);
					$sessionsListing .= "<li>";
					$sessionsListing .= $Slots->sessionname;
					$sessionsListing .= " - "/*.get_string('duration','scheduler').": "*/.$duration_text;
					$sessionsListing .= "</li>";
				}
				$sessionsListing .= "</ul>";
			}
		}
		$events = $DB->get_records_sql("SELECT ss.id,e.id as eid FROM mdl_scheduler_slots ss LEFT JOIN mdl_event as e ON ss.id = e.sessionid WHERE ss.isactive = 1 AND ss.schedulerid = ".$classId." AND e.userid = ".$userid);
		if(!empty($events)){
			$update = 0;
			$eventArr = array();
			foreach($events as $event){
				$eventArr[] = $event->eid;
			}
			$emailFile = generateIcsFile($eventArr,$addUpdate);
			$emailFileName = basename($emailFile);
		}
		if($mode == 'delete'){
			deleteEventForClass($classId,$userid);
		}
		$classDuration = getClassDuration($classDetails->startdate,$classDetails->enddate);
		$attachmentInfo = '';
		if($emailFile != ''){
			$attachmentInfo = get_string('class_outlook_info','email');
		}
		$locationText = '';
		if(trim($classDetails->location) != ''){
			$locationText = "<li>".get_string('location','scheduler').': '.$classDetails->location."</li>";
		}
		$summary = '';
		if($CFG->showCourseDescriptionInMail == 1){
			$summary = getCourseSummary($courseDetails);
		}
		$emailContent = array(
								'name'			=>$userDetails->user_name,
								'coursename'	=>$courseDetails->fullname.": ".$classDetails->name,
								'classstart'		=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
								'starttime'	=> $classDuration,
								'lmsLink'		=> $CFG->emailLmsLink,
								'lmsName'		=> $CFG->emailLmsName,
								'attachment_info' => $attachmentInfo,
								'location'	=>$locationText,
								'sessionsListing'	=>$sessionsListing,
								'summary'	=>$summary
							);
		$emailContent = (object)$emailContent;
		$messageTextForLearner = get_string('class_enrol_for_instructor'.$append,'email',$emailContent);
		$messageTextForLearner .= get_string('email_footer','email');
		$messageTextForLearner .= get_string('from_email','email');
		$subjectForLearner = get_string('class_enrol_for_instructor_subject'.$append,'email',$emailContent);
		$toLearnerUser->email = $userDetails->email;
		$toLearnerUser->firstname =  $userDetails->firstname;
		$toLearnerUser->lastname =  $userDetails->lastname;
		$toLearnerUser->maildisplay = true;
		$toLearnerUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toLearnerUser->id = $userDetails->id;
		$toLearnerUser->firstnamephonetic = '';
		$toLearnerUser->lastnamephonetic = '';
		$toLearnerUser->middlename = '';
		$toLearnerUser->alternatename = '';
		email_to_user($toLearnerUser, $fromUser, $subjectForLearner, $messageTextForLearner, '', $emailFile, $emailFileName, true);
	}
?>
