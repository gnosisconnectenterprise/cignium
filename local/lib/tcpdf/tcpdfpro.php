<?php

require_once('tcpdf/tcpdf.php');
require_once('tcpdf/config/lang/eng.php');

class TCPDFPRO extends TCPDF {

    public $isFooterOld = false;

    public function Header () {
        $ormargins = $this->getOriginalMargins();
        $headerfont = $this->getHeaderFont();
        $headerdata = $this->getHeaderData();
        if (($headerdata['logo']) AND ($headerdata['logo'] != K_BLANK_IMAGE)) {
            $imgtype = $this->getImageFileType(K_PATH_IMAGES . $headerdata['logo']);
            if (($imgtype == 'eps') OR ($imgtype == 'ai')) {
                $this->ImageEps(K_PATH_IMAGES . $headerdata['logo'], '', '', $headerdata['logo_width']);
            } elseif ($imgtype == 'svg') {
                $this->ImageSVG(K_PATH_IMAGES . $headerdata['logo'], '', '', $headerdata['logo_width']);
            } else {
                $this->Image(K_PATH_IMAGES . $headerdata['logo'], '', '', $headerdata['logo_width']);
            }
            $imgy = $this->getImageRBY();
        } else {
            $imgy = $this->GetY();
        }
        $cell_height = round(($this->getCellHeightRatio() * $headerfont[2]) / $this->getScaleFactor(), 2);
        // set starting margin for text data cell
        if ($this->getRTL()) {
            $header_x = $ormargins['right'] + ($headerdata['logo_width'] * 1.1);
        } else {
            $header_x = $ormargins['left'] + ($headerdata['logo_width'] * 1.1);
        }
        $this->SetTextColor(0, 0, 0);
        // header title
        $this->SetFont($headerfont[0], 'I', $headerfont[2] + 2);
        $this->SetX($header_x);
        $this->Cell(0, $cell_height, $headerdata['title'], 0, 1, 'R', 0, '', 0);
        // header string
        $this->SetFont($headerfont[0], $headerfont[1], $headerfont[2]);
        $this->SetX($header_x);
        $this->MultiCell(0, $cell_height, $headerdata['string'], 0, 'R', 0, 1, '', '', true, 0, false);
        // print an ending header line
        $this->SetLineStyle(array('width' => 0.85 / $this->getScaleFactor(), 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $this->SetY((2.835 / $this->getScaleFactor()) + max($imgy, $this->GetY()));
        if ($this->getRTL()) {
            $this->SetX($ormargins['right']);
        } else {
            $this->SetX($ormargins['left']);
        }
        $this->Cell(0, 0, '', '', 0, 'C');
    }

    public function Footer () {
        if ($this->isFooterOld == false) {
            $cur_y = $this->y;
            $this->SetTextColor(0, 0, 0);
            $this->SetY($cur_y);

            $content1 = '<table width="750" cellpadding="0" cellspacing="0" border="0"><tr><td width="50%" align="left" valign="top"><em>' . $this->header_title . '</em><br>' . $this->footer_copyright_string . '</td>';
            //$content1 = '<table width="750" cellpadding="0" cellspacing="0" border="0"><tr><td width="50%" align="left" valign="top"><h4><em>' . $this->footer_copyright_string . '</em></h4></td>';
            $content2 = '<td width="50%" align="right" valign="top">Page ' . $this->getAliasNumPage() . ' of ' . $this->getAliasNbPages() . '</td></tr></table>';

            //$this->MultiCell(0, 50, $content, 0, 'L', false, 0, '', '', true, 0, true);
            $this->MultiCell(140, 5, $content1 . $content2, 0, 'L', false, 0, '', '', true, 0, true);
            //$this->MultiCell(55, 5, $content2, 0, 'R', false, 1, '', '', true, 0, true);
        } else {
            $cur_y = $this->GetY();//$this->y;
            $this->SetTextColor(0, 0, 0);
            //set style for cell border
            $line_width = 0;

            //$this->SetLineStyle(array('width' => $line_width, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
            //print document barcode
            $barcode = $this->getBarcode();
            if (!empty($barcode)) {
                    $this->Ln($line_width);
                    //$barcode_width = round(($this->w - $this->original_lMargin - $this->original_rMargin) / 3);
                    $style = array(
                            'position' => $this->rtl?'R':'L',
                            'align' => $this->rtl?'R':'L',
                            'stretch' => false,
                            'fitwidth' => true,
                            'cellfitalign' => '',
                            'border' => false,
                            'padding' => 0,
                            'fgcolor' => array(0,0,0),
                            'bgcolor' => false,
                            'text' => false
                    );
                    $this->write1DBarcode($barcode, 'C128B', '', $cur_y + $line_width, '', (($this->footer_margin / 3) - $line_width), 0.3, $style, '');
            }
            if (empty($this->pagegroups)) {
                //$pagenumtxt = $this->l['w_page'].' '.$this->getAliasNumPage().' / '.$this->getAliasNbPages();
                //$pagenumtxt =  $this->coursename.' Preview '.$this->l['w_page'].' '.$this->getAliasNumPage() . ' of '.$this->getAliasNbPages();
                $pagenumtxt =  $this->coursename.' Preview '.$this->l['w_page'].' '.$this->getAliasNumPage('{n}') . ' of '.$this->getAliasNbPages('{p}');
            } else {
                $pagenumtxt =  $this->coursename.' Preview '.$this->l['w_page'].' '.$this->getPageNumGroupAlias();
            }
            $this->SetY($cur_y);

            if ($this->getRTL()) {
                    $this->SetX($this->original_rMargin);
                    $this->Image(PUBLIC_PATH.'/CPD/DOC/CPDDOC_files/logo.jpg', $this->SetX($this->original_rMargin), $cur_y+2, '' ,8, '', '', 'T', false, 300, 'L', false, false, 0, false, false, false);
            } else {
                    $this->SetX($this->original_lMargin);
                    $this->Image(PUBLIC_PATH.'/CPD/DOC/CPDDOC_files/logo.jpg', $this->SetX($this->original_lMargin), $cur_y+2, '' ,8, '', '', 'T', false, 300, 'L', false, false, 0, false, false, false);
            }
            $this->setRightMargin(0);
            if ($this->getRTL()) {
                $this->SetX($this->original_rMargin);
                $this->Cell(0, 0, $pagenumtxt, '', 0, 'L');
            } else {
                $this->SetX($this->original_lMargin);
                $this->Cell(0, 0, $this->getAliasRightShift().$pagenumtxt, 0, 0, 'R', false, '', 0, false, 'T');
            }
            $this->setRightMargin(5);

            //$this->SetY($this->GetY() + 3);
            $this->SetY($cur_y + 6);
            $this->MultiCell(0, 0, $this->footer_copyright_string, 0, 'R', false, 0, '', '', true, 0, true);
            //$this->Cell(0, 0, $this->footer_copyright_string, '', 1, 'R');
        }
    }

}

// END OF TCPDF CLASS

//============================================================+
// END OF FILE
//============================================================+
