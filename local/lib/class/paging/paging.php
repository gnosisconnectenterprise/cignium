<?php 
// this class and componenet are using for creating a custom paging 
// created by rajesh on 13-06-14
	class PageComponent {
	
			/**
			 * @var int The maximum number of pagelinks to display.
			 */
			public $maxdisplay = 18;
		
			/**
			 * @var int The total number of entries to be pages through..
			 */
			public $totalcount;
		
			/**
			 * @var int The page you are currently viewing.
			 */
			public $page;
		
			/**
			 * @var int The number of entries that should be shown per page.
			 */
			public $perpage;
		
			/**
			 * @var string|moodle_url If this  is a string then it is the url which will be appended with $pagevar,
			 * an equals sign and the page number.
			 * If this is a moodle_url object then the pagevar param will be replaced by
			 * the page no, for each page.
			 */
			public $baseurl;
		
			/**
			 * @var string This is the variable name that you use for the pagenumber in your
			 * code (ie. 'tablepage', 'blogpage', etc)
			 */
			public $pagevar;
		
			/**
			 * @var string A HTML link representing the "previous" page.
			 */
			public $previouslink = null;
		
			/**
			 * @var string A HTML link representing the "next" page.
			 */
			public $nextlink = null;
		
			/**
			 * @var string A HTML link representing the first page.
			 */
			public $firstlink = null;
		
			/**
			 * @var string A HTML link representing the last page.
			 */
			public $lastlink = null;
		
			/**
			 * @var array An array of strings. One of them is just a string: the current page
			 */
			public $pagelinks = array();
		
			/**
			 * Constructor paging_bar with only the required params.
			 *
			 * @param int $totalcount The total number of entries available to be paged through
			 * @param int $page The page you are currently viewing
			 * @param int $perpage The number of entries that should be shown per page
			 * @param string|moodle_url $baseurl url of the current page, the $pagevar parameter is added
			 * @param string $pagevar name of page parameter that holds the page number
			 */
			public function __construct($totalcount, $page, $perpage, $baseurl, $pagevar = 'page') {  
				$this->totalcount = $totalcount;
				$this->page       = $page;
				$this->perpage    = $perpage;
				$this->baseurl    = $baseurl;
				$this->pagevar    = $pagevar; 		
				
			}
		
			/**
			 * Prepares the paging bar for output.
			 *
			 * This method validates the arguments set up for the paging bar and then
			 * produces fragments of HTML to assist display later on.
			 *
			 * @param renderer_base $output
			 * @param moodle_page $page
			 * @param string $target
			 * @throws coding_exception
			 */
			public function prepare($output, $page, $target) {  
				if (!isset($this->totalcount) || is_null($this->totalcount)) {
					throw new coding_exception('paging_bar requires a totalcount value.');
				}
				if (!isset($this->page) || is_null($this->page)) {
					throw new coding_exception('paging_bar requires a page value.');
				}
				if (empty($this->perpage)) {
					throw new coding_exception('paging_bar requires a perpage value.');
				}
				if (empty($this->baseurl)) {
					throw new coding_exception('paging_bar requires a baseurl value.');
				}
		
				if ($this->totalcount > $this->perpage) {  
					$pagenum = $this->page - 1;
		
					if ($this->page > 0) {
						$this->previouslink = html_writer::link(new moodle_url($this->baseurl, array($this->pagevar=>$pagenum)), get_string('previous','paging'), array('class'=>'previous'));
					}
		
					if ($this->perpage > 0) {
						$lastpage = ceil($this->totalcount / $this->perpage);
					} else {
						$lastpage = 1;
					}
		
					if ($this->page > round(($this->maxdisplay/3)*2)) {
						$currpage = $this->page - round($this->maxdisplay/3);
		
						$this->firstlink = html_writer::link(new moodle_url($this->baseurl, array($this->pagevar=>0)), '1', array('class'=>'first'));
					} else {
						$currpage = 0;
					}
		
					$displaycount = $displaypage = 0;
		
		            
					// added by rajesh
		           
					$this->pagedropdown .= "<select name='pageno' id='custompageno' onchange='window.location.href=this.value' >";
					// end		
					while ($displaycount < $this->maxdisplay and $currpage < $lastpage) {
						$displaypage = $currpage + 1;
		
						if ($this->page == $currpage) {
							$this->pagelinks[] = $displaypage;
						} else {
							$pagelink = html_writer::link(new moodle_url($this->baseurl, array($this->pagevar=>$currpage)), $displaypage);
							$this->pagelinks[] = $pagelink;
						}
						
						// added by rajesh 
						$selecturl = new moodle_url($this->baseurl, array($this->pagevar=>$currpage));
						if ($this->page == $currpage) {
							$this->pagedropdown .= "<option value='".$selecturl."' selected='selected'>".$displaypage."</option>";
						} else {
							$this->pagedropdown .= "<option value='".$selecturl."'>".$displaypage."</option>";
						}
						// added by rajesh 
						
					
		
						$displaycount++;
						$currpage++;
					}
					
					$this->pagedropdown .= "</select>";  // added by rajesh 
					
					
		
					if ($currpage < $lastpage) {
						$lastpageactual = $lastpage - 1;
						$this->lastlink = html_writer::link(new moodle_url($this->baseurl, array($this->pagevar=>$lastpageactual)), $lastpage, array('class'=>'last'));
					}
		
					$pagenum = $this->page + 1;
		
					if ($pagenum != $displaypage) {
						$this->nextlink = html_writer::link(new moodle_url($this->baseurl, array($this->pagevar=>$pagenum)), get_string('next','paging'), array('class'=>'next'));
					}
				}
				
				
			}	
	
	}
	
	
	class load_paging {
		
		
		   protected $output;

			
			/**
			 * Returns HTML to display a single paging bar to provide access to other pages  (usually in a search)
			 * @param int $totalcount The total number of entries available to be paged through
			 * @param int $page The page you are currently viewing
			 * @param int $perpage The number of entries that should be shown per page
			 * @param string|moodle_url $baseurl url of the current page, the $pagevar parameter is added
			 * @param string $pagevar name of page parameter that holds the page number
			 * @return string the HTML to output.
			 */
			 
			public function paging_bar($totalcount, $page, $perpage, $baseurl) {  
			
			    global $CFG;
				$output = '';
				$pagingbar = new PageComponent($totalcount, $page, $perpage, $baseurl);
				$pagingbar = clone($pagingbar);
				
				$pagingbar->prepare($this, $this->page, $this->target);

				if ($pagingbar->totalcount > $pagingbar->perpage) { 
				
					$output .= get_string('page','paging') . ':';
					/*$output .= "<select name='perpagecount' id='perpagecount' onchange='window.location.href=this.value' >";
					$outputOpt = '';
                    $perpagecount = $CFG->perPageCount; 
					$i = $perpagecount;
					while ($totalcount > $perpage  &&  $totalcount%$perpagecount == 0) {
					 
					   
					  //$selecturl = new moodle_url($this->baseurl, array($this->pagevar=>$currpage));
					  $selecturl = '';
					  if($i == $perpage){
					    $outputOpt .= "<option value='".$selecturl."' selected='selected'>".$i."</option>";
					  }else{
					    $outputOpt .= "<option value='".$selecturl."' selected='selected'>".$i."</option>";
					  }
					  $i = $i+$perpagecount;
					}
					$output .= $outputOpt."</select";*
			*/
					if (!empty($pagingbar->firstlink)) {
						$output .= '&#160;' . $pagingbar->firstlink . '&#160;...';
					}
			

					foreach ($pagingbar->pagelinks as $link) {
						//$output .= "&#160;&#160;$link";
					}

			
			        $output .= $pagingbar->pagedropdown;
					$output .= " ".get_string('of','paging')." $totalcount";
					 
					if (!empty($pagingbar->lastlink)) {
						$output .= '&#160;...' . $pagingbar->lastlink . '&#160;';
					}
					
					
			
					if (!empty($pagingbar->nextlink)) {
						$output .= '&#160;&#160;' . $pagingbar->nextlink . '';
					}else{
					    $output .= '<span class="next">' . get_string('next','paging') . '</span>';
					}
					
					if (!empty($pagingbar->previouslink)) {
						$output .= '&#160;' . $pagingbar->previouslink . '&#160;';
					}else{
					    $output .= '<span class="previous">' . get_string('previous','paging') . '</span>';
					}
					
				}

				
				return html_writer::tag('div', $output, array('class' => 'paging'));
			}
			
		
		 
	}	 // end  theme_gourmet_plugin_renderer_base class
	
	
	$CUSTOM_PAGING = new load_paging();
?>