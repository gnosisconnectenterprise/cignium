<?php 

	defined('MOODLE_INTERNAL') || die();
		
	/**
	 * get external department html for login page
	 * @param int $element is an element type, if 1 then will load radio button part, 
	    if 2 then will load register part
	  * @return html $html is html for login page
	 */
	 
	 
	function getExternalDepartmentHtml($element = '', $loginTypeChecked = 1){
	
		global $DB, $CFG, $USER;
		//pr($_POST);die;
		$html = '';

		if($CFG->allowExternalDepartment == 1){
		
		  if($element == 1){
		    $loginTypeHtml = '';
			foreach($CFG->externalDepartmentOptions as $loginTypeId => $loginType){
			    $selected = $loginTypeId == $loginTypeChecked?"checked='checked'":'';
				$loginTypeHtml .= '<input name="logintype" type="radio" '.$selected.' value="'.$loginTypeId.'" autocomplete="off" id = "logintype"/> '.$loginType;
			}
			
			$html .= '<div class="login-type-box">'.$loginTypeHtml.'</div><div class="clear"></div>';
		  }elseif($element == 2){
		    $html .= '<div class="register-box">
			              <div class="clear"></div>
			              <span class="f-left npr-search"> <a href="javascript:void(0)" onclick="window.open(\''.$CFG->edNPRSearch[2].'\', \''.$CFG->edNPRSearch[1].'\', \''.$CFG->externalWindowParameter.'\')">'.$CFG->edNPRSearch[1].'</a> </span>
			              <span class="f-right register"> <a style="visibility:hidden" href="'.$CFG->edWantToRegister[2].'" >'.$CFG->edWantToRegister[1].'</a> </span>
			         </div>';
		   }
		   
		   $html .= "<script type='text/javascript'>	   
					   $(document).ready(function(){";
			
							if($loginTypeChecked == 1){
							  $html .= "  $('.register-box').hide();";
							}else{
							  $html .= " $('.register-box').show(); ";
							} 
							
							$html .= " $(document).on('click', '#logintype', function(){
							  
											   var val =  $(this).val();
											   $('.username-error').hide();
											   $('.password-error').hide();
											   if(val == 1){
												  $('.register-box').hide();
											   }else{
												  $('.register-box').show();
											   }
							 
							           });";
						 
					   $html .= "});";
					  $html .= "</script>";
		
		}
		return $html;
	}
	
	/**
	 * To check whether login user belongs to external department or not
	 *
	 * @param int $userId is user id
	 * @return bool $result if belongs to external department then return true else false
	 */
	 
	 
	function isLoginUserBelongsToED(){
	
		global $DB, $CFG, $USER;
		
		$result = false;
		if($CFG->allowExternalDepartment == 1){
			$query = "select id FROM {$CFG->prefix}department  WHERE 1 = 1  AND deleted='0'  AND is_external = 1 AND id = '".$USER->department."'";
			$result = $DB->get_field_sql($query);
			$result = $result?true:false;
		}
		return $result;
	}
	
	function getEDAstric($under="select", $display = '', $direction = ''){ // under select or label
	
	   global $CFG, $USER;
	
	   $identifierED = '';
	   if($CFG->showExternalDepartment == 1 && $USER->archetype == $CFG->userTypeAdmin){
	   
	       if($under == 'select'){
	         $identifierED = get_string('external_department_identifier','department');
		   }if($under == 'view'){
	         $identifierED = '<small class = "external-department-identifier" >'.get_string('external_department_identifier','department').'</small>';
		   }elseif($under == 'label'){
		      $identifierED = '<div class="f-left"><span><small class="external-department-identifier">'.get_string('external_department_identifier','department').'</small>'.get_string('list_external_department_instruction','department').'</span></div>';
		   }elseif($under == 'filter'){
		      $identifierED = '<span class="pull-right" '.$display.' id="filter-external-label"><small class="external-department-identifier">'.get_string('external_department_identifier','department').'</small>'.get_string('list_external_department_instruction','department').'</span>';
		   }elseif($under == 'lower-assign-filter'){
		      $direction = $direction?'-'.$direction:'';
		      $identifierED = '<div class="lower-filter-external-label" '.$display.'  id="lower-filter-external-label'.$direction.'" ><span><small class="external-department-identifier">'.get_string('external_department_identifier','department').'</small>'.get_string('list_external_department_instruction','department').'</span></div>';
		   }
	   }
	   
	   return $identifierED;
	}
	
	
	function getReportEDAstricFor($for="print", $sDepartment = '-1'){ 
	
	   global $CFG, $USER;
	
	   $identifierED = '';
	   $html = '';
	   if($for == 'print'){
			   
			   if($CFG->showExternalDepartment == 1 && $USER->archetype == $CFG->userTypeAdmin ){
				
					if($sDepartment && $sDepartment != '-1'){
						$identifierEDLabel = '<div class="f-left"><span><small class="external-department-identifier">'.get_string('external_department_identifier','department').'</small>'.get_string('list_external_department_instruction','department').'</span></div>';
						$html .= '<tr>';
							$html .= '<td colspan="2">'.$identifierEDLabel.'</td>';
						$html .= '</tr>';
					}
				}
				
	    }elseif($for == 'pdf'){
			   
			   if($CFG->showExternalDepartment == 1 && $USER->archetype == $CFG->userTypeAdmin ){
				
					if($sDepartment && $sDepartment != '-1'){
						$identifierEDLabel = '<div class="f-left"><span><small class="external-department-identifier">'.get_string('external_department_identifier','department').'</small>'.get_string('list_external_department_instruction','department').'</span></div>';
						$html .= '<tr>';
							$html .= '<td colspan="2">'.$identifierEDLabel.'</td>';
						$html .= '</tr>';
					}
				}
				
	    }elseif($for == 'csv'){
		
		   $html = get_string('external_department_identifier','department').' '.get_string('list_external_department_instruction','department')."\n";
	    }

	    return $html;
	}
	
	
	
	function createDepartmentSelectBoxForFilter($idElement = 'department',  $data = array(), $selectedArr = array('-1'), $display='', $multiple=true){
	   
		global $CFG, $USER, $DB;
		$HTML = '';
		$HTMLOptions = '';
		
		$selectedC = in_array('-1', $selectedArr)?'selected="selected"':'';
		$multiple = $multiple?'multiple="multiple"':'';
		$size = $multiple?'size="2"':'';
		$HTML .= '<select autocomplete="off" '.$size.' '.$multiple.' id="'.$idElement.'" name="'.$idElement.'" '.$display.' >';
		$HTMLOptions .= '<option value="-1" '.$selectedC.' >'.get_string('alldepartment','user').'</option>';
		
		if(count($data) > 0 ){
		   
		   if(!empty($data)){
				foreach($data as $rows){
					 $selectedC = count($selectedArr) > 0 && in_array($rows->id, $selectedArr)?'selected="selected"':'';
					 $HTMLOptions .= '<option value="'.$rows->id.'" '.$selectedC.' >'.$rows->title.'</option>';
				}
			}
		}
		$HTML .=  $HTMLOptions;
		
		$HTML .= '</select>';
		return  $HTML;
        
	}
	
	
	
	function createDepartmentSelectBox($mform, $action, $page, $displayDepartment='', $data){
	
		global $CFG, $USER, $DB;
		
		$extDeptIdentifier = get_string('external_department_identifier', 'department');	
		$depQuery = "SELECT id,(if(is_external = 1, concat('".$extDeptIdentifier."', title), title)) as title, is_external FROM {$CFG->prefix}department ";
		$depQuery .= " WHERE deleted = 0 AND status = 1 ";
		if($CFG->showExternalDepartment != 1){
		 $depQuery .= " AND is_external = '0' ";
		}
		$depQuery .= " order by title ASC";
		$departments = $DB->get_records_sql($depQuery);
		
		$departmentSelect = array('0'=>get_string('select_department'));
		foreach($departments as $department){
			$departmentSelect[$department->id] = $department->title;
		}
	   

		if($action == 'edit' && $page == 'user'){ // for user/editadvanced.php
		
				
			$disable_fields = "";
			if($CFG->isLdap==1){
				$disable_fields = 'class="disabled_user_form_fields" disabled="disabled"';
			}
			
			$mform->addElement('html', '<div id = "department_select" '.$displayDepartment.'>');
			$mform->addElement('select', 'department', get_string('department'), $departmentSelect, $disable_fields);
			//if($CFG->showExternalDepartment == 1){
				//$identifierED = getEDAstric('label');
				//$mform->addElement('static', 'external_department_instruction', '',$identifierED);
			//}
			$mform->addElement('html', '</div>');
			//$mform->getElement('department')->setMultiple(true);
			//$mform->addRule('department', get_string('required'), 'nonzero', null, 'client');
			
		}elseif($action == 'edit' && $page == 'group'){ // for group/addgroup.php
		
		$disabled_fields = "";
		if($data->auth=='ldap'){
			$disabled_fields = 'class="disabled_user_form_fields" disabled="disabled"';
		}
			
			$mform->addElement('select', 'department', get_string('department'), $departmentSelect,$disabled_fields);
		    $identifierED = getEDAstric('label');
			if($identifierED){
			  $mform->addElement('static', 'external_department_instruction', '',$identifierED);
            }
	  
			if($USER->archetype == $CFG->userTypeManager){
			
				$teamDepartmentArr = array();
				if(!empty($data->id)){
					
					$teamDepartment = $DB->get_records('group_department', array('team_id'=>$data->id, 'is_active'=>1));
					
					if(count($teamDepartment) > 0 ){
					  foreach($teamDepartment as $arr ){
					   $teamDepartmentArr[] = $arr->department_id;
					  }
					}
					
					$mform->SetDefault('department',$teamDepartmentArr);
					$mform->hardFreeze('department');
					
				}else{
				
					$userDepartment = $DB->get_records_sql("SELECT departmentid FROM {$CFG->prefix}department_members WHERE userid = ".$USER->id);
					$userDepartments = array();
					if($userDepartment){
						foreach($userDepartment as $department){
							$userDepartments[] = $department->departmentid;
						}
						//$mform->getElement('department')->setSelected($userDepartments);
					}
					
					if(!empty($userDepartments)){
						$mform->SetDefault('department',$userDepartments);
						$mform->hardFreeze('department');
					}
				
				}
			   
			   
			}elseif($USER->archetype == $CFG->userTypeAdmin){
				
				$teamDepartmentArr = array();
				if(!empty($data->id)){
					
					$teamDepartment = $DB->get_records('group_department', array('team_id'=>$data->id, 'is_active'=>1));
					if(count($teamDepartment) > 0 ){
					  foreach($teamDepartment as $arr ){
					   $teamDepartmentArr[] = $arr->department_id;
					  }
					  
					  $mform->SetDefault('department',$teamDepartmentArr);
					  $mform->hardFreeze('department');
					
					}
					
				}
			
			}
	     }
	}

	
	function externalCheckForDepartment($extraCond = ''){
	
	    $where = '';
	    global $CFG;
	    if($CFG->showExternalDepartment != 1){
		 $where .= " AND is_external = 0 ";
		 $where .= $extraCond;
		}
		
		return $where;

	}
	
	function getRadioFilters($displaySelector, $radioChecked1, $radioChecked2){
	
	   global $CFG;
	   
	   $html = '<div class = "field_selector" '.$displaySelector.' >
		<div class ="selector-1 selector-radio">
			<input type = "radio" name = "selector" value = "1" autocomplete="off" class = "sel_mode" '.$radioChecked1.' ><span>'.get_string("by_department").'</span>
		</div>
		<div class ="selector-2 selector-radio">
			<input type = "radio" name = "selector" value = "2" autocomplete="off" class = "sel_mode" '.$radioChecked2.' ><span>'.get_string("by_group").'</span>
		</div>';
       $html .= getEDAstric('filter'); 
	   $html .= '</div>';
	
	   return $html;
    
	
	}
	
	/**
	 * get company full name or all results
	 *
	 * @param mixed $compId is company id
	 * @return string $result company full name or all records (if $compId is null)
	 */
	 
	 
	function getCompany($compId, $isReport = false){
	
		global $DB, $CFG, $USER;
		$result = '';

		if(is_array($compId)){
		  $compIds = count($compId) > 0 ? implode(",",$compId) : 0;
		  if($compIds){
		    $query = "select GROUP_CONCAT(title  ORDER BY title ASC  SEPARATOR ', ') FROM {$CFG->prefix}company WHERE id IN (".$compIds.") AND deleted = '0'";
			if($isReport == false){
			 $query .= " AND status = '1'";
			}
			$query .= " ORDER BY title ASC";
		    $result = $DB->get_field_sql($query);
		  }
		}else{
			if($compId){
			    $query = "select GROUP_CONCAT(title  ORDER BY title ASC  SEPARATOR ', ') FROM {$CFG->prefix}company WHERE id IN (".$compId.") AND deleted = '0'";
				if($isReport == false){
				 $query .= " AND status = '1'";
				}
				$query .= " ORDER BY title ASC";
				$result = $DB->get_field_sql($query);
			}else{
			    $query = "select * FROM {$CFG->prefix}company WHERE deleted = '0' and title != ''";
				if($isReport == false){
				 $query .= " AND status = '1'";
				}
				$query .= " ORDER BY title ASC";
			    $result = $DB->get_records_sql($query);
			}
        }
			
		
		return $result;
	}
	
	/**
	 * get job title or all results
	 *
	 * @param mixed $jobId is job id
	 * @return string $result job title or all records (if $jobId is null)
	 */
	 
	 
	function getJobTitles($jobId, $isReport = false){
	
		global $DB, $CFG, $USER;
		$result = '';

		if(is_array($jobId)){
		  $jobIds = count($jobId) > 0 ? implode(",",$jobId) : 0;
		  if($jobIds){
		    $query = "select GROUP_CONCAT(title  ORDER BY title ASC  SEPARATOR ', ') FROM {$CFG->prefix}job_title WHERE id IN (".$jobIds.") AND deleted = '0'";
			if($isReport == false){
			 $query .= " AND status = '1'";
			}
			$query .= " ORDER BY title ASC";
		    $result = $DB->get_field_sql($query);
		  }
		}else{
			if($jobId){
			    $query = "select GROUP_CONCAT(title  ORDER BY title ASC  SEPARATOR ', ') FROM {$CFG->prefix}job_title WHERE id IN (".$jobId.") AND deleted = '0'";
				if($isReport == false){
				 $query .= " AND status = '1'";
				}
				$query .= " ORDER BY title ASC";
				$result = $DB->get_field_sql($query);
			}else{
			    $query = "select * FROM {$CFG->prefix}job_title WHERE deleted = '0' and title != '' ";
				if($isReport == false){
				 $query .= " AND status = '1'";
				}
				$query .= " ORDER BY title ASC";
			    $result = $DB->get_records_sql($query);
			}
        }
			
		
		return $result;
	}
	
	function createSelectBoxFor($mform, $for='', $display='', $disableFields = ''){
	
		global $CFG, $USER, $DB;
		
		if($for !=''){
			if($for == 'company'){
			  $recordArr = getCompany();
			  $canShow = $CFG->showCompany;
			}elseif($for == 'job_title'){
			  $recordArr = getJobTitles();
			  $canShow = $CFG->showJobTitle;
			}
			
			if($canShow == 1 ){
				$selectArr = array('0'=>get_string('select_'.$for, 'user'));
				foreach($recordArr as $records){
					$selectArr[$records->id] = $records->title;
				}

				$mform->addElement('html', '<div id = "'.$for.'-box" '.$display.'>');
				$mform->addElement('select', $for, get_string($for, 'user'), $selectArr, $disableFields);
				$mform->addElement('html', '</div>');
			  }	
		  }	  
	}
	
	function createSelectBoxFilterFor($for, $idElement, $selectedArr = array(0), $multiple=true, $display=''){
	   
		global $CFG, $USER, $DB;
		$HTML = '';
		$HTMLOptions = '';
		
		if($for !=''){
		
			if($for == 'company'){
			  $recordArr = getCompany();
			  $canShow = $CFG->showCompany;
			  $optLbl = get_string('all_company','user');
			}elseif($for == 'job_title'){
			  $recordArr = getJobTitles();
			  $canShow = $CFG->showJobTitle;
			  $optLbl = get_string('all_job_title','user');
			}
			
			if($canShow == 1 ){

					$selectedC = in_array('-1', $selectedArr)?'selected="selected"':'';
					$multiple = $multiple?'multiple="multiple"':'';
					$style = $multiple==false?'':'';
					$size = $multiple?'size="2"':'';
					$HTML .= '<select autocomplete="off" '.$size.' '.$multiple.' id="'.$idElement.'" name="'.$idElement.'" '.$display.'  style="'.$style.'">';
					$HTMLOptions .= '<option value="-1" '.$selectedC.' >'.$optLbl.'</option>';
					
					if(count($recordArr) > 0 ){
					   
					   if(!empty($recordArr)){
							foreach($recordArr as $rows){
								 $selectedC = count($selectedArr) > 0 && in_array($rows->id, $selectedArr)?'selected="selected"':'';
								 $HTMLOptions .= '<option value="'.$rows->id.'" '.$selectedC.' >'.$rows->title.'</option>';
							}
						}
					}
					$HTML .=  $HTMLOptions;
					
					$HTML .= '</select>';
					
			}
			
		}			
		return  $HTML;
        
	}
	
	function getSelectFilterCondition($data){
	  //ini_set('display_errors', 1);
  	  //error_reporting(E_ALL);

	   $where = ''; 
	   $paramArray = array(
				'department' => $data->department,
				'team' => $data->team,
				'managers' => $data->managers,
				'roles' => $data->roles,
				'sel_mode' => $data->selMode,
				'user_group' => $data->userGroup,
				'job_title' => $data->job_title,
				'company' => $data->company
			  );
			
		   if($data->department == '-1' && $data->team == '-1' &&  $data->managers == '-1' && $data->roles == '-1' && $data->userGroup == '-1' && $data->job_title == '-1' && $data->company == '-1'&& $data->country == '-1'){
		    
			  if($paramArray['sel_mode'] == 2){
					$usersArr = getUserListing($paramArray); 
					 if(count($usersArr) > 0 ){
						 $usersArrIds = array_keys($usersArr);
						 $userIds = implode(",", $usersArrIds);
						 $where .= " AND u.id IN (".$userIds.") ";
					 }else{
					   $where .= " AND u.id IN (0) ";
					 }
				}
				
		   }else{ 
		     $usersArr = getUserListing($paramArray);
			 
		     if(count($usersArr) > 0 ){
				 $usersArrIds = array_keys($usersArr);
				 $userIds = implode(",", $usersArrIds);
				 $where .= " AND u.id IN (".$userIds.") ";
			 }else{
		       $where .= " AND u.id IN (0) ";
		     }
		   }
		   
	
	     return $where;
	}
	
	function extractMDash($data){
	 
	 $edata = $data;
	 if($data){
	   $edata = iconv('UTF-8','ASCII//TRANSLIT',$data);
	 }
	 return $edata;
	
	}
	
	function extractMDashInSql($field){
	 
	 $eField = $field;
	 if($field){
	   $eField = "REPLACE(REPLACE(".$field.", 0xE28093, '-'), 0xE28094, '-')";
	 }
	 return $eField;
	
	}
	
	function getUserAndCourseReports($searchString, $page, $perpage, $sTypeArr){
	
	  global $DB, $USER, $CFG;

	  $reportArr = array();		
	  $finalArr = array();
	  $needCourses = array();

        $fullnameField = extractMDashInSql('fecdu.fullname');	 
		$queryFields = "SELECT @a:=@a+1 `serial_number`, fecdu.pid, fecdu.courseid, fecdu.enrolled_user, $fullnameField as fullname, fecdu.f_course_status, fecdu.user_mapping_status";
					 
		$query = " FROM vw_final_enrolled_course_details_of_users fecdu ";
		$query .= " LEFT JOIN mdl_course mc ON (mc.id = fecdu.courseid) ";
		$query .= " LEFT JOIN mdl_user mu ON (mu.id = fecdu.enrolled_user) ";
		$query .= " LEFT JOIN mdl_department_members AS mdm ON (mdm.userid = fecdu.enrolled_user) "; 
	    $query .= " LEFT JOIN mdl_department AS md ON (md.id = mdm.departmentid) ";
		$query .= " ,(SELECT @a:= 0) AS X ";
		$query .= "  WHERE 1 = 1 AND md.deleted = '0' AND mu.deleted = '0'";
		$query .= "  AND mc.coursetype_id = '".$CFG->courseTypeOnline."' ";
		$query = $queryFields.$query;
		$query .= " $searchString ";
		$query .= "  group by fecdu.courseid, fecdu.enrolled_user ";
		$query .= " ORDER BY fecdu.fullname ASC ";

		$allCourseArr = $DB->get_records_sql($query);
		$allCourseCount = count($allCourseArr);
		
		if($allCourseCount > 0 ){
		
		  $total_ip_count = 0;
		  $total_cl_count = 0;
		  $total_ns_count = 0;
		  foreach($allCourseArr as $arr){
		  
		      if(isset($sTypeArr) && $sTypeArr[0] != '-1' && in_array($arr->f_course_status, $sTypeArr)){
			    $needCourses[] = $arr->courseid;
			  }
		  
		      $reportArr['courses'][$arr->courseid]->courseid = $arr->courseid;
		      $reportArr['courses'][$arr->courseid]->pid = $arr->pid;
			  $reportArr['courses'][$arr->courseid]->enrolled_user = $arr->enrolled_user;
			  $reportArr['courses'][$arr->courseid]->fullname = $arr->fullname;
			  $reportArr['courses'][$arr->courseid]->f_course_status = $arr->f_course_status;
			  $reportArr['courses'][$arr->courseid]->user_mapping_status = $arr->user_mapping_status;
			  
			  if($arr->f_course_status == 1){
			    $reportArr['courses'][$arr->courseid]->IP_COUNT += 1;
				$total_ip_count++;
			  }elseif($arr->f_course_status == 2){
			    $reportArr['courses'][$arr->courseid]->CL_COUNT += 1;
				$total_cl_count++;
			  }elseif($arr->f_course_status == 3){
			    $reportArr['courses'][$arr->courseid]->NS_COUNT += 1;
				$total_ns_count++;
			  }
		  }
		  
		  $reportArr['total_ip_count'] = $total_ip_count;
		  $reportArr['total_cl_count'] = $total_cl_count;
		  $reportArr['total_ns_count'] = $total_ns_count;
		    
		}
		
		 $finalArr['reportArr'] = $reportArr;
		 if($perpage){
		  $offset = $page - 1;
		  $offset = $offset*$perpage;
		  
			  if(isset($sTypeArr) && $sTypeArr[0]!= '-1' && count($reportArr['courses']) > 0 ){
			  
			    $limitCourses = array();
				foreach($reportArr['courses'] as $arr){
				  if(in_array($arr->courseid, $needCourses)){
					$limitCourses[] = $arr;
				  }
				}
				
			    $finalArr['limitCount'] = count($limitCourses);

				$limitReportArr = array_splice($limitCourses, $offset, $perpage);
				 
			  }else{
			    $finalArr['limitCount'] = count($reportArr['courses']);
				$limitReportArr = array_splice($reportArr['courses'], $offset, $perpage);
			  }
		 }else{
		   $finalArr['limitCount'] = count($reportArr['courses']);
		   $limitReportArr = $reportArr['courses'];
		 }

		 $finalArr['limitReportArr'] = $limitReportArr;
		
		
				
	
		
		return $finalArr;

	}
	
	function getAllSystemAllowedTypes(){
	
	    /*$allowedTypesArr = get_mimetypes_array();
		$allowedTypes = count($allowedTypesArr) > 0?array_keys($allowedTypesArr):'*';
		array_walk($allowedTypes, function(&$item) { $item = '.'.$item; });*/
		
		$allowedTypes = '*';
		/*$allowedTypes = array('audio', 'video', 'document','TEXT','Text Document','.txt','.log','.LOG','.jpg','.png','.jpeg','.gif',
		'.3gp','.avi','.bmp','.csv','.doc','.docx','.flv','.gtar','.tgz','.gz'
		,'.gzip','.mov','.movie','.m3u','.mp3','.mp4','.m4v','.m4a','.mpeg','.mpe','.mpg','.ogg','.ogv','.pdf','.ppt','.pptx','.pptm','.rtf','.svg','.svgz'
		,'.swf','.swfl','.swa','.tar','.wav','.webm','.wmv','.xls','.xlsx','.xlsm','.xltx','.xltm','.xml','.xsl','.zip');*/
		
		return $allowedTypes;
	
	}
	
	function getTimeStampOfGivenDate($dateString = '') {
	    
		// Note: time() and getTimestamp() both work ok in 64bit environment (linux) after year 2038 .
	   // ini_set('display_errors', 1);
  	    //error_reporting(E_ALL);
		
	    if( ($dateString!="0000-00-00 00:00:00") && ($dateString!="") ){
		    //$dateString =  "2037-01-15 12:12:00";
	    	$dateObj = new DateTime($dateString);
			//$dateFormat = $dateObj->format($dateFormat);
			$getTimestamp = $dateObj->getTimestamp();
	    	return $getTimestamp;
		}else {
			return "";
		}
	}
	
	function getTimeStampOfDate($dateString){
	
	  $ftimestamp = 0;
	  if($dateString){
				
			list($month, $day, $year) = explode('-', $dateString);

			if($year < 2038){
			  $timestamp =  mktime(0, 0, 0, $month, $day, $year);
			  $ftimestamp = $timestamp;
			}else{
			    $timestamp =  mktime(0, 0, 0, $month, $day, 2037);
				$secs = 0;
				for($y = 2038; $y <= $year; $y++){
				   if(checkdate(2,29, $y)){
				      $secs += 366*24*60*60;
				   }else{
				      $secs += 365*24*60*60;
				   }
				}
				
				$ftimestamp = $timestamp + $secs;
			}
			
			return $ftimestamp;	
		
	   }	
	
	}
	
	function getFormattedTimeStampOfDate($dateString, $enddate = false){
	
	  $dateArr = 0;
	  if($dateString){
				
			$timestamp = 0;	
			list($month, $day, $year) = explode('-', $dateString);
			$dateYMD = $year."/".$month."/".$day;
			if($year < 2038){
			  if($enddate){
			    $timestamp = mktime(23, 59, 59, $month, $day, $year);
			  }else{
			    $timestamp = mktime(0, 0, 0, $month, $day, $year);
			  }
			  
			  $dateMDY = date("m/d/Y", $timestamp);
			}else{
			
			  $dateMDY = $month."/".$day."/".$year;
			  $timestamp = getTimeStampOfDate($dateString);
			  
			}

			if($dateMDY){
			 $dateArr = array();
			 $dateArr['dateMDY'] = $dateMDY;
			 $dateArr['timestamp'] = $timestamp;
			}

	   }	
	   	return $dateArr;
	
	}
	
	
	function setTeamActiveFlag($gid, $flagCheck) {
		global $DB;
		$data = new stdClass();

		$data->is_active = $flagCheck;
		$data->id = $gid;
		$nid = $DB->update_record('groups', $data);

		return true;
	}
	
	function getOverallCourseCreditReportForLnI($searchString='', $searchStringI='', $searchStringL='', $paramArray, $sort, $dir){
	       global $DB, $USER, $CFG;
	        $sUserType = $CFG->userTypeInstructor;
	        
	        $recordArrInstructor = getOverallCourseCreditReport($sUserType, $searchString, $searchStringI, $searchStringL, $paramArray, $sort, $dir);
	      //  $recordArrInstructor = getOverallCourseCreditReportByCronTable($sUserType, $searchString, $searchStringI, $searchStringL, $paramArray, $sort, $dir);
			//pr($recordArrInstructor);
			$sUserType = $CFG->userTypeStudent;
	        $recordArrLearner = getOverallCourseCreditReport($sUserType, $searchString, $searchStringI, $searchStringL, $paramArray, $sort, $dir);
	      //  $recordArrLearner = getOverallCourseCreditReportByCronTable($sUserType, $searchString, $searchStringI, $searchStringL, $paramArray, $sort, $dir);
			
			
			//pr($recordArrLearner);die;
			
			
			$recordArr['creditHoursArr'][1] = $recordArrInstructor['creditHoursArr'][1] + $recordArrLearner['creditHoursArr'][1];
			$recordArr['creditHoursArr'][2] = $recordArrInstructor['creditHoursArr'][2] + $recordArrLearner['creditHoursArr'][2];
			
			if(isset($recordArrLearner['allUserArr']['users']) && count($recordArrLearner['allUserArr']['users']) > 0 ){
			    foreach($recordArrLearner['allUserArr']['users'] as $userID => $users){
				    if(isset($recordArrInstructor['allUserArr']['users'][$userID])){
						$recordArr['allUserArr']['users'][$userID]->userid = $users->userid;
						$recordArr['allUserArr']['users'][$userID]->userfullname = $users->userfullname;
						$recordArr['allUserArr']['users'][$userID]->username = $users->username;
						$recordArr['allUserArr']['users'][$userID]->departmenttitle = $users->departmenttitle;
						$recordArr['allUserArr']['users'][$userID]->departmentid = $users->departmentid;
						$recordArr['allUserArr']['users'][$userID]->usermanager = $users->usermanager;
						$recordArr['allUserArr']['users'][$userID]->userfullname = $users->userfullname;
						$recordArr['allUserArr']['users'][$userID]->credithours = $recordArrInstructor['allUserArr']['users'][$userID]->credithours + $users->credithours;
						$recordArr['allUserArr']['users'][$userID]->course_count = $recordArrInstructor['allUserArr']['users'][$userID]->course_count + $users->course_count;
						
						$recordArr['allUserArr']['users'][$userID]->online_credithours = $recordArrInstructor['allUserArr']['users'][$userID]->online_credithours + $users->online_credithours;
						$recordArr['allUserArr']['users'][$userID]->online_course_count = $recordArrInstructor['allUserArr']['users'][$userID]->online_course_count + $users->online_course_count;
						$recordArr['allUserArr']['users'][$userID]->classroom_credithours = $recordArrInstructor['allUserArr']['users'][$userID]->classroom_credithours + $users->classroom_credithours;
						$recordArr['allUserArr']['users'][$userID]->classroom_course_count = $recordArrInstructor['allUserArr']['users'][$userID]->classroom_course_count + $users->classroom_course_count;
						
					}else{
						$recordArr['allUserArr']['users'][$userID]->userid = $users->userid;
						$recordArr['allUserArr']['users'][$userID]->userfullname = $users->userfullname;
						$recordArr['allUserArr']['users'][$userID]->username = $users->username;
						$recordArr['allUserArr']['users'][$userID]->departmenttitle = $users->departmenttitle;
						$recordArr['allUserArr']['users'][$userID]->departmentid = $users->departmentid;
						$recordArr['allUserArr']['users'][$userID]->usermanager = $users->usermanager;
						$recordArr['allUserArr']['users'][$userID]->userfullname = $users->userfullname;
						$recordArr['allUserArr']['users'][$userID]->credithours += $users->credithours;
						$recordArr['allUserArr']['users'][$userID]->course_count += $users->course_count;
						
						$recordArr['allUserArr']['users'][$userID]->online_credithours += $users->online_credithours;
						$recordArr['allUserArr']['users'][$userID]->online_course_count += $users->online_course_count;
						$recordArr['allUserArr']['users'][$userID]->classroom_credithours += $users->classroom_credithours;
						$recordArr['allUserArr']['users'][$userID]->classroom_course_count += $users->classroom_course_count;
					}
				}
			}
			
			
			if(isset($recordArrInstructor['allUserArr']['users']) && count($recordArrInstructor['allUserArr']['users']) > 0 ){
			    foreach($recordArrInstructor['allUserArr']['users'] as $userID => $users){
				    if(!isset($recordArrLearner['allUserArr']['users'][$userID])){
						$recordArr['allUserArr']['users'][$userID]->userid = $users->userid;
						$recordArr['allUserArr']['users'][$userID]->userfullname = $users->userfullname;
						$recordArr['allUserArr']['users'][$userID]->username = $users->username;
						$recordArr['allUserArr']['users'][$userID]->departmenttitle = $users->departmenttitle;
						$recordArr['allUserArr']['users'][$userID]->departmentid = $users->departmentid;
						$recordArr['allUserArr']['users'][$userID]->usermanager = $users->usermanager;
						$recordArr['allUserArr']['users'][$userID]->userfullname = $users->userfullname;
						$recordArr['allUserArr']['users'][$userID]->credithours = $users->credithours;
						$recordArr['allUserArr']['users'][$userID]->course_count = $users->course_count;
						
						$recordArr['allUserArr']['users'][$userID]->online_credithours = $users->online_credithours;
						$recordArr['allUserArr']['users'][$userID]->online_course_count = $users->online_course_count;
						$recordArr['allUserArr']['users'][$userID]->classroom_credithours = $users->classroom_credithours;
						$recordArr['allUserArr']['users'][$userID]->classroom_course_count = $users->classroom_course_count;
					}
				}
			}
			
			
			
			if(isset($recordArrInstructor['userCourseArr']) && count($recordArrInstructor['userCourseArr']) > 0 ){
			    foreach($recordArrInstructor['userCourseArr'] as $userId => $users){
			    	
			    	if(count($recordArrInstructor['userCourseArr'][$userId]) > 0 && $recordArrLearner['userCourseArr'][$userId] > 0 ){
			    		$userCourseArr[$userId] = array_merge($recordArrInstructor['userCourseArr'][$userId], $recordArrLearner['userCourseArr'][$userId]);
			    	}elseif(count($recordArrInstructor['userCourseArr'][$userId]) == 0 && $recordArrLearner['userCourseArr'][$userId] > 0 ){
			    		$userCourseArr[$userId] = $recordArrLearner['userCourseArr'][$userId];
			    	}elseif(count($recordArrInstructor['userCourseArr'][$userId]) > 0 && $recordArrLearner['userCourseArr'][$userId] == 0 ){
			    		$userCourseArr[$userId] = $recordArrInstructor['userCourseArr'][$userId];
			    	}
			    	
				  // $userCourseArr[$userId] = array_merge($recordArrInstructor['userCourseArr'][$userId], $recordArrLearner['userCourseArr'][$userId]);
				   $userCourseArr[$userId] = array_unique($userCourseArr[$userId]);
				}
			}
			
			if(isset($recordArrLearner['userCourseArr']) && count($recordArrLearner['userCourseArr']) > 0 ){
			    foreach($recordArrLearner['userCourseArr'] as $userId => $users){
			    	
			    	 if(count($recordArrInstructor['userCourseArr'][$userId]) > 0 && $recordArrLearner['userCourseArr'][$userId] > 0 ){	
				      $userCourseArr[$userId] = array_merge($recordArrInstructor['userCourseArr'][$userId], $recordArrLearner['userCourseArr'][$userId]);
			    	 }elseif(count($recordArrInstructor['userCourseArr'][$userId]) == 0 && $recordArrLearner['userCourseArr'][$userId] > 0 ){
			    	 	$userCourseArr[$userId] = $recordArrLearner['userCourseArr'][$userId];
			    	 }elseif(count($recordArrInstructor['userCourseArr'][$userId]) > 0 && $recordArrLearner['userCourseArr'][$userId] == 0 ){
			    	 	$userCourseArr[$userId] = $recordArrInstructor['userCourseArr'][$userId];
			    	 }
				     
				     $userCourseArr[$userId] = array_unique($userCourseArr[$userId]);
				      

				}
			}
			
			//pr($userCourseArr);die;
			if(isset($userCourseArr) && count($userCourseArr) > 0 ){
			    foreach($userCourseArr as $userId => $users){
				      $recordArr['allUserArr']['users'][$userId]->classroom_course_count = count($users);
				}
			}

			//pr($recordArr);die;
			return $recordArr;
				
	}
	
	function getOverallCourseCreditReport($sUserType, $searchString='', $searchStringI='', $searchStringL='', $paramArray, $sort, $dir){
	
	   global $DB, $USER, $CFG;

	   $creditHoursArr = array();
	   $allUserArr = array();
	   
	   $excludedUsers = count($CFG->excludedUsers) > 0 ? implode(",",$CFG->excludedUsers) : 0;
	   
	   $sStartDate     = $paramArray['startDate'];
	   $sEndDate       = $paramArray['endDate'];
	   $selMode       = $paramArray['sel_mode'];
			
	   
	   $sDateSelected = '';
	   if($sStartDate){
			//list($month1, $day1, $year1) = explode('-', $sStartDate);
			//$sDateSelected = date("m/d/Y",mktime(0, 0, 0, $month1, $day1, $year1));
			$sDateArr = getFormattedTimeStampOfDate($sStartDate);
			$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';

	   }		   
		
	   $eDateSelected = '';
	   if($sEndDate){
			//list($month2, $day2, $year2) = explode('-', $sEndDate);
			//$eDateSelected = date("m/d/Y",mktime(0, 0, 0, $month2, $day2, $year2));
		   $eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
		   $eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
	   }
			
			
	   if($sUserType == $CFG->userTypeInstructor){
		
			$queryAll = " SELECT  @a:=@a+1 `serial_number`, id , count(courseid) as course_count, userfullname, username, deleted as is_user_deleted, departmenttitle, departmentid, usermanager, sum(duration) AS credithours, courseid, fullname as coursename, coursetype_id, criteria, is_course_deleted, is_course_publish, class_endtime as complition_time, class_id, class_name, class_instructor, session_id, sessionname, jobid, comid, job_title, company  FROM vw_credit_hours_of_instructor, (SELECT @a:= 0) AS X WHERE 1 = 1";
			$queryAll .= " AND id NOT IN (".$excludedUsers.") ";
			$queryAll .= " AND deleted = '0' ";
			//$queryGraph = $queryAll." GROUP BY courseid, session_id, class_instructor ORDER BY id ASC";
			$queryAll .= " $searchString $searchStringI";
			$queryAll .= " GROUP BY courseid, class_id, session_id, class_instructor  ";
			//$queryAll .= " ORDER BY id ASC";
			if($sort){
			  $queryAll .= " ORDER BY $sort $dir";
			}
			
			
		}else{
			$queryAll = " SELECT  @a:=@a+1 `serial_number`, id, userfullname, username, departmenttitle, departmentid, usermanager, credithours, courseid, fullname as coursename, coursetype_id, criteria, complition_time, jobid, comid, job_title, company  FROM vw_credit_hours_of_learners, (SELECT @a:= 0) AS X WHERE 1 = 1";
			$queryAll .= " AND id NOT IN (".$excludedUsers.") ";
			$queryAll .= " AND deleted = '0' ";
			//$queryGraph = $queryAll." ORDER BY $sort $dir";
			$queryAll .= " $searchString $searchStringL";
			if($sort){
			  $queryAll .= " ORDER BY $sort $dir";
			}
			
		}	
		
		//echo $queryAll;die;

			$allSystemUserAllArr = $DB->get_records_sql($queryAll);
			//$allSystemUserAllForGraphArr = $DB->get_records_sql($queryGraph);

			//pr($allSystemUserAllArr);die;

			$creditHoursArr = array();
			$allUserArr = array();
			
			$sStartDateTime = '';
			$sEndDateTime = '';
			if($sDateSelected){ 
				//list($month, $day, $year) = explode('/', $sDateSelected);
				//$sStartDateTime =  mktime(0, 0, 0, $month, $day, $year);
				 $sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
			}
			
			if($eDateSelected){
			   //list($month, $day, $year) = explode('/', $eDateSelected);
			   //$sEndDateTime = mktime(23, 59, 59, $month, $day, $year);
			    $sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
			}
			
			$curTime = time();				
			
			//pr($allSystemUserAllArr);die;
			if(count($allSystemUserAllArr) > 0 ){
			      $totalDepCHours = 0;
				  $userCourseArr = array();
				  $x=0;
				  foreach($allSystemUserAllArr as $sArr){
				  
				    
					 $online_credithours = 0;
					 $online_course_count = 0;
					 $classroom_credithours = 0;
					 $classroom_course_count = 0;
							
					 if($sArr->id){
					 
							$allUserArr['users'][$sArr->id]->userid = $sArr->id;
							$allUserArr['users'][$sArr->id]->userfullname = $sArr->userfullname;
							$allUserArr['users'][$sArr->id]->username = $sArr->username;
							$allUserArr['users'][$sArr->id]->departmenttitle = $sArr->departmenttitle;
							$allUserArr['users'][$sArr->id]->departmentid = $sArr->departmentid;
							$allUserArr['users'][$sArr->id]->usermanager = $sArr->usermanager;
							$allUserArr['users'][$sArr->id]->userfullname = $sArr->userfullname;
							$allUserArr['users'][$sArr->id]->courseid = $sArr->courseid;
							$allUserArr['users'][$sArr->id]->coursename = $sArr->coursename;
							$allUserArr['users'][$sArr->id]->coursetype_id = $sArr->coursetype_id;
							$allUserArr['users'][$sArr->id]->criteria = $sArr->criteria;
							$allUserArr['users'][$sArr->id]->complition_time = $sArr->complition_time;
							$allUserArr['users'][$sArr->id]->credithours = $sArr->credithours;
							$allUserArr['users'][$sArr->id]->course_count = $sArr->course_count;
					

					   $canAddCreditHours = 0; 		
					   if($sArr->coursetype_id){		
					   
					        // going to check can we add credit hours between giving learning date for a course
					        $complitionTime = $sArr->complition_time;
							//echo date("Y-m-d",$complitionTime)."<br>";
					        if($complitionTime){
							
							   if($sUserType == $CFG->userTypeInstructor){
							   
							         if($complitionTime < $curTime){
								       
										//echo date("Y-m-d H:i:s", $complitionTime)."===".date("Y-m-d  H:i:s",$curTime)."<br>";
										//echo $complitionTime."===".$curTime."<br>";
										//echo $sStartDateTime."===".$sEndDateTime."<br>";
										if($sStartDateTime && $sEndDateTime){ 
			
											if($complitionTime >= $sStartDateTime && $complitionTime <= $sEndDateTime ){
											   $canAddCreditHours = 1;
											}
										}elseif($sStartDateTime && $sEndDateTime == ''){ 
											if($complitionTime >= $sStartDateTime ){
											   $canAddCreditHours = 1;
											}
										}elseif($sStartDateTime == ''  && $sEndDateTime){ 
											if($complitionTime <= $sEndDateTime ){
											   $canAddCreditHours = 1;
											}
										}
										
										  //echo $canAddCreditHours."==$sArr->courseid<br>";
									}	
								}else{
								
									//echo date("Y-m-d H:i:s", $complitionTime)."===".date("Y-m-d  H:i:s",$curTime)."<br>";
									//echo $complitionTime."===".$curTime."<br>";
									if($sStartDateTime && $sEndDateTime){ 
										if($complitionTime >= $sStartDateTime && $complitionTime <= $sEndDateTime ){
										   $canAddCreditHours = 1;
										}
									}elseif($sStartDateTime && $sEndDateTime == ''){ 
										if($complitionTime >= $sStartDateTime ){
										   $canAddCreditHours = 1;
										}
									}elseif($sStartDateTime == ''  && $sEndDateTime){ 
										if($complitionTime <= $sEndDateTime ){
										   $canAddCreditHours = 1;
										}
									}
								}
							}
							
							// going to add credit hours between giving learning date for a course
							if($canAddCreditHours == 1){
			
					
							  if($sArr->departmentid){
								 $credithours = 0;
								 $credithours = $sArr->credithours?$sArr->credithours:0;
								 $totalDepCHours += $credithours;
								 //$creditHoursArr['creditHoursInDept'][$sArr->departmentid] += $credithours;
								 
							  }
					 
					 
							   if($sArr->coursetype_id == $CFG->courseTypeOnline){
									$online_credithours = $sArr->credithours;
									$online_course_count = $sArr->courseid?1:0;
							   }else if($sArr->coursetype_id == $CFG->courseTypeClassroom){  
									$classroom_credithours = $sArr->credithours;
									$classroom_course_count = $sArr->courseid?1:0;
							   }
							   
							   if(isset($sArr->coursetype_id) && $sArr->coursetype_id){
							     $creditHoursArr[$sArr->coursetype_id] += $credithours;
							   }
						   }
					    } 
                        					   
					        $allUserArr['users'][$sArr->id]->online_credithours += $online_credithours;
							$allUserArr['users'][$sArr->id]->online_course_count += $online_course_count;
							$allUserArr['users'][$sArr->id]->classroom_credithours += $classroom_credithours;
							
							if($sUserType == $CFG->userTypeInstructor){  
							
							  if($canAddCreditHours){
									if( !in_array($sArr->courseid, $userCourseArr[$sArr->id]) ){  
										 //echo $canAddCreditHours."==$sArr->courseid<br>";
										  $userCourseArr[$sArr->id][] = $sArr->courseid;  
										  $allUserArr['users'][$sArr->id]->classroom_course_count += 1;
									}
							   } 
							   
							}else{
							
							   if($canAddCreditHours){
									if($sArr->coursetype_id == $CFG->courseTypeClassroom && !in_array($sArr->courseid, $userCourseArr[$sArr->id]) ){  
										 //echo $canAddCreditHours."==$sArr->courseid<br>";
										 //pr($userCourseArr);
										  $userCourseArr[$sArr->id][] = $sArr->courseid;  
										  $allUserArr['users'][$sArr->id]->classroom_course_count += 1;
									}
							   }
							   

							}
					 }
				  }
				  
				 // $creditHoursArr['totalCreditHoursInDept'] = $totalDepCHours;
			}
			
			//pr($userCourseArr);die;

			
			$recordArr['creditHoursArr'] = $creditHoursArr;
			$recordArr['allUserArr'] = $allUserArr;
			$recordArr['userCourseArr'] = $userCourseArr;
			return $recordArr;
	
	
	}
	
	function getOverallUserCourseCreditReport($sUserType, $searchStringI='', $searchStringL='', $paramArray, $sort, $dir){
	
	    global $DB, $USER, $CFG;

		$allCourses = array();
		$allCoursesCnt = 0;
		$crediHoursForGraph = array();
		
		$userId    = $paramArray['uid'];
		$curTime = time();	
		if($sUserType == $CFG->userTypeInstructor){
		
			/*$queryAll = " SELECT  @a:=@a+1 `serial_number`, chi.id , chi.userfullname, chi.username, chi.deleted as is_user_deleted, chi.departmenttitle, chi.departmentid, chi.usermanager, sum(chi.duration) AS credithours, chi.courseid, chi.fullname as coursename, chi.coursetype_id, chi.criteria, chi.is_course_deleted, chi.is_course_publish, chi.class_endtime as complition_time, FROM_UNIXTIME(chi.class_endtime) as complition_date, now() as curdate, chi.class_id, chi.class_name, chi.class_instructor, chi.session_id, chi.sessionname, mct.coursetype   ";
			$queryAll .= " FROM vw_credit_hours_of_instructor chi";
			$queryAll .= " LEFT JOIN mdl_coursetype AS mct ON (mct.id = chi.coursetype_id)";
			$queryAll .= " , (SELECT @a:= 0) AS X";
			$queryAll .= " WHERE 1 = 1 ";
			$queryAll .= " AND deleted = '0' ";
			$queryAll .= " AND chi.class_instructor = '".$userId."' ";
			$queryAll .= " AND chi.class_endtime < ".$curTime."";
			$queryAll .= " $searchString";
			$queryAll .= " GROUP BY chi.courseid, class_instructor";*/
			
			$fullnameField = extractMDashInSql('chi.fullname');	
			$classNameField = extractMDashInSql('chi.class_name');
			$queryAll = " SELECT  @a:=@a+1 `serial_number`, chi.id , chi.userfullname, chi.username, chi.deleted as is_user_deleted, chi.departmenttitle, chi.departmentid, chi.usermanager, (chi.duration) AS credithours, chi.courseid, $fullnameField as coursename, chi.coursetype_id, chi.criteria, chi.is_course_deleted, chi.is_course_publish, chi.class_endtime as complition_time, FROM_UNIXTIME(chi.class_endtime) as complition_date, now() as curdate, chi.class_id, $classNameField as class_name, chi.class_instructor, chi.session_id, chi.sessionname, chi.coursetype   ";
			
			$queryAll .= " FROM vw_credit_hours_of_instructor chi";
			$queryAll .= " , (SELECT @a:= 0) AS X";
			$queryAll .= " WHERE 1 = 1 ";
			$queryAll .= " AND deleted = '0' ";
			$queryAll .= " AND chi.class_instructor = '".$userId."' ";
			$queryAll .= " AND chi.class_endtime < ".$curTime."";
			$queryAll .= " $searchStringI";
			//$queryAll .= " GROUP BY chi.courseid, class_instructor";
			$queryAll .= " ORDER BY chi.fullname ASC";
			
			$allClasses = $DB->get_records_sql($queryAll);
			$allClassesCnt = count($allClasses);
			
			$classOfCourseUsers = array();
			if($allClassesCnt > 0 ){
			   $i = 0;
			   foreach($allClasses as $allClassesObj){
				   $classOfCourseUsers[$allClassesObj->class_instructor][$allClassesObj->courseid]->class_instructor = $allClassesObj->class_instructor;
				   $classOfCourseUsers[$allClassesObj->class_instructor][$allClassesObj->courseid]->courseid = $allClassesObj->courseid;
				   $classOfCourseUsers[$allClassesObj->class_instructor][$allClassesObj->courseid]->coursename = $allClassesObj->coursename;
				   $classOfCourseUsers[$allClassesObj->class_instructor][$allClassesObj->courseid]->coursetype_id = $allClassesObj->coursetype_id;
				   $classOfCourseUsers[$allClassesObj->class_instructor][$allClassesObj->courseid]->complition_time = $allClassesObj->complition_time;
				   $classOfCourseUsers[$allClassesObj->class_instructor][$allClassesObj->courseid]->coursetype = $allClassesObj->coursetype;
				   $classOfCourseUsers[$allClassesObj->class_instructor][$allClassesObj->courseid]->criteria = $allClassesObj->criteria;
				   $classOfCourseUsers[$allClassesObj->class_instructor][$allClassesObj->courseid]->credithours += $allClassesObj->credithours;
				   $classOfCourseUsers[$allClassesObj->class_instructor][$allClassesObj->courseid]->class_details[$allClassesObj->class_id]['class_id'] = $allClassesObj->class_id;
				   $classOfCourseUsers[$allClassesObj->class_instructor][$allClassesObj->courseid]->class_details[$allClassesObj->class_id]['class_name'] = $allClassesObj->class_name;
				  
				   $classOfCourseUsers[$allClassesObj->class_instructor][$allClassesObj->courseid]->class_details[$allClassesObj->class_id]['credithours'] += $allClassesObj->credithours;
				   $classOfCourseUsers[$allClassesObj->class_instructor][$allClassesObj->courseid]->class_details[$allClassesObj->class_id]['complition_time'] = $allClassesObj->complition_time;
				   $crediHoursForGraph[$allClassesObj->coursetype_id] += $allClassesObj->credithours;
				   $i++;
			   }
			   
			   $allCourses = isset($classOfCourseUsers[$userId])?$classOfCourseUsers[$userId]:array();
			   $allCoursesCnt = count($allCourses);
			}
			
		//pr($allCourses);die;
		}else{
		
			/*$queryAll = " SELECT  @a:=@a+1 `serial_number`, cchc.credithours, cchc.courseid, cchc.fullname as coursename, cchc.coursetype_id, cchc.criteria, cchc.complition_time  ,mct.coursetype";
			$queryAll .= " FROM vw_combined_credit_hours_of_course as cchc";
			$queryAll .= " LEFT JOIN mdl_coursetype AS mct ON (mct.id = cchc.coursetype_id)";
			$queryAll .= ", (SELECT @a:= 0) AS X WHERE 1 = 1";
			$queryAll .= " AND cchc.enrolled_user = '".$userId."'";
			$queryAll .= " $searchString";
			$queryAll .= " ORDER BY $sort $dir";*/
			
			$fullnameField = extractMDashInSql('cchl.fullname');	
			$classNameField = extractMDashInSql('cchl.class_name');	
			$queryAll = " SELECT  @a:=@a+1 `serial_number`, cchl.class_id, $classNameField as class_name, cchl.credithours, cchl.enrolled_user, cchl.courseid, $fullnameField as coursename, cchl.coursetype_id, cchl.criteria, cchl.complition_time, mct.coursetype";
			$queryAll .= " FROM vw_combined_credit_hours_of_course_and_class as cchl";
			$queryAll .= " LEFT JOIN mdl_coursetype AS mct ON (mct.id = cchl.coursetype_id)";
			$queryAll .= ", (SELECT @a:= 0) AS X WHERE 1 = 1";
			$queryAll .= " AND cchl.enrolled_user = '".$userId."'";
			$queryAll .= " $searchStringL";
			$queryAll .= " ORDER BY coursename, class_name ASC";
			
			$allClasses = $DB->get_records_sql($queryAll);
			$allClassesCnt = count($allClasses);
			
			$classOfCourseUsers = array();
			if($allClassesCnt > 0 ){
			   $i = 0;
			   foreach($allClasses as $allClassesObj){
				   $classOfCourseUsers[$allClassesObj->enrolled_user][$allClassesObj->courseid]->enrolled_user = $allClassesObj->enrolled_user;
				   $classOfCourseUsers[$allClassesObj->enrolled_user][$allClassesObj->courseid]->courseid = $allClassesObj->courseid;
				   $classOfCourseUsers[$allClassesObj->enrolled_user][$allClassesObj->courseid]->coursename = $allClassesObj->coursename;
				   $classOfCourseUsers[$allClassesObj->enrolled_user][$allClassesObj->courseid]->coursetype_id = $allClassesObj->coursetype_id;
				   $classOfCourseUsers[$allClassesObj->enrolled_user][$allClassesObj->courseid]->complition_time = $allClassesObj->complition_time;
				   $classOfCourseUsers[$allClassesObj->enrolled_user][$allClassesObj->courseid]->coursetype = $allClassesObj->coursetype;
				   $classOfCourseUsers[$allClassesObj->enrolled_user][$allClassesObj->courseid]->criteria = $allClassesObj->criteria;
				   $classOfCourseUsers[$allClassesObj->enrolled_user][$allClassesObj->courseid]->credithours += $allClassesObj->credithours;
				   $classOfCourseUsers[$allClassesObj->enrolled_user][$allClassesObj->courseid]->class_details[$allClassesObj->class_id]['class_id'] = $allClassesObj->class_id;
				   $classOfCourseUsers[$allClassesObj->enrolled_user][$allClassesObj->courseid]->class_details[$allClassesObj->class_id]['class_name'] = $allClassesObj->class_name;
				   $classOfCourseUsers[$allClassesObj->enrolled_user][$allClassesObj->courseid]->class_details[$allClassesObj->class_id]['credithours'] = $allClassesObj->credithours;
				   $classOfCourseUsers[$allClassesObj->enrolled_user][$allClassesObj->courseid]->class_details[$allClassesObj->class_id]['complition_time'] = $allClassesObj->complition_time;
				   $crediHoursForGraph[$allClassesObj->coursetype_id] += $allClassesObj->credithours;				   
				   $i++;
			   }
			   
			   $allCourses = isset($classOfCourseUsers[$userId])?$classOfCourseUsers[$userId]:array();
			   $allCoursesCnt = count($allCourses);
			}

		}
		
		$recordArr['crediHoursForGraph'] = $crediHoursForGraph;
		$recordArr['allCourses'] = $allCourses;
		$recordArr['allCoursesCnt'] = $allCoursesCnt;
		
		return $recordArr;
	
	}
	
	
	function getOverallUserCourseCreditReportForLnI($searchStringI='', $searchStringL='', $paramArray, $sort, $dir){
	
	        global $DB, $USER, $CFG;
			
			$userId    = $paramArray['uid'];
			
	        $sUserType = $CFG->userTypeInstructor;
	        $recordArrInstructor = getOverallUserCourseCreditReport($sUserType, $searchStringI, $searchStringL, $paramArray, $sort, $dir);
			//pr($recordArrInstructor);die;
			$sUserType = $CFG->userTypeStudent;
	        $recordArrLearner = getOverallUserCourseCreditReport($sUserType, $searchStringI, $searchStringL, $paramArray, $sort, $dir);
			//pr($recordArrLearner);die;
			$i = 0;	
			
			
			if(isset($recordArrLearner['allCourses']) && count($recordArrLearner['allCourses']) > 0 ){
			   
			    foreach($recordArrLearner['allCourses'] as $courseId => $courses){
				    if(isset($recordArrInstructor['allCourses'][$courseId])){
					
						$classOfCourseUsers['allCourses'][$courseId]->enrolled_user = $courses->enrolled_user;
						$classOfCourseUsers['allCourses'][$courseId]->courseid = $courses->courseid;
						$classOfCourseUsers['allCourses'][$courseId]->coursename = $courses->coursename;
						$classOfCourseUsers['allCourses'][$courseId]->coursetype_id = $courses->coursetype_id;
						$classOfCourseUsers['allCourses'][$courseId]->complition_time = $courses->complition_time;
						$classOfCourseUsers['allCourses'][$courseId]->coursetype = $courses->coursetype;
						$classOfCourseUsers['allCourses'][$courseId]->criteria = $courses->criteria;
						$classOfCourseUsers['allCourses'][$courseId]->credithours = $recordArrInstructor['allCourses'][$courseId]->credithours + $courses->credithours;
						
						if($courses->coursetype_id == $CFG->courseTypeOnline){
						  $classOfCourseUsers['allCourses'][$courseId]->class_details = array();
						}else{
						
							if(isset($courses->class_details) && count($courses->class_details) > 0 ){
								foreach($courses->class_details as $classId => $classDetails){
									if(isset($recordArrInstructor['allCourses'][$courseId]->class_details[$classId])){
										$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['class_id'] = $classId;
										$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['class_name'] = $classDetails['class_name'];
										$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['credithours'] = $recordArrInstructor['allCourses'][$courseId]->class_details[$classId]['credithours'] + $classDetails['credithours'];
										$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['complition_time'] = $classDetails['complition_time'];
									}else{
										$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['class_id'] = $classId;
										$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['class_name'] = $classDetails['class_name'];
										$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['credithours'] += $classDetails['credithours'];
										$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['complition_time'] = $classDetails['complition_time'];
									}
								}
							}
						
		                }
						$i++;
						
					}else{
						$classOfCourseUsers['allCourses'][$courseId]->enrolled_user = $courses->enrolled_user;
						$classOfCourseUsers['allCourses'][$courseId]->courseid = $courses->courseid;
						$classOfCourseUsers['allCourses'][$courseId]->coursename = $courses->coursename;
						$classOfCourseUsers['allCourses'][$courseId]->coursetype_id = $courses->coursetype_id;
						$classOfCourseUsers['allCourses'][$courseId]->complition_time = $courses->complition_time;
						$classOfCourseUsers['allCourses'][$courseId]->coursetype = $courses->coursetype;
						$classOfCourseUsers['allCourses'][$courseId]->criteria = $courses->criteria;
						$classOfCourseUsers['allCourses'][$courseId]->credithours += $courses->credithours;
                        
						if($courses->coursetype_id == $CFG->courseTypeOnline){
						  $classOfCourseUsers['allCourses'][$courseId]->class_details = array();
						}else{
							if(isset($courses->class_details) && count($courses->class_details) > 0 ){
								foreach($courses->class_details as $classId => $classDetails){
									$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['class_id'] = $classId;
									$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['class_name'] = $classDetails['class_name'];
									$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['credithours'] += $classDetails['credithours'];
									$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['complition_time'] = $classDetails['complition_time'];
	
								}
							}
						}
							

					}
				}
			}
			
			if(isset($recordArrInstructor['allCourses']) && count($recordArrInstructor['allCourses']) > 0 ){

			    foreach($recordArrInstructor['allCourses'] as $courseId => $courses){
				    if(isset($recordArrLearner['allCourses'][$courseId])){
						$classOfCourseUsers['allCourses'][$courseId]->enrolled_user = $courses->enrolled_user;
						$classOfCourseUsers['allCourses'][$courseId]->courseid = $courses->courseid;
						$classOfCourseUsers['allCourses'][$courseId]->coursename = $courses->coursename;
						$classOfCourseUsers['allCourses'][$courseId]->coursetype_id = $courses->coursetype_id;
						$classOfCourseUsers['allCourses'][$courseId]->complition_time = $courses->complition_time;
						$classOfCourseUsers['allCourses'][$courseId]->coursetype = $courses->coursetype;
						$classOfCourseUsers['allCourses'][$courseId]->criteria = $courses->criteria;
						$classOfCourseUsers['allCourses'][$courseId]->credithours += $courses->credithours;
						
						
						
						if($courses->coursetype_id == $CFG->courseTypeOnline){
						  $classOfCourseUsers['allCourses'][$courseId]->class_details = array();
						}else{
							if(isset($courses->class_details) && count($courses->class_details) > 0 ){
								foreach($courses->class_details as $classId => $classDetails){
									if(isset($recordArrLearner['allCourses'][$courseId]->class_details[$classId])){
										$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['class_id'] = $classId;
										$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['class_name'] = $classDetails['class_name'];
										$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['credithours'] = $recordArrLearner['allCourses'][$courseId]->class_details[$classId]['credithours'] + $classDetails['credithours'];
										$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['complition_time'] = $classDetails['complition_time'];
									}else{
										$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['class_id'] = $classId;
										$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['class_name'] = $classDetails['class_name'];
										$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['credithours'] += $classDetails['credithours'];
										$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['complition_time'] = $classDetails['complition_time'];
									}
								}
							}
						}	
						
					}else{

							$classOfCourseUsers['allCourses'][$courseId]->enrolled_user = $courses->enrolled_user;
							$classOfCourseUsers['allCourses'][$courseId]->courseid = $courses->courseid;
							$classOfCourseUsers['allCourses'][$courseId]->coursename = $courses->coursename;
							$classOfCourseUsers['allCourses'][$courseId]->coursetype_id = $courses->coursetype_id;
							$classOfCourseUsers['allCourses'][$courseId]->complition_time = $courses->complition_time;
							$classOfCourseUsers['allCourses'][$courseId]->coursetype = $courses->coursetype;
							$classOfCourseUsers['allCourses'][$courseId]->criteria = $courses->criteria;
							$classOfCourseUsers['allCourses'][$courseId]->credithours += $courses->credithours;
							
							if($courses->coursetype_id == $CFG->courseTypeOnline){
							  $classOfCourseUsers['allCourses'][$courseId]->class_details = array();
							}else{
								if(isset($courses->class_details) && count($courses->class_details) > 0 ){
									foreach($courses->class_details as $classId => $classDetails){
										$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['class_id'] = $classId;
										$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['class_name'] = $classDetails['class_name'];
										$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['credithours'] += $classDetails['credithours'];
										$classOfCourseUsers['allCourses'][$courseId]->class_details[$classId]['complition_time'] = $classDetails['complition_time'];
		
									}
								}
							}	

					}
				}
			}
			
			$recordArr['crediHoursForGraph'][1] = $recordArrInstructor['crediHoursForGraph'][1] + $recordArrLearner['crediHoursForGraph'][1];
			$recordArr['crediHoursForGraph'][2] = $recordArrInstructor['crediHoursForGraph'][2] + $recordArrLearner['crediHoursForGraph'][2];
			
			
			$allCourses = $classOfCourseUsers['allCourses'];
			$allCoursesCnt = count($allCourses);
			//pr($recordArr);pr($allCourses);die;
			$recordArr['allCourses'] = $allCourses;
			$recordArr['allCoursesCnt'] = $allCoursesCnt;

			return $recordArr;
				
	}
	
	function exportGraphPDF($file='', $html='', $html1='', $pdfTitle='') {
	   // ini_set('display_errors', 1);
  	   // error_reporting(E_ALL);
		global $USER, $DB, $CFG, $SITE, $OUTPUT, $PAGE;
	
	    ob_start();
		
		/*$haslogo = (!empty($PAGE->theme->settings->logo));

		#### Logo settings ####
		if ($haslogo) {
			$logourl = $PAGE->theme->setting_file_url('logo', 'logo');
		} else {
			$logourl = $OUTPUT->pix_url('logo', 'theme');
		}
        //echo $logourl;die;
		if($logourl){
			$logourlArr = explode(".", $logourl);
			$logoExt = $logourlArr[count($logourlArr) - 1];
			$logoUrl = "http://".str_replace("//","", $logourl);
			$logoPath = $CFG->dirroot.'/local/lib/tcpdf/images/logo.'.$logoExt;
			//if(!file_exists($logoPath)){
			 chmod($CFG->dirroot.'/local/lib/tcpdf/images', 0777);
		     copy($logoUrl, $logoPath);
			//}
		}*/
		
		require_once($CFG->dirroot .'/local/lib/tcpdf/config/tcpdf_config.php');
		require_once($CFG->dirroot .'/local/lib/tcpdf/config/lang/eng.php');

		require_once($CFG->dirroot .'/local/lib/pdflib.php');

		//$pdf = new pdf('P');
        $pdf = new pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor($SITE->fullname);
        $pdf->SetTitle(get_string('courseusagesreport','multicoursereport'));
        $pdf->SetSubject(get_string('courseusagesreport','multicoursereport'));
        $pdf->SetKeywords('usage,university');
		
		$copyright = get_string('copyright','multicoursereport').' <a href="'.$CFG->wwwroot.'" target="_blank">'.$SITE->fullname.'</a> '.get_string('allrightsreserved','multicoursereport');

		// set default header data
		//$pdf->SetHeaderData('logo0.png', '115', 'Text 1', 'Text 2');
		//$pdf->setFooterData(array(0, 0, 0), array(0, 0, 0), $copyright);
		//$pdf->Footer();

		$pdfHeaderTitle = get_string('courseusagesreport','multicoursereport');
		if(!empty($pdfTitle)) {
			$pdf->SetTitle($pdfTitle);
			$pdf->SetSubject($pdfTitle);
			$pdfHeaderTitle = $pdfTitle;
		}
		
		$pdfCreatedDate = date($CFG->customDefaultDateTimeFormatForPrint);
		
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $pdfHeaderTitle, $pdfCreatedDate, array(0, 0, 0), array(0, 0, 0));
		//$pdf->setFooterData(array(0, 0, 0), array(0, 0, 0), $copyright);
		
		// set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        // remove default header/footer
        $pdf->setPrintHeader(true);
        $pdf->setPrintFooter(true);
        //$zoom="100";
        $pdf->SetDisplayMode('real', 'SinglePage', 'UseNone');
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetMargins('10', '30', '10', true);
        //set margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin('5');
        $pdf->SetFooterMargin(15);

        //set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
     
        //set image scale factor
        //echo PDF_IMAGE_SCALE_RATIO;die;
      

        //set some language-dependent strings
        $pdf->setLanguageArray($l);

        // ---------------------------------------------------------
        // set default font subsetting mode
        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        //$pdf->SetFont('dejavusans', '', 14, '', true);
        //
        //$fontname = $pdf->addTTFfont(PUBLIC_PATH . '/tcpdf/fonts/verdana.ttf', 'TrueTypeUnicode', '', 32);
        //$pdf->SetFont($fontname, '', 10);
        $pdf->SetFont('helvetica', '', 10);
        $pdf->setFontSubsetting(true);		

		/*
		$pdf->setPrintHeader(true);
		$pdf->setPrintFooter(true);

		$pdf->SetDisplayMode('real', 'SinglePage', 'UseNone');
		
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins('14', '28', '14');

		$pdf->SetFont('helvetica', '', 11);*/
        
    
        //$html = $htmlImg.$html;
		        
      

		$pdf->AddPage();
		
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		$pdf->writeHTML($html, true, false, true, false, '');
	     
        if($html1){
		
			$newhtml = explode('~~~', $html1, 3);
		
			if(sizeof($newhtml) > 0) {
				$i=0;
				foreach($newhtml AS $key => $val) {
					$val = str_replace('~~~', '', $val);
					if(!empty($val)) {
						//if($i%2==0)
						if($key==0)
							$pdf->AddPage();
						$pdf->writeHTML('<p>&nbsp;</p>'.$val, false, false, true, false, '');
					}
					$i++;
				}
			}
		}
       
		$pdf->Output($file, 'D');
		die();
		//pdf generator ends
	}
	
	function getpdfCsvFileName($type, $string, $content = ''){
	   
	      global $CFG, $USER;
	      $filepathname = false;
		  if($type){

				$filepath = $CFG->pdfCsvFilePath;
				if(!is_dir($filepath)){  
				 mkdir($filepath);
				 chmod($filepath, 0777);		
				}		
				
				$filename = str_replace(' ', '_', $string)."_".date("m-d-Y")."_".$USER->id.".".$type;  
				$filepathname = $filepath.'/'.$filename;
				
				if($type === 'csv' && $content !=''){
				  $filepathname = $filepath.$filename;
				  @unlink($filepathname);
				  $handler = fopen($filepathname, "w");
		          fwrite($handler, $content);	
				  fclose($handler);
				}
				
				
			
		   }
		   return $filepathname;	
		
	}
	
	function deleteExportFolder(){
	
	     global $CFG;
	     $cwd =  getcwd();
		 if($cwd == $CFG->wwwpath && isset($CFG->exportFilePath) && $CFG->exportFilePath!=''){
		   $exportFilePath = $CFG->exportFilePath;
		   recursiveRemove($exportFilePath);
		   mkdir($exportFilePath);
		   chmod($exportFilePath, 0777);
		 }
	}
	
	function deleteExportResourceFolder(){
	
		global $CFG;
		$cwd =  getcwd();
		if($cwd == $CFG->wwwpath && isset($CFG->exportResourceFilePath) && $CFG->exportResourceFilePath!=''){
			$exportResourceFilePath = $CFG->exportResourceFilePath;
			recursiveRemove($exportResourceFilePath);
			mkdir($exportResourceFilePath);
			chmod($exportResourceFilePath, 0777);
		}
	}
	


	function recursiveRemove($dir) {
	
	    if($dir){
			$structure = glob(rtrim($dir, "/").'/*');
			if (is_array($structure)) {
				foreach($structure as $file) {
					if (is_dir($file)) recursiveRemove($file);
					elseif (is_file($file)) unlink($file);
				}
			}
			rmdir($dir);
			
		}
		
	}
	
	
	function getUserGroups($isReport = false){
		
		global $CFG, $DB;
		$query = "SELECT g.id,g.name FROM mdl_groups as g LEFT JOIN mdl_group_department as gd ON gd.team_id = g.id WHERE gd.id IS NULL ";
		if(!$isReport){
			$query .= " AND g.is_active = '1' ";
			//$query .= " AND gd.is_active = '1'";
		}
		$query .= " order by g.name asc ";
		$userGroups = $DB->get_records_sql($query);
		return $userGroups;
	}
	
	
	function getQueryString($format=1){
		
		$result = '';
		if($format == 1){
		  $result = $_SERVER[QUERY_STRING];
		}elseif($format == 2){
			$qStr = $_SERVER[QUERY_STRING];
			parse_str($qStr, $result);
		}
		
		return $result;
	}
	
	function getGraphImageHTML($title){
		
		global $CFG, $USER;
		$reportContentPDF = '';
		if($title){
			
			$graphImg = getGraphImageUrl($title);
			$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" >';
			$reportContentPDF .= '<tr><td>&nbsp;</td></tr>';
			// $reportContentPDF .= '<tr>';
			// $reportContentPDF .= '<td style="border-bottom:1px solid #000; overflow:visible;float:left;padding:10px;display:block;text-align:center;height:270px" ><img src="'.$CFG->pdfCsvFileUrl.$graphImg.'" width="670" style="border:1px solid #333; padding-top:50px !important;" align="absmiddle" top="30" /></td>';
			// $reportContentPDF .= '<td style="width:1000px; overflow:visible;float:left;padding:10px;display:block;text-align:center;height:350px; background-image:url('.$CFG->pdfCsvFileUrl.$graphImg.') no-repeat" ></td>';
			//$reportContentPDF .= '</tr>'; 
			//$reportContentPDF .= '<tr><td ><div style="border-bottom:1px solid #b9b9b9; background-color:#b9b9b9;"><img src="'.$CFG->pdfCsvFileUrl.$graphImg.'" width="700" height="198"    border="0" /></div></td></tr>';
			$reportContentPDF .= '<tr><td ><img src="'.$CFG->pdfCsvFileUrl.$graphImg.'" width="700" height="198"    border="0" /></td></tr>';
			//$reportContentPDF .= '<tr><td><img src="'.$CFG->pdfCsvFileUrl.$graphImg.'" width="700" height="192"   border="0" /></td></tr>';
			$reportContentPDF .= '<tr><td>&nbsp;</td></tr>';
			$reportContentPDF .= '</table>';
		}
		return $reportContentPDF;
		
	} 
	
	/* function getGraphImageHTML($title){
	
		global $CFG, $USER;
		$reportContentPDF = '';
		if($title){
				
			$graphImg = getGraphImageUrl($title);
			$reportContentPDF .= "<table border='0' cellpadding='0' cellspacing='0' >";
			$reportContentPDF .= "<tr><td>&nbsp;</td></tr>";
			// $reportContentPDF .= "<tr>";
			//$reportContentPDF .= "<td style='border-bottom:1px solid #000; overflow:visible;float:left;padding:10px;display:block;text-align:center;height:270px' ><img src='".$CFG->pdfCsvFileUrl.$graphImg."' width='670' style='border:1px solid #333; padding-top:50px !important;' align='absmiddle' top='30' /></td>";
			// $reportContentPDF .= "<td style='width:1000px; overflow:visible;float:left;padding:10px;display:block;text-align:center;height:350px; background-image:url(".$CFG->pdfCsvFileUrl.$graphImg.") no-repeat' ></td>";
			//$reportContentPDF .= "</tr>"; 
			//$reportContentPDF = "<img alt='GnosisConnect LMS' src='http://dev.gnosislms.com/theme/gourmet/pix/avatar.png' width='700' height='300' id='logo'>";
			$reportContentPDF .= "<tr><td><img src='".$CFG->pdfCsvFileUrl.$graphImg."' width='700' height='300' style='width:700px; height:300px;'   border='0' /></td></tr>";
			$reportContentPDF .= "<tr><td>&nbsp;</td></tr>";
			$reportContentPDF .= "</table>";
		}
		return $reportContentPDF;
	
	} */
		
	function getGraphImageUrl($title){
		
		global $USER, $CFG;
		$graphImg = '';
		if($title){
		 // $graphImg = str_replace(' ', '_', $title)."_".date("m-d-Y")."_".$USER->id.".png";
		  $graphImg = str_replace(' ', '_', $title)."_".date("m-d-Y")."_".$USER->id.".".$CFG->pdfFileExt;
		}
		return $graphImg;
	}
	

	
	function includeGraphFiles($title){
		
		global $CFG, $USER;
		$includeScript = '';
		if($title){
			$graphImg = getGraphImageUrl($title);
			$includeScript = '<script>
			var wwwroot = "'.$CFG->wwwroot.'";
			var graphImg = "'.$graphImg.'";
			jQuery(".exports_opt_box").hide();
			</script>
			<script src="'.$CFG->wwwroot.'/convertor/html2canvas.js"></script>
			<script type="text/javascript" src="'.$CFG->wwwroot.'/convertor/convert.js"></script>';
		}
		
		return $includeScript;
		
	}

	
	function setBackUrlForUserSection(){
		
		global $CFG;
		
		$pageURL = $_SERVER[PHP_SELF];
		$qStrArr = getQueryString(2);
		$user_back_url = genParameterizedURL($qStrArr, array(), $pageURL);		
		$_SESSION['User']['user_back_url'] = $user_back_url;
		
	}
	
	function getBackUrlForUserSection(){
	
		global $CFG;
		return $user_back_url = !empty($_SESSION['User']['user_back_url'])?$_SESSION['User']['user_back_url']:($CFG->wwwroot.'/admin/user.php');
	}
	
	
	
	
	
	function getResourceFile($contextId){ 
		 
		global $DB, $CFG;
        
		$resArr = array();
	    if($contextId){
				 
	    	    $component = 'mod_resource';
	    	    $filearea =  'content';
				$query = "Select id,contenthash, contextid, component, filearea, filename from {$CFG->prefix}files where contextid = ".$contextId." and component = '".$component."' and filearea = '".$filearea."' and filename !='' AND filename !='.'";
				$resFileArr = $DB->get_record_sql($query);
				if($resFileArr->contextid && $resFileArr->component  && $resFileArr->filearea && $resFileArr->filename){
					$resFilePath = $CFG->wwwroot.'/pluginfile.php/'.$resFileArr->contextid.'/'.$resFileArr->component.'/'.$resFileArr->filearea.'/'.$resFileArr->filename;
					//echo $resFilePath.'</br>';die;
					if($resFilePath){
						
						 $ff = substr($resFileArr->contenthash, 0, 2);
						 $sf = substr($resFileArr->contenthash, 2, 2);
						 $contenthash = $resFileArr->contenthash;
						 $resArr['sourceFilePath'] = $CFG->dataroot."/filedir/".$ff."/".$sf."/".$contenthash;
						 $resArr['sourceFileName'] = $resFileArr->filename;
					}
					
					
				}
		}
	
	
		return $resArr;
			
		 
	}
	
	function getResourceFileByItemId($itemId){ 
			
		global $DB, $CFG;
	
		$filename = '';
		if($itemId){
				
			$component = 'user';
			$filearea =  'draft';
			$query = "Select filename from {$CFG->prefix}files where component = '".$component."' and itemid = '".$itemId."' and filearea = '".$filearea."' and filename !='' AND filename !='.'";
			$filename = $DB->get_field_sql($query);
			
		}
	
	
		return $filename;
			
			
	}
	
	function getFVAllowedTypes(){

		$allowedTypes = getFVAllowedFileTypes();
		$FVAllowedAudioVideoTypes = getFVAllowedAudioVideoTypes();
		if(count($FVAllowedAudioVideoTypes) > 0 ){
			$allowedTypes = array_merge($allowedTypes, $FVAllowedAudioVideoTypes);
		}
		return $allowedTypes;
	
	}
	
	function getFVAllowedAudioVideoTypes(){

		$FVAllowedAudioTypes = getFVAllowedAudioTypes();
		$FVAllowedVideoTypes = getFVAllowedVideoTypes();
		
		if(count($FVAllowedAudioTypes) > 0 && count($FVAllowedVideoTypes) > 0 ){
			$allowedTypes = array_merge($FVAllowedAudioTypes, $FVAllowedVideoTypes); //,'3gp','ogv','webm'
		}elseif(count($FVAllowedAudioTypes) == 0 && count($FVAllowedVideoTypes) > 0 ){
			$allowedTypes = $FVAllowedVideoTypes; //,'3gp','ogv','webm'
		}elseif(count($FVAllowedAudioTypes) > 0 && count($FVAllowedVideoTypes) == 0 ){
			$allowedTypes = $FVAllowedAudioTypes; //,'3gp','ogv','webm'
		}
		
	
	
		return $allowedTypes;
	
	}
	
	function getFVAllowedFileTypes(){
	
		//$allowedTypes = array('txt','log','csv','doc','docx','pdf','ppt','pptx','pptm','rtf','xls','xlsx','xlsm','xltx','xltm','xml','xsl');
		$allowedTypes = array('pdf');
	
		return $allowedTypes;
	
	}
	
	function getFVAllowedAudioTypes(){
	
		$allowedTypes = array('mp3','wav','m4a');
	
		return $allowedTypes;
	
	}
	
	
	
	function getFVAllowedVideoTypes(){
	
		$allowedTypes = array('flv','mp4','m4v','webm','ogv');
	
		return $allowedTypes;
	
	}
	
	function getFVDownloadableTypes(){
	
		$types = array('zip','gzip','rar','tar');
	
		return $types;
	
	}
	
	
	
	function openResoursePreviewWindow($contextId){
		
		//ini_set('display_errors', 1);
		//error_reporting(E_ALL);
		
		//echo $contextId;die;
		global $DB, $CFG, $USER;
		if($contextId){
		  $resArr = getResourceFile($contextId);
		  if(count($resArr) > 0 ){
		  		
		  	$sourceFilePath = $resArr['sourceFilePath'];
		  	$sourceFileName = $resArr['sourceFileName'];
		  	$sourceFileName = str_replace(" ","_", $sourceFileName);
		  	
		  		
		  	if($sourceFilePath && $sourceFileName){

		  		$sourceFileNameArr = explode(".",$sourceFileName);
		  		$arrLen = count($sourceFileNameArr) > 0 ? (count($sourceFileNameArr) - 1):'';
		  		$fileExt = $sourceFileNameArr[$arrLen];
		  		$fvAllowedTypes = getFVAllowedTypes();

		  		if(in_array($fileExt, $fvAllowedTypes)){ // ext not supported

			  			$FVResourceUploadPath = $CFG->FVResourceUploadPath.$contextId."/";
			  			
			  			//if($update == 1){
			  				//recursiveRemove($FVResourceUploadPath);
			  			//}
			  			
			  			if(!is_dir($FVResourceUploadPath)){
			  				mkdir($FVResourceUploadPath, 0777, 1);
			  				chmod($CFG->FVResourceUploadPath, 0777);
			  				chmod($FVResourceUploadPath, 0777);
			  			}
			  			
			  			$destFilePath = $FVResourceUploadPath.$sourceFileName;
			  			
			  			$sourceFileNamePDF = str_replace(".".$fileExt,".pdf", $sourceFileName);
			  			$destFilePathPDF = $FVResourceUploadPath.$sourceFileNamePDF;

			  			$FVAllowedAudioVideoTypes = getFVAllowedAudioVideoTypes();
			  			if(file_exists($destFilePathPDF)){
			  						
			  					if($fileExt == 'pdf'){
			  						//$outputFile = count($sourceFileNameArr) > 0 ? ($sourceFileNameArr[0].".pdf"):'';
			  						$outputFile = $sourceFileName;
			  						$launchurl = $CFG->wwwroot."/viewerjs/view.php?inputFolder=".urlencode($destFilePathPDF)."&outputFile=".urlencode($outputFile)."&fileExt=".$fileExt."&contextId=".$contextId;
			  					}elseif(in_array($fileExt, $FVAllowedAudioVideoTypes)){
			  						$destFilePathPDF = $CFG->FVResourceUploadURL.$contextId."/".$sourceFileName;
			  						
			  						$outputFile = $sourceFileName;
			   						$launchurl = $CFG->wwwroot."/viewerjs/player.php?outputFile=".urlencode($outputFile)."&fileExt=".$fileExt."&contextId=".$contextId;
			   						//$launchurl = $CFG->wwwroot."/viewerjs/player.php?inputFolder=".urlencode($destFilePathPDF)."&outputFile=".urlencode($outputFile)."&fileExt=".$fileExt."&contextId=".$contextId;
			  						
			  					}else{
			  						$outputFile = count($sourceFileNameArr) > 0 ? ($sourceFileNameArr[0].".pdf"):'';
			  						$launchurl = $CFG->wwwroot."/viewerjs/view.php?inputFolder=".urlencode($destFilePathPDF)."&outputFile=".urlencode($outputFile)."&fileExt=".$fileExt."&contextId=".$contextId;
			  					}
			  						
			  					$script = "<script>
									  			    window.location.href = '".$launchurl."';
									
											  </script>";
			  					echo $script; die;
			  				
			  			}else{

						  		@unlink($destFilePath);
						  		$isFileCopied = copy($sourceFilePath, $destFilePath);
						      
						  		if($isFileCopied){
						  			
						  		    //$absPath = realpath($destFilePath);
				
							  		if(file_exists($destFilePath)){
							  			
							  			    convertResourceIntoPdf($contextId);
							  			    if($fileExt == 'pdf'){
							  			      //$outputFile = count($sourceFileNameArr) > 0 ? ($sourceFileNameArr[0].".pdf"):'';
							  			      $outputFile = $sourceFileName;
							  			      $launchurl = $CFG->wwwroot."/viewerjs/view.php?inputFolder=".urlencode($destFilePath)."&outputFile=".urlencode($outputFile)."&fileExt=".$fileExt."&contextId=".$contextId;
							  			    }elseif(in_array($fileExt, $FVAllowedAudioVideoTypes)){
							  			      $destFilePath = $CFG->FVResourceUploadURL.$contextId."/".$sourceFileName;
							  			      $outputFile = $sourceFileName;
							  			      $launchurl = $CFG->wwwroot."/viewerjs/player.php?outputFile=".urlencode($outputFile)."&fileExt=".$fileExt."&contextId=".$contextId;
							  			      //$launchurl = $CFG->wwwroot."/viewerjs/player.php?inputFolder=".urlencode($destFilePath)."&outputFile=".urlencode($outputFile)."&fileExt=".$fileExt."&contextId=".$contextId;
							  			    }else{
							  			      $outputFile = count($sourceFileNameArr) > 0 ? ($sourceFileNameArr[0].".pdf"):'';
							  			      $launchurl = $CFG->wwwroot."/viewerjs/view.php?inputFolder=".urlencode($destFilePath)."&outputFile=".urlencode($outputFile)."&fileExt=".$fileExt."&contextId=".$contextId;
							  			    }
								  			
									  		$script = "<script>
									  			    window.location.href = '".$launchurl."';	
											  </script>";
									  		echo $script; die;
							  		
							  		}
							  		
						  		}
						  		
			  			 }		

		  		} 		
		  			
		  	}
		  }
		  
		}
		
		return false;

	}
	
	function convertResourceIntoPdf($contextId, $update=0){
	
		//ini_set('display_errors', 1);
		//error_reporting(E_ALL);
	
		global $DB, $CFG, $USER;
		if($contextId){
			$resArr = getResourceFile($contextId);
			if(count($resArr) > 0 ){
	
				$sourceFilePath = $resArr['sourceFilePath'];
				$sourceFileName = $resArr['sourceFileName'];
				$sourceFileName = str_replace(" ","_", $sourceFileName);
	
				if($sourceFilePath && $sourceFileName){
	
					$sourceFileNameArr = explode(".",$sourceFileName);
					$arrLen = count($sourceFileNameArr) > 0 ? (count($sourceFileNameArr) - 1):'';
					$fileExt = $sourceFileNameArr[$arrLen];
					$fvAllowedTypes = getFVAllowedTypes();
	
					if(in_array($fileExt, $fvAllowedTypes)){ // ext not supported
						 
						$FVResourceUploadPath = $CFG->FVResourceUploadPath.$contextId."/";
						//if($update == 1){
					    	//recursiveRemove($FVResourceUploadPath);
					    // }
						if(!is_dir($FVResourceUploadPath)){
							mkdir($FVResourceUploadPath, 0777, 1);
							chmod($CFG->FVResourceUploadPath, 0777);
							chmod($FVResourceUploadPath, 0777);
						}
						
						$destFilePath = $FVResourceUploadPath.$sourceFileName;
						
						@unlink($destFilePath);
						$isFileCopied = copy($sourceFilePath, $destFilePath);
					  
						if($isFileCopied){
							 
							//$absPath = realpath($destFilePath);
	
							if(file_exists($destFilePath)){
									
								
								
								$FVAllowedAudioVideoTypes = getFVAllowedAudioVideoTypes();
								if($fileExt == 'pdf'){
									return true;
								}elseif(in_array($fileExt, $FVAllowedAudioVideoTypes)){
									return true;
								}else{
									return false;
									/*include_once($CFG->dirroot.'/viewerjs/settings.php');
									$outputFile = count($sourceFileNameArr) > 0 ? ($sourceFileNameArr[0].".pdf"):'';
									
									$inputFolder = $destFilePath;
									$outputFolder = $FVResourceUploadPath.$outputFile;
									$result = fvPDFCreator($inputFolder, $outputFolder);
									if($result == 0){
										return true;
									}
									*/
									
									/* $outputFile = $sourceFileName;
									$inputFolder = $destFilePath;
									$result = processPDFByOfficeServer($contextId, $outputFile, $inputFolder);
									$bool = ($result == 1)?false:true;
									return $bool; */
								}
	
							}
								
						}
	
					}
					 
				}
			}
	
		}
	
		return false;
	
	}
	
	
	function processPDFByOfficeServer($contextId, $outputFile, $inputFolder){
	
		//ini_set('display_errors', 1);
		//error_reporting(E_ALL);
		global $CFG, $DB;
	
		$error = 1;
		if($contextId && $inputFolder){

			$isCopied = CopyFileToOfficeServerViaFTP($contextId, $outputFile, $inputFolder);
			if($isCopied){
				$method = 'POST';
				$officeFTPServer = $CFG->OfficeServerRequestUrl;
				$data = array('id'=>$contextId, 'domain'=>$CFG->allowedDomainInOfficeServer, 'fileName' => $outputFile, 'request' => $CFG->OfficeRestApiMethod);
				$isPDFCreated = CallRestAPIToCreatePDF($method, $officeFTPServer, $data);
				if($isPDFCreated == 'success'){
					$isCopiedHere = CopyFileFromOfficeServerViaFTP($contextId, $outputFile);
					if($isCopiedHere){
						$error = 0;
					}
				}
			}
		}
	
		return $error;
	
	}
	
	
	function createDirByFTP($ftpcon,$ftpbasedir,$ftpath){
		//ini_set('display_errors', 1);
		//error_reporting(E_ALL);
		
		@ftp_chdir($ftpcon, $ftpbasedir);
		$parts = explode('/',$ftpath);
		
		//$mode = "777";
		
		foreach($parts as $part){
			if(!@ftp_chdir($ftpcon, $part)){
				ftp_mkdir($ftpcon, $part);
				ftp_chdir($ftpcon, $part);
				//ftp_chmod($ftpcon, 0777, $part);
			}
		}
	}

	
	function CopyFileToOfficeServerViaFTP($contextId, $outputFile, $localFolder){
	
	//	ini_set('display_errors', 1);
		//error_reporting(E_ALL);
		
		global $CFG, $DB;
	
		$result = 0;
		if($contextId && file_exists($localFolder)){
				
			
			$serverFolder = $CFG->OfficeFTPuploadPath.$contextId."/".$outputFile;
			// connect and login to FTP server
			$ftpServer = $CFG->OfficeFTPServer;
			$ftpUserName = $CFG->OfficeFTPUser;
			$ftpUserPass = $CFG->OfficeFTPPwd;
			
			$ftpConn = ftp_connect($ftpServer);
			
			if($ftpConn){
				
				$login = ftp_login($ftpConn, $ftpUserName, $ftpUserPass);
					
				if($login){	
					
					// upload file
					//echo "$localFolder, $serverFolder";
					//ftp_pasv($ftp_conn, true);  // to turns on the passive mode
					ftp_set_option($ftpConn, FTP_TIMEOUT_SEC, 172800);
					$ftpBaseDir = $CFG->OfficeFTPuploadPathBase;
					$ftpPath = $CFG->OfficeFTPuploadSubDirPath.$contextId;
					createDirByFTP($ftpConn, $ftpBaseDir, $ftpPath);
					$sucsess = ftp_put($ftpConn, $serverFolder, $localFolder, FTP_BINARY);
					if ($sucsess){
						//echo "Successfully uploaded.";
						$result = 1;
					}else{
					    //echo "Error uploading.";
					   $result = 0;
					}
					
				}else{
					$result = 0;
				}	
				// close connection
				ftp_close($ftpConn);
			}else{
				$result = 0;
				//die("Could not connect to $ftp_server");
			}	
	
		}
		
		return $result;
	
	}
	
	function CallRestAPIToCreatePDF($method, $url, $data = false){

		// Method: POST, PUT, GET etc
		// Data: array("param" => "value") ==> index.php?param=value
        //pr($method);pr($url);pr($data);die;
		$curl = curl_init();
	
		switch ($method){
			
			case "POST":
				curl_setopt($curl, CURLOPT_POST, 1);
	
				if ($data)
					curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
				break;
			case "PUT":
				curl_setopt($curl, CURLOPT_PUT, 1);
				break;
			default:
				if ($data)
					$url = sprintf("%s?%s", $url, http_build_query($data));
		}
	
		// Optional Authentication:
		//curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		//curl_setopt($curl, CURLOPT_USERPWD, "username:password");
	
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	
		$result = curl_exec($curl);
		//pr($result);die;
		curl_close($curl);
	
		return $result;
		
	}
	
	
	function CopyFileFromOfficeServerViaFTP($contextId, $outputFile){
	

		//ini_set('display_errors', 1);
		//error_reporting(E_ALL);
		
		global $CFG, $DB;
	
		$result = 0; 
		if($contextId){

			$sourceFileNameArr = explode(".",$outputFile);
			$arrLen = count($sourceFileNameArr) > 0 ? (count($sourceFileNameArr) - 1):'';
			$fileExt = $sourceFileNameArr[$arrLen];
			$fileNameInPdf = count($sourceFileNameArr) > 0 ? ($sourceFileNameArr[0].".pdf"):'';
			
			$localFile = $CFG->FVResourceUploadPath.$contextId."/".$fileNameInPdf;
			$serverFile = $CFG->OfficeFTPuploadPath.$contextId."/".$fileNameInPdf;

	
		    // connect and login to FTP server
			$ftpServer = $CFG->OfficeFTPServer;
			$ftpUserName = $CFG->OfficeFTPUser;
			$ftpUserPass = $CFG->OfficeFTPPwd;
			
			$ftpConn = ftp_connect($ftpServer);
			
			if($ftpConn){

			    $login = ftp_login($ftpConn, $ftpUserName, $ftpUserPass);
	            
				if($login){
					// try to download $server_file and save to $local_file
					ftp_set_option($ftpConn, FTP_TIMEOUT_SEC, 172800);
					$success = ftp_get($ftpConn, $localFile, $serverFile, FTP_BINARY);
					if ($success) {
					  //echo "Successfully written to $localFile\n";
					  $result = 1;
					} else {
					  //echo "There was a problem\n";
					  $result = 0;
					}
				}else{
					$result = 0;
				}
			    // close connection
				ftp_close($ftpConn);
				
			}else{
				
				$result = 1;
				// die("Could not connect to $ftp_server");
				
			}

		}	
		
		return $result;
	
	}


	
			
	function callPdfConvertor($fromfrom, $update=0){
		
		global $CFG;
		$result = false;
		if($CFG->fileViewerMode == 1){
			if($fromfrom->module == $CFG->resourceModuleId){
				$cmId = $fromfrom->coursemodule;
				if($cmId){
				   $context = context_module::instance($cmId);
				   if(isset($context->id) && $context->id){
				   	  $result = convertResourceIntoPdf($context->id, $update);
				   }
				}
				
			}

		}	
		
		return $result;
	
	}
	
	function getResourceFileExt($data){
	
		global $CFG;

	    $fileExt = '';
		if($CFG->fileViewerMode == 1){
			if($data->module == $CFG->resourceModuleId){
				$itemId = $data->files;
				if($itemId){
					$sourceFileName = getResourceFileByItemId($itemId); 
				
					if($sourceFileName){
						$sourceFileNameArr = explode(".",$sourceFileName);
						$arrLen = count($sourceFileNameArr) > 0 ? (count($sourceFileNameArr) - 1):'';
						$fileExt = $sourceFileNameArr[$arrLen];
					
					}			
					
				}
	
			}
	
		}
		
		return $fileExt;
	
	}
	
	function isResourceFileExtDownloadable($data){
	
		global $CFG;
		$downloadable = false;
		//pr($data);
		$fileExt = getResourceFileExt($data);
		$fvAllowedTypes = getFVAllowedTypes();
		$fvDTypes = getFVDownloadableTypes();
		
		if(!in_array($fileExt, $fvAllowedTypes)){
		  $downloadable = true;
		}
		
		return $downloadable;
	
	}
	
	function isResourceDownloadable($contextId){
		global $CFG, $DB, $USER;
		
		$canDownload = 0;
		
	    if( $USER->archetype == $CFG->userTypeStudent){
			$query = "SELECT mr.can_download FROM mdl_resource mr LEFT JOIN mdl_course_modules cm ON (mr.course = cm.course and mr.id = cm.instance) LEFT JOIN mdl_context ct ON (cm.id =  ct.instanceid) where ct.id = '".$contextId."'";
			$result = $DB->get_field_sql($query);
			
			if($result){
				$canDownload = 1;
			}
		}elseif( $USER->archetype == $CFG->userTypeManager){
			
		
			$query = "SELECT mc.createdby, mr.can_download, mdm.departmentid FROM mdl_resource mr LEFT JOIN mdl_course mc ON (mc.id = mr.course) LEFT JOIN mdl_department_members mdm ON (mdm.userid = mc.createdby and mdm.is_active = 1) LEFT JOIN mdl_course_modules cm ON (mr.course = cm.course and mr.id = cm.instance) LEFT JOIN mdl_context ct ON (cm.id =  ct.instanceid) where ct.id = '".$contextId."'";
			$result = $DB->get_record_sql($query);
			if($USER->department == $result->departmentid || $result->can_download == 1){
			  $canDownload = 1;
			}

			
		}else{
		   $canDownload = 1;
		}
		return $canDownload;
	} 
	
	function getResourceDownloadPath($contextId){
		global $CFG, $DB;
	
		$resDownloadPath = '';
		
		if($contextId){
			$resArr = getResourceFile($contextId);
		    //	pr($resArr);die;
			if(count($resArr) > 0 ){
			
				$sourceFilePath = $resArr['sourceFilePath'];
				$sourceFileName = $resArr['sourceFileName'];
				$sourceFileName = str_replace(" ","_", $sourceFileName);

				if($sourceFilePath && $sourceFileName){
			
					$sourceFileNameArr = explode(".",$sourceFileName);
					$arrLen = count($sourceFileNameArr) > 0 ? (count($sourceFileNameArr) - 1):'';
					$fileExt = $sourceFileNameArr[$arrLen];
					$fvAllowedTypes = getFVAllowedTypes();
			
					if(in_array($fileExt, $fvAllowedTypes)){
						
						$resDownloadPath = $CFG->FVResourceUploadRelativePath.$contextId."/".$sourceFileName;
			
					}
					
				}	
			} 	
		}
		
		return $resDownloadPath;
	}
	
	function isUserDeleted($userId){
		
		global $CFG, $DB;
		$isDeleted = 0;
		$query .= " SELECT mu.id FROM mdl_user mu ";
		$query .= " LEFT JOIN mdl_department_members AS mdm ON (mdm.userid = mu.id) ";
		$query .= " LEFT JOIN mdl_department AS md ON (md.id = mdm.departmentid) ";
		$query .= "  WHERE 1 = 1 AND md.deleted = '0' AND mu.deleted = '0' AND mu.id IN ($userId) ";
		$record = $DB->get_field_sql($query);
		
		if(empty($record)){
		  $isDeleted = 1; 
		}
		
		return $isDeleted;
		
	}
	
	function isResourceFileExist($cmId){
		
		$bool = false;
		if($cmId){
		
			$context = context_module::instance($cmId);
			$contextId = $context->id;
			$sourceFilePath = '';
			if($contextId){
				$resArr = getResourceFile($contextId);
				$sourceFilePath = $resArr['sourceFilePath'];
				$sourceFileName = $resArr['sourceFileName'];
				
				if($sourceFilePath && file_exists($sourceFilePath)){
					$bool = true;
				}
			}
			
			
		}	
		
		return $bool;
	}
	
	function getResourceDownloadNoteHtml(){
		
		global $CFG;
		$note = '';

		$allowedFileTypes = getFVAllowedFileTypes();
		$allowedAudioTypes = getFVAllowedAudioTypes();
		$allowedVideoTypes = getFVAllowedVideoTypes();
		
		$a = new stdClass();
		$a->fileFormat = '';
		$a->audioFormat = '';
		$a->videoFormat = '';
		
		if(count($allowedFileTypes) > 0 ){
			$allowedFileTypesStr = implode(", ", $allowedFileTypes);
			$a->fileFormat = "<strong>File:</strong> ". $allowedFileTypesStr." <br>";
		}
		
		if(count($allowedAudioTypes) > 0 ){
			$allowedAudioTypesStr = implode(", ", $allowedAudioTypes);
			$a->audioFormat = "<strong>Audio:</strong> ". $allowedAudioTypesStr." <br>";
		}
		
		if(count($allowedVideoTypes) > 0 ){
			$allowedVideoTypesStr = implode(", ", $allowedVideoTypes);
			$a->videoFormat = "<strong>Video:</strong> ". $allowedVideoTypesStr." <br>";
		}

		$note = get_string('noteforfvfilesdownload', 'moodle', $a);
		return $note;
	}
	
	
	function getReportPrintHeader($reportName){
	
		global $CFG, $DB, $USER;
		$HTML = '';
		if($reportName){
			$logo = '<img src="'.$CFG->pdfLogoURL.'" width="100" />';
			$currentDate = date($CFG->customDefaultDateTimeFormatForPrint);
			$HTML .= '<table border="0" cellpadding="0" cellspacing="0"  class="margin_bottom">';
			$HTML .= '<tr>';
			$HTML .= '<td width="60%">'.$logo.'</td>';
			$HTML .= '<td width="40%" style="text-align:right;"><div style="font-size:16px"><strong>'.$reportName.'</strong></div><div>'.$currentDate.'</div></td>';
			$HTML .= '</tr>';
			$HTML .= '</table>';
		}
		return $HTML;
	}
	
	
	function getOverallCourseCreditReportByCronTable($sUserType, $searchString='', $searchStringI='', $searchStringL='', $paramArray, $sort, $dir){
	
		global $DB, $USER, $CFG;
	
		$creditHoursArr = array();
		$allUserArr = array();
	
		$excludedUsers = count($CFG->excludedUsers) > 0 ? implode(",",$CFG->excludedUsers) : 0;
	
		$sStartDate     = $paramArray['startDate'];
		$sEndDate       = $paramArray['endDate'];
		$selMode       = $paramArray['sel_mode'];
			
	
		$sDateSelected = '';
		if($sStartDate){
			$sDateArr = getFormattedTimeStampOfDate($sStartDate);
			$sDateSelected = isset($sDateArr['dateMDY'])?$sDateArr['dateMDY']:'';
	
		}
	
		$eDateSelected = '';
		if($sEndDate){
			$eDateArr = getFormattedTimeStampOfDate($sEndDate, 1);
			$eDateSelected = isset($eDateArr['dateMDY'])?$eDateArr['dateMDY']:'';
		}
			
			
		if($sUserType == $CFG->userTypeInstructor){
	
			$queryAll = " SELECT  incid, id , user_type, course_count, userfullname, username, is_user_deleted, departmenttitle, departmentid, usermanager, credithours, courseid, coursename, coursetype_id, criteria, is_course_deleted, is_course_publish, complition_time, class_id, class_name, class_instructor, session_id, sessionname, jobid, comid, job_title, company  FROM mdl_overall_course_credit_report WHERE 1 = 1";
			$queryAll .= " AND id NOT IN (".$excludedUsers.") ";
			$queryAll .= " AND is_user_deleted = '0' ";
			$queryAll .= " AND user_type = '".$CFG->userTypeInstructor."' ";
			$queryAll .= " $searchString $searchStringI";
			$queryAll .= " GROUP BY courseid, class_id, session_id, class_instructor ";
			if($sort){
				$queryAll .= " ORDER BY $sort $dir";
			}
				
		}else{
			$queryAll = " SELECT  incid, id , user_type, userfullname, username, departmenttitle, departmentid, usermanager, credithours, courseid, coursename, coursetype_id, criteria, complition_time, jobid, comid, job_title, company  FROM mdl_overall_course_credit_report WHERE 1 = 1";
			$queryAll .= " AND id NOT IN (".$excludedUsers.") ";
			$queryAll .= " AND is_user_deleted = '0' ";
			$queryAll .= " AND user_type = '".$CFG->userTypeStudent."' ";
			$queryAll .= " $searchString $searchStringL";
			if($sort){
				$queryAll .= " ORDER BY $sort $dir";
			}
		}
	
		//echo $queryAll;die;
	
		$allSystemUserAllArr = $DB->get_records_sql($queryAll);
		//$allSystemUserAllForGraphArr = $DB->get_records_sql($queryGraph);
	
		//pr($allSystemUserAllArr);die;
	
		$creditHoursArr = array();
		$allUserArr = array();
			
		$sStartDateTime = '';
		$sEndDateTime = '';
		if($sDateSelected){
			//list($month, $day, $year) = explode('/', $sDateSelected);
			//$sStartDateTime =  mktime(0, 0, 0, $month, $day, $year);
			$sStartDateTime = isset($sDateArr['timestamp'])?$sDateArr['timestamp']:0;
		}
			
		if($eDateSelected){
			//list($month, $day, $year) = explode('/', $eDateSelected);
			//$sEndDateTime = mktime(23, 59, 59, $month, $day, $year);
			$sEndDateTime = isset($eDateArr['timestamp'])?$eDateArr['timestamp']:0;
		}
			
		$curTime = time();
			
		//pr($allSystemUserAllArr);die;
		if(count($allSystemUserAllArr) > 0 ){
			$totalDepCHours = 0;
			$userCourseArr = array();
			$x=0;
			foreach($allSystemUserAllArr as $sArr){
	
	
				$online_credithours = 0;
				$online_course_count = 0;
				$classroom_credithours = 0;
				$classroom_course_count = 0;
					
				if($sArr->id){
	
					$allUserArr['users'][$sArr->id]->userid = $sArr->id;
					$allUserArr['users'][$sArr->id]->userfullname = $sArr->userfullname;
					$allUserArr['users'][$sArr->id]->username = $sArr->username;
					$allUserArr['users'][$sArr->id]->departmenttitle = $sArr->departmenttitle;
					$allUserArr['users'][$sArr->id]->departmentid = $sArr->departmentid;
					$allUserArr['users'][$sArr->id]->usermanager = $sArr->usermanager;
					$allUserArr['users'][$sArr->id]->userfullname = $sArr->userfullname;
					$allUserArr['users'][$sArr->id]->courseid = $sArr->courseid;
					$allUserArr['users'][$sArr->id]->coursename = $sArr->coursename;
					$allUserArr['users'][$sArr->id]->coursetype_id = $sArr->coursetype_id;
					$allUserArr['users'][$sArr->id]->criteria = $sArr->criteria;
					$allUserArr['users'][$sArr->id]->complition_time = $sArr->complition_time;
					$allUserArr['users'][$sArr->id]->credithours = $sArr->credithours;
					$allUserArr['users'][$sArr->id]->course_count = $sArr->course_count;
						
	
					$canAddCreditHours = 0;
					if($sArr->coursetype_id){
	
						// going to check can we add credit hours between giving learning date for a course
						$complitionTime = $sArr->complition_time;
						//echo date("Y-m-d",$complitionTime)."<br>";
						if($complitionTime){
								
							if($sUserType == $CFG->userTypeInstructor){
	
								if($complitionTime < $curTime){
										
									//echo date("Y-m-d H:i:s", $complitionTime)."===".date("Y-m-d  H:i:s",$curTime)."<br>";
									//echo $complitionTime."===".$curTime."<br>";
									//echo $sStartDateTime."===".$sEndDateTime."<br>";
									if($sStartDateTime && $sEndDateTime){
											
										if($complitionTime >= $sStartDateTime && $complitionTime <= $sEndDateTime ){
											$canAddCreditHours = 1;
										}
									}elseif($sStartDateTime && $sEndDateTime == ''){
										if($complitionTime >= $sStartDateTime ){
											$canAddCreditHours = 1;
										}
									}elseif($sStartDateTime == ''  && $sEndDateTime){
										if($complitionTime <= $sEndDateTime ){
											$canAddCreditHours = 1;
										}
									}
	
									//echo $canAddCreditHours."==$sArr->courseid<br>";
								}
							}else{
	
								//echo date("Y-m-d H:i:s", $complitionTime)."===".date("Y-m-d  H:i:s",$curTime)."<br>";
								//echo $complitionTime."===".$curTime."<br>";
								if($sStartDateTime && $sEndDateTime){
									if($complitionTime >= $sStartDateTime && $complitionTime <= $sEndDateTime ){
										$canAddCreditHours = 1;
									}
								}elseif($sStartDateTime && $sEndDateTime == ''){
									if($complitionTime >= $sStartDateTime ){
										$canAddCreditHours = 1;
									}
								}elseif($sStartDateTime == ''  && $sEndDateTime){
									if($complitionTime <= $sEndDateTime ){
										$canAddCreditHours = 1;
									}
								}
							}
						}
							
						// going to add credit hours between giving learning date for a course
						if($canAddCreditHours == 1){
								
								
							if($sArr->departmentid){
								$credithours = 0;
								$credithours = $sArr->credithours?$sArr->credithours:0;
								$totalDepCHours += $credithours;
								//$creditHoursArr['creditHoursInDept'][$sArr->departmentid] += $credithours;
									
							}
	
	
							if($sArr->coursetype_id == $CFG->courseTypeOnline){
								$online_credithours = $sArr->credithours;
								$online_course_count = $sArr->courseid?1:0;
							}else if($sArr->coursetype_id == $CFG->courseTypeClassroom){
								$classroom_credithours = $sArr->credithours;
								$classroom_course_count = $sArr->courseid?1:0;
							}
	
							if(isset($sArr->coursetype_id) && $sArr->coursetype_id){
								$creditHoursArr[$sArr->coursetype_id] += $credithours;
							}
						}
					}
	
					$allUserArr['users'][$sArr->id]->online_credithours += $online_credithours;
					$allUserArr['users'][$sArr->id]->online_course_count += $online_course_count;
					$allUserArr['users'][$sArr->id]->classroom_credithours += $classroom_credithours;
						
					if($sUserType == $CFG->userTypeInstructor){
							
						if($canAddCreditHours){
							if( !in_array($sArr->courseid, $userCourseArr[$sArr->id]) ){
								//echo $canAddCreditHours."==$sArr->courseid<br>";
								$userCourseArr[$sArr->id][] = $sArr->courseid;
								$allUserArr['users'][$sArr->id]->classroom_course_count += 1;
							}
						}
	
					}else{
							
						if($canAddCreditHours){
							if($sArr->coursetype_id == $CFG->courseTypeClassroom && !in_array($sArr->courseid, $userCourseArr[$sArr->id]) ){
								//echo $canAddCreditHours."==$sArr->courseid<br>";
								//pr($userCourseArr);
								$userCourseArr[$sArr->id][] = $sArr->courseid;
								$allUserArr['users'][$sArr->id]->classroom_course_count += 1;
							}
						}
	
	
					}
				}
			}
	
			// $creditHoursArr['totalCreditHoursInDept'] = $totalDepCHours;
		}
			
		//pr($userCourseArr);die;
	
			
		$recordArr['creditHoursArr'] = $creditHoursArr;
		$recordArr['allUserArr'] = $allUserArr;
		$recordArr['userCourseArr'] = $userCourseArr;
		return $recordArr;
	
	
	}
        
/*
 * function to create All country select box
 * Created on 15th feb 2016
 */        
	function createCountrySelectBoxFilterFor($for, $idElement, $selectedArr = array(0), $multiple=true, $display=''){
	   
		global $CFG, $USER, $DB;
		$HTML = '';
		$HTMLOptions = '';
		
		if($for !=''){		
			if($for == 'country'){
			  $recordArr = get_string_manager()->get_list_of_countries();                      
                         
			  $canShow = $CFG->showCountryFilter;
			  $optLbl = get_string('all_country','user');
			}
			
			if($canShow == 1 ){

					$selectedC = in_array('-1', $selectedArr)?'selected="selected"':'';
					$multiple = $multiple?'multiple="multiple"':'';
					$style = $multiple==false?'':'';
					$size = $multiple?'size="2"':'';
					$HTML .= '<select autocomplete="off" '.$size.' '.$multiple.' id="'.$idElement.'" name="'.$idElement.'" '.$display.'  style="'.$style.'">';
					$HTMLOptions .= '<option value="-1" '.$selectedC.' >'.$optLbl.'</option>';
					
					if(count($recordArr) > 0 ){
					   
					   if(!empty($recordArr)){
							foreach($recordArr as $key=>$value){
								 $selectedC = count($selectedArr) > 0 && in_array($key, $selectedArr)?'selected="selected"':'';
								 $HTMLOptions .= '<option value="'.$key.'" '.$selectedC.' >'.$value.'</option>';
							}
						}
					}
					$HTML .=  $HTMLOptions;
					
					$HTML .= '</select>';
					
			}
			
		}			
		return  $HTML;
        
	}

/*
 * function to create All Role select box
 * Created on 15th feb 2016
 */        
	function createIsManagerSelectBoxFilterFor($for, $idElement, $selectedArr = array(0), $multiple=true, $display=''){
	   
		global $CFG, $USER, $DB;
		$HTML = '';
		$HTMLOptions = '';
		
		if($for !=''){		
			if($for == 'is_manager_yes'){
			  $recordArr = array(1=>'Managers',0=>'Non-Managers');                           
                         
			  $canShow = $CFG->showIsManageryesFilter;
			  $optLbl = get_string('ismanageryes');
			}
			
			if($canShow == 1 ){

					$selectedR = in_array('-1', $selectedArr)?'selected="selected"':'';
					$multiple = $multiple?'multiple="multiple"':'';
					$style = $multiple==false?'':'';
					$size = $multiple?'size="2"':'';
					$HTML .= '<select autocomplete="off" '.$size.' '.$multiple.' id="'.$idElement.'" name="'.$idElement.'" '.$display.'  style="'.$style.'">';
					$HTMLOptions .= '<option value="-1" '.$selectedR.' >'.$optLbl.'</option>';
					
					if(count($recordArr) > 0 ){
					   
					   if(!empty($recordArr)){
							foreach($recordArr as $key=>$value){
								 $selectedC = count($selectedArr) > 0 && in_array($key, $selectedArr)?'selected="selected"':'';
								 $HTMLOptions .= '<option value="'.$key.'" '.$selectedC.' >'.$value.'</option>';
							}
						}
					}
					$HTML .=  $HTMLOptions;
					
					$HTML .= '</select>';
					
			}
			
		}
               
		return  $HTML;
        
	}
        
        
        function createCountrySelectBoxForFilter($idElement = 'country',  $data = array(), $selectedArr = array('-1'), $display='', $multiple=true){
	   
		global $CFG, $USER, $DB;
                
                if($CFG->countryFilterEnabled!=1){
                    return '';
                }
                
                $data = get_string_manager()->get_list_of_countries();
                //$data= array(''=>get_string('selectacountry')) + $data;                
               
		$HTML = '';
		$HTMLOptions = '';
		
		$selectedC = in_array('-1', $selectedArr)?'selected="selected"':'';
		$multiple = $multiple?'multiple="multiple"':'';
		$size = $multiple?'size="2"':'';
		$HTML .= '<select autocomplete="off" '.$size.' '.$multiple.' id="'.$idElement.'" name="'.$idElement.'" '.$display.' >';
		$HTMLOptions .= '<option value="-1" '.$selectedC.' >'.get_string('all_location').'</option>';
		
		if(count($data) > 0 ){
		 //  pr($data);die;
		   if(!empty($data)){
				foreach($data as $key=>$value){
					 $selectedC = count($selectedArr) > 0 && in_array($key, $selectedArr)?'selected="selected"':'';
					 $HTMLOptions .= '<option value="'.$key.'" '.$selectedC.' >'.$value.'</option>';
				}
			}
		}
		$HTML .=  $HTMLOptions;
		
		$HTML .= '</select>';
		return  $HTML;
        
	}
        
        function createCitySelectBoxForFilter($idElement = 'city',  $data = array(), $selectedArr = array('-1'), $display='', $multiple=true){
	   
		global $CFG, $USER, $DB;
                
                if($CFG->cityFilterEnabled!=1){
                    return '';
                }
                
                $data = getAllUsersCity();
                //$data= array(''=>get_string('selectacountry')) + $data;                
               
		$HTML = '';
		$HTMLOptions = '';
		
		$selectedC = in_array('-1', $selectedArr)?'selected="selected"':'';
		$multiple = $multiple?'multiple="multiple"':'';
		$size = $multiple?'size="2"':'';
		$HTML .= '<select autocomplete="off" '.$size.' '.$multiple.' id="'.$idElement.'" name="'.$idElement.'" '.$display.' >';
		$HTMLOptions .= '<option value="-1" '.$selectedC.' >'.get_string('all_city').'</option>';
		
		if(count($data) > 0 ){
		 //  pr($data);die;
		   if(!empty($data)){
				foreach($data as $key=>$value){
					 $selectedC = count($selectedArr) > 0 && in_array($key, $selectedArr)?'selected="selected"':'';
					 $HTMLOptions .= '<option value="'.$key.'" '.$selectedC.' >'.$value.'</option>';
				}
			}
		}
		$HTML .=  $HTMLOptions;
		
		$HTML .= '</select>';
		return  $HTML;
        
	}

	function getAllUsersCity(){
            global $CFG, $USER, $DB;
            
            $cityOption = array();
          
            $sql = "SELECT DISTINCT city FROM mdl_user WHERE deleted=0 and city<>''";
            $records = $DB->get_records_sql($sql);
            
            if($records){
                $cnt = 1;
                foreach($records as $record){
                    $cityOption[$cnt]=$record->city;
                    $cnt++;
                }
            }
            return $cityOption;
        }
        function ConvertCityIdToNameArr($CityIdArr=array()){
                global $CFG, $USER, $DB;
               
                $CityNameArr = array();
                if($CityIdArr && !in_array('-1',$CityIdArr)){                   
                    $AllCityArr = getAllUsersCity();
                    
                    foreach($CityIdArr as $cid){
                        if(trim($cid)){
                            $CityNameArr[]= "'".$AllCityArr[$cid]."'";
                        }
                                              
                    }
                   
                }
             
                return $CityNameArr;
        }
        
        function createReportToSelectBoxForFilter($idElement = 'report_to',  $data = array(), $selectedArr = array('-1'), $display='', $multiple=true){
	   
		global $CFG, $USER, $DB;
             
                $HTML = '';
                if($CFG->showReportTo == 1){
                    
                    $HTMLOptions = '';                  
                   
                    $Lists = getLeadersNameList();
                    
                    $selectedC = in_array('-1', $selectedArr)?'selected="selected"':'';
                    $multiple = $multiple?'multiple="multiple"':'';
                    $size = $multiple?'size="2"':'';
                    
                    $HTML .= '<select autocomplete="off" '.$size.' '.$multiple.' id="'.$idElement.'" name="'.$idElement.'">';
                    $HTMLOptions .= '<option value="-1" '.$selectedC.' >'.get_string('allreport_to').'</option>';

                    if(count($Lists) > 0 ){

                       if(!empty($Lists)){
                                    foreach($Lists as $key=>$value){
                                             $selectedC = count($selectedArr) > 0 && in_array($key, $selectedArr)?'selected="selected"':'';
                                             $HTMLOptions .= '<option value="'.$key.'" '.$selectedC.' >'.$value.'</option>';
                                    }
                            }
                    }
                    $HTML .=  $HTMLOptions;

                    $HTML .= '</select>';                   
            }
                 return  $HTML;
		      
	}
        
        function getLeadersNameList(){
            global $CFG, $USER, $DB;
            
            $output = array();
            $sql = "SELECT DISTINCT report_to from mdl_user where report_to<>'' order by report_to asc";
         
            $list = $DB->get_records_sql($sql);
            if($list){
                foreach($list as $data){                    
                    $output[] = $data->report_to;
                }
            }
            return $output;
        }
        
        function getAdManagersNonManagersList($sIsManagerYesArr=array()){
            global $CFG, $USER, $DB;
            
            $sManagers = "";
            if(count($sIsManagerYesArr) && !in_array('-1',$sIsManagerYesArr)){
                    foreach($sIsManagerYesArr as $id){
                           $sManagers .= $CFG->isManagerYesOptions[$id].' | ';

                    }
                    $sManagers = trim($sManagers," | ");
            }else{
               $sManagers = get_string('all');
            }
            return $sManagers;
        }