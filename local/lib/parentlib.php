<?php
function getTeamList($departmentId = 0,$teamId = 0,$getChild = 0){
	Global $USER,$CFG,$DB;
	$where = '';
	if($teamId != 0){
		$where .= ' AND g.id != '.$teamId;
		$childList = fetchTeamChildList($teamId);
		if(!empty($childList)){
			$parentCheck = implode(',',$childList);
			if($getChild == 1){
				$where .= ' AND g.id IN ('.$parentCheck.') ';
			}else{
				$where .= ' AND g.id NOT IN ('.$parentCheck.') ';
			}
		} 
	}
	if($departmentId != 0){
		$teamLists = $DB->get_records_sql("SELECT g.id,g.name FROM mdl_groups AS g LEFT JOIN mdl_group_department AS gd ON g.id = gd.team_id WHERE gd.department_id = ".$departmentId." $where ORDER BY g.name ASC");
	}else{
		$teamLists = $DB->get_records_sql("SELECT g.id,g.name FROM mdl_groups AS g LEFT JOIN mdl_group_department as gd ON g.id = gd.team_id WHERE gd.id IS NULL $where ORDER BY g.name ASC");
	}
	return $teamLists;
}
function fetchTeamChildList($parent = 0, $user_tree_array = '') {
	GLOBAL $CFG,$DB,$USER;
	if (!is_array($user_tree_array))
		$user_tree_array = array();
	$sql = "SELECT id, name, parent_id FROM mdl_groups WHERE parent_id = $parent ORDER BY name ASC";
	$groupData = $DB->get_records_sql($sql);
	if (!empty($groupData) > 0) {
		foreach($groupData as $group) {
			$user_tree_array[] = "". $group->id."";
			$user_tree_array = fetchTeamChildList($group->id, $user_tree_array);
		}
	}
	return $user_tree_array;
}
function fetchGroupsList($userId = 0) {
	GLOBAL $CFG,$DB,$USER;
	if ($userId == 0)
		$userId = $USER->id;
	$sql = "SELECT g.id FROM mdl_groups g WHERE g.group_owner = ".$userId;
	$groupData = $DB->get_records_sql($sql);
	if (!empty($groupData) > 0) {
		$user_tree_array = array();
		foreach($groupData as $key=>$group) {
			$user_tree_array[$key] = fetchTeamChildList($group->id);
			array_push($user_tree_array[$key],$key);
		}
	}
	$finalarray = array();
	if(!empty($user_tree_array)){
		foreach($user_tree_array as $user_tree){
			foreach($user_tree as $user){
				$finalarray[] = $user;
			}
		}
	}
	return $finalarray;
}
function fetchGroupsCourseIds($userId = 0,$groupId = 0,$isReport = 0) {
	GLOBAL $CFG,$DB,$USER;
	if ($userId == 0)
		$userId = $USER->id;

	if ($groupId != 0){
		$sql = "SELECT g.id FROM mdl_groups g WHERE g.id = ".$groupId;
		$groupData = new stdClass;
		$groupData->$groupId->id = $groupId;
	}else{
		$sql = "SELECT g.id FROM mdl_groups g WHERE g.group_owner = ".$userId;
		$groupData = $DB->get_records_sql($sql);
	}
	if (!empty($groupData) > 0) {
		$user_tree_array = array();
		foreach($groupData as $key=>$group) {
			$user_tree_array[$key] = fetchTeamChildList($group->id);
			array_push($user_tree_array[$key],$key);
		}
	}
	$finalarray = array();
	$courseList = array();
	if(!empty($user_tree_array)){
		foreach($user_tree_array as $user_tree){
			foreach($user_tree as $user){
				$finalarray[] = $user;
			}
		}
		$groupsList =  implode(',',$finalarray);
		$where = "is_active = 1 AND";
		if($isReport == 1){
			$where = '';
		}
		$courses = $DB->get_records_sql("SELECT courseid as id FROM mdl_groups_course WHERE $where groupid IN (".$groupsList.")");
		if(!empty($courses)){
			foreach($courses as $course){
				$courseList[] = $course->id;
			}
		}
	}
	return $courseList;
}
function fetchGroupsUserIds($userId = 0,$isReport = 0) {
	GLOBAL $CFG,$DB,$USER;
	if ($userId == 0)
		$userId = $USER->id;
	$groupsList = fetchGroupsList($userId);
	$user = array();
	if(!empty($groupsList)){
		$groupsList =  implode(',',$groupsList);
		$where = "gm.is_active = 1 AND";
		if($isReport == 1){
			$where = "";
		}
		$users = $DB->get_records_sql("SELECT gm.userid FROM mdl_groups_members as gm WHERE $where gm.groupid IN (".$groupsList.")");
		if(!empty($users)){
			foreach($users as $userData){
				$user[] = $userData->userid;
			}
		}
	}
	return $user;
}
function getchild($teamId,$user_tree_array,$append = ''){
	GLOBAL $USER,$CFG,$DB;
	$user_tree_array = array();
	$sql = "SELECT id, name, parent_id FROM mdl_groups WHERE parent_id = $teamId ORDER BY name ASC";
	$groupData = $DB->get_records_sql($sql);
	if (!empty($groupData) > 0) {
		foreach($groupData as $group) {
			$user_tree_array[$group->id]['id'] = "". $group->id."";
			$group->name = str_replace("\\",'/',$group->name);
			$user_tree_array[$group->id]['name'] = "".$append. $group->name."";
			$user_tree_array[$group->id]['child'] = getchild($group->id, $user_tree_array,$append);
		}
	}
	return $user_tree_array;
}
function getRecursiveTeamList($departmentId = 0,$teamId = 0){
	Global $USER,$CFG,$DB;
	$where = '';
	$append = '';
	if($departmentId != 0){
		$depQuery = "SELECT g.id,g.name FROM mdl_groups AS g LEFT JOIN mdl_group_department AS gd ON g.id = gd.team_id WHERE gd.department_id = ".$departmentId." AND parent_id = 0 ORDER BY g.name ASC";
	}else{
		$append = '*';
		$depQuery = "SELECT g.id,g.name FROM mdl_groups AS g LEFT JOIN mdl_group_department AS gd ON g.id = gd.team_id WHERE gd.id IS NULL AND parent_id = 0 ORDER BY g.name ASC";
	}
	$teamLists = $DB->get_records_sql($depQuery);
	$teams = array();
	foreach($teamLists as $team){
		$teams[$team->id]['id'] = $team->id;
		$team->name = str_replace("\\",'/',$team->name);
		$teams[$team->id]['name'] = $append.$team->name;
		$teams[$team->id]['child'] = getchild($team->id,array(),$append);
	}
	return $teams;
}
function getJson($groupsData,$jsonArray){
	GLOBAL $USER,$CFG,$DB;
	$jsonArray = array();
	$dataCount = 0;
	foreach($groupsData as $groupData) {
		$jsonArray[$dataCount]['name'] = ($groupData['name']);
		$jsonArray[$dataCount]['ele_id'] = $groupData['id'];
		$jsonArray[$dataCount]['class_ele'] = "department";
		if (!empty($groupData['child']) > 0) {
			$count = 0;
			$jsonArray[$dataCount]['children'][$count] = array();
			foreach($groupData['child'] as $group) {
				$jsonArray[$dataCount]['children'][$count]['name'] = ($group['name']);
				$jsonArray[$dataCount]['children'][$count]['ele_id'] = $group['id'];
				$jsonArray[$dataCount]['children'][$count]['class_ele'] = "department";
				if(!empty($group['child'])){
					$jsonArray[$dataCount]['children'][$count]['children'] = getJson($group['child'],$jsonArray);
				}
				$count++;
			}
		}
		$dataCount++;
	}
	return $jsonArray;
}
/*
function to check user access to owner data.
*/
function checkOwnerAccess($userId){
	GLOBAL $CFG,$DB,$USER;
	$grpUsers = fetchGroupsUserIds($userId,1);
	if(!empty($grpUsers)){
			return true;
	}else{
		$grpCourses = fetchGroupsCourseIds($userId,0,1);
		if(!empty($grpCourses)){
			return true;
		}
	}
	return false;
}
function getParentsListing($paramArray){
	GLOBAL $DB,$CFG,$USER;
	if(isset($paramArray['sel_mode']) && $paramArray['sel_mode'] == 2){
		$parentTeam = $DB->get_records_sql("SELECT g.* FROM mdl_groups AS g LEFT JOIN mdl_group_department as gd ON gd.team_id = g.id WHERE g.id IN ( SELECT gm.parent_id FROM mdl_groups AS gm) AND gd.id IS NULL ORDER BY g.name ASC");
	}else{
		$where = '';
		if(isset($paramArray['department']) && $paramArray['department'] != '-1' && $paramArray['department'] != '0' && $paramArray['department'] != ''){
			$deptIds = explode("@",$paramArray['department']);
			$deptIds = implode(",",$deptIds);
			$where = ' AND gd.department_id IN ('.$deptIds.')';
		}
		$parentTeam = $DB->get_records_sql("SELECT g.* FROM mdl_groups AS g LEFT JOIN mdl_group_department as gd ON gd.team_id = g.id WHERE g.id IN ( SELECT gm.parent_id FROM mdl_groups AS gm) AND gd.id IS NOT NULL $where ORDER BY g.name ASC");
	}
	return $parentTeam;
}

	/**
	 * This function removes courses to department
	 * @global object
	 * @param int $departmentId department Id
	 * @param array $courses course Id array
	 * @return nothing
	 */
	function removeCourseFromDepartment($departmentId,$courses,$remarks = ''){
		global $DB,$USER,$CFG;
		if(!empty($courses)){
			$time = time();
			/*foreach($courses as $course){
				if($departmentId != 0){
					$last_id = deactivateUserMappingData(0,$course,0,'department',$departmentId);
				}else{
					$last_id = deactivateUserMappingData(0,$course,0,'department');
				}
			}*/
			foreach($courses as $course){
				if($departmentId != 0){
					$sqlCondition = array("courseid" => $course,"departmentid" => $departmentId);
				}else{
					$sqlCondition = array("courseid" => $course);
				}
				$records = $DB->get_records('department_course', $sqlCondition);
				if(!empty($records)){
					foreach($records as $record){
						$mappingData = new stdClass;
						$mappingData->is_active = 0;
						$mappingData->id = $record->id;
						$mappingData->updatedby = $USER->id;
						$mappingData->remarks = $remarks;
						$last_id = $DB->update_record('department_course',$mappingData);
						add_to_log(1, 'Unenrol course from department', 'add', 'course/courseallocation.php?id='.$course.'&departmentid='.$departmentId, "Unenrol course from department", 0, $USER->id);
					}
				}
			}
		}
	}
	/************** Unassign Process of course from department / team / users ends**********************/
	function getFileClass($ext){
		if(preg_match("/^mp3|MP3|mpeg|MPEG|m3u|M3U$/",$ext)){
			return 'audio';
		}
		if(preg_match("/^avi|AVI|wmv|WMV|flv|FLV|mpg|MPG|mp4|MP4$/",$ext)){
			return 'video';
		}
		if(preg_match("/^gif|jpg|jpeg|tiff|png|bmp$/",$ext)){
			return 'image';
		}
		if(preg_match("/^doc|docx$/",$ext)){
			return 'doc';
		}
		if(preg_match("/^csv|xls|xlsx$/",$ext)){
			return 'csv';
		}
		if(preg_match("/^pdf$/",$ext)){
			return 'pdf';
		}
		if(preg_match("/^ppt|pptx$/",$ext)){
			return 'ppt';
		}
		if(preg_match("/^print$/",$ext)){
			return 'print';
		}
		if(preg_match("/^txt$/",$ext)){
			return 'txt';
		}
		return 'default';
	}
	function get_learning_list1($sort,$type,$page,$perpage,$selectionType = 'all',$whereChk = '', $userId = ''){
	Global $DB,$CFG,$USER;
	if($perpage != 0) {
		$offset = ($page - 1) * $perpage;
		$courseQuery .= " LIMIT " . $offset . ", " . $perpage;
	}
	$userId =  $userId?$userId:$USER->id;
	$where = ' WHERE user_id = '. $userId;
	//$where .= " AND (CONCAT_WS(stat_scorm,' ',stat_resource,' ', final_classrooom_stat) != '  ')";
	
	if($type == 1){
		$where .= " AND 
					(
						(
						stat_scorm = 'not started' || 
						stat_scorm = 'incomplete' || 
						stat_resource ='not started' || 
						stat_resource = 'incomplete' || 
						(FIND_IN_SET('incomplete',REPLACE(final_classrooom_stat,' ',',')) && final_classrooom_stat !='')
						)
					)";
		//$where .= " AND (end_date IS NULL ||(end_date+86399) >=".strtotime('now').")";
	}elseif($type == 2){
		$where .= " AND  
((stat_scorm != 'not started' && stat_scorm != 'incomplete') && 
(stat_resource != 'not started' && stat_resource != 'incomplete')
&&(((FIND_IN_SET('completed',REPLACE(final_classrooom_stat,' ',',')) || final_classrooom_stat = '') AND program_id = 0 )
OR ((!FIND_IN_SET('incomplete',REPLACE(final_classrooom_stat,' ',',')) || final_classrooom_stat = '') AND program_id != 0 ))
)";
	}else{
		$where .= " AND (
						(stat_scorm = 'not started' AND stat_resource = '' AND final_classrooom_stat = '') ||
						(stat_scorm = '' AND stat_resource = 'not started' AND final_classrooom_stat = '') ||
						(stat_scorm = 'not started' AND stat_resource = 'not started' AND final_classrooom_stat = '')
					)";

		/*$where .= " AND (
					CONCAT_WS(stat_scorm,' ',stat_resource,' ', final_classrooom_stat) = CONCAT_WS(' ','','','') || CONCAT_WS(stat_scorm,' ',stat_resource,' ', final_classrooom_stat) = CONCAT_WS(' ','not started','','') || CONCAT_WS(stat_scorm,' ',stat_resource,' ', final_classrooom_stat) = CONCAT_WS(' ','','not started','') || CONCAT_WS(stat_scorm,' ',stat_resource,' ', final_classrooom_stat) = CONCAT_WS(' ','','','not started') || CONCAT_WS(stat_scorm,' ',stat_resource,' ', final_classrooom_stat) = CONCAT_WS(' ','not started','not started','not started')) ";*/
	}
	if($selectionType !='all'){
		//$where .= " AND program_id = 0";
	}
	if($whereChk != ''){
		$where .= " AND ".$whereChk;
	}
	if($sort == 'count'){
		$fields = 'count(*) as listcount';

		$courseProgramList = $DB->get_record_sql("SELECT ".$fields." FROM vw_user_complete_learning_data as v LEFT JOIN mdl_course AS c ON c.id = v.id ".$where);
		return $courseProgramList->listcount;
	}else{
		$fields = 'v.*';
		//echo "SELECT ".$fields." FROM vw_user_complete_learning_data ".$where.$courseQuery;die;
		//$courseProgramList = $DB->get_records_sql("SELECT ".$fields." FROM vw_user_complete_learning_data ".$where.$courseQuery);
		//echo "SELECT ".$fields." FROM vw_user_complete_learning_data as v LEFT JOIN mdl_course AS c ON c.id = v.id ".$where.$courseQuery;die;
		
		$courseProgramList = $DB->get_records_sql("SELECT ".$fields." FROM vw_user_complete_learning_data as v LEFT JOIN mdl_course AS c ON c.id = v.id ".$where.$courseQuery);
		return $courseProgramList;
	}
}
function getActiveCourses($courseId = 0){
	GLOBAL $DB;
	$where = "";
	if($courseId != 0){
		$where = " AND c.id = ".$courseId;
	}
	$query = "SELECT DISTINCT(c.id)
										FROM mdl_course AS c
										LEFT JOIN mdl_course_modules as cm ON cm.course = c.id
										LEFT JOIN 
											( SELECT * FROM
												(
													(SELECT CONCAT(CONVERT(s.id,char),'_','18') as id,s.course,s.is_active FROM mdl_scorm as s WHERE s.is_active = 1)
														UNION 
													(SELECT CONCAT(CONVERT(r.id,char),'_','17') as id,r.course,r.is_active FROM mdl_resource as r WHERE r.is_active = 1)
												) 
												as srm
											) as sr ON sr.id = CONCAT(CONVERT(cm.instance,char),'_',cm.module)
										WHERE c.id != 1 AND c.deleted = 0 AND c.is_active = 1 AND cm.id IS NOT NULL AND cm.module != 9 AND sr.is_active = 1 AND c.coursetype_id = 1 ".$where;
	$activeIds = $DB->get_records_sql($query);
	$ClassroomActiveIdsQuery = "	SELECT DISTINCT(c.id)
							FROM mdl_course AS c
							WHERE c.id != 1 AND c.deleted = 0 AND c.is_active = 1 AND c.coursetype_id = 2 ".$where;
	$ClassroomActiveIds = $DB->get_records_sql($ClassroomActiveIdsQuery);
	$activeIds = array_merge($activeIds,$ClassroomActiveIds);
	$ids = array();
	if(!empty($activeIds)){
		foreach($activeIds as $active){
			$ids[] = $active->id;
		}
		return  implode(',',$ids);
	}
	return '';
}
function getActiveCourseCount($courseId, $is_reference_material = 0){
	GLOBAL $DB;
	
	$activeIds = $DB->get_record_sql("SELECT c.id,count(sr.id) as cnt,c.fullname
										FROM mdl_course AS c
										LEFT JOIN mdl_course_modules as cm ON cm.course = c.id
										LEFT JOIN 
											( SELECT * FROM
												(
													(SELECT CONCAT(CONVERT(s.id,char),'_','18') as id,s.course,s.is_active FROM mdl_scorm as s WHERE s.is_active = 1)
														UNION 
													(SELECT CONCAT(CONVERT(r.id,char),'_','17') as id,r.course,r.is_active FROM mdl_resource as r WHERE r.is_active = 1 AND r.is_reference_material = '".$is_reference_material."')
												) 
												as srm
											) as sr ON sr.id = CONCAT(CONVERT(cm.instance,char),'_',cm.module)
										WHERE c.id != 1 AND c.deleted = 0 AND c.is_active = 1 AND cm.id IS NOT NULL AND cm.module != 9 AND sr.is_active = 1 AND c.id = ".$courseId."
										GROUP BY c.id");
										
	if(!empty($activeIds) && $activeIds->cnt > 1){
		return true;
	}
	return false;
}


function getDepartmentManagerAdmin($departmentID = 0){
	GLOBAL $DB,$USER,$CFG;
	$addQuery = '';
	if($departmentID != '' || $departmentID != 0){
		$addQuery = "UNION (
				SELECT u.id,u.firstname,u.lastname,r.name
				FROM mdl_user AS u
				LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id
				LEFT JOIN mdl_role AS r ON r.id = ra.roleid
				LEFT JOIN mdl_department_members AS mdm ON u.id = mdm.userid
				WHERE u.deleted = 0 AND u.suspended = 0 AND ra.roleid IN(3) AND mdm.departmentid IN ($departmentID))";
	}
	$query = "	SELECT DISTINCT(c.id),c.firstname,c.lastname FROM ((
				SELECT u.id,u.firstname,u.lastname,r.name
				FROM mdl_user AS u
				LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id
				LEFT JOIN mdl_role AS r ON r.id = ra.roleid
				WHERE u.deleted = 0 AND u.suspended = 0 AND ra.roleid IN(1))$addQuery) as c";
	$userList = $DB->get_records_sql($query);
	return $userList;
}
function getDepartmentTeams($departmentId, $isReport = 0){
	global $DB,$USER,$CFG;
	$departmentTeams = array();
	$where = '';
	if($departmentId && $departmentId != 0){
	    
		if(!$isReport){
		  $where .= " AND g.is_active = '1'";
		}
		$query = "SELECT g.id,g.name FROM mdl_groups AS g LEFT JOIN mdl_group_department AS gd ON gd.team_id = g.id WHERE gd.department_id = ".$departmentId.$where."  ORDER BY g.name ASC";
		$departmentTeams = $DB->get_records_sql($query);
	}
	return $departmentTeams;
}
function publishCourse($courseId,$publish){
	global $DB,$USER,$CFG;
	$courseTypeId = getCourseTypeIdByCourseId($courseId);
	if($courseTypeId != $CFG->courseTypeClassroom ){
		$scormData = $DB->get_records_sql("SELECT s.id FROM mdl_scorm s WHERE s.course = ".$courseId);
		$resourceData = $DB->get_records_sql("SELECT r.id FROM mdl_resource r WHERE r.course = ".$courseId);
		if(empty($scormData) && empty($resourceData)){
			return 0;
		}else{
			$data = new stdClass();
			$data->id		= $courseId;
			$data->publish	= $publish;
			$DB->update_record('course', $data);
			if($CFG->sendComplainceMail == 1){
				$courseData = $DB->get_record_sql("SELECT c.id,c.fullname,c.coursetype_id,c.criteria,c.createdby,c.enddate,c.idnumber,dm.departmentid FROM mdl_course as c LEFT JOIN mdl_department_members as dm ON dm.userid  = c.createdby AND dm.is_active = 1 WHERE c.id = ".$courseId);
				if($courseData->criteria == 1){
					if($USER->archetype == $CFG->userTypeManager){
						$paramArr = array('is_active'=>1);
						$userLists = get_users_listing('u.firstname', '', 1, 0, '', '', '','',$paramArr, '');
					}elseif($courseData->departmentid != '' && $courseData->departmentid != 0 && $courseData->departmentid != null){
						$paramArr = array('sel_mode'=>1,'department'=>$courseData->departmentid,'is_active'=>1);
						$userLists = get_users_listing('u.firstname', '', 1, 0, '', '', '','',$paramArr, '');
					}else{
						$paramArr = array('is_active'=>1);
						$userLists = get_users_listing('u.firstname', '', 1, 0, '', '', '','',$paramArr, '');
					}
					$fromUser = array();
					foreach($userLists as $user){
						$emailText = '';
						$user->user_name = getUserNameFormat($user);
						$emailContent = array(
												'name'			=>$user->user_name,
												'courseid'		=>$courseData->idnumber,
												'coursename'	=>$courseData->fullname,
												'lmsLink'		=> $CFG->emailLmsLink,
												'lmsName'		=> $CFG->emailLmsName,
												'enddate'		=> getDateFormat($courseData->enddate, $CFG->customDefaultDateFormat)
											);
						$emailContent = (object)$emailContent;
						$subject = get_string('complaince_mail_to_learner_subject','email');
						$emailText .= 	get_string('complaince_mail_to_learner','email',$emailContent);
						if($CFG->showCourseURL==1 && $course->coursetype_id==1){
							$emailText .= get_string('complaince_mail_to_learner_1','email').getCourseLaunchURL($courseData->id);
						}
						if($CFG->sendComplainceMail == 1){
							$emailText .= get_string('email_footer','email');
							$emailText .= get_string('from_email','email',$emailContent);
						}else{
							$emailText .= get_string('complaince_mail_to_learner_2','email');
						}

						$toLearnerUser->email = $user->email;
						$toLearnerUser->firstname =  $user->firstname;
						$toLearnerUser->lastname =  $user->lastname;
						$toLearnerUser->maildisplay = true;
						$toLearnerUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
						$toLearnerUser->id = $user->id;
						$toLearnerUser->firstnamephonetic = '';
						$toLearnerUser->lastnamephonetic = '';
						$toLearnerUser->middlename = '';
						$toLearnerUser->alternatename = '';
						email_to_user($toLearnerUser, $fromUser, $subject, $emailText,'', '', '', true);

					}
				}
			}
			return 1;
		}
	}else{
		$data = new stdClass();
		$data->id		= $courseId;
		$data->publish	= $publish;
		$DB->update_record('course', $data);
		return 1;
	}
}

function publishProgram($programId, $publish){
	global $DB,$USER,$CFG;
	
	if($programId){
	
	        require_once($CFG->dirroot . '/local/user/selector/lib.php');
	        $pCourses = new program_courses_selector('', array('programId' => $programId));
			$programCourses = $pCourses->getProgramCourses();
			$cntTotalCourses = count($programCourses);
			
			if($cntTotalCourses > 0){
				$data = new stdClass();
				$data->id		= $programId;
				$data->publish	= $publish;
				$DB->update_record('programs', $data);
				$_SESSION['update_msg'] = get_string('programhasbeenpublished');
				return 1;
			}else{
				return 0;
			}
	}
}


function getGMDate($ts = null){ 
	$k = array('seconds','minutes','hours','mday', 
			'wday','mon','year','yday','weekday','month',0); 
	return(array_combine($k,split(":", 
			gmdate('s:i:G:j:w:n:Y:z:D:M:U',is_null($ts)?time():$ts)))); 
} 

/*
This function insert the data in request log table

*/
function saveLogdata($programId, $courseId, $userId, $statusId, $requestText, $recepientId, $saveOrUpdate,$request_type,$accept = 0,$newStatus = 0){
	GLOBAL $DB,$CFG,$USER;
	$currentTime = time();
	if($saveOrUpdate == 1){
		$dataConditionArray = array('course_id'=>$courseId, 'program_id'=>$programId, 'user_id'=>$userId, 'request_status'=>$statusId);
		$previousRequestdata = $DB->get_record("course_request_log",$dataConditionArray);
		if($previousRequestdata){
			$data = new stdClass();
			if($accept == 1){
				$data->reponse_on		= $currentTime;
				$data->modified_on		= $currentTime;
			}else{
				$data->requested_on		= $currentTime;
				$data->created_on		= $currentTime;
			}
			$data->id				= $previousRequestdata->id;
			$data->course_id		= $courseId;
			$data->program_id		= $programId;
			$data->user_id			= $userId;
			$data->request_status	= $newStatus;
			$data->recepient_id		= $recepientId;
			$DB->update_record('course_request_log', $data);
			return true;
		}else{
			return false;
		}
	}else{
		$data = new stdClass();
		$data->course_id		= $courseId;
		$data->program_id		= $programId;
		$data->user_id			= $userId;
		$data->request_status	= $statusId;
		$data->requested_on		= $currentTime;
		$data->recepient_id		= $recepientId;
		$data->reponse_on		= $currentTime;
		$data->created_on		= $currentTime;
		$data->modified_on		= $currentTime;
		$data->request_text		= $requestText;
		$data->request_type		= $request_type;
		$lastinsert = $DB->insert_record('course_request_log', $data);
		return true;
	}
}
function getLogdata($programId, $courseId, $userId, $statusId,$extensionRequest = ''){
	GLOBAL $DB,$CFG,$USER;
	if($extensionRequest == 1){
		if($programId != 0){
			$requestType = get_string('program_extension');
		}else{
			$requestType = get_string('course_extension');
		}
		$dataConditionArray = array('course_id'=>$courseId, 'program_id'=>$programId, 'user_id'=>$userId, 'request_status'=>$statusId, 'request_type' => $requestType);
	}else{
		$dataConditionArray = array('course_id'=>$courseId, 'program_id'=>$programId, 'user_id'=>$userId, 'request_status'=>$statusId);
	}
	$previousRequestdata = $DB->get_record("course_request_log",$dataConditionArray);
	if($previousRequestdata){
		return $previousRequestdata;
	}else{
		return "0";
	}
}
function getRequestdata($limit = '',$statusId,$getData = 1,$searchText = ''){
	GLOBAL $DB,$USER,$CFG;
	$where = '';
	$where2 = '';
	if($USER->archetype == $CFG->userTypeAdmin){
		$where2 .= " AND c.request_type != 'classroom' AND c.request_type != 'Course Extension'";
		//$userIdCheck = ' u.parent_id = '.$USER->id;
		$userIdCheck = '1=1';
		if($statusId == 2){
			$where = "WHERE (c.request_status = 1 || c.request_status = 2) ";
			$orderBy = "ORDER BY c.reponse_on DESC ";
		}else{
			$where = " WHERE (c.request_status = 0)";
			$orderBy = "ORDER BY c.requested_on DESC ";
		}
		$where .= $where2;
		if($searchText != ''){
			$where .= " AND (LOWER(request_type) LIKE '%".$searchText."%' || LOWER(request_text) LIKE '%".$searchText."%')";
		}
                
		$programCoursesSql = "SELECT ##SELECT_VALUE## FROM 
						(
							(
							SELECT CONCAT_WS('_', CONVERT(c.id, CHAR), CONVERT(c.program_id, CHAR), CONVERT(c.course_id, CHAR),c.user_id) AS dif_id,c.id, 0 AS classid,c.course_id,cc.fullname,'name' AS name, c.program_id,if(c.request_type = 'course','online course',c.request_type) as request_type,c.user_id,c.request_status, c.request_text,c.requested_on,c.reponse_on,1 AS startdate,2 AS enddate,0 AS no_of_seats,0 AS enrolmenttype,0 AS createdby,recepient_id AS action_taken_by FROM mdl_course_request_log c
							LEFT JOIN mdl_course cc ON cc.id = c.course_id LEFT JOIN mdl_user as u ON c.user_id = u.id LEFT JOIN mdl_role_assignments as ra ON ra.userid = c.user_id LEFT JOIN mdl_role as r ON ra.roleid = r.id WHERE r.name = 'manager' AND u.deleted = 0 AND u.suspended = 0 AND c.program_id = 0 AND cc.is_active = 1
							) UNION 
							(
							SELECT CONCAT_WS('_', CONVERT(c.id, CHAR), CONVERT(c.program_id, CHAR), CONVERT(c.course_id, CHAR),c.user_id) AS dif_id,c.id, 0 AS classid,c.course_id,p.name AS fullname,'name' AS name, c.program_id,c.request_type,c.user_id,c.request_status, c.request_text,c.requested_on,c.reponse_on,1 AS startdate,2 AS enddate,0 AS no_of_seats,0 AS enrolmenttype,0 AS createdby,recepient_id AS action_taken_by FROM mdl_course_request_log c
							LEFT JOIN mdl_programs as p ON c.program_id = p.id LEFT JOIN mdl_user as u ON c.user_id = u.id LEFT JOIN mdl_role_assignments as ra ON ra.userid = c.user_id LEFT JOIN mdl_role as r ON ra.roleid = r.id WHERE r.name = 'manager' AND u.deleted = 0 AND u.suspended = 0 AND c.course_id = 0 AND p.status = 1
							)
						) as c 
						LEFT JOIN mdl_user as u ON c.user_id = u.id ";
	}else{
		$userIdCheck = '(u.parent_id = '.$USER->id.' || u.id = '.$USER->id.')';
		if($statusId == 2){
			$where = "WHERE (c.request_status = 1 || c.request_status = 2) ";
			$orderBy = "ORDER BY c.reponse_on DESC ";
		}else{
			//$where = " WHERE (c.request_status = 0)";
			$where = 'WHERE ((c.request_status = 0) || (c.request_status = 3 AND c.user_id = '.$USER->id.'))';
			$orderBy = "ORDER BY c.requested_on DESC ";
		}
		if($searchText != ''){
			$where .= " AND (LOWER(request_type) LIKE '%".$searchText."%' || LOWER(request_text) LIKE '%".$searchText."%')";
		}
		$programCoursesSql = "SELECT ##SELECT_VALUE## FROM 
						(
							(
								SELECT CONCAT_WS('_',CONVERT( s.id, char ),CONVERT( 0, char ),CONVERT( c.id, char ),u.id) as dif_id,se.id, s.id as classid,c.id as course_id,concat(c.fullname,': ',s.name) as fullname ,s.name, 0 as program_id,'classroom' as request_type,u.id as user_id, se.is_approved as request_status,CONCAT(CONCAT('Request for classroom (',' ',c.fullname,':',' ',s.name,')','##__TIME__## by ',u.firstname,' ',u.lastname),'(',u.username,')') as request_text, se.request_date as requested_on,se.response_date as reponse_on,s.startdate,s.enddate,s.no_of_seats,s.enrolmenttype,s.createdby,se.action_taken_by, count(*) as sessionCount, ss.starttime,ss.duration
								FROM mdl_scheduler s
								LEFT JOIN mdl_course c ON c.id = s.course
								LEFT JOIN mdl_scheduler_enrollment se ON se.scheduler_id = s.id
								LEFT JOIN mdl_scheduler_slots ss ON ss.schedulerid = s.id AND ss.isactive = 1
								LEFT JOIN mdl_user u ON u.id = se.userid
								WHERE $userIdCheck AND c.is_active = 1 AND s.isactive = 1 AND se.id IS NOT NULL
								GROUP BY s.id,se.userid
							)
						UNION
							(
								SELECT CONCAT_WS('_', CONVERT(c.id, CHAR), CONVERT(c.program_id, CHAR), CONVERT(c.course_id, CHAR),c.user_id) AS dif_id,c.id, 0 as classid,c.course_id,cc.fullname ,'name' AS name, c.program_id,if(c.request_type = 'course','online course',c.request_type) as request_type,c.user_id,c.request_status, c.request_text,c.requested_on,c.reponse_on,1 as startdate,2 as enddate,0 as no_of_seats,0 as enrolmenttype,0 as createdby,recepient_id as action_taken_by,1 as sessionCount,0 as starttime,0 as duration
								FROM mdl_course_request_log c
								LEFT JOIN mdl_course as cc ON c.course_id = cc.id
								WHERE c.recepient_id = $USER->id AND c.program_id = 0 AND cc.is_active = 1
							)
						UNION
							(
								SELECT CONCAT_WS('_', CONVERT(c.id, CHAR), CONVERT(c.program_id, CHAR), CONVERT(c.course_id, CHAR),c.user_id) AS dif_id,c.id, 0 as classid,c.course_id,p.name as fullname ,'name' AS name, c.program_id,c.request_type,c.user_id,c.request_status, c.request_text,c.requested_on,c.reponse_on,1 as startdate,2 as enddate,0 as no_of_seats,0 as enrolmenttype,0 as createdby,recepient_id as action_taken_by,1 as sessionCount,0 as starttime,0 as duration
								FROM mdl_course_request_log c LEFT JOIN mdl_programs as p ON c.program_id = p.id
								WHERE c.recepient_id = $USER->id AND c.course_id = 0 AND p.status = 1
							)
						) as c 
						INNER JOIN mdl_user as u ON c.user_id = u.id AND u.suspended=0 AND u.deleted=0 ";
	}
	
	$programCoursesSql .= $where;
        
        //echo $programCoursesSql;die;
	if($getData == 1){
		$dataSql = str_replace("##SELECT_VALUE##",'*,u.firstname,u.lastname,u.username',$programCoursesSql);
		$requests = $DB->get_records_sql($dataSql.$orderBy.$limit);
		return $requests;
	}else{
		$dataCountSql = str_replace("##SELECT_VALUE##",'count(*) as cnt',$programCoursesSql);
		$requestsCount = $DB->get_record_sql($dataCountSql);
		$requestsCount = $requestsCount->cnt;
		return $requestsCount;
	}
}
function getOnlyAssetHtml($assetdata,$courseTypeId,$courseId,$selectionType = 0,$expired = 0,$isProgram = 0,$showClasses = 'all',$showClassStatus = 'all'){
	
		GLOBAL $CFG,$USER,$DB;
		
		$assetDiv = '';
		$curtime = strtotime('now');
		$launchData = array();
		if(!empty($assetdata)){
			if($courseTypeId == $CFG->courseTypeClassroom ){
				$courseStartDate = $DB->get_record_sql("SELECT MIN(s.startdate) AS start_date FROM mdl_scheduler s LEFT JOIN mdl_scheduler_enrollment as se ON s.id = se.scheduler_id AND se.is_approved = 1 WHERE s.isactive = 1 AND s.course = ".$courseId." AND se.userid = ".$USER->id);
				$courseStartDate = $courseStartDate->start_date;
				$assetDiv .= '<div class="assetlabel" style="margin: 8px 0 -6px 20px;">'.get_string('assetlabel','learnercourse').': </div>';
			}else{
				
				$assetLaunchData = $DB->get_records_sql("SELECT * FROM vw_launch_log_data as vw WHERE vw.courseid = ".$courseId." AND vw.userid = ".$USER->id);
				foreach($assetLaunchData as $launchArray){
					$launchData[$launchArray->module.'_'.$launchArray->asset_id][] = $launchArray;
				}
			}
			$assetDiv .= "<div class='section-box'><table class='admintable generaltable' id='learnercourses'><thead><tr><th class='header c0 leftalign  w150' style='' scope='col'>".get_string('title','learnercourse')."</th><th class='header c1 leftalign w350' style='' scope='col'>".get_string('description','learnercourse')."</th>";
			if($courseTypeId == $CFG->courseTypeClassroom ){
				 $assetDiv .= "<th class='header c2 leftalign' style='' scope='col'>".get_string('resourcetype','learnercourse')."</th>
								<th class='header c3 leftalign' style='' scope='col'>".get_string('resourcecreatedby','learnercourse')."</th>
								<th class='header c4 lastcol centeralign' style='' scope='col'>".get_string('actions','learnercourse')."</th>";		
			}else{ 
				 $assetDiv .= "<th class='header c2 leftalign' style='' scope='col'>".get_string('currentstatus','learnercourse')."</th>
								<th class='header c3 leftalign' style='' scope='col'>".get_string('score','learnercourse')."</th>
								<th class='header c4 leftalign' style='' scope='col'>".get_string('lastaccessed','learnercourse')."</th>
								<th class='header c5 lastcol centeralign' style='' scope='col'>".get_string('actions','learnercourse')."</th>";
			}
			$assetDiv .= "		</tr></thead><tbody>";
			$astExists = '';
			$completed_on = array();
			$attemptLaunchScormtableTH = '<thead><tr>';
			$attemptLaunchScormtableTH .= '<th width = "15%">'.get_string('attempt').'</th>';
			$attemptLaunchScormtableTH .= '<th width = "15%">'.get_string('score').'</th>';
			$attemptLaunchScormtableTH .= '<th width = "30%">'.get_string('time_spent').'</th>';
			$attemptLaunchScormtableTH .= '<th width = "25%">'.get_string('access_date').'</th>';
			$attemptLaunchScormtableTH .= '<th width = "15%">'.get_string('launches').'</th>';
			$attemptLaunchScormtableTH .= '</tr></thead>';
			$attemptLaunchRestableTH = '<thead><tr>';
			$attemptLaunchRestableTH .= '<th width = "30%">'.get_string('attempt').'</th>';
			$attemptLaunchRestableTH .= '<th width = "70%">'.get_string('access_date').'</th>';
			$attemptLaunchRestableTH .= '</tr></thead>';
			foreach($assetdata as $assetArray){
				$attemptLaunchDetails = '';
				$removed_null = array();
				$all_completed_data = explode('_',$assetArray->completed_on);
				if($all_completed_data){
					foreach($all_completed_data as $scormdata){
						$scormdata = trim($scormdata);
						
						if($scormdata!=''){
							$removed_null[] = $scormdata;
						}
					}
				}
				$all_completed_data = $removed_null;
				//$removed_null = array_filter($all_completed_data);
			
				$min_completed_on = min($all_completed_data);
				if($min_completed_on){
					$completed_on[] = $min_completed_on;
				}
				
				if(($courseTypeId != $CFG->courseTypeClassroom) ||( $courseTypeId == $CFG->courseTypeClassroom && $assetArray->allow_student_access == '1' && $courseStartDate <= $curtime)){
				$headingText1 = $assetArray->fullname;
				$summary1 = strip_tags($assetArray->description);
				$learningObj1 = strip_tags($assetArray->learningobj);
				$performanceOut1 = strip_tags($assetArray->performanceout);
				$primaryInstructor1 = '';
				$classroomCourseDuration1 = '';
				$classroomInstruction1 = '';
				$classroomCreatedBy1 = '';

				if($courseTypeId == $CFG->courseTypeClassroom ){ 
					$classroomCreatedBy1 = getUsers($assetArray->course_createdby);
					$primaryInstructor1 = $assetArray->primary_instructor?getUsers($assetArray->primary_instructor):$classroomCreatedBy1;
					$classroomInstruction1 = strip_tags($assetArray->classroom_instruction);
					$classroomCourseDuration1 = strip_tags($assetArray->classroom_course_duration);
					
					$primaryInstructor1 = $primaryInstructor1?('<br><strong>'.get_string('primaryinstructor','course').':</strong>&nbsp; '.$primaryInstructor1):'';
					$classroomInstruction1 = $classroomInstruction1?('<br><strong>'.get_string('classroominstruction','course').':</strong>&nbsp; '.$classroomInstruction1):'';
					$classroomCourseDuration1 = $classroomCourseDuration1?('<br><strong>'.get_string('recommendationduration','course').':</strong>&nbsp; '.$classroomCourseDuration1):'';
					$classroomCreatedBy1 = $classroomCreatedBy1?('<br><strong>'.get_string('classroomcreatedby','course').':</strong>&nbsp; '.$classroomCreatedBy1):'';
				}


				$descriptionText1 = $summary1?'<br><strong>'.get_string('description','learnercourse').':</strong>&nbsp;  '.$summary1:'';
				$pcLearningObj1 = $learningObj1?('<br><strong>'.get_string('learningobj','course').':</strong>&nbsp; '.$learningObj1):'';
				$performanceOut1 = $performanceOut1?('<br><strong>'.get_string('performanceOut','course').':</strong>&nbsp; '.$performanceOut1):'';
				
				
				
				$assetIconClass = 'scorm';
				if($assetArray->module == 17){
					$score = get_string('NA','learnercourse');
					$launchClass = "download_icon";
					if($assetArray->combined_stat == 'completed'){
						$launchClass .= " downloaded";
					}
					
					$resourceTypeName = $assetArray->resource_type_name;
					$isRefMat = intval($assetArray->is_resource_reference_material);

					//pr($assetArr);
					$allowStudentAccess = $assetArray->allow_resource_student_access == 1?"Yes":'No';
					$resourceCreatedbyId = intval($assetArray->resource_createdby);
					$resourceCreatedBy = isset($resourceCreatedbyId) && $resourceCreatedbyId!=''?getUsers($resourceCreatedbyId):'';
					
					if($courseTypeId != $CFG->courseTypeClassroom ){ 
						if(!empty($launchData) && isset($launchData['17_'.$assetArray->instanceid])){
							$attemptLaunchDetails .= $attemptLaunchRestableTH;
							$attemptLaunchDetails .= '<tbody>';
							foreach($launchData['17_'.$assetArray->instanceid] as $assetLaunch){
								$lastAccessed = getDateFormat($assetLaunch->created_on, $CFG->customDefaultDateTimeFormat);
								$attemptLaunchDetails .= '<tr>';
									$attemptLaunchDetails .= '<td>'.$assetLaunch->attempt.'</td>';
									$attemptLaunchDetails .= '<td>'.$lastAccessed.'</td>';
								$attemptLaunchDetails .= '</tr>';
							}
							$attemptLaunchDetails .= '</tbody>';
						}
						if($assetArray->is_weblink==1){
							$fullurl = trim($assetArray->weblink_url);
							if($fullurl != ''){
								$fullurl = getWeblinkLaunchURL($fullurl);
								$launchClass = 'launch_icon';
								$linkUrl = "<a href='javascript:;' onclick=\"setCourseLastAccess($USER->id, $assetArray->courseid, $assetArray->instanceid);window.open('".$fullurl."', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \" title='".get_string('launch','learnercourse')."' class='".$launchClass."'   id='coursestatus_".$programOrCourseArray->id."_".$assetArray->instanceid."'>".get_string('download','learnercourse')."</a>";
							}
						}else{
							$context = context_module::instance($assetArray->moduleid);
							$fs = get_file_storage();
							$files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false);
							$file = reset($files);
							if($file){
								$path = '/'.$context->id.'/mod_resource/content/'.$assetArray->revision.$file->get_filepath().$file->get_filename();
								$resourseType = end(explode('.',$path));
								$assetIconClass = getFileClass($resourseType);
								//$assetIconClass = $resourseType;
								$fullurl = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
								$linkUrl = "<a href='javascript:;' onclick=\"setCourseLastAccess($USER->id, $assetArray->courseid, $assetArray->instanceid);window.open('".$fullurl."', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \" title='".get_string('download','learnercourse')."' class='".$launchClass."'   id='coursestatus_".$programOrCourseArray->id."_".$assetArray->instanceid."'>".get_string('download','learnercourse')."</a>";
							}else{
								$linkUrl = "<a href='javascript:;' onclick='alert(\"".get_string('clamdeletedfile')."\")' title='".get_string('download','learnercourse')."' class='".$launchClass."'   id='coursestatus_".$programOrCourseArray->id."_".$assetArray->instanceid."'>".get_string('download','learnercourse')."</a>";
							}
						}
					}else{
						$cmid = $assetArray->cmid;							   
					   if (!$cm = get_coursemodule_from_id('resource', $cmid)) {
							resource_redirect_if_migrated(0, $courseId);
							print_error('invalidcoursemodule');
					   }else{
							$resource = $DB->get_record('resource', array('id'=>$cm->instance));
							
							if(count($resource) > 0 ){
								$context = context_module::instance($cm->id);
								$fs = get_file_storage();
								$files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
					
								if (count($files) < 1) {
									//resource_print_filenotfound($resource, $cm, $course);
									//die;
								} else {
									$file = reset($files);
									unset($files);
								}
								if($file){
									$path = '/'.$context->id.'/mod_resource/content/'.$resource->revision.$file->get_filepath().$file->get_filename();
									//$fullurl = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
									$resourseType = end(explode('.',$path));
									$assetIconClass = getFileClass($resourseType);
									$fullurl = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
									$linkUrl = "<a href='javascript:;' onclick=\"window.open('".$fullurl."', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \" title='".$resoursename."'   ><span class = 'download_icon ' style=''></span>".$resoursename."</a>";
								}else{
									$linkUrl = "<a href='javascript:;' onclick='alert(\"".get_string('clamdeletedfile')."\")' title='".$resoursename."'   ><span class = 'download_icon ' style=''></span>".$resoursename."</a>";
								}
							}
						}
					}

				}else{
					if(!empty($launchData) && isset($launchData['18_'.$assetArray->instanceid])){
						$attemptLaunchDetails .= $attemptLaunchScormtableTH;
						$attemptLaunchDetails .= '<tbody>';
						foreach($launchData['18_'.$assetArray->instanceid] as $assetLaunch){
							$lastAccessed = getDateFormat($assetLaunch->created_on, $CFG->customDefaultDateTimeFormat);
							if($assetLaunch->score == ''){
								$assetLaunch->score = 0;
							}
							if($assetLaunch->total_time == ''){
								$assetLaunch->total_time = getMDash();
							}
							$attemptLaunchDetails .= '<tr>';
								$attemptLaunchDetails .= '<td>'.$assetLaunch->attempt.'</td>';
								$attemptLaunchDetails .= '<td>'.$assetLaunch->score.'</td>';
								$attemptLaunchDetails .= '<td>'.$assetLaunch->total_time.'</td>';
								$attemptLaunchDetails .= '<td>'.$lastAccessed.'</td>';
								$attemptLaunchDetails .= '<td>'.$assetLaunch->attempt_launch.'</td>';
							$attemptLaunchDetails .= '</tr>';
						}
						$attemptLaunchDetails .= '</tbody>';
					}
					$scores = explode('_',$assetArray->score);
					foreach($scores as $keys=>$scor){
						$scores[$keys] = round((float)$scor);
					}
					//pr($scores);
					$score = max($scores);
					//$launchurl = $CFG->wwwroot.'/mod/scorm/player.php?a='.$assetArray->instanceid.'&cm='.$assetArray->moduleid.'&scoid='.$assetArray->scoid.'&currentorg='.$assetArray->organization.'&newattempt=on&display=popup&mode=normal';
					$launchurl = $CFG->wwwroot.'/mod/scorm/player.php?a='.$assetArray->instanceid.'&cm='.$assetArray->moduleid.'&scoid='.$assetArray->scoid.'&currentorg=&newattempt=on&display=popup&mode=normal';
					$linkUrl = "<a href=\"javascript:void(0);\" onclick=\"window.open('".$launchurl."', '', 'width=".$assetArray->width.",height=".$assetArray->height.",toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \"  title='".get_string('launch','learnercourse')."' class = \"launch_icon\">".get_string('launch','learnercourse')."</a>";
				}
				if($assetArray->lastaccessed){
					//$remainAccessedTime = time() - $assetArray->lastaccessed;
					$lastAccessed = getDateFormat($assetArray->lastaccessed, $CFG->customDefaultDateTimeFormat);
				}else{
					 $lastAccessed = get_string('notaccessed','learnercourse');
				}
				$diffKey = "attempt_".$assetArray->instanceid.'_'.$assetArray->module;
				
			   $astExists .= "<tr class='r0 lastrow' rel = '".$diffKey."'>
									<td class='leftalign  w150 cell c0' style=''><span class = '".$assetIconClass."'></span><div class='assest_title_name'>".$assetArray->asset_name."</div></td>
									<td class='leftalign w350 cell c1' style=''>".($assetArray->asset_info?nl2br($assetArray->asset_info):'')."</td>";
				if($expired == 1){
					$linkUrl = get_string('expired');
				}	
				if($courseTypeId == $CFG->courseTypeClassroom ){
				
					 $astExists .= "<td class='leftalign cell c3' style=''>".$resourceTypeName."</td>
								<td class='leftalign  cell c4' style=''>".$resourceCreatedBy."</td>
								<td class='centeralign cell c5 lastcol' style=''>".$linkUrl."</td>";
								
				}else{	
					$attemptDiv = '<div class="full-height">';
					$attemptStatusRow = '';
					$attemptDiv .= '<div class = "asset-link">';
					$attemptDiv .= $linkUrl;
					$attemptDiv .= '</div>';
					if($CFG->showAttempt == 1 && $assetArray->max_attempt != 0 && $assetArray->max_attempt != ''){
							$attemptDiv .= '<div class = "attempt_div" id = "'.$diffKey.'"><a href = "javascript:void(0);" class = "showattempt attempt_'.$assetArray->instanceid.'_'.$assetArray->module.'" >'.get_string("attempt").': '.$assetArray->max_attempt.'</a></div>';
					}
					$attemptDiv .= '</div>';
					 $astExists .= "<td class='leftalign cell c2' style=''>".ucwords(str_replace('incomplete', get_string('inprogress','learnercourse'), $assetArray->combined_stat))."</td>";
                                         
                                          if($score=='0'){
                                            $score = getMDash(); 
                                         }
								$astExists .="<td class='leftalign cell c3' style=''>".$score."</td>
								<td class='leftalign  cell c4' style=''>".$lastAccessed."</td>
								<td class='centeralign cell c5 lastcol' style=''>".$attemptDiv."</td>";
				}
						
					$astExists .= "</tr>";
					if($attemptLaunchDetails != ''){
						$astExists .='<tr class="attempt_row"><td colspan = "6"><div class="session-box">';
						$astExists .= '<table class="admintable generaltable">';
						$astExists .= $attemptLaunchDetails;
						$astExists .= '</table>';
						$astExists .= '</div></td></tr>';
					}
				}
			}
			//echo $selectionType;
			$certificateHtml = ""; 
			$certificateLink = "";
			if($selectionType==2 && $CFG->isCertificate==1 && $isProgram==0){
				$completed_on = max($completed_on);
				
				if($courseTypeId==1){
					$isCertificateEnabled = isCertificateEnabled($courseId,'course');
					if($isCertificateEnabled==1){
					$certificateLink = certificateLink($courseId,'course',$completed_on);
					$certificateHtml .= "<tr><td colspan='6'>".$certificateLink."</td></tr>";
					}
				
				}
			}
			
			if($astExists == ''){
				if($courseTypeId == $CFG->courseTypeClassroom && $courseStartDate >= $curtime){
					$assetDiv .= '<tr><td colspan = "5">'.get_string('cannotAccessAssets').'</td></tr>';
				}else{
					$assetDiv .= '<tr><td colspan = "5">'.get_string('no_results').'</td></tr>';
				}
			}else{
				$assetDiv .= $astExists.$certificateHtml;
				
			}
			$assetDiv .= "</tbody></table></div>";
		}
		if($courseTypeId == $CFG->courseTypeClassroom ){
			$classHtml = getClassesPreviewHtmlForLearner($courseId,0,1,$selectionType,$isProgram,$showClasses,$showClassStatus);
			if(trim($classHtml) == ''){
				$assetDiv = '</br>'.get_string('reguest_class').'</br>';
			}else{
				$findme = get_string('norecordfound','course');
				$pos = strpos($classHtml, $findme);
				if($pos){
					$assetDiv = $classHtml;
					$assetDiv .= '<div class="assetlabel" style="margin: 8px 0 -6px 20px;">'.get_string('assetlabel','learnercourse').': </div>';
					$assetDiv .= "<div class='section-box'><table class='admintable generaltable' id='learnercourses'><thead><tr><th class='header c0 leftalign  w150' style='' scope='col'>".get_string('title','learnercourse')."</th><th class='header c1 leftalign w350' style='' scope='col'>".get_string('description','learnercourse')."</th>";
					$assetDiv .= "<th class='header c2 leftalign' style='' scope='col'>".get_string('resourcetype','learnercourse')."</th>
					<th class='header c3 leftalign' style='' scope='col'>".get_string('resourcecreatedby','learnercourse')."</th>
					<th class='header c4 lastcol centeralign' style='' scope='col'>".get_string('actions','learnercourse')."</th>";		
					$assetDiv .= "		</tr></thead><tbody>";
					$assetDiv .= '<tr><td colspan = "5">'.get_string('no_results').'</td></tr>';
					$assetDiv .= "</tbody></table></div>";
				}else{
			   		$assetDiv = $classHtml.$assetDiv;
				}
			}
			
			
		}
		
		
		return $assetDiv;
}
function getCourseAssetHtml($courseId,$courseTypeId,$divKey, $expired=0,$selectionType,$isProgram = 0){
	GLOBAL $USER,$DB,$CFG;
	$elementId = explode('_',$divKey);
	$programId = $elementId[0];
	$assetDiv = '';
	if($courseTypeId == $CFG->courseTypeClassroom){
		$sectionArr = getTopicsSection($courseId, $USER->id, '', $courseTypeId);
		if(count($sectionArr) > 0 ){
			foreach($sectionArr as $sectionId=>$sections){
				if(count($sections->courseModule) > 0){
					$topicsCourseNew = array();
					foreach($sections->courseModule as $cmId=>$courseModule){

						$topicsCourse = $courseModule['topicCourse'];
						if(count($topicsCourse)>0){
							foreach($topicsCourse as $topics){
								if($topics->allow_student_access == 1){
									$topicsCourseNew[$topics->resource_type_id]['name'] = $topics->resource_type;
									$topicsCourseNew[$topics->resource_type_id]['details'][] = $topics;
								}
							}
						}
					}

				}

			}

		}
		$assetdata = array();
		$j = 0;
		$courseDetails = $DB->get_record("course",array('id'=>$courseId));
		if(!empty($topicsCourseNew)){
			foreach($topicsCourseNew as $topicAsset){
				foreach($topicAsset['details'] as $topicsCourseArr){
						$assetdata[$j] = $topicsCourseArr;
						$assetdata[$j]->module = 17;
						$assetdata[$j]->description = $topicsCourseArr->resourcesummary;
						$assetdata[$j]->allow_student_access = $topicsCourseArr->allow_student_access;
						$assetdata[$j]->asset_info = $topicsCourseArr->resourcesummary;
						$assetdata[$j]->asset_name = $topicsCourseArr->resoursename;
						$assetdata[$j]->resource_type_name = $topicsCourseArr->resource_type;
						$assetdata[$j]->coursetype_id = $courseDetails->coursetype_id;
						$assetdata[$j]->primary_instructor = $courseDetails->primary_instructor;
						$assetdata[$j]->classroom_instruction = $courseDetails->classroom_instruction;
						$assetdata[$j]->classroom_course_duration = $courseDetails->classroom_course_duration;
						$assetdata[$j]->course_createdby = $courseDetails->createdby;
						$assetdata[$j]->resource_createdby = $topicsCourseArr->rcreatedby;
						$assetdata[$j]->learningobj = $courseDetails->learningobj;
						$assetdata[$j]->performanceout = $courseDetails->performanceout;
						$j++;
				}
			}
		}
	}else{
		$whr = '';
		if($programId != 0){
			$whr = ' AND c.program_id = '.$programId;
		}
		$assetdata = $DB->get_records_sql("SELECT CONCAT_WS('_','p', CONVERT(c.program_id, CHAR), CONVERT(c.courseid, CHAR), CONVERT(c.instanceid, CHAR), CONVERT(c.module, CHAR)) AS difId,c.*
		FROM (	(
		SELECT *
		FROM vw_user_full_program_resource) UNION (
		SELECT *
		FROM vw_user_full_program_scorm_status)) AS c
		WHERE c.userid = ".$USER->id." AND c.is_resource_reference_material = '0' AND IF(c.resource_type_id = 0, '1 = 1', c.allow_resource_student_access = 1) AND c.courseid = ".$courseId.$whr);
	}

	if($courseTypeId == $CFG->courseTypeClassroom){
		$assetDiv .= "<div class='e-box e-box-inner' id='ebox0_".$divKey."' style='display: none;'>";
		$assetDiv .= getOnlyAssetHtml($assetdata,$courseTypeId,$courseId,$selectionType,$expired,$isProgram);
		$assetDiv .= "</div>";
	}else{
		if(!empty($assetdata)){
			$assetDiv .= "<div class='e-box e-box-inner' id='ebox0_".$divKey."' style='display: none;'>";
			$assetDiv .= getOnlyAssetHtml($assetdata,$courseTypeId,$courseId,$selectionType,$expired,$isProgram);
			$assetDiv .= "</div>";
		}else{
			$assetDiv .= "<div class='e-box e-box-inner' id='ebox0_".$divKey."' style='display: none;'>";
			$assetDiv .= "</br></br>".get_string('no_results');
			$assetDiv .= "</div>";
		}
	}
	return $assetDiv;
}
function getProgramAsset($programId,$getAsset,$expired = 0,$selectionType = 0){
	GLOBAL $DB,$CFG,$USER;
	$courseDiv = '';
	$checkCriteriaCondition = 0;
	if($getAsset == 1){
		$checkCriteriaCondition = 1;
		$programCourses =  $DB->get_records_sql("SELECT c.id, c.fullname,c.coursetype_id, GROUP_CONCAT(pcc.course_id SEPARATOR ',') AS criteria_id, c.performanceout, c.learningobj, f.contextid, c.summary, f.component, f.filearea, f.filename,pci.credithours,c.enddate FROM mdl_course as c LEFT JOIN mdl_program_course as pc ON pc.courseid = c.id AND pc.is_active = 1 LEFT JOIN mdl_program_course_criteria AS pcc ON pc.id = pcc.program_map_id LEFT JOIN vw_user_program_credithours_intermediate as pci ON pci.program_id = pc.programid AND pci.id = c.id AND pci.user_id = ".$USER->id." LEFT JOIN mdl_context AS ct ON ct.instanceid = c.id AND ct.contextlevel = 50 LEFT JOIN mdl_files as f On f.contextid = ct.id AND f.component = 'course' AND f.filearea = 'overviewfiles' AND f.filename !='' AND f.filename !='.' WHERE c.is_active = 1 AND pc.programid = ".$programId." GROUP BY pc.courseid ORDER BY pc.course_order");
	}else{
		$programCourses =  $DB->get_records_sql("SELECT c.id, c.fullname,c.coursetype_id,0 AS criteria_id, c.performanceout, c.learningobj, f.contextid, c.summary, f.component, f.filearea, f.filename,pci.credithours,c.enddate FROM mdl_course as c LEFT JOIN mdl_program_course as pc ON pc.courseid = c.id AND pc.is_active = 1 LEFT JOIN vw_user_program_available_credithours as pci ON pci.program_id = pc.programid AND pci.id = c.id AND (pci.user_id = ".$USER->id." || pci.user_id = 0) LEFT JOIN mdl_context AS ct ON ct.instanceid = c.id AND ct.contextlevel = 50 LEFT JOIN mdl_files as f On f.contextid = ct.id AND f.component = 'course' AND f.filearea = 'overviewfiles' AND f.filename !='' AND f.filename !='.' WHERE c.is_active = 1 AND pc.programid = ".$programId);
	}
	$pcCompletionData = getAssetInfoForProgram($programId);
	$completionArray = array();
	if(!empty($pcCompletionData)){
		foreach($pcCompletionData as $pCourseData){
			$completionArray[$pCourseData->courseid]['stat'] = $pCourseData->course_stat;
			$completionArray[$pCourseData->courseid]['fullname'] = $pCourseData->fullname;
		}
	}
	$pClassCompletionData = $DB->get_records_sql("SELECT `com`.`program_id` AS `program_id`,`com`.`id` AS `id`,com.course_name as fullname FROM `vw_user_program_classroom_stat` `com` WHERE user_id = ".$USER->id." AND program_id = ".$programId." AND classroom_stat != 'completed'");
	if(!empty($pClassCompletionData)){
		foreach($pClassCompletionData as $pClass){
			$completionArray[$pClass->id]['stat'] = 'in progress';
			$completionArray[$pClass->id]['fullname'] = $pClass->fullname;
		}
	}
	$completed_on = getProgramCompletionDate($programCourses);
	if(!empty($programCourses)){
		$j = 0;
		foreach($programCourses as $assetArr){
			$key = $programId.'_'.$assetArr->id;
			$courseTypeId = getCourseTypeIdByCourseId($assetArr->id);
			$complianceDiv = getCourseComplianceIcon($assetArr->id);
			$j++;
			$courseImgURL = $CFG->courseDefaultImage;
			if($assetArr->contextid && $assetArr->component && $assetArr->filearea && $assetArr->filename){
				$programImgPath = $CFG->wwwroot.'/pluginfile.php/'.$assetArr->contextid.'/'.$assetArr->component.'/'.$assetArr->filearea.'/'.$assetArr->filename;
				
				if(isUrlExists($programImgPath)){
				  $courseImgURL = $programImgPath;
				}
				
			}
			$expiredcourse = $expired;
			if($courseTypeId != $CFG->courseTypeClassroom && $assetArr->enddate != ''){
				if(($assetArr->enddate+86399) < strtotime('now')){
					$expiredcourse = 1;
				}
			}
			$courseImg = '<img src="'.$courseImgURL.'" alt="'.$assetArr->fullname.'" title="'.$assetArr->fullname.'" border="0" />';
			$courseImagePath = '<div class = "course-img" >'.$courseImg.'</div>';
			$classAlt2 = $j%2==0?'even-course':'odd-course';

			$headingText1 = $assetArr->fullname;
			$summary1 = $assetArr->summary;
			$learningObj1 = $assetArr->learningobj;
			$performanceOut1 = $assetArr->performanceout;

			$descriptionText1 = $summary1?'<strong>'.get_string('description','learnercourse').': </strong>'.$summary1:'';
			
			$pcLearningObj1 = $learningObj1?('<br /><strong>'.get_string('learningobj','course').': </strong> '.$learningObj1):'';
			$performanceOut1 = $performanceOut1?('<br /><strong> '.get_string('performanceOut','course').': </strong>'.$performanceOut1):'';
			$creditHoursText = "";
			if($courseTypeId != $CFG->courseTypeClassroom){
				$creditHours = array_sum(explode('_',$assetArr->credithours));
				$creditHoursText = '<br/><strong>'.get_string('coursecredithours').':</strong> '.setCreditHoursFormat($creditHours);
			}
			$courseCompleteText = '';
			$showAsset = array();
			$courseCompleteArray = array();
			if($checkCriteriaCondition == 1 && isset($assetArr->criteria_id) && !empty($assetArr->criteria_id) && $assetArr->criteria_id != 0){
				$criteriaCourses = explode(',',$assetArr->criteria_id);
				foreach($criteriaCourses as $criCourse){
						if(isset($completionArray[$criCourse]) && $completionArray[$criCourse]['stat'] != 'completed'){
							$showAsset[] = 0;
							$courseCompleteArray[] = $completionArray[$criCourse]['fullname'];
						}
				}
				if(!empty($courseCompleteArray)){
					$courseCompleteArray = implode(', ',$courseCompleteArray);
					$courseCompleteText = '<br/><strong>'.get_string('accessed_after').':</strong> '.$courseCompleteArray;
				}
			}
			$courseDiv .= '<div class="programcourse no-course '.$classAlt2.'" rel="'.$key.'">';
				$courseDiv .= $courseImagePath;
				$courseDiv .= '<div class="pull-left left-content">';
					$courseDiv .= '<div class="c-heading">'.$headingText1.' '.$complianceDiv.'</div>';
					$courseDiv .= '<div class="c-text">'.$descriptionText1.$pcLearningObj1.$performanceOut1.$creditHoursText.$courseCompleteText.'</div>';
				$courseDiv .= '</div>';	
					if($getAsset == 1 && !in_array(0,$showAsset)){
							$courseDiv .= '<div class="pull-right right-icon cbox toggle-button-programcourse button-orange" rel="'.$key.'" id="plus-minus-icon-'.$key.'"><a href="javascript:void(0)" title="Show/Hide Course Material(s)"></a><img alt="" src="'.$CFG->wwwroot.'/theme/image.php?theme=gourmet&component=core&image=a%2Fadd_file"></div>';
							$courseDiv .= getCourseAssetHtml($assetArr->id,$courseTypeId,$key,$expiredcourse,$selectionType,1);
					}
			$courseDiv .= '</div>';
			
		}
		if($CFG->isCertificate==1 && $selectionType==2)
		{
			$isCertificateEnabled = isCertificateEnabled($programId,'programme');
			if($isCertificateEnabled==1){
				$certificateLink = certificateLink( $programId, 'programme', $completed_on );
				$courseDiv .= '<div class="certificate_of_completion"> ' . $certificateLink . '</div>';
			}
		}
	}else{
		$courseDiv .= get_string('no_results');
	}
	return $courseDiv;
}

    /**
	 * This function is using to get main content of course/program/classroom/team/department
	 * @global object
	 * @return array $moduleArr is an array of content that we need to show on header of course/program/classroom/team/department
	 * @return html $html	
	
	 */
	 
	function getModuleHeaderHtml($moduleArr, $module='', $noBorder = 0){
		global $DB, $CFG, $USER;
		
		$htmlDiv = '';
		if($module){
		    $noBorderStyle = $noBorder == 1?'style="border:none"':'';
				
			if($module == $CFG->classModule){
					//pr($moduleArr);die;
					$fullname = $moduleArr->classroomname;
					$moduleArr->fullname = $fullname;
					$className = $moduleArr->classname;
					$durationText = getClassDuration($moduleArr->startdate, $moduleArr->enddate);
					$classInstructorName = fullname(getClassInstructor($moduleArr));
					$noOfSeats = $moduleArr->no_of_seats;
					$submittedByName = $moduleArr->submitted_by_name;
					$className = $className?('<strong>'.get_string('name','scheduler').':</strong>&nbsp;'.$className):'';
					$durationText = $durationText?('<strong>'.get_string('class_date_label').':</strong>&nbsp;'.$durationText):'';
					$classInstructorName = $classInstructorName?('<strong>'.get_string('instructor','scheduler').':</strong>&nbsp;'.$classInstructorName):'';
					$noOfSeats = $noOfSeats?('<strong>'.get_string('no_of_seats','scheduler').':</strong>&nbsp;'.$noOfSeats):'';
					$submittedByName = $submittedByName?('<strong>'.get_string('classsubmittedby','scheduler').':</strong>&nbsp;'.$submittedByName):'';
					
					$imgIcon = getCourseImage($moduleArr);
					
					
					/*$htmlDiv .= "<div class='class_other_details'>";
					$htmlDiv .= "<div><span class='heading'>".get_string('classroomcourse').":</span> <span class='value'>".$classroomName."</<span></div>";
					$htmlDiv .= "<div><span class='heading'>".get_string('name','scheduler').":</span> <span>".$className."</span></div>";
					$htmlDiv .= "<div><span class='heading'>".get_string('duration','scheduler').":</span> <span>".$durationText."</span></div>";
					$htmlDiv .= "<div><span class='heading'>".get_string('instructor','scheduler').":</span> <span>".$classInstructorName."</span></div>";
					$htmlDiv .= "<div><span class='heading'>".get_string('no_of_seats','scheduler').":</span> <span>".$noOfSeats."</span></div>";
					$htmlDiv .= "</div>";*/
						
					$htmlDiv .= '<div class="course-header" '.$noBorderStyle.' >';
					  $htmlDiv .= '<div class="course-img" style="float: left;">'.$imgIcon.'</div>';
					  $htmlDiv .= '<div class="pull-left left-content" style="padding-left: 65px;">';
						$htmlDiv .= $fullname?'<div class="c-heading">'.$fullname.'</div>':'';
						$htmlDiv .= $className?'<div class="c-text">'.$className.'</div>':'';
						$htmlDiv .= $durationText?'<div class="c-text">'.$durationText.'</div>':'';
						$htmlDiv .= $classInstructorName?'<div class="c-text">'.$classInstructorName.'</div>':'';
						$htmlDiv .= $noOfSeats?'<div class="c-text">'.$noOfSeats.'</div>':'';
						$htmlDiv .= $submittedByName?'<div class="c-text">'.$submittedByName.'</div>':'';
					  $htmlDiv .= '</div>';
					$htmlDiv .= '</div>';
				
			}else{
				
				$imgIcon = '';
				$fullname = '';
				$summary = '';
				$suggestedUse = '';
				$learningObj = '';
				$performanceOut = '';
				$accessedAfter = '';
				$primaryInstructor = '';
				$classroomInstruction = '';
				$classroomCourseDuration = '';
				$complianceDiv = '';
				$creditHours = '';
				$createdBy = '';
				$teamDepartment = '';
				$receivedDate = '';
			
				if(in_array($module, array($CFG->courseModule, $CFG->classroomModule))){
				
					$courseId = $moduleArr->id;
					$fullname = $moduleArr->fullname;
					/*$summary = strip_tags($moduleArr->summary);
					$suggestedUse = strip_tags($moduleArr->suggesteduse);
					$learningObj = strip_tags($moduleArr->learningobj);
					$performanceOut = strip_tags($moduleArr->performanceout);*/
					
					$summary = $moduleArr->summary;
					$suggestedUse = $moduleArr->suggesteduse;
					$learningObj = $moduleArr->learningobj;
					$performanceOut = $moduleArr->performanceout;
					//$summary = strlen($summary)>150?substr($summary,0,150).'...':$summary;
					//$suggestedUse = strlen($suggestedUse)>150?substr($suggestedUse,0,150).'...':$suggestedUse;
					//$learningObj = strlen($learningObj)>150?substr($learningObj,0,150).'...':$learningObj;
					//$performanceOut = strlen($performanceOut)>150?substr($performanceOut,0,150).'...':$performanceOut;
					
					$summary = $summary?('<strong>'.get_string('description','course').':</strong>&nbsp;'.$summary):'';
					$suggestedUse = $suggestedUse?('<strong>'.get_string('suggesteduse','course').':</strong>&nbsp;'.$suggestedUse):'';
					$learningObj = $learningObj?('<strong>'.get_string('learningobj','course').':</strong>&nbsp;'.$learningObj):'';
					$performanceOut = $performanceOut?('<strong>'.get_string('performanceOut','course').':</strong>&nbsp;'.$performanceOut):'';
					$accessedAfter = $moduleArr->criteria_course_name?('<strong>'.get_string('accessed_after').':</strong>&nbsp;'.$moduleArr->criteria_course_name):'';
					
					$createdBy = $moduleArr->createdby?getUsers($moduleArr->createdby):'';
					$createdBy = $createdBy?('<strong>'.get_string('createdby').':</strong>&nbsp; '.$createdBy):'';
					$imgIcon = getCourseImage($moduleArr);
					
					$creditHours = '';  
					if($module == $CFG->classroomModule){ 
					
						$primaryInstructor = $moduleArr->primary_instructor?getUsers($moduleArr->primary_instructor):$classroomCreatedBy;
						if($USER->archetype != $CFG->userTypeStudent){
							$classroomInstruction = ($moduleArr->classroom_instruction);
							$classroomCourseDuration = ($moduleArr->classroom_course_duration);
							$classroomInstruction = strlen($classroomInstruction)>150?substr($classroomInstruction,0,150).'...':$classroomInstruction;
							$classroomCourseDuration = strlen($classroomCourseDuration)>150?substr($classroomCourseDuration,0,150).'...':$classroomCourseDuration;
						}
						
						$primaryInstructor = $primaryInstructor?('<strong>'.get_string('primaryinstructor','course').':</strong>&nbsp;'.$primaryInstructor):'';
						$classroomInstruction = $classroomInstruction?('<strong>'.get_string('classroominstruction','course').':</strong>&nbsp;'.$classroomInstruction):'';
						$classroomCourseDuration = $classroomCourseDuration?('<strong>'.get_string('recommendationduration','course').':</strong>&nbsp;'.$classroomCourseDuration):'';
					}else{
					   $complianceDiv = getCourseComplianceIcon($courseId);
					   $creditHours  = $moduleArr->credithours?$moduleArr->credithours:'';
					   $creditHours = setCreditHoursFormat($creditHours);
					   $creditHours = $creditHours?('<strong>'.get_string('coursecredithourswithtime').':</strong>&nbsp;'.$creditHours):'';
					}
					
					  
				
						
				}elseif($module == $CFG->programModule){
				
				    $creditHours = '';
					$fullname = $moduleArr->name;
					$summary = ($moduleArr->description);
					//$summary = strlen($summary)>150?substr($summary,0,150).'...':$summary;
					$summary = $summary?('<strong>'.get_string('description','course').':</strong>&nbsp;'.$summary):'';
					
					$createdBy = $moduleArr->createdby?getUsers($moduleArr->createdby):'';
					$createdBy = $createdBy?('<strong>'.get_string('createdby').':</strong>&nbsp; '.$createdBy):'';
					$imgIcon = getProgramImage($moduleArr);
				
				
				}elseif($module == $CFG->teamModule){
					
					$fullname = $moduleArr->name;
					$summary = ($moduleArr->description);
					//$summary = strlen($summary)>150?substr($summary,0,150).'...':$summary;
					$summary = $summary?('<strong>'.get_string('description','course').':</strong>&nbsp;'.$summary):'';
					$createdBy = $moduleArr->createdby?getUsers($moduleArr->createdby):'';
					$createdBy = $createdBy?('<strong>'.get_string('createdby').':</strong>&nbsp;'.$createdBy):'';
					$imgIcon = getTeamImage($moduleArr);
					
					if($moduleArr->id){
						$sql = "SELECT md.title, md.is_external FROM {$CFG->prefix}group_department mgd LEFT JOIN {$CFG->prefix}department md ON (md.id = mgd.department_id) WHERE mgd.team_id IN (".$moduleArr->id.") limit 1";
						$teamDepartmentArr = $DB->get_record_sql($sql);
						$identifierED = isset($teamDepartmentArr->is_external) && $teamDepartmentArr->is_external == 1 && $teamDepartmentArr->title?getEDAstric('view'):'';
					    $teamDepartment = $identifierED.$teamDepartmentArr->title;
						$teamDepartment = $teamDepartment?('<strong>'.get_string('department').':</strong>&nbsp;'.$teamDepartment):'<strong>'.get_string('department').':</strong>&nbsp;'.get_string("global_group_column");
		           }
					
				
				}elseif($module == $CFG->departmentModule){
	 
					$identifierED = isset($moduleArr->is_external) && $moduleArr->is_external == 1 && $moduleArr->title?getEDAstric('view'):'';
					$fullname = $identifierED.$moduleArr->title;
					$summary = ($moduleArr->description);
					//$summary = strlen($summary)>150?substr($summary,0,150).'...':$summary;
					$summary = $summary?('<strong>'.get_string('description','course').':</strong>&nbsp;'.$summary):'';
					$createdBy = $moduleArr->createdby?getUsers($moduleArr->createdby):'';
					$createdBy = $createdBy?('<strong>'.get_string('createdby').':</strong>&nbsp;'.$createdBy):'';
					$imgIcon = getDepartmentImage($moduleArr);
				}elseif($module == $CFG->userModule){
					GLOBAL $OUTPUT;
					$fullname = $moduleArr->firstname." ".$moduleArr->lastname." (".$moduleArr->username.")";
					$summary = ($moduleArr->description);
					//$summary = strlen($summary)>150?substr($summary,0,150).'...':$summary;
					$summary = $summary?('<strong>'.get_string('description','course').':</strong>&nbsp;'.$summary):'';
					$createdBy = $moduleArr->createdby?getUsers($moduleArr->createdby):'';
					$createdBy = $createdBy?('<strong>'.get_string('createdby').':</strong>&nbsp;'.$createdBy):'';
					$imgIcon = $OUTPUT->user_picture($moduleArr, array('courseid' => SITEID, 'size'=>64));
				}elseif($module == $CFG->messageModule){
					GLOBAL $OUTPUT;
					//$fullname = $moduleArr->message_title." - ".date($CFG->customDefaultDateFormat,$moduleArr->timecreated);
					$fullname = $moduleArr->message_title;
					$receivedDate = $moduleArr->timecreated?date($CFG->customDefaultDateTimeFormat,$moduleArr->timecreated):'';
					$suggestedUse = nl2br($moduleArr->message_content);
					$suggestedUse = $suggestedUse?('<strong>'.get_string('message','messages').':</strong>&nbsp;'.$suggestedUse):'';
					$user = $DB->get_record('user',array('id'=>$moduleArr->message_from));
					$fullnameSender = $user->firstname." ".$user->lastname." (".$user->username.")";
					$summary = '<strong>'.get_string('message_sender','messages').':</strong>&nbsp;'.$fullnameSender;
					$imgIcon = $OUTPUT->user_picture($user, array('courseid' => SITEID, 'size'=>64));
					$receivedDate = $receivedDate?('<strong>'.get_string('received','messages').':</strong>&nbsp;'.$receivedDate):'';
				}

				$htmlDiv .= '<div class="course-header" '.$noBorderStyle.'>';
				  $htmlDiv .= '<div class="course-img">'.$imgIcon.'</div>';
				  $htmlDiv .= '<div class="pull-left left-content">';
					$htmlDiv .= $fullname?'<div class="c-heading">'.$fullname.$complianceDiv.'</div>':'';
					$htmlDiv .= $receivedDate?'<div class="c-text">'.$receivedDate.'</div>':'';
					$htmlDiv .= $teamDepartment?'<div class="c-text">'.$teamDepartment.'</div>':'';
					$htmlDiv .= $summary?'<div class="c-text">'.$summary.'</div>':'';
					$htmlDiv .= $suggestedUse?'<div class="c-text">'.$suggestedUse.'</div>':'';
					$htmlDiv .= $learningObj?'<div class="c-text">'.$learningObj.'</div>':'';
					$htmlDiv .= $performanceOut?'<div class="c-text">'.$performanceOut.'</div>':'';
					$htmlDiv .= $createdBy?'<div class="c-text">'.$createdBy.'</div>':'';
					$htmlDiv .= $creditHours?'<div class="c-text">'.$creditHours.'</div>':'';
					$htmlDiv .= $primaryInstructor?'<div class="c-text">'.$primaryInstructor.'</div>':'';
					$htmlDiv .= $classroomInstruction?'<div class="c-text">'.$classroomInstruction.'</div>':'';
					$htmlDiv .= $classroomCourseDuration?'<div class="c-text">'.$classroomCourseDuration.'</div>':'';
					$htmlDiv .= $accessedAfter?'<div class="c-text">'.$accessedAfter.'</div>':'';
				  $htmlDiv .= '</div>';
				$htmlDiv .= '</div>';
			}
			
			  
		}

		return $htmlDiv;
		
    }	
	
	// To get mdash html 
	function getMDash(){
	  return "&mdash;";
	}
	// To get mdash html for csv
	function getMDashForCSV(){
	  return "�";
	}
	
	// To get ndash html 
	function getNDash(){
	   return "&ndash;";
	}
	
	// To get ndash html 
	function getNDashForCSV(){
	   return "�";
	}
	
	// check Page not is numeric
	function checkPageIsNumeric($returnUrl,$page){
		if (!is_numeric($page) && $page){
			
			redirect($returnUrl);
		}
	}
	function getUserNameFormat($user){
		return $user->firstname.' '.$user->lastname;
	}
	function courseEnrolMail($courseId = 0,$userId,$departmentId = 0,$teamId = 0,$programId = 0,$endDate = '',$updateMail = 0){
		GLOBAl $CFG,$USER,$DB;
		$userDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email,u.parent_id FROM mdl_user u WHERE u.id = ".$userId);
		$userDetails->name = getUserNameFormat($userDetails);
		if($courseId != 0){
			$courseDetails = $DB->get_record_sql("SELECT c.id,c.fullname FROM mdl_course as c WHERE c.id =  ".$courseId);
			$endDateText = '';
			if($endDate != ''){
				$endDateArr = array('enddate'=>date($CFG->customDefaultDateFormat,$endDate),
							'lmsName'		=> $CFG->emailLmsName);
				$endDateContent = (object)$endDateArr;
				$endDateText = get_string('available_till','email',$endDateContent);
			}
			if($departmentId != 0){
				$departmentDetails = $DB->get_record_sql("SELECT d.id,d.title FROM mdl_department as d WHERE d.id = ".$departmentId);
				$emailContent = array(
							'name'=>$userDetails->name,
							'coursename'=>$courseDetails->fullname,
							'departmentname'=>$departmentDetails->title,
							'datetime'=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
							'lmsLink'		=> $CFG->emailLmsLink,
							'lmsName'		=> $CFG->emailLmsName,
							'location'=>'India');
				$emailContent = (object)$emailContent;
				$emailText = get_string('course_enrol_department','email',$emailContent);
				$emailText .= get_string('email_footer','email');
				$emailText .= get_string('from_email','email',$emailContent);
				$subject	= get_string('course_enrol_department_subject','email',$emailContent);
				add_to_log(1, 'Enrol course', 'add', 'user/assigncourses.php?id='.$courseId.'&departmentid='.$departmentId, "Enrol course in department: ", 0, $USER->id);
			}elseif($teamId != 0){
				$teamDetails = $DB->get_record_sql("SELECT g.id,g.name FROM mdl_groups as g WHERE g.id = ".$teamId);
				$emailContent = array(
							'name'=>$userDetails->name,
							'coursename'=>$courseDetails->fullname,
							'availabletill'=>$endDateText,
							'teamname'=>$teamDetails->name,
							'datetime'=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
							'lmsLink'		=> $CFG->emailLmsLink,
							'lmsName'		=> $CFG->emailLmsName,
							'location'=>'India');
				$emailContent = (object)$emailContent;
				$emailText = get_string('course_enrol_team','email',$emailContent);
				$emailText .= get_string('email_footer','email');
				$emailText .= get_string('from_email','email',$emailContent);
				$subject	= get_string('course_enrol_team_subject','email',$emailContent);
				add_to_log(1, 'Enrol course', 'add', 'user/assigncourses.php?id='.$courseId.'&teamid='.$teamId, "Enrol course in team: ", 0, $USER->id);
			}else{
				$emailContent = array(
							'name'=>$userDetails->name,
							'coursename'=>$courseDetails->fullname,
							'availabletill'=>$endDateText,
							'datetime'=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
							'lmsLink'		=> $CFG->emailLmsLink,
							'lmsName'		=> $CFG->emailLmsName,
							'location'=>'India');
				$emailContent = (object)$emailContent;
				$emailText = get_string('user_enrol_course','email',$emailContent);
				$emailText .= get_string('email_before_footer_enrol_text','email');
				$emailText .= get_string('email_footer','email');
				$emailText .= get_string('from_email','email');
				$subject	= get_string('course_enrol_subject','email',$emailContent);
				add_to_log(1, 'Enrol course', 'add', 'user/assigncourses.php?id='.$courseId.'&user='.$userId, "Enrol course to user: ", 0, $USER->id);
			}
		}else{
			$courseDetails = $DB->get_record_sql("SELECT p.id,p.name,p.description FROM mdl_programs as p WHERE p.id = ".$programId);
			$endDateText = '';
			if($endDate != ''){
				$endDateArr = array('enddate'=>date($CFG->customDefaultDateFormat,$endDate),
							'lmsName'		=> $CFG->emailLmsName);
				$endDateContent = (object)$endDateArr;
				$endDateText = get_string('available_till','email',$endDateContent);
			}
			$summary = '';
			if($CFG->showProgramDescriptionInMail == 1){
				$summary = $courseDetails->description;
			}
			if($departmentId != 0){
				$departmentDetails = $DB->get_record_sql("SELECT d.id,d.title FROM mdl_department as d WHERE d.id = ".$departmentId);
				$emailContent = array(
							'name'=>$userDetails->name,
							'programname'=>$courseDetails->name,
							'departmentname'=>$departmentDetails->title,
							'datetime'=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
							'lmsLink'		=> $CFG->emailLmsLink,
							'lmsName'		=> $CFG->emailLmsName,
							'summary'		=> $summary,
							'location'=>'India');
				$emailContent = (object)$emailContent;
				$emailText = get_string('program_enrol_department','email',$emailContent);
				$emailText .= get_string('email_footer','email');
				$emailText .= get_string('from_email','email',$emailContent);
				$subject	= get_string('program_enrol_department_subject','email',$emailContent);
				add_to_log(1, 'Enrol Program', 'add', 'program/assignusers.php?program='.$programId.'&departmentid='.$departmentId, "Enrol Program in department: ", 0, $USER->id);
			}elseif($teamId != 0){
				$teamDetails = $DB->get_record_sql("SELECT g.id,g.name FROM mdl_groups as g WHERE g.id = ".$teamId);
				$emailContent = array(
							'name'=>$userDetails->name,
							'programname'=>$courseDetails->name,
							'teamname'=>$teamDetails->name,
							'availabletill'=>$endDateText,
							'datetime'=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
							'lmsLink'		=> $CFG->emailLmsLink,
							'lmsName'		=> $CFG->emailLmsName,
							'summary'		=> $summary,
							'location'=>'India');
				$emailContent = (object)$emailContent;
				if($updateMail == 1){
					$emailText = get_string('program_extend_team','email',$emailContent);
				}else{
					$emailText = get_string('program_enrol_team','email',$emailContent);
				}
				$emailText .= get_string('email_footer','email');
				$emailText .= get_string('from_email','email',$emailContent);
				$subject	= get_string('program_enrol_team_subject','email',$emailContent);
				add_to_log(1, 'Enrol Program', 'add', 'program/assignusers.php?program='.$programId.'&teamid='.$teamId, "Enrol Program in team: ", 0, $USER->id);
			}else{
				$emailContent = array(
							'name'=>$userDetails->name,
							'programname'=>$courseDetails->name,
							'availabletill'=>$endDateText,
							'datetime'=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
							'lmsLink'		=> $CFG->emailLmsLink,
							'lmsName'		=> $CFG->emailLmsName,
							'summary'		=> $summary,
							'location'=>'India');
				$emailContent = (object)$emailContent;
				if($updateMail == 1){
					$emailText = get_string('user_extend_program','email',$emailContent);
					$subject	= get_string('program_enrol_subject','email',$emailContent);
				}else{
					$emailText = get_string('user_enrol_program','email',$emailContent);
					$subject	= get_string('program_enrol_subject','email',$emailContent);
				}
				$emailText .= get_string('email_footer','email');
				$emailText .= get_string('from_email','email',$emailContent);
				add_to_log(1, 'Enrol Program', 'add', 'program/assignusers.php?program='.$programId.'&id='.$userId, "Enrol Program to user: ", 0, $USER->id);
			}
		}
		$fromUser->email = $USER->email;
		$fromUser->firstname = $USER->firstname;
		$fromUser->lastname = $USER->lastname;
		$fromUser->maildisplay = true;
		$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$fromUser->id = $USER->id;
		$fromUser->firstnamephonetic = '';
		$fromUser->lastnamephonetic = '';
		$fromUser->middlename = '';
		$fromUser->alternatename = '';

		$toUser->email = $userDetails->email;
		$toUser->firstname = $userDetails->firstname;
		$toUser->lastname = $userDetails->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $userId;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		$emailTextHtml = text_to_html($emailText, false, false, true);

		email_to_user($toUser, $fromUser, $subject, $emailText, "", "", "", true);
	}	

	function courseUnenrolMail($courseId = 0,$userId,$departmentId = 0,$teamId = 0,$programId = 0,$remarks){
		GLOBAl $CFG,$USER,$DB;
		$userDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email,parent_id FROM mdl_user u WHERE u.id = ".$userId);
		$userDetails->name = getUserNameFormat($userDetails);
		if($courseId != 0){
			$courseDetails = $DB->get_record_sql("SELECT c.id,c.fullname FROM mdl_course as c WHERE c.id =  ".$courseId);
			if($departmentId != 0){
				$departmentDetails = $DB->get_record_sql("SELECT d.id,d.title FROM mdl_department as d WHERE d.id = ".$departmentId);
				$emailContent = array(
							'name'=>$userDetails->name,
							'coursename'=>$courseDetails->fullname,
							'departmentname'=>$departmentDetails->title,
							'datetime'=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
							'location'=>'India');
				$emailContent = (object)$emailContent;
				$emailText = get_string('course_unenrol_department','email',$emailContent);
				if($remarks != ''){
					$emailText .= get_string('decline_remarks').': '.$remarks."\n\r";
				}
				$emailText .= get_string('course_unenrol_department_2','email',$emailContent);
				$emailText .= get_string('email_footer','email');
				$emailText .= get_string('from_email','email',$emailContent);
				$subject	= get_string('course_unenrol_department_subject','email',$emailContent);
				add_to_log(1, 'Enrol course', 'remove',  'user/assigncourses.php?id='.$courseId.'&user='.$departmentId, "Unenrol course from department", 0, $USER->id);
			}elseif($teamId != 0){
				$teamDetails = $DB->get_record_sql("SELECT g.id,g.name FROM mdl_groups as g WHERE g.id = ".$teamId);
				$emailContent = array(
							'name'=>$userDetails->name,
							'coursename'=>$courseDetails->fullname,
							'teamname'=>$teamDetails->name,
							'datetime'=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
							'location'=>'India');
				$emailContent = (object)$emailContent;
				$emailText = get_string('course_unenrol_team','email',$emailContent);
				if($remarks != ''){
					$emailText .= get_string('decline_remarks').': '.$remarks."\n\r";
				}
				$emailText .= get_string('course_unenrol_team_2','email');
				$emailText .= get_string('email_footer','email');
				$emailText .= get_string('from_email','email',$emailContent);
				$subject	= get_string('course_unenrol_team_subject','email',$emailContent);
				add_to_log(1, 'Enrol course', 'remove', 'user/assigncourses.php?id='.$courseId.'&user='.$teamId, "Unenrol course from team", 0, $USER->id);
			}else{
				$emailContent = array(
							'name'=>$userDetails->name,
							'coursename'=>$courseDetails->fullname,
							'datetime'=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
							'location'=>'India');
				$emailContent = (object)$emailContent;
				$emailText = get_string('user_enrol_cancelled_elective','email',$emailContent);
				if($remarks != ''){
					$emailText .= get_string('decline_remarks').': '.$remarks."\n\r";
				}
				$emailText .= get_string('user_enrol_cancelled_elective_2','email');
				$emailText .= get_string('email_footer','email');
				$emailText .= get_string('from_email','email',$emailContent);
				$subject	= get_string('course_unenrol_subject','email',$emailContent);
				add_to_log(1, 'Enrol course', 'remove',  'user/assigncourses.php?id='.$courseId.'&user='.$userId, "Unenrol course from user ", 0, $USER->id);
			}
		}else{
			$courseDetails = $DB->get_record_sql("SELECT p.id,p.name FROM mdl_programs as p WHERE p.id = ".$programId);
			if($departmentId != 0){
				$departmentDetails = $DB->get_record_sql("SELECT d.id,d.title FROM mdl_department as d WHERE d.id = ".$departmentId);
				$emailContent = array(
							'name'=>$userDetails->name,
							'programname'=>$courseDetails->name,
							'departmentname'=>$departmentDetails->title,
							'datetime'=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
							'lmsLink'		=> $CFG->emailLmsLink,
							'lmsName'		=> $CFG->emailLmsName,
							'location'=>'India');
				$emailContent = (object)$emailContent;
				$emailText = get_string('program_unenrol_department','email',$emailContent);
				if($remarks != ''){
					$emailText .= "\n\r".get_string('decline_remarks').': '.$remarks;
				}
				$emailText .= get_string('email_footer','email');
				$emailText .= get_string('from_email','email',$emailContent);
				$subject	= get_string('program_unenrol_department_subject','email',$emailContent);
				add_to_log(1, 'Enrol program', 'remove','program/assignusers.php?program='.$programId.'&id='.$departmentId, "Unenroll Program from department ", 0, $USER->id);
			}elseif($teamId != 0){
				$teamDetails = $DB->get_record_sql("SELECT g.id,g.name FROM mdl_groups as g WHERE g.id = ".$teamId);
				$emailContent = array(
							'name'=>$userDetails->name,
							'programname'=>$courseDetails->name,
							'teamname'=>$teamDetails->name,
							'datetime'=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
							'lmsLink'		=> $CFG->emailLmsLink,
							'lmsName'		=> $CFG->emailLmsName,
							'location'=>'India');
				$emailContent = (object)$emailContent;
				$emailText = get_string('program_unenrol_team','email',$emailContent);
				if($remarks != ''){
					$emailText .= "\n\r".get_string('decline_remarks').': '.$remarks;
				}
				$emailText .= get_string('program_unenrol_team_1','email');
				$emailText .= get_string('email_footer','email');
				$emailText .= get_string('from_email','email',$emailContent);
				$subject	= get_string('program_unenrol_team_subject','email',$emailContent);
				add_to_log(1, 'Enrol program', 'remove', 'program/assignusers.php?program='.$programId.'&id='.$teamId, "Unenroll Program from team ", 0, $USER->id);
			}else{
				$emailContent = array(
							'name'=>$userDetails->name,
							'programname'=>$courseDetails->name,
							'datetime'=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
							'lmsLink'		=> $CFG->emailLmsLink,
							'lmsName'		=> $CFG->emailLmsName,
							'location'=>'India');
				$emailContent = (object)$emailContent;
				$emailText = get_string('user_unenrol_program','email',$emailContent);
				$remarkText = '';
				if($remarks != ''){
					$remarkText = get_string('decline_remarks').': '.$remarks;
				}
				$emailText .= $remarkText;
				$emailText .= get_string('user_unenrol_program_1','email');
				$emailText .= get_string('email_footer','email');
				$emailText .= get_string('from_email','email',$emailContent);
				$subject	= get_string('program_unenrol_subject','email',$emailContent);
				add_to_log(1, 'Enrol program', 'remove', 'program/assignusers.php?program='.$programId.'&id='.$userId, "Unenroll Program from user", 0, $USER->id);
				$userManagerDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email,parent_id FROM mdl_user u WHERE u.id = ".$userDetails->parent_id);
				$userManagerDetails->name = getUserNameFormat($userManagerDetails);
				$emailContentManager = array(
						'name'=>$userManagerDetails->name,
						'user_name'=>$userDetails->name,
						'courseContent'=> $courseDetails->name,
						'reason'	=> "\n\r"."\n\r".$remarkText
				);
				$emailContentManager = (object)$emailContentManager;
				$toUserManager->email = $userManagerDetails->email;
				$toUserManager->firstname = $userManagerDetails->firstname;
				$toUserManager->lastname = $userManagerDetails->lastname;
				$toUserManager->maildisplay = true;
				$toUserManager->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
				$toUserManager->id = $userManagerDetails->id;
				$toUserManager->firstnamephonetic = '';
				$toUserManager->lastnamephonetic = '';
				$toUserManager->middlename = '';
				$toUserManager->alternatename = '';
				$emailTextManager = get_string("manager_user_unenrol_program_1",'email',$emailContentManager);
				
				$emailTextManager .= get_string('from_email','email',$emailContentManager);
				$subjectManager	= get_string('manager_user_unenrol_program_subject','email',$emailContentManager);
				$userRole = $DB->get_record_sql("SELECT r.id,r.name FROM mdl_role AS r LEFT JOIN mdl_role_assignments AS ra ON r.id = ra.roleid WHERE ra.userid = ".$userId." AND ra.contextid = 1");
				$mailToManager = 1;
				if($userRole->name == $CFG->userTypeManager){
					$mailToManager = 0;
				}
				if($mailToManager == 1){
					email_to_user($toUserManager, $fromUser, $subjectManager, $emailTextManager, '');
				}
				
			}
		}
		$fromUser->email = $USER->email;
		$fromUser->firstname = $USER->firstname;
		$fromUser->lastname = $USER->lastname;
		$fromUser->maildisplay = true;
		$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$fromUser->id = $USER->id;
		$fromUser->firstnamephonetic = '';
		$fromUser->lastnamephonetic = '';
		$fromUser->middlename = '';
		$fromUser->alternatename = '';

		$toUser->email = $userDetails->email;
		$toUser->firstname = $userDetails->firstname;
		$toUser->lastname = $userDetails->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $userId;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		$emailTextHtml = text_to_html($emailText, false, false, true);
		email_to_user($toUser, $fromUser, $subject, $emailText, "", "", "", true);
	}	
	function requestAccepted($userId,$classId){
		GLOBAl $CFG,$USER,$DB;
		$userDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email FROM mdl_user u WHERE u.id = ".$userId);
		$classDetails = $DB->get_record_sql("SELECT s.id,s.course,s.name,s.startdate,s.enddate, s.location,createdby FROM mdl_scheduler as s WHERE s.id = ".$classId);
		$courseDetails = $DB->get_record_sql("SELECT c.id,c.fullname FROM mdl_course as c WHERE c.id =  ".$classDetails->course);
		$userDetails->name = getUserNameFormat($userDetails);

		$creatorDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email FROM mdl_user u WHERE u.id = ".$classDetails->createdby);
		$creatorDetails->name = getUserNameFormat($creatorDetails);
		$approvalDetails = new stdClass;
		$approvalDetails->id = $USER->id;
		$approvalDetails->firstname = $USER->firstname;
		$approvalDetails->lastname = $USER->lastname;
		$approvalDetails->username = $USER->username;
		$approvalDetails->email = $USER->email;
		$approvalDetails->name = getUserNameFormat($approvalDetails);
		$events = $DB->get_records_sql("SELECT ss.id,e.id as eid FROM mdl_scheduler_slots ss LEFT JOIN mdl_event as e ON ss.id = e.sessionid WHERE ss.isactive = 1 AND ss.schedulerid = ".$classId." AND e.userid = ".$userId);
		$emailFile = '';
		$emailFileName = '';
		if(!empty($events)){
			$update = 0;
			$eventArr = array();
			foreach($events as $event){
				$eventArr[] = $event->eid;
			}
			$emailFile = generateIcsFile($eventArr,0);
			$emailFileName = basename($emailFile);
		}
		$locationText = '';
		if(trim($classDetails->location) != ''){
			$locationText = "<li>".get_string('location','scheduler').': '.$classDetails->location."</li>";
		}
		$sessionsListing = sessionListForEmails($classId);
		$emailContent = array(
							'name'		=>$userDetails->name,
							'coursename'=>$courseDetails->fullname.": ".$classDetails->name,
							'datetime'	=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
							'starttime'	=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
							'classduration'	=> getClassDuration($classDetails->startdate, $classDetails->enddate),
							'location'	=>$locationText,
							'sessionsListing'=>$sessionsListing
			);
		$emailContent = (object)$emailContent;
		$emailText = get_string('class_user_acceptance_request','email',$emailContent);
		$emailText .= get_string('email_footer_classroom','email');
		$emailText .= get_string('from_email','email');
		$subject	= get_string('approved_subject_request_class','email',$emailContent);
		$fromUser->email = $USER->email;
		$fromUser->firstname = $USER->firstname;
		$fromUser->lastname = $USER->lastname;
		$fromUser->maildisplay = true;
		$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$fromUser->id = $USER->id;
		$fromUser->firstnamephonetic = '';
		$fromUser->lastnamephonetic = '';
		$fromUser->middlename = '';
		$fromUser->alternatename = '';

		$toUser->email = $userDetails->email;
		$toUser->firstname = $userDetails->firstname;
		$toUser->lastname = $userDetails->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $userId;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		$emailTextHtml = text_to_html($emailText, false, false, true);
		email_to_user($toUser, $fromUser, $subject, $emailText, "", $emailFile, $emailFileName, true);
		//Email to creator

		$emailContent = array(
							'name'		=>$creatorDetails->name,
							'coursename'=>$courseDetails->fullname.": ".$classDetails->name,
							'datetime'	=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
							'starttime'	=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
							'classduration'	=> getClassDuration($classDetails->startdate, $classDetails->enddate),
							'location'	=>$locationText,
							'assign_name'	=>$userDetails->name,
							'approved_by_name'	=>$approvalDetails->name);
		$emailContent = (object)$emailContent;
		$emailText = get_string('class_user_acceptance_request_tocreator','email',$emailContent);
		$emailText .= get_string('email_footer_classroom','email');
		$emailText .= get_string('from_email','email');
		$subject	= get_string('approved_subject_request_class','email',$emailContent);

		$toUser->email = $creatorDetails->email;
		$toUser->firstname = $creatorDetails->firstname;
		$toUser->lastname = $creatorDetails->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $userId;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		if($CFG->sendEmailToCreator ==1){
			email_to_user($toUser, $fromUser, $subject, $emailText, "", "", "", true);
		}
		add_to_log(1, 'requests', 'accept', 'my/requests.php?userid='.$userId, "Accept Request", 0, $USER->id);
	}	
		
	function requestDeclined($userId,$classId){
		GLOBAl $CFG,$USER,$DB;
		$userDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email,parent_id FROM mdl_user u WHERE u.id = ".$userId);
		$classDetails = $DB->get_record_sql("SELECT s.id,s.course,s.name,s.startdate,s.enddate,se.declined_remarks,createdby,s.location FROM mdl_scheduler AS s LEFT JOIN mdl_scheduler_enrollment as se ON se.scheduler_id = s.id WHERE s.id = ".$classId." AND se.userid = ".$userId);
		$courseDetails = $DB->get_record_sql("SELECT c.id,c.fullname,c.summary FROM mdl_course as c WHERE c.id =  ".$classDetails->course);
		$userDetails->name = $userDetails->firstname.' '.$userDetails->lastname;

		$creatorDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email FROM mdl_user u WHERE u.id = ".$classDetails->createdby);
		$creatorDetails->name = getUserNameFormat($creatorDetails);
		$approvalDetails = new stdClass;
		$approvalDetails->id = $USER->id;
		$approvalDetails->firstname = $USER->firstname;
		$approvalDetails->lastname = $USER->lastname;
		$approvalDetails->username = $USER->username;
		$approvalDetails->email = $USER->email;
		$approvalDetails->name = getUserNameFormat($approvalDetails);

		$toUserDetails = $DB->get_record_sql("SELECT id, firstname, lastname, email, createdby FROM {$CFG->prefix}user WHERE id = '".$userDetails->parent_id."'");
		$toUserDetails->name = getUserNameFormat($toUserDetails);

		$declineByDetails = getUserNameFormat($USER);
		$locationText = '';
		if(trim($classDetails->location) != ''){
			$locationText = "<li>".get_string('location','scheduler').': '.$classDetails->location."</li>";
		}
		$summary = '';
		if($CFG->showCourseDescriptionInMail == 1){
			$summary = getCourseSummary($courseDetails);
		}
		$emailContent = array(
							'name'			=> $userDetails->name,
							'coursename'	=> $courseDetails->fullname.": ".$classDetails->name,
							'manager'		=> $declineByDetails,
							'reason'		=> $classDetails->declined_remarks,
							'datetime'		=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
							'classduration'	=> getClassDuration($classDetails->startdate, $classDetails->enddate),
							'lmsLink'		=> $CFG->emailLmsLink,
							'lmsName'		=> $CFG->emailLmsName,
							'summary'		=> $summary,
							'location'		=> $locationText);
		$emailContentSubject = array(
							'coursename'	=> $courseDetails->fullname.": ".$classDetails->name." - ".getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat));
		$emailContent = (object)$emailContent;
		$emailContentSubject = (object)$emailContentSubject;
		if($approvalDetails->id == $userDetails->id){
			$emailText = get_string('user_enrol_denied_compliance_tolearner','email',$emailContent);
			$emailText .= get_string('from_email','email');
			$subject = get_string('decline_subject_request_class','email',$emailContentSubject);
		}else{
			$emailText = get_string('user_enrol_denied_compliance','email',$emailContent);
			$emailText .= get_string('email_footer_classroom','email');
			$emailText .= get_string('from_email','email');
			$subject = get_string('decline_subject_request_class','email',$emailContentSubject);
		}
		$fromUser->email = $USER->email;
		$fromUser->firstname = $USER->firstname;
		$fromUser->lastname = $USER->lastname;
		$fromUser->maildisplay = true;
		$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$fromUser->id = $USER->id;
		$fromUser->firstnamephonetic = '';
		$fromUser->lastnamephonetic = '';
		$fromUser->middlename = '';
		$fromUser->alternatename = '';

		$toUser->email = $userDetails->email;
		$toUser->firstname = $userDetails->firstname;
		$toUser->lastname = $userDetails->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $userId;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		$emailTextHtml = text_to_html($emailText, false, false, true);

		$events = $DB->get_records_sql("SELECT ss.id,e.id as eid FROM mdl_scheduler_slots ss LEFT JOIN mdl_event as e ON ss.id = e.sessionid WHERE ss.isactive = 1 AND ss.schedulerid = ".$classId." AND e.userid = ".$userId);
		$emailFile = '';
		$emailFileName = '';
		if(!empty($events)){
			$update = 0;
			$eventArr = array();
			foreach($events as $event){
				$eventArr[] = $event->eid;
			}
			$emailFile = generateIcsFile($eventArr,2);
			$emailFileName = basename($emailFile);
		}
		email_to_user($toUser, $fromUser, $subject, $emailText, "", $emailFile, $emailFileName, true);


		//Email to creator
		if($CFG->sendEmailToCreator == 1){
			$emailContent = array(
								'name'		=>$creatorDetails->name,
								'coursename'=>$courseDetails->fullname.": ".$classDetails->name,
								'datetime'	=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
								'reason'		=>$classDetails->declined_remarks,
								'starttime'	=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
								'location'	=>$locationText,
								'assign_name'	=>$userDetails->name,
								'approved_by_name'	=>$approvalDetails->name);
			$emailContent = (object)$emailContent;
			$emailText = get_string('user_enrol_denied_compliance_tocreator','email',$emailContent);
			$emailText .= get_string('email_footer','email');
			$emailText .= get_string('from_email','email');
			$subject	= get_string('decline_subject_request_class','email',$emailContentSubject);

			$toUser->email = $creatorDetails->email;
			$toUser->firstname = $creatorDetails->firstname;
			$toUser->lastname = $creatorDetails->lastname;
			$toUser->maildisplay = true;
			$toUser->mailformat = 0;
			$toUser->id = $userId;
			$toUser->firstnamephonetic = '';
			$toUser->lastnamephonetic = '';
			$toUser->middlename = '';
			$toUser->alternatename = '';

			email_to_user($toUser, $fromUser, $subject, $emailText, "", "", "", true);
		}
		if($approvalDetails->id != $userDetails->parent_id){
			$emailContent = array(
							'name'		=>$toUserDetails->name,
							'coursename'=>$courseDetails->fullname.": ".$classDetails->name,
							'datetime'	=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
							'reason'		=>$classDetails->declined_remarks,
							'starttime'	=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
							'assign_name'	=>$userDetails->name,
							'approved_by_name'	=>$approvalDetails->name,
							'classduration'	=> getClassDuration($classDetails->startdate, $classDetails->enddate),
							'lmsLink'		=> $CFG->emailLmsLink,
							'lmsName'		=> $CFG->emailLmsName,
							'location'		=> $locationText);
			if($userDetails->id != $USER->id){
				$emailText = get_string('user_enrol_denied_compliance_tomanager_byothers','email',$emailContent);
			}else{
				$emailText = get_string('user_enrol_denied_compliance_tomanager','email',$emailContent);
			}
			$emailText .= get_string('email_footer_classroom','email');
			$emailText .= get_string('from_email','email');
			$subject	= get_string('decline_subject_request_class','email',$emailContentSubject);
			$toUser->email = $toUserDetails->email;
			$toUser->firstname = $toUserDetails->firstname;
			$toUser->lastname = $toUserDetails->lastname;
			$toUser->maildisplay = true;
			$toUser->mailformat = 0;
			$toUser->id = $userId;
			$toUser->firstnamephonetic = '';
			$toUser->lastnamephonetic = '';
			$toUser->middlename = '';
			$toUser->alternatename = '';

			email_to_user($toUser, $fromUser, $subject, $emailText, "", "", "", true);
		}
		add_to_log(1, 'requests', 'decline', 'my/requests.php?userid='.$userId, "Decline Request", 0, $USER->id);
	}	
	function CourseRequestAccepted($userId,$courseId=0,$programId=0,$endDate = ''){
		GLOBAl $CFG,$USER,$DB;
		$userDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email FROM mdl_user u WHERE u.id = ".$userId);
		$userDetails->name = getUserNameFormat($userDetails);
		if($endDate != ''){
			$endDateArr = array('enddate'=>date($CFG->customDefaultDateFormat,$endDate),
							'lmsName'		=> $CFG->emailLmsName);
			$endDateContent = (object)$endDateArr;
			$endDateText = get_string('available_till','email',$endDateContent);
		}
		if($courseId != 0){
			$courseDetails = $DB->get_record_sql("SELECT c.id,c.fullname FROM mdl_course as c WHERE c.id =  ".$courseId);
			$emailContent = array(
								'name'			=>$userDetails->name,
								'coursename'	=>$courseDetails->fullname,
								'availabletill'	=>$endDateText,
								'reason'		=>$classDetails->declined_remarks,
								'datetime'		=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
								'lmsLink'		=> $CFG->emailLmsLink,
								'lmsName'		=> $CFG->emailLmsName,
								'location'		=>'India');

			$emailContent = (object)$emailContent;
			$emailText = get_string('user_enrol_approved_course','email',$emailContent);
			$emailText .= get_string('email_footer','email');
			$emailText .= get_string('from_email','email');
			$subject	= get_string('approved_subject_request','email',$emailContent);
		}else{
			$courseDetails = $DB->get_record_sql("SELECT p.id,p.name as fullname FROM mdl_programs as p WHERE p.id =  ".$programId);
			$userDetails->name = $userDetails->firstname.' '.$userDetails->lastname;
			$emailContent = array(
								'name'			=>$userDetails->name,
								'coursename'	=>$courseDetails->fullname,
								'availabletill'	=>$endDateText,
								'reason'		=>$classDetails->declined_remarks,
								'datetime'		=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
								'lmsLink'		=> $CFG->emailLmsLink,
								'lmsName'		=> $CFG->emailLmsName,
								'location'		=>'India');

			$emailContent = (object)$emailContent;
			$emailText = get_string('user_enrol_approved_program','email',$emailContent);
			$emailText .= get_string('email_footer','email');
			$emailText .= get_string('from_email','email');
			$subject	= get_string('program_approved_subject','email',$emailContent);
		}
		$fromUser->email = $USER->email;
		$fromUser->firstname = $USER->firstname;
		$fromUser->lastname = $USER->lastname;
		$fromUser->maildisplay = true;
		$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$fromUser->id = $USER->id;
		$fromUser->firstnamephonetic = '';
		$fromUser->lastnamephonetic = '';
		$fromUser->middlename = '';
		$fromUser->alternatename = '';

		$toUser->email = $userDetails->email;
		$toUser->firstname = $userDetails->firstname;
		$toUser->lastname = $userDetails->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $userId;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		$emailTextHtml = text_to_html($emailText, false, false, true);

		email_to_user($toUser, $fromUser, $subject, $emailText, "", "", "", true);
		add_to_log(1, 'requests', 'accept', 'my/requests.php?userid='.$userId, "Accept Request", 0, $USER->id);
	}	
		
	function CourseRequestDeclined($userId,$courseId=0,$programId=0,$remarksText){
		GLOBAl $CFG,$USER,$DB;
		$userDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email,parent_id FROM mdl_user u WHERE u.id = ".$userId);

		$toUserQuery = "SELECT id, firstname, lastname, email, createdby FROM {$CFG->prefix}user WHERE id = '".$userDetails->parent_id."'";
		$toUserDetails = $DB->get_record_sql($toUserQuery);
		$userDetails->name = getUserNameFormat($userDetails);
		$toUserDetails->name = getUserNameFormat($toUserDetails);

		if($courseId != 0){
			$courseDetails = $DB->get_record_sql("SELECT c.id,c.fullname FROM mdl_course as c WHERE c.id =  ".$courseId);
			$emailContent = array(
								'name'			=>$userDetails->name,
								'manager'		=>$toUserDetails->name,
								'coursename'	=>$courseDetails->fullname,
								'reason'		=>$remarksText,
								'datetime'		=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
								'location'		=>'India');

			$emailContent = (object)$emailContent;
			$emailText = get_string('user_enrol_denied_compliance_course','email',$emailContent);
			$emailText .= get_string('email_footer','email');
			$emailText .= get_string('from_email','email');
			$subject = get_string('decline_subject','email',$emailContent);
		}else{
			$courseDetails = $DB->get_record_sql("SELECT p.id,p.name as fullname FROM mdl_programs as p WHERE p.id =  ".$programId);
			$userDetails->name = $userDetails->firstname.' '.$userDetails->lastname;
			$emailContent = array(
								'name'			=>$userDetails->name,
								'manager'		=>$toUserDetails->name,
								'coursename'	=>$courseDetails->fullname,
								'reason'		=>$remarksText,
								'datetime'		=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
								'location'		=>'India');

			$emailContent = (object)$emailContent;
			$emailText = get_string('user_enrol_denied_compliance_course','email',$emailContent);
			$emailText .= get_string('email_footer','email');
			$emailText .= get_string('from_email','email');
			$subject = get_string('decline_subject','email',$emailContent);
		}
		$fromUser->email = $USER->email;
		$fromUser->firstname = $USER->firstname;
		$fromUser->lastname = $USER->lastname;
		$fromUser->maildisplay = true;
		$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$fromUser->id = $USER->id;
		$fromUser->firstnamephonetic = '';
		$fromUser->lastnamephonetic = '';
		$fromUser->middlename = '';
		$fromUser->alternatename = '';

		$toUser->email = $userDetails->email;
		$toUser->firstname = $userDetails->firstname;
		$toUser->lastname = $userDetails->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $userId;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		$emailTextHtml = text_to_html($emailText, false, false, true);

		email_to_user($toUser, $fromUser, $subject, $emailText, "", "", "", true);
		add_to_log(1, 'requests', 'decline', 'my/requests.php?userid='.$userId, "Decline Request", 0, $USER->id);
	}	
	function requestForCourse($userId,$courseId=0,$programId=0,$extionsion=""){
		GLOBAl $CFG,$USER,$DB;
		$userDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email,parent_id,r.name FROM mdl_user u LEFT JOIN mdl_role_assignments ra ON u.id = ra.userid LEFT JOIN mdl_role AS r ON ra.roleid = r.id WHERE u.id = ".$userId);
		$deptMail = 0;
		if($userDetails->name == $CFG->userTypeManager){
			$deptMail = 1;
			$toUserQuery = "SELECT u.id,u.firstname,u.lastname,u.username,email,parent_id,r.name FROM mdl_user u LEFT JOIN mdl_role_assignments ra ON u.id = ra.userid LEFT JOIN mdl_role AS r ON ra.roleid = r.id WHERE r.name = '".$CFG->userTypeAdmin."'";
		}else{
			$toUserQuery = "SELECT id, firstname, lastname, email, createdby FROM {$CFG->prefix}user WHERE id = '".$userDetails->parent_id."'";
		}
		$toUserDetails = $DB->get_record_sql($toUserQuery);
		$userDetails->name = getUserNameFormat($userDetails);
		$toUserDetails->name = getUserNameFormat($toUserDetails);
		if($courseId != 0){
			if($extionsion == 1){
				$Reqtype = get_string('course_extension');
				$subjectText = "Request for Extension of Course ";
			}else{
				$Reqtype = 'course';
				$subjectText = "Request for Course ";
			}
			$courseDetails = $DB->get_record_sql("SELECT c.id,c.fullname FROM mdl_course as c WHERE c.id =  ".$courseId);
			if($USER->archetype == $CFG->userTypeStudent){
				$emailContent = array(
								'name'			=>$toUserDetails->name,
								'user_name'		=>$userDetails->name,
								'coursename'	=>$courseDetails->fullname,
								'datetime'		=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
								'lmsLink'		=> $CFG->emailLmsLink,
								'lmsName'		=> $CFG->emailLmsName,
								'location'		=>'India');
				$emailContent = (object)$emailContent;
				if($extionsion == 1){
					$emailText = get_string('user_enrol_request_course_extension','email',$emailContent);
					$subject	= get_string('request_subject_extension','email',$emailContent);
				}else{
					$emailText = get_string('user_enrol_request_course','email',$emailContent);
					$subject	= get_string('request_subject','email',$emailContent);
				}
				$emailText .= get_string('email_footer','email');
				$emailText .= get_string('from_email','email');
				$subjectText .= "'".$courseDetails->fullname."' by ".$userDetails->name."(".$userDetails->username.")";
			}else{
				$departmentData = $DB->get_record_sql("SELECT d.id,d.title FROM mdl_department as d
										LEFT JOIN mdl_department_members as dm ON dm.departmentid = d.id
										WHERE dm.is_active = 1 AND dm.userid = ".$USER->id);
				$emailContent = array(
								'name'			=>$toUserDetails->name,
								'user_name'		=>$userDetails->name.'('.$departmentData->title.')',
								'coursename'	=>$courseDetails->fullname,
								'datetime'		=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
								'lmsLink'		=> $CFG->emailLmsLink,
								'lmsName'		=> $CFG->emailLmsName,
								'location'		=>'India');
				$emailContent = (object)$emailContent;
				$emailText = get_string('user_enrol_request_course_department','email',$emailContent);
				$emailText .= get_string('email_footer','email');
				$emailText .= get_string('from_email','email');
				$subject	= get_string('request_subject','email',$emailContent);
				$subjectText .= "'".$courseDetails->fullname."' by ".$userDetails->name.'('.$departmentData->title.')';
			}
			$status1 = getLogdata(0, $courseId, $USER->id, 2,$extionsion);
			$status2 = getLogdata(0, $courseId, $USER->id, 0,$extionsion);
			if(!empty($status1)){
				$successError = saveLogdata(0, $courseId, $USER->id, $status1->request_status, $subjectText, $toUserDetails->id, 1,$Reqtype,0,0);
			}elseif(!empty($status2)){
				$successError = saveLogdata(0, $courseId, $USER->id, $status2->request_status, $subjectText, $toUserDetails->id, 1,$Reqtype,0,0);
			}else{
				$successError = saveLogdata(0, $courseId, $USER->id, 0, $subjectText, $toUserDetails->id, 0,$Reqtype,0,0);				
			}
		}else{
			if($extionsion == 1){
				$Reqtype = get_string('program_extension');
				$subjectText = "Request for Extension of Program ";
			}else{
				$Reqtype = 'program';
				$subjectText = "Request for Program ";
			}
			$courseDetails = $DB->get_record_sql("SELECT p.id,p.name as fullname FROM mdl_programs as p WHERE p.id =  ".$programId);
			if($USER->archetype == $CFG->userTypeStudent){
				$emailContent = array(
								'name'			=>$toUserDetails->name,
								'user_name'		=>$userDetails->name,
								'programname'	=>$courseDetails->fullname,
								'datetime'		=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
								'lmsLink'		=> $CFG->emailLmsLink,
								'lmsName'		=> $CFG->emailLmsName,
								'location'		=>'India');
				$emailContent = (object)$emailContent;
				if($extionsion == 1){
					$emailText = get_string('user_enrol_request_program_extension','email',$emailContent);
					$subject	= get_string('request_subject_program_extension','email',$emailContent);
				}else{
					$emailText = get_string('user_enrol_request_program','email',$emailContent);
					$subject	= get_string('request_subject_program','email',$emailContent);
				}
				
				$emailText .= get_string('email_footer','email');
				$emailText .= get_string('from_email','email',$emailContent);
				$subjectText .= "'".$courseDetails->fullname."' by ".$userDetails->name."(".$userDetails->username.")";
			}else{
				$departmentData = $DB->get_record_sql("SELECT d.id,d.title FROM mdl_department as d
										LEFT JOIN mdl_department_members as dm ON dm.departmentid = d.id
										WHERE dm.is_active = 1 AND dm.userid = ".$USER->id);
				$emailContent = array(
								'name'			=>$toUserDetails->name,
								'user_name'		=>$userDetails->name.'('.$departmentData->title.')',
								'programname'	=>$courseDetails->fullname,
								'datetime'		=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
								'lmsLink'		=> $CFG->emailLmsLink,
								'lmsName'		=> $CFG->emailLmsName,
								'location'		=>'India');
				$emailContent = (object)$emailContent;
				$emailText = get_string('user_enrol_request_program_department','email',$emailContent);
				$emailText .= get_string('email_footer','email');
				$emailText .= get_string('from_email','email');
				$subject	= get_string('request_subject_program','email',$emailContent);

				$subjectText .= "'".$courseDetails->fullname."' by ".$userDetails->name.'('.$departmentData->title.')';
			}
			$status1 = getLogdata($programId, 0, $USER->id, 2,$extionsion);
			$status2 = getLogdata($programId, 0, $USER->id, 0,$extionsion);
			if(!empty($status1)){
				$successError = saveLogdata($programId, 0, $USER->id, $status1->request_status, $subjectText, $toUserDetails->id, 1,$Reqtype,0,0);
			}elseif(!empty($status2)){
				$successError = saveLogdata($programId, 0, $USER->id, $status2->request_status, $subjectText, $toUserDetails->id, 1,$Reqtype,0,0);
			}else{
				$successError = saveLogdata($programId, 0, $USER->id, 0, $subjectText, $toUserDetails->id, 0,$Reqtype,0,0);				
			}
		}
		$fromUser->email = $userDetails->email;
		$fromUser->firstname = $userDetails->firstname;
		$fromUser->lastname = $userDetails->lastname;
		$fromUser->maildisplay = true;
		$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$fromUser->id = $userDetails->id;
		$fromUser->firstnamephonetic = '';
		$fromUser->lastnamephonetic = '';
		$fromUser->middlename = '';
		$fromUser->alternatename = '';

		$toUser->email = $toUserDetails->email;
		$toUser->firstname = $toUserDetails->firstname;
		$toUser->lastname = $toUserDetails->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $userId;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		$emailTextHtml = text_to_html($emailText, false, false, true);
		email_to_user($toUser, $fromUser, $subject, $emailText, "", "", "", true);
		add_to_log(1, 'requests', 'accept', 'my/requests.php?userid='.$userId, "Accept Request", 0, $USER->id);
	}	
	function requestForClass($userId,$classId){
		GLOBAl $CFG,$USER,$DB;
		$userDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email,parent_id FROM mdl_user u WHERE u.id = ".$userId);
		$userDetails->name = getUserNameFormat($userDetails);
		$classDetails = $DB->get_record_sql("SELECT s.id,s.course,s.name,s.startdate,s.enddate,s.location FROM mdl_scheduler as s WHERE s.id = ".$classId);

		$courseDetails = $DB->get_record_sql("SELECT c.id,c.fullname FROM mdl_course as c WHERE c.id =  ".$classDetails->course);

		$toUserDetails = $DB->get_record_sql("SELECT id, firstname, lastname, email, createdby FROM {$CFG->prefix}user WHERE id = '".$userDetails->parent_id."'");
		$toUserDetails->name = getUserNameFormat($toUserDetails);
		$site = get_site();
		$locationText = '';
		if(trim($classDetails->location) != ''){
			$locationText = "<li>".get_string('location','scheduler').': '.$classDetails->location."</li>";
		}
		$sessionsListing = sessionListForEmails($classId);
		$emailContent = array(
								'name'			=> $toUserDetails->name,
								'user_name'		=> $userDetails->name,
								'coursename'	=> $courseDetails->fullname.": ".$classDetails->name,
								'datetime'		=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
								'starttime'		=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
								'location'		=> $locationText,
								'sitename'		=> html_entity_decode(format_string($site->fullname)),
								'classduration'	=> getClassDuration($classDetails->startdate, $classDetails->enddate),
								'lmsLink'		=> $CFG->emailLmsLink,
								'lmsName'		=> $CFG->emailLmsName,
								'sessionsListing'=> $sessionsListing,
								'link'			=> $CFG->wwwroot
			);
		$emailContent = (object)$emailContent;
		$emailText = get_string('user_enrol_request','email',$emailContent);
		$emailText .= get_string('email_footer','email');
		$emailText .= get_string('from_email','email');
		$subject	= get_string('request_subject_classroom','email',$emailContent);

		$fromUser->email = $USER->email;
		$fromUser->firstname = $USER->firstname;
		$fromUser->lastname = $USER->lastname;
		$fromUser->maildisplay = true;
		$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$fromUser->id = $USER->id;
		$fromUser->firstnamephonetic = '';
		$fromUser->lastnamephonetic = '';
		$fromUser->middlename = '';
		$fromUser->alternatename = '';

		$toUser->email = $toUserDetails->email;
		$toUser->firstname = $toUserDetails->firstname;
		$toUser->lastname = $toUserDetails->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 1; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $userId;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		$emailTextHtml = text_to_html($emailText, false, false, true);

		email_to_user($toUser, $fromUser, $subject, $emailText, $emailTextHtml, "", "", true);
		add_to_log(1, 'requests', 'accept', 'my/requests.php?userid='.$userId, "Accept Request", 0, $USER->id);
		/*Email to learner - acknowledgement of request receipt*/
		$emailContent = array(
								'name'			=> $userDetails->name,
								'coursename'	=> $courseDetails->fullname.": ".$classDetails->name,
								'starttime'		=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
								'location'		=> $locationText,
								'classduration'	=> getClassDuration($classDetails->startdate, $classDetails->enddate),
								'sessionsListing'=> $sessionsListing,
								'lmsLink'		=> $CFG->emailLmsLink,
								'lmsName'		=> $CFG->emailLmsName
			);
		$emailContent = (object)$emailContent;
		$emailText = get_string('user_enrol_request_learner','email',$emailContent);
		$emailText .= get_string('email_footer','email');
		$emailText .= get_string('from_email','email');
		$subject	= get_string('request_subject_classroom','email',$emailContent);
		$toUser->email = $userDetails->email;
		$toUser->firstname = $userDetails->firstname;
		$toUser->lastname = $userDetails->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $userDetails->id;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';

		email_to_user($toUser, $fromUser, $subject, $emailText, "", "", "", true);
	}	
	function sendEnrolMailToDepartment($departmentId,$courseId,$programId=0){
		GLOBAL $DB,$USER,$CFG;
		$departmentManagers = getUsersList(array($CFG->userTypeManager),$departmentId);
		if(!empty($departmentManagers)){
			foreach($departmentManagers as $manager){
				if($programId == 0){
					courseEnrolMail($courseId,$manager->id,$departmentId);
				}else{
					courseEnrolMail(0,$manager->id,$departmentId,0,$programId);
				}
			}
		}else{
			add_to_log(1, 'department', 'add course to department', 'department/assigncourses.php?department='.$departmentId, "add course to department", 0, $USER->id);
		}
	}

	function sendUnEnrolMailToDepartment($departmentId,$courseId,$programId=0,$remarks=''){
		GLOBAL $DB,$USER,$CFG;
		$departmentManagers = getUsersList(array($CFG->userTypeManager),$departmentId);
		if(!empty($departmentManagers)){
			foreach($departmentManagers as $manager){
				if($programId == 0){
					courseUnenrolMail($courseId,$manager->id,$departmentId);
				}else{
					courseUnenrolMail(0,$manager->id,$departmentId,0,$programId,$remarks);
				}
			}
		}
	}

	function sendEnrolMailToTeam($TeamId,$courseId,$programId=0,$endDate,$updateMail = 0){
		GLOBAL $DB,$USER,$CFG;
		$teamMembers = $DB->get_records_sql("SELECT gm.id,gm.userid FROM mdl_groups_members AS gm WHERE gm.groupid = $TeamId AND gm.is_active = 1");
		if(!empty($teamMembers)){
			foreach($teamMembers as $teamMember){
				if($programId == 0){
					courseEnrolMail($courseId,$teamMember->userid,0,$TeamId,0,$endDate,$updateMail);
				}else{
					courseEnrolMail(0,$teamMember->userid,0,$TeamId,$programId,$endDate,$updateMail);
				}
			}
		}else{
			add_to_log(1, 'team', 'add course to group', 'group/assigncourses.php?group='.$TeamId, "add course to group", 0, $USER->id);
		}
	}
	function sendCoursesEnrolMailToTeam($TeamId,$courseArr,$endDate = '',$updateMail = 0){
		GLOBAL $DB,$USER,$CFG;
		$teamMembers = $DB->get_records_sql("SELECT gm.id,gm.userid FROM mdl_groups_members AS gm WHERE gm.groupid = $TeamId AND gm.is_active = 1");
		if(!empty($teamMembers)){
			foreach($teamMembers as $teamMember){
				teamEnrolMailToCourses($courseArr,$teamMember->userid,$TeamId,$endDate,$updateMail);
			}
		}else{
			add_to_log(1, 'team', 'add course to group', 'group/assigncourses.php?group='.$TeamId, "add course to group", 0, $USER->id);
		}
	}
	function sendCoursesUnEnrolMailToTeam($TeamId,$courseArr,$remarks = ''){
		GLOBAL $DB,$USER,$CFG;
		$teamMembers = $DB->get_records_sql("SELECT gm.id,gm.userid FROM mdl_groups_members AS gm WHERE gm.groupid = $TeamId AND gm.is_active = 1");
		if(!empty($teamMembers)){
			foreach($teamMembers as $teamMember){
				teamCoursesUnenrolMail($courseArr,$teamMember->userid,$TeamId,$remarks);
			}
		}else{
			add_to_log(1, 'team', 'add course to group', 'group/assigncourses.php?group='.$TeamId, "add course to group", 0, $USER->id);
		}
	}
	function sendUnEnrolMailToTeam($TeamId,$courseId,$programId=0,$remarks = ''){
		GLOBAL $DB,$USER,$CFG;
		$teamMembers = $DB->get_records_sql("SELECT gm.id,gm.userid FROM mdl_groups_members AS gm WHERE gm.groupid = $TeamId AND gm.is_active = 1");
		if(!empty($teamMembers)){
			foreach($teamMembers as $teamMember){
				if($programId == 0){
					courseUnenrolMail($courseId,$teamMember->userid,0,$TeamId,0,$remarks);
				}else{
					courseUnenrolMail(0,$teamMember->userid,0,$TeamId,$programId,$remarks);
				}
			}
		}else{
			add_to_log(1, 'team', 'unenrol course from group', 'group/assigncourses.php?group='.$TeamId, "Remove course from group", 0, $USER->id);
		}
	}
	function userEnrolInGroupEmail($TeamId, $userId){
		GLOBAL $DB,$USER,$CFG;
		$userDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email FROM mdl_user u WHERE u.id = ".$userId);
		$userDetails->name = getUserNameFormat($userDetails);
		$courseDetails = $DB->get_record_sql("SELECT g.id,g.name FROM mdl_groups g WHERE g.id = ".$TeamId);
		$emailContent = array(
						'name'=>$userDetails->name,
						'teamname'=>$courseDetails->name,
						'datetime'=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
						'location'=>'India');
		$emailContent = (object)$emailContent;
		$emailText = get_string('user_enrol_team','email',$emailContent);
		$emailText .= get_string('email_footer','email');
		$emailText .= get_string('from_email','email');
		$subject	= get_string('user_enrol_team_subject','email',$emailContent);
		$fromUser->email = $USER->email;
		$fromUser->firstname = $USER->firstname;
		$fromUser->lastname = $USER->lastname;
		$fromUser->maildisplay = true;
		$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$fromUser->id = $USER->id;
		$fromUser->firstnamephonetic = '';
		$fromUser->lastnamephonetic = '';
		$fromUser->middlename = '';
		$fromUser->alternatename = '';

		$toUser->email = $userDetails->email;
		$toUser->firstname = $userDetails->firstname;
		$toUser->lastname = $userDetails->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $userId;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		email_to_user($toUser, $fromUser, $subject, $emailText, "", "", "", true);
		add_to_log(1, 'Enrol Team', 'add','group/assignusers.php?group='.$TeamId.'&user='.$userId, "enroll user in team", 0, $USER->id);
	}

	function userUnenrolInGroupEmail($TeamId, $userId){
		GLOBAL $DB,$USER,$CFG;
		$userDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email FROM mdl_user u WHERE u.id = ".$userId);
		$userDetails->name = getUserNameFormat($userDetails);
		$courseDetails = $DB->get_record_sql("SELECT g.id,g.name FROM mdl_groups g WHERE g.id = ".$TeamId);
		$emailContent = array(
						'name'=>$userDetails->name,
						'teamname'=>$courseDetails->name,
						'datetime'=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
						'location'=>'India');
		$emailContent = (object)$emailContent;
		$emailText = get_string('user_unenrol_team','email',$emailContent);
		$emailText .= get_string('email_footer','email');
		$emailText .= get_string('from_email','email');
		$subject	= get_string('user_unenrol_team_subject','email',$emailContent);

		$fromUser->email = $USER->email;
		$fromUser->firstname = $USER->firstname;
		$fromUser->lastname = $USER->lastname;
		$fromUser->maildisplay = true;
		$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$fromUser->id = $USER->id;
		$fromUser->firstnamephonetic = '';
		$fromUser->lastnamephonetic = '';
		$fromUser->middlename = '';
		$fromUser->alternatename = '';

		$toUser->email = $userDetails->email;
		$toUser->firstname = $userDetails->firstname;
		$toUser->lastname = $userDetails->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $userId;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		email_to_user($toUser, $fromUser, $subject, $emailText, "", "", "", true);
		add_to_log(1, 'Unenrol Team', 'remove','group/assignusers.php?group='.$TeamId.'&user='.$userId, "Unenroll user from team ", 0, $USER->id);
	}
	function userEnrolMailToCourses($courseArr,$userId,$endDate = '',$updateMail = 0,$cc_emails=''){
		GLOBAL $CFG,$DB,$USER,$SESSION;
		require_once($CFG->libdir.'/formslib.php');
		$userDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email FROM mdl_user u WHERE u.id = ".$userId);
		$userDetails->name = getUserNameFormat($userDetails);
                
                $COURSE_EMAIL = '';
              
                 if(isset($SESSION->custom_email_content)){
                             $COURSE_EMAIL = '<p>'.$SESSION->custom_email_content.'<p><br/>';//$course->courseemail;
                           //  unset($_SESSION['custom_email_content']);
                 }else{
                     $COURSE_EMAIL = '';
                 }
                 
                
		$courseInClause = implode(',',$courseArr);
		$courseDetails = $DB->get_records_sql("SELECT c.id,c.fullname,c.summary,c.coursetype_id FROM mdl_course as c WHERE c.id IN(  ".$courseInClause.") order by c.fullname ASC");
		
		$expiryText = '';
		if($endDate != ''){
			$expiryText = '<strong> - <span style="color:red;">'.get_string("expiring").' '.date($CFG->customDefaultDateFormat,$endDate).'</span></strong>';
		}
		$courseCounter = 1;
		$courseContent = '';
		foreach($courseDetails as $course){
			$courseContent .= $courseCounter.". ";
			$courseContent .= "<strong>".$course->fullname."</strong>";
			if($CFG->showCourseURL==1 && $course->coursetype_id==1){
				$coursedirectlink = getCourseLaunchURL($course->id);
				$courseContent .= " - <a href = '".$coursedirectlink."'>".get_string('courseurl')."</a>";
			}
			$courseContent .= $expiryText;
			if($CFG->showCourseDescriptionInMail == 1 && trim($course->summary) != ''){
				$courseContent .= "\n\r".getCourseSummary($course);
			}
			if(count($courseDetails) != $courseCounter){
				$courseContent .= "\n\r";
			}
			$courseCounter++;
                        /*
                        if(count($courseArr)==1){
                             $COURSE_EMAIL = $course->courseemail;
                        }*/
		}
                
		$emailContent = array(
								'name'			=> $userDetails->name,
								'coursename'	=> 'Course Name',
								'datetime'		=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
								'location'		=> 'India',
								'lmsLink'		=> $CFG->emailLmsLink,
								'lmsName'		=> 'GNOSIS',
								'courseContent'	=> $courseContent
						);
		$emailContent = (object)$emailContent;
		if($updateMail == 1){
			$emailText = get_string("user_extend_courses",'email',$emailContent);
		}else{
			$emailText = get_string("user_enrol_courses",'email',$emailContent);
		}
		$emailText .= get_string('email_footer','email');
		$emailText .= get_string('from_email','email');
                
                $emailText = str_replace('##COURSE_EMAIL##',$COURSE_EMAIL, $emailText);
                                
                
		if($userId == $USER->id){
			$subject	= get_string('user_enrol_courses_subject_selfenrol','email',$emailContent);
		}else{
			$subject	= get_string('user_enrol_courses_subject','email',$emailContent);
		}
                if($cc_emails){
                    
                }
		$toUser->email = $userDetails->email;
                
                if(trim($cc_emails)){
                    $toUser->email .= "|".trim($cc_emails);
                }
		$toUser->firstname = $userDetails->firstname;
		$toUser->lastname = $userDetails->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $userId;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		$events = $DB->get_records_sql("SELECT e.id FROM mdl_event e WHERE e.courseid IN(".$courseInClause.")");
		$emailFile = '';
		$emailFileName = '';
		if(!empty($events) && $updateMail == 0){
			$update = 0;
			$eventArr = array();
			foreach($events as $event){
				$eventArr[] = $event->id;
			}
			$emailFile = generateIcsFile($eventArr,$update);
			$emailFileName = basename($emailFile);
		}             
                
               
                if($updateMail == 1 && count($courseArr)==1){
                    $course_id = $courseArr[0];    
                    $completionStatus = $DB->get_record('online_course_usage_details',array('course_id'=>$course_id,'user_id'=>$userId,'course_status'=>'completed'));
                    if(!$completionStatus){
                        email_to_user($toUser, $fromUser, $subject, $emailText, "", $emailFile, $emailFileName, true);
                    }
                    
               }else{
                   email_to_user($toUser, $fromUser, $subject, $emailText, "", $emailFile, $emailFileName, true);
               }
		add_to_log(1, 'Enrol course', 'add', 'user/assigncourses.php?id='.$courseId.'&user='.$userId, "Enrol course to user: ", 0, $USER->id);
                
	}
	function userCoursesUnenrolMail($courseArr,$userId,$remarks = ''){
		GLOBAL $CFG,$DB,$USER;
		$userDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email,parent_id FROM mdl_user u WHERE u.id = ".$userId);
		$userDetails->name = getUserNameFormat($userDetails);
		$userManagerDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email,parent_id FROM mdl_user u WHERE u.id = ".$userDetails->parent_id);
		$userManagerDetails->name = getUserNameFormat($userManagerDetails);
		
		$courseInClause = implode(',',$courseArr);
		$courseDetails = $DB->get_records_sql("SELECT c.id,c.fullname FROM mdl_course as c WHERE c.id IN(  ".$courseInClause.") order by c.fullname ASC");
		$courseCounter = 1;
		$courseText = '';
		foreach($courseDetails as $course){
			$courseText .= $courseCounter.". ".$course->fullname;
			if(count($courseDetails) != $courseCounter){
				$courseText .= "\n\r";
			}
			$courseCounter++;
		}
		if($remarks != ''){
			$courseText .= "\n\r"."\n\r".get_string('decline_remarks').': '.$remarks;
		}
		$emailContent = array(
							'name'=>$userDetails->name,
							'coursename'=>'Course Name',
							'datetime'=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
							'location'=>'India',
							'lmsLink'		=> $CFG->emailLmsLink,
							'lmsName'		=> $CFG->emailLmsName,
							'courseContent'=> $courseText
						);
		$emailContent = (object)$emailContent;
		//$emailText = get_string("user_unenrol_courses",'email',$emailContent);

		$userRole = $DB->get_record_sql("SELECT r.id,r.name FROM mdl_role AS r LEFT JOIN mdl_role_assignments AS ra ON r.id = ra.roleid WHERE ra.userid = ".$userId." AND ra.contextid = 1");
		$mailToManager = 1;
		if($userRole->name == $CFG->userTypeManager){
			$mailToManager = 0;
			$emailText = get_string('user_unenrol_courses_manager','email',$emailContent);
		}else{
			$emailText = get_string('user_unenrol_courses_learner','email',$emailContent);
		}
		//$emailText .= get_string('email_footer','email');
		$emailText .= get_string('from_email','email',$emailContent);
		$subject	= get_string('user_unenrol_courses_subject','email',$emailContent);
		$toUser->email = $userDetails->email;
		$toUser->firstname = $userDetails->firstname;
		$toUser->lastname = $userDetails->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $userId;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		$events = $DB->get_records_sql("SELECT e.id FROM mdl_event e WHERE e.courseid IN(".$courseInClause.")");
		$emailFile = '';
		$emailFileName = '';
		if(!empty($events)){
			$update = 0;
			$eventArr = array();
			foreach($events as $event){
				$eventArr[] = $event->id;
			}
			$emailFile = generateIcsFile($eventArr,2);
			$emailFileName = basename($emailFile);
		}
		email_to_user($toUser, $fromUser, $subject, $emailText, "", $emailFile, $emailFileName, true);
		add_to_log(1, 'Unenrol course', 'add', 'user/assigncourses.php?id='.$courseId.'&user='.$userId, "Unenrol course from user", 0, $USER->id);

		$toUserManager->email = $userManagerDetails->email;
		$toUserManager->firstname = $userManagerDetails->firstname;
		$toUserManager->lastname = $userManagerDetails->lastname;
		$toUserManager->maildisplay = true;
		$toUserManager->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUserManager->id = $userManagerDetails->id;
		$toUserManager->firstnamephonetic = '';
		$toUserManager->lastnamephonetic = '';
		$toUserManager->middlename = '';
		$toUserManager->alternatename = '';

		$emailContent = array(
					'name'=>$userManagerDetails->name,
					'user_name'=>$userDetails->name,
					'coursename'=>'Course Name',
					'datetime'=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
					'location'=>'India',
					'courseContent'=> $courseText
			);
		$emailContent = (object)$emailContent;
		$emailText = get_string("manager_user_unenrol_courses_1",'email',$emailContent);
		/*$emailText .= $courseText;
		if($remarks != ''){
			$emailText .= "\n\r".get_string('decline_remarks').': '.$remarks;
		}
		$emailText .= get_string("manager_user_unenrol_courses_2",'email',$emailContent);
		$emailText .= get_string('email_footer','email');*/
		$emailText .= get_string('from_email','email',$emailContent);
		$subject	= get_string('manager_user_unenrol_courses_subject','email',$emailContent);
		if($mailToManager == 1){
			email_to_user($toUserManager, $fromUser, $subject, $emailText, '');
		}
	}
	function departmentEnrolMailToCourses($userId,$courseArr,$departmentId,$departmentName){
		GLOBAL $CFG,$DB,$USER;
		$userDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email FROM mdl_user u WHERE u.id = ".$userId);
		$userDetails->name = getUserNameFormat($userDetails);
		$courseInClause = implode(',',$courseArr);
		$courseDetails = $DB->get_records_sql("SELECT c.id,c.fullname FROM mdl_course as c WHERE c.id IN(  ".$courseInClause.") order by c.fullname ASC");
		$courseCounter = 1;
		$courseText = '';
		foreach($courseDetails as $course){
			$courseText .= $courseCounter.". ".$course->fullname;
			if(count($courseDetails) != $courseCounter){
				$courseText .= "\n\r";
			}
			$courseCounter++;
		}
		$emailContent = array(
					'name'=>$userDetails->name,
					'departmentname'=>$departmentName,
					'coursename'=>'Course Name',
					'datetime'=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
					'courseContent'		=> $courseText,
					'lmsLink'		=> $CFG->emailLmsLink,
					'lmsName'		=> $CFG->emailLmsName,
					'location'=>'India');
		$emailContent = (object)$emailContent;
		$emailText = get_string("multiple_courses_enrol_department",'email',$emailContent);
		$emailText .= get_string('email_footer','email');
		$emailText .= get_string('from_email','email',$emailContent);
		$subject	= get_string('multiple_courses_department_subject','email',$emailContent);
		$toUser->email = $userDetails->email;
		$toUser->firstname = $userDetails->firstname;
		$toUser->lastname = $userDetails->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $userId;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		email_to_user($toUser, $fromUser, $subject, $emailText, '');
		add_to_log(1, 'Enrol course in department', 'add', 'department/assigncourses.php?id='.$departmentId.'&course='.$courseInClause.'&user='.$userId, "Enrol course to department: ", 0, $USER->id);
	}
	function departmentCoursesUnenrolMail($courseArr,$userId,$departmentId,$departmentName,$remarks = ''){
		GLOBAL $CFG,$DB,$USER;
		$userDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email,parent_id FROM mdl_user u WHERE u.id = ".$userId);
		$userDetails->name = getUserNameFormat($userDetails);
		$userManagerDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email,parent_id FROM mdl_user u WHERE u.id = ".$userDetails->parent_id);
		$userManagerDetails->name = getUserNameFormat($userManagerDetails);

		$courseInClause = implode(',',$courseArr);
		$courseDetails = $DB->get_records_sql("SELECT c.id,c.fullname FROM mdl_course as c WHERE c.id IN(  ".$courseInClause.") order by c.fullname ASC");
		$courseCounter = 1;
		$courseText = '';
		foreach($courseDetails as $course){
			$courseText .= $courseCounter.". ".$course->fullname;
			if(count($courseDetails) != $courseCounter){
				$courseText .= "\n\r";
			}
			$courseCounter++;
		}
		if($remarks != ''){
			$courseText .= "\n\r".get_string('decline_remarks').': '.$remarks;
		}
		$emailContent = array(
					'name'=>$userDetails->name,
					'coursename'=>'Course Name',
					'departmentname'=>$departmentName,
					'datetime'=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
					'courseContent'		=> $courseText,
					'lmsLink'		=> $CFG->emailLmsLink,
					'lmsName'		=> $CFG->emailLmsName,
					'location'=>'India');
		$emailContent = (object)$emailContent;
		$emailText = get_string("multiple_course_unenrol_department",'email',$emailContent);
		$emailText .= get_string('email_footer','email');
		$emailText .= get_string('from_email','email',$emailContent);
		$subject	= get_string('multiple_courses_unenrol_department_subject','email',$emailContent);
		$toUser->email = $userDetails->email;
		$toUser->firstname = $userDetails->firstname;
		$toUser->lastname = $userDetails->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $userId;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		email_to_user($toUser, $fromUser, $subject, $emailText, '');
		add_to_log(1, 'Unenrol course', 'add', 'department/assigncourses.php?id='.$departmentId.'&course='.$courseInClause.'&user='.$userId, "Unenrol course from department", 0, $USER->id);
	}
	function teamEnrolMailToCourses($courseArr,$userId,$teamId,$endDate = '',$updateMail = 0){
		GLOBAL $CFG,$DB,$USER;
		$userDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email FROM mdl_user u WHERE u.id = ".$userId);
		$userDetails->name = getUserNameFormat($userDetails);
		$teamDetails = $DB->get_record_sql("SELECT g.id,g.name FROM mdl_groups g WHERE g.id = ".$teamId);
		$courseInClause = implode(',',$courseArr);
		$courseDetails = $DB->get_records_sql("SELECT c.id,c.fullname FROM mdl_course as c WHERE c.id IN(  ".$courseInClause.") order by c.fullname ASC");
		$courseCounter = 1;

		$expiryText = '';
		if($endDate != ''){
			$expiryText = ' - Expiring '.date($CFG->customDefaultDateFormat,$endDate);
		}
		$courseText = '';
		foreach($courseDetails as $course){
			$courseText .= $courseCounter.". ".$course->fullname.$expiryText;
			if(count($courseDetails) != $courseCounter){
				$courseText .= "\n\r";
			}
			$courseCounter++;
		}
		$emailContent = array(
					'name'=>$userDetails->name,
					'teamname'=>$teamDetails->name,
					'coursename'=>'Course Name',
					'datetime'=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
					'courseContent'		=> $courseText,
					'lmsLink'		=> $CFG->emailLmsLink,
					'lmsName'		=> $CFG->emailLmsName,
					'location'=>'India');
		$emailContent = (object)$emailContent;
		if($updateMail == 1){
			$emailText = get_string("multiple_courses_extend_team",'email',$emailContent);
			$subject	= get_string('multiple_courses_team_subject_extend','email',$emailContent);
		}else{
			$emailText = get_string("multiple_courses_enrol_team",'email',$emailContent);
			$subject	= get_string('multiple_courses_team_subject','email',$emailContent);
		}
		$emailText .= get_string('email_footer','email');
		$emailText .= get_string('from_email','email',$emailContent);
		$toUser->email = $userDetails->email;
		$toUser->firstname = $userDetails->firstname;
		$toUser->lastname = $userDetails->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $userId;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		email_to_user($toUser, $fromUser, $subject, $emailText, '');
		add_to_log(1, 'Enrol course in team', 'add', 'group/assigncourses.php?group='.$teamId.'&course='.$courseInClause.'&user='.$userId, "Enrol course to team: ", 0, $USER->id);
	}
	function teamCoursesUnenrolMail($courseArr,$userId,$teamId,$remarks = ''){
		GLOBAL $CFG,$DB,$USER;
		$userDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email FROM mdl_user u WHERE u.id = ".$userId);
		$userDetails->name = getUserNameFormat($userDetails);
		$courseDetails = $DB->get_record_sql("SELECT g.id,g.name FROM mdl_groups g WHERE g.id = ".$teamId);
		$emailContent = array(
					'name'=>$userDetails->name,
					'teamname'=>$courseDetails->name,
					'coursename'=>'Course Name',
					'datetime'=> getDateFormat(strtotime('now'), $CFG->customDefaultDateFormat),
					'location'=>'India');
		$emailContent = (object)$emailContent;
		$emailText = get_string("multiple_course_unenrol_team",'email',$emailContent);
		$courseInClause = implode(',',$courseArr);
		$courseDetails = $DB->get_records_sql("SELECT c.id,c.fullname FROM mdl_course as c WHERE c.id IN(  ".$courseInClause.") order by c.fullname ASC");
		$courseCounter = 1;
		foreach($courseDetails as $course){
			$emailText .= $courseCounter.". ".$course->fullname;
			if(count($courseDetails) != $courseCounter){
				$emailText .= "\n\r";
			}
			$courseCounter++;
		}
		if($remarks != ''){
			$emailText .= "\n\r".get_string('decline_remarks').': '.$remarks;
		}
		$emailText .= get_string('email_footer','email');
		$emailText .= get_string('from_email','email',$emailContent);
		$subject	= get_string('multiple_courses_unenrol_team_subject','email',$emailContent);
		$toUser->email = $userDetails->email;
		$toUser->firstname = $userDetails->firstname;
		$toUser->lastname = $userDetails->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $userId;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		email_to_user($toUser, $fromUser, $subject, $emailText, '');
		add_to_log(1, 'Unenrol course in team', 'remove', 'group/assigncourses.php?group='.$teamId.'&course='.$courseInClause.'&user='.$userId, "Unenrol course to team: ", 0, $USER->id);
	}
	
	// To get mdash html 
	function getSpace($type=''){
	  
	  if($type == 'csv'){
	    return "   ";
	  }else{
	   return "&#09;&#09;";
	  }
	}

	function classNoShowMail($userId,$classId){
		GLOBAl $CFG,$USER,$DB;
		$userDetails = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname,u.username,email,parent_id FROM mdl_user u WHERE u.id = ".$userId);
		$classDetails = $DB->get_record_sql("SELECT s.id,s.course,s.name,s.startdate,s.enddate,se.declined_remarks,createdby,s.location FROM mdl_scheduler AS s LEFT JOIN mdl_scheduler_enrollment as se ON se.scheduler_id = s.id WHERE s.id = ".$classId." AND se.userid = ".$userId);
		$courseDetails = $DB->get_record_sql("SELECT c.id,c.fullname,c.summary FROM mdl_course as c WHERE c.id =  ".$classDetails->course);
		$userDetails->name = $userDetails->firstname.' '.$userDetails->lastname;


		$toUserDetails = $DB->get_record_sql("SELECT id, firstname, lastname, email, createdby FROM {$CFG->prefix}user WHERE id = '".$userDetails->parent_id."'");
		$toUserDetails->name = getUserNameFormat($toUserDetails);
		$locationText = '';
		if(trim($classDetails->location) != ''){
			$locationText = "<li>".get_string('location','scheduler').': '.$classDetails->location."</li>";
		}
		$summary = '';
		if($CFG->showCourseDescriptionInMail == 1){
			$summary = getCourseSummary($courseDetails);
		}
		$sessionList = sessionListForEmailsNoShow($classDetails->id,$userId);
		$emailContent = array(
							'name'			=> $userDetails->name,
							'coursename'	=> $courseDetails->fullname.": ".$classDetails->name,
							'manager'		=> $toUserDetails->name,
							'reason'		=> $classDetails->declined_remarks,
							'classduration'	=> getClassDuration($classDetails->startdate, $classDetails->enddate),
							'datetime'		=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
							'lmsLink'		=> $CFG->emailLmsLink,
							'lmsName'		=> $CFG->emailLmsName,
							'summary'		=> $summary,
							'sessionList'	=> $sessionList,
							'location'		=> $locationText);

		$emailContent = (object)$emailContent;
		$emailText = get_string('noshow_tolearner','email',$emailContent);
		$emailText .= get_string('email_footer','email');
		$emailText .= get_string('from_email','email');
		$subject = get_string('no_show_subject','email',$emailContent);
		$fromUser->email = $USER->email;
		$fromUser->firstname = $USER->firstname;
		$fromUser->lastname = $USER->lastname;
		$fromUser->maildisplay = true;
		$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$fromUser->id = $USER->id;
		$fromUser->firstnamephonetic = '';
		$fromUser->lastnamephonetic = '';
		$fromUser->middlename = '';
		$fromUser->alternatename = '';

		$toUser->email = $userDetails->email;
		$toUser->firstname = $userDetails->firstname;
		$toUser->lastname = $userDetails->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
		$toUser->id = $userId;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';
		$emailTextHtml = text_to_html($emailText, false, false, true);

		email_to_user($toUser, $fromUser, $subject, $emailText, "", "", "", true);

		$emailContent = array(
						'name'			=> $toUserDetails->name,
						'coursename'	=> $courseDetails->fullname.": ".$classDetails->name,
						'datetime'		=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
						'reason'		=> $classDetails->declined_remarks,
						'starttime'		=> getDateFormat($classDetails->startdate, $CFG->customDefaultDateFormat),
						'classduration'	=> getClassDuration($classDetails->startdate, $classDetails->enddate),
						'lmsLink'		=> $CFG->emailLmsLink,
						'lmsName'		=> $CFG->emailLmsName,
						'location'		=> $locationText,
						'sessionList'	=> $sessionList,
						'assign_name'	=> $userDetails->name);
		$emailText = get_string('noshow_tomanager','email',$emailContent);
		$emailText .= get_string('email_footer','email');
		$emailText .= get_string('from_email','email');
		$subject	= get_string('no_show_subject','email',$emailContent);
		$toUser->email = $toUserDetails->email;
		$toUser->firstname = $toUserDetails->firstname;
		$toUser->lastname = $toUserDetails->lastname;
		$toUser->maildisplay = true;
		$toUser->mailformat = 0;
		$toUser->id = $userId;
		$toUser->firstnamephonetic = '';
		$toUser->lastnamephonetic = '';
		$toUser->middlename = '';
		$toUser->alternatename = '';

		email_to_user($toUser, $fromUser, $subject, $emailText, "", "", "", true);
		add_to_log(1, 'requests', 'decline', 'my/requests.php?userid='.$userId, "Decline Request", 0, $USER->id);
	}
	
	// function to get Certificate download link
	
function certificateLink($id,$action,$completed_on='',$userid=0){
		global $CFG;
		if($completed_on==''){
			$completed_on = time();
		}
		if($userid){
                    $certificateLink = '<div class="certificateDwn reportcert"><a href="'.$CFG->wwwroot.'/mod/certificate/certificate.php?id='.$id.'&userid='.$userid.'&action='.$action.'&completed_on='.$completed_on.'" class="download_certificate" title="'.get_string('downloadcertificate','certificate').'" target="_blank">'.get_string('downloadcertificate','certificate').'</a></div>';
                }else{
                    $certificateLink = '<div class="certificateDwn"><span>'.get_string('certificateOfCompletion').'</span><a href="'.$CFG->wwwroot.'/mod/certificate/certificate.php?id='.$id.'&action='.$action.'&completed_on='.$completed_on.'" class="download_certificate" title="'.get_string('downloadcertificate','certificate').'" target="_blank">'.get_string('downloadcertificate','certificate').'</a></div>';
                }
		
		

		return $certificateLink;
	}
function isCertificateEnabled($id,$action){
	global $DB,$CFG,$USER;
	if($action=='course'){
		$sql = "select c.is_certificate as is_certificate from {$CFG->prefix}course as c  where c.id=".$id;
		$checkCertificate = $DB->get_record_sql($sql);
	}
	else if($action=='class'){
		$sql = "select c.is_certificate as is_certificate from {$CFG->prefix}course as c LEFT JOIN {$CFG->prefix}scheduler as s ON c.id=s.course where s.id=".$id;
		$checkCertificate = $DB->get_record_sql($sql);
	}
	else if($action=='programme'){
		$sql = "select p.is_certificate as is_certificate from {$CFG->prefix}programs as p  where p.id=".$id;
		$checkCertificate = $DB->get_record_sql($sql);
	}
	return $checkCertificate->is_certificate;
}
function getProgramCompletionDate($programCourses){
	global $DB,$CFG,$USER;
	$completed_on = array();
	
		if($programCourses){
			foreach($programCourses as $programCourse){
				if($programCourse->coursetype_id==$CFG->courseTypeOnline){
				$assetdata = $DB->get_records_sql("SELECT CONCAT_WS('_','p', CONVERT(c.program_id, CHAR), CONVERT(c.courseid, CHAR), CONVERT(c.instanceid, CHAR), CONVERT(c.module, CHAR)) AS difId,c.*
			FROM (	(
			SELECT *
			FROM vw_user_full_program_resource) UNION (
			SELECT *
			FROM vw_user_full_program_scorm_status)) AS c
			WHERE c.userid = ".$USER->id." AND c.is_resource_reference_material = '0' AND IF(c.resource_type_id = 0, '1 = 1', c.allow_resource_student_access = 1) AND c.courseid = ".$programCourse->id);
				if($assetdata){
					foreach($assetdata as $asse){
						$completed_on[] = $asse->completed_on;
					}
				}
			}
			else{
	 			$sql = "select max(se.timemodified) as completed_on From mdl_scheduler as s LEFT JOIN mdl_scheduler_enrollment as se ON(s.id=se.scheduler_id) where se.userid='".$USER->id."' and s.course='".$programCourse->id."' and se.is_completed=1";
				$classdata = $DB->get_record_sql($sql);
				$completed_on[] = $classdata->completed_on;
			}
		}
		}
	
	return max($completed_on);
}

	function dateToCal($timestamp) {
		if($timestamp === NULL) {
            $timestamp = time();
        }
        return gmstrftime('%Y%m%dT%H%M%SZ', $timestamp);
	}
	/*
		@$eventArray = event id array
		@$cancelDelete = 0 -> add, 1-> update,2->delete
	*/
	function generateIcsFile($eventArray,$cancelDelete = 0){
		GLOBAL $CFG,$DB,$USER;
		$eventFileName = '';
		$CalStr = '';
		$separator = "\r\n";
		if(!empty($eventArray)){
			$eventIds = implode(',',$eventArray);
			$events = $DB->get_records_sql("SELECT * FROM mdl_event as e WHERE e.id IN (".$eventIds.") AND e.timestart > ".strtotime('now'));
			if(!empty($events)){
				$status = "";
				if($cancelDelete == 0){
					$method = "METHOD:REQUEST".$separator;
				}elseif($cancelDelete == 1){
					$method = "METHOD:REQUEST".$separator;
				}elseif($cancelDelete == 2){
					$method = "METHOD:CANCEL".$separator;
					$status = "STATUS:CANCELLED".$separator;
				}
				$hostaddress = str_replace('http://', '', $CFG->wwwroot);
				$hostaddress = str_replace('https://', '', $hostaddress);
				$fileNameArray = array();
				foreach($events as $event){
					$CalStr = '';
					$CalStr .= 'BEGIN:VCALENDAR'.$separator.'VERSION:2.0'.$separator.'PRODID:-//hacksw/handcal//NONSGML v1.0//EN'.$separator;
					$CalStr .= $method;
					$CalStr .= 'BEGIN:VEVENT'.$separator;
					$CalStr .= 'SEQUENCE:'.$event->sequence.$separator;
					$CalStr .= 'ORGANIZER:'.get_string('from_email','email').$separator;
					$CalStr .= 'DTEND:'.dateToCal($event->timestart + $event->timeduration).$separator;
					$CalStr .= $status;
					$CalStr .= 'UID:'.$event->id.get_string('from_email','email').$separator;
					$CalStr .= 'DTSTAMP:'.dateToCal(strtotime('now')).$separator;
					$CalStr .= 'SUMMARY:'.clean_param($event->name, PARAM_NOTAGS).$separator;
					if(empty($event->description) || trim($event->description) == ''){
						$event->description = $event->name;
					}
					$CalStr .= 'DESCRIPTION:'.clean_param($event->description, PARAM_NOTAGS).$separator;
					if($event->repeatid != 0){
						$repeat = $DB->get_record_sql("SELECT count(*) as cnt FROM mdl_event e WHERE e.repeatid = ".$event->id);
						$CalStr .= 'RRULE:FREQ=WEEKLY;INTERVAL='.$repeat->cnt.$separator;
					}
					$CalStr .= 'DTSTART:'.dateToCal($event->timestart).$separator;
					$CalStr .= 'END:VEVENT'.$separator;
					$CalStr .= 'END:VCALENDAR'.$separator;
					$filename = 'cal_'.rand().time().'.ics';
					$icsDir = $CFG->dirroot."/event/callog/";
					if(!is_dir($icsDir)){
						mkdir($icsDir,0777);
					}
					$eventFileName = '';
					$eventFileName = $icsDir.$filename;
					$fileNameArray[] = $eventFileName;
					$fp = fopen($eventFileName, 'w');
					file_put_contents($eventFileName,$CalStr);
					fclose($fp);
				}
				$eventFileName = '';
				if(!empty($fileNameArray)){
					$eventFileName = implode(',',$fileNameArray);
				}
			}
		}
		return $eventFileName;
	}
	/*
		@$eventId = event Id
		@$mode = update / add
	*/
	function triggerEventMail($eventId,$mode = 'add'){
		GLOBAL $CFG,$DB,$USER;
		$users = array();
		$eventArray = array($eventId);
		$events = $DB->get_record_sql("SELECT * FROM mdl_event as e WHERE e.id = ".$eventId);
		$description = $events->description;
		if(!empty($events) && (empty($events->description) || trim($events->description) == '')){
			$description = $events->name;
		}
		$courseName = '';
		switch ($events->eventtype) {
			case 'user':
				$users = $DB->get_records_sql("SELECT u.id,u.firstname,u.lastname,u.username,email FROM mdl_user u WHERE u.suspended = 0 AND u.deleted = 0 ".$select." AND u.id = ".$events->userid);
				break;
			case 'global':
				$users = get_users_listing('', '', 1, 0, '', '', '','',array('is_active'=>1));
				break;
			case 'course':
				if($USER->archetype == $CFG->userTypeStudent){
					$users = array();
					$users[0] = $USER;
				}else{
					$courseNameArr = $DB->get_record_sql("SELECT c.fullname FROM mdl_course as c WHERE c.id = ".$events->courseid);
					$courseName = $courseNameArr->fullname;
					$excludedUsers = count($CFG->excludedUsers) > 0 ? implode(",",$CFG->excludedUsers) : 0;
					$select = " AND u.id NOT IN (".$excludedUsers.")";
					if($USER->archetype == $CFG->userTypeAdmin){
						$usersArr = $DB->get_records_sql("SELECT u.id,u.firstname,u.lastname,u.username,email FROM mdl_user_course_mapping AS ucm LEFT JOIN mdl_user AS u ON ucm.userid = u.id WHERE u.suspended = 0 AND u.deleted = 0 AND  ucm.courseid = $events->courseid AND ucm.`status` = 1 ".$select);
					}else{
						$departmentCourse = $DB->get_records_sql("SELECT courseid FROM mdl_department_course dc WHERE dc.courseid = $events->courseid AND dc.is_active = 1 AND dc.departmentid = $USER->department ");
						if(!empty($departmentCourse)){
							$usersArr = $DB->get_records_sql("SELECT u.id,u.firstname,u.lastname,u.username,email,dm.departmentid FROM mdl_user_course_mapping AS ucm LEFT JOIN mdl_user AS u ON ucm.userid = u.id LEFT JOIN mdl_department_members as dm ON dm.userid = u.id WHERE u.suspended = 0 AND u.deleted = 0 AND  ucm.courseid = $events->courseid AND ucm.`status` = 1 AND dm.departmentid = $USER->department ".$select);
						}else{
							$users = array();
							$users[0] = $USER;
						}
					}
					if(!empty($usersArr)){
						foreach($usersArr as $userArray){
							$users[] = $userArray;
						}
					}
				}
				break;
			default:
				$users = $DB->get_records_sql("SELECT u.id,u.firstname,u.lastname,u.username,email FROM mdl_user u WHERE u.suspended = 0 AND u.deleted = 0 AND u.id = ".$events->userid);
				break;
		}
		$fromUser = array();
		$append = '';
		$update = 0;
		if($mode == 'update'){
			$append = '_update';
			$update = 1;
		}elseif($mode == 'delete'){
			$append = '_delete';
			$update = 2;
		}
		$emailFile = generateIcsFile($eventArray,$update);
		$emailFileName = basename($emailFile);
		if(!empty($users)){
			foreach($users as $userDetails){
				$userDetails->name = getUserNameFormat($userDetails);
				$emailContent = array(
									'name'			=>$userDetails->name,
									'eventname'		=>$events->name,
									'eventtime'		=> getDateFormat($events->timestart, $CFG->customDefaultDateTimeFormat),
									'lmsLink'		=> $CFG->emailLmsLink,
									'lmsName'		=> $CFG->emailLmsName,
									'summary'		=> $description,
									'coursename'	=> $courseName
								);
				switch ($events->eventtype) {
					case 'user':
						$emailText	= get_string('user_event_body'.$append,'email',$emailContent);
						$subject	= get_string('user_event_subject','email',$emailContent);
						break;
					case 'global':
						$emailText	= get_string('global_event_body'.$append,'email',$emailContent);
						$subject	= get_string('global_event_subject','email',$emailContent);
						break;
					case 'course':
						$emailText	= get_string('course_event_body'.$append,'email',$emailContent);
						$subject	= get_string('course_event_subject','email',$emailContent);
						break;
					default:
						$emailText	= get_string('user_event_body'.$append,'email',$emailContent);
						$subject	= get_string('user_event_subject','email',$emailContent);
						break;
				}
				if($emailFileName !== ''){
					$emailText .= get_string('add_to_outlook','email');
				}
				$emailText .= get_string('email_footer','email');
				$emailText .= get_string('from_email','email');
				$toUser->email = $userDetails->email;
				$toUser->firstname = $userDetails->firstname;
				$toUser->lastname = $userDetails->lastname;
				$toUser->maildisplay = true;
				$toUser->mailformat = 0;
				$toUser->id = $userDetails->id;
				$toUser->firstnamephonetic = '';
				$toUser->lastnamephonetic = '';
				$toUser->middlename = '';
				$toUser->alternatename = '';
				email_to_user($toUser, $fromUser, $subject, $emailText, "", $emailFile, $emailFileName, true);
			}
		}
	}
	

	function getScormLaunchURL($courseId,$moduleId){
		global $DB,$CFG;
		$scormdata = $DB->get_record('scorm',array('course'=>$courseId));
		$cm = get_coursemodule_from_id ( 'scorm', $moduleId );
		$scodata = $DB->get_record_sql("SELECT * FROM mdl_scorm_scoes WHERE scorm = $scormdata->id ORDER BY id DESC");
		$launchurl = $CFG->wwwroot.'/mod/scorm/player.php?a='.$cm->instance.'&cm='.$moduleId.'&scoid='.$scodata->launch.'&newattempt=on&display=popup&mode=normal';
		return $launchurl;
	}
	function getScormLaunchURLWithLink($courseId,$moduleId, $preview = 0){
		global $DB,$CFG;
		$scormdata = $DB->get_record('scorm',array('course'=>$courseId));
		$cm = get_coursemodule_from_id ( 'scorm', $moduleId );
		$scodata = $DB->get_record_sql("SELECT * FROM mdl_scorm_scoes WHERE scorm = $scormdata->id ORDER BY id DESC");
		if($preview){
			$launchurl = $CFG->wwwroot.'/mod/scorm/player.php?a='.$cm->instance.'&cm='.$moduleId.'&scoid='.$scodata->launch.'&newattempt=off&display=popup&mode=preview';
			$launchURLLink = "<a href=\"javascript:void(0);\" onclick=\"window.open('".$launchurl."', '', 'width=".$scormdata->width.",height=".$scormdata->height.",toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \" class = \"view\" title=\"".get_string('previewlink','course')."\">".get_string('previewlink','course')."</a>";
		}else{
			$launchurl = $CFG->wwwroot.'/mod/scorm/player.php?a='.$cm->instance.'&cm='.$moduleId.'&scoid='.$scodata->launch.'&newattempt=on&display=popup&mode=normal';
			$launchURLLink = "<a href=\"javascript:void(0);\" onclick=\"window.open('".$launchurl."', '', 'width=".$scormdata->width.",height=".$scormdata->height.",toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \" class = \"view\" title=\"".get_string('previewlink','course')."\">".get_string('previewlink','course')."</a>";		
		}
		return $launchURLLink;
	}
	function getCourseLaunchURL($courseId){
		global  $CFG;
		$launchurl = $CFG->wwwroot.'/course/courselaunch.php?id='.$courseId.'&access=direct';	
		return $launchurl;
	}
	function enrolUserIntoCourseWithoutEmail($courseId,$userid=''){
		global $CFG,$USER,$DB;
		if($userid==''){
			$userid = $USER->id;
		}
		$enrolId  = CheckCourseEnrolment($courseId);
		$enrolledId = enrolUserToCourse($enrolId,$userid,$courseId);
		$courseEnddate = $DB->get_record_sql("SELECT MAX(gc.end_date) as enddate FROM mdl_groups_course AS gc LEFT JOIN mdl_groups_members as gm ON gm.groupid = gc.groupid LEFT JOIN mdl_groups as g ON g.id = gm.groupid AND gm.is_active = 1 WHERE gc.courseid = ".$courseId." AND gc.is_active = 1  AND gm.userid = ".$userid);
		$endDate = "";
		if(!empty($courseEnddate)){
			$endDate = $courseEnddate->enddate;
		}
		$userMapId = assignProgramCourseToUsers(0,$courseId,$userid,'user',$userid,$endDate);
		return;
	}
	
	function courseDirectAccess($id,$access){
	
		global $CFG,$USER,$DB;
		$userId = $USER->id;
	
		if($USER->archetype == $CFG->userTypeAdmin){
			redirect($CFG->wwwroot.'/coursenotassigned.php');
		}
		if( $USER->archetype == $CFG->userTypeManager) {
			$managerCourseArr = getManagerCourse();
			
			if($access=='direct' && $CFG->showCourseURL==1){
				if(in_array($id, $managerCourseArr)){
					enrolUserIntoCourseWithoutEmail($id);
				}
				
				$USER->original_archetype = $USER->archetype;
				$USER->archetype = $CFG->userTypeStudent;
				$USER->pageHeader = generatePageHeader();
				redirect($CFG->wwwroot.'/course/courselaunch.php?id='.$id.'&access=direct');
			}
			
			//pr($managerCourseArr);die;courselaunch.php
			if(!in_array($id, $managerCourseArr)){
				redirect($CFG->wwwroot."/course/index.php");
			}
		}	else  if($USER->archetype == $CFG->userTypeStudent){
			$usercourse = getAssignedCourseIdForUser($userId);
			
			if(!in_array($id, $usercourse)){
				$teamArr = array('-1');
				$sql = "select groupid from mdl_groups_members where is_active = 1 and userid = '".$USER->id."' ";
	
				$teamArrRecords = $DB->get_records_sql($sql);
				if(count($teamArrRecords) > 0 ){
					$teamArr = array_keys($teamArrRecords);
				}
				$teamCourses = getCourseListByDnT(array('-1'), $teamArr, array('-1'), array('-1'), 2, false, 2);
				
				if(in_array($id, $teamCourses)){
					enrolUserIntoCourseWithoutEmail($id);
				}
				else{
					$complienceOrGlobal = getComplianceAndGlobalCourses();
					$complienceCourses = $complienceOrGlobal['compliance'];
					if(in_array($id, $complienceCourses)){
						enrolUserIntoCourseWithoutEmail($id);
					}
					else{
						redirect($CFG->wwwroot.'/coursenotassigned.php');
					}
				}
				 
			}
		}
	
	}

// function to  set 12 hour time format in form where date_time_picker is used.
//Used in Event and Add Session Page.
function setTimeAsPerTimeFormat($starttime){
	
	
	$starttime_hour = date('h',$starttime);
	
	//die;
	if($starttime_hour==12){
		$starttime = $starttime-3600;
		$starttime = $starttime+(12*3600);
	}
	$timeFormat = date('A',$starttime);
	if($timeFormat=='PM'){
		$starttime = $starttime-(12*3600);
	}
	if($starttime_hour!=12){
		$starttime = $starttime-3600;
	}
	return $starttime;
}
// function to get time format from form where date_time_picker is used.
//Used in Event and Add Session Page.
function getTimeAsPerTimeFormat($starttime,$timeFormat){
	$starttime = $starttime+3600;
	$starttime_hour = date('h',$starttime);
	if($starttime_hour==12){
		$starttime = $starttime-(12*3600);
	}

	if($timeFormat==1){
		$starttime = $starttime+(12*3600);
	}

	return $starttime;
}

function updateDepartmentCoursesAsPerOwner($data){


	global $CFG,$USER,$DB;
	$department_of_owner = $DB->get_record('department_members',array('userid'=>$data->createdby));
	if($department_of_owner){
		$department_course = $DB->get_record('department_course',array('courseid'=>$data->id));
		if($department_course){
			if($department_course->departmentid!=$department_of_owner->departmentid){
				$update_rec = new stdClass();
				$update_rec->id = $department_course->id;
				$update_rec->departmentid = $department_of_owner->departmentid;
				$DB->update_record('department_course',$update_rec);
			}
		}
			
	}
	else{
		//$DB->delete_records('department_course',array('courseid'=>$data->id));
	}
}

function get_learning_list($sort,$type,$page,$perpage,$selectionType = 'all',$paramArr = array(), $userId = ''){
	Global $DB,$CFG,$USER;
	if($perpage != 0) {
		$offset = ($page - 1) * $perpage;
		$limitQuery .= " LIMIT " . $offset . ", " . $perpage;
	}
	$userId =  $userId?$userId:$USER->id;
	$where = ' WHERE user_id = '. $userId;
	$whereC = ' WHERE userid = '. $userId;

	if(isset($paramArr['showClasses']) && $paramArr['showClasses'] == 'in_progress'){
		$type = 4;
	}elseif(isset($paramArr['showClasses']) && $paramArr['showClasses'] == 'not_started'){
		$type = 3;
	}
	$WhrDateChkCourse = '';
	$WhrDateChkClassroom = '';
	$WhrDateChkProgram = '';
	$whereSearchProgram = '';
	$whereSearchCourse = '';
	if($paramArr['searchText'] != ''){
		$searchText = $paramArr['searchText'];
		$whereSearchProgram .= " AND (LOWER(program_name) LIKE '%".$searchText."%' || LOWER(program_description) LIKE '%".$searchText."%' || LOWER(learningobj) LIKE '%".$searchText."%' || LOWER(performanceout) LIKE '%".$searchText."%')";
		$whereSearchCourse .= " AND (LOWER(program_name) LIKE '%".$searchText."%' || LOWER(program_description) LIKE '%".$searchText."%' || LOWER(course_name) LIKE '%".$searchText."%' || LOWER(course_description) LIKE '%".$searchText."%' || LOWER(learningobj) LIKE '%".$searchText."%' || LOWER(performanceout) LIKE '%".$searchText."%')";
	}
	if(isset($paramArr['all']) && $paramArr['all'] == 1){
		$WhrDateChkCourse = '';
		$WhrDateChkClassroom = '';
		$WhrDateChkProgram = '';
	}elseif(isset($paramArr['expired']) && $paramArr['expired'] == 1){
		$WhrDateChkCourse = '';
		$WhrDateChkCourse .= " AND (((c.enddate+86399) <=".strtotime('now').") || (end_date IS NOT NULL AND end_date != 0 AND (end_date+86399) <=".strtotime('now').")) ";
		$WhrDateChkClassroom = " AND ((class_end IS NOT NULL AND class_end != 0 AND (class_end+86399) <=".strtotime('now').")) ";
		$WhrDateChkProgram = " AND ((end_date IS NOT NULL AND end_date != 0 AND (end_date+86399) <=".strtotime('now').")) ";
	}elseif(isset($paramArr['active']) && $paramArr['active'] == 1){
		$WhrDateChkCourse .= " AND (((c.enddate+86399) >=".strtotime('now').") AND (end_date IS NULL || end_date = 0 || (end_date+86399) >=".strtotime('now').")) ";
		$WhrDateChkClassroom = " AND ((class_end IS NULL || class_end = 0 || 
		(
			((class_end+86399) >=".strtotime('now')." && (class_start+86399) <= ".strtotime('now').")
			|| (class_start+86399) >= ".strtotime('now')."
		)
		)) ";
		$WhrDateChkProgram = " AND ((end_date IS NULL || end_date = 0 || (end_date+86399) >=".strtotime('now').")) ";
	}
	$where1 = '';
	$where2 = '';
	/*
		 $type = 1 All (not completed)
		 $type = 2 completed 
		 $type = 3 not started
		 $type = 4 in progress 
		 $type = 5 All (including completed)
	 */
	if($type == 1){
		$where1 = " AND	(stat_scorm = 'not started' || 
						stat_scorm = 'incomplete' || 
						stat_resource ='not started' || 
						stat_resource = 'incomplete')";
		$where2 = " AND class_submit != 1";
		$WhrProgram1 = " WHERE (	stat_scorm = 'not started' || 
						stat_scorm = 'incomplete' || 
						stat_resource LIKE '%not started%' || 
						stat_resource LIKE  '%in progress%' ||
						(stat_scorm = '' && 
						stat_resource =  '')
						)";
		$WhrProgram2 = "AND   class_submit != 1";
	}elseif($type == 2){
		$where1 = " AND	(stat_scorm != 'not started' && stat_scorm != 'incomplete') AND 
						(stat_resource != 'incomplete' && stat_resource != 'not started')";
		$where2 = " AND   class_submit = 1";

		$WhrProgram1 = " WHERE (stat_scorm != 'incomplete' && stat_scorm != 'not started' && stat_resource NOT LIKE '%not started%' AND stat_resource NOT LIKE '%in progress%') && (trim(stat_scorm) != '' OR trim(stat_resource) != '')";
		$WhrProgram2 = " AND   class_submit = 1";
	}elseif($type == 3){
		$where1 = " AND	(stat_scorm = '' && stat_resource = '') || 
		(stat_scorm = 'not started' && stat_resource = '') || 
		(stat_scorm = '' && stat_resource = 'not started') || 
		(stat_scorm = 'not started' && stat_resource = 'not started')";
		$where2 = " AND class_start > ".strtotime('now')." AND class_submit != 1";
		
		$WhrProgram1 = " WHERE (stat_scorm = '' && stat_resource = '') || 
		(stat_scorm = 'not started' && stat_resource = '') || 
		(stat_scorm = '' && (stat_resource LIKE '%not started%' && stat_resource NOT LIKE '%completed%' && stat_resource NOT LIKE '%in progress%')) || 
		(stat_scorm = 'not started' && (stat_resource LIKE '%not started%' && stat_resource NOT LIKE '%completed%' && stat_resource NOT LIKE '%in progress%'))";
		$WhrProgram2 = " AND class_start > ".strtotime('now')." AND class_submit != 1";

	}elseif($type == 4){
		$where1 = " AND	((stat_scorm = '' && stat_resource = 'incomplete') || 
						(stat_scorm = 'incomplete' && stat_resource = '') || 
						(stat_scorm = 'incomplete' && stat_resource = 'incomplete') || 
						(stat_scorm = 'incomplete' && stat_resource = 'not started') || 
						(stat_scorm = 'not started' && stat_resource = 'incomplete')|| 
						(stat_scorm = 'completed' && stat_resource = 'incomplete') || 
						(stat_scorm = 'incomplete' && stat_resource = 'completed') || 
						(stat_scorm = 'not started' && stat_resource = 'completed') || 
						(stat_scorm = 'completed' && stat_resource = 'not started'))";
		$where2 = " AND class_submit != 1 AND (class_start) <= ".strtotime('now')." ";
		$WhrProgram1 = " WHERE	((stat_scorm = '' && stat_resource LIKE '%in progress%') || 
						(stat_scorm = 'incomplete' && stat_resource = '') || 
						(stat_scorm = 'incomplete' && stat_resource LIKE '%in progress%') || 
						(stat_scorm = 'incomplete' && stat_resource LIKE '%not started%') || 
						(stat_scorm = 'not started' && stat_resource LIKE '%in progress%')|| 
						(stat_scorm = 'completed' && stat_resource LIKE '%in progress%') || 
						(stat_scorm = 'incomplete' && stat_resource LIKE '%completed%') || 
						(stat_scorm = 'not started' && stat_resource LIKE '%completed%') || 
						(stat_scorm = 'completed' && stat_resource LIKE '%not started%')
						)";
		$WhrProgram2 = "AND class_submit != 1";
	}
$programQuery = "SELECT * FROM( SELECT CONCAT_WS('_',`p`.`program_id`,0) AS `dif_id`,1 as rowcount,`p`.`program_id` AS `program_id`,0 AS `id`,`p`.`program_name` AS `program_name`,`p`.`program_description` AS `program_description`,`p`.`user_id` AS `user_id`,`p`.`course_name` AS `course_name`,'' AS `course_description`,`p`.`pobj` AS `learningobj`,`p`.`pout` AS `performanceout`, GROUP_CONCAT(IF((coursetype_id = 1 || coursetype_id IS NULL), stat_scorm,'') SEPARATOR ' ') AS `stat_scorm`,GROUP_CONCAT(IF(coursetype_id = 2, IF(class_start> ".strtotime('now').", 'not started', IF (class_submit = 1, 'completed','in progress')), '') SEPARATOR ' ') AS `stat_resource`,`p`.`classroom_stat` AS`classrooom_stat`,`coursetype_id`,`p`.`primary_instructor` AS `primary_instructor`,`p`.`classroom_instruction` AS `classroom_instruction`,`p`.`classroom_course_duration` AS `classroom_course_duration`,`p`.`course_createdby` AS `course_createdby`,`p`.`classroom_stat` AS `final_classrooom_stat`,`p`.`end_date` AS `end_date`, CAST(`p`.`credithours` AS CHAR CHARSET utf8) AS `credithours`, class_submit, class_start, class_end,sch_id, class_complete FROM (
(
SELECT `com`.`program_id` AS `program_id`,`com`.`id` AS `id`,`com`.`program_name` AS `program_name`,`com`.`program_description` AS `program_description`,`com`.`pobj` AS `pobj`,`com`.`pout` AS `pout`,`com`.`user_id` AS `user_id`,`com`.`course_name` AS `course_name`, @stat_scorm :=
GROUP_CONCAT(CONCAT_WS(' ',`com`.`scorm_stat`,`com`.`resourse_stat`) SEPARATOR '') AS `scorm_stat`, GROUP_CONCAT(`com`.`resourse_stat` SEPARATOR ' ') AS `resourse_stat`, IF(
	(GROUP_CONCAT(CONCAT_WS(' ',`com`.`scorm_stat`,`com`.`resourse_stat`) SEPARATOR '') REGEXP 'not started'),
	IF(
	(GROUP_CONCAT(CONCAT_WS(' ',`com`.`scorm_stat`,`com`.`resourse_stat`) SEPARATOR '') REGEXP 'incomplete|failed|passed|completed'),
	'incomplete','not started'),
	IF(
		(GROUP_CONCAT(CONCAT_WS(' ',`com`.`scorm_stat`,`com`.`resourse_stat`) SEPARATOR '') REGEXP 'incomplete'),
		IF(GROUP_CONCAT(CONCAT_WS(' ',`com`.`scorm_stat`,`com`.`resourse_stat`) SEPARATOR '') REGEXP 'not started|failed|passed|completed',
		'incomplete',
		'incomplete'),
		IF(
			(GROUP_CONCAT(CONCAT_WS(' ',`com`.`scorm_stat`,`com`.`resourse_stat`) SEPARATOR '') REGEXP 'passed|completed'),
			IF(GROUP_CONCAT(CONCAT_WS(' ',`com`.`scorm_stat`,`com`.`resourse_stat`) SEPARATOR '') REGEXP 'not started|failed|incomplete',
			'incomplete',
			'completed'),''
		)
	)
) AS stat_scorm, @stat_resource := IF((GROUP_CONCAT(`com`.`resourse_stat` SEPARATOR '') REGEXP '^(not started)+$'), IF(((GROUP_CONCAT(`com`.`resourse_stat` SEPARATOR '') REGEXP '^(passed|completed)+$') OR (GROUP_CONCAT(`com`.`resourse_stat` SEPARATOR '') REGEXP '^(incomplete|failed)+$')),'incomplete','not started'), IF((GROUP_CONCAT(`com`.`resourse_stat` SEPARATOR '') REGEXP 'incomplete'),'incomplete', IF((GROUP_CONCAT(`com`.`resourse_stat` SEPARATOR '') REGEXP 'incomplete|failed'),'incomplete', IF((GROUP_CONCAT(`com`.`resourse_stat` SEPARATOR '') REGEXP 'passed|completed'),'completed','')))) AS stat_resource,`com`.`coursetype_id` AS `coursetype_id`,`com`.`primary_instructor` AS `primary_instructor`,`com`.`classroom_instruction` AS `classroom_instruction`,`com`.`classroom_course_duration` AS `classroom_course_duration`,`com`.`course_createdby` AS `course_createdby`,`com`.`end_date` AS `end_date`, GROUP_CONCAT(`com`.`credithours` SEPARATOR '_') AS `credithours`,'' AS `classroom_stat`
,0 as class_submit,0 as  class_start, 0 as class_end,0 as sch_id, 0 as class_complete
FROM `vw_user_program_intermediate_stat` `com`
$where 
GROUP BY `com`.`program_id`,`com`.`user_id`
) 
UNION (
SELECT `com`.`program_id` AS `program_id`,`com`.`id` AS `id`,`com`.`program_name` AS `program_name`,`com`.`program_description` AS `program_description`,
`com`.`pobj` AS `pobj`,`com`.`pout` AS `pout`,`com`.`user_id` AS `user_id`,`com`.`course_name` AS `course_name`,'' AS `scorm_stat`,'' AS `resourse_stat`,'' AS stat_scorm,'' AS stat_resource,`com`.`coursetype_id` AS `coursetype_id`,`com`.`primary_instructor` AS `primary_instructor`,`com`.`classroom_instruction` AS `classroom_instruction`,`com`.`classroom_course_duration` AS `classroom_course_duration`,`com`.`course_createdby` AS `course_createdby`,`com`.`end_date` AS `end_date`, 
(`com`.`credithours`) AS `credithours`, 
(`com`.`classroom_stat`) AS `classroom_stat`, class_submit, class_start, class_end,0 as sch_id, class_complete
FROM `vw_user_program_classroom_stat` `com`
$where
)) as p GROUP BY p.program_id ) as a
$WhrProgram1 $WhrDateChkProgram $whereSearchProgram
";
$courseQuery = "SELECT * FROM (SELECT CONCAT_WS('_','0', CAST(`cs`.`id` AS CHAR CHARSET utf8)) AS `dif_id`,1 as rowcount,0 AS `program_id`,`cs`.`id` AS `id`,'' AS `program_name`,'' AS `program_description`, `cs`.`userid` AS `user_id`,`cs`.`course_name` AS `course_name`,`cs`.`summary` AS `course_description`,`cs`.`learningobj` AS `learningobj`,`cs`.`performanceout` AS `performanceout`, IF((`cs`.`scorm_stat` REGEXP 'not started'), IF((`cs`.`scorm_stat` REGEXP 'incomplete'),'incomplete', IF((`cs`.`scorm_stat` REGEXP 'passed|completed'),'incomplete','not started')), IF((`cs`.`scorm_stat` REGEXP 'incomplete'),'incomplete', IF((`cs`.`scorm_stat` REGEXP 'passed|completed'),'completed',''))) AS `stat_scorm`,`cs`.`resourse_stat` AS `stat_resource`,'' AS `classrooom_stat`,`cs`.`coursetype_id` AS `coursetype_id`,`cs`.`primary_instructor` AS `primary_instructor`,`cs`.`classroom_instruction` AS `classroom_instruction`,`cs`.`classroom_course_duration` AS `classroom_course_duration`,`cs`.`course_createdby` AS `course_createdby`,'' AS `final_classrooom_stat`,`cs`.`end_date` AS `end_date`, CAST(`cs`.`credithours` AS CHAR CHARSET utf8) AS `credithours`,0 as class_submit,0 as  class_start, 0 as class_end,0 as sch_id, 0 as class_complete FROM (SELECT `c`.`id` AS `id`,`ucm`.`userid` AS `userid`,`c`.`fullname` AS `course_name`,`c`.`summary` AS `summary`,`c`.`learningobj` AS `learningobj`,`c`.`performanceout` AS `performanceout`,group_concat(if((`a`.`combined_stat` regexp 'passed|completed'),'completed',if((`a`.`combined_stat` regexp 'not started'),if((`a`.`combined_stat` regexp 'incomplete'),'incomplete',if((`a`.`combined_stat` regexp 'passed|completed'),'incomplete','not started')),if((`a`.`combined_stat` regexp 'incomplete'),'incomplete',''))) separator ' ') AS `scorm_stat`,if((`b`.`combined_stat` regexp 'completed'),if((`b`.`combined_stat` regexp 'not started'),'incomplete','completed'),if(isnull(`b`.`combined_stat`),'','not started')) AS `resourse_stat`,`c`.`coursetype_id` AS `coursetype_id`,`c`.`primary_instructor` AS `primary_instructor`,`c`.`classroom_instruction` AS `classroom_instruction`,`c`.`classroom_course_duration` AS `classroom_course_duration`,`c`.`createdby` AS `course_createdby`,`ucm`.`end_date` AS `end_date`,`c`.`credithours` AS `credithours` FROM `mdl_user_course_mapping` `ucm` INNER JOIN `mdl_course` `c` ON `c`.`id` = `ucm`.`courseid` AND `c`.`is_active` = 1 AND `ucm`.`userid`=$USER->id LEFT JOIN `vw_user_scorm_stat` `a` ON `a`.`id` = `ucm`.`courseid` AND `a`.`userid` = `ucm`.`userid` LEFT JOIN `vw_user_resource_stat` `b` ON `b`.`id` = `ucm`.`courseid` AND `b`.`userid` = `ucm`.`userid` WHERE `ucm`.`status` = 1 GROUP BY `c`.`id`,`ucm`.`userid`) `cs` 
LEFT JOIN mdl_course as c ON cs.id = c.id
$whereC $WhrDateChkCourse)  as a ".$where.$where1.$whereSearchCourse;

$classroomSql = "SELECT `vw_classroom_data`.`dif_id` AS `dif_id`,count(*) as rowcount,`vw_classroom_data`.`program_id` AS `program_id`,`vw_classroom_data`.`id` AS `id`,`vw_classroom_data`.`program_name` AS `program_name`,`vw_classroom_data`.`program_description` AS `program_description`,`vw_classroom_data`.`user_id` AS `user_id`,`vw_classroom_data`.`course_name` AS `course_name`,`vw_classroom_data`.`course_description` AS `course_description`,`vw_classroom_data`.`learningobj` AS `learningobj`,`vw_classroom_data`.`performanceout` AS `performanceout`,`vw_classroom_data`.`stat_scorm` AS `stat_scorm`,`vw_classroom_data`.`stat_resource` AS `stat_resource`,`vw_classroom_data`.`classrooom_stat` AS `classrooom_stat`,`vw_classroom_data`.`coursetype_id` AS `coursetype_id`,`vw_classroom_data`.`primary_instructor` AS `primary_instructor`,`vw_classroom_data`.`classroom_instruction` AS `classroom_instruction`,`vw_classroom_data`.`classroom_course_duration` AS `classroom_course_duration`,`vw_classroom_data`.`course_createdby` AS `course_createdby`, GROUP_CONCAT(`vw_classroom_data`.`classrooom_stat` SEPARATOR ' ') AS `final_classrooom_stat`, MAX(`vw_classroom_data`.`end_date`) AS `end_date`, GROUP_CONCAT(CAST(`vw_classroom_data`.`credithours` AS CHAR CHARSET utf8) SEPARATOR '_') AS `credithours`, class_submit, class_start, class_end,0 as sch_id, class_complete FROM `vw_classroom_data` $where ".$where2.$WhrDateChkClassroom.$whereSearchCourse." GROUP BY `vw_classroom_data`.`dif_id`,`vw_classroom_data`.`user_id`";

$completeSql =  '';
if((!isset($paramArr['classroom']) && !isset($paramArr['course']) && !isset($paramArr['program']))
|| ( $paramArr['classroom'] == 0 &&  $paramArr['course'] == 0 &&  $paramArr['program'] == 0)){

	$completeSql =  '('.$programQuery.') UNION ('.$courseQuery.') UNION ('.$classroomSql.')';
}else{
	if(isset($paramArr['program']) && $paramArr['program'] == 1){
		$completeSql .= $programQuery.' UNION ';
	}
	if(isset($paramArr['course']) && $paramArr['course'] == 1){
		$completeSql .= $courseQuery.' UNION ';
	}
	if(isset($paramArr['classroom']) && $paramArr['classroom'] == 1){
		$completeSql .= $classroomSql.' UNION ';
	}
	$completeSql = substr($completeSql, 0, -6);
}
	if($sort == 'count' || $sort == 'pagecount'){
		$fields = 'count(*) as listcount';
		if($sort == 'count'){
			$courseProgramList = $DB->get_record_sql("SELECT ".$fields." FROM (".$completeSql.") as v ");
			return $courseProgramList->listcount;
		}else{
			$courseProgramList = $DB->get_records_sql("SELECT v.* FROM (".$completeSql.") as v ");
			$listCount = 0;
			if(!empty($courseProgramList)){
				foreach($courseProgramList as $courseProgram){
					$listCount += $courseProgram->rowcount;
				}
			}
			return $listCount;
		}
	}else{
		$fields = 'v.*';
		//echo "SELECT $fields FROM ( ".$completeSql.") as v ".$whereSearch;die;
		$courseProgramList = $DB->get_records_sql("SELECT $fields FROM ( ".$completeSql.") as v ".$limitQuery);
		return $courseProgramList;
	}
}
function getClassSessionList($classId){
	GLOBAL $CFG,$USER,$DB;

	$slotSql = "SELECT * FROM mdl_scheduler_slots AS ss WHERE ss.schedulerid = $classId AND ss.isactive = 1";
	$slots = $DB->get_records_sql($slotSql);
	return $slots;
}
/*
	This function returns site details - site name, site url, site link name
*/
function getSiteDetails(){
	GLOBAL $CFG;
	return (object)(array('lmsLink'=>$CFG->emailLmsLink,'lmsName'=>$CFG->emailLmsLinkName));
}
/*
 * Reset particular password
* @param int $userId - User ID
* return true/false - on success / failure
* */
function resetUserPassword($userId,$mailToUser){
	GLOBAL $CFG,$USER,$DB;
	$usernew = new stdClass();
	$usernew->id = $userId;
	$usernew->password = hash_internal_user_password($CFG->systemAutoPassword);
	if($DB->update_record('user',$usernew)){
		if($mailToUser == 1){
			reactivateEmailToUser($userId);
		}
		return true;
	}else{
		return false;
	}
}
/*
 * Email to reactivated user
 * @param int $userId - User ID
 * return nothing
 * */
function reactivateEmailToUser($userId){
	GLOBAL $CFG,$USER,$DB;
	$userDetails = $DB->get_record('user',array('id'=>$userId));
        
        $checkRole = $DB->get_record("role_assignments", array('userid' => $userDetails->id, 'contextid'=>1));
        if($userDetails->department==$CFG->tranzactdepartment_id && $checkRole->roleid==5){
             $lmslink = $CFG->wwwroot;
        }else{
            $lmslink = $CFG->wwwroot.'/home/index.php?logintype=external';
        }
        
        //CFG->emailLmsLink,
	$emailContent = array(
			'name'			=> getUserNameFormat($userDetails),
			'user_name'		=> $userDetails->username,
			'password'		=> $CFG->systemAutoPassword,
			'lmsLink'		=> $lmslink,
			'lmsName'		=> $CFG->emailLmsName,
			'LmsSubName'	=> $CFG->emailLmsSubjectName
	);
	$emailContent = (object)$emailContent;
	$emailText =  get_string('user_reactivation_body','email',$emailContent);
	$emailText .= get_string('email_footer','email');
	$emailText .= get_string('from_email','email',$emailContent);
	$subject	= get_string('user_reactivation_subject','email',$emailContent);
	
	$toUser->email = $userDetails->email;
	$toUser->firstname = $userDetails->firstname;
	$toUser->lastname = $userDetails->lastname;
	$toUser->maildisplay = true;
	$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
	$toUser->id = $userId;
	$toUser->firstnamephonetic = '';
	$toUser->lastnamephonetic = '';
	$toUser->middlename = '';
	$toUser->alternatename = '';
	$emailTextHtml = text_to_html($emailText, false, false, true);
	$fromUser = array();
	email_to_user($toUser, $fromUser, $subject, $emailText, "", "", "", true);
}
function getCourseSummary($course){
	GLOBAL $DB,$CFG,$USER;
	/*$coursecontext = context_course::instance($course->id);
	$editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES, 'maxbytes'=>$CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true);
	$editoroptions['context'] = $coursecontext;
	
	$editoroptions['subdirs'] = file_area_contains_subdirs($coursecontext, 'course', 'summary', 0);
	
	$course = file_prepare_standard_editor($course, 'summary', $editoroptions, $coursecontext, 'course', 'summary', 0);
	*/
	return $course->summary;
}

function getWeblinkLaunchURL($urlStr){
	$parsed = parse_url($urlStr);
	if (empty($parsed['scheme'])) {
		$urlStr = 'http://' . ltrim($urlStr, '/');
	}
	return $urlStr;
}

/*
This fucntion loads deparment specific theme file from styles folder.
*/
function loadThemeCss(){
	GLOBAL $CFG,$USER,$PAGE;
	if($CFG->themeChange == 0){
		return false;
	}
	if(!empty($USER) && $USER->archetype != $CFG->userTypeAdmin ){
		$csspath = $CFG->dirroot."/theme/gourmet/style/theme_".$USER->department.".css";
		if(!file_exists($csspath)){
			$theme = theme_config::load('gourmet');
			$file = $CFG->dirroot.'/theme/gourmet/style/dept'.$USER->department.'.css';
			if(file_exists($file)){
				$css = file_get_contents($file)."\n";
				$css = $theme->post_process($css);
				$css = core_minify::css($css);
				$fp = fopen($csspath, 'w');
				file_put_contents($csspath,$css);
				fclose($fp);
			}
		}
		if(file_exists($csspath)){
			$PAGE->requires->css("/theme/gourmet/style/theme_".$USER->department.".css",true);
		}
	}
}
/*
Add http to url
*/
function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
}

/*
* function to optimize performance of dashboard and learning page for learner
*/
function get_learning_list_optimized2($sort, $type, $page, $perpage, $selectionType = 'all', $paramArr = array(), $userId = '') {
    Global $DB, $CFG, $USER;
	

    $userId = $userId ? $userId : $USER->id;
    $where = ' WHERE user_id = ' . $userId;
    $whereC = ' WHERE userid = ' . $userId;

    if (isset($paramArr['showClasses']) && $paramArr['showClasses'] == 'in_progress') {
        $type = 4;
    } elseif (isset($paramArr['showClasses']) && $paramArr['showClasses'] == 'not_started') {
        $type = 3;
    }
    $WhrDateChkCourse = '';
    $WhrDateChkClassroom = '';
    $WhrDateChkProgram = '';
    $whereSearchProgram = '';
    $whereSearchCourse = '';
    if ($paramArr['searchText'] != '') {
        $searchText = $paramArr['searchText'];
        $whereSearchProgram .= " AND (LOWER(program_name) LIKE '%" . $searchText . "%' || LOWER(program_description) LIKE '%" . $searchText . "%' || LOWER(learningobj) LIKE '%" . $searchText . "%' || LOWER(performanceout) LIKE '%" . $searchText . "%')";
        $whereSearchCourse .= " AND (LOWER(program_name) LIKE '%" . $searchText . "%' || LOWER(program_description) LIKE '%" . $searchText . "%' || LOWER(course_name) LIKE '%" . $searchText . "%' || LOWER(course_description) LIKE '%" . $searchText . "%' || LOWER(learningobj) LIKE '%" . $searchText . "%' || LOWER(performanceout) LIKE '%" . $searchText . "%')";
    }
    
        $now = "STR_TO_DATE('".date('m-d-Y')."','%m-%d-%Y')";
       
        $cenddate = "STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(c.enddate+86399),'%m-%d-%Y'),'%m-%d-%Y')";
        $csenddate = "STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(enddate+86399),'%m-%d-%Y'),'%m-%d-%Y')";
        $penddate = "STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(end_date+86399),'%m-%d-%Y'),'%m-%d-%Y')";
        $classstartdate = "STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(class_start+86399),'%m-%d-%Y'),'%m-%d-%Y')";
        $classenddate = "STR_TO_DATE(DATE_FORMAT(FROM_UNIXTIME(class_end+86399),'%m-%d-%Y'),'%m-%d-%Y')";
        $programDsiplay = 1;
        if(isset($paramArr['course_cat_id']) && $paramArr['course_cat_id']){
            $online_course_cat_id = " AND category=".$paramArr['course_cat_id'];
            $classroom_course_cat_id = " AND category=".$paramArr['course_cat_id'];
            $programDsiplay = 0;
        }
    if (isset($paramArr['all']) && $paramArr['all'] == 1) {
        $WhrDateChkCourse = '';
        $WhrDateChkClassroom = '';
        $WhrDateChkProgram = '';
    } elseif (isset($paramArr['expired']) && $paramArr['expired'] == 1) {             
               
        $WhrDateChkCourse = '';         
        $WhrDateChkCourse .= " AND ((($cenddate) <=".$now.") || (end_date IS NOT NULL AND end_date != 0 AND ($penddate) <=".$now.")) ";
        $WhrDateChkClassroom = " AND ((class_end IS NOT NULL AND class_end != 0 AND ($classenddate) <=".$now.")) ";      
        $WhrDateChkProgram = " AND ((end_date IS NOT NULL AND end_date != 0 AND ($penddate) <=".$now.")) ";
        
    } elseif (isset($paramArr['active']) && $paramArr['active'] == 1) {
        
        $WhrDateChkCourse .= " AND ((($cenddate) >=$now) AND (end_date IS NULL || end_date = 0 || ($penddate) >=" . $now . ")) ";
        
        $WhrDateChkClassroom = " AND ((class_end IS NULL || class_end = 0 || 
		(
			(($classenddate) >=".$now." && ($classstartdate) <= ".$now.")
			|| ($classstartdate) >= ".$now."
		)
		)) ";
        $WhrDateChkProgram = " AND ((end_date IS NULL || end_date = 0 || ($penddate) >=".$now.")) ";
        
    } 
    
    $where1 = '';
    $where2 = '';
    /*
      $type = 1 All (not completed)
      $type = 2 completed
      $type = 3 not started
      $type = 4 in progress
      $type = 5 All (including completed)
     */
    if ($type == 1) {
        $where1 = " AND	(stat_scorm = 'not started' || 
						stat_scorm = 'incomplete' || 
						stat_resource ='not started' || 
						stat_resource = 'incomplete')";
        $where2 = " AND class_submit != 1";
        $WhrProgram1 = " WHERE (	stat_scorm = 'not started' || 
						stat_scorm = 'incomplete' || 
						stat_resource LIKE '%not started%' || 
						stat_resource LIKE  '%in progress%' ||
						(stat_scorm = '' && 
						stat_resource =  '')
						)";
        $WhrProgram2 = "AND   class_submit != 1";
    } elseif ($type == 2) {
        $where1 = " AND	(stat_scorm != 'not started' && stat_scorm != 'incomplete') AND 
						(stat_resource != 'incomplete' && stat_resource != 'not started')";
        $where2 = " AND   class_submit = 1";

        $WhrProgram1 = " WHERE (stat_scorm != 'incomplete' && stat_scorm != 'not started' && stat_resource NOT LIKE '%not started%' AND stat_resource NOT LIKE '%in progress%') && (trim(stat_scorm) != '' OR trim(stat_resource) != '')";
        $WhrProgram2 = " AND   class_submit = 1";
    } elseif ($type == 3) {
        $where1 = " AND	(stat_scorm = '' && stat_resource = '') || 
		(stat_scorm = 'not started' && stat_resource = '') || 
		(stat_scorm = '' && stat_resource = 'not started') || 
		(stat_scorm = 'not started' && stat_resource = 'not started')";
        $where2 = " AND class_start > " . strtotime('now') . " AND class_submit != 1";

        $WhrProgram1 = " WHERE (stat_scorm = '' && stat_resource = '') || 
		(stat_scorm = 'not started' && stat_resource = '') || 
		(stat_scorm = '' && (stat_resource LIKE '%not started%' && stat_resource NOT LIKE '%completed%' && stat_resource NOT LIKE '%in progress%')) || 
		(stat_scorm = 'not started' && (stat_resource LIKE '%not started%' && stat_resource NOT LIKE '%completed%' && stat_resource NOT LIKE '%in progress%'))";
        $WhrProgram2 = " AND class_start > " . strtotime('now') . " AND class_submit != 1";
    } elseif ($type == 4) {
        $where1 = " AND	((stat_scorm = '' && stat_resource = 'incomplete') || 
						(stat_scorm = 'incomplete' && stat_resource = '') || 
						(stat_scorm = 'incomplete' && stat_resource = 'incomplete') || 
						(stat_scorm = 'incomplete' && stat_resource = 'not started') || 
						(stat_scorm = 'not started' && stat_resource = 'incomplete')|| 
						(stat_scorm = 'completed' && stat_resource = 'incomplete') || 
						(stat_scorm = 'incomplete' && stat_resource = 'completed') || 
						(stat_scorm = 'not started' && stat_resource = 'completed') || 
						(stat_scorm = 'completed' && stat_resource = 'not started'))";
        $where2 = " AND class_submit != 1 AND (class_start) <= " . strtotime('now') . " ";
        $WhrProgram1 = " WHERE	((stat_scorm = '' && stat_resource LIKE '%in progress%') || 
						(stat_scorm = 'incomplete' && stat_resource = '') || 
						(stat_scorm = 'incomplete' && stat_resource LIKE '%in progress%') || 
						(stat_scorm = 'incomplete' && stat_resource LIKE '%not started%') || 
						(stat_scorm = 'not started' && stat_resource LIKE '%in progress%')|| 
						(stat_scorm = 'completed' && stat_resource LIKE '%in progress%') || 
						(stat_scorm = 'incomplete' && stat_resource LIKE '%completed%') || 
						(stat_scorm = 'not started' && stat_resource LIKE '%completed%') || 
						(stat_scorm = 'completed' && stat_resource LIKE '%not started%')
						)";
        $WhrProgram2 = 'AND class_submit != 1';
    }
    $programQuery = "SELECT * FROM( SELECT CONCAT_WS('_',`p`.`program_id`,0) AS `dif_id`,1 as rowcount,`p`.`program_id` AS `program_id`,0 AS `id`,`p`.`program_name` AS `program_name`,`p`.`program_description` AS `program_description`,`p`.`user_id` AS `user_id`,`p`.`course_name` AS `course_name`,'' AS `course_description`,`p`.`pobj` AS `learningobj`,`p`.`pout` AS `performanceout`, GROUP_CONCAT(IF((coursetype_id = 1 || coursetype_id IS NULL), stat_scorm,'') SEPARATOR ' ') AS `stat_scorm`,GROUP_CONCAT(IF(coursetype_id = 2, IF(DATE_FORMAT(class_start,'%m-%d-%Y')> " . $now . ", 'not started', IF (class_submit = 1, 'completed','in progress')), '') SEPARATOR ' ') AS `stat_resource`,`p`.`classroom_stat` AS`classrooom_stat`,`coursetype_id`,`p`.`primary_instructor` AS `primary_instructor`,`p`.`classroom_instruction` AS `classroom_instruction`,`p`.`classroom_course_duration` AS `classroom_course_duration`,`p`.`course_createdby` AS `course_createdby`,`p`.`classroom_stat` AS `final_classrooom_stat`,`p`.`end_date` AS `end_date`, CAST(`p`.`credithours` AS CHAR CHARSET utf8) AS `credithours`, class_submit, class_start, class_end,sch_id, class_complete FROM (
(
SELECT `com`.`program_id` AS `program_id`,`com`.`id` AS `id`,`com`.`program_name` AS `program_name`,`com`.`program_description` AS `program_description`,`com`.`pobj` AS `pobj`,`com`.`pout` AS `pout`,`com`.`user_id` AS `user_id`,`com`.`course_name` AS `course_name`, @stat_scorm :=
GROUP_CONCAT(CONCAT_WS(' ',`com`.`scorm_stat`,`com`.`resourse_stat`) SEPARATOR '') AS `scorm_stat`, GROUP_CONCAT(`com`.`resourse_stat` SEPARATOR ' ') AS `resourse_stat`, IF(
	(GROUP_CONCAT(CONCAT_WS(' ',`com`.`scorm_stat`,`com`.`resourse_stat`) SEPARATOR '') REGEXP 'not started'),
	IF(
	(GROUP_CONCAT(CONCAT_WS(' ',`com`.`scorm_stat`,`com`.`resourse_stat`) SEPARATOR '') REGEXP 'incomplete|failed|passed|completed'),
	'incomplete','not started'),
	IF(
		(GROUP_CONCAT(CONCAT_WS(' ',`com`.`scorm_stat`,`com`.`resourse_stat`) SEPARATOR '') REGEXP 'incomplete'),
		IF(GROUP_CONCAT(CONCAT_WS(' ',`com`.`scorm_stat`,`com`.`resourse_stat`) SEPARATOR '') REGEXP 'not started|failed|passed|completed',
		'incomplete',
		'incomplete'),
		IF(
			(GROUP_CONCAT(CONCAT_WS(' ',`com`.`scorm_stat`,`com`.`resourse_stat`) SEPARATOR '') REGEXP 'passed|completed'),
			IF(GROUP_CONCAT(CONCAT_WS(' ',`com`.`scorm_stat`,`com`.`resourse_stat`) SEPARATOR '') REGEXP 'not started|failed|incomplete',
			'incomplete',
			'completed'),''
		)
	)
) AS stat_scorm, @stat_resource := IF((GROUP_CONCAT(`com`.`resourse_stat` SEPARATOR '') REGEXP '^(not started)+$'), IF(((GROUP_CONCAT(`com`.`resourse_stat` SEPARATOR '') REGEXP '^(passed|completed)+$') OR (GROUP_CONCAT(`com`.`resourse_stat` SEPARATOR '') REGEXP '^(incomplete|failed)+$')),'incomplete','not started'), IF((GROUP_CONCAT(`com`.`resourse_stat` SEPARATOR '') REGEXP 'incomplete'),'incomplete', IF((GROUP_CONCAT(`com`.`resourse_stat` SEPARATOR '') REGEXP 'incomplete|failed'),'incomplete', IF((GROUP_CONCAT(`com`.`resourse_stat` SEPARATOR '') REGEXP 'passed|completed'),'completed','')))) AS stat_resource,`com`.`coursetype_id` AS `coursetype_id`,`com`.`primary_instructor` AS `primary_instructor`,`com`.`classroom_instruction` AS `classroom_instruction`,`com`.`classroom_course_duration` AS `classroom_course_duration`,`com`.`course_createdby` AS `course_createdby`,`com`.`end_date` AS `end_date`, GROUP_CONCAT(`com`.`credithours` SEPARATOR '_') AS `credithours`,'' AS `classroom_stat`
,0 as class_submit,0 as  class_start, 0 as class_end,0 as sch_id, 0 as class_complete
FROM `vw_user_program_intermediate_stat` `com`
$where 
GROUP BY `com`.`program_id`,`com`.`user_id`
) 
UNION (
SELECT `com`.`program_id` AS `program_id`,`com`.`id` AS `id`,`com`.`program_name` AS `program_name`,`com`.`program_description` AS `program_description`,
`com`.`pobj` AS `pobj`,`com`.`pout` AS `pout`,`com`.`user_id` AS `user_id`,`com`.`course_name` AS `course_name`,'' AS `scorm_stat`,'' AS `resourse_stat`,'' AS stat_scorm,'' AS stat_resource,`com`.`coursetype_id` AS `coursetype_id`,`com`.`primary_instructor` AS `primary_instructor`,`com`.`classroom_instruction` AS `classroom_instruction`,`com`.`classroom_course_duration` AS `classroom_course_duration`,`com`.`course_createdby` AS `course_createdby`,`com`.`end_date` AS `end_date`, 
(`com`.`credithours`) AS `credithours`, 
(`com`.`classroom_stat`) AS `classroom_stat`, class_submit, class_start, class_end,0 as sch_id, class_complete
FROM `vw_user_program_classroom_stat` `com`
$where
)) as p GROUP BY p.program_id ) as a
$WhrProgram1 $WhrDateChkProgram $whereSearchProgram
";
    /*$courseQuery = "SELECT * FROM (SELECT CONCAT_WS('_','0', CAST(`cs`.`id` AS CHAR CHARSET utf8)) AS `dif_id`,1 as rowcount,0 AS `program_id`,`cs`.`id` AS `id`,'' AS `program_name`,'' AS `program_description`, `cs`.`userid` AS `user_id`,`cs`.`course_name` AS `course_name`,`cs`.`category` AS `category`,`cs`.`summary` AS `course_description`,`cs`.`learningobj` AS `learningobj`,`cs`.`performanceout` AS `performanceout`, IF((`cs`.`scorm_stat` REGEXP 'not started'), IF((`cs`.`scorm_stat` REGEXP 'incomplete'),'incomplete', IF((`cs`.`scorm_stat` REGEXP 'passed|completed'),'incomplete','not started')), IF((`cs`.`scorm_stat` REGEXP 'incomplete'),'incomplete', IF((`cs`.`scorm_stat` REGEXP 'passed|completed'),'completed',''))) AS `stat_scorm`,`cs`.`resourse_stat` AS `stat_resource`,'' AS `classrooom_stat`,`cs`.`coursetype_id` AS `coursetype_id`,`cs`.`primary_instructor` AS `primary_instructor`,`cs`.`classroom_instruction` AS `classroom_instruction`,`cs`.`classroom_course_duration` AS `classroom_course_duration`,`cs`.`course_createdby` AS `course_createdby`,'' AS `final_classrooom_stat`,`cs`.`end_date` AS `end_date`, CAST(`cs`.`credithours` AS CHAR CHARSET utf8) AS `credithours`,0 as class_submit,0 as  class_start, 0 as class_end,0 as sch_id, 0 as class_complete FROM (SELECT `c`.`id` AS `id`,`ucm`.`userid` AS `userid`,`c`.`fullname` AS `course_name`,`c`.`category` AS `category`,`c`.`summary` AS `summary`,`c`.`learningobj` AS `learningobj`,`c`.`performanceout` AS `performanceout`,group_concat(if((`a`.`combined_stat` regexp 'passed|completed'),'completed',if((`a`.`combined_stat` regexp 'not started'),if((`a`.`combined_stat` regexp 'incomplete'),'incomplete',if((`a`.`combined_stat` regexp 'passed|completed'),'incomplete','not started')),if((`a`.`combined_stat` regexp 'incomplete'),'incomplete',''))) separator ' ') AS `scorm_stat`,if((`b`.`combined_stat` regexp 'completed'),if((`b`.`combined_stat` regexp 'not started'),'incomplete','completed'),if(isnull(`b`.`combined_stat`),'','not started')) AS `resourse_stat`,`c`.`coursetype_id` AS `coursetype_id`,`c`.`primary_instructor` AS `primary_instructor`,`c`.`classroom_instruction` AS `classroom_instruction`,`c`.`classroom_course_duration` AS `classroom_course_duration`,`c`.`createdby` AS `course_createdby`,`ucm`.`end_date` AS `end_date`,`c`.`credithours` AS `credithours` FROM `mdl_user_course_mapping` `ucm` INNER JOIN `mdl_course` `c` ON `c`.`id` = `ucm`.`courseid` AND `c`.`is_active` = 1 AND `ucm`.`userid`=$USER->id LEFT JOIN `vw_user_scorm_stat` `a` ON `a`.`id` = `ucm`.`courseid` AND `a`.`userid` = `ucm`.`userid` LEFT JOIN `vw_user_resource_stat` `b` ON `b`.`id` = `ucm`.`courseid` AND `b`.`userid` = `ucm`.`userid` WHERE `ucm`.`status` = 1 GROUP BY `c`.`id`,`ucm`.`userid`) `cs` LEFT JOIN mdl_course as c ON cs.id = c.id
$whereC $WhrDateChkCourse)  as a " . $where . $where1 . $whereSearchCourse . $online_course_cat_id;*/

$courseQuery = "SELECT * FROM (SELECT CONCAT_WS('_','0', CAST(`cs`.`id` AS CHAR CHARSET utf8)) AS `dif_id`,1 as rowcount,0 AS `program_id`,`c`.`idnumber`,`cs`.`id` AS `id`,'' AS `program_name`,'' AS `program_description`, `cs`.`userid` AS `user_id`,`cs`.`course_name` AS `course_name`,`cs`.`category` AS `category`,`cs`.`summary` AS `course_description`,`cs`.`learningobj` AS `learningobj`,`cs`.`performanceout` AS `performanceout`, IF((`cs`.`scorm_stat` REGEXP 'not started'), IF((`cs`.`scorm_stat` REGEXP 'incomplete'),'incomplete', IF((`cs`.`scorm_stat` REGEXP 'passed|completed'),'incomplete','not started')), IF((`cs`.`scorm_stat` REGEXP 'incomplete'),'incomplete', IF((`cs`.`scorm_stat` REGEXP 'passed|completed'),'completed',''))) AS `stat_scorm`,`cs`.`resourse_stat` AS `stat_resource`,'' AS `classrooom_stat`,`cs`.`coursetype_id` AS `coursetype_id`,`cs`.`primary_instructor` AS `primary_instructor`,`cs`.`classroom_instruction` AS `classroom_instruction`,`cs`.`classroom_course_duration` AS `classroom_course_duration`,`cs`.`course_createdby` AS `course_createdby`,'' AS `final_classrooom_stat`,`cs`.`end_date` AS `end_date`, CAST(`cs`.`credithours` AS CHAR CHARSET utf8) AS `credithours`,0 as class_submit,0 as  class_start, 0 as class_end,0 as sch_id, 0 as class_complete FROM (SELECT `c`.`id` AS `id`,`ucm`.`userid` AS `userid`,`c`.`fullname` AS `course_name`,`c`.`category` AS `category`, `c`.`summary` AS `summary`,`c`.`learningobj` AS `learningobj`,`c`.`performanceout` AS `performanceout`,group_concat(if((`a`.`combined_stat` regexp 'passed|completed'),'completed',if((`a`.`combined_stat` regexp 'not started'),if((`a`.`combined_stat` regexp 'incomplete'),'incomplete',if((`a`.`combined_stat` regexp 'passed|completed'),'incomplete','not started')),if((`a`.`combined_stat` regexp 'incomplete'),'incomplete',''))) separator ' ') AS `scorm_stat`,if((`b`.`combined_stat` regexp 'completed'),if((`b`.`combined_stat` regexp 'not started'),'incomplete','completed'),if(isnull(`b`.`combined_stat`),'','not started')) AS `resourse_stat`,`c`.`coursetype_id` AS `coursetype_id`,`c`.`primary_instructor` AS `primary_instructor`,`c`.`classroom_instruction` AS `classroom_instruction`,`c`.`classroom_course_duration` AS `classroom_course_duration`,`c`.`createdby` AS `course_createdby`,`ucm`.`end_date` AS `end_date`,`c`.`credithours` AS `credithours` FROM `mdl_user_course_mapping` `ucm` INNER JOIN `mdl_course` `c` ON `c`.`id` = `ucm`.`courseid` AND `c`.`is_active` = 1 AND `ucm`.`userid`=$USER->id LEFT JOIN (select `c`.`id` AS `id`,`s`.`id` AS `scormid`,`map`.`userid` AS `userid`,`c`.`fullname` AS `fullname`,`m`.`module` AS `module`,group_concat(if(isnull(`sst`.`value`),'not started',if((`sst`.`value` regexp '[0-9]+'),'incomplete',`sst`.`value`)) separator ' ') AS `combined_stat` from ((((((`mdl_user_course_mapping` `map` inner join `mdl_course` `c` on((`map`.`userid` = $USER->id) and (`c`.`id` = `map`.`courseid`))) left join `mdl_course_categories` `cc` on((`cc`.`id` = `c`.`category`))) left join `mdl_course_modules` `m` on(((`m`.`course` = `c`.`id`) and (`m`.`module` = 18)))) left join `mdl_scorm` `s` on(((`s`.`course` = `c`.`id`) and (`s`.`id` = `m`.`instance`)))) left join `mdl_scorm_scoes_track` `sst` on(((`m`.`instance` = `sst`.`scormid`) and (`m`.`module` = 18) and (`map`.`userid` = `sst`.`userid`)))) left join `mdl_scorm_scoes` `sco` on(((`m`.`instance` = `sco`.`scorm`) and (`sco`.`id` = `sst`.`scoid`) and (`sco`.`scormtype` = 'sco')))) where ((`c`.`id` <> 1) and (`map`.`status` = 1) and (`map`.`program_user_ref_id` = 0) and (`m`.`module` = 18) and (`s`.`is_active` = 1)) group by `c`.`id`,`s`.`id`,`map`.`userid`)`a` ON `a`.`id` = `ucm`.`courseid` AND `a`.`userid` = `ucm`.`userid` LEFT JOIN (select `c`.`id` AS `id`,`r`.`id` AS `resourceid`,`map`.`userid` AS `userid`,`c`.`fullname` AS `fullname`,`m`.`module` AS `module`,group_concat(if((`nl`.`id` > 0),_utf8'completed',_utf8'not started') separator ' ') AS `combined_stat` from (((((`mdl_user_course_mapping` `map` inner join `mdl_course` `c` on((`map`.`userid` = $USER->id) and (`c`.`id` = `map`.`courseid`))) left join `mdl_course_categories` `cc` on((`cc`.`id` = `c`.`category`))) left join `mdl_course_modules` `m` on(((`m`.`course` = `c`.`id`) and (`m`.`module` = 17)))) left join `mdl_resource` `r` on(((`r`.`course` = `c`.`id`) and (`r`.`id` = `m`.`instance`) and (`m`.`module` = 17)))) left join `mdl_user_noncourse_lastaccess` `nl` on(((`nl`.`courseid` = `c`.`id`) and (`nl`.`resource_id` = `r`.`id`) and (`m`.`module` = 17) and (`map`.`userid` = `nl`.`userid`)))) where ((`c`.`id` <> 1) and (`map`.`status` = 1) and (`map`.`program_user_ref_id` = 0) and (`m`.`module` = 17) and (`r`.`is_active` = 1)) group by `c`.`id`,`map`.`userid`)`b` ON `b`.`id` = `ucm`.`courseid` AND `b`.`userid` = `ucm`.`userid` WHERE `ucm`.`status` = 1 GROUP BY `c`.`id`,`ucm`.`userid`) `cs` 
LEFT JOIN mdl_course as c ON cs.id = c.id
$whereC $WhrDateChkCourse)  as a " . $where . $where1 . $whereSearchCourse . $online_course_cat_id;
    $grpby = '';
    if ($type == 5) {
        $grpby = ', `vw_classroom_data`.class_id';
    }
    $classroomSql = "SELECT `vw_classroom_data`.`dif_id` AS `dif_id`,count(*) as rowcount,`vw_classroom_data`.`program_id` AS `program_id`,`vw_classroom_data`.`id` AS `id`,`vw_classroom_data`.`classname` AS `program_name`,`vw_classroom_data`.`program_description` AS `program_description`,`vw_classroom_data`.`user_id` AS `user_id`,`vw_classroom_data`.`course_name` AS `course_name`,`vw_classroom_data`.`course_description` AS `course_description`,`vw_classroom_data`.`learningobj` AS `learningobj`,`vw_classroom_data`.`performanceout` AS `performanceout`,`vw_classroom_data`.`stat_scorm` AS `stat_scorm`,`vw_classroom_data`.`stat_resource` AS `stat_resource`,`vw_classroom_data`.`classrooom_stat` AS `classrooom_stat`,`vw_classroom_data`.`coursetype_id` AS `coursetype_id`,`vw_classroom_data`.`primary_instructor` AS `primary_instructor`,`vw_classroom_data`.`classroom_instruction` AS `classroom_instruction`,`vw_classroom_data`.`classroom_course_duration` AS `classroom_course_duration`,`vw_classroom_data`.`course_createdby` AS `course_createdby`, GROUP_CONCAT(`vw_classroom_data`.`classrooom_stat` SEPARATOR ' ') AS `final_classrooom_stat`, MAX(`vw_classroom_data`.`end_date`) AS `end_date`, GROUP_CONCAT(CAST(`vw_classroom_data`.`credithours` AS CHAR CHARSET utf8) SEPARATOR '_') AS `credithours`, class_submit, class_start, class_end,0 as sch_id, class_complete, `vw_classroom_data`.`category` as `category`  FROM `vw_classroom_data` $where " . $where2 . $WhrDateChkClassroom . $whereSearchCourse . $classroom_course_cat_id . " GROUP BY `vw_classroom_data`.`dif_id`,`vw_classroom_data`.`user_id`" . $grpby;

     
    $prgSQL = '';
    $courseSQL='';
    $clsrmsSQL ='';
    if ((!isset($paramArr['classroom']) && !isset($paramArr['course']) && !isset($paramArr['program'])) || ( $paramArr['classroom'] == 0 && $paramArr['course'] == 0 && $paramArr['program'] == 0)) {

        $prgSQL = $programQuery;
        $courseSQL = $courseQuery;
        $clsrmsSQL = $classroomSql;
        
    } else {

        if (isset($paramArr['program']) && $paramArr['program'] == 1) {
            $prgSQL = $programQuery;
        }
        if (isset($paramArr['course']) && $paramArr['course'] == 1) {
            $courseSQL = $courseQuery;
        }
        if (isset($paramArr['classroom']) && $paramArr['classroom'] == 1) {
            $clsrmsSQL = $classroomSql;
        }
        
    }
    
    //echo $clsrmsSQL;die;
    if(!$programDsiplay){
        $prgSQL='';
    }
    $getOutPutData = getQueryOutputData($prgSQL,$courseSQL,$clsrmsSQL);
  
    return $getOutPutData;
    
}

function getQueryOutputData($programQuery,$courseQuery,$classroomSql){
        global $USER, $DB, $CFG;       
         
        $onlineCoursesArr = array();
        $programsArr = array();
        $classroomCoursesArr = array();
        $outputArray = array();
        
/*
echo $courseQuery."<br/><br/>";
//die;

echo $programQuery."<br/><br/>";
echo $classroomSql."<br/><br/>";
die;*/

        if($courseQuery){
            $onlineCoursesArr = $DB->get_records_sql($courseQuery);
          
            $setDataorder = setdisplayorderofdata($onlineCoursesArr,'id');
              //pr($onlineCoursesArr);die;
        }    
     
        if($programQuery){
            $programsArr = $DB->get_records_sql($programQuery);
            /*foreach($programsArr as $key=>$value){
                $programsArr[$key]->course_name = $value->program_name;
            }*/
        }
       
        
        if($classroomSql){
            $classroomCoursesArr = $DB->get_records_sql($classroomSql);
        }        
       
        $outputArray = array_merge($onlineCoursesArr, $programsArr, $classroomCoursesArr);        
        
        //usort($outputArray, "cmp");
        // pr($outputArray);
        
        return $outputArray;
}

function cmp($a, $b)
{
    return $a->end_date < $b->end_date;
    //return strcasecmp($a->course_name, $b->course_name);
   
}