<?php

require_once($CFG->dirroot.'/enrol/locallib.php');
class local_course_enrolment_manager extends course_enrolment_manager {


	 /**
     * Gets an array of the users that can be enrolled in this course.
     *
     * @global moodle_database $DB
     * @param int $enrolid
     * @param string $search
     * @param bool $searchanywhere
     * @param int $page Defaults to 0
     * @param int $perpage Defaults to 25
     * @param int $addedenrollment Defaults to 0
     * @return array Array(totalusers => int, users => array)
     */
    public function local_get_potential_users($enrolid, $search='', $searchanywhere=false, $page=0, $perpage=25, $addedenrollment=0) { 
        global $DB;

        list($ufields, $params, $wherecondition) = $this->get_basic_search_conditions($search, $searchanywhere);

      /*  $fields      = 'SELECT '.$ufields;
        $countfields = 'SELECT COUNT(1)';
        $sql = " FROM {user} u
            LEFT JOIN {user_enrolments} ue ON (ue.userid = u.id AND ue.enrolid = :enrolid)
                WHERE $wherecondition
                      AND ue.id IS NULL";*/

		$fields      = 'SELECT '.$ufields.',ra.roleid';
        $countfields = 'SELECT COUNT(1)';
        $sql = " FROM {user} u
            LEFT JOIN {user_enrolments} ue ON (ue.userid = u.id AND ue.enrolid = :enrolid)
			LEFT JOIN {role_assignments} ra ON (ra.userid = u.id AND ra.contextid = 1)
                WHERE $wherecondition
                      AND ue.id IS NULL AND ra.roleid  = 5";
        $params['enrolid'] = $enrolid;

        return $this->execute_search_queries($search, $fields, $countfields, $sql, $params, $page, $perpage, $addedenrollment);
    }

}
