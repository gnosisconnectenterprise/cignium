<?php 
global $CFG, $DB;
$sdepartmentArr	= array('-1');
$sParentArr		= array('-1');
$sOwnerArr	= array('-1');
$searchParamArray = array('sel_mode'=>1);
$searchUserParamArray = array('is_active'=>1,'group_owner_search'=>1,'sel_mode_own'=>1);
if(!empty($paramArray['department'])){
	$sdepartmentArr	= explode('@',$paramArray['department']);
	$searchParamArray['department'] = $paramArray['department'];
	$searchUserParamArray['department'] = $paramArray['department'];
	$searchUserParamArray['sel_mode_own'] = 1;
}else{
	if($paramArray['sel_mode'] == 2){
		$searchParamArray['sel_mode'] = 2;
		$searchUserParamArray['sel_mode_own'] = 2;
	}
}
if(!empty($paramArray['teams'])){
	$sParentArr	= explode('@',$paramArray['teams']);
}
if(!empty($paramArray['users'])){
	$sOwnerArr	= explode('@',$paramArray['users']);
}
if($USER->archetype == $CFG->userTypeManager){
	$display = "style = 'display:none;'";
	$departmentRecords = $DB->get_records_sql("SELECT d.id,d.title FROM mdl_department_members as dm LEFT JOIN mdl_department as d on d.id = dm.departmentid WHERE userid = ".$USER->id." and d.deleted = 0 and status=1 order by d.title ASC");
	foreach($departmentRecords as $departmentRecord){
		$sdepartmentArr = array($departmentRecord->id);
	}
}else{
	$display = "";
	$departmentRecords = $DB->get_records_sql("SELECT d.id,d.title FROM mdl_department as d WHERE d.deleted = 0 and status=1 order by d.title ASC");
}
$usersList = get_users_listing('u.firstname', '', 1, 0, '', '', '','',$searchUserParamArray);
$groupsList = getParentsListing($searchParamArray);
if($USER->archetype == $CFG->userTypeManager){
	$globalTeamOption = "";
}else{
	$globalTeamOption = "<option value = '-2'>".get_string("global_group_column")."</option>";
}
if($USER->archetype == $CFG->userTypeManager){
	$display = "style = 'display:none;'";
	$displayFilter1 = "style = 'display:none;'";
	$displaySelector = "style = 'display:none;'";
	$displayFilter2 = "style = 'display:block;'";
}else{
	$display = "style = 'display:block;'";
	$displayFilter1 = "style = 'display:none;'";
	$displaySelector = "style = 'display:block;'";
	$displayFilter2 = "style = 'display:block;'";

	$radioChecked1 = "checked";
	$radioChecked2 = "";
	if($paramArray['sel_mode'] == 2){
		$display = "style = 'display:none;'";
		$displayFilter1 = "style = 'display:block;'";
		$displayFilter2 = "style = 'display:none;'";

		$radioChecked1 = "";
		$radioChecked2 = "checked";
	}
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/style/jquery-ui.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-ui.js"></script>
<div class="subs_reports_status_bar subsReport" id="report-search">
  
  <div class="charc-filter" >
	<div class = "field_selector" <?php echo $displaySelector;?> >
		<div class ="selector-1 selector-radio">
			<input type = "radio" name = "selector" value = "1" class = "sel_mode" <?php echo $radioChecked1;?> ><span><?php echo get_string("by_department"); ?></span>
		</div>
		<div class ="selector-2 selector-radio">
			<input type = "radio" name = "selector" value = "2" class = "sel_mode" <?php echo $radioChecked2;?> ><span><?php echo get_string("by_group"); ?></span>
		</div>
	</div>
    <div class="select_box_mul" >
		<select autocomplete="off" size="2" multiple="multiple" class="" id="department" name="department" <?php echo $display; ?> >
			<option value="-1" <?php echo in_array('-1', $sdepartmentArr)?'selected="selected"':''; ?> ><?php echo get_string('alldepartment','user');?></option>
			<?php
			if(!empty($departmentRecords)){
				foreach($departmentRecords as $departmentRecord){
					$selectedC = in_array($departmentRecord->id, $sdepartmentArr)?'selected="selected"':'';
				?>
					 <option value="<?php echo $departmentRecord->id?>" <?php echo $selectedC;?> ><?php echo $departmentRecord->title;?></option>
				<?php
				}
			}
			?>
        </select>
		<select autocomplete="off" size="2" multiple="multiple" class="" id="teams" name="teams" >
			<option value="-1" <?php echo in_array('-1', $sParentArr)?'selected="selected"':''; ?> ><?php echo get_string('select_all_parent');?></option>
			<?php
			if(!empty($groupsList)){
				foreach($groupsList as $groupList){
					$selectedC = in_array($groupList->id, $sParentArr)?'selected="selected"':'';
				?>
					 <option value="<?php echo $groupList->id?>" <?php echo $selectedC;?> ><?php echo $groupList->name;?></option>
				<?php
				}
			}
			?>
        </select>
		<select autocomplete="off" size="2" multiple="multiple" class="" id="users" name="users" >
			<option value="-1" <?php echo in_array('-1', $sOwnerArr)?'selected="selected"':''; ?> ><?php echo get_string('select_all_owner');?></option>
			<?php
			if(!empty($usersList)){
				foreach($usersList as $user){
					$selectedC = in_array($user->id, $sOwnerArr)?'selected="selected"':'';
				?>
					 <option value="<?php echo $user->id?>" <?php echo $selectedC;?> ><?php echo $user->firstname." ".$user->lastname;?></option>
				<?php
				}
			}
			?>
        </select>
    </div>
	<div>
        <input type="button" id="user_search" class="f-left" value="<?php echo get_string('go','user');?>" >
        &nbsp; <input type="button"  class="f-left" onclick="resetElement();" value="<?php echo get_string('reset','user');?>">
     </div>
     <div class="clear"></div>
    </div>
  </div>

<script>

function resetElement() {

       var url = '<?php echo $CFG->wwwroot."/group/managegroups.php";?>';
		window.location.href = url;

}

function loadTeams(department,getSelected){
	if(getSelected == 1){
		var team = "<?php echo $sTeamArr; ?>";
	}else{
		var team = "-1";
	}
	var sel_mode = $("input[name=selector]:checked").val();
	if(sel_mode == undefined || sel_mode == 'undefined'){
		sel_mode = 1;
	}
    $.ajax({	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getDepartmentParentTeams&department='+department+"&team="+team+"&sel_mode="+sel_mode,
		dataType:'json',
		success:function(data){		  
			var success = data.success;
		    var error = data.error;
			if(success == 1){
			  var selectHtml = $.trim(data.response);
			  var allTeam = 'All Teams';
			  var allTeamReplace = "<?php echo get_string('select_all_parent'); ?>";
			  var res = selectHtml.split("<option");
			  res[1] = res[1].replace(allTeam, allTeamReplace); 
			  res = res.join("<option");
			  $('#teams').html(res);
		   }else{
			 alert(error);
		   }
		}	  
	}); 
}
function loadUser(department,team,user){

    //var user = '<?php echo $sUser;?>';
	var sel_mode = $("input[name=selector]:checked").val();
	if(sel_mode == undefined || sel_mode == 'undefined'){
		sel_mode = 1;
	}
    $.ajax({
	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getUserListByDnT&department='+department+"&team="+team+"&user="+user+"&isReport=1&search_owner=1&sel_mode="+sel_mode,
		dataType:'json',
		success:function(data){
		  
		   var success = data.success;
		     var error = data.error;
			   if(success == 1){
			      var selectHtml = data.response;
				  var allTeam = 'All Users';
				  var allTeamReplace = "<?php echo get_string('select_all_owner'); ?>";
				  var res = selectHtml.split("<option");
				  res[1] = res[1].replace(allTeam, allTeamReplace); 
				  res = res.join("<option");
				  $('#users').html(res);
			   }else{
				 alert(error);
			   }
		}
	  
	  });
	  

}
jQuery(document).ready(function($) {

	$("#department").on('change', function() {
	
		var department = loopSelected('department');
		
		loadTeams(department,0);
		if(department != '-1'){
			department = department.replace('-2@', '');
			department = department.replace('-2', '');
		}
		if(department == ''){
			department = '-1';
		}
		loadUser(department,'-1','-1');
			
	});	
	$("#teams").on('change', function() {
		/*var teams = loopSelected('teams');
		var department = '-1';
		loadUser(department,teams,'-1');*/
	});	
	
	$("#user_search").click(function() {
		
			var department = loopSelected('department');
			var teams = loopSelected('teams');
			var users = loopSelected('users');
			var key = $('#key').val();
			var sel_mode = $("input[name=selector]:checked").val();
			if(sel_mode == undefined || sel_mode == 'undefined'){
				sel_mode = 0;
			}
			var url = '<?php echo $CFG->wwwroot;?>/group/managegroups.php?sel_mode='+sel_mode;
			
			if($.trim(key) != '') {
				url = url+'&key='+key;
			}
			if(department != -1 && sel_mode == 1) {
				url = url+'&department='+department;
			}
			if(teams != -1) {
				url = url+'&teams='+teams;
			}
			if(users != -1) {
				url = url+'&users='+users;
			}
			window.location.href = url;
	});
	$("input[name=selector]").change(function(){
		var sel_mode = $(this).val();
		if(sel_mode == 2){
			loadTeams('-2',0);
			$("#department").hide();
		}else{
			loadTeams('-1',0);
			$("#department").show();
			$("#user_group").hide();
		}
		loadUser('-1','-1','-1');
	});
});

function loopSelected(id) {
	var selectedArray = new Array();
	selectedArray[0] = '-1';
	if($('#'+id).length) {
		$('#'+id+' :selected').each(function(i, selected) {
			selectedArray[i] = $(selected).val();
		});
	}
	if($.inArray("-1", selectedArray) > -1) {
		return -1;
	} else {
		return selectedArray.join('@');
	}
}
</script>