<?php 
global $CFG, $DB,$USER;
//$pgArr = getPrograms();
$isReport = true;
$dpArr = getDepartments('', $isReport);
$tArr = getTeams();

if(!empty($paramArray['user_group'])){
	$sUserGroupArr	= explode('@',$paramArray['user_group']);
}
if($USER->archetype == $CFG->userTypeManager){
	$userGroups = array();
}else{
	$display = "";
	$userGroups = getUserGroups(true);
}

if($USER->archetype == $CFG->userTypeManager){
	$display = "style = 'display:none;'";
	$displayFilter1 = "style = 'display:none;'";
	$displaySelector = "style = 'display:none;'";
	$displayFilter2 = "style = 'display:block;'";
}else{
	$display = "style = 'display:block;'";
	$displayFilter1 = "style = 'display:none;'";
	$displaySelector = "style = 'display:block;'";
	$displayFilter2 = "style = 'display:block;'";

	$radioChecked1 = "checked";
	$radioChecked2 = "";
	if(!empty($_REQUEST['sel_mode']) && $_REQUEST['sel_mode'] == 2){
		$display = "style = 'display:none;'";
		$displayFilter1 = "style = 'display:block;'";
		$displayFilter2 = "style = 'display:none;'";

		$radioChecked1 = "";
		$radioChecked2 = "checked";
	}
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/style/jquery-ui.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-ui.js"></script>
<div class="subs_reports_status_bar subsReport" id="report-search">
  
  <div class="charc-filter" >
	<?php /*?><div class = "field_selector" <?php echo $displaySelector;?> >
		<div class ="selector-1 selector-radio">
			<input type = "radio" name = "selector" value = "1" class = "sel_mode" <?php echo $radioChecked1;?> ><span><?php echo get_string("by_department"); ?></span>
		</div>
		<div class ="selector-2 selector-radio">
			<input type = "radio" name = "selector" value = "2" class = "sel_mode" <?php echo $radioChecked2;?> ><span><?php echo get_string("by_group"); ?></span>
		</div>
	</div><?php */?>
    <?php echo getRadioFilters($displaySelector, $radioChecked1, $radioChecked2);?>
    <div class="select_box_mul" >
		<select autocomplete="off" size="2" multiple="multiple" class="" id="user_group" name="user_group" <?php echo $displayFilter1;?> >
			<option value="-1" <?php echo $userGroup == '-1'?'selected="selected"':'';?> ><?php echo get_string('select_user_group');?></option>
			<?php
			if(!empty($userGroups)){
				foreach($userGroups as $usersGroup){
					$selectedC = in_array($usersGroup->id, $sUserGroupArr)?'selected="selected"':'';
				?>
					 <option value="<?php echo $usersGroup->id?>" <?php echo $selectedC;?> ><?php echo $usersGroup->name;?></option>
				<?php
				}
			}
			?>
        </select>
      <?php /*if(count($pgArr) > 0 ){?>
      <select autocomplete="off" size="2" multiple="multiple" class="" id="program" name="program" >
        <option value="-1" <?php echo $sProgram == '-1'?'selected="selected"':'';?> ><?php echo get_string('allprograms','multicoursereport');?></option>
        <?php foreach($pgArr as $pg ){
		       $pSelected = in_array($pg->id, $sProgramArr)?'selected="selected"':'';
		?>
        <option value="<?php echo $pg->id;?>"  <?php echo $pSelected;?> ><?php echo $pg->name;?></option>
        <?php } ?>
      </select>
      <?php } */ ?>
      
    <?php /*?>  <?php if(count($dpArr) > 0 ){?>
      <select autocomplete="off" size="2" multiple="multiple" class="" id="department" name="department" <?php echo $display;?>>
        <option value="-1" <?php echo $sDepartment == '-1'?'selected="selected"':'';?> ><?php echo get_string('alldepartments','multicoursereport');?></option>
        <?php foreach($dpArr as $dp ){
		       $dSelected = in_array($dp->id, $sDepartmentArr)?'selected="selected"':'';
		?>
        <option value="<?php echo $dp->id;?>"  <?php echo $dSelected;?> ><?php echo $dp->title;?></option>
        <?php } ?>
      </select>
      <?php } ?><?php */?>
      
       <?php echo createDepartmentSelectBoxForFilter('department', $dpArr, $sDepartmentArr, $display); ?>
       
 
      <?php /*if(count($tArr) > 0 ){?>
      <select autocomplete="off" size="2" multiple="multiple" class="" id="team" name="team" <?php echo $displayFilter2;?>>
        <option value="-1" <?php echo $sTeam == '-1'?'selected="selected"':'';?> ><?php echo get_string('allteams','multicoursereport');?></option>
        <?php foreach($tArr as $teams ){
		   
		   $tSelected = in_array($teams->id, $sTeamArr)?'selected="selected"':'';
		?>
        <option value="<?php echo $teams->id;?>" <?php echo $tSelected;?> ><?php echo $teams->name;?></option>
        <?php } ?>
      </select>
      <?php }*/ ?>
      
      <select autocomplete="off" size="2" multiple="multiple" class="loadingusers" id="team" name="team" <?php echo $displayFilter2;?>>
        <option value=""><?php echo get_string('loadingteams','multiuserreport');?></option>
      </select>
      
      <!--<select autocomplete="off" size="2" multiple="multiple" class="" id="user" name="user" >
      </select>-->  
	 
      
		<select autocomplete="off" size="2" multiple="multiple" class="" id="program" name="program" >
            <option value=""><?php echo get_string('loadingprograms','multiuserreport');?></option>
        </select>
		<select autocomplete="off" size="2" multiple="multiple" class="" id="course" name="course" >
             <option value=""><?php echo get_string('loadingcourses','multiuserreport');?></option>
        </select>
      
       <?php /*if(count($allSystemUserAllArr) > 0 ){?>
      <select autocomplete="off" size="2" multiple="multiple" class="" id="user" name="user" >
        <option value="-1" <?php echo in_array('-1', $sUserArr)?'selected="selected"':'';?> ><?php echo get_string('allusers','multicoursereport');?></option>
        <?php foreach($allSystemUserAllArr as $users ){
		    $uSelected = in_array($users->id, $sUserArr)?'selected="selected"':'';
		?>
        <option value="<?php echo $users->id;?>"  <?php echo $uSelected;?> ><?php echo $users->fullname;?></option>
        <?php } ?>
      </select>
      <?php }*/ ?>
            
		<select autocomplete="off" size="2" multiple="multiple" class="" id="type" name="type" >
			<option value="-1" <?php echo in_array('-1', $sTypeArr)?'selected="selected"':'';?> ><?php echo get_string('allstatus','multicoursereport');?></option>
			<option value="3"  <?php echo in_array('3', $sTypeArr)?'selected="selected"':'';?> ><?php echo get_string('notstarted','multicoursereport');?></option>
			<option value="1" <?php echo in_array('1', $sTypeArr)?'selected="selected"':'';?>  ><?php echo get_string('inprogress','multicoursereport');?></option>
			<option value="2" <?php echo in_array('2', $sTypeArr)?'selected="selected"':'';?>  ><?php echo get_string('completed','multicoursereport');?></option>
		</select>
		  
	    <select autocomplete="off" size="2" multiple="multiple" class="" id="userstatus" name="userstatus" >
            <option value="-1" <?php echo in_array('-1', $userStatusArr) ? 'selected="selected"' : ''; ?> >
			<?php echo get_string('allusers', 'multiuserreport'); ?></option>
            <option value="0"  <?php echo in_array('0', $userStatusArr) ? 'selected="selected"' : ''; ?> >
			<?php echo get_string('statusactive', 'multiuserreport'); ?></option>
            <option value="1" <?php echo in_array('1', $userStatusArr) ? 'selected="selected"' : ''; ?>  >
			<?php echo get_string('statusdeactive', 'multiuserreport'); ?></option>
        </select>
        
        <?php 
        echo $jobTitleDD = createSelectBoxFilterFor('job_title','job_title', $sJobTitleArr);
        echo createReportToSelectBoxForFilter('report_to', '', $sLeadersNameArr, $display);
        echo createCountrySelectBoxForFilter('country', $cntArr, $sCountryArr);
         
        echo createCitySelectBoxForFilter('city', $cityArr, $sCityArr);
        echo createIsManagerSelectBoxFilterFor('is_manager_yes', $filterBox.'is_manager_yes', $sisManagerArr);
        ?>
        
	</div>
    

    <div class="date_sec">
      <div >
        <input type="button" id="report_allsubssription" class="f-left" value="<?php echo get_string('go','multicoursereport');?>" >
        &nbsp; <input type="button"  class="f-left" onclick="resetElement();" value="<?php echo get_string('reset','multicoursereport');?>" >
      </div>
    </div>
    
  </div>
</div>

<script>


function loadTeams(department){

    var team = '<?php echo $sTeam;?>';
	

    $.ajax({
	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getTeamListByDepartment&department='+department+"&team="+team,
		dataType:'json',
		success:function(data){
		  
		   var success = data.success;
		     var error = data.error;
			   if(success == 1){
			      var selectHtml = data.response;
				  $('#team').html(selectHtml);
				  var department = loopSelected('department');
				  var team = loopSelected('team');
				  loadPrograms(department, team);
			   }else{
				 alert(error);
			   }
		}
	  
	  });
	  

}



function loadCourses(department, team, program){
    var course = '<?php echo $sCourse;?>';
    $.ajax({	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getCourseListByDnT&department='+department+"&program="+program+"&team="+team+"&course="+course,
		dataType:'json',
		success:function(data){		  
			var success = data.success;
			var error = data.error;
			if(success == 1){
				var selectHtml = data.response;
				$('#course').html(selectHtml);
			}else{
				alert(error);
			}
		}	  
	}); 
}

function loadPrograms(department, team){
	var program = '<?php echo $sProgram;?>';
    $.ajax({	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getProgramListByDnT&department='+department+"&team="+team+"&program="+program,
		dataType:'json',
		success:function(data){		  
			var success = data.success;
			var error = data.error;
			if(success == 1){
				var selectHtml = data.response;
				$('#program').html(selectHtml);
				var program = loopSelected('program');
				var sel_mode = $("input[name=selector]:checked").val();
				if(sel_mode == 2){
					var department = '-1';
					var user_group = loopSelected('user_group');
					loadCourses(department, user_group, program);
				}else{
					var department = loopSelected('department');
					var team = loopSelected('team');
					loadCourses(department, team, program);
				}
			}else{
				alert(error);
			}
		}	  
	}); 
}

function resetElement() {
	var url = '<?php echo $CFG->wwwroot.$_SERVER["PHP_SELF"];?>';
	window.location.href = url;
}


jQuery(document).ready(function($) {
		
	$("#department").on('change', function() {
	
		var department = loopSelected('department');
		//var team = loopSelected('team');
		//var program = loopSelected('program');
		//loadPrograms(department, team);
		loadTeams(department);
			
	});
	
	$("#team").on('change', function() {
	
		var department = loopSelected('department');
		var team = loopSelected('team');
		//var program = loopSelected('program');
		loadPrograms(department, team);
			
	});
	$("#user_group").change(function() {
	
		var department = '-1';
		var team = loopSelected('user_group');
		
		loadPrograms(department, team);
			
	});
	
	$("#program").on('change', function() {

		var program = loopSelected('program');
		var sel_mode = $("input[name=selector]:checked").val();
		if(sel_mode == 2){
			var department = '-1';
			var user_group = loopSelected('user_group');
			loadCourses(department, user_group, program);
		}else{
			var department = loopSelected('department');
			var team = loopSelected('team');
			loadCourses(department, team, program);
		}
			
	});
	
	
	$("#report_allsubssription").on('click', function() {
			var sel_mode = $("input[name=selector]:checked").val();
			var url = '<?php echo $CFG->wwwroot.$_SERVER["PHP_SELF"];?>?sel_mode='+sel_mode;

			if(sel_mode == 2){
				var userGroup = loopSelected('user_group');

				if(userGroup != -1) {
					url = url+'&user_group='+userGroup;
				}
			}else{
				var department = loopSelected('department');
				var team = loopSelected('team');
				if(department != -1) {
					url = url+'&department='+department;
				}
				if(team != -1) {
					url = url+'&team='+team;
				}
			}
			var program = loopSelected('program');
			var user = loopSelected('user');
			var type = loopSelected('type');
			var course = loopSelected('course');
			
			var userstatus=loopSelected('userstatus');
            //if(userstatus != -1) {
                url = url + '&userstatus=' + userstatus;
            //}
			var country = loopSelected('country');
                        if(country != -1) {
                            url = url+'&country='+country;
                        }
                        
                        var city = loopSelected('city');
                        if(city != -1) {
                            url = url+'&city='+city;
                        }
                        var job_title = loopSelected('job_title');
                        if(job_title != -1) {
					url = url+'&job_title='+job_title;
			}
                        var report_to = loopSelected('report_to');
                        if(report_to != -1) {
				url = url+'&report_to='+report_to;
			}
                        
                        var is_manager_yes = loopSelected('is_manager_yes');
                        if(is_manager_yes != -1) {
				url = url+'&is_manager_yes='+is_manager_yes;
			}
                         
                        
                       // alert(url);
			if(program != -1) {
				url = url+'&program='+program;
			}
			if(user != -1) {
				url = url+'&user='+user;
			}
			if(course != -1) {
				url = url+'&course='+course;
			}
			if(type != -1) {
				url = url+'&type='+type;
			}
                        var key = $('#key').val();
                        if($.trim(key) != '') {
				url = url+'&key='+key;
			}
                       // alert(url);
			window.location.href = url;
	});
	var old_start_date = "";
	$('#start_date').click(function(){
		old_start_date = $('#start_date').val();
	});
	$('#start_date').datepicker({
		changeMonth: true,
	      changeYear: true,
		dateFormat: 'mm/dd/yy',
		onSelect: function() {			
			var d1 = $('#start_date').datepicker('getDate');
			var d2 = $('#end_date').datepicker('getDate');
			var diff = 0;
			if (d1 && d2) {
				diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
			}
		}
	});
	var old_end_date = "";
	$('#end_date').click(function(){
		old_end_date = $('#end_date').val();
	});
	$('#end_date').datepicker({
		changeMonth: true,
	      changeYear: true,
		dateFormat: 'mm/dd/yy',
		onSelect: function(dateStr) {
			var startDate = new Date($("#start_date").val());
			var endDate = new Date($('#end_date').val());
			if(startDate > endDate){
				alert("<?php echo get_string('startdatecannotbegreaterthanenddate', 'multicoursereport');?>");
				$('#end_date').val(old_end_date);
			}
			
			var d1 = $('#start_date').datepicker('getDate');
			var d2 = $('#end_date').datepicker('getDate');
			var diff = 0;
			if (d1 && d2) {
				diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
			}
		}
	});
	
	/*Selection mode */
	$("input[name=selector]").change(function(){
		var sel_mode = $(this).val();
		if(sel_mode == 2){
			$("#user_group").show();

			$("#department").hide();
			$("#team").hide();
			 //$("#filter-external-label").hide();
		}else{
			var department = loopSelected('department');
			loadTeams(department);
			$("#department").show();
			$("#team").show();

			$("#user_group").hide();
			// $("#filter-external-label").show();
		}
	});
});

function loopSelected(id) {
	var selectedArray = new Array();
	selectedArray[0] = '-1';
	if($('#'+id).length) {
		$('#'+id+' :selected').each(function(i, selected) {
			selectedArray[i] = $(selected).val();
		});
	}
	//if(selectedArray.indexOf("-1") > -1) {
	if($.inArray("-1", selectedArray) > -1) {
		return -1;
	} else {
		return selectedArray.join('@');
	}
}


 
	<?php
		if(!empty($_REQUEST['sel_mode']) && $_REQUEST['sel_mode'] == 2){
		?>
			var department = '-1';
			var user_group = loopSelected('user_group');
			loadPrograms(department, user_group);
		<?php
		}else{
			if(isset($sDepartmentArr) || isset($sTeamArr) || isset($sProgramArr)){?>
		
				var department = loopSelected('department');
				  //var team = loopSelected('team');
			   //var program = loopSelected('program');
				//loadPrograms(department, team);
				//loadCourses(department, team, program);
				loadTeams(department);
				   
			
	   <?php } }?>
 
</script>
