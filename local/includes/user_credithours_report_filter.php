<?php 

global $CFG, $DB;
$dpArr = getDepartments();
$tArr = getTeams();

$dSelected = '';
$tSelected = '';
$uSelected = '';
//$uTypeSelected = ($sUserType && in_array($sUserType, array($CFG->userTypeStudent, $CFG->userTypeInstructor)))?$sUserType:$CFG->userTypeStudent;

?>
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/style/jquery-ui.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-ui.js"></script>
<div class="subs_reports_status_bar subsReport"  id="report-search" style="border-top:none;float:left;width:100%">
  
  <div class="charc-filter" >
  
  <?php if($USER->is_instructor == 1 || $USER->archetype == $CFG->userTypeAdmin || $USER->archetype == $CFG->userTypeManager){ ?>
    <div class="select_box_mul">
    
      <!--select autocomplete="off" size="2" class="f-left" id="userType" name="userType" >
        <option value="<?php //echo $CFG->userTypeStudent;?>" <?php //echo $uTypeSelected == $CFG->userTypeStudent?'selected="selected"':'';?> ><?php //echo ucfirst($CFG->userTypeStudent);?></option>
       <option value="<?php //echo $CFG->userTypeInstructor;?>" <?php //echo $uTypeSelected == $CFG->userTypeInstructor?'selected="selected"':'';?> ><?php //echo ucfirst($CFG->userTypeInstructor);?></option>
      </select-->
      
      <select autocomplete="off" size="2"  multiple="multiple" class="f-left" id="userType" name="userType" >
        <option value="-1" <?php echo in_array('-1', $sUserTypeArr)?'selected="selected"':'';?> ><?php echo get_string('allusertype');?></option>
       <option value="<?php echo $CFG->userTypeStudent;?>" <?php echo in_array($CFG->userTypeStudent, $sUserTypeArr)?'selected="selected"':'';?> ><?php echo ucfirst($CFG->userTypeStudent);?></option>
       <option value="<?php echo $CFG->userTypeInstructor;?>" <?php echo in_array($CFG->userTypeInstructor, $sUserTypeArr)?'selected="selected"':'';?> ><?php echo ucfirst($CFG->userTypeInstructor);?></option>
      </select>
      
      <?php /*?>
       <?php if(count($dpArr) > 0 ){?>
      <select autocomplete="off" size="2" multiple="multiple" class="" id="department" name="department" >
        <option value="-1" <?php echo $sDepartment == '-1'?'selected="selected"':'';?> ><?php echo get_string('alldepartments','multiuserreport');?></option>
        <?php foreach($dpArr as $dp ){
		       $dSelected = in_array($dp->id, $sDepartmentArr)?'selected="selected"':'';
		?>
        <option value="<?php echo $dp->id;?>"  <?php echo $dSelected;?> ><?php echo $dp->title;?></option>
        <?php } ?>
      </select>
      <?php } ?>
 
      
       <!--select autocomplete="off" size="2" multiple="multiple" class="loadingusers" id="team" name="team" >
        <option value=""><?php //echo get_string('loadingteams','multiuserreport');?></option>
      </select-->
      
      <select autocomplete="off" size="2" multiple="multiple" class="loadingusers" id="user" name="user" >
        <option value=""><?php echo get_string('loadingusers','multiuserreport');?></option>
      </select>
<?php */?>
   </div>
   <?php } ?> 

   <div class="date_sec">
      <div class="startenddate"><span><?php echo get_string('credithourscomletiondate');?></span>
           <span class="fdate_selector">
             <input type="text" id="start_date" value="<?php echo $sDateSelected;?>" name="start_date" readonly = "true">
           </span>
      </div>
      <div class="startenddate"><span><?php echo get_string('daterangeto','multiuserreport');?></span>
           <span class="fdate_selector">
              <input type="text" id="end_date" value="<?php echo $eDateSelected;?>" name="end_date" readonly = "true">
           </span>
      </div>
      <div >
        <input type="button" id="report_allsubssription" class="f-left" value="<?php echo get_string('go','multiuserreport');?>">
        &nbsp;&nbsp; <input type="button"  class="f-left" onclick="resetElement();" value="<?php echo get_string('reset','multiuserreport');?>">
      </div>
    </div>
   
  </div>
</div>

<script>

function resetElement() {

        var url = "<?php echo $CFG->wwwroot;?>/reports/user_credithours_report.php?uid=<?php echo $userId;?>&userType=<?php echo $sUserType;?>&user=<?php echo $sUser;?>&department=<?php echo $sDepartment;?>&back=1";
		window.location.href = url;

}


/*function loadTeams(department){

    var team = '<?php// echo $sTeam;?>';
	

    $.ajax({
	  
	    url:'<?php// echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getTeamListByDepartment&department='+department+"&team="+team,
		dataType:'json',
		success:function(data){
		  
		   var success = data.success;
		     var error = data.error;
			   if(success == 1){
			      var selectHtml = data.response;
				  $('#team').html(selectHtml);
				  var team = loopSelected('team');
				  loadUsers(department, team);
			   }else{
				 alert(error);
			   }
		}
	  
	  });
	  

}

function loadUsers(department, userType){

    var user = '<?php //echo $sUser;?>';
	 var userTypeInstructor = '<?php //echo $CFG->userTypeInstructor;?>';
	
	var checkInstructor = userType == userTypeInstructor?1:0;

    $.ajax({
	  
	    url:'<?php //echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getUserListByDnUserType&department='+department+"&checkInstructor="+checkInstructor+"&user="+user,
		dataType:'json',
		success:function(data){
		  
		   var success = data.success;
		     var error = data.error;
			   if(success == 1){
			      var selectHtml = data.response;
				  $('#user').html(selectHtml);
			   }else{
				 alert(error);
			   }
		}
	  
	  });
	  

}
*/
jQuery(document).ready(function($) {

	/*	
	$("#department").change(function() {
	
		var department = loopSelected('department');
		var userType = loopSelected('userType');
		loadUsers(department, userType);
	});
	
	$("#userType").change(function() {
	
		var department = loopSelected('department');
		var userType = loopSelected('userType');
		loadUsers(department, userType);
	});*/
	
	/*($("#team").change(function() {
	
		var department = loopSelected('department');
		var team = loopSelected('team');
		
		loadUsers(department, team);
			
	});*/
	
	$("#report_allsubssription").click(function() {
		//validate daterange
		var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
		var sdate = $('#start_date').val();
		var edate = $('#end_date').val();
		var firstDate = new Date(sdate);
		var secondDate = new Date(edate);
		var todayDate = new Date();
		//var diffDays = Math.round(Math.abs((secondDate.getTime() - firstDate.getTime())/(oneDay)));
		var diffDays = Math.floor((secondDate.getTime() - firstDate.getTime()) / 86400000); // ms per day

		/*if(firstDate.getTime() > todayDate.getTime()) {
			alert('<?php //echo get_string('startdatecannotbegreaterthantodaydate', 'multiuserreport');?>');
			return false;
		} else if(secondDate.getTime() > todayDate.getTime()) {
			alert('<?php //echo get_string('enddatecannotbegreaterthantodaydate', 'multiuserreport');?>');
			return false;
		} else*/ if (secondDate.getTime() < firstDate.getTime()) {
			alert('<?php echo get_string('startdatecannotbegreaterthanenddate', 'multiuserreport');?>');
			return false;
		/*} else if(diffDays>365) {
			alert('<?php //echo get_string('maxperiodisoneyear', 'multiuserreport');?>');
			return false;*/
		} else {

			var startDate = $('#start_date').val();
			startDate = startDate.replace(/\//g, '-');			
			var endDate = $('#end_date').val();
			endDate = endDate.replace(/\//g, "-");
			
			var userType = loopSelected('userType');

			//var department = loopSelected('department');
			//var team = loopSelected('team');
			//var user = loopSelected('user');
			
			//var type = loopSelected('type');
			
			var url = '<?php echo $CFG->wwwroot;?>/reports/user_credithours_report.php?startDate='+startDate+'&endDate='+endDate+"&uid=<?php echo $userId;?>&user=<?php echo $sUser;?>&department=<?php echo $sDepartment;?>&back=1";
			
			/*if(department != -1) {
				url = url+'&department='+department;
			}*/
			/*if(team != -1) {
				url = url+'&team='+team;
			}*/
			/*if(user != -1) {
				url = url+'&user='+user;
			}
			*/
			//if(type != -1) {
				//url = url+'&type='+type;
			//}
			
			if(userType != -1) {
				url = url+'&userType='+userType;
			}
			var key = $('#key').val();
                        if($.trim(key) != '') {
				url = url+'&key='+key;
			}
			window.location.href = url;
		}
	});
	var old_start_date = "";
	$('#start_date').click(function(){
		old_start_date = $('#start_date').val();
	});
	$('#start_date').datepicker({
		changeMonth: true,
	      changeYear: true,
		dateFormat: 'mm/dd/yy',
		onSelect: function() {			
			var d1 = $('#start_date').datepicker('getDate');
			var d2 = $('#end_date').datepicker('getDate');
			var diff = 0;
			if (d1 && d2) {
				diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
			}
			/*if(diff > 365) {
				alert("<?php //echo get_string('maxperiodisoneyear', 'multiuserreport');?>");
				$('#start_date').val(old_start_date);
			}*/
		}
	});
	var old_end_date = "";
	$('#end_date').click(function(){
		old_end_date = $('#end_date').val();
	});
	$('#end_date').datepicker({
		changeMonth: true,
	      changeYear: true,
		dateFormat: 'mm/dd/yy',
		onSelect: function(dateStr) {
			var startDate = new Date($("#start_date").val());
			var endDate = new Date($('#end_date').val());
			if(startDate > endDate){
				alert("<?php echo get_string('startdatecannotbegreaterthanenddate', 'multiuserreport');?>");
				$('#end_date').val(old_end_date);
			}
			
			var d1 = $('#start_date').datepicker('getDate');
			var d2 = $('#end_date').datepicker('getDate');
			var diff = 0;
			if (d1 && d2) {
				diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
			}
			/*if(diff > 365) {
				alert("<?php //echo get_string('maxperiodisoneyear', 'multiuserreport');?>");
				$('#end_date').val(old_end_date);
			}*/
		}
	});
	
	
});

function loopSelected(id) {
	var selectedArray = new Array();
	selectedArray[0] = '-1';
	if($('#'+id).length) {
		$('#'+id+' :selected').each(function(i, selected) {
			selectedArray[i] = $(selected).val();
		});
	}
	//if(selectedArray.indexOf("-1") > -1) {
	if($.inArray("-1", selectedArray) > -1) {
		return -1;
	} else {
		return selectedArray.join('@');
	}
}


 
	<?php /*if(isset($sDepartmentArr)){?>
	
	    var department = loopSelected('department');
		     // var team = loopSelected('team');
			   var userType = loopSelected('userType');
			   alert(userType+"=="+department);
			    loadUsers(department, userType);
		       //loadTeams(department);
		
   <?php }*/ ?>
 
 </script>
