<?php 
global $CFG, $DB;
$sdepartmentArr	= array('-1');
$sTeamArr		= '-1';
$sManagerArr	= array();
$srolesArr		= array();
$sJobTitleArr = array('-1');
$sCompanyArr = array('-1'); 

if(!empty($paramArray['department'])){
	$sdepartmentArr	= explode('@',$paramArray['department']);
}
if(!empty($paramArray['user_group'])){
	$sUserGroupArr	= explode('@',$paramArray['user_group']);
}
if(!empty($paramArray['team']))
	$sTeamArr		= $paramArray['team'];
if(!empty($paramArray['managers']))
	$sManagerArr	= $paramArray['managers'];
if(!empty($paramArray['roles']))
	$srolesArr		= explode('@',$paramArray['roles']);

if($USER->archetype == $CFG->userTypeManager){
	$display = "style = 'display:none;'";
	$departmentRecords = getDepartments();
	foreach($departmentRecords as $departmentRecord){
		$sdepartmentArr = array($departmentRecord->id);
	}
	$userGroups = array();
}else{
	$display = "";
	$departmentRecords = getDepartments();
	$userGroups = getUserGroups();
}
if($USER->archetype == $CFG->userTypeManager){
	$display = "style = 'display:none;'";
	$displayFilter1 = "style = 'display:none;'";
	$displaySelector = "style = 'display:none;'";
	$displayFilter2 = "style = 'display:block;'";
}else{
	$display = "style = 'display:block;'";
	$displayFilter1 = "style = 'display:none;'";
	$displaySelector = "style = 'display:block;'";
	$displayFilter2 = "style = 'display:block;'";

	$radioChecked1 = "checked";
	$radioChecked2 = "";
	if(!empty($_REQUEST['sel_mode']) && $_REQUEST['sel_mode'] == 2){
		$display = "style = 'display:none;'";
		$displayFilter1 = "style = 'display:block;'";
		$displayFilter2 = "style = 'display:none;'";

		$radioChecked1 = "";
		$radioChecked2 = "checked";
	}
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/style/jquery-ui.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-ui.js"></script>
<div class="subs_reports_status_bar subsReport" id="report-search" style = "clear:both;">
  
  <div class="charc-filter" >
	<div class = "field_selector" <?php echo $displaySelector;?> >
		<div class ="selector-1 selector-radio">
			<input type = "radio" name = "enrolled-selector" value = "1" class = "sel_mode" <?php echo $radioChecked1;?> ><span><?php echo get_string("by_department"); ?></span>
		</div>
		<div class ="selector-2 selector-radio">
			<input type = "radio" name = "enrolled-selector" value = "2" class = "sel_mode" <?php echo $radioChecked2;?> ><span><?php echo get_string("by_group"); ?></span>
		</div>
	</div>
    <div class="select_box_mul" >
		<select autocomplete="off" size="2" multiple="multiple" class="" id="enrolled-user_group" name="enrolled-user_group" <?php echo $displayFilter1;?> >
		<option value="-1" <?php echo in_array('-1', $sUserGroupArr)?'selected="selected"':'' ?>  ><?php echo get_string('select_user_group');?></option>
		<?php
		if(!empty($userGroups)){
			foreach($userGroups as $userGroup){
				$selectedC = in_array($userGroup->id, $sUserGroupArr)?'selected="selected"':'';
			?>
				 <option value="<?php echo $userGroup->id?>" <?php echo $selectedC;?> ><?php echo $userGroup->name;?></option>
			<?php
			}
		}
		?>
        </select>

       
         <?php echo createDepartmentSelectBoxForFilter('enrolled-department', $departmentRecords, $sdepartmentArr, $display); ?>
        <?php echo getEDAstric('lower-assign-filter',$display, 'right');?>
        
        
		<select autocomplete="off" size="2" multiple="multiple" class="loadingteams" id="enrolled-team" name="enrolled-team" <?php echo $displayFilter2;?> >
			<option value="" <?php echo count($sTeamArr) == 0?'selected="selected"':'' ?> ><?php echo get_string('loadingteams','user');?></option>
		</select>
        
         <?php 
		   echo createSelectBoxFilterFor('job_title', 'enrolled-job_title', $sJobTitleArr);
		   echo createSelectBoxFilterFor('company', 'enrolled-company', $sCompanyArr);
                   
                   //Code added to add country filter, on 15th feb 2016
                   echo createCountrySelectBoxFilterFor('country', 'enrolled-country', $sCountryArr);

		?>
        
    </div>

    </div>
  </div>


<style>
.selector-1,.select_box_mul #enrolled-department,.select_box_mul #enrolled-user_group,.select_box_mul #enrolled-managers, .select_box_mul #enrolled-team, .select_box_mul #enrolled-team, .select_box_mul #enrolled-program  {float:left;}
</style>
<script>
var programId = "<?php echo $programId; ?>";
var filterBoxEnrolled = 'enrolled-';

function loadTeamsEnrolled(department,getSelected){

	if(getSelected == 1){
		var team = "<?php echo $sTeamArr; ?>";
	}else{
		var team = "-1";
	}
    $.ajax({	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getTeamListByDepartment&department='+department+"&team="+team+"&isReport=false",
		dataType:'json',
		success:function(data){		  
			var success = data.success;
		    var error = data.error;
			if(success == 1){
			  var selectHtml = data.response;
			  $('#'+filterBoxEnrolled+'team').html(selectHtml);
		   }else{
			 alert(error);
		   }
		}	  
	}); 
}


function resetElementEnrolled() {


      $("input[name="+filterBoxEnrolled+"selector]:first").prop('checked', true);
	  resetBoxEnrolled(1);

}



function searchUsersEnrolled(){
	var searchText = $("#user_removeselect_searchtext").val();
	var sel_mode = $("input[name="+filterBoxEnrolled+"selector]:checked").val();
	var user_group = loopSelectedEnrolled(filterBoxEnrolled+'user_group');
	var department = loopSelectedEnrolled(filterBoxEnrolled+'department');
	var managers = '-1';
	var team = loopSelectedEnrolled(filterBoxEnrolled+'team');
	var roles = '-1';
	
	var job_title = loopSelected(filterBoxEnrolled+'job_title');
	var company = loopSelected(filterBoxEnrolled+'company');
        
        var country = loopSelected(filterBoxEnrolled+'country');
	
	$.ajax({
			url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
			type:'POST',
			//data:'action=searchCourseNonUser&courseid='+courseId+'&search_text='+searchText,
			//data:'action=searchProgramUser&programid='+programId+'&search_text='+searchText+'&sel_mode='+sel_mode+'&user_group='+user_group+'&department='+department+'&managers='+managers+'&team='+team+'&roles='+roles+'&job_title='+job_title+'&company='+company,
                        //Code edit to add country filter on 15th feb 2016
                        data:'action=searchProgramUser&programid='+programId+'&search_text='+searchText+'&sel_mode='+sel_mode+'&user_group='+user_group+'&department='+department+'&managers='+managers+'&team='+team+'&roles='+roles+'&job_title='+job_title+'&country='+country+'&company='+company,
			success:function(data){
				$('#user_removeselect_wrapper').find('select').html(data);
			}
	});
}

 function resetBoxEnrolled(sel_mode){

		$("#"+filterBoxEnrolled+"department").val('-1');
		$("#"+filterBoxEnrolled+"team").val('-1');
		$("#"+filterBoxEnrolled+"user_group").val('-1');

		if(sel_mode == 2){
			$("#"+filterBoxEnrolled+"user_group").show();

			$("#"+filterBoxEnrolled+"department").hide();
			$("#"+filterBoxEnrolled+"team").hide();
			 $("#lower-filter-external-label-right").hide();
			//$("#"+filterBoxEnrolled+"managers").hide();
		}else{
			<?php if($USER->archetype == $CFG->userTypeAdmin) {?>
			  $("#"+filterBoxEnrolled+"department").show();
			   $("#lower-filter-external-label-right").show();
			<?php }else{ ?>
			   $("#"+filterBoxEnrolled+"department").hide();
			    $("#lower-filter-external-label-right").hide();
			<?php } ?>
			$("#"+filterBoxEnrolled+"team").show();
			//$("#"+filterBoxEnrolled+"managers").show();

			$("#"+filterBoxEnrolled+"user_group").hide();
		}
			
			searchUsersEnrolled();
		
}
		
jQuery(document).ready(function($) {

   	$("#"+filterBoxEnrolled+"department").on('change', function() {
	
		var department = loopSelectedEnrolled(filterBoxEnrolled+'department');
		searchUsersEnrolled();
		loadTeamsEnrolled(department,0);
		
	});	
	//code edit to add country filter on 15th feb 2016
	$("#"+filterBoxEnrolled+"team, #"+filterBoxEnrolled+"user_group, #"+filterBoxEnrolled+"job_title, #"+filterBoxEnrolled+"country, #"+filterBoxEnrolled+"company").on('change', function() {

		searchUsersEnrolled();
		
	});	
	
	/*$("#"+filterBoxEnrolled+"user_search").click(function(){
	      searchUsersEnrolled();
	});*/
	
	$("input[name="+filterBoxEnrolled+"selector]").change(function(){
	   var sel_mode = $(this).val();
	   resetBoxEnrolled(sel_mode);
	});
	
});

function loopSelectedEnrolled(id) {
	var selectedArray = new Array();
	selectedArray[0] = '-1';
	if($('#'+id).length) {
		$('#'+id+' :selected').each(function(i, selected) {
			selectedArray[i] = $(selected).val();
		});
	}
	if($.inArray("-1", selectedArray) > -1) {
		return -1;
	} else {
		return selectedArray.join('@');
	}
}


var departmentEnrolled = loopSelectedEnrolled(filterBoxEnrolled+'department');
loadTeamsEnrolled(departmentEnrolled,1);
 
</script>