<?php 
global $CFG, $DB;
$catArr = getCourseCategory();
?>
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/style/jquery-ui.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-ui.js"></script>
<div class="subs_reports_status_bar subsReport" id="report-search">
  
  <div class="charc-filter" >
    <div class="select_box_mul" >
     <?php if(count($catArr) > 0 ){?>
      <select autocomplete="off" size="2" multiple="multiple" class="" id="category" name="category" >
        <option value="-1" <?php echo $sCategory == '-1'?'selected="selected"':'';?> ><?php echo get_string('allcategory','multicoursereport');?></option>
        <?php foreach($catArr as $cat ){
		       $catSelected = in_array($cat->id, $sCategoryArr)?'selected="selected"':'';
		?>
        <option value="<?php echo $cat->id;?>"  <?php echo $catSelected;?> ><?php echo $cat->name;?></option>
        <?php } ?>
      </select>
      <?php } ?>
      
          
     <?php /* ?>
      <select autocomplete="off" size="2" multiple="multiple" class="" id="publish" name="publish" >
        <option value="-1" <?php echo in_array('-1', $sPublishArr)?'selected="selected"':'';?> ><?php echo get_string('active','multicoursereport');?></option>
        <option value="1"  <?php echo in_array('1', $sPublishArr)?'selected="selected"':'';?> ><?php echo get_string('publishyes','multicoursereport');?></option>
        <option value="0" <?php echo in_array('0', $sPublishArr)?'selected="selected"':'';?>  ><?php echo get_string('publishno','multicoursereport');?></option>
      </select>
      
      <select autocomplete="off" size="2" multiple="multiple" class="" id="active" name="active" >
        <option value="-1" <?php echo in_array('-1', $sActiveArr)?'selected="selected"':'';?> ><?php echo get_string('active','multicoursereport');?></option>
        <option value="1"  <?php echo in_array('1', $sActiveArr)?'selected="selected"':'';?> ><?php echo get_string('activeyes','multicoursereport');?></option>
        <option value="0" <?php echo in_array('0', $sActiveArr)?'selected="selected"':'';?>  ><?php echo get_string('activeno','multicoursereport');?></option>
      </select>
      <?php */ ?>
      
      <select autocomplete="off" size="2" multiple="multiple" class="" id="active" name="active" >
        <option value="-1" <?php echo in_array('-1', $sActiveArr)?'selected="selected"':'';?> ><?php echo get_string('all','multicoursereport');?></option>
        <option value="1"  <?php echo in_array('1', $sActiveArr)?'selected="selected"':'';?> ><?php echo get_string('statusactive','multicoursereport');?></option>
        <option value="0" <?php echo in_array('0', $sActiveArr)?'selected="selected"':'';?>  ><?php echo get_string('statusdeactive','multicoursereport');?></option>
        <option value="2" <?php echo in_array('2', $sActiveArr)?'selected="selected"':'';?>  ><?php echo get_string('statusunpublished','multicoursereport');?></option>
      </select>
      
    </div>
    
    <div class="date_sec">
      <div class="startenddate"><span><?php echo get_string('daterangefrom','multicoursereport');?></span>
           <span class="fdate_selector">
             <input type="text" id="start_date" value="<?php echo $sDateSelected;?>" name="start_date" readonly = "true">
           </span>
      </div>
      <div class="startenddate"><span><?php echo get_string('daterangeto','multicoursereport');?></span>
           <span class="fdate_selector">
              <input type="text" id="end_date" value="<?php echo $eDateSelected;?>" name="end_date" readonly = "true">
           </span>
      </div>
      <div >
        <input type="button" id="report_allsubssription" class="f-left" value="<?php echo get_string('go','multicoursereport');?>" >
        &nbsp; <input type="button"  class="f-left" onclick="resetElement();" value="<?php echo get_string('reset','multicoursereport');?>" >
      </div>
    </div>
    
  </div>
</div>

<script>


function resetElement() {

       /* $(':input').each(function() {
            switch(this.type) {
                case 'password':
                case 'select-multiple':
                case 'select-one':
                case 'text':
                case 'textarea':
                    $(this).val('');
                    break;
                case 'checkbox':
                case 'radio':
                    this.checked = false;
            }
        });*/



        var url = '<?php echo $CFG->wwwroot.'/reports/'.$CFG->pageCourseReport;?>';
		window.location.href = url;

}


jQuery(document).ready(function($) {


	$("#report_allsubssription").on('click', function() {
		/*validate daterange*/
		var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
		var sdate = $('#start_date').val();
		var edate = $('#end_date').val();
		var firstDate = new Date(sdate);
		var secondDate = new Date(edate);
		var todayDate = new Date();
		//var diffDays = Math.round(Math.abs((secondDate.getTime() - firstDate.getTime())/(oneDay)));
		var diffDays = Math.floor((secondDate.getTime() - firstDate.getTime()) / 86400000); // ms per day

		/*if(firstDate.getTime() > todayDate.getTime()) {
			alert('<?php //echo get_string('startdatecannotbegreaterthantodaydate', 'multicoursereport');?>');
			return false;
		}else if(secondDate.getTime() > todayDate.getTime()) {
			alert('<?php //echo get_string('enddatecannotbegreaterthantodaydate', 'multicoursereport');?>');
			return false;
		} else */ if (secondDate.getTime() < firstDate.getTime()) {
			alert('<?php echo get_string('startdatecannotbegreaterthanenddate', 'multicoursereport');?>');
			return false;
		/*} else if(diffDays>365) {
			alert('<?php //echo get_string('maxperiodisoneyear', 'multicoursereport');?>');
			return false;*/
		} else {

			var startDate = $('#start_date').val();
			startDate = startDate.replace(/\//g, '-');			
			var endDate = $('#end_date').val();
			endDate = endDate.replace(/\//g, "-");
			

			var category = loopSelected('category');
			var publish = loopSelected('publish');
			var active = loopSelected('active');
			
			var url = '<?php echo $CFG->wwwroot.'/reports/'.$CFG->pageCourseReport;?>?startDate='+startDate+'&endDate='+endDate;
			
			
			if(category != -1) {
				url = url+'&category='+category;
			}
			if(publish != -1) {
				url = url+'&publish='+publish;
			}
			if(active != -1) {
				url = url+'&active='+active;
			}
			
                        var key = $('#key').val();
                        if($.trim(key) != '') {
				url = url+'&key='+key;
			}
			window.location.href = url;
		}
	});
	var old_start_date = "";
	$('#start_date').click(function(){
		old_start_date = $('#start_date').val();
	});
	$('#start_date').datepicker({
		changeMonth: true,
	      changeYear: true,
		dateFormat: 'mm/dd/yy',
		onSelect: function() {			
			var d1 = $('#start_date').datepicker('getDate');
			var d2 = $('#end_date').datepicker('getDate');
			var diff = 0;
			if (d1 && d2) {
				diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
			}
			/*if(diff > 365) {
				alert("<?php //echo get_string('maxperiodisoneyear', 'multicoursereport');?>");
				$('#start_date').val(old_start_date);
			}*/
		}
	});
	var old_end_date = "";
	$('#end_date').click(function(){
		old_end_date = $('#end_date').val();
	});
	$('#end_date').datepicker({
		changeMonth: true,
	      changeYear: true,
		dateFormat: 'mm/dd/yy',
		onSelect: function(dateStr) {
			var startDate = new Date($("#start_date").val());
			var endDate = new Date($('#end_date').val());
			if(startDate > endDate){
				alert("<?php echo get_string('startdatecannotbegreaterthanenddate', 'multicoursereport');?>");
				$('#end_date').val(old_end_date);
			}
			
			var d1 = $('#start_date').datepicker('getDate');
			var d2 = $('#end_date').datepicker('getDate');
			var diff = 0;
			if (d1 && d2) {
				diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
			}
			/*if(diff > 365) {
				alert("<?php //echo get_string('maxperiodisoneyear', 'multicoursereport');?>");
				$('#end_date').val(old_end_date);
			}*/
		}
	});
	
});

function loopSelected(id) {
	var selectedArray = new Array();
	selectedArray[0] = '-1';
	if($('#'+id).length) {
		$('#'+id+' :selected').each(function(i, selected) {
			selectedArray[i] = $(selected).val();
		});
	}
	//if(selectedArray.indexOf("-1") > -1) {
	if($.inArray("-1", selectedArray) > -1) {
		return -1;
	} else {
		return selectedArray.join('@');
	}
}

 
</script>