<?php 
global $CFG, $DB;
$sdepartmentArr	= array('-1');
$sTeamArr		= '-1';
$sManagerArr	= '-1';
$srolesArr		= array('-1');
$courseStatusArr = array('-1');
$sJobTitleArr = array('-1');
$sCompanyArr = array('-1');
$sUserGroupArr = array('-1');

$sCityArr = array('-1');
$sLeadersNameArr = array('-1');

if(!empty($paramArray['city'])){
	$sCityArr	= explode('@',$paramArray['city']);
}
if($paramArray['report_to']!=''){
	$sLeadersNameArr = explode('@',$paramArray['report_to']);
}
if(!empty($paramArray['department'])){
	$sdepartmentArr	= explode('@',$paramArray['department']);
}
if(!empty($paramArray['user_group'])){
	$sUserGroupArr	= explode('@',$paramArray['user_group']);
}
if(!empty($paramArray['team']))
	$sTeamArr		= $paramArray['team'];
if(!empty($paramArray['managers']))
	$sManagerArr	= $paramArray['managers'];
if((strpos($paramArray['roles'],'-1')) == 0 && (!empty($paramArray['roles'])  || (strlen($paramArray['roles']) == 1 && intval($paramArray['roles']) == 0)))
	$srolesArr		= explode('@',$paramArray['roles']);

if(!empty($paramArray['is_active'])  || $paramArray['is_active'] == 0)
	$courseStatusArr = explode('@',$paramArray['is_active']);	
//pr($srolesArr);die;
if($USER->archetype == $CFG->userTypeManager){
	$display = "style = 'display:none;'";
	$departmentRecords = getDepartments();
	foreach($departmentRecords as $departmentRecord){
		$sdepartmentArr = array($departmentRecord->id);
	}
	$userGroups = array();
}else{
	$display = "";
	$departmentRecords = getDepartments();
	$userGroups = getUserGroups();
}
if($USER->archetype == $CFG->userTypeManager){
	$display = "style = 'display:none;'";
	$displayFilter1 = "style = 'display:none;'";
	$displaySelector = "style = 'display:none;'";
	$displayFilter2 = "style = 'display:block;'";
}else{
	$display = "style = 'display:block;'";
	$displayFilter1 = "style = 'display:none;'";
	$displaySelector = "style = 'display:block;'";
	$displayFilter2 = "style = 'display:block;'";

	$radioChecked1 = "checked";
	$radioChecked2 = "";
	if(!empty($_REQUEST['sel_mode']) && $_REQUEST['sel_mode'] == 2){
		$display = "style = 'display:none;'";
		$displayFilter1 = "style = 'display:block;'";
		$displayFilter2 = "style = 'display:none;'";

		$radioChecked1 = "";
		$radioChecked2 = "checked";
	}
}

if(!empty($paramArray['job_title'])){
	$sJobTitleArr	= explode('@',$paramArray['job_title']);
}

if(!empty($paramArray['company'])){
	$sCompanyArr	= explode('@',$paramArray['company']);
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/style/jquery-ui.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-ui.js"></script>
<div class="subs_reports_status_bar subsReport" id="report-search">
  
  <div class="charc-filter" >
	<?php /*?><div class = "field_selector" <?php echo $displaySelector;?> >
		<div class ="selector-1 selector-radio">
			<input type = "radio" name = "selector" value = "1" autocomplete="off" class = "sel_mode" <?php echo $radioChecked1;?> ><span><?php echo get_string("by_department"); ?></span>
		</div>
		<div class ="selector-2 selector-radio">
			<input type = "radio" name = "selector" value = "2" autocomplete="off" class = "sel_mode" <?php echo $radioChecked2;?> ><span><?php echo get_string("by_group"); ?></span>
		</div>
         <?php echo $identifierED = getEDAstric('filter'); ?>
	</div><?php */?>
    
    <?php echo getRadioFilters($displaySelector, $radioChecked1, $radioChecked2);?>
    <div class="select_box_mul" >
		<select autocomplete="off" size="2" multiple="multiple" class="" id="user_group" name="user_group" <?php echo $displayFilter1;?> >
		<option value="-1" <?php echo (in_array('-1', $sUserGroupArr))?'selected="selected"':''; ?> ><?php echo get_string('select_user_group');?></option>
		<?php
		if(!empty($userGroups)){
			foreach($userGroups as $userGroup){
				$selectedC = in_array($userGroup->id, $sUserGroupArr)?'selected="selected"':'';
			?>
				 <option value="<?php echo $userGroup->id?>" <?php echo $selectedC;?> ><?php echo $userGroup->name;?></option>
			<?php
			}
		}
		
		?>
        </select>

		<?php /*?><select autocomplete="off" size="2" multiple="multiple" class="" id="department" name="department" <?php echo $display;?> >
		<option value="-1"><?php echo get_string('alldepartment','user');?></option>
		<?php
		
		if(!empty($departmentRecords)){
			foreach($departmentRecords as $ClassroomRecords){
				 $selectedC = in_array($ClassroomRecords->id, $sdepartmentArr)?'selected="selected"':'';
				 
				 $identifierED = $ClassroomRecords->is_external == 1?getEDAstric():'';
				
			?>
				 <option value="<?php echo $ClassroomRecords->id?>" <?php echo $selectedC;?> ><?php echo $identifierED.$ClassroomRecords->title;?></option>
			<?php
			}
		}
		?>
        </select><?php */?>
        
        <?php 
       // pr($srolesArr);die;
		if(!empty($departmentRecords)){
		  echo createDepartmentSelectBoxForFilter('department', $departmentRecords, $sdepartmentArr, $display);
		}   ?>
        
		<select autocomplete="off" size="2" multiple="multiple" class="loadingclasses" id="managers" name="managers" <?php echo $displayFilter2;?> >
			<option value=""><?php echo get_string('loadingmanagers','user');?></option>
		</select>
		<select autocomplete="off" size="2" multiple="multiple" class="loadingteams" id="teams" name="teams" <?php echo $displayFilter2;?> >
			<option value=""><?php echo get_string('loadingteams','user');?></option>
		</select>
		<select autocomplete="off" size="2" multiple="multiple" class="loadingroles" id="user_roles" name="roles" >
		<option value="-1" <?php echo (in_array('-1', $srolesArr))?'selected="selected"':''; ?> ><?php echo get_string('allroles','user');?></option>
		<?php
		if($USER->archetype == $CFG->userTypeAdmin){
			$userRoles = $CFG->userTypeInSystem;
		}elseif($USER->archetype == $CFG->userTypeManager){
			$userRoles = $CFG->userTypeInSystemForManagerAccess;
		}elseif($USER->archetype == $CFG->userTypeStudent){
			$userRoles = $CFG->userTypeStudent;
		}else{
			$userRoles = $CFG->userTypeInSystem;
		}
		
		
		foreach($userRoles as $role_key=>$roles){
			?>
				<option value="<?php echo $role_key;?>" <?php echo in_array($role_key, $srolesArr)?'selected="selected"':''; ?> ><?php echo ucwords($roles);?></option>
			<?php
		}
		
		
		?>
		</select>
        
        <?php 
		  echo $jobTitleDD = createSelectBoxFilterFor('job_title','job_title', $sJobTitleArr);
                  echo createReportToSelectBoxForFilter('report_to', '', $sLeadersNameArr, $display);
                  echo createIsManagerSelectBoxFilterFor('is_manager_yes', $filterBox.'is_manager_yes', $sIsManagerYesArr);
		  echo $companyDD = createSelectBoxFilterFor('company','company', $sCompanyArr);
                  echo createCitySelectBoxForFilter('city', $cityArr, $sCityArr);
                   
                   
		?>
		<select autocomplete="off" size="2" multiple="multiple" class="" id="is_active" name="is_active" >
        <option value="-1" <?php echo in_array('-1', $courseStatusArr)?'selected="selected"':'';?> ><?php echo get_string('all','multicoursereport');?></option>
        <option value="1"  <?php echo in_array('1', $courseStatusArr)?'selected="selected"':'';?> ><?php echo get_string('statusactive','multicoursereport');?></option>
        <option value="0" <?php echo in_array('0', $courseStatusArr)?'selected="selected"':'';?>  ><?php echo get_string('statusdeactive','multicoursereport');?></option>
      </select>
        
    </div>
	<div>
        <input type="button" id="user_search" class="f-left" value="<?php echo get_string('go','user');?>">
        &nbsp; <input type="button"  class="f-left" onclick="resetElement();" value="<?php echo get_string('reset','user');?>">
        
     </div>
     

    

     
    </div>
  </div>

<script>

function loadTeams(department,getSelected){
	if(getSelected == 1){
		var team = "<?php echo $sTeamArr; ?>";
	}else{
		var team = "-1";
	}
    $.ajax({	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getTeamListByDepartment&department='+department+"&team="+team,
		dataType:'json',
		success:function(data){		  
			var success = data.success;
		    var error = data.error;
			if(success == 1){
			  var selectHtml = data.response;
			  $('#teams').html(selectHtml);
		   }else{
			 alert(error);
		   }
		}	  
	}); 
}
function loadManager(department,getSelected){
	if(getSelected == 1){
		var manager= "<?php echo $sManagerArr; ?>";
	}else{
		var manager = "-1";
	}
    $.ajax({	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getManagersByDepartment&department='+department+"&manager="+manager,
		dataType:'json',
		success:function(data){	  
			var success = data.success;
			var error = data.error;
			if(success == 1){
				var selectHtml = data.response;
				$('#managers').html(selectHtml);
			}else{
				alert(error);
			}
		}	  
	}); 
}

function resetElement() {

       var url = '<?php echo $CFG->wwwroot."/admin/user.php";?>';
		window.location.href = url;

}


jQuery(document).ready(function($) {
	
	$("#department").on('change', function() {
	
		var department = loopSelected('department');
		
		loadTeams(department,0);
		loadManager(department,0);
			
	});	
	$("#user_search").click(function() {

			var sel_mode = $("input[name=selector]:checked").val();
			if(sel_mode == undefined || sel_mode == 'undefined'){
				sel_mode = 0;
			}
			var url = '<?php echo $CFG->wwwroot;?>/admin/user.php?sel_mode='+sel_mode;
			var key = $('#key').val();
			var roles = loopSelected('user_roles');
			var is_active = loopSelected('is_active');
			if($.trim(key) != '') {
				url = url+'&key='+key;
			}
			if(roles != -1) {
				url = url+'&roles='+roles;
			}
			//if(is_active != -1) {
				url = url+'&is_active='+is_active;
			//}
			
			var job_title = loopSelected('job_title');
			var company = loopSelected('company');
                        var city = loopSelected('city');
				
			if(sel_mode == 2){
				var userGroup = loopSelected('user_group');

				if(userGroup != -1) {
					url = url+'&user_group='+userGroup;
				}
			}else{
				var department = loopSelected('department');
				var team = loopSelected('teams');
				var managers = loopSelected('managers');
				

				if(department != -1) {
					url = url+'&department='+department;
				}
				if(team != -1) {
					url = url+'&team='+team;
				}
				if(managers != -1) {
					url = url+'&managers='+managers;
				}
			}
			
			if(job_title != -1) {
					url = url+'&job_title='+job_title;
			}
                        
                        if(city != -1) {
			       url = url+'&city='+city;
			}
			
			if(company != -1) {
				url = url+'&company='+company;
			}
                        
                        var report_to = loopSelected('report_to');
                        if(report_to != -1) {
				url = url+'&report_to='+report_to;
			}
                        var is_manager_yes = loopSelected('is_manager_yes');
                        if(is_manager_yes != -1) {
				url = url+'&is_manager_yes='+is_manager_yes;
			}
			window.location.href = url;
	});
	$("input[name=selector]").change(function(){
		var sel_mode = $(this).val();
		if(sel_mode == 2){
			$("#user_group").show();

			$("#department").hide();
			$("#teams").hide();
			$("#managers").hide();
			//$("#filter-external-label").hide();
		}else{
			$("#department").show();
			$("#teams").show();
			$("#managers").show();
            //$("#filter-external-label").show();
			$("#user_group").hide();
		}
	});
});

function loopSelected(id) {
	var selectedArray = new Array();
	selectedArray[0] = '-1';
	if($('select#'+id).length) {
		$('select#'+id+' :selected').each(function(i, selected) {
			selectedArray[i] = $(selected).val();
		});
	}
	if($.inArray("-1", selectedArray) > -1) {
		return -1;
	} else {
		return selectedArray.join('@');
	}
}


var department = loopSelected('department');
loadTeams(department,1);
loadManager(department,1);
	   
 
</script>