<?php 
global $CFG, $DB;
$typeArr	= array('-1');
$catArr		= array('-1');
$criteriaArr		= array('-1');
$isElectiveGlobalArr = array();
$courseStatusArr = array('-1');
$publishedArr	= array('-1');

$categoryRecords = $DB->get_records_sql("SELECT c.id,c.name FROM mdl_course_categories as c WHERE c.is_active = 1 order by c.name ASC");
if(!empty($paramArray['course_type']))
	$typeArr	= explode('@',$paramArray['course_type']);
if(!empty($paramArray['course_cat']))
	$catArr		= explode('@',$paramArray['course_cat']);
//if(!empty($paramArray['course_publish']) || $paramArray['course_publish'] == 0)
if(!empty($paramArray['course_publish']))
	$publishedArr	= explode('@',$paramArray['course_publish']);
if(!empty($paramArray['criteria']))
	$criteriaArr	= explode('@',$paramArray['criteria']);
/*if(!empty($paramArray['is_elective_global'])  || $paramArray['is_elective_global'] == 0 )
	$isElectiveGlobalArr = explode('@',$paramArray['is_elective_global']);*/
if(!empty($paramArray['is_active'])  || $paramArray['is_active'] == 0)
	$courseStatusArr = explode('@',$paramArray['is_active']);			
	//echo count($typeArr);
	

$showCriteria = 0;

if(count($typeArr) == 0 || count($typeArr) == 2  || (count($typeArr) == 1 && in_array($CFG->courseTypeOnline, $typeArr)) ) {
  $showCriteria = 1;
} 
/*
$showGlobalElective = 0;
if($showCriteria == 1 && (count($criteriaArr) == 0 || count($criteriaArr) == 2  || (count($criteriaArr) == 1 && in_array(2, $criteriaArr)) )) {
  $showGlobalElective = 1;
} 
*/
?>
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/style/jquery-ui.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-ui.js"></script>
<div class="subs_reports_status_bar subsReport" id="report-search">
  
  <div class="charc-filter" >
    <div class="select_box_mul" >
		
        
        <select autocomplete="off" size="2" multiple="multiple" class="loadingtypes" id="course_type" name="course_type"  >
			<option value="-1" 
			<?php echo (in_array('-1', $typeArr))?'selected="selected"':''; ?> >
			<?php echo get_string('alltypes','course');?></option>

			<option value="<?php echo $CFG->courseTypeOnline; ?>" 
			<?php echo (in_array($CFG->courseTypeOnline, $typeArr))? 'selected="selected"':''; ?> >
			<?php echo get_string('onlinecourse');?></option>

			<option value="<?php echo $CFG->courseTypeClassroom; ?>" 
			<?php echo (in_array($CFG->courseTypeClassroom, $typeArr))?'selected="selected"':''; ?> >
			<?php echo get_string('classroomcourse');?></option>
		</select>
        
      
        <select autocomplete="off" size="2" multiple="multiple" class="loadingtypes" id="criteria" name="criteria" >
			<option value="-1" 
			<?php echo (in_array('-1', $criteriaArr))?'selected="selected"':''; ?> >
			<?php echo get_string('allcriterias','course');?></option>

			<option value="<?php echo $CFG->courseCriteriaCompliance; ?>" 
			<?php echo (in_array($CFG->courseCriteriaCompliance, $criteriaArr))? 'selected="selected"':''; ?> >
			<?php echo $CFG->courseCompliance;?></option>

			<option value="<?php echo $CFG->courseCriteriaElective; ?>" 
			<?php echo (in_array($CFG->courseCriteriaElective, $criteriaArr))?'selected="selected"':''; ?> >
			<?php echo $CFG->courseElective;?></option>
            
            <option value="<?php echo $CFG->courseCriteriaElectiveGlobal; ?>" 
			<?php echo (in_array($CFG->courseCriteriaElectiveGlobal, $criteriaArr))?'selected="selected"':''; ?> >
			<?php echo $CFG->courseGlobalElective;?></option>
            
		</select>

		<select autocomplete="off" size="2" multiple="multiple" class="" id="course_cat" name="course_cat">
		<option value="-1" <?php echo (in_array('-1', $catArr))?'selected="selected"':''; ?> ><?php echo get_string('allcategories','course');?></option>
		<?php
		if(!empty($categoryRecords)){
			foreach($categoryRecords as $categoryRecord){
				$selectedC = in_array($categoryRecord->id, $catArr)?'selected="selected"':'';
			?>
				 <option value="<?php echo $categoryRecord->id?>" <?php echo $selectedC;?> ><?php echo $categoryRecord->name;?></option>
			<?php
			}
		}
		?>
        </select>
		<select autocomplete="off" size="2" multiple="multiple" class="loadingteams" id="course_publish" name="course_publish" >
			<option value="-1" <?php echo (in_array('-1', $publishedArr))?'selected="selected"':''; ?> >
			<?php echo get_string('all','course');?></option>
			<option value="1"  <?php echo in_array('1', $publishedArr)?'selected="selected"':''; ?> >
			<?php echo get_string('published','course');?></option>
			<option value="0"  <?php echo in_array('0', $publishedArr)?'selected="selected"':''; ?> ><?php echo get_string('not_published','course');?></option>
		</select>
        
       <select autocomplete="off" size="2" multiple="multiple" class="" id="is_active" name="is_active" >
        <option value="-1" <?php echo in_array('-1', $courseStatusArr)?'selected="selected"':'';?> ><?php echo get_string('all','multicoursereport');?></option>
        <option value="1"  <?php echo in_array('1', $courseStatusArr)?'selected="selected"':'';?> ><?php echo get_string('statusactive','multicoursereport');?></option>
        <option value="0" <?php echo in_array('0', $courseStatusArr)?'selected="selected"':'';?>  ><?php echo get_string('statusdeactive','multicoursereport');?></option>
      </select>
      
    </div>
	<div>
        <input type="button" id="user_search" class="f-left" value="<?php echo get_string('go','user');?>" >
        &nbsp; <input type="button"  class="f-left" onclick="resetElement();" value="<?php echo get_string('reset','user');?>">
     </div>
     <div class="clear"></div>
    </div>
  </div>

<script>

function resetElement() {

       var url = '<?php echo $CFG->wwwroot."/course/index.php";?>';
		window.location.href = url;

}


jQuery(document).ready(function($) {


    if('<?php echo $showCriteria;?>' == 1){
	     $('#criteria').show();
		 
	}else{
	     $('#criteria').hide();
		 
	}
	
    $('#course_type').change(function(){
	
	   var course_type = $(this).val();
	   
	    if(course_type ==  '-1' || course_type ==  '1,2' || course_type ==  '-1,1,2' || course_type == '<?php echo $CFG->courseTypeOnline;?>'){
		  $('#criteria').show();
		  //$('#is_elective_global').show();
		}else if(course_type == '<?php echo $CFG->courseTypeClassroom;?>'){
		  $('#criteria').hide();
		  $('#criteria').val('-1');
		}
	
	});
	
	$("#user_search").click(function() {
		
			var criteria = loopSelected('criteria');
			var course_type = loopSelected('course_type');
			var course_cat = loopSelected('course_cat');
			var course_publish = loopSelected('course_publish');
			//var is_elective_global = loopSelected('is_elective_global');
			var is_active = loopSelected('is_active');
			var key = $('#key').val();
			
			var url = '<?php echo $CFG->wwwroot;?>/course/index.php?';
			
			if($.trim(key) != '') {
				url = url+'&key='+key;
			}
			if(criteria != -1) {
				url = url+'&criteria='+criteria;
			}
			if(course_type != -1) {
				url = url+'&course_type='+course_type;
			}
			if(course_cat != -1) {
				url = url+'&course_cat='+course_cat;
			}
			if(course_publish != -1) {
				url = url+'&course_publish='+course_publish;
			}
			/*if(is_elective_global != -1) {
				url = url+'&is_elective_global='+is_elective_global;
			}*/
			//if(is_active != -1) {
				url = url+'&is_active='+is_active;
			//}
			window.location.href = url;
	});
});

function loopSelected(id) {
	var selectedArray = new Array();
	selectedArray[0] = '-1';
	if($('#'+id).length) {
		$('#'+id+' :selected').each(function(i, selected) {
			selectedArray[i] = $(selected).val();
		});
	}
	if($.inArray("-1", selectedArray) > -1) {
		return -1;
	} else {
		return selectedArray.join('@');
	}
}
</script>