<?php 

global $CFG, $DB, $USER;
$isReport = true;
$dpArr = getDepartments('', $isReport);
$tArr = getTeams();

$dSelected = '';
$tSelected = '';
$uSelected = '';
$display = "";

$sLeadersNameArr = array('-1');
if($paramArray['report_to']!=''){
	$sLeadersNameArr = explode('@',$paramArray['report_to']);
}
if(!empty($paramArray['user_group'])){
	$sUserGroupArr	= explode('@',$paramArray['user_group']);
}
if($USER->archetype == $CFG->userTypeManager){
	$userGroups = array();
	$display = "style = 'display:none;'";
	$displayFilter1 = "style = 'display:none;'";
	$displaySelector = "style = 'display:none;'";
	$displayFilter2 = "style = 'display:block;'";
}else{
	$displayFilter1 = "style = 'display:none;'";
	$displaySelector = "style = 'display:block;'";
	$displayFilter2 = "style = 'display:block;'";

	$radioChecked1 = "checked";
	$radioChecked2 = "";
	if(!empty($_REQUEST['sel_mode']) && $_REQUEST['sel_mode'] == 2){
		$display = "style = 'display:none;'";
		$displayFilter1 = "style = 'display:block;'";
		$displayFilter2 = "style = 'display:none;'";

		$radioChecked1 = "";
		$radioChecked2 = "checked";
	}
	$userGroups = getUserGroups(true);
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/style/jquery-ui.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-ui.js"></script>

 <div class="subs_reports_status_bar subsReport"  id="report-search">
  
  <div class="charc-filter" >
	<?php /*?><div class = "field_selector" <?php echo $displaySelector;?> >
		<div class ="selector-1 selector-radio">
			<input type = "radio" name = "selector" value = "1" class = "sel_mode" <?php echo $radioChecked1;?> ><span><?php echo get_string("by_department"); ?></span>
		</div>
		<div class ="selector-2 selector-radio">
			<input type = "radio" name = "selector" value = "2" class = "sel_mode" <?php echo $radioChecked2;?> ><span><?php echo get_string("by_group"); ?></span>
		</div>
	</div><?php */?>
    <?php echo getRadioFilters($displaySelector, $radioChecked1, $radioChecked2);?>
    <div class="select_box_mul">
		<select autocomplete="off" size="2" multiple="multiple" class="" id="user_group" name="user_group" <?php echo $displayFilter1;?> >
		<option value="-1" <?php echo in_array('-1', $sUserGroupArr)?'selected="selected"':'';?> ><?php echo get_string('select_user_group');?></option>
		<?php
		if(!empty($userGroups)){
			foreach($userGroups as $userGroup){
				$selectedC = in_array($userGroup->id, $sUserGroupArr)?'selected="selected"':'';
			?>
				 <option value="<?php echo $userGroup->id?>" <?php echo $selectedC;?> ><?php echo $userGroup->name;?></option>
			<?php
			}
		}
		?>
        </select>
<?php /*?>
      <?php if(count($dpArr) > 0 ){?>
      <select autocomplete="off" size="2" multiple="multiple" class="" id="department" name="department" <?php echo $display;?>>
        <option value="-1" <?php echo $sDepartment == '-1'?'selected="selected"':'';?> ><?php echo get_string('alldepartments','multiuserreport');?></option>
        <?php foreach($dpArr as $dp ){
		       $dSelected = in_array($dp->id, $sDepartmentArr)?'selected="selected"':'';
		?>
        <option value="<?php echo $dp->id;?>"  <?php echo $dSelected;?> ><?php echo $dp->title;?></option>
        <?php } ?>
      </select>
      <?php } ?><?php */?>
      
       <?php echo createDepartmentSelectBoxForFilter('department', $dpArr, $sDepartmentArr, $display); ?>
 
      <?php /*if(count($tArr) > 0 ){?>
      <select autocomplete="off" size="2" multiple="multiple" class="" id="team" name="team" <?php echo $displayFilter2;?> >
        <option value="-1" <?php echo $sTeam == '-1'?'selected="selected"':'';?> ><?php echo get_string('allteams','multiuserreport');?></option>
        <?php foreach($tArr as $teams ){
		   
		   $tSelected = in_array($teams->id, $sTeamArr)?'selected="selected"':'';
		?>
        <option value="<?php echo $teams->id;?>" <?php echo $tSelected;?> ><?php echo $teams->name;?></option>
        <?php } ?>
      </select>
      <?php }*/ ?>
      
       <select autocomplete="off" size="2" multiple="multiple" class="loadingusers" id="team" name="team"  <?php echo $displayFilter2;?> >
        <option value=""><?php echo get_string('loadingteams','multiuserreport');?></option>
      </select>
      
        <?php 
                echo createSelectBoxFilterFor('job_title', 'job_title', $sJobTitleArr);
                echo createReportToSelectBoxForFilter('report_to', '', $sLeadersNameArr, $display);
                echo createIsManagerSelectBoxFilterFor('is_manager_yes', $filterBox.'is_manager_yes', $sIsManagerArr);
                echo createSelectBoxFilterFor('company', 'company', $sCompanyArr);
	?>
        
        
      
      <select autocomplete="off" size="2" multiple="multiple" class="loadingusers" id="user" name="user" >
        <option value=""><?php echo get_string('loadingusers','multiuserreport');?></option>
      </select>
       

      <select autocomplete="off" size="2" multiple="multiple" class="" id="type" name="type" >
        <option value="-1" <?php echo in_array('-1', $sTypeArr)?'selected="selected"':'';?> ><?php echo get_string('allstatus','multiuserreport');?></option>
        <option value="3"  <?php echo in_array('3', $sTypeArr)?'selected="selected"':'';?> ><?php echo get_string('notstarted','multiuserreport');?></option>
        <option value="1" <?php echo in_array('1', $sTypeArr)?'selected="selected"':'';?>  ><?php echo get_string('inprogress','multiuserreport');?></option>
        <option value="2" <?php echo in_array('2', $sTypeArr)?'selected="selected"':'';?>  ><?php echo get_string('completed','multiuserreport');?></option>
      </select>
    </div>
    
    <div class="date_sec">
      <div >
        <input type="button" id="report_allsubssription" class="f-left" value="<?php echo get_string('go','multiuserreport');?>" >
        &nbsp;&nbsp; <input type="button"  class="f-left" onclick="resetElement();" value="<?php echo get_string('reset','multiuserreport');?>" >
      </div>
    </div>    
    <div class="clear"></div>
  </div>
</div>

<script>

function resetElement() {
	var url = "<?php echo $CFG->wwwroot.$_SERVER['PHP_SELF'];?>";
	window.location.href = url;
}


function loadTeams(department){

    var team = '<?php echo $sTeam; ?>';
	$('#team').html('<option value=""><?php echo get_string('loadingteams','multiuserreport');?></option>');

    $.ajax({
	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getTeamListByDepartment&department='+department+"&team="+team,
		dataType:'json',
		success:function(data){
		  
		   var success = data.success;
		     var error = data.error;
			   if(success == 1){
			      var selectHtml = data.response;
				  $('#team').html(selectHtml);
				  var team = loopSelected('team');
				  var job_title = loopSelected('job_title');
		          var company = loopSelected('company');
				  loadUsers(department, team, job_title, company);
			   }else{
				 alert(error);
			   }
		}
	  
	  });
	  

}


function loadUsers(department, team, job_title, company){

    var user = '<?php echo $sUser;?>';
    $('#user').html('<option value=""><?php echo get_string('loadingusers','multiuserreport');?></option>');
    $.ajax({
	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getUserListByDnT&department='+department+"&team="+team+"&user="+user+"&job_title="+job_title+"&company="+company,
		dataType:'json',
		success:function(data){
		  
		   var success = data.success;
		     var error = data.error;
			   if(success == 1){
			      var selectHtml = data.response;
				  $('#user').html(selectHtml);
			   }else{
				 alert(error);
			   }
		}
	  
	  });
	  

}

jQuery(document).ready(function($) {

		
	$("#department").change(function() {
		var department = loopSelected('department');		
		loadTeams(department);
	});
	
	$("#team").change(function() {
		var department = loopSelected('department');
		var team = loopSelected('team');
		var job_title = loopSelected('job_title');
		var company = loopSelected('company');
		loadUsers(department, team, job_title, company);
			
	});
	$("#user_group").change(function() {
		var department = '-1';
		var team = loopSelected('user_group');
		var job_title = loopSelected('job_title');
		var company = loopSelected('company');
		loadUsers(department, team, job_title, company);
	});
	
	$("#job_title, #company").change(function() {
		var department = loopSelected('department');
		var sel_mode = $("input[name=selector]:checked").val(); 
		var team = sel_mode==1?loopSelected('team'):loopSelected('user_group');
		var job_title = loopSelected('job_title');
		var company = loopSelected('company');
		loadUsers(department, team, job_title, company);
	});
	
	$("#report_allsubssription").click(function() {
		var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
		var sdate = $('#start_date').val();
		var edate = $('#end_date').val();
		var firstDate = new Date(sdate);
		var secondDate = new Date(edate);
		var todayDate = new Date();
		var diffDays = Math.floor((secondDate.getTime() - firstDate.getTime()) / 86400000); // ms per day
		if (secondDate.getTime() < firstDate.getTime()) {
			alert('<?php echo get_string('startdatecannotbegreaterthanenddate', 'multiuserreport');?>');
			return false;
		} else {
			var user = loopSelected('user');
			
			var job_title = loopSelected('job_title');
			var company = loopSelected('company');
			var type = loopSelected('type');
			var sel_mode = $("input[name=selector]:checked").val();
			var url = '<?php echo $CFG->wwwroot.$_SERVER["PHP_SELF"];?>?sel_mode='+sel_mode;
			if(sel_mode == 2){
				var userGroup = loopSelected('user_group');
				if(userGroup != -1) {
					url = url+'&user_group='+userGroup;
				}
			}else{
				var department = loopSelected('department');
				var team = loopSelected('team');
				if(department != -1) {
					url = url+'&department='+department;
				}
				if(team != -1) {
					url = url+'&team='+team;
				}
			}
			if(user != -1) {
				url = url+'&user='+user;
			}
			
			if(job_title != -1) {
				url = url+'&job_title='+job_title;
			}                        
                       
                        var report_to = loopSelected('report_to');
                        if(report_to != -1) {
				url = url+'&report_to='+report_to;
			}
			var is_manager_yes = loopSelected('is_manager_yes');
                        if(is_manager_yes != -1) {
				url = url+'&is_manager_yes='+is_manager_yes;
			}
			if(company != -1) {
				url = url+'&company='+company;
			}
			
			if(type != -1) {
				url = url+'&type='+type;
			}
                        var key = $('#key').val();
                        if($.trim(key) != '') {
				url = url+'&key='+key;
			}
                        //alert(url);
			window.location.href = url;
		}
	});
	var old_start_date = "";
	$('#start_date').click(function(){
		old_start_date = $('#start_date').val();
	});
	$('#start_date').datepicker({
		changeMonth: true,
	      changeYear: true,
		dateFormat: 'mm/dd/yy',
		onSelect: function() {			
			var d1 = $('#start_date').datepicker('getDate');
			var d2 = $('#end_date').datepicker('getDate');
			var diff = 0;
			if (d1 && d2) {
				diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
			}
		}
	});
	var old_end_date = "";
	$('#end_date').click(function(){
		old_end_date = $('#end_date').val();
	});
	$('#end_date').datepicker({
		changeMonth: true,
	      changeYear: true,
		dateFormat: 'mm/dd/yy',
		onSelect: function(dateStr) {
			var startDate = new Date($("#start_date").val());
			var endDate = new Date($('#end_date').val());
			if(startDate > endDate){
				alert("<?php echo get_string('startdatecannotbegreaterthanenddate', 'multiuserreport');?>");
				$('#end_date').val(old_end_date);
			}
			
			var d1 = $('#start_date').datepicker('getDate');
			var d2 = $('#end_date').datepicker('getDate');
			var diff = 0;
			if (d1 && d2) {
				diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
			}
		}
	});
	
	
	$("input[name=selector]").change(function(){
		var sel_mode = $(this).val();
		$("#department").val('-1');
		$("#team").val('-1');
		$("#user_group").val('-1');
		if(sel_mode == 2){
			$("#user_group").show();

			$("#department").hide();
			$("#team").hide();
			 $(".filter-external-label").hide();
		}else{
			var department = loopSelected('department');
			loadTeams(department);
			$("#department").show();
			$("#team").show();

			$("#user_group").hide();
			 $(".filter-external-label").show();
		}
	});	
});

function loopSelected(id) {
	var selectedArray = new Array();
	selectedArray[0] = '-1';
	if($('#'+id).length) {
		$('#'+id+' :selected').each(function(i, selected) {
			selectedArray[i] = $(selected).val();
		});
	}
	if($.inArray("-1", selectedArray) > -1) {
		return -1;
	} else {
		return selectedArray.join('@');
	}
}
	<?php
		if(!empty($_REQUEST['sel_mode']) && $_REQUEST['sel_mode'] == 2){
		?>
			var department = '-1';
			var user_group = loopSelected('user_group');
			var job_title = loopSelected('job_title');
		    var company = loopSelected('company');
			loadUsers(department, user_group, job_title, company);
		<?php
		}else{
			if(isset($sDepartmentArr) || isset($sTeamArr)){?>
	
			var department = loopSelected('department');
				loadTeams(department);
			
	   <?php } 
		}
		?>
 
 </script>
