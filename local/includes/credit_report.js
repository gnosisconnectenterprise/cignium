
function resetElement() {

        var url = wwwroot+'/user/'+pagename;
		window.location.href = url;

}

function loadTeams(department){

    var team = sTeam;
	$('#team').html(loadingteams);

    $.ajax({
	  
	    url:wwwroot+'/local/ajax.php',
		type:'POST',
		data:'action=getTeamListByDepartment&department='+department+"&team="+team,
		dataType:'json',
		success:function(data){
		  
		   var success = data.success;
		     var error = data.error;
			   if(success == 1){
			      var selectHtml = data.response;
				  $('#team').html(selectHtml);
				  var team = loopSelected('team');
				  var job_title = loopSelected('job_title');
		          var company = loopSelected('company');
				  var userType = loopSelected('userType');
				  loadUsers(userType, department, team, job_title, company);
			   }else{
				 alert(error);
			   }
		}
	  
	  });
	  

}


function loadUsers(userType, department, team, job_title, company){

    
    
    var user = sUser;
	var userTypeInstructor = userTypeInstructor;
	var userType = loopSelected('userType');
	var checkInstructor = userType == userTypeStudent?0:1;
	var sel_mode = $("input[name=selector]:checked").val();

    $('#user').html(loadingusers);
    $.ajax({
	  
	    url:wwwroot+'/local/ajax.php',
		type:'POST',
		data:'action=getUserListByDnUserType&department='+department+"&team="+team+"&checkInstructor="+checkInstructor+"&user="+user+"&job_title="+job_title+"&company="+company+"&sel_mode="+sel_mode+"&userType="+userType,
		dataType:'json',
		success:function(data){
		 
		       var success = data.success;
		       var error = data.error;
			   if(success == 1){
			      var selectHtml = data.response;
				  $('#user').html(selectHtml);
			   }else{ 
				 alert(error);
			   }
		}
	  
	  });
	  

}

jQuery(document).ready(function($) {

		
	$("#department").change(function() {
	
		/*var department = loopSelected('department');
		var userType = loopSelected('userType');
		var job_title = loopSelected('job_title');
		var company = loopSelected('company');
		loadUsers(department, userType, job_title, company);*/
		var department = loopSelected('department');
		loadTeams(department);
	});
	
	
	$("#userType, #team, #user_group, #job_title, #company").change(function() {
	
	   
	    var userType = loopSelected('userType');
		var department = loopSelected('department');
		var sel_mode = $("input[name=selector]:checked").val(); 
		var team = sel_mode==2?loopSelected('user_group'):loopSelected('team');
		var job_title = loopSelected('job_title');
		var company = loopSelected('company');
		
		loadUsers(userType, department, team, job_title, company);
	});
	
	$("input[name=selector]").change(function(){
		var sel_mode = $(this).val();
		$("#department").val('-1');
		$("#team").val('-1');
		$("#user_group").val('-1');
		if(sel_mode == 2){
			$("#user_group").show();
			$("#department").hide();
			$("#team").hide();
			 $(".filter-external-label").hide();
		}else{
			var department = loopSelected('department');
			loadTeams(department);
			$("#department").show();
			$("#team").show();
			$("#user_group").hide();
			$(".filter-external-label").show();
		}
	});
	
	
	$("#report_allsubssription").click(function() {
		//validate daterange
		var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
		var sdate = $('#start_date').val();
		var edate = $('#end_date').val();
		var firstDate = new Date(sdate);
		var secondDate = new Date(edate);
		var todayDate = new Date();
		//var diffDays = Math.round(Math.abs((secondDate.getTime() - firstDate.getTime())/(oneDay)));
		var diffDays = Math.floor((secondDate.getTime() - firstDate.getTime()) / 86400000); // ms per day

		/*if(firstDate.getTime() > todayDate.getTime()) {
			alert('<?php //echo get_string('startdatecannotbegreaterthantodaydate', 'multiuserreport');?>');
			return false;
		} else if(secondDate.getTime() > todayDate.getTime()) {
			alert('<?php //echo get_string('enddatecannotbegreaterthantodaydate', 'multiuserreport');?>');
			return false;
		} else*/ if (secondDate.getTime() < firstDate.getTime()) {
			alert(startdatecannotbegreaterthanenddate);
			return false;
		/*} else if(diffDays>365) {
			alert('<?php //echo get_string('maxperiodisoneyear', 'multiuserreport');?>');
			return false;*/
		} else {

			var startDate = $('#start_date').val();
			startDate = startDate.replace(/\//g, '-');			
			var endDate = $('#end_date').val();
			endDate = endDate.replace(/\//g, "-");
			
			var userType = loopSelected('userType');
			var user = loopSelected('user');
			
			var job_title = loopSelected('job_title');
			var company = loopSelected('company');
			
			//var type = loopSelected('type');
			
			var sel_mode = $("input[name=selector]:checked").val();
			
			var url = wwwroot+'/user/'+pagename+'?startDate='+startDate+'&endDate='+endDate+'&sel_mode='+sel_mode;
			
			
			if(sel_mode == 2){
				var userGroup = loopSelected('user_group');
				if(userGroup != -1) {
					url = url+'&user_group='+userGroup;
				}
			}else{
				var department = loopSelected('department');
				var team = loopSelected('team');
				if(department != -1) {
					url = url+'&department='+department;
				}
				if(team != -1) {
					url = url+'&team='+team;
				}
			}
			
		
			if(user != -1) {
				url = url+'&user='+user;
			}
			if(userType != -1) {
				url = url+'&userType='+userType;
			}
			
			if(job_title != -1) {
				url = url+'&job_title='+job_title;
			}
			
			if(company != -1) {
				url = url+'&company='+company;
			}
			
			//if(type != -1) {
				//url = url+'&type='+type;
			//}
			
			
			window.location.href = url;
		}
	});
	var old_start_date = "";
	$('#start_date').click(function(){
		old_start_date = $('#start_date').val();
	});
	$('#start_date').datepicker({
		changeMonth: true,
	      changeYear: true,
		dateFormat: 'mm/dd/yy',
		onSelect: function() {			
			var d1 = $('#start_date').datepicker('getDate');
			var d2 = $('#end_date').datepicker('getDate');
			var diff = 0;
			if (d1 && d2) {
				diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
			}
			/*if(diff > 365) {
				alert("<?php //echo get_string('maxperiodisoneyear', 'multiuserreport');?>");
				$('#start_date').val(old_start_date);
			}*/
		}
	});
	var old_end_date = "";
	$('#end_date').click(function(){
		old_end_date = $('#end_date').val();
	});
	$('#end_date').datepicker({
		changeMonth: true,
	      changeYear: true,
		dateFormat: 'mm/dd/yy',
		onSelect: function(dateStr) {
			var startDate = new Date($("#start_date").val());
			var endDate = new Date($('#end_date').val());
			if(startDate > endDate){
				alert(startdatecannotbegreaterthanenddate);
				$('#end_date').val(old_end_date);
			}
			
			var d1 = $('#start_date').datepicker('getDate');
			var d2 = $('#end_date').datepicker('getDate');
			var diff = 0;
			if (d1 && d2) {
				diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
			}
			/*if(diff > 365) {
				alert("<?php //echo get_string('maxperiodisoneyear', 'multiuserreport');?>");
				$('#end_date').val(old_end_date);
			}*/
		}
	});
	
	
});

function loopSelected(id) {
	var selectedArray = new Array();
	selectedArray[0] = '-1';
	if($('#'+id).length) {
		$('#'+id+' :selected').each(function(i, selected) {
			selectedArray[i] = $(selected).val();
		});
	}
	//if(selectedArray.indexOf("-1") > -1) {
	if($.inArray("-1", selectedArray) > -1) {
		return -1;
	} else {
		return selectedArray.join('@');
	}
}
