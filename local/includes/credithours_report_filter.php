<?php 

global $CFG, $DB;
$dpArr = getDepartments('', true);
$tArr = getTeams();

$dSelected = '';
$tSelected = '';
$uSelected = '';
//$uTypeSelected = ($sUserType && in_array($sUserType, array($CFG->userTypeStudent, $CFG->userTypeInstructor, '-1')))?$sUserType:$CFG->userTypeStudent;


$display = "";
if(!empty($paramArray['user_group'])){
	$sUserGroupArr	= explode('@',$paramArray['user_group']);
}

$sLeadersNameArr = array('-1');
if($paramArray['report_to']!=''){
	$sLeadersNameArr = explode('@',$paramArray['report_to']);
}
if($USER->archetype != $CFG->userTypeAdmin){
	$userGroups = array();
	$display = "style = 'display:none;'";
	$displayFilter1 = "style = 'display:none;'";
	$displaySelector = "style = 'display:none;'";
	$displayFilter2 = "style = 'display:block;'";
}else{
	$displayFilter1 = "style = 'display:none;'";
	$displaySelector = "style = 'display:block;'";
	$displayFilter2 = "style = 'display:block;'";

	$radioChecked1 = "checked";
	$radioChecked2 = "";
	if(!empty($_REQUEST['sel_mode']) && $_REQUEST['sel_mode'] == 2){
		$display = "style = 'display:none;'";
		$displayFilter1 = "style = 'display:block;'";
		$displayFilter2 = "style = 'display:none;'";

		$radioChecked1 = "";
		$radioChecked2 = "checked";
	}
	$userGroups = getUserGroups(true);
}


?>
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/style/jquery-ui.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-ui.js"></script>
<div class="subs_reports_status_bar subsReport"  id="report-search">
  
  <div class="charc-filter" >
    
    <?php echo getRadioFilters($displaySelector, $radioChecked1, $radioChecked2);?>
    <div class="select_box_mul" style="float:left; ">
       
      <select autocomplete="off" size="2"  multiple="multiple" class="f-left" id="userType" name="userType" >
        <option value="-1" <?php echo in_array('-1', $sUserTypeArr)?'selected="selected"':'';?> ><?php echo get_string('allusertype');?></option>
       <option value="<?php echo $CFG->userTypeStudent;?>" <?php echo in_array($CFG->userTypeStudent, $sUserTypeArr)?'selected="selected"':'';?> ><?php echo ucfirst($CFG->userTypeStudent);?></option>
       <option value="<?php echo $CFG->userTypeInstructor;?>" <?php echo in_array($CFG->userTypeInstructor, $sUserTypeArr)?'selected="selected"':'';?> ><?php echo ucfirst($CFG->userTypeInstructor);?></option>
      </select>
      
            
     <select autocomplete="off" size="2" multiple="multiple" class="" id="user_group" name="user_group" <?php echo $displayFilter1;?> >
		<option value="-1" <?php echo in_array('-1', $sUserGroupArr)?'selected="selected"':'';?> ><?php echo get_string('select_user_group');?></option>
		<?php
		if(!empty($userGroups)){
			foreach($userGroups as $userGroup){
				$selectedC = in_array($userGroup->id, $sUserGroupArr)?'selected="selected"':'';
			?>
				 <option value="<?php echo $userGroup->id?>" <?php echo $selectedC;?> ><?php echo $userGroup->name;?></option>
			<?php
			}
		}
		?>
        </select>
      
        <?php
	
		 if(count($dpArr) > 0 && $USER->archetype == $CFG->userTypeAdmin){
		  echo createDepartmentSelectBoxForFilter('department',  $dpArr, $sDepartmentArr, $display, true);
		 }
	  ?>
      
      
       <select autocomplete="off" size="2" multiple="multiple" class="loadingusers" id="team" name="team"  <?php echo $displayFilter2;?> >
        <option value=""><?php echo get_string('loadingteams','multiuserreport');?></option>
      </select>
      
        <?php 
		   echo createSelectBoxFilterFor('job_title', 'job_title', $sJobTitleArr);
                   echo createReportToSelectBoxForFilter('report_to', '', $sLeadersNameArr, $display);
                   echo createIsManagerSelectBoxFilterFor('is_manager_yes', $filterBox.'is_manager_yes', $sIsManagerYesArr);
		   echo createSelectBoxFilterFor('company', 'company', $sCompanyArr);
		?>
      
      
      
      <select autocomplete="off" size="2" multiple="multiple" class="loadingusers" id="user" name="user" >
        <option value=""><?php echo get_string('loadingusers','multiuserreport');?></option>
      </select>
      
      
      

   </div>
    

   <div class="date_sec">
      <div class="startenddate"><span><?php echo get_string('credithourscomletiondate');?></span>
           <span class="fdate_selector">
             <input type="text" id="start_date" value="<?php echo $sDateSelected;?>" name="start_date" readonly = "true">
           </span>
      </div>
      <div class="startenddate"><span><?php echo get_string('daterangeto','multiuserreport');?></span>
           <span class="fdate_selector">
              <input type="text" id="end_date" value="<?php echo $eDateSelected;?>" name="end_date" readonly = "true">
           </span>
      </div>
      <div >
        <input type="button" id="report_allsubssription" class="f-left" value="<?php echo get_string('go','multiuserreport');?>" >
        &nbsp;&nbsp; <input type="button"  class="f-left" onclick="resetElement();" value="<?php echo get_string('reset','multiuserreport');?>" >
      </div>
    </div>
    
    
    <div class="clear"></div>
  </div>
</div>

<script>
	var wwwroot = '<?php echo $CFG->wwwroot;?>';
	var pagename = 'credithours_report.php';
	var sTeam = '<?php echo $sTeam;?>';
	var loadingteams = '<option value=""><?php echo get_string('loadingteams','multiuserreport');?></option>';
	var sUser = '<?php echo $sUser;?>';
	var userTypeInstructor = '<?php echo $CFG->userTypeInstructor;?>';
	var userTypeStudent = '<?php echo $CFG->userTypeStudent;?>';
	var loadingusers = '<option value=""><?php echo get_string('loadingusers','multiuserreport');?></option>';
	var startdatecannotbegreaterthanenddate = '<?php echo get_string('startdatecannotbegreaterthanenddate', 'multiuserreport');?>';
</script>

<script src="<?php echo $CFG->wwwroot;?>/local/includes/credit_report_filter.js" type="text/javascript"></script>

<script>

	<?php
		if(!empty($_REQUEST['sel_mode']) && $_REQUEST['sel_mode'] == 2){
		?>
		
			var department = loopSelected('department');
			var team = loopSelected('team');
			var userType = loopSelected('userType');
			var job_title = loopSelected('job_title');
			var company = loopSelected('company');
			loadUsers(userType, department, team, job_title, company);
		<?php
		}else{
			if(isset($sDepartmentArr) || isset($sTeamArr)){?>
	
				var department = loopSelected('department');
				loadTeams(department);
			
	   <?php } 
		}
	 ?>
		

 </script>
