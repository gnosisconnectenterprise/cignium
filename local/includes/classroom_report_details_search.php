<?php 
global $CFG, $DB;
$isReport = true;


$classesOfClassroom = getClassesOfClassroom($cid);

?>
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/style/jquery-ui.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-ui.js"></script>
<div class="subs_reports_status_bar subsReport" id="report-search">
  
  <div class="charc-filter" >
    <div class="select_box_mul" >       

		<select autocomplete="off" size="2"  class="" id="classname" name="classname" >
		<optgroup label="<?php echo get_string('selectclass');?>">
		<?php
		if(isset($classesOfClassroom) && count($classesOfClassroom) > 0 ){
		 foreach($classesOfClassroom as $clsId=>$clsArr){

			$selectedClass = "";
			if($classId == $clsId){
			  $selectedClass = "selected";
			}

		?>
             <option value="<?php echo $clsId;?>" <?php echo $selectedClass;?> ><?php echo $clsArr->name;?></option>
		<?php
		}}
		?>
		</optgroup>
        </select>

            
      <?php /*?><select autocomplete="off" size="2" multiple="multiple" class="" id="type" name="type" >
        <option value="-1" <?php echo in_array('-1', $sTypeArr)?'selected="selected"':'';?> ><?php echo get_string('all','classroomreport');?></option>
        <option value="4" <?php echo in_array('4', $sTypeArr)?'selected="selected"':'';?>  ><?php echo get_string('classattended','classroomreport');?></option>
        <option value="3" <?php echo in_array('3', $sTypeArr)?'selected="selected"':'';?>  ><?php echo get_string('classnotattended','classroomreport');?></option>
        <option value="2" <?php echo in_array('2', $sTypeArr)?'selected="selected"':'';?>  ><?php echo get_string('classcompleted','classroomreport');?></option>
        <option value="1" <?php echo in_array('1', $sTypeArr)?'selected="selected"':'';?>  ><?php echo get_string('classincomplete','classroomreport');?></option>
        <!--option value="5" <?php echo in_array('5', $sTypeArr)?'selected="selected"':'';?>  ><?php echo get_string('classnoshow','classroomreport');?></option-->
      </select><?php */?>
      
      
       <select autocomplete="off" size="2" multiple="multiple" class="" id="type" name="type" >
        <option value="-1" <?php echo in_array('-1', $sTypeArr)?'selected="selected"':'';?> ><?php echo get_string('allstatus','classroomreport');?></option>
        <option value="2" <?php echo in_array('2', $sTypeArr)?'selected="selected"':'';?>  ><?php echo get_string('classnoshow','classroomreport');?></option>
        <option value="0" <?php echo in_array('0', $sTypeArr)?'selected="selected"':'';?>  ><?php echo get_string('inprogress','classroomreport');?></option>
        <option value="1" <?php echo in_array('1', $sTypeArr)?'selected="selected"':'';?>  ><?php echo get_string('completed','classroomreport');?></option>
      </select>
      
         
    </div>
    

    <div class="date_sec">
      <?php /*?><div class="startenddate"><span><?php echo get_string('daterangefrom','classroomreport');?></span>
           <span class="fdate_selector">
             <input type="text" id="start_date" value="<?php echo $sDateSelected;?>" name="start_date" readonly = "true">
           </span>
      </div>
      <div class="startenddate"><span><?php echo get_string('daterangeto','classroomreport');?></span>
           <span class="fdate_selector">
              <input type="text" id="end_date" value="<?php echo $eDateSelected;?>" name="end_date" readonly = "true">
           </span>
      </div><?php */?>
      <div >
        <input type="button" id="report_allsubssription" class="f-left" value="<?php echo get_string('go','classroomreport');?>" >
        &nbsp; <input type="button"  class="f-left" onclick="resetElement();" value="<?php echo get_string('reset','classroomreport');?>">
      </div>
    </div>
    
    <?php if($is_class_submitted == 0 && $userCount ){?>
    <div class="date_sec margin_top">
    <?php echo getAstricHtml(get_string('reporthasnotbeensubmittedyet','classroomreport')); ?>
    </div>
    <?php } ?>
    
  </div>
</div>

<script>


function resetElement() {

   var url = '<?php echo $CFG->wwwroot.$pageURL;?>?cid=<?php echo $cid;?>&class_id=<?php echo $classId;?>';
   
   window.location.href = url;

}


jQuery(document).ready(function($) {

	
	$("#report_allsubssription").on('click', function() {
		/*validate daterange*/
		/*var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
		var sdate = $('#start_date').val();
		var edate = $('#end_date').val();
		var firstDate = new Date(sdate);
		var secondDate = new Date(edate);
		var todayDate = new Date();
		var diffDays = Math.floor((secondDate.getTime() - firstDate.getTime()) / 86400000); // ms per day

		if (secondDate.getTime() < firstDate.getTime()) {
			alert('<?php echo get_string('startdatecannotbegreaterthanenddate', 'multicoursereport');?>');
			return false;
		} else {

			var startDate = $('#start_date').val();
			startDate = startDate.replace(/\//g, '-');			
			var endDate = $('#end_date').val();
			endDate = endDate.replace(/\//g, "-");
			*/

			var class_id = loopSelected('classname');
			var type = loopSelected('type');
			
			var url = '<?php echo $CFG->wwwroot.$pageURL;?>?cid=<?php echo $cid;?>&class_id='+class_id;
			
			if(type != -1) {
				url = url+'&type='+type;
			}
			
			window.location.href = url;
		/*}*/
	});
	/*var old_start_date = "";
	$('#start_date').click(function(){
		old_start_date = $('#start_date').val();
	});
	$('#start_date').datepicker({
		changeMonth: true,
	      changeYear: true,
		dateFormat: 'mm/dd/yy',
		onSelect: function() {			
			var d1 = $('#start_date').datepicker('getDate');
			var d2 = $('#end_date').datepicker('getDate');
			var diff = 0;
			if (d1 && d2) {
				diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
			}
			//if(diff > 365) {
			//	alert("<?php //echo get_string('maxperiodisoneyear', 'classroomreport');?>");
				//$('#start_date').val(old_start_date);
			//}
		}
	});
	var old_end_date = "";
	$('#end_date').click(function(){
		old_end_date = $('#end_date').val();
	});
	$('#end_date').datepicker({
		changeMonth: true,
	      changeYear: true,
		dateFormat: 'mm/dd/yy',
		onSelect: function(dateStr) {
			var startDate = new Date($("#start_date").val());
			var endDate = new Date($('#end_date').val());
			if(startDate > endDate){
				
				alert("<?php echo get_string('startdatecannotbegreaterthanenddate', 'multicoursereport');?>");
				$('#end_date').val(old_end_date);
			}
			
			var d1 = $('#start_date').datepicker('getDate');
			var d2 = $('#end_date').datepicker('getDate');
			var diff = 0;
			if (d1 && d2) {
				diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
			}
			//if(diff > 365) {
				//alert("<?php //echo get_string('maxperiodisoneyear', 'classroomreport');?>");
				//$('#end_date').val(old_end_date);
			//}
		}
	});*/
	
});

function loopSelected(id) {
	var selectedArray = new Array();
	selectedArray[0] = '-1';
	if($('#'+id).length) {
		$('#'+id+' :selected').each(function(i, selected) {
			selectedArray[i] = $(selected).val();
		});
	}
	//if(selectedArray.indexOf("-1") > -1) {
	if($.inArray("-1", selectedArray) > -1) {
		return -1;
	} else {
		return selectedArray.join('@');
	}
}
	

</script>