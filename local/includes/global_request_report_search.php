<?php 
global $CFG, $DB,$USER;
//$pgArr = getPrograms();
$isReport = true;
$dpArr = getDepartments('', $isReport);
$tArr = getTeams();


if(!empty($paramArray['user_group'])){
	$sUserGroupArr	= explode('@',$paramArray['user_group']);
}
if($USER->archetype == $CFG->userTypeManager){
	$userGroups = array();
}else{
	$display = "";
	$userGroups = getUserGroups(true);
}

if($USER->archetype == $CFG->userTypeManager){
	$display = "style = 'display:none;'";
	$displayFilter1 = "style = 'display:none;'";
	$displaySelector = "style = 'display:none;'";
	$displayFilter2 = "style = 'display:block;'";
}else{
	$display = "style = 'display:block;'";
	$displayFilter1 = "style = 'display:none;'";
	$displaySelector = "style = 'display:block;'";
	$displayFilter2 = "style = 'display:block;'";

	$radioChecked1 = "checked";
	$radioChecked2 = "";
	if(!empty($_REQUEST['sel_mode']) && $_REQUEST['sel_mode'] == 2){
		$display = "style = 'display:none;'";
		$displayFilter1 = "style = 'display:block;'";
		$displayFilter2 = "style = 'display:none;'";

		$radioChecked1 = "";
		$radioChecked2 = "checked";
	}
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/style/jquery-ui.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-ui.js"></script>
<div class="subs_reports_status_bar subsReport" id="report-search">
  
  <div class="charc-filter" >
	<?php /*?><div class = "field_selector" <?php echo $displaySelector;?> >
		<div class ="selector-1 selector-radio">
			<input type = "radio" name = "selector" value = "1" class = "sel_mode" <?php echo $radioChecked1;?> ><span><?php echo get_string("by_department"); ?></span>
		</div>
		<div class ="selector-2 selector-radio">
			<input type = "radio" name = "selector" value = "2" class = "sel_mode" <?php echo $radioChecked2;?> ><span><?php echo get_string("by_group"); ?></span>
		</div>
	</div><?php */?>
    <?php echo getRadioFilters($displaySelector, $radioChecked1, $radioChecked2);?>
    <div class="select_box_mul" >
		<select autocomplete="off" size="2" multiple="multiple" class="" id="user_group" name="user_group" <?php echo $displayFilter1;?> >
			<option value="-1" <?php echo $sUserGroup == '-1'?'selected="selected"':'';?> ><?php echo get_string('select_user_group');?></option>
			<?php
			if(!empty($userGroups)){
				foreach($userGroups as $usersGroup){
					$selectedC = in_array($usersGroup->id, $sUserGroupArr)?'selected="selected"':'';
				?>
					 <option value="<?php echo $usersGroup->id?>" <?php echo $selectedC;?> ><?php echo $usersGroup->name;?></option>
				<?php
				}
			}
			?>
        </select>
   <?php /*?>   <?php if(count($dpArr) > 0 ){?>
      <select autocomplete="off" size="2" multiple="multiple" class="" id="department" name="department" <?php echo $display;?>>
        <option value="-1" <?php echo $sDepartment == '-1'?'selected="selected"':'';?> ><?php echo get_string('alldepartments','multicoursereport');?></option>
        <?php foreach($dpArr as $dp ){
		       $dSelected = in_array($dp->id, $sDepartmentArr)?'selected="selected"':'';
		?>
        <option value="<?php echo $dp->id;?>"  <?php echo $dSelected;?> ><?php echo $dp->title;?></option>
        <?php } ?>
      </select>
      <?php } ?><?php */?>
      
      <?php  if(count($dpArr) > 0 ){
	       echo createDepartmentSelectBoxForFilter('department',  $dpArr, $sDepartmentArr, $display, true);
	 }?>

      
      <select autocomplete="off" size="2" multiple="multiple" class="loadingusers" id="team" name="team" <?php echo $displayFilter2;?>>
        <option value=""><?php echo get_string('loadingteams','multiuserreport');?></option>
      </select>
	 
      
		<select autocomplete="off" size="2" multiple="multiple" class="" id="program" name="program" >
            <option value=""><?php echo get_string('loadingprograms','multiuserreport');?></option>
        </select>
        
		<select autocomplete="off" size="2" multiple="multiple" class="" id="course" name="course" >
             <option value=""><?php echo get_string('loadingcourses','multiuserreport');?></option>
        </select>
        
        <select autocomplete="off" size="2"  class="loadingclasses" id="classes" name="classes" >
			<option value=""><?php echo get_string('loadingclasses','classroomreport');?></option>
		</select>
        
        <?php 
		   echo createSelectBoxFilterFor('job_title', 'job_title', $sJobTitleArr);
		   echo createSelectBoxFilterFor('company', 'company', $sCompanyArr);
		?>
        
       <select autocomplete="off" size="2" multiple="multiple" class="loadingusers" id="user" name="user" >
        <option value=""><?php echo get_string('loadingusers','multiuserreport');?></option>
      </select>
      
      <select autocomplete="off" size="2" multiple="multiple" class="" id="type" name="type" >
        <option value="-1" <?php echo in_array('-1', $sTypeArr)?'selected="selected"':'';?> ><?php echo get_string('allstatus','multicoursereport');?></option>
        <option value="1" <?php echo in_array('1', $sTypeArr)?'selected="selected"':'';?>  ><?php echo get_string('openrequestreport');?></option>
        <option value="2" <?php echo in_array('2', $sTypeArr)?'selected="selected"':'';?>  ><?php echo get_string('accept_request_closed');?></option>
        <option value="3"  <?php echo in_array('3', $sTypeArr)?'selected="selected"':'';?> ><?php echo get_string('decline_request_closed');?></option>

      </select>
    </div>
    

    <div class="date_sec">
      <div class="startenddate"><span><?php echo get_string('daterangefrom','multicoursereport');?></span>
           <span class="fdate_selector">
             <input type="text" id="start_date" value="<?php echo $sDateSelected;?>" name="start_date" readonly = "true">
           </span>
      </div>
      <div class="startenddate"><span><?php echo get_string('daterangeto','multicoursereport');?></span>
           <span class="fdate_selector">
              <input type="text" id="end_date" value="<?php echo $eDateSelected;?>" name="end_date" readonly = "true">
           </span>
      </div>
      <div >
        <input type="button" id="report_allsubssription" class="f-left" value="<?php echo get_string('go','multicoursereport');?>" >
        &nbsp; <input type="button"  class="f-left" onclick="resetElement();" value="<?php echo get_string('reset','multicoursereport');?>" >
      </div>
    </div>
    
  </div>
</div>

<script>


function loadTeams(department){

    var team = '<?php echo $sTeam;?>';
	$('#team').html('<option value=""><?php echo get_string('loadingteams','multiuserreport');?></option>');

    $.ajax({
	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getTeamListByDepartment&department='+department+"&team="+team,
		dataType:'json',
		success:function(data){
		  
		   var success = data.success;
		     var error = data.error;
			   if(success == 1){
			      var selectHtml = data.response;
				  $('#team').html(selectHtml);
				  var department = loopSelected('department');
				  var team = loopSelected('team');
				  loadPrograms(department, team);
				  var job_title = loopSelected('job_title');
		          var company = loopSelected('company');
				  loadUsers(department, team, job_title, company);
				 
				  
			   }else{
				 alert(error);
			   }
		}
	  
	  });
	  

}



function loadCourses(department, team, program){
    var course = '<?php echo $sCourse;?>';
    $('#course').html('<option value=""><?php echo get_string('loadingcourses','multiuserreport');?></option>');
    $.ajax({	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getCourseNClassroomListByDnT&department='+department+"&program="+program+"&team="+team+"&course="+course,
		dataType:'json',
		success:function(data){		  
			var success = data.success;
			var error = data.error;
			if(success == 1){
				var selectHtml = data.response;
				$('#course').html(selectHtml);
				loadClasses(course);
			}else{
				alert(error);
			}
		}	  
	}); 
}

function loadClasses(courses){
	var classes = '<?php echo $sClasses;?>';
    $('#classes').html('<option value=""><?php echo get_string('loadingclasses','classroomreport');?></option>');
    $.ajax({	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getClasses&courses='+courses+'&classes='+classes,
		dataType:'json',
		success:function(data){	  
			var success = data.success;
			var error = data.error;
			if(success == 1){
				var selectHtml = data.response;
				$('#classes').html(selectHtml);
			}else{
				alert(error);
			}
		}	  
	}); 
}

function loadPrograms(department, team){
    //var course = '<?php echo $sCourse;?>';
	var program = '<?php echo $sProgram;?>';
	var sel_mode = 1;
	$('#program').html('<option value=""><?php echo get_string('loadingprograms','multiuserreport');?></option>');
    $.ajax({	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getProgramListByDnT&department='+department+"&team="+team+"&program="+program,
		dataType:'json',
		success:function(data){		  
			var success = data.success;
			var error = data.error;
			if(success == 1){
				var selectHtml = data.response;
				$('#program').html(selectHtml);
				var program = loopSelected('program');
				<?php if($USER->archetype == $CFG->userTypeAdmin){?>
				    sel_mode = $("input[name=selector]:checked").val();
				<?php } ?>
				if(sel_mode == 2){
					var department = '-1';
					var user_group = loopSelected('user_group');
					loadCourses(department, user_group, program);
				}else{
					var department = loopSelected('department');
					var team = loopSelected('team');
					loadCourses(department, team, program);
				}
			}else{
				alert(error);
			}
		}	  
	}); 
}


function loadUsers(department, team, job_title, company){

    var user = '<?php echo $sUser;?>';
    $('#user').html('<option value=""><?php echo get_string('loadingusers','multiuserreport');?></option>');
    $.ajax({
	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getUserListByDnT&department='+department+"&team="+team+"&user="+user+"&job_title="+job_title+"&company="+company,
		dataType:'json',
		success:function(data){
		  
		   var success = data.success;
		     var error = data.error;
			   if(success == 1){
			      var selectHtml = data.response;
				  $('#user').html(selectHtml);
			   }else{
				 alert(error);
			   }
		}
	  
	  });
	  

}

function resetElement() {
	
       var url = '<?php echo $CFG->wwwroot.'/my/'.$CFG->pageGlobalRequestReport;?>';
		window.location.href = url;
}



jQuery(document).ready(function($) {
		
	$("#department").on('change', function() {
	
		var department = loopSelected('department');
		//var team = loopSelected('team');
		//var program = loopSelected('program');
		//loadPrograms(department, team);
		loadTeams(department);
	
			
	});
	
	$("#team").on('change', function() {
	
		var department = loopSelected('department');
		var team = loopSelected('team');
		//var program = loopSelected('program');
		loadPrograms(department, team);
		
		var job_title = loopSelected('job_title');
		var company = loopSelected('company');
		
		loadUsers(department, team, job_title, company);
			
	});
	$("#user_group").change(function() {
	
		var department = '-1';
		var team = loopSelected('user_group');
		
		loadPrograms(department, team);
		
		var job_title = loopSelected('job_title');
		var company = loopSelected('company');
		
		loadUsers(department, team, job_title, company);
			
	});
	
	$("#job_title, #company").change(function() {
	
		var department = loopSelected('department');
		var sel_mode = $("input[name=selector]:checked").val(); 
		var team = sel_mode==1?loopSelected('team'):loopSelected('user_group');
		var job_title = loopSelected('job_title');
		var company = loopSelected('company');
		
		loadUsers(department, team, job_title, company);
			
	});
	
	
	$("#program").on('change', function() {

		var program = loopSelected('program');
		var sel_mode = 1;
		<?php if($USER->archetype == $CFG->userTypeAdmin){?>
			sel_mode = $("input[name=selector]:checked").val();
		<?php } ?>
		if(sel_mode == 2){
			var department = '-1';
			var user_group = loopSelected('user_group');
			loadCourses(department, user_group, program);
		}else{
			var department = loopSelected('department');
			var team = loopSelected('team');
			loadCourses(department, team, program);
		}
			
	});
	
	$("#course").on('change', function() {
		var course = loopSelected('course');
		loadClasses(course);
	});
	
	
	$("#report_allsubssription").on('click', function() {
		/*validate daterange*/
		var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
		var sdate = $('#start_date').val();
		var edate = $('#end_date').val();
		var firstDate = new Date(sdate);
		var secondDate = new Date(edate);
		var todayDate = new Date();
		//var diffDays = Math.round(Math.abs((secondDate.getTime() - firstDate.getTime())/(oneDay)));
		var diffDays = Math.floor((secondDate.getTime() - firstDate.getTime()) / 86400000); // ms per day

		/*if(firstDate.getTime() > todayDate.getTime()) {
			alert('<?php //echo get_string('startdatecannotbegreaterthantodaydate', 'multicoursereport');?>');
			return false;
		}else if(secondDate.getTime() > todayDate.getTime()) {
			alert('<?php //echo get_string('enddatecannotbegreaterthantodaydate', 'multicoursereport');?>');
			return false;
		} else */ if (secondDate.getTime() < firstDate.getTime()) {
			alert('<?php echo get_string('startdatecannotbegreaterthanenddate', 'multicoursereport');?>');
			return false;
		/*} else if(diffDays>365) {
			alert('<?php //echo get_string('maxperiodisoneyear', 'multicoursereport');?>');
			return false;*/
		} else {

			var sel_mode = 1;
			<?php if($USER->archetype == $CFG->userTypeAdmin){?>
				sel_mode = $("input[name=selector]:checked").val();
			<?php } ?>
			var startDate = $('#start_date').val();
			startDate = startDate.replace(/\//g, '-');			
			var endDate = $('#end_date').val();
			endDate = endDate.replace(/\//g, "-");
			var url = '<?php echo $CFG->wwwroot.'/my/'.$CFG->pageGlobalRequestReport;?>?startDate='+startDate+'&endDate='+endDate+'&sel_mode='+sel_mode;

			if(sel_mode == 2){
				var userGroup = loopSelected('user_group');

				if(userGroup != -1) {
					url = url+'&user_group='+userGroup;
				}
			}else{
				var department = loopSelected('department');
				var team = loopSelected('team');
				if(department != -1) {
					url = url+'&department='+department;
				}
				if(team != -1) {
					url = url+'&team='+team;
				}
			}
			var program = loopSelected('program');
			var user = loopSelected('user');
			var type = loopSelected('type');
			var course = loopSelected('course');
			var classes = loopSelected('classes');
			var job_title = loopSelected('job_title');
			var company = loopSelected('company');
			
			if(program != -1) {
				url = url+'&program='+program;
			}
			if(user != -1) {
				url = url+'&user='+user;
			}
			if(course != -1) {
				url = url+'&course='+course;
			}
			if(classes != -1) {
				url = url+'&classes='+classes;
			}
			if(type != -1) {
				url = url+'&type='+type;
			}
			
			if(job_title != -1) {
				url = url+'&job_title='+job_title;
			}
			
			if(company != -1) {
				url = url+'&company='+company;
			}
			
			window.location.href = url;
		}
	});
	var old_start_date = "";
	$('#start_date').click(function(){
		old_start_date = $('#start_date').val();
	});
	$('#start_date').datepicker({
		changeMonth: true,
	      changeYear: true,
		dateFormat: 'mm/dd/yy',
		onSelect: function() {			
			var d1 = $('#start_date').datepicker('getDate');
			var d2 = $('#end_date').datepicker('getDate');
			var diff = 0;
			if (d1 && d2) {
				diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
			}
			/*if(diff > 365) {
				alert("<?php //echo get_string('maxperiodisoneyear', 'multicoursereport');?>");
				$('#start_date').val(old_start_date);
			}*/
		}
	});
	var old_end_date = "";
	$('#end_date').click(function(){
		old_end_date = $('#end_date').val();
	});
	$('#end_date').datepicker({
		changeMonth: true,
	      changeYear: true,
		dateFormat: 'mm/dd/yy',
		onSelect: function(dateStr) {
			var startDate = new Date($("#start_date").val());
			var endDate = new Date($('#end_date').val());
			if(startDate > endDate){
				alert("<?php echo get_string('startdatecannotbegreaterthanenddate', 'multicoursereport');?>");
				$('#end_date').val(old_end_date);
			}
			
			var d1 = $('#start_date').datepicker('getDate');
			var d2 = $('#end_date').datepicker('getDate');
			var diff = 0;
			if (d1 && d2) {
				diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
			}
			/*if(diff > 365) {
				alert("<?php //echo get_string('maxperiodisoneyear', 'multicoursereport');?>");
				$('#end_date').val(old_end_date);
			}*/
		}
	});
	
	/*Selection mode */
	$("input[name=selector]").change(function(){
		var sel_mode = $(this).val();
		
		$("#department").val('-1');
		$("#team").val('-1');
		$("#user_group").val('-1');
		$("#program").val('-1');
		$("#course").val('-1');
		$("#classes").val('-1');
		$("#type").val('-1');
		if(sel_mode == 2){
			$("#user_group").show();
			$("#department").hide();
			$("#team").hide();
			$(".filter-external-label").hide();
		}else{
			var department = loopSelected('department');
			loadTeams(department);
			$("#department").show();
			$("#team").show();
			$("#user_group").hide();
			 $(".filter-external-label").show();
		}
	});
});

function loopSelected(id) {
	var selectedArray = new Array();
	selectedArray[0] = '-1';
	if($('#'+id).length) {
		$('#'+id+' :selected').each(function(i, selected) {
			selectedArray[i] = $(selected).val();
		});
	}
	//if(selectedArray.indexOf("-1") > -1) {
	if($.inArray("-1", selectedArray) > -1) {
		return -1;
	} else {
		return selectedArray.join('@');
	}
}


 
	<?php
		if(!empty($_REQUEST['sel_mode']) && $_REQUEST['sel_mode'] == 2){
		?>
			var department = '-1';
			var user_group = loopSelected('user_group');
			loadPrograms(department, user_group);
			var job_title = loopSelected('job_title');
		    var company = loopSelected('company');
			loadUsers(department, user_group, job_title, company);
		<?php
		}else{
			if(isset($sDepartmentArr) || isset($sTeamArr) || isset($sProgramArr)){?>
		
				var department = loopSelected('department');
				  //var team = loopSelected('team');
			   //var program = loopSelected('program');
				//loadPrograms(department, team);
				//loadCourses(department, team, program);
				loadTeams(department);
				   
			
	   <?php } }?>
 
</script>