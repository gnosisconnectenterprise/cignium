<?php 
global $CFG, $DB;
$sdepartmentArr	= array('-1');
$sTeamArr		= '-1';
$sManagerArr	= array();
$srolesArr		= array();
$sJobTitleArr = array('-1');
$sCompanyArr = array('-1'); 

$sCountryArr = array('-1');
$sisManagerArr =array('-1');

$sLeadersNameArr = array('-1');
if($paramArray['report_to']!=''){
	$sLeadersNameArr = explode('@',$paramArray['report_to']);
}

if(!empty($paramArray['department'])){
	$sdepartmentArr	= explode('@',$paramArray['department']);
}
if(!empty($paramArray['user_group'])){
	$sUserGroupArr	= explode('@',$paramArray['user_group']);
}
if(!empty($paramArray['team']))
	$sTeamArr		= $paramArray['team'];
if(!empty($paramArray['managers']))
	$sManagerArr	= $paramArray['managers'];
if(!empty($paramArray['roles']))
	$srolesArr		= explode('@',$paramArray['roles']);

if($USER->archetype == $CFG->userTypeManager){
	$display = "style = 'display:none;'";
	//$departmentRecords = $DB->get_records_sql("SELECT d.id,d.title,d.is_external FROM mdl_department_members as dm LEFT JOIN mdl_department as d on d.id = dm.departmentid WHERE userid = ".$USER->id." order by d.title ASC");
	$departmentRecords = getDepartments();
	foreach($departmentRecords as $departmentRecord){
		$sdepartmentArr = array($departmentRecord->id);
	}
	$userGroups = array();
}else{
	$display = "";
	$departmentRecords = getDepartments();
		$userGroups = getUserGroups();
}
if($USER->archetype == $CFG->userTypeManager){
	$display = "style = 'display:none;'";
	$displayFilter1 = "style = 'display:none;'";
	$displaySelector = "style = 'display:none;'";
	$displayFilter2 = "style = 'display:block;'";
}else{
	$display = "style = 'display:block;'";
	$displayFilter1 = "style = 'display:none;'";
	$displaySelector = "style = 'display:block;'";
	$displayFilter2 = "style = 'display:block;'";

	$radioChecked1 = "checked";
	$radioChecked2 = "";
	if(!empty($_REQUEST['sel_mode']) && $_REQUEST['sel_mode'] == 2){
		$display = "style = 'display:none;'";
		$displayFilter1 = "style = 'display:block;'";
		$displayFilter2 = "style = 'display:none;'";

		$radioChecked1 = "";
		$radioChecked2 = "checked";
	}
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/style/jquery-ui.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-ui.js"></script>
<div class="subs_reports_status_bar subsReport" id="report-search" style = "clear:both;">
  
  <div class="charc-filter" >
	<div class = "field_selector" <?php echo $displaySelector;?> >
		<div class ="selector-1 selector-radio">
			<input type = "radio" name = "<?php echo $filterBox;?>selector" value = "1" class = "sel_mode" <?php echo $radioChecked1;?> ><span><?php echo get_string("by_department"); ?></span>
		</div>
		<div class ="selector-2 selector-radio">
			<input type = "radio" name = "<?php echo $filterBox;?>selector" value = "2" class = "sel_mode" <?php echo $radioChecked2;?> ><span><?php echo get_string("by_group"); ?></span>
		</div>
	</div>
    <div class="select_box_mul" >
		<select autocomplete="off" size="2" multiple="multiple" class="" id="<?php echo $filterBox;?>user_group" name="<?php echo $filterBox;?>user_group" <?php echo $displayFilter1;?> >
		<option value="-1"><?php echo get_string('select_user_group');?></option>
		<?php
		if(!empty($userGroups)){
			foreach($userGroups as $userGroup){
				$selectedC = in_array($userGroup->id, $sUserGroupArr)?'selected="selected"':'';
			?>
				 <option value="<?php echo $userGroup->id?>" <?php echo $selectedC;?> ><?php echo $userGroup->name;?></option>
			<?php
			}
		}
		?>
        </select>

        
         <?php echo createDepartmentSelectBoxForFilter($filterBox.'department', $departmentRecords, $sdepartmentArr, $display); ?>
         <?php echo getEDAstric('lower-assign-filter',$display, 'left');?>
        
      
		<select autocomplete="off" size="2" multiple="multiple" class="loadingteams" id="<?php echo $filterBox;?>team" name="<?php echo $filterBox;?>team" <?php echo $displayFilter2;?> >
			<option value=""><?php echo get_string('loadingteams','user');?></option>
		</select>
        
         <?php 
		   echo createSelectBoxFilterFor('job_title', $filterBox.'job_title', $sJobTitleArr);
                   
                   echo createReportToSelectBoxForFilter($filterBox.'report_to', '', $sLeadersNameArr, $display);
                   
		   echo createSelectBoxFilterFor('company', $filterBox.'company', $sCompanyArr);
                   
                   //Code added to add country filter, on 15th feb 2016
                   echo createCountrySelectBoxFilterFor('country', $filterBox.'country', $sCountryArr);
                   echo createIsManagerSelectBoxFilterFor('is_manager_yes', $filterBox.'is_manager_yes', $sisManagerArr);
                   
		  
		?>
        
       
    </div>
	
      <?php if($CFG->isdisplayhiredate){
		if(($USER->archetype == $CFG->userTypeAdmin) || ($USER->archetype == $CFG->userTypeManager))
		{	?>
		<div>&nbsp;</div>
	<div>
	<span class="dateLable">HD From: </span><input id="start_date" name="start_date" type="text" value="<?php echo $_REQUEST['start_date'];?>" readonly = "true">
	</div>
	<div>&nbsp;</div>
	<div>
	<span class="dateLable">HD To: </span><input id="end_date" name="end_date" type="text" value="<?php echo $_REQUEST['end_date'];?>" readonly = "true">
	</div>	

	<?php
		}	
	
	}?>
    </div>
  </div>

<script>

jQuery(document).ready(function($) {

   	 $('#start_date').datepicker({
		changeMonth: true,
		changeYear: true,
		//yearRange: '1900:'+(new Date).getFullYear() ,
		//maxDate: +0
	});

$('#end_date').datepicker({
		changeMonth: true,
		changeYear: true,
		//yearRange: '1900:'+(new Date).getFullYear() ,
		//maxDate: +0
	});

		  
			
	});

</script>
<style>
.selector-1,.select_box_mul #<?php echo $filterBox;?>department,.select_box_mul #<?php echo $filterBox;?>user_group,.select_box_mul #<?php echo $filterBox;?>managers, .select_box_mul #<?php echo $filterBox;?>team, .select_box_mul #<?php echo $filterBox;?>team, .select_box_mul #<?php echo $filterBox;?>program  {float:left;}
</style>
<script>
var courseId = "<?php echo $courseid; ?>";
var filterBox = "<?php echo $filterBox; ?>";

function loadTeams(department,getSelected){
	if(getSelected == 1){
		var team = "<?php echo $sTeamArr; ?>";
	}else{
		var team = "-1";
	}
    $.ajax({	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getTeamListByDepartment&department='+department+"&team="+team+"&isReport=false",
		dataType:'json',
		success:function(data){		  
			var success = data.success;
		    var error = data.error;
			if(success == 1){
			  var selectHtml = data.response;
			  $('#'+filterBox+'team').html(selectHtml);
		   }else{
			 alert(error);
		   }
		}	  
	}); 
}
/*function loadManager(department,getSelected){
	if(getSelected == 1){
		var manager= "<?php echo $sManagerArr; ?>";
	}else{
		var manager = "-1";
	}
	
    $.ajax({	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getManagersByDepartment&department='+department+"&manager="+manager,
		dataType:'json',
		success:function(data){	  
			var success = data.success;
			var error = data.error;
			if(success == 1){
				var selectHtml = data.response;
				$('#'+filterBox+'managers').html(selectHtml);
			}else{
				alert(error);
			}
		}	  
	}); 
}
*/

function resetElement() {


      $("input[name="+filterBox+"selector]:first").prop('checked', true);
	  resetBox(1);

}



function searchUsers(){
        $("#user_addselect_wrapper_offset").val(100);
	var searchText = $("#user_addselect_searchtext").val();
	var sel_mode = $("input[name="+filterBox+"selector]:checked").val();
	var user_group = loopSelected(filterBox+'user_group');
	var department = loopSelected(filterBox+'department');
	//var managers = loopSelected(filterBox+'managers');
	var managers = '-1';
	var team = loopSelected(filterBox+'team');
	//var roles = loopSelected(filterBox+'user_roles');
	var roles = '-1';
	
	var job_title = loopSelected(filterBox+'job_title');
        var report_to = loopSelected(filterBox+'report_to');
	var company = loopSelected(filterBox+'company');
        
        var country = loopSelected(filterBox+'country');
         var is_manager_yes = loopSelected(filterBox+'is_manager_yes');
	
	$.ajax({
			url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
			type:'POST',
			data:'action=searchCourseNonUser&courseid='+courseId+'&search_text='+searchText+'&sel_mode='+sel_mode+'&user_group='+user_group+'&department='+department+'&managers='+managers+'&team='+team+'&roles='+roles+'&country='+country+'&job_title='+job_title+'&report_to='+report_to+'&company='+company+'&is_manager_yes='+is_manager_yes,        
			success:function(data){
				$('#user_addselect_wrapper').find('select').html(data);
			}
	});
}

 function resetBox(sel_mode){

		$("#"+filterBox+"department").val('-1');
		$("#"+filterBox+"team").val('-1');
		$("#"+filterBox+"user_group").val('-1');

		if(sel_mode == 2){
			$("#"+filterBox+"user_group").show();
			$("#"+filterBox+"department").hide();
			$("#"+filterBox+"team").hide();
			$("#lower-filter-external-label-left").hide();
			//$("#"+filterBox+"managers").hide();
		}else{
		
		    <?php if($USER->archetype == $CFG->userTypeAdmin) {?>
			  $("#"+filterBox+"department").show();
			  $("#lower-filter-external-label-left").show();
			<?php }else{ ?>
			   $("#"+filterBox+"department").hide();
			   $("#lower-filter-external-label-left").hide();
			<?php } ?>
			$("#"+filterBox+"team").show();
			//$("#"+filterBox+"managers").show();

			$("#"+filterBox+"user_group").hide();
		}
			
			searchUsers();
		
}
		
jQuery(document).ready(function($) {

   	$("#"+filterBox+"department").on('change', function() {
	
		var department = loopSelected(filterBox+'department');
		//$("#user_addselect_wrapper_offset").val(100);
		searchUsers();
		loadTeams(department,0);
		
		//loadManager(department,0);
			
	});	
//code edit to add country filter, on 15th Feb 2016
	$("#"+filterBox+"team, #"+filterBox+"user_group, #"+filterBox+"job_title, #"+filterBox+"company,#"+filterBox+"is_manager_yes, #"+filterBox+"country, #"+filterBox+"report_to").on('change', function() {
                //console.log('filter called');
                //$("#user_addselect_wrapper_offset").val(100);
		searchUsers();
		
	});	
	
	/*$("#"+filterBox+"user_search").click(function(){
	      searchUsers();
	});*/
	
	$("input[name="+filterBox+"selector]").change(function(){
	   var sel_mode = $(this).val();
	   resetBox(sel_mode);
	});
	
});

function loopSelected(id) {
	var selectedArray = new Array();
	selectedArray[0] = '-1';
	if($('#'+id).length) {
		$('#'+id+' :selected').each(function(i, selected) {
			selectedArray[i] = $(selected).val();
		});
	}
	if($.inArray("-1", selectedArray) > -1) {
		return -1;
	} else {
		return selectedArray.join('@');
	}
}


var department = loopSelected(filterBox+'department');
loadTeams(department,1);
//loadManager(department,1);
	   
 
</script>