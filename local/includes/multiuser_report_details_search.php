<?php 
global $CFG, $DB,$USER;
$isReport = true;

?>
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/style/jquery-ui.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-ui.js"></script>
<div class="subs_reports_status_bar subsReport" id="report-search">
  
  <div class="charc-filter" >
   

    <div class="date_sec">
      <div class="startenddate"><span><?php echo get_string('lastaccesseddate');?></span>
           <span class="fdate_selector">
             <input type="text" id="start_date" value="<?php echo $sDateSelected;?>" name="start_date" readonly = "true">
           </span>
      </div>
      <div class="startenddate"><span><?php echo get_string('daterangeto','multicoursereport');?></span>
           <span class="fdate_selector">
              <input type="text" id="end_date" value="<?php echo $eDateSelected;?>" name="end_date" readonly = "true">
           </span>
      </div>
      <div >
        <input type="button" id="report_allsubssription" class="f-left" value="<?php echo get_string('go','multicoursereport');?>" >
        &nbsp; <input type="button"  class="f-left" onclick="resetElement();" value="<?php echo get_string('reset','multicoursereport');?>" >
      </div>
    </div>
    
  </div>
</div>

<script>

var selfUrl = '<?php echo $pageURL; ?>';


function resetElement() {
		
	   if(selfUrl){	
         var url = '<?php echo $CFG->wwwroot;?>'+selfUrl+'?uid=<?php echo $userId;?>&cid=<?php echo $sCid;?>&back=<?php echo $sBack;?>';
	   }else{
	     var url = '<?php echo $CFG->wwwroot;?>'+selfUrl+'?uid=<?php echo $userId;?>&cid=<?php echo $sCid;?>&back=<?php echo $sBack;?>';
	   }
	   window.location.href = url;

}


jQuery(document).ready(function($) {
		
	
	$("#report_allsubssription").on('click', function() {
		/*validate daterange*/
		var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
		var sdate = $('#start_date').val();
		var edate = $('#end_date').val();
		var firstDate = new Date(sdate);
		var secondDate = new Date(edate);
		var todayDate = new Date();
		//var diffDays = Math.round(Math.abs((secondDate.getTime() - firstDate.getTime())/(oneDay)));
		var diffDays = Math.floor((secondDate.getTime() - firstDate.getTime()) / 86400000); // ms per day

		/*if(firstDate.getTime() > todayDate.getTime()) {
			alert('<?php //echo get_string('startdatecannotbegreaterthantodaydate', 'multicoursereport');?>');
			return false;
		}else if(secondDate.getTime() > todayDate.getTime()) {
			alert('<?php //echo get_string('enddatecannotbegreaterthantodaydate', 'multicoursereport');?>');
			return false;
		} else */ if (secondDate.getTime() < firstDate.getTime()) {
			alert('<?php echo get_string('startdatecannotbegreaterthanenddate', 'multicoursereport');?>');
			return false;
		/*} else if(diffDays>365) {
			alert('<?php //echo get_string('maxperiodisoneyear', 'multicoursereport');?>');
			return false;*/
		} else {

			
			var startDate = $('#start_date').val();
			startDate = startDate.replace(/\//g, '-');			
			var endDate = $('#end_date').val();
			endDate = endDate.replace(/\//g, "-");
			
            if(selfUrl){	
			    var url = '<?php echo $CFG->wwwroot;?>'+selfUrl+'?uid=<?php echo $userId;?>&cid=<?php echo $sCid;?>&back=<?php echo $sBack;?>&startDate='+startDate+'&endDate='+endDate;
			}else{
			    var url = '<?php echo $CFG->wwwroot;?>'+selfUrl+'?uid=<?php echo $userId;?>&cid=<?php echo $sCid;?>&back=<?php echo $sBack;?>&startDate='+startDate+'&endDate='+endDate;
			}  

			
			window.location.href = url;
		}
	});
	var old_start_date = "";
	$('#start_date').click(function(){
		old_start_date = $('#start_date').val();
	});
	$('#start_date').datepicker({
		changeMonth: true,
	      changeYear: true,
		dateFormat: 'mm/dd/yy',
		onSelect: function() {			
			var d1 = $('#start_date').datepicker('getDate');
			var d2 = $('#end_date').datepicker('getDate');
			var diff = 0;
			if (d1 && d2) {
				diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
			}
			/*if(diff > 365) {
				alert("<?php //echo get_string('maxperiodisoneyear', 'multicoursereport');?>");
				$('#start_date').val(old_start_date);
			}*/
		}
	});
	var old_end_date = "";
	$('#end_date').click(function(){
		old_end_date = $('#end_date').val();
	});
	$('#end_date').datepicker({
		changeMonth: true,
	      changeYear: true,
		dateFormat: 'mm/dd/yy',
		onSelect: function(dateStr) {
			var startDate = new Date($("#start_date").val());
			var endDate = new Date($('#end_date').val());
			if(startDate > endDate){
				alert("<?php echo get_string('startdatecannotbegreaterthanenddate', 'multicoursereport');?>");
				$('#end_date').val(old_end_date);
			}
			
			var d1 = $('#start_date').datepicker('getDate');
			var d2 = $('#end_date').datepicker('getDate');
			var diff = 0;
			if (d1 && d2) {
				diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
			}
			/*if(diff > 365) {
				alert("<?php //echo get_string('maxperiodisoneyear', 'multicoursereport');?>");
				$('#end_date').val(old_end_date);
			}*/
		}
	});
	
	
});

function loopSelected(id) {
	var selectedArray = new Array();
	selectedArray[0] = '-1';
	if($('#'+id).length) {
		$('#'+id+' :selected').each(function(i, selected) {
			selectedArray[i] = $(selected).val();
		});
	}
	//if(selectedArray.indexOf("-1") > -1) {
	if($.inArray("-1", selectedArray) > -1) {
		return -1;
	} else {
		return selectedArray.join('@');
	}
}



</script>