<?php 
global $CFG, $DB;
$isReport = true; 

//pr($classroomArr);die;
?>
<link type="text/css" rel="stylesheet" href="<?php echo $CFG->wwwroot;?>/theme/gourmet/style/jquery-ui.css" />
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-ui.js"></script>
<div class="subs_reports_status_bar subsReport" id="report-search">
  
  <div class="charc-filter" >
    <div class="select_box_mul" >       
		<!--select autocomplete="off" size="2" multiple="multiple" class="" id="program" name="program" >
            <option value=""><?php //echo get_string('loadingprograms','classroomreport');?></option>
        </select-->
        <input type="hidden" id="program" name="program" value="-1" />
		<select autocomplete="off" size="2"  class="" id="course" name="course" >
		<optgroup label="<?php echo get_string('course','classroomreport');?>">
		<?php
		
		foreach($classroomArr as $arr){
		
			if(isset($sCourseArr) && in_array($sCourseArr[0], array(0, '-1'))){
			  $selectedC = $arr->id == $classroomId?'selected="selected"':'';
			}else{
			  $selectedC = in_array($arr->id, $sCourseArr)?'selected="selected"':'';
			}

		?>
             <option value="<?php echo $arr->id?>" <?php echo $selectedC;?> ><?php echo $arr->fullname;?></option>
		<?php
		}
		?>
		</optgroup>
        </select>
		<select autocomplete="off" size="2"  class="loadingclasses" id="classes" name="classes" >
			<option value=""><?php echo get_string('loadingclasses','classroomreport');?></option>
		</select>
         <select autocomplete="off" size="2" multiple="multiple"  id="status" name="status" >
			<option value="-1"  <?php echo in_array('-1', $sStatusArr)?'selected="selected"':'';?>>All</option>
			<option value="3"  <?php echo in_array('3', $sStatusArr)?'selected="selected"':'';?>><?php echo get_string('totalinviteduser','classroomreport');?></option>
            <option value="0"  <?php echo in_array('0', $sStatusArr)?'selected="selected"':'';?>><?php echo get_string('managerforapproval','classroomreport');?></option>
			<option value="1"  <?php echo in_array('1', $sStatusArr)?'selected="selected"':'';?>><?php echo get_string('approveduser','classroomreport');?></option>
			<option value="2"  <?php echo in_array('2', $sStatusArr)?'selected="selected"':'';?>><?php echo get_string('declineduser','classroomreport');?></option>
		</select>
      <!-- select autocomplete="off" size="2" multiple="multiple" class="" id="type" name="type" >
        <option value="-1" <?php echo in_array('-1', $sTypeArr)?'selected="selected"':'';?> ><?php echo get_string('allstatus','classroomreport');?></option>
        <option value="1" <?php echo in_array('1', $sTypeArr)?'selected="selected"':'';?>  ><?php echo get_string('inprogress','classroomreport');?></option>
        <option value="2" <?php echo in_array('2', $sTypeArr)?'selected="selected"':'';?>  ><?php echo get_string('completed','classroomreport');?></option>
      </select -->
    </div>
    

    <div class="date_sec">
  
      <div >
        <input type="button" id="report_allsubssription" class="f-left" value="<?php echo get_string('go','classroomreport');?>">
        &nbsp; <input type="button"  class="f-left" onclick="resetElement();" value="<?php echo get_string('reset','classroomreport');?>">
      </div>
    </div>
    
  </div>
</div>

<script>

function loadCourses(department, team){
    var course = '<?php echo $sCourse;?>';
    $.ajax({	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getCourseListByDnT&department='+department+"&team="+team+"&course="+course+"&program=-1&course_type_id=<?php echo $CFG->courseTypeClassroom;?>",
		dataType:'json',
		success:function(data){		  
			var success = data.success;
			var error = data.error;
			if(success == 1){
				var selectHtml = data.response;
				$('#course').html(selectHtml);
			}else{
				alert(error);
			}
		}	  
	}); 
}
function loadClasses(courses){
	
	//var classes = loopSelected('classes');
	<?php 
	if(isset($sclassesArr) && in_array($sclassesArr[0], array(0, '-1'))){?>
	 var classes = '<?php echo $classId;?>';
	<?php }else{ ?>
	 var classes = '<?php echo $sclasses;?>';
	<?php } ?>
			
    $.ajax({	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=getClassesWithoutAll&courses='+courses+'&classes='+classes,
		dataType:'json',
		success:function(data){	  
			var success = data.success;
			var error = data.error;
			if(success == 1){
				var selectHtml = data.response;
				$('#classes').html(selectHtml);
			}else{
				alert(error);
			}
		}	  
	}); 
}

function resetElement() {

       var url = '<?php echo $CFG->wwwroot.'/course/'.$CFG->pageDeclinedLearnerReport;?>';
		window.location.href = url;

}


jQuery(document).ready(function($) {
	
	$("#course").on('change', function() {
	
		var course = loopSelected('course');
		loadClasses(course);
			
	});
	
	
	$("#report_allsubssription").on('click', function() {
		/*validate daterange*/
		
			var classes = loopSelected('classes');	
			var status = loopSelected('status');
			if(classes==-1){
				alert('Please select a class.');
				return false;
			}
			var type = loopSelected('type');
			var course = loopSelected('course');
			
			var url = '<?php echo $CFG->wwwroot.'/course/'.$CFG->pageDeclinedLearnerReport;?>?status='+status;
			
			if(classes != -1) {
				url = url+'&classes='+classes;
			}
			if(course != -1) {
				url = url+'&course='+course;
			}
			if(type != -1) {
				url = url+'&type='+type;
			}
			
			window.location.href = url;
		
	});
	var old_start_date = "";
	$('#start_date').click(function(){
		old_start_date = $('#start_date').val();
	});
	$('#start_date').datepicker({
		changeMonth: true,
	      changeYear: true,
		dateFormat: 'mm/dd/yy',
		onSelect: function() {			
			var d1 = $('#start_date').datepicker('getDate');
			var d2 = $('#end_date').datepicker('getDate');
			var diff = 0;
			if (d1 && d2) {
				diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
			}
			/*if(diff > 365) {
				alert("<?php //echo get_string('maxperiodisoneyear', 'classroomreport');?>");
				$('#start_date').val(old_start_date);
			}*/
		}
	});
	var old_end_date = "";
	$('#end_date').click(function(){
		old_end_date = $('#end_date').val();
	});
	$('#end_date').datepicker({
		changeMonth: true,
	      changeYear: true,
		dateFormat: 'mm/dd/yy',
		onSelect: function(dateStr) {
			var startDate = new Date($("#start_date").val());
			var endDate = new Date($('#end_date').val());
			if(startDate > endDate){
				
				alert("<?php echo get_string('startdatecannotbegreaterthanenddate', 'multicoursereport');?>");
				$('#end_date').val(old_end_date);
			}
			
			var d1 = $('#start_date').datepicker('getDate');
			var d2 = $('#end_date').datepicker('getDate');
			var diff = 0;
			if (d1 && d2) {
				diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
			}
			/*if(diff > 365) {
				alert("<?php //echo get_string('maxperiodisoneyear', 'classroomreport');?>");
				$('#end_date').val(old_end_date);
			}*/
		}
	});
	
});

function loopSelected(id) {
	var selectedArray = new Array();
	selectedArray[0] = '-1';
	if($('#'+id).length) {
		$('#'+id+' :selected').each(function(i, selected) {
			selectedArray[i] = $(selected).val();
		});
	}
	//if(selectedArray.indexOf("-1") > -1) {
	if($.inArray("-1", selectedArray) > -1) {
		return -1;
	} else {
		return selectedArray.join('@');
	}
}


 
	<?php //if(isset($sDepartmentArr) || isset($sTeamArr) || isset($sProgramArr)){?>
	
	    var courses = loopSelected('course');
		      //var team = loopSelected('team');
		   //var program = loopSelected('program');
			//loadPrograms(department, team);
		    //loadCourses(department, team, program);
			//loadTeams(department);
			loadClasses(courses);
			   
		
   <?php //} ?>
 
</script>
