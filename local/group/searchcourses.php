<?php
require_once('../../config.php');
require_once($CFG->dirroot . '/local/user/selector/lib.php');

$groupid = optional_param('groupid',0, PARAM_INT);
$searchText = optional_param('search_text', '', PARAM_RAW);
$action = optional_param('action', 0, PARAM_ALPHA);
if($action == "searchNonCourse"){
	
	$courses = new non_group_courses_selector($searchText, array('groupid' => $groupid));
	$courses->get_non_group_courses();
	$courses->course_option_list(1);
}
if($action == "searchGroupCourse"){
	
	$courses = new non_group_courses_selector($searchText, array('groupid' => $groupid));
	$courses->get_group_courses();
	$courses->course_option_list(2);
}

if($action == "searchNonDepartmentCourse"){
	$departmentId = optional_param('departmentId',0, PARAM_INT);
	$courses = new department_courses_selector($searchText, array('departmentId' => $departmentId));
	$courses->get_non_department_courses();
	$courses->course_option_list(1);
}
if($action == "searchDepartmentCourse"){
	$departmentId = optional_param('departmentId',0, PARAM_INT);
	$courses = new department_courses_selector($searchText, array('departmentId' => $departmentId));
	$courses->get_department_courses();
	$courses->course_option_list(2);
}

if($action == "searchNonProgramCourse"){
	$programId = optional_param('programId',0, PARAM_INT);
	$courses = new program_courses_selector($searchText, array('programId' => $programId));
	$courses->get_non_program_courses();
	$courses->course_option_list(1);
}
if($action == "searchProgramCourse"){
	$programId = optional_param('programId',0, PARAM_INT);
	$courses = new program_courses_selector($searchText, array('programId' => $programId));
	$courses->get_program_courses();
	$courses->course_option_list(2);
}


if($action == "searchNonProgramUser"){
	$programId = optional_param('programId',0, PARAM_INT);
	$users = new program_users_selector($searchText, array('programId' => $programId));
	$users->get_non_program_users();
	$users->user_option_list(1);
}
if($action == "searchProgramUser"){
	$programId = optional_param('programId',0, PARAM_INT);
	$users = new program_users_selector($searchText, array('programId' => $programId));
	$users->get_program_users();
	$users->user_option_list(2);
}
?>