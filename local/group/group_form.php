<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * A form for the creation and editing of groups.
 *
 * @copyright 2006 The Open University, N.D.Freear AT open.ac.uk, J.White AT open.ac.uk
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package   core_group
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot.'/lib/formslib.php');

/**
 * Group form class
 *
 * @copyright 2006 The Open University, N.D.Freear AT open.ac.uk, J.White AT open.ac.uk
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package   core_group
 */
class group_form extends moodleform {

    /**
     * Definition of the form
     */
    function definition () {
        global $USER, $CFG, $DB;
        //$coursecontext = context_course::instance($COURSE->id);

        $mform =& $this->_form;
        $editoroptions = $this->_customdata['editoroptions'];
        $groupData = $this->_customdata['groupdata'];
        $disabled_fields = "";
        if($groupData->auth=='ldap'){
        	$disabled_fields = 'class="disabled_user_form_fields" disabled="disabled"';
        }
       // $mform->addElement('header', 'general', get_string('general', 'form'));
		/*if(!empty($groupData->id)){
				if($groupData->picture != ''){
					$view =  $CFG->teamDefaultImagePath.$groupData->picture;
					$view1 =  $CFG->teamDefaultImageURL.$groupData->picture;
					//echo $view;die;
					if(file_exists($view) && $groupData->picture !=''){
						$mform->addElement('html','<div class="fitem"><div class="fitemtitle"><label for="id_image">'.get_string("current_image").'</label></div>');
						$mform->addElement('html','<div class="felement fstatic"><img width="64" height="64" class="userpicture defaultuserpic" src="'.$view1.'">');
						//$mform->addElement('html','<div class="felement fimage">');
						$mform->addElement('html','<a class = "removeimage" rel = "'.$groupData->id.'"><span id="'.$groupData->id.'" rel="login">'.get_string("delete").'</span></a>');
						$mform->addElement('html','</div></div>');
					}
				}
			}*/
        $mform->addElement('text','name', get_string('groupname', 'group'),'maxlength="254" size="50"'.$disabled_fields);
        $mform->addRule('name', get_string('required'), 'required', null, 'client');
        $mform->setType('name', PARAM_TEXT);
		
		
		createDepartmentSelectBox($mform, 'edit', 'group', '', $groupData);
		/*$departments = $DB->get_records_sql("SELECT id,title,is_external FROM {$CFG->prefix}department WHERE deleted = 0 AND status = 1 order by title ASC");
		$departmentSelect = array('0'=>get_string('select_department'));
		if(!empty($groupData->id)){
			$teamDepartment = $DB->get_records('group_department', array('team_id'=>$groupData->id, 'is_active'=>1));
			if(count($teamDepartment) > 0 ){
			}else{
				$departmentSelect = array('0'=>get_string('global_team'));
			}
		}
		foreach($departments as $department){
			$identifierED = $department->is_external == 1?getEDAstric('input'):'';
		    $departmentSelect[$department->id] = $identifierED.$department->title;
		}
	
	   // $mform->addElement('html','<img src="'.$CFG->wwwroot.'/theme/image.php/gourmet/core/1409042017/req" alt="Required field" title="Required field" class="customreq" >');
		$mform->addElement('select', 'department', get_string('department'), $departmentSelect);
		//$mform->getElement('department')->setMultiple(true);
		//$mform->addRule('department', get_string('required'), 'nonzero', null, 'client');
		
		
	      if($USER->archetype == $CFG->userTypeManager){
		
		   $teamDepartmentArr = array();
		   if(!empty($groupData->id)){
		    	
				$teamDepartment = $DB->get_records('group_department', array('team_id'=>$groupData->id, 'is_active'=>1));
				
				if(count($teamDepartment) > 0 ){
				  foreach($teamDepartment as $arr ){
				   $teamDepartmentArr[] = $arr->department_id;
				  }
				}
				
				$mform->SetDefault('department',$teamDepartmentArr);
				$mform->hardFreeze('department');
				
		   }else{
		   
		        $userDepartment = $DB->get_records_sql("SELECT departmentid FROM {$CFG->prefix}department_members WHERE userid = ".$USER->id);
				$userDepartments = array();
				if($userDepartment){
					foreach($userDepartment as $department){
						$userDepartments[] = $department->departmentid;
					}
					//$mform->getElement('department')->setSelected($userDepartments);
				}
				
				if(!empty($userDepartments)){
					$mform->SetDefault('department',$userDepartments);
					$mform->hardFreeze('department');
				}

		   }
		   
		   
		}elseif($USER->archetype == $CFG->userTypeAdmin){
		   
		   $teamDepartmentArr = array();
		   if(!empty($groupData->id)){
		    	
				$teamDepartment = $DB->get_records('group_department', array('team_id'=>$groupData->id, 'is_active'=>1));
				if(count($teamDepartment) > 0 ){
				  foreach($teamDepartment as $arr ){
				   $teamDepartmentArr[] = $arr->department_id;
				  }
				  
				  $mform->SetDefault('department',$teamDepartmentArr);
				  $mform->hardFreeze('department');
				
				}else{
					$mform->SetDefault('department',0);
					$mform->hardFreeze('department');
				}
				
		   }
		
		}*/

		
		if($USER->archetype != $CFG->userTypeManager && $USER->archetype != $CFG->userTypeStudent){
			//$mform->addElement('html','<div class="fitem"><div class="fl italicmsg">'.get_string("create_usergroup_instruction").'</div></div>');
			$mform->addElement('static', 'create_usergroup_instruction', '',get_string("create_usergroup_instruction"));
		}
		$teamId = 0;
		$selmode = 1;
		if($groupData->id == '' || $groupData->id == '-1'){
			$selmode = 2;
		}else{
			$teamId = $groupData->id;
		}
		if($USER->archetype != $CFG->userTypeAdmin){
			$selmode = 1;
		}
		if($groupData->department_id == '' || $groupData->department_id == 0){
			$selmode = 2;
		}
		//$teamLists = getTeamList($groupData->department_id,$teamId);
		$teamLists = getGroupsListing('mg.name', 0,'ASC', 0, 0,'','',array('department'=>$groupData->department_id,'teamId'=>$teamId,'teams'=>$teamId,'removeChild'=>1,'sel_mode'=>$selmode));
		//pr($teamLists);die;
		$teamSelect = array('0'=>get_string('select_parent_team'));
		if(!empty($teamLists)){
			foreach($teamLists as $teamList){
				$teamSelect[$teamList->id] = $teamList->name;
			}
		}
		$mform->addElement('select', 'parent_id', get_string('select_parent'), $teamSelect,$disabled_fields);
		$paramArr = array('is_active'=>1);
		if($paramArr){
			$paramArr = array('department'=>$groupData->department_id,'is_active'=>1);
		}
		$userLists = get_users_listing('u.firstname', '', 1, 0, '', '', '','',$paramArr, '');
		$teamSelect = array('0'=>get_string('select_owner_dropdown'));
		if(!empty($userLists)){
			foreach($userLists as $userList){
				$teamSelect[$userList->id] = $userList->firstname.' '.$userList->lastname.' ('.$userList->username.')';
			}
		}
		$mform->addElement('select', 'group_owner', get_string('select_owner'), $teamSelect);

        /*$mform->addElement('text','idnumber', get_string('idnumbergroup'), 'maxlength="100" size="10"');
        $mform->addHelpButton('idnumber', 'idnumbergroup');
        $mform->setType('idnumber', PARAM_RAW);
        if (!has_capability('moodle/course:changeidnumber', $coursecontext)) {
            $mform->hardFreeze('idnumber');
        }*/

        $mform->addElement('editor', 'description_editor', get_string('groupdescription', 'group'), null, $editoroptions);
        $mform->setType('description_editor', PARAM_RAW);

       /* $mform->addElement('passwordunmask', 'enrolmentkey', get_string('enrolmentkey', 'group'), 'maxlength="254" size="24"', get_string('enrolmentkey', 'group'));
        $mform->addHelpButton('enrolmentkey', 'enrolmentkey', 'group');
        $mform->setType('enrolmentkey', PARAM_RAW);

        $options = array(get_string('no'), get_string('yes'));
        $mform->addElement('select', 'hidepicture', get_string('hidepicture'), $options);

        $mform->addElement('filepicker', 'imagefile', get_string('newpicture', 'group'));
        $mform->addHelpButton('imagefile', 'newpicture', 'group');*/
		//$mform->addElement('file','group_image', get_string('group_image', 'plugin'));
		//$mform->addElement('html','<div class="fitem"><div class="fl italicmsg">'.get_string("group_image_size", 'plugin').'</div></div>');
		if ($filesOptions = programOverviewFilesOptions($programData)) {
			$mform->addElement('filemanager', 'teamimage_filemanager', get_string('group_image', 'plugin'), null, $filesOptions);
		}
		if($USER->archetype == $CFG->userTypeAdmin && (isset($groupData->id) || !empty($groupData->id))){
			GLOBAL $CFG,$DB,$USER;
			$userDepartment = $DB->get_record('department_members',array('userid'=>$groupData->createdby,'is_active'=>1));
			$userRole = array($CFG->userTypeManager,$CFG->userTypeAdmin);
			$userList = getDepartmentManagerAdmin($userDepartment->departmentid);
			if(!empty($userList)){
				foreach($userList as $user){
					$changeownership[$user->id] = $user->firstname.' '.$user->lastname;
				}
			}
			$mform->addElement('select', 'changeownership', get_string('teamownership'), $changeownership);
			$mform->getElement('changeownership')->setSelected($groupData->createdby);
		}

        $mform->addElement('hidden','id');
        $mform->setType('id', PARAM_INT);

        $mform->addElement('hidden','courseid');
        $mform->setType('courseid', PARAM_INT);
        
		$showResetButton = true;
		if(!empty($groupData->id)){
		  $showResetButton = false;
		}
		if($groupData->id){
			$created_on = getDateFormat($groupData->timecreated, $CFG->customDefaultDateFormat);
			$mform->addElement('html','<div class="fitem "><div class="fitemtitle"><div class="fstaticlabel"><label>'.get_string("timecreated","program").'</label></div></div><div class="felement fstatic">'.$created_on.'</div></div>');
		}
        $this->add_action_buttons(true, null, $showResetButton);
		
    }
	function definition_after_data() {
        global $USER, $CFG, $DB, $OUTPUT;
        $mform =& $this->_form;
		$groupid = $mform->getElementValue('id');
		if ($groupid = $mform->getElementValue('id')) {
            $group = $DB->get_record('groups', array('id'=>$groupid));
			if(isset($_POST['group_owner'])){
				$mform->getElement('group_owner')->setSelected($_POST['group_owner']);
			}else{
				$mform->getElement('group_owner')->setSelected($group->group_owner);
			}
        } else {
            $group = false;
        }
	}
    /**
     * Form validation
     *
     * @param array $data
     * @param array $files
     * @return array $errors An array of errors
     */
    function validation($data, $files) {
        global $COURSE, $DB, $CFG;

        $errors = parent::validation($data, $files);
		$data['name'] = addslashes($data['name']);
        if ($data['id'] and $group = $DB->get_record('groups', array('id'=>$data['id']))) {
            /*if (core_text::strtolower($group->name) != core_text::strtolower($data['name'])) {
            }*/
			 if (isGroupAlreadyExist($data)) {
				 if($data['department'] == 0){
					 $errors['name'] = get_string('groupnameexistsusergroup', 'group', stripslashes($data['name']));
				 }else{
                    $errors['name'] = get_string('groupnameexistsindepartment', 'group', stripslashes($data['name']));
				}
			 }

        } else if (isGroupAlreadyExist($data)) {
			if($data['department'] == 0){
				$errors['name'] = get_string('groupnameexistsusergroup', 'group', stripslashes($data['name']));
			}else{
				$errors['name'] = get_string('groupnameexistsindepartment', 'group', stripslashes($data['name']));
			}
        } 
		if(!empty($_FILES) && $_FILES["group_image"]["error"] == 0){
				$imgname = $_FILES["group_image"]["tmp_name"];
				$size = getimagesize($imgname);
				if($size[0]>300 || $size[1]>300){
					$errors['group_image'] = get_string('filedimlogingroupdepartment','plugin');
				}
				$filecheck = basename($_FILES["group_image"]['name']);
				$ext = substr($filecheck, strrpos($filecheck, '.') + 1);
				if ($_FILES["group_image"]['name'] != '' && ($ext == "jpg" || $ext == "gif" || $ext == "png") && ($_FILES["group_image"]["type"] == "image/jpeg" || $_FILES["group_image"]["type"] == "image/gif" || $_FILES["group_image"]["type"] == "image/png")){
				}
				else{
					$errors["group_image"] = get_string('supportedfiletype','plugin');
				}
		}
		
        return $errors;
    }

    /**
     * Get editor options for this form
     *
     * @return array An array of options
     */
    function get_editor_options() {
        return $this->_customdata['editoroptions'];
    }
}
