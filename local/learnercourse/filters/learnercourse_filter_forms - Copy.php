<?php

require_once($CFG->libdir.'/formslib.php');

class learnercourse_add_filter_form extends moodleform {

    function definition() { 
        $mform       =& $this->_form;
        $fields      = $this->_customdata['fields'];
        $extraparams = $this->_customdata['extraparams'];

        $mform->addElement('hidden', 'type', isset($_REQUEST['type'])?$_REQUEST['type']:1);
		$mform->setDefault('value',1);
		
		$mform->addElement('header', 'newfilter', get_string('newfilter','filters'));

        foreach($fields as $ft) {
            $ft->setupForm($mform);
        }

        // in case we wasnt to track some page params
        if ($extraparams) {
            foreach ($extraparams as $key=>$value) {
                $mform->addElement('hidden', $key, $value);
                $mform->setType($key, PARAM_RAW);
            }
        }

        // Add button
        $mform->addElement('submit', 'addfilter', get_string('addfilter','filters'));
		$mform->addElement('submit', 'removeall', get_string('removeall','filters')); // added by rajesh

    }
}

class learnercourse_active_filter_form extends moodleform {

    function definition() {  echo "learnercourse_active_filter_form";die;
        global $SESSION; // this is very hacky :-(

        $mform       =& $this->_form;
        $fields      = $this->_customdata['fields'];
        $extraparams = $this->_customdata['extraparams'];
		
		$mform->addElement('hidden', 'type', isset($_REQUEST['type'])?$_REQUEST['type']:1);
		$mform->setDefault('value',1);

        if (!empty($SESSION->learnercourse_filtering)) {
            // add controls for each active filter in the active filters learnercourse
            $mform->addElement('header', 'actfilterhdr', get_string('actfilterhdr','filters'));

            foreach ($SESSION->learnercourse_filtering as $fname=>$datas) {
                if (!array_key_exists($fname, $fields)) {
                    continue; // filter not used
                }
                $field = $fields[$fname];
                foreach($datas as $i=>$data) {
                    $description = $field->get_label($data);
                    $mform->addElement('checkbox', 'filter['.$fname.']['.$i.']', null, $description);
                }
            }

            if ($extraparams) {
                foreach ($extraparams as $key=>$value) {
                    $mform->addElement('hidden', $key, $value);
                    $mform->setType($key, PARAM_RAW);
                }
            }

            $objs = array();
            $objs[] = &$mform->createElement('submit', 'removeselected', get_string('removeselected','filters'));
            $objs[] = &$mform->createElement('submit', 'removeall', get_string('removeall','filters'));
            $mform->addElement('group', 'actfiltergrp', '', $objs, ' ', false);
        }
    }
}
