<?php

require_once($CFG->libdir.'/formslib.php');

class learnercourse_add_filter_form extends moodleform {

    function definition() { 
        $mform       =& $this->_form;
        $fields      = $this->_customdata['fields'];
        $extraparams = $this->_customdata['extraparams'];

        $mform->addElement('hidden', 'type', isset($_REQUEST['type'])?$_REQUEST['type']:1);
		$mform->setDefault('value',1);
		
		$mform->addElement('header', 'newfilter', get_string('newfilter','filters'));

        foreach($fields as $ft) {
            $ft->setupForm($mform);
        }

        // in case we wasnt to track some page params
        if ($extraparams) {
            foreach ($extraparams as $key=>$value) {
                $mform->addElement('hidden', $key, $value);
                $mform->setType($key, PARAM_RAW);
            }
        }

        // Add button
        $mform->addElement('submit', 'addfilter', get_string('addfilter','filters'));
		$mform->addElement('submit', 'removeall', get_string('removeall','filters')); // added by rajesh

    }
}

