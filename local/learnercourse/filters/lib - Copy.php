<?php

require_once($CFG->dirroot.'/local/learnercourse/filters/text.php');
require_once($CFG->dirroot.'/local/learnercourse/filters/checkbox.php');
require_once($CFG->dirroot.'/local/learnercourse/filters/select.php');
require_once($CFG->dirroot.'/local/learnercourse/filters/simpleselect.php');
require_once($CFG->dirroot.'/local/learnercourse/filters/date.php');
require_once($CFG->dirroot.'/local/learnercourse/filters/learnercourse_filter_forms.php');
/**
 * learnercourse filtering wrapper class.
 */

class learnercourse_filtering {
    var $_fields;
    var $_addform;
    var $_activeform;

    /**
     * Contructor
     * @param array array of visible learnercourse fields
     * @param string base url used for submission/return, null if the same of current page
     * @param array extra page parameters
     */
    function learnercourse_filtering($fieldnames=null, $baseurl=null, $extraparams=null) {
        global $SESSION;

        if (!isset($SESSION->learnercourse_filtering)) {
            $SESSION->learnercourse_filtering = array();
        }

        if (empty($fieldnames)) {  
            //$fieldnames = array('fullname'=>0, 'categoryname'=>1, 'timecreated'=>1, 'timemodified'=>1);
			
			//if(isset($SESSION->learnercourse_filtering['categoryname']) || isset($SESSION->learnercourse_filtering['timecreated'])){
			  // $fieldnames = array('fullname'=>0, 'categoryname'=>0,'timecreated'=>0);
			//}else{
			  $fieldnames = array('fullname'=>0, 'categoryname'=>1,'timecreated'=>1);
			//}
			
        }else{
		
		       //if(isset($SESSION->learnercourse_filtering['ctimecreated'])){
			    // $fieldnames = array('resoursename'=>0, 'ctimecreated'=>0);
			  // }
		
		}


        $this->_fields  = array();

        foreach ($fieldnames as $fieldname=>$advanced) {
            if ($field = $this->get_field($fieldname, $advanced)) {
                $this->_fields[$fieldname] = $field;
            }
        }
		
		//pr($fieldnames);

        // fist the new filter form
        $this->_addform = new learnercourse_add_filter_form($baseurl, array('fields'=>$this->_fields, 'extraparams'=>$extraparams));
		
		
		 
        if ($adddata = $this->_addform->get_data()) { 
		
		    unset($SESSION->learnercourse_filtering);

		    if (!empty($adddata->removeall)) { 
                //$SESSION->learnercourse_filtering = array();
				//$SESSION->learnercourse_filtering = '';
				unset($SESSION->learnercourse_filtering);
				unset($adddata);
				unset($_POST);
				
				
            }elseif (!empty($adddata->addfilter)) { 
			
			
				foreach($this->_fields as $fname=>$field) {
					$data = $field->check_data($adddata);
					if ($data === false) {
						continue; // nothing new
					}
					
					if (!array_key_exists($fname, $SESSION->learnercourse_filtering)) { 
						$SESSION->learnercourse_filtering[$fname] = array();
					}
					//$SESSION->learnercourse_filtering[$fname][] = $data;
					$SESSION->learnercourse_filtering[$fname][0] = $data;
				}
				
				// die;
			}

            // clear the form
            $_POST = array();
            $this->_addform = new learnercourse_add_filter_form($baseurl, array('fields'=>$this->_fields, 'extraparams'=>$extraparams));
			
        }

        // now the active filters
        /*$this->_activeform = new learnercourse_active_filter_form($baseurl, array('fields'=>$this->_fields, 'extraparams'=>$extraparams));
        if ($adddata = $this->_activeform->get_data()) {
            if (!empty($adddata->removeall)) {
                $SESSION->learnercourse_filtering = array();

            } else if (!empty($adddata->removeselected) and !empty($adddata->filter)) {
                foreach($adddata->filter as $fname=>$instances) {
                    foreach ($instances as $i=>$val) {
                        if (empty($val)) {
                            continue;
                        }
                        unset($SESSION->learnercourse_filtering[$fname][$i]);
                    }
                    if (empty($SESSION->learnercourse_filtering[$fname])) {
                        unset($SESSION->learnercourse_filtering[$fname]);
                    }
                }
            }
            // clear+reload the form
            $_POST = array();
            $this->_activeform = new learnercourse_active_filter_form($baseurl, array('fields'=>$this->_fields, 'extraparams'=>$extraparams));
        }*/
        // now the active filters
    }

    /**
     * Creates known learnercourse filter if present
     * @param string $fieldname
     * @param boolean $advanced
     * @return object filter
     */
    function get_field($fieldname, $advanced) {
        global $USER, $CFG, $DB, $SITE;
		
		$learnerCategoryRec = get_learnercourses_category(); 
		$learnerCategory = array();
		if(count($learnerCategoryRec) > 0){
		  foreach($learnerCategoryRec as $lc){
		     $learnerCategory[$lc->name] = $lc->name;
		  }
		}

        switch ($fieldname) {
            case 'resoursename':    return new learnercourse_filter_text('resoursename', get_string('resoursename','learnercourse'), $advanced, 'mr.name');
			case 'ctimecreated':    return new learnercourse_filter_date('ctimecreated', get_string('timecreated','learnercourse'), $advanced, 'mc.timecreated');
			case 'fullname':    return new learnercourse_filter_text('fullname', get_string('fullname','learnercourse'), $advanced, 'fullname');
			//case 'categoryname': return new learnercourse_filter_checkbox('categoryname', get_string('category', 'learnercourse'), $advanced, 'categoryname', array('lastaccess_sck', 'lastaccess_eck', 'firstaccess_eck', 'firstaccess_sck'));
			case 'categoryname':     return new learnercourse_filter_simpleselect('categoryname', get_string('category','learnercourse'), $advanced, 'mcc.name', $learnerCategory);
			case 'timecreated':    return new learnercourse_filter_date('timecreated', get_string('timecreated','learnercourse'), $advanced, 'mc.timecreated');
			//case 'timemodified':    return new learnercourse_filter_date('timemodified', get_string('timemodified','learnercourse'), $advanced, 'timemodified');
            default:            return null;
        }
    }

    /**
     * Returns sql where statement based on active user filters
     * @param string $extra sql
     * @param array named params (recommended prefix ex)
     * @return array sql string and $params
     */
    function get_sql_filter($extra='', array $params=null) {
        global $SESSION;

        $sqls = array();
        if ($extra != '') {
            $sqls[] = $extra;
        }
        $params = (array)$params;

        if (!empty($SESSION->learnercourse_filtering)) {
            foreach ($SESSION->learnercourse_filtering as $fname=>$datas) {
                if (!array_key_exists($fname, $this->_fields)) {
                    continue; // filter not used
                }
                $field = $this->_fields[$fname];
                foreach($datas as $i=>$data) {
                    list($s, $p) = $field->get_sql_filter($data);
                    $sqls[] = $s;
					
                    $params = $params + $p;
                }
            }
        }

        if (empty($sqls)) {
            return array('', array());
        } else {
            $sqls = implode(' AND ', $sqls);
            return array($sqls, $params);
        }
    }

    /**
     * Print the add filter form.
     */
    function display_add() {
        $this->_addform->display();
    }

    /**
     * Print the active filter form.
     */
    function display_active() {
        $this->_activeform->display();
    }

}

/**
 * The base user filter class. All abstract classes must be implemented.
 */
class learnercourse_filter_type {
    /**
     * The name of this filter instance.
     */
    var $_name;

    /**
     * The label of this filter instance.
     */
    var $_label;

    /**
     * Advanced form element flag
     */
    var $_advanced;

    /**
     * Constructor
     * @param string $name the name of the filter instance
     * @param string $label the label of the filter instance
     * @param boolean $advanced advanced form element flag
     */
    function learnercourse_filter_type($name, $label, $advanced) {
        $this->_name     = $name;
        $this->_label    = $label;
        $this->_advanced = $advanced;
    }

    /**
     * Returns the condition to be used with SQL where
     * @param array $data filter settings
     * @return string the filtering condition or null if the filter is disabled
     */
    function get_sql_filter($data) {
        print_error('mustbeoveride', 'debug', '', 'get_sql_filter');
    }

    /**
     * Retrieves data from the form data
     * @param object $formdata data submited with the form
     * @return mixed array filter data or false when filter not set
     */
    function check_data($formdata) {
        print_error('mustbeoveride', 'debug', '', 'check_data');
    }

    /**
     * Adds controls specific to this filter in the form.
     * @param object $mform a MoodleForm object to setup
     */
    function setupForm(&$mform) {
        print_error('mustbeoveride', 'debug', '', 'setupForm');
    }

    /**
     * Returns a human friendly description of the filter used as label.
     * @param array $data filter settings
     * @return string active filter label
     */
    function get_label($data) {
        print_error('mustbeoveride', 'debug', '', 'get_label');
    }
}
