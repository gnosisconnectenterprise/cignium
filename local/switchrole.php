<?php

require('../config.php');
global $USER,$CFG;
$switchrole = optional_param('role', 1, PARAM_INT);
if($USER->archetype == $CFG->userTypeAdmin || $USER->original_archetype == $CFG->userTypeAdmin){
	if($switchrole == 2){
		$USER->original_archetype = $USER->archetype;
		$USER->archetype = $CFG->userTypeStudent;
	}else{
		$USER->original_archetype = $USER->archetype;
		$USER->archetype = $CFG->userTypeAdmin;
	}
}elseif($USER->archetype == $CFG->userTypeManager || $USER->original_archetype == $CFG->userTypeManager){
	if($switchrole == 2){
		$USER->original_archetype = $USER->archetype;
		$USER->archetype = $CFG->userTypeStudent;
	}else{
		$USER->original_archetype = $USER->archetype;
		$USER->archetype = $CFG->userTypeManager;
	}
}elseif($USER->archetype == $CFG->userTypeStudent || $USER->currentRole == $CFG->userTypeStudent){
	if($switchrole == 2){
		$USER->currentRole = $CFG->userTypeStudent;
	}else{
		$USER->currentRole = $CFG->userTypeInstructor;
	}
}
$USER->pageHeader = generatePageHeader();
redirect($CFG->wwwroot .'/index.php?rand='.rand(1000,9999));
?>