<?php
//define('AJAX_SCRIPT', true);

require('../config.php');

//require($CFG->dirroot.'/local/lib/reportlib.php');

global $USER,$DB,$CFG;

$GLOBALS['statusLog'] = array();
$GLOBALS['errorLog'] = array();
$GLOBALS['viewLog'] = array();

$GLOBALS['viewLog']['success'] = array();
$GLOBALS['viewLog']['success']['users'] = array();
$GLOBALS['viewLog']['success']['users']['learner'] = 0;
$GLOBALS['viewLog']['error'] = array();
$GLOBALS['viewLog']['error']['users']['learner'] = 0;

// Must have the sesskey


$action  = required_param('action', PARAM_ALPHANUMEXT);

checkLogin();
$outcome = new stdClass();
$outcome->success = true;
$outcome->response = new stdClass();
$outcome->error = '';

if(isset($SESSION->custom_email_content)){
    unset($SESSION->custom_email_content);
}
switch ($action) {  
	case 'csv_user_upload_into_course':
		$courseid = required_param('courseid', PARAM_INT);
                $custom_email_content = optional_param('custom_email_content','', PARAM_RAW);
                
                if($custom_email_content){
                     $SESSION->custom_email_content = $custom_email_content;
                }
               
                $userUploadResult = array();
                
		$file_name = time().'_'.basename($_FILES["user_list"]["name"]);
		$allowed =  array('csv');
		$filename = $file_name;
		$ext = pathinfo($filename, PATHINFO_EXTENSION);                
		$target_dir = $CFG->dirroot.'/user/upload//'.$file_name;            
                if (in_array($ext,$allowed) && $_FILES["user_list"]["error"] == 0 && move_uploaded_file($_FILES["user_list"]["tmp_name"], $target_dir)) {
			//$userUploadResult = upload_user($target_dir);
                        
                        $userUploadResult = upload_csv_user_into_course($target_dir);
                      // pr($userUploadResult);die;
                        if(isset($SESSION->custom_email_content)){
                            unset($SESSION->custom_email_content);
                        }
                        print_results($userUploadResult);
                }
                
        break;		
	
    default:
        throw new enrol_ajax_exception('unknowajaxaction');
}

//echo json_encode($userUploadResult);
//die();


/**
 * This function is created to upload users in system.
 * @param string $target_dir => uploaded csv file path(complete)
 * @return array $resultArray => status array for this upload.
*/

function upload_csv_user_into_course($target_dir) {
	GLOBAL $DB,$CFG,$USER,$statusLog,$errorLog,$viewLog;
	$i = 0;
	$errorFile = 0;
	
	$statusLog['success']['learner'] = 0;      
        
	$statusLog['fail'] = 0;
        
	/* **** User Upload Start **** */
	// Open uploaded user file
	$csvDataArray = array();
	$rowCount = 0;
	if (($handle = fopen($target_dir, "r")) !== FALSE) {
		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
		{
			$inx = count($data);
			$rowCount++;
			if($rowCount == 1){
				$dataArray = array('Username*','Enrollment End Date(MM/DD/YYYY)');
				if($data === $dataArray){ // Correct file
					$errorLog[$rowCount] = $data;
					$errorLog[$rowCount][$inx] = 'Status'; // Add another column to show error.
				}else{ // File mismatch
					$errorLog[$rowCount] = $data;
					$j = $rowCount+1;
					$errorLog[$j][] = get_string('invalid_csv_file');
					$errorFile = 1;
					break;
				}
			}else{
				$flatRow = trim(implode('', $data));
				if($flatRow == ''){
					$errorLog[$rowCount] = array();
				}else{
					$data['curr_index'] = $rowCount;
					$csvDataArray['learner'][] = $data;
					$errorLog[$rowCount] = $data;
				}
			}
		}
	}  
      
	if ($errorFile == 0) { 
		
		if(!empty($csvDataArray['learner'])){ 
                    //pr($csvDataArray['learner']);
			foreach($csvDataArray['learner'] as $csvDataLearnerRow){
				uploadUserInToCourse($csvDataLearnerRow);
			}
		}
	}
	// Close uploaded user file
	fclose($handle);
	/* **** User Upload ends here **** */

	$resultArray = array();
	
		if($errorFile == 1){ // Invalid file format
			$resultArray['status'] = 2;
			$resultArray['message'] = get_string('invalid_csv_file');
			return $resultArray;
		}else{ // error in file data
			if(count($viewLog['error']['log']) < 1){ // Data inserted successfully
				$resultArray['status'] = 1;
			}else{
				$resultArray['status'] = 3;
			}
			$filename = time().'_enrollment_status_log.csv';
			$file = $CFG->dirroot."/user/download/user_log//".$filename;
			$fp = fopen($file, 'w');
                       // pr($errorLog);die;
			foreach ($errorLog as $fields) {
				fputcsv($fp, $fields);
			}
			fclose($fp);
			$resultArray['message'] = $filename;
			$resultArray['upload_log'] = $statusLog;
			$resultArray['viewLog'] = $viewLog;
                        
                        //pr($resultArray);die;
			return $resultArray;
		}
	//}
}


function uploadUserInToCourse($data){
	GLOBAL $USER,$DB,$CFG,$statusLog,$errorLog,$viewLog,$courseid;
        
        $errStr = array();
        $err = 0;
	$i = 0;
	$inx = count($data);
        
        $username = $data[0];
        $enroll_end_date = trim($data[1]);
       // echo $enroll_end_date;
       // echo strtotime($enroll_end_date);die;
        if($enroll_end_date){
            
            list($m, $d, $y) = explode('/', $enroll_end_date);

            if(checkdate($m, $d, $y)){
                $enroll_end_date = strtotime($enroll_end_date);
                $end_date = $enroll_end_date;
                if($enroll_end_date < time()){                
                    $err = 1;
                    $errStr[] = get_string('enroll_end_date_check');                
                    $cntInx = $inx-1;
                    $CsvRow = $data['curr_index'];
                    unset($errorLog[$CsvRow]['curr_index']);
                    // insert error log in array		
                    $errorLog[$CsvRow][$cntInx] = 'Error: '.implode('; ',$errStr).';';
                    $viewLog['error']['log'][$CsvRow] = "#".$CsvRow.' Error: '.implode('; ',$errStr).';';
                    $viewLog['error']['users']['learner'] = $viewLog['error']['users']['learner'] + 1;
                    $statusLog['fail'] = $statusLog['fail'] + 1; 

                }
            }else{
                
                    $err = 1;
                    $errStr[] = get_string('invaliddateformat');                
                    $cntInx = $inx-1;
                    $CsvRow = $data['curr_index'];
                    unset($errorLog[$CsvRow]['curr_index']);
                    // insert error log in array		
                    $errorLog[$CsvRow][$cntInx] = 'Error: '.implode('; ',$errStr).';';
                    $viewLog['error']['log'][$CsvRow] = "#".$CsvRow.' Error: '.implode('; ',$errStr).';';
                    $viewLog['error']['users']['learner'] = $viewLog['error']['users']['learner'] + 1;
                    $statusLog['fail'] = $statusLog['fail'] + 1; 
                
            }
            
            
        }else{
            $end_date = "";//strtotime(date('Y-m-d', strtotime('+1 years')));
        }        
        
        if(trim($username) == ''){
		$err = 1;
		$errStr[] = get_string('user_name_check');                
                
	}
        
        if($err == 1){		
                
                $cntInx = $inx-1;
		$CsvRow = $data['curr_index'];
		unset($errorLog[$CsvRow]['curr_index']);
		// insert error log in array		
		$errorLog[$CsvRow][$cntInx] = 'Error: '.implode('; ',$errStr).';';
		$viewLog['error']['log'][$CsvRow] = "#".$CsvRow.' Error: '.implode('; ',$errStr).';';
		$viewLog['error']['users']['learner'] = $viewLog['error']['users']['learner'] + 1;
		$statusLog['fail'] = $statusLog['fail'] + 1;
	}else{
                        
                        $cntInx = $inx-1;
                        $CsvRow = $data['curr_index'];
                        unset($errorLog[$CsvRow]['curr_index']);
                        $userExists = $DB->get_record('user',array('username'=>$username,'deleted'=>0),'id,suspended');
                       // pr($userExists);
                        //$userExists = isUserAlreadyExist($username); // check if user already exists
			if(!$userExists){                          
				$err = 1;                                
				$errStr[] = get_string('user_doesnot_exists');
                                			
                                $errorLog[$CsvRow][$cntInx] = 'Error: '.get_string('user_doesnot_exists');
                                $viewLog['error'][] = "#".$CsvRow.'Error: '.get_string('user_doesnot_exists');
                                $viewLog['error']['log'][$CsvRow] = "#".$CsvRow.'Error: '.get_string('user_doesnot_exists');
                                $statusLog['fail'] = $statusLog['fail'] + 1;
                        }else{
                            if($userExists->suspended==1){
                                $err = 1;                                
				$errStr[] = get_string('user_deactivated');
                                			
                                $errorLog[$CsvRow][$cntInx] = 'Error: '.get_string('user_deactivated');
                                $viewLog['error'][] = "#".$CsvRow.'Error: '.get_string('user_deactivated');
                                $viewLog['error']['log'][$CsvRow] = "#".$CsvRow.'Error: '.get_string('user_deactivated');
                                $statusLog['fail'] = $statusLog['fail'] + 1;
                            }else{
                            $userid = $userExists->id;
                            $courseArr = array($courseid);                           
                            
                            try{
                                $cc_emails = optional_param('cc_emails','',PARAM_RAW);
                                $mapCondition = array('courseid'=>$courseid, 'userid'=>$userid,'status'=>1);
                                $mappingExists = $DB->get_record('user_course_mapping',$mapCondition);
                                if($mappingExists){ //update
                                    
                                    $completionStatus = $DB->get_record('online_course_usage_details',array('course_id'=>$courseid,'user_id'=>$userid,'course_status'=>'completed'));
                                    if($completionStatus){
                                        //$userAssignedIntoCourse = assignCourseToUser($userid, $courseArr, 0, $end_date, 1);
                                        $errorLog[$CsvRow][$cntInx] = 'Already completed course';                                    
                                        $viewLog['success']['users']['log'][$CsvRow] = "#".$CsvRow.' Already completed course';                                    
                                        
                                    }else{
                                        //pr($mappingExists);
                                       // echo $end_date;die;
                                        if($mappingExists->end_date != $end_date){
                                            $userAssignedIntoCourse = assignCourseToUser($userid, $courseArr, 1, $end_date, 1,$cc_emails);
                                        
                                            $errorLog[$CsvRow][$cntInx] = 'Enrollment date updated';                                    
                                            $viewLog['success']['users']['log'][$CsvRow] = "#".$CsvRow.' User Enrollment date updated';                                    
                                            $statusLog['update']['learner'] = $statusLog['update']['learner'] + 1;
                                            $viewLog['update']['users']['learner'] = $viewLog['udpate']['users']['learner'] + 1;
                                        }else{
                                            $errorLog[$CsvRow][$cntInx] = 'Already Enrolled';                                    
                                            $viewLog['success']['users']['log'][$CsvRow] = "#".$CsvRow.' User Already Enrolled';                                    
                                            $statusLog['already_enrolled']['learner'] = $statusLog['already_enrolled']['learner'] + 1;
                                            $viewLog['already_enrolled']['users']['learner'] = $viewLog['already_enrolled']['users']['learner'] + 1;
                                            
                                        }
                                        
                                        
                                    }
                                     
                                }else{
                                    $userAssignedIntoCourse = assignCourseToUser($userid, $courseArr, 1, $end_date, 0,$cc_emails);
                                    $errorLog[$CsvRow][$cntInx] = 'Enrolled';
                                    
                                    $viewLog['success']['users']['log'][$CsvRow] = "#".$CsvRow.' User Enrolled';                                    
                                    $statusLog['success']['learner'] = $statusLog['success']['learner'] + 1;
                                    $viewLog['success']['users']['learner'] = $viewLog['success']['users']['learner'] + 1;  
                                }
                               
                                
                            } catch (Exception $ex) {
                                    $err = 1;
                                    $errorLog[$CsvRow][$cntInx] = 'Error: '.get_string('error_uploading_user');
                                    $viewLog['error'][] = "#".$CsvRow.'Error: '.get_string('error_uploading_user');
                                    $statusLog['fail'] = $statusLog['fail'] + 1;
                            }                   
                            
                        }
                       }
		
	}
}

function print_results($userUploadResult){
    
    if($userUploadResult['status'] == 1){
            $update_msg = get_string('enrolled_success');

            echo '<div class="clear"></div>';
            echo '<div class="bg-success text-success text-center" style = "display:block !important;">'.$update_msg.'</div>';
            echo '<div class="clear"></div>';
    }elseif($userUploadResult['status'] == 2){
           $update_msg = $userUploadResult['message'];
           
            echo '<div class="clear"></div>';
            echo '<div class="bg-success text-success text-center error_bg" style = "display:block !important;">'.$update_msg.'</div>';
            echo '<div class="clear"></div>';


            $userUploadResult = array();
    }  
    
    if(!empty($userUploadResult)){       
                
                        $fileDownload = $userUploadResult['message'];
                        $userUploadResult = $userUploadResult['viewLog'];
                        $errorCount = count($userUploadResult['error']['log']);
		
		?>
        <div class="clear"></div>
		<div class = "status_report_main">
			<?php
				//if(count($userUploadResult['error']['log'])>0){
			?>
			<div class = "download_report">
				<span> <?php echo get_string('error_complete_user_csv').'<U><a href="'.$CFG->wwwroot.'/local/download.php?download=4&filename='.$fileDownload.'" class="download-sample">here</a>.</U>'; ?></span>
			</div>
			<?php
				//}
			?>
			<?php
			if(count($userUploadResult['error']['log'])>0){
			?>
				<div class = "error_block_main">
					<div class = "error_report_header">Error Report</div>
					<div class = "error_user_report">
						<ul class = "error_user_list">
							<li><?php echo $errorCount ; ?> user(s) not enrolled.
						<?php
							if(count($userUploadResult['error']['log'])>0){
								echo '<ul>';
								ksort($userUploadResult['error']['log']);
								foreach($userUploadResult['error']['log'] as $log){
									?>
									<li class = "error_user"><?php echo $log; ?></li>
									<?php
								}
								echo '</ul>';
							}
							?>
                            </li>
                            </ul>
                            
					</div>
				</div>
			<?php
			}
			?>
			<div class = "success_block_main">
				<div class = "success_report_header">Success Report</div>
				<div class = "success_report">				
					
					<div class = "user_report">
						<span class = "user_header">
						<?php 
						
						$learnerCount = ($userUploadResult['success']['users']['learner'])?$userUploadResult['success']['users']['learner']:0;
                                                $learnerUpdateCount = ($userUploadResult['update']['users']['learner'])?$userUploadResult['update']['users']['learner']:0;
                                                $learnerAlreadyEnrolledCount = ($userUploadResult['already_enrolled']['users']['learner'])?$userUploadResult['already_enrolled']['users']['learner']:0;
						echo $learnerCount + $learnerUpdateCount; ?> user(s) enrolled/updated.</span>
						<ul class = "user_list">							
							<li class = "user_name"><?php echo $learnerCount; ?> learner(s) enrolled.</li>
                                                        <li class = "user_name"><?php echo $learnerUpdateCount; ?> learner(s) enrollment updated.</li>
                                                         <li class = "user_name"><?php echo $learnerAlreadyEnrolledCount; ?> learner(s) already enrolled.</li>
						</ul>
					</div>                                   
                                        
				</div>
			</div>
		</div>
		<?php
	}
    
}