<?php

/////////////////////////////////////////////////////////////////////////////
//                                                                         //
// NOTICE OF COPYRIGHT                                                     //
//                                                                         //
// Moodle - Calendar extension                                             //
//                                                                         //
// Copyright (C) 2003-2004  Greek School Network            www.sch.gr     //
//                                                                         //
// Designed by:                                                            //
//     Avgoustos Tsinakos (tsinakos@teikav.edu.gr)                         //
//     Jon Papaioannou (pj@moodle.org)                                     //
//                                                                         //
// Programming and development:                                            //
//     Jon Papaioannou (pj@moodle.org)                                     //
//                                                                         //
// For bugs, suggestions, etc contact:                                     //
//     Jon Papaioannou (pj@moodle.org)                                     //
//                                                                         //
// The current module was developed at the University of Macedonia         //
// (www.uom.gr) under the funding of the Greek School Network (www.sch.gr) //
// The aim of this project is to provide additional and improved           //
// functionality to the Asynchronous Distance Education service that the   //
// Greek School Network deploys.                                           //
//                                                                         //
// This program is free software; you can redistribute it and/or modify    //
// it under the terms of the GNU General Public License as published by    //
// the Free Software Foundation; either version 2 of the License, or       //
// (at your option) any later version.                                     //
//                                                                         //
// This program is distributed in the hope that it will be useful,         //
// but WITHOUT ANY WARRANTY; without even the implied warranty of          //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           //
// GNU General Public License for more details:                            //
//                                                                         //
//          http://www.gnu.org/copyleft/gpl.html                           //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////

//  Display the calendar page.

require_once('../config.php');
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot.'/calendar/lib.php');

require_login();

$courseid = optional_param('course', SITEID, PARAM_INT);
$view = optional_param('view', 'upcoming', PARAM_ALPHA);
$day = optional_param('cal_d', 0, PARAM_INT);
$mon = optional_param('cal_m', 0, PARAM_INT);
$year = optional_param('cal_y', 0, PARAM_INT);
$time = optional_param('time', 0, PARAM_INT);

$url = new moodle_url('/calendar/view.php');
//echo date("Y-m-d H:i:s", 1413524400);die;
 // its defined in event/event.php
$_SESSION['sess_add_event_from'] = '';
unset($_SESSION['sess_add_event_from']);

if(!isset($_GET['view']) || (isset($_GET['view']) && !in_array($_GET['view'], array('month','day')))){
 redirect(new moodle_url('/calendar/view.php', array('view' => 'day')));
}

if ($courseid != SITEID) {
    $url->param('course', $courseid);
}

if ($view !== 'upcoming') {
    $url->param('view', $view);
}

// If a day, month and year were passed then convert it to a timestamp. If these were passed
// then we can assume the day, month and year are passed as Gregorian, as no where in core
// should we be passing these values rather than the time. This is done for BC.
if (!empty($day) && !empty($mon) && !empty($year)) {
    if (checkdate($mon, $day, $year)) {
        $time = make_timestamp($year, $mon, $day);
    } else {
        $time = time();
    }
} else if (empty($time)) {
    $time = time();
}

$url->param('time', $time);

$PAGE->set_url($url);

if ($courseid != SITEID && !empty($courseid)) {
    $course = $DB->get_record('course', array('id' => $courseid));
    $courses = array($course->id => $course);
    $issite = false;
    navigation_node::override_active_url(new moodle_url('/course/view.php', array('id' => $course->id)));
} else {
    $course = get_site();
    $courses = calendar_get_default_courses();
    $issite = true;
}

require_course_login($course);

$calendar = new calendar_information(0, 0, 0, $time);
$calendar->prepare_for_view($course, $courses);

$pagetitle = '';

$strcalendar = get_string('calendar', 'calendar');

switch($view) {
    case 'day':
        //$PAGE->navbar->add(userdate($time, get_string('strftimedate')));
        $pagetitle = get_string('dayviewtitle', 'calendar', userdate($time, get_string('strftimedaydate')));
    break;
    case 'month':
       // $PAGE->navbar->add(userdate($time, get_string('strftimemonthyear')));
        $pagetitle = get_string('detailedmonthviewtitle', 'calendar', userdate($time, get_string('strftimemonthyear')));
    break;
    case 'upcoming':
        $pagetitle = get_string('upcomingevents', 'calendar');
    break;
}
$PAGE->navbar->add(get_string('events'));
// Print title and header
$PAGE->set_pagelayout('standard');
$PAGE->set_title("$course->shortname: $strcalendar: $pagetitle");
$PAGE->set_heading($COURSE->fullname);
$PAGE->set_button(calendar_preferences_button($course));

$renderer = $PAGE->get_renderer('core_calendar');
$calendar->add_sidecalendar_blocks($renderer, true, $view);

echo $OUTPUT->header();
echo $renderer->start_layout();
echo html_writer::start_tag('div', array('class'=>'heightcontainer'));

switch($view) {
    case 'day':
        echo $renderer->show_day($calendar);
    break;
    case 'month':
        echo $renderer->show_month_detailed($calendar, $url);
    break;
    case 'upcoming':
        $defaultlookahead = CALENDAR_DEFAULT_UPCOMING_LOOKAHEAD;
        if (isset($CFG->calendar_lookahead)) {
            $defaultlookahead = intval($CFG->calendar_lookahead);
        }
        $lookahead = get_user_preferences('calendar_lookahead', $defaultlookahead);

        $defaultmaxevents = CALENDAR_DEFAULT_UPCOMING_MAXEVENTS;
        if (isset($CFG->calendar_maxevents)) {
            $defaultmaxevents = intval($CFG->calendar_maxevents);
        }
        $maxevents = get_user_preferences('calendar_maxevents', $defaultmaxevents);
        echo $renderer->show_upcoming_events($calendar, $lookahead, $maxevents);
    break;
}

/********* Commented the entire export section as discussed with Management - Madhab ********/
/*
//Link to calendar export page.
echo $OUTPUT->container_start('bottom');
if (!empty($CFG->enablecalendarexport)) {
    echo $OUTPUT->single_button(new moodle_url('export.php', array('course'=>$courseid)), get_string('exportcalendar', 'calendar'));
    if (calendar_user_can_add_event($course)) {
        //echo $OUTPUT->single_button(new moodle_url('/calendar/managesubscriptions.php', array('course'=>$courseid)), get_string('managesubscriptions', 'calendar'));
    }
    if (isloggedin()) {
        $authtoken = sha1($USER->id . $DB->get_field('user', 'password', array('id'=>$USER->id)) . $CFG->calendar_exportsalt);
        $link = new moodle_url('/calendar/export_execute.php', array('preset_what'=>'all', 'preset_time'=>'recentupcoming', 'userid' => $USER->id, 'authtoken'=>$authtoken));
        $icon = html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/ical'), 'height'=>'14', 'width'=>'36', 'alt'=>get_string('ical', 'calendar'), 'title'=>get_string('quickdownloadcalendar', 'calendar')));
        echo html_writer::tag('a', $icon, array('href'=>$link));
    }
}
echo $OUTPUT->container_end();
*/

echo html_writer::end_tag('div');
echo $renderer->complete_layout();
?>
<script>


function loadCalendar(viewType, time, target){

    $('.eventlist').html('<?php echo get_string('loadingcalender', 'calendar');?>');
    var viewType = viewType;
	var timeVal = typeof(time) == 'undefined' ? 0 : time;
	var target = target;
	var calenderUrl = '<?php echo $CFG->wwwroot;?>/local/calender/<?php echo $CFG->pageAjaxCalendar;?>';

	if(viewType && timeVal){
	  dataVal = 'view='+viewType+'&time='+timeVal;
	}else if(viewType == '' && timeVal){
	  dataVal = 'time='+timeVal;
	}else if(viewType && timeVal == ''){
	  dataVal = 'view='+viewType;
	}else{
	  dataVal = '';
	}
	
	 if(dataVal!= ''){
	   calenderUrl = '<?php echo $CFG->wwwroot;?>/local/calender/<?php echo $CFG->pageAjaxCalendar;?>?'+dataVal;
	 }
		
    $.ajax({
	  
	    url:'<?php echo $CFG->wwwroot;?>/local/calender/<?php echo $CFG->pageAjaxCalendar;?>',
		type:'GET',
		data:dataVal,
		dataType:'html',
		success:function(data){

          if(data!=''){
            var heightContainerData = $('.heightcontainer', data);
			$('.maincalendar').html(heightContainerData);

				
				if(viewType == 'day'){

					  $('.commands a.edit').each(function(key, val){
		
						  $(this).attr('href', $(this).attr('href')+"&viewType="+viewType+'&timeVal='+timeVal);
						
					  });
				
					  if(target.length){
		
							$('html, body').animate({
								scrollTop: ($('[name = "'+target+'"]').offset().top - 100)
							},500);
						
					  }else{
						$('html, body').animate({
							scrollTop: ($('body').offset().top - 100)
						},500);
					  }
				}
		
		  }
		  
		  $('.maincalendar').append('<input type="hidden" name="calender_url" id="calender_url" value="'+calenderUrl+'" />');
			   
		}
	  
	  });
	  

}



$(document).ready(function(){

 
   $('.maincalendar').append('<input type="hidden" name="calender_url" id="calender_url" value="'+window.location+'" />');
   
   <?php  
    if(isset($_GET['view']) && $_GET['view'] == 'day'){ ?>
		
		if(window.location.hash.length > 0 ){

            var targetL = window.location.hash.slice(1); 
			$('html, body').animate({
				scrollTop: ($('[name = "'+targetL+'"]').offset().top - 100)
			},500);
		}
		
		
		
   <?php }  ?>
 
   <?php  
         if(isset($_GET['view']) && $_GET['view']!='' && isset($_GET['time']) && $_GET['time']!=''){ ?>

			  $('.commands a.edit').each(function(key, val){

				  $(this).attr('href', $(this).attr('href')+'&viewType=<?php echo $_GET['view'];?>&timeVal=<?php echo $_GET['time'];?>');
				
			  });
					  
   <?php }  ?>
				

    var re = /([^&=]+)=?([^&]*)/g;
		var decodeRE = /\+/g; // Regex for replacing addition symbol with a space
		var decode = function (str) {return decodeURIComponent( str.replace(decodeRE, " ") );};
		$.parseParams = function(query) {
				var params = {}, e;
				while ( e = re.exec(query) ) {
				var k = decode( e[1] ), v = decode( e[2] );
				if (k.substring(k.length - 2) === '[]') {
				k = k.substring(0, k.length - 2);
				(params[k] || (params[k] = [])).push(v);
				}
				else params[k] = v;
				}
				return params;
	};

	$(document).on('click', '.day-view', function(event){
	    
		event.preventDefault();
		$('.month-view').removeClass('active');
		$(this).addClass('active');
	    loadCalendar('day', 0, 0);
	
	});
	
	$(document).on('click', '.month-view', function(event){
	    
		event.preventDefault();
		$('.day-view').removeClass('active');
		$(this).addClass('active');
	    loadCalendar('month', 0, 0);
	
	});
	
	$(document).on('click', '.maincalendar .previous, .maincalendar .next', function(event){
	    
		event.preventDefault();
		var viewType = 'day';
		var url = $(this).attr('href');
		var jsonData = $.parseParams( url.split('?')[1] || '' );
		
	    viewType = jsonData.view;
	    time = jsonData.time.length > 0 ? jsonData.time: 0 ;
	    loadCalendar(viewType, time, 0);
	
	});
	
	$(document).on('click', '.calendar_event_classroom a:not(.delete, .edit), .calendar_event_course a:not(.delete, .edit), .calendar_event_global a:not(.delete, .edit), .calendar_event_user a:not(.delete, .edit), .maincalendar table.calendarmonth td div.day a,  .maincalendar table.calendarmonth td a.more-event', function(event){
	    
		
		var viewType = 'day';
		var url = $(this).attr('href');
		var jsonData = $.parseParams( url.split('?')[1] || '' );

        target = this.hash.slice(1);
	    viewType = jsonData.view;
		
		if(jsonData.time == undefined){
		 window.document.href=url;
		}else{
		  event.preventDefault();
	      time = jsonData.time.length > 0 ? jsonData.time: 0 ;
	      loadCalendar(viewType, time, target);
		}
	
	});
	
	$(document).on('click', '.calendar_event_classroom a.delete, .calendar_event_course a.delete, .calendar_event_global a.delete, .calendar_event_user a.delete', function(event){
	    
		event.preventDefault();
		var deleteMsg = '<?php echo get_string('confirmeventdelete', 'calendar'); ?>';
		if(confirm(deleteMsg)){
           var url = $(this).attr('href');
		   var deleteUrl = url.replace("event/delete.php","local/calender/delete_event.php");
		   var tableToBeDeleted = $(this).parents('table.event').attr('id');
		   $('#'+tableToBeDeleted).html('<tr><td class="deletingevent"><?php echo get_string('deletingevent', 'calendar'); ?></td></tr>');
		    $.ajax({
	  
				url:deleteUrl,
				type:'GET',
				data:'sesskey=<?php echo sesskey();?>',
				dataType:'html',
				success:function(data){
		          
				    if(data == 'success'){
					    $('#'+tableToBeDeleted).html('<tr><td class="eventdeletedsuccessfully"><?php echo get_string('eventdeletedsuccessfully', 'calendar'); ?></td></tr>');
					    $('#'+tableToBeDeleted).fadeOut(500, function() { $('#'+tableToBeDeleted).remove(); });
					    var calenderUrl = $('#calender_url').val();
						var viewType = 'day';
						var jsonData = $.parseParams( calenderUrl.split('?')[1] || '' );

						viewType = typeof(jsonData.view) == 'undefined' ? 0: jsonData.view ;
						time = typeof(jsonData.time) == 'undefined' ? 0: jsonData.time ;
						loadCalendar(viewType, time, 0);
					}else{
					  $('#'+tableToBeDeleted).html('<tr><td class="eventnotdeletedsuccessfully"><?php echo get_string('eventnotdeletedsuccessfully', 'calendar'); ?></td></tr>');
					}
					
		
					   
				}
		  
		  });
	  
		 	
		  
		}else{
		   return false;
		}
		
	
	});
	
	
	
});


</script>

<?php 
echo $OUTPUT->footer();