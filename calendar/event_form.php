<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The mform for creating and editing a calendar event
 *
 * @copyright 2009 Sam Hemelryk
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package calendar
 */

 /**
  * Always include formslib
  */
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->dirroot.'/lib/formslib.php');

/**
 * The mform class for creating and editing a calendar
 *
 * @copyright 2009 Sam Hemelryk
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class event_form extends moodleform {
    /**
     * The form definition
     */
    function definition () {
        global $CFG, $USER, $OUTPUT, $DB;
        $mform = $this->_form;
		//pr($mform);die;
        $newevent = (empty($this->_customdata->event) || empty($this->_customdata->event->id));
        $repeatedevents = (!empty($this->_customdata->event->eventrepeats) && $this->_customdata->event->eventrepeats>0);
        $hasduration = (!empty($this->_customdata->hasduration) && $this->_customdata->hasduration);
		$mform->addElement('html','<div class="filedset_outer ">');
        $mform->addElement('header', 'general', get_string('general'));
        
        $mform->addElement('hidden', 'timestartformat');
        $mform->addElement('hidden', 'timestartformatuntil');
        $currenttime = time();
        
        $timeformat_selected = 'AM';
        if($CFG->is12HourFormat==1){    	   	
        	
        	$timeFormat = date('A',$currenttime);        	
        	if($timeFormat=='PM'){
        		  $timeformat_selected = 'PM';
        	}
        	$currenttime = setTimeAsPerTimeFormat($currenttime);
        	
        }
        $mform->setDefault('timestartformat', $timeformat_selected);
        $mform->setDefault('timestartformatuntil', $timeformat_selected);
		//// Commented by Madhab, enabling the eventtype and course list available at edit event too /////
        //if ($newevent) {

		$eventtypes = $this->_customdata->eventtypes;
		$options = array();
		//// Commented by Madhab, to show the course list if eventtype selected as course ////
		/*
		if (!empty($eventtypes->courses)) {
			$options['course'] = get_string('course');
		}
		if (!empty($eventtypes->site)) {
			if( $USER->archetype!='student' )
			{
				$options['global'] = get_string('global');
			}
		}
		if (!empty($eventtypes->groups) && is_array($eventtypes->groups)) {
			$options['group'] = get_string('group');
		}
		if (!empty($eventtypes->user)) {
			$options['user'] = get_string('user');
		}*/

		$options['course'] = get_string('course');
		if( $USER->archetype != $CFG->userTypeStudent ){
			$options['global'] = get_string('global');
		}
		$options['user'] = get_string('user');	

		$myselectEventType = $mform->addElement('select', 'eventtype', get_string('eventkind', 'calendar'), $options, 'onChange="javascript:if(this.value == \'course\') {$(\'#fitem_id_courseid\').show();} else {$(\'#fitem_id_courseid\').hide();}"');
		$mform->addRule('eventtype', get_string('required'), 'required', null, 'client');
		$myselectEventType->setSelected("course");

		////// Added by Madhab, to load course list dynamically /////
		$courseOption = array();
		/*if( $USER->archetype != $CFG->userTypeStudent ) {
			$courseQuery = "SELECT c.id, c.fullname FROM {$CFG->prefix}course c" .
						   " WHERE c.id != 1 AND c.is_active = 1" . 
						   " ORDER BY c.fullname ASC";		
		}*/
		if( $USER->archetype == $CFG->userTypeManager ) {			
			//$courseQuery = "select m.id, m.fullname from( SELECT cm.courseid as id FROM {$CFG->prefix}user_course_mapping AS cm WHERE cm.userid = '".$USER->id."' AND cm.status=1 union select c.id from {$CFG->prefix}course AS c WHERE c.createdby = '".$USER->id."' AND c.is_active = 1 union select dc.courseid as id from {$CFG->prefix}department_members as dm left join {$CFG->prefix}department_course as dc on dc.departmentid=dm.departmentid and dc.is_active=1 WHERE dm.userid='53' and dm.is_active=1) as mq left join {$CFG->prefix}course AS m on mq.id = m.id";	
			$courseDetails = getCourseCountByUser('list');		
		}
		if($USER->archetype == $CFG->userTypeAdmin){		
			//$courseQuery = "SELECT c.id, c.fullname FROM {$CFG->prefix}course as c where c.is_active = 1";
			$courseDetails = getCourseCountByUser('list', 'event');					
		}
		if( $USER->archetype == $CFG->userTypeStudent ) {			
			$courseIdArray = getAssignedCourseIdForUser($USER->id, false);					
			$courseId = (count($courseIdArray)>0)?implode(',', $courseIdArray):0;
			$courseQuery = "SELECT c.id, c.fullname FROM {$CFG->prefix}course c WHERE c.id != 1 AND c.id IN (".$courseId.") AND c.is_active = 1 AND c.publish = 1 AND c.coursetype_id='1'  ORDER BY c.fullname ASC";
			$courseDetails = $DB->get_records_sql($courseQuery);				
		}
		
		//pr($courseDetails);die;
		
		foreach($courseDetails as $key => $course){
			$courseOption[$course->id] = ucfirst($course->fullname);
		}
		asort($courseOption);
        
		if(empty($courseOption)){
		  $courseOption = array('0'=>'Select');
		}
		
		$myselectCourse = $mform->addElement('select', 'courseid', get_string('availablecourses'), $courseOption);
		$courseId = optional_param('courseid', 0, PARAM_INT);
		if($courseId != 0) {
			$myselectCourse->setSelected($courseId);
		}
		//$mform->addRule('courseid', get_string('required'), 'nonzero', 'nonzero', 'client');
		$mform->addRule('courseid', get_string('required'), 'required', null, 'client');
		////// Added by Madhab, to load course list dynamically /////

		if (!empty($eventtypes->groups) && is_array($eventtypes->groups)) {
			$groupoptions = array();
			foreach ($eventtypes->groups as $group) {
				$groupoptions[$group->id] = $group->name;
			}
			$mform->addElement('select', 'groupid', get_string('typegroup', 'calendar'), $groupoptions);
			$mform->disabledIf('groupid', 'eventtype', 'noteq', 'group');
		}

		//// Commented by Madhab, enabling the eventtype and course list available at edit event too /////
        //}

        // Add some hidden fields
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        $mform->setDefault('id', 0);
		
		// added by rajesh for setting a query string value for redirection
	    $mform->addElement('hidden', 'viewType');
        $mform->setType('viewType', PARAM_ALPHA);
        $mform->setDefault('viewType', isset($_GET['viewType'])?$_GET['viewType']:0);

        $mform->addElement('hidden', 'timeVal');
        $mform->setType('timeVal', PARAM_INT);
        $mform->setDefault('timeVal', isset($_GET['timeVal'])?$_GET['timeVal']:0);
		
		// end
		
        //$mform->addElement('hidden', 'courseid');
        //$mform->setType('courseid', PARAM_INT);

        $mform->addElement('hidden', 'userid');
        $mform->setType('userid', PARAM_INT);
        $mform->setDefault('userid', $USER->id);

        $mform->addElement('hidden', 'modulename');
        $mform->setType('modulename', PARAM_INT);
        $mform->setDefault('modulename', '');

        $mform->addElement('hidden', 'instance');
        $mform->setType('instance', PARAM_INT);
        $mform->setDefault('instance', 0);

        $mform->addElement('hidden', 'action');
        $mform->setType('action', PARAM_INT);

        // Normal fields
        $mform->addElement('text', 'name', get_string('eventname','calendar'), 'size="50"');
        $mform->addRule('name', get_string('required'), 'required', null, 'client');
        $mform->setType('name', PARAM_TEXT);

        $mform->addElement('editor', 'description', get_string('eventdescription','calendar'), null, $this->_customdata->event->editoroptions);
        $mform->setType('description', PARAM_RAW);

        $mform->addElement('date_time_selector', 'timestart', get_string('date'));
        $mform->addRule('timestart', get_string('required'), 'required', null, 'client');

        $mform->addElement('header', 'durationdetails', get_string('eventduration', 'calendar'));
        $mform->addElement('radio', 'duration', null, get_string('durationnone', 'calendar'), 0);

        $mform->addElement('radio', 'duration', null, get_string('durationuntil', 'calendar'), 1);
        $mform->addElement('date_time_selector', 'timedurationuntil', '&nbsp;');
        $mform->disabledIf('timedurationuntil','duration','noteq', 1);
      
        $mform->setDefault('timedurationuntil',$currenttime);
        $mform->addElement('radio', 'duration', null, get_string('durationminutes', 'calendar'), 2);
        $mform->addElement('text', 'timedurationminutes', null);
        $mform->setType('timedurationminutes', PARAM_INT);
        $mform->disabledIf('timedurationminutes','duration','noteq', 2);

        $mform->setDefault('duration', ($hasduration)?1:0);

        if ($newevent) {

            $mform->addElement('header', 'repeatevents', get_string('repeatedevents', 'calendar'));
            $mform->addElement('checkbox', 'repeat', get_string('repeatevent', 'calendar'), null, 'repeat');
            $mform->addElement('text', 'repeats', get_string('repeatweeksl', 'calendar'), 'maxlength="10" size="10"');
            $mform->setType('repeats', PARAM_INT);
            $mform->setDefault('repeats', 1);
            $mform->disabledIf('repeats','repeat','notchecked');

        } else if ($repeatedevents) {

            $mform->addElement('hidden', 'repeatid');
            $mform->setType('repeatid', PARAM_INT);

            $mform->addElement('header', 'repeatedevents', get_string('repeatedevents', 'calendar'));
            $mform->addElement('radio', 'repeateditall', null, get_string('repeateditall', 'calendar', $this->_customdata->event->eventrepeats), 1);
            $mform->addElement('radio', 'repeateditall', null, get_string('repeateditthis', 'calendar'), 0);

            $mform->setDefault('repeateditall', 1);

        }
		
		$showResetButton = true;
		if(!empty($this->_customdata->event->id)){
		  $showResetButton = false;
		}

        $this->add_action_buttons(true, get_string('savechanges'), $showResetButton);
		$mform->addElement('html','</div>');
    }

    /**
     * A bit of custom validation for this form
     *
     * @param array $data An assoc array of field=>value
     * @param array $files An array of files
     * @return array
     */
    function validation($data, $files) {
        global $DB, $CFG;

        $errors = parent::validation($data, $files);

		///// Added by Madhab, validate course start time if the eventtype is course only /////

		if($data['eventtype'] == 'course') {
			if ($data['courseid'] > 0) {
				if ($course = $DB->get_record('course', array('id'=>$data['courseid']))) {
					if ($data['timestart'] < $course->startdate) {
						$errors['timestart'] = get_string('errorbeforecoursestart', 'calendar');
					}
				} else {
					$errors['courseid'] = get_string('invalidcourse', 'error');
				}

			}else{
			  $errors['courseid'] = get_string('coursecantempty', 'error');
			}
		}

        if ($data['duration'] == 1 && $data['timestart'] >= $data['timedurationuntil']) {
            $errors['timedurationuntil'] = get_string('invalidtimedurationuntil', 'calendar');
        } else if ($data['duration'] == 2 && (trim($data['timedurationminutes']) == '' || $data['timedurationminutes'] < 1)) {
            $errors['timedurationminutes'] = get_string('invalidtimedurationminutes', 'calendar');
        }

        return $errors;
    }

}
