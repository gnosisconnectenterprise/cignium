<?php
require('../config.php');
$username = required_param('username',PARAM_RAW);
$userExists = $DB->get_record('user',array('username'=>$username,'suspended'=>0,'deleted'=>0,'usertype'=>'internal'));

$output = array();
if(!$userExists){
    $output['error']=1;
    $output['error_message']=get_string('user_doesnot_exists');
}else{
    $output['error']=0;
}
$output = json_encode($output);
echo $output;