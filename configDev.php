<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'mysqli';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'localhost';

//echo "<pre>";print_r($_SERVER['HTTP_HOST']);die;
/*if($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '10.1.7.180'){

	$CFG->dbname    = 'moodle26';
	$CFG->dbuser    = '26moodleuser';
	$CFG->dbpass    = '!26moodleDB1680';
	
	$CFG->wwwroot   = 'http://10.1.7.180/moodle';
	$CFG->dataroot  = '/var/www/moodledata';
	$CFG->wwwpath  = '/var/www/moodle';
	
	ini_set ('display_errors', 'on');
	ini_set ('log_errors', 'on');
	ini_set ('display_startup_errors', 'on');
	ini_set ('error_reporting', E_ALL);
	
}elseif($_SERVER['HTTP_HOST'] == 'dev.gnosislms.com' || $_SERVER['HTTP_HOST'] == '10.1.4.26'){

	$CFG->dbhost    = '10.1.7.238';
	$CFG->dbname    = 'moodle26';
	$CFG->dbuser    = '26moodleuser';
	$CFG->dbpass    = '!26moodleDB1680';
	
	/*$CFG->dbhost    = 'localhost';
	$CFG->dbname    = 'gnosislms';
	$CFG->dbuser    = 'root';
	$CFG->dbpass    = '';*/
	/*
	
	$CFG->wwwroot   = 'http://'.$_SERVER['HTTP_HOST'];
	$CFG->dataroot  = 'D:\sites\Gnosis-LMS\moodledata';
	$CFG->wwwpath  = 'D:\sites\Gnosis-LMS\moodle';
	
	ini_set ('display_errors', 'on');
	ini_set ('log_errors', 'on');
	ini_set ('display_startup_errors', 'on');
	ini_set ('error_reporting', E_ALL);
	
}elseif($_SERVER['HTTP_HOST'] == 'gnosislms.infoprolearning.com'){

	$CFG->dbname    = 'gnosislms';
	$CFG->dbuser    = 'gnosisuser';
	$CFG->dbpass    = 'S!Sdbuser';
	
	$CFG->wwwroot   = 'http://gnosislms.infoprolearning.com';
	$CFG->dataroot  = '/var/www/html/gnosislms/moodledata';
	$CFG->wwwpath  = '/var/www/html/gnosislms/gnosislms';
	
	/*ini_set ('display_errors', 'on');
	ini_set ('log_errors', 'on');
	ini_set ('display_startup_errors', 'on');
	ini_set ('error_reporting', E_ALL);*/
  /* 
}
*/
	$CFG->dbhost    = 'localhost';
	$CFG->dbname    = 'gnosislmsdb';
	$CFG->dbuser    = 'root';
	$CFG->dbpass    = '';
	$CFG->wwwroot   = 'http://'.$_SERVER['HTTP_HOST'];
	$CFG->dataroot  = 'D:\sites\Gnosis-LMS\moodledata';
	$CFG->wwwpath  = 'D:\sites\Gnosis-LMS\moodle';
	
	ini_set ('display_errors', 'on');
	ini_set ('log_errors', 'on');
	ini_set ('display_startup_errors', 'on');
	ini_set ('error_reporting', E_ALL);
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => 3306,
  'dbsocket' => '',
);

$CFG->admin     = 'admin';

/*$CFG->custommanagerroleid     = array(1);
$CFG->customstudentroleid     = array(5);
$CFG->customsiteadminroleid     = array(0);
*/

$CFG->directorypermissions = 0777;

// custom variables
$CFG->perPageCount = 5;
$CFG->excludedUsers = array(1,2);
$CFG->customDefaultDateFormat = 'dS M Y h:i A';
$CFG->customDateFormat = 'm/d/Y';
$CFG->reportSearchStartDate = date("m-d-Y", mktime(0, 0, 0, 01, 01, (date('Y'))));
$CFG->reportSearchEndDate = date('m-d-Y', strtotime("+1 years"));
$CFG->programOverviewFilesLimit = 1;
$CFG->programOverviewFilesExt = '.jpg,.gif,.png';
$CFG->courseType = 'course';
$CFG->programType = 'program';
$CFG->programImageFileArea = 'programimage';
$CFG->programMaterialFileArea = 'programmaterial';

//// Open Setting User Type to be used in different places/ validation - Madhab ////
$CFG->userTypeStudent = 'learner';
$CFG->userTypeAdmin = 'admin';
$CFG->userTypeManager = 'manager';
$CFG->userTypeInSystem = array($CFG->userTypeAdmin,$CFG->userTypeStudent,$CFG->userTypeManager);
$CFG->userTypeInSystemForManagerAccess = array($CFG->userTypeStudent);
$CFG->userTypeAccessByManager = array($CFG->userTypeStudent);
//// Closed Setting User Type to be used in different places/ validation - Madhab ////

//// Open Setting Page Name to be used in different places/ validation - Madhab ////
$CFG->pageDashboard = 'dashboard.php';
$CFG->pageAdminDashboard = 'admin_dashboard.php';
$CFG->pageManagerDashboard = 'manager_dashboard.php';
$CFG->pageLearnerDashboard = 'learnerdashboard.php';
$CFG->pageDashboardCalendar = 'dashboard_calender.php';
$CFG->pageDepartmentlib = 'departmentlib.php';
$CFG->pageAddDepartment = 'add_department.php';
$CFG->pageAddDepartmentForm = 'adddepartment_form.php';
$CFG->pageDeleteDepartment = 'delete_department.php';
$CFG->pageDepartmentAssigncourses = 'assigncourses.php';

$CFG->pageTeamAssigncourses = 'assigncourses.php';

$CFG->pageCmsLib = 'cmslib.php';
$CFG->pageDeleteCms = 'delete_cms.php';
$CFG->pageAddCms = 'add_cms.php';
$CFG->pageAddCmsForm = 'addcms_form.php';

$CFG->pageManageBadges = 'managebadges.php';
$CFG->pageEditBadgesForm = 'editbadges_form.php';
$CFG->pageEditBadges = 'editbadges.php';


// Program pages
$CFG->pageAddPrograms= 'addprogram.php';
$CFG->pageProgramAssignCourses = 'assigncourses.php';
$CFG->pageProgramAssignUsers = 'assignusers.php';
//// Closed Setting Page Name to be used in different places/ validation - Madhab ////

//// Open Setting default event type in different places/ validation - Madhab ////
$CFG->defaultEventType = 'course';
//// Closed Setting Page Name to be used in different places/ validation - Madhab ////

//define('THEME_DESIGNER_CACHE_LIFETIME', 1);
//$CFG->debug = (E_ALL | E_STRICT); 
//$CFG->debugdisplay = 0;
 
// You can specify a comma separated list of user ids that that always see
// debug messages, this overrides the debug flag in $CFG->debug and $CFG->debugdisplay
// for these users only.
//$CFG->debugusers = '2';



require_once(dirname(__FILE__) . '/lib/setup.php');



// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
