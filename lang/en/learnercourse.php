<?php
$string['mylearningpage'] = 'My Learning Page';
$string['mycolloborationpage'] = 'My Colloboration Page';
$string['courseoverview'] = 'Course Overview';
$string['nonlearnercourse'] = 'Non-Course Material(s)';
$string['learnercourse'] = 'Course(s)';
$string['idnumber'] = 'Id Number';
$string['fullname'] = 'Name';
$string['title'] = 'Title';
$string['resoursename'] = 'Name';
$string['category'] = 'Category';
$string['summary'] = 'Description';
$string['timecreated'] = 'Creation Date';
$string['datesent'] = 'Date Sent';
$string['timemodified'] = 'Modify Date';
$string['lastaccessed'] = 'Last Accessed';
$string['timespent'] = 'Time Spent';
$string['score'] = 'Score';
$string['status'] = 'Status';
$string['currentstatus'] = 'Status';
$string['users'] = 'Users';
$string['nolearnercoursesfound'] = 'No record(s) found';
$string['norecordsfound'] = 'No record(s) found';
$string['notstarted'] = 'Not Started';
$string['incomplete'] = 'In Progress';
$string['inprogress'] = 'In Progress';
$string['completed'] = 'Completed';
$string['ago'] = 'Ago';
$string['NA'] = 'NA';
$string['launch'] = 'Launch';
$string['points'] = 'Points';

// MY documents
$string['mydocuments'] = 'Documents';
$string['uploaddocument'] = 'Upload Document';
$string['view'] = 'View';
$string['delete'] = 'Delete';
$string['areyousureyouwanttodeletethisfile'] = 'Are you sure you want to delete this file?';
$string['pleaseselectfile'] = 'Please select file';
$string['invalidfileextension'] = 'Invalid file';
$string['download'] = 'Download';
$string['course'] = 'Course';
$string['description'] = 'Description';
$string['actions'] = 'Action';
$string['downloaded'] = 'Downloaded';
$string['notdownloaded'] = 'Not Downloaded';
$string['notaccessed'] = 'Not Accessed';

$string['clicktomanage'] = 'Click to Manage';
$string['messages'] = 'Messages';
$string['newcourses'] = 'New Courses';
$string['calendar'] = 'Calendar';
$string['month'] = 'Month';
$string['week'] = 'Week';
$string['day'] = 'Day';
$string['viewdetails'] = 'View Details';
$string['clicktolaunch'] = 'Click to Launch';


$string['programhasbeenenroled'] = 'Program has been enrolled successfully';
$string['coursehasbeenenroled'] = 'Course has been enrolled successfully';
$string['todo'] = 'To-Do';
$string['available'] = 'Available';
$string['enrolment'] = 'Enroll';
$string['program'] = 'Program';
$string['resourcetype'] = 'Type';
$string['assetlabel'] = 'Asset(s)';
$string['referencemateriallabel'] = 'Reference Materials';
$string['classlabel'] = 'Class(es)';
$string['resourcecreatedby'] = 'Created By';



