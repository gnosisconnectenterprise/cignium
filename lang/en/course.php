<?php
$string['courses'] = 'Courses';
$string['managecourses'] = 'Manage Courses';
$string['addcourse'] = 'Add course';
$string['addnewcourse'] = 'Add new course';
$string['delete'] = 'Delete';
$string['edit'] = 'Edit';
$string['enrolusers'] = 'Enroll Users';
$string['enrolusersfor'] = 'Enroll Users For';
$string['enrolgroups'] = 'Enroll groups';
$string['editcourse'] = 'Edit Course';
$string['addcourse'] = 'Add Course';
$string['addnewscormpackage'] = 'Adding a new SCORM package';
$string['updatescrom'] = 'Update SCORM';
$string['addafile'] = 'Add File';
$string['updatefile'] = 'Update File';
$string['users'] = 'Users';
$string['enrolledusers'] = 'Enrolled Users';
$string['editenrolement'] = 'Edit enrollment';
$string['unenrol'] = 'Unenroll';
$string['enrolledgroups'] = 'Enrolled groups';
$string['managegroups'] = 'Manage groups';
$string['enrolgp'] = 'Enroll';
$string['unenrolgp'] = 'Unenroll';
$string['assigendcourses'] = 'Assigend courses';
$string['enrolnotpermitted'] = 'You do not have permission or are not allowed to enroll someone in this course';
$string['donnothavepermissiontoenrol'] = 'You do not have permission or are not allowed to enroll.';
$string['donnothavepermissiontounenrol'] = 'You do not have permission or are not allowed to unenroll.';
$string['thisgroupisalreadyenroledforthiscourse'] = 'This group is already enrolled for this course';
$string['thisgroupdoesnothaveanymemberstoenrol'] = 'This group does not have any members to enroll';
$string['courseidorgroupidismissing'] = 'course id or group id is missing';
$string['backtomanagecourses'] = 'Back to Manage Courses';
$string['managetopics'] = 'Manage Topics';


///// Open - Added by Madhab /////
$string['search'] = 'Search';
$string['clearsearch'] = 'Clear';
$string['filterby'] = 'Search By:';
$string['id'] = 'ID';
$string['name'] = 'Name';
$string['description'] = 'Description';
$string['tag'] = 'Tags';
$string['category'] = 'Category';   

$string['coursename'] = 'Course Name';
$string['coursetype'] = 'Course Type';
$string['coursenumber'] = 'Course ID';
$string['manage'] = 'Manage';      

$string['previewlink'] = 'Preview';
$string['updatelink'] = 'Update';
$string['activatelink'] = 'Activate';
$string['deactivatelink'] = 'Deactivate';    

$string['norecordfound'] = 'No record(s) found';

$string['managecategory'] = 'Manage Course Categories';
$string['addcategory'] = 'Add Category';
$string['editcategory'] = 'Edit Category';
$string['categoryname'] = 'Name';
$string['categorydesc'] = 'Description';
$string['countcourse'] = 'Courses';

$string['coursecatalog'] = 'Course Catalog';
$string['assigncourse'] = 'Enroll Course';
$string['requestcourse'] = 'Request Course';
$string['launchcourse'] = 'Launch Course';

$string['sentassigncourserequest'] = 'Do you want to send a request for program/course enrollment?';
$string['emailsentsuccess'] = 'Request for program/course enrollment has been sent successfully.';
$string['emailsenterror'] = 'Error occurred in sending program/course enrollment request. Please try again.';
///// Closed - Added by Madhab /////
$string['course_allocation'] = 'Course Enrollment';
$string['needrequest'] = 'Need Request';
$string['selfallocation'] = 'Self Enrollment';
$string['catalog'] = 'Catalog';
$string['view'] = 'View';
$string['showcourse'] = 'Show';
$string['hidecourse'] = 'Hide';
$string['programlaunch'] = 'Launch Program';
$string['requestprogram'] = 'Request Program';
$string['suggesteduse'] = 'Suggested Use';
$string['learningobj'] = 'Learning Objectives';
$string['performanceOut'] = 'Performance Outcomes';
$string['enrolcourse'] = 'Enrollment';
$string['enrolprogram'] = 'Enrollment';
$string['coursehasbeenenroled'] = 'Course - {$a} has been enrolled successfully';
$string['courseisalreadyenroled'] = 'Course - {$a} is already enrolled';
$string['notabletoenrolthiscourse'] = 'Sorry! You are not able to able to enroll course - {$a}';
$string['areyousureyouwanttoenrolthiscourse'] = 'Are you sure you want to enroll course - {$a} ?';
$string['programhasbeenenroled'] = 'Program - {$a} has been enrolled successfully';
$string['programisalreadyenroled'] = 'Program - {$a} is already enrolled';
$string['notabletoenrolthisprogram'] = 'Sorry! You are not able to able to enroll program - {$a}';
$string['areyousureyouwanttoenrolthisprogram'] = 'Are you sure you want to enroll program - {$a} ?';

$string['saveandcontinue'] = 'Save & Continue';
$string['saveandexit'] = 'Save & Exit';
$string['for'] = 'For';
$string['published'] = 'Published';
$string['publish'] = 'Publish';
$string['publish_course'] = 'Once the course is published, it cannot be unpublished. Do you want to continue?';
$string['publish_course_form'] = '(once the course is published, it cannot be unpublished)';
$string['allowedfiletype'] = ', Allowed File Types: JPG/PNG/GIF';
$string['assetquickadd'] = 'Asset has been added successfully.';
$string['assetquickupdate'] = 'Asset has been updated successfully.';
$string['quickupdate'] = 'Save';
$string['selectfile'] = 'Select File';
$string['coursetype'] = 'Course Type';
$string['schedulingandtracking'] = 'Classes';
$string['selecttype'] = 'Select type';
$string['coursescorm'] = 'SCORM';
$string['courseresource'] = 'Resource';
$string['allowstudentaccess'] = 'Allow Student Access?';
$string['primaryinstructor'] = 'Primary Instructor';
$string['classroominstruction'] = 'Note to Instructor';
$string['recommendationduration'] = 'Recommended Duration';
$string['classroomcreatedby'] = 'Classroom Created By';
$string['referencemateriallabel'] = 'Reference Material(s)';
$string['refmatname'] = 'Name';
$string['refmattype'] = 'Type';
$string['noteforspecificasset'] = 'Classroom cannot be published unless there is at least one active \'{$a}\' asset.';
$string['musthavespecificasset'] = 'Classroom cannot be published unless there is at least one active \'{$a}\' asset.';
$string['cannotdeletespecificasset'] = 'This asset cannot be deleted. There should be at least one active \'{$a}\' asset.';
$string['assetcannotdeletehaveenrollmentinclassroom'] = 'This asset cannot be deleted. It is already a part of enrolled class.';
$string['refmatcannotdeletehaveenrollmentinclassroom'] = 'This reference material cannot be deleted. It is already a part of enrolled class.';
$string['cannotdeleteifuserenrolled'] = 'This asset cannot be deleted. It is already a part of enrollment.';
$string['cannotdeactivesingleonlineasset'] = 'This asset cannot be deactivated. There should be at least one active asset.';
$string['cannotdeactivespecificasset'] = 'This asset cannot be deactivated. There should be at least one active \'{$a}\' asset.';
$string['coursecategory'] = 'Categories';
$string['publishcoursewithoutanyasset'] = 'Are you sure you want to publish classroom without any asset loaded?';
$string['cannotdeletesignleasset'] = 'This asset cannot be deleted. There should be at least one active asset.';

/*Course Filter strings starts*/

$string['alltypes'] = 'All Course Types';
$string['all'] = 'All';
$string['published'] = 'Published';
$string['not_published'] = 'Not Published';
$string['allcategories'] = 'All Categories';
$string['allcriterias'] = 'All Classfications';

/*Course Filter strings ends*/

$string['isglobalcourse'] = 'Global Course?';
$string['isglobal'] = 'Is Global Elective';
$string['course_global_yes'] = 'Yes';
$string['course_global_no'] = 'No';
$string['enroluser'] = 'Enroll';
$string['courseemailcontent'] = 'Course Enrollment Email Content';
$string['custom_email_text'] = 'Custom Email Content';
$string['cc_emails'] = 'Cc to manager(s)';