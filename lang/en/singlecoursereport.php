<?php

/**
	* Custom module - Signle Report Page
	* Date Creation - 26/06/2014
	* Date Modification : 26/06/2014
	* Created By : Rajesh Kumar
	* Last Modified By : Rajesh Kumar
*/
	
$string['reports'] = 'Reports';
$string['viewuserreports'] = 'View user reports';
$string['overalluserprogressreport'] = 'Overall User Progress Report';
$string['numberofusers'] = 'Total Enrollment';
$string['course'] = 'Course';
$string['notstarted'] = 'Not Started';
$string['inprogress'] = 'In Progress';
$string['completed'] = 'Completed';
$string['more'] = 'More';
$string['loading'] = 'Please wait... while loading graph';
$string['coursetitle'] = 'Course Title';
$string['title'] = 'Title';
$string['lastaccessed'] = 'Last Accessed';
$string['attempts'] = '# Attempts';
$string['status'] = 'Status';
$string['score'] = 'Score';
$string['userid'] = 'User Id';
$string['fullname'] = 'Name';
$string['description'] = 'Description';
$string['username'] = 'Username';
$string['viewdetails'] = 'View Details';
$string['norecordfound'] = 'No record(s) found';
$string['course'] = 'Course';
$string['cancel'] = 'Cancel';
$string['department'] = 'Department';
$string['team'] = 'Team';
$string['users'] = 'Users';
$string['search'] = 'Search';
$string['all'] = 'All';
$string['alldepartments'] = 'All Departments';
$string['allteams'] = 'All Teams';
$string['allstatus'] = 'All Status';
$string['allusers'] = 'All Users';
$string['allcourses'] = 'All Courses';
$string['go'] = 'Go';
$string['startdate'] = 'Start Date';
$string['enddate'] = 'End Date';
$string['courseusagesreport'] = 'Online Course Usage Report';
$string['courseusagesreportgraph'] = 'Online Course Usage Report Graph';
$string['usereperformancereport'] = 'Learner Performance Report';
$string['coursevsmultipleuserreport'] = 'Course VS Multiple User Report';
$string['copyright'] = 'Copyright &copy; '.date('Y');
$string['allrightsreserved'] = 'All rights reserved';
$string['downloadcsv'] = 'Download CSV';
$string['downloadpdf'] = 'Download PDF';
$string['print'] = 'Print';
$string['any'] = 'NA';
$string['none'] = 'None';
$string['enrolmentdate'] = 'Enrollment Date';


?>






