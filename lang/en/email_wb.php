<?php

$string['from_email'] = 'InstituteofExcellence@wordandbrown.com';
//$string['from_email'] = 'admin@gnosisconnect.com';
$string['from_firstname'] = 'admin';

$string['email_footer'] = ''."\n\r".'Please do not reply to this e-mail. If you have questions, contact the Institute of Excellence at:'."\n\r";

$string['email_before_footer_enrol_text'] = ''."\n\r".'Your manager will also be notified.';
$string['email_before_footer_decline_text'] = ''."\n\r".'Your manager will also be notified.'."\n\r".'The Institute of Excellence will reschedule you or contact you concerning the available options.';


// Course
$string['user_enrol_course'] = '{$a->name},'."\n\r".'You have been enrolled for \'{$a->coursename}\'.';

$string['user_enrol_approved'] = '{$a->name},'."\n\r".'Your manager has approved your enrollment request to attend  \'{$a->coursename}\'.';

$string['user_enrol_denied_elective'] = '{$a->name},'."\n\r".'Your manager has denied your request to enroll for \'{$a->coursename}\' on {$a->datetime} for the following reason: {$a->reason}';

$string['user_enrol_denied_compliance'] = '{$a->name},'."\n\r".'Your enrollment for \'{$a->coursename}\' on {$a->datetime} has been denied for the following reason: {$a->reason}.'."\n\r".'Concerning Rescheduling: The Institute of Excellence will reschedule you or contact you concerning the available options.'."\n\r".'Thank you,'."\n\r".'{$a->manager}';

$string['manager_user_enrol_denied_compliance'] = '{$a->name},'."\n\r".'Due to administrative issues, we must cancel the course enrollment for your employee \'{$a->coursename}\' on {$a->datetime} has been denied for the following reason: {$a->reason}.'."\n\r".'Concerning Rescheduling: The Institute of Excellence will reschedule you or contact you concerning the available options.'."\n\r".'Thank you,'."\n\r".'{$a->manager}';

$string['user_enrol_denied_compliance_course'] = '{$a->name},'."\n\r".'Your enrollment for \'{$a->coursename}\' has been denied for the following reason: {$a->reason}.'."\n\r".'Concerning Rescheduling: The Institute of Excellence will reschedule you or contact you concerning the available options.'."\n\r".'Thank you,'."\n\r".'{$a->manager}';

$string['user_enrol_cancelled_compliance'] = '{$a->name},'."\n\r".'Due to administrative issues, we cancelled your enrollment for \'{$a->coursename}\' on {$a->datetime}.'."\n\r".'Your manager will also be notified.'."\n\r".'The Institute of Excellence will reschedule you or contact you concerning the available options.';

$string['user_enrol_cancelled_elective'] = '{$a->name},'."\n\r".'Due to administrative issues, we cancelled your enrollment for \'{$a->coursename}\'.'."\n\r".'Your manager will also be notified.'."\n\r".'Please contact the Institute of Excellence if you�re still interested in attending this course.';


$string['user_no_show'] = '{$a->name},'."\n\r".'According to our records you did not attend \'{$a->coursename}\' on {$a->datetime} as scheduled.'."\n\r".'Please check the calendar for additional dates and times. If none are available, please contact the Institute of Excellence at the e-mail address below.'."\n\r".'Your manager will receive notification that you did not attend the course.';


//department
$string['course_enrol_department'] = '{$a->name},'."\n\r".'A course \'{$a->coursename}\' has been added to your department \'{$a->departmentname}\'. Now you can enroll users in this course';

$string['course_unenrol_department'] = '{$a->name},'."\n\r".'Due to administrative issues, we cancelled your department\'s enrollment for \'{$a->coursename}\' from \'{$a->departmentname}\'.'."\n\r".'Please contact the Institute of Excellence if you�re still interested in this course.';
//team
$string['course_enrol_team'] = '{$a->name},'."\n\r".'You have been enrolled for \'{$a->coursename}\' in \'{$a->teamname}\' . Please go to your available section to enroll for this course.';

$string['course_unenrol_team'] = '{$a->name},'."\n\r".'Due to administrative issues, we cancelled your enrollment for \'{$a->coursename}\' from \'{$a->teamname}\'.'."\n\r".'Please contact the Institute of Excellence if you�re still interested in attending this course.';

//team
$string['user_enrol_team'] = '{$a->name},'."\n\r".'You have been enrolled in team \'{$a->teamname}\'. Please go to your available section to enroll for new courses.';

$string['user_unenrol_team'] = '{$a->name},'."\n\r".'Due to administrative issues, we cancelled your enrollment for team \'{$a->teamname}\'.'."\n\r".'Please contact the Institute of Excellence if you�re still interested in this team.';


// Subject Strings
$string['decline_subject'] = 'Request Declined: {$a->coursename}';
$string['approved_subject'] = 'Request Approved: {$a->coursename}';

$string['program_decline_subject'] = 'Request Declined: {$a->coursename}';
$string['program_approved_subject'] = 'Request Approved: {$a->coursename}';

$string['course_enrol_subject'] = 'Enrollment: {$a->coursename}';
$string['course_unenrol_subject'] = 'Cancelled: {$a->coursename}';

$string['course_enrol_department_subject'] = 'Enrollment: {$a->coursename}';
$string['course_unenrol_department_subject'] = 'Cancelled: {$a->coursename}';

$string['course_enrol_team_subject'] = 'Enrollment: {$a->coursename}';
$string['course_unenrol_team_subject'] = 'Cancelled: {$a->coursename}';

$string['user_enrol_program_subject'] = 'Enrollment: {$a->programname}';
$string['user_unenrol_program_subject'] = 'Cancelled: {$a->programname}';

$string['user_enrol_team_subject'] = 'Enrollment: {$a->teamname}';
$string['user_unenrol_team_subject'] = 'Cancelled: {$a->teamname}';



/*******************************************************************************************************/


// user
$string['user_enrol_program'] = '{$a->name},'."\n\r".'You have been enrolled for \'{$a->programname}\'';
$string['user_unenrol_program'] = '{$a->name},'."\n\r".'Due to administrative issues, we cancelled your enrollment for \'{$a->programname}\'}.'."\n\r".'Please contact the Institute of Excellence if you�re still interested in attending this Program.';
//department
$string['program_enrol_department'] = '{$a->name},'."\n\r".'A program \'{$a->programname}\' has been added to your department \'{$a->departmentname}\'. Now you can enroll users in this program';
$string['program_unenrol_department'] = '{$a->name},'."\n\r".'Due to administrative issues, we cancelled your department\'s enrollment for \'{$a->programname}\' from \'{$a->departmentname}\'.'."\n\r".'Please contact the Institute of Excellence if you�re still interested in this program.';
//team
$string['program_enrol_team'] = '{$a->name},'."\n\r".'You have been enrolled for \'{$a->programname}\' in \'{$a->teamname}\' . Please go to your available section to enroll for this program.';
$string['program_unenrol_team'] = '{$a->name},'."\n\r".'Due to administrative issues, we cancelled your enrollment for \'{$a->programname}\' from \'{$a->teamname}\'.'."\n\r".'Please contact the Institute of Excellence if you�re still interested in attending this program.';

// Subject Strings
$string['program_decline_subject'] = 'Request Declined: {$a->programname}';
$string['program_approved_subject'] = 'Request Approved: {$a->programname}';

$string['program_enrol_department_subject'] = 'Enrollment: {$a->programname}';
$string['program_unenrol_department_subject'] = 'Cancelled: {$a->programname}';

$string['program_enrol_team_subject'] = 'Enrollment: {$a->programname}';
$string['program_unenrol_team_subject'] = 'Cancelled: {$a->programname}';

$string['program_enrol_subject'] = 'Enrollment: {$a->programname}';
$string['program_unenrol_subject'] = 'Cancelled: {$a->programname}';

/*************************************************************************************************/

$string['user_enrol_request'] = '{$a->name},'."\n\r".'{$a->user_name} has requested for enrollment in course \'{$a->coursename}\' on {$a->datetime}. Location: {$a->location}.'."\n\r".'Please go to your requests section to view complete details.';

$string['user_enrol_request_course'] = '{$a->name},'."\n\r".'{$a->user_name} has requested for enrollment in course \'{$a->coursename}\'.'."\n\r".'Please go to your requests section to view complete details.';

/*Not Found*/
$string['user_acceptance_request'] = '{$a->name},'."\n\r".'Due to administrative issues, your manager has cancelled your enrollment for course \'{$a->coursename}\' on {$a->datetime}.'."\n\r".'Please contact the Institute of Excellence if you�re still interested in attending this Course.';
$string['user_decline_request'] = '{$a->name},'."\n\r".'Due to administrative issues, your manager has cancelled your enrollment for \'{$a->coursename}\' on {$a->datetime}';
$string['user_acceptance_request_program'] = '{$a->name},'."\n\r".'Due to administrative issues, your manager has cancelled your enrollment for program \'{$a->programname}\' on {$a->datetime}.'."\n\r".'Please contact the Institute of Excellence if you�re still interested in attending this Program.';
$string['user_decline_request_program'] = '{$a->name},'."\n\r".'Due to administrative issues, your manager has cancelled your enrollment for \'{$a->programname}\' on {$a->datetime}.';
/*Not Found*/

$string['request_subject'] = 'Request for course enrollment: {$a->coursename}';

$string['decline_subject'] = 'Course Cancellation: {$a->coursename}';
$string['accept_subject'] = 'Course Acceptance: {$a->coursename}';



$string['user_enrol_request_program'] = '{$a->name},'."\n\r".'{$a->user_name} has requested for enrollment in program \'{$a->programname}\'.'."\n\r".'Please go to your requests section to view complete details.';


$string['request_subject_program'] = 'Enrollment Request: {$a->programname}';

$string['decline_subject_program'] = 'Cancellation Request: {$a->programname}';
$string['accept_subject_program'] = 'Acceptance Request: {$a->programname}';


$string['class_user_acceptance_request'] = '{$a->name},'."\n\r".'Your manager has approved your enrollment request to attend \'{$a->coursename}\' on {$a->starttime}. Location: {$a->location}.'."\n\r";

//chk
$string['user_enrol_invite'] = '{$a->name},'."\n\r".'Your approval is needed to enroll {$a->user_name} for \'{$a->coursename}\' on {$a->starttime}.'."\n\r".'Please log into W&B�s Learning Management System (LMS) to respond to this request.'."\n\r".'Note: This is a time sensitive request; please respond as soon as possible.';

$string['user_enrol_invite_for_learner'] = '{$a->name},'."\n\r".'You have been invited for enrollment in course \'{$a->coursename}\' on {$a->starttime}. Location: {$a->location}.'."\n\r".'Please wait for your manager\'s approval.';


/***********************************************************************************************************/
/*Multiple assignment email texts*/

$string['user_enrol_courses'] = '{$a->name},'."\n\r".'You have been enrolled for following course(s). '."\n\r";
$string['user_enrol_courses_subject'] = 'Enrollment: Course(s)';

$string['user_unenrol_courses'] = '{$a->name},'."\n\r".'Due to administrative issues, we cancelled your enrollment for following course(s).'."\n\r";
$string['user_unenrol_courses_subject'] = 'Cancellation: Course(s)';


$string['manager_user_unenrol_courses'] = '{$a->name},'."\n\r".'Due to administrative issues, we must cancel the course enrollment for your employee {$a->user_name} for following course(s). '."\n\r".'Your employee will be contacted by the Institute of Excellence concerning rescheduling. '."\n\r".'Course(s) '."\n\r".'';
$string['manager_user_unenrol_courses_subject'] = 'Cancellation: Course(s)';


$string['multiple_courses_enrol_department'] = '{$a->name},'."\n\r".'The following course(s) has been added to your department \'{$a->departmentname}\'. Now you can enroll users in these course(s)';

$string['multiple_courses_subject'] = 'Enrollment: Course(s)';

$string['multiple_courses_enrol_department'] = '{$a->name},'."\n\r".'The following course(s) has been added to your department \'{$a->departmentname}\'. Now you can enroll users in these course(s)';
$string['multiple_courses_department_subject'] = 'Enrollment: Course(s)';

$string['multiple_course_unenrol_department'] = '{$a->name},'."\n\r".'Due to administrative issues, we cancelled your department\'s enrollment for following course(s) from  \'{$a->departmentname}\'.'."\n\r".'Please contact the Institute of Excellence if you�re still interested in these course(s).'."\n\r".'Course(s)'."\n\r";
$string['multiple_courses_unenrol_department_subject'] = 'Cancellation: Course(s)';

$string['multiple_courses_enrol_team'] = '{$a->name},'."\n\r".'The following course(s) has been added to your team \'{$a->teamname}\'. Please go to your available section to enroll for this course(s).'."\n\r";
$string['multiple_courses_team_subject'] = 'Enrollment: Course(s)';

$string['multiple_course_unenrol_team'] = '{$a->name},'."\n\r".'Due to administrative issues, we cancelled your team\'s enrollment for following course(s) from  \'{$a->teamname}\'.'."\n\r".'Please contact the Institute of Excellence if you�re still interested in these course(s).'."\n\r".'Course(s)'."\n\r";
$string['multiple_courses_unenrol_team_subject'] = 'Cancellation: Course(s)';

/***********************************************************************************************************/

$string['open_class_invitation'] = '{$a->name},'."\n\r".'You have been invited for class \'{$a->coursename}\' on {$a->starttime}. Location: {$a->location}.'."\n\r".' Please go to your available section to request for this class.';
$string['open_class_invitation_subject'] = 'Invite: {$a->coursename}';