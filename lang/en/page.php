<?php

$string['addnewpage'] = 'Add a new page';
$string['deletepage'] = 'Delete a page';
$string['deletepagecheck'] = 'Are you absolutely sure you want to completely delete this page and all the data it contains?';
$string['editpagesettings'] = 'Edit page settings';
$string['fulllistofpages'] = 'All pages';
$string['page'] = 'Page';
$string['pages'] = 'Pages';
$string['pagemgmt'] = 'Add/edit pages';

?>
