<?PHP 

$string['departments'] = 'Departments';
$string['add_department_page'] = 'Add Department';
$string['adddepartment'] = 'Add Department';
$string['browse_department'] = 'Manage Departments';
$string['edit_department'] = 'Edit Department';
$string['update_msg'] = 'Department has been updated successfully.';
$string['insert_msg'] = 'Department has been added successfully.';
$string['valid_pagename'] = 'Please enter department title';
$string['title'] = 'Title';
$string['name'] = 'Name';
$string['valid_title'] = 'Please enter department title';
$string['description'] = 'Description';
$string['department_delete'] = 'Delete Department';
$string['delete_msg'] = 'Department has been deleted successfully.';
$string['con_department_deactive'] = 'Do you want to deactivate';
$string['con_department_active'] = 'Do you want to activate';
$string['primary_manager'] = 'Primary Manager';
$string['select_primary_manager'] = 'Select primary manager';
$string['department_title_already_exist'] = 'The department name \'{$a}\' already exists , please choose another one.';


/*************External Department**********/
$string['isexternaldepartment'] = 'External Department?';
$string['external_department_column'] = 'External Department?';
$string['external_department_identifier'] = '*';  // also change this value in vw_user_details view, vw_credit_hours_of_learners, vw_enrolled_session_details_of_instructor
$string['list_external_department_instruction'] = ' External Department';
/*************External Department**********/
