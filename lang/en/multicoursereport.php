<?php

/**
	* Custom module - Multi Course Report Page
	* Date Creation - 01/07/2014
	* Date Modification : 01/07/2014
	* Created By : Rajesh Kumar
	* Last Modified By : Rajesh Kumar
*/
	
$string['reports'] = 'Reports';
$string['coursereport'] = 'Online Course Report';
$string['viewreportdetails'] = 'View Report Details';
$string['overallcourseprogressreport'] = 'Overall Online Course Report';
$string['numberofusers'] = 'Total Enrollment';
$string['numberofcourses'] = 'Course(s)';
$string['loading'] = 'Please wait... while loading graph';
$string['loadingusers'] = 'loading users...';
$string['loadingteams'] = 'loading teams...';
$string['loadingprograms'] = 'loading programs...';
$string['loadingcourses'] = 'loading courses...';
$string['coursetitle'] = 'Course Name';
$string['category'] = 'Category Name';
$string['totalassignedusers'] = 'Total Enrollment';
$string['enrolled'] = 'Enrolled';
$string['viewdetails'] = 'View Details';
$string['norecordfound'] = 'No record(s) found';
$string['nocoursefound'] = 'No record(s) found';
$string['totalassignedcourses'] = 'Total Enrollment';

$string['notstarted'] = 'Not Started';
$string['inprogress'] = 'In Progress';
$string['completed'] = 'Completed';
$string['back'] = 'Back';
$string['department'] = 'Department';
$string['team'] = 'Team';
$string['users'] = 'Users';
$string['search'] = 'Search';
$string['course'] = 'Course';
$string['status'] = 'Status';
$string['all'] = 'All';
$string['alldepartments'] = 'All Departments';
$string['allteams'] = 'All Teams';
$string['allstatus'] = 'All Status';
$string['allusers'] = 'All Users';
$string['allcourses'] = 'All Courses';
$string['allcategory'] = 'All Categories';
$string['go'] = 'Go';
$string['startdate'] = 'Start Date';
$string['enddate'] = 'End Date';
$string['courseusagesreport'] = 'Online Course Usage Report';
$string['courseusagesreportgraph'] = 'Online Course Usage Report Graph';
$string['usereperformancereport'] = 'Learner Performance Report';
$string['copyright'] = 'Copyright &copy; '.date('Y');
$string['allrightsreserved'] = 'All rights reserved';
$string['downloadcsv'] = 'Download CSV';
$string['downloadpdf'] = 'Download PDF';
$string['print'] = 'Print';
$string['any'] = 'NA';
$string['none'] = 'None';
$string['publish'] = 'Publish';
$string['unpublish'] = 'Un-Publish';
$string['publishyes'] = 'Yes';
$string['publishno'] = 'No';
$string['reset'] = 'Reset';
$string['publishedandactive'] = 'Published and Active';
$string['publishedandadective'] = 'Published and Deactive';
$string['unpublishedanddeactive'] = 'Unpublished and Deactive';
$string['unpublishedandaactive'] = 'Unpublished and active';
$string['dateofcreation'] = 'Date of Creation';
$string['createdby'] = 'Created By';
$string['modifiedby'] = 'Modified By';
$string['daterange'] = 'Date Range';
$string['daterangefrom'] = 'From';
$string['daterangeto'] = 'To';
$string['active'] = 'Active';
$string['deactivated'] = 'Deactivated';
$string['unpublished'] = 'Unpublished';
$string['statusactive'] = 'Active';
$string['statusdeactive'] = 'Deactivated';
$string['statusunpublished'] = 'Unpublished';
$string['statuspublished'] = 'Published';
$string['activeyes'] = 'Yes';
$string['activeno'] = 'No';
$string['program'] = 'Program';
$string['allprograms'] = 'All Programs';
$string['noprogramfound'] = 'No record(s) found';

$string['startdatecannotbegreaterthantodaydate'] = 'Start date cannot be greater than today date';
$string['enddatecannotbegreaterthantodaydate'] = 'End date cannot be greater than today date';
$string['startdatecannotbegreaterthanenddate'] = 'Start date cannot be greater than End date';
$string['maxperiodisoneyear'] = 'Max period is 1 year = 12 months. Please, change the Start/End date';

$string['class'] = 'Class';
$string['user'] = 'User';



