<?php

/**
	* Custom module - Signle Report Page
	* Date Creation - 25/06/2014
	* Date Modification : 25/06/2014
	* Created By : Rajesh Kumar
	* Last Modified By : Rajesh Kumar
*/
	
	
$string['reports'] = 'Reports';
$string['viewuserreports'] = 'View User Reports';
$string['overallcourseprogressreport'] = 'Overall Classroom Course Progress Report';
$string['numberofcourses'] = 'Total Enrollment';
$string['notstarted'] = 'Not Started';
$string['inprogress'] = 'Incomplete';
$string['completed'] = 'Completed';
$string['more'] = 'More';
$string['loading'] = 'Please wait... while loading graph';
$string['coursetitle'] = 'Classroom Name';
$string['title'] = 'Title';
$string['description'] = 'Description';
$string['lastaccessed'] = 'Last Accessed';
$string['attempts'] = '# Attempts';
$string['status'] = 'Status';
$string['score'] = 'Grade';
$string['viewdetails'] = 'View Details';
$string['course'] = 'Classroom';
$string['department'] = 'Department';
$string['team'] = 'Team';
$string['users'] = 'Users';
$string['search'] = 'Search';
$string['all'] = 'All';
$string['alldepartments'] = 'All Departments';
$string['allteams'] = 'All Teams';
$string['allstatus'] = 'All Status';
$string['allusers'] = 'All Users';
$string['allcourses'] = 'All Classrooms';
$string['allclasses'] = 'All Classes';
$string['go'] = 'Go';
$string['startdate'] = 'Start Date';
$string['enddate'] = 'End Date';
$string['courseusagesreport'] = 'Classroom Course Usage Report';
$string['classrequestreport'] = 'Class Request Report';
$string['courseusagesreportgraph'] = 'Classroom Course Usage Report Graph';
$string['usereperformancereport'] = 'Learner Performance Report';
$string['copyright'] = 'Copyright &copy; '.date('Y');
$string['allrightsreserved'] = 'All rights reserved';
$string['downloadcsv'] = 'Download CSV';
$string['downloadpdf'] = 'Download PDF';
$string['print'] = 'Print';
$string['any'] = 'NA';
$string['none'] = 'None';

$string['numberofusers'] = 'Total Enrollment';
$string['coursevsmultipleuserreport'] = 'Classroom V/S Multiple User Report';
$string['classroomvsuser'] = 'Classroom V/S User Report';
$string['overalluserprogressreport'] = 'Overall User Progress Report';
$string['fullname'] = 'Name';
$string['cancel'] = 'Cancel';
$string['primaryinstructor'] = 'Primary Instructor';
$string['secondaryinstructor'] = 'Secondary Instructor';
$string['classroomusagesreport'] = 'Classroom Course Usage Report';
$string['totalassignedusers'] = 'Total Enrollment';
$string['daterange'] = 'Date Range';
$string['daterangefrom'] = 'From';
$string['daterangeto'] = 'To';
$string['active'] = 'Active';
$string['reset'] = 'Reset';
$string['loadingteams'] = 'loading teams...';
$string['loadingcourses'] = 'loading classrooms...';
$string['loadingclasses'] = 'loading classes...';
$string['totalassignedcourses'] = 'Total Enrollment';
$string['enrolled'] = 'Enrolled';
$string['viewreportdetails'] = 'View Report Details';
$string['totalinviteduser'] = 'Invited';
$string['managerforapproval'] = 'Waiting';
$string['approveduser'] = 'Accepted';
$string['declineduser'] = 'Declined';
$string['classname'] = 'Class';
$string['username'] = 'User Name';
$string['document'] = 'Document';

$string['classattended'] = 'Attended';
$string['classnotattended'] = 'Not Attended';
$string['classcompleted'] = 'Completed';
$string['classincomplete'] = 'Incomplete';
$string['classnoshow'] = 'No Show';
$string['class_incomplete'] = 'Incomplete';
$string['classnodata'] = 'No Data';
$string['reporthasnotbeensubmittedyet'] = 'Report has not been submitted yet.';


?>






