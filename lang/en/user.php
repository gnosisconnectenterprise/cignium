<?PHP 

$string['viewprofile'] = 'View Profile';
$string['editprofile'] = 'Edit Profile';
$string['changepassword'] = 'Change Password';
$string['logout'] = 'Logout';
$string['switchtoadmin'] = 'Switch to Admin';
$string['switchtomanager'] = 'Switch to Manager';
$string['switchtolearner'] = 'Switch to Learner';
$string['switchtoinstructor'] = 'Switch to Instructor';
$string['alldepartment'] = 'All Departments';
$string['allmanagers'] = 'All Managers';
$string['loadingmanagers'] = 'Loading Managers...';
$string['loadingteams'] = 'Loading Teams...';
$string['go'] = 'Go';
$string['reset'] = 'Reset';
$string['allroles'] = 'All Roles';

/* Job Title and Company */
$string['select_job_title'] = 'Select Title';
$string['select_company'] = 'Select Company';
$string['job_title'] = 'Title';
$string['company'] = 'Company';
$string['all_job_title'] = 'All Titles';
$string['all_company'] = 'All Companies';
$string['name'] = 'Name (Username)';
$string['all_country'] = 'All Countries';
/* End */