<?php

/**
	* Custom module - Multi Report Page
	* Date Creation - 01/07/2014
	* Date Modification : 01/07/2014
	* Created By : Rajesh Kumar
	* Last Modified By : Rajesh Kumar
*/
	
	
$string['reports'] = 'Reports';
$string['userreport'] = 'User Report';
$string['overalluserprogressreport'] = 'Overall User Progress Report';
$string['numberofusers'] = 'Total Enrollment';
$string['numberofcourses'] = 'Total Enrollment';
$string['loading'] = 'Please wait... while loading graph';
$string['loadingusers'] = 'loading users...';
$string['loadingteams'] = 'loading teams...';
$string['loadingprograms'] = 'loading programs...';
$string['loadingcourses'] = 'loading courses...';
$string['viewreportdetails'] = 'View Report Details';
$string['coursetitle'] = 'Course Title';
$string['category'] = 'Category Name';
$string['totalassignedcourses'] = 'Total Enrollment';
$string['viewdetails'] = 'View Details';
$string['norecordfound'] = 'No record(s) found';
$string['notstarted'] = 'Not Started';
$string['inprogress'] = 'In Progress';
$string['completed'] = 'Completed';
$string['back'] = 'Back';
$string['userid'] = 'User Id';
$string['fullname'] = 'Name';
$string['description'] = 'Description';
$string['username'] = 'Username';
$string['contactdetails'] = 'Details';
$string['multiusercoursereport'] = 'Multi User Course Report';
$string['totalcourses'] = 'Total Enrollment';
$string['enrolled'] = 'Enrolled';
$string['alldepartments'] = 'All Departments';
$string['allteams'] = 'All Teams';
$string['allstatus'] = 'All Status';
$string['allusers'] = 'All Users';
$string['allcourses'] = 'All Courses';
$string['go'] = 'Go';
$string['startdate'] = 'Start Date';
$string['enddate'] = 'End Date';
$string['nouserfound'] = 'No record(s) found';
$string['nocoursefound'] = 'No record(s) found';
$string['noteamfound'] = 'No record(s) found';
$string['courseusagesreport'] = 'Online Course Usage Report';
$string['courseusagesreportgraph'] = 'Online Course Usage Report Graph';
$string['usereperformancereport'] = 'Learner Performance Report';
$string['manager'] = 'Manager';
$string['department'] = 'Department';
$string['team'] = 'Team';
$string['search'] = 'Search';
$string['course'] = 'Course';
$string['user'] = 'User';
$string['status'] = 'Status';
$string['all'] = 'All';
$string['copyright'] = 'Copyright &copy; '.date('Y');
$string['allrightsreserved'] = 'All rights reserved';
$string['downloadcsv'] = 'Download CSV';
$string['downloadpdf'] = 'Download PDF';
$string['print'] = 'Print';
$string['any'] = 'NA';
$string['none'] = 'None';
$string['reset'] = 'Reset';
$string['dateofcreation'] = 'Date of Creation';
$string['createdby'] = 'Created By';
$string['modifiedby'] = 'Modified By';
$string['daterange'] = 'Date Range';
$string['daterangefrom'] = 'From';
$string['daterangeto'] = 'To';
$string['active'] = 'Active';
$string['activeyes'] = 'Yes';
$string['activeno'] = 'No';
$string['program'] = 'Program';
$string['allprograms'] = 'All Programs';
$string['noprogramfound'] = 'No record(s) found';

$string['startdatecannotbegreaterthantodaydate'] = 'Start date cannot be greater than today date';
$string['enddatecannotbegreaterthantodaydate'] = 'End date cannot be greater than today date';
$string['startdatecannotbegreaterthanenddate'] = 'Start date cannot be greater than End date';
$string['maxperiodisoneyear'] = 'Max period is 1 year = 12 months. Please, change the Start/End date';
$string['departmentvsuserchart'] = 'Department v/s User Graph';
$string['graphusers'] = 'User(s)';
$string['statusactive'] = 'Active';
$string['statusdeactive'] = 'Deactivated';


