<?php

/* CustomMenu */
$string['home'] = 'My Home';
$string['dashboard'] = 'Dashboard';
$string['mydashboard'] = 'Dashboard';
$string['users'] = 'Users';
$string['myprofile'] = 'My Profile';
$string['viewprofile'] = 'View Profile';
$string['editprofile'] = 'Edit Profile';
$string['changepassword'] = 'Change Password';
$string['messages'] = 'Messages';
$string['accounts'] = 'Accounts';
$string['browselistofusers'] = 'Browse list of users';
$string['bulkuseractions'] = 'Bulk user actions';
$string['addanewuser'] = 'Add a new user';
$string['uploadusers'] = 'Upload users';
$string['permissions'] = 'Permissions';
$string['defineroles'] = 'Define roles';
$string['assignsystemroles'] = 'Assign system roles';
$string['settings'] = 'Settings';
$string['administration'] = 'Administration';
$string['siteadministration'] = 'Site administration';

// Dashboard
$string['calendar'] = 'Calendar';
$string['calendarexport'] = 'export';
$string['badges'] = 'Badges';
$string['privatefiles'] = 'Private files';
$string['filemanagement'] = 'File management';
$string['forum'] = 'Forum';
$string['blogs'] = 'Blog';
$string['logout'] = 'logout';

// My Learning page
$string['mylearningpage'] = 'Learning Page';
$string['mycolloborationpage'] = 'My colloboration page';
$string['courselist'] = 'Course list';
$string['noncoursematerial'] = 'Non-course material';

// Groups
$string['groups'] = 'Teams';
$string['addgroup'] = 'Add team';
$string['managegroups'] = 'Teams';

// Events
$string['events'] = 'Events';
$string['addevent'] = 'Add Event';
$string['editevent'] = 'Edit Event';
$string['manageevents'] = 'Events';

//appearance
$string['appearance'] = 'Appearance';
$string['themes'] = 'Themes';
$string['themesettings'] = 'Theme settings';
$string['themeselector'] = 'Theme selector';

// security
$string['security'] = 'Security';
$string['httpsecurity'] = 'HTTP security';
$string['plugins'] = 'Plugins';
$string['authentication'] = 'Authentication';
$string['ldapserver'] = 'LDAP server';
$string['casserversso'] = 'CAS server (SSO)';
$string['easybackup'] = 'Backup';

// course category

$string['managecoursesandcategories'] = 'Manage courses and categories';
$string['addacategory'] = 'Add a category';
$string['coursedefaultsettings'] = 'Course default settings';
$string['courserequest'] = 'Course request';

// courses

$string['courses'] = 'Courses';
$string['managecourses'] = 'Manage courses';
$string['addnewcourse'] = 'Add new course';

// Reports

$string['reports'] = 'Reports';
$string['viewreports'] = 'View Reports';
$string['coursereport'] = 'Online Course Report';
$string['userreport'] = 'User Report';
$string['courseusagesreport'] = 'Online Course Usage Report';
$string['usereperformancereport'] = 'Learner Performance Report';


// Gourmet Theme
$string['gourmetgeneralsettings'] = 'General settings';
$string['gourmetsiteheadersettings'] = 'Site header settings';
$string['gourmetcustommenus'] = 'Custom menus';
$string['gourmetfootercontentblocks'] = 'Footer content blocks';
$string['gourmetfrontpagealerts'] = 'Frontpage alerts';
$string['gourmetfrontpageslideshow'] = 'Frontpage slideshow';
$string['gourmetfrontpagepromobox'] = 'Frontpage promo box';
$string['gourmetfrontpagecontentblocks'] = 'Frontpage content blocks';
$string['gourmetfrontpageawardssection'] = 'Frontpage awards section';
$string['gourmetfrontpagetestimonials'] = 'Frontpage testimonials';
$string['gourmetsocialnetworking'] = 'Social networking';
$string['gourmetgoogleanalytics'] = 'Google analytics';
$string['myblogs'] = 'Blog';
$string['manage'] = 'Manage';
$string['managecms'] = 'CMS Pages';
$string['managemessages'] = 'Messages';
$string['managedepartments'] = 'Departments';
$string['learnerdashboard'] = 'Learner dashboard';
$string['admindashboard'] = 'Admin Dashboard';
$string['course_category'] = 'Course Categories';
$string['course_catalog'] = 'Catalog';
$string['course_catalog_learner'] = 'Available Courses';
$string['requests'] = 'Requests';



?>


