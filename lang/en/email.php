<?php

//$string['from_email'] = 'InstituteofExcellence@wordandbrown.com';
$string['from_email'] = '<a href= "https://support.cignium.com">https://support.cignium.com</a>.';
$string['from_firstname'] = 'Portal Helpdesk';

$string['email_footer'] = ''."\n\r"."\n\r".'Please do not reply to this e-mail. If you require support, please log a ticket with the Help Desk at ';
$string['email_footer_classroom'] = 'Please do not reply to this e-mail. If you require support, please log a ticket with the Help Desk at ';

$string['email_footer_decline'] = ''."\n\r"."\n\r".'Please do not reply to this e-mail. If you have questions, contact your manager directly or you may contact the portal helpdesk at ';

$string['email_before_footer_enrol_text'] = ''."\n\r"."\n\r".'Your manager will also be notified.';
$string['email_before_footer_decline_text'] = ''."\n\r"."\n\r".'Your manager will also be notified.'."\n\r"."\n\r".'Please contact the portal helpdesk if you\'re still interested in attending this course.';


// Course
$string['user_enrol_course'] = '{$a->name},'."\n\r"."\n\r".'You have been enrolled for "{$a->coursename}"{$a->availabletill}';

$string['user_enrol_approved_course'] = '{$a->name},'."\n\r"."\n\r".'Your manager has approved your enrollment request for the following online course: "{$a->coursename}"{$a->availabletill}'."\n\r"."\n\r".'Please login to the {$a->lmsName} using the following link to view complete details: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';

$string['user_enrol_approved_program'] = '{$a->name},'."\n\r"."\n\r".'Your manager has approved your enrollment request for the following training program: "{$a->coursename}"{$a->availabletill}'."\n\r"."\n\r".'For more details, please log in to {$a->lmsName} and access your learning page using the following link: <a target = "_blank" href = "{$a->lmsLink}" >{$a->lmsLink}</a>';;

$string['user_enrol_denied_elective'] = '{$a->name},'."\n\r"."\n\r".'Your manager has denied your request to enroll for "{$a->coursename}" on {$a->datetime} for the following reason: {$a->reason}';

$string['user_enrol_denied_compliance'] = '{$a->name},'."\n\r"."\n\r".'Your enrollment for the following classroom course has been declined:<ul>'.'<li>Course: {$a->coursename}</li>'.'<li>Date/Date Range: {$a->classduration}</li>{$a->location}'.'<li style="margin-top:10px;padding-top:10px;">Reason/Remarks: {$a->reason}</li></ul>';

$string['manager_user_enrol_denied_compliance'] = '{$a->name},'."\n\r"."\n\r".'Due to administrative issues, we must cancel the course enrollment for your employee,"{$a->user_name}" for "{$a->coursename}" on {$a->datetime}.'."\n\r"."\n\r".'If you\'re still interested in having your employee attend this course, please have them contact admin.';

$string['user_enrol_denied_compliance_course'] = '{$a->name},'."\n\r"."\n\r".'Your enrollment for "{$a->coursename}" has been denied for the following reason: {$a->reason}.'."\n\r"."\n\r".'Concerning Rescheduling: Admin will reschedule you or contact you concerning the available options.'."\n\r"."\n\r".'Thank you,'."\n\r"."\n\r".'{$a->manager}';

$string['user_enrol_cancelled_compliance'] = '{$a->name},'."\n\r"."\n\r".'Due to administrative issues, we cancelled your enrollment for "{$a->coursename}" on {$a->datetime}.'."\n\r"."\n\r".'Your manager will also be notified.'."\n\r"."\n\r".'Admin will reschedule you or contact you concerning the available options.';

$string['user_enrol_cancelled_elective'] = '{$a->name},'."\n\r"."\n\r".'Due to administrative issues, we cancelled your enrollment for "{$a->coursename}".'."\n\r"."\n\r";
$string['user_enrol_cancelled_elective_2'] = 'Please contact admin if you\'re still interested in attending this course.';


$string['user_no_show'] = '{$a->name},'."\n\r"."\n\r".'According to our records you did not attend "{$a->coursename}" on {$a->datetime} as scheduled.'."\n\r"."\n\r".'Please check the calendar for additional dates and times. If none are available, please contact admin at the e-mail address below.'."\n\r"."\n\r".'Your manager will receive notification that you did not attend the course.';


//department
$string['course_enrol_department'] = '{$a->name},'."\n\r"."\n\r".'A course "{$a->coursename}" has been added to your department "{$a->departmentname}". Now you can enroll users in this course.';

$string['course_unenrol_department'] = '{$a->name},'."\n\r"."\n\r".'Due to administrative issues, we cancelled your enrollment for "{$a->coursename}" from "{$a->departmentname}" department.'."\n\r"."\n\r".'Please contact admin if you\'re still interested in this course.';
//team
/*$string['course_enrol_team'] = '{$a->name},'."\n\r"."\n\r".'You have been enrolled for "{$a->coursename}" in "{$a->teamname}" . Please go to your available section to enroll for this course.';*/
$string['course_enrol_team'] = '{$a->name},'."\n\r"."\n\r".'The "{$a->coursename}" course has been enrolled to your "{$a->teamname}" team{$a->availabletill} Please go to your available section for self enrolment.';

$string['available_till'] = ''."\n\r"."\n\r".'It will be available to you in {$a->lmsName} until {$a->enddate}.';

$string['course_unenrol_team'] = '{$a->name},'."\n\r"."\n\r".'Due to administrative issues, we cancelled your enrollment for "{$a->coursename}" from "{$a->teamname}".'."\n\r"."\n\r";

$string['course_unenrol_team_2'] = 'Please contact admin if you\'re still interested in attending this course.';

//team
$string['user_enrol_team'] = '{$a->name},'."\n\r"."\n\r".'You have been enrolled in team "{$a->teamname}". Please go to your available section to enroll for new courses.';

$string['user_unenrol_team'] = '{$a->name},'."\n\r"."\n\r".'Due to administrative issues, we cancelled your enrollment for team "{$a->teamname}".'."\n\r"."\n\r".'Please contact admin if you\'re still interested in this team.';


// Subject Strings
$string['decline_subject_request'] = 'LMS Online Course Request Declined: {$a->coursename}';
$string['approved_subject_request'] = 'LMS Online Course Request Approved: {$a->coursename}';

$string['program_decline_subject'] = 'LMS Training Program Request Declined: {$a->coursename}';
$string['program_approved_subject'] = 'LMS Training Program Request Approved: {$a->coursename}';

$string['decline_subject_request_class'] = 'LMS Classroom Request Declined: {$a->coursename}';
$string['approved_subject_request_class'] = 'LMS Classroom Request Approved: {$a->coursename}';

$string['course_enrol_subject'] = 'Enrollment: {$a->coursename}';
$string['course_unenrol_subject'] = 'Cancelled: {$a->coursename}';
$string['manager_course_unenrol_subject'] = 'Cancelled: {$a->coursename}';

$string['course_enrol_department_subject'] = 'Enrollment: {$a->coursename}';
$string['course_unenrol_department_subject'] = 'Cancelled: {$a->coursename}';

$string['course_enrol_team_subject'] = 'Enrollment: {$a->coursename}';
$string['course_unenrol_team_subject'] = 'Cancelled: {$a->coursename}';

$string['user_enrol_program_subject'] = 'Enrollment: {$a->programname}';
$string['user_unenrol_program_subject'] = 'Cancelled: {$a->programname}';

$string['user_enrol_team_subject'] = 'Enrollment: {$a->teamname}';
$string['user_unenrol_team_subject'] = 'Cancelled: {$a->teamname}';



/*******************************************************************************************************/


// user
$string['user_enrol_program'] = '{$a->name},'."\n\r"."\n\r".'You have been enrolled for the following training program: "{$a->programname}"{$a->availabletill}'."\n\r"."\n\r".'Please login to the {$a->lmsName} using the following link to view complete details: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';
$string['user_extend_program'] = '{$a->name},'."\n\r"."\n\r".'Your enrollment end date has been changed for training program "{$a->programname}"{$a->availabletill}'."\n\r"."\n\r".'Please go to your Requests Section in {$a->lmsName} using the following link to view complete details: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';

$string['user_unenrol_program'] = '{$a->name},'."\n\r"."\n\r".'Due to administrative issues, we cancelled your enrollment for "{$a->programname}".'."\n\r"."\n\r";
//$string['user_unenrol_program_1'] = 'Please contact admin if you\'re still interested in attending this Program.';
$string['user_unenrol_program_1'] = '';

//department
$string['program_enrol_department'] = '{$a->name},'."\n\r"."\n\r".'The program "{$a->programname}" has been made available to your department - "{$a->departmentname}" in {$a->lmsName}.'."\n\r"."\n\r".'You can now enroll your learners into this program.'."\n\r"."\n\r".'To enroll Learners please go to your "Manage Programs" tab in {$a->lmsName} using the following link: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';
$string['program_unenrol_department'] = '{$a->name},'."\n\r"."\n\r".'Due to administrative issues, we cancelled your department\'s enrollment for the following program:  "{$a->programname}".';/*."\n\r"."\n\r".'Please contact admin if you\'re still interested in this program.'*/
//team
/*$string['program_enrol_team'] = '{$a->name},'."\n\r"."\n\r".'You have been enrolled for "{$a->programname}" in "{$a->teamname}" . Please go to your available section to enroll for this program.';*/

$string['program_enrol_team'] = '{$a->name},'."\n\r"."\n\r".'The program "{$a->programname}" has been made available to your team - "{$a->teamname}" in {$a->lmsName}.{$a->availabletill}'."\n\r"."\n\r".'Please go to your Available Page in {$a->lmsName} using the following link to view complete details: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';

$string['program_extend_team'] = '{$a->name},'."\n\r"."\n\r".'The "{$a->programname}" program\'s end date has been changed for your team - "{$a->teamname}" in {$a->lmsName}.{$a->availabletill}'."\n\r"."\n\r".'Please go to your Available Page in {$a->lmsName} using the following link to view complete details: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';

$string['program_unenrol_team'] = '{$a->name},'."\n\r"."\n\r".'Due to administrative issues, we cancelled your enrollment for  the following training program: "{$a->programname}"'."\n\r";
//$string['program_unenrol_team_1'] = 'Please contact admin if you\'re still interested in attending this program.';
$string['program_unenrol_team_1'] = '';

// Subject Strings
$string['program_decline_subject'] = 'LMS Training Program Request Cancellation: {$a->coursename}';
$string['program_approved_subject'] = 'LMS Training Program Request Approved: {$a->coursename}';

$string['program_enrol_department_subject'] = 'LMS Department Training Program Enrollment';
$string['program_unenrol_department_subject'] = 'LMS Training Program Cancellation: {$a->programname}';

$string['program_enrol_team_subject'] = 'LMS Training Program Enrollment';
$string['program_unenrol_team_subject'] = 'LMS Training Program Cancellation: {$a->programname}';

$string['program_enrol_subject'] = 'LMS Training Program Enrollment';
$string['program_unenrol_subject'] = 'LMS Program Cancellation: {$a->programname}';

/*************************************************************************************************/

$string['user_enrol_request'] = '{$a->name},'."\n\r"."\n\r".'"{$a->user_name}" has requested enrollment for the following classroom course:<ul><li>'.'Course: {$a->coursename}</li>'.'<li>Date/Date Range: {$a->classduration}</li>{$a->location}</ul>{$a->sessionsListing}'.'Please go to your Requests Section in {$a->lmsName} using the following link to view complete details: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';

$string['user_enrol_request_learner'] = '{$a->name},'."\n\r"."\n\r".'Your request for enrollment for the following classroom course has been received:<ul><li>'.'Course: {$a->coursename}</li>'.'<li>Date/Date Range: {$a->classduration}</li>{$a->location}</ul>{$a->sessionsListing}'.'Please wait for your manager\'s approval.';

$string['user_enrol_request_course'] = '{$a->name},'."\n\r"."\n\r".'"{$a->user_name}" has requested enrollment for the following online course: "{$a->coursename}".'."\n\r"."\n\r".'Please go to your Requests Section in {$a->lmsName} using the following link to view complete details: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';

/*Not Found*/
$string['user_acceptance_request'] = '{$a->name},'."\n\r"."\n\r".'Due to administrative issues, your manager has cancelled your enrollment for course "{$a->coursename}" on {$a->starttime}.'."\n\r"."\n\r".'Please contact admin if you\'re still interested in attending this Course.';
$string['user_decline_request'] = '{$a->name},'."\n\r"."\n\r".'Due to administrative issues, your manager has cancelled your enrollment for "{$a->coursename}" on {$a->datetime}';
$string['user_acceptance_request_program'] = '{$a->name},'."\n\r"."\n\r".'Due to administrative issues, your manager has cancelled your enrollment for program "{$a->programname}" on {$a->datetime}.'."\n\r"."\n\r".'Please contact admin if you\'re still interested in attending this Program.';
$string['user_decline_request_program'] = '{$a->name},'."\n\r"."\n\r".'Due to administrative issues, your manager has cancelled your enrollment for "{$a->programname}" on {$a->datetime}.';
/*Not Found*/

$string['request_subject'] = 'LMS Online Course Enrollment Request: {$a->coursename}';
$string['request_subject_class_manager'] = 'Time Sensitive: Course Enrollment Request - {$a->coursename} - {$a->starttime}';
$string['request_subject_extension'] = 'LMS Online Course Extension Request: {$a->coursename}';

$string['request_subject_classroom'] = 'LMS Classroom Course Enrollment Request: {$a->coursename} - {$a->starttime}';
$string['request_subject_invite'] = 'LMS Classroom Invitation: {$a->coursename} - {$a->starttime}';
$string['request_subject_invite_only'] = 'LMS Classroom Enrollment Request: {$a->coursename} - {$a->starttime}';

$string['decline_subject'] = 'Course Cancellation: {$a->coursename}';
$string['accept_subject'] = 'Course Acceptance: {$a->coursename}';



$string['user_enrol_request_program'] = '{$a->name},'."\n\r"."\n\r".'"{$a->user_name}" has requested enrollment for the following training program: "{$a->programname}".'."\n\r"."\n\r".'Please go to your Requests Section in {$a->lmsName} using the following link to view complete details: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';


$string['request_subject_program'] = 'LMS Online Program Enrollment Request: {$a->programname}';
$string['request_subject_program_extension'] = 'LMS Online Program Extension Request: {$a->programname}';

$string['decline_subject_program'] = 'Cancellation Request: {$a->programname}';
$string['accept_subject_program'] = 'Acceptance Request: {$a->programname}';


$string['class_user_acceptance_request'] = '{$a->name},'."\n\r"."\n\r".'Your manager has approved your enrollment request to attend the following classroom course:<ul><li>'.'Course: {$a->coursename}</li>'.'<li>Date/Date Range: {$a->classduration}</li>{$a->location}</ul>{$a->sessionsListing}';

//chk
$string['user_enrol_invite'] = '{$a->name},'."\n\r"."\n\r".'Your approval is required to enroll "{$a->user_name}" for "{$a->coursename}" on {$a->starttime}.{$a->sessionsListing}Please log into your Requests Section in {$a->lmsName} to respond to this request using the following link: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>'."\n\r"."\n\r".'Note: This is a time sensitive request; please respond as soon as possible.';

$string['user_enrol_invite_for_learner'] = '{$a->name},'."\n\r"."\n\r".'Your request for enrollment for the following classroom course has been received:<ul><li>'.'Course: {$a->coursename}</li>'.'<li>Date/Date Range: {$a->classduration}</li>{$a->location}</ul>{$a->sessionsListing}Please wait for your manager\'s approval.';

$string['skip_user_enrol_invite_for_learner'] = '{$a->name},'."\n\r"."\n\r".'You have been invited to attend the following classroom course:<ul><li>'.'Course: {$a->coursename}</li>'.'<li>Date/Date Range: {$a->classduration}</li>{$a->location}</ul>{$a->sessionsListing}';
$string['skip_user_enrol_invite_for_learner_outlook_info'] = 'To accept and add this to your Outlook calendar, please open the attached file.'."\n\r".'To decline or to see additional details, please log into {$a->lmsName} and proceed to your Learning Page using the following link: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';
$string['skip_user_enrol_invite_for_learner_info'] = 'Please log into {$a->lmsName} and proceed to your Learning Page using the following link: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';


/***********************************************************************************************************/
/*Multiple assignment email texts*/

$string['user_enrol_courses'] = '{$a->name},'."\n\r"."\n\r".'##COURSE_EMAIL##You have been enrolled in the following online course(s):'."\n\r"."\n\r".'{$a->courseContent}'."\n\r"."\n\r".'Please login to the {$a->lmsName} using the following link to view complete details: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';

$string['user_extend_courses'] = '{$a->name},'."\n\r"."\n\r".'##COURSE_EMAIL##Your enrollment end date has been changed for the following online course(s):'."\n\r"."\n\r".'{$a->courseContent}'."\n\r"."\n\r".'Please login to the {$a->lmsName} using the following link to view complete details: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';
$string['user_enrol_courses_subject'] = 'LMS Online Course Enrollment';
$string['user_enrol_courses_subject_selfenrol'] = 'LMS Online Course Enrollment';

$string['user_unenrol_courses_learner'] = '{$a->name},'."\n\r"."\n\r".'Due to administrative issues, we cancelled your enrollment for the following online course(s):'."\n\r".'{$a->courseContent}'."\n\r".'Your manager will also be notified.'."\n\r"."\n\r".'Please do not reply to this e-mail.'."\n\r"."\n\r".'If you are still interested in attending this course or if you have any questions, please contact the admin  at:'."\n\r"."\n\r";

$string['user_unenrol_courses_manager'] = '{$a->name},'."\n\r"."\n\r".'Due to administrative issues, we cancelled your enrollment for the following online course(s):'."\n\r"."\n\r".'{$a->courseContent}'."\n\r"."\n\r".'Please do not reply to this e-mail.'."\n\r"."\n\r".'If you are still interested in attending this course or if you have any questions, please contact the admin  at:'."\n\r"."\n\r";
$string['user_unenrol_courses_subject'] = 'LMS Online Course Cancellation';


$string['manager_user_unenrol_courses_1'] = '{$a->name},'."\n\r"."\n\r".'Due to administrative issues, we must cancel the online course enrollment for your employee "{$a->user_name}" for the following online course(s):'."\n\r"."\n\r".'{$a->courseContent}'."\n\r"."\n\r".'Please do not reply to this e-mail.'."\n\r"."\n\r".'If you are still interested in having your employee complete this online course or if you have any questions, please contact the admin at:'."\n\r"."\n\r";
$string['manager_user_unenrol_courses_subject'] = 'LMS Online Course Cancellation';


$string['multiple_courses_enrol_department'] = '{$a->name},'."\n\r"."\n\r".'The following online course(s) has been added to your department "{$a->departmentname}".'."\n\r"."\n\r".'{$a->courseContent}'."\n\r"."\n\r".'You can now enroll learners into these course(s):'."\n\r"."\n\r".'Please go to your Requests Section in {$a->lmsName} using the following link to view complete details: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';;

$string['multiple_courses_department_subject'] = 'LMS Department Online Course Enrollment';
$string['multiple_courses_subject'] = 'Enrollment: Course(s)';

$string['multiple_course_unenrol_department'] = '{$a->name},'."\n\r"."\n\r".'Due to administrative issues, we cancelled your department\'s enrollment for the following online course(s).'."\n\r"."\n\r".'{$a->courseContent}'."\n\r"."\n\r".'Please contact admin if you\'re still interested in these course(s):'."\n\r"."\n\r";
$string['multiple_courses_unenrol_department_subject'] = 'LMS Online Course Cancellation';

$string['multiple_courses_enrol_team'] = '{$a->name},'."\n\r"."\n\r".'The following online course(s) has been added to your team "{$a->teamname}".'."\n\r"."\n\r".'{$a->courseContent}'."\n\r"."\n\r".'Please go to your Available Section in {$a->lmsName} using the following link to view complete details: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';

$string['multiple_courses_extend_team'] = '{$a->name},'."\n\r"."\n\r".'End date for the following online course(s) has been changed for your team "{$a->teamname}".'."\n\r"."\n\r".'{$a->courseContent}'."\n\r"."\n\r".'Please go to your Available Section in {$a->lmsName} using the following link to view complete details: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';
$string['multiple_courses_team_subject'] = 'LMS Online Course Enrollment';
$string['multiple_courses_team_subject_extend'] = 'LMS Online Course Enrollment';

$string['multiple_course_unenrol_team'] = '{$a->name},'."\n\r"."\n\r".'Due to administrative issues, we cancelled your enrollment for the following online course(s) from  "{$a->teamname}" team.'."\n\r"."\n\r".'Please contact admin if you\'re still interested in these course(s):'."\n\r"."\n\r";
$string['multiple_courses_unenrol_team_subject'] = 'Cancellation: Course(s)';

/***********************************************************************************************************/

$string['open_class_invitation'] = '{$a->name},'."\n\r"."\n\r".'The following classroom course has been made available for you to request:<ul><li>'.'Course: {$a->coursename}</li>'.'<li>Date/Date Range: {$a->classduration}</li>{$a->location}</ul>'.'{$a->sessionsListing}To see additional details please log into {$a->lmsName} and proceed to your Learning Page using the following link: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';
$string['open_class_invitation_subject'] = 'LMS Classroom Course: {$a->coursename} - {$a->starttime}';




/************Course/Program Extension******************/

$string['user_enrol_request_course_extension'] = '{$a->name},'."\n\r"."\n\r".'"{$a->user_name}" has requested an extension of enrollment for the following online course: "{$a->coursename}".'."\n\r"."\n\r".'Please go to your Requests Section in {$a->lmsName} using the following link to view complete details: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';

$string['user_enrol_request_program_extension'] = '{$a->name},'."\n\r"."\n\r".'"{$a->user_name}" has requested an extension of enrollment for the following program: "{$a->programname}".'."\n\r"."\n\r".'Please go to your Requests Section in {$a->lmsName} using the following link to view complete details: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';

$string['class_user_acceptance_request_tocreator'] = '{$a->name},'."\n\r"."\n\r".'"{$a->approved_by_name}" has approved the enrollment of {$a->assign_name} to attend the following classroom <ul><li>'.'Course: {$a->coursename}</li>'.'<li>Date/Date Range: {$a->classduration}</li>{$a->location}</ul>';
$string['user_enrol_denied_compliance_tocreator'] = '{$a->name},'."\n\r"."\n\r".'{$a->assign_name}\'s enrollment for "{$a->coursename}" on {$a->datetime} has been denied by "{$a->approved_by_name}" for the following reason: {$a->reason}.';
$string['user_enrol_denied_compliance_tomanager'] = '{$a->name},'."\n\r"."\n\r".'{$a->assign_name}\'s enrollment has been declined for the following classrooom course:<ul><li>'.'Course: {$a->coursename}</li>'.'<li>Date/Date Range: {$a->classduration}</li>{$a->location}'.'<li>Reason/Remarks: {$a->reason}</li></ul>';

$string['user_enrol_denied_compliance_tomanager_byothers'] = '{$a->name},'."\n\r"."\n\r".'{$a->assign_name}\'s enrollment has been declined for the following classrooom course:<ul><li>'.'Course: {$a->coursename}</li>'."<li>".'Date/Date Range: {$a->classduration}</li>{$a->location}'.'<li style="margin-top:10px;padding-top:10px;">Reason/Remarks: {$a->reason}</li></ul>';

$string['user_enrol_denied_compliance_tolearner'] = '{$a->name},'."\n\r"."\n\r".'You declined your enrollment for the following classrooom course:<ul><li>'.'Course: {$a->coursename}</li>'."<li>".'Date/Date Range: {$a->classduration}</li>{$a->location}'."<li style=\"margin-top:10px;padding-top:10px;\">".'Reason/Remarks: {$a->reason}</li></ul>'.'Your name has been removed from the enrollment list.'."\n\r".'Your manager will be notified.'."\n\r"."\n\r".'Please do not reply to this e-mail.'."\n\r"."\n\r".'If you are still interested in taking this course, please check the calendar for additional dates and times in {$a->lmsName} using the following link: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>'."\n\r"."\n\r".'If none are available, please contact the admin at the e-mail address below:'."\n\r";

$string['noshow_tolearner'] = '{$a->name},'."\n\r"."\n\r".'According to our records you did not attend the following classroom course as scheduled:<ul><li>'.'Course: {$a->coursename}</li>'.'<li>Date/Date Range: {$a->classduration}</li>{$a->location}</ul>'.'{$a->sessionList}Your manager will receive notification that you did not attend the course.'."\n\r"."\n\r".'Please check the calendar in {$a->lmsName} using the following link for additional dates and times: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';

$string['noshow_tomanager'] = '{$a->name},'."\n\r"."\n\r".'According to our records, "{$a->assign_name}" did not attend the following classrooom course as scheduled:<ul><li>'.'Course: {$a->coursename}</li>'.'<li>Date/Date Range: {$a->classduration}</li>{$a->location}</ul>'.'{$a->sessionList}Your employee will be contacted by admin concerning rescheduling.';

$string['no_show_subject'] = 'LMS Classroom Absence: {$a->coursename} - {$a->datetime}';


/***********************************************************************************************************/
/* Blog and Comment Email Text */


$string['comment_admin_approval_subject_to_admin'] = 'Need Approval: {$a->name} has posted a comment on "{$a->postSubject}"';
$string['comment_admin_approval_to_admin'] = '{$a->postCreatorFullName},'."\n\r"."\n\r".' {$a->name} has posted a comment like "{$a->comment}" on "{$a->postSubject}", so your approval is needed to access his/her comment to another user who are in this blog.'."\n\r"."\n\r".'Please log into {$a->siteTitleForBlog} to respond to this request by clicking on this link: <a href = "{$a->link}">{$a->sitename}</a>.'."\n\r"."\n\r";

$string['comment_admin_approval_subject_to_user'] = 'Waiting For Approval: You have posted a comment on "{$a->postSubject}"';
$string['comment_admin_approval_to_user'] =  '{$a->name},'."\n\r"."\n\r".' You have posted a comment like "{$a->comment}" on "{$a->postSubject}", so moderator approval is needed to access your comment to another user who are in this blog.'."\n\r"."\n\r".'Please log into {$a->siteTitleForBlog} to see your comment by clicking on this link: <a href = "{$a->link}">{$a->sitename}</a>.'."\n\r"."\n\r";

$string['comment_approval_cancelled_subject_to_user'] = 'Cancellation: Blog comment request for "{$a->postSubject}" ';
$string['comment_approval_cancelled_to_user'] = '{$a->name},'."\n\r"."\n\r".'Due to administrative issues, we have cancelled your request for following blog "{$a->postSubject}" to access your comment like "{$a->comment}" to another user who are in this blog.'."\n\r"."\n\r".'Please log into {$a->siteTitleForBlog} to see your comment by clicking on this link: <a href = "{$a->link}">{$a->sitename}</a>.'."\n\r"."\n\r";


$string['comment_approval_subject_to_user'] = 'Request Approved: Blog comment request for "{$a->postSubject}" ';
$string['comment_approval_to_user'] =  '{$a->name},'."\n\r"."\n\r".'Moderator has approved your comment request to access  your comment like "{$a->comment}"  to another user who are in this blog "{$a->postSubject}".'."\n\r"."\n\r".'Please log into {$a->siteTitleForBlog} to see your comment by clicking on this link: <a href = "{$a->link}">{$a->sitename}</a>.'."\n\r"."\n\r";


/***********************************************************************************************************/
/***** Event Mails Starts Here*******/
$string['user_event_subject'] = 'LMS Calendar Event';
$string['global_event_subject'] = 'LMS Calendar Event';
$string['course_event_subject'] = 'LMS Calendar Event';
$string['classroom_event_subject'] = 'LMS Calendar Event';

$string['user_event_body'] = '{$a->name},'."\n\r"."\n\r".'A new calendar event "{$a->eventname}" on "{$a->eventtime}" has been added by you in {$a->lmsName}.';

$string['global_event_body'] = '{$a->name},'."\n\r"."\n\r".'A new calendar event "{$a->eventname}" on "{$a->eventtime}" has been added in {$a->lmsName}.';

$string['course_event_body'] = '{$a->name},'."\n\r"."\n\r".'A new calendar event "{$a->eventname}" on "{$a->eventtime}" has been added in {$a->lmsName}.';

$string['classroom_event_body'] = '{$a->name},'."\n\r"."\n\r".'A new calendar event "{$a->eventname}" on "{$a->eventtime}" has been added in {$a->lmsName}. ';

$string['user_event_body_update'] = '{$a->name},'."\n\r"."\n\r".'A calendar event "{$a->eventname}" on "{$a->eventtime}" has been updated in {$a->lmsName}.';
$string['global_event_body_update'] = '{$a->name},'."\n\r"."\n\r".'A calendar event "{$a->eventname}" on "{$a->eventtime}" has been updated in {$a->lmsName}.';
$string['course_event_body_update'] = '{$a->name},'."\n\r"."\n\r".'A calendar event "{$a->eventname}" on "{$a->eventtime}" has been updated in {$a->lmsName}.';
$string['classroom_event_body_update'] = '{$a->name},'."\n\r"."\n\r".'A calendar event "{$a->eventname}" on "{$a->eventtime}" has been updated in {$a->lmsName}.';

$string['user_event_body_delete'] = '{$a->name},'."\n\r"."\n\r".'A calendar event "{$a->eventname}" on "{$a->eventtime}" has been deleted from {$a->lmsName}.';
$string['global_event_body_delete'] = '{$a->name},'."\n\r"."\n\r".'A calendar event "{$a->eventname}" on "{$a->eventtime}" has been deleted from {$a->lmsName}.';
$string['course_event_body_delete'] = '{$a->name},'."\n\r"."\n\r".'A calendar event "{$a->eventname}" on "{$a->eventtime}" has been deleted from {$a->lmsName}.';
$string['classroom_event_body_delete'] = '{$a->name},'."\n\r"."\n\r".'A new event "{$a->eventname}" on "{$a->eventtime}" has been deleted from {$a->lmsName}.';

$string['class_enrol_for_instructor'] = '{$a->name},'."\n\r"."\n\r".'You have been assigned as an instructor for the following classroom course:<ul><li>'.'Course: {$a->coursename}</li>'.'<li>Date/Date Range: {$a->starttime}</li>{$a->location}</ul>{$a->sessionsListing}{$a->attachment_info}'.'For additional course details please log into {$a->lmsName} using the following link: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';

$string['class_enrol_for_instructor_update'] = '{$a->name},'."\n\r"."\n\r".'The following Classroom Course has been updated:<ul><li>'.'Course: {$a->coursename}</li>'.'<li>Date/Date Range: {$a->starttime}</li>{$a->location}</ul>{$a->attachment_info}'.'For additional course details please log into {$a->lmsName} using the following link: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';
$string['class_enrol_for_instructor_delete'] = '{$a->name},'."\n\r"."\n\r".'Your enrollment for the following Classroom Course has been cancelled as an instructor:<ul><li>'.'Course: {$a->coursename}</li>'.'<li>Date/Date Range: {$a->starttime}</li>{$a->location}</ul>{$a->attachment_info}'.'For additional course details please log into {$a->lmsName} using the following link: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';
$string['class_enrol_for_instructor_subject'] = 'LMS Classroom Enrollment: {$a->coursename} {$a->classstart}';
$string['class_enrol_for_instructor_subject_delete'] = 'LMS Classroom Cancellation: {$a->coursename} - {$a->classstart}';
$string['class_enrol_for_instructor_subject_update'] = 'LMS Classroom Enrollment: {$a->coursename} - {$a->classstart}';
/***** Event Mails Ends Here*******/

$string['email_before_footer_decline_text_manager'] = ''."\n\r"."\n\r".'Please contact admin if you\'re still interested in attending this course.';

/*Complaince course mail*/
$string['complaince_mail_to_learner'] = '{$a->name},'."\n\r"."\n\r".'Important message from {$a->lmsName}! A new optional course or webinar has been added to the LMS.'."\n\r"."\n\r".'Course ID is: {$a->courseid}'."\n\r"."\n\r".'Name of course is: {$a->coursename}';
$string['complaince_mail_to_learner_1'] = "\n\r".'Click here if you would like to enroll: ';
$string['complaince_mail_to_learner_2'] = "\n\r".'You will use the same username & password that you use to access your Entercom computer and email account.'."\n\r"."\n\r".'Questions, please see your manager.';
$string['complaince_mail_to_learner_subject'] = 'New Course';
/*Complaince course mail*/

/* User reactivation email */
$string['user_reactivation_subject'] = 'Welcome to {$a->LmsSubName}';
$string['user_reactivation_body'] = 'Hello {$a->name},'."\n\r"."\n\r".'Your {$a->lmsName} account has been reinstated. '."\n\r"."\n\r".'Your login credentials are as follows:'."\n\r".'Username: {$a->user_name}'."\n\r".'Password: {$a->password}'."\n\r"."\n\r".'For Security reasons, you must change your password.'."\n\r"."\n\r".'To login, please go to  TRANZACT Learning Management System (LMS) using the following link: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>'."\n\r"."\n\r".'In most mail programs, this should appear as a blue link which you can just click on. If that doesn\'t work, please copy and paste the web address into the address line at the top of your web browser window.';
/* User reactivation email */

$string['go_to_link_text'] = ''."\n\r"."\n\r".'Please login to the {$a->lmsName} using the following link to view complete details: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';
$string['add_to_outlook'] = ''."\n\r"."\n\r".'To update your Outlook calendar, please open the attached file and respond appropriately.';
$string['class_outlook_info'] = ''.'To update your Outlook calendar, please open the attached file and respond appropriately.'."\n\r"."\n\r";
$string['sessions_text'] = 'This class has following session(s):';


$string['manager_user_unenrol_program_1'] = '{$a->name},'."\n\r"."\n\r".'Due to administrative issues, we must cancel the enrollment for your employee "{$a->user_name}" for the following training program: '.'{$a->courseContent}.{$a->reason}'."\n\r"."\n\r".'Please do not reply to this e-mail.'."\n\r"."\n\r".'If you are still interested in having your employee complete this training program or if you have any questions, please contact the admin at:'."\n\r"."\n\r";
$string['manager_user_unenrol_program_subject'] = 'LMS Training Program Cancellation: {$a->courseContent}';

$string['user_enrol_request_course_department'] = '{$a->name},'."\n\r"."\n\r".'"{$a->user_name}" has requested the following online course for their department: "{$a->coursename}".'."\n\r"."\n\r".'Please go to your Requests Section in {$a->lmsName} using the following link to view complete details: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';
$string['user_enrol_request_program_department'] = '{$a->name},'."\n\r"."\n\r".'"{$a->user_name}" has requested the following Training Program for their department: "{$a->programname}".'."\n\r"."\n\r".'Please go to your Requests Section in {$a->lmsName} using the following link to view complete details: <a target = "_blank" href = "{$a->lmsLink}">{$a->lmsLink}</a>';

//$string['email_confirmationsubject_user'] = 'Welcome to {$a}';
//$string['email_confirmation_to_user'] = 'Hello {$a->firstname},'."\n\r"."\n\r".''.'A new account has been created in {$a->sitename} using your e-mail address.'."\n\r"."\n\r".'Your login credentials are as follows:'."\n\r".'Username: {$a->username}'."\n\r".'Password: {$a->password}'."\n\r"."\n\r".'We recommend that you change your password upon initial login by accessing your profile and then clicking on the "change password" link.'."\n\r"."\n\r".'To login, please go to {$a->sitename} using the following link: <a href = "{$a->link}">{$a->linkname}</a>.'."\n\r"."\n\r".''.'(In most mail programs, this should appear as a blue link which you can just click on. If that doesn\'t work, please copy and paste the web address into the address line at the top of your web browser window.)';
$string['email_confirmationsubject_user'] = 'Welcome to the {$a} Learning Portal';
$string['email_confirmation_to_user'] = 'Hello {$a->firstname},'."\n\r"."\n\r".''.'A new account has been created for you so that you can access our corporate-wide {$a->sitename}. Please use the following credentials to login for the first time:'."\n\r".'Username: {$a->username}'."\n\r".'Password: {$a->password}'."\n\r"."\n\r".'We recommend that you change your password upon initial login by accessing your profile and then clicking on the "Change Password" link.'."\n\r"."\n\r".'To login, please use the following link:'."\n\r".'<a href = "{$a->link}">{$a->link}</a>.'."\n\r"."\n\r".'If you are experiencing any difficulties,  please copy and paste the web address into the address line in your browser window.';