<?php

/**
	* Custom module - Signle Report Page
	* Date Creation - 25/06/2014
	* Date Modification : 25/06/2014
	* Created By : Rajesh Kumar
	* Last Modified By : Rajesh Kumar
**/
	
	
$string['reports'] = 'Reports';
$string['viewuserreports'] = 'View User Reports';
$string['overallcourseprogressreport'] = 'Overall Online Course Report';
$string['numberofcourses'] = 'Total Enrollment';
$string['notstarted'] = 'Not Started';
$string['inprogress'] = 'In Progress';
$string['completed'] = 'Completed';
$string['more'] = 'More';
$string['loading'] = 'Please wait... while loading graph';
$string['coursetitle'] = 'Course Name';
$string['title'] = 'Title';
$string['description'] = 'Description';
$string['lastaccessed'] = 'Last Accessed';
$string['attempts'] = '# Attempts';
$string['status'] = 'Status';
$string['score'] = 'Score';
$string['viewdetails'] = 'View Details';
$string['course'] = 'Course';
$string['department'] = 'Department';
$string['team'] = 'Team';
$string['users'] = 'Users';
$string['search'] = 'Search';
$string['all'] = 'All';
$string['alldepartments'] = 'All Departments';
$string['allteams'] = 'All Teams';
$string['allstatus'] = 'All Status';
$string['allusers'] = 'All Users';
$string['allcourses'] = 'All Courses';
$string['go'] = 'Go';
$string['startdate'] = 'Start Date';
$string['enddate'] = 'End Date';
$string['courseusagesreport'] = 'Online Course Usage Report';
$string['courseusagesreportgraph'] = 'Online Course Usage Report Graph';
$string['usereperformancereport'] = 'Learner Performance Report';
$string['copyright'] = 'Copyright &copy; '.date('Y');
$string['allrightsreserved'] = 'All rights reserved';
$string['downloadcsv'] = 'Download CSV';
$string['downloadpdf'] = 'Download PDF';
$string['print'] = 'Print';
$string['any'] = 'NA';
$string['none'] = 'None';
$string['user'] = 'Name (Username)';
$string['assettimespent'] = 'Time Spent (hh:mm)';