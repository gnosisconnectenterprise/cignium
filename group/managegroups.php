<?php

    require_once('../config.php');
    require_once($CFG->dirroot.'/local/group/filters/lib.php');
	require_once($CFG->dirroot.'/local/user/selector/lib.php');

	require_login();

    if($USER->archetype == $CFG->userTypeStudent) // if user is not manager redirect to home page.
	{
		redirect($CFG->wwwroot."/");
	}

    $courseid       = optional_param('courseid', 0, PARAM_INT);
    $delete       = optional_param('delete', 0, PARAM_INT);
    $confirm      = optional_param('confirm', '', PARAM_ALPHANUM);   //md5 confirmation hash
    $sort         = optional_param('sort', 'id', PARAM_ALPHANUM);
    $dir          = optional_param('dir', 'DESC', PARAM_ALPHA);
    $page         = optional_param('page', 1, PARAM_INT);
    $perpage      = optional_param('perpage', $CFG->perpage, PARAM_INT);        // how many per page
	$status       = optional_param('status', 0, PARAM_INT);


    $PAGE->set_title($SITE->fullname.": ".get_string('managegroups', 'group'));
    $stredit   = get_string('edit');
    $strdelete = get_string('delete');
    $strdeletecheck = get_string('deletecheck');
    $strassign   = get_string('assignusers', 'group');    	
    $strconfirm = get_string('confirm');
    $assigncourses = get_string('assign_courses', 'group');
    
    $default_set_mode = 1;
    if($CFG->isLdap==1){
    	$default_set_mode = 2;
    }
    
	
	$paramArray = array(
					'ch' => optional_param('ch', '', PARAM_ALPHA),
					'key' => optional_param('key', '', PARAM_RAW),
					//'name' => optional_param('name', '', PARAM_RAW),
					'des' => optional_param('des', '', PARAM_RAW),
					'department' => optional_param('department', '', PARAM_RAW),
					'sel_mode' => optional_param('sel_mode', $default_set_mode, PARAM_RAW),
					'teams' => optional_param('teams', '', PARAM_RAW),
					'users' => optional_param('users', '', PARAM_RAW)
					);
	$filterArray = array(							
						'name'=>get_string('name'),
						'des'=>get_string('description')
				   );
				   
	$removeKeyArray = array('id','action','perpage');
	
	$pageURL = '/group/managegroups.php';
		////// Getting common URL for the Search List //////
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
	 	
	if($status){	
	    $flagCheck = optional_param('flag', '', PARAM_ALPHANUM);
		if( ($flagCheck == '0') || ($flagCheck == '1') ){
			$gid = $status;		
			setTeamActiveFlag($gid, (int)$flagCheck);
			$_SESSION['update_msg'] = get_string('record_updated');
			redirect($genURL);
		}
	}

    if (empty($CFG->loginhttps)) {
        $securewwwroot = $CFG->wwwroot;
    } else {
        $securewwwroot = str_replace('http:','https:',$CFG->wwwroot);
    }

    $returnurl = new moodle_url('/group/managegroups.php', array('sort' => $sort, 'dir' => $dir));
    //$qStrArr = getQueryString(2);
    
    if ($delete and confirm_sesskey()) {              // Delete a selected group, after confirmation
		$groupid = $delete;
		$delArr = array('groups'=>$groupid,'sesskey'=>sesskey(),'confirm'=>1);
		$fArr = array_merge($delArr, $paramArray);
		redirect(new moodle_url('/local/group/delete.php', $fArr));
    } 

    checkPageIsNumeric($pageURL,$_REQUEST['page']);

    echo $OUTPUT->header();
    

	
	echo genCommonSearchForm($paramArray, $filterArray, $pageURL);
	ob_start();
	require_once($CFG->dirroot . '/local/includes/group_filter.php');
	$SEARCHHTML = ob_get_contents();
	ob_end_clean();
    $extracolumns = array();
    $columns = array_merge(array('name', 'department'),$extracolumns,
            array('timecreated'));

    foreach ($columns as $column) {
        $string[$column] = getGroupFieldName($column);
        if ($sort != $column) {
            $columnicon = "";
            if ($column == "timecreated") {
                $columndir = "DESC";
            } else {
                $columndir = "ASC";
            }
        } else {
            $columndir = $dir == "ASC" ? "DESC":"ASC";
            if ($column == "timecreated") {
                $columnicon = ($dir == "ASC") ? "sort_desc" : "sort_asc";
            } else {
                $columnicon = ($dir == "ASC") ? "sort_asc" : "sort_desc";
            }
            $columnicon = "<img class='iconsort' src=\"" . $OUTPUT->pix_url('t/' . $columnicon) . "\" alt=\"\" />";

        }
        //$$column = "<a href=\"managegroups.php?sort=$column&amp;dir=$columndir\">".$string[$column]."</a>$columnicon";
        $$column = $string[$column];
    }

	$extrasql = '';
	$params = array();
    $groups = getGroupsListing($sort, $courseid, $dir, $page, $perpage, '', $extrasql, $paramArray);
    $groupcount = getGroupsListing('groupcount', $courseid, $dir, $page, $perpage, '', $extrasql, $paramArray);
		

    $strall = get_string('all');

    $baseurl = new moodle_url('/group/managegroups.php', array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage));
	$pagingUrl = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);

    flush();

	// Start listing table
	$tableHtml = '<table class = "table1"><thead><tr>';
	$tableHtml .= "<td width = '16%' align='align_left' >$name</td>";		
	//$tableHtml .= "<td width = '30%' align='align_left' >$description</td>";
	
	if($CFG->isLdap == 1){
      $tableHtml .= "<td width = '8%' align='align_left' >$department</td>";
	  $tableHtml .= "<td width = '7%' align='align_left' >".get_string('groupsource','group')."</td>";
	}else{
	  $tableHtml .= "<td width = '15%' align='align_left' >$department</td>";
	}
	$tableHtml .= "<td width = '10%' align='align_left' >".get_string('select_parent')."</td>";
	$tableHtml .= "<td width = '15%' align='align_left' >".get_string('select_owner')."</td>";
	$tableHtml .= "<td width = '7%' align='align_left' >".get_string('noofusers')."</td>";
	$tableHtml .= "<td width = '9%' align='align_left' >".get_string('noofcourses')."</td>";	
	$tableHtml .= "<td width = '28%' align='align_left' >".get_string('manage')."</td>";	
	$tableHtml .= '</tr></thead>';
    if (!$groups) {
        $match = array();
      //  echo $OUTPUT->heading(get_string('nogroupsfound','group'));
		$tableHtml .= "<tr><td colspan = '4'>".get_string('no_results')."</td></tr>";		
        $table = NULL;

    } else {
        foreach ($groups as $group) {
		    $cntTotalUsers = $group->learnercount;
			$cntTotalCourses = $group->coursecount;

		    $createdBy = $group->createdby;
			$groupSource = $group->auth;
            $buttons = array();
            $lastcolumn = '';
			
			$statusArr = array_merge($paramArray, array('status'=>$group->id));
			$deleteArr = array_merge($paramArray, array('delete'=>$group->id, 'sesskey'=>sesskey()));
			$enrollUserArr = array_merge($paramArray, array('group'=>$group->id));
			$enrollCourseArr = array_merge($paramArray, array('group'=>$group->id));
			$editCourseArr = array_merge($paramArray, array('id'=>$group->id));
			$tableHtml .= "<tr>";
           // if($createdBy == $USER->id || $USER->archetype == $CFG->userTypeAdmin ){

			 //if($USER->archetype == $CFG->userTypeAdmin ){
			    if($group->is_active == 1){
					if($USER->archetype == $CFG->userTypeAdmin ){
						  // edit button
							 
								 if($USER->archetype == $CFG->userTypeAdmin || ($USER->archetype != $CFG->userTypeAdmin && $USER->department == $group->dep_id)){
									$buttons[] = html_writer::link(new moodle_url($securewwwroot.'/group/addgroup.php', $editCourseArr), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>$stredit, 'class'=>'iconsmall')), array('title'=>$stredit, 'class'=>'edit'));
								 }else{
									$buttons[] = html_writer::link('javascript:void(0);', html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>$stredit, 'class'=>'iconsmall')), array('title'=>$stredit, 'class'=>'edit-disable'));
								 }
			
							  
						}else{
						
							 if($USER->department == $group->dep_id){
								  $buttons[] = html_writer::link(new moodle_url($securewwwroot.'/group/addgroup.php', $editCourseArr), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>$stredit, 'class'=>'iconsmall')), array('title'=>$stredit, 'class'=>'edit'));
							 }else{
									$buttons[] = html_writer::link('javascript:void(0);', html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/edit'), 'alt'=>$stredit, 'class'=>'iconsmall')), array('title'=>$stredit, 'class'=>'edit-disable'));
							 }
						}
						
			
						$buttons[] = html_writer::link(new moodle_url($securewwwroot.'/group/assignusers.php', $enrollUserArr), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/group'), 'alt'=>$strassign, 'class'=>'iconsmall')), array('title'=>$strassign, 'class'=>'assign-group'));

						$buttons[] = html_writer::link(new moodle_url($securewwwroot.'/group/'.$CFG->pageTeamAssigncourses, $enrollCourseArr), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/group'), 'alt'=>$assigncourses, 'class'=>'iconsmall')), array('title'=>$assigncourses, 'class'=>'assign-courses'));
						if($group->auth=='manual')
						{
						$buttons[] = html_writer::link(new moodle_url($securewwwroot.'/group/managegroups.php?flag=0', $statusArr), get_string('deactivatelink'), array('title'=>get_string('deactivatelink'), 'class'=>'disable'));
						}
				 }else{
				 
				   $buttons[] = html_writer::link('javascript:void(0);', '', array('title'=>$stredit, 'class'=>'edit-disable'));
				 
				   $buttons[] = html_writer::link('javascript:void(0);', '', array('title'=>$strassign, 'class'=>'assign-group-disabled'));

				   $buttons[] = html_writer::link('javascript:void(0);', html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/group'), 'alt'=>$assigncourses, 'class'=>'iconsmall')), array('title'=>$assigncourses, 'class'=>'assign-courses disable'));
				   if($group->auth=='manual')
				   {
				  	$buttons[] = html_writer::link(new moodle_url($securewwwroot.'/group/managegroups.php?flag=1', $statusArr), get_string('activatelink'), array('title'=>get_string('activatelink'), 'class'=>'enable'));
				   }
				  
				   
				 }
				 
			// }

			
			if($CFG->isLdap == 1 && $groupSource == $CFG->authLdap){
			}else{
				//if(($createdBy == $USER->id || $USER->archetype == $CFG->userTypeAdmin ) && $cntTotalUsers == 0){
				if($USER->archetype == $CFG->userTypeAdmin  && $cntTotalUsers == 0){
					// delete button
					if($USER->archetype != $CFG->userTypeAdmin && $USER->department != $group->dep_id){
						$buttons[] = html_writer::link('#', html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/delete'), 'alt'=>$strdelete, 'class'=>'iconsmall')), array('title'=>$strdelete, 'class'=>'delete disabled'));
					}else{
						$buttons[] = html_writer::link(new moodle_url($returnurl, $deleteArr), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/delete'), 'alt'=>$strdelete, 'class'=>'iconsmall')), array('title'=>$strdelete, 'class'=>'delete delete_team'));
					}
				}else{
				
				    if($USER->archetype != $CFG->userTypeAdmin && $USER->department != $group->dep_id ){
						$buttons[] = html_writer::link('#', html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/delete'), 'alt'=>$strdelete, 'class'=>'iconsmall')), array('title'=>$strdelete, 'class'=>'delete disabled'));
					}else{
					    if($cntTotalUsers == 0){
					      $buttons[] = html_writer::link(new moodle_url($returnurl, $deleteArr), html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/delete'), 'alt'=>$strdelete, 'class'=>'iconsmall')), array('title'=>$strdelete, 'class'=>'delete delete_team'));
					    }else{
						  $buttons[] = html_writer::link('#', html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/delete'), 'alt'=>$strdelete, 'class'=>'iconsmall')), array('title'=>$strdelete, 'class'=>'delete disabled'));
						}	
					}
				}
			}
			
			
			
			//start group image
			
			/*if($group->picture != ''){
				$groupImageRootPath =  $CFG->dirroot.'/theme/gourmet/pix/group/'.$group->picture;
				$groupImagePath =  $CFG->wwwroot.'/theme/gourmet/pix/group/'.$group->picture;
				if(!file_exists($groupImageRootPath)){
				 $groupImagePath =  $CFG->wwwroot.'/theme/gourmet/pix/group-img.jpg';
			    }
			}else{
			   $groupImagePath =  $CFG->wwwroot.'/theme/gourmet/pix/group-img.jpg';
			}
			$groupImage = '<img src="'.$groupImagePath.'" alt="'.$group->name.'" title="'.$group->name.'" border="0" />';*/
            
			$groupImage = getTeamImage($group);
			
			//end group image

			//$tableHtml .= "<td>".$group->name."</td>";
			if($group->dept_title == ''){
				$DeptTitle = get_string("global_group_column");
				$identifierGlobal = '<small class = "global-identifier" >'.get_string("global_team_identifier").'</small>';
			}else{
				$DeptTitle = $group->dept_title;
				$identifierGlobal = '';
			}
			$tableHtml .= '<td class="align_left">'.$groupImage . '<span class="f-left">'.$identifierGlobal.$group->name.'</span></td>';
			if(strlen($group->description) >200){
				$description = substr($group->description,0,200)." ...";
			}else{
				$description = $group->description;
			}
			
			
			//$tableHtml .= "<td>".nl2br($description)."</td>";
			if(!$group->parent_name){
				$group->parent_name = getMDash();
			}
            if($CFG->isLdap == 1){
			  $tableHtml .= "<td>".$DeptTitle."</td>";
			  $tableHtml .= "<td>".strtoupper($group->auth)."</td>";
			}else{
			  $tableHtml .= "<td>".$DeptTitle."</td>";
			}
			$tableHtml .= "<td>".$group->parent_name."</td>";
			$ownerName = getMDash();
			if($group->uid != '' && $group->uid != 0){
				$ownerName = $group->firstname." ".$group->lastname." (".$group->username.")";
			}
			$tableHtml .= "<td>".$ownerName."</td>";

			$tableHtml .= "<td>".$cntTotalUsers."</td>";
			$tableHtml .= "<td>".$cntTotalCourses."</td>";
			//$tableHtml .= "<td>".getDateFormat($group->timecreated, $CFG->customDefaultDateFormat)."</td>";
			$tableHtml .= "<td class='adminiconsBar'>".implode(' ', $buttons)."</td>";
			$tableHtml .= "</tr>";
        }
    }
	$tableHtml .= "</table>";
	// End listing table

   /* added by rajesh to redirect the page otherwise filter were adding again and again while we refresh the page */	
    if(isset($_REQUEST['addfilter']) && !empty($_REQUEST['addfilter'])){
	    redirect($returnurl);
	}
	// end 


	echo '<div class = "add-course-button"><a class="button-link add-team" href="'.$securewwwroot.'/group/addgroup.php" ><i></i><span>'.get_string("addnewgroup","group").'</span></a></div>';
	echo $SEARCHHTML;
   
	//// Record update Message
	if($_SESSION['update_msg']!=''){
		echo '<div class="clear"></div>';
		echo '<div class="bg-success text-success text-center" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
		echo '<div class="clear"></div>';
		$_SESSION['update_msg']='';
	}
	/// Record update message ends
    if (!empty($tableHtml)) {
		echo '<div class = "borderBlock">';
		echo "<h2 class='icon_title'><span class='group'></span>".get_string('ggroup','group')." (".$groupcount.")</h2>";
        echo html_writer::start_tag('div', array('class'=>'borderBlockSpace'));
        echo $tableHtml;
        echo html_writer::end_tag('div');
        echo html_writer::end_tag('div');

		//Pring paging bar
        echo paging_bar($groupcount, $page, $perpage, $pagingUrl);
    }

    echo $OUTPUT->footer();
    ?>
<script>


$(document).ready(function(){


	$(".delete_team").click(function(){
		
		var slotid = $(this).attr("rel");
		var txt = "<?php echo get_string('teamdeleteconfirm')?>";
	
		
		var response = confirm(txt);
		
		if (response == true) {
		return true;
		}
		return false;
	});
});

</script>
