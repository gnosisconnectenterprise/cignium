<?php

	require_once('../config.php');
	require_once('../local/group/group_form.php');

	require_login();

	if($USER->archetype == $CFG->userTypeStudent) // if user is not manager redirect to home page.
	{
		redirect($CFG->wwwroot."/");
	}

	$courseid = 0;
	// get url variables
	$id       = optional_param('id', 0, PARAM_INT);
	$confirm  = optional_param('confirm', 0, PARAM_BOOL);

	if ($id) {
		if (!$group = $DB->get_record('groups', array('id'=>$id))) {
			print_error('invalidgroupid');
		}
		
		$groupDepartment = $DB->get_record_sql('SELECT g.id,gd.department_id FROM mdl_groups AS g LEFT JOIN mdl_group_department AS gd ON g.id = gd.team_id WHERE g.id = '.$group->id);
		if(!empty($groupDepartment)){
			$group->department_id = $groupDepartment->department_id;
			if($group->department_id == '' || $group->department_id == null){
				$group->department_id = 0;
			}
		}
		
	} else {

		$group = new stdClass();
		$group->courseid = $courseid;
		$group->department_id = 0;
	}

	if ($id !== 0) {
		$PAGE->set_url('/group/addgroup.php', array('id'=>$id));
	} else {
		$PAGE->set_url('/group/addgroup.php');
	}
	



	$site = get_site();
	$strgroups = get_string('groups');
	$PAGE->set_title($site->fullname.": ".$strgroups);
	$PAGE->set_heading($site->fullname . ': '.$strgroups);
	$PAGE->set_pagelayout('standard');
	navigation_node::override_active_url(new moodle_url('/group/managegroups.php'));
	
	
	$removeKeyArray = array('id','action','perpage');
	$qStrArr = getQueryString(2);
	$pagePostURL = '/group/addgroup.php';
	$posturl = genParameterizedURL($qStrArr, array('id'), $pagePostURL);
	
	$pageURL = '/group/managegroups.php';
	$returnUrl = genParameterizedURL($qStrArr, $removeKeyArray, $pageURL);
	
	//$returnUrl = $CFG->wwwroot.'/group/managegroups.php';

	if($group->courseid != 0){

		$context = context_course::instance($group->courseid);
		require_capability('moodle/course:managegroups', $context);

	}else{
		$context = context_system::instance();
	}


	// Prepare the description editor: We do support files for group descriptions
	$editoroptions = array('maxfiles'=>EDITOR_UNLIMITED_FILES, 'maxbytes'=>@$course->maxbytes, 'trust'=>false, 'context'=>$context, 'noclean'=>true);
	if (!empty($group->id)) { 
		if($group->courseid != 0){ 
		  $editoroptions['subdirs'] = file_area_contains_subdirs($context, 'group', 'description', $group->id);
		  $group = file_prepare_standard_editor($group, 'description', $editoroptions, $context, 'group', 'description', $group->id);
		}else{
		  $editoroptions['subdirs'] = false;
		  $group = file_prepare_standard_editor($group, 'description', $editoroptions, $context, 'group', 'description', null);
		}

	   
	} else { 
		$editoroptions['subdirs'] = false;
		$group = file_prepare_standard_editor($group, 'description', $editoroptions, $context, 'group', 'description', null);
	}

	/// First create the form
	$contextLevel = 140; 
	$query = "SELECT * FROM {$CFG->prefix}context WHERE contextlevel = '".$contextLevel."' AND instanceid = '".$group->id."'";
	$programContext = $DB->get_record_sql($query);
	$program->id = $programContext->id;
	if ($filesOptions = programOverviewFilesOptionsGroup($program)) {
	   file_prepare_standard_filemanager($program, 'teamimage', $filesOptions, $programContext, 'team', 'teamimage', 0);
	}
	$group->teamimage_filemanager = $program->teamimage_filemanager;
	
	
	
	
	$editform = new group_form($posturl, array('editoroptions'=>$editoroptions,'groupdata'=>$group));
	$editform->set_data($group);

	if ($editform->is_cancelled()) {
		redirect($returnUrl);

	} elseif ($data = $editform->get_data()) {
		$data->parent_id = $_POST['parent_id'];
		if ($data->id) { 

			// Upload image file
			if(!empty($_FILES) && $_FILES["group_image"]["error"] == 0){
				$path = $CFG->dirroot.'/theme/gourmet/pix/group/'; 
				if(!is_dir($path)){
					mkdir($path,0777);
					chmod($path,0777);
				}
				$imgname = time().'_'.$_FILES["group_image"]["name"];
				$imgDataExists = $DB->get_record_sql("select * from mdl_groups where id = ".$data->id);
				if(!empty($imgDataExists)){
					$img2 = $imgDataExists->picture;
					$path_old = $path.$img2;
					if($img2 != ''){
						if(file_exists($path_old)){
							@unlink($path_old);
						}
					}
					if(move_uploaded_file($_FILES["group_image"]["tmp_name"], $path.$imgname)){
						$data->picture = $imgname;
					}
				}else{
					if(move_uploaded_file($_FILES["group_image"]["tmp_name"], $path.$imgname)){
						$data->picture = $imgname;
					}
				}
			}
			customGroupsUpdateGroup($data, $editform, $editoroptions);
			$_SESSION['update_msg'] = get_string('record_updated');
		} else {
			// Upload image file
			/*if($_FILES["group_image"]["error"] == 0){
				$path = $CFG->dirroot.'/theme/gourmet/pix/group/'; 
				if(!is_dir($path)){
					mkdir($path,0777);
					chmod($path,0777);
				}
				$imgname = time().'_'.$_FILES["group_image"]["name"];
				if(move_uploaded_file($_FILES["group_image"]["tmp_name"], $path.$imgname)){
					$data->picture = $imgname;
				}
			}*/
			$id = customGroupsCreateGroup($data, $editform, $editoroptions);
			$_SESSION['update_msg'] = get_string('record_added');
			$returnUrl = $CFG->wwwroot.'/group/managegroups.php';
		}

		redirect($returnUrl);
	}

	$strgroups = get_string('managegroups','menubar');
	$strparticipants = get_string('users','menubar');

	if ($id) {
		$strheading = get_string('editgroup', 'group');
	} else {
		$strheading = get_string('creategroup', 'group');
	}

	$PAGE->navbar->add($strparticipants);
	$PAGE->navbar->add($strgroups, new moodle_url('/group/managegroups.php'));
	$PAGE->navbar->add($strheading);

	/// Print header
	echo $OUTPUT->header();
	echo '<div class = "borderBlock borderBlockSpace">';
	$editform->display();
	echo '</div>';
function programOverviewFilesOptions($program) {
		global $CFG, $DB;
		if (empty($CFG->programOverviewFilesLimit)) {
			return null;
		}

		$accepted_types = preg_split('/\s*,\s*/', trim($CFG->programOverviewFilesExt), -1, PREG_SPLIT_NO_EMPTY);
		if (in_array('*', $accepted_types) || empty($accepted_types)) {
			$accepted_types = '*';
		} else {
			// Since config for $CFG->programOverviewFilesExt is a text box, human factor must be considered.
			// Make sure extensions are prefixed with dot unless they are valid typegroups
			foreach ($accepted_types as $i => $type) {
				if (substr($type, 0, 1) !== '.') {
					require_once($CFG->libdir. '/filelib.php');
					if (!count(file_get_typegroup('extension', $type))) {
						// It does not start with dot and is not a valid typegroup, this is most likely extension.
						$accepted_types[$i] = '.'. $type;
						$corrected = true;
					}
				}
			}
			if (!empty($corrected)) {
				set_config('programOverviewFilesExt', join(',', $accepted_types));
			}
		}
		$options = array(
			'maxfiles' => $CFG->programOverviewFilesLimit,
			'maxbytes' => $CFG->maxbytes,
			'subdirs' => 0,
			'accepted_types' => $accepted_types
		);
		if (!empty($program->id)) {
		
		  $contextLevel = 140; 
		  $query = "SELECT * FROM {$CFG->prefix}context WHERE contextlevel = '".$contextLevel."' AND instanceid = '".$program->id."' ";
		  $context = $DB->get_record_sql($query);
		  $options['context'] = $context;
		  
		  
		} else if (is_int($program) && $program > 0) {
		  
		  $contextLevel = 140; 
		  $query = "SELECT * FROM {$CFG->prefix}context WHERE contextlevel = '".$contextLevel."' AND instanceid = '".$program."' ";
		  $context = $DB->get_record_sql($query);
		  $options['context'] = $context;
		}
		 return $options;
	
   }
	?>
    <style>
    .customreq{  float: left;
    margin-left: 244px;
    margin-top: 6px;
	position: absolute;}
    </style>
	<script>
	$(document).ready(function(){
		$(".removeimage").click(function(){
			var groupId = $(this).attr('rel');
			message = "<?php echo get_string('delete_image_alert_message')?>";
			var r = confirm(message);
			if (r == true) {
				$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
					type:'POST',
					data:'action=deleteGroupImage&groupId='+groupId,
					success:function(data){
						alert("<?php echo get_string('image_deleted_successfully');?>");
						window.location.reload(true);
					}
				});
			}else{
				return false;
			}
		});
		$(document).on('blur', '#id_name', function(){

			   var cVal = $(this).val();
			   cVal = $.trim(cVal);
			   $(this).val(cVal);


		});
		
		$(document).on("click", "#id_submitbutton", function(){

			var id_name = $('#id_name').val();
			id_name =  $.trim(id_name);
	
			if( id_name != '' ){  
				$(this.form).submit(function(){
				
					 $("#id_submitbutton").prop('disabled', 'disabled');
					 $('.disabled_user_form_fields').attr('disabled', false);
				
				});
			}
		});	
		$("#id_department").on('change',function(){
			var department = $(this).val();
			if(department == 0){
				department = '-2';
			}
			team = '-1';
			user = '-1';
			$.ajax({	  
				url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
				type:'POST',
				data:'action=getTeamListByDepartment&department='+department+"&team="+team,
				dataType:'json',
				success:function(data){		  
					var success = data.success;
					var error = data.error;
					if(success == 1){
					  var selectHtml = data.response;
					  selectHtml = $.trim(selectHtml);
					  var allTeam = 'All Teams';
					  var allTeamReplace = "<?php echo get_string('select_parent_team'); ?>";
					  var res = selectHtml.split("<option");
					  console.log(res[1]);
					  res[1] = res[1].replace(allTeam, allTeamReplace); 
					  console.log(res[1]);
					  res = res.join("<option");
					  $('#id_parent_id').html(res);
				   }else{
					 alert(error);
				   }
				}	  
			}); 
			 $.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
				type:'POST',
				data:'action=getUserListByDnT&department='+department+"&team="+team+"&user="+user+"&isReport=1",
				dataType:'json',
				success:function(data){
				  
				   var success = data.success;
					 var error = data.error;
					   if(success == 1){
						  var selectHtml = data.response;
						  selectHtml = $.trim(selectHtml);
						  var allTeam = 'All Users';
						  var allTeamReplace = "<?php echo get_string('select_owner_dropdown'); ?>";
						  var res = selectHtml.split("<option");
						  console.log(res[1]);
						  res[1] = res[1].replace(allTeam, allTeamReplace); 
						  console.log(res[1]);
						  res = res.join("<option");
						  $('#id_group_owner').html(res);
					   }else{
						 alert(error);
					   }
				}
			  
			  });
		});
	});
	</script>
	<?php
	echo $OUTPUT->footer();
