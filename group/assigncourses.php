<?php

	require_once(dirname(__FILE__) . '/../config.php');
	require_once(dirname(__FILE__) . '/lib.php');
	require_once($CFG->dirroot . '/local/user/selector/lib.php');
	require_once($CFG->dirroot . '/course/lib.php');
	require_once($CFG->libdir . '/filelib.php');
	require_login();
	$site = get_site();
	$groupid = required_param('group', PARAM_INT);
	$cancel  = optional_param('cancel', false, PARAM_BOOL);
	$group = $DB->get_record('groups', array('id'=>$groupid), '*', MUST_EXIST);
	$isGlobalArray = $DB->get_record_sql("SELECT id FROM mdl_group_department WHERE team_id = ".$groupid);
	$isGlobal = 0;
	if(empty($isGlobalArray)){
		$isGlobal = 1;
	}
	// Start assign course post operations
	if (optional_param('add', false, PARAM_BOOL) && confirm_sesskey()) {
		addCourseToGroup($groupid,$_REQUEST['addcourse']);
		redirect($CFG->wwwroot."/group/".$CFG->pageTeamAssigncourses."?group=".$groupid);
	}
	if (optional_param('remove', false, PARAM_BOOL) && confirm_sesskey()) {
		removeCourseFromGroup($groupid,$_REQUEST['removecourse']);
		sendCoursesUnEnrolMailToTeam($groupid,$_REQUEST['removecourse']);
		redirect($CFG->wwwroot."/group/".$CFG->pageTeamAssigncourses."?group=".$groupid);
	}
	// End assign course post operations
	$context = context_system::instance();
	
	$removeKeyArray = array('id','action','perpage','group');
	$pagePostURL = '/group/'.$CFG->pageTeamAssigncourses;
	$pageURL = '/group/managegroups.php';
	
	$qStrArr = getQueryString(2);
	$posturl = genParameterizedURL($qStrArr, array(), $pagePostURL);
	$returnurl = genParameterizedURL($qStrArr, $removeKeyArray, $pageURL);
	//$returnurl = $CFG->wwwroot.'/group/managegroups.php';

	if ($cancel) {
		redirect($returnurl);
	}
	
	//Starts set page setting data
	$PAGE->set_url('/group/'.$CFG->pageTeamAssigncourses, array('group'=>$groupid));
	$PAGE->set_pagelayout('admin');
	$PAGE->navbar->add(get_string('managegroups','group'), new moodle_url('/group/managegroups.php'));
	$PAGE->navbar->add(get_string('assign_courses','group'));
	$courses = new non_group_courses_selector('', array('groupid' => $groupid,'isGlobal'=>$isGlobal));
	$courses->get_non_group_courses();
	$courses->get_group_courses();
	$PAGE->set_title("$site->fullname: $stradduserstogroup");
	$PAGE->set_heading($site->fullname);
	echo $OUTPUT->header();
	//Ends set page setting data

	$groupinforow = array();

	//Start show team image
	$picturecell = new html_table_cell();
	$picturecell->attributes['class'] = 'left side picture';
	$picturecell->text ='<div class="felement fstatic">'.getTeamImage($group).'</div>';
	$groupinforow[] = $picturecell;
	//End show team image

	// Check if there is a description to display.
	$group->description = file_rewrite_pluginfile_urls($group->description, 'pluginfile.php', $context->id, 'group', 'description', $group->id);
	if (!isset($group->descriptionformat)) {
		$group->descriptionformat = FORMAT_MOODLE;
	}

	$options = new stdClass;
	$options->overflowdiv = true;

	$contentcell = new html_table_cell();
	$contentcell->attributes['class'] = 'content';
	$contentcell->text = '<b>'.$group->name.'</b></br>';
	if($group->description != ''){
		$contentcell->text .= format_text($group->description, $group->descriptionformat, $options);
	}
	$groupinforow[] = $contentcell;

	// Check if we have something to show.
	if (!empty($groupinforow)) {
		$groupinfotable = new html_table();
		$groupinfotable->attributes['class'] = 'groupinfobox';
		$groupinfotable->data[] = new html_table_row($groupinforow);
		//echo html_writer::table($groupinfotable);
	}
	
	$headerHtml = getModuleHeaderHtml($group, $CFG->teamModule);
    echo $headerHtml;
	
	$isLDapOn = 0;				  
	if($CFG->isLdap == 1){
	
	  $groupSource = $group->auth;
	  if($groupSource == $CFG->authLdap){
		$isLDapOn = 1;
	  }	
	}


	/// Print the editing form
	?>

	<div class="borderBlock borderBlockSpace" id="addmembersform">
		<!--form id="assignform" method="post" action="<?php //echo $CFG->wwwroot; ?>/group/<?php echo $CFG->pageTeamAssigncourses; ?>?group=<?php //echo $groupid; ?>"-->
		<form id="assignform" method="post" action="<?php echo $posturl; ?>">
		<div>
		<input type="hidden" name="sesskey" value="<?php p(sesskey()); ?>" />

		 <table class="assignTable">
        <tr>
          <td id='potentialcell' class="potentialcell">
			  <p>
				<label for="addselect"><?php echo get_string('nonteamcourses','group');?></label>
			  </p>
			  <div id="addselect_wrapper" class="userselector">
				<select multiple="multiple" name = "addcourse[]" size = '20'>
					<?php $courses->course_option_list(1); ?>
				</select>
                <div class="search-course-box searchBoxDiv" >
                	<div class="search-input" ><input type="text" value="" placeholder="Search" id="addselect_searchtext" name="addselect_searchtext" ></div>
                    <div class="search_clear_button">
                    	<input type="button" title="Search" id="addselect_searchtext_btn" value="Search" name="search">
                        	<a title="Clear"  id="addselect_clearbutton"  href="javascript:void(0);">Clear</a>
                     </div>      
                 </div>
			  </div>
		  </td>
		  <td id='buttonscell'>
			<div class="arrow_button">
				<input class="moveLeftButton" type="button" name="remove" id="remove" value="<?php echo get_string('remove').'&nbsp;'.$OUTPUT->rarrow(); ?>" title="<?php print_string('remove'); ?>" disabled = ""/><input class="moveRightButton" type="button" name="add" id="add" value="<?php echo $OUTPUT->larrow().'&nbsp;'.get_string('add'); ?>" title="<?php print_string('add'); ?>" disabled = ""/>
			</div>
		  </td>
		   <td id='existingcell' class="potentialcell">
			  <p>
				<label for="removeselect"><?php echo get_string('teamcourses','group');?></label><a href="#" name="updatecourse" id="updateCourse" class="updateDate"  title = "<?php echo get_string('update_date'); ?>">update</a>
			  </p>
			  <div id="removeselect_wrapper" class="userselector">
				<select multiple="multiple" name = "removecourse[]" size = '20'>
					<?php $courses->course_option_list(2); ?>
				</select>
                <div class="search-course-box searchBoxDiv" >
                	<div class="search-input" ><input type="text" value="" placeholder="Search" id="removeselect_searchtext" name="removeselect_searchtext" ></div>
                    <div class="search_clear_button">
                    	<input type="button" title="Search" id="removeselect_searchtext_btn" value="Search" name="search">
                        <a title="Clear"  id="removeselect_clearbutton"  href="javascript:void(0);">Clear</a>
                     </div>      
                 </div>
			  </div>
			</td>
		  <!--<td>
			<p><?php echo($strusergroupmembership) ?></p>
			<div id="group-usersummary"></div>
		  </td>-->
		</tr>
        
       
    
		<tr><td colspan="3" id='backcell' >
			<input type="submit" name="cancel" value="<?php print_string('back'); ?>" />
		</td></tr>
		</table>
		</div>
		</form>
	</div>
<a href="#end-date-div" id="inline-iframe" rel="inline-iframe" title="<?php print_string('allocation'); ?>">click</a>
<div class="end-date-div" id="end-date-div">
	<iframe src="" id="i-frame" width="100%" height="100%"></iframe>
</div>
	<script>
	$(document).ready(function(){
		$("#inline-iframe").colorbox({inline:true, width:"675px", height:"468px"});
		var teamId = "<?php echo $groupid; ?>";
		$("#addselect_searchtext_btn").click(function(){
			var searchText = $('#addselect_searchtext').val();

			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/group/searchcourses.php',
					type:'POST',
					data:'action=searchNonCourse&groupid=<?php echo $groupid; ?>&search_text='+searchText,
					success:function(data){
						$('#addselect_wrapper').find('select').html(data);
					}
			});
		});
		$("#addselect_clearbutton").click(function(){
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/group/searchcourses.php',
					type:'POST',
					data:'action=searchNonCourse&groupid=<?php echo $groupid; ?>',
					success:function(data){
						$('#addselect_wrapper').find('select').html(data);
						$("#addselect_searchtext").val('');
					}
			});
		});
		$("#removeselect_clearbutton").click(function(){
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/group/searchcourses.php',
					type:'POST',
					data:'action=searchGroupCourse&groupid=<?php echo $groupid; ?>',
					success:function(data){
						$('#removeselect_wrapper').find('select').html(data);
						$("#removeselect_searchtext").val('');
					}
			});
		});
		$("#removeselect_searchtext_btn").click(function(){
			var searchText = $('#removeselect_searchtext').val();
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/group/searchcourses.php',
					type:'POST',
					data:'action=searchGroupCourse&groupid=<?php echo $groupid; ?>&search_text='+searchText,
					success:function(data){
						$('#removeselect_wrapper').find('select').html(data);
					}
			});
		});
		$(document).on("change", "#addselect_wrapper select",function(){
			$("#add").removeAttr("disabled");
		});
		$(document).on("change", "#removeselect_wrapper select",function(){
			$("#remove").removeAttr("disabled");
		});
		var fromCourse = $('#addselect_wrapper select');
		var toCourse = $('#removeselect_wrapper select');
		$(document).on("click","#add",function(){
			var courseList = '';
			fromCourse.find('option:selected').each(function(){
				courseList += $(this).val()+',';
			});	

			if(courseList == '' || courseList == ','){
				alert("<?php echo get_string('select_course_to_enrol');?>");
				return false;
			}
			courseList = courseList.substr(0, courseList.length - 1);

			var source = '<?php echo $CFG->wwwroot;?>/course/enrolcourse.php?element=course&assigntype=team&assignId='+teamId+'&elementList='+courseList;
			$("#i-frame").attr('src',source);
			$(".cboxContent #end-date-div").attr("style",'display:block');
			$("#inline-iframe").trigger("click");
		});
		$(document).on("click","#updateCourse",function(){
			var courseList = '';
			toCourse.find('option:selected').each(function(){
				courseList += $(this).val()+',';
			});	
			
			if(courseList == '' || courseList == ','){
				alert("<?php echo get_string('select_course_to_update');?>");
				return false;
			}
			courseList = courseList.substr(0, courseList.length - 1);
			var source = '<?php echo $CFG->wwwroot;?>/course/enrolcourse.php?element=course&action=update&assigntype=team&assignId='+teamId+'&elementList='+courseList;
			$("#i-frame").attr('src',source);
			$(".cboxContent #end-date-div").attr("style",'display:block');
			$("#inline-iframe").trigger("click");
		});
		$(document).on("click","#remove",function(){
			var courseList = '';
			var coursesNotAllowed = '';
			toCourse.find('option:selected').each(function(){
				if($(this).attr('class') == 'disable-grey'){
					coursesNotAllowed += $(this).text()+',';
				}else{
					courseList += $(this).val()+',';
				}
			});
			if(coursesNotAllowed != ''){
				coursesNotAllowed = coursesNotAllowed.substr(0, coursesNotAllowed.length - 1);
				alert("You cannot remove "+coursesNotAllowed+" course(s).");
				return false;
			}
			if(courseList == '' || courseList == ','){
				alert("<?php echo get_string('select_course_to_unenrol');?>");
				return false;
			}
			courseList = courseList.substr(0, courseList.length - 1);
			var source = '<?php echo $CFG->wwwroot;?>/course/unenrolcourse.php?element=course&assigntype=team&assignId='+teamId+'&elementList='+courseList;
			$("#i-frame").attr('src',source);
			$(".cboxContent #end-date-div").attr("style",'display:block');
			$("#inline-iframe").trigger("click");

		});
	});
	
	
	
	</script>
	<style>
#end-date-div{ display:none;}
#colorbox #end-date-div{display:block}
#inline-iframe{ position:absolute; left:-100000px;}
</style>
	<?php
	echo $OUTPUT->footer();
