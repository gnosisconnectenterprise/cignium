<?php

require_once(dirname(__FILE__) . '/../config.php');
require_once(dirname(__FILE__) . '/lib.php');
require_once($CFG->dirroot . '/local/user/selector/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->libdir . '/filelib.php');
require_login();
$site = get_site();
$groupid = required_param('group', PARAM_INT);
$cancel  = optional_param('cancel', false, PARAM_BOOL);

$group = $DB->get_record('groups', array('id'=>$groupid), '*', MUST_EXIST);

$course->id = 0;
if($group->courseid){
  $course = $DB->get_record('course', array('id'=>$group->courseid), '*', MUST_EXIST);
}

$PAGE->set_url('/group/assignusers.php', array('group'=>$groupid));
$PAGE->set_pagelayout('admin');

//checkLogin('assignusertogroup');

if($group->courseid){
	$context = context_course::instance($course->id);
	require_capability('moodle/course:managegroups', $context);
}else{
  $context = context_system::instance();
}

$removeKeyArray = array('id','action','perpage','group');
$pagePostURL = '/group/assignusers.php';
$pageURL = '/group/managegroups.php';

$qStrArr = getQueryString(2);
$posturl = genParameterizedURL($qStrArr, array(), $pagePostURL);
$returnurl = genParameterizedURL($qStrArr, $removeKeyArray, $pageURL);

//$returnurl = $CFG->wwwroot.'/group/managegroups.php';

if ($cancel) {
    redirect($returnurl);
}


$groupmembersselector = new group_members_selector('removeselect', array('groupid' => $groupid, 'courseid' => $course->id));

$potentialmembersselector = new group_non_members_selector('addselect', array('groupid' => $groupid, 'courseid' => $course->id));

if (optional_param('add', false, PARAM_BOOL) && confirm_sesskey()) {
    $userstoadd = $potentialmembersselector->get_selected_users();
	
    if (!empty($userstoadd)) {
        foreach ($userstoadd as $user) {  
            if (!customGroupsAddMember($groupid, $user->id)) {  
                print_error('erroraddremoveuser', 'group', $returnurl);
            }
			
            $groupmembersselector->invalidate_selected_users();
            $potentialmembersselector->invalidate_selected_users();
        }
    }
}

if (optional_param('remove', false, PARAM_BOOL) && confirm_sesskey()) {
    $userstoremove = $groupmembersselector->get_selected_users();
    if (!empty($userstoremove)) {
        foreach ($userstoremove as $user) {
            /*if (!CustomGroupsRemoveMemberAllowed($groupid, $user->id)) {
                print_error('errorremovenotpermitted', 'group', $returnurl,
                        $user->fullname);
            }*/
            if (!customGroupsRemoveMember($groupid, $user->id)) {
                print_error('erroraddremoveuser', 'group', $returnurl);
            }
            $groupmembersselector->invalidate_selected_users();
            $potentialmembersselector->invalidate_selected_users();
        }
    }
}

// Print the page and form

$strparticipants = get_string('guser','group');
$strgroups = get_string('ggroup','group');
$strmanagegroups = get_string('managegroups','group');
$stradduserstogroup = get_string('adduserstogroup', 'group');
$strusergroupmembership = get_string('usergroupmembership', 'group');

$groupname = format_string($group->name);

$PAGE->requires->js('/group/clientlib.js');
$PAGE->navbar->add($strparticipants);
$PAGE->navbar->add($strgroups);
$PAGE->navbar->add($strmanagegroups, new moodle_url('/group/managegroups.php'));
$PAGE->navbar->add($stradduserstogroup);

/// Print header
$PAGE->set_title("$site->fullname: $stradduserstogroup");
$PAGE->set_heading($site->fullname);
echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('adduserstogroup', 'group').": $groupname", 3);

// Store the rows we want to display in the group info.
$groupinforow = array();

// Check if there is a picture to display.
//if (!empty($group->picture) ) {
    /*$picturecell = new html_table_cell();
    $picturecell->attributes['class'] = 'left side picture';
    $picturecell->text = print_group_picture($group, 1, true, true, false);*/

	/*if($group->picture != '' && $group->picture != 0){
		$view =  $CFG->dirroot.'/theme/gourmet/pix/group/'.$group->picture;
		$view1 =  $CFG->wwwroot.'/theme/gourmet/pix/group/'.$group->picture;
	}else{
		$view =  $CFG->dirroot.'/theme/gourmet/pix/group-img.jpg';
		$view1 =  $CFG->wwwroot.'/theme/gourmet/pix/group-img.jpg';
	}*/
	$picturecell = new html_table_cell();
	$picturecell->attributes['class'] = 'left side picture';
	$picturecell->text ='<div class="felement fstatic">'.getTeamImage($group).'</div></div>';
	$groupinforow[] = $picturecell;
//}

// Check if there is a description to display.
$group->description = file_rewrite_pluginfile_urls($group->description, 'pluginfile.php', $context->id, 'group', 'description', $group->id);
//if (!empty($group->description)) {
    if (!isset($group->descriptionformat)) {
        $group->descriptionformat = FORMAT_MOODLE;
    }

    $options = new stdClass;
    $options->overflowdiv = true;

    $contentcell = new html_table_cell();
    $contentcell->attributes['class'] = 'content';
    $contentcell->text = '<b>'.$group->name.'</b></br>';
	if($group->description != ''){
		$contentcell->text .= format_text($group->description, $group->descriptionformat, $options);
	}
    $groupinforow[] = $contentcell;
//}

// Check if we have something to show.
if (!empty($groupinforow)) {
    $groupinfotable = new html_table();
    $groupinfotable->attributes['class'] = 'groupinfobox';
    $groupinfotable->data[] = new html_table_row($groupinforow);
   // echo html_writer::table($groupinfotable);
}

$headerHtml = getModuleHeaderHtml($group, $CFG->teamModule);
echo $headerHtml;

$isLDapOn = 0;				  
if($CFG->isLdap == 1){

  $groupSource = $group->auth;
  if($groupSource == $CFG->authLdap){
    $isLDapOn = 1;
  }	
}


/// Print the editing form
?>

<div id="addmembersform" class="borderBlock borderBlockSpace">
    
    <!--form id="assignform" method="post" action="<?php //echo $CFG->wwwroot; ?>/group/assignusers.php?group=<?php //echo $groupid; ?>"-->
    <form id="assignform" method="post" action="<?php echo $posturl; ?>">
    <div>
    <input type="hidden" name="sesskey" value="<?php p(sesskey()); ?>" />

    <table class="assignTable">
        <tr>
          <td id='potentialcell' class="potentialcell">
          <p>
            <label for="addselect"><?php echo get_string('coursenonuser')." (".get_string('username').")"; ?></label>
          </p>
          <?php $potentialmembersselector->display(); ?>
      </td>
      <td id='buttonscell'>
        <div class="arrow_button">
            <input class="moveLeftButton" name="remove" id="remove" type="submit" value="<?php echo get_string('remove').'&nbsp;'.$OUTPUT->rarrow(); ?>" title="<?php print_string('remove'); ?>" /><input class="moveRightButton" name="add" id="add" type="submit" value="<?php echo $OUTPUT->larrow().'&nbsp;'.get_string('add'); ?>" title="<?php print_string('add'); ?>" />
        </div>
      </td>
	   <td id='existingcell'  class="potentialcell">
          <p>
            <label for="removeselect"><?php print_string('courseuser'); ?></label>
          </p>
          <?php $groupmembersselector->display(); ?>
        </td>
      <!--<td>
        <p><?php echo($strusergroupmembership) ?></p>
        <div id="group-usersummary"></div>
      </td>-->
    </tr>
    
    <?php if($isLDapOn == 1){?>
  
    <tr><td colspan="3" style="padding-top:10px" >
			<span style="color:#FF0000">*</span> <?php print_string('cannotenrollforldap','group'); ?>
		</td></tr>
    <?php } ?>    
     <tr><td colspan="3" id='backcell'>
        <input type="submit" name="cancel" value="<?php print_string('back'); ?>" />
    </td></tr>
   
    </table>
    </div>
    </form>
    
 
 <script>
 
  
  $(document).ready(function(){
  
    <?php if($isLDapOn == 1){?>
	 $('#addselect, #removeselect, .arrow_button #add, .arrow_button #remove').prop('disabled','disabled');
	<?php } ?>
  
  });
 
 </script>   
    
</div>

<?php


    //outputs the JS array used to display the other groups users are in
    $potentialmembersselector->print_user_summaries($course->id);

    //this must be after calling display() on the selectors so their setup JS executes first
    $PAGE->requires->js_init_call('init_add_remove_members_page', null, false, $potentialmembersselector->get_js_module());

    echo $OUTPUT->footer();
