<?php 

	defined('MOODLE_INTERNAL') || die();
	
	// added by rajesh 
	// we have included reportlib.php for custom global report functions 
	require_once($CFG->dirroot."/local/lib/reportlib.php");
	
	
	/**
	 * this function is using for print an array in preformatted form 
	 * @param array $arr
	 * @print an array in preformatted form 
	 */
	 
	 
	function pr($arr){
	
	  echo "<pre>";print_r($arr);echo "</pre>";
	
	}
	
	
	
	/**
	 * This function is using for execute a query and return an error if exist
	 * @param string $query
	 * @return an error in json format if exist 
	*/
	 
	 
	function executeSql($query){
	
	  global $CFG;
	  $errorArr = array();
	  $error = '';
	  if($query){
		$link =  mysql_connect($CFG->dbhost, $CFG->dbuser, $CFG->dbpass);
		mysql_select_db($CFG->dbname, $link);
		mysql_query($query) or $errorArr = array('error'=>"Error::".mysql_error()."<br>".$query);
	  }
	  
	  if(count($errorArr) > 0){
		//$error = json_encode($error);
		$error = json_encode($errorArr);
	  }
	  
	  return $error;
	  
	}
	
	
	/**
	 *  This function is using for getting date format
	 *  @param int $timeStamp the timestamp in UTC.
	 *  @param string $format
	 *  @return string the formatted date/time.
	*/
	
	function getDateFormat($timeStamp, $format='"Y-m-d H:i:s"'){
	
	  $dateFormat = $timeStamp;
	  if($timeStamp){
		$dateFormat = date($format, $timeStamp);
	  }
	  
	  return $dateFormat;
	}
	
	
	/**
	 *  This function is using for total time spend.
	 *  @param int $timeSpent the timestamp in UTC.
	 *  @return total spend time.
	*/
	
	function timePassed($timeSpent=0){
		
		//intervals in seconds
		$intervals      = array (
			'year' => 31556926, 'month' => 2629744, 'week' => 604800, 'day' => 86400, 'hour' => 3600, 'minute'=> 60
		);
		
		//now we just find the difference
		if ($timeSpent == 0){
			return 'just now';
		}elseif ($timeSpent < 60){
			return ($timeSpent == 1 ? $timeSpent . ' Sec ' : $timeSpent . ' Secs ').get_string('ago','learnercourse');
		}elseif ($timeSpent >= 60 && $timeSpent < $intervals['hour']){
			$timeSpent = floor($timeSpent/$intervals['minute']);
			return ($timeSpent == 1 ? $timeSpent . ' Min ' : $timeSpent . ' Mins ').get_string('ago','learnercourse');
		}elseif ($timeSpent >= $intervals['hour'] && $timeSpent < $intervals['day']){
			$timeSpent = floor($timeSpent/$intervals['hour']);
			return ($timeSpent == 1 ? $timeSpent . ' Hour ' : $timeSpent . ' Hours ').get_string('ago','learnercourse');
		}elseif ($timeSpent >= $intervals['day'] && $timeSpent < $intervals['week']){
			$timeSpent = floor($timeSpent/$intervals['day']);
			return ($timeSpent == 1 ? $timeSpent . ' Day ' : $timeSpent . ' Days ').get_string('ago','learnercourse');
		}elseif ($timeSpent >= $intervals['week'] && $timeSpent < $intervals['month']){
			$timeSpent = floor($timeSpent/$intervals['week']);
			return ($timeSpent == 1 ? $timeSpent . ' Week ' : $timeSpent . ' Weeks ').get_string('ago','learnercourse');
		}elseif ($timeSpent >= $intervals['month'] && $timeSpent < $intervals['year']){
			$timeSpent = floor($timeSpent/$intervals['month']);
			return ($timeSpent == 1 ? $timeSpent . ' Month ' : $timeSpent . ' Months ').get_string('ago','learnercourse');
		}elseif ($timeSpent >= $intervals['year']){
			$timeSpent = floor($timeSpent/$intervals['year']);
			return ($timeSpent == 1 ? $timeSpent . ' Year ' : $timeSpent . ' Years ').get_string('ago','learnercourse');
		}
	}
	
	
	/**
	 *  This function is using for converting a H:i:s format time in second and then return total time spend
	 *  @param string $timeSpent in H:i:s format.
	 *  @return total spend time.
	*/
	
	
	function convertCourseSpentTime($timeSpent){
	
		$time = $timeSpent;
		$timeInSec = 0;
		
		if($timeSpent && $timeSpent!=get_string('NA','learnercourse')){
		  $timeSpentArr = explode(":", $timeSpent);
		  if(count($timeSpentArr) > 0 ){
			   $timeInSec += ($timeSpentArr[0]*60*60);
			   $timeInSec += ($timeSpentArr[1]*60);
			   $timeInSec += ($timeSpentArr[2]);
			   $timeInSec = round($timeInSec);
			   $time = timePassed($timeInSec);
		  }
		}
		
		return $time;
				
	}
	
	/**
	 *  This function is being used in /lib/classes/component.php in fetch_subsystems() function to combine custom created language files.
	 *  @return array of custom created language file name with their path 
	*/
	
	
	function libClassesComponentFetchSubsystems() {
			global $CFG;
			// NOTE: Any additions here must be verified to not collide with existing add-on modules and subplugins!!!
			$info = array(
				
				'menubar'  => $CFG->dirroot.'/menubar',
				'easybackup'  => $CFG->dirroot.'/easybackup',
				'login'  => $CFG->dirroot.'/login',
				'course'  => $CFG->dirroot.'/course',
				'page'  => $CFG->dirroot.'/page',
				'event'  => $CFG->dirroot.'/event',
				'learnercourse' => $CFG->dirroot.'/learnercourse',
				'user' => $CFG->dirroot.'/user',
				'cms' => $CFG->dirroot.'/cms',
				'messages' => $CFG->dirroot.'/messages',
				'department' => $CFG->dirroot.'/department',
				'paging' => $CFG->dirroot.'/paging',
				'admindashboard' => $CFG->dirroot.'/admindashboard',
				'singlereport' => $CFG->dirroot.'/singlereport',
				'singlecoursereport' => $CFG->dirroot.'/singlecoursereport',
				'multicoursereport' => $CFG->dirroot.'/multicoursereport',
				'multiuserreport' => $CFG->dirroot.'/multiuserreport',				
				'adminreportdashboard' => $CFG->dirroot.'/adminreportdashboard',
				'program' => $CFG->dirroot.'/program',
			
			);
	
			return $info;
	}
	
	
	/**
	 * This function is using for sorting a multiple array by their column
	 * @param $arr array you just want to sort
	 * @param $col array key by which you want to sort
	 * @param $dir direction is an order of array
	 * @return sorting array
	*/
	 
	
	function arraySortByColumn(&$arr, $col, $dir = SORT_ASC) {
		$sortCol = array();
		
		if(count($arr) > 0){
		
			foreach ($arr as $key=> $row) {
				$sortCol[$key] = $row[$col];
			}

			array_multisort($sortCol, $dir, $arr);
		
		}
	}
	
	/**
	 *  This function checks that the current user is logged in, and optionally
	 *  whether they are allowed to access a particular activity 
		@param string $activity is activity for current user
	 *  @return If they are not logged in, then it redirects them to the site login page and if login
	 *   & not able to allow a particular activity then they are redirected to dashboard page of the current user.  
	*/
	
	function checkLogin($activity = ''){
	 global $USER, $CFG;
	 
	 if(isset($USER) && $USER->id!=''){
	 
		switch($activity){
		  
		  case 'addeditgroup':
			  
			   //$userrole =  getUserRole($USER->id);
			   if( $USER->archetype != $CFG->userTypeAdmin ) {
			   //if(!in_array($userrole, $CFG->custommanagerroleid)){ 
					$url = new moodle_url($CFG->wwwroot);
					redirect($url);
			   }
		   
		  case 'managegroups':
		  
			   //$userrole =  getUserRole($USER->id);
			   if( $USER->archetype != $CFG->userTypeAdmin ) {
			   //if(!in_array($userrole, $CFG->custommanagerroleid)){ 
				  $url = new moodle_url($CFG->wwwroot);
				  redirect($url);
			   }
		  case 'assignusertogroup':
		  
			   //$userrole =  getUserRole($USER->id);
			   if( $USER->archetype != $CFG->userTypeAdmin ) {
			   //if(!in_array($userrole, $CFG->custommanagerroleid)){ 
				  $url = new moodle_url($CFG->wwwroot);
				  redirect($url);
		  }
		  default : 
		   
		}
		
	 }else{
	  $url = new moodle_url("$CFG->wwwroot/login/index.php");
	  redirect($url);
	 }
	 
	}
	
	/**
	 *  This function is using for getting all system user's role
	 *  @return object All system available user's role
	*/
	
	function getSystemAvailableRole(){
	
		global $DB, $CFG;
		$query = "select * from {$CFG->prefix}role";
		$records = $DB->get_records_sql($query);
		return $records;
	
	}

	/**
	 *  This function is using for getting Login user role 
	 *  @return int $field return user role
	*/
	
	
	function getUserRole(){
		
		global $DB, $CFG, $USER;

		$archetype = $USER->archetype;	
	    $query = "SELECT id FROM {$CFG->prefix}role WHERE name = '".$archetype."'"; 		
		$userRole = $DB->get_field_sql($query);		
		return $userRole;
		
	}
	
	/**
	 *  This function is using for getting breadcrumb html 
	 *  @param array $bcarr is an array with name value pair where
	 *  name is page link label and value is page link, if page link is blank then link label will not be clickable 
	 *  @return breadcrumb html 
	*/
	
	
	
	function getBreadCrumb($bcarr){
	
		global $CFG;
		$breadCrumb = '';
	
		if(count($bcarr) >= 0){
		
		 $breadCrumb .= '<div class="clearfix" id="page-navbar">
								 <nav class="breadcrumb-button"></nav>
								 <div class="breadcrumb-nav" > 
									<span class="accesshide">Page path</span>
									  <ul class="breadcrumb" >
											<li><a href="'.$CFG->wwwroot.'/my/" >'.get_string('dashboard','menubar').'</a> 
												<span class="divider"> 
													<span class="accesshide ">
													  <span class="arrow_text">/</span>&nbsp;
													</span>
													<span class="arrow sep">&gt;</span>
											   </span>
										   </li>';
										   
										   foreach($bcarr as $label=>$url){
											   if($url!=''){
												 $breadCrumb .= '<li><a href="'.$url.'">'.$label.'</a></li>';
											   }else{
												 $breadCrumb .= '<li>
																	<span tabindex="0">'.$label.'</span>
																	<span class="divider">
																	  <span class="accesshide ">
																		<span class="arrow_text">/</span>&nbsp;
																	  </span>
																	  <span class="arrow sep">&gt;</span>
																	</span>
																 </li>';
											   }
											   
										  }   
										 
										 
										
									$breadCrumb .= '</ul> 
							 </div>
					   </div>';
				  
		}else{
			$breadCrumb .= '<div class="clearfix" id="page-navbar">
								 <nav class="breadcrumb-button"></nav>
								 <div class="breadcrumb-nav" > 
									<span class="accesshide">Page path</span>
									  <ul class="breadcrumb" >
											<li><a href="'.$CFG->wwwroot.'/my/" >'.get_string('dashboard','menubar').'</a> 
												<span class="divider"> 
													<span class="accesshide ">
													  <span class="arrow_text">/</span>&nbsp;
													</span>
													<span class="arrow sep">&gt;</span>
											   </span>
										   </li>';
			$breadCrumb .= '</ul> 
							 </div>
					   </div>';
		}
	
		return $breadCrumb;
	}
	
	

	
	/**
	 *  Getting course format for singleactivity only
	 *  @param int $courseId is course id
	 *  @param string $format is format of course
	 *  @param string $name is activity type of course
	 *  @return string course format of scorm package 
	*/
	
	
	
	function getCourseFormatOptions($courseId, $format = 'singleactivity', $name = 'activitytype'){ 
	 global $DB, $CFG;
	 $courseformat = 0;
	 if($courseId && $format && $name){
	 
	   $query = "select value from {$CFG->prefix}course_format_options where courseid='$courseId' AND format='$format' AND name='$name' order by id asc limit 0,1";
	   $courseformat = $DB->get_field_sql($query);
	 }
	 return $courseformat;
	}
	
	/**
	 *  Getting course format
	 *  @param int $courseId is course id
	 *  @return string course format for all course type
	*/
	
	
	function getCourseFormat($courseId){ 
	 global $DB, $CFG;
	 $courseformat = 0;
	 if($courseId){
	 
	   $query = "select format from {$CFG->prefix}course where id='$courseId'";
	   $courseformat = $DB->get_field_sql($query);
	 }
	 return $courseformat;
	}
	
	
	/**
	 *  Get module id 
	 *  @param int $courseId is course id
	 *  @return int module id 
	*/
	function getModuleId($courseId){ 
	 global $DB, $CFG;
	 $moduleId = $DB->get_field_sql("select module from {$CFG->prefix}course_modules where course='$courseId' order by id asc limit 0,1");
	 return $moduleId;
	}
	
	
	/**
	 *  get Scorm Or Resource Id
	 *  @param int $courseId is course id
	 *  @param string $module is module name
	 *  @return int Scorm Or Resource Id
	*/
	function getScormOrResourceId($courseId, $module){
	 global $DB, $CFG;
	 $resourceOrScormid = 0;
	 if($courseId && $module){
	   $resourceOrScormid = $DB->get_field_sql("select id from {$CFG->prefix}course_modules where course='$courseId' and module ='$module' order by id asc limit 0,1");
	 }
	 return $resourceOrScormid;
	}
	
	/**
	 *  get Resource Id (Non Course Matterial ID)
	 *  @param int $courseId is course id
	 *  @return int Resource Id
	*/
	
	/*function getResourceId($courseId){ 
	 global $DB, $CFG;
	 $resourceId = $DB->get_field_sql("select id from {$CFG->prefix}course_modules where course={$courseId} and module ='17' order by id asc limit 0,1");
	 return $resourceId;
	}*/
	
	
	/**
	 *  To check, if scorm exist for a course
	 *  @param int $courseId is course id
	 *  @return int scormid, if exist or return 0
	*/
	
	
	function isScormExistForCourse($courseId){
	 global $DB, $CFG;
	  
	  $scormId = 0;
	  if($courseId){
		$scormId = $DB->get_field_sql("select id from {$CFG->prefix}scorm where course='".$courseId."'");
	  }
	  return $scormId;
	}
	
	
	/**
	 *  To check, if resource exist for a course
	 *  @param int $courseId is course id
	 *  @return int $resourceId, if exist or return 0
	*/
	
	function isResourceExistForCourse($courseId){ // non course matterial
	 global $DB, $CFG;
	  
	  $resourceId = 0;
	  if($courseId){
		$resourceId = $DB->get_field_sql("select id from {$CFG->prefix}resource where course='".$courseId."'");
	  }
	  return $resourceId;
	}
	
	 
	// Start groups functions
	
	/**
	 *  To check, if group is exist for the same name
	 *  @param string $data is data of the group
	 *  @return bool true, if exist or return false
	*/
	
	function isGroupAlreadyExist($data) {
	
		global $CFG, $DB, $USER;
	
		$deptStr = 0;
		if(is_array($data['department']) && count($data['department']) > 0 ){
		   $deptStr = implode(",", $data['department']);
		}else{
		   if(is_numeric($data['department'])){
			$deptStr = $data['department'];
		   }	
		}
		
		$sql = "SELECT team_id from {$CFG->prefix}group_department WHERE department_id IN (".$deptStr.") AND is_active = '1'";
		$teamDepartment = $DB->get_records_sql($sql);

		if(!empty($teamDepartment)){
		  $allTeams = array_keys($teamDepartment);
		}
		
		$allTeamsSTR = 0;		
		if(count($allTeams) > 0 ){
		  $allTeamsSTR = implode(",", $allTeams);
		}
		
		$query = "Select id from {$CFG->prefix}groups WHERE lower(name) = '".strtolower(trim($data['name']))."' and id IN (".$allTeamsSTR.") ";
		$records = $DB->get_records_sql($query);
		
		if(count($records) > 0 ){
		  return true;
		}else{
		  return false;
		}		
	
	}
	
	
	/**
	 *  To get the  groups of deaprtment
	 *  @param mixed $department is department id
	 * @param bool $isReport if true then records will not check deleted, is_active condition
	 *  @return $data is dataset of team
	*/
	
	function getDepartmentGroups($department, $isReport = false) {
	
		global $CFG, $DB, $USER;
	
		$deptStr = 0;
		if(is_array($department) && count($department) > 0 ){
		   $deptStr = implode(",", $department);
		}else{
		   if(is_numeric($department)){
			$deptStr = $department;
		   }	
		}
		
		$sql = "SELECT team_id from {$CFG->prefix}group_department WHERE department_id IN (".$deptStr.") ";
		if(!$isReport){
		  $sql .=  " AND is_active = '1' ";
		}
		$teamDepartment = $DB->get_records_sql($sql);

		if(!empty($teamDepartment)){
		  $allTeams = array_keys($teamDepartment);
		}
		
		$allTeamsSTR = 0;		
		if(count($allTeams) > 0 ){
		  $allTeamsSTR = implode(",", $allTeams);
		}
		
		$query = "SELECT * FROM {$CFG->prefix}groups WHERE id IN (".$allTeamsSTR.") ORDER BY name ASC";
		$records = $DB->get_records_sql($query);
		
		if(count($records) > 0 ){
		  return $records;
		}else{
		  return array();
		}		
	
	}
	
	/**
	 *  To get the departments of group 
	 *  @param mixed $group is group id
	 *  @return $data is dataset of deaprtment
	*/
	
	function getGroupDepartments($group) {
	
		global $CFG, $DB, $USER;
	
		$gpStr = 0;
		if(is_array($group) && count($group) > 0 ){
		   $gpStr = implode(",", $group);
		}else{
		   if(is_numeric($group)){
			$gpStr = $group;
		   }	
		}
		
		$sql = "SELECT department_id from {$CFG->prefix}group_department WHERE team_id IN (".$gpStr.") AND is_active = '1'";
		$teamDepartment = $DB->get_records_sql($sql);

		if(!empty($teamDepartment)){
		  $allDepts = array_keys($teamDepartment);
		}
		
		$allDeptsSTR = 0;		
		if(count($allDepts) > 0 ){
		  $allDeptsSTR = implode(",", $allDepts);
		}
		
		$query = "Select id, title from {$CFG->prefix}department WHERE id IN (".$allDeptsSTR.") ";
		$records = $DB->get_records_sql($query);
		
		if(count($records) > 0 ){
		  return $records;
		}else{
		  return array();
		}		
	
	}
	
	/**
	 *  Get group listing
	 *  @return object groups
	*/
	
	function getGroupListing() {
	
		global $DB;
		$records = $DB->get_records('groups', array(), 'name ASC');
	
		return $records;
	}
	
	/**
	 * Delete a group best effort, first removing members and links with courses and groupings.
	 * Removes group avatar too.
	 *
	 * @param mixed $grouporid The id of group to delete or full group object
	 * @return bool True if deletion was successful, false otherwise
	 */
	
	// from group/lib.php
	function customGroupsDeleteGroup($grouporid) {
		global $CFG, $DB;
		require_once("$CFG->libdir/gdlib.php");
	
		if (is_object($grouporid)) {
			$groupId = $grouporid->id;
			$group   = $grouporid;
		} else {
			$groupId = $grouporid;
			if (!$group = $DB->get_record('groups', array('id'=>$groupId))) {
				//silently ignore attempts to delete missing already deleted groups ;-)
				return true;
			}
		}
	
		// delete group calendar events
		$DB->delete_records('event', array('groupid'=>$groupId));
		//first delete usage in groupings_groups
		$DB->delete_records('groupings_groups', array('groupid'=>$groupId));
		//delete members
		$DB->delete_records('groups_members', array('groupid'=>$groupId));
		//group itself last
		$DB->delete_records('groups', array('id'=>$groupId));
	
		// Delete all files associated with this group
		
		if($group->courseid){
			$context = context_course::instance($group->courseid);
			$fs = get_file_storage();
			$fs->delete_area_files($context->id, 'group', 'description', $groupId);
			$fs->delete_area_files($context->id, 'group', 'icon', $groupId);
		
			// Invalidate the grouping cache for the course
			cache_helper::invalidate_by_definition('core', 'groupdata', array(), array($group->courseid));
		
			// Trigger group event.
			$params = array(
				'context' => $context,
				'objectid' => $groupId
			);
			$event = \core\event\group_deleted::create($params);
			$event->add_record_snapshot('groups', $group);
			$event->trigger();
		}		
	
		return true;
	}
	
	
	/**
	 * Update group
	 *
	 * @param stdClass $data group properties (with magic quotes)
	 * @param stdClass $editform
	 * @param array $editoroptions
	 * @return bool true or exception
	 */
	 
	 // from group/lib.php
	function customGroupsUpdateGroup($data, $editform = false, $editoroptions = false) {
		global $USER, $CFG, $DB;
	
		if($data->courseid){
		   $context = context_course::instance($data->courseid);
		}
		
		$data->timemodified = time();
		$data->name         = trim($data->name);
		if(isset($data->picture)){
			$data->picture      = $data->picture;
		}
		if (isset($data->idnumber)) {
			$data->idnumber = trim($data->idnumber);
			if($data->courseid){
				$existing = groups_get_group_by_idnumber($data->courseid, $data->idnumber);
				if ($existing && $existing->id != $data->id) {
					throw new moodle_exception('idnumbertaken');
				}
			}
		}
	
		if($data->courseid){
			if ($editform and $editoroptions) {
				die;
				$data = file_postupdate_standard_editor($data, 'description', $editoroptions, $context, 'group', 'description', $data->id);
			}
		}
		$data->description = $data->description_editor['text'];
		$data->updatedby = $USER->id;
		if(isset($data->changeownership) && $data->changeownership != '' && $data->changeownership != 0){
			$data->createdby = $data->changeownership;
		}
		$DB->update_record('groups', $data);
	
		if($data->courseid){
			// Invalidate the group data.
			cache_helper::invalidate_by_definition('core', 'groupdata', array(), array($data->courseid));
		}
		
			
		$group = $DB->get_record('groups', array('id'=>$data->id));
		
		if($group->id){
		
		  $teamDepartment = $DB->get_records('group_department', array('team_id'=>$group->id, 'is_active'=>1));
		  $teamDepartmentArr = array();
		  if(count($teamDepartment) > 0 ){
			  foreach($teamDepartment as $arr ){
			   $teamDepartmentArr[] = $arr->department_id;
			  }
		  }
		 
		  
		  if($USER->archetype == $CFG->userTypeManager){
		      
		        $userDepartment = $DB->get_records_sql("SELECT departmentid FROM {$CFG->prefix}department_members WHERE userid = ".$USER->id);
				$userDepartments = array();
				if($userDepartment){
					foreach($userDepartment as $department){
						$userDepartments[] = $department->departmentid;
					}
					//$mform->getElement('department')->setSelected($userDepartments);
				}
				
				$data->department = $userDepartments;
				
				
		  }

		  if(count($teamDepartmentArr) == 0){
			  if(is_array($data->department) && count($data->department) > 0 ){
				$dataTnD = new stdClass();
				foreach($data->department as $deptId){
					$dataTnD->team_id = $group->id;
					$dataTnD->department_id = $deptId;
					$dataTnD->is_active = 1;
					$DB->insert_record('group_department', $dataTnD);
				}
			  }else{
				$dataTnD = new stdClass();
				$dataTnD->team_id = $group->id;
				$dataTnD->department_id = $data->department;
				$dataTnD->is_active = 1;
				$DB->insert_record('group_department', $dataTnD);
			  }
		  }
	    }
	
		if ($editform) {
			//custom_groups_update_group_icon($group, $data, $editform);
		}
	
		if($data->courseid){
			// Trigger group event.
			$params = array(
				'context' => $context,
				'objectid' => $group->id
			);
			$event = \core\event\group_updated::create($params);
			$event->add_record_snapshot('groups', $group);
			$event->trigger();
		}
		 
		return true;
	}
	
	/**
	 * Add a new group
	 *
	 * @param stdClass $data group properties
	 * @param stdClass $editform
	 * @param array $editoroptions editor content
	 * @return id of group or false if error
	 */
	 
	  // from group/lib.php
	function customGroupsCreateGroup($data, $editform = false, $editoroptions = false) {
		global $CFG, $DB,$USER;
	
		$data->timecreated  = time();
		$data->timemodified = $data->timecreated;
		$data->name         = trim($data->name);
		if(isset($data->picture)){
			$data->picture      = $data->picture;
		}
		
		if ($editform and $editoroptions) {
			$data->description = $data->description_editor['text'];
			$data->descriptionformat = $data->description_editor['format'];
		}
	
		$data->id = $DB->insert_record('groups', $data);
		
		
		
		if ($editform and $editoroptions) {
			// Update description from editor with fixed files
			$data = file_postupdate_standard_editor($data, 'description', $editoroptions, $context, 'group', 'description', $data->id);
			$upd = new stdClass();
			$upd->id                = $data->id;
			$upd->description       = $data->description;
			$upd->descriptionformat = $data->descriptionformat;
			$upd->createdby = $USER->id;
			$upd->updatedby = $USER->id;
			$DB->update_record('groups', $upd);
		}
	
		$group = $DB->get_record('groups', array('id'=>$data->id));
		
		if($group->id){
		  if(is_array($data->department) && count($data->department) > 0 ){
		    $dataTnD = new stdClass();
		    foreach($data->department as $deptId){
				$dataTnD->team_id = $group->id;
				$dataTnD->department_id = $deptId;
				$dataTnD->is_active = 1;
				$DB->insert_record('group_department', $dataTnD);
			}
		  }else{
			$dataTnD = new stdClass();
			$dataTnD->team_id = $group->id;
			$dataTnD->department_id = $data->department;
			$dataTnD->is_active = 1;
			$DB->insert_record('group_department', $dataTnD);
     	  }
	    }
	
		return $group->id;
	}
	
	/**
	 * Gets array of all groups 
	 *
	 * @category group
	 * @param mixed $userid optional user id or array of ids, returns only groups of the user.
	 * @param int $groupingid optional returns only groups in the specified grouping.
	 * @param string $fields
	 * @return array Returns an array of the group objects (userid field returned if array in $userid)
	 */
	
	function customGroupsGetAllGroups($userId=0, $groupingid=0, $fields='g.*') {
		global $DB;
	
		// We need to check that we each field in the fields list belongs to the group table and that it has not being
		// aliased. If its something else we need to avoid the cache and run the query as who knows whats going on.
		$knownfields = true;
		if ($fields !== 'g.*') {
			// Quickly check if the first field is no longer g.id as using the
			// cache will return an array indexed differently than when expect
			if (strpos($fields, 'g.*') !== 0 && strpos($fields, 'g.id') !== 0) {
				$knownfields = false;
			} else {
				$fieldbits = explode(',', $fields);
				foreach ($fieldbits as $bit) {
					$bit = trim($bit);
					if (strpos($bit, 'g.') !== 0 or stripos($bit, ' AS ') !== false) {
						$knownfields = false;
						break;
					}
				}
			}
		}
	
		if (empty($userId) && $knownfields) {
		   
			$groups = getGroupListing();
			return $groups;
		}
	
	
		if (empty($userId)) {
			$userfrom  = "";
			$userwhere = "";
			$params = array();
	
		} else {
			list($usql, $params) = $DB->get_in_or_equal($userId);
			$userfrom  = ", {groups_members} gm";
			$userwhere = "AND g.id = gm.groupid AND gm.userid $usql";
		}
	
		if (!empty($groupingid)) {
			$groupingfrom  = ", {groupings_groups} gg";
			$groupingwhere = "AND g.id = gg.groupid AND gg.groupingid = ?";
			$params[] = $groupingid;
		} else {
			$groupingfrom  = "";
			$groupingwhere = "";
		}
	
		return $DB->get_records_sql("SELECT $fields FROM {groups} g $userfrom $groupingfrom  WHERE 1 = 1 $userwhere $groupingwhere ORDER BY name ASC");
	}
	
	
	/**
	 * Lists users in a group based on their role on the course.
	 * Returns false if there's an error or there are no users in the group.
	 * Otherwise returns an array of role ID => role data, where role data includes:
	 * (role) $id, $shortname, $name
	 * $users: array of objects for each user which include the specified fields
	 * Users who do not have a role are stored in the returned array with key '-'
	 * and pseudo-role details (including a name, 'No role'). Users with multiple
	 * roles, same deal with key '*' and name 'Multiple roles'. You can find out
	 * which roles each has by looking in the $roles array of the user object.
	 *
	 * @param int $groupId
	 * @param int $courseid Course ID (should match the group's course)
	 * @param string $fields List of fields from user table prefixed with u, default 'u.*'
	 * @param string $sort SQL ORDER BY clause, default (when null passed) is what comes from users_order_by_sql.
	 * @param string $extrawheretest extra SQL conditions ANDed with the existing where clause.
	 * @param array $whereorsortparams any parameters required by $extrawheretest (named parameters).
	 * @return array Complex array as described above
	 */
	 
	function customGroupsGetMembersByRole($groupId=0, $courseId = 0, $fields='u.*',
		$sort=null, $extrawheretest='', $whereorsortparams=array()) {
		global $DB, $USER, $CFG;
	
		$relatedctxsql = '';
		$relatedctxparams = '';
		$searchcondition = '';
		$context = context_system::instance();
		
		if($courseId){
		
			// Retrieve information about all users and their roles on the course or
			// parent ('related') contexts
			//$context = context_course::instance($courseId);
		
			// We are looking for all users with this role assigned in this context or higher.
			//list($relatedctxsql, $relatedctxparams) = $DB->get_in_or_equal($context->get_parent_context_ids(true), SQL_PARAMS_NAMED, 'relatedctx');
	
		}
		
		
		if ($extrawheretest) {
			$extrawheretest = ' AND ' . $extrawheretest;
		}
	
		if (is_null($sort)) {
			list($sort, $sortparams) = users_order_by_sql('u');
			$whereorsortparams = array_merge($whereorsortparams, $sortparams);
		}
		
		$extrasql = '';
		if($relatedctxsql){
		 // $extrasql = " AND ra.contextid $relatedctxsql  ";
		}
		$roleChk = "and ra.roleid IN( ".$CFG->studentTypeId.",".$CFG->managerTypeId.")";
		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$searchcondition .= '';
				break;
			CASE $CFG->userTypeManager:
					//$searchcondition .=	' AND u.id IN(Select u.id FROM mdl_user as u WHERE u.parent_id = '.$USER->id.' OR u.createdby = '.$USER->id.') AND r.name = "learner"';
					$roleChk = "and ra.roleid = ".$CFG->studentTypeId;

					if($groupId){
						  $groupDepts = getGroupDepartments($groupId);
						  $groupDeptsStr = 0;
						  if(count($groupDepts) > 0){
								$groupDeptsIds = array_keys($groupDepts); 
								$groupDeptsStr = implode(",", $groupDeptsIds);
								$searchcondition .=	" AND u.id IN(SELECT DISTINCT(uu.id)
																			FROM mdl_user AS uu
																			LEFT JOIN mdl_department_members AS dm ON dm.userid = uu.id
																			WHERE dm.is_active = '1' AND dm.departmentid IN (".$groupDeptsStr.")) ";
						  }
					  
					}
						
				break;
		}
		$sql = "SELECT DISTINCT(u.id) AS userid,r.id AS roleid, $fields
				  FROM {groups_members} gm
				  JOIN {user} u ON u.id = gm.userid
			 LEFT JOIN {role_assignments} ra ON (ra.userid = u.id $extrasql)
			 LEFT JOIN {role} r ON r.id = ra.roleid";
			 
		 $sql .= "  WHERE 1 = 1 AND gm.is_active = 1 ".$roleChk.$searchcondition;
		 if($groupId){
		  $sql .= " AND gm.groupid=:mgroupid";
		 }
			 
		 $sql .= $extrawheretest." ORDER BY r.sortorder, $sort";
		//if($relatedctxparams && $groupId){
		 // $whereorsortparams = array_merge($whereorsortparams, $relatedctxparams, array('mgroupid' => $groupId));
		//}else{
		  if($groupId){
			$whereorsortparams = array_merge($whereorsortparams, array('mgroupid' => $groupId));
		  }
		//}
		$rs = $DB->get_recordset_sql($sql, $whereorsortparams);
		return customGroupsCalculateRolePeople($rs, $context);
	}
	
	/**
	 * Checks whether the current user is permitted (using the normal UI) to
	 * remove a specific group member, assuming that they have access to remove
	 * group members in general.
	 *
	 * For automatically-created group member entries, this checks with the
	 * relevant plugin to see whether it is permitted. The default, if the plugin
	 * doesn't provide a function, is true.
	 *
	 * For other entries (and any which have already been deleted/don't exist) it
	 * just returns true.
	 *
	 * @param mixed $grouporid The group id or group object
	 * @param mixed $userorid The user id or user object
	 * @return bool True if permitted, false otherwise
	 */
	 
	function CustomGroupsRemoveMemberAllowed($grouporid, $userorid) {
		global $DB;
	
		if (is_object($userorid)) {
			$userId = $userorid->id;
		} else {
			$userId = $userorid;
		}
		if (is_object($grouporid)) {
			$groupId = $grouporid->id;
		} else {
			$groupId = $grouporid;
		}
		$groupCourses = $DB->get_records('groups_course',array('groupid'=>$groupId));
		foreach($groupCourses as $course){
			$removeId = deactivateUserMappingData(0,$course->courseid,$userId,'group',$groupId);
		}
		// Get entry
		if (!($entry = $DB->get_record('groups_members',
				array('groupid' => $groupId, 'userid' => $userId), '*', IGNORE_MISSING))) {
			// If the entry does not exist, they are allowed to remove it (this
			// is consistent with groups_remove_member below).
			return true;
		}
	
		// If the entry does not have a component value, they can remove it
		if (empty($entry->component)) {
			return true;
		}
		// It has a component value, so we need to call a plugin function (if it
		// exists); the default is to allow removal
		return component_callback($entry->component, 'allow_group_member_remove',
				array($entry->itemid, $entry->groupid, $entry->userid), true);
	}
	
	/**
	 * Deletes the link between the specified user and group.
	 *
	 * @param mixed $grouporid  The group id or group object
	 * @param mixed $userorid   The user id or user object
	 * @return bool True if deletion was successful, false otherwise
	 */
	function customGroupsRemoveMember($grouporid, $userorid) {
		global $DB;
	
		if (is_object($userorid)) {
			$userId = $userorid->id;
		} else {
			$userId = $userorid;
		}
	
		if (is_object($grouporid)) {
			$groupId = $grouporid->id;
			$group   = $grouporid;
		} else {
			$groupId = $grouporid;
			$group = $DB->get_record('groups', array('id'=>$groupId), '*', MUST_EXIST);
		}

		$groupCourses = $DB->get_records('groups_course',array('groupid'=>$groupId));
		foreach($groupCourses as $course){
			$removeId = deactivateUserMappingData(0,$course->courseid,$userId,'group',$groupId);
		}

		if (!groups_is_member($groupId, $userId)) {
			return true;
		}
		$groupMember = $DB->get_record('groups_members',array('groupid'=>$groupId, 'userid'=>$userId));
		if($groupMember){
			$mappingData = new stdClass;
			$mappingData->is_active = 0;
			$mappingData->id = $groupMember->id;
			$last_id = $DB->update_record('groups_members',$mappingData);
		}
		//$DB->delete_records('groups_members', array('groupid'=>$groupId, 'userid'=>$userId));
		// Update group info.
		$time = time();
		$DB->set_field('groups', 'timemodified', $time, array('id' => $groupId));
		$group->timemodified = $time;
	
	   if($group->courseid){
			// Trigger group event.
			$params = array(
				'context' => context_course::instance($group->courseid),
				'objectid' => $groupId,
				'relateduserid' => $userId
			);
			$event = \core\event\group_member_removed::create($params);
			$event->add_record_snapshot('groups', $group);
			$event->trigger();
		}
		return true;
	}
	
	/**
	 * Internal function used by groups_get_members_by_role to handle the
	 * results of a database query that includes a list of users and possible
	 * roles on a course.
	 *
	 * @param moodle_recordset $rs The record set (may be false)
	 * @param int $context ID of course context
	 * @return array As described in groups_get_members_by_role
	 */
	 
	function customGroupsCalculateRolePeople($rs, $context) {
		global $CFG, $DB;
	
		if (!$rs) {
			return array();
		}
	
		$allroles = array(); 
		if($context){
		  $allroles = role_fix_names(get_all_roles($context), $context);
		}
		// Array of all involved roles
		$roles = array();
		// Array of all retrieved users
		$users = array();
		// Fill arrays
		foreach ($rs as $rec) {
			// Create information about user if this is a new one
			if (!array_key_exists($rec->userid, $users)) {
				// User data includes all the optional fields, but not any of the
				// stuff we added to get the role details
				$userdata = clone($rec);
				unset($userdata->roleid);
				unset($userdata->roleshortname);
				unset($userdata->rolename);
				unset($userdata->userid);
				$userdata->id = $rec->userid;
	
				// Make an array to hold the list of roles for this user
				$userdata->roles = array();
				$users[$rec->userid] = $userdata;
			}
			// If user has a role...
			if (!is_null($rec->roleid)) { 
				// Create information about role if this is a new one
				if (!array_key_exists($rec->roleid, $roles)) { 
					$role = count($allroles) > 0?$allroles[$rec->roleid]:0;
					$roledata = new stdClass();
					$roledata->id        = $role->id;
					$roledata->shortname = $role->shortname;
					$roledata->name      = $role->localname;
					$roledata->users = array();
					$roles[$roledata->id] = $roledata;
				}
				// Record that user has role
				$users[$rec->userid]->roles = $roles[$rec->roleid];
			}
		}
		$rs->close();
	
		// Return false if there weren't any users
		/*
		if (count($users) == 0) {
			return false;
		}
	
		// Add pseudo-role for multiple roles
		$roledata = new stdClass();
		$roledata->name = get_string('multipleroles','role');
		$roledata->users = array();
		$roles['*'] = $roledata;
	
		$roledata = new stdClass();
		$roledata->name = get_string('noroles','role');
		$roledata->users = array();
		$roles[0] = $roledata;
	
		// Now we rearrange the data to store users by role
		foreach ($users as $userId=>$userdata) {
			$rolecount = count($userdata->roles);
			if ($rolecount == 0) {
				// does not have any roles
				$roleid = 0;
			} else if($rolecount > 1) {
				$roleid = '*';
			} else {
				$userrole = reset($userdata->roles);
				$roleid = 5;
			}
			
			$roles[$roleid]->users[$userId] = $userdata;
		}
	
		// Delete roles not used
		foreach ($roles as $key=>$roledata) {
			if (count($roledata->users)===0) {
				unset($roles[$key]);
			}
		}*/
		$roles = array();
		foreach ($users as $userId=>$userdata) {
			if(array_key_exists($userdata->roles->name,$roles)){
				array_push($roles[$userdata->roles->name],$userdata);
			}else{
				$roles[$userdata->roles->name] = array();
				array_push($roles[$userdata->roles->name],$userdata);
			}
		}
		// Return list of roles containing their users
		return $roles;
	}
	  
	  /**
	 * A centralised location for the all name fields. Returns an array / sql string snippet.
	 *
	 * @param bool $returnsql True for an sql select field snippet.
	 * @param string $tableprefix table query prefix to use in front of each field.
	 * @param string $prefix prefix added to the name fields e.g. authorfirstname.
	 * @param string $fieldprefix sql field prefix e.g. id AS userid.
	 * @return array|string All name fields.
	 */
	  
	 function customGetAllUserNameFields($returnsql = false, $tableprefix = null, $prefix = null, $fieldprefix = null) {
		$alternatenames = array('firstnamephonetic' => 'firstnamephonetic',
								'lastnamephonetic' => 'lastnamephonetic',
								'middlename' => 'middlename',
								'alternatename' => 'alternatename',
								'firstname' => 'firstname',
								'lastname' => 'lastname');
	
		// Let's add a prefix to the array of user name fields if provided.
		if ($prefix) {
			foreach ($alternatenames as $key => $altname) {
				$alternatenames[$key] = $prefix . $altname;
			}
		}
	
		// Create an sql field snippet if requested.
		if ($returnsql) {
			if ($tableprefix) {
				if ($fieldprefix) {
					foreach ($alternatenames as $key => $altname) {
						$alternatenames[$key] = $tableprefix . '.' . $altname . ' AS ' . $fieldprefix . $altname;
					}
				} else {
					foreach ($alternatenames as $key => $altname) {
						$alternatenames[$key] = $tableprefix . '.' . $altname;
					}
				}
			}
			$alternatenames = implode(',', $alternatenames);
		}
		return $alternatenames;
	} 
	
	
	
	/**
	 * Return filtered (if provided) list of groups in site
	 *
	 * @param string $sort An SQL field to sort by
	 * @param int $courseId is course id
	 * @param string $dir The sort direction ASC|DESC
	 * @param int $page The page or records to return
	 * @param int $perpage The number of records to return per page
	 * @param string $search A simple string to search for
	 * @param string $extraSelect An additional SQL select statement to append to the query
	 * @param array $extraParams Additional parameters to use for the above $extraSelect
	 * @return object Array of group records
	 */
	 
	function getGroupsListing($sort='id', $courseId = 0, $dir='ASC', $page=0, $perpage=0,
							   $search='', $extraSelect='', array $paramArray=null) {
		global $DB, $CFG,$USER;
		$searchString = "AND ";
		if($paramArray['ch'] != '') {
			if($paramArray['ch'] == 'OTH')
				$searchString .= "(mg.name REGEXP '^[^a-zA-Z]') AND";
			else
				$searchString .= "(mg.name like '".$paramArray['ch']."%') AND";
		}
		$searchKeyAll = true;
		if( ($paramArray['name']==1) || ($paramArray['des']==1) )
			$searchKeyAll = false;

		$paramArray['key'] = addslashes($paramArray['key']);
		if($searchKeyAll && ($paramArray['key'] != '')) {
			$searchString .= " (mg.name like '%".$paramArray['key']."%' OR strip_tags(mg.description) like '%".$paramArray['key']."%' OR md.title like '%".$paramArray['key']."%' ) AND";
		} else if ($paramArray['key'] != '') {
			$searchKeyAllString .= "(";
			if( $paramArray['name']==1 )
				$searchKeyAllString .= " mg.name like '%".$paramArray['key']."%' OR";
			//if( $paramArray['dept']==1 )
				//$searchKeyAllString .= " md.title like '%".$paramArray['key']."%' OR";
			if( $paramArray['des']==1 )
				$searchKeyAllString .= " strip_tags(mg.description) like '%".$paramArray['key']."%' OR";

			$searchKeyAllString = substr($searchKeyAllString, 0, -2);
			$searchKeyAllString .= ") AND";
			$searchString .= $searchKeyAllString;
		}
		if($USER->archetype != $CFG->userTypeAdmin ) { 
		
		    $managerDepts = getDepartments();
			
			$managerDeptsStr = 0;
			if(count($managerDepts) > 0 ){
			  $managerDeptsIds = array_keys($managerDepts);
			  $managerDeptsStr = implode(",", $managerDeptsIds);
			}
			//$searchString .= " mg.createdby = ".$USER->id.' AND';
			$searchString .= " md.id IN (".$managerDeptsStr.") AND";
		}
		$searchString .= " 1=1";
		if($sort == 'groupcount'){
			$sort = '';
			$limit = '';
			//$query = "SELECT count(DISTINCT(mg.id)) as groupcount FROM {$CFG->prefix}groups mg WHERE 1 = 1 $searchString $sort $limit";
			$query = "SELECT count(DISTINCT(mg.id)) as groupcount FROM {$CFG->prefix}groups mg LEFT JOIN {$CFG->prefix}group_department mgd ON (mg.id = mgd.team_id) LEFT JOIN {$CFG->prefix}department md ON (md.id = mgd.department_id) WHERE 1 = 1 $searchString  ";
			$groupCount = $DB->get_record_sql($query);
			return $groupCount->groupcount;
		}else{
			if ($sort) {
				$sort = $sort;
				$sort = " ORDER BY mg.$sort $dir";
			}
			$page = $page - 1;
			$page = $page*$perpage;
			$limit = '';
			if($perpage != 0){
				$limit = "LIMIT $page,$perpage";
			}
			
			//$query = "SELECT * FROM {$CFG->prefix}groups WHERE 1 = 1 $searchString $sort $limit";
			$query = "SELECT mg.*, group_concat(md.title) as dept_title FROM {$CFG->prefix}groups mg LEFT JOIN {$CFG->prefix}group_department mgd ON (mg.id = mgd.team_id) LEFT JOIN {$CFG->prefix}department md ON (md.id = mgd.department_id) WHERE 1 = 1 $searchString  group by mg.id $sort $limit";
			
			return $DB->get_records_sql($query);
		}
	
	}
	
	
	/**
	 * Returns a subset of groups
	 *
	 * @global object
	 * @uses DEBUG_DEVELOPER
	 * @uses SQL_PARAMS_NAMED
	 * @param bool $get If false then only a count of the records is returned
	 * @param int $courseId is course id
	 * @param string $search A simple string to search for
	 * @param array $exceptions A list of IDs to ignore, eg 2,4,5,8,9,10
	 * @param string $sort A SQL snippet for the sorting criteria to use
	 * @param string $page The page or records to return
	 * @param string $recordsPerPage The number of records to return per page
	 * @param string $fields A comma separated list of fields to be returned from the chosen table.
	 * @param string $extraSelect An additional SQL select statement to append to the query
	 * @param array $extraParams Additional parameters to use for the above $extraSelect
	 * @return array|int|bool  Groups records unless get is false in which case the integer count of the records found is returned.
	 *                        False is returned if an error is encountered.
	 * copy of get_users function from lib/datalib.php
	 */
	 
	function getGroups($get=true, $courseId = 0, $search='', array $exceptions=null, $sort='name ASC', $page='', $recordsPerPage='', $fields='*', $extraSelect='', array $extraParams=null) {
		global $DB, $CFG;
	
		if ($get && !$recordsPerPage) {
			debugging('Call to getGroups with $get = true no $recordsPerPage limit. ' .
					'On large installations, this will probably cause an out of memory error. ' .
					'Please think again and change your code so that it does not try to ' .
					'load so much data into memory.', DEBUG_DEVELOPER);
		}
	
	
		$select = " 1 = 1 ";
		$params = array();
	
		if (!empty($search)){
			$search = trim($search);
			$select .= " AND (idnumber = :search1 OR name = :search2 OR description = :search3)";
			$params['search1'] = "%$search%";
			$params['search2'] = "%$search%";
			$params['search3'] = "$search";
		}
	
	
		if ($exceptions) {
			list($exceptions, $eparams) = $DB->get_in_or_equal($exceptions, SQL_PARAMS_NAMED, 'ex', false);
			$params = $params + $eparams;
			$select .= " AND id $exceptions";
		}
	
	   
		if ($extraSelect) {
			$select .= " AND $extraSelect";
			$params = $params + (array)$extraParams;
		}
		
		if($courseId){
			//$select .= " AND courseid = :courseid";
			//$params['courseid'] = $courseId;
		}
	
		if ($get) {
			return $DB->get_records_select('groups', $select, $params, $sort, $fields, $page, $recordsPerPage);
		} else {
			return $DB->count_records_select('groups', $select, $params);
		}
	}
	
	/**
	 * Returns the display name of a field in the group table. Works for most fields that are commonly displayed to groups.
	 * @param string $field Field name, e.g. 'gname'
	 * @return string Text description taken from language file, e.g. 'Name'
	 */
	 
	function getGroupFieldName($field) {
		// Some fields have language strings which are not the same as field name.
		switch ($field) {
		
			case 'idnumber' : {
				return get_string('gidnumber','group');
			}
			case 'name' : {
				return get_string('gname','group');
			}
			case 'description' : {
				return get_string('gdescription','group');
			}
			case 'department' : {
				return get_string('department','group');
			}
		   
			case 'timecreated' : {
				return get_string('gtimecreated','group');
			}
			case 'timemodified' : {
				 return get_string('gtimemodified','group');
			}
			
		}
		// Otherwise just use the same lang string.
		return get_string($field);
	}
	
	
	
	/**
	 * get assigend course id for user
	 *
	 * @param int $userId is userid 
	 * @param bool $isReport, if true then it will return also unassigned or deleted course id for this user
	 * @return array of assigend course ids
	 */
	 
	 
	function getAssignedCourseIdForUser($userId, $isReport = false){
		
		global $DB, $CFG;
		$courseIds = array();
	
		if($userId){
	
	
	       $query = "SELECT DISTINCT ucm.courseid FROM {$CFG->prefix}user_course_mapping as ucm   ";
		   $query .= " WHERE ucm.userid = '".$userId."' AND ucm.program_user_ref_id = 0  ";
			   
	       if($isReport){
		       //$query = "SELECT me.courseid FROM {$CFG->prefix}user_enrolments as mue LEFT JOIN {$CFG->prefix}enrol as me ON (me.id = mue.enrolid)  ";
			   //$query .= " WHERE me.enrol = 'manual' AND mue.userid = '".$userId."'";

		   }else{
		       $query .= " AND ucm.status = '1' ";
		   }
		   
		   $records = $DB->get_records_sql($query);
		   $courseIds = array_keys($records);	
		   
		   
		   // Get Courses by Assigned Program

			$progArrRec = getAssignedProgramIdForUser($userId);
			$allPC = array();
			$allGetCourses = array_keys($allGetCourses['courses']);
			//pr($allGetCourses);die;
			if(count($progArrRec) > 0 ){
			  foreach($progArrRec as $pid ){
			     $programCourses = getProgramCourses($pid);
				 if(count($programCourses) > 0 ){
				   foreach($programCourses as $cid => $pcArr){
				     if(!in_array($cid, $courseIds)){
					   array_push($courseIds, $cid);
					 }  
				   }
				 }
			   }
		  }
			  
		}
		
		
		
		return $courseIds;
	}
	
	/**
	 * get assigend program id for user
	 *
	 * @param int $userId is userid 
	 * @param bool $isReport, if true then it will return also unassigned or deleted program id for this user
	 * @return array of assigend program ids
	 */
	 
	 
	function getAssignedProgramIdForUser($userId, $isReport = false){
		
		global $DB, $CFG;
		$programIds = array();
	
		if($userId){
	
	
	       $query = "SELECT DISTINCT pum.program_id FROM {$CFG->prefix}program_user_mapping as pum   ";
		   $query .= " WHERE pum.user_id = '".$userId."'  ";
			   
	       if($isReport){
		       //$query = "SELECT me.programid FROM {$CFG->prefix}user_enrolments as mue LEFT JOIN {$CFG->prefix}enrol as me ON (me.id = mue.enrolid)  ";
			   //$query .= " WHERE me.enrol = 'manual' AND mue.userid = '".$userId."'";

		   }else{
		       $query .= " AND pum.status = '1' ";
		   }
			   $records = $DB->get_records_sql($query);
			   $programIds = array_keys($records);	
			  
		}
		
		
		
		return $programIds;
	}
	
	
	/**
	 * get scorm status of course for user
	 *
	 * @param int $scormId is id of scorm
	 * @param int $userId is userid 
	 * @param string $want is the type of scorm value like status
	 * @return string status, timespent, score of scorm
	 */
	 
	 
	function getScormStatusForUser($scormId, $userId, $want='status'){
		
		global $DB, $CFG;
		$scormStatus = '---';
	
		if($scormId && $userId && $want){
	
			if($want == 'status'){
			  $element = 'cmi.core.lesson_status';
			}elseif($want == 'lastaccessed'){
			  $element = 'x.start.time';
			}elseif($want == 'timespent'){
			  $element = 'cmi.core.total_time';
			}elseif($want == 'score'){
			  $element = 'cmi.core.score.raw';
			}
			
			$query = "SELECT MAX(attempt) FROM {$CFG->prefix}scorm_scoes_track as msst WHERE msst.scormid = '".$scormId."' AND msst.userid = '".$userId."'";
			$maxAttempt = $DB->get_field_sql($query);
			
			if($maxAttempt){
			  $query = "SELECT msst.value FROM {$CFG->prefix}scorm_scoes_track as msst WHERE msst.scormid = '".$scormId."' AND msst.userid = '".$userId."' AND element = '".$element."' AND attempt = '".$maxAttempt."' ORDER BY msst.id DESC LIMIT 1";
			  $scormStatus = $DB->get_field_sql($query);
			  if($want == 'score'){
				 if($scormStatus){
					$scormStatus = $scormStatus." ".get_string('points','learnercourse');
				 }else{
					$scormStatus = get_string('NA','learnercourse');
				 }
			  }elseif($want == 'status'){
				 if($scormStatus == ''){
				   $scormStatus = get_string('notstarted','learnercourse');
				 }elseif($scormStatus == 'incomplete'){
				   $scormStatus = get_string('inprogress','learnercourse');
				 }elseif($scormStatus == 'passed'){
				   $scormStatus = get_string('completed','learnercourse');  
				 }elseif($scormStatus == 'failed'){
				   $scormStatus = get_string('completed','learnercourse');  
				 }
				 
				 
			  }elseif($want == 'timespent'){
			  
				 if($scormStatus){
					$scormStatus = $scormStatus;
				 }else{
					$scormStatus = get_string('NA','learnercourse');
				 }  
			  }elseif($want == 'lastaccessed'){
			  
				 if($scormStatus){
					$scormStatus = $scormStatus;
				 }else{
					$scormStatus = get_string('NA','learnercourse');
				 }  
			  }
			}
		   
			  
		}
	
		return $scormStatus;
	}
	
	
	/**
	 * get learner courses category
	 *
	 * @return object of learner courses category
	 */
	 
	  
	function getLearnerCoursesCategory(){
	  global $DB, $CFG;
	  
	  return $DB->get_records_sql("SELECT * FROM {$CFG->prefix}course_categories ORDER BY name ASC");
	}
	
	
	/**
	 * get course module id of scorm
	 *
	 * @param int $scormId is id of scorm
	 * @param int $courseId is course id 
	 * @param int $module is module id
	 * @return int course module id
	 */
	 
	 
	function getScormCourseModuleId($scormId, $courseId, $module = 18){
	
		global $DB, $CFG;
		$id = 0;
		if($scormId && $courseId){
		   $id = $DB->get_field_sql("select id from {$CFG->prefix}course_modules where instance = '".$scormId."' AND module = '".$module."' AND course = '".$courseId."'  ");
		}
		
		return $id;
	}
	
	/**
	 * get course module name
	 *
	 * @param int $moduleId is module id
	 * @return string course module name
	 */
	 
	
	function getModuleName($moduleId){
	
		global $DB, $CFG;
		$moduleName = '';
		if($moduleId){
		   $moduleName = $DB->get_field_sql("select name from {$CFG->prefix}modules where id = '".$moduleId."'");
		}
		
		return $moduleName;
	}
	
	
	
	
	
	/**
	 * Returns the display name of a field in the course table. Works for most fields that are commonly displayed to courses.
	 * @param string $field Field name, e.g. 'category'
	 * @return string Text description taken from language file, e.g. 'Category'
	 */
	 
	 
	function getLearnerCourseFieldName($field) { 
		// Some fields have language strings which are not the same as field name.
		switch ($field) {
		
			case 'idnumber' : {
				return get_string('idnumber','learnercourse');
			}
			case 'category' : {
				return get_string('category','learnercourse');
			}
			case 'fullname' : {
				return get_string('fullname','learnercourse');
			}
			case 'scormname' : {
				return get_string('title','learnercourse');
			}
			case 'resoursename' : {
				return get_string('title','learnercourse');
			}
			case 'lastaccessed' : {
				return get_string('lastaccessed','learnercourse');
			}
			case 'timespent' : {
				return get_string('timespent','learnercourse');
			}
			case 'score' : {
				return get_string('score','learnercourse');
			}
			case 'scormsummary' : {
				return get_string('summary','learnercourse');
			}
			case 'summary' : {
				return get_string('summary','learnercourse');
			}
			case 'timecreated' : {
				return get_string('timecreated','learnercourse');
			}
			case 'ctimecreated' : {
				return get_string('datesent','learnercourse');
			}
			case 'timemodified' : {
				 return get_string('timemodified','learnercourse');
			}
			case 'rstatus' : {
				 return get_string('currentstatus','learnercourse');
			}
			
		}
		// Otherwise just use the same lang string.
		return get_string($field);
	}
	
	
	/**
	 * Returns the display name of a field in the course report table. Works for most fields that are commonly displayed to course report.
	 * @param string $field Field name, e.g. 'title'
	 * @return string Text description taken from language file, e.g. 'Category'
	 */
	 
	function getMultiCourseReportFieldName($field) { 
		// Some fields have language strings which are not the same as field name.
		switch ($field) {
		
			case 'fullname' : {
				return get_string('coursetitle','multicoursereport');
			}
			case 'totalassignedusers' : {
				return get_string('totalassignedusers','multicoursereport');
			}
			case 'notstarted' : {
				return get_string('fullname','multicoursereport');
			}
			case 'inprogress' : {
				return get_string('title','multicoursereport');
			}
			case 'completed' : {
				return get_string('title','multicoursereport');
			}
			
		}
		// Otherwise just use the same lang string.
		return get_string($field);
	}
	
	
	
	/**
	 * Adds a specified user to a group
	 *
	 * @param mixed $grouporid  The group id or group object
	 * @param mixed $userorid   The user id or user object
	 * @param string $component Optional component name e.g. 'enrol_imsenterprise'
	 * @param int $itemid Optional itemid associated with component
	 * @return bool True if user added successfully or the user is already a
	 * member of the group, false otherwise.
	 */
	
	
	function customGroupsAddMember($grouporid,  $userorid, $component=null, $itemid=0) {
		global $DB;
		if (is_object($userorid)) {
			$userId = $userorid->id;
			$user   = $userorid;
			if (!isset($user->deleted)) {
				$user = $DB->get_record('user', array('id'=>$userId), '*', MUST_EXIST);
			}
		} else {
			$userId = $userorid;
			$user = $DB->get_record('user', array('id'=>$userId), '*', MUST_EXIST);
		}
	
		if ($user->deleted) {
			return false;
		}
	
		if (is_object($grouporid)) {
			$groupId = $grouporid->id;
			$group   = $grouporid;
		} else {
			$groupId = $grouporid;
			$group = $DB->get_record('groups', array('id'=>$groupId), '*', MUST_EXIST);
		}
	
		/*if($group->courseid){
			// Check if the user a participant of the group course.
			$context = context_course::instance($group->courseid);
			if (!is_enrolled($context, $userId)) {
				return false;
			}
		}*/
		
		if (groups_is_member($groupId, $userId)) {
			$groupUserData = $DB->get_record('groups_members', array('groupid'=>$groupId, 'userid'=>$userId));
			if($groupUserData->is_active == 0){
				$mappingData = new stdClass;
				$mappingData->is_active = 1;
				$mappingData->id = $groupUserData->id;
				$last_id = $DB->update_record('groups_members',$mappingData);
			}
			assignGroupCourses($groupId, $userId);
			return true;
		}
		$member = new stdClass();
		$member->groupid   = $groupId;
		$member->userid    = $userId;
		$member->timeadded = time();
		$member->component = '';
		$member->itemid = 0;
	
		// Check the component exists if specified
	   /* if (!empty($component)) {
			$dir = core_component::get_component_directory($component);
			if ($dir && is_dir($dir)) {
				// Component exists and can be used
				$member->component = $component;
				$member->itemid = $itemid;
			} else {
				throw new coding_exception('Invalid call to groups_add_member(). An invalid component was specified');
			}
		}
	
		if ($itemid !== 0 && empty($member->component)) {
			// An itemid can only be specified if a valid component was found
			throw new coding_exception('Invalid call to groups_add_member(). A component must be specified if an itemid is given');
		}*/
	
		$lastID = $DB->insert_record('groups_members', $member);
		$assigned = assignGroupCourses($groupId, $userId);
	
		// Update group info, and group object.
		$DB->set_field('groups', 'timemodified', $member->timeadded, array('id'=>$groupId));
		$group->timemodified = $member->timeadded;
	
		/*if($group->courseid){
			// Trigger group event.
			$params = array(
				'context' => $context,
				'objectid' => $groupId,
				'relateduserid' => $userId,
				'other' => array(
					'component' => $member->component,
					'itemid' => $member->itemid
				)
			);
			$event = \core\event\group_member_added::create($params);
			$event->add_record_snapshot('groups', $group);
			$event->trigger();
		}
		*/
		return true;
	}
	
	
	
	
	
	/**
	 * To enrol users in any specific course
	 *
	 * @param int $enrolId enrol id belongs to a course which one we want to enrol user
	 * @param int $userId id of userid
	 * @return string $error, if exist, else return blank
	 */
	 
	 
	function enrolUserInCourse($enrolId, $userId){
	  
	  $error = '';
	  global $DB, $CFG, $USER;
	  $modifierid = $USER->id;
	  //$userrole =  getUserRole($modifierid);
	  $datetime = time();
	

	 if( $USER->archetype == $CFG->userTypeAdmin ) {
	 // if(in_array($userrole, $CFG->custommanagerroleid)){ 
	  
		  if($enrolId && $userId){
		  
				$roleid = 5;
				$duration = 0;
				$startdate = 3;
				$recovergrades = 0;
				$status = 0;
		
				if (empty($roleid)) {
					$roleid = null;
				}
			
				switch($startdate) {
					case 2:
						$timestart = $course->startdate;
						break;
					case 3:
					default:
						$today = time();
						$today = make_timestamp(date('Y', $today), date('m', $today), date('d', $today), 0, 0, 0);
						$timestart = $today;
						break;
				}
				if ($duration <= 0) {
					$timeend = 0;
				} else {
					$timeend = $timestart + ($duration*24*60*60);
				}
				
				$query = "SELECT id FROM {$CFG->prefix}user_enrolments  where enrolid = '".$enrolId."' AND userid = '".$userId."'";
				$user_enrolment_id = $DB->get_field_sql($query);
			
				if(!$user_enrolment_id){
				   $query = "INSERT INTO {$CFG->prefix}user_enrolments set status = '".$status."', enrolid = '".$enrolId."', userid = '".$userId."', timestart = '".$timestart."', timeend = '".$timeend."', modifierid = '".$modifierid."', timecreated = '".$datetime."', timemodified = '".$datetime."'";
				   $error = executeSql($query);
				}
				// $plugin->enrol_user($instance, $userId, $roleid, $timestart, $timeend, null, $recovergrades);
			}else {
				$error = get_sting('donnothavepermissiontoenrol','course');
			}
			
	   }else{
		   $error = get_sting('donnothavepermissiontoenrol','course');
	   }
		
	   return $error;
	}
	
	/**
	 * To unenrol users from any specific course
	 *
	 * @param int $enrolId enrol id belongs to a course which one we want to enrol user
	 * @param int $userId id of userid
	 * @return string $error, if exist, else return blank
	 */
	 
	 
	function unEnrolUserFromCourse($enrolId, $userId){
	  
	  $error = '';
	  global $DB, $CFG, $USER;
	  //$userrole =  getUserRole($USER->id);
	
	  if( $USER->archetype == $CFG->userTypeAdmin ) {
	  //if(in_array($userrole, $CFG->custommanagerroleid)){ 
	  
			if($enrolId && $userId){
		  
				$query = "SELECT id FROM {$CFG->prefix}user_enrolments  where enrolid = '".$enrolId."' AND userid = '".$userId."'";
				$user_enrolment_id = $DB->get_field_sql($query);
				if($user_enrolment_id){
					$error = executeSql("DELETE FROM {$CFG->prefix}user_enrolments WHERE id = '".$user_enrolment_id."'");
				}
				
			}else{
				$error = get_sting('donnothavepermissiontounenrol','course');
			}
			
	   }else{
		   $error = get_sting('donnothavepermissiontounenrol','course');
	   }
		
	   return $error;
	}
	
	/**
	 * To enrol groups in any specific course
	 *
	 * @param int $groupId is a group id
	 * @param int $courseId is a course id
	 * @return object $outcome 
	 */
	 
	 
	function enrolUser($groupId, $courseId){
			
		if($groupId  &&  $courseId){
				
			global $DB, $CFG, $USER;
			
			$outcome = new stdClass();
			$outcome->success = false;
			$outcome->response = new stdClass();
			$outcome->error = '';
			$error = '';
		
			$groups_members  = getGroupMembers($groupId);
			$isCourseGroupExist = isCourseGroupExist($groupId, $courseId);
			$datetime = time();
	
			$enrolId = $DB->get_field_sql("select id from {$CFG->prefix}enrol where enrol = 'manual' and courseid = '".$courseId."' and roleid = '5'");
			$error = '';
			
			 if(count($groups_members) > 0){
		  
				if(!$isCourseGroupExist){
					   $error = executeSql("INSERT INTO {$CFG->prefix}groups_course set courseid = '".$courseId."', groupid = '".$groupId."' , timecreated = '".$datetime."', timemodified = '".$datetime."'");
					  if($error){
					  
						 $outcome->error = $error;
						 $outcome->success = false;
						 
					  }else{
					  
							 $outcome->success = true;
						  
							 foreach($groups_members as $userId => $groups_members_obj){  
								$error = enrolUserInCourse($enrolId, $userId);
								if($error!=''){ 
								  $outcome->error = $error;
								  $outcome->success = false;
								  break;
								}
							 }
								 
							 $outcome->response = $assignCoursesForGroup = getAssignCoursesForGroup($groupId, 2);
							 $outcome->groupid = $groupId;
				  
					   }
				}
				
			}else{
			   $error = get_string('thisgroupdoesnothaveanymemberstoenrol','course');
			   $outcome->error = $error;
			   $outcome->success = false;
			}
			
		 }else{
			   $error = get_string('courseidorgroupidismissing','course');
			   $outcome->error = $error;
			   $outcome->success = false;
		 }
		 
		 return $outcome;	
	}
	
	/**
	 * To unenrol groups from any specific course
	 *
	 * @param int $groupId is a group id
	 * @param int $courseId is a course id
	 * @return object $outcome 
	 */
	 
	 
	function unEnrolUser($groupId, $courseId){
	
		global $DB, $CFG, $USER;
		
		$outcome = new stdClass();
		$outcome->success = false;
		$outcome->response = new stdClass();
		$outcome->error = '';
		$error = '';
			
			$groups_members  = getGroupMembers($groupId);
		
			$isCourseGroupExist = isCourseGroupExist($groupId, $courseId);
		
			if($isCourseGroupExist && $isCourseGroupExist == $courseId){
			
			   $enrolId = $DB->get_field_sql("select id from {$CFG->prefix}enrol where enrol = 'manual' and courseid = '".$courseId."' and roleid = '5'");
				
			   $error = executeSql("DELETE FROM {$CFG->prefix}groups_course where courseid = '".$courseId."' AND groupid = '".$groupId."'");
			   if($error){
			  
				 $outcome->error = $error;
				 $outcome->success = false;
				 
			  }else{
				  
					$outcome->success = true;
					if(count($groups_members) > 0){
					   foreach($groups_members as $userId => $groups_members_obj){
						   $error = unEnrolUserFromCourse($enrolId, $userId);
						   if($error!=''){ 
							  $outcome->error = $error;
							  $outcome->success = false;
							  break;
						   }
					   }
					}
					
					$outcome->response = $assignCoursesForGroup = getAssignCoursesForGroup($groupId, 2);
					$outcome->groupid = $groupId;
			   }
		
			}else{ 	
			  $error = get_sting('donnothavepermissiontounenrol','course');
			  $outcome->success = false;
			  $outcome->error = '';
			}
		
		   
		   return $outcome;
		
	}
	
	// end groups functions
	
	
	/**
	 * To get documents for logined user which they have uploaded in the system
	 *
	 * @return object $records uploaded docuemnts for logined users 
	 */
	 
	 
	function getMyDocuments(){
	
	 global $DB,$CFG,$USER;
	 $userId = $USER->id;
	 $context = context_user::instance($userId);
	 $filearea = 'private';
	 $component = 'user';
	 $contextId = $context->id;
	 
	
	 $query = "SELECT * FROM  {$CFG->prefix}files  WHERE  filearea LIKE  '".$filearea."' AND component = '".$component."' AND  userid = '".$userId."' AND `contextid` = '".$contextId."' AND author!='' AND status = '0' ORDER BY id DESC";
	 $records = $DB->get_records_sql($query);
	 return $records;
	
	}
	
	
	/**
	 * To delete my documents for logined user which they have uploaded in the system
	 *
	 * @return object $outcome 
	 */
	 
	 
	function deleteMyDocuments($id){ // ajax calling
	
	 global $DB,$CFG,$USER;
	 $error = '';
	 $outcome = new stdClass();
	 $outcome->success = false;
	 $outcome->response = new stdClass();
	 if($id){
	 
		 $userId = $USER->id;
		 $result = executeSql("DELETE FROM {$CFG->prefix}files where id='$id' AND userid='$userId' ");
		  if($result == ''){
			$outcome->success = true;
			$outcome->response = $id;
			
		  }
	 
	 }
	 
	 return $outcome;
	
	}
	
	
	/**
	 * To check, if user have accessed a course 
	 * This function we are using only for a non learning course
	 * @param int $userId is a user id
	 * @param int $courseId is a course id
	 * @param int $resourceId is resource id
	 * @param int $tableFields is fields to be extracted from table
	 * @return int $courseAccessed id of the user_noncourse_lastaccess table else return zero
	 */
	 
	 
	function isUserAccessedCourse($userId, $courseId, $resourceId, $tableFields = 'id'){
		global $DB, $CFG;
		$courseAccessed = 0;
		if($userId && $courseId){
			if($tableFields == 'id'){
				 $courseAccessed = $DB->get_field_sql("select $tableFields from {$CFG->prefix}user_noncourse_lastaccess where userid = '".$userId."' AND courseid = '".$courseId."' AND resource_id = '".$resourceId."' limit 1");
			}else{
				$courseAccessed = $DB->get_record_sql("select $tableFields from {$CFG->prefix}user_noncourse_lastaccess where userid = '".$userId."'AND courseid = '".$courseId."'  AND resource_id = '".$resourceId."' limit 1");
			}
		}
		return $courseAccessed;
	}
	
	
	/**
	 * To set a course is accessed
	 * This function we are using only for a non learning course
	 * @param int $userId is a user id
	 * @param int $courseId is a course id
	 * @param int $resourceId is resource id
	 * @return object $outcome 
	 */
	 
	 
	function setCourseLastAccess($userId, $courseId, $resourceId){ // ajax calling
	
		global $DB, $CFG;
		$error = '';
		$outcome = new stdClass();
		$outcome->success = false;
		$outcome->response = new stdClass();
		$timeAccess = time();
	
		if($userId && $courseId &&  $resourceId){
		
			$isUserAccessCourse = isUserAccessedCourse($userId, $courseId, $resourceId);
			if($isUserAccessCourse){
			  $query = "UPDATE {$CFG->prefix}user_noncourse_lastaccess SET timeaccess = '".$timeAccess."' WHERE userid = '".$userId."' AND courseid = '".$courseId."' AND resource_id = '".$resourceId."'";
			 $error = executeSql($query);
			}else{
			  $query = "INSERT INTO {$CFG->prefix}user_noncourse_lastaccess SET userid = '".$userId."', courseid = '".$courseId."', resource_id = '".$resourceId."', timeaccess = '".$timeAccess."'";
			 $error = executeSql($query);
			}	
			
			if($error == ''){
			 $outcome->success = true;
			 $outcome->response = $courseId."_".$resourceId;
			}
			   
		}
		
		
	   return $outcome;
	
	}
	
	/**
	 * To assign a role to user is being created
	 * @param int $userId is a user id
	 * @param int $learnerRole is a role of user
	 * @return int last insert id, if success else zero
	 */
	
	function assignRoleToUser($userId,$learnerRole = 5){
		// by bhavana
		GLOBAL $USER,$CFG,$DB;
		//$userrole =  getUserRole($USER->id);
		$roleinsert = 0;
		
		if( $USER->archetype == $CFG->userTypeAdmin || $USER->archetype == $CFG->userTypeManager ) {
		//if(in_array($userrole, $CFG->custommanagerroleid)){ // check for site admin
			//echo 'in1';die;
			$userRole = $DB->get_record('role_assignments',array('userid'=>$userId));
			if(empty($userRole)){
				$roleData = new stdClass();
				$roleData->roleid = $learnerRole; // default role 5 for learner
				$roleData->contextid = 1;
				$roleData->userid = $userId;
				$roleData->timemodified = time();
				$roleData->modifierid = $USER->id;
				$roleinsert = $DB->insert_record('role_assignments',$roleData);
			}else{
				$roleData = new stdClass();
				$roleData->roleid = $learnerRole;
				$roleData->id = $userRole->id;
				$roleData->timemodified = time();
				$roleinsert = $DB->update_record('role_assignments',$roleData);
			}
		}
		return $roleinsert;
		
	}
	/* 
	$status is the scorm course status
	function to check status string
		'failed', 'incomplete' = Inprogress
		'complete','passed' = Complete
	*/
	function getcourseStatusString($status){
		if(in_array(strtolower($status),array('failed','incomplete'))){
			$status =  ucwords(get_string('inprogress','learnercourse'));
		}
		if(in_array(strtolower($status),array('complete','passed'))){
			$status =  ucwords(get_string('completed','learnercourse'));
		}
		
		return $status;
	}
	/*
		delete course Image
		$courseId = course id
	*/
	function deleteCourseImage($courseId){
		/*global $DB,$USER,$CFG;
		if($courseId != 0 && $courseId != ''){
			$path1 = $CFG->dirroot.'/theme/gourmet/pix/course/'; 
			$path = $CFG->dirroot.'/theme/gourmet/pix/course/'.$courseId.'/'; 
			//$img2 = $DB->get_field_sql("select course_image from mdl_course where id = ".$courseId);
			$imgDataExists = $DB->get_record_sql("select * from mdl_course_image where course_id = ".$courseId);
			$path_old = $path.$imgDataExists->course_image;
			if(file_exists($path_old)){
				@unlink($path_old);
			}
			$imageData = new stdClass;
			$imageData->course_image = '';
			$imageData->id = $imgDataExists->id;
			$DB->update_record('course_image',$imageData);
			return true;
		}
		return false;*/
	}
	
	/*
	This function will set the user archetype according to role in user global variable
	*/
	function loadCurrentUserDetails(){
	
		global $USER,$CFG,$DB;
		
		$userid = $USER->id;
		$query = "SELECT MRA.* FROM  {$CFG->prefix}role_assignments as MRA left join {$CFG->prefix}user as MU on MU.id=MRA.userid  where MRA.userid=".$userid." and MU.deleted='0'";
		
		$details = $DB->get_record_sql($query);
		if(!empty($details)){
			$role = $DB->get_record('role', array('id'=>$details->roleid));		
			/* Set User Role*/
			if($role->archetype!=''){
				if(!isset($USER->archetype)){
					$USER->archetype=$role->name;		
				}
			}
		}
		$USER->pageHeader = generatePageHeader();
	}
	
	function checkCurrentActionsHeader(){ 
		global $CFG,$DB,$USER;
		$jsonArray = array();
		if(!empty($USER->id)){
			$time = strtotime(date('Y-m-d h:i:s',time()));
			$eventStartTime = $time - 20*60;
			$eventEndTime = $time;
			$role = $DB->get_record('role', array('name'=>$USER->archetype));
			if(!empty($role)){
				$roleIdCheck = "AND FIND_IN_SET(".$role->id.",show_to)";
			}else{
				$roleIdCheck = '';
			}
			/**
			* fetching groups for messages filter
			**/
			$groupSql = "SELECT groupid from {$CFG->prefix}groups_members where userid='".$USER->id."'";
			$groupRow = $DB->get_records_sql($groupSql);
			$groupId = array();
			$teamId = "";
			if(count($groupRow)>0){
				foreach($groupRow as $groupValue){		
					$groupId[] = $groupValue->groupid; 		
				}
				$teamId =  implode(",",$groupId);
			}
			
			//displaying message count
			//$messageQuery = "SELECT * from {$CFG->prefix}my_messages where deleted = 0 AND  status = 1 and ( FIND_IN_SET('".$USER->department."',department) || FIND_IN_SET('".$USER->id."',show_to) ||  FIND_IN_SET('".$teamId."',team)) ORDER BY timecreated DESC";
			$messageQuery = "SELECT *
								FROM mdl_my_messages AS m
								WHERE m.deleted = 0 AND m.`status` = 1 AND m.id NOT IN(
								SELECT ts.type_id
								FROM mdl_type_status AS ts
								WHERE ts.`type` = 0 AND ts.user_id = $USER->id) and ( FIND_IN_SET('".$USER->department."',department) || FIND_IN_SET('".$USER->id."',show_to) ||  FIND_IN_SET('".$teamId."',team))
								ORDER BY m.timecreated DESC;
								";
			//echo $messageQuery;die;
			$messageDetails = $DB->get_records_sql($messageQuery);
			$jsonArray['messages_data']['count'] = sizeof($messageDetails);	
			$jsonArray['messages_data']['details'] = $messageDetails;
			
	
			//Event Data
			if( $USER->archetype == $CFG->userTypeAdmin ) {
				$eventQuery = "SELECT * FROM {$CFG->prefix}event where (eventtype = 'user' OR eventtype = 'course' OR eventtype = 'global') AND (DATEDIFF(now(), FROM_UNIXTIME(timestart)) = 0)";
			} else if( $USER->archetype == $CFG->userTypeManager ) {
				//// Getting All the Assigned and Created Course by the Manager ////
				$courseDetails = genCourseListingResult(array(), false);
				$courseIds = '';
				if(count($courseDetails) > 0) {
					foreach($courseDetails as $key => $course) {
						$courseIds .= $course->id.',';
					}
					$courseIds = substr($courseIds, 0, -1);

					$whereclause .= ' OR (groupid = 0 AND courseid IN ('.$courseIds.'))';
				}
				$eventQuery = "SELECT * FROM {$CFG->prefix}event where (eventtype = 'user' OR eventtype = 'global'".$whereclause.") AND (DATEDIFF(now(), FROM_UNIXTIME(timestart)) = 0)";
			} else {
				$courseIdArray = getAssignedCourseIdForUser($USER->id);
				$courseId = implode(',', $courseIdArray);
			
				$eventQuery = "SELECT * FROM {$CFG->prefix}event where (userid = ".$USER->id.( ($courseId !='') ? " OR courseid IN (".$courseId.")" : "")." OR eventtype = 'global') AND (DATEDIFF(now(), FROM_UNIXTIME(timestart)) = 0)";
			}
			$eventDetails = $DB->get_records_sql($eventQuery);
			$jsonArray['events_data']['count'] = sizeof($eventDetails);	
			$jsonArray['events_data']['details'] = $eventDetails;

			//Course Data
			if($USER->archetype == $CFG->userTypeStudent){
				//$nonLearningNotStartedCourses = array();
				//$scormCourses = getAssignedCourseForUser(18,'lastaccessed', $USER->id,'DESC',1,0,'','',null,3);
				$notStartedCount = get_learning_list('count',3,0,0,'course');

				/*$nonCourseMaterials = getAssignedCourseForUser(17,'lastaccessed', $USER->id,'DESC',0,0,'','',null);
				if(!empty($nonCourseMaterials)){
					foreach($nonCourseMaterials as $key=>$resourse){  
						$filedownloaded = isUserAccessedCourse($USER->id,$resourse->id ,$resourse->resourceid,'id,timeaccess');
						if(!empty($filedownloaded)){
							$resourse->lastaccessed = $filedownloaded->timeaccess;
							$nonLearningNotStartedCourses[$resourse->id] = $resourse;
						}
					}
				}*/
				//$totalNotStartedArray = $scormCourses + $nonLearningNotStartedCourses;
				//$totalNotStartedArray = $scormCourses;
				$jsonArray['course_data']['count'] = $notStartedCount;	
				$jsonArray['course_data']['details'] = array();
			}
		} 
		return $jsonArray;
	}
	
	
	
	/**
	 * This function is using for getting cms pages 
	 * @global object
	 * $id int is a page id
	 * @return cms pages 
	 */
	 
	function getCMSPages($id=''){
	 
		global $CFG, $DB;
		$pages = array();
		$query = "SELECT * FROM {$CFG->prefix}cms where status = '1' AND deleted = '0'";
		if($id){
		  $query .= " AND id = '$id'";
		}
		$query .= "  ORDER BY display_order ASC";
		
		if($id){
		  $pages = $DB->get_record_sql($query);
		}else{
		  $pages = $DB->get_records_sql($query);
		}
		return $pages;
	 
	}
	
	/**
	 * This function is using for getting cms pages 
	 * @global object
	 * $id int is a page id
	 * @return cms pages 
	 */
	 
	function getFooterCMSPages($id=''){
	 
		global $CFG, $DB;
		$pages = array();
		$query = "SELECT * FROM {$CFG->prefix}cms where status = '1' AND show_footer=1 AND deleted = '0'";
		if($id){
		  $query .= " AND id = '$id'";
		}
		$query .= "  ORDER BY display_order ASC";
		
		if($id){
		  $pages = $DB->get_record_sql($query);
		}else{
		  $pages = $DB->get_records_sql($query);
		}
		return $pages;
	 
	}
	
	
	/**
	 * This function is using for overriding a moodle default paging
	 * @global object
	 * @param int $totalcount The total number of entries available to be paged through
     * @param int $page The page you are currently viewing
     * @param int $perpage The number of entries that should be shown per page
     * @param string|moodle_url $baseurl url of the current page, the $pagevar parameter is added
     * @param string $pagevar name of page parameter that holds the page number
	 * @return string the HTML to output.
	 */
	 
	function overridePaging($totalcount, $page, $perpage, $baseurl, $pagevar='page'){
	
	    global $CFG, $USER, $CUSTOM_PAGING; 
		//$userrole =  getUserRole($USER->id);
        
		$pagingHTML = '';
		if( $USER->archetype == $CFG->userTypeStudent || $USER->archetype == $CFG->userTypeAdmin ) {
		//if(in_array($userrole, array(1,5))){ 
		    require_once($CFG->dirroot.'/local/lib/class/paging/paging.php');
		    $pagingHTML = $CUSTOM_PAGING->paging_bar($totalcount, $page, $perpage, $baseurl, $pagevar);
	        return $pagingHTML;
		}
		
	}

	function getMyMessages($messageId = 0,$limi = '',$sortField = 'id',$sortOrder = 'DESC'){
		global $USER,$CFG,$DB;
		if($messageId != 0){
			$select = "SELECT * FROM {$CFG->prefix}my_messages WHERE id = $messageId";
			$messageDetails = $DB->get_record_sql($select);
			return $messageDetails;
		}else{
			$role = $DB->get_record('role', array('archetype'=>$USER->archetype));
			if(!empty($role)){
				$roleIdCheck = "AND FIND_IN_SET(".$role->id.",show_to)";
			}else{
				$roleIdCheck = '';
			}
			if($sortField ==''){
				$sortField = 'id';
			}
			if($sortOrder ==''){
				$sortOrder = 'DESC';
			}
			if($sortField == 'read_time'){
				$sortField = 'ts.read_time, m.timecreated';
			}
			//$messageQuery = "SELECT * FROM {$CFG->prefix}my_messages where deleted = 0 AND  status = 1 AND message_from != ".$USER->id." ".$roleIdCheck." ORDER BY ".$sortField." ". $sortOrder;
			$messageQuery = "SELECT m.*,u.firstname,u.lastname,u.id as userId,u.picture FROM mdl_my_messages as m LEFT JOIN mdl_type_status as ts ON ts.`type_id` = m.id AND `type` = 0 LEFT JOIN mdl_user as u on u.id = m.message_from where m.deleted = 0 AND m.status = 1 AND m.message_from != ".$USER->id." ".$roleIdCheck." ORDER BY ".$sortField." ". $sortOrder; // ts.read_time, m.timecreated
			//echo $messageQuery;die;
			if($limi != ''){
				$messageQuery .= $limi;
			}
			$messageDetails = $DB->get_records_sql($messageQuery);
			return $messageDetails;
		}
	}





	/***** Open Section - Added By Madhab *****/
	
	/**
	 * This function is using to generate the Parameterized URL based on given 
	 * @global object
	 * @param Array $paramArray => List all the available parameters in the page
	 * @param Array $removeKeyArray => List all the parameter which need to be skipped during URL generation
	 * @param String $pageURL => Hold Page URL
	 * @return String the HTML to output.
	 */
	 
	function genParameterizedURL($paramArray=array(), $removeKeyArray=array(), $pageURL){
		global $CFG;

		////// Generate common URL for the Search //////
		$genURL = $CFG->wwwroot. $pageURL;

		////// Append pre exist URL parameters without the character parameter into the URL //////
		$countParam = 0;
		foreach($paramArray as $key => $val) {			
			if(!in_array($key, $removeKeyArray)) { 
				if($val != '') {
					if($countParam == 0)
						$genURL .= '?'.$key.'='.$val;
					else
						$genURL .= '&'.$key.'='.$val;

					$countParam++;
				}
			}
		}

		return $genURL;
	}

	/**
	 * This function is using to generate the commoon Search Area in different Module e.g. Course etc.
	 * @global object
	 * @param Array $paramArray => List all the available parameters in the page
	 * @param Array $filterArray => Show the different Filters in the Serach Area
	 * @param String $pageURL => Hold Page URL
	 * @return String the HTML to output.
	 */
	 
	function genCommonSearchForm($paramArray=array(), $filterArray=array(), $pageURL){
		global $CFG;

		////// Define those key, which we need to remove first time //////
		$removeKeyArray = array('ch','id','flag'); 

		
		$removeSearchKeyArray = array('key','id','flag');

		////// Getting common URL for the Search //////
		//echo $pageURL."<br>";
		$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
		
		$chkParamFirst = strpos($genURL, '?');

		////// Getting common URL for the Search without keyword //////
		$genClearKeywordURL = genParameterizedURL($paramArray, $removeSearchKeyArray, $pageURL);

		////// Generate Character Filter area //////
		$characterArray = array('All','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','Other');

		$htmlOutput = '<div id="common-search">';

		////// Generate Character Filter Area //////
		$htmlOutput .= '<div class="charc-filter">';
		foreach($characterArray as $key => $val) {
			$charShow = '';
			if($paramArray['ch'] != '') {
				if( ($paramArray['ch'] == 'OTH') && ($val == 'Other') ) {
					$charShow = '<span class="charc-selected">'.strtoupper($val).'</span>';
				} else if($paramArray['ch'] == $val) {
					$charShow = '<span class="charc-selected">'.strtoupper($val).'</span>';
				} else {
					$charShow = '<span class="charc">'.strtoupper($val).'</span>';
				}
			} else {
				if($val == 'All')
					$charShow = '<span class="charc-selected">'.strtoupper($val).'</span>';
				else
					$charShow = '<span class="charc">'.strtoupper($val).'</span>';
			}

			if($val == 'All') {
				//$htmlOutput .= '<a href="'.$CFG->wwwroot.$pageURL.'">'.$charShow.'</a>'; // commented by rajesh 
				$htmlOutput .= '<a href="'.$genURL.'">'.$charShow.'</a>';  // changed by rajesh 
			} else if($val == 'Other') {
				if($chkParamFirst === false)
					$htmlOutput .= '<a href="'.$genURL.'?ch=OTH">'.$charShow.'</a>';
				else
					$htmlOutput .= '<a href="'.$genURL.'&ch=OTH">'.$charShow.'</a>';
			} else {  
				if($chkParamFirst === false)
					$htmlOutput .= '<a href="'.$genURL.'?ch='.$val.'">'.$charShow.'</a>';
				else
					$htmlOutput .= '<a href="'.$genURL.'&ch='.$val.'">'.$charShow.'</a>';
			}
		}
		$htmlOutput .= '</div>';

		////// Generate opening common Search Form //////
		$htmlOutput .= '<form name="searchForm" id="searchForm" action="'.$genURL.'" method="get"><div id="search-form">';

		////// Generate hidden variable for character param if any //////
		if($paramArray['ch'] != '')
			$htmlOutput .= '<input type="hidden" name="ch" id="ch" value="'.$paramArray['ch'].'" />';

		////// Generate input variable for keyword param if any //////
		$htmlOutput .= '<div class="search-input"><input type="text" name="key" id="key" placeholder="Search" value="'.$paramArray['key'].'" /></div>';

		////// Generate check boxes variable for Filter param if any //////
		$htmlOutput .= '<div class="filter"><span>'.get_string('filterby','course').'</span>';
		foreach($filterArray as $key => $val) {
			$htmlOutput .= '<span><input type="checkbox" name="'.$key.'" id="'.$key.'" value="1" '.( (($paramArray['key'] != '') && ($paramArray[$key] == 1)) ? 'checked' : '').' />'.$val.'</span>';
		}
		$htmlOutput .= '</div>';

		////// Generate closing common Search Form and keep the submit button outside the form, so that it doesn't enter into the URL parameter list //////
		$htmlOutput .= '</div></form>';
		$htmlOutput .= '<input type="button" name="search" value="Search" onClick="document.searchForm.submit();" title="'.get_string('search','course').'" /><a href="'.$genClearKeywordURL.'" class="close" title="'.get_string('clearsearch','course').'">'.get_string('clearsearch','course').'</a></div>';

		///// Return HTML Output /////
		return $htmlOutput;
	}


	/**
	 * This function is using to update the is_active Flag of a Course
	 * @global object
	 * @param Integer $courseId => Hold the Course Id
	 * @param Boolean $flagCheck => Hold Course Active Flag Value
	 * @return boolean True/ False upon saving.
	 */
	 
	function setCourseActiveFlag($courseId, $flagCheck) {
		global $DB;
		$data = new stdClass();

		$data->is_active = $flagCheck;
		$data->id = $courseId;
		$nid = $DB->update_record('course', $data);

		return true;
	}


	/**
	 * This function is using to update the is_active Flag of a Category
	 * @global object
	 * @param Integer $categoryId => Hold the Category Id
	 * @param Boolean $flagCheck => Hold Category Active Flag Value
	 * @return boolean True/ False upon saving.
	 */
	 
	function setCategoryActiveFlag($categoryId, $flagCheck) {
		global $CFG,$DB;

		if($flagCheck == 1) {
			$data = new stdClass();

			$data->is_active = $flagCheck;
			$data->id = $categoryId;
			$nid = $DB->update_record('course_categories', $data);

			return true;
		} else {
			if(countCourseByCategoryId($categoryId) > 0) {			
				return false;
			} else {
				$data = new stdClass();

				$data->is_active = $flagCheck;
				$data->id = $categoryId;
				$nid = $DB->update_record('course_categories', $data);

				return true;
			}
		}
	}


	/**
	 * This function is using to count the Result Set of Course according to different Searching Criteria
	 * @global object
	 * @param Array $paramArray => List all the available parameters in the page
	 * @return Integer => Count of the Data ResultSet.
	 */
	 
	function countCourseListingResult($paramArray){
		$courseDetails = genCourseListingResult($paramArray, false);
		$courseCnt = 0;
		foreach($courseDetails as $key => $course) {
			$courseCnt++;
		}

		return $courseCnt;
	}


	/**
	 * This function is using to count the Result Set of Category according to different Searching Criteria
	 * @global object
	 * @param Array $paramArray => List all the available parameters in the page
	 * @return Integer => Count of the Data ResultSet.
	 */
	 
	function countCategoryListingResult($paramArray){
		$courseDetails = genCategoryListingResult($paramArray, false);
		$courseCnt = 0;
		foreach($courseDetails as $key => $course) {
			$courseCnt++;
		}

		return $courseCnt;
	}


	/**
	 * This function is using to count the Result Set of Category according to different Searching Criteria
	 * @global object
	 * @param Array $paramArray => List all the available parameters in the page
	 * @return Integer => Count of the Data ResultSet.
	 */
	 
	function countCourseByCategoryId($categoryId){
		global $CFG,$DB;

		$courseCountQuery = "SELECT COUNT(c.id) AS cnt_course FROM {$CFG->prefix}course c" .
					   " WHERE c.category = '" . $categoryId . "'" . 
					   " ORDER BY c.id ASC";
		$courseDetails = $DB->get_record_sql($courseCountQuery);

		return $courseDetails->cnt_course;
	}


	/**
	 * This function is using to generate the Result Set of Course according to different Searching Criteria
	 * @global object
	 * @param Array $paramArray => List all the available parameters in the page
	 * @return Object of the Data ResultSet.
	 */
	 
	function genCourseListingResult($paramArray, $limitFlag=true){
		global $USER,$CFG,$DB;
		$searchString = "";
		if($paramArray['ch'] != '') {
			if($paramArray['ch'] == 'OTH')
				$searchString = "c.fullname REGEXP '^[^a-zA-Z]' AND";
			else
				$searchString = "c.fullname like '".$paramArray['ch']."%' AND";
		}
		$paramArray['key'] = addslashes($paramArray['key']);
		$searchKeyAll = true;
		if( ($paramArray['id']==1) || ($paramArray['nm']==1) || ($paramArray['desc']==1) || ($paramArray['cnm']==1) )
			$searchKeyAll = false;

		if($searchKeyAll && ($paramArray['key'] != '')) {
			$searchString .= " (c.idnumber like '%".$paramArray['key']."%' OR c.fullname like '%".$paramArray['key']."%' OR c.shortname like '%".$paramArray['key']."%' OR strip_tags(c.summary) like '%".$paramArray['key']."%' OR c.keywords like '%".$paramArray['key']."%' OR ca.name like '%".$paramArray['key']."%') AND";
		} else if ($paramArray['key'] != '') {
			$searchKeyAllString .= "(";

			if( $paramArray['id']==1 )
				$searchKeyAllString .= " c.idnumber like '%".$paramArray['key']."%' OR";
			if( $paramArray['nm']==1 )
				$searchKeyAllString .= " c.fullname like '%".$paramArray['key']."%' OR c.shortname like '%".$paramArray['key']."%' OR";
			if( $paramArray['desc']==1 )
				$searchKeyAllString .= " strip_tags(c.summary) like '%".$paramArray['key']."%' OR";
			if( $paramArray['tag']==1 )
				$searchKeyAllString .= " c.keywords like '%".$paramArray['key']."%' OR";
			if( $paramArray['cnm']==1 )
				$searchKeyAllString .= " ca.name like '%".$paramArray['key']."%' OR";

			$searchKeyAllString = substr($searchKeyAllString, 0, -2);

			$searchKeyAllString .= ") AND";

			$searchString .= $searchKeyAllString;
		}

		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$departmentCourseSelect = '';
				break;
			CASE $CFG->userTypeManager:
					/*$departmentCourseSelect =	" AND (c.id IN(SELECT dc.courseid
													FROM mdl_department_course AS dc
													LEFT JOIN mdl_department AS d ON d.id = dc.departmentid
													LEFT JOIN mdl_department_members as dm ON dm.departmentid = d.id
													WHERE dm.userid = '".$USER->id."' AND dc.is_active = 1) OR c.createdby = '".$USER->id."')";*/
													
					$departmentCourseSelect =	" AND (c.id IN(SELECT dc.courseid
													FROM mdl_department_course AS dc
													LEFT JOIN mdl_department AS d ON d.id = dc.departmentid
													LEFT JOIN mdl_department_members as dm ON dm.departmentid = d.id
													WHERE dm.userid = '".$USER->id."' AND dc.is_active = 1) )";								
				break;
		}

		$searchString .= " c.id != 1 AND";

		if($searchString != '')
			$searchString .= " 1=1";


		$courseQuery = "SELECT c.id, c.category, c.fullname, c.idnumber, c.keywords, c.is_active, ca.name as cat_name, c.createdby, c.publish FROM {$CFG->prefix}course c" . 
					   " LEFT JOIN {$CFG->prefix}course_categories ca ON c.category = ca.id" . 
					   ( ($searchString != '') ? " WHERE " . $searchString : "") .$departmentCourseSelect.
					   " ORDER BY c.fullname ASC";
		
		if($limitFlag) {
			$page = optional_param('page', 1, PARAM_INT);
			$perpage = optional_param('perpage', 10, PARAM_INT);

			if($perpage != 0) {
				$offset = ($page - 1) * $perpage;

				$courseQuery .= " LIMIT " . $offset . ", " . $perpage;
			}
		}
		
		
		$courseDetails = $DB->get_records_sql($courseQuery);
		return $courseDetails;
	}	


	/**
	 * This function is using to generate the Result Set of Category according to different Searching Criteria
	 * @global object
	 * @param Array $paramArray => List all the available parameters in the page
	 * @return Object of the Data ResultSet.
	 */
	 
	function genCategoryListingResult($paramArray, $limitFlag=true){
		global $USER,$CFG,$DB;

		$searchString = "";
		if($paramArray['ch'] != '') {
			if($paramArray['ch'] == 'OTH')
				$searchString = "c.name REGEXP '^[^a-zA-Z]' AND";
			else
				$searchString = "c.name like '".$paramArray['ch']."%' AND";
		}

		$searchKeyAll = true;
		if( ($paramArray['id']==1) || ($paramArray['nm']==1) || ($paramArray['desc']==1) || ($paramArray['cnm']==1) )
			$searchKeyAll = false;
		$paramArray['key'] = addslashes($paramArray['key']);
		if($searchKeyAll && ($paramArray['key'] != '')) {
			$searchString .= " (c.name like '%".$paramArray['key']."%' OR strip_tags(c.description) like '%".$paramArray['key']."%') AND";
		} else if ($paramArray['key'] != '') {

			$searchKeyAllString .= "(";

			if( $paramArray['nm']==1 )
				$searchKeyAllString .= " c.name like '%".$paramArray['key']."%' OR";
			if( $paramArray['desc']==1 )
				$searchKeyAllString .= " strip_tags(c.description) like '%".$paramArray['key']."%' OR";

			$searchKeyAllString = substr($searchKeyAllString, 0, -2);

			$searchKeyAllString .= ") AND";

			$searchString .= $searchKeyAllString;

			/*if( $paramArray['nm']==1 )
				$searchString .= " c.name like '%".$paramArray['key']."%' AND ";
			if( $paramArray['desc']==1 )
				$searchString .= " c.description like '%".$paramArray['key']."%' AND";*/
		}

		if($searchString != '')
			$searchString .= " 1=1";

		$categoryQuery = "SELECT c.id, c.name, c.description, c.is_active FROM {$CFG->prefix}course_categories c" . 
					   ( ($searchString != '') ? " WHERE " . $searchString : "") .
					   " ORDER BY c.name ASC";

		if($limitFlag) {
			$page = optional_param('page', 1, PARAM_INT);
			$perpage = optional_param('perpage', 10, PARAM_INT);

			if($perpage != 0) {
				$offset = ($page - 1) * $perpage;

				$categoryQuery .= " LIMIT " . $offset . ", " . $perpage;
			}
		}		
		$categoryDetails = $DB->get_records_sql($categoryQuery);

		return $categoryDetails;
	}

	
	/**
	 * This function is using to generate the Listing of Course according to different Searching Criteria
	 * @global object
	 * @param Array $paramArray => List all the available parameters in the page
	 * @param String $pageURL => Hold Page URL
	 * @return string the HTML to output.
	 */
	 
	function genModifiedCourseListing($paramArray, $pageURL){
		global $CFG,$DB,$USER;

		$pageParamArray = array(
							'page' => optional_param('page', '', PARAM_ALPHANUM),
							'perpage' => optional_param('perpage', '', PARAM_ALPHANUM)
						  );
		$paramArray = array_merge($paramArray, $pageParamArray);

		////// Define those key, which we need to remove first time from General URL //////
		$removeKeyArray = array('id','flag','publish');		

		////// Getting common URL for the Search List //////
		$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);

		$flagCheck = optional_param('flag', '', PARAM_ALPHANUM);
		if( ($flagCheck == '0') || ($flagCheck == '1') ){
			$courseId = optional_param('id', '', PARAM_INT);		
			setCourseActiveFlag($courseId, (int)$flagCheck);
			$_SESSION['update_msg'] = get_string('record_updated');
			redirect($genURL);
		}
		$publishCheck = optional_param('publish', '', PARAM_ALPHANUM);
		if($publishCheck == '1'){
			$courseId = optional_param('id', '', PARAM_INT);		
			$published = publishCourse($courseId, (int)$publishCheck);
			$_SESSION['update_msg'] = get_string('record_updated');
			redirect($genURL);
		}

		$chkParamFirst = strpos($genURL, '?');

		////// Define those key, which we need to remove first time from Update URL //////
		$removeKeyUpdateArray = array('id','category','returnto');

		////// Getting Update URL for the Search List //////
		$genUpdateURL = genParameterizedURL($paramArray, $removeKeyUpdateArray, '/course/edit.php');
		$chkParamFirstInUpdate = strpos($genUpdateURL, '?');
		
		$courseHTML = '<div class="course-listing">' . 
							'<h2><span></span>Courses ('.countCourseListingResult($paramArray).')</h2>' ;
		$courseHTML .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'. 
							'<tr>'.
								'<td width="30%">'.get_string('coursename','course').'</td>'.
								'<td width="14%">'.get_string('coursenumber','course').'</td>'.
								'<td width="14%">'.get_string('tag','course').'</td>'.
								'<td width="14%">'.get_string('categoryname','course').'</td>'.
								'<td width="28%">'.get_string('manage','course').'</td>'.
							'</tr>';
		$courseDetails = genCourseListingResult($paramArray);

		if(count($courseDetails) > 0) {
			foreach($courseDetails as $key => $course) {
				$courseImg = getCourseImage($course);
				$rowClass = "";
				if($course->is_active == 1) {
					if($chkParamFirst === false) {
						$flagURL = '?id='.$course->id.'&flag=0';
					} else {
						$flagURL = '&id='.$course->id.'&flag=0';
					}

					if($chkParamFirstInUpdate === false) {
						$updateURL = '?category='.$course->category.'&returnto=category&id='.$course->id;
					} else {
						$updateURL = '&category='.$course->category.'&returnto=category&id='.$course->id;
					}
				
					$activeDeactiveLink = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'" class="view" title="'.get_string('previewlink','course').'">'.get_string('previewlink','course').'</a>';	
					if($USER->archetype == $CFG->userTypeAdmin){
						if($course->publish == 1){
							$activeDeactiveLink .= '<a href="'.$CFG->wwwroot.'/course/courseallocation.php?id='.$course->id.'" class="assign-group" title = "'.get_string('assignusers').'">'.get_string('assignusers').'</a> ';
						}
						$activeDeactiveLink .= '<a href="'.$CFG->wwwroot.'/course/courseview.php?id='.$course->id.'" class="edit" title="'.get_string('edit').'">'.get_string('edit').'</a> ' ;
						$activeDeactiveLink .= '<a href="'.$genURL . $flagURL.'" class="disable" title="'.get_string('deactivatelink','course').'">'.get_string('deactivatelink','course').'</a>';
						
						
					}else{
						if($course->publish == 1){
							$activeDeactiveLink .= '<a href="'.$CFG->wwwroot.'/course/courseallocation.php?id='.$course->id.'" class="assign-group" title = "'.get_string('assignusers').'">'.get_string('assignusers').'</a> ';
						}
						if($course->createdby == $USER->id){
							$activeDeactiveLink .= '<a href="'.$CFG->wwwroot.'/course/courseview.php?id='.$course->id.'" class="edit" title="'.get_string('edit').'">'.get_string('edit').'</a> ' ;
							$activeDeactiveLink .= '<a href="'.$genURL . $flagURL.'" class="disable" title="'.get_string('deactivatelink','course').'">'.get_string('deactivatelink','course').'</a>';
						}
					}
				} else {
					if($chkParamFirst === false)
						$flagURL = '?id='.$course->id.'&flag=1';
					else
						$flagURL = '&id='.$course->id.'&flag=1';

					$activeDeactiveLink = '<a href="javascript:void(0);" class="view-disable">'.get_string('previewlink','course').'</a> ';
					if($USER->archetype == $CFG->userTypeAdmin){
						$activeDeactiveLink .= '<a href="javascript:void(0);" class="edit-disable">'.get_string('updatelink','course').'</a> ';
						$activeDeactiveLink .= '<a href="'.$genURL . $flagURL.'" class="enable" title="'.get_string('activatelink','course').'">'.get_string('activatelink','course').'</a>';
					}else{
						$activeDeactiveLink .= '<a href="javascript:void(0);" class="assign-courses disable">'.get_string('assignusers').'</a> ';
						if($course->createdby == $USER->id){
							$activeDeactiveLink .= '<a href="javascript:void(0);" class="edit-disable">'.get_string('updatelink','course').'</a> ';
							$activeDeactiveLink .= '<a href="'.$genURL . $flagURL.'" class="enable" title="'.get_string('activatelink','course').'">'.get_string('activatelink','course').'</a>';
						}
					}
					$rowClass = ' class="disable"';
				}
				if($chkParamFirst === false){
						$publishUrl = '?id='.$course->id.'&publish=1';
				}else{
						$publishUrl = '&id='.$course->id.'&publish=1';
				}

				if( ($USER->archetype == $CFG->userTypeAdmin) || ($course->createdby == $USER->id) ){
					if($course->publish == 1)
					{
						$activeDeactiveLink .= '<a href="javascript:void(0);" class="published" style="cursor: text;" title="'.get_string('published','course').'">'.get_string('published','course').'</a>';
					}else{
						$activeDeactiveLink .= '<a href="'.$genURL.$publishUrl.'" class="publish" title="'.get_string('publish','course').'">'.get_string('publish','course').'</a>';
					}
				}	
				$courseHTML .= '<tr'.$rowClass.'>'.
									'<td>'.$courseImg . '<span class="f-left">'.$course->fullname.'</span></td>'.
									'<td>'.$course->idnumber.'</td>'.
									'<td>'.$course->keywords.'</td>'.
									'<td>'.$course->cat_name.'</td>'.
									'<td>' . 
										$activeDeactiveLink . 
									'</td>'.
							  '</tr>';
			}
		} else {
			$courseHTML .= '<tr>'.
								'<td colspan="5">'.get_string('no_results').'</td>'.
						   '</tr>';
		}
		$courseHTML .= '</table>';
		$courseHTML .= '</div>';

		return $courseHTML;
	}		

	
	/**
	 * This function is using to generate the Listing of Course according to different Searching Criteria
	 * @global object
	 * @param Array $paramArray => List all the available parameters in the page
	 * @param String $pageURL => Hold Page URL
	 * @return string the HTML to output.
	 */
	 
	function genModifiedCategoryListing($paramArray, $pageURL){
		global $CFG;

		$pageParamArray = array(
							'page' => optional_param('page', '', PARAM_ALPHANUM),
							'perpage' => optional_param('perpage', '', PARAM_ALPHANUM)
						  );
		$paramArray = array_merge($paramArray, $pageParamArray);

		////// Define those key, which we need to remove first time from General URL //////
		$removeKeyArray = array('id','flag');		

		////// Getting common URL for the Search List //////
		$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);

		$flagCheck = optional_param('flag', '', PARAM_ALPHANUM);
		if( ($flagCheck == '0') || ($flagCheck == '1') ){
			$categoryId = optional_param('id', '', PARAM_INT);		
			setCategoryActiveFlag($categoryId, (int)$flagCheck);
			$_SESSION['update_msg'] = get_string('record_updated');
			redirect($genURL);
		}

		$chkParamFirst = strpos($genURL, '?');


		////// Define those key, which we need to remove first time from Update URL //////
		$removeKeyUpdateArray = array('id');

		////// Getting Update URL for the Search List //////
		$genUpdateURL = genParameterizedURL($paramArray, $removeKeyUpdateArray, '/course/editcategory.php');
		$chkParamFirstInUpdate = strpos($genUpdateURL, '?');
		
		$categoryHTML = '<div class="course-listing">' . 
							'<h2>Categories ('.countCategoryListingResult($paramArray).')</h2>' ;
		$categoryHTML .= '<table cellspacing="0" cellpadding="0" border="0" width="100%">'. 
							'<tr>'.
								'<td width="20%">'.get_string('categoryname','course').'</td>'.
								'<td width="40%">'.get_string('categorydesc','course').'</td>'.
								'<td width="20%">'.get_string('countcourse','course').'</td>'.
								'<td width="20%">'.get_string('manage','course').'</td>'.
							'</tr>';

		$categoryDetails = genCategoryListingResult($paramArray);
		if(count($categoryDetails) > 0) {
			foreach($categoryDetails as $key => $category) {

				$rowClass = "";
				if($category->is_active == 1) {
					if($chkParamFirst === false) {
						$flagURL = '?id='.$category->id.'&flag=0';
					} else {
						$flagURL = '&id='.$category->id.'&flag=0';
					}

					if($chkParamFirstInUpdate === false) {
						$updateURL = '?id='.$category->id;
					} else {
						$updateURL = '&id='.$category->id;
					}

					$activeDeactiveLink = '<a href="'.$genUpdateURL . $updateURL.'" class="edit" title="'.get_string('edit').'">'.get_string('edit').'</a> ' . 
										  ( (countCourseByCategoryId($category->id) > 0) ? '' : '<a href="'.$genURL . $flagURL.'" class="disable" title="'.get_string('deactivatelink','course').'">'.get_string('deactivatelink','course').'</a>');				
				} else {
					if($chkParamFirst === false)
						$flagURL = '?id='.$category->id.'&flag=1';
					else
						$flagURL = '&id='.$category->id.'&flag=1';

					$activeDeactiveLink = '<a href="javascript:void(0);" class="edit-disable">'.get_string('edit').'</a> ' . 
										  '<a href="'.$genURL . $flagURL.'" class="enable" title="'.get_string('activatelink','course').'">'.get_string('activatelink','course').'</a>';
					$rowClass = ' class="disable"';
				}
				$categoryHTML .= '<tr'.$rowClass.'>'.
									'<td><span class="f-left">'.$category->name.'</span></td>'.
									'<td>'.strip_tags($category->description).'</td>'.
									'<td>'.countCourseByCategoryId($category->id).'</td>'.
									'<td>' . 
										$activeDeactiveLink . 
									'</td>'.
							  '</tr>';
			}
		} else {
			$categoryHTML .= '<tr>'.
								'<td colspan="4">'.get_string('no_results').'</td>'.
						   '</tr>';
		}
		$categoryHTML .= '</table>';
		$categoryHTML .= '</div>';

		return $categoryHTML;
	}

	/**
	 * This function is using to count the Result Set of Course Catalog according to different Searching Criteria
	 * @global object
	 * @param Array $paramArray => List all the available parameters in the page
	 * @return Integer => Count of the Data ResultSet.
	 */
	 
	function countCourseCatalogListingResult($paramArray){
		$catalogDetails = genCourseCatalogListingResult($paramArray);
		$catalogCnt = 0;
		foreach($catalogDetails as $key => $catalog) {
			$catalogCnt++;
		}

		return $catalogCnt;
	}	


	/**
	 * This function is using to generate the Result Set of Course Catalog according to different Searching Criteria
	 * @global object
	 * @param Array $paramArray => List all the available parameters in the page
	 * @return Object of the Data ResultSet.
	 */
	 
	function genCourseCatalogListingResult($paramArray){
		global $USER,$CFG,$DB;

		$searchStringP = " 1 = 1 ";
		$searchStringC = " 1 = 1";
		
		
		/*$searchString = "";
		if($paramArray['ch'] != '') {
			if($paramArray['ch'] == 'OTH')
				$searchString = "c.fullname REGEXP '^[^a-zA-Z]' AND";
			else
				$searchString = "c.fullname like '".$paramArray['ch']."%' AND";
		}

		$searchKeyAll = true;
		if( ($paramArray['nm']==1) || ($paramArray['desc']==1) || ($paramArray['cnm']==1) )
			$searchKeyAll = false;

		if($searchKeyAll && ($paramArray['key'] != '')) {
			$searchString .= " (c.fullname like '%".$paramArray['key']."%' OR c.summary like '%".$paramArray['key']."%') AND";
		} else if ($paramArray['key'] != '') {
			if( $paramArray['nm']==1 )
				$searchString .= " c.fullname like '%".$paramArray['key']."%' AND ";
			if( $paramArray['desc']==1 )
				$searchString .= " c.summary like '%".$paramArray['key']."%' AND";
			//if( $paramArray['cnm']==1 )
				//$searchString .= " cc.name like '%".$paramArray['key']."%' AND";
		}
		$searchString .= " c.is_active=1 AND";

		if($searchString != '')
			$searchString .= " 1=1";
			
		*/
		$paramArray['key'] = addslashes($paramArray['key']);
		if(isset($paramArray['key']) && $paramArray['key'] != '') {
		    $searchStringP .= " AND (p.name like '%".$paramArray['key']."%' OR p.description like '%".$paramArray['key']."%') ";
			$searchStringC .= " AND (c.fullname like '%".$paramArray['key']."%' OR c.summary like '%".$paramArray['key']."%') ";
			
		}

        $searchStringP .= " AND p.status = 1 AND p.deleted = 0 ";
		$searchStringC .= " AND c.id NOT IN (1) AND c.is_active = 1 AND c.deleted = 0 ";
	
		$programCatalogQuery = "SELECT CONCAT('p',p.id) AS keyid, 'program' as type, p.id, p.name as fullname, p.description as summary, '' as suggesteduse, '' as learningobj, '' as performanceout FROM {$CFG->prefix}programs p ";
		$programCatalogQuery .= " WHERE " .$searchStringP;
        $programCatalogQuery .= " ORDER BY fullname ASC";
					   
		$courseCatalogQuery = "SELECT CONCAT('c',c.id) AS keyid, 'course' as type, c.id, c.fullname, c.summary, c.suggesteduse, c.learningobj, c.performanceout FROM {$CFG->prefix}course c"; 
		$courseCatalogQuery .= " WHERE " .$searchStringC;
		$courseCatalogQuery .= "	ORDER BY fullname ASC";
		
		$catalogQuery = "($programCatalogQuery) UNION ($courseCatalogQuery) ";

		$catalogDetails = $DB->get_records_sql($catalogQuery);
		return $catalogDetails;
	}


	/**
	 * This function is using to generate the Listing of Course Catalog according to different Searching Criteria
	 * @global object
	 * @param Array $paramArray => List all the available parameters in the page
	 * @param String $pageURL => Hold Page URL
	 * @return string the HTML to output.
	 */
	 
	function genCourseCatalogListing($paramArray, $pageURL){
		global $CFG,$USER;

		$userCourseId = getAssignedCourseIdForUser($USER->id);
		$userProgramId = getAssignedProgramIdForUser($USER->id);
		
		$assignGroupsForUser = getAssignGroupsForUser($USER->id, 3);
		

		////// Define those key, which we need to remove first time from General URL //////
		$removeKeyArray = array('pid','cid','mailflag');		

		////// Getting common URL for the Search List //////
		$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
		$chkParamFirst = strpos($genURL, '?');
		
		$catalogHTML = '<div class="course-listing scroll-pane">' . 
		$catalogHTML .= '<div class="catalog-box">';

		$courseCatalogDetails = genCourseCatalogListingResult($paramArray);
		$mCourseIdArr = getManagerCourse();
		$mProgramIds = getManagerProgram();


		if(count($courseCatalogDetails) > 0) {
		
				$catalogHTML .= '<div class="catalog-category">
									<span class="catalog_category_name">'.get_string('catalog','course').'</span>
								 </div>';
                
				$i=-1;
				foreach($courseCatalogDetails as $key=>$catalog) {
		            
					$i++;
					$type = $catalog->type;
					$id = $catalog->id;
					$name = $catalog->fullname;
					$summary = $catalog->summary;
					$suggestedUse = $catalog->suggesteduse;
					$learningObj = $catalog->learningobj;
					$performanceOut = $catalog->performanceout;
					
					
					$summary = $summary?(strlen(strip_tags($summary)) > 85?(substr(strip_tags($summary), 0, 85).' ....'):strip_tags($summary)):'';
					$suggestedUse = $suggestedUse?(strlen(strip_tags($suggestedUse)) > 85?(substr(strip_tags($suggestedUse), 0, 85).' ....'):strip_tags($suggestedUse)):'';
					$learningObj = $learningObj?(strlen(strip_tags($learningObj)) > 85?(substr(strip_tags($learningObj), 0, 85).' ....'):strip_tags($learningObj)):'';
					$performanceOut = $performanceOut?(strlen(strip_tags($performanceOut)) > 85?(substr(strip_tags($performanceOut), 0, 85).' ....'):strip_tags($performanceOut)):'';
											
					$summary = $summary?('<br /><strong>'.get_string('description','course').': </strong>'.$summary):'';
					$suggestedUse = $suggestedUse?('<br /><strong>'.get_string('suggesteduse','course').': </strong>'.$suggestedUse):'';
					$learningObj = $learningObj?('<br /><strong>'.get_string('learningobj','course').': </strong>'.$learningObj):'';
					$performanceOut = $performanceOut?('<br /><strong>'.get_string('performanceOut','course').': </strong>'.$performanceOut):'';
										
					
					$oddEvenCatalog = $i %2 == 0 ? 'evenCatalog' : 'oddCatalog' ;
					if($type == $CFG->programType){
					
					    $catalogImg = getProgramImage($catalog);
						$programCourses = getProgramCourses($id);
						$cntPCourse = count($programCourses);
						
						$assignLaunchLinkP = '';
						
						if( $USER->archetype != $CFG->userTypeAdmin ) {

							if( $USER->archetype == $CFG->userTypeManager ) {
							
								//$assignLaunchLinkP = '<a href="'.$CFG->wwwroot.'/course/programlaunch.php?pid='.$id.'" class="launch-course" title="'.get_string('programlaunch','course').'">'.get_string('launchcourse','course').'</a>';
							
								if( !in_array($id, $mProgramIds ) ) {
									
									if($chkParamFirst === false) {
										$flagURLP = '?cid='.$id.'&mailflag=1';
									} else {
										$flagURLP = '&cid='.$id.'&mailflag=1';
									}
									$assignLaunchLinkP = '<a href="'.$genURL . $flagURLP.'" class="assign-course" title="'.get_string('requestprogram','course').'">'.get_string('requestprogram','course').'</a>';
								}
								
							} elseif( $USER->archetype == $CFG->userTypeStudent ) { 
							
								if( in_array($id, $userProgramId ) ) {
									//$assignLaunchLink = '<a href="'.$CFG->wwwroot.'/course/programlaunch.php?pid='.$id.'" class="launch-course" title="'.get_string('programlaunch','course').'">'.get_string('launchcourse','course').'</a>';
								} else {
									
									if($chkParamFirst === false) {
										$flagURLP = '?pid='.$id.'&enrolflag=1';
									} else {
										$flagURLP = '&pid='.$id.'&enrolflag=1';
									}
									
									if(count($assignGroupsForUser) > 0){
									
											$isUserAndProgramInSameTeam = false;
											$assignGroupsForProgram = getAssignGroupsForProgram($id, 3);
	
											foreach($assignGroupsForUser as $assignedGroup){
											   if(in_array($assignedGroup, $assignGroupsForProgram)){
												  $isUserAndProgramInSameTeam = true;
												  break;
											   }
											}
											
											if( $isUserAndProgramInSameTeam) {
											
												if($chkParamFirst === false) {
													$flagURLP = '?pid='.$id.'&enrolflag=1';
												} else {
													$flagURLP = '&pid='.$id.'&enrolflag=1';
												}
	                                            
												$a->name = $name;  
												$assignLaunchLinkP = '<a href="javascript:;" data-url = "'.$genURL . $flagURLP.'" data-msg="'.get_string('areyousureyouwanttoenrolthisprogram','course', $a->name).'" class="enrollment-program" title="'.get_string('enrolprogram','course').'">'.get_string('enrolprogram','course').'</a>';
	
											}else{
											    $assignLaunchLinkP = '<a href="'.$genURL . $flagURLP.'" class="assign-course" title="'.get_string('requestprogram','course').'">'.get_string('requestprogram','course').'</a>';
											} 
										
								      }else{
									
									     $assignLaunchLinkP = '<a href="'.$genURL . $flagURLP.'" class="assign-course" title="'.get_string('requestprogram','course').'">'.get_string('requestprogram','course').'</a>';
								      }
										   
									
									
									
								}
							}
							
						}
						   
						$assignLaunchLink = $assignLaunchLinkP.'&nbsp;<div id="plus-minus-icon-'.$key.'" rel="'.$key.'" class="pull-left right-icon program-box"><a href="javascript:void(0)" title= "'.get_string('showcourse','course').'" ></a></div>';
						   
 
						$courseDiv = '';
						$courseDiv .= '<div class="c-box" id="cbox'.$key.'" >';
						
						
						$pcDiv = '';
						
						
						//pr($sectionArr);
						if($cntPCourse > 0 ){

                            $j = -1; 
							foreach($programCourses as $pCourses){
							            
										$j++;
										$buttons = array();
										$lastcolumn = '';
										$curtime = time();
										
									    $pcId = $pCourses->id;
										$pcNname = $pCourses->fullname;
										$pcSummary = $pCourses->summary;
										$pcSuggestedUse = $pCourses->suggesteduse;
										$pcLearningObj = $pCourses->learningobj;
										$pcPerformanceOut = $pCourses->performanceout;
										
										$pcSummary = $pcSummary?(strlen(strip_tags($pcSummary)) > 85?(substr(strip_tags($pcSummary), 0, 85).' ....'):strip_tags($pcSummary)):'';
										$pcSuggestedUse = $pcSuggestedUse?(strlen(strip_tags($pcSuggestedUse)) > 85?(substr(strip_tags($pcSuggestedUse), 0, 85).' ....'):strip_tags($pcSuggestedUse)):'';
										$pcLearningObj = $pcLearningObj?(strlen(strip_tags($pcLearningObj)) > 85?(substr(strip_tags($pcLearningObj), 0, 85).' ....'):strip_tags($pcLearningObj)):'';
										$pcPerformanceOut = $pcPerformanceOut?(strlen(strip_tags($pcPerformanceOut)) > 85?(substr(strip_tags($pcPerformanceOut), 0, 85).' ....'):strip_tags($pcPerformanceOut)):'';
																
										$pcSummary = $pcSummary?('<br /><strong>'.get_string('description','course').': </strong>'.$pcSummary):'';
										$pcSuggestedUse = $pcSuggestedUse?('<br /><strong>'.get_string('suggesteduse','course').': </strong>'.$pcSuggestedUse):'';
										$pcLearningObj = $pcLearningObj?('<br /><strong>'.get_string('learningobj','course').': </strong>'.$pcLearningObj):'';
										$pcPerformanceOut = $performanceOut?('<br /><strong>'.get_string('performanceOut','course').': </strong>'.$pcPerformanceOut):'';
				
										$pcImg = getCourseImage($pCourses);
										$oddEvenPCourse = $j %2 == 0 ? 'evenPCourse' : 'oddPCourse' ;
										
										$assignLaunchLinkPC = '';
										
										if( $USER->archetype != $CFG->userTypeAdmin ) {
										
											if( $USER->archetype == $CFG->userTypeStudent ) {
											
												//if( in_array($id, $userProgramId ) && in_array($pcId, $userCourseId )) {
												if( in_array($id, $userProgramId ) ) {
												    $assignLaunchLinkPC = '<a href="'.$CFG->wwwroot.'/course/courselaunch.php?id='.$pcId.'" class="launch-course" title="'.get_string('launchcourse','course').'">'.get_string('launchcourse','course').'</a>';
												}
												
											} elseif( $USER->archetype == $CFG->userTypeManager ) {
											
												/*if( in_array($id, $userCourseId ) ) {
													$assignLaunchLinkPC = '<a href="'.$CFG->wwwroot.'/course/courselaunch.php?id='.$pcId.'" class="launch-course" title="'.get_string('launchcourse','course').'">'.get_string('launchcourse','course').'</a>';
												} else {
													if($chkParamFirst === false) {
														$flagURLPC = '?cid='.$pcId.'&mailflag=1';
													} else {
														$flagURLPC = '&cid='.$pcId.'&mailflag=1';
													}
													$assignLaunchLinkPC = '<a href="'.$genURL . $flagURLPC.'" class="assign-course" title="'.get_string('requestcourse','course').'">'.get_string('requestcourse','course').'</a>';
												}*/
											}
							            }
										
										$pcDiv .= '<div class="catalog-course '.$oddEvenPCourse.'" >'.
										'<div class="catalog_course_img">'.$pcImg.'</div>'.
										'<div class="catalog_course_detail"><span class="catalog_course_name">'.$pcNname.'</span>';
										
										$pcDiv .= $pcSummary;
										$pcDiv .= $pcSuggestedUse;
										$pcDiv .= $pcLearningObj;
										$pcDiv .= $pcPerformanceOut;
										
										$pcDiv .= '</div>';
										$pcDiv .= '<div class="catalog_course_button">'.$assignLaunchLinkPC.'</div>';
										$pcDiv .= '</div>';
					
								
							}

						}else{
						  
						  $pcDiv .= '<div class="catalog-course '.$oddEvenPCourse.'" >'.
										'<div class="catalog_course_detail"><span class="catalog_course_name">'.get_string('norecordfound', 'course').'</span></div>';
						  $pcDiv .= '</div>';
										
						} 
								
						$courseDiv .= $pcDiv;
						
						$courseDiv .= '</div>';
						
						   
					  
					}elseif($type == $CFG->courseType){
					
						    $catalogImg = getCourseImage($catalog);
						    $assignLaunchLink = '';
							
							if( $USER->archetype != $CFG->userTypeAdmin ) {
							
								if( $USER->archetype == $CFG->userTypeManager ) {
								
									//$assignLaunchLink = '<a href="'.$CFG->wwwroot.'/course/courselaunch.php?id='.$id.'" class="launch-course" title="'.get_string('launchcourse','course').'">'.get_string('launchcourse','course').'</a>';
									
									
									if( !in_array($id, $mCourseIdArr ) ) {
									
									    if($chkParamFirst === false) {
											$flagURL = '?cid='.$id.'&mailflag=1';
										} else {
											$flagURL = '&cid='.$id.'&mailflag=1';
										}
										$assignLaunchLink = '<a href="'.$genURL . $flagURL.'" class="assign-course" title="'.get_string('requestcourse','course').'">'.get_string('requestcourse','course').'</a>';
									}

								} elseif( $USER->archetype == $CFG->userTypeStudent ) { 
								

									if( in_array($id, $userCourseId ) ) {
										$assignLaunchLink = '<a href="'.$CFG->wwwroot.'/course/courselaunch.php?id='.$id.'" class="launch-course" title="'.get_string('launchcourse','course').'">'.get_string('launchcourse','course').'</a>';
									}else{
									
											if($chkParamFirst === false) {
												$flagURL = '?cid='.$id.'&mailflag=1';
											} else {
												$flagURL = '&cid='.$id.'&mailflag=1';
											}
													
											if(count($assignGroupsForUser) > 0){
											
											    $isUserAndCourseInSameTeam = false;
											    $assignedGroupsForCourse = getAssignGroupsForCourse($id, 3);

											    foreach($assignGroupsForUser as $assignedGroup){
												   if(in_array($assignedGroup, $assignedGroupsForCourse)){
												      $isUserAndCourseInSameTeam = true;
													  break;
												   }
												}
												
												if( $isUserAndCourseInSameTeam) {
												
													if($chkParamFirst === false) {
													    $flagURL = '?cid='.$id.'&enrolflag=1';
													} else {
														$flagURL = '&cid='.$id.'&enrolflag=1';
													}
                                                    
													$a->name = $name;  
													$assignLaunchLink = '<a href="javascript:;" data-url = "'.$genURL . $flagURL.'"   data-msg="'.get_string('areyousureyouwanttoenrolthiscourse','course', $a->name).'" class="enrollment-course" title="'.get_string('enrolcourse','course').'">'.get_string('enrolcourse','course').'</a>';

												 }else{
												   $assignLaunchLink = '<a href="'.$genURL . $flagURL.'" class="assign-course" title="'.get_string('requestcourse','course').'">'.get_string('requestcourse','course').'</a>';
												 } 
												
										      }else{
										         $assignLaunchLink = '<a href="'.$genURL . $flagURL.'" class="assign-course" title="'.get_string('requestcourse','course').'">'.get_string('requestcourse','course').'</a>';
										      }	
											
									  }		
								}
                            }

					}

					$catalogHTML .= '<div class="catalog-course '.$oddEvenCatalog.' "  id="catalog-course-'.$key.'" >'.
										'<div class="catalog_course_img">'.$catalogImg.'</div>'.
										'<div class="catalog_course_detail"><span class="catalog_course_name">'.$name.'</span>';
										
					$catalogHTML .= $summary;
					$catalogHTML .= $suggestedUse;
					$catalogHTML .= $learningObj;
					$catalogHTML .= $performanceOut;
	                
					$catalogHTML .= '</div>';
					$catalogHTML .= '<div class="catalog_course_button">'.$assignLaunchLink.'</div>';
				    $catalogHTML .= '</div>';
					
				    $catalogHTML .= $courseDiv;
					
				}
				
		} else {
			$catalogHTML .= '<div>'.
								'<div width="100%">'.get_string('norecordfound','course').'</div>'.
						   '</div>';
		}
		$catalogHTML .= '</div>';
		$catalogHTML .= '</div>';

		return $catalogHTML;
	}

	/***** Closed Section - Added By Madhab *****/

	/*
		*This function is used to generate paging on each page.
		*@param $totalcount -> Total number of records
		*@param $page -> Current page
		*@param $perpage -> Total number of records to be shown per page
		*@param $baseurl -> Page url on which paging is to be generated
		*return string html of paging
	*/
	function paging_bar($totalcount, $page, $perpage, $baseurl) {  
		global $CFG;

		//return blank if no record
		if($totalcount == 0 || $totalcount == ''){
			return '';
		}

		// Check if '?' exists in URL
		$chkParamFirst = strpos($baseurl, '?');
		if($chkParamFirst === false) {
			$flagURL = '?perpage=';
		} else {
			$flagURL = '&perpage=';
		}

		// Start paging block
		$output = '<div class = "paging"><div class="f-right">';
		//Get total numer of pages

		if($perpage == 0){
			$totalPages  = 1;
		}else{
			$totalPages =ceil($totalcount/$perpage); // Total number of pages
		}
		$currentPage = $page;
		$curpage = 1;
		if($page != 0){
			$curpage = $page;
		}
		$totalRecords = $curpage*$perpage;
		$disablePrevious = '';
		$disableNext = '';

		// Start Generate previous link
		if($page == 1 || $totalcount < $perpage){
			$disablePrevious = 'disable-prev';	
			$prev_page = 0;
			$prevLink = 'Previous';
		}else{
			$prev_page = $page-1;
			$prevLink = '<a href="'.$baseurl.$flagURL.$perpage.'&page='.$prev_page.'">Previous</a>';
		}
		// End Generate previous link

		// Start generate next link
		if($currentPage<$totalPages){
			$next_page = $page+1;
			$nextLink = '<a href="'.$baseurl.$flagURL.$perpage.'&page='.$next_page.'">Next</a>';
		}else{
			$disableNext = 'disable-next';
			$next_page = $page+1;
			$nextLink = 'Next';
		}
		// End generate next link
		
		// Previous page link
		$prev = "<div class = 'next-prev-icon'><span class = 'prev-link $disablePrevious'>$prevLink</span>";

		//Next page link
		$next = "<span class = 'next-link $disableNext'>$nextLink</span></div>";

		$output .= '<div class = "paging-select"><span class = "paging-select-label">'.get_string('view').'</span>';
		$ChangeUrl = $baseurl.$flagURL;
		// Start perpage dropdown
		$output .= "<select name='perpage' rel = '".$ChangeUrl."' id='perpagecount'>";

		$outputOpt = '';
		// Perpage Array
		$perpageArray = array('10','20','50','100','All');
		// Generate perpage drop down
		$perpagecount = $CFG->perPageCount; 
		$i = $perpagecount;
		foreach($perpageArray as $perpageArr){
			$selected = '';
			if($perpageArr == $perpage){
				$selected = 'selected';
			}
				$outputOpt .= "<option value='".$perpageArr."' $selected >".$perpageArr."</option>";
		}
		// per page drop down ends here
		$output .= $outputOpt."</select></div>";
		// End perpage dropdown

		// Current page details
		$output .= "<div class = 'paging-detail'><span>Page</span><span class = 'page-number'>".$currentPage."</span>";
		$output .= "<span>".get_string('of','paging')." $totalPages</span></div>";
		$output .= $prev.' '.$next;
		$output .= '</div></div>';
		//End paging block
		return $output;
	}

	/**
	 * This function will return list of departments and department count also 
	 * @global object $DB $CFG
	 * @param string $sort Sorting field
	 * @param string $dir Sorting order
	 * @param int $page current page
	 * @param int $perpage number of records per page.
	 * @param array $paramArray search paramenters array
	 * @return int department count OR array list of separtments
	 */
	function getDepartmentListing($sort='title', $dir='ASC', $page=1, $perpage=10,array $paramArray=null){
		global $DB, $CFG;
		$paramArray['key'] = strtolower($paramArray['key']);
		$paramArray['key'] = addslashes($paramArray['key']);
		$searchString = "AND ";

		// Start first character search
		if($paramArray['ch'] != '') {
			if($paramArray['ch'] == 'OTH')
				$searchString .= "(title REGEXP '^[^a-zA-Z]') AND";
			else
				$searchString .= "(title like '".$paramArray['ch']."%' OR title like '".ucwords($paramArray['ch'])."%') AND";
		}
		// End first character search
		$searchKeyAll = true;
		// Start keyword search
		if( ($paramArray['title']==1) || ($paramArray['des']==1) )
			$searchKeyAll = false;

		if($searchKeyAll && ($paramArray['key'] != '')) {
			$searchString .= " (LOWER(title) like '%".$paramArray['key']."%' OR LOWER(strip_tags(description)) like '%".$paramArray['key']."%') AND";
		} else if ($paramArray['key'] != '') {
			$searchKeyAllString .= "(";
			if( $paramArray['title']==1 )
				$searchKeyAllString .= " LOWER(title) like '%".$paramArray['key']."%' OR";
			if( $paramArray['des']==1 )
				$searchKeyAllString .= " LOWER(strip_tags(description)) like '%".$paramArray['key']."%' OR";

			$searchKeyAllString = substr($searchKeyAllString, 0, -2);
			$searchKeyAllString .= ") AND";
			$searchString .= $searchKeyAllString;
		}
		$searchString .= " 1=1";
		// End keyword search

		if($sort == 'departmentcount'){ // returns department count
			$limit = '';
			$sort = '';
			$department = $DB->get_record_sql("SELECT count(*) as departmentcount FROM {$CFG->prefix}department where deleted='0' ".$searchString." order by title ASC");
			return $department->departmentcount;
		}else{ // returns department list
			if ($sort) {
				$sort = $sort;
				$sort = " ORDER BY $sort $dir";
			}
			$page = $page - 1;
			$page = $page*$perpage;
			$limit = '';
			if($perpage != 0){
				$limit = "LIMIT $page,$perpage";
			}
			return $DB->get_records_sql("SELECT * FROM {$CFG->prefix}department where deleted='0' $searchString $sort $limit");
		}
	}

	/*
		delete depertment Image
		$departmentId = depertment id
	*/
	function deleteDepartmentImage($departmentId){
		global $DB,$USER,$CFG;
		if($departmentId != 0 && $departmentId != ''){
			$path = $CFG->dirroot.'/theme/gourmet/pix/department/';
			$imgDataExists = $DB->get_record_sql("select * from mdl_department where id = ".$departmentId);
			$path_old = $path.$imgDataExists->picture;
			if(file_exists($path_old)){
				@unlink($path_old);
			}
			$imageData = new stdClass;
			$imageData->picture = '';
			$imageData->id = $imgDataExists->id;
			$DB->update_record('department',$imageData);
			return true;
		}
		return false;
	}
	/*
		delete group Image
		$groupId = group id
	*/
	function deleteGroupImage($groupId){
		global $DB,$USER,$CFG;
		if($groupId != 0 && $groupId != ''){
			$path = $CFG->dirroot.'/theme/gourmet/pix/group/';
			$imgDataExists = $DB->get_record_sql("select * from mdl_groups where id = ".$groupId);
			$path_old = $path.$imgDataExists->picture;
			if(file_exists($path_old)){
				@unlink($path_old);
			}
			$imageData = new stdClass;
			$imageData->picture = '';
			$imageData->id = $imgDataExists->id;
			$DB->update_record('groups',$imageData);
			return true;
		}
		return false;
	}
	/**
	 * This function add courses to group
	 * @global object
	 * @param int $groupid group Id
	 * @param array $courses course Id array
	 * @return nothing
	 * add course to user
	 * assign added course to group members
	 * manage enrolments entries
	 */
	function addCourseToGroup($groupid,$courses){
		global $DB,$USER,$CFG;
		if(!empty($courses)){
			$time = time();
			foreach($courses as $course){
				$groupCourse = $DB->get_record('groups_course',array('courseid'=>$course,'groupid'=>$groupid));
				// update group course data
				if(empty($groupCourse)){
					$groupCourseTb = new stdClass;
					$groupCourseTb->courseid = $course;
					$groupCourseTb->groupid = $groupid;
					$groupCourseTb->timecreated = $time;
					$groupCourseTb->timemodified = $time;
					$groupCourseTb->is_active = 1;
					$groupCourseTb = $DB->insert_record('groups_course', $groupCourseTb);
				}else{
					if($groupCourse->is_active == 0){
						$mappingData = new stdClass;
						$mappingData->is_active = 1;
						$mappingData->id = $groupCourse->id;
						$groupCourseTb = $DB->update_record('groups_course',$mappingData);
					}
				}
				/*
				// update group group - user - course data
				$groupUsers =  $DB->get_records_sql('SELECT gm.*,ra.roleid FROM mdl_groups_members as gm LEFT JOIN mdl_role_assignments as ra on ra.userid = gm.userid WHERE gm.groupid = '.$groupid);
				foreach($groupUsers as $user){
					$groupcourseId = assignProgramCourseToUsers(0,$course,$user->userid,'group',$groupid);
				}
				$enrolId = CheckCourseEnrolment($course);
				foreach($groupUsers as $user){
					$userEnrolId = enrolUserToCourse($enrolId,$user->userid,$course);
				}
				*/
			}
		}
	}

	/**
	 * This function add courses to user
	 * @global object
	 * @param int $userid user Id
	 * @param array $courses course Id array
	 * @return nothing
	 * assign course to user
	 * manage enrolments entries
	 */
	function assignCourseToUser($userid,$courses){
		global $DB,$USER,$CFG;
		if(!empty($courses)){
			$time = time();
			foreach($courses as $course){
				$userCourseId = assignProgramCourseToUsers(0,$course,$userid,'user',$userid);
				$enrolId = CheckCourseEnrolment($course);
				$userEnrolId = enrolUserToCourse($enrolId,$userid,$course);
			}
		}
	}
	/**
	 * This function add courses to department
	 * @global object
	 * @param int $departmentId department Id
	 * @param array $courses course Id array
	 * @return nothing
	 * assign course to department
	 * assign added course to department members
	 * manage enrolments entries
	 */
	function assignCourseToDepartment($departmentId,$courses,$assignToUsers = 1){
		global $DB,$USER,$CFG;
		if(!empty($courses)){
			$time = time();
			foreach($courses as $course){
				$departmentCourse = $DB->get_record('department_course',array('courseid'=>$course,'departmentid'=>$departmentId));
				if(empty($departmentCourse)){
					$groupCourseTb = new stdClass;
					$groupCourseTb->courseid = $course;
					$groupCourseTb->departmentid = $departmentId;
					$groupCourseTb->timecreated = $time;
					$groupCourseTb->timemodified = $time;
					$groupCourseTb->is_active = 1;
					$groupCourseTb = $DB->insert_record('department_course', $groupCourseTb);
				}else{
					if($departmentCourse->is_active == 0){
						$mappingData = new stdClass;
						$mappingData->is_active = 1;
						$mappingData->id = $departmentCourse->id;
						$groupCourseTb = $DB->update_record('department_course',$mappingData);
					}
				}
				/*if($assignToUsers == 1){
					$departmentUsers =  $DB->get_records('user',array('department'=>$departmentId));
					if(!empty($departmentUsers)){
						foreach($departmentUsers as $departmentUser){
							assignProgramCourseToUsers(0,$course,$departmentUser->id,'department',$departmentId);
						}
						$enrolId = CheckCourseEnrolment($course);
						foreach($departmentUsers as $departmentUser){
							enrolUserToCourse($enrolId,$departmentUser->id,$course);
						}
					}
				}*/
			}
		}
	}


	/**
	 * This function assign course to teams,departments,and individual user
	 * @global object
	 * @param array $data array consists of course id,array of team ids,array of department ids,array of user ids
	 * @return nothing
	 */
	function SaveCourseMappingData($data){
		global $DB;
		$courseIdArr = array($data['id']);
		$courseId = $data['id'];
		if(isset($data['add_department_course']) && !empty($data['add_department_course'])){
			$lastId = removeCourseFromDepartment(0,$courseIdArr);
			foreach($data['add_department_course'] as $department){
				assignCourseToDepartment($department,$courseIdArr);
				
			}
		}elseif(isset($data['add_department_course'])){
			$lastId = removeCourseFromDepartment(0,$courseIdArr);
		}
		if(isset($data['addteamcourses']) && !empty($data['addteamcourses'])){
			$lastId = removeCourseFromGroup(0,$courseIdArr);
			foreach($data['addteamcourses'] as $team){
				addCourseToGroup($team,$courseIdArr);
				
			}
		}else{
			$lastId = removeCourseFromGroup(0,$courseIdArr);
		}
		if(isset($data['addusercourse']) && !empty($data['addusercourse'])){
			$deactivated = deactivateUserMappingData(0,$courseId,0,'user',0);
			foreach($data['addusercourse'] as $user){
				assignCourseToUser($user,$courseIdArr);
			}
		}else{
			$deactivated = deactivateUserMappingData(0,$courseId,0,'user',0);
		}
	}
	/**
	 * This function deactivates all courses for that particular group
	 * @global object
	 * @param array $data array consists of course id,array of team ids,array of department ids,array of user ids
	 * @return nothing
	 */
	function deActivateAllCoursesForTeam($Id,$type,$userId = 0,$courseId){
		global $DB;
		if($type == 'group'){
			if($userId != 0){
				$teamRecords = $DB->get_records('user_course_mapping',array('typeid'=>$Id,'type'=>'group','courseid'=>$courseId,'userid'=>$userId));
			}else{
				$teamRecords = $DB->get_records('user_course_mapping',array('typeid'=>$Id,'type'=>'group','courseid'=>$courseId));
			}
		}elseif($type == 'user'){
			$teamRecords = $DB->get_records('user_course_mapping',array('userid'=>$userId,'type'=>'user','courseid'=>$courseId));
		}elseif($type == 'department'){
			$teamRecords = $DB->get_records('user_course_mapping',array('typeid'=>$Id,'type'=>'department','courseid'=>$courseId));
		}
		if(!empty($teamRecords)){
			foreach($teamRecords as $mapRecord){
				$userEnrolCourse = new stdClass;
				$userEnrolCourse->status = 0;
				$userEnrolCourse->id = $mapRecord->id;
				$last_id = $DB->update_record('user_course_mapping',$userEnrolCourse);
			}
		}
	}
	/**
	 * This function assigns all courses of a group to specific user
	 * @global object
	 * @param int $groupId is group id
	 * @param int $userId is user id
	 * @return nothing
	 */
	function assignGroupCourses($groupId, $userId){
		global $DB,$USER,$CFG;
		$time = time();
		$groupCourses = $DB->get_records('groups_course',array('groupid'=>$groupId));
		
		if(!empty($groupCourses)){
			foreach($groupCourses as $course){
				//$groupcourseId = assignProgramCourseToUsers(0,$course->courseid,$userId,'group',$groupId);
				//$enrolId = CheckCourseEnrolment($course->courseid);
				//$userEnrolId = enrolUserToCourse($enrolId,$userId,$course->courseid);
			}
		}
		return true;
	}
	
	
	/**
	 * This function will return list of badges and badge count also 
	 * @global object
	 * @param string $sort is sorting field
	 * @param string $dir is sorting order
	 * @param int $page current page
	 * @param int $perpage is number of records per page
	 * @param array $paramArray is search array
	 * @return int $count total no. of department for an user 
	 */
	function getBadgesList($sort='name', $dir='ASC', $page=1, $perpage=10,array $paramArray=null){
		global $DB, $CFG, $USER;
		$paramArray['key'] = addslashes($paramArray['key']);
		$searchString = '';
		$searchString = "AND ";
		if($paramArray['ch'] != '') {
			if($paramArray['ch'] == 'OTH')
				$searchString .= "(name REGEXP '^[^a-zA-Z]') AND";
			else
				$searchString .= "(name like '".$paramArray['ch']."%' OR name like '".ucwords($paramArray['ch'])."%') AND";
		}
		$searchKeyAll = true;
		if( ($paramArray['name']==1) || ($paramArray['des']==1) || ($paramArray['issuename']==1))
			$searchKeyAll = false;

		if($searchKeyAll && ($paramArray['key'] != '')) {
			$searchString .= " (name like '%".$paramArray['key']."%' OR strip_tags(description) like '%".$paramArray['key']."%' OR issuername like '%".$paramArray['key']."%') AND";
		} else if ($paramArray['key'] != '') {
			$searchKeyAllString.= "(";
			if( $paramArray['name']==1 )
				$searchKeyAllString .= "name like '%".$paramArray['key']."%' OR";
			if( $paramArray['des']==1 )
				$searchKeyAllString .= " strip_tags(description) like '%".$paramArray['key']."%' OR";
			if( $paramArray['issuename']==1 )
				$searchKeyAllString .= " issuername like '%".$paramArray['key']."%' OR";

			$searchKeyAllString = substr($searchKeyAllString, 0, -2);
			$searchKeyAllString .= ") AND";
			$searchString .= $searchKeyAllString;
		}
		$searchString .= " 1=1";

		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$searchString .= '';
				break;
			CASE $CFG->userTypeManager:
					$searchString .=	' AND usercreated = '.$USER->id;
				break;
		}
		if($sort == 'badgecount'){
			$limit = '';
			$sort = '';
			$department = $DB->get_record_sql("SELECT count(*) as badgecount FROM {$CFG->prefix}badge where 1=1 ".$searchString." order by name ASC");
			return $department->badgecount;
		}else{
			if ($sort) {
				$sort = $sort;
				$sort = " ORDER BY $sort $dir";
			}
			$page = $page - 1;
			$page = $page*$perpage;
			$limit = '';
			if($perpage != 0){
				$limit = "LIMIT $page,$perpage";
			}
			return $DB->get_records_sql("SELECT * FROM {$CFG->prefix}badge where 1=1 $searchString $sort $limit");
		}
	}
	
	
		
	function setCourseBadge($userid,$courseid){
		GLOBAL $DB,$USER,$CFG;
		$now = time();
		$badges = $DB->get_records('badge',array('courseid'=>$courseid));
		if(!empty($badges)){
			foreach($badges as $badge){
				$getassignedBadges = $DB->get_records('badge_issued',array('userid'=>$userid,'badgeid'=>$badge->id));
				if(!$getassignedBadges){
					$issued = new stdClass();
					$issued->badgeid = $badge->id;
					$issued->userid = $userid;
					$issued->uniquehash = sha1(rand() . $userid . $badge->id . $now);
					$issued->dateissued = $now;
					$issued->dateexpire = null;

					$issued->visible = 1;
					$result = $DB->insert_record('badge_issued', $issued, true);
				}
			}
		}
	}
	
	/**
	 * Send email to new user
	 *
	 * @param stdClass $user A {@link $USER} object
	 * @return bool Returns true if mail was sent OK and false if there was an error.
	 */
	function send_confirmation_email_to_user($user) {
		global $CFG;
		$site = get_site();
		$supportuser = core_user::get_support_user();

		$data = new stdClass();
		$data->firstname = fullname($user);
		$data->sitename  = format_string($site->fullname);
		$data->username  = $user->username;
		$data->password  = $user->newpassword;
		$data->admin     = generate_email_signoff();

		$subject = get_string('email_confirmationsubject_user', '', format_string($site->fullname));

		$username = urlencode($user->username);
		$username = str_replace('.', '%2E', $username); // Prevent problems with trailing dots.
		$data->link  = $CFG->wwwroot;
		$message     = get_string('email_confirmation_to_user', '', $data);
		$messagehtml = text_to_html(get_string('email_confirmation_to_user', '', $data), false, false, true);

		$user->mailformat = 1;  // Always send HTML version as well.

		// Directly email rather than using the messaging system to ensure its not routed to a popup or jabber.
		return email_to_user($user, $supportuser, $subject, $message, $messagehtml);
	}
	
	/**
	 * This function is using for checking a url is correct or not
	 * @param string $url url an image
	 * @return bool , if exist then true else false
	 */
	 
	
	 function isUrlExists($url) {
	   if(@file_get_contents($url,0,NULL,0,1)){return 1;}else{ return 0;}
	 } 
	   
	  /**
	 * This function is using for getting a course image
	 * @global object
	 * @param object $course object of an course
	 * @return html of course image, if exists otherwise it will return default image
	 */
	 
	 
	  function getCourseImage($course){
	  
	    global $DB, $CFG;
	  
	     if(is_array($course)){
		    $course =  (object)$course;
		 }

	     $courseImgURL = $CFG->courseDefaultImage;
		 $courseImgPath = '';
		 $courseName = '';
	     if(count($course) > 0){
		  
		    $courseId = $course->id;
			$courseName = $course->fullname;
	        $courseContextId = $DB->get_field('context','id',array('instanceid'=>$courseId,'contextlevel'=> 50));
			if($courseContextId){
			    
				$query = "Select id,contextid, component, filearea, filename from {$CFG->prefix}files where contextid = ".$courseContextId." and component = 'course' and filearea = 'overviewfiles' and filename !='' AND filename !='.'";
				$courseImage = $DB->get_record_sql($query);
				if(count($courseImage) > 0 && $courseImage->contextid && $courseImage->component  && $courseImage->filearea && $courseImage->filename){
					$courseImgPath = $CFG->wwwroot.'/pluginfile.php/'.$courseImage->contextid.'/'.$courseImage->component.'/'.$courseImage->filearea.'/'.$courseImage->filename;
					if(isUrlExists($courseImgPath)){
					  $courseImgURL = $courseImgPath;
					}
				}
			}

	    }
		
		
		$courseImg = '<img src="'.$courseImgURL.'" alt="'.$courseName.'" title="'.$courseName.'" border="0" />';
	
		
		return $courseImg;
			
	  
	  }
	  
	 /*
		This function will return list of users
		*param array $userRole is the role array
		*param int $departmentId is the department id
	 */
	function getUsersList($userRole, $departmentId = 0){
		global $DB,$USER,$CFG;
		if(empty($userRole)){
			$userRole = $CFG->userTypeInSystem;
		}
		$roleIN = array();
		$departmentCheck = '';

		$roleCheck = getRolesToShow($userRole);
		if($departmentId != 0){
			//$departmentCheck = ' AND u.department = '.$departmentId;
			$departmentCheck = ' AND mdm.departmentid in ('.$departmentId.')';
		}
		$departmentManagers = $DB->get_records_sql("SELECT u.id,u.firstname,u.lastname
													FROM mdl_user AS u
													LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id
													LEFT JOIN mdl_role AS r ON r.id = ra.roleid
													LEFT JOIN mdl_department_members AS mdm ON u.id = mdm.userid
													WHERE u.deleted = 0 AND u.suspended = 0".$roleCheck.$departmentCheck);
		return $departmentManagers;
	}	
	function getRolesToShow($userRole){
		global $DB,$CFG;
		$roleCheck = '';
		$roleIN = array();
		if(in_array($CFG->userTypeAdmin,$userRole)){
			$roleIN[] =  $DB->get_field_sql("SELECT id FROM mdl_role WHERE name = '".$CFG->userTypeAdmin."'");
		}
		if(in_array($CFG->userTypeManager,$userRole)){
			$roleIN[] =  $DB->get_field_sql("SELECT id FROM mdl_role WHERE name = '".$CFG->userTypeManager."'");
		}
		if(in_array($CFG->userTypeStudent,$userRole)){
			$roleIN[] =  $DB->get_field_sql("SELECT id FROM mdl_role WHERE name = '".$CFG->userTypeStudent."'");
		}
		if(!empty($roleIN)){
			$roleCheck = ' AND ra.roleid IN('.implode(',',$roleIN).')';
		}
		return $roleCheck;
	}
	function getUserRoleDetails($userID = 0){
		global $USER,$DB;
		if($userID == 0 || $userID == '' || $userID == -1){
			$userID = $USER->id;
		}
		$userRoleDetail = $DB->get_record_sql("SELECT ra.roleid,u.id,r.name
													FROM mdl_user AS u
													LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id
													LEFT JOIN mdl_role AS r ON r.id = ra.roleid
													WHERE u.id = ".$userID);
		return $userRoleDetail;
	}
	function generatePageHeader(){
		global $USER,$DB,$CFG;
		$where = '';
		switch($USER->archetype){
			CASE $CFG->userTypeAdmin:
					$where = ' AND is_super = 1';
				break;
			CASE $CFG->userTypeManager:
					$where = ' AND is_manager = 1';
				break;
			CASE $CFG->userTypeStudent:
					$where = ' AND is_learner = 1';
				break;
			default:
					$where = ' AND is_super = 1';
				break;
		}
		$parentPages = $DB->get_records_sql("SELECT * FROM {$CFG->prefix}header_navigation where parent = 0 AND status = 1 $where ORDER BY display_order ASC");
		$headerHtml = '<ul class="nav" >';
		foreach($parentPages as $page){
			if($page->path == 'javascript:void(0);' || $page->path == '' || $page->path == '#'){
				$ulUrl = 'javascript:void(0);';
			}else{
				$ulUrl = $CFG->wwwroot.$page->path;
			}
			$childPages = $DB->get_records_sql("SELECT * FROM {$CFG->prefix}header_navigation where status = 1 AND parent = '".$page->id."' $where ORDER BY display_order ASC");
			$dropdownClass = '';
			$headerChildHtml = '';
			$dropdownToggleClass = '';
			if($childPages){
				$dropdownClass = 'dropdown';
				$dropdownToggleClass = 'data-toggle="dropdown"';
				$headerChildHtml .= '<ul class="dropdown-menu">';
					foreach($childPages as $childPage){
						$childTitle = ucwords($childPage->title);
						if($childPage->path == 'javascript:void(0);' || $childPage->path == '' || $childPage->path == '#'){
							$childPageUrl = 'javascript:void(0);';
						}else{
							$childPageUrl = $CFG->wwwroot.$childPage->path;
						}
						$headerChildHtml .= '<li><a href="'.$childPageUrl.'" title="'.$childTitle.'">'.$childTitle.'</a></li>';
					}
				$headerChildHtml .= '</ul>';
			}
			$parentTitle = ucwords($page->title);
			$headerHtmlStart = '<li class="'.$dropdownClass.'"><a class="'.$page->link_class.'" href="'.$ulUrl.'" title="'.$parentTitle.'" '.$dropdownToggleClass.'>'.$parentTitle.'<b class="caret"></b></a>';
			$headerHtmlEnd = '</li>';
			$headerHtml .= $headerHtmlStart.$headerChildHtml.$headerHtmlEnd;
		}
		$headerHtml .= '</ul>';
		return $headerHtml;
	}
	function getHeaderPages($sort = 'display_order', $dir='ASC', $page, $perpage,$paramArray){
		global $DB, $CFG;
		$searchString = "AND ";
		if(isset($paramArray['key'])){
			$paramArray['key'] = strtolower($paramArray['key']);
			$paramArray['key'] = addslashes($paramArray['key']);
		}
		if($paramArray['ch'] != '') {
			if($paramArray['ch'] == 'OTH')
				$searchString .= "(LOWER(title) REGEXP '^[^a-zA-Z]') AND";
			else
				$searchString .= "(LOWER(title) like '".$paramArray['ch']."%' OR LOWER(title) like '".ucwords($paramArray['ch'])."%') AND";
		}
		$searchKeyAll = true;
		if( ($paramArray['title']==1) )
			$searchKeyAll = false;

		if($searchKeyAll && ($paramArray['key'] != '')) {
			$searchString .= " (LOWER(title) like '%".$paramArray['key']."%') AND";
		} else if ($paramArray['key'] != '') {
			$searchKeyAllString .= "(";
			if( $paramArray['title']==1 )
				$searchKeyAllString .= " LOWER(title) like '%".$paramArray['key']."%' OR";

			$searchKeyAllString = substr($searchKeyAllString, 0, -2);
			$searchKeyAllString .= ") AND";
			$searchString .= $searchKeyAllString;
		}
		$searchString .= " 1=1";
		if($sort == 'cmscount'){
			$limit = '';
			$sort = '';
			$department = $DB->get_record_sql("SELECT count(*) as cmscount FROM {$CFG->prefix}header_navigation where 1=1 ".$searchString." order by display_order ASC");
			return $department->cmscount;
		}else{
			if ($sort) {
				$sort = $sort;
				$sort = " ORDER BY $sort $dir";
			}
			$page = $page - 1;
			$page = $page*$perpage;
			$limit = '';
			if($perpage != 0){
				$limit = "LIMIT $page,$perpage";
			}
			return $DB->get_records_sql("SELECT * FROM {$CFG->prefix}header_navigation where 1=1 $searchString $sort $limit");
		}
	}
	function checkUserAccess($type , $id){
	
		global $USER,$CFG,$DB;
		if($USER->archetype != $CFG->userTypeAdmin && $id != 0 && $id != -1){
			switch($type){
				CASE 'course':
					$courseCreatedBy = $DB->get_record_sql("SELECT id,createdby FROM  mdl_course WHERE id =".$id);
					if($courseCreatedBy->createdby != $USER->id){
						redirect($CFG->wwwroot.'/course/index.php');
					}
					break;
				CASE 'user':
					$departmentUserSelect =	" AND u.id IN(SELECT DISTINCT(u.id)
															FROM mdl_user AS u
															LEFT JOIN mdl_department_members AS dm ON dm.userid = u.id
															WHERE dm.departmentid IN (
															SELECT departmentid
															FROM mdl_department_members
															WHERE userid = $USER->id) OR u.createdby = $USER->id) AND r.name = 'learner'";
					$userArray = $DB->get_record_sql("SELECT u.id,r.name
															FROM mdl_user AS u
															LEFT JOIN mdl_role_assignments as ra ON ra.userid = u.id
															LEFT JOIN mdl_role as r on r.id = ra.roleid
															WHERE u.id = '".$id."' $departmentUserSelect");
					if(empty($userArray)){
						redirect($CFG->wwwroot.'/admin/user.php');
					}
					break;
				CASE 'messages':
					$userArray = $DB->get_record_sql("select id from mdl_my_messages where createdby ='".$USER->id."' and id='".$id."'");
					if(empty($userArray)){
						redirect($CFG->wwwroot.'/messages/index.php');
					}
					break;
				CASE 'badges':
					$userArray = $DB->get_record_sql("select id from mdl_badge where usercreated ='".$USER->id."' and id='".$id."'");
					if(empty($userArray)){
						redirect($CFG->wwwroot.'/badges/managebadges.php');
					}
					break;
			}
		}
	}
	/************************* Assign Course to user starts *********************************/
	/**
		** This function will return course enrollment id
		**@param int $courseId course id
		** If course is enrolled than returns enrollment id else make course enrollment entry in enrollment table and retrun enrollment id.
	**/
	function CheckCourseEnrolment($courseId){
		global $CFG,$DB,$USER;
		$time = time();
		$enrolledOrNot = $DB->get_record('enrol',array('courseid'=>$courseId,'enrol'=>'manual'));
		if($enrolledOrNot){
			$enrolId = $enrolledOrNot->id;
		}else{
			$userRole =  $DB->get_record_sql('SELECT roleid FROM mdl_role_assignments WHERE contextid = 1 AND userid = '.$USER->id);
			$EnrolCourse = new stdClass;
			$EnrolCourse->enrol = 'manual';
			$EnrolCourse->status = 0;
			$EnrolCourse->courseid = $courseId;
			$EnrolCourse->sortorder = 0;
			$EnrolCourse->expirythreshold = 86400;
			$EnrolCourse->roleid = $userRole->roleid;
			$EnrolCourse->timecreated = $time;
			$EnrolCourse->timemodified = $time;
			$enrolId = $DB->insert_record('enrol', $EnrolCourse);
		}
		return $enrolId;
	}
	/**
		** This function will enrol user to course
		**@param int $enrolId enrolment id
		**@param int $userId user id
		**@param int $courseId course id
		** If user is enrolled for that course than returns user enrollment id else make user-course enrollment entry in user enrollment table and retrun user enrollment id.
	**/
	function enrolUserToCourse($enrolId,$userId,$courseId){
		GLOBAL $CFG,$DB,$USER;
		$time = time();
		$userEnrolId = 0;
		$enrolCondition = array('enrolid'=>$enrolId,'userid'=>$userId);
		$userEnrolledOrNot = $DB->get_record('user_enrolments',$enrolCondition);
		if(empty($userEnrolledOrNot)){
			$courseData =  $DB->get_record_sql('SELECT id,startdate FROM mdl_course WHERE id = '.$courseId);
			$userEnrolCourse = new stdClass;
			$userEnrolCourse->status = 0;
			$userEnrolCourse->enrolid = $enrolId;
			$userEnrolCourse->userid = $userId;
			$userEnrolCourse->timestart = $courseData->startdate;
			$userEnrolCourse->timeend = 0;
			$userEnrolCourse->modifierid = $USER->id;
			$userEnrolCourse->expirythreshold = 86400;
			$userEnrolCourse->timecreated = $time;
			$userEnrolCourse->timemodified = $time;
			$userEnrolId = $DB->insert_record('user_enrolments', $userEnrolCourse);
		}
		return $userEnrolId;
	}

	/**
		** This function will make user course mapping entry
		**@param int $refId program reference id
		**@param int $courseId course id
		**@param int $userId user id
		**@param string $type type of assignment
		**@param int $typeId type id (for team - teamId, for department- departmentId, for user - userId)
		** If user is enrolled for that course than returns user enrollment id else make user-course enrollment entry in user enrollment table and retrun user enrollment id.
	**/
	function assignProgramCourseToUsers($refId,$courseId,$userId,$type,$typeId){
		global $CFG,$DB,$USER;
		$mapCondition = array('courseid'=>$courseId, 'program_user_ref_id'=>$refId, 'typeid'=>$typeId, 'userid'=>$userId, 'type' => $type);
		$mappingExists = $DB->get_record('user_course_mapping',$mapCondition);
		if(empty($mappingExists)){
			$groupCourse = new stdClass;
			$groupCourse->courseid = $courseId;
			$groupCourse->program_user_ref_id = $refId;
			$groupCourse->typeid = $typeId;
			$groupCourse->type = $type;
			$groupCourse->userid = $userId;
			$groupCourse->status = 1;
			$groupCourse->created_on = time();
			$groupCourse->updated_on = time();
			$groupcourseId = $DB->insert_record('user_course_mapping', $groupCourse);
		}else{
			if($mappingExists->status == 0){
				$groupCourse = new stdClass;
				$groupCourse->status = 1;
				$groupCourse->id = $mappingExists->id;
				$groupCourse->updated_on = time();
				$last_id = $DB->update_record('user_course_mapping',$groupCourse);
			}
		}
	}
	/***************************** Assign Course to user ends*******************************************/

	/**************** Unassign Process of course from department / team / users ************************/
	function deactivateUserMappingData($refId,$courseId,$userId,$type,$typeId){
		GLOBAL $DB;
		$mapCondition = array('type' => $type);
		if($refId){
			$mapCondition['program_user_ref_id'] = $refId;
		}
		if($courseId){
			$mapCondition['courseid'] = $courseId;
		}
		if($userId){
			$mapCondition['userid'] = $userId;
		}
		if($typeId){
			$mapCondition['typeid'] = $typeId;
		}
		$records = $DB->get_records('user_course_mapping', $mapCondition);
		if(!empty($records)){
			foreach($records as $record){
				$userEnrolCourse = new stdClass;
				$userEnrolCourse->status = 0;
				$userEnrolCourse->id = $record->id;
				$last_id = $DB->update_record('user_course_mapping',$userEnrolCourse);
			}
		}
	}
	/**
	 * This function removes courses from user
	 * @global object
	 * @param int $userid user Id
	 * @param array $courses course Id array
	 * @return nothing
	 */
	function removeCourseFromUser($userid,$courses){
		global $DB,$USER,$CFG;
		if(!empty($courses)){
			$time = time();
			foreach($courses as $course){
				$last_id = deactivateUserMappingData(0,$course,$userid,'user',$userid);
			}
		}
	}

	/**
	 * This function removes courses to group
	 * @global object
	 * @param int $groupid group Id
	 * @param array $courses course Id array
	 * @return nothing
	 */
	function removeCourseFromGroup($groupid,$courses){
		global $DB,$USER,$CFG;
		if(!empty($courses)){
			$time = time();
			/*foreach($courses as $course){
				if($groupid != 0){
					$last_id = deactivateUserMappingData(0,$course,0,'group',$groupid);
				}else{
					$last_id = deactivateUserMappingData(0,$course,0,'group');
				}
			}*/
			foreach($courses as $course){
				if($groupid != 0){
					$sqlCondition = array("courseid" => $course,"groupid" => $groupid);
				}else{
					$sqlCondition = array("courseid" => $course);
				}
				$records = $DB->get_records('groups_course', $sqlCondition);
				if(!empty($records)){
					foreach($records as $record){
						$mappingData = new stdClass;
						$mappingData->is_active = 0;
						$mappingData->id = $record->id;
						$last_id = $DB->update_record('groups_course',$mappingData);
					}
				}
			}
		}
	}

	/**
	 * This function removes courses to department
	 * @global object
	 * @param int $departmentId department Id
	 * @param array $courses course Id array
	 * @return nothing
	 */
	function removeCourseFromDepartment($departmentId,$courses){
		global $DB,$USER,$CFG;
		if(!empty($courses)){
			$time = time();
			/*foreach($courses as $course){
				if($departmentId != 0){
					$last_id = deactivateUserMappingData(0,$course,0,'department',$departmentId);
				}else{
					$last_id = deactivateUserMappingData(0,$course,0,'department');
				}
			}*/
			foreach($courses as $course){
				if($departmentId != 0){
					$sqlCondition = array("courseid" => $course,"departmentid" => $departmentId);
				}else{
					$sqlCondition = array("courseid" => $course);
				}
				$records = $DB->get_records('department_course', $sqlCondition);
				if(!empty($records)){
					foreach($records as $record){
						$mappingData = new stdClass;
						$mappingData->is_active = 0;
						$mappingData->id = $record->id;
						$last_id = $DB->update_record('department_course',$mappingData);
					}
				}
			}
		}
	}
	/************** Unassign Process of course from department / team / users ends**********************/
	function getFileClass($ext){
		if(preg_match("/^mp3|MP3|mpeg|MPEG|m3u|M3U$/",$ext)){
			return 'audio';
		}
		if(preg_match("/^avi|AVI|wmv|WMV|flv|FLV|mpg|MPG|mp4|MP4$/",$ext)){
			return 'video';
		}
		if(preg_match("/^gif|jpg|jpeg|tiff|png|bmp$/",$ext)){
			return 'image';
		}
		if(preg_match("/^doc|docx$/",$ext)){
			return 'doc';
		}
		if(preg_match("/^csv|xls|xlsx$/",$ext)){
			return 'csv';
		}
		if(preg_match("/^pdf$/",$ext)){
			return 'pdf';
		}
		if(preg_match("/^ppt|pptx$/",$ext)){
			return 'ppt';
		}
		if(preg_match("/^print$/",$ext)){
			return 'print';
		}
		if(preg_match("/^txt$/",$ext)){
			return 'txt';
		}
		return 'default';
	}
	function get_learning_list($sort,$type,$page,$perpage,$selectionType = 'all'){
	Global $DB,$CFG,$USER;
	if($perpage != 0) {
		$offset = ($page - 1) * $perpage;
		$courseQuery .= " LIMIT " . $offset . ", " . $perpage;
	}
	$where = ' WHERE user_id = '. $USER->id;
	$where .= " AND (CONCAT(stat_scorm,' ',stat_resource) != ' ')";
	if($type == 1){
		$where .= " AND (CONCAT(stat_scorm,' ',stat_resource) != 'completed ' AND CONCAT(stat_scorm,' ',stat_resource) != ' completed' AND CONCAT(stat_scorm,' ',stat_resource) != 'completed completed')";
	}elseif($type == 2){
		$where .= " AND (CONCAT(stat_scorm,' ',stat_resource) = 'completed ' || CONCAT(stat_scorm,' ',stat_resource) = ' completed' || CONCAT(stat_scorm,' ',stat_resource) = 'completed completed')";
	}else{
		$where .= " AND (CONCAT(stat_scorm,' ',stat_resource) = ' ' || CONCAT(stat_scorm,' ',stat_resource) = 'not started ' || CONCAT(stat_scorm,' ',stat_resource) = ' not started' || CONCAT(stat_scorm,' ',stat_resource) = 'not started not started') ";
	}
	if($selectionType !='all'){
		//$where .= " AND program_id = 0";
	}
	if($sort == 'count'){
		$fields = 'count(*) as listcount';
		$courseProgramList = $DB->get_record_sql("SELECT ".$fields." FROM vw_user_complete_learning_data ".$where);
		return $courseProgramList->listcount;
	}else{
		$fields = '*';
		$courseProgramList = $DB->get_records_sql("SELECT ".$fields." FROM vw_user_complete_learning_data ".$where.$courseQuery);
		return $courseProgramList;
	}
}
function getActiveCourses($courseId = 0){
	GLOBAL $DB;
	$where = "";
	if($courseId != 0){
		$where = " AND c.id = ".$courseId;
	}
	$activeIds = $DB->get_records_sql("SELECT DISTINCT(c.id)
										FROM mdl_course AS c
										LEFT JOIN mdl_course_modules as cm ON cm.course = c.id
										LEFT JOIN 
											( SELECT * FROM
												(
													(SELECT CONCAT(CONVERT(s.id,char),'_','18') as id,s.course,s.is_active FROM mdl_scorm as s WHERE s.is_active = 1)
														UNION 
													(SELECT CONCAT(CONVERT(r.id,char),'_','17') as id,r.course,r.is_active FROM mdl_resource as r WHERE r.is_active = 1)
												) 
												as srm
											) as sr ON sr.id = CONCAT(CONVERT(cm.instance,char),'_',cm.module)
										WHERE c.id != 1 AND c.deleted = 0 AND c.is_active = 1 AND cm.id IS NOT NULL AND cm.module != 9 AND sr.is_active = 1".$where);
	$ids = array();
	if(!empty($activeIds)){
		foreach($activeIds as $active){
			$ids[] = $active->id;
		}
		return  implode(',',$ids);
	}
	return '';
}
function getActiveCourseCount($courseId){
	GLOBAL $DB;
	$activeIds = $DB->get_record_sql("SELECT c.id,count(sr.id) as cnt,c.fullname
										FROM mdl_course AS c
										LEFT JOIN mdl_course_modules as cm ON cm.course = c.id
										LEFT JOIN 
											( SELECT * FROM
												(
													(SELECT CONCAT(CONVERT(s.id,char),'_','18') as id,s.course,s.is_active FROM mdl_scorm as s WHERE s.is_active = 1)
														UNION 
													(SELECT CONCAT(CONVERT(r.id,char),'_','17') as id,r.course,r.is_active FROM mdl_resource as r WHERE r.is_active = 1)
												) 
												as srm
											) as sr ON sr.id = CONCAT(CONVERT(cm.instance,char),'_',cm.module)
										WHERE c.id != 1 AND c.deleted = 0 AND c.is_active = 1 AND cm.id IS NOT NULL AND cm.module != 9 AND sr.is_active = 1 AND c.id = ".$courseId."
										GROUP BY c.id");
	if(!empty($activeIds) && $activeIds->cnt > 1){
		return true;
	}
	return false;
}
function getDepartmentManagerAdmin($departmentID = 0){
	GLOBAL $DB,$USER,$CFG;
	$addQuery = '';
	if($departmentID != '' || $departmentID != 0){
		$addQuery = "UNION (
				SELECT u.id,u.firstname,u.lastname,r.name
				FROM mdl_user AS u
				LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id
				LEFT JOIN mdl_role AS r ON r.id = ra.roleid
				LEFT JOIN mdl_department_members AS mdm ON u.id = mdm.userid
				WHERE u.deleted = 0 AND u.suspended = 0 AND ra.roleid IN(3) AND mdm.departmentid IN ($departmentID))";
	}
	$query = "	SELECT DISTINCT(c.id),c.firstname,c.lastname FROM ((
				SELECT u.id,u.firstname,u.lastname,r.name
				FROM mdl_user AS u
				LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id
				LEFT JOIN mdl_role AS r ON r.id = ra.roleid
				WHERE u.deleted = 0 AND u.suspended = 0 AND ra.roleid IN(1))$addQuery) as c";
	$userList = $DB->get_records_sql($query);
	return $userList;
}
function getDepartmentTeams($departmentId){
	global $DB,$USER,$CFG;
	$departmentTeams = array();
	if($departmentId && $departmentId != 0){
		$departmentTeams = $DB->get_records_sql("SELECT g.id,g.name FROM mdl_groups AS g LEFT JOIN mdl_group_department AS gd ON gd.team_id = g.id WHERE gd.department_id = ".$departmentId);
	}
	return $departmentTeams;
}
function publishCourse($courseId,$publish){
	global $DB,$USER,$CFG;
	$data = new stdClass();
	$data->id		= $courseId;
	$data->publish	= $publish;
	$DB->update_record('course', $data);
}

function getGMDate($ts = null){ 
	$k = array('seconds','minutes','hours','mday', 
			'wday','mon','year','yday','weekday','month',0); 
	return(array_combine($k,split(":", 
			gmdate('s:i:G:j:w:n:Y:z:D:M:U',is_null($ts)?time():$ts)))); 
} 
?>
