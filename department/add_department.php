<?php
	require_once('../config.php');
	require_once($CFG->pageAddDepartmentForm);
	require_once($CFG->pageDepartmentlib);
	
	global $USER,$CFG;

	require_login();

	$context = get_context_instance(CONTEXT_SYSTEM);

	if($USER->archetype == $CFG->userTypeManager || $USER->archetype == $CFG->userTypeStudent)// if user is not manager redirect to home page.
	{
		redirect($CFG->wwwroot."/");
	}
	
	$header = "$SITE->shortname";	
	
	//Start Editor options
	$editorOptions = array('maxfiles' => EDITOR_UNLIMITED_FILES, 'maxbytes'=>$CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true);	
	//End Editor options

	$id = optional_param('id', 0, PARAM_INT);
	$returnto = optional_param('returnto', 0, PARAM_ALPHANUM);

	// Start setting up the page
	$params = array();
	$PAGE->set_context($context);
	$PAGE->set_url('/department/index.php', $params);
	$PAGE->set_pagelayout('general');
	if($id==0){
		$navBarString = get_string('adddepartment','department');
	}else{
		$navBarString = get_string('edit_department','department');
	}
	$header .= ': '.$navBarString;
	$PAGE->set_title($header);
	$PAGE->set_heading($header);
	$PAGE->navbar->add(get_string('browse_department','department'), new moodle_url('/department/index.php'));
	$PAGE->navbar->add($navBarString);
	// End setting up the page


	//$addDepartmentForm = new adddepartment_form($CFG->pageAddDepartment,$adddepartmentdata);

	$returnUrl=$CFG->wwwroot."/department/index.php";
	

	// Start Add department form
	if($id != 0){
		//Start edit department form data
		$select = "SELECT * FROM {$CFG->prefix}department WHERE id = $id";
		$get_department = $DB->get_record_sql($select);
		/* if($get_department->auth==$CFG->authLdap){
			redirect($CFG->wwwroot.'/department/index.php');
		} */
		$adddepartmentdata = new stdClass();
		$adddepartmentdata->title = $get_department->title;
		$adddepartmentdata->description = $get_department->description;
		$adddepartmentdata->picture = $get_department->picture;
		$adddepartmentdata->createdby = $get_department->createdby;
		$adddepartmentdata->id = $id;
		$adddepartmentdata->timecreated = $get_department->timecreated;
		$adddepartmentdata->is_external = $get_department->is_external;
		
		$attachmentoptions = array('subdirs'=>false, 'maxfiles'=>99, 'maxbytes'=>$maxbytes);
		$adddepartmentdata->editoroptions=$editorOptions;
		$adddepartmentdata->returnto=$returnto;
		$adddepartmentdata->attachmentoptions=$attachmentoptions;
		$adddepartmentdata->auth=$get_department->auth;
		//End edit department form data

		// Start Set Add department form data
		$addDepartmentForm = new adddepartment_form($CFG->pageAddDepartment,$adddepartmentdata);

		$contextLevel = 150; 
		$query = "SELECT * FROM {$CFG->prefix}context WHERE contextlevel = '".$contextLevel."' AND instanceid = '".$id."'";
		$programContext = $DB->get_record_sql($query);
		$program->id = $programContext->id;
		if ($filesOptions = programOverviewFilesOptions($program)) {
		   file_prepare_standard_filemanager($program, 'testimage', $filesOptions, $programContext, 'test', 'testimage', 0);
		}
		$fileData = new stdClass();
		$fileData->testimage_filemanager = $program->testimage_filemanager;
		$addDepartmentForm->set_data($fileData);
		// End Set Add department form data
	}else{
	
	    
		if($CFG->canAddDepartment == 0){
		  redirect($returnUrl);
		}
	
		// Start Add form and Set Add department form data
		$attachmentoptions = array('subdirs'=>false, 'maxfiles'=>99, 'maxbytes'=>$maxbytes);
		$addDepartmentForm = new adddepartment_form(NULL, array('editoroptions'=>$editorOptions, 'returnto'=>$returnto, 'attachmentoptions'=>$attachmentoptions));
		// End Add form and Set Add department form data
	}
	// End Set Add department form data

	// Form Submit or cancel Event
	if($addDepartmentForm->is_cancelled()) {	// If form cancelled, return to listing page
		redirect($returnUrl);
    }else if($departmentData= $addDepartmentForm->get_data()){		//If submit clicked
		// Update department data
		if($departmentData->id!=''){
			$nid=update_department($departmentData);
			$_SESSION['update_msg'] = get_string('record_updated');
			redirect($returnUrl);	
		}else{
			// Insert department data
			$nid=insert_department($departmentData);
			$_SESSION['update_msg'] = get_string('record_added');
			redirect($returnUrl);	
		}		
	}
	//Print header
	echo $OUTPUT->header();	
	
	//Start Print form
	echo '<div class="left_content">
			<div class="category_section">';
	$addDepartmentForm->display(); 
	echo '</div></div>';
	//End print form

	// End Add department form
	/*Start Javascript to delete department image*/
?>
<script>
	$(document).ready(function(){
		$(".removeimage").click(function(){
			message = "<?php echo get_string('delete_image_alert_message')?>";
			var r = confirm(message);
			if (r == true) {
				var departmentId = $(this).attr('rel');
				$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
					type:'POST',
					data:'action=deleteDepartmentImage&departmentId='+departmentId,
					success:function(data){
						alert("<?php echo get_string('image_deleted_successfully');?>");
						window.location.reload(true);
					}
				});
			}else{
				return false;
			}
		});
		
		<?php if($CFG->canAddDepartment == 0){?>
		 $('#id_title').attr('readonly',true);
		<?php } ?>
		$(document).on('blur', '#id_title', function(){

			   var cVal = $(this).val();
			   cVal = $.trim(cVal);
			   $(this).val(cVal);


		});
		
		$(document).on("click", "#id_submitbutton", function(){

			   var id_name = $('#id_title').val();
			   id_name =  $.trim(id_name);
	
				if( id_name != '' ){  
					$(this.form).submit(function(){
					
						 $("#id_submitbutton").prop('disabled', 'disabled');
						 $('.disabled_user_form_fields').attr('disabled', false);
					});
				}
        });	


	});
</script>
<?php
/*End Javascript to delete department image*/

//Print footer
	echo $OUTPUT->footer();
?>