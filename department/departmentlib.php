<?php
/*
	To Perform department add and update query.
	Created By: Bhavana kadwal
	Created On : 27th June 2014
	Updated On : 17th July 2014
*/
	require_once('../config.php');		
	require_once($CFG->libdir."/dmllib.php");
	require_once($CFG->libdir."/deprecatedlib.php");
	require_once($CFG->libdir."/accesslib.php");
	
	/*
		*This function is used to Insert department Data in database.
		*@param obj $departmentData -> Form Data
		*return int insert id
	*/
	function insert_department($departmentData)	{ //Insert Department Data
    	global $USER, $DB, $CFG;
		$time = time();
		$data = new stdClass();
		$data->createdby=$USER->id;
		$data->title = $departmentData->title;
		$data->description = $departmentData->description['text'];
		$data->deleted=0;
		$data->status=1;
		$data->timecreated = $time;
		$data->timemodified = $time;
		$data->display_order = 1;
		if($CFG->allowExternalDepartment == 1){
		  $data->is_external = isset($departmentData->is_external)?1:0;
		}
		//Start Upload department image
		if($_FILES["department_image"]["error"] == 0){
			$path = $CFG->dirroot.'/theme/gourmet/pix/department/'; 
			if(!is_dir($path)){
				mkdir($path,0777);
				chmod($path,0777);
			}
			$imageName = $time.'_'.$_FILES["department_image"]["name"];
			if(move_uploaded_file($_FILES["department_image"]["tmp_name"], $path.$imageName)){
				$data->picture = $imageName;
			}
		}
		//End Upload department image
		$data->id = $DB->insert_record('department', $data, true);

		$maxContextId = $DB->get_field_sql("SELECT max(id) FROM {$CFG->prefix}context");
		$contextLevel = 150;
		$depth = 3;
		$maxContextId = $maxContextId + 1;
		$contextPath = "/1/".$depth."/".$maxContextId;
		$component = 'test';

		$query = "INSERT INTO {$CFG->prefix}context set contextlevel = '".$contextLevel."' , instanceid = '".$data->id."', path = '".$contextPath."', depth = '".$depth."'";
		executeSql($query);
			
		$programId = $data->id;
		$contextLevel = 150; 
	    $query = "SELECT * FROM {$CFG->prefix}context WHERE contextlevel = '".$contextLevel."' AND instanceid = '".$programId."' ";
	    $contextProgram = $DB->get_record_sql($query);
		  
		if ($filesOptions = programOverviewFilesOptions($programId)) {
		  $departmentData = file_postupdate_standard_filemanager($departmentData, 'testimage', $filesOptions, $contextProgram, $component, 'testimage', 0);
		}

		return $data->id;
	}
	/*
		*This function is used to Update department Data in database.
		*@param obj $departmentData -> Form Data
		*return int update id
	*/
	function update_department($departmentData)	{
		global $USER, $DB, $CFG;
		$time = time();
		$data = new stdClass();
		$primaryManagerData = $DB->get_records_sql("SELECT u.id,u.firstname,u.lastname
											FROM mdl_user AS u
											LEFT JOIN mdl_department_members AS dm ON dm.userid = u.id
											WHERE dm.is_active = 1 AND u.deleted = 0 AND u.is_primary = 1 AND dm.departmentid = $departmentData->id");
											
										
		if(!empty($primaryManagerData)){
			foreach($primaryManagerData as $primaryOld){
				$oldPrimary = new stdClass();
				$oldPrimary->id			= $primaryOld->id;
				$oldPrimary->is_primary = 0;
				$updatedOld = $DB->update_record('user', $oldPrimary);
			}
		}
		if($departmentData->primary_manager != 0){
			$newPrimary				= new stdClass();
			$newPrimary->id			= $departmentData->primary_manager;
			$newPrimary->is_primary = 1;
			$updatedOld				= $DB->update_record('user', $newPrimary);
		}
		$data->createdby=$departmentData->createdby;
		$data->title = $departmentData->title;
		$data->timemodified = $time;
		$data->id = $departmentData->id;
		
		
		
		if(isset($departmentData->department_images) && $departmentData->department_images != '' && $departmentData->department_images != 0){
			$data->picture = $departmentData->department_images;
		}

		//Start Upload department image
		if(!empty($_FILES) && $_FILES["department_image"]["error"] == 0){
			$path = $CFG->dirroot.'/theme/gourmet/pix/department/'; 
			if(!is_dir($path)){
				mkdir($path,0777);
				chmod($path,0777);
			}
			$imageName = $time.'_'.$_FILES["department_image"]["name"];
			$imgDataExists = $DB->get_record_sql("select * from mdl_department where id = ".$data->id);
			if(!empty($imgDataExists)){
				$previousImane = $imgDataExists->picture;
				$oldPath = $path.$previousImane;
				if($previousImane != ''){
					if(file_exists($oldPath)){
						@unlink($oldPath);
					}
				}
				if(move_uploaded_file($_FILES["department_image"]["tmp_name"], $path.$imageName)){
					$data->picture = $imageName;
				}
			}else{
				if(move_uploaded_file($_FILES["department_image"]["tmp_name"], $path.$imageName)){
					$data->picture = $imageName;
				}
			}
		}
		$programId = $departmentData->id;
		$contextLevel = 150; 
	    $query = "SELECT * FROM {$CFG->prefix}context WHERE contextlevel = '".$contextLevel."' AND instanceid = '".$programId."' ";
	    $contextProgram = $DB->get_record_sql($query);
		if(empty($contextProgram)){
			$maxContextId = $DB->get_field_sql("SELECT max(id) FROM {$CFG->prefix}context");
			$contextLevel = 150;
			$depth = 3;
			$maxContextId = $maxContextId + 1;
			$contextPath = "/1/".$depth."/".$maxContextId;
			$component = 'test';

			$query = "INSERT INTO {$CFG->prefix}context set contextlevel = '".$contextLevel."' , instanceid = '".$data->id."', path = '".$contextPath."', depth = '".$depth."'";
			executeSql($query);
			 $query = "SELECT * FROM {$CFG->prefix}context WHERE contextlevel = '".$contextLevel."' AND instanceid = '".$programId."' ";
			$contextProgram = $DB->get_record_sql($query);
		}
		$component = 'test';
		if ($filesOptions = programOverviewFilesOptions($programId)) {
		     $data = file_postupdate_standard_filemanager($departmentData, 'testimage', $filesOptions, $contextProgram, $component, 'testimage', 0);
		}
		//End Upload department image
		if($CFG->allowExternalDepartment == 1){
		  $data->is_external = isset($departmentData->is_external)?1:0;
		}
		//pr($data);die;
		$data->description = $departmentData->description['text'];		
		$updateId = $DB->update_record('department', $data); 
		return $updateId;
	}	
	function programOverviewFilesOptions($program) {
		global $CFG, $DB;
		if (empty($CFG->programOverviewFilesLimit)) {
			return null;
		}

		$accepted_types = preg_split('/\s*,\s*/', trim($CFG->programOverviewFilesExt), -1, PREG_SPLIT_NO_EMPTY);
		if (in_array('*', $accepted_types) || empty($accepted_types)) {
			$accepted_types = '*';
		} else {
			// Since config for $CFG->programOverviewFilesExt is a text box, human factor must be considered.
			// Make sure extensions are prefixed with dot unless they are valid typegroups
			foreach ($accepted_types as $i => $type) {
				if (substr($type, 0, 1) !== '.') {
					require_once($CFG->libdir. '/filelib.php');
					if (!count(file_get_typegroup('extension', $type))) {
						// It does not start with dot and is not a valid typegroup, this is most likely extension.
						$accepted_types[$i] = '.'. $type;
						$corrected = true;
					}
				}
			}
			if (!empty($corrected)) {
				set_config('programOverviewFilesExt', join(',', $accepted_types));
			}
		}
		$options = array(
			'maxfiles' => $CFG->programOverviewFilesLimit,
			'maxbytes' => $CFG->maxbytes,
			'subdirs' => 0,
			'accepted_types' => $accepted_types
		);
		if (!empty($program->id)) {
		
		  $contextLevel = 150; 
		  $query = "SELECT * FROM {$CFG->prefix}context WHERE contextlevel = '".$contextLevel."' AND instanceid = '".$program->id."' ";
		  $context = $DB->get_record_sql($query);
		  $options['context'] = $context;
		  
		  
		} else if (is_int($program) && $program > 0) {
		  
		  $contextLevel = 150; 
		  $query = "SELECT * FROM {$CFG->prefix}context WHERE contextlevel = '".$contextLevel."' AND instanceid = '".$program."' ";
		  $context = $DB->get_record_sql($query);
		  $options['context'] = $context;
		}
		 return $options;
	
   }
?>