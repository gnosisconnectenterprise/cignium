<?php
	require_once('../config.php');
	require_once($CFG->pageDepartmentlib);
	
	global $USER,$CFG;
	
	//if userid not available, ask user to login
	require_login();

	$context = get_context_instance(CONTEXT_SYSTEM);

	
	if($USER->archetype != $CFG->userTypeAdmin) // if user is not manager redirect to home page.
	{
		redirect($CFG->wwwroot."/");
	}
	
	// Start setting up the page
	$params = array();
	$PAGE->set_context($context);
	$PAGE->set_url('/department/index.php', $params);
	$PAGE->set_pagelayout('globaladmin');
	$PAGE->set_title($header);
	$PAGE->set_heading($header);
	$PAGE->navbar->add(get_string('browse_department','department'), new moodle_url('/department/index.php'));
	$PAGE->navbar->add(get_string('department_delete','department'));
	// End setting up the page

	$id = optional_param('id', 0, PARAM_INT); // Delete Id
	$action = optional_param('action', 0, PARAM_INT); // Delete Confirm
	if($id!=''){
		// get delete Data
		$select = "SELECT * FROM {$CFG->prefix}department where deleted='0'  and id='".$id."'";
		$get_department = $DB->get_record_sql($select);
		if($get_department=='')
		{
			redirect($CFG->wwwroot."/department/index.php");
		}
	}	

	//Start delete department data
	if($action=='1')
	{
		$DeleteData = new stdClass();
		$DeleteData->deleted=1;
		$DeleteData->id=$id;
		$UpdateId = $DB->update_record_raw('department', $DeleteData);
		$_SESSION['update_msg']=get_string('record_deleted');
		redirect($CFG->wwwroot."/department/index.php");
	}
	//End delete department data

	// Start Print delete Form
	$message = $get_department->title.'</br>';
	$message .= get_string("delete_msg");
	echo $OUTPUT->header();
	echo '<div class="box generalbox" id="notice">
			<p>'.$message.'</p>
			<div class="buttons">
				<div class="singlebutton">
					<form action="delete_department.php" method="post">
						<div>
							<input type="submit" value="Yes">
							<input type="hidden" value="'.$id.'" name="id">
							<input type="hidden" value="1" name="action">
						</div>
					</form>
				</div>
				<div class="singlebutton">
					<form action="index.php" method="post">
						<div><input type="submit" value="No"></div>
					</form>
				</div>
			</div>
		</div>';
	// End Print delete Form
	echo $OUTPUT->footer();
?>