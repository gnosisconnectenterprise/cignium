<?php
	require_once(dirname(__FILE__) . '/../config.php');
	require_once($CFG->dirroot . '/local/user/selector/lib.php');
	require_once($CFG->dirroot . '/course/lib.php');
	require_once($CFG->libdir . '/filelib.php');
	Global $USER;
	require_login();

	if($USER->archetype != $CFG->userTypeAdmin) // if user is not manager redirect to home page.
	{
		redirect($CFG->wwwroot."/");
	}

	$site = get_site();

	$departmentId = required_param('department', PARAM_INT);
	$cancel  = optional_param('cancel', false, PARAM_BOOL);

	$department = $DB->get_record('department', array('id'=>$departmentId), '*', MUST_EXIST);

	//Start Assignment Post Data
	if (optional_param('add', false, PARAM_BOOL) && confirm_sesskey()) {
		if(isset($_REQUEST['assignment_user'])){
			$assignToUsers = $_REQUEST['assignment_user'];
		}else{
			$assignToUsers = 0;
		}
		$_SESSION['assignToUsers'] = $assignToUsers;
		assignCourseToDepartment($departmentId,$_REQUEST['addcourse'],$assignToUsers);
		if(!empty($_REQUEST['addcourse']) && $CFG->courseenrolDeptMail == 1){
			$departmentManagers = getUsersList(array($CFG->userTypeManager),$departmentId);
			if(!empty($departmentManagers)){
				foreach($departmentManagers as $manager){
					departmentEnrolMailToCourses($manager->id,$_REQUEST['addcourse'],$departmentId,$department->title);
				}
			}else{
				add_to_log(1, 'department', 'add course to department', 'department/assigncourses.php?department='.$departmentId, "add course to department", 0, $USER->id);
			}
		}
		redirect($CFG->wwwroot."/department/".$CFG->pageDepartmentAssigncourses."?department=".$departmentId);
	}
	if (optional_param('remove', false, PARAM_BOOL) && confirm_sesskey()) {
		removeCourseFromDepartment($departmentId,$_REQUEST['removecourse']);
		if(!empty($_REQUEST['removecourse']) && $CFG->courseenrolDeptMail == 1){
			$departmentManagers = getUsersList(array($CFG->userTypeManager),$departmentId);
			if(!empty($departmentManagers)){
				foreach($departmentManagers as $manager){
					departmentCoursesUnenrolMail($_REQUEST['removecourse'],$manager->id,$departmentId,$department->title);
				}
			}else{
				add_to_log(1, 'department', 'add course to department', 'department/assigncourses.php?department='.$departmentId, "add course to department", 0, $USER->id);
			}
		}
		redirect($CFG->wwwroot."/department/".$CFG->pageDepartmentAssigncourses."?department=".$departmentId);
	}
	//End Assignment Post Data

	$context = context_system::instance();
	$returnUrl = $CFG->wwwroot.'/department/index.php';

	if ($cancel) {
		redirect($returnUrl);
	}
	//Start Setting Page data
	$PAGE->set_url('/department/'.$CFG->pageDepartmentAssigncourses, array('department'=>$departmentId));
	$PAGE->set_pagelayout('globaladmin');
	$PAGE->navbar->add(get_string('browse_department',"department"), new moodle_url('/department/index.php'));
	$PAGE->navbar->add(get_string('assign_courses', 'group'));
	$PAGE->set_title("$site->fullname: ".get_string('assign_courses', 'group'));
	$PAGE->set_heading($site->fullname);
	echo $OUTPUT->header();
	//End Setting Page data

	//Start Set Array of assigned and unassigned data
	$courses = new department_courses_selector('', array('departmentId' => $departmentId));
	$courses->get_non_department_courses();
	$courses->get_department_courses();
	//End Set Array of assigned and unassigned data

	$groupinforow = array();

	// Check if there is a picture to display.
	/*if($department->picture != ''){
		$view =  $CFG->dirroot.'/theme/gourmet/pix/department/'.$department->picture;
		$view1 =  $CFG->wwwroot.'/theme/gourmet/pix/department/'.$department->picture;
	}else{
		$view =  $CFG->dirroot.'/theme/gourmet/pix/department-img.jpg';
		$view1 =  $CFG->wwwroot.'/theme/gourmet/pix/department-img.jpg';
	}*/
	//Start Display picture
	$picturecell = new html_table_cell();
	$picturecell->attributes['class'] = 'left side picture';
	$picturecell->text ='<div class="felement fstatic">'.getDepartmentImage($department).'</div>';
	$groupinforow[] = $picturecell;
	//End Display picture

	// Check if there is a description to display.
	$department->description = file_rewrite_pluginfile_urls($department->description, 'pluginfile.php', $context->id, 'department', 'description', $department->id);
	if (!isset($department->descriptionformat)) {
		$department->descriptionformat = FORMAT_MOODLE;
	}

	$options = new stdClass;
	$options->overflowdiv = true;

	$contentcell = new html_table_cell();
	$contentcell->attributes['class'] = 'content';
	$contentcell->text = '<b>'.$department->title.'</b></br>';
	if($department->description != ''){
		$contentcell->text .= format_text($department->description, $department->descriptionformat, $options);
	}
	$groupinforow[] = $contentcell;

	// Check if we have something to show.
	if (!empty($groupinforow)) {
		$groupinfotable = new html_table();
		$groupinfotable->attributes['class'] = 'groupinfobox';
		$groupinfotable->data[] = new html_table_row($groupinforow);
		//echo html_writer::table($groupinfotable);
	}
	
	$headerHtml = getModuleHeaderHtml($department, $CFG->departmentModule);
    echo $headerHtml;

	if(isset($_SESSION['assignToUsers'])){
		if($_SESSION['assignToUsers'] == 1){
			$checkUSer = 'checked = "checked"';
		}else{
			$uncheckUSer = 'checked = "checked"';
		}
	}else{
		$checkUSer = 'checked = "checked"';
		$uncheckUSer = '';
	}
	/// Print the editing form
	?>
	<div id="addmembersform" class="borderBlock borderBlockSpace">
		<form id="assignform" method="post" action="<?php echo $CFG->wwwroot; ?>/department/<?php echo $CFG->pageDepartmentAssigncourses; ?>?department=<?php echo $departmentId; ?>">
		<!-- div class = "assignment-radio-select">
			<div><span><input type = "radio" name = "assignment_user" value = "0" <?php echo $uncheckUSer; ?>>Assign to department only</span></div>
			<div><span><input type = "radio" name = "assignment_user" value = "1" <?php echo $checkUSer; ?>>Assign to department's Users</span></div>
		</div -->
			<div>
				<input type="hidden" name="sesskey" value="<?php p(sesskey()); ?>" />
				<table class="assignTable">
        <tr>
          <td id='potentialcell' class="potentialcell">
						  <p><label for="addselect"><?php echo get_string('nonteamcourses','group');?></label></p>
						  <div id="addselect_wrapper" class="userselector">
							<select multiple="multiple" name = "addcourse[]" size = '20'>
								<?php 
									$courses->course_option_list(1);
									// This function will print courses options
								?>
							</select>
                            
                             <div class="search-noncourse-box searchBoxDiv" >
                                <div class="search-input" ><input type="text" value="" placeholder="Search" id="addselect_searchtext" name="addselect_searchtext" ></div>
                                <div class="search_clear_button"><input type="button" title="Search" id="addselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="addselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
                      
                            </div>
                            
						  </div>
						</td>
						<td id='buttonscell'>
							<div class="arrow_button">
								<input class="moveLeftButton" name="remove" id="remove" type="button" value="<?php echo get_string('remove').'&nbsp;'.$OUTPUT->rarrow(); ?>" title="<?php print_string('remove'); ?>" disabled = ""/><input class="moveRightButton" name="add" id="add" type="submit" value="<?php echo $OUTPUT->larrow().'&nbsp;'.get_string('add'); ?>" title="<?php print_string('add'); ?>" disabled = ""/>
							</div>
						</td>
						<td id='existingcell' class="potentialcell">
							  <p><label for="removeselect"><?php echo get_string('teamcourses','group');?></label></p>
							  <div id="removeselect_wrapper" class="userselector">
								<select multiple="multiple" name = "removecourse[]" size = '20'>
									<?php $courses->course_option_list(2); ?>
								</select>
                                
                                  <div class="search-course-box searchBoxDiv" >
                                        <div class="search-input" ><input type="text" value="" placeholder="Search" id="removeselect_searchtext" name="removeselect_searchtext" ></div>
                                        <div class="search_clear_button"><input type="button" title="Search" id="removeselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="removeselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
                              
                                  </div>
							 </div>
						</td>
					</tr>
					<tr><td colspan="3" id='backcell'>
						<input type="submit" name="cancel" style="margin-top:10px"  value="<?php print_string('back'); ?>" />
					</td></tr>
				</table>
			</div>
		</form>
	</div>
<a href="#end-date-div" id="inline-iframe" rel="inline-iframe" title="<?php print_string('allocation'); ?>">click</a>
<div class="end-date-div" id="end-date-div">
	<iframe src="" id="i-frame" width="100%" height="100%"></iframe>
</div>
<style>
#end-date-div{ display:none;}
#colorbox #end-date-div{display:block}
#inline-iframe{ position:absolute; left:-100000px;}
</style>

	<script>
	$(document).ready(function(){
		$("#inline-iframe").colorbox({inline:true, width:"675px", height:"468px"});
		var departmentId = "<?php echo $departmentId; ?>";
		//Unassigned course search
		$("#addselect_searchtext_btn").click(function(){
			var searchText = $('#addselect_searchtext').val();
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/group/searchcourses.php',
					type:'POST',
					data:'action=searchNonDepartmentCourse&departmentId=<?php echo $departmentId; ?>&search_text='+searchText,
					success:function(data){
						$('#addselect_wrapper').find('select').html(data);
					}
			});
		});

		//Assigned course search
		$("#addselect_clearbutton").click(function(){
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/group/searchcourses.php',
					type:'POST',
					data:'action=searchNonDepartmentCourse&departmentId=<?php echo $departmentId; ?>',
					success:function(data){
						$('#addselect_wrapper').find('select').html(data);
						$("#addselect_searchtext").val('');
					}
			});
		});

		//unassigned course clear search
		$("#removeselect_clearbutton").click(function(){
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/group/searchcourses.php',
					type:'POST',
					data:'action=searchDepartmentCourse&departmentId=<?php echo $departmentId; ?>',
					success:function(data){
						$('#removeselect_wrapper').find('select').html(data);
						$("#removeselect_searchtext").val('');
					}
			});
		});
		//assigned course clear search
		$("#removeselect_searchtext_btn").click(function(){
			var searchText = $('#removeselect_searchtext').val();
			$.ajax({
					url:'<?php echo $CFG->wwwroot;?>/local/group/searchcourses.php',
					type:'POST',
					data:'action=searchDepartmentCourse&departmentId=<?php echo $departmentId; ?>&search_text='+searchText,
					success:function(data){
						$('#removeselect_wrapper').find('select').html(data);
					}
			});
		});
		var fromCourse = $('#addselect_wrapper select');
		var toCourse = $('#removeselect_wrapper select');
		//enable add icon
		$(document).on("change", "#addselect_wrapper select",function(){
			$("#add").removeAttr("disabled");
		});
		//enable remove icon
		$(document).on("change", "#removeselect_wrapper select",function(){
			$("#remove").removeAttr("disabled");
		});
		$(document).on("click","#remove",function(){
			var courseList = '';
			toCourse.find('option:selected').each(function(){
				courseList += $(this).val()+',';
			});
			if(courseList == '' || courseList == ','){
				alert("<?php echo get_string('select_course_to_unenrol');?>");
				return false;
			}
			courseList = courseList.substr(0, courseList.length - 1);
			var source = '<?php echo $CFG->wwwroot;?>/course/unenrolcourse.php?element=course&assigntype=department&assignId='+departmentId+'&elementList='+courseList;
			$("#i-frame").attr('src',source);
			$(".cboxContent #end-date-div").attr("style",'display:block');
			$("#inline-iframe").trigger("click");

		});
	});
	</script>
	<?php
	echo $OUTPUT->footer();