<?php
	require_once('../config.php');	
	global $CFG;
	require_once($CFG->libdir.'/formslib.php');
	require_once($CFG->pageDepartmentlib);
	
	class adddepartment_form extends moodleform{
		//This method contains definition of the form i.e. all the elements to be displayed
		function definition() {
			//Defining global objects User and Configuration		
			global $USER, $CFG, $DB;
			//Form object
			
			$mform =& $this->_form;
			//data received to populate this form   
			$adddepartmentdata = $this->_customdata;
			$disabled_fields = "";
			if($adddepartmentdata->auth=='ldap'){
				$disabled_fields = 'class="disabled_user_form_fields" disabled="disabled"';
			}
			if (!isset($adddepartmentdata->id)){ 
				$editoroptions = $this->_customdata['editoroptions'];
			}
			else{
				$editoroptions = $adddepartmentdata->editoroptions;
			}
			$mform->addElement('html','<div class="filedset_outer ">');
			$mform->addElement('text','title', get_string('department_name','plugin'),'maxlength="254" size="50"'.$disabled_fields);   
			$mform->setDefault('title', $adddepartmentdata->title);       
			$mform->addRule('title', get_string('valid_title','department'), 'required', null, 'server');
			
			
			$test = array("text"=>$adddepartmentdata->description, 'format'=>1);
			
			$mform->addElement('editor','description', get_string('department_description','plugin'), null, $editoroptions);
			$mform->setDefault('description', $test);  
			$mform->setType('description', PARAM_RAW);

			//$mform->addElement('file','department_image', get_string('department_image', 'plugin'));
			//$mform->addElement('html','<div class="fitem"><div class="fl italicmsg">'.get_string("department_image_size", 'plugin').'</div></div>');
			if ($filesOptions = programOverviewFilesOptions($programData)) {
				
				$mform->addElement('filemanager', 'testimage_filemanager', get_string('department_image', 'plugin'), null, $filesOptions);
			}
			
	
			if($CFG->allowExternalDepartment == 1){
			  $mform->addElement('checkbox', 'is_external', get_string('isexternaldepartment','department'),'','style="margin-bottom: 16px"'.$disabled_fields);
			  $mform->setDefault('is_external', $adddepartmentdata->is_external);  
			}		
			$showResetButton = true;
			if (isset($adddepartmentdata->id)) {
				$dbManagerSql = "SELECT u.id,u.firstname,u.lastname,u.username,u.is_primary FROM mdl_user as u
								LEFT JOIN mdl_department_members as dm ON dm.userid = u.id
								LEFT JOIN mdl_role_assignments as ra ON ra.userid = u.id AND ra.contextid = 1
								LEFT JOIN mdl_role as r ON r.id = ra.roleid
								WHERE dm.departmentid = $adddepartmentdata->id AND LOWER(r.name) = '".strtolower($CFG->userTypeManager)."'";
				$dbManagers = $DB->get_records_sql($dbManagerSql);
				$dbManagersSelect = array('0'=>get_string("select_primary_manager","department"));		
				$primary = 0;
				if(!empty($dbManagers)){
					foreach($dbManagers as $dbManager){
						if($dbManager->is_primary == 1){
							$primary = $dbManager->id;
						}
						$dbManagersSelect[$dbManager->id] = $dbManager->firstname.' '.$dbManager->lastname.' ('.$dbManager->username.')';
					}
				}
				$mform->addElement('select', 'primary_manager', get_string("primary_manager","department"), $dbManagersSelect);
				$mform->setDefault('primary_manager', $primary);
				
				if($adddepartmentdata->id){
					$created_on = getDateFormat($adddepartmentdata->timecreated, $CFG->customDefaultDateFormat);
					$mform->addElement('html','<div class="fitem "><div class="fitemtitle"><div class="fstaticlabel"><label>'.get_string("timecreated","program").'</label></div></div><div class="felement fstatic">'.$created_on.'</div></div>');
				}
				
				
			
			
				$mform->addElement('hidden','id');
				$mform->setDefault('id', $adddepartmentdata->id);
				
				$mform->addElement('hidden','createdby');
				$mform->setDefault('createdby', $adddepartmentdata->createdby);
				$showResetButton = false;
			}
			
			$this->add_action_buttons(true, get_string('savechanges'), $showResetButton);
			$mform->addElement('html','</div>');
		}
		function validation($data){ 
			global $USER, $CFG,$DB;
			$errors = array();
			
			if(trim($data['title']) == ''){
				$errors['title'] = get_string('required');
			}
		

			/*if($data['id']){
			  $query = "select title from mdl_department where id != '".$data['id']."' AND lower(title) = '".strtolower($data['title'])."' AND deleted = '0'";
			}else{
			  $query = "select title from mdl_department where title = '".$data['title']."' AND deleted = '0'";
			}
			$exists =  $DB->get_field_sql($query);
			if($exists){
				$errors['title'] = get_string('department_title_already_exist','department');
			}*/
			
			
			
			if ($data['id'] and $department = $DB->get_record('department', array('id'=>$data['id']))) {
			
				//if (core_text::strtolower($department->title) != core_text::strtolower(trim($data['title']))) {
					if (isDepartmentAlreadyExist(trim($data['title']),$data['id'])) {
						$errors['title'] = get_string('department_title_already_exist', 'department', $data['title']);
					}
				//}
	
			} else if (isDepartmentAlreadyExist(trim($data['title']))) {
				$errors['title'] = get_string('department_title_already_exist', 'department', $data['title']);
			} 
		

			if(!empty($_FILES) && $_FILES["department_image"]["error"] == 0){
				$imgname = $_FILES["department_image"]["tmp_name"];
				$size = getimagesize($imgname);
				if($size[0]>300 || $size[1]>300){
					$errors['department_image'] = get_string('department_image_error','plugin');
				}
				$filecheck = basename($_FILES["department_image"]['name']);
				$ext = substr($filecheck, strrpos($filecheck, '.') + 1);
				if ($_FILES["department_image"]['name'] != '' && ($ext == "jpg" || $ext == "gif" || $ext == "png") && ($_FILES["department_image"]["type"] == "image/jpeg" || $_FILES["department_image"]["type"] == "image/gif" || $_FILES["department_image"]["type"] == "image/png")){
				}
				else{
					$errors["department_image"] = get_string('supportedfiletype','plugin');
				}
			}
			return $errors;
		}
	}
?>