<?php
	require_once('../config.php');
	require_once($CFG->dirroot . '/local/user/selector/lib.php');
	global $USER,$CFG,$DB;
		
	require_once($CFG->pageDepartmentlib);

        $page = optional_param('page', 1, PARAM_INT); // which page to show
	$perPage = optional_param('perpage', $CFG->perpage, PARAM_INT); // how many per page


    //if userid not available, ask user to login
	require_login();

	$context = get_context_instance(CONTEXT_SYSTEM);
	//check role assign

	if($USER->archetype == $CFG->userTypeManager || $USER->archetype == $CFG->userTypeStudent) // if user is not manager redirect to home page.
	{
		redirect($CFG->wwwroot."/");
	}
	
	$header = "$SITE->shortname";
	// Start setting up the page
	$params = array();
	$PAGE->set_context($context);
	$PAGE->set_url('/department/index.php', $params);
	$PAGE->set_pagelayout('listing');
	$PAGE->set_pagetype('my-index');
	$PAGE->blocks->add_region('content');
	$PAGE->set_subpage($currentpage->id);
	$PAGE->set_title($header);
	$PAGE->set_heading($header);
	
	$PAGE->navbar->add(get_string('browse_department',"department"));
	// End setting up the page

	//Start Activate Deactivate department
	$id = optional_param('id', 0, PARAM_INT);
	$action = optional_param('action', 2, PARAM_INT);

	$paramArray = array(
					'ch' => optional_param('ch', '', PARAM_ALPHA),
					'key' => optional_param('key', '', PARAM_RAW),
					'title' => optional_param('title', '', PARAM_RAW),
					'des' => optional_param('des', '', PARAM_RAW)
					);
	$filterArray = array(							
						'title'=>get_string('name'),
						'des'=>get_string('description')
				   );
	$pageURL = '/department/index.php';
	checkPageIsNumeric($pageURL,$_REQUEST['page']);
	////// Define those key, which we need to remove first time from General URL //////
	$removeKeyArrayForLink = array('id','action');

	////// Getting common return URL for the page//////
	$returnurlLink = genParameterizedURL($paramArray, $removeKeyArrayForLink, $pageURL);
	if($action=='1')
	{
		$updateData = new stdClass();
		$updateData->status=1;
		$updateData->id=$id;
		$updateId = $DB->update_record("department", $updateData);
		$_SESSION['update_msg']=get_string('record_updated');
		redirect($returnurlLink);
	}elseif($action=='0')
	{
		$updateData = new stdClass();
		$updateData->status=0;
		$updateData->id=$id;
		$updateId = $DB->update_record("department", $updateData);
		$_SESSION['update_msg']=get_string('record_updated');
		redirect($returnurlLink);
	}
	//Start Activate Deactivate department

	//Get department listing
	$departments = getDepartmentListing('title','ASC',$page,$perPage,$paramArray);
	//Get department count
	$departmentCount = getDepartmentListing('departmentcount','ASC',$page,$perPage,$paramArray);

	echo $OUTPUT->header(); // print header

	////// Define those key, which we need to remove first time from General URL //////
	$removeKeyArray = array('id','action','perpage');

	////// Getting common URL for the paging bar //////
	$pagingUrl = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);

	/*Start Print search form*/
	echo genCommonSearchForm($paramArray, $filterArray, $pageURL);
	/*End Print search form*/

	echo '<div class="category_section">';

	//Start Print add new department button.
	
	if($CFG->canAddDepartment == 1){
	  echo '<a class="button-link adddepartment" href="'.$CFG->wwwroot.'/department/'.$CFG->pageAddDepartment.'" ><i></i><span>'.get_string("add_department_page","department").'</span></a>';
	}
	//End Print add new department button.

	//Start Print update message
	if($_SESSION['update_msg']!=''){
		echo '<div class="clear"></div>';
		echo '<div class="bg-success text-success text-center" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
		echo '<div class="clear"></div>';
		$_SESSION['update_msg']='';
	}
	//Ends  Print update message

	//Start page Listing content
	echo '<div class = "borderBlock">';

	//Check for ? in url
	$chkParamFirst = strpos($returnurlLink, '?');
	if($chkParamFirst === false) {
		$returnurlLink .= '?action=';
	} else {
		$returnurlLink .= '&action=';
	}

	//Print header
	echo "<h2 class='icon_title'><span class='depart-ment'></span>".get_string('departments',"department")." (".$departmentCount.")</h2>";
	//Start print table
	echo '<div class="borderBlockSpace"><table class="table1" cellpadding="0" cellspacing="0" style="width: 100%;">
	<thead><tr class="table1_head">';
	echo '<td width="30%" class="align_left">'.get_string("name","department").'</td>';
	if($CFG->isLdap == 1){		
		echo  "<td width = '7%' align='align_left' >".get_string('groupsource','group')."</td>";
		echo '<td width="13%" class="align_left">'.get_string("primary_manager","department").'</td>';
	}
	else{
		echo '<td width="20%" class="align_left">'.get_string("primary_manager","department").'</td>';
	}
	echo '<td width="10%" class="align_left">'.get_string("noofcourses").'</td>
	<td width="10%" class="align_left">'.get_string("noofusers").'</td>
	<td width="30%" align="align_left">'.get_string("manage").'</td>
	</tr></thead>';
	
	$depUsersArr = getDepartmentMembers();
	if($departments){
		foreach($departments as $key => $department){
			$departmentSource = $department->auth;
			$departmentTeam = $DB->get_records_sql("SELECT gd.id FROM mdl_group_department as gd LEFT JOIN mdl_groups as g ON gd.team_id = g.id WHERE g.id IS NOT NULL AND gd.department_id = ".$department->id);
			//pr($departmentTeam);die;
			/*$departmentManager = $DB->get_record_sql("SELECT u.id,u.firstname,u.lastname FROM mdl_user as u 
LEFT JOIN mdl_department_members as dm ON dm.userid = u.id AND dm.is_active = 1
WHERE u.is_primary = 1 AND dm.departmentid = ".$department->id);*/
            $cntTotalCourses = 0;
            $dCoursesObj = new department_courses_selector('', array('departmentId' => $department->id));
			
			$dCoursesArr = $dCoursesObj->getAssignedDepartmentCourses();
			$cntTotalCourses = is_array($dCoursesArr)?count($dCoursesArr):0;
			$depUsersCnt = isset($depUsersArr[$department->id])?count($depUsersArr[$department->id]):0;
			$managerName = $department->firstname?$department->firstname.' '.$department->lastname:'';
			$editDisable = 'edit';
			$disabledclass = '';
			$linkAssignCoursesDisableClass = '';
			$linkEdit = $CFG->wwwroot.'/department/'.$CFG->pageAddDepartment.'?id='.$department->id;
			$linkImpesunate = $CFG->wwwroot.'/local/autologin.php?id='.$department->id;
			$linkAssignCourses = new moodle_url($securewwwroot.'/department/'.$CFG->pageDepartmentAssigncourses, array('department'=>$department->id));
			if($department->status=='0')
			{
				$disabledclass = 'class="disable"';
				$editDisable = 'edit-disable';
				$linkEdit = 'javascript:void(0);';
				$linkAssignCourses = 'javascript:void(0);';
				$linkAssignCoursesDisableClass = ' disable';
				//Status link
				$tooltip=get_string("activate");
				$status='<a href="'.$returnurlLink.'1&id='.$department->id.'" title="'.$tooltip.'" class = "enable" ></a>';
			}else{
				//Status link
				$tooltip=get_string("inactivate");
				$status='<a href="'.$returnurlLink.'0&id='.$department->id.'" title="'.$tooltip.'" class = "disable" ></a>';
			}

			//Print only first 200 characters of description
			if(strlen($department->description) >200){
				$description = substr($department->description,0,200)." ...";
			}else{
				$description = $department->description;
			}

			$assignCourses = html_writer::link($linkAssignCourses, html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('i/department'), 'alt'=>get_string('assigncourses'), 'class'=>'iconsmall')), array('title'=>get_string('assigncourses'), 'class'=>'assign-courses'.$linkAssignCoursesDisableClass));

			//start department image
			/*if($department->picture != ''){
				$departmentImageRootPath =  $CFG->dirroot.'/theme/gourmet/pix/department/'.$department->picture;
				$departmentImagePath =  $CFG->wwwroot.'/theme/gourmet/pix/department/'.$department->picture;
			}
			if(!file_exists($departmentImageRootPath)){
				$departmentImagePath =  $CFG->wwwroot.'/theme/gourmet/pix/department-img.jpg';
			}
			$departmentImage = '<img src="'.$departmentImagePath.'" alt="'.$department->title.'" title="'.$department->title.'" border="0" />';*/
			$departmentImage = getDepartmentImage($department);
			//end department image
			
			
			//Start print table row
			
			echo '<tr '.$disabledclass.'>
			<td class="align_left">'.$departmentImage . '<span class="f-left">'.$department->title.'</span></td>';
			if($CFG->isLdap == 1){
				echo  "<td  >".strtoupper($department->auth)."</td>";
			}
			echo '<td class="align_left">'.($managerName?$managerName:getMDash()).'</td>
			
			<td class="align_left">'.$cntTotalCourses.'</td>
			<td class="align_left">'.$depUsersCnt.'</td>
			<td>
			<div class="adminiconsBar order">';
		
			echo '<a title="'.get_string("edit").'" class="'.$editDisable.'" href="'.$linkEdit.'">'.get_string("edit").'</a> ';
			
			if($depUsersCnt>0 and $disabledclass1==''){
			//	echo '<a title="'.get_string("buy_courses","cart").'" class="'.$editDisable.$disabledclass1.' cart" href="'.$linkCart.'">'.get_string("buy_courses","cart").'</a> ';
					echo '<a title="'.get_string("impursanute").'" class="'.$disabledclass1.' impersonate enabled-impersonate" href="'.$linkImpesunate.'">'.get_string("impursanute").'</a> ';
			}else{
			//	echo '<a title="'.get_string("please_create_manager_first","cart").'" class="cart disabled" cart" href="javascript:void(0)">'.get_string("buy_courses","cart").'</a> ';
				echo '<a title="'.get_string("impursanute").'" class="impersonate disable"  href="javascript:void(0)">'.get_string("impursanute").'</a> ';
			}
			echo $assignCourses.$status;
			if($depUsersCnt == 0 ){
				if(empty($departmentTeam)){
					echo ' <a title="'.get_string("delete").'" class="delete delete_department" href="'.$CFG->wwwroot.'/department/'.$CFG->pageDeleteDepartment.'?id='.$department->id.'&action=1">'.get_string("delete").'</a>';
				}else{
					echo ' <a title="'.get_string("cannot_delete_department").'" class="delete disabled" href="#">'.get_string("delete").'</a>';
				}
			}else{
				echo ' <a title="'.get_string("cannot_delete_department").'" class="delete disabled" href="#">'.get_string("delete").'</a>';
			}
			echo'</div>
			</td>
			</tr>';
			//End print table row
		}
	}else{
		//Print message if no record found
		echo '<tr>
		<td colspan="5">'.get_string("no_results").'</td>
		</tr>';
	}	
	echo '</table></div>';
	//End print table

	echo '</div>';
	echo '</div>';
	//End page Listing content

	//Print paging bar
	echo paging_bar($departmentCount, $page, $perPage,$pagingUrl);
	//print footer
	echo $OUTPUT->footer();
?>
<script language="javascript" type="application/javascript">
setInterval(function(){$('.update_msg').slideUp();},10000);
$(document).ready(function(){


	$(".delete_department").click(function(){
		
		var slotid = $(this).attr("rel");
		var txt = "<?php echo get_string('departmentdeleteconfirm')?>";
		
		
		var response = confirm(txt);
		
		if (response == true) {
		return true;
		}
		return false;
	});

	$(".enabled-impersonate").click(function(){

		var txt = "<?php echo get_string('impursanute_confirmation')?>";
		
		
		var response = confirm(txt);
		
		if (response == true) {
		return true;
		}
		return false;
	});

	
	

	
});
</script>