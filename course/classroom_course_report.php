<?php

	/**
		* Custom module - Signle Course Report Detail Page
		* Date Creation - 02/07/2014
		* Date Modification : 01/07/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/

	
	require_once("../config.php");

	global $DB,$CFG,$USER, $SITE;
	
	$sort    = optional_param('sort', '', PARAM_ALPHANUM);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 10, PARAM_INT);        // how many per page
	$export  = optional_param('action', '', PARAM_ALPHANUM);
	$type  = optional_param('type', '-1', PARAM_RAW);
	$status  = optional_param('status', '-1', PARAM_RAW);
	$back = optional_param('back', 1, PARAM_INT);
	$cType = optional_param('ctype', 2, PARAM_INT);
	$course = optional_param('course', '-1', PARAM_INT);
	$classes = optional_param('classes', '-1', PARAM_INT);
			  
	if( $USER->archetype == $CFG->userTypeStudent ) {
	  $userid = $USER->id;
	}else{
	  $userid = required_param('userid', PARAM_INT);
	}
	
	$sDate = '';
	$eDate = '';
		
	/*$sDate =  $CFG->reportSearchStartDate;
	$eDate = $CFG->reportSearchEndDate*/

	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);
	
	$paramArray = array(
	'back' => $back,
	'type' => $type,
	'status' => $status,
	'back' => $back,
	'userid' => $userid,
	'ctype' => $cType,
	'course' => $course,
	'classes' => $classes,
	'startDate' => $sStartDate,
	'endDate' => $sEndDate
    );
  
	
	$removeKeyArray = array();
	checkLogin();
	
	require_once ($CFG->dirroot . '/mod/scheduler/lib.php');
	$PAGE->set_pagelayout('globaladmin');
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('overallclassroomreport','scheduler'));
	
	if($USER->archetype == 'learner'){
	     $PAGE->navbar->add(get_string('reports','multiuserreport'));
		 $PAGE->navbar->add(get_string('classroomreport'));
	}else{
		if($back == 2){
		 $PAGE->navbar->add(get_string('reports','multiuserreport'));
		 $PAGE->navbar->add(get_string('usereperformancereport','multiuserreport'), new moodle_url($CFG->wwwroot.'/user/multiuser_report.php'));
		 //$PAGE->navbar->add(get_string('viewreportdetails','multiuserreport'));
		}elseif($back == 3){
		  $PAGE->navbar->add(get_string('reports','multiuserreport'));
		  $PAGE->navbar->add(get_string('userreport','multiuserreport'), new moodle_url($CFG->wwwroot.'/user/user_report.php'));
		}elseif($back == 1){
		  $PAGE->navbar->add(get_string('manage_user'),'/admin/user.php');
		  //$PAGE->navbar->add(get_string('viewuserreports','singlereport'));
		}
		
		$PAGE->navbar->add(getUsers($userid));	
	}
	
	$classroomCourseReport = getClassroomCourseReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
	$courseHTML = $classroomCourseReport->courseHTML;
		   
	echo $OUTPUT->header(); 
	echo $courseHTML;
	echo includeGraphFiles(get_string('overallclassroomreport','scheduler')); 
    echo $OUTPUT->footer();
	