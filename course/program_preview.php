<?php

	/**
		* Custom module - program launch page
		* Date Creation - 06/08/2014
		* Date Modification : 06/08/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/

	require_once(dirname(__FILE__) . '/../config.php'); 
	require_once($CFG->dirroot.'/mod/scheduler/lib.php'); 
	
	global $USER,$PAGE,$OUTPUT,$DB;
	$userId = $USER->id;

    require_login();

    $dir          = optional_param('dir', 'DESC', PARAM_ALPHA);
    $page         = optional_param('page', 1, PARAM_INT);
    $perpage      = optional_param('perpage', 10, PARAM_INT);        // how many per page
	$pid       = optional_param('pid', 0, PARAM_INT);
  
	if( $USER->archetype == $CFG->userTypeManager) { // in the case of manager, we will get the manager department ids and these ids will be used to get courses by department  
	    $managerProgramArr = getManagerProgram(1, true);
		if(!in_array($pid, $managerProgramArr)){ 
			redirect($CFG->wwwroot."/program/index.php"); 
		}
	}


	if($pid == 0){
		redirect($CFG->wwwroot);
	}
	
	$programName = '---';
	$programDesc = '---';
	$programDetails = getPrograms('', true, 3, 0);

	if(isset($programDetails[$pid]) && count($programDetails[$pid]) > 0 ){
	   $programName = $programDetails[$pid]->name;
	   $programDesc = $programDetails[$pid]->description;	   
	}
	$programCourses = getProgramCourses($pid);

    $header = $SITE->fullname.": ".get_string('preview');
    $PAGE->set_title($header);
	$PAGE->set_heading($header);
	$PAGE->set_pagelayout('globaladmin');
	$manageprograms = get_string('manageprograms','program');
	$PAGE->navbar->add($manageprograms, new moodle_url('/program/index.php'));
   	$PAGE->navbar->add(get_string('preview'));
	
    if (empty($CFG->loginhttps)) {
        $securewwwroot = $CFG->wwwroot;
    } else {
        $securewwwroot = str_replace('http:','https:',$CFG->wwwroot);
    }

    echo $OUTPUT->header();
	
	$extrasql='';
	$params=array();
	$_SESSION['extrasql'] = '';
	//require_once("../local/searchForm.php");
	$extrasql =  $_SESSION['extrasql'];//die;

    $courseCnt = count($programCourses);

  
	 $html = '';
	 $outerDivStart = "<div class='borderBlock'>";
	 $outerDivEnd = "</div>";
	
     $programHeaderHtml = getModuleHeaderHtml($programDetails[$pid], $CFG->programModule);
	 $html .= $programHeaderHtml;
	 		
	 /*$html .= '<div style="width:100%;float:left;margin:0px 5px 5px 0;padding:0px 5px 5px 0"><!--span style="font-weight:bold;margin-right:5px">'.get_string('program','program').':</span--><div class="c-heading">'.$programName.'</div></div>';
	 $html .= '<div style="width:100%;float:left;margin:0px 5px 5px 0;padding:0px 5px 5px 0"><span style="font-weight:bold;margin-right:5px">'.get_string('description','program').':</span>'.$programDesc.'</div>';*/
	 if ($courseCnt > 0) {
	 
        $html .= $outerDivStart;
	    $htmlDiv = '';
	    $html .= html_writer::start_tag('div', array('class'=>'borderBlockSpace'));
            $i=0;
			//pr( $programCourses);die;
			
			foreach ($programCourses as $courses) {
			
				$i++;
				$courseformat = 'topics';
				$courseId = $courses->id;
				
				$courseTypeId = getCourseTypeIdByCourseId($courseId);
			
				if($i == count($courses)){
				  $htmlDiv = '<div class="borderBlock borderBlockSpace">';
				}else{
				  $htmlDiv = '<div class="borderBlock borderBlockSpace borTop_none">';
				}
				
				$courseHeaderHtml = getModuleHeaderHtml($courses, $CFG->courseModule, $noBorder=1);
				$htmlDiv .= $courseHeaderHtml;
		               
				if($courseTypeId == $CFG->courseTypeClassroom ){

						// Asset Box Start						 
						$courseDiv = '';
						$ccDiv = '';
							
						$sectionArr = getTopicsSection($courseId, $userId, '', $courseTypeId);
						
						$sectionHtml = '';
						
						//pr($sectionArr);die;
						if(count($sectionArr) > 0 ){
						
							foreach($sectionArr as $sectionId=>$sections){

									if(count($sections->courseModule) > 0){
									
									
									   //$sectionDiv .= '<div class="c-box" style="display:block !important;">';
									   $topicsCourseNew = array();
									   foreach($sections->courseModule as $cmId=>$courseModule){

										  $topicsCourse = $courseModule['topicCourse'];
										  //pr($topicsCourse);
										  if(count($topicsCourse)>0){
												   foreach($topicsCourse as $topics){
													 $topicsCourseNew[$topics->resource_type_id]['name'] = $topics->resource_type;
													 $topicsCourseNew[$topics->resource_type_id]['details'][] = $topics;
												   }
												 } // end if for topicsCourse
										  }
									
									}

									//$sectionHtml .= '</div>'; // end section html
								
							} // end sectionArr foreach
								
						} // end sectionArr if
						
						// Asset Box End
	
								
								
					
					   //pr($topicsCourseNew);
			 
						// Asset Box Start
						
						if(count($topicsCourseNew) > 0){
						
						   $ccDiv .= '<div class="d-box paddBottom">';
						   $ccDiv .= '<div class="assetlabel" >'.get_string('assetlabel','learnercourse').': </div>';
						   $ccDiv .= '<div class="classroom-resource-box-outer">';
						   $i = 0;
						   foreach($topicsCourseNew as $topicsCourseArr){
						   
							  $i++;
							  $resourceType = $topicsCourseArr['name'];
							  $resourceTypeDetails = $topicsCourseArr['details'];
							  
							  if(count($resourceTypeDetails) > 0 ){
							  
								$ccDiv .= '<div class="classroom-resource-box" ><table cellspacing="0" width="100%" cellpadding="0" border="0" class=""><tr>'; 
								$ccDiv .= '<td><!--('.$i.') -->'.$resourceType.' </td><td>'; 
								$rDetailsDiv = '';
								//pr($resourceTypeDetails);die();
								foreach($resourceTypeDetails as $rtdArr){
								
								   $resoursename = $rtdArr->resoursename;
								   $resourcesummary = strip_tags($rtdArr->resourcesummary);
								   $categoryName = $rtdArr->categoryname;
								   $allowStudentAccess = $topicsCourseArr->allow_student_access == 1?"Yes":'No';
								   $assetIconClass = '';
								   $cmid = $rtdArr->cmid;							   
								   if (!$cm = get_coursemodule_from_id('resource', $cmid)) {
										resource_redirect_if_migrated(0, $courseId);
										print_error('invalidcoursemodule');
								   }else{
											$resource = $DB->get_record('resource', array('id'=>$cm->instance));
											
											if(count($resource) > 0 ){
											
												$context = context_module::instance($cm->id);
												$fs = get_file_storage();
												$files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
									
												if (count($files) < 1) { 
													 $linkUrl = "<div class='classroom-resource-details'><a href='javascript:;' onclick='alert(\"".get_string('clamdeletedfile')."\")' title='".$resoursename."'   ><span class = '".$assetIconClass."' style='height:30px'></span>".$resoursename."</a> </div>";
												} else {
													 $file = reset($files);
													 unset($files);
													  $path = '/'.$context->id.'/mod_resource/content/'.$resource->revision.$file->get_filepath().$file->get_filename();
													  $resourseType = end(explode('.',$path));
													  $assetIconClass = getFileClass($resourseType);
													  $fullurl = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
													  $linkUrl = "<div class='classroom-resource-details'><a href='javascript:;' onclick=\"window.open('".$fullurl."', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \" title='".$resoursename."'   ><span class = '".$assetIconClass."' style='height:30px'></span>".$resoursename."</a> </div>";
												}
											}
									}
																

									
									$rDetailsDiv .= $linkUrl;
								}
								$ccDiv .= $rDetailsDiv.'</td></tr></table></div>'; 
							  }
						   }
						   $ccDiv .= '</div>';
						   $ccDiv .= '</div>';
						}
			 
						// Asset Box End
						
						// Class Box End 
  
					$classHtml = getClassesPreviewHtmlForUser($courseId);
					
					$ccDiv .= $classHtml;
					$courseDiv .= $ccDiv;
			
				}else{
						 
							if($courseformat == 'singleactivity'){
			
										$courseDiv = '';
										$courseDiv .= '<div class="d-box" id="dbox'.$courseId.'">';
						   
										$table = new html_table();
						
										$table->head = array ();
										$table->colclasses = array();
										$table->head[] = $scormname;
										$table->attributes['class'] = 'admintable generaltable commonTD';
										$table->colclasses[] = 'leftalign  w150';
										$table->head[] = $scormsummary;
										$table->colclasses[] = 'leftalign w600';
										//$table->head[] = $timespent;
										/*
										$table->head[] = get_string('currentstatus','learnercourse');
										$table->colclasses[] = 'leftalign';
										$table->head[] = $score;
										$table->colclasses[] = 'leftalign';
										$table->head[] = $lastaccessed;
										$table->colclasses[] = 'leftalign ';
										*/
										$table->head[] = get_string('action');
										$table->colclasses[] = 'centeralign';
										//pr($learnercourses);
										$table->id = "learnercourses";
									
										$buttons = array();
										$lastcolumn = '';
										$curtime = time();
							
										$scormid = $courses->scormid;
										$scormtitle = $courses->scormname;
										$scormdiscription = strip_tags($courses->scormsummary);
										$categoryname = $courses->categoryname;
										$timecreated = $courses->ctimecreated;
										
										//$lastAccessedDT = getScormStatusForUser($scormid, $userId, 'lastaccessed');
										$lastAccessedTime = $courses->lastaccessed;
										if($lastAccessedTime){
											$remainAccessedTime = $curtime - $lastAccessedTime;
											$lastAccessed = timePassed($remainAccessedTime);
										}else{
											 $lastAccessed = get_string('notaccessed','learnercourse');
										}
										
										$timespentVal = $courses->timespent;
										
										if($timespentVal){
										   $timeSpentResult = convertCourseSpentTime($timespentVal);
										   $timeSpentResult = str_replace(get_string('ago','learnercourse'),'',$timeSpentResult);
										}else{
											$timeSpentResult = '---';
										}   
			
										$scoreResult = $courses->score!=''?($courses->score." ".get_string('points','learnercourse')):'---';
										$scormStatus = $courses->scormstatus;
										
										 if($scormStatus == ''){
										   $scormStatus = get_string('notstarted','learnercourse');
										 }elseif($scormStatus == 'incomplete'){
										   $scormStatus = get_string('inprogress','learnercourse');
										 }elseif(in_array($scormStatus, array('passed','completed'))){
										   $scormStatus = get_string('completed','learnercourse');  
										 }elseif($scormStatus == 'failed'){
										   $scormStatus = get_string('completed','learnercourse');  
										 }
										 
										
										$scormCourseModuleId = getScormCourseModuleId($scormid, $courseId);
										
										$scormLaunch = "<a href=\"../mod/scorm/view.php?id=$scormCourseModuleId\" class = \"launch_icon\">".get_string('launch','learnercourse')."</a>";
			
										$row = array ();
										$row[] = "<a href=\"../mod/scorm/view.php?id=$scormCourseModuleId\">".$scormtitle."</a>";
										$row[] = $scormdiscription;
										//$row[] = $timeSpentResult;
										/*
										$row[] = $scormStatus;
										$row[] = $scoreResult;
										$row[] = $lastAccessed;
										*/
										$row[] = $scormLaunch;
										$table->data[] = $row;
										
										if (!empty($table)) {
											//$courseDiv .= html_writer::start_tag('div', array('class'=>'no-overflow'));
											$courseDiv .= html_writer::table($table);
											//$courseDiv .= html_writer::end_tag('div');
										}
			
								
								$courseDiv .= '</div>';
								
							}elseif($courseformat == 'topics'){
							
										$courseDiv = '';
										$courseDiv .= '<div class="d-box" id="dbox'.$courseId.'">';
										
										$sectionArr = getTopicsSection($courseId, $userId);
										
										$sectionHtml = '';
										
										//pr($sectionArr);
										if(count($sectionArr) > 0 ){
				
											foreach($sectionArr as $sectionId=>$sections){
											
														$table = new html_table();
						
														$table->head = array ();
														$table->colclasses = array();
														$table->head[] = get_string('title','learnercourse');
														$table->attributes['class'] = 'admintable generaltable commonTD';
														$table->colclasses[] = 'leftalign  w150';
														$table->head[] = get_string('summary','learnercourse');
														$table->colclasses[] = 'leftalign w600';
														//$table->head[] = get_string('timespent','learnercourse');
														
														/*
														$table->head[] = get_string('currentstatus','learnercourse');
														$table->colclasses[] = 'leftalign';
														$table->head[] = get_string('score','learnercourse');
														$table->colclasses[] = 'leftalign';
														$table->head[] = get_string('lastaccessed','learnercourse');
														$table->colclasses[] = 'leftalign ';
														*/
		
														$table->head[] = get_string('action');
														$table->colclasses[] = 'centeralign';
														//pr($learnercourses);
														$table->id = "learnercourses";
													
														$buttons = array();
														$lastcolumn = '';
														$curtime = time();
														
											
														$sectionDiv = '';
													   $sectionName = $sections->sectionname;
														  $sequence = $sections->sequence;
													$sectionSummary = $sections->summary;
													  $topicsCourse = $sections->courseModule;
												   
														$sectionHtml .= '<div class="section-box" >'; // start section div
														//$sectionHtml .= '<div style="float:left;width:100%;padding:10px" class="sbox"  rel="'.$sectionId.'" id="sbox'.$sectionId.'"><a href="javascript:void(0)"></a><img alt="" src="'.$CFG->wwwroot.'/theme/image.php?theme=gourmet&component=core&image=a%2Fadd_file">$sectionName</div>';
														
					
													//pr($sectionArr);die;
													//$topicsCourse = getTopicsCourse($courseId, $userId, 18);
													//$nonCourseStatusForUser = getNonCourseStatusForUser($userId);
													//pr($nonCourseStatusForUser);
													
													if(count($sections->courseModule) > 0){
													
													
													  $sectionDiv .= '<div class="c-box" id="cbox'.$sectionId.'" style="display:block !important;">';
													   foreach($sections->courseModule as $cmId=>$courseModule){
		
														  $topicsCourse = $courseModule['topicCourse'];
														  //pr($topicsCourse);
														  if(count($topicsCourse)>0){
																   foreach($topicsCourse as $topicsCourseArr){
																   //pr($topicsCourseArr);
																		$coursetype = $topicsCourseArr->coursetype;
																		
																		if($coursetype == 'scorm'){
																		
																				$assetIconClass = 'scorm';
																				$scormid = $topicsCourseArr->scormid;
																				$scormtitle = $topicsCourseArr->scormname;
																				$scormdiscription = strip_tags($topicsCourseArr->scormsummary);
																				$categoryname = $topicsCourseArr->categoryname;
																				$timecreated = $topicsCourseArr->ctimecreated;
																				$timespentVal = $topicsCourseArr->timespent;
																				$coursesScore = $topicsCourseArr->score;
																				$scormStatus = $topicsCourseArr->scormstatus;
																				$lastAccessedTime = $topicsCourseArr->lastaccessed;
																				if($lastAccessedTime){
																					$remainAccessedTime = $curtime - $lastAccessedTime;
																					$lastAccessed = timePassed($remainAccessedTime);
																				}else{
																					 $lastAccessed = get_string('notaccessed','learnercourse');
																				}
												
																				if($timespentVal){
																				   $timeSpentResult = convertCourseSpentTime($timespentVal);
																				   $timeSpentResult = str_replace(get_string('ago','learnercourse'),'',$timeSpentResult);
																				}else{
																					$timeSpentResult = '---';
																				}   
													
																				$scoreResult = $coursesScore!=''?($coursesScore." ".get_string('points','learnercourse')):'---';
																				
																				
																				if($scormStatus == ''){
																				   $scormStatus = get_string('notstarted','learnercourse');
																				 }elseif($scormStatus == 'incomplete'){
																				   $scormStatus = get_string('inprogress','learnercourse');
																				 }elseif(in_array($scormStatus, array('passed','completed'))){
																				   $scormStatus = get_string('completed','learnercourse');  
																				 }elseif($scormStatus == 'failed'){
																				   $scormStatus = get_string('completed','learnercourse');  
																				 }
																				 
																				
																				$scormCourseModuleId = getScormCourseModuleId($scormid, $courseId);
																				$scodata = $DB->get_record_sql("SELECT * FROM mdl_scorm_scoes WHERE scorm = $scormid ORDER BY id DESC");
																				$scormdata = $DB->get_record('scorm',array('id'=>$scormid));
																				//$launchurl = $CFG->wwwroot.'/mod/scorm/player.php?a='.$scormid.'&cm='.$scormCourseModuleId.'&scoid='.$scodata->id.'&currentorg='.$scodata->organization.'&newattempt=off&display=popup&mode=preview';
																				$launchurl = $CFG->wwwroot.'/mod/scorm/player.php?a='.$scormid.'&cm='.$scormCourseModuleId.'&scoid='.$scodata->id.'&currentorg=&newattempt=off&display=popup&mode=preview';
																				
																				//$scormLaunch = "<a href=\"../mod/scorm/view.php?id=$scormCourseModuleId\" class = \"launch_icon\">".get_string('launch','learnercourse')."</a>";
		
																				$scormLaunch = "<a href=\"javascript:void(0);\" onclick=\"window.open('".$launchurl."', '', 'width=".$scormdata->width.",height=".$scormdata->height.",toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \" class = \"launch_icon\">".get_string('launch','learnercourse')."</a>";
		
																				$row = array ();
													
																				$row[] = "<span class = '".$assetIconClass."'></span><a href=\"../mod/scorm/view.php?id=$scormCourseModuleId\">".$scormtitle."</a>";
																				$row[] = $scormdiscription;
																				//$row[] = $timeSpentResult;
																				/*
																				$row[] = $scormStatus;
																				$row[] = $scoreResult;
																				$row[] = $lastAccessed;
																				*/
																				$row[] = $scormLaunch;
																				$table->data[] = $row;
																				
																		}elseif($coursetype == 'resource'){
																		  
																		  
																			$resourceCourseId = $topicsCourseArr->id;
																			$resourceid = $topicsCourseArr->resourceid;
																			$cmid = $topicsCourseArr->cmid;
																			
																			$resoursename = $topicsCourseArr->resoursename;
																			$resourcesummary = strip_tags($topicsCourseArr->resourcesummary);
																			$categoryname = $topicsCourseArr->categoryname;
																			$lastAccessedTime = $topicsCourseArr->lastaccessed;
																			
																			$lastaccessed = $lastaccessed?getDateFormat($lastaccessed, "d/m/Y"):'---';
																			if($lastAccessedTime){
																				$remainAccessedTime = $curtime - $lastAccessedTime;
																				$lastAccessed = timePassed($remainAccessedTime);
																			}else{
																				 $lastAccessed = get_string('notaccessed','learnercourse');
																			}
																								
																		
																		   $styDownload = '';
																		   
																		   $filedownloaded = isUserAccessedCourse($userId, $courseId, $resourceid);
																		   
																		   if($filedownloaded){
																			 $styDownload = "downloaded";
																		   }
																		   
																		   if($styDownload){
																			 $resourceStatus = get_string('downloaded','learnercourse');
																		   }else{
																			 $resourceStatus = get_string('notdownloaded','learnercourse');
																		   }
																
																			if (!$cm = get_coursemodule_from_id('resource', $cmid)) {
																				resource_redirect_if_migrated(0, $courseId);
																				print_error('invalidcoursemodule');
																			}
																			
																			
																			$resource = $DB->get_record('resource', array('id'=>$cm->instance), '*', MUST_EXIST);
																			$context = context_module::instance($cm->id);
																			$fs = get_file_storage();
																			$files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
																
																			if (count($files) < 1) { 
																				//resource_print_filenotfound($resource, $cm, $course);
																				//die;
																				$fileStatus = "<a href='javascript:void(0);' onclick='alert(\"".get_string('clamdeletedfile')."\")' title='".get_string('download','learnercourse')."' class='download_icon ".$styDownload."'   id='coursestatus_".$courseId."_".$resourceid."'>".get_string('download','learnercourse')."</a>";
																			} else {
																				$file = reset($files);
																				unset($files);
																				$path = '/'.$context->id.'/mod_resource/content/'.$resource->revision.$file->get_filepath().$file->get_filename();
																				$fullurl = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
																	   
																				$resourseType = end(explode('.',$path));
																				$assetIconClass = getFileClass($resourseType);
																	   
																				$fileStatus = "<a href='javascript:void(0);' onclick=\"setCourseLastAccess($userId, $courseId, $resourceid);window.open('".$fullurl."', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \" title='".get_string('download','learnercourse')."' class='download_icon ".$styDownload."'   id='coursestatus_".$courseId."_".$resourceid."'>".get_string('download','learnercourse')."</a>";
																			}
																			
																	
																			$row = array();
																			
																			$row[] = "<span class = '".$assetIconClass."'></span>".$resoursename;
																			$row[] = $resourcesummary;
																			/*
																			$row[] = $resourceStatus;
																			$row[] = get_string('NA','learnercourse');
																			$row[] = $lastAccessed;
																			*/															$row[] = $fileStatus;
																			$table->data[] = $row;
																			
																		}
																		
																	
																 } // end foreach for topicsCourse	
										
																 } // end if for topicsCourse
														  }
														  
														   
														   if (!empty($table)) {
																$sectionDiv .= html_writer::table($table);
														   }
														   
														   $sectionDiv .= '</div>';
														   $sectionHtml .= $sectionDiv;
													
													}
		
													$sectionHtml .= '</div>'; // end section html
												
											} // end sectionArr foreach
											
											$courseDiv .= $sectionHtml; 
											
										} // end sectionArr if
										
										
			
								
								$courseDiv .= '</div>';
								
							}
					
		            }
					$htmlDiv .= $courseDiv.'</div>';
					
					$html .= $htmlDiv;
				
			}
			

	 
	  
		  $html .= html_writer::end_tag('div');
		  $html .= $outerDivEnd;

	  
      }else{
	     //$html .= $OUTPUT->heading(get_string('nolearnercoursesfound','learnercourse'));
	  }
	  
	   echo $html;
	
 ?>
 

 <script type="text/javascript">
 
 
 function setCourseLastAccess(userid, courseid, resourceid){
	
     
	   /*$.ajax({
	  
	    url:'<?php //echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=setCourseLastAccess&userid='+userid+"&courseid="+courseid+"&resourceid="+resourceid,
		dataType:'json',
		success:function(data){
		  
		   var success = data.success;
		     var error = data.error;
			   if(success == 1){
			      var course_resource_id = data.response;
				  $('#coursestatus_'+course_resource_id).addClass('downloaded');
			   }else{
				 alert(error);
			   }
		}
	  
	  });*/

	   $.ajax({
	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=setCourseBadge&userid='+userid+"&courseid="+courseid+"&resourceid="+resourceid,
		dataType:'json',
		success:function(data){  
		}
	  
	  });
	
 }
	
  

 </script>
 <?php	

 echo $OUTPUT->footer();
 
