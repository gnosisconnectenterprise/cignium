<?php

	/**
		* Custom module - Learning page
		* Date Creation - 04/05/2014
		* Date Modification : 23/06/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/

	require_once(dirname(__FILE__) . '/../config.php'); 
	
	global $USER;
	$userId = $USER->id;

    require_login();

    $sort         = optional_param('sort', 'id', PARAM_ALPHANUM);
    $dir          = optional_param('dir', 'DESC', PARAM_ALPHA);
    $page         = optional_param('page', 1, PARAM_INT);
    $perpage      = optional_param('perpage', 10, PARAM_INT);        // how many per page
	$id       = optional_param('id', 0, PARAM_INT);
	$access = optional_param('access', '', PARAM_ALPHA);
	$courseTypeId = getCourseTypeIdByCourseId($id);
	
	
	//pr(count($arr));die;
	
	if(!$courseTypeId || $id == 1){
	  redirect($CFG->wwwroot."/course/index.php"); 
	}
	
	if( $courseTypeId == $CFG->courseTypeClassroom) {
	   redirect($CFG->wwwroot."/course/".$CFG->pageClassroomPreview."?id=$id"); 
	}
	

	
	if($access=='direct' && $CFG->showCourseURL==1){

		courseDirectAccess($id,$access);
		
	}else{
		
		if( $USER->archetype == $CFG->userTypeManager) {
		
			$managerCourseArr = getManagerCourse();
			//pr($managerCourseArr);die;courselaunch.php
			if(!in_array($id, $managerCourseArr)){
				redirect($CFG->wwwroot."/course/index.php");
			}
		}elseif($USER->archetype == $CFG->userTypeStudent){
			
			$usercourse = getAssignedCourseIdForUser($userId);
			 
			if(!in_array($id, $usercourse)){
				redirect($CFG->wwwroot);
			}
		}
	}
	
	if($id == 0){
		redirect($CFG->wwwroot);
	}

    $header = $SITE->fullname.": ".get_string('preview');
    $PAGE->set_title($header);
	$PAGE->set_heading($header);
	$PAGE->set_pagelayout('globaladmin');
	$PAGE->navbar->add(get_string('managecourses'), new moodle_url($CFG->wwwroot.'/course/index.php'));
    $PAGE->navbar->add(get_string('launchcourse','course'));

    if (empty($CFG->loginhttps)) {
        $securewwwroot = $CFG->wwwroot;
    } else {
        $securewwwroot = str_replace('http:','https:',$CFG->wwwroot);
    }

    $returnurl = new moodle_url('/course/courselaunch.php', array('id' => $id, 'sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'page'=>$page));

    echo $OUTPUT->header();

    $columns = array('scormname', 'scormsummary', 'lastaccessed', 'timespent', 'score');

    foreach ($columns as $column) {
        $string[$column] = getLearnerCourseFieldName($column);
        if ($sort != $column) {
            $columnicon = "";
            if ($column == "timecreated") {
                $columndir = "DESC";
            } else {
                $columndir = "ASC";
            }
        } else {
            $columndir = $dir == "ASC" ? "DESC":"ASC";
            if ($column == "timecreated") {
                $columnicon = ($dir == "ASC") ? "sort_desc" : "sort_asc";
            } else {
                $columnicon = ($dir == "ASC") ? "sort_asc" : "sort_desc";
            }
            $columnicon = "<img class='iconsort' src=\"" . $OUTPUT->pix_url('t/' . $columnicon) . "\" alt=\"\" />";

        }
		
        if(in_array($column, array('scormname', 'scormsummary', 'lastaccessed', 'timespent', 'score'))){
          $$column = $string[$column];
		}else{
		  $$column = "<a href=\"courselaunch.php?sort=$column&amp;dir=$columndir&amp;id=$id\">".$string[$column]."</a>$columnicon";
		}
    }

	 $query = "SELECT mc.* FROM {$CFG->prefix}course mc WHERE mc.id = '".$id."' AND mc.id NOT IN (1) AND mc.is_active = '1' AND mc.deleted = '0' ";
	 $courseArr = $DB->get_records_sql($query);
	 $expired = 0;
		if($courseArr[$id]->enddate != '' && $USER->archetype == $CFG->userTypeStudent){
			if(($courseArr[$id]->enddate+86399) < strtotime('now')){
				$expired = 1;
			}
		}
	 $courseCnt = count($courseArr);
	 $baseurl = new moodle_url('/course/courselaunch.php', array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'id' => $id));

	 $html = '';
	 $outerDivStart = "<div class=''>";
	 $outerDivEnd = "</div>";
    
	 $headerHtml = getModuleHeaderHtml($courseArr[$id], $CFG->courseModule);
	 $html .= $headerHtml;
	
	 
	 
	 if ($courseCnt > 0) {
	 
	 
	  
            $i=0;
			//pr( $courseArr);die;
			
			foreach ($courseArr as $courses) {
			
				$i++;
				$courseformat = 'topics';
				$courseId = $courses->id;
					
				  if($courseformat == 'topics'){	

					            $sectionArr = getTopicsSection($courseId, $userId);
								$sectionHtml = '';
								
								//pr($sectionArr);
								if(count($sectionArr) > 0 ){
								
									$html .= $outerDivStart;
									$htmlDiv = '';
									$html .= html_writer::start_tag('div', array('class'=>'no-overflow','style'=>'border-style:none'));	
									$courseDiv = '';
									$courseDiv .= '<div class="d-box" id="dbox'.$courseId.'" style="display:block !important;border-style: none !important;">';
								
								    if($courseTypeId == $CFG->courseTypeClassroom ){
									   $courseDiv .= '<div class="assetlabel" >'.get_string('assetlabel','learnercourse').': </div>'; 
									   
									}
		
								    foreach($sectionArr as $sectionId=>$sections){
									
												$table = new html_table();
				
												$table->head = array ();
												$table->colclasses = array();
												$table->head[] = get_string('title','learnercourse');
												$table->attributes['class'] = 'admintable generaltable';
												$table->colclasses[] = 'leftalign  w250';
												$table->head[] = get_string('summary','learnercourse');
												$table->colclasses[] = 'leftalign w600';
													
												if($courseTypeId == $CFG->courseTypeClassroom ){
												 $table->head[] = get_string('resourcetype','learnercourse');
												 $table->colclasses[] = 'centeralign w150';
												}else{
												  $table->head[] = get_string('action');
												  $table->colclasses[] = 'centeralign';
												}
												
												
												//pr($learnercourses);
												$table->id = "learnercourses";
											
												$buttons = array();
												$lastcolumn = '';
												$curtime = time();
												
									
									            $sectionDiv = '';
									           $sectionName = $sections->sectionname;
											      $sequence = $sections->sequence;
										    $sectionSummary = $sections->summary;
										      $topicsCourse = $sections->courseModule;
										   
										       
								                $sectionHtml .= '<div class="section-box" >'; // start section div
      
											if(count($sections->courseModule) > 0){
											
											
											  $sectionDiv .= '<div class="c-box" id="cbox'.$sectionId.'" style="display:block !important;">';
											   foreach($sections->courseModule as $cmId=>$courseModule){

												  $topicsCourse = $courseModule['topicCourse'];
												  //pr($topicsCourse);
												  if(count($topicsCourse)>0){
														   foreach($topicsCourse as $topicsCourseArr){
																$coursetype = $topicsCourseArr->coursetype;
																
																if($coursetype == 'scorm'){
																		$assetIconClass = 'scorm';
																		$scormid = $topicsCourseArr->scormid;
																		$scormtitle = $topicsCourseArr->scormname;
																		$scormdiscription = strip_tags($topicsCourseArr->scormsummary);
																		$categoryname = $topicsCourseArr->categoryname;
																		$timecreated = $topicsCourseArr->ctimecreated;
																		$timespentVal = $topicsCourseArr->timespent;
																		$coursesScore = $topicsCourseArr->score;
																		$scormStatus = $topicsCourseArr->scormstatus;
																		$lastAccessedTime = $topicsCourseArr->lastaccessed;
																		if($lastAccessedTime){
																			$remainAccessedTime = $curtime - $lastAccessedTime;
																			$lastAccessed = timePassed($remainAccessedTime);
																		}else{
																			 $lastAccessed = get_string('notaccessed','learnercourse');
																		}
										
																		if($timespentVal){
																		   $timeSpentResult = convertCourseSpentTime($timespentVal);
																		   $timeSpentResult = str_replace(get_string('ago','learnercourse'),'',$timeSpentResult);
																		}else{
																			$timeSpentResult = '---';
																		}   
											
																		$scoreResult = $coursesScore!=''?($coursesScore." ".get_string('points','learnercourse')):'---';
																		
																		
																		if($scormStatus == ''){
																		   $scormStatus = get_string('notstarted','learnercourse');
																		 }elseif($scormStatus == 'incomplete'){
																		   $scormStatus = get_string('inprogress','learnercourse');
																		 }elseif(in_array($scormStatus, array('passed','completed'))){
																		   $scormStatus = get_string('completed','learnercourse');  
																		 }elseif($scormStatus == 'failed'){
																		   $scormStatus = get_string('completed','learnercourse');  
																		 }
																		 
																		
																		$scormCourseModuleId = getScormCourseModuleId($scormid, $courseId);
																		$scodata = $DB->get_record_sql("SELECT * FROM mdl_scorm_scoes WHERE scorm = $scormid ORDER BY sortorder DESC");
																		$scormdata = $DB->get_record('scorm',array('id'=>$scormid));
																		$launchurl = $CFG->wwwroot.'/mod/scorm/player.php?a='.$scormid.'&cm='.$scormCourseModuleId.'&scoid='.$scodata->id.'&currentorg='.$scodata->organization.'&newattempt=on&display=popup&mode=normal';
																		
																		//$scormLaunch = "<a href=\"../mod/scorm/view.php?id=$scormCourseModuleId\" class = \"launch_icon\">".get_string('launch','learnercourse')."</a>";

																		$scormLaunch = "<a href=\"javascript:void(0);\" onclick=\"window.open('".$launchurl."', '', 'width=".$scormdata->width.",height=".$scormdata->height.",toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \" class = \"launch_icon\">".get_string('launch','learnercourse')."</a>";

																		$row = array ();
											
																		//$row[] = "<span class = '".$assetIconClass."'></span>"."<a href=\"../mod/scorm/view.php?id=$scormCourseModuleId\">".$scormtitle."</a>";
																		$row[] = "<span class = '".$assetIconClass."'></span>".$scormtitle;
																		$row[] = $scormdiscription;
																		//$row[] = $timeSpentResult;
																		/*
																		$row[] = $scormStatus;
																		$row[] = $scoreResult;
																		$row[] = $lastAccessed;
																		*/
																		if($expired == 1){
																			$row[] = get_string('expired');
																		}else{
																			$row[] = $scormLaunch;
																		}
																		$table->data[] = $row;
																		
																}elseif($coursetype == 'resource'){
																  
																  //pr($topicsCourseArr);
																	$resourceCourseId = $topicsCourseArr->id;
																	$resourceid = $topicsCourseArr->resourceid;
																	$cmid = $topicsCourseArr->cmid;
																	
																	$resoursename = $topicsCourseArr->resoursename;
																	$resourceType = $topicsCourseArr->resource_type;
																	$resourcesummary = strip_tags($topicsCourseArr->resourcesummary);
																	$categoryname = $topicsCourseArr->categoryname;
																	$lastAccessedTime = $topicsCourseArr->lastaccessed;
																	
																	$lastaccessed = $lastaccessed?getDateFormat($lastaccessed, "d/m/Y"):'---';
																	if($lastAccessedTime){
																		$remainAccessedTime = $curtime - $lastAccessedTime;
																		$lastAccessed = timePassed($remainAccessedTime);
																	}else{
																		 $lastAccessed = get_string('notaccessed','learnercourse');
																	}
																						
																
																   $styDownload = '';
																   
																   $filedownloaded = isUserAccessedCourse($userId, $courseId, $resourceid);
																   
																   if($filedownloaded){
																	 $styDownload = "downloaded";
																   }
																   
																   if($styDownload){
																     $resourceStatus = get_string('downloaded','learnercourse');
																   }else{
																     $resourceStatus = get_string('notdownloaded','learnercourse');
																   }
														
																	if (!$cm = get_coursemodule_from_id('resource', $cmid)) {
																		resource_redirect_if_migrated(0, $id);
																		print_error('invalidcoursemodule');
																	}
																	
																	
																	$resource = $DB->get_record('resource', array('id'=>$cm->instance), '*', MUST_EXIST);
																	if($resource->is_weblink==1){
																		$fullurl = trim($resource->weblink_url);
																		if($fullurl != ''){
																			$fullurl = getWeblinkLaunchURL($fullurl);
																			$launchClass = 'launch_icon';
																			$assetIconClass = 'scorm';
																			$fileStatus = "<a href='javascript:void(0);' onclick=\"setCourseLastAccess($userId, $courseId, $resourceid);window.open('".$fullurl."', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \" title='".get_string('launch','learnercourse')."' class='".$launchClass."'    id='coursestatus_".$courseId."_".$resourceid."'>".get_string('launch','learnercourse')."</a>";
																			//$fileStatus = "<a href='javascript:;' onclick=\"setCourseLastAccess($USER->id, $assetArray->courseid, $assetArray->instanceid);window.open('".$fullurl."', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \" title='".get_string('launch','learnercourse')."' class='".$launchClass."'   id='coursestatus_".$programOrCourseArray->id."_".$assetArray->instanceid."'>".get_string('download','learnercourse')."</a>";
																			
																		}
																	}
																	else{
																	$context = context_module::instance($cm->id);
																	$fs = get_file_storage();
																	$files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
																	$isRExist = isResourceFileExist($cm->id);
																	//if (count($files) < 1) {
																	if(!$isRExist){
																		$fileStatus = "<a href='javascript:void(0);' onclick='alert(\"".get_string('clamdeletedfile')."\")'  title='".get_string('download','learnercourse')."' class='download_icon ".$styDownload."'   id='coursestatus_".$courseId."_".$resourceid."'>".get_string('download','learnercourse')."</a>";
																	} else {
																		$file = reset($files);
																		unset($files);
																		
																		$path = '/'.$context->id.'/mod_resource/content/'.$resource->revision.$file->get_filepath().$file->get_filename();
																		$fullurl = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
																		$resourseType = end(explode('.',$path));
																		$assetIconClass = getFileClass($resourseType);
															   
																		$fileStatus = "<a href='javascript:void(0);' onclick=\"setCourseLastAccess($userId, $courseId, $resourceid);window.open('".$fullurl."', '', 'width=620,height=450,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes'); \" title='".get_string('download','learnercourse')."' class='download_icon ".$styDownload."'   id='coursestatus_".$courseId."_".$resourceid."'>".get_string('download','learnercourse')."</a>";
																	}
																	}
															
																	$row = array();
																	
																	$row[] = "<span class = '".$assetIconClass."'></span>".$resoursename;
																	$row[] = $resourcesummary;
																	if($courseTypeId == $CFG->courseTypeClassroom ){
																	  $row[] = $resourceType;
																	}else{
																		if($expired == 1){
																			$row[] = get_string('expired');
																		}else{
																			 $row[] = $fileStatus;
																		}
																	}
																	/*
																	$row[] = $resourceStatus;
																	$row[] = get_string('NA','learnercourse');
																	$row[] = $lastAccessed;
																	*/
																	
																	$table->data[] = $row;
																	
																}
																
															
														 } // end foreach for topicsCourse	
								
														 }else{  // end if for topicsCourse
														 
														   // $courseDiv .= $OUTPUT->heading(get_string('nolearnercoursesfound','learnercourse'));
														 }
											      }
												  
												   
												   if (!empty($table)) {
														$sectionDiv .= html_writer::table($table);
												   }
												   
												   $sectionDiv .= '</div>';
												   $sectionHtml .= $sectionDiv;
											
											}

									        $sectionHtml .= '</div>'; // end section html
										
									} // end sectionArr foreach
									
									$courseDiv .= $sectionHtml; 
									
									$courseDiv .= '</div>';
									//$htmlDiv .= $courseDiv.'</div>';
									$htmlDiv .= $courseDiv;
									
									$html .= $htmlDiv;
									
									$html .= html_writer::end_tag('div');
									$html .= $outerDivEnd;
									
								} // end sectionArr if

						
				    }
				
			}
			
		  
	  }
	  
	  echo $html;
	
	
 ?>
 
 
 <script type="text/javascript">
 
 
 function setCourseLastAccess(userid, courseid, resourceid){
	
     
	   $.ajax({
	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=setCourseLastAccess&userid='+userid+"&courseid="+courseid+"&resourceid="+resourceid,
		dataType:'json',
		success:function(data){
		  
		   var success = data.success;
		     var error = data.error;
			   if(success == 1){
			      var course_resource_id = data.response;
				  $('#coursestatus_'+course_resource_id).addClass('downloaded');
			   }else{
				 alert(error);
			   }
		}
	  
	  });

	   $.ajax({
	  
	    url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
		type:'POST',
		data:'action=setCourseBadge&userid='+userid+"&courseid="+courseid+"&resourceid="+resourceid,
		dataType:'json',
		success:function(data){  
		}
	  
	  });
	
 }
	
 
 $(document).ready(function(){
 
      $('.cbox').click(function(){
	         var courseId = $(this).attr('rel');
			 //$('.cbox').removeClass('minus');
			 $('[id^="cbox"]').hide();
			 $('[id^="dbox"]').not('#dbox'+courseId).hide();
	         $('#dbox'+courseId).slideToggle();
			 
			 $('.cbox').not('[rel="'+courseId+'"]').removeClass('minus');
			 $(this).toggleClass('minus');
			// $(window).scrollTop($('#dbox'+courseId).offset().top);
	  
	  });
	  
	   $('.sbox').click(function(){
	         var sectionId = $(this).attr('rel');
			 $('[id^="cbox"]').not('#cbox'+sectionId).hide();
	         $('#cbox'+sectionId).slideToggle();
			 //$(this).parent('.no-course').find('.right-icon').toggleClass('minus');
			// $(window).scrollTop($('#dbox'+courseId).offset().top);
	  
	  });
	  
 });

 $('.empty').parents('.section-box').hide();
 </script>
 <?php	

 echo $OUTPUT->footer();
 