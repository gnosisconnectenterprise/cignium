<?php

	/**
		* Custom module - Classroom Course Report Print Page
		* Date Creation - 28/04/2015
		* Date Modification : 28/04/2015
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
	
	require_once("../config.php");
	
	global $DB,$CFG,$USER, $SITE;
	
	$sort    = optional_param('sort', '', PARAM_ALPHANUM);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 0, PARAM_INT);        // how many per page
	$export  = optional_param('action', '', PARAM_ALPHANUM);
	$type  = optional_param('type', '-1', PARAM_RAW);
	$status  = optional_param('status', '-1', PARAM_RAW);
	$back = optional_param('back', 1, PARAM_INT);
	$cType = optional_param('ctype', 2, PARAM_INT);
	$course = optional_param('course', '-1', PARAM_INT);
	$classes = optional_param('classes', '-1', PARAM_INT);
	  
			  
	$sDate = '';
	$eDate = '';
		
	/*$sDate =  $CFG->reportSearchStartDate;
	$eDate = $CFG->reportSearchEndDate*/

	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);
	

    if( $USER->archetype == $CFG->userTypeStudent ) {
	  $userid = $USER->id;
	}else{
	  $userid = required_param('userid', PARAM_INT);
	}
	
	$paramArray = array(
	'back' => $back,
	'type' => $type,
	'status' => $status,
	'back' => $back,
	'userid' => $userid,
	'ctype' => $cType,		
	'course' => $course,
	'classes' => $classes,
	'startDate' => $sStartDate,
	'endDate' => $sEndDate
    );
  
	
	$removeKeyArray = array();
	checkLogin();
	
	require_once ($CFG->dirroot . '/mod/scheduler/lib.php');
	$header = $SITE->fullname.": ".get_string('overallclassroomreport','scheduler');
	$PAGE->set_title($header);
	$PAGE->set_heading($header);
	$PAGE->set_pagelayout('print');

	$courseHTML = '';
	$reportContentCSV = '';
	$reportContentPDF = '';
    if($export && in_array($export, array('exportcsv', 'exportpdf', 'print'))){

		$classroomCourseReport = getClassroomCourseReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
		$courseHTML = $classroomCourseReport->courseHTML;
		$reportContentCSV = $classroomCourseReport->reportContentCSV;
		$reportContentPDF = $classroomCourseReport->reportContentPDF;
	
    }else{
	    redirect(new moodle_url('/'));
	}

	/* bof export to csv */
	
    if(isset($export) && $export == 'exportcsv') {
	
		$filepath = $CFG->dirroot."/local/reportexport/temp";		
		chmod($filepath, 0777);		
		$filename = str_replace(' ', '_', get_string('overallclassroomreport','scheduler'))."_".date("m-d-Y").".csv";  
		$filepathname = $filepath.'/'.$filename;
		unlink($filepathname);
		$handler = fopen($filepathname, "w");
		fwrite($handler, $reportContentCSV);
   	    exportCSV($filepathname);
	}
	/* eof export to csv */	
	
	
	/* bof export to pdf */
    if(isset($export) && $export == 'exportpdf') {
			
		$filename = str_replace(' ', '_', get_string('overallclassroomreport','scheduler'))."_".date("m-d-Y").".pdf";
		exportPDF($filename, $reportContentPDF, '', get_string('overallclassroomreport','scheduler'));
		
	}
	/* eof export to pdf */	
	   
	echo $OUTPUT->header(); 
	echo $courseHTML;	
?>
<style>
#page { margin: 20px auto 0;}
</style>