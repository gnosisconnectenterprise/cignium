<?php

/**
	* Custom module - Single Classroom Report Print Page
	* Date Creation - 17/11/2014
	* Date Modification : 17/11/2014
	* Created By : Rajesh Kumar
	* Last Modified By : Rajesh Kumar
*/
	

	require_once("../config.php");
	global $DB,$CFG,$USER;
	
	$sort    = optional_param('sort', 'COALESCE(NULLIF(mu.firstname, ""), mu.lastname), mu.lastname', PARAM_ALPHANUM);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 0, PARAM_INT);        // how many per page
	$cid     = required_param('cid', PARAM_INT);
	$classId     = optional_param('class_id', 0, PARAM_INT);
	$export  = optional_param('action', '', PARAM_ALPHANUM);
	$type = optional_param('type', '-1', PARAM_RAW);
	
	
	$paramArray = array(
	'cid' => $cid,
	'export' => $export,
	'class_id' => $classId,
	'type' => $type,
  );
  
	
	$removeKeyArray = array('perpage');

	if($cid >0){
		$course = $DB->get_record('course',array('id'=> $cid));
	}else{
		redirect($CFG->wwwroot);
	}
	
	$haveAccess = haveClassroomCourseAccess($cid);
	if($haveAccess == 0){
	  redirect($CFG->wwwroot);
	}
	
	
	require_login(); 
	
	$PAGE->set_pagelayout('print');
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('overalluserprogressreport','classroomreport'));

	
   
    $courseHTML = '';
	$reportContentCSV = '';
	$reportContentPDF = '';
    if($export && in_array($export, array('exportcsv', 'exportpdf', 'print'))){

		$courseVsMultipleUserReport = getClassroomVsMultipleUserReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
		$courseHTML = $courseVsMultipleUserReport->courseHTML;
		$reportContentCSV = $courseVsMultipleUserReport->reportContentCSV;
		$reportContentPDF = $courseVsMultipleUserReport->reportContentPDF;
	
    }else{
	    redirect(new moodle_url('/'));
	}

	/* bof export to csv */
	
    if(isset($export) && $export == 'exportcsv') {
	
		$filepath = $CFG->dirroot."/local/reportexport/temp";		
		chmod($filepath, 0777);		
		$filename = str_replace(' ', '_', get_string('overalluserprogressreport','classroomreport'))."_".date("m-d-Y").".csv";  
		$filepathname = $filepath.'/'.$filename;
		unlink($filepathname);
		$handler = fopen($filepathname, "w");
		fwrite($handler, $reportContentCSV);
   	    exportCSV($filepathname);
	}
	/* eof export to csv */	
	
	
	/* bof export to pdf */
    if(isset($export) && $export == 'exportpdf') {
			
		$filename = str_replace(' ', '_', get_string('overalluserprogressreport','classroomreport'))."_".date("m-d-Y").".pdf";
		exportPDF($filename, $reportContentPDF, '', get_string('overalluserprogressreport','classroomreport'));
		
	}
	/* eof export to pdf */	
		
	   
	echo $OUTPUT->header(); 
	echo $courseHTML;

?>
<style>
#page { margin: 20px auto 0;}
</style>
