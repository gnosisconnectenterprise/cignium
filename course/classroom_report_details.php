<?php

	/**
		* Custom module - Signle Course Report Detail Page
		* Date Creation - 02/07/2014
		* Date Modification : 01/07/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/

	
	require_once("../config.php");

	global $DB,$CFG,$USER, $SITE;
	
	$sort    = optional_param('sort', 'COALESCE(NULLIF(mu.firstname, ""), mu.lastname), mu.lastname', PARAM_ALPHANUM);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 10, PARAM_INT);        // how many per page
	$cid     = required_param('cid', PARAM_INT);
	$classId     = optional_param('class_id', 0, PARAM_INT);
	$export  = optional_param('action', '', PARAM_ALPHANUM);
	$type  = optional_param('type', '-1', PARAM_RAW);
	$status  = optional_param('status', '-1', PARAM_RAW);
	$back  = optional_param('back', 2, PARAM_INT);
			  
	
	$paramArray = array(
	'cid' => $cid,
	'type' => $type,
	'status' => $status,
	'back' => $back,
	'class_id' => $classId,
    );
  
	
	$removeKeyArray = array('perpage');

	if($cid >0){
		$course = $DB->get_record('course',array('id'=> $cid));
		//$course = $DB->get_record('course',array('id'=> $cid), '*', MUST_EXIST);
	}else{
		redirect($CFG->wwwroot);
	}
	
	$haveAccess = haveClassroomCourseAccess($cid);
	if($haveAccess == 0){
	  redirect($CFG->wwwroot);
	}
	
		
	//require_login(); 
	checkLogin();
	
	
	$PAGE->set_pagelayout('globaladmin');
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('viewreportdetails','multicoursereport'));
	//$PAGE->navbar->add(get_string('reports','multicoursereport'), new moodle_url($CFG->wwwroot.'/my/adminreportdashboard.php'));
	$PAGE->navbar->add(get_string('reports','multicoursereport'));
	
	
    if($back == 1){
	  $PAGE->navbar->add(get_string('coursereport','classroomreport'), new moodle_url($CFG->wwwroot.'/course/course_report.php'));
    }else{
	  $PAGE->navbar->add(get_string('courseusagesreport','classroomreport'), new moodle_url($CFG->wwwroot.'/course/'.$CFG->pageMultiClassroomReport)); 
    } 

	//$PAGE->navbar->add(get_string('viewreportdetails','classroomreport'));
	$PAGE->navbar->add(getCourses($cid));
	
	$courseVsMultipleUserReport = getClassroomVsMultipleUserReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
	$courseHTML = $courseVsMultipleUserReport->courseHTML;
		   
	echo $OUTPUT->header(); 
	echo $courseHTML;
	echo includeGraphFiles(get_string('overalluserprogressreport','classroomreport'));
	?>
    
<!--    <script>
      $(document).ready(function(){
	  
		 $('#classname').change(function(){
			  var classId = $(this).val();
			  url = '<?php echo $CFG->wwwrot.$_SERVER['PHP_SELF'];?>?cid=<?php echo $cid;?>&class_id='+classId;
			  window.location.href = url;
		 });
	  });
	  
	  
    </script>-->
<?php echo $OUTPUT->footer();
	