<?php

	/**
		* Custom module - Multi Course Report Page
		* Date Creation - 01/07/2014
		* Date Modification : 01/07/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
	

	require_once("../config.php");

	global $DB,$CFG,$USER;
	
		
	//require_login(); 
	checkLogin();
	
	
	
	$PAGE->set_pagelayout('globaladmin');
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('courseusagesreport','classroomreport'));
	//$PAGE->navbar->add(get_string('reports','classroomreport'), new moodle_url($CFG->wwwroot.'/my/adminreportdashboard.php'));
	$PAGE->navbar->add(get_string('reports','classroomreport'));
	$PAGE->navbar->add(get_string('courseusagesreport','classroomreport'));

	$sort    = optional_param('sort', 'fullname', PARAM_RAW);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 10, PARAM_INT);        // how many per page
	$export = optional_param('action', '', PARAM_ALPHANUM);
	$pageURL = '/course/multi_classroom_report.php';
	checkPageIsNumeric($pageURL,$_REQUEST['page']);
	
				
	$sDate = '';
	$eDate = '';
		
	/*$sDate =  $CFG->reportSearchStartDate;
	$eDate = $CFG->reportSearchEndDate*/
	$sclasses    = optional_param('classes', '-1', PARAM_RAW);   
	$sCourse        = optional_param('course', '-1', PARAM_RAW);       
	$sType          = optional_param('type', '-1', PARAM_RAW); 
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate       = optional_param('endDate', $eDate, PARAM_RAW);
	
	$paramArray = array(
					'classes' => $sclasses,
					'course' => $sCourse,
					'type' => $sType,
					'startDate' => $sStartDate,
					'endDate' => $sEndDate
				  );
	
	$removeKeyArray = array();

  
	echo $OUTPUT->header(); 
	$courseUsagesReport = getClassroomUsagesReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
	$courseHTML = $courseUsagesReport->courseHTML;
	echo $courseHTML;
	echo includeGraphFiles(get_string('courseusagesreport','classroomreport'));
	echo $OUTPUT->footer();

?>