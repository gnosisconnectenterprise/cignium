<?php

	/**
		* Custom module - Signle Course Report Detail Page
		* Date Creation - 02/07/2014
		* Date Modification : 01/07/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/

	
	require_once("../config.php");

	global $DB,$CFG,$USER, $SITE;
	
	$sort    = optional_param('sort', 'COALESCE(NULLIF(mu.firstname, ""), mu.lastname), mu.lastname', PARAM_ALPHANUM);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 10, PARAM_INT);        // how many per page
	$cid     = required_param('cid', PARAM_INT);
	$export  = optional_param('action', '', PARAM_ALPHANUM);
	$type  = optional_param('type', '', PARAM_RAW);
	$back  = optional_param('back', 2, PARAM_INT);
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);
			  

	$paramArray = array(
	'cid' => $cid,
	'type' => $type,
	'back' => $back,
	'startDate' => $sStartDate,
	'endDate' => $sEndDate
    );
  
	
	$removeKeyArray = array('perpage');

	if($cid >0){
		$course = $DB->get_record('course',array('id'=> $cid));
		//$course = $DB->get_record('course',array('id'=> $cid), '*', MUST_EXIST);
	}else{
		redirect($CFG->wwwroot);
	}
	
	//$userrole =  getUserRole($USER->id);
	if( $USER->archetype == $CFG->userTypeStudent ) {
		$groupCourses = checkOwnerAccess($USER->id);
		if(!$groupCourses)
		{
			redirect($CFG->wwwroot);
		}
	}
	
		
	//require_login(); 
	checkLogin();
	
	
	$PAGE->set_pagelayout('globaladmin');
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('viewreportdetails','multicoursereport'));
	//$PAGE->navbar->add(get_string('reports','multicoursereport'), new moodle_url($CFG->wwwroot.'/my/adminreportdashboard.php'));
	$PAGE->navbar->add(get_string('reports','multicoursereport'));
	
	
    if($back == 1){
	  $PAGE->navbar->add(get_string('coursereport','multicoursereport'), new moodle_url($CFG->wwwroot.'/course/course_report.php'));
    }else{
	  $PAGE->navbar->add(get_string('courseusagesreport','multicoursereport'), new moodle_url($CFG->wwwroot.'/course/multicoursereport.php')); 
    } 

	//$PAGE->navbar->add(get_string('viewreportdetails','multicoursereport'));
	$PAGE->navbar->add(getCourses($cid));
	
	$courseVsMultipleUserReport = getCourseVsMultipleUserReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
	$courseHTML = $courseVsMultipleUserReport->courseHTML;
		   
	echo $OUTPUT->header(); 
	echo $courseHTML;
	

	echo includeGraphFiles(get_string('overalluserprogressreport','singlecoursereport'));
	
	echo $OUTPUT->footer();	
	//$courseHTML .= '<input type = "button" value = "'.get_string('back','multicoursereport').'" onclick="location.href=\''.$CFG->wwwroot.'/course/multicoursereport.php\';">';
