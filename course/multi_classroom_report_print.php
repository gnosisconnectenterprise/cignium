<?php

/**
	* Custom module - Multi Course Report Page
	* Date Creation - 01/07/2014
	* Date Modification : 01/07/2014
	* Created By : Rajesh Kumar
	* Last Modified By : Rajesh Kumar
*/
	

	require_once("../config.php");
	$site = get_site();
	global $DB,$CFG,$USER;

		
	//require_login(); 
	checkLogin();
	
	
	$PAGE->set_pagelayout('print');
	$PAGE->set_heading($site->fullname);
	$PAGE->set_title($site->fullname.": ".get_string('courseusagesreport','classroomreport'));

	
	$sort    = optional_param('sort', 'fullname', PARAM_ALPHANUM);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 0, PARAM_INT);        // how many per page
	$export = optional_param('action', '', PARAM_ALPHANUM);
	
	
				
	$sDate = '';
	$eDate = '';
		
	/*$sDate =  $CFG->reportSearchStartDate;
	$eDate = $CFG->reportSearchEndDate*/
	
	$sDepartment    = optional_param('department', '-1', PARAM_RAW);
	$sTeam          = optional_param('team', '-1', PARAM_RAW);    
    $sProgram       = optional_param('program', '-1', PARAM_RAW); 	
	$sCourse         = optional_param('course', '-1', PARAM_RAW);       
	$sType          = optional_param('type', '-1', PARAM_RAW); 
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);
	$sClasses      = optional_param('classes', $eDate, PARAM_RAW);
	$export = optional_param('action', '', PARAM_ALPHANUM);
	
	
	$paramArray = array(
					'sort' => $sort,
					'department' => $sDepartment,
					'team' => $sTeam,
					'program' => $sProgram,
					'course' => $sCourse,
					'type' => $sType,
					'classes' => $sClasses,
					'startDate' => $sStartDate,
					'endDate' => $sEndDate
				  );
	
	$removeKeyArray = array();

   
    $courseHTML = '';
	$reportContentCSV = '';
	$reportContentPDF = '';
    if($export && in_array($export, array('exportcsv', 'exportpdf', 'print'))){

		$courseUsagesReport = getClassroomUsagesReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
		$courseHTML			= $courseUsagesReport->courseHTML;
		$reportContentCSV	= $courseUsagesReport->reportContentCSV;
		$reportContentPDF	= $courseUsagesReport->reportContentPDF;
	
    }else{
	    redirect(new moodle_url('/'));
	}

	/* bof export to csv */
	
    if(isset($export) && $export == 'exportcsv') {
	
		$filepath = $CFG->dirroot."/local/reportexport/temp";		
		chmod($filepath, 0777);		
		$filename = str_replace(' ', '_', get_string('courseusagesreport','classroomreport'))."_".date("m-d-Y").".csv";  
		$filepathname = $filepath.'/'.$filename;
		unlink($filepathname);
		$handler = fopen($filepathname, "w");
		fwrite($handler, $reportContentCSV);
   	    exportCSV($filepathname);
	}
	/* eof export to csv */	
	
	
	/* bof export to pdf */
    if(isset($export) && $export == 'exportpdf') {
			
		$filename = str_replace(' ', '_', get_string('courseusagesreport','classroomreport'))."_".date("m-d-Y").".pdf";
		exportPDF($filename, $reportContentPDF, '', get_string('courseusagesreport','classroomreport'));
		
	}
	/* eof export to pdf */	
		
	   
	echo $OUTPUT->header(); 
	echo $courseHTML;

?>
<style>
#page { margin: 20px auto 0;}
</style>