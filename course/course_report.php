<?php

	/**
		* Custom module - Course Report Page
		* Date Creation - 21/07/2014
		* Date Modification : 21/07/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
	

	require_once("../config.php");


	global $DB,$CFG,$USER;
	
	//$userrole =  getUserRole($USER->id);
	if( $USER->archetype == $CFG->userTypeStudent ) {
		$groupCourses = checkOwnerAccess($USER->id);
		if(!$groupCourses)
		{
			redirect(new moodle_url('/my/learnerdashboard.php'));
		}
	//if(in_array($userrole, $CFG->customstudentroleid)){  // added by rajesh
	 //redirect(new moodle_url('/my/learnerdashboard.php'));
	}
	
	
	//require_login(); 
	checkLogin();
	
	
	$PAGE->set_pagelayout('globaladmin');
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('coursereport','multicoursereport'));
	//$PAGE->navbar->add(get_string('reports','multicoursereport'), new moodle_url($CFG->wwwroot.'/my/adminreportdashboard.php'));
	$PAGE->navbar->add(get_string('reports','multicoursereport'));
	$PAGE->navbar->add(get_string('coursereport','multicoursereport'));

	$sort    = optional_param('sort', 'mc.fullname', PARAM_RAW);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 10, PARAM_INT);        // how many per page
	$export = optional_param('action', '', PARAM_ALPHANUM);
	$pageURL = '/course/course_report.php';
	checkPageIsNumeric($pageURL,$_REQUEST['page']);
				
	$sDate = '';
	$eDate = '';
		
	/*$sDate =  $CFG->reportSearchStartDate;
	$eDate = $CFG->reportSearchEndDate*/
	$sCategory          = optional_param('category', '-1', PARAM_RAW); 
	//$sPublish          = optional_param('publish', '-1', PARAM_RAW); 
	$sActive          = optional_param('active', '-1', PARAM_RAW); 
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);
	
	
	$paramArray = array(
					'category' => $sCategory,
					//'publish' => $sPublish,
					'active' => $sActive,
					'startDate' => $sStartDate,
					'endDate' => $sEndDate
				  );
	
	$removeKeyArray = array();

    $courseReport = getCourseReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
	$courseHTML = $courseReport->courseHTML;
	   
	echo $OUTPUT->header(); 
	echo $OUTPUT->skip_link_target();
	echo $courseHTML;
	
    echo includeGraphFiles(get_string('coursereport','multicoursereport'));


echo $OUTPUT->footer();
