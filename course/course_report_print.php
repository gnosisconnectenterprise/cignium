<?php

/**
	* Custom module - Multi Course Report Page
	* Date Creation - 01/07/2014
	* Date Modification : 01/07/2014
	* Created By : Rajesh Kumar
	* Last Modified By : Rajesh Kumar
*/
	

	require_once("../config.php");
	$site = get_site();
	global $DB,$CFG,$USER;
	$userrole =  getUserRole($USER->id);
	if( $USER->archetype == $CFG->userTypeStudent ) {
		$groupCourses = checkOwnerAccess($USER->id);
		if(!$groupCourses)
		{
			redirect(new moodle_url('/my/learnerdashboard.php'));
		}
	}
	checkLogin();
	$PAGE->set_pagelayout('print');
	$PAGE->set_heading($site->fullname);
	$PAGE->set_title($site->fullname.": ".get_string('coursereport','multicoursereport'));
	$sort    = optional_param('sort', 'mc.fullname', PARAM_RAW);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 0, PARAM_INT);        // how many per page
	$export = optional_param('action', '', PARAM_ALPHANUM);
	$sDate = '';
	$eDate = '';
	$sCategory          = optional_param('category', '-1', PARAM_RAW); 
	$sPublish          = optional_param('publish', '-1', PARAM_RAW); 
	$sActive          = optional_param('active', '-1', PARAM_RAW); 
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);
	
	$paramArray = array(
					'category' => $sCategory,
					'publish' => $sPublish,
					'active' => $sActive,
					'startDate' => $sStartDate,
					'endDate' => $sEndDate,
					'graphImg' => optional_param('graphImg', '', PARAM_RAW)
				  );
	
	
	$removeKeyArray = array();

   
    $courseHTML = '';
	$reportContentCSV = '';
	$reportContentPDF = '';
    if($export && in_array($export, array('exportcsv', 'exportpdf', 'print'))){

		$courseReport = getCourseReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
		$courseHTML = $courseReport->courseHTML;
		$reportContentCSV = $courseReport->reportContentCSV;
		$reportContentPDF = $courseReport->reportContentPDF;
	
    }else{
	    redirect(new moodle_url('/'));
	}

	/* bof export to csv */
	
    if(isset($export) && $export == 'exportcsv') {
		$filepathname = getpdfCsvFileName('csv', get_string('coursereport','multicoursereport'), $reportContentCSV);
		exportCSV($filepathname);
	}
	/* eof export to csv */	
	
	
	/* bof export to pdf */
    if(isset($export) && $export == 'exportpdf') {
			
		$filename = str_replace(' ', '_', get_string('coursereport','multicoursereport'))."_".date("m-d-Y")."_".$USER->id.".pdf";
		exportPDF($filename, $reportContentPDF, '', get_string('coursereport','multicoursereport'));
		
	}
	
	/* eof export to pdf */	
	echo $OUTPUT->header(); 
	echo $courseHTML;

?>
<style>
#page { margin: 20px auto 0;}
</style>