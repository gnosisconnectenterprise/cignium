<?php
require_once(dirname(__FILE__) . '/../config.php');
GLOBAL $DB, $CFG, $USER;
require_login();
//pr($SESSION);
$site = get_site();
$PAGE->set_url('/course/enrolcourse.php', array('id' => $courseid));
$PAGE->set_pagelayout('classroompopup');
$PAGE->set_title("$site->fullname: $stradduserstogroup");
$PAGE->set_heading($site->fullname);

$elementType = optional_param('element', false, PARAM_RAW);
$assignId = optional_param('assignId', false, PARAM_RAW);
$assignType = optional_param('assigntype', false, PARAM_RAW);
$elementList = optional_param('elementList', false, PARAM_RAW);
$action = optional_param('action', "", PARAM_RAW);
$isRequest = optional_param('is_request', "", PARAM_RAW);

if (($elementType == 'course' && $assignType == 'user') || ($elementType == 'user' && $assignType == 'course')) {
    $showCustomEmailInput = 1;
} else {
    $showCustomEmailInput = 0;
}

$optionList = array();
$label = get_string('noofusers');

if ($elementType == 'team' && $assignType == 'course') {
    if ($action == 'update') {
        $query = "SELECT DISTINCT(g.id),g.name as namewithouttime, IF(((gc.end_date = '') || (gc.end_date = 0) || gc.end_date IS NULL),'', FROM_UNIXTIME(gc.end_date,' - " . $CFG->customDefaultDateFormatForDB . "')) as enddate, CONCAT(IF((d.title IS NULL),'" . get_string("global_team_identifier") . "',''),g.name,IF((d.title IS NULL), IF(((gc.end_date = '') || (gc.end_date = 0) || gc.end_date IS NULL),'', FROM_UNIXTIME(gc.end_date,' - " . $CFG->customDefaultDateFormatForDB . "')), CONCAT('(',d.title,')', IF(((gc.end_date = '') || (gc.end_date = 0) || gc.end_date IS NULL),'', FROM_UNIXTIME(gc.end_date,' - " . $CFG->customDefaultDateFormatForDB . "'))))) AS name FROM mdl_groups_course as gc LEFT JOIN mdl_groups as g ON g.id = gc.groupid LEFT JOIN mdl_group_department as gd ON gd.team_id = g.id LEFT JOIN mdl_department d ON d.deleted = 0 AND d.id = gd.department_id WHERE gc.is_active = 1 AND gc.courseid = " . $assignId . " AND g.id IN (" . $elementList . ") ORDER BY g.name ASC";
    } else {
        $query = "SELECT g.id,g.name as namewithouttime, IF(((gc.end_date = '') || (gc.end_date = 0) || gc.end_date IS NULL),'', FROM_UNIXTIME(gc.end_date,' - " . $CFG->customDefaultDateFormatForDB . "')) as enddate,CONCAT(IF((d.title IS NULL),'" . get_string("global_team_identifier") . "',''),g.name,IF((d.title IS NULL),'',CONCAT('(',d.title,')'))) as name FROM mdl_groups AS g LEFT JOIN mdl_groups_course gc ON (g.id = gc.groupid AND gc.is_active = 1 AND gc.courseid = " . $assignId . ") LEFT JOIN mdl_group_department as gd ON gd.team_id = g.id LEFT JOIN mdl_department d ON d.deleted = 0 AND d.id = gd.department_id WHERE gc.groupid IS NULL AND g.id IN (" . $elementList . ") ORDER BY g.name ASC";
    }
    $optionListArray = $DB->get_records_sql($query);
    $label = get_string('team');
} elseif ($elementType == 'user' && $assignType == 'course') {
    $where = " AND u.id IN (" . $elementList . ")";
    if ($action == 'update') {
        $query = "	SELECT u.id,CONCAT(u.firstname,' ', u.lastname,' (',u.username,')') as namewithouttime,IF(((uc.end_date = '') || (uc.end_date = 0) || uc.end_date IS NULL),'', FROM_UNIXTIME(uc.end_date,' - " . $CFG->customDefaultDateFormatForDB . "')) as enddate,CONCAT(u.firstname,' ', u.lastname,' (',u.username,')',IF(((uc.end_date = '') || (uc.end_date = 0) || uc.end_date IS NULL),'', FROM_UNIXTIME(uc.end_date,' - " . $CFG->customDefaultDateFormatForDB . "')))  AS name,uc.type FROM mdl_user as u LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id LEFT JOIN mdl_role AS r ON ra.roleid = r.id LEFT JOIN mdl_user_course_mapping AS uc ON u.id = uc.userid WHERE u.deleted = 0 AND u.suspended = 0 AND uc.courseid = $assignId AND uc.type = 'user' AND uc.status = 1";
        //$orderBy = " ORDER BY u.firstname ASC";
        $orderBy = "  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
        $query = $query . $where . $orderBy;
    } else {
        $query = "SELECT DISTINCT(u.id), CONCAT(u.firstname,' ', u.lastname,' (',u.username,')') AS name FROM mdl_user AS u  WHERE u.suspended = 0 AND u.deleted = 0 ";
        $orderBy = "  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
        $query = $query . $where . $orderBy;
    }

    $optionListArray = $DB->get_records_sql($query);
    if (empty($optionListArray) && $isRequest == 1) {
        $query = "SELECT u.id, CONCAT(u.firstname,' ', u.lastname,' (',u.username,')') AS namewithouttime, IF(((uc.end_date = '') || (uc.end_date = 0) || uc.end_date IS NULL),'', FROM_UNIXTIME(uc.end_date,' - %b %d, %Y')) AS enddate,CONCAT(u.firstname,' ', u.lastname,' (',u.username,')', IF(((uc.end_date = '') || (uc.end_date = 0) || uc.end_date IS NULL),'', FROM_UNIXTIME(uc.end_date,' - %b %d, %Y'))) AS name FROM mdl_user AS u LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id LEFT JOIN mdl_role AS r ON ra.roleid = r.id LEFT JOIN mdl_groups_members as gm ON gm.userid = u.id AND gm.is_active = 1 LEFT JOIN mdl_groups_course AS uc ON uc.groupid = gm.groupid WHERE u.deleted = 0 AND u.suspended = 0 AND uc.courseid = $assignId AND uc.is_active = 1";
        $orderBy = "  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
        $query = $query . $where . $orderBy;
        $optionListArray = $DB->get_records_sql($query);
    }
    $label = get_string('noofusers');
} elseif ($elementType == 'course' && $assignType == 'user') {
    if ($action == 'update') {
        $where = " AND c.id IN (" . $elementList . ")";
        $query = "SELECT DISTINCT(c.id),CONCAT(c.fullname,IF(((cm.end_date = '') || (cm.end_date = 0) || cm.end_date IS NULL),'', FROM_UNIXTIME(cm.end_date,' - " . $CFG->customDefaultDateFormatForDB . "'))) as name,IF(((cm.end_date = '') || (cm.end_date = 0) || cm.end_date IS NULL),'', FROM_UNIXTIME(cm.end_date,' - " . $CFG->customDefaultDateFormatForDB . "')) as enddate FROM mdl_user_course_mapping as cm LEFT JOIN mdl_course as c ON c.id = cm.courseid";
        $query .= " WHERE cm.status = 1 AND c.id != '' AND cm.userid = " . $assignId . $where;
        $query .= "ORDER BY LOWER(c.fullname) ASC";
    } else {
        $query = "SELECT id,fullname as name FROM mdl_course WHERE id IN (" . $elementList . ")";
    }
    $optionListArray = $DB->get_records_sql($query);
    $label = get_string('noofcourses');
} elseif ($elementType == 'course' && $assignType == 'team') {
    if ($action == 'update') {
        $where = " AND c.id IN (" . $elementList . ")";

        $query = "SELECT DISTINCT(c.id),CONCAT(c.fullname,IF(((gc.end_date = '') || (gc.end_date = 0) || gc.end_date IS NULL),'', FROM_UNIXTIME(gc.end_date,' - " . $CFG->customDefaultDateFormatForDB . "'))) as name,IF(((gc.end_date = '') || (gc.end_date = 0) || gc.end_date IS NULL),'', FROM_UNIXTIME(gc.end_date,' - " . $CFG->customDefaultDateFormatForDB . "')) as enddate FROM mdl_groups_course as gc LEFT JOIN mdl_course as c ON gc.courseid = c.id WHERE gc.is_active = 1 AND c.is_active = 1 " . $where . " AND gc.groupid = " . $assignId . " ORDER BY LOWER(c.fullname) ASC";
    } else {
        $query = "SELECT id,fullname as name FROM mdl_course WHERE id IN (" . $elementList . ")";
    }
    $optionListArray = $DB->get_records_sql($query);
    $label = get_string('noofcourses');
} elseif ($elementType == 'team' && $assignType == 'program') {
    $where = " AND g.id IN (" . $elementList . ")";
    if ($action == 'update') {
        $query = "	SELECT DISTINCT(g.id), IF(((pg.end_date = '') || (pg.end_date = 0) || pg.end_date IS NULL),'', FROM_UNIXTIME(pg.end_date,' - " . $CFG->customDefaultDateFormatForDB . "')) as enddate, CONCAT(IF((d.title IS NULL),'" . get_string("global_team_identifier") . "',''),g.name,IF((d.title IS NULL), IF(((pg.end_date = '') || (pg.end_date = 0) || pg.end_date IS NULL),'', FROM_UNIXTIME(pg.end_date,' - " . $CFG->customDefaultDateFormatForDB . "')), CONCAT('(',d.title,')', IF(((pg.end_date = '') || (pg.end_date = 0) || pg.end_date IS NULL),'', FROM_UNIXTIME(pg.end_date,' - " . $CFG->customDefaultDateFormatForDB . "'))))) AS name FROM mdl_groups as g LEFT JOIN mdl_program_group as pg ON g.id = pg.group_id LEFT JOIN mdl_group_department as gd ON gd.team_id = g.id LEFT JOIN mdl_department d ON d.deleted = 0 AND d.id = gd.department_id WHERE pg.program_id = $assignId AND pg.is_active = 1";

        $query = $query . $where . " ORDER by g.name ASC";
    } else {
        $query = "SELECT g.id,CONCAT(IF((d.title IS NULL),'" . get_string("global_team_identifier") . "',''),g.name,IF((d.title IS NULL),'',CONCAT('(',d.title,')'))) as name FROM mdl_groups g LEFT JOIN mdl_program_group pg ON (g.id = pg.group_id AND pg.program_id = " . $assignId . " AND  pg.is_active = 1) LEFT JOIN mdl_group_department as gd ON gd.team_id = g.id LEFT JOIN mdl_department d ON d.id = gd.department_id WHERE pg.group_id IS NULL" . $where . " ORDER by g.name ASC";
    }
    $optionListArray = $DB->get_records_sql($query);
} elseif ($elementType == 'user' && $assignType == 'program') {
    $where = " AND u.id IN (" . $elementList . ")";
    if ($action == 'update') {
        $query = "SELECT u.id, CONCAT(u.firstname,' ', u.lastname,' (',u.username,')',IF(((p.end_date = '') || (p.end_date = 0) || p.end_date IS NULL),'', FROM_UNIXTIME(p.end_date,' - " . $CFG->customDefaultDateFormatForDB . "')))  AS name,IF(((p.end_date = '') || (p.end_date = 0) || p.end_date IS NULL),'', FROM_UNIXTIME(p.end_date,' - " . $CFG->customDefaultDateFormatForDB . "')) as enddate,				p.type FROM mdl_user as u
					LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id
					LEFT JOIN mdl_role AS r ON ra.roleid = r.id
					LEFT JOIN mdl_program_user_mapping AS p ON u.id = p.user_id
					WHERE u.deleted = 0 AND u.suspended = 0 AND p.program_id = $assignId AND p.type = 'user' AND p.`status` = 1";
        $orderBy = "  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
        $query = $query . $where . $orderBy;
    } else {
        $query = "	SELECT u.id, CONCAT(u.firstname,' ', u.lastname,' (',u.username,')')  AS name, p.type FROM mdl_user AS u LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id LEFT JOIN mdl_role AS r ON ra.roleid = r.id LEFT JOIN mdl_program_user_mapping AS p ON (u.id = p.user_id AND p.program_id = $assignId AND p.status = 1 AND p.type = 'user') WHERE p.id IS NULL AND u.deleted = 0 AND u.suspended = 0 $where  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
    }
    $optionListArray = $DB->get_records_sql($query);
    if (empty($optionListArray) && $isRequest == 1) {
        $query = "SELECT u.id, CONCAT(u.firstname,' ', u.lastname,' (',u.username,')', IF(((p.end_date = '') || (p.end_date = 0) || p.end_date IS NULL),'', FROM_UNIXTIME(p.end_date,' - %b %d, %Y'))) AS name, IF(((p.end_date = '') || (p.end_date = 0) || p.end_date IS NULL),'', FROM_UNIXTIME(p.end_date,' - %b %d, %Y')) AS enddate FROM mdl_user AS u LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id LEFT JOIN mdl_role AS r ON ra.roleid = r.id LEFT JOIN mdl_groups_members AS gm ON gm.userid = u.id AND gm.is_active = 1 LEFT JOIN mdl_program_group AS p ON p.group_id = gm.groupid AND p.program_id = $assignId AND p.is_active = 1 WHERE u.deleted = 0 AND u.suspended = 0";
        $orderBy = "  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
        $query = $query . $where . $orderBy;
        $optionListArray = $DB->get_records_sql($query);
    }
}

if ($assignType == 'team') {
    $headerElement = $DB->get_record_sql("SELECT * FROM mdl_groups WHERE id = " . $assignId);
    $headerHtml = getModuleHeaderHtml($headerElement, $CFG->teamModule);
} elseif ($assignType == 'user') {
    $headerElement = $DB->get_record_sql("SELECT * FROM mdl_user WHERE id =" . $assignId);
    $headerHtml = getModuleHeaderHtml($headerElement, $CFG->userModule);
} elseif ($assignType == 'course') {
    $headerElement = $DB->get_record_sql("SELECT * FROM mdl_course WHERE id =" . $assignId);
    $headerHtml = getModuleHeaderHtml($headerElement, $CFG->courseModule);
} elseif ($assignType == 'program') {
    $headerElement = $DB->get_record_sql("SELECT * FROM mdl_programs WHERE id =" . $assignId);
    $headerHtml = getModuleHeaderHtml($headerElement, $CFG->programModule);
}
echo $OUTPUT->header();
?>
<form id="assignform" method="post" action="">
    <div class = "enrol-main">
        <div class = "row heading">
<?php echo $headerHtml; ?>
        </div>
        <div class="row_outer">
            <div class = "row row1">
                <label for = "element_list" class="labelSelect"><?php echo $label; ?></label>
                <span class = "element selectBoxPlace">
                    <div class = "elements" id = "element_list" >
<?php
if (!empty($optionListArray)) {
    foreach ($optionListArray as $optionList) {
        ?>
                                <span rel = "<?php echo $optionList->id; ?>" enddate="<?php echo $optionList->enddate; ?>"><?php echo $optionList->name; ?></span>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <!--  <select class = "elements" id = "element_list" name = "element_list" multiple disabled>
                        <?php
                        if (!empty($optionListArray)) {
                            foreach ($optionListArray as $optionList) {
                                ?>
                                            <option value = "<?php echo $optionList->id; ?>" enddate="<?php echo $optionList->enddate; ?>"><?php echo $optionList->name; ?></option>
                            <?php
                        }
                    }
                    ?>
                    <select> -->
                </span>
            </div>
            <div class = "row row2">
                <label for = "end_date">End Date</label>
                <span class = "element">
                    <input type = "text" name = "end_date" id = "start_date" readonly="true">
                </span>
            </div>
<?php if ($showCustomEmailInput == 1) { ?>
                <div class = "row row4">
                    <label for="custom_email_content"><?php echo get_string('custom_email_text', 'course'); ?></label>
                    <span class = "element">                    
                        <textarea rows="3" cols="50" name="custom_email_content" id="custom_email_content" ></textarea>
                    </span>
                </div>
             <div class = "row row5">
                    <label for="custom_email_content"><?php echo get_string('cc_emails', 'course'); ?></label>
                    <span class = "element">                    
                        <textarea rows="1" cols="50" name="cc_emails" id="cc_emails" placeholder="<?php echo get_string('cc_emails_placeholder');?>"></textarea>
                    </span>(Comma separated)
                </div>
<?php } ?>
            <div class = "row row3">
                <input type="button" name="cancel" id="cancel_data" value="cancel">
            <?php
            if ($action == 'update') {
                ?>
                    <input type="button" name="save" id="save_data" value="update">
                    <?php
                } else {
                    ?>
                    <input type="button" name="save" id="save_data" value="save">
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</form>
<link href='<?php echo $CFG->wwwroot; ?>/theme/gourmet/jquery/jquery/css/jquery-ui.css' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="<?php echo $CFG->wwwroot; ?>/theme/gourmet/jquery/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot; ?>/theme/gourmet/jquery/jquery/jquery-ui.min.js"></script>
<script>
    var assignElement = "<?php echo $assignType; ?>";
    var assignId = "<?php echo $assignId; ?>";
    var assignTo = "<?php echo $elementType; ?>";
    var action = "<?php echo $action; ?>";
    var isRequest = "<?php echo $isRequest; ?>";

    var fromTeam = "";
    var toTeam = "";

    var fromUser = "";
    var toUser = "";

    var fromCourse = "";
    var toCourse = "";
    var emaillistError=0;

    $(document).ready(function () {
        fromTeam = parent.top.$('#team_addselect_wrapper select');
        toTeam = parent.top.$('#team_removeselect_wrapper select');

        fromUser = parent.top.$('#user_addselect_wrapper select');
        toUser = parent.top.$('#user_removeselect_wrapper select');

        fromCourse = parent.top.$('#addselect_wrapper select');
        toCourse = parent.top.$('#removeselect_wrapper select');
        $('#start_date').datepicker({
            changeMonth: true,
            changeYear: true,
            minDate: -0
        });
        $("#cancel_data").click(function () {
            parent.$.colorbox.close();
        });
        $("#save_data").click(function () {
            
            checkEmails();
            
            if(emaillistError==1){
                return false;
            }
            $(this).attr("disabled", "disabled");

            custom_email_content = $("#custom_email_content").val();

            if (custom_email_content != '') {
                // alert(custom_email_content);
                setValueInSession(custom_email_content);               
            } else {
                var end_date = '';
                end_date = $("#start_date").val();
                var customEmailContent = '';
                
                var cc_emails = $("#cc_emails").val();

                var elementList = "";
                $("#element_list").find('span').each(function () {
                    elementList += $(this).attr('rel') + ',';
                });
                elementList = elementList.substr(0, elementList.length - 1);

                if (isRequest == 1) {
                    if (elementList != "") {
                        var enrolUser = 1;
                        if (assignElement == 'course') {
                            courseId = assignId;
                            programId = 0;
                            userId = elementList;
                        } else {
                            courseId = 0;
                            programId = assignId;
                            userId = elementList;
                        }
                        saveRequestData(courseId, programId, userId, enrolUser, end_date);
                    } else {
                        parent.$.colorbox.close();
                    }
                } else {

                    if (elementList != "") {
                        saveElementData(assignElement, assignTo, assignId, elementList, end_date,cc_emails);
                    } else {
                        parent.$.colorbox.close();
                    }
                }
            }

        });
    });
    function saveRequestData(courseId, programId, userId, enrolUser, end_date) {
        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/ajax.php',
            type: 'POST',
            data: "action=enrolUserOnRequest&courseId=" + courseId + "&programId=" + programId + "&userId=" + userId + "&enrolUser=" + enrolUser + "&end_date=" + end_date,
            success: function (data) {
                parent.window.location.reload();
            }
        });
    }
    function setValueInSession(emailcontent) {
            
        $.ajax({
            url: '<?php echo $CFG->wwwroot; ?>/local/ajax.php',
            type: 'POST',
            async: "false",
            data: "action=setValueInSession&custom_email_content=" + emailcontent,
            success: function (data) {
                //console.log("value set in session");
                var end_date = '';
                end_date = $("#start_date").val();
                var customEmailContent = '';


                var elementList = "";
                $("#element_list").find('span').each(function () {
                    elementList += $(this).attr('rel') + ',';
                });
                elementList = elementList.substr(0, elementList.length - 1);

                if (isRequest == 1) {
                    if (elementList != "") {
                        var enrolUser = 1;
                        if (assignElement == 'course') {
                            courseId = assignId;
                            programId = 0;
                            userId = elementList;
                        } else {
                            courseId = 0;
                            programId = assignId;
                            userId = elementList;
                        }
                        saveRequestData(courseId, programId, userId, enrolUser, end_date);
                    } else {
                        parent.$.colorbox.close();
                    }
                } else {
                    var cc_emails = $("#cc_emails").val();
                    if (elementList != "") {
                        saveElementData(assignElement, assignTo, assignId, elementList, end_date,cc_emails);
                    } else {
                        parent.$.colorbox.close();
                    }
                }
            }
        });

    }

    function sortOptions(optionsId) {
        optionsId.sort(function (a, b) {
            if (a.text.toUpperCase() > b.text.toUpperCase())
                return 1;
            else if (a.text.toUpperCase() < b.text.toUpperCase())
                return -1;
            else
                return 0;
        });
        return optionsId;
    }
    function saveElementData(assignElement, assignTo, assignId, elementList, end_date,cc_emails) {
        if (assignElement != '' && assignTo != '' && assignId != '' && assignId != 0 && elementList != '' && elementList != 0) {
            $.ajax({
                url: '<?php echo $CFG->wwwroot; ?>/local/ajax.php',
                type: 'POST',
                data: 'action=saveCourseEnrolmentData&assignElement=' + assignElement + '&assignTo=' + assignTo + '&assignId=' + assignId + '&elementList=' + elementList + '&end_date=' + end_date+ '&cc_emails=' + cc_emails + '&update=' + action,
                success: function (data) {
                    var obj = JSON.parse(data);
                    var success = obj.success;
                    var dateStr = obj.response;
                    var appendStr = "";
                    if (dateStr != "") {
                        appendStr = " - " + dateStr;
                    }
                    if (success == 'success') {
                        if (assignTo == 'team') {
                            if (action == 'update') {
                                var TextVal = '';
                                toTeam.find('option:selected').each(function () {
                                    $(this).remove();
                                });
                                $("#element_list").find('span').each(function () {
                                    TextVal = $(this).text();
                                    var enddate = $(this).attr('enddate');
                                    if ($.trim(enddate) == '') {
                                        TextVal = $(this).text() + appendStr;
                                    } else {
                                        TextVal = TextVal.replace($(this).attr('enddate'), appendStr);
                                    }
                                    optionHtml = "<option value = '" + $(this).attr('rel') + "'>" + TextVal + "</option>";
                                    toTeam.find('optgroup').append(optionHtml);
                                });
                            } else {
                                $("#element_list").find('span').each(function () {
                                    optionHtml = "<option value = '" + $(this).attr('rel') + "'>" + $(this).text() + appendStr + "</option>";
                                    toTeam.find('optgroup').append(optionHtml);
                                });
                                fromTeam.find('option:selected').each(function () {
                                    $(this).remove();
                                });
                                maintainSelectCount(fromTeam, toTeam, "<?php echo get_string('teams'); ?>");
                            }
                            toTeamOptionOptions = sortOptions(toTeam.find('option'));
                            toTeam.find('optgroup').empty();
                            toTeam.find('optgroup').append(toTeamOptionOptions);
                        } else {
                            if (assignTo == 'user') {
                                if (action == 'update') {

                                    var TextVal = '';
                                    toUser.find('option:selected').each(function () {
                                        $(this).remove();
                                    });
                                    $("#element_list").find('span').each(function () {
                                        TextVal = $(this).text();
                                        var enddate = $(this).attr('enddate');
                                        if ($.trim(enddate) == '') {
                                            TextVal = $(this).text() + appendStr;
                                        } else {
                                            TextVal = TextVal.replace($(this).attr('enddate'), appendStr);
                                        }
                                        optionHtml = "<option value = '" + $(this).attr('rel') + "'>" + TextVal + "</option>";
                                        toUser.find('optgroup').append(optionHtml);
                                    });
                                } else {
                                    $("#element_list").find('span').each(function () {
                                        optionHtml = "<option value = '" + $(this).attr('rel') + "'>" + $(this).text() + appendStr + "</option>";
                                        toUser.find('optgroup').append(optionHtml);
                                    });
                                    fromUser.find('option:selected').each(function () {
                                        $(this).remove();
                                    });
                                    maintainSelectCount(fromUser, toUser, "<?php echo get_string('users'); ?>");
                                }
                                toUserOptionOptions = sortOptions(toUser.find('option'));
                                toUser.find('optgroup').empty();
                                toUser.find('optgroup').append(toUserOptionOptions);
                            } else {
                                if (assignTo == 'course' || assignTo == 'team') {
                                    if (action == 'update') {
                                        var TextVal = '';
                                        toCourse.find('option:selected').each(function () {
                                            $(this).remove();
                                        });
                                        $("#element_list").find('span').each(function () {
                                            TextVal = $(this).text();
                                            var enddate = $(this).attr('enddate');
                                            if ($.trim(enddate) == '') {
                                                TextVal = $(this).text() + appendStr;
                                            } else {
                                                TextVal = TextVal.replace($(this).attr('enddate'), appendStr);
                                            }
                                            optionHtml = "<option value = '" + $(this).attr('rel') + "'>" + TextVal + "</option>";
                                            toCourse.find('optgroup').append(optionHtml);
                                        });
                                    } else {

                                        $("#element_list").find('span').each(function () {
                                            optionHtml = "<option value = '" + $(this).attr('rel') + "'>" + $(this).text() + appendStr + "</option>";
                                            toCourse.find('optgroup').append(optionHtml);
                                        });
                                        fromCourse.find('option:selected').each(function () {
                                            $(this).remove();
                                        });
                                        maintainSelectCount(fromCourse, toCourse, "<?php echo get_string('courses'); ?>");
                                    }
                                    toUserOptionOptions = sortOptions(toCourse.find('option'));
                                    toCourse.find('optgroup').empty();
                                    toCourse.find('optgroup').append(toUserOptionOptions);
                                }
                            }
                        }
                    }
                    parent.$.colorbox.close();
                }
            });

        }
    }
</script>


<script language="javascript">
function checkEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;  

  return(regex.test(email));

}

function checkEmails(){
	//var emails = document.getElementById("cc_emails").value;
        
       var emails = $("#cc_emails").val();
       if (typeof emails == "undefined"){
           return true;
       }
       
       if(emails != ""){
           var emailArray = emails.split(",");
            var invEmails = "";
            for(i = 0; i <= (emailArray.length - 1); i++){
                    if(checkEmail(emailArray[i].trim())){
                            //Do what ever with the email.
                    }else{
                            invEmails += emailArray[i] + "\n";
                    }
            }
            if(invEmails != ""){
                    emaillistError=1;                   
                    alert("Invalid emails:\n" + invEmails);                
                    return false;
            }else{
                emaillistError=0;
            }
       }
	
        
        
}
</script>

<?php
echo $OUTPUT->footer();
?>