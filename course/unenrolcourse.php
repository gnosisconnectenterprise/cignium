<?php
require_once(dirname(__FILE__) . '/../config.php');
GLOBAL $DB,$CFG,$USER;
require_login();

$site = get_site();
$PAGE->set_url('/course/enrolcourse.php', array('id'=>$courseid));
$PAGE->set_pagelayout('classroompopup');
$PAGE->set_title("$site->fullname: $stradduserstogroup");
$PAGE->set_heading($site->fullname);

$elementType  = optional_param('element', false, PARAM_RAW);
$assignId  = optional_param('assignId', false, PARAM_RAW);
$assignType  = optional_param('assigntype', false, PARAM_RAW);
$elementList  = optional_param('elementList', false, PARAM_RAW);
$action  = optional_param('action', "", PARAM_RAW);
$isRequest  = optional_param('is_request', "", PARAM_RAW);

$optionList = array();
$label = get_string('noofusers');
if($elementType == 'team' && $assignType == 'course')
{
	$query = "SELECT DISTINCT(g.id),g.name as namewithouttime, IF(((gc.end_date = '') || (gc.end_date = 0) || gc.end_date IS NULL),'', FROM_UNIXTIME(gc.end_date,' - ".$CFG->customDefaultDateFormatForDB."')) as enddate, CONCAT(IF((d.title IS NULL),'".get_string("global_team_identifier")."',''),g.name,IF((d.title IS NULL), IF(((gc.end_date = '') || (gc.end_date = 0) || gc.end_date IS NULL),'', FROM_UNIXTIME(gc.end_date,' - ".$CFG->customDefaultDateFormatForDB."')), CONCAT('(',d.title,')', IF(((gc.end_date = '') || (gc.end_date = 0) || gc.end_date IS NULL),'', FROM_UNIXTIME(gc.end_date,' - ".$CFG->customDefaultDateFormatForDB."'))))) AS name FROM mdl_groups_course as gc LEFT JOIN mdl_groups as g ON g.id = gc.groupid LEFT JOIN mdl_group_department as gd ON gd.team_id = g.id LEFT JOIN mdl_department d ON d.deleted = 0 AND d.id = gd.department_id WHERE gc.is_active = 1 AND gc.courseid = ".$assignId." AND g.id IN (".$elementList.") ORDER BY g.name ASC";
	$optionListArray = $DB->get_records_sql($query);
	$label = get_string('team');
}elseif($elementType == 'user' && $assignType == 'course'){
	$where = " AND u.id IN (".$elementList.")";
	if($isRequest != ""){
		$query = "SELECT DISTINCT(u.id), CONCAT(u.firstname,' ', u.lastname,' (',u.username,')') AS name FROM mdl_user AS u  WHERE u.suspended = 0 AND u.deleted = 0 ";
		$orderBy = "  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
		$query = $query.$where.$orderBy;
	}else{
		$query = "	SELECT u.id,CONCAT(u.firstname,' ', u.lastname,' (',u.username,')') as namewithouttime,IF(((uc.end_date = '') || (uc.end_date = 0) || uc.end_date IS NULL),'', FROM_UNIXTIME(uc.end_date,' - ".$CFG->customDefaultDateFormatForDB."')) as enddate,CONCAT(u.firstname,' ', u.lastname,' (',u.username,')',IF(((uc.end_date = '') || (uc.end_date = 0) || uc.end_date IS NULL),'', FROM_UNIXTIME(uc.end_date,' - ".$CFG->customDefaultDateFormatForDB."')))  AS name,uc.type FROM mdl_user as u LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id LEFT JOIN mdl_role AS r ON ra.roleid = r.id LEFT JOIN mdl_user_course_mapping AS uc ON u.id = uc.userid WHERE u.deleted = 0 AND u.suspended = 0 AND uc.courseid = $assignId AND uc.type = 'user' AND uc.status = 1";
		$orderBy = "  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
		$query = $query.$where.$orderBy;
	}
	$optionListArray = $DB->get_records_sql($query);
	$label = get_string('noofusers');
}elseif($elementType == 'course' && $assignType == 'user'){
	$where = " AND c.id IN (".$elementList.")";
	$query = "SELECT DISTINCT(c.id),CONCAT(c.fullname,IF(((cm.end_date = '') || (cm.end_date = 0) || cm.end_date IS NULL),'', FROM_UNIXTIME(cm.end_date,' - ".$CFG->customDefaultDateFormatForDB."'))) as name,IF(((cm.end_date = '') || (cm.end_date = 0) || cm.end_date IS NULL),'', FROM_UNIXTIME(cm.end_date,' - ".$CFG->customDefaultDateFormatForDB."')) as enddate FROM mdl_user_course_mapping as cm LEFT JOIN mdl_course as c ON c.id = cm.courseid";
	$query .= " WHERE cm.status = 1 AND c.id != '' AND cm.userid = ".$assignId.$where;
	$query .= "ORDER BY LOWER(c.fullname) ASC";
	$optionListArray = $DB->get_records_sql($query);

	$label = get_string('noofcourses');
}elseif($elementType == 'course' && $assignType == 'team'){
	$where = " AND c.id IN (".$elementList.")";

	$query = "SELECT DISTINCT(c.id),CONCAT(c.fullname,IF(((gc.end_date = '') || (gc.end_date = 0) || gc.end_date IS NULL),'', FROM_UNIXTIME(gc.end_date,' - ".$CFG->customDefaultDateFormatForDB."'))) as name,IF(((gc.end_date = '') || (gc.end_date = 0) || gc.end_date IS NULL),'', FROM_UNIXTIME(gc.end_date,' - ".$CFG->customDefaultDateFormatForDB."')) as enddate FROM mdl_groups_course as gc LEFT JOIN mdl_course as c ON gc.courseid = c.id WHERE gc.is_active = 1 AND c.is_active = 1 ".$where." AND gc.groupid = ".$assignId." ORDER BY LOWER(c.fullname) ASC";
	$optionListArray = $DB->get_records_sql($query);

	$label = get_string('noofcourses');
}elseif($elementType == 'team' && $assignType == 'program'){
	$where = " AND g.id IN (".$elementList.")";
	$query = "	SELECT DISTINCT(g.id), IF(((pg.end_date = '') || (pg.end_date = 0) || pg.end_date IS NULL),'', FROM_UNIXTIME(pg.end_date,' - ".$CFG->customDefaultDateFormatForDB."')) as enddate, CONCAT(IF((d.title IS NULL),'".get_string("global_team_identifier")."',''),g.name,IF((d.title IS NULL), IF(((pg.end_date = '') || (pg.end_date = 0) || pg.end_date IS NULL),'', FROM_UNIXTIME(pg.end_date,' - ".$CFG->customDefaultDateFormatForDB."')), CONCAT('(',d.title,')', IF(((pg.end_date = '') || (pg.end_date = 0) || pg.end_date IS NULL),'', FROM_UNIXTIME(pg.end_date,' - ".$CFG->customDefaultDateFormatForDB."'))))) AS name FROM mdl_groups as g LEFT JOIN mdl_program_group as pg ON g.id = pg.group_id LEFT JOIN mdl_group_department as gd ON gd.team_id = g.id LEFT JOIN mdl_department d ON d.deleted = 0 AND d.id = gd.department_id WHERE pg.program_id = $assignId AND pg.is_active = 1";
	$query  = $query.$where." ORDER by g.name ASC";
	$optionListArray = $DB->get_records_sql($query);
}elseif($elementType == 'user' && $assignType == 'program'){
	$where = " AND u.id IN (".$elementList.")";
	if($isRequest != ""){
		$query = "	SELECT u.id, CONCAT(u.firstname,' ', u.lastname,' (',u.username,')')  AS name, 'user' as type FROM mdl_user AS u WHERE u.deleted = 0 AND u.suspended = 0 $where  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
	}else{
		$query = "SELECT u.id, CONCAT(u.firstname,' ', u.lastname,' (',u.username,')',IF(((p.end_date = '') || (p.end_date = 0) || p.end_date IS NULL),'', FROM_UNIXTIME(p.end_date,' - ".$CFG->customDefaultDateFormatForDB."')))  AS name,IF(((p.end_date = '') || (p.end_date = 0) || p.end_date IS NULL),'', FROM_UNIXTIME(p.end_date,' - ".$CFG->customDefaultDateFormatForDB."')) as enddate,				p.type FROM mdl_user as u
					LEFT JOIN mdl_role_assignments AS ra ON ra.userid = u.id
					LEFT JOIN mdl_role AS r ON ra.roleid = r.id
					LEFT JOIN mdl_program_user_mapping AS p ON u.id = p.user_id
					WHERE u.deleted = 0 AND u.suspended = 0 AND p.program_id = $assignId AND p.type = 'user' AND p.`status` = 1";
		$orderBy = "  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
		$query = $query.$where.$orderBy;
	}
	$optionListArray = $DB->get_records_sql($query);
}elseif($elementType == 'user' && $assignType == 'classroom'){
	$where = " AND u.id IN (".$elementList.")";
	if($isRequest != ""){
		$query = "SELECT u.id,CONCAT(u.firstname,' ', u.lastname,' (',u.username,')') AS name,'' as enddate,'' as namewithouttime FROM mdl_user as u LEFT JOIN mdl_scheduler_enrollment as se ON se.userid = u.id WHERE se.scheduler_id = $assignId AND se.is_approved IN(0,3)";
		$orderBy = "  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
		$query = $query.$where.$orderBy;
	}else{
		$query = "SELECT u.id,CONCAT(u.firstname,' ', u.lastname,' (',u.username,')') AS name,'' as enddate,'' as namewithouttime FROM mdl_user as u LEFT JOIN mdl_scheduler_enrollment as se ON se.userid = u.id WHERE se.scheduler_id = $assignId ";
		$orderBy = "  ORDER BY COALESCE(NULLIF(u.firstname, ''), u.lastname), u.lastname ASC";
		$query = $query.$where.$orderBy;
	}
	$optionListArray = $DB->get_records_sql($query);
	$label = get_string('noofusers');
}elseif($elementType == 'department' && $assignType == 'course'){
	if($isRequest != ""){
		$query = "SELECT d.id,d.title as name,'' as enddate,'' as namewithouttime,cr.user_id FROM mdl_course_request_log AS cr LEFT JOIN mdl_department_members as dm ON cr.user_id = dm.userid AND dm.is_active = 1 LEFT JOIN mdl_department as d ON dm.departmentid = d.id WHERE cr.id IN (".$elementList.")";
		$orderBy = "  ORDER BY d.title ASC";
	}else{
		$query = "SELECT d.id,d.title as name,'' as enddate,'' as namewithouttime FROM mdl_department AS d WHERE id IN (".$elementList.")";
		$orderBy = "  ORDER BY name ASC";
	}
	$optionListArray = $DB->get_records_sql($query);
	$label = get_string('noofdepartments');
}elseif($elementType == 'course' && $assignType == 'department'){
	$query = "SELECT id,fullname AS name,'' as enddate,'' as namewithouttime FROM mdl_course AS c WHERE id IN (".$elementList.")";
	$orderBy = "  ORDER BY name ASC";
	$optionListArray = $DB->get_records_sql($query);
	$label = get_string('noofcourses');
}elseif($elementType == 'department' && $assignType == 'program'){
	if($isRequest != ""){
		$query = "SELECT d.id,d.title as name,'' as enddate,'' as namewithouttime,cr.user_id FROM mdl_course_request_log AS cr LEFT JOIN mdl_department_members as dm ON cr.user_id = dm.userid AND dm.is_active = 1 LEFT JOIN mdl_department as d ON dm.departmentid = d.id WHERE cr.id IN (".$elementList.")";
		$orderBy = "  ORDER BY d.title ASC";
	}else{
		$query = "SELECT d.id,d.title as name,'' as enddate,'' as namewithouttime FROM mdl_department AS d WHERE id IN (".$elementList.")";
		$orderBy = "  ORDER BY name ASC";
	}
	$optionListArray = $DB->get_records_sql($query);
	$label = get_string('noofdepartments');
}

if($assignType == 'team')
{
	$headerElement = $DB->get_record_sql("SELECT * FROM mdl_groups WHERE id = ".$assignId);
	$headerHtml = getModuleHeaderHtml($headerElement, $CFG->teamModule);
}elseif($assignType == 'user'){
	$headerElement = $DB->get_record_sql("SELECT * FROM mdl_user WHERE id =".$assignId);
	$headerHtml = getModuleHeaderHtml($headerElement, $CFG->userModule);
}elseif($assignType == 'course'){
	$headerElement = $DB->get_record_sql("SELECT * FROM mdl_course WHERE id =".$assignId);
	$headerHtml = getModuleHeaderHtml($headerElement, $CFG->courseModule);
}elseif($assignType == 'program'){
	$headerElement = $DB->get_record_sql("SELECT * FROM mdl_programs WHERE id =".$assignId);
	$headerHtml = getModuleHeaderHtml($headerElement, $CFG->programModule);
}elseif($assignType == 'classroom'){
	require_once($CFG->dirroot.'/mod/scheduler/lib.php');
	$class_details = getClassDetailsFromModuleId($assignId);
	$headerHtml = getModuleHeaderHtml($class_details, $CFG->classModule);
}elseif($assignType == 'department'){
	$headerElement = $DB->get_record_sql("SELECT * FROM mdl_department WHERE id =".$assignId);
	$headerHtml = getModuleHeaderHtml($headerElement, $CFG->departmentModule);
}
echo $OUTPUT->header();
$display = "";
if($elementList == $USER->id && $USER->archetype == $CFG->userTypeStudent){
	$display = "style='display:none;'";
}
?>
<form id="assignform" method="post" action="">
<div class = "enrol-main">
	<div class = "row heading">
		<?php echo $headerHtml; ?>
	</div>
	<div class="row_outer">
	<div class = "row row1" <?php echo $display;?>>
		<label for = "element_list" class="labelSelect"><?php echo $label;?></label>
		<span class = "element selectBoxPlace">
			<div class = "elements" id = "element_list" >
				<?php 
				if(!empty($optionListArray)){
					foreach($optionListArray as $optionList){?>
						<span rel = "<?php echo $optionList->id; ?>" enddate="<?php echo $optionList->enddate; ?>" relUser = "<?php if(isset($optionList->user_id)) echo $optionList->user_id;?>" ><?php echo $optionList->name; ?></span>
					<?php
					}
				}
				?>
			</div>
		<!--	<select class = "elements" id = "element_list" name = "element_list" multiple disabled>
			<?php 
			if(!empty($optionListArray)){
				foreach($optionListArray as $optionList){?>
					<option value = "<?php echo $optionList->id; ?>" enddate="<?php echo $optionList->enddate; ?>" relUser = "<?php if(isset($optionList->user_id)) echo $optionList->user_id;?>" ><?php echo $optionList->name; ?></option>
				<?php
				}
			}
			?>
			<select> -->
		</span>
	</div>
	<div class = "row row2">
		<label for = "remarks_text"><?php echo get_string('decline_remarks'); ?><img src="<?php echo $CFG->wwwroot; ?>/theme/image.php?theme=gourmet&amp;component=core&amp;image=req" alt="Required field" title="Required field" class="req" style = "vertical-align: 4px;">
</label>
		<span class = "element">
			<textarea id =  "remarks_text" name = "remarks_text"></textarea>
		</span>
	</div>
	<div class = "row row3">
		<input type="button" name="cancel" id="cancel_data" value="cancel">
		<input type="button" name="save" id="save_data" value="save">
	</div>
	</div>
</div>
</form>
<link href='<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/css/jquery-ui.css' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/jquery/jquery/jquery-ui.min.js"></script>
<script>
var assignElement = "<?php echo $assignType; ?>";
var assignId = "<?php echo $assignId; ?>";
var assignTo = "<?php echo $elementType; ?>";
var action = "<?php echo $action; ?>";
var isRequest = "<?php echo $isRequest; ?>";

var fromTeam = "";
var toTeam = "";

var fromUser = "";
var toUser = "";

var fromCourse = "";
var toCourse = "";

$(document).ready(function(){
	fromTeam = parent.top.$('#team_addselect_wrapper select');
	toTeam = parent.top.$('#team_removeselect_wrapper select');

	fromUser = parent.top.$('#user_addselect_wrapper select');
	toUser = parent.top.$('#user_removeselect_wrapper select');
	
	fromCourse	= parent.top.$('#addselect_wrapper select');
	toCourse	= parent.top.$('#removeselect_wrapper select');

	fromDepartment	= parent.top.$('#department_select_wrapper select');
	toDepartment	= parent.top.$('#department_remove_wrapper select');

	$("#cancel_data").click(function(){
		parent.parent.top.$('#cboxLoadedContent').css('margin-top','43px').css('height','100%');
		parent.parent.top.$('#cboxTitle,#cboxClose').css('z-index','0');
		parent.$.colorbox.close();
	});
	$("#save_data").click(function(){
		parent.parent.top.$('#cboxLoadedContent').css('margin-top','43px').css('height','100%');
		parent.parent.top.$('#cboxTitle,#cboxClose').css('z-index','0');
		var remarks = '';
		remarks = $("#remarks_text").val();
		if($.trim(remarks) == ''){
			alert("<?php echo get_string('enter_remarks');?>");
			return false;
		}
		$(this).attr("disabled","disabled");
		var elementList = "";
		$("#element_list").find('span').each(function(){
			elementList += $(this).attr('rel')+',';
		});
		elementList = elementList.substr(0, elementList.length - 1);
               
		if(isRequest == 1){                     
			if(elementList != "" && assignElement != "classroom"){
				var enrolUser = 0;
				if(assignElement == 'course'){
					courseId	= assignId;
					programId	= 0;
					var userIds = '';
					if(assignTo == 'department'){
						$("#element_list").find('span').each(function(){
							userIds += $(this).attr('reluser')+',';
						});
						userIds = userIds.substr(0, userIds.length - 1);
						userId	= userIds;
					}else{
						userId	= elementList;
					}
				}else{
					courseId	= 0;
					programId	= assignId;
					var userIds = '';
					if(assignTo == 'department'){
						$("#element_list").find('span').each(function(){
							userIds += $(this).attr('reluser')+',';
						});
						userIds = userIds.substr(0, userIds.length - 1);
						userId	= userIds;
					}else{
						userId	= elementList;
					}
				}
				saveRequestData(courseId,programId,userId,enrolUser);
			}else{
				if(elementList != "" && assignElement == "classroom"){
					saveClassroomRequestData(assignId,elementList);
				}else{
					parent.$.colorbox.close();
				}
			}
		}else{
			if(elementList != "" && assignElement != "classroom"){
				saveElementData(assignElement,assignTo,assignId,elementList,remarks);
			}else{
				if(elementList != "" && assignElement == "classroom"){
					saveClassroomRequestData(assignId,elementList);
				}else{
					parent.$.colorbox.close();
				}
			}
		}
	});
});
function saveRequestData(courseId,programId,userId,enrolUser){
        
	var url = "action=enrolUserOnRequest&courseId="+courseId+"&programId="+programId+"&userId="+userId+"&enrolUser="+enrolUser;
	//alert(url);return false;
	$.post( '<?php echo $CFG->wwwroot;?>/local/ajax.php?'+url, $("#assignform").serialize(), function(data) {
		parent.window.location.reload();
	});
}
function saveClassroomRequestData(enrolid,enrolUser){
	var url2 = "action=enrolUserInClassroom&enrolid="+enrolid+"&enrolUser=0"+"&userIds="+enrolUser;
	$.post( '<?php echo $CFG->wwwroot;?>/local/ajax.php?'+url2,$("#assignform").serialize(), function(data) {
		if(data == 'success'){
			parent.window.location.reload();
		}else{
			alert("unable to process request. Please try again later.");
		}
	});
	/*var url = "action=checkSeatEnrol&enrolid="+enrolid+"&enrolUser="+enrolUser;
	$.post( '<?php echo $CFG->wwwroot;?>/local/ajax.php?'+url, $("#assignform").serialize(), function(data) {
		var url2 = "action=enrolUserInClassroom&enrolid="+enrolid+"&enrolUser=0"+"&userIds="+enrolUser;
			if(data == 'success'){
				$.post( '<?php echo $CFG->wwwroot;?>/local/ajax.php?'+url2,$("#assignform").serialize(), function(data) {
					if(data == 'success'){
						parent.window.location.reload();
					}else{
						alert("unable to process request. Please try again later.");
					}
				});
			}else{
				if(data == 'error'){
					alert("unable to process request. Please try again later.");
				}else{
					var msg = '<?php echo get_string("errornoofinvite","scheduler","'+data+'");?>';
					alert(msg);
				}
			}

	});*/
}
function sortOptions(optionsId){
	optionsId.sort(function(a,b) {
		if (a.text.toUpperCase() > b.text.toUpperCase()) return 1;
		else if (a.text.toUpperCase() < b.text.toUpperCase()) return -1;
		else return 0;
	});
	return optionsId;
}
function saveElementData(assignElement,assignTo,assignId,elementList,remarks){
	if(assignElement != '' && assignTo != '' && assignId != '' && assignId != 0 && elementList != '' && elementList != 0){
		var url = 'action=saveCourseUnEnrolmentData&assignElement='+assignElement+'&assignTo='+assignTo+'&assignId='+assignId+'&elementList='+elementList;
		
		$.post( '<?php echo $CFG->wwwroot;?>/local/ajax.php?'+url, $("#assignform").serialize(), function(data) {
			var obj = JSON.parse(data);
			var success = obj.success;
			var error = obj.error;
			if(success == 'success'){
				if(assignTo == 'team'){
					var TextVal = '';
					toTeam.find('option:selected').each(function(){
						$(this).remove();
					});
					$("#element_list").find('span').each(function(){
						TextVal = $(this).text();
						var enddate = $(this).attr('enddate');
						if($.trim(enddate) == '' ){
							TextVal = $(this).text();
						}else{
							TextVal = TextVal.replace($(this).attr('enddate'), '');
						}
						optionHtml = "<option value = '"+$(this).attr('rel')+"'>"+TextVal+"</option>";
						fromTeam.find('optgroup').append(optionHtml);
					});
					fromTeamOptionOptions = sortOptions(fromTeam.find('option'));
					fromTeam.find('optgroup').empty();
					fromTeam.find('optgroup').append(fromTeamOptionOptions);
					maintainSelectCount(fromTeam,toTeam,"<?php echo get_string('teams'); ?>");
				}else{
					if(assignTo == 'user'){
						var TextVal = '';
						toUser.find('option:selected').each(function(){
							$(this).remove();
						});
						$("#element_list").find('span').each(function(){
							TextVal = $(this).text();
							var enddate = $(this).attr('enddate');
							if($.trim(enddate) == '' ){
								TextVal = $(this).text();
							}else{
								TextVal = TextVal.replace($(this).attr('enddate'), '');
							}
							optionHtml = "<option value = '"+$(this).attr('rel')+"'>"+TextVal+"</option>";
							fromUser.find('optgroup').append(optionHtml);
						});
						fromUserOptionOptions = sortOptions(fromUser.find('option'));
						fromUser.find('optgroup').empty();
						fromUser.find('optgroup').append(fromUserOptionOptions);
						maintainSelectCount(fromUser,toUser,"<?php echo get_string('users'); ?>");
					}else{
						if(assignTo == 'course'){
							if(assignElement == 'department'){
								parent.window.location.reload();
							}
							var TextVal = '';
							toCourse.find('option:selected').each(function(){
								$(this).remove();
							});
							$("#element_list").find('span').each(function(){
								TextVal = $(this).text();
								var enddate = $(this).attr('enddate');
								if($.trim(enddate) == '' ){
									TextVal = $(this).text();
								}else{
									TextVal = TextVal.replace($(this).attr('enddate'), '');
								}
								optionHtml = "<option value = '"+$(this).attr('rel')+"'>"+TextVal+"</option>";
								fromCourse.find('optgroup').append(optionHtml);
							});
							fromUserOptionOptions = sortOptions(fromCourse.find('option'));
							fromCourse.find('optgroup').empty();
							fromCourse.find('optgroup').append(fromUserOptionOptions);
							maintainSelectCount(fromCourse,toCourse,"<?php echo get_string('courses'); ?>");
						}else{
							if(assignTo == 'department'){
								var TextVal = '';
								toDepartment.find('option:selected').each(function(){
									$(this).remove();
								});
								$("#element_list").find('span').each(function(){
									TextVal = $(this).text();
									var enddate = $(this).attr('enddate');
									if($.trim(enddate) == '' ){
										TextVal = $(this).text();
									}else{
										TextVal = TextVal.replace($(this).attr('enddate'), '');
									}
									optionHtml = "<option value = '"+$(this).attr('rel')+"'>"+TextVal+"</option>";
									fromDepartment.find('optgroup').append(optionHtml);
								});
								fromUserOptionOptions = sortOptions(fromDepartment.find('option'));
								fromDepartment.find('optgroup').empty();
								fromDepartment.find('optgroup').append(fromUserOptionOptions);
								maintainSelectCount(fromDepartment,toDepartment,"<?php echo get_string('departments'); ?>");
							}
						}
					}
				}
			}
			parent.$.colorbox.close();
		});
	}
}
</script>

<?php
echo $OUTPUT->footer();
?>