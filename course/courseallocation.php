<?php

require_once(dirname(__FILE__) . '/../config.php');
require_once(dirname(__FILE__) . '/lib.php');
require_once($CFG->dirroot . '/local/user/selector/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->libdir . '/filelib.php');


require_login();
$site = get_site();
$courseid = required_param('id', PARAM_INT);
$course->id = $courseid;

$course = $DB->get_record('course',array('id'=>$courseid),'id,fullname');
$cancel  = optional_param('cancel', false, PARAM_BOOL);
global $DB,$CFG,$USER;
$PAGE->set_url('/user/assignusers.php', array('id'=>$courseid));
$PAGE->set_pagelayout('globaladmin');        
        
if(isset($_REQUEST['formsubmit']) && $_REQUEST['formsubmit'] == 1){
	SaveCourseMappingData($_REQUEST);
	redirect($CFG->wwwroot.'/course/index.php');
}

$context = context_system::instance();
$returnurl = $CFG->wwwroot.'/course/index.php';

if ($cancel) {
	$urlCancel = new moodle_url($CFG->wwwroot.'/course/courseview.php', array('id'=>$course->id));
    redirect($urlCancel);
}
$courseCreatedBy = $DB->get_record_sql("SELECT id,createdby,publish,criteria, is_global FROM  mdl_course WHERE id =".$courseid);
$PAGE->navbar->add(get_string('managecourses'), new moodle_url($CFG->wwwroot.'/course/index.php'));
if($courseCreatedBy->createdby == $USER->id || $USER->archetype == $CFG->userTypeAdmin){
	$PAGE->navbar->add(get_string('editcourse','course'), new moodle_url($CFG->wwwroot.'/course/edit.php',array('id'=>$courseid)));
}
if($courseCreatedBy->publish == 0){
	redirect($returnurl);
}
//$PAGE->navbar->add(get_string('course_allocation','course'));
$PAGE->navbar->add(get_string('allocation'));
$departmentCourse = array();
if($USER->archetype != $CFG->userTypeAdmin){
	$departmentCourse = $DB->get_record_sql("SELECT dc.id FROM mdl_department_course as dc WHERE dc.is_active = 1 AND dc.courseid = $courseid AND dc.departmentid  = $USER->department");
}
$courses = new courses_assignment_selector('', array('courseid' => $courseid));

$courses->courseDepartmentList();
$courses->courseNonDepartmentList();

$courses->courseTeamList();
$courses->courseNonTeamList();

$courses->courseUserList();
$courses->courseNonUserList();


/// Print header
$PAGE->set_title("$site->fullname: $stradduserstogroup");
$PAGE->set_heading($site->fullname);
echo $OUTPUT->header();


/// Print the editing form
$courseHTML = '';
$courseHTML .= "<div class='tabsOuter'>";
	if($courseCreatedBy->createdby == $USER->id || $USER->archetype == $CFG->userTypeAdmin){
		$courseHTML .= "<div class='tabLinks'>";
		include_once('course_tabs.php');
		$courseHTML .= "</div>";
	}
	echo $courseHTML;
/*
$block IS 'team_course'
$block IS 'non_team_course'
$block IS 'department_course'
$block IS 'non_department_course'
$block IS 'user_course'
$block IS 'non_user_course'
*/

$activeIds = getActiveCourses($courseid);

if($activeIds == ''){
	echo '<div style="text-align: center; margin-top: 20px; height: 60px;">';
	echo get_string('cannotenroluser');
	echo '</div>';
	echo $OUTPUT->footer();
	die;
}
if($courseCreatedBy->criteria == $CFG->courseCriteriaCompliance){
	echo '<div style="text-align: center; margin-top: 20px; height: 60px;">';
	echo get_string('cannotenrolusertocompliancecourse');
	echo '</div>';
	echo $OUTPUT->footer();
	die;
}



$display = '';
$displayTeams = '';
$activeLinkForUser = '';
if($USER->archetype != $CFG->userTypeAdmin){
	$display = 'style = "display:none"';
	if(empty($departmentCourse)){
		$displayTeams = 'style = "display:none"';
		$activeLinkForUser = 'activeLink';
	}
}

?>
<form id="assignform" method="post" enctype="multipart/form-data" action="<?php echo $CFG->wwwroot; ?>/course/courseallocation.php?id=<?php echo $courseid; ?>">
<input type = 'hidden' name = "formsubmit" value = "1">
<?php
echo '<fieldset class="" id="yui_3_13_0_2_1404228340264_14">';
echo '<div class="msg-tab-html">
    <div class="coursenametitle"><h3>'.$course->fullname.'</h3></div>
		<!-- <div class="msg-tab-label">'.get_string('enroll_to').'</div> -->
		<div class="msg-tab-block borderBlockSpace">
			
		<div class="tab-box">';
$activeLink = 'activeLink';
if($USER->archetype == $CFG->userTypeAdmin){
	$activeLink = '';
	echo '<a href="javascript:;" class="tabLink" id="cont-1" '.$display.'>'.get_string('departments').'</a>';
}
if($USER->archetype == $CFG->userTypeAdmin || !empty($departmentCourse)){
	echo '<a href="javascript:;" class="tabLink '.$activeLink.'" id="cont-2">'.get_string('teams').'</a>';
}
echo '<a href="javascript:;" class="tabLink '.$activeLinkForUser.' activeLink" id="cont-3">'.get_string('users').'</a></div>';
//echo '<a href="javascript:;" class="tabLink '.$activeLinkForUser.' " id="cont-4">'.get_string('csv_users').'</a></div>';
$hide = '';
$hide2 = 'hide';
if($USER->archetype == $CFG->userTypeAdmin){
	echo '<div class="tabcontent paddingAll hide" id="cont-1-1"><div class="departments">';
	$hide = 'hide';
?>
<div id="addmembersform">
    <div>
    <input type="hidden" name="sesskey" value="<?php p(sesskey()); ?>" />

    <table class="assignTable" summary="">
    <tr>
	 <td id='potentialcell' class="potentialcell">
          <p>
            <label for="addselect"><?php echo get_string('coursenondepartment');?></label>
          </p>
		  <div id="department_select_wrapper" class="userselector">
			<select multiple="multiple" name = "department_course[]" size = '20'>
				<?php $courses->course_option_list('non_department_course'); ?>
			</select>
             <?php echo getEDAstric('lower-assign-filter',$display);?>
		<!--	<div>
				<label for="addselect_searchtext"><?php echo get_string('search');?></label>
				<input type="text" value="" size="15" id="department_addselect_searchtext" name="addselect_searchtext">
				<input type="button" value="Clear" id="department_addselect_clearbutton" >
			</div>-->
              
              <div class="searchBoxDiv">
                <div id="search_non_dept_course"><div class="search-input" ><input class="searchBoxInput" type="text" value="" placeholder="Search" id="department_addselect_searchtext" name="addselect_searchtext" ></div></div>
                
                <div class="search_clear_button search_non_dept_course"><input type="button" title="Search" id="department_addselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="department_addselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
          
            </div>
            
		  </div>
      </td>
      <td id='buttonscell'>
        <div class="arrow_button">
            <input class="moveLeftButton" name="remove" id="department_remove" type="button" value="<?php echo get_string('remove').'&nbsp;'.$OUTPUT->rarrow(); ?>" title="<?php print_string('remove'); ?>" disabled = ""/><input class="moveRightButton" name="add" id="department_add" type="button" value="<?php echo $OUTPUT->larrow().'&nbsp;'.get_string('add'); ?>" title="<?php print_string('add'); ?>" disabled = ""/>
        </div>
      </td>
	   <td id='existingcell' class="potentialcell">
          <p>
            <label for="removeselect"><?php echo get_string('coursedepartment');?></label>
          </p>
          <div id="department_remove_wrapper" class="userselector">
			<select multiple="multiple" name = "add_department_course[]" size = '20'>
				<?php $courses->course_option_list('department_course'); ?>
			</select>
             <?php echo getEDAstric('lower-assign-filter',$display);?>
			<!--<div>
				<label for="removeselect_searchtext"><?php echo get_string('search');?></label>
				<input type="text" value="" size="15" id="department_removeselect_searchtext" name="removeselect_searchtext">
				<input type="button" value="Clear" id="department_removeselect_clearbutton" >
			</div>-->
            
              <div class="searchBoxDiv">
                <div id="search_dept_course"><div class="search-input" ><input class="searchBoxInput" type="text" value="" placeholder="Search" id="department_removeselect_searchtext" name="removeselect_searchtext" ></div></div>
                
                <div class="search_clear_button search_dept_course"><input type="button" title="Search" id="department_removeselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="department_removeselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
          
            </div>
            
		  </div>
        </td>
    </tr>
    </table>
    </div>
</div>
<?php
echo '</div>																					
  </div>';
}
if($USER->archetype == $CFG->userTypeAdmin || !empty($departmentCourse)){
echo ' <div class="tabcontent paddingAll '.$hide.'" id="cont-2-1">';
  ?>
<div id="addmembersform">
    <div>
    <input type="hidden" name="sesskey" value="<?php p(sesskey()); ?>" />

    <table class="assignTable" summary="">
    <tr>
	 <td id='potentialcell' class="potentialcell">
          <p>
            <label for="addselect"><?php echo get_string('coursenonteams')." (".get_string('department').")";?></label>
          </p>
		  <div id="team_addselect_wrapper" class="userselector">
			<select multiple="multiple" name = "addteam[]" size = '20'>
				<?php $courses->course_option_list('non_team_course'); ?>
			</select>
		<!--	<div>
				<label for="addselect_searchtext"><?php echo get_string('search');?></label>
				<input type="text" value="" size="15" id="team_addselect_searchtext" name="addselect_searchtext">
				<input type="button" value="Clear" id="team_addselect_clearbutton" >
			</div>-->
            
             <div class="searchBoxDiv">
                <div id="search_non_team_course"><div class="search-input" ><input class="searchBoxInput" type="text" value="" placeholder="Search" id="team_addselect_searchtext" name="addselect_searchtext" ></div></div>
                
                <div class="search_clear_button search_non_team_course"><input type="button" title="Search" id="team_addselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="team_addselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
          
            </div>
                    
                    
		  </div>
      </td>
      <td id='buttonscell'>
        <div class="arrow_button">
            <input class="moveLeftButton" name="remove" id="team_remove" type="button" value="<?php echo get_string('remove').'&nbsp;'.$OUTPUT->rarrow(); ?>" title="<?php print_string('remove'); ?>" disabled = ""/><input class="moveRightButton" class = "date-popup" name="add" id="team_add" type="button" value="<?php echo $OUTPUT->larrow().'&nbsp;'.get_string('add'); ?>" title="<?php print_string('add'); ?>" disabled = ""/>
        </div>
      </td>
	   <td id='existingcell' class="potentialcell">
          <p>
            <label for="removeselect"><?php echo get_string('courseteams');?></label><a href="#" name="updateteam" id="updateTeam" class="updateDate" title = "<?php echo get_string('update_date'); ?>">update</a>
          </p>
          <div id="team_removeselect_wrapper" class="userselector">
			<select multiple="multiple" name = "addteamcourses[]" size = '20'>
				<?php $courses->course_option_list('team_course'); ?>
			</select>
			<!--div>
				<label for="removeselect_searchtext"><?php echo get_string('search');?></label>
				<input type="text" value="" size="15" id="team_removeselect_searchtext" name="removeselect_searchtext">
				<input type="button" value="Clear" id="team_removeselect_clearbutton" >
			</div-->
            
             <div class="searchBoxDiv">
                <div id="search_team_course"><div class="search-input" ><input class="searchBoxInput" type="text" value="" placeholder="Search" id="team_removeselect_searchtext" name="removeselect_searchtext" ></div></div>
                
                <div class="search_clear_button search_team_course"><input type="button" title="Search" id="team_removeselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="team_removeselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
          
            </div>
                    
		  </div>
        </td>
    </tr>
    </table>
    </div>
</div>
<?php
echo '</div>';
}else{	
}
	echo '<div class="tabcontent paddingAll" id="cont-3-1">';
 ?>
<div id="addmembersform">
    <div>
    <input type="hidden" name="sesskey" value="<?php p(sesskey()); ?>" />

    <table class="assignTable">
    <tr>
	 <td id='potentialcell' class="potentialcell">
          <p>
            <label for="addselect"><?php echo get_string('coursenonuser')." (".get_string('username').")";?></label>
          </p>
		  <div id="user_addselect_wrapper" class="userselector">
                <select multiple="multiple" name = "adduser[]" size = '20'>
                    <?php $courses->course_option_list('non_user_course'); ?>
                </select>
              
		  </div>
          
           <fieldset>
                <legend><?php echo get_string('filter_legend');?></legend>
                <div class="non-enrolled-user-filter-box">
                 
                    <div class="searchBoxDiv">
                        <!--label for="addselect_searchtext">'.get_string('search').'</label-->
                        <div id="search-form"><div class="search-input" ><input class="searchBoxInput" type="text" value="" placeholder="Search" id="user_addselect_searchtext" name="addselect_searchtext" ></div></div>
                        
                        <div class="search_clear_button search-form-non-user"><input type="button" title="Search" id="user_addselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="user_addselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
                        
                    </div>
                        
                        
                    <?php 
                    ob_start();
                    $filterBox = "non-enrolled-";
                    $filterBoxNonEnrolled = $filterBox;
                    $filter = 1;
                    require_once($CFG->dirroot . '/local/includes/non_enrolled_user_filter.php');
                    $SEARCHHTML = ob_get_contents();
                    ob_end_clean();
                    echo $SEARCHHTML;
                    ?>
                </div>
           </fieldset>
      </td>
      <td id='buttonscell'>
        <div class="arrow_button">
            <input class="moveLeftButton" name="remove" id="user_remove" type="button" value="<?php echo get_string('remove').'&nbsp;'.$OUTPUT->rarrow(); ?>" title="<?php print_string('remove'); ?>" disabled = ""/><input class="moveRightButton" name="add" id="user_add" type="button" value="<?php echo $OUTPUT->larrow().'&nbsp;'.get_string('add'); ?>" title="<?php print_string('add'); ?>" disabled = ""/>
        </div>
      </td>
	   <td id='existingcell' class="potentialcell">
          <p>
            <label for="removeselect"><?php echo get_string('courseuser');?></label><a href="#" name="updateuser" id="updateUser" class="updateDate" title = "<?php echo get_string('update_date'); ?>">update</a><a href="<?php echo $CFG->wwwroot.'/local/upload_csv_users.php?courseid='.$courseid; ?>" name="uploadcsvuser" id="uploadcsvUser" class="uploadCSV" title = "<?php echo get_string('upload_csv'); ?>">upload csv</a>
          </p>
          <div id="user_removeselect_wrapper" class="userselector">
                <select multiple="multiple" name = "addusercourse[]" size = '20'>
                    <?php $courses->course_option_list('user_course'); ?>
                </select>
		  </div>
          
           <fieldset>
                <legend><?php echo get_string('filter_legend');?></legend>
                <div class="enrolled-user-filter-box">
                     <div class="searchBoxDiv">
                        <!--label for="removeselect_searchtext">'.get_string('search').'</label-->
                        <div id="search-form"><div class="search-input" ><input class="searchBoxInput" type="text" value="" placeholder="Search" id="user_removeselect_searchtext" name="removeselect_searchtext" ></div></div>
                        
                        <div class="search_clear_button search-form-user"><input type="button" title="Search" id="user_removeselect_searchtext_btn" value="Search" name="search"><a title="Clear"  id="user_removeselect_clearbutton"  href="javascript:void(0);">Clear</a></div>
                  
                    </div>
                    <?php 
                    ob_start();
                    $filterBox = "enrolled-";
                    $filterBoxEnrolled = $filterBox;
                    $filter = 2; 
                    require($CFG->dirroot . '/local/includes/enrolled_user_filter.php');
                    $SEARCHHTML = ob_get_contents();
                    ob_end_clean();
                    echo $SEARCHHTML;
                    ?>
                </div>
           </fieldset>
        </td>
    </tr>
    </table>
    </div>
    
</div>


<?php
$downloadLink = 1;
echo '</div>';

echo '<div class="tabcontent paddingAll '.$hide2.'" id="cont-4-1">';
        echo '<div class="uploaduserresults"></div>';
	echo "<div class = 'upload-user-div' style='display: block;'>";
		echo '<a href="'.$securewwwroot.'/local/download.php?download='.$downloadLink.'" class="download-sample">'.get_string('download_sample').'</a>';
		echo '<div id="user_listOuter"><input type = "file" name = "user_list" id = "user_list"></div>
					<input type = "hidden" name = "upload_type" value = "1">
                                        <input type = "hidden" name = "courseid" id = "courseid" value = "'.$courseid.'">
                                        <input type = "hidden" name = "action" id = "action" value = "csv_user_upload_into_course">
					<input type = "button" name = "upload" value = "upload" id = "upload-submit">';
	echo '</div>';
echo '</div>';
echo '</div></div>';
?>
</fieldset>
</form>
<a href="#end-date-div" id="inline-iframe" rel="inline-iframe" title="<?php print_string('allocation'); ?>">click</a>

<div class="end-date-div" id="end-date-div">
	<iframe src="" id="i-frame" width="100%" height="100%"></iframe>
</div>
<script>
    
    $('#upload-submit').click(function(){
			var filename = $("#user_list").val();
			if($.trim(filename) == ''){
				alert('Please select file');
				return false;
			}
                        var file_data = $('#user_list').prop('files')[0];
                        var action_data = $("#action").val();
                        var course_id = $("#courseid").val();

                        var form_data = new FormData();                  
                        form_data.append('user_list', file_data);
                        form_data.append('action', action_data);
                        form_data.append('courseid', course_id);
                        
                        $.ajax({
                                url: '<?php echo $CFG->wwwroot;?>/local/upload_users_into_course.php', // point to server-side PHP script 
                                contentType: false,
                                processData: false,
                                data: form_data,                         
                                type: 'post',
                                success: function(data){
                                    //alert(data); // display response from the PHP script, if any
                                    $('.uploaduserresults').html(data);
                                    //window.location.reload();
                                }
                        });
			//$("#assignform").submit();
		});
function saveDepartmentData(assignElement,assignTo,assignId,elementList){
	if(assignElement != '' && assignTo != '' && assignId != '' && assignId != 0 && elementList != '' && elementList != 0){
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
				type:'POST',
				data:'action=saveCourseEnrolmentData&assignElement='+assignElement+'&assignTo='+assignTo+'&assignId='+assignId+'&elementList='+elementList+'&end_date=',
				success:function(data){
				}
		});
	}
}
function removeElementData(element,courseID,elementIds){
	if(elementIds != '' && elementIds != 0 && elementIds != ',' && courseID != '' && courseID != 0 && element != '' && element != 0 ){
		var queryString = 'action=saveCourseUnEnrolmentData&assignElement=course&assignTo='+element+'&assignId='+courseID+'&elementList='+elementIds;
		$.ajax({
				url		:	'<?php echo $CFG->wwwroot;?>/local/ajax.php',
				type	:	'POST',
				data	:	queryString,
				success	:	function(data){
				}
		});
	}
}
function sortOptions(optionsId){
	optionsId.sort(function(a,b) {
		if (a.text.toUpperCase() > b.text.toUpperCase()) return 1;
		else if (a.text.toUpperCase() < b.text.toUpperCase()) return -1;
		else return 0;
	});
	return optionsId;
}
$(document).ready(function(){
	var userRole = "<?php echo $USER->archetype; ?>";
	var userManager = "<?php echo $CFG->userTypeManager; ?>";
	var courseId = "<?php echo $courseid; ?>";
       
/***********Search starts****************/
	$("#department_addselect_searchtext_btn").click(function(){
		var searchText = $('#department_addselect_searchtext').val();
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				data:'action=searchCourseNonDepartment&courseid='+courseId+'&search_text='+searchText,
				success:function(data){
					$('#department_select_wrapper').find('select').html(data);
				}
		});
	});
	$("#department_addselect_clearbutton").click(function(){
		var searchText = '';
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				data:'action=searchCourseNonDepartment&courseid='+courseId+'&search_text='+searchText,
				success:function(data){
					$('#department_select_wrapper').find('select').html(data);
					$("#department_addselect_searchtext").val("");
				}
		});
	});

	$("#department_removeselect_searchtext_btn").click(function(){
		var searchText = $('#department_removeselect_searchtext').val();
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				data:'action=searchCourseDepartment&courseid='+courseId+'&search_text='+searchText,
				success:function(data){
					$('#department_remove_wrapper').find('select').html(data);
				}
		});
	});
	$("#department_removeselect_clearbutton").click(function(){
		var searchText = '';
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				data:'action=searchCourseDepartment&courseid='+courseId+'&search_text='+searchText,
				success:function(data){
					$('#department_remove_wrapper').find('select').html(data);
					$("#department_removeselect_searchtext").val("");
				}
		});
	});

	//************** TEAM ***************
	$("#team_addselect_searchtext_btn").click(function(){
		var searchText = $('#team_addselect_searchtext').val();
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				data:'action=searchCourseNonTeam&courseid='+courseId+'&search_text='+searchText,
				success:function(data){
					$('#team_addselect_wrapper').find('select').html(data);
				}
		});
	});
	$("#team_addselect_clearbutton").click(function(){
		var searchText = '';
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				data:'action=searchCourseNonTeam&courseid='+courseId+'&search_text='+searchText,
				success:function(data){
					$('#team_addselect_wrapper').find('select').html(data);
					$("#team_addselect_searchtext").val("");
				}
		});
	});

	$("#team_removeselect_searchtext_btn").click(function(){
		var searchText = $('#team_removeselect_searchtext').val();
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				data:'action=searchCourseTeam&courseid='+courseId+'&search_text='+searchText,
				success:function(data){
					$('#team_removeselect_wrapper').find('select').html(data);
				}
		});
	});
	$("#team_removeselect_clearbutton").click(function(){
		var searchText = '';
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				data:'action=searchCourseTeam&courseid='+courseId+'&search_text='+searchText,
				success:function(data){
					$('#team_removeselect_wrapper').find('select').html(data);
					$("#team_removeselect_searchtext").val("");
				}
		});
	});

	//************** USERS ***************
	//$("#user_addselect_searchdate_btn").click(function(){
	$("#start_date, #end_date").change(function() {
		var start_date = $("#start_date").val();
		var end_date = $("#end_date").val();		

		var sel_mode = $("input[name=<?php echo $filterBoxNonEnrolled;?>selector]:checked").val();
		var user_group = loopSelected('<?php echo $filterBoxNonEnrolled;?>user_group');
		var department = loopSelected('<?php echo $filterBoxNonEnrolled;?>department');
		//var managers = loopSelected('<?php echo $filterBoxNonEnrolled;?>managers');
		var managers = '-1';
		var team = loopSelected('<?php echo $filterBoxNonEnrolled;?>team');
		//var roles = loopSelected('<?php echo $filterBoxNonEnrolled;?>user_roles');
		var roles = '-1';
		
		var job_title = loopSelected('<?php echo $filterBoxNonEnrolled;?>job_title');
                var company = loopSelected('<?php echo $filterBoxNonEnrolled;?>company');
		
                var country = loopSelected('<?php echo $filterBoxNonEnrolled;?>country');
                 
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				//data:'action=searchCourseNonUser&courseid='+courseId+'&search_text='+searchText,
				data:'action=searchCourseNonUser&courseid='+courseId+'&start_date='+start_date+'&end_date='+end_date+'&sel_mode='+sel_mode+'&user_group='+user_group+'&department='+department+'&managers='+managers+'&team='+team+'&roles='+roles+'&job_title='+job_title+'&company='+company+'&country='+country,
				success:function(data){
					$('#user_addselect_wrapper').find('select').html(data);
				}
		});
	});

	
	$("#user_addselect_searchtext_btn").click(function(){
		var searchText = $("#user_addselect_searchtext").val();
		var sel_mode = $("input[name=<?php echo $filterBoxNonEnrolled;?>selector]:checked").val();
		var user_group = loopSelected('<?php echo $filterBoxNonEnrolled;?>user_group');
		var department = loopSelected('<?php echo $filterBoxNonEnrolled;?>department');
		//var managers = loopSelected('<?php echo $filterBoxNonEnrolled;?>managers');
		var managers = '-1';
		var team = loopSelected('<?php echo $filterBoxNonEnrolled;?>team');
		//var roles = loopSelected('<?php echo $filterBoxNonEnrolled;?>user_roles');
		var roles = '-1';
		
		var job_title = loopSelected('<?php echo $filterBoxNonEnrolled;?>job_title');
                var report_to = loopSelected('<?php echo $filterBoxNonEnrolled;?>report_to');
                var company = loopSelected('<?php echo $filterBoxNonEnrolled;?>company');
		
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				//data:'action=searchCourseNonUser&courseid='+courseId+'&search_text='+searchText,
				data:'action=searchCourseNonUser&courseid='+courseId+'&search_text='+searchText+'&sel_mode='+sel_mode+'&user_group='+user_group+'&department='+department+'&managers='+managers+'&team='+team+'&roles='+roles+'&job_title='+job_title+'&report_to='+report_to+'&company='+company,
				success:function(data){
					$('#user_addselect_wrapper').find('select').html(data);
				}
		});
	});
	$("#user_addselect_clearbutton").click(function(){
		var searchText = '';
		var sel_mode = $("input[name=<?php echo $filterBoxNonEnrolled;?>selector]:checked").val();
		var user_group = loopSelected('<?php echo $filterBoxNonEnrolled;?>user_group');
		var department = loopSelected('<?php echo $filterBoxNonEnrolled;?>department');
		//var managers = loopSelected('<?php echo $filterBoxNonEnrolled;?>managers');
		var managers = '-1';
		var team = loopSelected('<?php echo $filterBoxNonEnrolled;?>team');
		//var roles = loopSelected('<?php echo $filterBoxNonEnrolled;?>user_roles');
		var roles = '-1';
		
		var job_title = loopSelected('<?php echo $filterBoxNonEnrolled;?>job_title');
                var report_to = loopSelected('<?php echo $filterBoxNonEnrolled;?>report_to');
                var company = loopSelected('<?php echo $filterBoxNonEnrolled;?>company');
		
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				//data:'action=searchCourseNonUser&courseid='+courseId+'&search_text='+searchText,
				data:'action=searchCourseNonUser&courseid='+courseId+'&search_text='+searchText+'&sel_mode='+sel_mode+'&user_group='+user_group+'&department='+department+'&managers='+managers+'&team='+team+'&roles='+roles+'&job_title='+job_title+'&report_to='+report_to+'&company='+company,
				success:function(data){
					$('#user_addselect_wrapper').find('select').html(data);
					$("#user_addselect_searchtext").val("");
				}
		});
	});

	$("#user_removeselect_searchtext_btn").click(function(){
		var searchText = $("#user_removeselect_searchtext").val();
		
		var sel_mode = $("input[name=enrolled-selector2]:checked").val();
		var user_group = loopSelected('enrolled-user_group');
		var department = loopSelected('enrolled-department');
		//var managers = loopSelected('enrolled-managers');
		var managers = '-1';
		var team = loopSelected('enrolled-team');
		//var roles = loopSelected('enrolled-user_roles');
		var roles = '-1';
		
		var job_title = loopSelected('<?php echo $filterBoxEnrolled;?>job_title');
                var report_to = loopSelected('<?php echo $filterBoxEnrolled;?>report_to');
	    var company = loopSelected('<?php echo $filterBoxEnrolled;?>company');
		
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				//data:'action=courseUserList&courseid='+courseId+'&search_text='+searchText,
				data:'action=courseUserList&courseid='+courseId+'&search_text='+searchText+'&sel_mode='+sel_mode+'&user_group='+user_group+'&department='+department+'&managers='+managers+'&team='+team+'&roles='+roles+'&job_title='+job_title+'&report_to='+report_to+'&company='+company,
				success:function(data){
					$('#user_removeselect_wrapper').find('select').html(data);
				}
		});
	});
	$("#user_removeselect_clearbutton").click(function(){
		var searchText = '';
		
		var sel_mode = $("input[name=enrolled-selector]:checked").val();
		var user_group = loopSelected('enrolled-user_group');
		var department = loopSelected('enrolled-department');
		//var managers = loopSelected('enrolled-managers');
		var managers = '-1';
		var team = loopSelected('enrolled-team');
		//var roles = loopSelected('enrolled-user_roles');
		var roles = '-1';
		
		var job_title = loopSelected('<?php echo $filterBoxEnrolled;?>job_title');
                var report_to = loopSelected('<?php echo $filterBoxEnrolled;?>report_to');
                var company = loopSelected('<?php echo $filterBoxEnrolled;?>company');
		
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/searchpage.php',
				type:'POST',
				//data:'action=courseUserList&courseid='+courseId+'&search_text='+searchText,
				data:'action=courseUserList&courseid='+courseId+'&search_text='+searchText+'&sel_mode='+sel_mode+'&user_group='+user_group+'&department='+department+'&managers='+managers+'&team='+team+'&roles='+roles+'&job_title='+job_title+'&report_to='+report_to+'&company='+company,
				success:function(data){
					$('#user_removeselect_wrapper').find('select').html(data);
					$("#user_removeselect_searchtext").val("");
				}
		});
	});
/***********Search ends****************/

	var element = "";
	var assigntype = "course";
	var assignId = "";
	var elementList = "";
	var addRemove = "";
	$("#inline-iframe").colorbox({inline:true, width:"675px", height:"468px"});
        
	$(".tabLink").each(function(){
      $(this).click(function(){
        tabeId = $(this).attr("id");
        $(".tabLink").removeClass("activeLink");
        $(this).addClass("activeLink");
        $(".tabcontent").addClass("hide");
        $("#"+tabeId+"-1").removeClass("hide");					
        return false;	  
      });
    }); 
	 
	$("#addselect_searchtext").keyup(function(){
		var searchText = $(this).val();
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/user/searchcourses.php',
				type:'POST',
				data:'action=searchNonCourse&courseid=<?php echo $courseid; ?>&search_text='+searchText,
				success:function(data){
					$('#addselect_wrapper').find('select').html(data);
				}
		});
	});
	
	$("#addselect_clearbutton").click(function(){
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/user/searchcourses.php',
				type:'POST',
				data:'action=searchNonCourse&courseid=<?php echo $courseid; ?>',
				success:function(data){
					$('#addselect_wrapper').find('select').html(data);
					$("#addselect_searchtext").val('');
				}
		});
	});
	
	$("#removeselect_clearbutton").click(function(){
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/user/searchcourses.php',
				type:'POST',
				data:'action=searchUserCourse&courseid=<?php echo $courseid; ?>',
				success:function(data){
					$('#removeselect_wrapper').find('select').html(data);
					$("#removeselect_searchtext").val('');
				}
		});
	});
	
	$("#removeselect_searchtext").keyup(function(){
		var searchText = $(this).val();
		$.ajax({
				url:'<?php echo $CFG->wwwroot;?>/local/user/searchcourses.php',
				type:'POST',
				data:'action=searchUserCourse&courseid=<?php echo $courseid; ?>&search_text='+searchText,
				success:function(data){
					$('#removeselect_wrapper').find('select').html(data);
				}
		});
	});
	
	if(userRole != userManager){
		var fromDepartment = $('#department_select_wrapper select');
		var toDepartment = $('#department_remove_wrapper select');
	}

	var fromTeam = $('#team_addselect_wrapper select');
	var toTeam = $('#team_removeselect_wrapper select');

	var fromUser = $('#user_addselect_wrapper select');
	var toUser = $('#user_removeselect_wrapper select');

	$(document).on("change", "[name = 'department_course[]']",function(){ 
		$("#department_add").removeAttr("disabled");
	});
	$(document).on("change", "[name = 'add_department_course[]']",function(){
		$("#department_remove").removeAttr("disabled");
	});

	$(document).on("change", "[name = 'addteam[]']",function(){
		$("#team_add").removeAttr("disabled");
	});
	$(document).on("change", "[name = 'addteamcourses[]']",function(){
		$("#team_remove").removeAttr("disabled");
	});
	$(document).on("change", "[name = 'adduser[]']",function(){
		$("#user_add").removeAttr("disabled");
	});
	$(document).on("change", "[name = 'addusercourse[]']",function(){
		if($(this).attr('class') == 'option-disable'){
			$("#user_remove").addAttr("disabled");
		}else{
			$("#user_remove").removeAttr("disabled");                        
		}
	});

	$(document).on("click","#department_add",function(){
			var departmentList = '';
			fromDepartment.find('option:selected').each(function(){
				departmentList += $(this).val()+',';
			});
			if(departmentList == '' || departmentList == ','){
				alert("<?php echo get_string('select_department_to_enrol');?>");
				return false;
			}
			departmentList = departmentList.substr(0, departmentList.length - 1);
			saveDepartmentData('course','department',"<?php echo $courseid; ?>",departmentList);
			fromDepartment.find('option:selected').appendTo(toDepartment.find('optgroup'));
			maintainSelectCount(fromDepartment,toDepartment,"<?php echo get_string('departments'); ?>");
			toDepartmentOptionOptions = sortOptions(toDepartment.find('option'));
			toDepartment.find('optgroup').empty();
			toDepartment.find('optgroup').append(toDepartmentOptionOptions);
	});
	$(document).on("click","#department_remove",function(){
			var departmentList = '';
			toDepartment.find('option:selected').each(function(){
				departmentList += $(this).val()+',';
			});
			if(departmentList == '' || departmentList == ','){
				alert("<?php echo get_string('select_department_to_unenrol');?>");
				return false;
			}
			departmentList = departmentList.substr(0, departmentList.length - 1);

			var source = '<?php echo $CFG->wwwroot;?>/course/unenrolcourse.php?element=department&assigntype=course&assignId='+courseId+'&elementList='+departmentList;
			$("#i-frame").attr('src',source);
			$(".cboxContent #end-date-div").attr("style",'display:block');
			$("#inline-iframe").trigger("click");

			/*removeElementData('department',courseId,departmentList);
			toDepartment.find('option:selected').appendTo(fromDepartment.find('optgroup'));
			maintainSelectCount(fromDepartment,toDepartment,"<?php echo get_string('departments'); ?>");
			fromDepartmentOptionOptions = sortOptions(fromDepartment.find('option'));
			fromDepartment.find('optgroup').empty();
			fromDepartment.find('optgroup').append(fromDepartmentOptionOptions);*/
	});
	$(document).on("click","#team_add",function(){
			var teamList = '';
			fromTeam.find('option:selected').each(function(){
				teamList += $(this).val()+',';
			});	
			if(teamList == '' || teamList == ','){
				alert("<?php echo get_string('select_team_to_enrol');?>");
				return false;
			}
			teamList = teamList.substr(0, teamList.length - 1);
			addRemove = 1;
			element = 'team';
			var source = '<?php echo $CFG->wwwroot;?>/course/enrolcourse.php?element=team&assigntype=course&assignId='+courseId+'&elementList='+teamList;
			$("#i-frame").attr('src',source);
			$(".cboxContent #end-date-div").attr("style",'display:block');
			$("#inline-iframe").trigger("click");
			//saveElementData('team',courseId,teamList);
			//fromTeam.find('option:selected').appendTo(toTeam.find('optgroup'));
			//maintainSelectCount(fromTeam,toTeam,"<?php echo get_string('teams'); ?>");
	});
	$(document).on("click","#updateTeam",function(){
			var teamList = '';
			toTeam.find('option:selected').each(function(){
				teamList += $(this).val()+',';
			});	
			if(teamList == '' || teamList == ','){
				alert("<?php echo get_string('select_team_to_update');?>");
				return false;
			}
			teamList = teamList.substr(0, teamList.length - 1);
			addRemove = 1;
			element = 'team';
			var source = '<?php echo $CFG->wwwroot;?>/course/enrolcourse.php?element=team&action=update&assigntype=course&assignId='+courseId+'&elementList='+teamList;
			$("#i-frame").attr('src',source);
			$(".cboxContent #end-date-div").attr("style",'display:block');
			$("#inline-iframe").trigger("click");
	});

	$(document).on("click","#team_remove",function(){
		var teamList = '';
		toTeam.find('option:selected').each(function(){
			teamList += $(this).val()+',';
		});
		if(teamList == '' || teamList == ','){
			alert("<?php echo get_string('select_team_to_unenrol');?>");
			return false;
		}
		teamList = teamList.substr(0, teamList.length - 1);
		var source = '<?php echo $CFG->wwwroot;?>/course/unenrolcourse.php?element=team&assigntype=course&assignId='+courseId+'&elementList='+teamList;
		$("#i-frame").attr('src',source);
		$(".cboxContent #end-date-div").attr("style",'display:block');
		$("#inline-iframe").trigger("click");

		//toTeam.find('option:selected').appendTo(fromTeam.find('optgroup'));
		//maintainSelectCount(fromTeam,toTeam,"<?php echo get_string('teams'); ?>");
	});
	$(document).on("click","#user_add",function(){
			var userList = '';
			fromUser.find('option:selected').each(function(){
				userList += $(this).val()+',';
			});
			if(userList == '' || userList == ','){
				alert("<?php echo get_string('select_user_to_enrol');?>");
				return false;
			}
			addRemove = 1;
			element = 'user';
			userList = userList.substr(0, userList.length - 1);
			var source = '<?php echo $CFG->wwwroot;?>/course/enrolcourse.php?element=user&assigntype=course&assignId='+courseId+'&elementList='+userList;
			$("#i-frame").attr('src',source);
			$(".cboxContent #end-date-div").attr("style",'display:block');
			$("#inline-iframe").trigger("click");

			//saveElementData('user',courseId,userList);
			//fromUser.find('option:selected').appendTo(toUser.find('optgroup'));
			//maintainSelectCount(fromUser,toUser,"<?php echo get_string('users'); ?>");
	});
	$(document).on("click","#updateUser",function(){
			var userList = '';
			toUser.find('option:selected').each(function(){
				userList += $(this).val()+',';
			});
			if(userList == '' || userList == ','){
				alert("<?php echo get_string('select_user_to_update');?>");
				return false;
			}
			addRemove = 1;
			element = 'user';
			userList = userList.substr(0, userList.length - 1);
			var source = '<?php echo $CFG->wwwroot;?>/course/enrolcourse.php?element=user&action=update&assigntype=course&assignId='+courseId+'&elementList='+userList;
			$("#i-frame").attr('src',source);
			$(".cboxContent #end-date-div").attr("style",'display:block');
			$("#inline-iframe").trigger("click");
	});
        
        /*$(document).on("click","#uploadcsvUser",function(){
						
			var source = '<?php echo $CFG->wwwroot;?>/local/upload_csv_users.php?element=user&action=update&assigntype=course&courseid='+courseId;
			//$("#i-frame").attr('src',source);                                              
			//$("#inline-iframe").trigger("click");
                         $("#uploadcsvUser").colorbox({iframe:true, width:"675px", height:"468px"});                 
                       // $("#inline-iframe").colorbox({inline:true, width:"675px", height:"468px"});
                     
	});*/
	$(document).on("click","#user_remove",function(){
		var err = 0;
		var userList = '';
		toUser.find( "option:selected" ).each(function() {
			if($(this).attr('class') == 'option-disable'){
				err = 1;
			}else{
				userList += $(this).val()+',';
			}
		});
		if(userList == '' || userList == ','){
			alert("<?php echo get_string('select_user_to_unenrol');?>");
			return false;
		}
		userList = userList.substr(0, userList.length - 1);
		var source = '<?php echo $CFG->wwwroot;?>/course/unenrolcourse.php?element=user&assigntype=course&assignId='+courseId+'&elementList='+userList;
		$("#i-frame").attr('src',source);
		$(".cboxContent #end-date-div").attr("style",'display:block');
		$("#inline-iframe").trigger("click");
		/*removeElementData('user',courseId,userList);
		if(err == 1){
			alert("<?php echo get_string('assignment_error');?>");
			toUser.find( "option" ).each(function() {
				//$(this).removeAttr('selected');
			});
		}
		maintainSelectCount(fromUser,toUser,"<?php echo get_string('users'); ?>");*/
	});
	$("#id_submitbutton").click(function(){
		if(userRole != userManager){
			fromDepartment.find('optgroup').find("option").each(function(){
				$(this).removeAttr("selected");
			});
		}
		fromTeam.find('optgroup').find("option").each(function(){
			$(this).removeAttr("selected");
		});
		fromUser.find('optgroup').find("option").each(function(){
			$(this).removeAttr("selected");
		});
		if($("#department_all").is(':checked')){
			if(userRole != userManager){
				fromDepartment.find('optgroup').find("option").each(function(){
					$(this).appendTo(toDepartment.find('optgroup'));
				});
			}
			fromTeam.find('optgroup').find("option").each(function(){
				$(this).appendTo(toTeam.find('optgroup'));
			});
			fromUser.find('optgroup').find("option").each(function(){
				$(this).appendTo(toUser.find('optgroup'));
			});
		}
		if(userRole != userManager){
			toDepartment.find('option').prop('selected', true);
		}
		toTeam.find('option').prop('selected', true);
		toUser.find('option').prop('selected', true);
		$("#assignform").submit();
	});
	$("#department_all").click(function(){
		if($("#department_all").is(':checked')){
			$(".tab-box").hide();
			$(".tabcontent").hide();
		}else{
			$(".tab-box").show();
			$(".tabcontent").show();
		}
	});
	$('#id_cancel').click(function(){
		window.location.href = '<?php $urlCancel = new moodle_url($CFG->wwwroot.'/course/courseview.php', array('id'=>$courseid)); echo $urlCancel; ?>';
	});
	/*  Below code is being used for focus on departments/teams/users automatically while we are using chrome*/
	/*$('[name = "department_course[]"]').focus();
	$('[name = "department_course[]"]').focus(function () {
      $(this).select();
    });
	*/
	// End
        
        $("#uploadcsvUser").colorbox({iframe:true, width:"675px", height:"468px",title:"CSV Enrollment",onClosed: function () {

                    window.location.reload();

                }});
        defaultTab();
        function defaultTab(){
            $("#cont-3").trigger("click");
        }
 
});
</script>
<style>
#end-date-div{ display:none;}
#colorbox #end-date-div{display:block}
#inline-iframe{ position:absolute; left:-100000px;}
</style>
<?php
echo '</div>';
echo $OUTPUT->footer();