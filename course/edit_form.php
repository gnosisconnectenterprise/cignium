<?php

defined('MOODLE_INTERNAL') || die;


require_once($CFG->libdir.'/formslib.php');
require_once($CFG->libdir.'/completionlib.php');


require_once($CFG->libdir. '/coursecatlib.php');

// for course setting only
/*require_once($CFG->dirroot . '/local/user/selector/lib.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->libdir . '/filelib.php');*/
// end


/**
 * The form for handling editing a course.
 */
 
/* class abc extends moodleform_mod {
   
   function get_button(){
   
      $this->add_action_buttons(true, get_string('saveandcontinue', 'course'), 'exit');
   
   }
 
 }*/
class course_edit_form extends moodleform {
    protected $course;
    protected $context;

    /**
     * Form definition.
     */
    function definition() {
	
        global $CFG, $PAGE, $USER, $DB, $OUTPUT;
		
		//$userrole =  getUserRole($USER->id);
        
        $mform    = $this->_form;
        $PAGE->requires->yui_module('moodle-course-formatchooser', 'M.course.init_formatchooser',
                array(array('formid' => $mform->getAttribute('id'))));

        $course        = $this->_customdata['course']; // this contains the data of this form
        $category      = $this->_customdata['category'];
        $editoroptions = $this->_customdata['editoroptions'];
        $returnto = $this->_customdata['returnto'];

        $systemcontext   = context_system::instance();
        $categorycontext = context_coursecat::instance($category->id);

        if (!empty($course->id)) {
            $coursecontext = context_course::instance($course->id);
            $context = $coursecontext;
        } else {
            $coursecontext = null;
            $context = $categorycontext;
        }

        $courseconfig = get_config('moodlecourse');

        $this->course  = $course;
        $this->context = $context;
		
		$mform->addElement('html',"<div class='tabsOuter'>");

		$mform->addElement('html',"<div class='tabLinks'>");
		$courseHTML = '';
		include_once('course_tabs.php');		
		$mform->addElement('html',$courseHTML);
		$mform->addElement('html',"</div>");
		$mform->addElement('html','<div class="userprofile add_course">');

		//$mform->addElement('html', '<div class= "user-image">');
		//	 $mform->addElement('static', 'currentpicture', get_string('currentpicture'));
		//	$mform->addElement('html', '</div>');
        // Form definition with new course defaults.
        $mform->addElement('header','general', get_string('general', 'form'));

        $mform->addElement('hidden', 'returnto', null);
        $mform->setType('returnto', PARAM_ALPHANUM);
        $mform->setConstant('returnto', $returnto);

        $mform->addElement('text','fullname', get_string('course_name'),'maxlength="254" size="50"');
        //$mform->addElement('html',"<span style='padding-left:200px;'>".get_string('coursenamewatermarktext')."</span>");
        $mform->addElement('static', 'courseversionwatermarktext', '', get_string('coursenamewatermarktext').'.');
        //$mform->addHelpButton('fullname', 'fullnamecourse');
        $mform->addRule('fullname', get_string('required'), 'required', null, 'client');
        $mform->setType('fullname', PARAM_TEXT);
        if (!empty($course->id) and !has_capability('moodle/course:changefullname', $coursecontext)) {
            $mform->hardFreeze('fullname');
            $mform->setConstant('fullname', $course->fullname);
        }
		
		$mform->addElement('text','idnumber', get_string('idnumbercourse'),'maxlength="100"  size="10"');
        //$mform->addHelpButton('idnumber', 'idnumbercourse');
        $mform->setType('idnumber', PARAM_RAW);
        if (!empty($course->id) and !has_capability('moodle/course:changeidnumber', $coursecontext)) {
            $mform->hardFreeze('idnumber');
            $mform->setConstants('idnumber', $course->idnumber);
        }
		
		/*
		$mform->addElement('checkbox','publish', get_string('publish'),'<div class="fl label_text itemnote">'.get_string("publish_course_form", 'course').'</div>',array('style'=>'float:left;'));
		if($course->publish && $course->publish ==1){
			$mform->hardFreeze('publish');
		}
		*/
		if (empty($course->id)) {
            if (has_capability('moodle/course:create', $categorycontext)) {
			
				  /*if(in_array($userrole, $CFG->custommanagerroleid)){ // hack by rajesh while login with super admin (1)
				  
					$mform->addElement('hidden', 'category', null);
					$mform->setType('category', PARAM_INT);
					$mform->setConstant('category', $category->id);
					
				  }else{*/
					$displaylist = coursecat::make_categories_list('moodle/course:create');
					$mform->addElement('select', 'category', get_string('coursecategory'), $displaylist);
					//$mform->addHelpButton('category', 'coursecategory');
					$mform->setDefault('category', $category->id);
				  //}
			  	
            } else {
                $mform->addElement('hidden', 'category', null);
                $mform->setType('category', PARAM_INT);
                $mform->setConstant('category', $category->id);
            }
        } else {
            if (has_capability('moodle/course:changecategory', $coursecontext)) {
                $displaylist = coursecat::make_categories_list('moodle/course:create');
                if (!isset($displaylist[$course->category])) {
                    //always keep current
                    $displaylist[$course->category] = coursecat::get($course->category, MUST_EXIST, true)->get_formatted_name();
                }
                $mform->addElement('select', 'category', get_string('coursecategory'), $displaylist);
               // $mform->addHelpButton('category', 'coursecategory');
            } else {
                //keep current
                $mform->addElement('hidden', 'category', null);
                $mform->setType('category', PARAM_INT);
                $mform->setConstant('category', $course->category);
            }
        }
		
		
		 
		// course type
		 $courseTypeArr = array();
		 $courseTypeList = getCourseType();
		 if(count($courseTypeList) >0 ){
			 foreach($courseTypeList as $arr){
			    $courseTypeArr[$arr->id] = $arr->coursetype;
			 }
		 }

		 $mform->addElement('select', 'coursetype_id', get_string('coursetype'), $courseTypeArr);
		 //$mform->addHelpButton('coursetype_id', 'coursetype');
		 $mform->addRule('coursetype_id', get_string('required'), 'required', null, 'client');
		 $mform->setDefault('coursetype_id', 1);
		 if (!empty($course->id)) {
		 
            $mform->hardFreeze('coursetype_id');
            $mform->setConstant('coursetype_id', $course->coursetype_id);
           
         }
		 
		 // course type end
         if($CFG->isCertificate==1){
         	$mform->addElement('checkbox', 'is_certificate', get_string('iscertificaterequired'),'');
         	$mform->setDefault('is_certificate', 1);
         }
		 
		 if( !empty($course->id) && $course->coursetype_id == $CFG->courseTypeClassroom){
		 }else{
		 
		     // course criteria
			 
				 $courseCriteriaArr = getCourseCriteria(); 
				 $mform->addElement('select', 'criteria', get_string('coursecriteria'), $courseCriteriaArr, 'style="width:117px"');
				 //$mform->addHelpButton('criteria', 'coursecriteria');
				 $mform->addRule('criteria', get_string('required'), 'required', null, 'client');
				 $mform->setDefault('criteria', $CFG->courseCriteriaElective);
				 
				 $mform->addElement('checkbox', 'is_global', get_string('isglobalcourse','course'),'','style="margin-bottom: 16px"');
				 
				 if (!empty($course->id)) {
					//$mform->hardFreeze('criteria');
					//$mform->setConstant('criteria', $course->criteria);
					
					//$mform->hardFreeze('is_global');
					//$mform->setConstant('is_global', $course->is_global);
				 }
				 
			 // course criteria end
			 
			 // credit hours
			 
			 
			  
			    if (!empty($course->id)) {  
				     $isCoursePublished = isCoursePublished($course->id);
					 if($isCoursePublished){
					   $duration_arr = durationArray();
					   $mform->addElement('select','credithours', get_string('coursecredithourswithtime'),$duration_arr, array('style'=>'min-width:200px', 'disabled'=>true));
					 }else{
					   $duration_arr = durationArray();
					   $mform->addElement('select','credithours', get_string('coursecredithourswithtime'),$duration_arr, array('style'=>'min-width:200px'));
					 }
					
				}else{
				   $duration_arr = durationArray();
				   $mform->addElement('select','credithours', get_string('coursecredithourswithtime'),$duration_arr, array('style'=>'min-width:200px'));
				   // $mform->addElement('text','credithours', get_string('coursecredithours'),'maxlength="3"  size="10"');
				}
			 // credit hours
		 
		 }	 
         
		 
		
		 
		
		 
		 // primary instructor
		 $instructorArr[''][''] = get_string('selectinstructor');
		 $instructorList = getInstructor();
		 if(count($instructorList) >0 ){
			 foreach($instructorList as $arr){
			    $roleIdArr = getUserRoleDetails($arr->id);
				$roleName = $roleIdArr->name;
				if($roleName){
				  $roleName = ucfirst($roleName);
			      $instructorArr[$roleName][$arr->id] = $arr->fullname;
				}
			 }
		 }
         //$elementName=null, $elementLabel=null, $optgrps=null, $attributes=null, $showchoose=false
		 $mform->addElement('selectgroups', 'primary_instructor', get_string('primaryinstructor'), $instructorArr, array('style'=>'min-width:200px'));
		 //$mform->addHelpButton('primary_instructor', 'primaryinstructor');
		// $mform->addRule('primary_instructor', get_string('required'), 'required', null, 'client');
        
		 
		 // primary instructor end
		 
		 // classroom Instruction
		
		$mform->addElement('editor','classroom_instruction_editor', get_string('classroominstruction'), null, $editoroptions);
       // $mform->addHelpButton('classroom_instruction_editor', 'classroominstruction');
        $mform->setType('classroom_instruction_editor', PARAM_RAW);
        //$summaryfields = 'classroom_instruction_editor';
		
		//$mform->addElement('text','classroom_instruction', get_string('classroominstruction'),'maxlength="254" size="50"');
       // $mform->addHelpButton('classroom_instruction', 'classroominstruction');
        //$mform->addRule('classroom_instruction', get_string('required'), 'required', null, 'client');
       // $mform->setType('classroom_instruction', PARAM_TEXT);
		
		// classroom instruction end 
		
		// classroom recommendation course duration
		 
		$mform->addElement('text','classroom_course_duration', get_string('recommendationcourseduration'),'maxlength="254" size="50"');
        //$mform->addHelpButton('classroom_course_duration', 'recommendationcourseduration');
       // $mform->addRule('classroom_course_duration', get_string('required'), 'required', null, 'client');
        $mform->setType('classroom_course_duration', PARAM_TEXT);
		
		// classroom recommendation course duration end
		
		
		 if (!empty($course->id)) {
		  $created_on = getDateFormat($course->timecreated, $CFG->customDefaultDateFormat);
            $mform->addElement('html','<div class="fitem "><div class="fitemtitle"><div class="fstaticlabel"><label>'.get_string("timecreated","program").'</label></div></div><div class="felement fstatic">'.$created_on.'</div></div>');
		 }
		
                 //Auto enrollment feature
                
                
                 if($CFG->enable_autoenrollment_course==1){                     
                     
                        $mform->addElement('header', 'autoenrollment', get_string('autoenrollment'));
                        $mform->setExpanded('autoenrollment');
                       /* $mform->addElement('checkbox', 'new_hire', get_string('new_hire'),'');
                        $mform->setDefault('new_hire', 0);
                        $mform->addElement('checkbox', 'manager_us', get_string('manager_us'),'');
                        $mform->setDefault('manager_us', 0);
                        $mform->addElement('checkbox', 'manager_non_us', get_string('manager_non_us'),'');
                        $mform->setDefault('manager_non_us', 0);*/
                        $enrollment_criteria = autoEnrollmentCriteriaArray();
                        $mform->addElement('select','enrollment_criteria', get_string('enrollment_criteria'),$enrollment_criteria, array('style'=>'min-width:200px'));
                        $mform->getElement('enrollment_criteria')->setMultiple(true);
                        
                        //New enrollment criteria(title)
                        $title_criteria = autoTitleCriteriaArray();
                        $arr = array('0' => 'Please Select');
                        
                        $title_criteria = $arr + $title_criteria;
                        $mform->addElement('select','title_criteria', get_string('title_criteria'),$title_criteria, array('style'=>'min-width:200px'));
                        $mform->getElement('title_criteria')->setMultiple(true);
                        
                        
                        $selectedCriteriaArr = array();
                        $selectedTitleCriteriaArr = array();
                        $enrollment_duration_unit = 'year';
                        $enrollment_duration = '';
                        if( !empty($course->id)){
                            $selectedCriteria = $DB->get_record_sql("SELECT enrollment_criteria, title_criteria, enrollment_duration_unit,enrollment_duration FROM mdl_course_autoenrollment_criteria WHERE courseid=".$course->id);
                            $selectedCriteriaArr = array();
                            $selectedTitleCriteriaArr = array();
                            if($selectedCriteria){
                                $selectedCriteriaArr = explode(',', $selectedCriteria->enrollment_criteria);
                                $selectedTitleCriteriaArr = explode(',', $selectedCriteria->title_criteria);
                                $enrollment_duration_unit = $selectedCriteria->enrollment_duration_unit;
                                $enrollment_duration = $selectedCriteria->enrollment_duration;
                            }                           
                        }
//                        pr($selectedTitleCriteriaArr); die;
                        $mform->getElement('enrollment_criteria')->setSelected($selectedCriteriaArr);
                        $mform->getElement('title_criteria')->setSelected($selectedTitleCriteriaArr);
                        
                        $enrollmentDurationUnit = enrollmentDurationUnitArray();                        
                        $mform->addElement('select','enrollment_duration_unit', get_string('enrollment_duration_unit'),$enrollmentDurationUnit, array('style'=>'min-width:200px'));
                        $mform->setDefault('enrollment_duration_unit',$enrollment_duration_unit);
                        
                        $duration_enrollment = enrollmentDurationArray();                        
                        $mform->addElement('select','enrollment_duration', get_string('enrollment_duration'),$duration_enrollment, array('style'=>'min-width:200px'));
                 
                        $mform->setDefault('enrollment_duration',$enrollment_duration);
                        }
                 
                        //Code for Auto Annual Enrollments
                        if($CFG->enable_auto_annual_enrollment_course==1){
                            $mform->addElement('header', 'autoannualenrollment', get_string('autoannualenrollment'));
                            $mform->setExpanded('autoannualenrollment');
                            
                            $coursesChoices = getActiveCoursesChoices();
                             if (!empty($course->id) && isset($coursesChoices[$course->id])) {
                                 unset($coursesChoices[$course->id]);
                             }
                            $mform->addElement('select','parent_courseid', get_string('previous_course'),$coursesChoices, array('style'=>'min-width:200px'));
                            
                            $mform->addElement('date_selector', 'ann_enroll_startdate', get_string('enrollmentstartdate'));
                            
                            
                            $enrollmentDurationUnit = enrollmentDurationUnitArray();                        
                            $mform->addElement('select','ann_enrollment_duration_unit', get_string('enrollment_duration_unit'),$enrollmentDurationUnit, array('style'=>'min-width:200px'));
                            $mform->setDefault('ann_enrollment_duration_unit',$enrollment_duration_unit);

                            $duration_enrollment = enrollmentDurationArray();                        
                            $mform->addElement('select','ann_enrollment_duration', get_string('enrollment_duration'),$duration_enrollment, array('style'=>'min-width:200px'));

                            $mform->setDefault('ann_enrollment_duration',$enrollment_duration);
                            
                            $annEnrollmentCriteria = selectionCriteriaChoices();                        
                            $mform->addElement('select','ann_enrollment_criteria', get_string('ann_enrollment_criteria'),$annEnrollmentCriteria, array('style'=>'min-width:200px'));
                            $mform->setDefault('ann_enrollment_criteria',$annEnrollmentCriteria);
                            
                           // $mform->addElement('text','version', get_string('version'),'maxlength="254" size="50"');
                            
                            $ann_duration_enrollment = enrollmentDurationInMonthsArray();                        
                            $mform->addElement('select','ann_enrollment_criteria_interval', get_string('ann_enrollment_criteria_duration'),$ann_duration_enrollment, array('style'=>'min-width:200px'));
                            $mform->setDefault('ann_enrollment_criteria_interval',$ann_duration_enrollment[12]);
                        }
                        //End code of Annual Enrollment
                 
		//$mform->addElement('html','<div class="fitem"><div class="fl italicmsg">'.get_string("publish_course_form", 'course').'</div></div>');
		$mform->addElement('header', 'descriptionhdr', get_string('description'));
       //$mform->setExpanded('descriptionhdr');
     
		$mform->addElement('editor','summary_editor', get_string('moduleintro'), null, $editoroptions);
        //$mform->addHelpButton('summary_editor', 'coursesummary');
        $mform->setType('summary_editor', PARAM_RAW);	
        $summaryfields = 'summary_editor';

		$mform->addElement('editor','suggesteduse_editor', get_string('suggested_use'), null, $editoroptions);
        //$mform->addHelpButton('suggested_use', 'coursesummary');
        $mform->setType('suggesteduse_editor', PARAM_RAW);
        $summaryfields = 'suggesteduse_editor';


		$mform->addElement('editor','learningobj_editor', get_string('learning_obj'), null, $editoroptions);
        //$mform->addHelpButton('learning_obj', 'coursesummary');
        $mform->setType('learningobj_editor', PARAM_RAW);
        $summaryfields = 'learningobj_editor';

	$mform->addElement('editor','performanceout_editor', get_string('performance_out'), null, $editoroptions);
        //$mform->addHelpButton('performance_out', 'coursesummary');
        $mform->setType('performanceout_editor', PARAM_RAW);
        $summaryfields = 'performanceout_editor';
        

       /*
       $editoroptions['enable_filemanagement'] = false;
       $editoroptions['maxfiles'] = 0;
       $editoroptions['maxbytes'] = 0;
       $editoroptions['subdirs'] = 0;
        $editoroptions['trusttext'] = 0;
       pr($editoroptions);*/
        //Add field to take input of enrolment email content
        //$mform->addElement('editor','courseemail_editor', get_string('courseemailcontent','course'), null, $editoroptions);   
       // $mform->setType('courseemail_editor', PARAM_RAW);
        //$summaryfields = 'courseemail_editor';
        //end of email code
        
        
		
		if( $USER->archetype == $CFG->userTypeAdmin || $USER->archetype == $CFG->userTypeManager ) {
		//if(in_array($userrole, $CFG->custommanagerroleid)){ // hack by rajesh while login with super admin (1)
		}else{
	
			if ($overviewfilesoptions = course_overviewfiles_options($course)) {
				$mform->addElement('filemanager', 'overviewfiles_filemanager', get_string('courseoverviewfiles'), null, $overviewfilesoptions);
				//$mform->addHelpButton('overviewfiles_filemanager', 'courseoverviewfiles');
				$summaryfields .= ',overviewfiles_filemanager';
			}
		}

        if (!empty($course->id) and !has_capability('moodle/course:changesummary', $coursecontext)) {
            // Remove the description header it does not contain anything any more.
            $mform->removeElement('descriptionhdr');
            $mform->hardFreeze($summaryfields);
        }

		$mform->addElement('hidden', 'shortname', null);
		$mform->setDefault('shortname', 'course_shortname');
        /*$mform->addElement('text', 'shortname', get_string('shortnamecourse'), 'maxlength="100" size="20"');
        $mform->addHelpButton('shortname', 'shortnamecourse');
        $mform->addRule('shortname', get_string('missingshortname'), 'required', null, 'client');
        $mform->setType('shortname', PARAM_TEXT);
        if (!empty($course->id) and !has_capability('moodle/course:changeshortname', $coursecontext)) {
            $mform->hardFreeze('shortname');
            $mform->setConstant('shortname', $course->shortname);
        }*/

        // Verify permissions to change course category or keep current.
		$mform->addElement('header', 'courseformathdr', get_string('type_format', 'plugin'));
        
		
		$mform->addElement('html', '<div style="display:none;">');
        $choices = array();
        $choices['0'] = get_string('hide');
        $choices['1'] = get_string('show');
        $mform->addElement('select', 'visible', get_string('visible'), $choices);
        //$mform->addHelpButton('visible', 'visible');
        $mform->setDefault('visible', $courseconfig->visible);
        if (!empty($course->id)) {
            if (!has_capability('moodle/course:visibility', $coursecontext)) {
                $mform->hardFreeze('visible');
                $mform->setConstant('visible', $course->visible);
            }
        } else {
            if (!guess_if_creator_will_have_course_capability('moodle/course:visibility', $categorycontext)) {
                $mform->hardFreeze('visible');
                $mform->setConstant('visible', $courseconfig->visible);
            }
        }

        $mform->addElement('date_selector', 'startdate', get_string('startdate'));
        //$mform->addHelpButton('startdate', 'startdate');
        $mform->setDefault('startdate', time());

        $mform->addElement('html', '</div>');
       
	    if(!$course->id){
		 $cal_options = array('startyear' => date('Y')); 
		}
		$mform->addElement('date_selector', 'enddate', get_string('expiry_date_course_form'), $cal_options );
        if(!$course->id){
			$mform->setDefault('enddate', strtotime("+1 year"));
		}
		
		$mform->addElement('textarea','keywords', get_string('keywords'),'maxlength="254" size="50"');

		$mform->addElement('text','author', get_string('author'),'maxlength="254" size="50"');
        // Description.
        //$mform->addElement('header', 'descriptionhdr', get_string('description'));
       // $mform->setExpanded('descriptionhdr');

        
        // Course image.
       // $mform->addElement('header', 'courseformathdr', get_string('course_image', 'plugin'));
		//$mform->addElement('file','course_image', get_string('course_image', 'plugin'));

		//$mform->addHelpButton('course_image', 'courseoverviewfiles');
		
		 $mform->addElement('header', 'courseformathdr', get_string('course_image', 'plugin'));

		if ($overviewfilesoptions = course_overviewfiles_options($course)) {
			
            $mform->addElement('filemanager', 'overviewfiles_filemanager', get_string('course_image', 'plugin'), null, $overviewfilesoptions);
            //$mform->addHelpButton('overviewfiles_filemanager', 'courseoverviewfiles');
            $summaryfields .= ',overviewfiles_filemanager';
        }

		$mform->addElement('html','<div class="fitem"><div class="fl italicmsg">'.get_string("course_image_size", 'plugin').'</div></div>');
		if(!empty($course->id)){
			if($course->course_image != ''){
				$view =  $CFG->dirroot.'/theme/gourmet/pix/course/'.$course->id.'/'.$course->course_image;
				$view1 =  $CFG->wwwroot.'/theme/gourmet/pix/course/'.$course->id.'/'.$course->course_image;
				//echo $view;die;
				if(file_exists($view) && $course->course_image !=''){
					$mform->addElement('html','<div class="fitem"><div class="fitemtitle"><label for="id_image"></label></div>');
					$mform->addElement('html','<div class="felement fimage">');
					$mform->addElement('html','<img src="'.$view1.'" id="id_image"/><a class = "removeimage" rel = "'.$course->id.'"><span id="'.$course->id.'" rel="login">Delete</span></a>');
					$mform->addElement('html','</div></div>');
				}
			}
		}
        // Course format.
        //$mform->addElement('header', 'courseformathdr', get_string('type_format', 'plugin'));

        $courseformats = get_sorted_course_formats(true);
        $formcourseformats = array();
        foreach ($courseformats as $courseformat) {
            $formcourseformats[$courseformat] = get_string('pluginname', "format_$courseformat");
        }
        if (isset($course->format)) {
            $course->format = course_get_format($course)->get_format(); // replace with default if not found
            if (!in_array($course->format, $courseformats)) {
                // this format is disabled. Still display it in the dropdown
                $formcourseformats[$course->format] = get_string('withdisablednote', 'moodle',
                        get_string('pluginname', 'format_'.$course->format));
            }
        }
		if(empty($course->id)){ 
		     $courseconfig->format = 'topics';
		}
		$mform->addElement('html','<div style="display:none;">');
        $mform->addElement('select', 'format', get_string('format'), $formcourseformats);
       // $mform->addHelpButton('format', 'format');
        $mform->setDefault('format', $courseconfig->format);

        // Button to update format-specific options on format change (will be hidden by JavaScript).
        $mform->registerNoSubmitButton('updatecourseformat');
        $mform->addElement('submit', 'updatecourseformat', get_string('courseformatudpate'));
		
        // Just a placeholder for the course format options.
        $mform->addElement('hidden', 'addcourseformatoptionshere');
        $mform->setType('addcourseformatoptionshere', PARAM_BOOL);
		$mform->addElement('html','<div');
		if(empty($course->id)){ 
			$mform->setDefault('numsections', 1);
		}
		
		if( $USER->archetype == $CFG->userTypeAdmin || $USER->archetype == $CFG->userTypeManager ) {
        //if(in_array($userrole, $CFG->custommanagerroleid)){ // added by rajesh 
		}else{
			// Appearance.
			$mform->addElement('header', 'appearancehdr', get_string('appearance'));
	
			if (!empty($CFG->allowcoursethemes)) {
				$themeobjects = get_list_of_themes();
				$themes=array();
				$themes[''] = get_string('forceno');
				foreach ($themeobjects as $key=>$theme) {
					if (empty($theme->hidefromselector)) {
						$themes[$key] = get_string('pluginname', 'theme_'.$theme->name);
					}
				}
				$mform->addElement('select', 'theme', get_string('forcetheme'), $themes);
			}
	
			$languages=array();
			$languages[''] = get_string('forceno');
			$languages += get_string_manager()->get_list_of_translations();
			$mform->addElement('select', 'lang', get_string('forcelanguage'), $languages);
			$mform->setDefault('lang', $courseconfig->lang);
	
			// Multi-Calendar Support - see MDL-18375.
			$calendartypes = \core_calendar\type_factory::get_list_of_calendar_types();
			// We do not want to show this option unless there is more than one calendar type to display.
			if (count($calendartypes) > 1) {
				$calendars = array();
				$calendars[''] = get_string('forceno');
				$calendars += $calendartypes;
				$mform->addElement('select', 'calendartype', get_string('forcecalendartype', 'calendar'), $calendars);
			}
	
			$options = range(0, 10);
			$mform->addElement('select', 'newsitems', get_string('newsitemsnumber'), $options);
			//$mform->addHelpButton('newsitems', 'newsitemsnumber');
			$mform->setDefault('newsitems', $courseconfig->newsitems);
	
			$mform->addElement('selectyesno', 'showgrades', get_string('showgrades'));
			//$mform->addHelpButton('showgrades', 'showgrades');
			$mform->setDefault('showgrades', $courseconfig->showgrades);
	
			$mform->addElement('selectyesno', 'showreports', get_string('showreports'));
			//$mform->addHelpButton('showreports', 'showreports');
			$mform->setDefault('showreports', $courseconfig->showreports);
	
			// Files and uploads.
			$mform->addElement('header', 'filehdr', get_string('filesanduploads'));
	
			if (!empty($course->legacyfiles) or !empty($CFG->legacyfilesinnewcourses)) {
				if (empty($course->legacyfiles)) {
					//0 or missing means no legacy files ever used in this course - new course or nobody turned on legacy files yet
					$choices = array('0'=>get_string('no'), '2'=>get_string('yes'));
				} else {
					$choices = array('1'=>get_string('no'), '2'=>get_string('yes'));
				}
				$mform->addElement('select', 'legacyfiles', get_string('courselegacyfiles'), $choices);
				//$mform->addHelpButton('legacyfiles', 'courselegacyfiles');
				if (!isset($courseconfig->legacyfiles)) {
					// in case this was not initialised properly due to switching of $CFG->legacyfilesinnewcourses
					$courseconfig->legacyfiles = 0;
				}
				$mform->setDefault('legacyfiles', $courseconfig->legacyfiles);
			}
	
			// Handle non-existing $course->maxbytes on course creation.
			$coursemaxbytes = !isset($course->maxbytes) ? null : $course->maxbytes;
	
			// Let's prepare the maxbytes popup.
			$choices = get_max_upload_sizes($CFG->maxbytes, 0, 0, $coursemaxbytes);
			$mform->addElement('select', 'maxbytes', get_string('maximumupload'), $choices);
			//$mform->addHelpButton('maxbytes', 'maximumupload');
			$mform->setDefault('maxbytes', $courseconfig->maxbytes);
	
			// Completion tracking.
			if (completion_info::is_enabled_for_site()) {
				$mform->addElement('header', 'completionhdr', get_string('completion', 'completion'));
				$mform->addElement('selectyesno', 'enablecompletion', get_string('enablecompletion', 'completion'));
				$mform->setDefault('enablecompletion', $courseconfig->enablecompletion);
				//$mform->addHelpButton('enablecompletion', 'enablecompletion', 'completion');
			} else {
				$mform->addElement('hidden', 'enablecompletion');
				$mform->setType('enablecompletion', PARAM_INT);
				$mform->setDefault('enablecompletion', 1);
			}
	
			enrol_course_edit_form($mform, $course, $context);
	
			$mform->addElement('header','groups', get_string('groupsettingsheader', 'group'));
	
			$choices = array();
			$choices[NOGROUPS] = get_string('groupsnone', 'group');
			$choices[SEPARATEGROUPS] = get_string('groupsseparate', 'group');
			$choices[VISIBLEGROUPS] = get_string('groupsvisible', 'group');
			$mform->addElement('select', 'groupmode', get_string('groupmode', 'group'), $choices);
			//$mform->addHelpButton('groupmode', 'groupmode', 'group');
			$mform->setDefault('groupmode', $courseconfig->groupmode);
	
			$mform->addElement('selectyesno', 'groupmodeforce', get_string('groupmodeforce', 'group'));
			//$mform->addHelpButton('groupmodeforce', 'groupmodeforce', 'group');
			$mform->setDefault('groupmodeforce', $courseconfig->groupmodeforce);
	
			//default groupings selector
			$options = array();
			$options[0] = get_string('none');
			$mform->addElement('select', 'defaultgroupingid', get_string('defaultgrouping', 'group'), $options);
	
			// Customizable role names in this course.
			$mform->addElement('header','rolerenaming', get_string('rolerenaming'));
			//$mform->addHelpButton('rolerenaming', 'rolerenaming');
	
			if ($roles = get_all_roles()) {
				$roles = role_fix_names($roles, null, ROLENAME_ORIGINAL);
				$assignableroles = get_roles_for_contextlevels(CONTEXT_COURSE);
				foreach ($roles as $role) {
					$mform->addElement('text', 'role_'.$role->id, get_string('yourwordforx', '', $role->localname));
					$mform->setType('role_'.$role->id, PARAM_TEXT);
				}
			}
        }
		if($USER->archetype == $CFG->userTypeAdmin && (isset($course->id) || !empty($course->id))){
			$mform->addElement('header','ownership', get_string('others'));
			$userDepartment = $DB->get_record('department_members',array('userid'=>$course->createdby,'is_active'=>1));
			$userRole = array($CFG->userTypeManager,$CFG->userTypeAdmin);
			$userList = getDepartmentManagerAdmin($userDepartment->departmentid);
			//$changeownership = array(0=>get_string('select_owner'));
			if(!empty($userList)){
				foreach($userList as $user){
					$changeownership[$user->id] = $user->firstname.' '.$user->lastname;
				}
			}
			$mform->addElement('select', 'changeownership', get_string('courseownership'), $changeownership);
			$mform->getElement('changeownership')->setSelected($course->createdby);
		}
		######################### For course setting only  #########################################
		
		
		//$mform->addElement('header', 'courseformathdr', get_string('allocationsettings', 'plugin'));
		//require_once("course_allocation_settings.php");
		
		######################### End for course setting only  ######################################### 
		
		
		$showResetButton = null;
		if(!isset($course->id) || empty($course->id)){
			$showResetButton = false;
		}
	
		
        $this->add_action_buttons_custom(true, get_string('saveandcontinue', 'course'), get_string('saveandexit', 'course'), $showResetButton);
		

        $mform->addElement('hidden', 'id', null);
        $mform->setType('id', PARAM_INT);
		$this->set_data($course);

		$mform->addElement('html',"</div>");
		$mform->addElement('html',"</div>");
        // Finally set the current form data
        
    }

    /**
     * Fill in the current page data for this course.
     */
    function definition_after_data() {
        global $DB;

        $mform = $this->_form;

        // add available groupings
        if ($courseid = $mform->getElementValue('id') and $mform->elementExists('defaultgroupingid')) {
            $options = array();
            if ($groupings = $DB->get_records('groupings', array('courseid'=>$courseid))) {
                foreach ($groupings as $grouping) {
                    $options[$grouping->id] = format_string($grouping->name);
                }
            }
            core_collator::asort($options);
            $gr_el =& $mform->getElement('defaultgroupingid');
            $gr_el->load($options);
        }

        // add course format options
        $formatvalue = $mform->getElementValue('format');
        if (is_array($formatvalue) && !empty($formatvalue)) {
            $courseformat = course_get_format((object)array('format' => $formatvalue[0]));

            $elements = $courseformat->create_edit_form_elements($mform);
            for ($i = 0; $i < count($elements); $i++) {
                $mform->insertElementBefore($mform->removeElement($elements[$i]->getName(), false),
                        'addcourseformatoptionshere');
            }
        }
    }

    /**
     * Validation.
     *
     * @param array $data
     * @param array $files
     * @return array the errors that were found
     */
    function validation($data, $files) {
        global $DB;

        $errors = parent::validation($data, $files);

        $curtime = time();
        
		if(isset($data['id']) && !empty($data['id'])){
		}else{
			if(isset($data['enddate']) && $data['enddate']!='' && $data['enddate'] < $curtime){
				$errors['enddate'] = get_string('expiration_can_not_be_less_than_current_date', 'error');
			}
		}

        // Add field validation check for duplicate shortname.
        if ($course = $DB->get_record('course', array('shortname' => $data['shortname']), '*', IGNORE_MULTIPLE)) {
            if (empty($data['id']) || $course->id != $data['id']) {
                //$errors['shortname'] = get_string('shortnametaken', '', $course->fullname);
            }
        }

        // Add field validation check for duplicate idnumber.
        if (!empty($data['idnumber']) && (empty($data['id']) || $this->course->idnumber != $data['idnumber'])) {
            if ($course = $DB->get_record('course', array('idnumber' => $data['idnumber']), '*', IGNORE_MULTIPLE)) {
                if (empty($data['id']) || $course->id != $data['id']) {
                    $errors['idnumber'] = get_string('courseidnumbertaken', 'error', $course->fullname);
                }
            }
        }

        $errors = array_merge($errors, enrol_course_edit_validation($data, $this->context));

        $courseformat = course_get_format((object)array('format' => $data['format']));
        $formaterrors = $courseformat->edit_form_validation($data, $files, $errors);
        if (!empty($formaterrors) && is_array($formaterrors)) {
            $errors = array_merge($errors, $formaterrors);
        }

		if(!empty($_FILES) && $_FILES["course_image"]["error"] == 0){
				$imgname = $_FILES["course_image"]["tmp_name"];
				$size = getimagesize($imgname);
				if($size[0]>80 || $size[1]>80){
					$errors['course_image'] = get_string('filedimlogin','plugin');
				}
				$filecheck = basename($_FILES["course_image"]['name']);
				$ext = substr($filecheck, strrpos($filecheck, '.') + 1);
				if ($_FILES["course_image"]['name'] != '' && ($ext == "jpg" || $ext == "gif" || $ext == "png") && ($_FILES["course_image"]["type"] == "image/jpeg" || $_FILES["course_image"]["type"] == "image/gif" || $_FILES["course_image"]["type"] == "image/png")){
				}
				else{
					$errors["course_image"] = get_string('supportedfiletype','plugin');
				}
		}
        return $errors;
    }
}

