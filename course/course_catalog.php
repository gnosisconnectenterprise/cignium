<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Lists the course categories
 *
 * @copyright 1999 Martin Dougiamas  http://dougiamas.com
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package course
 */

require_once("../config.php");
require_once($CFG->dirroot. '/course/lib.php');
require_once($CFG->libdir. '/coursecatlib.php');

require_login(); 

$PAGE->navbar->add(get_string('coursecatalog', 'course'));

$header = $SITE->fullname.": ".get_string('coursecatalog', 'course');
$PAGE->set_title($header);

echo $OUTPUT->header(); 
//echo $OUTPUT->skip_link_target();

///// Open - Added by Madhab to show the Common Search Area /////
$paramArray = array(
				'ch' => optional_param('ch', '', PARAM_ALPHA),
				'key' => optional_param('key', '', PARAM_RAW),
				'nm' => optional_param('nm', '', PARAM_ALPHANUM),
				'desc' => optional_param('desc', '', PARAM_ALPHANUM),
				'cnm' => optional_param('cnm', '', PARAM_ALPHANUM),
			  );
$filterArray = array(	
					'nm'=>get_string('name','course'),
					'desc'=>get_string('description','course'),
					'cnm'=>get_string('category','course')
			   );
$pageURL = '/course/course_catalog.php';

////// Send Course Assignment Email to the Learner Creator, if User click "Assign Course" link //////
$chkMailFlag = optional_param('mailflag', '0', PARAM_INT);
$chkEnrolFlag = optional_param('enrolflag', '0', PARAM_INT);
$fromUrl = optional_param('from', '0', PARAM_RAW);
if($chkMailFlag == 1) {

	$toUser = new stdClass();
	$fromUser = new stdClass();

	$genRedirectURL = $CFG->wwwroot."/";
	$courseId = optional_param('cid', '0', PARAM_INT);
	$programId = optional_param('pid', '0', PARAM_INT);
	
	if($programId){
	
			 ////// Getting Program Detail /////
			$programDetails = getPrograms('', " AND id IN ($programId)");
		
			if(count($programDetails) > 0){
				////// Define those key, which we need to remove first time //////
				$removeKeyArray = array('pid','mailflag');
				////// Getting common URL for the Search //////
				$genRedirectURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
		
				////// Getting From User /////
				$fromUserQuery = "SELECT id, firstname, lastname, email, createdby FROM {$CFG->prefix}user WHERE id = '".$USER->id."'";
				$fromUserDetails = $DB->get_record_sql($fromUserQuery);
				
				$userCreatorId = $fromUserDetails->createdby;
				$fromUser->email = $fromUserDetails->email;
				$fromUser->firstname = $fromUserDetails->firstname;
				$fromUser->lastname = $fromUserDetails->lastname;
				$fromUser->maildisplay = true;
				$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
				$fromUser->id = $USER->id;
				$fromUser->firstnamephonetic = '';
				$fromUser->lastnamephonetic = '';
				$fromUser->middlename = '';
				$fromUser->alternatename = '';
		
				////// Getting To User /////
				$toUserQuery = "SELECT id, firstname, lastname, email, createdby FROM {$CFG->prefix}user WHERE id = '".$userCreatorId."'";
				$toUserDetails = $DB->get_record_sql($toUserQuery);
				
				$toUser->email = $toUserDetails->email;
				$toUser->firstname = $toUserDetails->firstname;
				$toUser->lastname = $toUserDetails->lastname;
				$toUser->maildisplay = true;
				$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
				$toUser->id = $userCreatorId;
				$toUser->firstnamephonetic = '';
				$toUser->lastnamephonetic = '';
				$toUser->middlename = '';
				$toUser->alternatename = '';
		
				$subject = 'Request for Assign Program ('.(($programDetails->id != '') ? $programDetails->id.' - ' : '').$programDetails->name.') to '.$fromUserDetails->firstname.' '.$fromUserDetails->lastname;
		
				$messageText = 'Dear '.$toUserDetails->firstname.','."\n\n".'Please assign the program ('.(($programDetails->id != '') ? $programDetails->id.' - ' : '').$programDetails->name.') to '.$fromUserDetails->firstname.' '.$fromUserDetails->lastname."\n\n".'From,'."\n".$SITE->fullname;
		
				email_to_user($toUser, $fromUser, $subject, $messageText, $messageHtml, "", "", true);
		
				$_SESSION['update_msg'] = get_string('emailsentsuccess', 'course');
			} else {
				////// Define those key, which we need to remove first time //////
				$removeKeyArray = array('pid','mailflag');
				////// Getting common URL for the Search //////
				$genRedirectURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
		
				$_SESSION['update_msg'] = get_string('emailsenterror', 'course');
			}
				
	}elseif($courseId){

			 ////// Getting Course Detail /////
			$courseQuery = "SELECT id, fullname, idnumber FROM {$CFG->prefix}course WHERE id = '".$courseId."'";
			$courseDetails = $DB->get_record_sql($courseQuery);
		
			if(!empty($courseDetails)){
				////// Define those key, which we need to remove first time //////
				$removeKeyArray = array('cid','mailflag');
				////// Getting common URL for the Search //////
				$genRedirectURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
		
				////// Getting From User /////
				$fromUserQuery = "SELECT id, firstname, lastname, email, createdby FROM {$CFG->prefix}user WHERE id = '".$USER->id."'";
				$fromUserDetails = $DB->get_record_sql($fromUserQuery);
				
				$userCreatorId = $fromUserDetails->createdby;
				$fromUser->email = $fromUserDetails->email;
				$fromUser->firstname = $fromUserDetails->firstname;
				$fromUser->lastname = $fromUserDetails->lastname;
				$fromUser->maildisplay = true;
				$fromUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
				$fromUser->id = $USER->id;
				$fromUser->firstnamephonetic = '';
				$fromUser->lastnamephonetic = '';
				$fromUser->middlename = '';
				$fromUser->alternatename = '';
		
				////// Getting To User /////
				$toUserQuery = "SELECT id, firstname, lastname, email, createdby FROM {$CFG->prefix}user WHERE id = '".$userCreatorId."'";
				$toUserDetails = $DB->get_record_sql($toUserQuery);
				
				$toUser->email = $toUserDetails->email;
				$toUser->firstname = $toUserDetails->firstname;
				$toUser->lastname = $toUserDetails->lastname;
				$toUser->maildisplay = true;
				$toUser->mailformat = 0; // 0 (zero) text-only emails, 1 (one) for HTML/Text emails.
				$toUser->id = $userCreatorId;
				$toUser->firstnamephonetic = '';
				$toUser->lastnamephonetic = '';
				$toUser->middlename = '';
				$toUser->alternatename = '';
		
				$subject = 'Request for Assign Course ('.(($courseDetails->idnumber != '') ? $courseDetails->idnumber.' - ' : '').$courseDetails->fullname.') to '.$fromUserDetails->firstname.' '.$fromUserDetails->lastname;
		
				$messageText = 'Dear '.$toUserDetails->firstname.','."\n\n".'Please assign the course ('.(($courseDetails->idnumber != '') ? $courseDetails->idnumber.' - ' : '').$courseDetails->fullname.') to '.$fromUserDetails->firstname.' '.$fromUserDetails->lastname."\n\n".'From,'."\n".$SITE->fullname;
		
				email_to_user($toUser, $fromUser, $subject, $messageText, $messageHtml, "", "", true);
		
				$_SESSION['update_msg'] = get_string('emailsentsuccess', 'course');
			} else {
				////// Define those key, which we need to remove first time //////
				$removeKeyArray = array('cid','mailflag');
				////// Getting common URL for the Search //////
				$genRedirectURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
		
				$_SESSION['update_msg'] = get_string('emailsenterror', 'course');
			}
	}
	
	if($fromUrl){
	  redirect( $fromUrl );
	}else{
	  redirect( $genRedirectURL );
	}
}



if($chkEnrolFlag == 1) {

	$genRedirectURL = $CFG->wwwroot."/";
	$courseId = optional_param('cid', '0', PARAM_INT);
	$programId = optional_param('pid', '0', PARAM_INT);
	
	if($programId){
	
			$userAbleToEnrollmentStatus = isUserAbleToEnrollment($programId, $CFG->programType);
			$programName = getPrograms($programId);
			$a->programname = $programName;
		
			 ////// Define those key, which we need to remove first time //////
			$removeKeyArray = array('pid','enrolflag');
			////// Getting common URL for the Search //////
			$genRedirectURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
				
			if($userAbleToEnrollmentStatus == 1){
				
				assignProgramToUser($programId, $USER->id);
				$_SESSION['update_msg'] = get_string('programhasbeenenroled', 'course', $a->programname);
				
			}else{

				  if($userAbleToEnrollmentStatus == 0){
				   $_SESSION['error_msg'] = get_string('notabletoenrolthisprogram', 'course', $a->programname);
				  }elseif($userAbleToEnrollmentStatus == 2){
					$_SESSION['error_msg'] = get_string('programisalreadyenroled', 'course', $a->programname);
				  }	
		 	}
				
	}elseif($courseId){

			$userAbleToEnrollmentStatus = isUserAbleToEnrollment($courseId, $CFG->courseType);
			$courseName = getCourses($courseId);
			$a->coursename = $courseName;
		
			 ////// Define those key, which we need to remove first time //////
			$removeKeyArray = array('cid','enrolflag');
			////// Getting common URL for the Search //////
			$genRedirectURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
				
			if($userAbleToEnrollmentStatus == 1){
				
				assignCourseToUser($USER->id, array($courseId));
				$_SESSION['update_msg'] = get_string('coursehasbeenenroled', 'course', $a->coursename);
				
			}else{

				  if($userAbleToEnrollmentStatus == 0){
				   $_SESSION['error_msg'] = get_string('notabletoenrolthiscourse', 'course',$a->coursename);
				  }elseif($userAbleToEnrollmentStatus == 2){
					$_SESSION['error_msg'] = get_string('courseisalreadyenroled', 'course', $a->coursename);
				  }	
		 	}
	}
	
	if($fromUrl){
	  redirect( $fromUrl );
	}else{
	  redirect( $genRedirectURL );
	}
}

////// Show Search Form on the page //////
echo genCommonSearchForm($paramArray, $filterArray, $pageURL);
///// Closed - Added by Madhab to show the Common Search Area /////

////// Show Message after sending email //////
if($_SESSION['update_msg'] != ''){
	echo '<div class="clear"></div>';
	echo '<div class="update_msg">'.$_SESSION['update_msg'].'</div>';
	echo '<div class="clear"></div>';
	$_SESSION['update_msg']='';
}

if($_SESSION['error_msg'] != ''){
	echo '<div class="clear"></div>';
	echo '<div class="error_msg">'.$_SESSION['error_msg'].'</div>';
	echo '<div class="clear"></div>';
	$_SESSION['error_msg']='';
}

///// Open - Added by Madhab to show Listing of Course Catalog /////
echo genCourseCatalogListing($paramArray, $pageURL);
///// Open - Added by Madhab to show Listing of Course Catalog ///
?>
<style>
.c-box{ display:none;}
</style>

<script language="javascript" type="application/javascript">
 
 setInterval(function(){$('.update_msg, .error_msg').slideUp();},10000);

 $(document).ready(function(){
 
      $('.enrollment-program').click(function(){

		 var dataMsg = $(this).attr('data-msg');
		 var enrolUrl = $(this).attr('data-url');
		
	     if(enrolUrl){
		  if(confirm(dataMsg)){
		     window.location.href = enrolUrl;
		  }
		 }
		
	  });
	  
	  $('.enrollment-course').click(function(){
	  
	    var dataMsg = $(this).attr('data-msg');
		var enrolUrl = $(this).attr('data-url');
	    
		if(enrolUrl){  
		  if(confirm(dataMsg)){
		     window.location.href = enrolUrl;
		  }
		}
	  
	  });

      //$('.c-box').hide(); 
      $('.program-box').click(function(){
	  
	         var catalogId = $(this).attr('rel');
			 //$('.catalog-course').removeClass('selected');
			 //$('.program-box').not('[rel="'+catalogId+'"]').removeClass('minus');
			 var hasClass = $('#plus-minus-icon-'+catalogId).hasClass('minus');
			 if(hasClass){
			   $('#plus-minus-icon-'+catalogId).removeClass('minus');
			   $('#catalog-course-'+catalogId).removeClass('selected');
			   $('#plus-minus-icon-'+catalogId+' a').attr('title','<?php echo get_string('showcourse', 'course')?>');
			 }else{
			   $('#plus-minus-icon-'+catalogId).addClass('minus');
			   $('#catalog-course-'+catalogId).addClass('selected');
			   $('#plus-minus-icon-'+catalogId+' a').attr('title','<?php echo get_string('hidecourse', 'course')?>');
			 }
			 
			 //$('.c-box').not('#cbox'+catalogId).hide();
			 
			 if($('#cbox'+catalogId).is(':visible')){
			    $('#cbox'+catalogId).hide();
			 }else{
			   $('#cbox'+catalogId).show();
			 }
			 
			 
	  
	  });
  });
  
  
 
</script>
<?php 
echo $OUTPUT->footer();
?>

