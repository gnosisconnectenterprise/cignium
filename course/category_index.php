<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Lists the course categories
 *
 * @copyright 1999 Martin Dougiamas  http://dougiamas.com
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package course
 */

require_once("../config.php");
require_once($CFG->dirroot. '/course/lib.php');
require_once($CFG->libdir. '/coursecatlib.php');

$site = get_site();

//$PAGE->navbar->add(get_string('courses', 'course'), $CFG->wwwroot.'/course/index.php');
$PAGE->navbar->add(get_string('managecategory', 'course'));

if($USER->archetype == $CFG->userTypeStudent)
{
	redirect($CFG->wwwroot."/");
}

$PAGE->set_url('/course/category_index.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('coursecategory');
$courserenderer = $PAGE->get_renderer('core', 'course');

require_login(); // added by rajesh 

$PAGE->set_heading($site->fullname);

$content = $courserenderer->course_category($categoryid);
$pageURL = '/course/category_index.php';
checkPageIsNumeric($pageURL,$_REQUEST['page']);

echo $OUTPUT->header(); 
echo $OUTPUT->skip_link_target();

///// Open - Added by Madhab to show the Common Search Area /////
$paramArray = array(
				'ch' => optional_param('ch', '', PARAM_ALPHA),
				'key' => optional_param('key', '', PARAM_RAW),
				'nm' => optional_param('nm', '', PARAM_ALPHANUM),
				'desc' => optional_param('desc', '', PARAM_ALPHANUM),
			  );
$filterArray = array(	
					'nm'=>get_string('name','course'),
					'desc'=>get_string('description','course'),
			   );


echo genCommonSearchForm($paramArray, $filterArray, $pageURL);
echo '<div class="add-course-button"><a class="button-link add-category" href="'.$CFG->wwwroot.'/course/editcategory.php"><i></i>'.get_string('addcategory','course').'</a></div>';
///// Closed - Added by Madhab to show the Common Search Area /////

//// Record update Message
if($_SESSION['update_msg']!=''){
	echo '<div class="clear"></div>';
	echo '<div class="bg-success text-success text-center" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
	echo '<div class="clear"></div>';
	$_SESSION['update_msg']='';
}
/// Record update message ends

///// Open - Added by Madhab to show new Listing of Category /////
//echo $content;
echo genModifiedCategoryListing($paramArray, $pageURL);

////// Define those key, which we need to remove first time //////
$removeKeyArray = array('id','flag','perpage');
////// Getting common URL for the Search //////
$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);

$page = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage',$CFG->perpage, PARAM_INT);
echo paging_bar(countCategoryListingResult($paramArray), $page, $perpage, $genURL);
///// Open - Added by Madhab to show new Listing of Category ///

echo $OUTPUT->footer();
