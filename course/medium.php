<?php
require_once('../config.php');

if($_POST){

    $allow_student_access = isset($_POST['allow_student_access'])?$_POST['allow_student_access']:0; 
    $classId = isset($_POST['classId'])?$_POST['classId']:0; 
	$resourceType = isset($_POST['resource_type'])?$_POST['resource_type']:0; 
	
	$_SESSION['asset_name'] = $_POST['asset_name'];
	$_SESSION['asset_description'] = $_POST['asset_description']; 
	$_SESSION['allow_student_access'] = $allow_student_access;
	$_SESSION['resource_type'] = $resourceType;
	
	
	if($_POST['action_type'] == 'add'){
		redirect(new moodle_url($CFG->wwwroot.'/course/modedit.php', array('add'=>$_POST['asset_type'],'type'=>'','course'=>$_POST['course_id'],'section'=>1,'return'=>0,'sr'=>0,'resource_type'=>$resourceType,'allow_student_access'=>$allow_student_access,'classId'=>$classId)));
	}elseif($_POST['action_type'] == 'update'){
	
	    if(isset($_POST['quickedit']) && $_POST['quickedit'] == 1){
		    global $DB, $CFG;
		   
		    $module = isset($_POST['asset_type']) && $_POST['asset_type'] == 'resource'?17:18;
			$courseId = $_POST['course_id'];
			$moduleId = $_POST['moduleid'];
			$query = "SELECT instance FROM {$CFG->prefix}course_modules WHERE id = '".$moduleId."' AND course = '".$courseId."' AND module =  '".$module."'  ";
			$instance = $DB->get_field_sql($query);
			
			if($instance && $module){	
			
				$asset->id  = $instance;
				$asset->name = $_POST['asset_name'];
				$asset->intro = $_POST['asset_description'];
				$asset->allow_student_access = $allow_student_access; 
				$asset->resource_type = $resourceType; 
	
				$DB->update_record($_POST['asset_type'], $asset);
				
				if($classId){ // for reference material of class for any classroom
					$_SESSION['update_msg'] = get_string('referencematerialupdated');
					$_SESSION['error_class'] = 'success';
					redirect(new moodle_url($CFG->wwwroot.'/mod/scheduler/materialsview.php', array('id'=>$courseId,'classId'=>$classId)));
				}else{
                   	$_SESSION['update_msg'] = get_string('assetquickupdate','course');
					$_SESSION['error_class'] = 'success';
					redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php', array('id'=>$courseId)));
				}
			
		    }
		}
		
		redirect(new moodle_url($CFG->wwwroot.'/course/modedit.php', array('update'=>$_POST['moduleid'])));
	}
}else{
	redirect(new moodle_url($CFG->wwwroot));
}
?>
