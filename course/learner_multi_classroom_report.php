<?php

	/**
		* Custom module - Multi Course Report Page
		* Date Creation - 01/07/2014
		* Date Modification : 01/07/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
	

	require_once("../config.php");

	global $DB,$CFG,$USER;
	
		
	//require_login(); 
	checkLogin();
	
	
	require_once ($CFG->dirroot . '/mod/scheduler/lib.php');
	
	
	$userid = optional_param('userid', $USER->id, PARAM_INT);
	$back = optional_param('back', 1, PARAM_INT);
	$cType = optional_param('ctype', 2, PARAM_INT);
	
	$PAGE->set_pagelayout('globaladmin');
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('courseusagesreport','classroomreport'));
	//$PAGE->navbar->add(get_string('reports','classroomreport'), new moodle_url($CFG->wwwroot.'/my/adminreportdashboard.php'));
	
	
	if($back == 2){
	 $PAGE->navbar->add(get_string('reports','multiuserreport'));
	 $PAGE->navbar->add(get_string('usereperformancereport','multiuserreport'), new moodle_url($CFG->wwwroot.'/user/multiuser_report.php'));
	 //$PAGE->navbar->add(get_string('viewreportdetails','multiuserreport'));
	 $PAGE->navbar->add(getUsers($userid));	
	}elseif($back == 1){
      $PAGE->navbar->add(get_string('manage_user'),'/admin/user.php');
	  //$PAGE->navbar->add(get_string('viewuserreports','singlereport'));
	  $PAGE->navbar->add(getUsers($userid));
	}
												
	

	echo $OUTPUT->header(); 
	$completedClass = 0;
	$incompleteClass = 0;
	$classes_data = getUserClasses($userid);	
	foreach($classes_data as $class_data){
		if($class_data->is_completed){
			$completedClass++;
		}
		else{
			$incompleteClass++;
		}
	}
	$totalClasses = $completedClass+$incompleteClass;
	$completedClassPers = round(($completedClass*100)/$totalClasses);
	$incompleteClassPers = round(($incompleteClass*100)/$totalClasses);
	$courseHTML = '';
	$genPrintURL = $CFG->wwwroot.'/course/learner_classroom_report_print.php?userid='.$userid;
	$courseHTML .= '<div class="clear"></div>';
	if($USER->archetype != 'learner'){
	    $cSelected1 = $cType == 1?"selected='selected'":"";
		$cSelected2 = $cType == 2?"selected='selected'":"";
		$courseHTML .= '<select name="" backto="'.$back.'" id="reporttype" rel="'.$userid.'"><option value="'.$CFG->courseTypeOnline.'" '.$cSelected1.'>'.get_string('onlinecourse').'</option><option value="'.$CFG->courseTypeClassroom.'" '.$cSelected2.' >'.get_string('classroomcourse').'</option></select>';
	
	}
	$courseHTML .= "<div class='tabsOuter' style='border-top:1px solid #ddd'>";
	$courseHTML .= '<div class="userprofile">';	
	
	if($totalClasses>0){
		$exportHTML .= '<div class="exports_opt_box margin_top"> ';
		$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','singlereport').'" href="'.$genPrintURL.'&action=exportcsv">'.get_string('downloadcsv','singlereport').'</a> ';
		$exportHTML .= '<span class="seperater">&nbsp;</span>';
		$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','singlereport').'" id="exportpdf1" href="'.$genPrintURL.'&action=exportpdf">'.get_string('downloadpdf','singlereport').'</a> ';
		$exportHTML .= '<span class="seperater">&nbsp;</span>';
		$exportHTML .= '<a rel="'.$genPrintURL.'&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','singlereport').'" class="print_icon">'.get_string('print','singlereport').'</a>';
		$exportHTML .= '</div>';
		
		  $courseHTML .= ' <div class = "single-report-start">
						<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('overallclassroomreport','scheduler').$exportHTML.'</span>';
					$courseHTML .= '<div class="single-report-graph-left" id="chartContainer">'.get_string('loading','classroomreport').'</div>
						  <div class="single-report-graph-right" style="margin-bottom: 25px;">
							<div class="course-count-heading">'.get_string('numberofclasses','scheduler').'</div>
							<div class="course-count">'.$totalClasses.'</div>
							<div class="seperator">&nbsp;</div>
							
							<div class="seperator">&nbsp;</div>
							<div class="course-status">
							  <div class="inprogress"><h6>'.get_string('inprogress','classroomreport').'</h6><span>'.$incompleteClass.'</span></div>
							  <div class="clear"></div>
							  <div class="completed"><h6>'.get_string('completed','classroomreport').'</h6><span>'.$completedClass.'</span></div>
							</div>
						  </div>
						</div>
					  </div>
					  <div class="clear"></div>';
	}
	$courseHTML .= '<div class="clear"></div>';
	$courseHTML .= '<div class="course-listing">'; 
	$courseHTML .= '<table class = "table1" id="table1"><tr class=""><th  width="20%">Title</th>';
	
	
	
	$courseHTML .= "<th width = '20%' align='align_left' >" . get_string ( 'attended', 'scheduler' ) . "</th>";
	$courseHTML .= "<th width = '19%' align='align_left' >" . get_string ( 'grade', 'scheduler' ) . "</th>";
	$courseHTML .= "<th width = '11%' align='align_left' >" . get_string ( 'score', 'scheduler' ) . "</th>";
	$courseHTML .= "<th width = '10%' align='align_left' >" . get_string ( 'classcompletedstatus', 'scheduler' ) . "</th>";	
	$courseHTML .= '</tr>';	
	if (! $classes_data) {
		$courseHTML .= '<tr><td colspan="5">'.get_string('norecordfound','scheduler').'</td></tr>';
	}
	else{
		foreach ( $classes_data as $class_rec ) {				

			$courseHTML .= "<tr class='oddR'><td class='align_left'  width = '14%'><span class='f-left'>" . $class_rec->coursename." - ". $class_rec->classname . "</span></td>";
			$courseHTML .= "<td></td>";
			$courseHTML .= "<td></td>";
			$courseHTML .= "<td></td>";
			$courseHTML .= '<td>'.($class_rec->is_completed == 1?get_string( 'classcompleted', 'scheduler' ):get_string('classincompleted', 'scheduler')).'</td>';
			$courseHTML .= "</tr>";			
	
			$session_list = classSessionList ($class_rec->id,'classid');
			$courseHTML .= "<tr><td colspan='5'><div class='a-box'>";
			$i=0;
			if($session_list){
			foreach ( $session_list as $session ) {
				if($i%2!=0){
					$class='even';
				}
				else{
					$class='odd';
				}
				$i++;
				$session_rec = $DB->get_record ( 'scheduler_appointment', array (
						'slotid' => $session->id,
						'studentid' => $userid
				) );			
				$courseHTML .= "<div class='".$class."' style='float:left;width:100%;'><div>" . $session->sessionname . "</div>";
				$courseHTML .= "<div>".($session_rec->attended == 1?'Yes':'No')."</div>";
				$courseHTML .= "<div>".($session_rec->grade!=''?$session_rec->grade:getMDash())."</div>";
				$courseHTML .= "<div>".($session_rec->score!=''?$session_rec->score:getMDash())."</div>";
				$courseHTML .= "<div></div></div>";			
				
			}
			}
			else{
				$courseHTML .= get_string('norecordfound','scheduler');
			}
			$courseHTML .= "</div></td></tr>";			
		
		}
	}
			//	$courseHTML .= "<tr><td colspan='4'><input type='submit' name='submit' value='Save & Continue' /></td></tr>";
		
	
		$courseHTML .= "</table>";	
	$courseHTML .="</div>";
	
	$styleSheet = 'style="margin-top:10px"';  
	$courseHTML .= '<input type = "button" value = "'.get_string('back','multiuserreport').'" '.$styleSheet.' onclick="location.href=\''.$CFG->wwwroot.'/user/multiuser_report.php\';">';
	$courseHTML .="</div>";

	// teacher side
	
	if (! empty ( $courseHTML )) {
		
	
		echo html_writer::start_tag ( 'div', array () );		
			$courseHTML .=  '<script>	$(document).ready(function(){
											$("#reporttype").change(function(){
												var userid = $(this).attr("rel");
												var backto = $(this).attr("backto");
												if(backto == 2){
												  url = "'.$CFG->wwwroot.'/user/multiuser_report_details.php?uid="+userid+"&course=1";			
												}else if(backto == 1){
												  url = "'.$CFG->wwwroot.'/user/single_report.php?uid="+userid+"&course=1";		
												}
												window.location.href = url;
											});
													
					  						$("#printbun").bind("click", function(e) {
											var url = $(this).attr("rel");
											window.open(url, "'.get_string('viewuserreports','singlereport').'", "width=400, height=400, top=100, left=100, scrollbars=1");
											});	
										});';
		 	$courseHTML .=  ' window.onload = function () {
				
											var chart = new CanvasJS.Chart("chartContainer",
											{
												title:{
													text: ""
												},
												theme: "theme4",
												data: [
												{
													type: "doughnut",
													indexLabelFontFamily: "Arial",
													indexLabelFontSize: 12,
													startAngle:0,
													indexLabelFontColor: "dimgrey",
													indexLabelLineColor: "darkgrey",
													toolTipContent: "{y}%",
						
													dataPoints: [
													{  y: '.$incompleteClassPers.', label: "'.get_string('inprogress','classroomreport').' ('.$incompleteClass.')", exploded: "" },
													{  y: '.$completedClassPers.', label: "'.get_string('completed','classroomreport').' ('.$completedClass.')", exploded: "" },
													
						
													]
			
												
			
			
												}
												]
											}); ';
				
			
				$courseHTML .=  '	chart.render(); ';
		
			
		$courseHTML .=  '  }'; 
		$courseHTML .=  '</script><script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js"></script>';
		
		$courseHTML .= html_writer::end_tag ( 'div' );
		$courseHTML .= html_writer::end_tag ( 'div' );
	    echo $courseHTML;
		// Pring paging bar

	}


	//echo $courseHTML;
	echo $OUTPUT->footer();

?>