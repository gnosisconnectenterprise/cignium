<?php

	/**
		* Custom module - Multi Course Report Page
		* Date Creation - 01/07/2014
		* Date Modification : 01/07/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
	

	require_once("../config.php");

	global $DB,$CFG,$USER;
	require_once ($CFG->dirroot . '/mod/scheduler/lib.php');
		
	//if( $USER->archetype == $CFG->userTypeStudent) {
	 redirect(new moodle_url('/'));
	//}
	
	//require_login(); 
	checkLogin();
	
	
	$PAGE->set_pagelayout('print');
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('classrequestreport','classroomreport'));
	//$PAGE->navbar->add(get_string('reports','classroomreport'), new moodle_url($CFG->wwwroot.'/my/adminreportdashboard.php'));
	$PAGE->navbar->add(get_string('reports','classroomreport'));
	$PAGE->navbar->add(get_string('courseusagesreport','classroomreport'));

	$sort    = optional_param('sort', 'COALESCE(NULLIF(firstname, ""), mu.lastname), mu.lastname', PARAM_RAW);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 0, PARAM_INT);        // how many per page
	$export = optional_param('action', '', PARAM_ALPHANUM);
	$pageURL = '/course/multi_classroom_report.php';
	checkPageIsNumeric($pageURL,$_REQUEST['page']);
	
				
	$sDate = '';
	$eDate = '';
		
	/*$sDate =  $CFG->reportSearchStartDate;
	$eDate = $CFG->reportSearchEndDate*/
	$sclasses    = optional_param('classes', '1', PARAM_RAW);   
	$sCourse        = optional_param('course', '1', PARAM_RAW);       
	$sType          = optional_param('type', '-1', PARAM_RAW); 
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate       = optional_param('endDate', $eDate, PARAM_RAW);
	$sStatus       = optional_param('status', '-1', PARAM_RAW);
	$paramArray = array(
					'classes' => $sclasses,
					'course' => $sCourse,
					'type' => $sType,
					'startDate' => $sStartDate,
					'endDate' => $sEndDate,
					'page'=>$page,
					'perpage'=>$perpage,
					'status'=>$sStatus
				  );
	
	
	$removeKeyArray = array();

	

	if($export && in_array($export, array('exportcsv', 'exportpdf', 'print'))){	
		$courseUsagesReport = getDeclinedUsersReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);		
		$courseHTML			= $courseUsagesReport->courseHTML;
		$reportContentCSV	= $courseUsagesReport->reportContentCSV;
		$reportContentPDF	= $courseUsagesReport->reportContentPDF;
	
	}else{
		redirect(new moodle_url('/'));
	}
	
	 if(isset($export) && $export == 'exportcsv') {
	
		$filepath = $CFG->dirroot."/local/reportexport/temp";		
		chmod($filepath, 0777);		
		$filename = str_replace(' ', '_', get_string('classrequestreport','classroomreport'))."_".date("m-d-Y").".csv";  
		$filepathname = $filepath.'/'.$filename;
		unlink($filepathname);
		$handler = fopen($filepathname, "w");
		fwrite($handler, $reportContentCSV);
		
   	    exportCSV($filepathname);
   	   
	}
	/* eof export to csv */	
	
	
	/* bof export to pdf */
    if(isset($export) && $export == 'exportpdf') {
			
		$filename = str_replace(' ', '_', get_string('classrequestreport','classroomreport'))."_".date("m-d-Y").".pdf";
		exportPDF($filename, $reportContentPDF, '', get_string('classrequestreport','classroomreport'));
		
	}
	/* eof export to pdf */	
		
	   
	echo $OUTPUT->header(); 
	echo $courseHTML;
	?>
<style>
#page { margin: 20px auto 0;}
#page #region-main .region-main-inner{ width:80% !IMPORTANT;}
.single-report-start {  background-color:#FFF;}
</style>
