<?php

	/**
		* Custom module - Multi Course Report Page
		* Date Creation - 01/07/2014
		* Date Modification : 01/07/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
	

	require_once("../config.php");

	global $DB,$CFG,$USER;
	
		
	//require_login(); 
	checkLogin();
	
	
	require_once ($CFG->dirroot . '/mod/scheduler/lib.php');
	$PAGE->set_pagelayout('print');
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('courseusagesreport','classroomreport'));
	//$PAGE->navbar->add(get_string('reports','classroomreport'), new moodle_url($CFG->wwwroot.'/my/adminreportdashboard.php'));
	$PAGE->navbar->add(get_string('reports','classroomreport'));
	$PAGE->navbar->add(get_string('courseusagesreport','classroomreport'));


	
	$userid = optional_param('userid', $USER->id, PARAM_INT);

	$export = optional_param('action', '', PARAM_ALPHANUM);
	/*$sDate =  $CFG->reportSearchStartDate;
	$eDate = $CFG->reportSearchEndDate*/

	

	
	$removeKeyArray = array();

  
	
	//$courseUsagesReport = getClassroomUsagesReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
	//$courseHTML = $courseUsagesReport->courseHTML;
	$classes_data = getUserClasses($userid);
	
	$courseHTML = '';
	$reportContentPDF = '';
	$completedClass = 0;
	$incompleteClass = 0;
	$classes_data = getUserClasses($userid);
	foreach($classes_data as $class_data){
		if($class_data->is_completed){
			$completedClass++;
		}
		else{
			$incompleteClass++;
		}
	}
	$totalClasses = $completedClass+$incompleteClass;
	$completedClassPers = round(($completedClass*100)/$totalClasses);
	$incompleteClassPers = round(($incompleteClass*100)/$totalClasses);
	$courseHTML = '';
	$genPrintURL = $CFG->wwwroot.'/course/learner_classroom_report_print.php?userid='.$userid;
	$courseHTML .= '<div class="clear"></div>';
	$courseHTML .= "<div class='tabsOuter' style='border-top:1px solid #ddd'>";
	$courseHTML .= '<div class="userprofile" style=" padding-bottom:10px">';	
	
	if(!empty($export)){

		$courseHTML .= '<table border="0" cellpadding="0" cellspacing="0" width="100%">';
			$courseHTML .= '<tr>';
				$courseHTML .= '<td><strong>'.get_string('user','singlereport').':</strong> '.getUsers($userid).'</td>';
			$courseHTML .= '</tr>';

		 $courseHTML .= '</table>';
		 
		
	 
	 //$courseHTML .= '<span style="font-size:16pt;">&nbsp;<br /><strong>'.get_string('coursevsmultipleuserreport','classroomreport').'</strong></span><br /><br />';
	}
			
	if($totalClasses>0){
	
	    
		$courseHTML .= ' <div class = "single-report-start">
						<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('overallclassroomreport','scheduler').'</span>';
		$courseHTML .= '<div class="single-report-graph-left" id="chartContainer">'.get_string('loading','classroomreport').'</div>
						  <div class="single-report-graph-right" style="margin-bottom: 25px;">
							<div class="course-count-heading">'.get_string('numberofclasses','scheduler').'</div>
							<div class="course-count">'.$totalClasses.'</div>
							<div class="seperator">&nbsp;</div>
				
							<div class="seperator">&nbsp;</div>
							<div class="course-status">
							  <div class="inprogress"><h6>'.get_string('inprogress','classroomreport').'</h6><span>'.$incompleteClass.'</span></div>
							  <div class="clear"></div>
							  <div class="completed"><h6>'.get_string('completed','classroomreport').'</h6><span>'.$completedClass.'</span></div>
							</div>
						  </div>
						</div>
					  </div>
					  <div class="clear"></div>';
	}
	$courseHTML .=  '<script> window.onload = function () {
	
											var chart = new CanvasJS.Chart("chartContainer",
											{
												title:{
													text: ""
												},
												theme: "theme4",
												data: [
												{
													type: "doughnut",
													indexLabelFontFamily: "Arial",
													indexLabelFontSize: 12,
													startAngle:0,
													indexLabelFontColor: "dimgrey",
													indexLabelLineColor: "darkgrey",
													toolTipContent: "{y}%",
	
													dataPoints: [
													{  y: '.$incompleteClassPers.', label: "'.get_string('inprogress','classroomreport').' ('.$incompleteClass.')", exploded: "" },
													{  y: '.$completedClassPers.', label: "'.get_string('completed','classroomreport').' ('.$completedClass.')", exploded: "" },
							
	
													]
		
	
		
		
												}
												]
											}); ';
	
		
	$courseHTML .=  '	chart.render(); ';
	
		
	$courseHTML .=  '  }';
	$courseHTML .=  '</script><script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js"></script>';
	
	$reportContentPDF .= '<table border="0" cellpadding="0" cellspacing="0" width="100%">';
			$reportContentPDF .= '<tr>';
				$reportContentPDF .= '<td><strong>'.get_string('user','singlereport').':</strong> '.getUsers($userid).'</td>';
				$reportContentPDF .= '<td><strong>'.get_string('numberofclasses','scheduler').':</strong> '.$totalClasses.'</td>';
			$reportContentPDF .= '</tr>';

			$reportContentPDF .= '<tr>';
				$reportContentPDF .= '<td><strong>'.get_string('inprogress','classroomreport').':</strong> '.$incompleteClass.'</td>';
				$reportContentPDF .= '<td><strong>'.get_string('completed','classroomreport').':</strong> '.$completedClass.'</td>';
			$reportContentPDF .= '</tr>';

		 $reportContentPDF .= '</table>';
		 
	$courseHTML .= '<div class="course-listing">'; 
	//$reportContentPDF .= '<div class="clear"></div>';
	$courseHTML .= '<div class="clear"></div>';

		// / route to screen
	$reportContentCSV = "";
	$reportContentPDF .= '<span style="font-size:16pt;">&nbsp;<br /><strong>'.get_string('overallclassroomreport','scheduler').'</strong></span><br /><br />';
	
	$reportContentCSV .= get_string('user','singlereport').",".getUsers($userid)."\n";
	$reportContentCSV .= get_string('numberofclasses','scheduler').",".$totalClasses."\n";
	$reportContentCSV .= get_string('inprogress','classroomreport').",".$incompleteClass."\n";
	$reportContentCSV .= get_string('completed','classroomreport').",".$completedClass."\n";
	
	$reportContentCSV .= get_string('overallclassroomreport','scheduler')."\n";
	$reportContentCSV .= get_string('title').','.get_string ( 'attended', 'scheduler' ).','.get_string ( 'grade', 'scheduler' ).','. get_string ( 'score', 'scheduler' ).','.get_string ( 'classcompletedstatus', 'scheduler' )."\n";
	$reportContentPDF .= '<table '.$CFG->pdfTableStyle.' id="table1"><tr '.$CFG->pdfTableHeaderStyle.'>';
	    $reportContentPDF .= '<td><strong>'.get_string('title').'</strong></td>';
		$reportContentPDF .= "<td><strong>" . get_string ( 'attended', 'scheduler' ) . "</strong></td>";
		$reportContentPDF .= "<td><strong>" . get_string ( 'grade', 'scheduler' ) . "</strong></td>";
		$reportContentPDF .= "<td><strong>" . get_string ( 'score', 'scheduler' ) . "</strong></td>";
		$reportContentPDF .= "<td><strong>" . get_string ( 'classcompletedstatus', 'scheduler' ) . "</strong></td>";
		$reportContentPDF .= '</tr>';
		
		$courseHTML .= '<table class = "table1" id="table1"><tr class="">';
		$courseHTML .= "<th  width='25%'>".get_string('title')."</th>";
		$courseHTML .= "<th width = '15%' >" . get_string ( 'attended', 'scheduler' ) . "</th>";
		$courseHTML .= "<th width = '15%' >" . get_string ( 'grade', 'scheduler' ) . "</th>";
		$courseHTML .= "<th width = '15%' >" . get_string ( 'score', 'scheduler' ) . "</th>";
		$courseHTML .= "<th width = '10%' >" . get_string ( 'classcompletedstatus', 'scheduler' ) . "</th>";
		
		$courseHTML .= '</tr>';
	
		if (! $classes_data) {
			$reportContentPDF .= '<tr><td colspan="5">'.get_string('norecordfound','scheduler').'</td></tr>';
			$courseHTML .= '<tr><td colspan="5">'.get_string('norecordfound','scheduler').'</td></tr>';
		}else{
		
			 $sessionListArr = $DB->get_records_sql ("SELECT msc.*, msa.attended, msa.grade, msa.score, msa.studentid FROM mdl_scheduler_slots msc LEFT JOIN mdl_scheduler_appointment msa ON (msc.id = msa.slotid AND msa.studentid = '".$userid."') WHERE 1 = 1");
			$sessionList = array();
			if(count($sessionListArr) > 0 ){
			  foreach($sessionListArr as $arr){
				$sessionList[$arr->schedulerid][] = $arr;
			  }
			}
		
			foreach ( $classes_data as $class_rec ) {
	
				
				// print_object($learner);
				$reportContentCSV .=  $class_rec->coursename.', , , ,'.($class_rec->is_completed == 1?get_string( 'classcompleted', 'scheduler' ):get_string('classincompleted', 'scheduler'))."\n";
				$reportContentPDF .= "<tr ".$CFG->pdfTableEvenRowStyle."><td width = '14%'><span>" . $class_rec->coursename." - ". $class_rec->classname . "</span></td>";
				$reportContentPDF .= "<td></td>";
				$reportContentPDF .= "<td></td>";
				$reportContentPDF .= "<td></td>";
				$reportContentPDF .= '<td>'.($class_rec->is_completed == 1?get_string( 'classcompleted', 'scheduler' ):get_string('classincompleted', 'scheduler')).'</td>';
				$reportContentPDF .= "</tr>";
				
				$courseHTML .= "<tr class='evenR'><td class='align_left'  width = '14%'><span class='f-left'>" . $class_rec->coursename." - ". $class_rec->classname . "</span></td>";
				$courseHTML .= "<td></td>";
				$courseHTML .= "<td></td>";
				$courseHTML .= "<td></td>";
				$courseHTML .= '<td>'.($class_rec->is_completed == 1?get_string( 'classcompleted', 'scheduler' ):get_string('classincompleted', 'scheduler')).'</td>';
				$courseHTML .= "</tr>";
				
		
				//$session_list = classSessionList ($class_rec->id,'classid');
				$courseHTML .= "<tr><td colspan='5'><div class='a-box'>";
				$i=0;
				if(isset($sessionList[$class_rec->id])){
				foreach ( $sessionList[$class_rec->id] as $session ) {
					        $class = $i%2!=0?'even':'odd';
							$classSession = $i%2!=0?$CFG->pdfInnerTableOddRowStyle:$CFG->pdfInnerTableEvenRowStyle;
							$i++;
							$reportContentCSV .= getSpace('csv').$session->sessionname.','.($session->attended == 1?'Yes':'No').','.($session->grade!=''?$session->grade:getMDashForCSV()).','.($session->score!=''?$session->score:getMDashForCSV())."\n";
							
							$reportContentPDF .= "<tr ".$classSession."><td>" .getSpace(). $session->sessionname . "</td>";
							$reportContentPDF .= "<td>".($session->attended == 1?'Yes':'No')."</td>";
							$reportContentPDF .= "<td>".($session->grade!=''?$session->grade:getMDash())."</td>";
							$reportContentPDF .= "<td>".($session->score!=''?$session->score:getMDash())."</td>";
							$reportContentPDF .= "<td></td></tr>";
							
							/*$courseHTML .= "<tr><td>" . $session->sessionname . "</td>";
							$courseHTML .= "<td>".($session->attended == 1?'Yes':'No')."</td>";
							$courseHTML .= "<td>".($session->grade!=''?$session->grade:getMDash())."</td>";
							$courseHTML .= "<td>".($session->score!=''?$session->score:getMDash())."</td>";
							$courseHTML .= "<td></td></tr>";*/
							
							$courseHTML .= "<div class='".$class."' style='float:left;width:100%;'><div>" . $session->sessionname . "</div>";
							$courseHTML .= "<div>".($session->attended == 1?'Yes':'No')."</div>";
							$courseHTML .= "<div>".($session->grade!=''?$session->grade:getMDash())."</div>";
							$courseHTML .= "<div>".($session->score!=''?$session->score:getMDash())."</div>";
							$courseHTML .= "<div></div></div>";	
						
							
						}
				}else{
					$reportContentCSV .= get_string('norecordfound','scheduler')." \n";
					$reportContentPDF .= '<tr><td  colspan="5">&nbsp;'.get_string('norecordfound','scheduler').'</td></tr>';
					$courseHTML .= get_string('norecordfound','scheduler');
				}
			    $courseHTML .= "</div></td></tr>";	
			}}
			//	$courseHTML .= "<tr><td colspan='4'><input type='submit' name='submit' value='Save & Continue' /></td></tr>";
		
			$reportContentPDF .= "</table>";
			
	
		$courseHTML .= "</table>";
	
		
	
	$courseHTML .="</div>";
	
	// teacher side
	
	if (! empty ( $courseHTML )) {
		//echo '<div class = "course-listing">';
	
		echo html_writer::start_tag ( 'div', array (
				'class' => 'no-overflow'
		) );
		
		if($export && in_array($export, array('exportcsv', 'exportpdf', 'print'))){
		
			
			$HTML = $courseHTML;
			$reportContentCSV = $reportContentCSV;
			$reportContentPDF = $reportContentPDF;
		
		}else{
			redirect(new moodle_url('/'));
		}
		if(isset($export) && $export == 'exportcsv') {
		
			$filepath = $CFG->dirroot."/local/reportexport/temp";
			chmod($filepath, 0777);
			$filename = str_replace(' ', '_', get_string('overallclassroomreport','scheduler'))."_".date("m-d-Y").".csv";
			$filepathname = $filepath.'/'.$filename;
			unlink($filepathname);
			$handler = fopen($filepathname, "w");
			fwrite($handler, $reportContentCSV);
			exportCSV($filepathname);
		}
		if(isset($export) && $export == 'exportpdf') {
				
			$filename = str_replace(' ', '_', get_string('overallclassroomreport','scheduler'))."_".date("m-d-Y").".pdf";
			exportPDF($filename, $reportContentPDF, '', get_string('overallclassroomreport','scheduler'));
		
		}
		//echo $courseHTML;
	

	echo $OUTPUT->header();
		echo $courseHTML;
		echo html_writer::end_tag ( 'div' );
		echo html_writer::end_tag ( 'div' );
	
		// Pring paging bar

	}


	//echo $courseHTML;


?>
<style>
#page { margin: 20px auto 0;}
</style>
