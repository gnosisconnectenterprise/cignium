<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Lists the course categories
 *
 * @copyright 1999 Martin Dougiamas  http://dougiamas.com
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package course
 */

require_once("../config.php");
require_once($CFG->dirroot. '/course/lib.php');
require_once($CFG->libdir. '/coursecatlib.php');
require_once($CFG->libdir. '/filelib.php');

//$categoryid = optional_param('categoryid', 0, PARAM_INT); // Category id
$categoryid = optional_param('categoryid', 1, PARAM_INT); // Category id
$cid = optional_param('id', 0, PARAM_INT);

$courseTypeIdInTab = getCourseTypeIdByCourseId($cid);
if($courseTypeIdInTab == $CFG->courseTypeClassroom){
 checkUserAccess('classroom',$cid);
}else{
 checkUserAccess('course',$cid);
}

$site = get_site();
global $DB,$CFG;
if($cid >0){
	$course = $DB->get_record('course',array('id'=> $cid));
}else{
	redirect($CFG->wwwroot);
}
//$userrole =  getUserRole($USER->id);

$haveCourseAccess = haveClassroomCourseAccess($cid);
if( $USER->archetype == $CFG->userTypeStudent ) {
//if(in_array($userrole, $CFG->customstudentroleid)){  // added by rajesh
  //redirect(new moodle_url('/my/dashboard.php'));
}

if ($categoryid) {
    $PAGE->set_category_by_id($categoryid);
    $PAGE->set_url(new moodle_url('/course/index.php', array('categoryid' => $categoryid)));
    $PAGE->set_pagetype('course-index-category');
    // And the object has been loaded for us no need for another DB call
    $category = $PAGE->category;
} else {
    $categoryid = 0;
    $PAGE->set_url('/course/index.php');
    $PAGE->set_context(context_system::instance());
}

$PAGE->set_pagelayout('coursecategory');
$courserenderer = $PAGE->get_renderer('core', 'course');

require_login(); // added by rajesh 
/*if ($CFG->forcelogin) {
    require_login();
}*/

if ($categoryid && !$category->visible && !has_capability('moodle/category:viewhiddencategories', $PAGE->context)) {
    throw new moodle_exception('unknowncategory');
}

$PAGE->set_heading($site->fullname);
$PAGE->navbar->add(get_string('managecourses'), new moodle_url($CFG->wwwroot.'/course/index.php'));
$PAGE->navbar->add(get_string('editcourse','course'));
$PAGE->navbar->add(get_string('general', 'form'));
$content = $courserenderer->course_category($categoryid);

echo $OUTPUT->header(); 
echo $OUTPUT->skip_link_target();
///// Open - Added by Madhab to show the Common Search Area /////
$paramArray = array(
				'ch' => optional_param('ch', '', PARAM_ALPHA),
				'key' => optional_param('key', '', PARAM_RAW),
				'id' => optional_param('id', '', PARAM_ALPHANUM),
				'nm' => optional_param('nm', '', PARAM_ALPHANUM),
				'desc' => optional_param('desc', '', PARAM_ALPHANUM),
				'tag' => optional_param('tag', '', PARAM_ALPHANUM),
				'cnm' => optional_param('cnm', '', PARAM_ALPHANUM),
			  );
$filterArray = array(							
					'id'=>get_string('id','course'),
					'nm'=>get_string('name','course'),
					'desc'=>get_string('description','course'),
					'tag'=>get_string('tag','course'),
					'cnm'=>get_string('category','course')
			   );
$pageURL = '/course/index.php';


$img_url = getCourseImage($course);
//echo '<pre>'; print_r($course_image); 

$courseHTML = '';
$courseHTML .= "<div class='tabsOuter'>";
$courseHTML .= "<div class='tabLinks'>";
include_once('course_tabs.php');
$courseHTML .= "</div>";
$courseHTML .= '<div class="borderBlockSpace userprofile view_course">';
$courseHTML .= '<div class="user-picture-div">'.$img_url.'</div>';
$courseHTML .= '<div class="user-profile-description"><table cellspacing="0" cellpadding="0" border="0" width="100%">'. 
					'<tr><th>'.get_string('course_name').'</th><td>'.$course->fullname.'</td></tr>'.
					'<tr><th>'.get_string('idnumbercourse').'</th><td>'.$course->idnumber.'</td></tr>'.
					'<tr><th>'.get_string('moduleintro').'</th><td>'.$course->summary.'</td></tr>';
					if($courseTypeIdInTab == 1){ 
					  $courseHTML .= '<tr><th>'.get_string('coursecredithourswithtime').'</th><td>'.(setCreditHoursFormat($course->credithours)).'</td></tr>';
					}
					
					$courseHTML .= '
					 <tr><th>'.get_string('coursetype').'</th><td>'.($course->coursetype_id == $CFG->courseTypeOnline?get_string('onlinecourse'):get_string('classroomcourse')).'</td></tr>
					 <tr><th>'.get_string('suggested_use').'</th><td>'.$course->suggesteduse.'</td></tr>'.
					'<tr><th>'.get_string('learning_obj').'</th><td>'.$course->learningobj.'</td></tr>'.
					
					'<tr><th>'.get_string('performance_out').'</th><td>'.$course->performanceout.'</td></tr>'.
					'<tr><th>'.get_string('keywords').'</th><td>'.$course->keywords.'</td></tr>'.
					'<tr><th>'.get_string('author').'</th><td>'.$course->author.'</td></tr>';
					
					if($CFG->showCourseURL==1 && $course->coursetype_id==1){
					$courseHTML .= 	'<tr><th>'.get_string('courseurl').'</th><td>'.getCourseLaunchURL($course->id).'</td></tr>';
					}
				$courseHTML .= '	</table></div><div class="clear"></div>';
$courseHTML .= '<div class="button"><input type = "button" value = "Cancel" onclick="location.href=\''.$CFG->wwwroot.'/course/index.php\';">';
if($haveCourseAccess == 1){
  $courseHTML .= '<input type = "button" value = "Edit" onclick="location.href=\''.$CFG->wwwroot.'/course/edit.php?id='.$course->id.'\';"></div>';
}
$courseHTML .= '</div></div>';
echo $courseHTML;

echo $OUTPUT->footer();


