<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Lists the course categories
 *
 * @copyright 1999 Martin Dougiamas  http://dougiamas.com
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package course
 */

require_once("../config.php");
require_once($CFG->dirroot. '/course/lib.php');
require_once($CFG->libdir. '/coursecatlib.php');
require_once($CFG->libdir. '/filelib.php');

//$categoryid = optional_param('categoryid', 0, PARAM_INT); // Category id
$categoryid = optional_param('categoryid', 1, PARAM_INT); // Category id
$cid = optional_param('id', 0, PARAM_INT);
$updateid = optional_param('updateid', 0, PARAM_INT);
$updatetype = optional_param('type', '', PARAM_ALPHANUM);
$moduleid = optional_param('moduleid', 0, PARAM_INT);
$flag = optional_param('flag', 0, PARAM_INT);
$showAddDiv = optional_param('show', 0, PARAM_INT);
$deleteId = optional_param('deleteid', 0, PARAM_INT);
$page    = optional_param('page', 1, PARAM_INT);
$perpage = optional_param('perpage', 1, PARAM_INT); 



$courseTypeId = getCourseTypeIdByCourseId($cid);

if($courseTypeId == $CFG->courseTypeClassroom){
	$haveClassroomAccess = haveClassroomCourseAccess($cid);
	if($haveClassroomAccess == $CFG->classroomNoAccess){
	   redirect($CFG->wwwroot.'/course/index.php');
	}
}			
$site = get_site();
global $DB,$CFG,$USER;
if($cid >0){
	$course = $DB->get_record('course',array('id'=> $cid));
}else{
	redirect($CFG->wwwroot);
}

	
//checkUserAccess('course',$cid);


$resourceDetails = '';
if($cid && $updateid){ 

  if($courseTypeId == $CFG->courseTypeClassroom ){
		//$haveResourceAccess = haveResourceAccess($rdata->id);
		if(!haveResourceAssetAccess($updateid)){
		  redirect($CFG->wwwroot.'/course/assetsview.php?id='.$cid.'&show=1');
		}
   }else{		
     //if(!haveResourceAccess($updateid)){
       // redirect($CFG->wwwroot.'/course/assetsview.php?id='.$cid.'&show=1');
     //}
   }
}

//$userrole =  getUserRole($USER->id);
if( $USER->archetype == $CFG->userTypeStudent ) {
//if(in_array($userrole, $CFG->customstudentroleid)){  // added by rajesh
 //redirect(new moodle_url('/my/dashboard.php'));
}

if ($categoryid) {
    $PAGE->set_category_by_id($categoryid);
    $PAGE->set_url(new moodle_url('/course/index.php', array('categoryid' => $categoryid)));
    $PAGE->set_pagetype('course-index-category');
    // And the object has been loaded for us no need for another DB call
    $category = $PAGE->category;
} else {
    $categoryid = 0;
    $PAGE->set_url('/course/index.php');
    $PAGE->set_context(context_system::instance());
}

$PAGE->set_pagelayout('coursecategory');
$courserenderer = $PAGE->get_renderer('core', 'course');

require_login(); // added by rajesh 
/*if ($CFG->forcelogin) {
    require_login();
}*/

if ($categoryid && !$category->visible && !has_capability('moodle/category:viewhiddencategories', $PAGE->context)) {
    throw new moodle_exception('unknowncategory');
}


if($cid && $deleteId && $moduleid ){
	
  if($updatetype == $CFG->resourceModule){
  
		 if($courseTypeId == $CFG->courseTypeClassroom){
		 
			   //$resourcehaveSpecificAsset = isResourceHaveSpecificAsset($deleteId);
			   $haveEnrollment = isClassroomHaveAnyEnrollment($cid);
			  
			   if($haveEnrollment){
			   
					 $_SESSION['update_msg'] = get_string('assetcannotdeletehaveenrollmentinclassroom','course');
					 $_SESSION['error_class'] = 'error_bg';
					 redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php?id='.$cid));
					 
			   }
			   
			  
			   
		 }elseif($courseTypeId == $CFG->courseTypeOnline){
		 
				 $havePublished = isCoursePublished($cid);
				 
				 $haveOUEnrollment = 0;
				 if($havePublished){
				
				     $haveOUEnrollment = getAssignUsersForCourse($cid);
					 if($haveOUEnrollment > 0){
						 $_SESSION['update_msg'] = get_string('cannotdeleteifuserenrolled','course');
						 $_SESSION['error_class'] = 'error_bg';
						 redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php?id='.$cid));
					 }else{

						 $updateResult = getActiveCourseCount($cid);
					   // pr($updateResult);die;
						 if(!$updateResult){
						 
							  $_SESSION['update_msg'] = get_string('cannotdeletesignleasset', 'course');
							  $_SESSION['error_class'] = 'error_bg';
							  redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php?id='.$cid));
						
						 }
					 
					 }	 

				 }

		 }

		  $isResourceDeleted = deleteResource($cid, $moduleid, $deleteId);
			
		  if($isResourceDeleted){
			  $_SESSION['update_msg'] = get_string('record_deleted');
			  $_SESSION['error_class'] = '';
			  redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php?id='.$cid));
		  }else{
			  redirect(new moodle_url($CFG->wwwroot.'/course/index.php'));
		  }
			
	}elseif($updatetype == $CFG->scormModule){

		$havePublished = isCoursePublished($cid);
		  $haveOUEnrollment = 0;
		  if($havePublished){
		
			 $haveOUEnrollment = getAssignUsersForCourse($cid);
			 if($haveOUEnrollment > 0){
				 $_SESSION['update_msg'] = get_string('cannotdeleteifuserenrolled','course');
				 $_SESSION['error_class'] = 'error_bg';
				 redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php?id='.$cid));
			 }else{

				 $updateResult = getActiveCourseCount($cid);
			   // pr($updateResult);die;
				 if(!$updateResult){
				 
					  $_SESSION['update_msg'] = get_string('cannotdeletesignleasset', 'course');
					  $_SESSION['error_class'] = 'error_bg';
					  redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php?id='.$cid));
				
				 }
			 
			 }	 

		  }
				 
	      $isScromDeleted = deleteScrom($cid, $moduleid, $deleteId);
	  
	      if($isScromDeleted){
			  $_SESSION['update_msg'] = get_string('record_deleted');
			  $_SESSION['error_class'] = '';
			  redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php?id='.$cid));
		  }else{
			  redirect(new moodle_url($CFG->wwwroot.'/course/index.php'));
		  }
	}	
}

$PAGE->set_heading($site->fullname);

$PAGE->navbar->add(get_string('managecourses'), new moodle_url($CFG->wwwroot.'/course/index.php'));
$PAGE->navbar->add(get_string('editcourse','course'), new moodle_url($CFG->wwwroot.'/course/edit.php',array('id'=>$cid)));
//$PAGE->navbar->add(get_string('manage_assets'));
$PAGE->navbar->add(get_string('assets'));

$content = $courserenderer->course_category($categoryid);

if($course->format == 'singleactivity'){
	$course_modules = $DB->get_record('course_format_options',array('courseid'=>$course->id));
}elseif($course->format == 'topics'){
	$course_modules = $DB->get_record('course_format_options',array('courseid'=>$course->id,'name'=>'numsections'));
}

if($course->format == 'singleactivity' && $course_modules->value == $CFG->scormModule){
	$data = $DB->get_record('scorm',array('course'=>$course->id));
	if($data){
		$data->modtype = 'Scorm';
	}
}elseif($course->format == 'singleactivity' && $course_modules->value == 'resource'){
	$data = $DB->get_record('resource',array('course'=>$course->id, 'is_reference_material'=>0));
	if($data){
		$data->modtype = 'Resource';
	}
}elseif($course->format == 'topics' && $course_modules->value >0){

    $resourceParams = array('course'=>$course->id, 'is_reference_material'=>0);
    if($courseTypeId == $CFG->courseTypeClassroom ){
	
	    $resourceSql = "SELECT * FROM {$CFG->prefix}resource WHERE 1 = 1";
		if($USER->archetype == $CFG->userTypeManager ){
		   if($course->createdby == $USER->id){
			 $resourceSql .= " AND course = '".$course->id."' and is_reference_material='0'";
		   }else{
			 $resourceSql .= " AND course = '".$course->id."' and is_reference_material='0' AND if(createdby = '".$USER->id."', (createdby = '".$USER->id."' OR is_active = '1'), is_active = '1') ";
		   } 
		}elseif($USER->archetype == $CFG->userTypeStudent ){
		    $resourceSql .= " AND course = '".$course->id."' and is_reference_material='0' AND if(createdby = '".$USER->id."', (createdby = '".$USER->id."' OR is_active = '1'), is_active = '1') ";
		}else{
			$resourceSql .= " AND course = '".$course->id."' and is_reference_material = 0";
		   $resourceParams = array('course'=>$course->id, 'is_reference_material'=>0);
		}
		//echo $resourceSql;die;
		$resdata = $DB->get_records_sql($resourceSql);
		
	}else{	
	    $resourceParams = array('course'=>$course->id, 'is_reference_material'=>0);
		$resdata = $DB->get_records('resource', $resourceParams);
	}
	
	
	$scodata = $DB->get_records('scorm',array('course'=>$course->id));
	//redirect($CFG->wwwroot.'/course/view.php?id='.$course->id.'&edit=on&sesskey='.$USER->sesskey);
} 
$courseHTML = '';
$courseHTML .= "<div class='tabsOuter'>";
$courseHTML .= "<div class='tabLinks'>";
include_once('course_tabs.php');
$courseHTML .= "</div>";
$courseHTML .= '<div class="clear"></div><div class="borderBlockSpace view_assests">';
if($course->format == 'singleactivity' && $course_modules->value == $CFG->scormModule  && !$data){
	$courseHTML .= '<div class="add-course-button"><a class="button-link add-assests" href="'.$CFG->wwwroot.'/course/modedit.php?add=scorm&type=&course='.$course->id.'&section=0&return=0&sr=">'.get_string('add_assets').'</a></div>';
	if($_SESSION['update_msg']!=''){
		$courseHTML .= '<div class="clear"></div>';
		$courseHTML .= '<div class="bg-success text-success text-center '.$_SESSION['error_class'].'" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
		$courseHTML .= '<div class="clear"></div>';
		$_SESSION['update_msg']='';
	}
}elseif($course->format == 'singleactivity' && $course_modules->value == 'resource'  && !$data){
	$courseHTML .= '<div class="add-course-button"><a class="button-link add-assests" href="'.$CFG->wwwroot.'/course/modedit.php?add=resource&type=&course='.$course->id.'&section=0&return=0&sr=">'.get_string('add_assets').'</a></div>';
	if($_SESSION['update_msg']!=''){
		$courseHTML .= '<div class="clear"></div>';
		$courseHTML .= '<div class="bg-success text-success text-center '.$_SESSION['error_class'].'" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
		$courseHTML .= '<div class="clear"></div>';
		$_SESSION['update_msg']='';
	}
}elseif($course->format == 'topics' && $course_modules->value >0){
	if($updatetype == '' && $updateid == 0){
	    
		if($courseTypeId == $CFG->courseTypeClassroom){
			$addAssetAccess = haveClassroomCourseAccess($cid);
			if($addAssetAccess == $CFG->classroomFullAccess || $addAssetAccess == $CFG->classroomMediumAccess ){
			  $courseHTML .= '<div class="add-course-button"><a class="button-link add-assests" href="javascript:void(0);"  id="add-assests"><i></i>'.get_string('add_assets').'</a></div>';
			} 
		}else{
		  $courseHTML .= '<div class="add-course-button"><a class="button-link add-assests" href="javascript:void(0);"  id="add-assests"><i></i>'.get_string('add_assets').'</a></div>';
		}
		if($_SESSION['update_msg']!=''){
			$courseHTML .= '<div class="clear"></div>';
			$courseHTML .= '<div class="bg-success text-success text-center '.$_SESSION['error_class'].'" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
			$courseHTML .= '<div class="clear"></div>';
			$_SESSION['update_msg']='';
		}
		$courseHTML .= '<div class="assets_ele clear" id ="assets_element" style="'.(($showAddDiv == 1) ? 'display:block;' : 'display:none;').'"><table class="generaltable" cellspacing="0" cellpadding="0" border="0"><tr><th width="30%">Asset Name</th><th width="35%">Description</th><th width="20%">Type</th><th width="15%">Upload File</th></tr>';
		$courseHTML .= '<tr><form method="post" id="asset_form" action="'.$CFG->wwwroot.'/course/medium.php">';
		$courseHTML .= '<td style="text-align:center;"><input type ="text" id="asset_name" name ="asset_name"></td><td style="text-align:center;"><textarea id="asset_description" name ="asset_description" style="width:300px;height:50px;" ></textarea><!--input type ="textarea" id="asset_description" name ="asset_description" --></td>';
		
		if($courseTypeId == $CFG->courseTypeClassroom ){
            $resourceType = getResourceType();

		    $courseHTML .= '<td style="text-align:center;"><input type ="hidden" id="asset_type" name ="asset_type" value="resource">
			<select id="resource_type" name= "resource_type">
			<option value="">'.get_string('selecttype','course').'</option>';
			if(count($resourceType) > 0 ){
			  foreach($resourceType as $resourceTypeArr){
			    $courseHTML .= '<option value="'.$resourceTypeArr->id.'">'.$resourceTypeArr->name.'</option>';
			  }
			}
			$courseHTML .= '</select>
			'.get_string('allowstudentaccess','course').' <input type="checkbox"  name="allow_student_access" id="allow_student_access" value="1">
			</td>';
		   
		}else{
		    $courseHTML .= '<td style="text-align:center;"><select id="asset_type" name= "asset_type"><option value="">'.get_string('selecttype','course').'</option><option value="resource">'.get_string('courseresource','course').'</option><option value="scorm">'.get_string('coursescorm','course').'</option></select></td>';
		}   
		
		$courseHTML .= '<td style="text-align:center;"><a href="javascript:void(0);" title="Upload" id="asset_upload_file" class="uploadFileBtn">Upload</a><input type="hidden" name="course_id" id="course_id" value="'.$course->id.'"><input type="hidden" name="action_type" id="action_type" value="add"></td>';
		$courseHTML .= '</form></tr>';
		$courseHTML .= '</table></div>';
	}elseif($updatetype != '' && $updateid > 0){ 
		if($moduleid >0){
		$table_name = "mdl_".$updatetype;
		$asset_data = $DB->get_record_sql("select * from $table_name where id = $updateid");
		//$courseHTML .= '<div class="add-course-button add-assests"><a href="javascript:void(0);"  id="add-assests"><i></i>'.get_string('add_assets').'</a></div>';
		$courseHTML .= '<div class="add-course-button"><a class="button-link add-assests" href="'.$CFG->wwwroot.'/course/assetsview.php?id='.$course->id.'&show=1"  id="add-assests"><i></i>'.get_string('add_assets').'</a></div>';
		$courseHTML .= '<div class="assets_ele clear" id ="assets_element" style="display:block;"><table class="generaltable" cellspacing="0" cellpadding="0" border="0"><tr><th width="30%">Asset Name</th><th width="35%">Description</th><th width="20%">Type</th><th width="15%">Upload File</th></tr>';
		if($updatetype == "resource"){
			$courseHTML .= '<tr><form method="post" id="asset_form" action="'.$CFG->wwwroot.'/course/medium.php">';
			$courseHTML .= '<td style="text-align:center;"><input type ="text" id="asset_name" name ="asset_name" value="'.$asset_data->name.'"></td><td style="text-align:center;"><textarea id="asset_description" name ="asset_description" style="width:300px;height:50px;" >'.strip_tags($asset_data->intro).'</textarea><!--input type ="textarea" id="asset_description" name ="asset_description" value="'.strip_tags($asset_data->intro).'"--></td>';
			if($courseTypeId == $CFG->courseTypeClassroom ){
                 $resourceType = getResourceType();
				 
				  // $classroomSpecificAsset = getClassroomSpecificAsset('id');
				  // $classroomSpecificAssetArr = !empty($classroomSpecificAsset)?explode(",",$classroomSpecificAsset):array();
				   //$resourceTypeDisabled = ''; 
				   //if(count($classroomSpecificAssetArr) > 0 && in_array($asset_data->resource_type, $classroomSpecificAssetArr)){
					// $resourceTypeDisabled = 'disabled';
				  // }
				 
				 $resourceTypeDisabled = '';	
				 $courseHTML .= '<td style="text-align:center;"><input type ="hidden" id="asset_type" name ="asset_type" value="resource">
								 <select id="resource_type" name= "resource_type" '.$resourceTypeDisabled.' >
								 <option value="">'.get_string('selecttype','course').'</option>';
								 
									if(count($resourceType) > 0 ){
									  foreach($resourceType as $resourceTypeArr){
									    if($asset_data->resource_type == $resourceTypeArr->id){
										   $courseHTML .= '<option value="'.$resourceTypeArr->id.'" selected="selected" >'.$resourceTypeArr->name.'</option>';
										}else{
										   $courseHTML .= '<option value="'.$resourceTypeArr->id.'">'.$resourceTypeArr->name.'</option>';
										}
									  }
									}
			     $courseHTML .= '</select>
				 
				 '.get_string('allowstudentaccess','course').' : <input type="checkbox"  name="allow_student_access" id="allow_student_access" value="1" '.($asset_data->allow_student_access == 1?"checked='checked'":"").'>
				 </td>';	  
				 /*$resourceTypeDisabled = 'disabled';  
				 $courseHTML .= '<td style="text-align:center;"><input type ="hidden" id="asset_type" name ="asset_type" value="resource">
								 <select id="resource_type1" name= "resource_type1" '.$resourceTypeDisabled.' >
								 <option value="">'.get_string('selecttype','course').'</option>';
								 
									if(count($resourceType) > 0 ){
									  foreach($resourceType as $resourceTypeArr){
									    if($asset_data->resource_type == $resourceTypeArr->id){
										   $courseHTML .= '<option value="'.$resourceTypeArr->id.'" selected="selected" >'.$resourceTypeArr->name.'</option>';
										}else{
										   $courseHTML .= '<option value="'.$resourceTypeArr->id.'">'.$resourceTypeArr->name.'</option>';
										}
									  }
									}
			     $courseHTML .= '</select>
				 <input type ="hidden" id="resource_type" name ="resource_type" value="'.$asset_data->resource_type.'">
				 '.get_string('allowstudentaccess','course').' : <input type="checkbox"  name="allow_student_access" id="allow_student_access" value="1" '.($asset_data->allow_student_access == 1?"checked='checked'":"").'>
				 </td>';*/
			}else{
			    $courseHTML .= '<td style="text-align:center;"><select id="asset_type" name= "asset_type" disabled = "disabled"><option value="">'.get_string('selecttype','course').'</option><option value="resource" selected>Resource</option><option value="scorm">SCORM</option></select></td>';
			}
			
			$courseHTML .= '<td style="text-align:center;"><a href="javascript:void(0);" title="Upload" id="asset_upload_file" class="uploadFileBtn">Upload</a><input type="hidden" name="course_id" id="course_id" value="'.$course->id.'"><input type="hidden" name="moduleid" id="moduleid" value="'.$moduleid.'"><input type="hidden" name="action_type" id="action_type" value="update"></td></form></tr>';
		}elseif($updatetype == "scorm"){
			$courseHTML .= '<tr><form method="post" id="asset_form" action="'.$CFG->wwwroot.'/course/medium.php"><td style="text-align:center;"><input type ="text" id="asset_name" name ="asset_name" value="'.$asset_data->name.'"></td><td style="text-align:center;"><textarea id="asset_description" name ="asset_description" style="width:300px;height:50px;" >'.strip_tags($asset_data->intro).'</textarea><!--input type ="textarea" id="asset_description" name ="asset_description" value="'.strip_tags($asset_data->intro).'"--></td><td style="text-align:center;"><select id="asset_type" name= "asset_type" disabled = "disabled"><option value="">Select type</option><option value="resource" >Resource</option><option value="scorm" selected>SCORM</option></select></td><td style="text-align:center;"><a href="javascript:void(0);" title="Upload" id="asset_upload_file" class="uploadFileBtn">Upload</a><input type="hidden" name="course_id" id="course_id" value="'.$course->id.'"><input type="hidden" name="moduleid" id="moduleid" value="'.$moduleid.'"><input type="hidden" name="action_type" id="action_type" value="update"></td></form></tr>';
			
		}
		$courseHTML .= '</table></div>';
		$courseHTML .= '<div class="asset_save paddBottom"><input type="button" id="asset_save" value="'.get_string('quickupdate', 'course').'" name="asset_save"></div>';
		}else{
		
				if($courseTypeId == $CFG->courseTypeClassroom ){
				 
						if($updatetype == 'resource'){

                            /*$resourcehaveSpecificAsset = isResourceHaveSpecificAsset($updateid);
						    if($resourcehaveSpecificAsset){
						   
								 $totalSpecificAsset = isClassroomHaveSpecificAsset($cid);
								 $havePublished = isCoursePublished($cid);
								 
								 if($havePublished && $flag == 0){
									  if($totalSpecificAsset <= 1){
										 $a = new stdClass();
										 $a->specific = getClassroomSpecificAsset();
										 $_SESSION['update_msg'] = get_string('cannotdeactivespecificasset','course', $a->specific);
										 $_SESSION['error_class'] = 'error_bg';
										 redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php?id='.$cid));
									   }
								 }
							  }*/
							  
							  $DB->set_field('resource', 'is_active', $flag, array('id'=>$updateid));
							  $_SESSION['update_msg'] = get_string('record_updated');
							  $_SESSION['error_class'] = '';
							  redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php?id='.$course->id));
						}
						
							
				}else{


                        /*$updateResult = getActiveCourseCount($course->id);
						
						if(!$updateResult && $flag == 0){
							$_SESSION['update_msg'] = get_string('cannotdesctivateasset');
							$_SESSION['error_class'] = 'error_bg';
						}else{
							if($updatetype == $CFG->scormModule){
								$DB->set_field('scorm', 'is_active', $flag, array('id'=>$updateid));
							}else{
								$DB->set_field('resource', 'is_active', $flag, array('id'=>$updateid));
							}
							$_SESSION['update_msg'] = get_string('record_updated');
							$_SESSION['error_class'] = '';
						}*/
						
						$havePublished = isCoursePublished($course->id);
						$haveOUEnrollment = 0;
						if($havePublished){
				
							 $haveOUEnrollment = getAssignUsersForCourse($course->id);
							 if($haveOUEnrollment > 0){
								 $updateResult = getActiveCourseCount($course->id);
							    // pr($updateResult);die;
								 if(!$updateResult && $flag == 0){
									$_SESSION['update_msg'] = get_string('cannotdesctivateasset');
									$_SESSION['error_class'] = 'error_bg';
									redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php?id='.$course->id));
								 }
								 
							 }
		
					    }
						

						if($updatetype == $CFG->scormModule){
							$DB->set_field('scorm', 'is_active', $flag, array('id'=>$updateid));
						}else{
							$DB->set_field('resource', 'is_active', $flag, array('id'=>$updateid));
						}
						$_SESSION['update_msg'] = get_string('record_updated');
						$_SESSION['error_class'] = '';
						redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php?id='.$course->id));
						
	
				 }	
			
		}
	}
}
$courseHTML .= '<div class="coursenametitle"><h3>'.$course->fullname.'</h3></div>';
$courseHTML .= '<div class="borderBlock"><h2 class="icon_title">Assets ('.(count($scodata) + count($resdata)).')</h2><div class="borderBlockSpace"><table cellspacing="0" cellpadding="0" border="0" width="100%"><thead>'. 
					'<tr>';
					
					if($courseTypeId == $CFG->courseTypeClassroom ){
					  $courseHTML .= '<td width="30%">'.get_string('asset_name').'</td>';
					  $courseHTML .= '<td width="20%">'.get_string('type').'</td>';
					  $courseHTML .= '<td width="25%">'.get_string('assestcreatedby').'</td>';
					  
						
					}else{
					  $courseHTML .= '<td width="25%">'.get_string('asset_name').'</td>';
					  $courseHTML .= '<td width="15%">'.get_string('type').'</td>';
					  $courseHTML .= '<td width="35%">'.get_string('description').'</td>';
					}
					$courseHTML .= '<td width="25%">'.get_string('manage').'</td></tr></thead><tbody>';
					
					
		$dataIntro = $data->intro?nl2br($data->intro):$data->intro;			
		if($course_modules->value == $CFG->scormModule && $data){
			$course_moduleid = $DB->get_field('course_modules','id',array('course'=>$course->id));
			if($data->is_active == 1){
				$courseHTML .=  '<tr><td>'.$data->name.'</td><td>'.$data->modtype.'</td><td>'.$dataIntro.'</td>';
				$courseHTML .= '<td><div class="adminiconsBar order"><a href="'.$CFG->wwwroot.'/mod/scorm/view.php?id='.$course_moduleid.'" class="view" title="'.get_string('previewlink','course').'">'.get_string('previewlink','course').'</a> ' . 
						  '<a href="'.$CFG->wwwroot.'/course/modedit.php?update='.$course_moduleid.'" class="edit" title="'.get_string('edit','course').'">'.get_string('edit','course').'</a> ' . 
						  '<a href="'.$genURL . $flagURL.'" class="disable" title="'.get_string('deactivatelink','course').'">'.get_string('deactivatelink','course').'</a>';
				
			
					  		  
				$courseHTML .=  '</div></td></tr>';
			}else{
				$courseHTML .=  '<tr class="disable"><td>'.$data->name.'</td><td>'.$data->modtype.'</td><td>'.$dataIntro.'</td>';
				$courseHTML .= '<td><div class="adminiconsBar order"><a  href="javascript:void(0);" class="view-disable"  title="'.get_string('previewlink','course').'">'.get_string('previewlink','course').'</a> ' . 
						  '<a href="javascript:void(0);" class="edit-disable" title="'.get_string('edit','course').'">'.get_string('edit','course').'</a> ';
				$courseHTML .= '<a href="'.$genURL . $flagURL.'" class="disable" title="'.get_string('deactivatelink','course').'">'.get_string('deactivatelink','course').'</a>';
				$courseHTML .=  '</div></td></tr>';
			}
		}elseif($course_modules->value == 'resource'  && $data){
			$course_moduleid = $DB->get_field('course_modules','id',array('course'=>$course->id));
			if($data->is_active == 1){
			
				$courseHTML .=  '<tr><td>'.$data->name.'</td><td>'.$data->modtype.'</td><td>'.$dataIntro.'</td>';
				$courseHTML .= '<td><div class="adminiconsBar order"><a href="'.$CFG->wwwroot.'/mod/resource/view.php?id='.$course_moduleid.'" class="view" title="'.get_string('previewlink','course').'">'.get_string('previewlink','course').'</a> ' . 
							  '<a href="'.$CFG->wwwroot.'/course/assetsview.php?updateid='.$data->id.'&type=resource&id='.$course->id.'&moduleid='.$course_moduleid.'" class="edit" title="'.get_string('edit','course').'">'.get_string('edit','course').'</a> ';
				$courseHTML .= '<a href="'.$genURL . $flagURL.'" class="disable" title="'.get_string('deactivatelink','course').'">'.get_string('deactivatelink','course').'</a>';
			}else{
				$courseHTML .=  '<tr class="disable"><td>'.$data->name.'</td><td>'.$data->modtype.'</td><td>'.$dataIntro.'</td>';
				$courseHTML .= '<td><div class="adminiconsBar order"><a  href="javascript:void(0);" class="view-disable"  title="'.get_string('previewlink','course').'">'.get_string('previewlink','course').'</a> ' . 
						  '<a href="javascript:void(0);" class="edit-disable" title="'.get_string('edit','course').'">'.get_string('edit','course').'</a> ';
				$courseHTML .= '<a href="'.$genURL . $flagURL.'" class="disable" title="'.get_string('deactivatelink','course').'">'.get_string('deactivatelink','course').'</a>';
			}
		}elseif($course_modules->value  >0  && ($resdata || $scodata)){
		
			if($resdata){
				foreach($resdata as $rdata){
				
				$haveResourceAccess = true;
				$rdataIntro = $rdata->intro?nl2br($rdata->intro):$rdata->intro;
				
				if(strlen($rdataIntro) >100){
					$rdataIntro = substr($rdataIntro,0,100)." ...";
				}
				
				$course_moduleid = $DB->get_field('course_modules','id',array('course'=>$course->id,'instance'=>$rdata->id,'module'=>17));
				
				if($rdata->is_active == 1){
				    if($rdata->resource_type){
					
					  $resourceTypeName = getResourceType($rdata->resource_type);
					  //$classroomSpecificAsset = getClassroomSpecificAsset('id');
					  //$classroomSpecificAssetArr = !empty($classroomSpecificAsset)?explode(",",$classroomSpecificAsset):array();
					  //$isAbleToAccess = false; 
					 // if(count($classroomSpecificAssetArr) > 0 && !in_array($rdata->resource_type, $classroomSpecificAssetArr)){
					  //  $isAbleToAccess = true;
					  //}
					  
					   $courseHTML .=  '<tr><td>'.$rdata->name.'</td><td>'.$resourceTypeName.'</td>';
					  
					   if($courseTypeId == $CFG->courseTypeClassroom ){
							$haveResourceAccess = haveResourceAssetAccess($rdata->id);
					        $courseHTML .=  '<td>'.getUsers($rdata->createdby).'</td>';
					   }else{
					        $courseHTML .=  '<td>'.$rdataIntro.'</td>';
					   }	 
					}else{
					  $courseHTML .=  '<tr><td>'.$rdata->name.'</td><td>Resource</td><td>'.$rdataIntro.'</td>';
					}
					
					
					$courseHTML .= '<td><div class="adminiconsBar order">';
					
					$launchurl = $CFG->wwwroot.'/mod/resource/view.php?id='.$course_moduleid.'&redirect=1';

					$isRExist = isResourceFileExist($course_moduleid);
					
					if($isRExist || $rdata->is_weblink == 1){
					  $courseHTML .= "<a href=\"javascript:void(0);\" onclick=\"window.open('".$launchurl."', '', '".$CFG->FVPreviewWindowParameter."'); \" class = \"view\" title=\"".get_string('previewlink','course')."\">".get_string('previewlink','course')."</a>";
				    }else{
					  $courseHTML .= "<a href='javascript:;' onclick='alert(\"".get_string('clamdeletedfile')."\")' title='".get_string('download','learnercourse')."' class = \"view\" title=\"".get_string('previewlink','course')."\">".get_string('previewlink','course')."</a>";
					}
					
					
					if($courseTypeId == $CFG->courseTypeClassroom ){
						
						if($haveResourceAccess){
							
							
							$courseHTML .= '<a href="'.$CFG->wwwroot.'/course/assetsview.php?updateid='.$rdata->id.'&type=resource&id='.$course->id.'&moduleid='.$course_moduleid.'" class="edit" title="'.get_string('edit','course').'">'.get_string('edit','course').'</a> ' ;
							//if($haveEnrollment){
							$courseHTML .= '<a href="'.$CFG->wwwroot.'/course/assetsview.php?updateid='.$rdata->id.'&type=resource&id='.$course->id.'&flag=0" class="disable" title="'.get_string('deactivatelink','course').'">'.get_string('deactivatelink','course').'</a>';
							//}			  
							
							$courseHTML .= '<a href="'.$CFG->wwwroot.'/course/assetsview.php?deleteid='.$rdata->id.'&type=resource&id='.$course->id.'&moduleid='.$course_moduleid.'" class="delete" title="'.get_string('delete','course').'">'.get_string('delete','course').'</a> ';
								  
						}
					
					}else{
					  $courseHTML .= '<a href="'.$CFG->wwwroot.'/course/assetsview.php?updateid='.$rdata->id.'&type=resource&id='.$course->id.'&moduleid='.$course_moduleid.'" class="edit" title="'.get_string('edit','course').'">'.get_string('edit','course').'</a> ' ;
					  $courseHTML .= '<a href="'.$CFG->wwwroot.'/course/assetsview.php?updateid='.$rdata->id.'&type=resource&id='.$course->id.'&flag=0" class="disable" title="'.get_string('deactivatelink','course').'">'.get_string('deactivatelink','course').'</a>';
									  
					  $haveOCPublished = isCoursePublished($course->id);				  
					  if($haveOCPublished){
					  
					     $uAssigned = getAssignUsersForCourse($course->id);	 
						 
					     if($uAssigned > 0 ){
						   $courseHTML .= '<a href="'.$CFG->wwwroot.'/course/assetsview.php?deleteid='.$rdata->id.'&type=resource&id='.$course->id.'&moduleid='.$course_moduleid.'" class="delete" title="'.get_string('delete','course').'">'.get_string('delete','course').'</a> ';
					     }else{
					       $courseHTML .= '<a href="'.$CFG->wwwroot.'/course/assetsview.php?deleteid='.$rdata->id.'&type=resource&id='.$course->id.'&moduleid='.$course_moduleid.'" class="delete" title="'.get_string('delete','course').'">'.get_string('delete','course').'</a> ';
						 }
					  }else{
					    $courseHTML .= '<a href="'.$CFG->wwwroot.'/course/assetsview.php?deleteid='.$rdata->id.'&type=resource&id='.$course->id.'&moduleid='.$course_moduleid.'" class="delete" title="'.get_string('delete','course').'">'.get_string('delete','course').'</a> ';
					  }
					}
							  
					$courseHTML .=  '</div></td></tr>';
				}else{
				
				    if($rdata->resource_type){
					  $resourceTypeName = getResourceType($rdata->resource_type);
					   $courseHTML .=  '<tr><td class="disable">'.$rdata->name.'</td><td>'.$resourceTypeName.'</td>';
					  
					   if($courseTypeId == $CFG->courseTypeClassroom ){
					        //$haveResourceAccess = haveResourceAccess($rdata->id);
					        $courseHTML .=  '<td>'.getUsers($rdata->createdby).'</td>';
					   }else{
					        $courseHTML .=  '<td>'.$rdataIntro.'</td>';
					   }	 
					}else{
					  $courseHTML .=  '<tr class="disable"><td>'.$rdata->name.'</td><td>Resource</td><td>'.$rdataIntro.'</td>';
					}
					
					
					$courseHTML .= '<td><div class="adminiconsBar order">' ;
					$launchurl = $CFG->wwwroot.'/mod/resource/view.php?id='.$course_moduleid.'&redirect=1';
					
					$isRExist = isResourceFileExist($course_moduleid);
						
					if($isRExist || $rdata->is_weblink == 1){
						$courseHTML .= "<a href=\"javascript:void(0);\" onclick=\"window.open('".$launchurl."', '', '".$CFG->FVPreviewWindowParameter."'); \" class = \"view\" title=\"".get_string('previewlink','course')."\">".get_string('previewlink','course')."</a>";
					}else{
					  $courseHTML .= "<a href='javascript:;' onclick='alert(\"".get_string('clamdeletedfile')."\")' title='".get_string('download','learnercourse')."' class = \"view\" title=\"".get_string('previewlink','course')."\">".get_string('previewlink','course')."</a>";
					}
					
							  
					if($courseTypeId == $CFG->courseTypeClassroom ){
					
					     if(haveResourceAssetAccess($rdata->id)){
						  $courseHTML .= '<a href="javascript:void(0);" class="edit-disable" title="'.get_string('edit','course').'">'.get_string('edit','course').'</a> ';	  
						  $courseHTML .=  '<a href="'.$CFG->wwwroot.'/course/assetsview.php?updateid='.$rdata->id.'&type=resource&id='.$course->id.'&flag=1" class="enable" title="'.get_string('activatelink','course').'">'.get_string('deactivatelink','course').'</a>';
						}else{
						  // $courseHTML .= '<a href="javascript:void(0);" class="edit-disable">'.get_string('edit','course').'</a> ';	
						 // $courseHTML .=  '<a href="javascript:void(0);" class="status-disable" title="'.get_string('activatelink','course').'">'.get_string('deactivatelink','course').'</a>';
						}
					}else{
					
					    $courseHTML .= '<a href="javascript:void(0);" class="edit-disable" title="'.get_string('edit','course').'">'.get_string('edit','course').'</a> ';	
							  
					    $courseHTML .=  '<a href="'.$CFG->wwwroot.'/course/assetsview.php?updateid='.$rdata->id.'&type=resource&id='.$course->id.'&flag=1" class="enable" title="'.get_string('activatelink','course').'">'.get_string('deactivatelink','course').'</a>';
					}
					$courseHTML .=  '</div></td></tr>';
				}
				}
			}
			
			//pr($scodata);die;
			if($scodata){
				foreach($scodata as $sdata){
					$sdataIntro = $sdata->intro?nl2br($sdata->intro):$sdata->intro;
					
					if(strlen($sdataIntro) >100){
						$sdataIntro = substr($sdataIntro,0,100)." ...";
					}
				$course_moduleid = $DB->get_field('course_modules','id',array('course'=>$course->id,'instance'=>$sdata->id,'module'=>18));
				if($sdata->is_active == 1){
					
					$launchurlpopup = getScormLaunchURLWithLink($course->id,$course_moduleid, 1);
					
					$courseHTML .=  '<tr><td>'.$sdata->name.'</td><td>SCORM</td><td>'.$sdataIntro.'</td>';
					
					$courseHTML .= "<td><div class='adminiconsBar order'>".$launchurlpopup;

					//$courseHTML .= '<td><a href="'.$CFG->wwwroot.'/mod/scorm/view.php?id='.$course_moduleid.'" class="view"   title="'.get_string('previewlink','course').'">'.get_string('previewlink','course').'</a> ';

					$courseHTML .= '<a href="'.$CFG->wwwroot.'/course/assetsview.php?updateid='.$sdata->id.'&type=scorm&id='.$course->id.'&moduleid='.$course_moduleid.'" class="edit" title="'.get_string('edit','course').'">'.get_string('edit','course').'</a> ' . 
							  '<a href="'.$CFG->wwwroot.'/course/assetsview.php?updateid='.$sdata->id.'&type=scorm&id='.$course->id.'&flag=0" class="disable" title="'.get_string('deactivatelink','course').'">'.get_string('deactivatelink','course').'</a>';
							  
							  	
				    //if( ($USER->archetype == $CFG->userTypeAdmin ) || ($USER->archetype == $CFG->userTypeManager && $data->createdby == $USER->id) ){
					  $courseHTML .= '<a href="'.$CFG->wwwroot.'/course/assetsview.php?deleteid='.$sdata->id.'&type='.$CFG->scormModule.'&id='.$course->id.'&moduleid='.$course_moduleid.'" class="delete" title="'.get_string('delete','course').'">'.get_string('delete','course').'</a> ';
				    //}
				  
					$courseHTML .=  '</div></td></tr>';
				}else{
				
				    $launchurlpopup = getScormLaunchURLWithLink($course->id,$course_moduleid, 1);
					$courseHTML .=  '<tr class="disable"><td>'.$sdata->name.'</td><td>SCORM</td><td>'.$sdataIntro.'</td>';
					$courseHTML .= "<td><div class='adminiconsBar order'>".$launchurlpopup;
					$courseHTML .= '<a href="javascript:void(0);" class="edit-disable" title="'.get_string('edit','course').'">'.get_string('edit','course').'</a> ' . 
							  '<a href="'.$CFG->wwwroot.'/course/assetsview.php?updateid='.$sdata->id.'&type=scorm&id='.$course->id.'&flag=1" class="enable" title="'.get_string('activatelink','course').'">'.get_string('deactivatelink','course').'</a>';
					$courseHTML .=  '</div></td></tr>';
				}
				}
			}
		}else{
			$courseHTML .=  '<tr><td colspan="4">'.get_string('no_results').'</td></tr>';
		}
	$courseHTML .= '</tbody></table></div></div>';
//$courseHTML .= '<input type = "button" value = "Edit" onclick="location.href=\''.$CFG->wwwroot.'/course/edit.php?id='.$course->id.'\';">';
$urlCancel = new moodle_url($CFG->wwwroot.'/course/courseview.php', array('id'=>$cid));
$courseHTML .= '<div class="clear" style="padding-top:18px;"></div><input type = "button" value = "Cancel" onclick="location.href=\''.$urlCancel.'\';">';
$courseHTML .= '</div></div>';

$paramArray = array(
				'ch' => optional_param('ch', '', PARAM_ALPHA),
				'key' => optional_param('key', '', PARAM_RAW),
				'id' => optional_param('id', '', PARAM_ALPHANUM),
				'nm' => optional_param('nm', '', PARAM_ALPHANUM),
				'desc' => optional_param('desc', '', PARAM_ALPHANUM),
				'tag' => optional_param('tag', '', PARAM_ALPHANUM),
				'cnm' => optional_param('cnm', '', PARAM_ALPHANUM),
			  );
$filterArray = array(							
					'id'=>get_string('id','course'),
					'nm'=>get_string('name','course'),
					'desc'=>get_string('description','course'),
					'tag'=>get_string('tag','course'),
					'cnm'=>get_string('category','course')
			   );
$pageURL = '/course/index.php';

echo $OUTPUT->header(); 
echo $OUTPUT->skip_link_target();
///// Open - Added by Madhab to show the Common Search Area /////

echo $courseHTML;

echo $OUTPUT->footer();


?>
<script language="javascript" type="text/javascript">
$(document).ready(function(){
	$('#add-assests').click(function(event){
		$('#assets_element').show();
	});
	$('#asset_upload_file').click(function(event){
		var asset_name = $.trim($('#asset_name').val());
		var asset_description = $.trim($('#asset_description').val());
	
		<?php if($courseTypeId == $CFG->courseTypeClassroom ){?>
		     var asset_type = $('#resource_type').val();
		<?php }else{?>
		     var asset_type = $('#asset_type').val();
		<?php } ?>
		if(asset_name == ''){
			alert("Please Enter Asset Name");
			$('#asset_name').val(asset_name);
			return false;
		}
		if(asset_description == ''){
			alert("Please Enter Description");
			$('#asset_description').val(asset_description);
			return false;
		}
		if(asset_type == ''){
			alert("Please Select Type");
			return false;
		}
		$('#asset_form').submit();
		/*var rooturl = "<?php echo $CFG->wwwroot; ?>";
		var courseid = "<?php echo $course->id; ?>";
		document.cookie="asset_name="+asset_name;
		document.cookie="asset_description="+asset_description;
		var url = rooturl+"/course/modedit.php?add="+asset_type+"&type=&course="+courseid+"&section=1&return=0&sr=0";
		location.href= url;*/
	});
	
	$('#asset_save').click(function(event){
		var asset_name = $('#asset_name').val();
		var asset_description = $('#asset_description').val();
		
		//var asset_type = $('#asset_type').val();
		<?php if($courseTypeId == $CFG->courseTypeClassroom ){?>
		     var asset_type = $('#resource_type').val();
		<?php }else{?>
		     var asset_type = $('#asset_type').val();
		<?php } ?>
		
		if(asset_name == ''){
			alert("Please enter asset name");
			return false;
		}
		if(asset_description == ''){
			alert("Please enter description");
			return false;
		}
		if(asset_type == ''){
			alert("Please select type");
			return false;
		}
		
		$("#asset_type").removeAttr("disabled");
		$('#asset_form').append('<input type = "hidden" name="quickedit" value="1">');
		$('#asset_form').submit();
		
	});
	
	$('a.delete').click(function(event){
	
		var status = $(this).attr('class');
		var message = '';
		if(status == "delete"){
			message = "<?php echo get_string('delete_record')?>";
		}
		var r = confirm(message);
		if (r == true) {
			return true;
		} else {
			return false;
		}
	});
	
	
});
</script>