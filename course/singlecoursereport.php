<?php

	/**
		* Custom module - Signle Course Report Page
		* Date Creation - 25/06/2014
		* Date Modification : 25/06/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
		
	
	require_once("../config.php");

	global $DB,$CFG,$USER;
	
	$sort    = optional_param('sort', 'COALESCE(NULLIF(mu.firstname, ""), mu.lastname), mu.lastname', PARAM_ALPHANUM);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 10, PARAM_INT);        // how many per page
	$cid     = required_param('cid', PARAM_INT);
	$export  = optional_param('action', '', PARAM_ALPHANUM);
	$back  = optional_param('back', '5', PARAM_ALPHANUM);
			  
	checkUserAccess('course',$cid);
	$paramArray = array(
	'cid' => $cid,
	'ch' => optional_param('ch', '', PARAM_ALPHA),
	'key' => optional_param('key', '', PARAM_RAW),
	'nm' => optional_param('nm', '', PARAM_ALPHANUM),
	'type' => optional_param('type', '', PARAM_RAW),
	'back' => $back,
  );
  
	
	$removeKeyArray = array('perpage');

	if($cid >0){
		$course = $DB->get_record('course',array('id'=> $cid));
		//$course = $DB->get_record('course',array('id'=> $cid), '*', MUST_EXIST);
	}else{
		redirect($CFG->wwwroot);
	}
	

	//$userrole =  getUserRole($USER->id);
	if( $USER->archetype == $CFG->userTypeStudent ) {
	//if(in_array($userrole, $CFG->customstudentroleid)){  // added by rajesh
	 redirect(new moodle_url('/my/learnerdashboard.php'));
	}
	
		
	//require_login(); 
	checkLogin();
	
	
	$PAGE->set_pagelayout('globaladmin');
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('reports','multicoursereport'));
	$PAGE->navbar->add(get_string('managecourses'), new moodle_url($CFG->wwwroot.'/course/index.php'));
	$PAGE->navbar->add(get_string('editcourse','course'), new moodle_url($CFG->wwwroot.'/course/edit.php',array('id'=>$cid)));
	$PAGE->navbar->add(get_string('reports','singlecoursereport'));
	
	
	$courseVsMultipleUserReport = getCourseVsMultipleUserReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
	$courseHTML = $courseVsMultipleUserReport->courseHTML;
	   
	echo $OUTPUT->header(); 
	echo $courseHTML;
	echo includeGraphFiles(get_string('overalluserprogressreport','singlecoursereport'));
	echo $OUTPUT->footer();	
