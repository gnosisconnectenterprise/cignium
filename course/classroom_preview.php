<?php

	/**
		* Custom module - Learning page
		* Date Creation - 04/05/2014
		* Date Modification : 23/06/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/

	require_once(dirname(__FILE__) . '/../config.php');
	require_once($CFG->dirroot.'/mod/scheduler/lib.php'); 
	
	global $USER;
	$userId = $USER->id;

    require_login();

    $sort         = optional_param('sort', 'id', PARAM_ALPHANUM);
    $dir          = optional_param('dir', 'DESC', PARAM_ALPHA);
    $page         = optional_param('page', 1, PARAM_INT);
    $perpage      = optional_param('perpage', 10, PARAM_INT);        // how many per page
	$id       = optional_param('id', 0, PARAM_INT);
	$classId       = optional_param('classId', 0, PARAM_INT);

	$courseTypeId = getCourseTypeIdByCourseId($id);
	
	$pageClassroomPreview = $CFG->pageClassroomPreview;
	
	if($id == 0){
		redirect($CFG->wwwroot);
	}
	
	if( $courseTypeId == $CFG->courseTypeOnline) {
	   redirect($CFG->wwwroot."/course/".$CFG->pageCourseLaunch."?id=$id"); 
	}

    $header = $SITE->fullname;
    $PAGE->set_title($header);
	$PAGE->set_heading($header);
	$PAGE->set_pagelayout('globaladmin');
	$PAGE->navbar->add(get_string('managecourses'), new moodle_url($CFG->wwwroot.'/course/index.php'));
	if($classId){
	  $PAGE->navbar->add(get_string('schedulingandtracking'), new moodle_url($CFG->wwwroot.'/mod/scheduler/list.php?id='.$id));
      $PAGE->navbar->add(get_string('preview'));
    }else{
	  $PAGE->navbar->add(get_string('preview'));
	}
    if (empty($CFG->loginhttps)) {
        $securewwwroot = $CFG->wwwroot;
    } else {
        $securewwwroot = str_replace('http:','https:',$CFG->wwwroot);
    }

    $returnurl = new moodle_url('/course/'.$CFG->pageClassroomPreview, array('id' => $id, 'sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'page'=>$page));

    echo $OUTPUT->header();

	 $query = "SELECT mc.* FROM {$CFG->prefix}course mc WHERE mc.id = '".$id."' AND mc.id NOT IN (1) AND mc.deleted = '0' ";
	 $query .= "  AND mc.coursetype_id = '".$courseTypeId."' ";
	 $courseArr = $DB->get_records_sql($query);
		 
	 $courseCnt = count($courseArr);
	 $baseurl = new moodle_url('/course/'.$pageClassroomPreview, array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'id' => $id));

	 $html = '';
	 $outerDivStart = "<div class='borderBlock'>";
	 $outerDivEnd = "</div>";

	 $headerHtml = getModuleHeaderHtml($courseArr[$id], $CFG->classroomModule);
	 $html .= $headerHtml;
				
	 $html .= $outerDivStart;
	 $htmlDiv = '';
	 $html .= html_writer::start_tag('div', array('class'=>''));
	 
	 if ($courseCnt > 0) {

            $i=0;
			//pr( $courseArr);die;
			
			foreach ($courseArr as $courses) {
			
				$i++;
				$courseformat = 'topics';
				$courseId = $courses->id;
		
				
				
				if($courseformat == 'topics'){	

				   // Asset Box Start						 
					$courseDiv = '';
						
					$sectionArr = getTopicsSection($courseId, $userId, '', $courseTypeId);
					
					$sectionHtml = '';
					
					//pr($sectionArr);die;
					if(count($sectionArr) > 0 ){
					
						foreach($sectionArr as $sectionId=>$sections){

								if(count($sections->courseModule) > 0){
								
								
								   //$sectionDiv .= '<div class="c-box" style="display:block !important;">';
								   $topicsCourseNew = array();
								   foreach($sections->courseModule as $cmId=>$courseModule){

									  $topicsCourse = $courseModule['topicCourse'];
									  //pr($topicsCourse);
									  if(count($topicsCourse)>0){
											   foreach($topicsCourse as $topics){
												 $topicsCourseNew[$topics->resource_type_id]['name'] = $topics->resource_type;
												 $topicsCourseNew[$topics->resource_type_id]['details'][] = $topics;
											   }
											 } // end if for topicsCourse
									  }
								
								}

								//$sectionHtml .= '</div>'; // end section html
							
						} // end sectionArr foreach
							
					} // end sectionArr if
					
					// Asset Box End
					
				}
								
								
					
			   //pr($topicsCourseNew);
	          
				// Asset Box Start
				
				$assetDiv = '';
				if(count($topicsCourseNew) > 0){
				
				   $assetDiv .= '<div class="d-box margin_top">';
				   $assetDiv .= '<div class="assetlabel" >'.get_string('assetlabel','learnercourse').': </div>';
				   $assetDiv .= '<div class="classroom-resource-box-outer" >';
				   $i = 0;
				   foreach($topicsCourseNew as $topicsCourseArr){
				   
					  $i++;
					  $resourceType = $topicsCourseArr['name'];
					  $resourceTypeDetails = $topicsCourseArr['details'];
					  
					  if(count($resourceTypeDetails) > 0 ){
					  
						$assetDiv .= '<div class="classroom-resource-box" ><table cellspacing="0" width="100%" cellpadding="0" border="0" class=""><tr>'; 
						$assetDiv .= '<td><!--('.$i.') -->'.$resourceType.' </td><td>'; 
						$rDetailsDiv = '';
						//pr($resourceTypeDetails);die();
						foreach($resourceTypeDetails as $rtdArr){
						
						   $resoursename = $rtdArr->resoursename;
						   $resourcesummary = strip_tags($rtdArr->resourcesummary);
						   $categoryName = $rtdArr->categoryname;
						   $allowStudentAccess = $topicsCourseArr->allow_student_access == 1?"Yes":'No';
						   $assetIconClass = '';
						   $cmid = $rtdArr->cmid;							   
						   if (!$cm = get_coursemodule_from_id('resource', $cmid)) {
								resource_redirect_if_migrated(0, $courseId);
								print_error('invalidcoursemodule');
						   }else{
									$resource = $DB->get_record('resource', array('id'=>$cm->instance));
									
									if(count($resource) > 0 ){
									
										$context = context_module::instance($cm->id);
										$fs = get_file_storage();
										$files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
							
										if (count($files) < 1) { 
											 $linkUrl = "<div class='classroom-resource-details'><a href='javascript:;' onclick='alert(\"".get_string('clamdeletedfile')."\")' title='".$resoursename."'   ><span class = '".$assetIconClass."' style='height:30px'></span>".$resoursename."</a> </div>";
										} else {
											 $file = reset($files);
											 unset($files);
											  $path = '/'.$context->id.'/mod_resource/content/'.$resource->revision.$file->get_filepath().$file->get_filename();
											  $resourseType = end(explode('.',$path));
											  $assetIconClass = getFileClass($resourseType);
											  $fullurl = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == RESOURCELIB_DISPLAY_DOWNLOAD);
											  $linkUrl = "<div class='classroom-resource-details'><a href='javascript:;' onclick=\"window.open('".$fullurl."', '', '".$CFG->FVPreviewWindowParameter."'); \" title='".$resoursename."'   ><span class = '".$assetIconClass."' style='height:30px'></span>".$resoursename."</a> </div>";
										}
									}
							}
														

							
							$rDetailsDiv .= $linkUrl;
						}
						$assetDiv .= $rDetailsDiv.'</td></tr></table></div>'; 
					  }
				   }
				   $assetDiv .= '</div>';
				   $assetDiv .= '</div>';
				}
	 
					            // Asset Box End
						
			    // Class Box Start Here  

				$classHtml = getClassesPreviewHtmlForUser($courseId, $classId);
				
				// Class Box End Here 
				
                $courseDiv .= $classHtml;
				$courseDiv .= $assetDiv;
				$htmlDiv = '<div class="borderBlockSpace">';
				$htmlDiv .= $courseDiv;
				$htmlDiv .= '</div>';
				$html .= $htmlDiv;
				
			}
			

	  }
	  
	  $html .= html_writer::end_tag('div');
	  $html .= $outerDivEnd;
	  echo $html;
      echo $OUTPUT->footer();
 
