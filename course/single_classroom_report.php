<?php

	/**
		* Custom module - Single Classroom Report Page
		* Date Creation - 17/11/2014
		* Date Modification : 17/11/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
		
	
	require_once("../config.php");
	require_once ($CFG->dirroot . '/mod/scheduler/lib.php');
    require_once ($CFG->dirroot . '/mod/scheduler/locallib.php');

	global $DB,$CFG,$USER;
	
	$sort    = optional_param('sort', 'COALESCE(NULLIF(mu.firstname, ""), mu.lastname), mu.lastname', PARAM_ALPHANUM);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 10, PARAM_INT);        // how many per page
	$cid     = required_param('cid', PARAM_INT);
	$class_id     = optional_param('class_id', 0, PARAM_INT);
	$export  = optional_param('action', '', PARAM_ALPHANUM);
	$type = optional_param('type', '-1', PARAM_RAW);
		
					  
	$paramArray = array(
	'cid' => $cid,
	'export' => $export,
	'class_id' => $class_id,
	'type' => $type,
  );
  
	
	$removeKeyArray = array('perpage');

	if($cid >0){
		$course = $DB->get_record('course',array('id'=> $cid));
		//$course = $DB->get_record('course',array('id'=> $cid), '*', MUST_EXIST);
	}else{
		redirect($CFG->wwwroot);
	}
	

	$haveAccess = haveClassroomCourseAccess($cid);
	if($haveAccess == 0){
	  redirect($CFG->wwwroot);
	}
	
		
	//require_login(); 
	checkLogin();
	
	
	$PAGE->set_pagelayout('globaladmin');
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('reports','multicoursereport'));
	$PAGE->navbar->add(get_string('managecourses'), new moodle_url($CFG->wwwroot.'/course/index.php'));
	$PAGE->navbar->add(get_string('editcourse','course'), new moodle_url($CFG->wwwroot.'/course/edit.php',array('id'=>$cid)));
	$PAGE->navbar->add(get_string('reports','classroomreport'));
	
	
	$courseVsMultipleUserReport = getClassroomVsMultipleUserReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
	$courseHTML = $courseVsMultipleUserReport->courseHTML;
	   
	echo $OUTPUT->header(); 
	echo $courseHTML;
	echo includeGraphFiles(get_string('overalluserprogressreport','classroomreport'));
    echo $OUTPUT->footer();
	