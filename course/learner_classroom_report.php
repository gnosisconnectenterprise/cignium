<?php

	/**
		* Custom module - Multi Course Report Page
		* Date Creation - 01/07/2014
		* Date Modification : 01/07/2014
		* Created By : Rajesh Kumar
		* Last Modified By : Rajesh Kumar
	*/
	

	require_once("../config.php");

	global $DB,$CFG,$USER;
	
		
	//require_login(); 
	checkLogin();
	
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 10, PARAM_INT);
	$userid = optional_param('userid', $USER->id, PARAM_INT);
	$back = optional_param('back', 1, PARAM_INT);
	$cType = optional_param('ctype', 2, PARAM_INT);
	$paramArray = array(
			'back' => $back,
			'userid' => $userid,
			'ctype' => $cType,			
			
			
	);
	require_once ($CFG->dirroot . '/mod/scheduler/lib.php');
	$PAGE->set_pagelayout('globaladmin');
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('courseusagesreport','classroomreport'));
	//$PAGE->navbar->add(get_string('reports','classroomreport'), new moodle_url($CFG->wwwroot.'/my/adminreportdashboard.php'));
	//$PAGE->navbar->add(get_string('reports','classroomreport'));
	//$PAGE->navbar->add(get_string('courseusagesreport','classroomreport'));
	
	if($USER->archetype == 'learner'){
	     $PAGE->navbar->add(get_string('reports','multiuserreport'));
		 $PAGE->navbar->add(get_string('classroomreport'));
	}else{
		if($back == 2){
		 $PAGE->navbar->add(get_string('reports','multiuserreport'));
		 $PAGE->navbar->add(get_string('usereperformancereport','multiuserreport'), new moodle_url($CFG->wwwroot.'/user/multiuser_report.php'));
		 //$PAGE->navbar->add(get_string('viewreportdetails','multiuserreport'));
		}elseif($back == 3){
		  $PAGE->navbar->add(get_string('reports','multiuserreport'));
		  $PAGE->navbar->add(get_string('userreport','multiuserreport'), new moodle_url($CFG->wwwroot.'/user/user_report.php'));
		}elseif($back == 1){
		  $PAGE->navbar->add(get_string('manage_user'),'/admin/user.php');
		  //$PAGE->navbar->add(get_string('viewuserreports','singlereport'));
		}
		
		$PAGE->navbar->add(getUsers($userid));	
	}


	echo $OUTPUT->header(); 
	$completedClass = 0;
	$incompleteClass = 0;
	$offset = $page - 1;
	$offset = $offset*$perpage;
	$limit = '';
	if($perpage != 0){
		$limit = " LIMIT $offset, $perpage";
	}
	$removeKeyArray = array();
	$pageURL = $_SERVER['PHP_SELF'];
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
	
	
	$sql = "SELECT s.id,s.name as classname,se.is_completed,c.fullname as coursename from {$CFG->prefix}course AS c LEFT JOIN {$CFG->prefix}scheduler AS s ON c.id=s.course
	LEFT JOIN {$CFG->prefix}scheduler_enrollment AS se ON s.id=se.scheduler_id WHERE se.userid=".$userid." AND se.is_approved='1' order by s.id,s.name";
	
	$sqllimit = $sql.$limit;
	
	$classcount_rec = $DB->get_records_sql($sql);
	$classcount = count($classcount_rec);
	$classes_data = $DB->get_records_sql($sqllimit);
	
	foreach($classes_data as $class_data){
		if($class_data->is_completed){
			$completedClass++;
		}else{
			$incompleteClass++;
		}
	}
	$totalClasses = $completedClass+$incompleteClass;
	$completedClassPers = round(($completedClass*100)/$totalClasses);
	$incompleteClassPers = round(($incompleteClass*100)/$totalClasses);
	$courseHTML = '';
	$genPrintURL = $CFG->wwwroot.'/course/learner_classroom_report_print.php?userid='.$userid;
	$courseHTML .= '<div class="clear"></div>';
	if($USER->archetype != 'learner'){
	    $cSelected1 = $cType == 1?"selected='selected'":"";
		$cSelected2 = $cType == 2?"selected='selected'":"";
		$courseHTML .= '<select name="" backto="'.$back.'" id="reporttype" rel="'.$userid.'" class="reportTypeSelect" ><option value="'.$CFG->courseTypeOnline.'" '.$cSelected1.'>'.get_string('onlinecourse').'</option><option value="'.$CFG->courseTypeClassroom.'" '.$cSelected2.' >'.get_string('classroomcourse').'</option></select>';
	
	}
	
	$style = ($back == 2)?:'';
	$courseHTML .= "<div class='tabsOuter borderBlockSpace' ".$style." >";
			
	if($USER->archetype != 'learner'){
		
		if($back == 1){
		
			$user->id = $userid;
			ob_start();
			include_once($CFG->dirroot.'/user/user_tabs.php');
			$HTMLTabs .= ob_get_contents();
			ob_end_clean();
			$courseHTML .= $HTMLTabs;
			
		}
	}
	$courseHTML .= '<div class="">';
	
	if($totalClasses>0){
		$exportHTML .= '<div class="exports_opt_box"> ';
		$exportHTML .= '<a class="csv_icon" id="exportcsv" title="'.get_string('downloadcsv','singlereport').'" href="'.$genPrintURL.'&action=exportcsv">'.get_string('downloadcsv','singlereport').'</a> ';
		$exportHTML .= '<span class="seperater">&nbsp;</span>';
		$exportHTML .= '<a class="pdf_icon" title="'.get_string('downloadpdf','singlereport').'" id="exportpdf1" href="'.$genPrintURL.'&action=exportpdf">'.get_string('downloadpdf','singlereport').'</a> ';
		$exportHTML .= '<span class="seperater">&nbsp;</span>';
		$exportHTML .= '<a rel="'.$genPrintURL.'&action=print" href="javascript: void(0);" id="printbun" title="'.get_string('print','singlereport').'" class="print_icon">'.get_string('print','singlereport').'</a>';
		$exportHTML .= '</div>';
		
		  $courseHTML .= ' <div class = "single-report-start">
						<div class = "single-report-graph-box"> <span class="main-heading">'.get_string('overallclassroomreport','scheduler').$exportHTML.'</span>';
					$courseHTML .= '<div class="single-report-graph-left" id="chartContainer">'.get_string('loading','classroomreport').'</div>
						  <div class="single-report-graph-right" style="margin-bottom: 25px;">
							<div class="course-count-heading">'.get_string('numberofclasses','scheduler').'</div>
							<div class="course-count">'.$totalClasses.'</div>
							<div class="seperator">&nbsp;</div>
							
							<div class="seperator">&nbsp;</div>
							<div class="course-status">
							  <div class="inprogress"><h6>'.get_string('inprogress','classroomreport').'</h6><span>'.$incompleteClass.'</span></div>
							  <div class="clear"></div>
							  <div class="completed"><h6>'.get_string('completed','classroomreport').'</h6><span>'.$completedClass.'</span></div>
							</div>
						  </div>
						</div>
					  </div>
					  <div class="clear"></div>';
	}
	$courseHTML .= '<div class="clear"></div>';
	$courseHTML .= '<div class="">'; 
	$courseHTML .= '<table class = "generaltable" id="table1"><tr class=""><th  width="40%">Title</th>';
	$courseHTML .= "<th width = '20%' align='align_left' >" . get_string ( 'attended', 'scheduler' ) . "</th>";
	$courseHTML .= "<th width = '19%' align='align_left' >" . get_string ( 'grade', 'scheduler' ) . "</th>";
	$courseHTML .= "<th width = '11%' align='align_left' >" . get_string ( 'score', 'scheduler' ) . "</th>";
	$courseHTML .= "<th width = '10%' align='align_left' >" . get_string ( 'classcompletedstatus', 'scheduler' ) . "</th>";	
	$courseHTML .= '</tr>';	
	if (! $classes_data) {
		$courseHTML .= '<tr><td colspan="5">'.get_string('norecordfound','scheduler').'</td></tr>';
	}else{
	
	    $sessionListArr = $DB->get_records_sql ("SELECT msc.*, msa.attended, msa.grade, msa.score, msa.studentid FROM mdl_scheduler_slots msc LEFT JOIN mdl_scheduler_appointment msa ON (msc.id = msa.slotid AND msa.studentid = '".$userid."') WHERE 1 = 1");
		$sessionList = array();
		if(count($sessionListArr) > 0 ){
		  foreach($sessionListArr as $arr){
		    $sessionList[$arr->schedulerid][] = $arr;
		  }
		}
		//pr($sessionList);die;
		foreach ( $classes_data as $class_rec ) {				

			$courseHTML .= "<tr class='evenR'><td class='align_left'  width = '14%'><span class='f-left'>" . $class_rec->coursename." - ". $class_rec->classname . "</span></td>";
			$courseHTML .= "<td></td>";
			$courseHTML .= "<td></td>";
			$courseHTML .= "<td></td>";
			$courseHTML .= '<td>'.($class_rec->is_completed == 1?get_string( 'classcompleted', 'scheduler' ):get_string('classincompleted', 'scheduler')).'</td>';
			$courseHTML .= "</tr>";			
	
			//$session_list = $sessionListArr ($class_rec->id,'classid');
			//pr($sessionList[$class_rec->id]);die;
			$courseHTML .= "<tr><td colspan='5' class='borderLeftRight'><div class='a-box'>";
			$i=0;
			if(isset($sessionList[$class_rec->id])){
				foreach ( $sessionList[$class_rec->id] as $session ) {
					$class = $i%2!=0?'even':'odd';
					$i++;
					$courseHTML .= "<div class='".$class."' ><div style='width:40%'>" . $session->sessionname . "</div>";
					$courseHTML .= "<div style='width:20%'>".($session->attended == 1?'Yes':'No')."</div>";
					$courseHTML .= "<div style='width:19%'>".($session->grade!=''?$session->grade:getMDash())."</div>";
					$courseHTML .= "<div style='width:11%'>".($session->score!=''?$session->score:getMDash())."</div><div  style='width:10%'></div>";
					$courseHTML .= "</div>";			
					
				}
			}else{
				$courseHTML .= get_string('norecordfound','scheduler');
			}
			$courseHTML .= "</div></td></tr>";			
		
		}
	}
			//	$courseHTML .= "<tr><td colspan='4'><input type='submit' name='submit' value='Save & Continue' /></td></tr>";
		
	
		$courseHTML .= "</table>";	
	$courseHTML .="</div></div></div>";
	// teacher side
	
	if (! empty ( $courseHTML )) {
		
	
		echo html_writer::start_tag ( 'div', array () );		
			$courseHTML .=  '<script>	$(document).ready(function(){
												
											$("#reporttype").change(function(){
												var userid = $(this).attr("rel");
												var backto = $(this).attr("backto");
												if(backto == 2){
												  url = "'.$CFG->wwwroot.'/user/multiuser_report_details.php?uid="+userid+"&course=1";			
												}else if(backto == 1){
												  url = "'.$CFG->wwwroot.'/user/single_report.php?uid="+userid;		
												}else if(backto == 3){
												  url = "'.$CFG->wwwroot.'/user/multiuser_report_details.php?uid="+userid+"&back=3";		
												}
												window.location.href = url;
											});
												
					  						$("#printbun").bind("click", function(e) {
											var url = $(this).attr("rel");
											window.open(url, "'.get_string('viewuserreports','singlereport').'", "'.$CFG->printWindowParameter.'");
											});	
										});';
		 	$courseHTML .=  ' window.onload = function () {
				
											var chart = new CanvasJS.Chart("chartContainer",
											{
												title:{
													text: ""
												},
												theme: "theme4",
												data: [
												{
													type: "doughnut",
													indexLabelFontFamily: "Arial",
													indexLabelFontSize: 12,
													startAngle:0,
													indexLabelFontColor: "dimgrey",
													indexLabelLineColor: "darkgrey",
													toolTipContent: "{y}%",
						
													dataPoints: [
													{  y: '.$incompleteClassPers.', label: "'.get_string('inprogress','classroomreport').' ('.$incompleteClass.')", exploded: "" },
													{  y: '.$completedClassPers.', label: "'.get_string('completed','classroomreport').' ('.$completedClass.')", exploded: "" },
													
						
													]
			
												
			
			
												}
												]
											}); ';
				
			
				$courseHTML .=  '	chart.render(); ';
		
			
		$courseHTML .=  '  }'; 
		$courseHTML .=  '</script><script type="text/javascript" src="'.$CFG->wwwroot.'/charts/canvaschart/canvasjs.min.js"></script>';
		if(count($classes_data) > 0) {
			
			
			$courseHTML .= paging_bar($classcount, $page, $perpage, $genURL);
			
			if($USER->archetype == 'learner'){
			}else{
				if($back == 2){
				  $courseHTML .= '<input type = "button" value = "'.get_string('back').'" onclick="location.href=\''.$CFG->wwwroot.'/user/multiuser_report.php\';">';			}elseif($back == 3){
				  $courseHTML .= '<input type = "button" value = "'.get_string('back').'" onclick="location.href=\''.$CFG->wwwroot.'/user/user_report.php\';">';			
				}elseif($back == 1){
				  $courseHTML .= '<input type = "button" value = "'.get_string('back').'" onclick="location.href=\''.$CFG->wwwroot.'/admin/user.php\';">';	
				}
				
				$PAGE->navbar->add(getUsers($userid));	
			}
	
			
		}
		echo $courseHTML;
	
		
		//echo html_writer::end_tag ( 'div' );
		//echo html_writer::end_tag ( 'div' );
	
		// Pring paging bar

	}


	//echo $courseHTML;
	echo $OUTPUT->footer();

?>