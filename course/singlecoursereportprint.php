<?php

/**
	* Custom module - Multi Course Report Print Page
	* Date Creation - 10/07/2014
	* Date Modification : 10/07/2014
	* Created By : Rajesh Kumar
	* Last Modified By : Rajesh Kumar
*/
	

	require_once("../config.php");
	global $DB,$CFG,$USER;
	
	$sort    = optional_param('sort', 'COALESCE(NULLIF(mu.firstname, ""), mu.lastname), mu.lastname', PARAM_ALPHANUM);
	$dir     = optional_param('dir', 'ASC', PARAM_ALPHA);
	$page    = optional_param('page', 1, PARAM_INT);
	$perpage = optional_param('perpage', 0, PARAM_INT);        // how many per page
	$cid     = required_param('cid', PARAM_INT);
	$export  = optional_param('action', '', PARAM_ALPHANUM);
	
	$sDate = '';
	$eDate = '';
		
		
	$sStartDate     = optional_param('startDate', $sDate , PARAM_RAW);
	$sEndDate      = optional_param('endDate', $eDate, PARAM_RAW);
	
	
	$paramArray = array(
	'cid' => $cid,
	'ch' => optional_param('ch', '', PARAM_ALPHA),
	'key' => optional_param('key', '', PARAM_RAW),
	'nm' => optional_param('nm', '', PARAM_ALPHANUM),
	'type' => optional_param('type', '', PARAM_RAW),
	'startDate' => $sStartDate,
	'endDate' => $sEndDate
    );
  
	
	$removeKeyArray = array('perpage');

	if($cid >0){
		$course = $DB->get_record('course',array('id'=> $cid));
	}else{
		redirect($CFG->wwwroot);
	}
	
	$userRole =  getUserRole($USER->id);
	if( $USER->archetype == $CFG->userTypeStudent ) {
		$groupCourses = fetchGroupsList($USER->id);
		if(empty($groupCourses))
		{
			redirect(new moodle_url('/my/learnerdashboard.php'));
		}
	}
	
		
	//require_login(); 
	checkLogin();
	
	
	$PAGE->set_pagelayout('print');
	$PAGE->set_heading($SITE->fullname);
	$PAGE->set_title($SITE->fullname.": ".get_string('overalluserprogressreport','singlecoursereport'));

	
   
    $courseHTML = '';
	$reportContentCSV = '';
	$reportContentPDF = '';
    if($export && in_array($export, array('exportcsv', 'exportpdf', 'print'))){

		$courseVsMultipleUserReport = getCourseVsMultipleUserReport($paramArray, $removeKeyArray, $sort, $dir, $page, $perpage, $export);
		$courseHTML = $courseVsMultipleUserReport->courseHTML;
		$reportContentCSV = $courseVsMultipleUserReport->reportContentCSV;
		$reportContentPDF = $courseVsMultipleUserReport->reportContentPDF;
	
    }else{
	    redirect(new moodle_url('/'));
	}

	/* bof export to csv */
	
    if(isset($export) && $export == 'exportcsv') {
	
		$filepath = $CFG->dirroot."/local/reportexport/temp";		
		chmod($filepath, 0777);		
		$filename = str_replace(' ', '_', get_string('overalluserprogressreport','singlecoursereport'))."_".date("m-d-Y").".csv";  
		$filepathname = $filepath.'/'.$filename;
		unlink($filepathname);
		$handler = fopen($filepathname, "w");
		fwrite($handler, $reportContentCSV);
   	    exportCSV($filepathname);
	}
	/* eof export to csv */	
	
	
	/* bof export to pdf */
    if(isset($export) && $export == 'exportpdf') {
			
		$filename = str_replace(' ', '_', get_string('overalluserprogressreport','singlecoursereport'))."_".date("m-d-Y").".pdf";
		exportPDF($filename, $reportContentPDF, '', get_string('overalluserprogressreport','singlecoursereport'));
		
	}
	/* eof export to pdf */	
		
	   
	echo $OUTPUT->header(); 
	echo $courseHTML;

?>
<style>
#page { margin: 20px auto 0;}
</style>
