<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Admin-only code to delete a course utterly.
 *
 * @package core_course
 * @copyright 2002 onwards Martin Dougiamas (http://dougiamas.com)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../config.php');
require_once($CFG->dirroot . '/course/lib.php');

$id = required_param('id', PARAM_INT); // Course ID.
$delete = optional_param('delete', '', PARAM_ALPHANUM); // Confirmation hash.

$course = $DB->get_record('course', array('id' => $id), '*', MUST_EXIST);
$coursecontext = context_course::instance($course->id);
$paramArray = array(
		'ch' => optional_param('ch', '', PARAM_ALPHA),
		'key' => optional_param('key', '', PARAM_RAW),
		
		'nm' => optional_param('nm', '', PARAM_ALPHANUM),
		'desc' => optional_param('desc', '', PARAM_ALPHANUM),
		'tag' => optional_param('tag', '', PARAM_ALPHANUM),
		'cnm' => optional_param('cnm', '', PARAM_ALPHANUM),
		'criteria' => optional_param('criteria', '', PARAM_RAW),
		'course_type' => optional_param('course_type', '', PARAM_RAW),
		'course_cat' => optional_param('course_cat', '', PARAM_RAW),
		'course_publish' => optional_param('course_publish', '', PARAM_RAW),
		'is_elective_global' => optional_param('is_elective_global', '', PARAM_RAW),
		'is_active' => optional_param('is_active', '', PARAM_RAW)
);

$pageParamArray = array(
		'page' => optional_param('page', '', PARAM_ALPHANUM),
		'perpage' => optional_param('perpage', '', PARAM_ALPHANUM)
);
$paramArray = array_merge($paramArray, $pageParamArray);
$pageURL = '/course/index.php';
$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
require_login();

//$userrole =  getUserRole($USER->id);

if ($SITE->id == $course->id || !can_delete_course($id)) {
    // Can not delete frontpage or don't have permission to delete the course.
    print_error('cannotdeletecourse');
}

GLOBAL $USER,$CFG;
/* Block to check if the course is already published starts here by bhavana */
	if($course->publish == 1){
		redirect($CFG->wwwroot.'/course/index.php');
	}
	if($USER->archetype == $CFG->userTypeManager){
		if($course->createdby != $USER->id){
			redirect($CFG->wwwroot.'/course/index.php');
		}
	}
/* Block to check if the course is already published ends here by bhavana */

$categorycontext = context_coursecat::instance($course->category);
$PAGE->set_url('/course/delete.php', array('id' => $id));
$PAGE->set_context($categorycontext);
$PAGE->set_pagelayout('admin');
navigation_node::override_active_url(new moodle_url('/course/management.php', array('categoryid'=>$course->category)));

$courseshortname = format_string($course->shortname, true, array('context' => $coursecontext));
$coursefullname = format_string($course->fullname, true, array('context' => $coursecontext));

if($USER->archetype == $CFG->userTypeAdmin) {
//if(in_array($userrole, $CFG->custommanagerroleid)){  // added by rajesh 
  $categoryurl = new moodle_url('/course/index.php');
}else{
  $categoryurl = new moodle_url('/course/management.php', array('categoryid' => $course->category));
}

// Check if we've got confirmation.
if ($delete === md5($course->timemodified)) {
    // We do - time to delete the course.
    require_sesskey();

    $strdeletingcourse = get_string("deletingcourse", "", $courseshortname);

    $PAGE->navbar->add($strdeletingcourse);
    $PAGE->set_title("$SITE->shortname: $strdeletingcourse");
    $PAGE->set_heading($SITE->fullname);

   // echo $OUTPUT->header();
  //  echo $OUTPUT->heading($strdeletingcourse);
 
    // We do this here because it spits out feedback as it goes.
    delete_course($course);
    $_SESSION['update_msg']=get_string('record_deleted');
    // Update course count in categories.
    fix_course_sortorder();
	redirect($genURL);
	echo $OUTPUT->heading( get_string("deletedcourse", "", $courseshortname) );
	echo $OUTPUT->continue_button($categoryurl);
	echo $OUTPUT->footer();
	exit; // We must exit here!!!
	
}

$strdeletecheck = get_string("deletecheck", "", $courseshortname);
$strdeletecoursecheck = get_string("deletecoursecheck");
$message = "{$strdeletecoursecheck} '{$coursefullname} ({$courseshortname})' ?";

$continueurl = new moodle_url('/course/delete.php', array('id' => $course->id, 'delete' => md5($course->timemodified)));

//$PAGE->navbar->add($strdeletecheck);
$PAGE->set_title("$SITE->shortname: $strdeletecheck");
$PAGE->set_heading($SITE->fullname);
echo $OUTPUT->header();
echo $OUTPUT->confirm($message, $continueurl, $categoryurl);
echo $OUTPUT->footer();
exit;