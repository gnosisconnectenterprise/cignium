<?php 
        $courseId = $course->id;
        $allocationType  = getProgramCourseAllocationType($courseId, $CFG->courseType );
		
		$radioarray=array();
		$attributes = ''; 
		$radioarray[] =& $mform->createElement('radio', 'allocation_type', '', get_string('needrequest', 'course'), 1, $attributes);
		$radioarray[] =& $mform->createElement('radio', 'allocation_type', '', get_string('selfallocation', 'course'), 2, $attributes);
		$mform->addGroup($radioarray, 'allocation_type_radioar', '', array(' '), false);
		$mform->setDefault('allocation_type', $allocationType);
		
		    
			$courses = new courses_assignment_selector('', array('courseid' => $courseId));
			$courses->courseTeamList();
			$courses->courseNonTeamList();
			
			$courses->courseUserList();
			$courses->courseNonUserList();
			
			$courses->courseDepartmentList();
			$courses->courseNonDepartmentList();
		
			//$block IS 'team_course'
			//$block IS 'non_team_course'
			//$block IS 'department_course'
			//$block IS 'non_department_course'
			//$block IS 'user_course'
			//$block IS 'non_user_course'
			
			$display = '';
			if($USER->archetype != $CFG->userTypeAdmin){
				$display = 'style = "display:none"';
			}
			
			$mform->addElement('html', '<div class="self-allocation-box">');
			$mform->addElement('html', '<input type = "hidden" name = "formsubmit" value = "1">');
			$mform->addElement('html', '<fieldset class="" id="yui_3_13_0_2_1404228340264_14">');
			$mform->addElement('html', '<div class="msg-tab-html">
					<!--<div class="msg-tab-label">'.get_string('notify_to','messages').'</div>-->
					<div class="msg-tab-block">
						<div class="tab-msg-all">
							<div class="msg-tab-block" style="float:left;padding:1%;">
								<input type="checkbox" name="department_all" id="department_all"  > All
							</div>
						</div>
					<div class="tab-box">');
					
			$activeLink = 'activeLink';
			
			if($USER->archetype == $CFG->userTypeAdmin){
				$activeLink = '';
				$mform->addElement('html', '<a href="javascript:;" class="tabLink activeLink" id="cont-1" '.$display.'>'.get_string('departments').'</a>');
			} 
			
			$mform->addElement('html', '<a href="javascript:;" class="tabLink '.$activeLink.'" id="cont-2">'.get_string('teams').'</a> 
						<a href="javascript:;" class="tabLink " id="cont-3">'.get_string('users').'</a> ');
			$mform->addElement('html', '</div>');
			 
			$hide = '';
			if($USER->archetype == $CFG->userTypeAdmin){
				$mform->addElement('html', '<div class="tabcontent paddingAll" id="cont-1-1"><div class="departments">');
				$hide = 'hide';
			
			
			 $mform->addElement('html', '
			  <div id="addmembersform">
				<div>
				<input type="hidden" name="sesskey" value="'.sesskey().'" />
			
				<table class="generaltable generalbox groupmanagementtable boxaligncenter" summary="">
				<tr>
				 <td id="potentialcell">
					  <p>
						<label for="addselect">'.get_string('coursenondepartment').'</label>
					  </p>
					  <div id="department_select_wrapper" class="userselector">
						<select multiple="multiple" name = "department_course[]" size = "20">
							'.$courses->course_option_list('non_department_course', true).'
						</select>

					  </div>
				  </td>
				  <td id="buttonscell">
					<p class="arrow_button">
						<input name="add" id="department_add" type="button" value="'.$OUTPUT->larrow().'&nbsp;'.get_string('add').'" title="'.get_string('add').'" disabled = ""/>
						<input name="remove" id="department_remove" type="button" value="'.get_string('remove').'&nbsp;'.$OUTPUT->rarrow().'" title="'.get_string('remove').'" disabled = ""/>
					</p>
				  </td>
				   <td id="existingcell">
					  <p>
						<label for="removeselect">'.get_string('coursedepartment').'</label>
					  </p>
					  <div id="department_remove_wrapper" class="userselector">
						<select multiple="multiple" name = "add_department_course[]" size = "20">
							'.$courses->course_option_list('department_course', true).'
						</select>

					  </div>
					</td>
				</tr>
				</table>
				</div>
			</div></div></div>');
			}
			
			$mform->addElement('html', '<div class="tabcontent paddingAll '.$hide.'" id="cont-2-1">');
			$mform->addElement('html', '  
			<div id="addmembersform">
				<div>
				<input type="hidden" name="sesskey" value="'.sesskey().'" />
			
				<table class="generaltable generalbox groupmanagementtable boxaligncenter" summary="">
				<tr>
				 <td id="potentialcell">
					  <p>
						<label for="addselect">'.get_string('coursenonteams').'</label>
					  </p>
					  <div id="team_addselect_wrapper" class="userselector">
						<select multiple="multiple" name = "addteam[]" size = "20">
							'.$courses->course_option_list('non_team_course', true).'
						</select>

					  </div>
				  </td>
				  <td id="buttonscell">
					<p class="arrow_button">
						<input name="add" id="team_add" type="button" value="'.$OUTPUT->larrow().'&nbsp;'.get_string('add').'" title="'.get_string('add').'" disabled = ""/>
						<input name="remove" id="team_remove" type="button" value="'.get_string('remove').'&nbsp;'.$OUTPUT->rarrow().'" title="'.get_string('remove').'" disabled = ""/>
					</p>
				  </td>
				   <td id="existingcell">
					  <p>
						<label for="removeselect">'.get_string('courseteams').'</label>
					  </p>
					  <div id="team_removeselect_wrapper" class="userselector">
						<select multiple="multiple" name = "addteamcourses[]" size = "20">
							'.$courses->course_option_list('team_course', true).'
						</select>
	
					  </div>
					</td>
				</tr>
				</table>
				</div>
			</div>');


			$mform->addElement('html', '</div><div class="tabcontent paddingAll hide" id="cont-3-1">');
			 
			 $mform->addElement('html', '<div id="addmembersform">
				<div>
				<input type="hidden" name="sesskey" value="'.sesskey().'" />
			
				<table class="generaltable generalbox groupmanagementtable boxaligncenter" summary="">
				<tr>
				 <td id="potentialcell">
					  <p>
						<label for="addselect">'.get_string('coursenonuser').'</label>
					  </p>
					  <div id="user_addselect_wrapper" class="userselector">
						<select multiple="multiple" name = "adduser[]" size = "20">
							'.$courses->course_option_list('non_user_course', true).'
						</select>
	
					  </div>
				  </td>
				  <td id="buttonscell">
					<p class="arrow_button">
						<input name="add" id="user_add" type="button" value="'.$OUTPUT->larrow().'&nbsp;'.get_string('add').'" title="'.get_string('add').'" disabled = ""/>
						<input name="remove" id="user_remove" type="button" value="'.get_string('remove').'&nbsp;'.$OUTPUT->rarrow().'" title="'.get_string('remove').'" disabled = ""/>
					</p>
				  </td>
				   <td id="existingcell">
					  <p>
						<label for="removeselect">'.get_string('courseuser').'</label>
					  </p>
					  <div id="user_removeselect_wrapper" class="userselector">
						<select multiple="multiple" name = "addusercourse[]" size = "20">
							'.$courses->course_option_list('user_course', true).'
						</select>
	
					  </div>
					</td>
				</tr>
				</table>
				</div>
				
			</div>');
			$mform->addElement('html', '</div></div></div>');
			
			$mform->addElement('html', '</fieldset>');
			$mform->addElement('html', '<div>');
			
			