<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Edit course settings
 *
 * @package    core_course
 * @copyright  1999 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../config.php');
require_once('lib.php');
require_once('edit_form.php');

$id = optional_param('id', 0, PARAM_INT); // Course id.
$categoryid = optional_param('category', 1, PARAM_INT); // Course category - can be changed in edit form.
$returnto = optional_param('returnto', 0, PARAM_ALPHANUM); // Generic navigation return page switch.
//$userrole =  getUserRole($USER->id);

$courseTypeId = getCourseTypeIdByCourseId($id);
//echo $USER->id;
$courseCreatedBy = getCourseFieldByCourseId($id, 'createdby');
/*if($courseTypeId == $CFG->courseTypeClassroom && $courseCreatedBy != $USER->id && ($USER->archetype == $CFG->userTypeManager || $USER->archetype == $CFG->userTypeStudent)){
  
   if($USER->archetype == $CFG->userTypeStudent){
       $allTypeOfUsersOfClassroom = getAllTypeOfUsersOfClassroom($id, 2);
	   if(isset($allTypeOfUsersOfClassroom['primary_instructor']) && in_array($USER->id, $allTypeOfUsersOfClassroom['primary_instructor'])){
	      redirect($CFG->wwwroot."/course/assetsview.php?id=".$id);
	   }elseif(isset($allTypeOfUsersOfClassroom['createdby']) && in_array($USER->id, $allTypeOfUsersOfClassroom['createdby'])){
	      redirect($CFG->wwwroot."/course/assetsview.php?id=".$id);
	   }elseif(isset($allTypeOfUsersOfClassroom['secondary_instructor']) && in_array($USER->id, $allTypeOfUsersOfClassroom['secondary_instructor'])){
	      redirect($CFG->wwwroot."/mod/scheduler/list.php?id=".$id);
	   }
   }
   
   redirect($CFG->wwwroot."/course/assetsview.php?id=".$id);
}*/

require_login();
$PAGE->set_pagelayout('admin');
$pageparams = array('id'=>$id);
if (empty($id)) {
    $pageparams = array('category'=>$categoryid);
}

checkUserAccess('course',$id);
$PAGE->set_url('/course/edit.php', $pageparams);
$site = get_site();
$PAGE->set_heading($site->fullname);


	
// Basic access control checks.
if ($id) {
    // Editing course.
    if ($id == SITEID){
        // Don't allow editing of  'site course' using this from.
        print_error('cannoteditsiteform');
    }

    // Login to the course and retrieve also all fields defined by course format.
    $course = get_course($id);
    require_login($course);
    $course = course_get_format($course)->get_course();

    $category = $DB->get_record('course_categories', array('id'=>$course->category), '*', MUST_EXIST);
    $coursecontext = context_course::instance($course->id);
    require_capability('moodle/course:update', $coursecontext);

} else if ($categoryid) {
    // Creating new course in this category.
    $course = null;
    require_login();
    $category = $DB->get_record('course_categories', array('id'=>$categoryid), '*', MUST_EXIST);
    $catcontext = context_coursecat::instance($category->id);
    require_capability('moodle/course:create', $catcontext);
    $PAGE->set_context($catcontext);

} else {
   require_login();
   print_error('needcoursecategroyid');
}

// Prepare course and the editor.
$editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES, 'maxbytes'=>$CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true);
$overviewfilesoptions = course_overviewfiles_options($course);
if (!empty($course)) {
    // Add context for editor.
    $editoroptions['context'] = $coursecontext;
    $editoroptions['subdirs'] = file_area_contains_subdirs($coursecontext, 'course', 'summary', 0);
    $course = file_prepare_standard_editor($course, 'classroom_instruction', $editoroptions, $coursecontext, 'course', 'classroom_instruction', 0);
	$course = file_prepare_standard_editor($course, 'summary', $editoroptions, $coursecontext, 'course', 'summary', 0);
	$course = file_prepare_standard_editor($course, 'suggesteduse', $editoroptions, $coursecontext, 'course', 'suggesteduse', 0);
	$course = file_prepare_standard_editor($course, 'learningobj', $editoroptions, $coursecontext, 'course', 'learningobj', 0);
	$course = file_prepare_standard_editor($course, 'performanceout', $editoroptions, $coursecontext, 'course', 'performanceout', 0);
        //$course = file_prepare_standard_editor($course, 'courseemail', $editoroptions, $coursecontext, 'course', 'courseemail', 0);
    if ($overviewfilesoptions) {
        file_prepare_standard_filemanager($course, 'overviewfiles', $overviewfilesoptions, $coursecontext, 'course', 'overviewfiles', 0);
    }

    // Inject current aliases.
    $aliases = $DB->get_records('role_names', array('contextid'=>$coursecontext->id));
    foreach($aliases as $alias) {
        $course->{'role_'.$alias->roleid} = $alias->name;
    }
	$imgDataExists = $DB->get_field_sql("select course_image from mdl_course_image where course_id = ".$course->id);
	$course->course_image = $imgDataExists;
} else {
    // Editor should respect category context if course context is not set.
    $editoroptions['context'] = $catcontext;
    $editoroptions['subdirs'] = 0;
	$course = file_prepare_standard_editor($course, 'classroom_instruction', $editoroptions, null, 'course', 'classroom_instruction', null);
    $course = file_prepare_standard_editor($course, 'summary', $editoroptions, null, 'course', 'summary', null);
	$course = file_prepare_standard_editor($course, 'suggesteduse', $editoroptions, null, 'course', 'suggesteduse', null);
	$course = file_prepare_standard_editor($course, 'learningobj', $editoroptions, null, 'course', 'learningobj', null);
	$course = file_prepare_standard_editor($course, 'performanceout', $editoroptions, null, 'course', 'performanceout', null);
       // $course = file_prepare_standard_editor($course, 'courseemail', $editoroptions, null, 'course', 'courseemail', null);
    if ($overviewfilesoptions) {
        file_prepare_standard_filemanager($course, 'overviewfiles', $overviewfilesoptions, null, 'course', 'overviewfiles', 0);
    }
}

// First create the form.
$editform = new course_edit_form(NULL, array('course'=>$course, 'category'=>$category, 'editoroptions'=>$editoroptions, 'returnto'=>$returnto));

if ($editform->is_cancelled()) {
	   // hack cancel url when user role is manager . added by rajesh 
	   if( $USER->archetype == $CFG->userTypeAdmin ) {
		 //if(in_array($userrole, $CFG->custommanagerroleid)){
		 if (!empty($course->id)) {
			$url = new moodle_url($CFG->wwwroot.'/course/courseview.php', array('id'=>$course->id));
		 } else {
			$url = new moodle_url($CFG->wwwroot.'/course/index.php');
		 }
		 redirect($url);
	   }else{

			switch ($returnto) {
				case 'category':
					$url = new moodle_url($CFG->wwwroot.'/course/index.php', array('categoryid' => $categoryid));
					break;
				case 'catmanage':
					$url = new moodle_url($CFG->wwwroot.'/course/management.php', array('categoryid' => $categoryid));
					break;
				case 'topcatmanage':
					$url = new moodle_url($CFG->wwwroot.'/course/management.php');
					break;
				case 'topcat':
					$url = new moodle_url($CFG->wwwroot.'/course/index.php');
					break;
				default:
				
					if (!empty($course->id)) {
						$url = new moodle_url($CFG->wwwroot.'/course/courseview.php', array('id'=>$course->id));
					} else {
						$url = new moodle_url($CFG->wwwroot.'/course/index.php');
					}
					break;
			}
		
	    }	
		$url = new moodle_url($CFG->wwwroot.'/course/index.php');
        redirect($url);

} else if ($data = $editform->get_data()) {
	/*if(isset($data->publish) && $data->publish == 1){
		$data->publish = 1;
	}else{
		$data->publish = 0;
	}*/

	if (empty($course->id))
		$data->publish = 0;

    // Process data if submitted.
    if (empty($course->id)) {
		global $DB;
                
                // In creating the course.
        $course = create_course($data, $editoroptions);
        
        //Add auto enrollment criteria
        addAutoEnrollmentCriteria($data,$course->id);
        
		if($USER->archetype == $CFG->userTypeManager){
			$userDepartment = $DB->get_record('department_members',array('userid'=>$USER->id,'is_active'=>1));
			if (!empty($userDepartment)){
				$courseAssignment = assignCourseToDepartment($userDepartment->departmentid,array($course->id));
			}
		}
        // Get the context of the newly created course.
        $context = context_course::instance($course->id, MUST_EXIST);

        if (!empty($CFG->creatornewroleid) and !is_viewing($context, NULL, 'moodle/role:assign') and !is_enrolled($context, NULL, 'moodle/role:assign')) {
            // Deal with course creators - enrol them internally with default role.
            enrol_try_internal_enrol($course->id, $USER->id, $CFG->creatornewroleid);
        }
        if (!is_enrolled($context)) {
            // Redirect to manual enrolment page if possible.
            $instances = enrol_get_instances($course->id, true);
            foreach($instances as $instance) {
                if ($plugin = enrol_get_plugin($instance->enrol)) {
                    if ($plugin->get_manual_enrol_link($instance)) {
                        // We know that the ajax enrol UI will have an option to enrol.
						
						//redirect(new moodle_url('/enrol/users.php', array('id'=>$course->id)));
						
						if( $USER->archetype == $CFG->userTypeAdmin ) {
						
							if(isset($data->submitbutton2) && $data->submitbutton2!=''){
								  if (!empty($course->id)) {
									$url = new moodle_url($CFG->wwwroot.'/course/courseview.php', array('id'=>$course->id));
								  } else {
									$url = new moodle_url($CFG->wwwroot.'/course/index.php');
								  }
								  redirect($url);
							}else{											
								  $cFormat = getCourseFormat($course->id);
								  if($cFormat == 'topics'){
									 $url = new moodle_url($CFG->wwwroot.'/course/view.php', array('id'=>$course->id,'sesskey'=>sesskey(),'edit'=>1));
								  }else{
									 $url = new moodle_url($CFG->wwwroot.'/course/view.php', array('id'=>$course->id));
								  }
						          redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php', array('id'=>$course->id)));
						   }
						  
						  /*$courseFormat = getCourseFormatOptions($course->id, $format = 'singleactivity', $activitytypename = 'activitytype');
						  
						  if($courseFormat == 'scorm'){
                              redirect(new moodle_url('/course/modedit.php', array('add'=>'scorm','type'=>'','course'=>$course->id,'section'=>0,'return'=>0,'sr'=>'')));
						  }elseif($courseFormat == 'resource'){
						     
						  }*/
						}else{
						
						    if(isset($data->submitbutton2) && $data->submitbutton2!=''){
								  redirect(new moodle_url('/course/index.php', array()));
							}else{
											  //redirect(new moodle_url('/enrol/users.php', array('id'=>$course->id)));
						        redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php', array('id'=>$course->id)));
							}	
						}   
                    }
                }
            }
        }
    } else {
        // Save any changes to the files used in the editor.description 
        //Add auto enrollment criteria
        addAutoEnrollmentCriteria($data,$course->id);
        
        if($course->id && $course->coursetype_id == 2){
        	$sessions = array();
        	include_once("../mod/scheduler/lib.php");
        	$allSessionArr = classSessionListOfCourse($course->id);
        	if(count($allSessionArr) > 0 ){
        		foreach($allSessionArr as $classArr){
        			foreach($classArr as $sessionArr){
        				$sessions[] = $sessionArr->id;
        			}
        		}
        		
        		if(count($sessions) > 0 ){

        			$eventDesc = $course->summary_editor['text'];
        			$sessionIds = implode(",", $sessions);
        			if($sessionIds){
	        			$sql = "UPDATE mdl_event SET description = '".$eventDesc."' where eventtype = 'user' AND courseid = '0' AND sessionid IN ($sessionIds) ";
	        			//$sparams = array('description'=>$eventDesc);
	        			$DB->execute($sql);
        			}
        		}
        	}
        	
	        
        }
        
        update_course($data, $editoroptions);
        
        
    }
	
	// adding and updating course setings
	
	
	//addUpdateProgramCourseSettings($data, $CFG->courseType);
	//pr($_REQUEST);die;
	// Assigning deprtment, team and users to course under course setting 
	/*if(isset($_REQUEST['formsubmit']) && $_REQUEST['formsubmit'] == 1){
	 SaveCourseMappingData($_REQUEST);
	}*/
	// end 
	
	
		// Redirect user to newly created/updated course.
		//redirect(new moodle_url('/course/view.php', array('id' => $course->id))); // commented by rajesh 
		
		if(isset($data->submitbutton2) && $data->submitbutton2!=''){
  			  redirect(new moodle_url('/course/index.php', array()));
	    }else{
		
			if( $USER->archetype == $CFG->userTypeAdmin ) {
			  redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php', array('id'=>$course->id)));
			}else{
			  //redirect(new moodle_url('/course/view.php', array('id' => $course->id))); 
			  redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php', array('id'=>$course->id)));
			} 
		}
	
}
//
$allocationType  = getProgramCourseAllocationType($course->id, $CFG->courseType );

// Print the form.

$site = get_site();

$streditcoursesettings = get_string("editcoursesettings");
$straddnewcourse = get_string("addnewcourse");
$stradministration = get_string("administration");
$strcategories = get_string("categories");



//$PAGE->set_title($title);
//$PAGE->set_heading($fullname);

$PAGE->navbar->add(get_string('managecourses'), new moodle_url($CFG->wwwroot.'/course/index.php'));
if (!empty($course->id)) {
    $PAGE->navbar->add(get_string('editcourse','course'));
    $title = "$site->shortname: ".get_string('editcourse','course');
    $fullname = $course->fullname;
} else {
    //$PAGE->navbar->add($stradministration, new moodle_url('/admin/index.php'));
   // $PAGE->navbar->add($strcategories, new moodle_url('/course/index.php'));
    $PAGE->navbar->add(get_string('addcourse','course'));
    $title = "$site->shortname: ".get_string('addcourse','course');
    $fullname = $site->fullname;
}
$PAGE->set_title($title);
$PAGE->set_heading($fullname);

$PAGE->navbar->add(get_string('general', 'form'));
echo $OUTPUT->header();

$editform->display();

?>

<style>
.mform fieldset.error { border: none !IMPORTANT; }
</style>
<script>

function toggleClassroomBox(type){

   if(type == 1){ // online
       $('#fitem_id_criteria').show();
	   $('#fitem_id_credithours').show();
	   $('#fitem_id_primary_instructor').hide();
	   $('#fitem_id_classroom_instruction_editor').hide();
	   $('#fitem_id_classroom_course_duration').hide();
	   $('#fitem_id_is_global').show();
	   $('#fitem_id_enddate').show();
           $('#id_autoenrollment').show();
            $('#id_autoannualenrollment').show();
	   
	   
   }else if(type == 2){ // classroom
   
       $('#id_criteria').val('<?php echo $CFG->courseCriteriaElective; ?>');
	   $('#fitem_id_criteria').hide();
	   $('#fitem_id_credithours').hide();
	   $('#fitem_id_primary_instructor').show();
	   $('#fitem_id_classroom_instruction_editor').show();
	   $('#fitem_id_classroom_course_duration').show();
	   $('#id_is_global').attr('checked',false);
	   $('#fitem_id_is_global').hide();
	   $('#fitem_id_enddate').hide();
           $('#id_autoenrollment').hide();
           $('#id_autoannualenrollment').hide();
	   
   }
   $('#id_primary_instructor').val('');
   
}
$(document).ready(function(){
   
    $(document).on("click", "#id_submitbutton, #id_submitbutton2", function(){

		    var id_fullname = $('#id_fullname').val();
			id_fullname =  $.trim(id_fullname);

			if( id_fullname != '' ){  
				
				$(this.form).submit(function(){
				
					 $("#id_submitbutton").prop('disabled', true);
					 $("#id_submitbutton2").prop('disabled', true);
				
				});
			}
	});
	
	

    // $('#id_credithours').after('&nbsp;<?php echo get_string('minutes')?>');
	$(".removeimage").click(function(){
		var courseId = $(this).attr('rel');
		$.ajax({
			url:'<?php echo $CFG->wwwroot;?>/local/ajax.php',
			type:'POST',
			data:'action=deleteCourseImage&courseId='+courseId,
			success:function(data){
				alert("<?php echo get_string('course_image_delete_message');?>");
				window.location.reload(true);
			}
		});
	});
	//$("#fitem_id_summary_editor").css('height','150px');
	
	<?php  
	if( $USER->archetype == $CFG->userTypeAdmin ) {
	//if(in_array($userrole, $CFG->custommanagerroleid)){  // added by rajesh ?>
	
				$('#fitem_id_hiddensections').hide();
				$('#fitem_id_coursedisplay').hide();
				
   <?php } ?>	
   
   $("#id_coursetype_id").change(function(){
       toggleClassroomBox($(this).val());
   });	
   
   $("#id_criteria").change(function(){
   
       var id_criteria = $(this).val();
	   if(id_criteria == '<?php echo $CFG->courseCriteriaCompliance;?>'){
         $('#id_is_global').attr('checked',false);
		 $('#fitem_id_is_global').hide();
	   }else{
	     $('#fitem_id_is_global').show();
	   }
   });	
   
   		
	
	<?php if($id){ 
		
	       if($course->coursetype_id == $CFG->courseTypeOnline){
	?>
	          toggleClassroomBox(<?php echo $course->coursetype_id;?>);
			  
			  <?php if($course->criteria == 1){ // conmpliance ?>
			      $('#id_is_global').attr('checked',false);
		          $('#fitem_id_is_global').hide();
			  <?php }else{ // elective?>
			     $('#fitem_id_is_global').show();
			  <?php } ?>
			  
	<?php }
  		else if($course->coursetype_id == $CFG->courseTypeClassroom){
	?>
	          toggleClassroomBox(<?php echo $course->coursetype_id;?>);
			  
			  <?php if($course->criteria == 1){ // conmpliance ?>
			      $('#id_is_global').attr('checked',false);
		          $('#fitem_id_is_global').hide();
			  <?php }else{ // elective?>
			     $('#fitem_id_is_global').show();
			  <?php } ?>
			  
	<?php }}else{ ?>
	          toggleClassroomBox(<?php echo $CFG->courseTypeOnline;?>);
	<?php } ?>
	
});

/*
$(document).on('keyup', '#id_credithours', function(){

  // var cHoursDB = <?php //echo $course->credithours?$course->credithours:'';?>;
   var cHours = $(this).val();
   cHours = $.trim(cHours);
   if(cHours != '' && isNaN(cHours)){
      $(this).val('');
   }else{
     $(this).val(cHours);
   }

});*/

$(document).on('blur', '#id_fullname, #id_idnumber', function(){

   var cVal = $(this).val();
   cVal = $.trim(cVal);
   $(this).val(cVal);


});

</script>


<!--  for course setting  -->


<!--script>
$(document).ready(function(){
	var userRole = "<?php //echo $USER->archetype; ?>";
	var userManager = "<?php //echo $CFG->userTypeManager; ?>";
	var allocationType = "<?php //echo $allocationType; ?>";
	
	
	if(allocationType == 1){
	  $('.self-allocation-box').addClass('hide');
	}else if(allocationType == 2){
	  $('.self-allocation-box').removeClass('hide');
	} 
	
	$(document).on('click','[name = "allocation_type"]', function(){
	    var allocationType = $(this).val();
		if(allocationType == 1){
		  $('.self-allocation-box').addClass('hide');
		}else if(allocationType == 2){
		  $('.self-allocation-box').removeClass('hide');
		} 
	});
	
	$(".tabLink").each(function(){
      $(this).click(function(){
        tabeId = $(this).attr("id");
        $(".tabLink").removeClass("activeLink");
        $(this).addClass("activeLink");
        $(".tabcontent").addClass("hide");
        $("#"+tabeId+"-1").removeClass("hide");					
        return false;	  
      });
    }); 
	
	
	
	$("#addselect_searchtext").keyup(function(){
		var searchText = $(this).val();
		$.ajax({
				url:'<?php //echo $CFG->wwwroot;?>/local/user/searchcourses.php',
				type:'POST',
				data:'action=searchNonCourse&courseid=<?php //echo $courseid; ?>&search_text='+searchText,
				success:function(data){
					$('#addselect_wrapper').find('select').html(data);
				}
		});
	});
	$("#addselect_clearbutton").click(function(){
		$.ajax({
				url:'<?php //echo $CFG->wwwroot;?>/local/user/searchcourses.php',
				type:'POST',
				data:'action=searchNonCourse&courseid=<?php //echo $courseid; ?>',
				success:function(data){
					$('#addselect_wrapper').find('select').html(data);
					$("#addselect_searchtext").val('');
				}
		});
	});
	$("#removeselect_clearbutton").click(function(){
		$.ajax({
				url:'<?php //echo $CFG->wwwroot;?>/local/user/searchcourses.php',
				type:'POST',
				data:'action=searchUserCourse&courseid=<?php //echo $courseid; ?>',
				success:function(data){
					$('#removeselect_wrapper').find('select').html(data);
					$("#removeselect_searchtext").val('');
				}
		});
	});
	$("#removeselect_searchtext").keyup(function(){
		var searchText = $(this).val();
		$.ajax({
				url:'<?php //echo $CFG->wwwroot;?>/local/user/searchcourses.php',
				type:'POST',
				data:'action=searchUserCourse&courseid=<?php //echo $courseid; ?>&search_text='+searchText,
				success:function(data){
					$('#removeselect_wrapper').find('select').html(data);
				}
		});
	});
	if(userRole != userManager){
		var fromDepartment = $('#department_select_wrapper select');
		var toDepartment = $('#department_remove_wrapper select');
	}

	var fromTeam = $('#team_addselect_wrapper select');
	var toTeam = $('#team_removeselect_wrapper select');

	var fromUser = $('#user_addselect_wrapper select');
	var toUser = $('#user_removeselect_wrapper select');

	$(document).on("click", "#department_select_wrapper select option",function(){
		$("#department_add").removeAttr("disabled");
	});
	$(document).on("click", "#department_remove_wrapper select option",function(){
		$("#department_remove").removeAttr("disabled");
	});

	$(document).on("click", "#team_addselect_wrapper select option",function(){
		$("#team_add").removeAttr("disabled");
	});
	$(document).on("click", "#team_removeselect_wrapper select option",function(){
		$("#team_remove").removeAttr("disabled");
	});
	$(document).on("click", "#user_addselect_wrapper select option",function(){
		$("#user_add").removeAttr("disabled");
	});
	$(document).on("click", "#user_removeselect_wrapper select option",function(){
		if($(this).attr('class') == 'option-disable'){
			$("#user_remove").addAttr("disabled");
		}else{
			$("#user_remove").removeAttr("disabled");
		}
	});

	$(document).on("click","#department_add",function(){
			fromDepartment.find('option:selected').appendTo(toDepartment.find('optgroup'));
	});
	$(document).on("click","#team_add",function(){
			fromTeam.find('option:selected').appendTo(toTeam.find('optgroup'));
	});
	$(document).on("click","#user_add",function(){
			fromUser.find('option:selected').appendTo(toUser.find('optgroup'));
	});

	$(document).on("click","#department_remove",function(){
			toDepartment.find('option:selected').appendTo(fromDepartment.find('optgroup'));
	});
	$(document).on("click","#team_remove",function(){
			toTeam.find('option:selected').appendTo(fromTeam.find('optgroup'));
	});
	$(document).on("click","#user_remove",function(){
		var err = 0;
		toUser.find( "option:selected" ).each(function() {
			if($(this).attr('class') == 'option-disable'){
				err = 1;
			}else{
				$(this).appendTo(fromUser.find('optgroup'));
			}
		});
		if(err == 1){
			alert("<?php //echo get_string('assignment_error');?>");
			toUser.find( "option" ).each(function() {
				//$(this).removeAttr('selected');
			});
		}
	});
	$("#id_submitbutton").click(function(){
		if(userRole != userManager){
			fromDepartment.find('optgroup').find("option").each(function(){
				$(this).removeAttr("selected");
			});
		}
		fromTeam.find('optgroup').find("option").each(function(){
			$(this).removeAttr("selected");
		});
		fromUser.find('optgroup').find("option").each(function(){
			$(this).removeAttr("selected");
		});
		if($("#department_all").is(':checked')){
			if(userRole != userManager){
				fromDepartment.find('optgroup').find("option").each(function(){
					$(this).appendTo(toDepartment.find('optgroup'));
				});
			}
			fromTeam.find('optgroup').find("option").each(function(){
				$(this).appendTo(toTeam.find('optgroup'));
			});
			fromUser.find('optgroup').find("option").each(function(){
				$(this).appendTo(toUser.find('optgroup'));
			});
		}
		if(userRole != userManager){
			toDepartment.find('option').prop('selected', true);
		}
		toTeam.find('option').prop('selected', true);
		toUser.find('option').prop('selected', true);
		$("#assignform").submit();
	});
	$("#department_all").click(function(){
		if($("#department_all").is(':checked')){
			$(".tab-box").hide();
			$(".tabcontent").hide();
		}else{
			$(".tab-box").show();
			$(".tabcontent").show();
		}
	});
	$('#id_cancel').click(function(){
		window.location.href = '<?php// echo $CFG->wwwroot; ?>/course/index.php';
	});
});
</script->

<!--  end course setting  -->

<?php echo $OUTPUT->footer();
