<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
* Adds or updates modules in a course using new formslib
*
* @package    moodlecore
* @copyright  1999 onwards Martin Dougiamas (http://dougiamas.com)
* @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
*/

require_once("../config.php");
require_once("lib.php");
require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/gradelib.php');
require_once($CFG->libdir.'/completionlib.php');
require_once($CFG->libdir.'/conditionlib.php');
require_once($CFG->libdir.'/plagiarismlib.php');
require_once($CFG->dirroot . '/course/modlib.php');

$add    = optional_param('add', '', PARAM_ALPHA);     // module name
$update = optional_param('update', 0, PARAM_INT);
$return = optional_param('return', 0, PARAM_BOOL);    //return to course/view.php if false or mod/modname/view.php if true
$type   = optional_param('type', '', PARAM_ALPHANUM); //TODO: hopefully will be removed in 2.0
$sectionreturn = optional_param('sr', null, PARAM_INT);
$resourceType = optional_param('resource_type', 0, PARAM_INT);
$allow_student_access = optional_param('allow_student_access', 0, PARAM_INT);
$classId = optional_param('classId', 0, PARAM_INT);

$moduleType = '';
if($add == $CFG->schedulerModule){
   $moduleType = $CFG->schedulerModuleId;
}



if($USER->archetype == $CFG->userTypeStudent){ 

	if(!empty($add)){

	       if(in_array($add, array($CFG->resourceModule)) && isset($classId)){
		   }else{
			 redirect($CFG->wwwroot.'/course/index.php');
		   }	
	}

}

if($update){

    $courseModules = $DB->get_record('course_modules', array('id'=>$update), '*', MUST_EXIST);
	$moduleType = $courseModules->module;
	
	$courseTypeId = getCourseTypeIdByCourseId($courseModules->course); 
	if($courseTypeId == $CFG->courseTypeClassroom){ 
	
	  if($courseModules->module == $CFG->resourceModuleId){ 

	      $resourceDetails = $DB->get_record('resource', array('id'=>$courseModules->instance), '*', MUST_EXIST);
          if(!empty($resourceDetails) && $resourceDetails->is_reference_material == 1){

			 $classRefMatDetails = $DB->get_record('class_reference_material', array('resource_id'=>$courseModules->instance), '*', MUST_EXIST);
			 $classId = $classRefMatDetails->scheduler_id;

		  }else{
		    if($USER->archetype == $CFG->userTypeStudent){ 
			   checkUserAccess($CFG->courseClassroom , $courseModules->course);
		    }
		  }
	  }else{

	    if($USER->archetype == $CFG->userTypeStudent){ 
			checkUserAccess($CFG->courseClassroom , $courseModules->course);
		}
	  
	  }	
    }
	
}

//die('here');

//$userrole =  getUserRole($USER->id);
if($moduleType != $CFG->schedulerModuleId){
	if(!isset($_SESSION['asset_name']) && !isset($_SESSION['asset_description'])){
		if (!empty($add)) {
			$course  = required_param('course', PARAM_INT);
			redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php', array('id'=>$course)));
		}else if (!empty($update)) {
			redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php', array('id'=>$update)));
		}
	}elseif((isset($_SESSION['asset_name']) && empty($_SESSION['asset_name'])) || (isset($_SESSION['asset_description']) && empty($_SESSION['asset_description']))){
		if (!empty($add)) {
			$course  = required_param('course', PARAM_INT);
			redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php', array('id'=>$course)));
		}else if (!empty($update)) {
			redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php', array('id'=>$update)));
		}
	}
}


//checkUserAccess

$url = new moodle_url('/course/modedit.php');
$url->param('sr', $sectionreturn);
if (!empty($return)) {
    $url->param('return', $return);
}
$PAGE->navbar->add(get_string('managecourses'), new moodle_url($CFG->wwwroot.'/course/index.php'));

if (!empty($add)) { 
	
    $section = required_param('section', PARAM_INT);
    $course  = required_param('course', PARAM_INT);
    $PAGE->navbar->add(get_string('editcourse','course'), new moodle_url($CFG->wwwroot.'/course/edit.php',array('id'=>$course)));
    if($moduleType != $CFG->schedulerModuleId){
	
	    if(in_array($add, array($CFG->resourceModule)) && isset($classId) && !empty($classId)){
		  $PAGE->navbar->add(get_string('schedulingandtracking'), new moodle_url($CFG->wwwroot.'/mod/scheduler/list.php',array('id'=>$course)));
    	  $PAGE->navbar->add(get_string('addreferencematerial'));
		}else{
			//$PAGE->navbar->add(get_string('manage_assets'), new moodle_url($CFG->wwwroot.'/course/assetsview.php',array('id'=>$course)));
			$PAGE->navbar->add(get_string('assets'), new moodle_url($CFG->wwwroot.'/course/assetsview.php',array('id'=>$course)));
			$PAGE->navbar->add(get_string('add_asset'));
		}
    }else{
    	$PAGE->navbar->add(get_string('schedulingandtracking'), new moodle_url($CFG->wwwroot.'/mod/scheduler/list.php',array('id'=>$course)));
    	$PAGE->navbar->add(get_string('add_classroom','scheduler'));
    }
    $url->param('add', $add);
    $url->param('section', $section);
    $url->param('course', $course);
    $PAGE->set_url($url);

    $course = $DB->get_record('course', array('id'=>$course), '*', MUST_EXIST);
    require_login($course);

    // There is no page for this in the navigation. The closest we'll have is the course section.
    // If the course section isn't displayed on the navigation this will fall back to the course which
    // will be the closest match we have.
    navigation_node::override_active_url(course_get_url($course, $section));

    list($module, $context, $cw) = can_add_moduleinfo($course, $add, $section);

    $cm = null;

    $data = new stdClass();
    $data->section          = $section;  // The section number itself - relative!!! (section column in course_sections)
    $data->visible          = $cw->visible;
    $data->course           = $course->id;
    $data->module           = $module->id;
    $data->modulename       = $module->name;
    $data->groupmode        = $course->groupmode;
    $data->groupingid       = $course->defaultgroupingid;
    $data->groupmembersonly = 0;
    $data->id               = '';
    $data->instance         = '';
    $data->coursemodule     = '';
    $data->add              = $add;
    $data->return           = 0; //must be false if this is an add, go back to course view on cancel
    $data->sr               = $sectionreturn;
    if($moduleType != $CFG->schedulerModuleId){
	$data->name             = $_SESSION['asset_name'];
	$data->intro      = $_SESSION['asset_description'];
	$data->classId      = $classId;
    }
	$data->resource_type      = $resourceType;
	$data->allow_student_access      = $allow_student_access;
	$data->is_reference_material      = $classId?1:0;

	if (plugin_supports('mod', $data->modulename, FEATURE_MOD_INTRO, true)) {
        $draftid_editor = file_get_submitted_draft_itemid('introeditor');
        file_prepare_draft_area($draftid_editor, null, null, null, null, array('subdirs'=>true));
        $data->introeditor = array('text'=>$data->intro, 'format'=>FORMAT_HTML, 'itemid'=>$draftid_editor); // TODO: add better default
    }

    if (plugin_supports('mod', $data->modulename, FEATURE_ADVANCED_GRADING, false)
            and has_capability('moodle/grade:managegradingforms', $context)) {
        require_once($CFG->dirroot.'/grade/grading/lib.php');

        $data->_advancedgradingdata['methods'] = grading_manager::available_methods();
        $areas = grading_manager::available_areas('mod_'.$module->name);

        foreach ($areas as $areaname => $areatitle) {
            $data->_advancedgradingdata['areas'][$areaname] = array(
                'title'  => $areatitle,
                'method' => '',
            );
            $formfield = 'advancedgradingmethod_'.$areaname;
            $data->{$formfield} = '';
        }
    }

    if (!empty($type)) { //TODO: hopefully will be removed in 2.0
        $data->type = $type;
    }

    $sectionname = get_section_name($course, $cw);
    $fullmodulename = get_string('modulename', $module->name);

    if ($data->section && $course->format != 'site') {
        $heading = new stdClass();
        $heading->what = $fullmodulename;
        $heading->to   = $sectionname;
        $pageheading = get_string('addinganewto', 'moodle', $heading);
    } else {
        $pageheading = get_string('addinganew', 'moodle', $fullmodulename);
    }
    $navbaraddition = $pageheading;

} else if (!empty($update)) {

    $url->param('update', $update);
    $PAGE->set_url($url);
	
    // Select the "Edit settings" from navigation.
    navigation_node::override_active_url(new moodle_url('/course/modedit.php', array('update'=>$update, 'return'=>1)));

    // Check the course module exists.
    $cm = get_coursemodule_from_id('', $update, 0, false, MUST_EXIST);

    // Check the course exists.
    $course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);
    $PAGE->navbar->add(get_string('editcourse','course'), new moodle_url($CFG->wwwroot.'/course/edit.php',array('id'=>$course->id)));
    if($moduleType != $CFG->schedulerModuleId){	
	     if(isset($classId) && !empty($classId)){
		  $PAGE->navbar->add(get_string('schedulingandtracking'), new moodle_url($CFG->wwwroot.'/mod/scheduler/list.php',array('id'=>$course->id)));
    	  $PAGE->navbar->add(get_string('editreferencematerial'));
		 }else{
		  $PAGE->navbar->add(get_string('manage_assets'), new moodle_url($CFG->wwwroot.'/course/assetsview.php',array('id'=>$course->id)));
		  $PAGE->navbar->add(get_string('edit_asset'));
		}
    } else{
    	$PAGE->navbar->add(get_string('schedulingandtracking'), new moodle_url($CFG->wwwroot.'/mod/scheduler/list.php',array('id'=>$course->id)));
    	$PAGE->navbar->add(get_string('edit_classroom','scheduler'));
    }
    // require_login
    require_login($course, false, $cm); // needed to setup proper $COURSE

    list($cm, $context, $module, $data, $cw) = can_update_moduleinfo($cm);

    $data->coursemodule       = $cm->id;
    $data->section            = $cw->section;  // The section number itself - relative!!! (section column in course_sections)
    $data->visible            = $cm->visible; //??  $cw->visible ? $cm->visible : 0; // section hiding overrides
    $data->cmidnumber         = $cm->idnumber;          // The cm IDnumber
    $data->groupmode          = groups_get_activity_groupmode($cm); // locked later if forced
    $data->groupingid         = $cm->groupingid;
    $data->groupmembersonly   = $cm->groupmembersonly;
    $data->course             = $course->id;
    $data->module             = $module->id;
    $data->modulename         = $module->name;
    $data->instance           = $cm->instance;
    $data->return             = $return;
    $data->sr                 = $sectionreturn;
    $data->update             = $update;
    $data->completion         = $cm->completion;
    $data->completionview     = $cm->completionview;
    $data->completionexpected = $cm->completionexpected;
    $data->completionusegrade = is_null($cm->completiongradeitemnumber) ? 0 : 1;
    $data->showdescription    = $cm->showdescription;
    if($moduleType != $CFG->schedulerModuleId){
	$data->name             = $_SESSION['asset_name'];
	$data->intro      = $_SESSION['asset_description'];
	$data->classId      = $classId;
    }
	$data->allow_student_access      = $_SESSION['allow_student_access'];
	$data->resource_type      = $_SESSION['resource_type'];
	
    if (!empty($CFG->enableavailability)) {
        $data->availablefrom      = $cm->availablefrom;
        $data->availableuntil     = $cm->availableuntil;
        $data->showavailability   = $cm->showavailability;
    }

    if (plugin_supports('mod', $data->modulename, FEATURE_MOD_INTRO, true)) {
        $draftid_editor = file_get_submitted_draft_itemid('introeditor');
        $currentintro = file_prepare_draft_area($draftid_editor, $context->id, 'mod_'.$data->modulename, 'intro', 0, array('subdirs'=>true), $data->intro);
        $data->introeditor = array('text'=>$currentintro, 'format'=>$data->introformat, 'itemid'=>$draftid_editor);
    }

    if (plugin_supports('mod', $data->modulename, FEATURE_ADVANCED_GRADING, false)
            and has_capability('moodle/grade:managegradingforms', $context)) {
        require_once($CFG->dirroot.'/grade/grading/lib.php');
        $gradingman = get_grading_manager($context, 'mod_'.$data->modulename);
        $data->_advancedgradingdata['methods'] = $gradingman->get_available_methods();
        $areas = $gradingman->get_available_areas();

        foreach ($areas as $areaname => $areatitle) {
            $gradingman->set_area($areaname);
            $method = $gradingman->get_active_method();
            $data->_advancedgradingdata['areas'][$areaname] = array(
                'title'  => $areatitle,
                'method' => $method,
            );
            $formfield = 'advancedgradingmethod_'.$areaname;
            $data->{$formfield} = $method;
        }
    }

    if ($items = grade_item::fetch_all(array('itemtype'=>'mod', 'itemmodule'=>$data->modulename,
                                             'iteminstance'=>$data->instance, 'courseid'=>$course->id))) {
        // add existing outcomes
        foreach ($items as $item) {
            if (!empty($item->outcomeid)) {
                $data->{'outcome_'.$item->outcomeid} = 1;
            }
        }

        // set category if present
        $gradecat = false;
        foreach ($items as $item) {
            if ($gradecat === false) {
                $gradecat = $item->categoryid;
                continue;
            }
            if ($gradecat != $item->categoryid) {
                //mixed categories
                $gradecat = false;
                break;
            }
        }
        if ($gradecat !== false) {
            // do not set if mixed categories present
            $data->gradecat = $gradecat;
        }
    }

    $sectionname = get_section_name($course, $cw);
    $fullmodulename = get_string('modulename', $module->name);

    if ($data->section && $course->format != 'site') {
        $heading = new stdClass();
        $heading->what = $fullmodulename;
        $heading->in   = $sectionname;
        $pageheading = get_string('updatingain', 'moodle', $heading);
    } else {
        $pageheading = get_string('updatinga', 'moodle', $fullmodulename);
    }
    $navbaraddition = null;

} else {
    require_login();
    print_error('invalidaction');
}

$pagepath = 'mod-' . $module->name . '-';
if (!empty($type)) { //TODO: hopefully will be removed in 2.0
    $pagepath .= $type;
} else {
    $pagepath .= 'mod';
}
$PAGE->set_pagetype($pagepath);

if($classId){
  $PAGE->set_pagelayout('classroompopup');
}else{
  $PAGE->set_pagelayout('admin');
}
$modmoodleform = "$CFG->dirroot/mod/$module->name/mod_form.php";
if (file_exists($modmoodleform)) {
    require_once($modmoodleform);
} else {
    print_error('noformdesc');
}


$mformclassname = 'mod_'.$module->name.'_mod_form';
$mform = new $mformclassname($data, $cw->section, $cm, $course);
$mform->set_data($data);

if ($mform->is_cancelled()) {
	$fromform = $mform->get_data();
	if($classId){ // means reference material of class for any classroom course exist
	  redirect(new moodle_url($CFG->wwwroot.'/mod/scheduler/materialsview.php?id='.$course->id.'&classId='.$classId));
	}else{
		  if( $USER->archetype == $CFG->userTypeAdmin ) {
		  //if(in_array($userrole, $CFG->custommanagerroleid)){ // added by rajesh 
			  $url = new moodle_url($CFG->wwwroot.'/course/index.php?categoryid='.$course->category);
			  //redirect($url);
		  }else{
		  
			if ($return && !empty($cm->id)) {
				//redirect("$CFG->wwwroot/mod/$module->name/view.php?id=$cm->id");
			} else {
				//redirect(course_get_url($course, $cw->section, array('sr' => $sectionreturn)));
			}
			
		  }
			
		  if($module->name=='scheduler'){
		  	
			redirect(new moodle_url($CFG->wwwroot.'/mod/scheduler/list.php?id='.$course->id));
		  }else{
			redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php?id='.$course->id));
		  }
	}
	  
} else if ($fromform = $mform->get_data()) {
	
	unset($_SESSION['asset_name']);
	unset($_SESSION['asset_description']);
	unset($_SESSION['allow_student_access']);
	unset($_SESSION['resource_type']);
	
    if (!empty($fromform->update)) {
    	
    //	pr($fromform);
    	
		$fromform->modifiedby = $USER->id;
		if($fromform->modulename=='scheduler'){
			if(trim($_REQUEST['open_for'])){
				$fromform->open_for =1;
			}
			else{
				$fromform->open_for =0;
			}
				
		}

        list($cm, $fromform) = update_moduleinfo($cm, $fromform, $course, $mform);
		
        if($fromform->modulename=='scheduler'){
        	//code addeded for open classroom course . enrol department, team and users into classrooom
        	$issendinvite = false;
        	
        	//enrolIntoOpenCourse($fromform->instance,$_REQUEST['addteamcourses'],'team');
        	//enrolIntoOpenCourse($fromform->instance,$_REQUEST['add_department_course'],'department');
        	
        	
        	//enrolIntoOpenCourse($fromform->instance,$_REQUEST['addusercourse'],'user');
        	/* if (isset($fromform->sendinvite)) {
        		$open_for_department = explode(',',$_REQUEST['invite_to_department']) ;
        		$open_for_team = explode(',',$_REQUEST['invite_to_team']) ;
        		$open_for_user=  explode(',',$_REQUEST['invite_to_user']) ;
        		
        		sendOpenInvitetoUsers($fromform->instance,$open_for_department,$open_for_team,$open_for_user,$_REQUEST['open_for']);
        	} */
        	$_SESSION['update_msg'] = get_string('classupdated','scheduler');
        	
        	$_SESSION['error_class'] = 'success';
        }else{
		
        	 callPdfConvertor($fromform, 1); // to create a resource file into pdf file
        	
			 if(isset($fromform->classId) && $fromform->classId){
			  $_SESSION['update_msg'] = get_string('referencematerialupdated');
		      $_SESSION['error_class'] = 'success';
			}else{
				$_SESSION['update_msg'] = get_string('assetquickupdate','course');
				$_SESSION['error_class'] = 'success';
			}	
        }
    } else if (!empty($fromform->add)) {
	
	    if(isset($fromform->classId) && $fromform->classId){
		  $fromform->is_reference_material = 1;
		}
        
		$fromform->createdby = $USER->id;
		$fromform->modifiedby = $USER->id;
		if($fromform->modulename=='scheduler'){
			if(trim($_REQUEST['open_for']) && $fromform->enrolmenttype==0){
				$fromform->open_for =1;
			}
			else{
				$fromform->open_for =0;
			}
			
		}
	
        $fromform = add_moduleinfo($fromform, $course, $mform);

		
        if($fromform->modulename=='scheduler'){
        
        	
        	enrolIntoOpenCourse($fromform->instance,$_REQUEST['add_department_course'],'department');
        	 
        	enrolIntoOpenCourse($fromform->instance,$_REQUEST['addteamcourses'],'team');
        	enrolIntoOpenCourse($fromform->instance,$_REQUEST['addusercourse'],'user');
        	if (isset($fromform->sendinvite)) {
        		
        		 $open_for_department = explode(',',$_REQUEST['invite_to_department']) ;
        		 $open_for_team = explode(',',$_REQUEST['invite_to_team']) ;
        		 $open_for_user=  explode(',',$_REQUEST['invite_to_user']) ;
        		sendOpenInvitetoUsers($fromform->instance,$open_for_department,$open_for_team,$open_for_user);
        	}
        	$_SESSION['update_msg'] = get_string('classadded','scheduler');
        	$_SESSION['error_class'] = 'success';
        }else{
			
        	callPdfConvertor($fromform); // to create a resource file into pdf file
        	
			if(isset($fromform->classId) && $fromform->classId){
				$resourcId = $fromform->id;
				$scheduleId = $classId;
				$query = "INSERT INTO {$CFG->prefix}class_reference_material SET resource_id = '".$resourcId."', scheduler_id = '".$scheduleId."', timecreated = '".time()."', timemodified = '".time()."'";
				executeSql($query);
				$_SESSION['update_msg'] = get_string('referencematerialadded');
				$_SESSION['error_class'] = 'success';
			}else{
		
				$_SESSION['update_msg'] = get_string('assetquickadd','course');
				$_SESSION['error_class'] = 'success';
				
			}
        }
		
    } else {
        print_error('invaliddata');
    }

    if (isset($fromform->submitbutton) || isset($fromform->sendinvite)) { 
    	
        if (empty($fromform->showgradingmanagement)) {

        	if($fromform->modulename=="scheduler" ){
        		redirect(new moodle_url($CFG->wwwroot.'/mod/scheduler/list.php?id='.$course->id));
        	}else{
			
			    if(isset($fromform->classId) && $fromform->classId){
				    redirect(new moodle_url($CFG->wwwroot.'/mod/scheduler/materialsview.php?id='.$course->id.'&classId='.$fromform->classId));
				}else{
			   		redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php?id='.$course->id));
				}	
        	}
			 		
        } else { 

        	if($fromform->modulename=="scheduler" ){
        		redirect(new moodle_url($CFG->wwwroot.'/mod/scheduler/list.php?id='.$course->id));
        	}else{
        		 if(isset($fromform->classId) && $fromform->classId){
				    redirect(new moodle_url($CFG->wwwroot.'/mod/scheduler/materialsview.php?id='.$course->id.'&classId='.$fromform->classId));
				}else{
			   		redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php?id='.$course->id));
				}
        	}
			
        }
    } else {
    	
        //redirect(course_get_url($course, $cw->section, array('sr' => $sectionreturn)));
		  redirect(new moodle_url($CFG->wwwroot.'/course/assetsview.php?id='.$course->id));
    }
    exit;

} else {

    $streditinga = get_string('editinga', 'moodle', $fullmodulename);
    $strmodulenameplural = get_string('modulenameplural', $module->name);

    if (!empty($cm->id)) {
        $context = context_module::instance($cm->id);
    } else {
        $context = context_course::instance($course->id);
    }

    $PAGE->set_heading($course->fullname);
    $PAGE->set_title($streditinga);
    $PAGE->set_cacheable(false);

    if (isset($navbaraddition)) {
       // $PAGE->navbar->add($navbaraddition);
    }

    echo $OUTPUT->header();
    
    echo '<script src="'.$CFG->wwwroot.'/mod/scheduler/scripts/assignuser.js"></script>';
    if (get_string_manager()->string_exists('modulename_help', $module->name)) {
      //  echo $OUTPUT->heading_with_help($pageheading, 'modulename', $module->name, 'icon');
    } else {
       // echo $OUTPUT->heading_with_help($pageheading, '', $module->name, 'icon');
    }

    $mform->display();
?>

<style>
 <?php if($classId){
        echo '#page-mod-resource-mod .mform .fdescription.required{ margin-left:15px; }';
       }
 
       if($moduleType != $CFG->schedulerModuleId){
	     echo '#fitem_id_name,#fitem_id_introeditor,#fitem_id_showdescription{display:none;}';
       }
 ?>

</style>

<?php  
    echo $OUTPUT->footer();
}
?>
<script>
	$(document).ready(function(){

		 
		 checkWebLink = function(){
			 
			 isChecked =  $("input[type='radio'][name='is_weblink']:checked");
			 isChecked = isChecked.val();
			 // isChecked = $('#id_is_weblink').is(":checked");

			  if(isChecked == 1){
				  
				  $('#fitem_id_weblink_url label').html('<?php echo get_string('weblinkurl')?> <img class="req" title="Required field" alt="Required field" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/pix_core/req.png">');
				  $('#fitem_id_files').hide();
				  $('#fitem_id_can_download').hide();
				  $('#fitem_id_weblink_url').show();
			  }else{
				  $('#fitem_id_files label').html('<?php echo get_string('selectfile', 'course')?> <img class="req" title="Required field" alt="Required field" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/pix_core/req.png">');
				  $('#fitem_id_files').show();
				  $('#fitem_id_weblink_url').hide();
				  $('#fitem_id_can_download').show();
			  }
			  
	    }

		displayRequiredStarInScorm = function(){
			var scormtype = $("#id_scormtype").val();
			
			 // isChecked = $('#id_is_weblink').is(":checked");

			  if(scormtype == 'local'){
				  
				  $('#fitem_id_packagefile label').html('<?php echo get_string('package', 'scorm')?> <img class="req" title="Required field" alt="Required field" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/pix_core/req.png">');
				  $('#fitem_id_packageurl label').html('<?php echo get_string('packageurl', 'scorm')?>');
				  $('#fitem_id_packageurl span.error').remove();
				  $('#fitem_id_packageurl .error br').remove();
				  $('#fitem_id_packageurl .felement').removeClass('error');
				  $('#id_packageurl').val('');
				  
			  }else{
				  $('#fitem_id_packagefile label').html('<?php echo get_string('package', 'scorm')?> ');
				  $('#fitem_id_packageurl label').html('<?php echo get_string('packageurl', 'scorm')?> <img class="req" title="Required field" alt="Required field" src="<?php echo $CFG->wwwroot;?>/theme/gourmet/pix_core/req.png">');
				  $('#fitem_id_packagefile span.error').remove();
				  $('#fitem_id_packagefile .error br').remove();
				  $('#fitem_id_packagefile .felement').removeClass('error');
			  }
		}

		 if($('#id_can_download').length > 0){

			 <?php if($classId){
			     $checkedDownloaded = isset($resourceDetails->can_download) && $resourceDetails->can_download == 1?"checked":"";	
			 ?>
				 $('#fitem_id_can_download div.fcheckbox span').remove();
				 $('#fitem_id_can_download div.fitemtitle label').after('&nbsp;&nbsp;<span><input type="checkbox" <?php echo $checkedDownloaded;?> id="id_can_download" value="1" name="can_download"></span>');
				 $('#fitem_id_can_download').after('<div style="font-size:16px;float:left; margin-top: -15px;"><?php echo getResourceDownloadNoteHtml();?></div>');
			 <?php } else{ ?>
		         $('#fitem_id_can_download div.fcheckbox').append('<span><br><br><?php echo getResourceDownloadNoteHtml();?></span>');
			 <?php } ?>
		 }
		

		//$('#fitem_id_web_link_url').hide();
		$(document).on('blur', '#id_name', function(){

			   var cVal = $(this).val();
			   cVal = $.trim(cVal);
			   $(this).val(cVal);


			});
		$(document).on('click', '#id_is_weblink', function(){

			 checkWebLink();

		});
		$(document).on('change', '#id_scormtype', function(){

			displayRequiredStarInScorm();

		});
		displayRequiredStarInScorm();
		
	/*	$(document).on('click', '#id_submitbutton', function(){
			$("#id_is_weblink").removeAttr("disabled");
		});*/


		
		

		<?php  if(isset($resourceType) && $resourceType){?>
        <?php  }else{?>
             checkWebLink();
        <?php }?> 
        <?php if(isset($update) && $update){?>
          $('#fgroup_id_is_weblink').hide();
        <?php } ?>
	
		
		
		
	});
	</script>
