<?php 
		global $USER,$CFG;
		$current1 = $current2 = $current3 = $current4 =''; 
	
		$courseTypeIdInTab = getCourseTypeIdByCourseId($course->id);
		
		if(strstr($_SERVER['REQUEST_URI'], 'courseview.php') || strstr($_SERVER['REQUEST_URI'], 'course/edit.php')){
		  $current1 = 'current';   
		}elseif( ( (!isset($_GET['classId']) &&  !isset($is_reference_material)) || (isset($_GET['classId']) && $_GET['classId'] == 0) )  && ( strstr($_SERVER['REQUEST_URI'], 'course/assetsview.php') || strstr($_SERVER['REQUEST_URI'], 'course/modedit.php'))  ){
		  $current2 = 'current';   
		}elseif(strstr($_SERVER['PHP_SELF'], '/reports/course_progress_report.php') || strstr($_SERVER['PHP_SELF'], '/reports/user_performance_report.php')){
		  $current3 = 'current';   
		}elseif(strstr($_SERVER['REQUEST_URI'], 'courseallocation.php')){
		  $current4 = 'current';   
		}elseif( (isset($_GET['classId']) && $_GET['classId']) || (isset($is_reference_material) && $is_reference_material == 1)  || (strstr($_SERVER['REQUEST_URI'], 'mod/scheduler/list.php') || strstr($_SERVER['REQUEST_URI'], 'mod/scheduler/materialsview.php') || strstr($_SERVER['REQUEST_URI'], 'mod/scheduler/learner_grade.php') || strstr($_SERVER['REQUEST_URI'], 'mod/scheduler/view.php') || strstr($_SERVER['REQUEST_URI'], 'mod/scheduler/classroom_settings.php')) ){
		  $current5 = 'current';   
		}elseif(strstr($_SERVER['PHP_SELF'], '/reports/class_usage_report.php')){
		  $current6 = 'current';   
		}
		$UrlView = 'javascript:void(0);';
		$assetsView = 'javascript:void(0);';
		$reportView = 'javascript:void(0);';
		$allocationView = 'javascript:void(0);';
		$schedulingView = 'javascript:void(0);';
		$classrommReportView = 'javascript:void(0);';
		$class = 'disabled-tab';
		$published = 0;
		
		if (!empty($course->id)) {
			$UrlView		= $CFG->wwwroot."/course/courseview.php?id=".$course->id;
			$assetsView		= $CFG->wwwroot."/course/assetsview.php?id=".$course->id;
			$reportView		= $CFG->wwwroot."/reports/course_progress_report.php?back=6&cid=".$course->id;
			$allocationView = $CFG->wwwroot."/course/courseallocation.php?id=".$course->id;
			$schedulingView = $CFG->wwwroot."/mod/scheduler/list.php?id=".$course->id;
			$classrommReportView = $CFG->wwwroot."/reports/class_usage_report.php?back=3&cid=".$course->id;
			$class = '';
			GLOBAL $DB;
			$publishedOrNot = $DB->get_record_sql("SELECT id,publish FROM mdl_course WHERE id = ".$course->id);
			$published = $publishedOrNot->publish;
		}
		
		

	   $courseHTML .= "<div><a href='".$UrlView."' title='".get_string('general')."' class='$current1 $class'>".get_string('general')."<span></span></a></div>";
	   if (!empty($course->id)) {
	     $courseHTML .= "<div><a href='".$assetsView."' title='".get_string('assets')."' class='$current2 $class'>".get_string('assets')."<span></span></a></div>";
	   }
	
		if($courseTypeIdInTab == $CFG->courseTypeClassroom){
		
			  $courseHTML .= "<div><a href='".$schedulingView."' title='".get_string('schedulingandtracking')."'  class='$current5 $class'>".get_string('schedulingandtracking')."<span></span></a></div>";
			  $courseHTML .= "<div><a href='".$classrommReportView."' title='".get_string('reports')."'  class='$current6 $class'>".get_string('reports')."<span></span></a></div>";
				if (!empty($course->id)) {
				    
					$haveAnyAsset = isClassroomHaveAsset($course->id);
					$haveSpecificAsset = isClassroomHaveSpecificAsset($course->id);
					$msgSpecificAsset = get_string('publish');
					if(!$haveSpecificAsset){
				          
						  $a = new stdClass();
						  $a->specific = getClassroomSpecificAsset();
						  $msgSpecificAsset = get_string('noteforspecificasset','course', $a->specific);
						  //$courseHTML .= "<div style='margin:10px;width:98%'>".$msgSpecificAsset." </div>";
					}else{
					   
					   	// Its means there is no specific asset set into mdl_resource_type table, so we are here, so first we will check that there is any asset or not, if not then we will prompt a message to user to let him publish the course without having any asset, either we will allow him to publish a course with deafult publish message. 

						  $haveSpecificAsset = '-1';
						  if(!$haveAnyAsset){
					         $msgSpecificAsset = get_string('publishcoursewithoutanyasset','course');
						  }
					
					}
			  
					if($published == 1){
						 $courseHTML .= "<div class = 'publish-course-div' style='float: right;'><a href='javascript:void(0);' title='".get_string('published')."'  class='published_course' rel = '".$course->id."'>".get_string('published')."<span></span></a></div>";
					}else{
						//$haveSpecificAsset = isClassroomHaveSpecificAsset($course->id);
						//$a = new stdClass();
						//$a->specific = getClassroomSpecificAsset();
						//$msgSpecificAsset = get_string('musthavespecificasset','course', $a->specific);
                        
						if($USER->archetype != $CFG->userTypeStudent ){
						   $courseHTML .= '<div class = "publish-course-div" style="float: right;"><a href="javascript:void(0);" have-asset = "'.$haveAnyAsset.'"  have-specific = "'.$haveSpecificAsset.'" msg-specific="'.$msgSpecificAsset.'"  class="publish_course" title="'.get_string('publish','course').'" rel = "'.$course->id.'">'.get_string('publish','course').'<span></span></a></div>';
						
						}

					}
				}
			 
		 	
		}else{
		
			if($published == 1){
	
				 $courseHTML .= "<div><a href='".$allocationView."' title='".get_string('allocation')."'  class='$current4 $class'>".get_string('allocation')."<span></span></a></div>";
				 $courseHTML .= "<div><a href='".$reportView."' title='".get_string('reports')."'  class='$current3 $class'>".get_string('reports')."<span></span></a></div>";
				
			}
			if (!empty($course->id)) {
				if($published == 1){
					 $courseHTML .= "<div class = 'publish-course-div' style='float: right;'><a href='javascript:void(0);' title='".get_string('published')."'  class='published_course' rel = '".$course->id."'>".get_string('published')."<span></span></a></div>";
				}else{
					//$courseHTML .= "<div class = 'publish-course-div' style='float: right;'><a href='javascript:void(0);' title='".get_string('publish')."' have-specific = '".$haveSpecificAsset."' msg-specific='".$msgSpecificAsset."'  class='publish' rel = '".$course->id."'>".get_string('publish')."<span></span></a></div>";
					$courseHTML .= '<div class = "publish-course-div" style="float: right;"><a href="javascript:void(0);" class="publish_course" title="'.get_string('publish','course').'" rel = "'.$course->id.'">'.get_string('publish','course').'<span></span></a></div>';
				}
			}
		}
?>