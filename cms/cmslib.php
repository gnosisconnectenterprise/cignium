<?php
	require_once('../config.php');		
	global $USER,$CFG,$DB;	
	require_once($CFG->libdir."/dmllib.php");
	require_once($CFG->libdir."/deprecatedlib.php");
	require_once($CFG->libdir."/accesslib.php");
	
	function insert_cms($cmsdata)
	{
		global $USER, $DB, $CFG;
		$data = new stdClass();
		$data->createdby=$USER->id;
		$data->page_name = $cmsdata->pagename;
		$data->page_title = $cmsdata->pagetitle;
		$data->keyword = $cmsdata->pagekeyword;
		$data->description = $cmsdata->pagedescription;
		$data->content = $cmsdata->pagecontent_editor['text'];
		if($cmsdata->show_footer==''){
			$data->show_footer=0;
		}else{
			$data->show_footer=$cmsdata->show_footer;
		}
		if($cmsdata->is_external == '' || $cmsdata->is_external == 0){
			$data->is_external=0;
		}else{
			$data->is_external=$cmsdata->is_external;
			$data->external_url=addhttp($cmsdata->external_url);
		}
		$data->display_order=$cmsdata->display_order;
		$data->deleted=0;
		$data->status=1;
		$data->timecreated = time();
		$data->timemodified = time();
		$select = "SELECT display_order+1 as display_order FROM {$CFG->prefix}cms where deleted='0' order by display_order DESC LIMIT 0,1";
		$get_cms = $DB->get_record_sql($select);
		if($get_cms){
			$displayorder = $get_cms->display_order;
		}else{
			$displayorder = 1;
		}
		$data->display_order = $displayorder;
		$nid = $DB->insert_record('cms', $data,true); 
		return $nid;
	}
	
	function update_cms($cmsdata)
	{
		global $USER, $DB, $CFG;
		$data = new stdClass();
		$data->createdby=$cmsdata->createdby;
		$data->page_name = $cmsdata->pagename;
		$data->page_title = $cmsdata->pagetitle;
		$data->keyword = $cmsdata->pagekeyword;
		$data->description = $cmsdata->pagedescription;
		$data->content = $cmsdata->pagecontent_editor['text'];
		if($cmsdata->show_footer==''){
			$data->show_footer=0;
		}else{
			$data->show_footer=$cmsdata->show_footer;
		}
		if($cmsdata->is_external == '' || $cmsdata->is_external == 0){
			$data->is_external=0;
		}else{
			$data->is_external=$cmsdata->is_external;
			$data->external_url=addhttp($cmsdata->external_url);
		}
		$data->timemodified = time();
		$data->id = $cmsdata->id;
		$nid = $DB->update_record('cms', $data); 
		return $nid;
	}
	function get_Cms_Pages($sort = 'display_order', $dir='ASC', $page, $perpage,$paramArray){
		global $DB, $CFG;
		$searchString = "AND ";
		if(isset($paramArray['key'])){
			$paramArray['key'] = strtolower($paramArray['key']);
			$paramArray['key'] = addslashes($paramArray['key']);
		}
		if($paramArray['ch'] != '') {
			if($paramArray['ch'] == 'OTH')
				$searchString .= "(LOWER(page_name) REGEXP '^[^a-zA-Z]') AND";
			else
				$searchString .= "(LOWER(page_name) like '".$paramArray['ch']."%' OR LOWER(page_name) like '".ucwords($paramArray['ch'])."%') AND";
		}
		$searchKeyAll = true;
		if( ($paramArray['page_name']==1) || ($paramArray['page_title']==1) )
			$searchKeyAll = false;

		if($searchKeyAll && ($paramArray['key'] != '')) {
			$searchString .= " (LOWER(page_name) like '%".$paramArray['key']."%' OR LOWER(page_title) like '%".$paramArray['key']."%') AND";
		} else if ($paramArray['key'] != '') {
			$searchKeyAllString .= "(";
			if( $paramArray['page_name']==1 )
				$searchKeyAllString .= " LOWER(page_name) like '%".$paramArray['key']."%' OR";
			if( $paramArray['page_title']==1 )
				$searchKeyAllString .= " LOWER(page_title) like '%".$paramArray['key']."%' OR";

			$searchKeyAllString = substr($searchKeyAllString, 0, -2);
			$searchKeyAllString .= ") AND";
			$searchString .= $searchKeyAllString;
		}
		$searchString .= " 1=1";
		if($sort == 'cmscount'){
			$limit = '';
			$sort = '';
			$department = $DB->get_record_sql("SELECT count(*) as cmscount FROM {$CFG->prefix}cms where deleted='0' ".$searchString." order by display_order ASC");
			return $department->cmscount;
		}else{
			if ($sort) {
				$sort = $sort;
				$sort = " ORDER BY $sort $dir";
			}
			$page = $page - 1;
			$page = $page*$perpage;
			$limit = '';
			if($perpage != 0){
				$limit = "LIMIT $page,$perpage";
			}
			return $DB->get_records_sql("SELECT * FROM {$CFG->prefix}cms where deleted='0' $searchString $sort $limit");
		}
	}
	function insert_headerpage($cmsdata)
	{
		global $USER, $DB, $CFG;
		$data = new stdClass();
		$data->title = $cmsdata->title;
		$data->path = $cmsdata->path;
		$data->link_class = $cmsdata->link_class;
		$data->parent = $cmsdata->parent;
		if(!isset($cmsdata->is_super) || $cmsdata->is_super == '' || $cmsdata->is_super == 0){
			$data->is_super=0;
		}else{
			$data->is_super=$cmsdata->is_super;
			$data->is_super=$cmsdata->is_super;
		}

		if(!isset($cmsdata->is_manager) || $cmsdata->is_manager == '' || $cmsdata->is_manager == 0){
			$data->is_manager=0;
		}else{
			$data->is_manager=$cmsdata->is_manager;
			$data->is_manager=$cmsdata->is_manager;
		}

		if(!isset($cmsdata->is_learner) || $cmsdata->is_learner == '' || $cmsdata->is_learner == 0){
			$data->is_learner=0;
		}else{
			$data->is_learner=$cmsdata->is_learner;
			$data->is_learner=$cmsdata->is_learner;
		}
		$data->status=1;
		if($data->parent == 0){
			$select = "SELECT display_order+1 as display_order FROM {$CFG->prefix}header_navigation order by display_order DESC LIMIT 0,1";
		}else{
			$select = "SELECT display_order+1 as display_order FROM {$CFG->prefix}header_navigation where parent='".$data->parent."' order by display_order DESC LIMIT 0,1";
		}
		$get_cms = $DB->get_record_sql($select);
		if($get_cms){
			$displayorder = $get_cms->display_order;
		}else{
			$displayorder = 1;
		}
		$data->display_order = $displayorder;
		$nid = $DB->insert_record('header_navigation', $data); 
		return $nid;
	}
	function update_headerpage($cmsdata)
	{
		global $USER, $DB, $CFG;
		$data = new stdClass();
		$data->title = $cmsdata->title;
		$data->path = $cmsdata->path;
		$data->link_class = $cmsdata->link_class;
		$data->parent = $cmsdata->parent;
		if(!isset($cmsdata->is_super) || $cmsdata->is_super == '' || $cmsdata->is_super == 0){
			$data->is_super=0;
		}else{
			$data->is_super=$cmsdata->is_super;
			$data->is_super=$cmsdata->is_super;
		}

		if(!isset($cmsdata->is_manager) || $cmsdata->is_manager == '' || $cmsdata->is_manager == 0){
			$data->is_manager=0;
		}else{
			$data->is_manager=$cmsdata->is_manager;
			$data->is_manager=$cmsdata->is_manager;
		}

		if(!isset($cmsdata->is_learner) || $cmsdata->is_learner == '' || $cmsdata->is_learner == 0){
			$data->is_learner=0;
		}else{
			$data->is_learner=$cmsdata->is_learner;
			$data->is_learner=$cmsdata->is_learner;
		}
		$data->status=1;
		$data->id = $cmsdata->id;
		$nid = $DB->update_record('header_navigation', $data);
		return $nid;
	}
?>