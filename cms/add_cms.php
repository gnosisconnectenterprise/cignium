<?php
	require_once('../config.php');
	require_once($CFG->pageAddCmsForm);
	require_once($CFG->pageCmsLib);
	
	global $USER,$CFG;

	if(empty($USER->id)){
		require_login();
	}

	$context = get_context_instance(CONTEXT_SYSTEM);

	//check role assign univmanager
	if($USER->archetype!= $CFG->userTypeAdmin)
	{
		redirect($CFG->wwwroot."/");
	}
	//End role assing
	$strmymoodle = get_string('add_cms','cms');
	$header = "$SITE->shortname";	
	
	$editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES, 'maxbytes'=>$CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true);
	

	// Start setting up the page
	$id = optional_param('id', 0, PARAM_INT);
	$returnto = optional_param('returnto', 0, PARAM_ALPHANUM);
	$params = array();
	$PAGE->set_context($context);
	$PAGE->set_url('/cms/index.php', $params);
	$PAGE->set_pagelayout('general');
	$PAGE->set_pagetype('my-index');
	$PAGE->blocks->add_region('content');
	$PAGE->set_subpage($currentpage->id);
	
	$PAGE->set_heading($header);
	$PAGE->navbar->add(get_string('browse_cms','cms'), new moodle_url('/cms/index.php'));
	if($id==0){
		$strmymoodle = get_string('addcms',"cms");
		$PAGE->navbar->add($strmymoodle);
		$PAGE->set_title($header.": ".$strmymoodle);
	}
	else{
		$strmymoodle = get_string('edit_cms',"cms");
		$PAGE->navbar->add(get_string('edit_cms','cms'));
		$PAGE->set_title($header.": ".$strmymoodle);
	}
	
	$addcms_form = new addcms_form($CFG->pageAddCms,$addcmsdata);
	$back_url=$CFG->wwwroot."/cms/index.php";
	
	
	if($id != 0){
		$select = "SELECT * FROM {$CFG->prefix}cms WHERE id = $id";
		$get_cms = $DB->get_record_sql($select);
		$addcmsdata = new stdClass();   
		//print_r($get_cms);
		$addcmsdata->pagename = $get_cms->page_name;
		$addcmsdata->pagetitle = $get_cms->page_title;
		$addcmsdata->pagekeyword = $get_cms->keyword;
		$addcmsdata->pagedescription = $get_cms->description;
		$addcmsdata->pagecontent = $get_cms->content;
		$addcmsdata->pageuniversity_id = $get_cms->createdby;
		$addcmsdata->show_footer = $get_cms->show_footer;
		$addcmsdata->page_identifier = $get_cms->page_identifier;
		$addcmsdata->external_url = $get_cms->external_url;
		$addcmsdata->is_external = $get_cms->is_external;
		$addcmsdata->id = $id;
		$attachmentoptions = array('subdirs'=>false, 'maxfiles'=>99, 'maxbytes'=>$maxbytes);
		$addcmsdata->editoroptions=$editoroptions;
		$addcmsdata->returnto=$returnto;
		$addcmsdata->attachmentoptions=$attachmentoptions;
		$addcms_form = new addcms_form($CFG->pageAddCms,$addcmsdata);
	}
	else{
		$attachmentoptions = array('subdirs'=>false, 'maxfiles'=>99, 'maxbytes'=>$maxbytes);
	
		$addcms_form = new addcms_form(NULL, array('editoroptions'=>$editoroptions, 'returnto'=>$returnto, 'attachmentoptions'=>$attachmentoptions));
	}
	if($addcms_form->is_cancelled()) {		
		redirect($back_url);
    }else if($cmsdata= $addcms_form->get_data()){	
			$cmsdata->external_url = $_POST['external_url'];
			if($cmsdata->id!=''){
				$nid=update_cms($cmsdata);
				// Log the event entry.
				add_to_log(1, 'cms', 'update', 'cms/add_cms.php?id='.$cmsdata->id, $cmsdata->pagename, 0, $USER->id);
				$_SESSION['update_msg'] = get_string('record_updated');
				redirect($back_url);	
			}else{
			
				$nid=insert_cms($cmsdata);
				
				// Log the event entry.
				add_to_log(1, $CFG->cmsModule, 'add', 'cms/add_cms.php?id='.$nid, $cmsdata->pagename, 0, $USER->id);
				$_SESSION['update_msg'] = get_string('record_added');
				redirect($back_url);	
			}
			
		}	
		echo $OUTPUT->header();
	echo '<div class="left_content">
			<div class="category_section">';
	$addcms_form->display(); 
	echo '</div></div>';
?>

<script>
$(document).ready(function(){
	$("#id_is_external").click(function(){
		if($("#id_is_external").is(':checked')){
			$("#fitem_id_external_url").show();
		}else{
			$("#fitem_id_external_url").hide();
		}
	});
	$(document).on('blur', '#id_pagename,#id_pagetitle', function(){

		   var cVal = $(this).val();
		   cVal = $.trim(cVal);
		   $(this).val(cVal);


	});
	
	
	 $(document).on("click", "#id_submitbutton", function(){

		    var id_name = $('#id_pagename').val();
			var id_pagetitle = $('#id_pagetitle').val();
			id_name =  $.trim(id_name);
			id_pagetitle =  $.trim(id_pagetitle);

			if( id_name != '' && id_pagetitle != ''){  
				
				$(this.form).submit(function(){
					 $("#id_submitbutton").prop('disabled', true);
				
				});
			}
	});
	
});
</script>
<?php
	echo $OUTPUT->footer();
?>