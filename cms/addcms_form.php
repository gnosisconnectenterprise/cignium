<?php
	require_once('../config.php');	
	global $CFG;
	require_once($CFG->libdir.'/formslib.php');
	require_once($CFG->pageCmsLib);
	
	class addcms_form extends moodleform{
		//This method contains definition of the form i.e. all the elements to be displayed
		function definition() {
			//Defining global objects User and Configuration		
			global $USER, $CFG;
			//Form object
			$mform =& $this->_form;
			//data received to populate this form   
			$addcmsdata = $this->_customdata;
			if (!isset($addcmsdata->id)){ 
				$editoroptions = $this->_customdata['editoroptions'];
			}
			else{
				$editoroptions = $addcmsdata->editoroptions;
			}
			
			if (!isset($addcmsdata->id)) {
				//$mform->addElement('header','create_cms', get_string('addcms','cms'));
			}else{
				//$mform->addElement('header','create_cms', get_string('edit_cms','cms'));
			}
			$mform->addElement('html','<div class="filedset_outer ">');
			$mform->addElement('text','pagename', get_string('pagename','cms'),'maxlength="50" size="50"');
			
			$mform->setDefault('pagename', $addcmsdata->pagename);      
			$mform->addRule('pagename', get_string('required'), 'required', null);
			
			$mform->addElement('text','pagetitle', get_string('pagetitle','cms'),'maxlength="50" size="50"');   
			$mform->setDefault('pagetitle', $addcmsdata->pagetitle);           
			$mform->addRule('pagetitle', get_string('required'), 'required', null);

			$mform->addElement('checkbox', 'is_external', get_string('is_external','cms'));
			
			$display = '';
			if(($addcmsdata->is_external!='' && $addcmsdata->is_external!=0 ) || ($_POST['is_external']!='' && $_POST['is_external']!=0)){
				$mform->setDefault('is_external', 1);
				if (isset($addcmsdata->id)) {
					$externalUrl = $addcmsdata->external_url;
				}else{
					$externalUrl = $_POST['external_url'];
				}
			
			 }else{
				$display = 'style= "display:none;"';
				$mform->setDefault('is_external', 0);
			 } 
			 
			$errorTxt = "";
			$errorClass = "";
			if($_POST['is_external'] == 1 && trim($_POST['external_url']) == ''){
				$errorTxt = '<span class="error">'.get_string('required').'</span></br>';
				$errorClass = " error";
			}
			$mform->addElement('html','<div class="fitem fitem_ftext " id="fitem_id_external_url" '.$display.'><div class="fitemtitle"><label for="id_external_url">'.get_string('external_url','cms').' </label></div><div class="felement ftext'.$errorClass.'" >'.$errorTxt.'<input type="text" id="id_external_url" name="external_url" value = "'.$externalUrl.'"></div></div>');   
			$mform->setDefault('external_url', $externalUrl); 

			$test = array("text"=>$addcmsdata->pagecontent, 'format'=>1);
				
			$mform->addElement('editor','pagecontent_editor', get_string('pagecontent','cms'), null, $editoroptions);
			$mform->setDefault('pagecontent_editor', $test);  
			$mform->setType('pagecontent_editor', PARAM_RAW);
			
			$mform->addElement('textarea','pagekeyword', get_string('metakey','cms')); 
			$mform->setDefault('pagekeyword', $addcmsdata->pagekeyword);             
			
			$mform->addElement('textarea','pagedescription', get_string('metadesc','cms'));  
			$mform->setDefault('pagedescription', $addcmsdata->pagedescription); 
			
			if (isset($addcmsdata->id)) {
				$viewUrl = $CFG->wwwroot.'/cms/cms_content.php?pageid='.$addcmsdata->id;
				$mform->addElement('text','pageurl', get_string('pageurl','cms'),'readonly" value="'.$viewUrl.'" class="pageurl"');   
				$mform->setDefault('pageurl', $addcmsdata->pageurl); 
				
			}
			//if($addcmsdata->page_identifier=='default' or $addcmsdata->page_identifier==''){
				$mform->addElement('checkbox', 'show_footer', get_string('show_footer','cms'));
				if($addcmsdata->show_footer!=''){
					$mform->setDefault('show_footer', $addcmsdata->show_footer);
				 }else{
					$mform->setDefault('show_footer', 1);
				 }
			//}
			
			 $showResetButton = true;
			if (isset($addcmsdata->id)) {
				$mform->addElement('hidden','id');
				$mform->setDefault('id', $addcmsdata->id);
				
				$mform->addElement('hidden','createdby');
				$mform->setDefault('createdby', $addcmsdata->pageuniversity_id);
				 $showResetButton = false;
			}
			$this->add_action_buttons(true, get_string('savechanges'),  $showResetButton);	
			$mform->addElement('html','</div>');	
		}
	function validation($data) {
		if($_POST['is_external'] == 1 && $_POST['external_url'] == ''){
			$errors['external_url'] = get_string('required');
		}
		return $errors;
    }
}
?>