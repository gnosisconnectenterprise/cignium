<?php
/**
	* Message module - CMS Pages Content
	* Date Creation - 13/06/2014
	* Date Modification : 13/06/2014
	* Last Modified By : Neeraj Kumar Mourya
*/
	require_once('../config.php');
	global $USER,$CFG,$DB;

	require_once('cmslib.php');
	//require_login();
	$id = optional_param('pageid', 0, PARAM_INT);
	if($id!=''){
		$CMSPages = getCMSPages($id); 
	}	

	$strmymoodle = $CMSPages->page_title;
	
	if (isguestuser()) {  // Force them to see system default, no editing allowed
		$userid = NULL; 
		$USER->editing = $edit = 0;  // Just in case
		$context = get_context_instance(CONTEXT_SYSTEM);
		$PAGE->set_blocks_editing_capability('moodle/my:configsyspages');  // unlikely :)
		$header = "$SITE->shortname: $strmymoodle (GUEST)";
		//$header = "$SITE->shortname";	
	} else {        // We are trying to view or edit our own My Moodle page
		$userid = $USER->id;  // Owner of the page
		$context = get_context_instance(CONTEXT_USER, $USER->id);
		$PAGE->set_blocks_editing_capability('moodle/my:manageblocks');
		$header = "$SITE->shortname: $strmymoodle";
		//$header = "$SITE->shortname";
	}
	
	if (!$currentpage->userid) {
		$context = get_context_instance(CONTEXT_SYSTEM);  // So we even see non-sticky blocks
	}
	$_SESSION['Tab'] = '';
	
	// Start setting up the page
	$params = array();
	$PAGE->set_context($context);
	$PAGE->set_url('/cms/cms_content.php', $params);

	if($USER->archetype == $CFG->userTypeManager){		
		$PAGE->set_pagelayout('cmspages');		
	}else if($USER->archetype == $CFG->userTypeStudent){
		$PAGE->set_pagelayout('studentdashboard');
	}else{		
		$PAGE->set_pagelayout('cmspages');
	}
	$PAGE->set_pagetype('my-index');
	$PAGE->blocks->add_region('content');
	$PAGE->set_subpage($currentpage->id);
	$PAGE->set_title($header);
	$PAGE->set_heading($header);
	
	$PAGE->navbar->add($CMSPages->page_title);
	
	echo $OUTPUT->header();
	
	echo '<aside id="leftside">
		<div class="institue_about">
          <h2>'.$CMSPages->page_title.'</h2>
          '.$CMSPages->content.'
        </div>
    </aside>';
	 echo $OUTPUT->footer();
?>