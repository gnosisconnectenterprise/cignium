<?php
	require_once('../config.php');
	require_once($CFG->pageCmsLib);
	
	global $USER,$CFG;
	
	//if userid not available, ask user to login
	if(empty($USER->id)){
		require_login();
	}
	$context = get_context_instance(CONTEXT_SYSTEM);

	//check role assign
	if($USER->archetype != $CFG->userTypeAdmin)
	{
		redirect($CFG->wwwroot."/");
	}
	//End role assing
	$strmymoodle = get_string('cms_delete','cms');
	
	if (isguestuser()) {  // Force them to see system default, no editing allowed
		$userid = NULL; 
		$USER->editing = $edit = 0;  // Just in case
		$context = get_context_instance(CONTEXT_SYSTEM);
		$PAGE->set_blocks_editing_capability('moodle/my:configsyspages');  // unlikely :)
		//$header = "$SITE->shortname: $strmymoodle (GUEST)";
		$header = "$SITE->shortname";	
	} else {        // We are trying to view or edit our own My Moodle page
		$userid = $USER->id;  // Owner of the page
		$context = get_context_instance(CONTEXT_USER, $USER->id);
		$PAGE->set_blocks_editing_capability('moodle/my:manageblocks');
		//$header = "$SITE->shortname: $strmymoodle";
		$header = "$SITE->shortname";
	}
	
	
	// Start setting up the page
	$params = array();
	$PAGE->set_context($context);
	$PAGE->set_url('/cms/index.php', $params);
	$PAGE->set_pagelayout('general');
	$PAGE->set_pagetype('my-index');
	$PAGE->blocks->add_region('content');
	$PAGE->set_subpage($currentpage->id);
	$PAGE->set_title($header);
	$PAGE->set_heading($header);
	$PAGE->navbar->add(get_string('browse_cms','cms'), new moodle_url('/cms/index.php'));
	$PAGE->navbar->add(get_string('cms_delete','cms'));
	
	
	$id = optional_param('id', 0, PARAM_INT);
	$action = optional_param('action', 0, PARAM_INT);
	if($id!=''){
		$select = "SELECT * FROM {$CFG->prefix}cms where deleted='0' and createdby='".$USER->id."' and id='".$id."'";
		$get_cms = $DB->get_record_sql($select);
		if($get_cms=='')
		{
			redirect($CFG->wwwroot."/cms/index.php");
		}
	}	
	
	if($action=='1')
	{	
		// code below modified to delete record permanantly.
		$recordDeleteSql = "DELETE from {$CFG->prefix}cms where id='".$id."'";
		$updid = $DB->execute($recordDeleteSql);

		$sql = "SELECT id,display_order FROM {$CFG->prefix}cms where deleted='0' order by display_order asc";
		
		$result = $DB->get_records_sql($sql);
		
		// code below modified to re-order cms pages.
		$sortOrder = 1;
		foreach($result as $value){
			$newCmsClass = new stdClass();
			$newCmsClass->display_order=$sortOrder;
			$newCmsClass->id=$value->id;
			$DB->update_record('cms', $newCmsClass);
		$sortOrder++;
		}
		// Log the event entry.
		add_to_log(1, $CFG->cmsModule, 'delete', 'cms/delete_cms.php?id='.$id, '', 0, $USER->id);
		
		$_SESSION['update_msg']=get_string('record_deleted');
		redirect($CFG->wwwroot."/cms/index.php");
	}
	$message = $get_cms->page_title.'</br>';
	$message .= get_string("delete_msg");
	echo $OUTPUT->header();
	echo '<div class="box generalbox" id="notice">
	<p>'.$message.'</p><div class="buttons">
	<div class="singlebutton"><form action="'.$CFG->pageDeleteCms.'" method="post"><div><input type="submit" value="Yes"><input type="hidden" value="'.$id.'" name="id"><input type="hidden" value="1" name="action"></div></form></div><div class="singlebutton"><form action="index.php" method="post"><div><input type="submit" value="No"></div></form></div></div></div></div>';
	
	echo $OUTPUT->footer();
?>