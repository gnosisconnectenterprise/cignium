<?php
	require_once('../config.php');
	global $USER,$CFG,$DB;
		
	require_once($CFG->pageCmsLib);

    $page = optional_param('page', 1, PARAM_INT); // which page to show
	$perpage = optional_param('perpage', 10, PARAM_INT); // how many per page
	$ord      = optional_param('ord', 'asc', PARAM_TEXT);
	$searchText = (isset($_REQUEST['searchText']) ? $_REQUEST['searchText'] : '');
	if(get_string('searchtext')==$searchText)
	{
		$searchText='';
	}
	
    //if userid not available, ask user to login
    if(empty($USER->id)){
    	require_login();
    }
	$context = get_context_instance(CONTEXT_SYSTEM);
	//check role assign

	if($USER->archetype != $CFG->userTypeAdmin)
	{
		redirect($CFG->wwwroot."/");
	}

	$strmymoodle = get_string('browse_cms',"cms");
	
	$header = "$SITE->shortname: ".$strmymoodle;
	// Start setting up the page
	$params = array();
	$PAGE->set_context($context);
	$PAGE->set_url('/cms/headerpage.php', $params);
	$PAGE->set_pagelayout('listing');
	$PAGE->set_pagetype('my-index');
	$PAGE->blocks->add_region('content');
	$PAGE->set_subpage($currentpage->id);
	$PAGE->set_title($header);
	$PAGE->set_heading($header);
	
	$PAGE->navbar->add(get_string('browse_cms',"cms"));
	
	//for Active Deactive
	$id = optional_param('id', 0, PARAM_INT);
	$action = optional_param('action', 2, PARAM_INT);
	$sort = optional_param('sort', 0, PARAM_INT);
	$sta = optional_param('sta', 'no', PARAM_ALPHANUM);

	$pageURL = '/cms/headerpage.php';
	$paramArray = array(
				'ch' => optional_param('ch', '', PARAM_ALPHA),
				'key' => optional_param('key', '', PARAM_RAW),
				'page_name' => optional_param('page_name', '', PARAM_RAW),
				'page_title' => optional_param('page_title', '', PARAM_RAW)
			  );
	$filterArray = array(							
						'page_name'=>get_string("pageheadname","cms"),
						'page_title'=>get_string("pageheadtitle","cms")
				   );
	$removeKeyArray = array('id','action','sta','sort');

		////// Getting common URL for the Search List //////
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
	if($action=='1')
	{
		$newdiscussion = new stdClass();
		$newdiscussion->status=1;
		$newdiscussion->id=$id;
		$updid = $DB->update_record("header_navigation", $newdiscussion);
		$_SESSION['update_msg'] = get_string('record_updated');
		redirect($genURL);
	}elseif($action=='0')
	{
		$newdiscussion = new stdClass();
		$newdiscussion->status=0;
		$newdiscussion->id=$id;
		$updid = $DB->update_record("header_navigation", $newdiscussion);
		$_SESSION['update_msg'] = get_string('record_updated');
		redirect($genURL);
	}
	//end Active Deactive
	$sortTitleImage='arrow02.png';
	$sortNameImage='arrow02.png'; 
	
	
	$records = getHeaderPages('display_order','ASC', $page, $perpage,$paramArray);
	$cmscount = getHeaderPages('cmscount','ASC', $page, $perpage,$paramArray);
	
	echo $OUTPUT->header();
	echo '<div class="add-cms"><a class="addbutton" href="'.$CFG->wwwroot.'/cms/add_header_link.php" title="'.get_string("add_cms_page","cms").'"><span>'.get_string("add_cms_page","cms").'</span></a></div>';
	echo '<div class="category_section">';
	if($_SESSION['update_msg']!=''){
			echo '<div class="clear"></div>';
			echo '<div class="bg-success text-success text-center" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
			echo '<div class="clear"></div>';
			$_SESSION['update_msg']='';
	}
	$parameter='';
	foreach($_GET as $key=>$para){
		if($key!='ord' && $key!='sort'){
			$parameter.="$key=$para&";
		}
	}
	
	echo '<div class="add_cms"><h2 class="heading">'.get_string("manage_cms_page","cms").'</h2><table class="table1" cellpadding="0" cellspacing="0" style="width: 100%;">
	<tr class="table1_head">
	<td width="10%" class="align_left">Title</td>
	<td width="30%" class="align_left">Path</td>
	<td width="30%" align="align_left">Parent</td>	
	<td width="10%" align="align_left">Access</td>
	<td width="20%" align="align_left">Access</td>
	</tr>';
	if($records){
		$rowCount=0;
		$totalRec=count($records);
		$tooltipView=get_string("view");
		$tooltipUp=get_string("up");
		$tooltipDown=get_string("down");
		foreach($records as $key => $cms){
			$editDisable = 'edit';
			$linkEdit = $CFG->wwwroot.'/cms/add_header_link.php?id='.$cms->id;
			if($cms->status=='0')
			{	
				$classDisable = 'disable';
				$editDisable = 'edit-disable';
				$linkEdit = 'javascript:void(0);';
				$tooltip=get_string("activate");
				$status='<a href="'.$CFG->wwwroot.'/cms/headerpage.php?action=1&id='.$cms->id.'" title="'.$tooltip.'" class="enable" ></a>';
				
			}else{			
				$classDisable = 'enable';
				$tooltip=get_string("deactivatelink","cms");
				$status='<a href="'.$CFG->wwwroot.'/cms/headerpage.php?action=0&id='.$cms->id.'" class="disable" title="'.$tooltip.'" ></a>';
				
			}
			
			if(++$rowCount % 2 == 0){
				$rowClass = ' class="table_odd '.$classDisable.'"';
			}else{
				$rowClass = ' class="'.$classDisable.'"';
			}
			
			echo '<tr'.$rowClass.' >
			<td class="align_left ">'.$cms->title.'</td>
			<td class="align_left ">'.$CFG->wwwroot.$cms->path.'</td>			
			<td class="align_left ">'.$cms->parent.'</td>			
			<td class="align_left ">'.$cms->parent.'</td>			
			<td>
			<div class="adminiconsBar order">';

			echo '<a title="'.get_string("edit").'" class="'.$editDisable.'" href="'.$linkEdit.'">'.get_string("edit").'</a>';
			echo $status;				
			echo'</div>
			</td>
			</tr>';
		}
	}else{
		echo '<tr>
		<td colspan="4" align="center">'.get_string("no_results").'</td>
		</tr>';
	}	
	echo '</table></div>';
	//$removeKeyArray = array('id','flag','perpage','action','sta');
	//$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
	echo paging_bar($cmscount, $page, $perpage, $CFG->wwwroot.$pageURL);
	echo '</div>';
	
	echo $OUTPUT->footer();
?>
<script language="javascript" type="application/javascript">
setInterval(function(){$('.update_msg').slideUp();},10000);
</script>
