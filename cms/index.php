<?php
	require_once('../config.php');
	global $USER,$CFG,$DB;
		
	require_once($CFG->pageCmsLib);

    $page = optional_param('page', 1, PARAM_INT); // which page to show
	$perpage = optional_param('perpage', $CFG->perpage, PARAM_INT); // how many per page
	$ord      = optional_param('ord', 'asc', PARAM_TEXT);
	$searchText = (isset($_REQUEST['searchText']) ? $_REQUEST['searchText'] : '');
	if(get_string('searchtext')==$searchText)
	{
		$searchText='';
	}
	
    //if userid not available, ask user to login
    if(empty($USER->id)){
    	require_login();
    }
	$context = get_context_instance(CONTEXT_SYSTEM);
	//check role assign

	if($USER->archetype != $CFG->userTypeAdmin)
	{
		redirect($CFG->wwwroot."/");
	}

	$strmymoodle = get_string('browse_cms',"cms");
	
	$header = "$SITE->shortname: ".$strmymoodle;
	// Start setting up the page
	$params = array();
	$PAGE->set_context($context);
	$PAGE->set_url('/cms/index.php', $params);
	$PAGE->set_pagelayout('listing');
	$PAGE->set_pagetype('my-index');
	$PAGE->blocks->add_region('content');
	$PAGE->set_subpage($currentpage->id);
	$PAGE->set_title($header);
	$PAGE->set_heading($header);
	
	$PAGE->navbar->add(get_string('browse_cms',"cms"));
	
	//for Active Deactive
	$id = optional_param('id', 0, PARAM_INT);
	$action = optional_param('action', 2, PARAM_INT);
	$sort = optional_param('sort', 0, PARAM_INT);
	$sta = optional_param('sta', 'no', PARAM_ALPHANUM);

	$pageURL = '/cms/index.php';
	$paramArray = array(
				'ch' => optional_param('ch', '', PARAM_ALPHA),
				'key' => optional_param('key', '', PARAM_RAW),
				'page_name' => optional_param('page_name', '', PARAM_RAW),
				'page_title' => optional_param('page_title', '', PARAM_RAW)
			  );
	$filterArray = array(							
						'page_name'=>get_string("pageheadname","cms"),
						'page_title'=>get_string("pageheadtitle","cms")
				   );
	$removeKeyArray = array('id','action','sta','sort');

		////// Getting common URL for the Search List //////
	$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
	if($action=='1')
	{
		$newdiscussion = new stdClass();
		$newdiscussion->status=1;
		$newdiscussion->id=$id;
		$updid = $DB->update_record("cms", $newdiscussion);
		
		// Log the event entry.
		add_to_log(1, $CFG->cmsModule, 'active', 'cms/index.php?action=1&id='.$id, '', 0, $USER->id);
				
		$_SESSION['update_msg'] = get_string('record_updated');
		redirect($genURL);
		
	}elseif($action=='0')
	{
		$newdiscussion = new stdClass();
		$newdiscussion->status=0;
		$newdiscussion->id=$id;
		$updid = $DB->update_record("cms", $newdiscussion);
		// Log the event entry.
		add_to_log(1, $CFG->cmsModule, 'deactive', 'cms/index.php?action=0&id='.$id, '', 0, $USER->id);
		$_SESSION['update_msg'] = get_string('record_updated');
		redirect($genURL);
	}

	if($sta!='no'){
		$select = "SELECT * FROM {$CFG->prefix}cms WHERE id = '$id'";
		$get_cms = $DB->get_record_sql($select);
		$sortorder = $get_cms->display_order;
		
		if($sta=='up'){
			$newdisplayorder = $sortorder-1;
			
			$select = "SELECT * FROM {$CFG->prefix}cms WHERE display_order= '$newdisplayorder' and deleted=0";
			
			$get_cms = $DB->get_record_sql($select);
			$editObj = new stdClass(); 
			$editObj->display_order=$sortorder;
			$editObj->id=$get_cms->id;
			
			$updid = $DB->update_record_raw("cms", $editObj);
			
			$editObj = new stdClass();
			$editObj->display_order=$newdisplayorder;
			$editObj->id=$id;
			
			$updid = $DB->update_record_raw("cms", $editObj);
			$_SESSION['update_msg'] = get_string('record_updated');
			redirect($genURL);
			
		}if($sta=='down'){
			$newdisplayorder=$sortorder+1;			
			$select = "SELECT * FROM {$CFG->prefix}cms WHERE display_order= '$newdisplayorder' and deleted=0";
			$get_cms = $DB->get_record_sql($select);
			$editObj = new stdClass();
			$editObj->display_order=$sortorder;
			$editObj->id=$get_cms->id;   
			$updid = $DB->update_record_raw("cms", $editObj);
			
			$editObj = new stdClass();
			$editObj->display_order=$newdisplayorder;
			$editObj->id=$id;
			$updid = $DB->update_record_raw("cms", $editObj);	
			$_SESSION['update_msg'] = get_string('record_updated');
			redirect($genURL);		
		}		
		/**/
	}
	//end Active Deactive
	$sortTitleImage='arrow02.png';
	$sortNameImage='arrow02.png'; 
	
	
	$records = get_Cms_Pages('display_order','ASC', $page, $perpage,$paramArray);
	$cmscount = get_Cms_Pages('cmscount','ASC', $page, $perpage,$paramArray);
	echo $OUTPUT->header();
	//echo genCommonSearchForm($paramArray, $filterArray, $pageURL);
	echo '<div class=""><a class="button-link add-cms" href="'.$CFG->wwwroot.'/cms/'.$CFG->pageAddCms.'" ><i></i><span>'.get_string("add_cms_page","cms").'</span></a></div>';
	if($_SESSION['update_msg']!=''){
			echo '<div class="clear"></div>';
			echo '<div class="bg-success text-success text-center" style = "display:block !important;">'.$_SESSION['update_msg'].'</div>';
			echo '<div class="clear"></div>';
			$_SESSION['update_msg']='';
	}
	echo '<div class="borderBlock">';
	
	//echo '<a class="addbutton" href="'.$CFG->wwwroot.'/home/index.php" title="'.get_string("managebanner").'"><span>'.get_string("managebanner").'</span></a>';
	//echo '<a class="addbutton" href="'.$CFG->wwwroot.'/admin/tool/customlang/index.php" title="'.get_string("language_custom").'"><span>'.get_string("language_custom").'</span></a>';
	$parameter='';
	foreach($_GET as $key=>$para){
		if($key!='ord' && $key!='sort'){
			$parameter.="$key=$para&";
		}
	}
	//echo '<p class="table_title">'.get_string("browse_cms").'</p>';
	
	echo '<div class=""><h2 class="icon_title">'.get_string("manage_cms_page","cms").'</h2><div class="borderBlockSpace"><table class="table1" cellpadding="0" cellspacing="0" style="width: 100%;">
	<tr class="table1_head">
	<td width="20%" class="align_left">'.get_string("pageheadname","cms").'</td>
	<td width="42%" class="align_left">'.get_string("pageheadtitle","cms").'</td>	
	<td width="38%" align="align_left">'.get_string("manage").'</td>
	</tr>';
	if($records){
		$rowCount=0;
		$totalRec=count($records);
		$tooltipView=get_string("preview");
		$tooltipUp=get_string("up");
		$tooltipDown=get_string("down");
		foreach($records as $key => $cms){
			$editDisable = 'edit';
			$linkEdit = $CFG->wwwroot.'/cms/'.$CFG->pageAddCms.'?id='.$cms->id;
			if($cms->status=='0')
			{	
				$classDisable = 'disable';
				$editDisable = 'edit-disable';
				$linkEdit = 'javascript:void(0);';
				//$view = '<a href="'.$CFG->wwwroot.'/cms/cms_content.php?pageid='.$cms->id.'" title="'.$tooltipView.'" target="_blank" class="view" >'.get_string("preview").'</a> ';
				$tooltip=get_string("activate");
				$status='<a href="'.$CFG->wwwroot.'/cms/index.php?action=1&id='.$cms->id.'" title="'.$tooltip.'" class="enable" ></a>';
				
			}else{			
				$classDisable = 'enable';
				$tooltip=get_string("deactivatelink","cms");
				$status='<a href="'.$CFG->wwwroot.'/cms/index.php?action=0&id='.$cms->id.'" class="disable" title="'.$tooltip.'" ></a>';
				
			}
			if($cms->is_external == 1){
				$targetLink = $cms->external_url;
			}else{
				$targetLink = $CFG->wwwroot.'/cms/cms_content.php?pageid='.$cms->id;
			}
			$view = '<a href="'.$targetLink.'" title="'.$tooltipView.'" target="_blank" class="view" >'.get_string("preview").'</a> ';
			if(++$rowCount % 2 == 0){
				$rowClass = ' class="table_odd '.$classDisable.'"';
			}else{
				$rowClass = ' class="'.$classDisable.'"';
			}
			
			echo '<tr'.$rowClass.' >
			<td class="align_left ">'.$cms->page_name.'</td>
			<td class="align_left ">'.$cms->page_title.'</td>			
			<td>
			<div class="adminiconsBar order">';

			echo $view.'<a title="'.get_string("edit").'" class="'.$editDisable.'" href="'.$linkEdit.'">'.get_string("edit").'</a>';
			echo $status;
			echo '<a title="'.get_string("delete").'" class="delete delete_cms" href="'.$CFG->wwwroot.'/cms/'.$CFG->pageDeleteCms.'?id='.$cms->id.'&action=1" >'.get_string("delete").'</a> ';
				if($rowCount==1 && $totalRec != 1){
					
					echo '<a href="'.$CFG->wwwroot.'/cms/index.php?sta=down&id='.$cms->id.'" class="down" title="'.$tooltipDown.'">'.get_string("down").'</a> ';
				}else if($rowCount==$totalRec && $totalRec != 1){
					echo '<a href="'.$CFG->wwwroot.'/cms/index.php?sta=up&id='.$cms->id.'" class="up" title="'.$tooltipUp.'" >'.get_string("up").'</a> ';
				}else{
					if($totalRec != 1){
						echo '<a href="'.$CFG->wwwroot.'/cms/index.php?sta=up&id='.$cms->id.'" class="up" title="'.$tooltipUp.'" >'.get_string("up").'</a> ';
						echo '<a href="'.$CFG->wwwroot.'/cms/index.php?sta=down&id='.$cms->id.'" class="down"  title="'.$tooltipDown.'" >'.get_string("down").'</a> ';
					}
				}
				
			echo'</div>
			</td>
			</tr>';
		}
	}else{
		echo '<tr>
		<td colspan="4" align="center">'.get_string("no_results").'</td>
		</tr>';
	}	
	echo '</table></div></div></div>';
	//$removeKeyArray = array('id','flag','perpage','action','sta');
	//$genURL = genParameterizedURL($paramArray, $removeKeyArray, $pageURL);
	echo paging_bar($cmscount, $page, $perpage, $CFG->wwwroot.$pageURL);
	echo '';
	
	echo $OUTPUT->footer();
?>
<script language="javascript" type="application/javascript">
setInterval(function(){$('.update_msg').slideUp();},10000);
</script>
<script>


$(document).ready(function(){


	$(".delete_cms").click(function(){
		
		var slotid = $(this).attr("rel");
		var txt = "<?php echo get_string('cmsdeleteconfirm')?>";
		
		var response = confirm(txt);
		
		if (response == true) {
		return true;
		}
		return false;
	});
	
	

	
});

</script>