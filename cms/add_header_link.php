<?php
	require_once('../config.php');
	require_once('add_header_link_form.php');
	
	global $USER,$CFG;

	$strmymoodle = get_string('add_cms','cms');
	$header = "$SITE->shortname";	
	
	$editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES, 'maxbytes'=>$CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true);
	

	// Start setting up the page
	$id = optional_param('id', 0, PARAM_INT);
	$returnto = optional_param('returnto', 0, PARAM_ALPHANUM);
	$params = array();
	$PAGE->set_context($context);
	$PAGE->set_url('/cms/index.php', $params);
	$PAGE->set_pagelayout('general');
	$PAGE->set_pagetype('my-index');
	$PAGE->blocks->add_region('content');
	$PAGE->set_subpage($currentpage->id);
	
	$PAGE->set_heading($header);
	$PAGE->navbar->add(get_string('browse_cms','cms'), new moodle_url('/cms/index.php'));
	if($id==0){
		$strmymoodle = get_string('addcms',"cms");
		$PAGE->navbar->add($strmymoodle);
		$PAGE->set_title($header.": ".$strmymoodle);
	}
	else{
		$strmymoodle = get_string('edit_cms',"cms");
		$PAGE->navbar->add(get_string('edit_cms','cms'));
		$PAGE->set_title($header.": ".$strmymoodle);
	}
	echo $OUTPUT->header();
	$addcms_form = new addcms_form('add_header_link.php',$addcmsdata);
	$back_url=$CFG->wwwroot."/cms/headerpage.php";
	
	
	if($id != 0){
		$select = "SELECT * FROM {$CFG->prefix}header_navigation WHERE id = $id";
		$get_cms = $DB->get_record_sql($select);
		$addcmsdata = new stdClass();   
		$addcmsdata->title = $get_cms->title;
		$addcmsdata->path = $get_cms->path;
		$addcmsdata->link_class = $get_cms->link_class;
		$addcmsdata->parent = $get_cms->parent;
		$addcmsdata->is_super = $get_cms->is_super;
		$addcmsdata->is_manager = $get_cms->is_manager;
		$addcmsdata->is_learner = $get_cms->is_learner;
		$addcmsdata->id = $id;
		$addcmsdata->returnto=$returnto;
		$addcms_form = new addcms_form('add_header_link.php',$addcmsdata);
		$addcms_form->set_data($addcmsdata);
	}
	else{
		$attachmentoptions = array('subdirs'=>false, 'maxfiles'=>99, 'maxbytes'=>$maxbytes);
	
		$addcms_form = new addcms_form(NULL, array('editoroptions'=>$editoroptions, 'returnto'=>$returnto, 'attachmentoptions'=>$attachmentoptions));
	}
	if($addcms_form->is_cancelled()) {		
		redirect($back_url);
    }else if($cmsdata= $addcms_form->get_data()){	
			if($cmsdata->id!=''){
				$nid=update_headerpage($cmsdata);
				$_SESSION['update_msg'] = get_string('record_updated');
				redirect($back_url);	
			}else{
				$nid=insert_headerpage($cmsdata);
				$_SESSION['update_msg'] = get_string('record_added');
				redirect($back_url);	
			}
			
		}	
	
	echo '<div class="left_content">
			<div class="category_section">';
	$addcms_form->display(); 
	echo '</div></div>';
	echo $OUTPUT->footer();
?>