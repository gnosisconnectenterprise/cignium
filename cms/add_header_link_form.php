<?php
	require_once('../config.php');	
	global $CFG;
	require_once($CFG->libdir.'/formslib.php');
	require_once($CFG->pageCmsLib);
	
	class addcms_form extends moodleform{
		//This method contains definition of the form i.e. all the elements to be displayed
		function definition() {
			//Defining global objects User and Configuration		
			global $USER, $CFG,$DB;
			//Form object
			$mform =& $this->_form;
			//data received to populate this form   
			$addcmsdata = $this->_customdata;
			if (!isset($addcmsdata->id)){ 
				$editoroptions = $this->_customdata['editoroptions'];
			}
			else{
				$editoroptions = $addcmsdata->editoroptions;
			}
			
			if (!isset($addcmsdata->id)) {
				//$mform->addElement('header','create_cms', get_string('addcms','cms'));
			}else{
				//$mform->addElement('header','create_cms', get_string('edit_cms','cms'));
			}
			$mform->addElement('text','title', 'Title','maxlength="50" size="50"');
			
			$mform->setDefault('title', $addcmsdata->title);      
			$mform->addRule('title', get_string('required'), 'required', null, 'server');
			
			$mform->addElement('text','path', 'path','maxlength="50" size="50"');   
			$mform->setDefault('path', $addcmsdata->path);           
			$mform->addRule('path', get_string('required'), 'required', null, 'server');

			$mform->addElement('text','link_class', 'link class','maxlength="50" size="50"');   
			$mform->setDefault('link_class', $addcmsdata->link_class);

			$mform->addElement('checkbox', 'is_super', 'is_super');
			$mform->addElement('checkbox', 'is_manager', 'is_manager');
			$mform->addElement('checkbox', 'is_learner', 'is_learner');
			$where ='';
			if($addcmsdata->id){
				$where  = ' AND id != '.$addcmsdata->id;
			}
			$departments = $DB->get_records_sql("SELECT id,title FROM {$CFG->prefix}header_navigation WHERE parent = 0 AND status = 1".$where);
			$departmentselect = array('0'=>get_string('select_department'));
			if(!empty($departments)){
				foreach($departments as $department){
					$departmentselect[$department->id] = $department->title;
				}
			}
			$mform->addElement('select', 'parent','parent', $departmentselect);
			
			if (isset($addcmsdata->id)) {
				$mform->addElement('hidden','id');
				$mform->setDefault('id', $addcmsdata->id);
				
				$mform->addElement('hidden','createdby');
				$mform->setDefault('createdby', $addcmsdata->pageuniversity_id);
			}
			$this->add_action_buttons(true, get_string('save_button',"cms"));		
		}
	function validation($data) {
		$errors = array();
		return $errors;
    }
}
?>