<!--#########Code Start to implement SAML Authentication########-->
<?php
if($_REQUEST['logintype']!='external'){
        require_once (dirname(__FILE__) . '/../simplesaml/lib/_autoload.php'); 
	
	$as = new SimpleSAML_Auth_Simple('transishun-sp');

	if (array_key_exists('logout', $_REQUEST)) {		
		$as->logout(SimpleSAML_Utilities::selfURLNoQuery());
		
	}

	if (array_key_exists('login', $_REQUEST)) {

		$as->requireAuth();

	}
		
	$isAuth = $as->isAuthenticated();

	$attributes = $as->getAttributes();
	//print_r($attributes);die;
             
}

require_once('../config.php');
require_once($CFG->dirroot .'/course/lib.php');
require_once($CFG->libdir .'/filelib.php');
$logintype = optional_param('logintype','internal',PARAM_RAW);
$errmsg = "";
if ($isAuth){
            
            if(is_array($attributes) && !empty($attributes)){
                
                foreach($attributes as $k=>$v){
                    $pos = strrpos($k, "/name");
                    if ($pos !== false) {

                            $userEmail = $v[0];
                    }
                    
                    if($userEmail!=''){
						
                        if ($userdata = $DB->get_record('user',array('username'=>$userEmail))) {
                            redirect($CFG->wwwroot.'/login/autologin.php?user_id='.$userdata->id);
                        }else{
                            $errmsg = "Username ".$userEmail." doesn't exists.";
							
                           // die("email id doesn't exists in the system");
                        }
                    }
                }
            }
            
        }  	
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $CFG->siteTitle;?></title>

<link rel="shortcut icon" type="image/x-icon" href="<?php echo $CFG->wwwroot;?>/home/images/favicon.ico">
<link rel="stylesheet" type="text/css" href="index.css" />
<link href="<?php echo $CFG->wwwroot;?>/home/jquery.bxslider.css" rel="stylesheet" />
<script src="<?php echo $CFG->wwwroot;?>/home/js/jquery.min.js"></script>
<script src="<?php echo $CFG->wwwroot;?>/home/js/jquery.bxslider.min.js"></script>
</head>
<body>
<div class="portraitMessage">This website is viewable in landscape mode only.</div>
<div class="wrapper">
  <div class="homePage">
  	<div id="page">
           
            <div class="logoform"><img src="<?php echo $CFG->wwwroot;?>/home/images/tranzact_logo.png"/></div>
    	<h1>Welcome to the<span> Learning Portal!</span></h1>
    	
    	<div role="dialog" class="login-box" <?php echo $displayForm;?> >
              <div class="clear"></div>
              <?php
				$displayForm = "";
				
				$loginlinkShow = '';
				if($SESSION->loginerrormsg){
					$displayForm = 'style="display: block;"';
					$errmsg = $SESSION->loginerrormsg;
					$loginlinkShow = 'login';
					$SESSION->loginerrormsg = '';
				}
				
			  ?>
              <div class="logo-right" <?php echo $displayForm;?> >
                <div id="loginform">
                   <?php 
					if($CFG->isLdap==1){
					$authldap_skipntlmsso = optional_param('authldap_skipntlmsso', 0, PARAM_INT);
					$loginActionUrl = $CFG->wwwroot.'/login/index.php?authldap_skipntlmsso='.$authldap_skipntlmsso;
					$loginInfoText = '*User your windows credential to login.';
					} 
					else{
					$loginActionUrl = $CFG->wwwroot.'/login/index.php';
					$loginInfoText = "";
					}
					
                                         if(isset($_SESSION['sess_logintype'])){                                         
                                                $logintype = isset($_SESSION['sess_logintype'])?$_SESSION['sess_logintype']:"internal";
                                                $_SESSION['sess_logintype'] = '';
                                                unset($_SESSION['sess_logintype']);                                        
                                         }					
					?>
                  <form action="<?php echo $loginActionUrl ?>" method="POST" id = "login-form">
<!--					<div class="usertype">
						<span class="internalUser logintype"><input type="radio" name="logintype" value="Internal User" <?php echo $checkedinternal;?> > Tranzact User</span>
						<span class="externalUser logintype"><input type="radio" name="logintype" value="External User" <?php echo $checkedexternal;?>> External User</span>
					</div>-->
                      
                    <input type="hidden" name="logintype" id="logintype" value="<?php echo $logintype; ?>"> 
                   
                  
                    <span class = 'username-error'><?php echo $errmsg;?></span>
                                        
                    <label for="login-email" class="hide_text">Login-Email</label>
                    <input name="username" type="text" value="Username" onblur="if(this.value==''){this.value='Username'}" onfocus="if(this.value=='Username'){this.value=''}" autocomplete="off" id = "login-email"/>
                    <div class="clear"></div>
                    <span class = 'password-error'></span>
                    <label for="login-password" class="hide_text">Login-password</label>
                    <input name="password" type="password" value="Password" onblur="if(this.value==''){this.value='Password'}" onfocus="if(this.value=='Password'){this.value=''}" autocomplete="off" id = "login-password"/>
                    <div class="clear"></div>
                    <label for="log-in" class="hide_text">login</label>
                    <input name="Login" type="submit" value="Login" id="log-in"/>
                    <span class="f-left">
                    <div class="rememberpass">
                      <input type="checkbox" value="1" id="rememberusername" name="rememberusername">
                      <label for="rememberusername">Keep me signed in</label>
                    </div>
                   
                    </span>
                    <div class="clear"></div>
                    <div class="forgetbox">
						<?php if($CFG->isLdap==0){
						echo '<span class="f-left forgot-password"> <a href="javascript:void(0)">Forgot Password?</a> </span>';
						}
						?>
                      	<a href="<?php echo $CFG->wwwroot;?>/docs/Company_Resources_Contacts.pdf" target="_blank" class="hrc">Company Resources</a>
                      </div>
                     <div class="clear"></div>
                      <div class="login_info" style="margin-left: 10px;"> <?php echo $loginInfoText; ?></div>
                      
                     <?php echo getExternalDepartmentHtml(2, $loginTypeChecked);?>
                       
                  </form>
                  
                </div>
                <div class="clear"></div>
                <div id="forgot-password-form" style="display:none">
                  <form action="<?php echo $CFG->wwwroot;?>/login/forgot_password.php" method="POST" id = "forgot-passwordform">
                    <span class = 'email-error'></span>
                     <label for="forgot-passowrd-email" class="hide_text">forgot-passowrd-email</label>
                    <input name="email" type="text" value="Email / Username" onblur="if(this.value==''){this.value='Email / Username'}" onfocus="if(this.value=='Email / Username'){this.value=''}" autocomplete="off"  id = "forgot-passowrd-email"/>
                    <div class="clear"></div>
                    <label for="forgot-password-click" class="hide_text">forgot-password-click</label>
                    <input name="search" type="submit" value="Submit" id = "forgot-password-click"/>
                    <a href="javascript:void(0)"  class="f-left forgot-login-password">Click to Login</a> 
                  </form>
                </div>
              </div>
            </div>
    </div>
  </div>
  <footer>
    <div id="page">
     <!-- <div class="f-left">Copyright &copy; . All rights reserved.</div>-->
	  <div class="arrow"></div>
      <div class="f-right botm_foot">Powered By GnosisConnect LMS<span></span></div>
	</div>
      <!--<div class="arrow"></div>-->
      <!--<div class="f-right">
        <?php 
				$CMSPages = getFooterCMSPages(); 
				if(count($CMSPages) > 0){
				                   
                        foreach($CMSPages as $pages){
							if($pages->is_external == 1){
								$target = 'target = "_blank"';
								$targetLink = $pages->external_url;
							}else{
								$target = '';
								$targetLink = $CFG->wwwroot.'/cms/cms_content.php?pageid='.$pages->id;
							}
						?>
                            <a href="<?php echo $targetLink;?>" title="<?php echo $pages->page_name;?>" <?php echo $target;?>><?php echo $pages->page_name;?></a>
        <?php }   
                        
                 } ?>
                 </div>-->
                 
               <!--  
                 <?php 
				$CMSPages = getFooterCMSPages(); 
				if(count($CMSPages) > 0){
					$addCss = '';
					if($CFG->showPoweredBy == 0){
						$addCss = " pull-right";
					}
				?>
                    <div class="footer-menu <?php echo $addCss;?>">
                        <ul>
                        <?php 
						$OthersLi = '';
						$OthersLiStart = '';
						$OthersLiEnd = '';
						$cmsCount = 1;
						$totalCmsPages = count($CMSPages);
						if($totalCmsPages > 3){
							$OthersLiStart	= '<li class="linkPopup"><a href="javascript:void(0);"><span class="hide_text">Additional Items</span></a><ul>';
							$OthersLiEnd 	= '</ul></li>';
						}
						foreach($CMSPages as $pages){
								if($pages->is_external == 1){
									$target = 'target = "_blank"';
									$targetLink = $pages->external_url;
								}else{
									$target = '';
									$targetLink = $CFG->wwwroot.'/cms/cms_content.php?pageid='.$pages->id;
								}
								if($cmsCount>3){	
									$OthersLi .= '<li><a href="'.$targetLink.'" title="'.$pages->page_name.'" '.$target.'>'.$pages->page_name.'</a></li>';
								}else{
							?>
                            	<li><a href="<?php echo $targetLink;?>" title="<?php echo $pages->page_name;?>" <?php echo $target;?>><?php echo $pages->page_name;?></a></li>
                        	<?php
							}
							$cmsCount++;
						 } 
						 echo $OthersLiStart.$OthersLi.$OthersLiEnd;
						 ?>    
                        </ul>
                    </div>
                    
                <?php } ?>   
                 
                 -->
                 
      
  </footer>
  <div class="drop" title="Top"></div>
</div>


<script type="text/javascript">

$(document).ready(function(){
 
    var logintype = $("#logintype").val();
   
    if(logintype=="internal"){
        $('#login-password').fadeOut();
        $('.forgot-password').fadeOut();   
    }else{
        $('#login-password').fadeIn();
        $('.forgot-password').fadeIn();
    } 


    
	if($('.username-error').length > 0 && $('.username-error').html() != '' && $('.password-error').length > 0 && $('.password-error').html() == ''){
	  $('.password-error').hide();
	}
	$('.bx-default-pager').remove();
	pageHeight();
});
function pageHeight(){
	var footerHeight = $('footer').height();
	var winHeight = $(window).height();
	var homePageHeight = winHeight-footerHeight;
	$('.homePage').css('height',homePageHeight);
       var pageheight = $('.homePage #page').height();
       
       $('.homePage #page').css('margin-top',((homePageHeight-pageheight)/2)-53);
}
$(window).resize(function() {
	pageHeight();
})

$(window).bind("load", function() {
	var hgt = $('.bx-viewport').css('height');
	$('.pos-fix').css('height',parseInt(hgt)+'px');
	$('.slider-five-bts-bx').fadeIn();
	

	if($('.username-error').length > 0 && $('.username-error').html() != ''){
	  $('.login-open').addClass('show');
	}

});

$('.callus-now a').click(function(){
	$('.callus-now-bx').toggle('slow');	
});

$('.drop').click(function(){
  $('html,body').animate({scrollTop:"0px"},'slow')
});
$(window).scroll(function(){
if($(this).scrollTop() >0){
	$('.drop').show();
}
else{
	$('.drop').hide();
}
});

$('a.login-open').click(function(){
	$(this).toggleClass('show');
	$('.login-box').toggle('slow');
	$(this).parent('.login-link').toggleClass('login');
	$(this).parent('.login-link').find('.right-down-arrow').toggleClass('right-close-arrow');
	$('.password-error').html();
	$('.password-error').hide();
	$('.username-error').html('');
	//$('.username-error').hide();
	$('.email-error').hide();
	var forgotEmail = $.trim($("#forgot-passowrd-email").val());
	var email = $.trim($("#login-email").val());
	var password = $.trim($("#login-password").val());
	if(email != '' && email.toLowerCase () != 'username' ){
		$("#login-email").val('Username');
	}
	if(forgotEmail != '' && forgotEmail != 'Email / Username' ){
		$("#forgot-passowrd-email").val('Email / Username');
	}
	if(password != '' && password.toLowerCase () != 'password' ){
		$("#login-password").val('Password');
	}
});

$('.forgot-password a').click(function(){
	$(this).parents('#loginform').hide('slow');
	$(this).parents('.login-box').find('#forgot-password-form').show('slow');
	$('.email-error').hide();
	var forgotEmail = $.trim($("#forgot-passowrd-email").val());
	$("#forgot-passowrd-email").val('Email / Username');
});

$('.forgot-login-password').click(function(){
        $('.username-error,.password-error').hide();
	$(this).parents('#forgot-password-form').hide('slow');
	$(this).parents('.login-box').find('#loginform').show('slow');
	var forgotEmail = $.trim($("#forgot-passowrd-email").val());
	var email = $.trim($("#login-email").val());
	var password = $.trim($("#login-password").val());
	$("#login-email").val('Username');
	$("#login-password").val('Password');
});

$(document).on("click","#forgot-password-click",function(event){
	event.preventDefault();
	var email = $.trim($("#forgot-passowrd-email").val());
	var err = 0;
	var msg = '';
	if(email == '' || email == 'Email / Username' ){
		msg = "<?php echo get_string('provide_email_username'); ?>";
		$('.email-error').html(msg);
		$('.email-error').show();
		return false;
	}
	$.ajax({
			url:'<?php echo $CFG->wwwroot;?>/login/forgot_password.php?email='+email,
			type:'POST',
			success:function(data){
				if(data == 0 || data == '0'){
					msg = "<?php echo get_string('provide_email_username'); ?>";
					$('.email-error').html(msg);
					$('.email-error').show();
					return false;
				}else if(data == 1 || data == '1'){
					msg = "<?php echo get_string('user_doesnot_exists'); ?>";
					$('.email-error').html(msg);
					$('.email-error').show();
					return false;
				}else if(data == 'SUCCESS'){
					msg = "<?php echo get_string('mail_have_been_sent'); ?>";
					$('.email-error').addClass('success');
					$('.email-error').html(msg);
					$('.email-error').show();
					return false;
				}else if(data == 'FAILED'){
					msg = "<?php echo get_string('error_sending_email'); ?>";
					$('.email-error').addClass('success');
					$('.email-error').html(msg);
					$('.email-error').show();
					return false;
				}
			}
	});
	return false;
});
$(document).on("click","#log-in",function(event){
	event.preventDefault();
	var email = $.trim($("#login-email").val());
	var password = $.trim($("#login-password").val());
	var err = 0;
	var msg = '';
	if(email == '' || email.toLowerCase () == 'username' ){
		msg = "<?php echo get_string('provide_username'); ?>";
		$('.username-error').html(msg);
		$('.username-error').show();
		err = 1;
	}else{
		$('.username-error').html('');
		//$('.username-error').hide();
		//err = 0;
	}
	var logintype = $("#logintype").val();
        //alert(usertypes);
	if((password == '' || password.toLowerCase ()== 'password')&& logintype=='external' ){
		msg = "<?php echo get_string('provide_password'); ?>";
		$('.password-error').html(msg);
		$('.password-error').show();
		err = 1;
	}else{
		$('.password-error').html();
		$('.password-error').hide();
		//err = 0;
	}
	if(err == 1){
		return false;
	}else{
                if(logintype=='external'){
                    $("#login-form").submit();
                }else{
                    var username = $("#login-email").val();
                    $.ajax({
			url:'<?php echo $CFG->wwwroot;?>/login/validateuser.php?username='+username,
			type:'POST',
			success:function(data){
                          
                          var obj = $.parseJSON(data);
                          //alert(obj.error);return false;
				if(obj.error == 1){
					msg = obj.error_message;
					$('.username-error').html(msg);
					$('.username-error').show();
					return false;
				}else{
                                    window.location.href = '?login';
                                }
			}
                    });
                   // 
                }
		
	}
});

$(".linkPopup > a").on('click',function(event){
	$(".contactIcon").next('ul').hide();
	$(this).next('ul').toggle();
	return false;
});
$(".contactIcon").on('click',function(event){
	$(".linkPopup > a").next('ul').hide();
	$(this).next('ul').toggle();
	return false;
});
$(document).on('click',function(event){
	$(".linkPopup > a,.contactIcon").next('ul').hide();
})
$(".internalUser input[name='logintype']").click(function(){
    
    $('#login-password').fadeOut();
    $('.forgot-password').fadeOut();
   
    $('.password-error').hide();
    $('.username-error').hide();
});
$(".externalUser input[name='logintype']").click(function(){   
    $('#login-password').fadeIn();
    $('.forgot-password').fadeIn();
    $('.password-error').hide();
    $('.username-error').hide();
   
});

</script>
</body>
</html>
