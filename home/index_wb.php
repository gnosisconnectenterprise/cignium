<?php 
    require_once('../config.php');
    require_once($CFG->dirroot .'/course/lib.php');
    require_once($CFG->libdir .'/filelib.php');	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="<?php echo $CFG->wwwroot;?>/home/images/favicon.ico">
<link rel="stylesheet" type="text/css" href="index_wb.css" />
<title>GnosisConnect LMS</title>
<!-- jQuery library (served from Google) -->
<script src="<?php echo $CFG->wwwroot;?>/home/js/jquery.min.js"></script>
<!-- bxSlider Javascript file -->
<script src="<?php echo $CFG->wwwroot;?>/home/js/jquery.bxslider.min.js"></script>
<!-- bxSlider CSS file -->
<link href="<?php echo $CFG->wwwroot;?>/home/jquery.bxslider_wb.css" rel="stylesheet" />
</head>
<body>
<div class="wrapper">
  <!--Header-->
  <ul class="bxslider">
    <li><img src="<?php echo $CFG->wwwroot;?>/home/images/banner_bg.png" /></li>
    <li><img src="<?php echo $CFG->wwwroot;?>/home/images/banner_bg.png" /></li>
  </ul>
  <div class="pos-fix">
    <div class="header">
      <div id="page">
        <h1 class="f-left"><a href="<?php echo $CFG->wwwroot;?>/index.php" title = "GnosisConnect LMS"> </a></h1>
        <!--Top Nav-->
        <ul class="collapse-navbar f-right">
              <li class="login-link  <?php echo $loginlinkShow;?>">
              <span class="lock_icon"></span>
            <?php if(isset($USER->id) && $USER->id != 0){ ?>
              <a href="<?php echo $CFG->wwwroot;?>/login/logout.php?sesskey=<?php echo sesskey();?>" title="Logout"><?php echo "Logout"; ?></a>
            <?php }else{ ?>
            <!--<a href="<?php echo $CFG->wwwroot;?>/login" title="Login"><?php echo "Login";?></a> -->
			<a href="#" class="login-open">Login</a>
            <?php }?>
            <!--<i class="right-down-arrow"></i>-->
            <div class="login-box" <?php echo $displayForm;?> >
              <div class="clear"></div>
              <!--<div class="logo-left"></div>-->
              <div class="logo-right" <?php echo $displayForm;?> >
                <div id="loginform">
                  <form action="<?php echo $CFG->wwwroot;?>/login/index.php" method="POST" id = "login-form">
                    <span class = 'username-error'><?php echo $errmsg;?></span>
                    <input name="username" type="text" value="Username" onblur="if(this.value==''){this.value='Username'}" onfocus="if(this.value=='Username'){this.value=''}" autocomplete="off" id = "login-email"/>
                    <div class="clear"></div>
                    <span class = 'password-error'></span>
                    <input name="password" type="password" value="Password" onblur="if(this.value==''){this.value='Password'}" onfocus="if(this.value=='Password'){this.value=''}" autocomplete="off" id = "login-password"/>
                    <div class="clear"></div>
                    <input name="Login" type="submit" value="Login" id = "log-in"/>
                    <span class="f-left">
                    <div class="rememberpass">
                      <input type="checkbox" value="1" id="rememberusername" name="rememberusername">
                      <label for="rememberusername">Keep me signed in</label>
                    </div>
                    </span>
                    <div class="clear"></div>
                    <span class="f-left forgot-password"> <a href="javascript:void(0)">Forgot Password?</a> </span>
                  </form>
                </div>
                <div class="clear"></div>
                <div id="forgot-password-form" style="display:none">
                  <form action="<?php echo $CFG->wwwroot;?>/login/forgot_password.php" method="POST" id = "forgot-passwordform">
                    <span class = 'email-error'></span>
                    <input name="email" type="text" value="Email / Username" onblur="if(this.value==''){this.value='Email / Username'}" onfocus="if(this.value=='Email / Username'){this.value=''}" autocomplete="off"  id = "forgot-passowrd-email"/>
                    <div class="clear"></div>
                    <input name="search" type="submit" value="Submit" id = "forgot-password-click"/>
                    <span class="f-left forgot-login-password"> <a href="javascript:void(0)">Click to Login</a> </span>
                  </form>
                </div>
              </div>
            </div>
          </li>
        </ul>
        <!--Top Nav End-->
        <div class="clear"></div>
        <!--Main Nav Start-->
        <!--<div class="search f-right"><a href="javascript:void(0);"></a></div>
    <ul class="nav">
      <li><a href="javascript:void(0);">Home</a></li>
      <li><a href="javascript:void(0);">Solutions</a></li>
      <li><a href="javascript:void(0);">Services</a></li>
      <li><a href="javascript:void(0);">Industries</a></li>
      <li><a href="javascript:void(0);">Resources</a></li>
      <li><a href="javascript:void(0);">Contact Us</a></li>
    </ul>-->
      </div>
      <!--<div class="slider-five-bts-bx" style="display:none">
        <div class="slider-five-btns">
          <ul>
            <li>
              <div class="icon-30k-hours">
                <p>Learning<br />Developed</p>
              </div>
            </li>
            <li>
              <div class="icon-30k-hours1">
                <p>Legacy<br />content<br />modernized</p>
              </div>
            </li>
            <li>
              <div class="icon-30k-hours2">
                <p>200+ consulting<br />engagements</p>
              </div>
            </li>
            <li>
              <div class="icon-30k-hours3">
                <p>2.5m employees<br />Achieve<br />performance<br />improvement</p>
              </div>
            </li>
            <li>
              <div class="icon-30k-hours4">
                <p>30% -50%<br />gain in time<br />to efficiency</p>
              </div>
            </li>
          </ul>
        </div>
      </div>-->
    </div>
    
    <!--Header End-->
    <div id="page" class="pos-rel min-h">
      <div class="bgt">
        <h2>Mission</h2>
        <p>To provide quality training programs that focus on enhancing and increasing our <br />employees’ expertise and knowledge by equipping them with the necessary skills <br />to become more productive in meeting future challenges, goals and personal growth</p>
        <!--<a href="javascript:void(0);">Read More</a><a href="javascript:void(0);">Start a Demo</a>-->
      </div>
      <div class="clear"></div>
      <!--<div class="slide-container">
    <nav class="slides-pagination"><a href="#1" class="">1</a><a href="#2" class="current">2</a><a href="#3">3</a><a href="#4">4</a><a href="#5">5</a></nav>
  </div>-->
    </div>
  </div>
  <!--Five Buttons End-->
  <!--About Us-->
  <div class="about">
    <div id="page">
      <h3>Word & Brown Service of Unequalled Excellence</h3>
      <div class="content">
        <p>We've committed ourselves for more than 20 years to delivering Service of Unequalled Excellence to our clients. We pride ourselves in providing ground-breaking tools and technology, superior sales and service solutions, and excellent customer service. Our employees demonstrate an uncompromising focus on service and professionalism in all their actions.
We recognize that our employees are our greatest asset and encourage their growth and satisfaction throughout the company. We believe that allegiance to our customers, staff, and professional values is the key to professional success and satisfaction. </p>
        <!--<a href="javascript:void(0);">Read More</a><a href="javascript:void(0);">Start a Demo</a>-->
      </div>
      
    </div>
  </div>
  <!--features Start-->
  <div class="solutions">
    <div id="page">
      <h3>TRAINING PROGRAMS<i></i></h3>
      <ul class="slider_training">
        
        <li> <h3>Certified Management Career Development Program</h3>
          <p>This program was developed for individuals on your team who have demonstrated a high degree of on-the-job success and have the potential to become future leaders. This multi-tiered career-path training program prepares and develops Individual Contributors to grow their skills to be considered for future Team Leader positions; Team Leader to 
Supervisor positions; and Supervisor to Manager positions.  </p>
        </li>
        <li> <h3>FREE Information Technology Training</h3>
          <p>The W&B Companies entered into an agreement with Saisoft Corporation that offers FREE high-quality IT training through a grant with the State of California. The courses offered include the latest technologies and provide W&B IT professionals the opportunity to improve their job skills, productivity, and personal growth.</p>
        </li>
        <li> <h3>Customized Departmental Training</h3>
          <p>Provides customized instructor-led training for your team in areas of your business that requires specific on-the-job training support. Examples include cross-training team members, creating or updating training documents, systems training, developing  quick-reference tools, eLearning modules, etc. </p>
        </li>
         <li> <h3>Employee Skills and Knowledge Training</h3>
          <p>Provides employees with skill-based trainings that will help them to improve their on-the-job performance while increasing productivity. Examples include Time Management, Business Writing Skills, Communication Skills, Ownership & Accountability, Facilitating Meetings, Multi-Tasking, Listening Skills, etc.  </p>
        </li>
         <li> <h3>Management & Leadership Skills Training</h3>
          <p> Provides managers and supervisors the basic fundamental skills to succeed in their leadership roles and become better equipped to manage, motivate, and grow their teams. Examples include Interviewing skills, Coaching, Time Management, Communications, Boot Camp for New Workplace Leaders, Delegation, Managing Meetings, Team Building, Motivation, Addressing Employee Performance Issues, etc. </p>
        </li>
         <li> <h3>Manager Lunch & Learn Training Program</h3>
          <p> Provides busy managers the opportunity to attend an interactive one hour training event focused on enhancing their leadership skills.  Examples include Improving your Coaching Style, Managing Multigenerational Teams, Motivating and Optimizing Performance, Giving Effective Feedback and Managing Change.</p>
        </li>
         <li> <h3>Management Guest Trainer Program</h3>
          <p>Provides managers the opportunity to step outside their normal daily duties and grow their professional skills by co-facilitating an instructor-led management training class. All class materials and speaker notes are provided by the Corporate Training & Development Department. </p>
        </li>
         <li> <h3>The Word & Brown Company’s History Training</h3>
          <p>Provides  Word & Brown employees an opportunity to  hear more about the history of The Word & Brown Companies’ as well as an overview of all of the various Business Units, market niches, products, and the clients they serve. </p>
        </li>
         <li> <h3>On-Site Microsoft Office Skills Training: </h3>
          <p>This training, delivered by Soft-Train Corporation, offers employees the opportunity to increase their MS-Office productivity skills at the Beginner, Intermediate and Advanced proficiency levels.  </p>
        </li>
        <li> <h3>Insurance Industry Training</h3>
          <p>Provides an overview of current events and up-to-date regulatory information on several of the rapid changes occurring in the Insurance Industry.   </p>
        </li>
         <li> <h3>New Hire Orientation Training</h3>
          <p>Offers new hires the opportunity to  learn about The Word & Brown Companies, Benefits, Events, Compliance, W&B Toastmasters Club, Safety and Emergency procedures, Facilities, how to use UltiPro, and other important on-boarding information. </p>
        </li>
        <li> <h3>Compliance Training</h3>
          <p>Provides regulatory information to keep management and employees up-to-date on all compliance requirements. Some of the course offerings include  training on the Family and Medical Leave Act (FMLA), Americans with Disabilities Act (ADA), Preventing Workplace Harassment, Understanding Employment Laws, HIPAA, and Security Awareness. </p>
        </li>
         <li> <h3>Special Training Program for On-Boarding New Hires</h3>
          <p> Provides managers with support to accelerate a new hire’s development during the first 30-60 days. Examples include providing training on soft skills, products, policies, processes, services, systems, compliance, etc. </p>
        </li>
        
      </ul>
    </div>
  </div>

  
  <!--Footer-->
  
  <footer>
    <div id="page">
      <div class="f-left">Copyright &copy; 2014 wordandbrowncompanies.com. All rights reserved. </div>
      <div class="arrow"></div>
      <div class="f-right botm_foot">Powered By GnosisConnect LMS<span></span></div>
    </div>
  </footer>
  <div class="drop" title="Top"></div>
</div>


<script>

$(document).ready(function(){
	slider = $('.bxslider').bxSlider();
	slider_training = $('.slider_training').bxSlider({
		minSlides: 3,
  		maxSlides: 3,
		slideWidth: 435,
 		slideMargin: 5
	});
	slider.startAuto();
	slider_training.startAuto();
	if($('.username-error').length > 0 && $('.username-error').html() != '' && $('.password-error').length > 0 && $('.password-error').html() == ''){
	  $('.password-error').hide();
	}
	
});

$(window).bind("load", function() {
	var hgt = $('.bx-viewport').css('height');
	$('.pos-fix').css('height',parseInt(hgt)+'px');
	$('.slider-five-bts-bx').fadeIn();
	

	if($('.username-error').length > 0 && $('.username-error').html() != ''){
	  $('.login-open').addClass('show');
	}

});

$('.callus-now a').click(function(){
	$('.callus-now-bx').toggle('slow');	
});

$('.drop').click(function(){
  $('html,body').animate({scrollTop:"0px"},'slow')
});
$(window).scroll(function(){
if($(this).scrollTop() >0){
	$('.drop').show();
}
else{
	$('.drop').hide();
}
});

$('a.login-open').click(function(){
	$(this).toggleClass('show');
	$('.login-box').toggle('slow');
	$(this).parent('.login-link').toggleClass('login');
	$(this).parent('.login-link').find('.right-down-arrow').toggleClass('right-close-arrow');
	$('.password-error').html();
	$('.password-error').hide();
	$('.username-error').html();
	$('.username-error').hide();
	$('.email-error').hide();
	var forgotEmail = $.trim($("#forgot-passowrd-email").val());
	var email = $.trim($("#login-email").val());
	var password = $.trim($("#login-password").val());
	if(email != '' && email.toLowerCase () != 'username' ){
		$("#login-email").val('');
	}
	if(forgotEmail != '' && forgotEmail != 'Email / Username' ){
		$("#forgot-passowrd-email").val('');
	}
	if(password != '' && password.toLowerCase () != 'password' ){
		$("#login-password").val('');
	}
});

$('.forgot-password a').click(function(){
	$(this).parents('#loginform').hide('slow');
	$(this).parents('.login-box').find('#forgot-password-form').show('slow');
	$('.email-error').hide();
	var forgotEmail = $.trim($("#forgot-passowrd-email").val());
	$("#forgot-passowrd-email").val('Email / Username');
});

$('.forgot-login-password a').click(function(){
	$(this).parents('#forgot-password-form').hide('slow');
	$(this).parents('.login-box').find('#loginform').show('slow');
	var forgotEmail = $.trim($("#forgot-passowrd-email").val());
	var email = $.trim($("#login-email").val());
	var password = $.trim($("#login-password").val());
	$("#login-email").val('Username');
	$("#login-password").val('Password');
});

$(document).on("click","#forgot-password-click",function(event){
	event.preventDefault();
	var email = $.trim($("#forgot-passowrd-email").val());
	var err = 0;
	var msg = '';
	if(email == '' || email == 'Email / Username' ){
		msg = "<?php echo get_string('provide_email_username'); ?>";
		$('.email-error').html(msg);
		$('.email-error').show();
		return false;
	}
	$.ajax({
			url:'<?php echo $CFG->wwwroot;?>/login/forgot_password.php?email='+email,
			type:'POST',
			success:function(data){
				if(data == 0 || data == '0'){
					msg = "<?php echo get_string('provide_email_username'); ?>";
					$('.email-error').html(msg);
					$('.email-error').show();
					return false;
				}else if(data == 1 || data == '1'){
					msg = "<?php echo get_string('user_doesnot_exists'); ?>";
					$('.email-error').html(msg);
					$('.email-error').show();
					return false;
				}else if(data == 'SUCCESS'){
					msg = "<?php echo get_string('mail_have_been_sent'); ?>";
					$('.email-error').addClass('success');
					$('.email-error').html(msg);
					$('.email-error').show();
					return false;
				}else if(data == 'FAILED'){
					msg = "<?php echo get_string('error_sending_email'); ?>";
					$('.email-error').addClass('success');
					$('.email-error').html(msg);
					$('.email-error').show();
					return false;
				}
			}
	});
	return false;
});
$(document).on("click","#log-in",function(event){
	event.preventDefault();
	var email = $.trim($("#login-email").val());
	var password = $.trim($("#login-password").val());
	var err = 0;
	var msg = '';
	if(email == '' || email.toLowerCase () == 'username' ){
		msg = "<?php echo get_string('provide_username'); ?>";
		$('.username-error').html(msg);
		$('.username-error').show();
		err = 1;
	}else{
		$('.username-error').html();
		$('.username-error').hide();
		err = 0;
	}

	if(password == '' || password.toLowerCase ()== 'password' ){
		msg = "<?php echo get_string('provide_password'); ?>";
		$('.password-error').html(msg);
		$('.password-error').show();
		err = 1;
	}else{
		$('.password-error').html();
		$('.password-error').hide();
		err = 0;
	}
	if(err == 1){
		return false;
	}else{
		$("#login-form").submit();
	}
});
</script>
</body>
</html>
